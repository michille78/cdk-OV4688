#!/bin/bash

# Set constants
CDK_TOOLS_DIR=../tools
CDK_MDK_DIR=../mdk
CDK_PLATFORM=`uname -s`

rm -f ./flash.scr

MVCMDSIZE=`du -b "$1"|awk '{print $1}'`

cat ../mdk/common/scripts/debug/default_flash_myriad2_mdbg2.scr | \
		sed "s,XX_FLASHER_ELF_XX,../mdk/common/utils/jtag_flasher/flasher_ma2150.elf," | \
		sed "s,XX_TARGET_MVCMD_SIZE_XX,$MVCMDSIZE," | \
		sed "s,XX_TARGET_MVCMD_XX,$1," > flash.scr

if echo "$CDK_PLATFORM" | grep Linux 2>&1 >/dev/null
then
  $CDK_TOOLS_DIR/linux64/bin/moviDebug2 --script ./flash.scr -cv:ma2150 -ddrInit:../mdk/common/utils/ddrInit/ddrInit_ma2150.elf -srvIP:127.0.0.1 -serverPort:30001
else
  $CDK_TOOLS_DIR/win32/bin/moviDebug2 --script ./flash.scr -cv:ma2150 -ddrInit:../mdk/common/utils/ddrInit/ddrInit_ma2150.elf -srvIP:127.0.0.1 -serverPort:30001
fi		

if cmp $1 $1.readback 2>&1 >/dev/null
then
  echo "=> Reading back from DDR unchanged [OK]"
  rm $1.readback
else
  echo "=> Reading back from DDR was different! [BAD]"
  exit 1
fi
