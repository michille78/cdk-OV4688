#!/bin/bash

# Set constants
CDK_TOOLS_DIR=../tools
CDK_PLATFORM=`uname -s`

# Generate debug script
rm -f debugScr.scr

echo -n $'namespace path ::mdbg\nuart off\nbreset\ntarget LOS\nloadfile '$1$'\npipe create LOS\npipe configure LOS -readsym mvConsoleTxQueue -stdout\npipe create LRT\npipe configure LRT -readsym lrt_mvConsoleTxQueue -stdout\nrun\n' > debugScr.scr

# Start moviDebug
if echo "$CDK_PLATFORM" | grep -q Linux ; then
OS=linux64
else
OS=win32
fi

$CDK_TOOLS_DIR/$OS/bin/moviDebug2 --interactive --script debugScr.scr -cv:ma2450 -ddrInit:../mdk/common/utils/ddrInit/ddrInit_ma2450.elf -srvIP:127.0.0.1 --server-port 30001

