#!/bin/bash

# Set constants
CDK_TOOLS_DIR=../tools
CDK_PLATFORM=`uname -s`

# Start moviDebug

if echo "$CDK_PLATFORM" | grep Linux 2>&1 >/dev/null
then
  $CDK_TOOLS_DIR/linux64/bin/moviDebugServer -tcpPort:30001
else
  $CDK_TOOLS_DIR/win32/bin/moviDebugServer -tcpPort:30001
fi
