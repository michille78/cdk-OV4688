# 1. Update repo and clean, clean elfs
make clean
make mrproper

mkdir -p ./cdk_elfs

rm -rf ./cdk_elfs/*.elf
rm -rf ./cdk_elfs/*.mvcmd


# 3.2 Test A5658_B5658_C5658_ma2150
make clean
make mrproper
make configs/rtems_sensor_sdk_A4x88_B4x88_ma2150
make install_header
make -j4
make configs/rtems_mv182_A4x88_B4x88_ma2150
make -j4
cd mdk/projects/Ipipe2/apps/GuzziSpi_mv182_A4x88_B4x88_ma2150_multispec/
make clean
make -j4 all MV_SOC_REV=ma2150
make spi-boot
# Copy elf
cp output/GuzziSpi_mv182_A4x88_B4x88_ma2150_multispec.elf ../../../../../cdk_elfs/
cp output/GuzziSpi_mv182_A4x88_B4x88_ma2150_multispec.mvcmd ../../../../../cdk_elfs/
# Go back to root
cd ../../../../../





