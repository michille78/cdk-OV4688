/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file plat_csi2.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <platform/simulator/drv/inc/plat_csi2_resource.h>

#include <utils/mms_debug.h>

mmsdbg_define_variable(
        simulator_drv_test_plat_csi2,
        DL_DEFAULT,
        0,
        "simulator_drv_test_plat_csi2",
        "Camera module test application."
    );
#define MMSDEBUGLEVEL mmsdbg_use_variable(simulator_drv_test_plat_csi2)

int simulator_drv_csi_cfg(hat_cm_socket_csi2_cfg_t* cfg)
{
    mmsdbg(DL_PRINT, "//VIV PLAT: CSI2: csi_cfg executed: %d %d", cfg->a, cfg->b);
    return 0;
}

int simulator_drv_csi_ctx_cfg(hat_cm_socket_csi2_ctx_cfg_t* ctx_cfg)
{
    mmsdbg(DL_PRINT, "//VIV PLAT: CSI2: csi_ctx_cfg executed: %d %d", ctx_cfg->c, ctx_cfg->d);
    return 0;
}

int simulator_drv_csi_ctx_start(void)
{
    mmsdbg(DL_PRINT, "//VIV PLAT: CSI2: csi_ctx_start executed");
    return 0;
}

int simulator_drv_csi_ctx_stop(void)
{
    mmsdbg(DL_PRINT, "//VIV PLAT: CSI2: csi_ctx_stop executed");
    return 0;
}

int simulator_drv_csi_ctx_imm_stop(void)
{
    mmsdbg(DL_PRINT, "//VIV PLAT: CSI2: csi_ctx_imm_stop executed");
    return 0;
}

int simulator_drv_csi_ctx_set_buffer(void* buff)
{
    mmsdbg(DL_PRINT, "//VIV PLAT: CSI2: csi_ctx_set_buffer executed: %010p", buff);
    return 0;
}

int simulator_drv_csi_ctx_enable_event(void)
{
    mmsdbg(DL_PRINT, "//VIV PLAT: CSI2: csi_ctx_enable_event executed");
    return 0;
}

int simulator_drv_csi_ctx_disable_event(void)
{
    mmsdbg(DL_PRINT, "//VIV PLAT: CSI2: csi_ctx_disable_event executed");
    return 0;
}

int simulator_drv_csi2_init(plat_res_desc_csi2_t *prms, hat_cm_socket_csi2_hndl_t *hndl)
{
    mmsdbg(DL_PRINT, "//VIV PLAT: CSI2: Create CSI2 with params: %s %d",
        prms->name,
        prms->ctx_num);

    //TODO: - The real function should call CSI2 platform driver with
    // these parameters and return handle to all CSI2 operations
    hndl->ctx_num = prms->ctx_num;
    hndl->csi_cfg = simulator_drv_csi_cfg;
    hndl->csi_ctx_cfg = simulator_drv_csi_ctx_cfg;
    hndl->csi_ctx_start = simulator_drv_csi_ctx_start;
    hndl->csi_ctx_stop = simulator_drv_csi_ctx_stop;
    hndl->csi_ctx_imm_stop = simulator_drv_csi_ctx_imm_stop;
    hndl->csi_ctx_set_buffer = simulator_drv_csi_ctx_set_buffer;
    hndl->csi_ctx_enable_event = simulator_drv_csi_ctx_enable_event;
    hndl->csi_ctx_disable_event = simulator_drv_csi_ctx_disable_event;

    mmsdbg(DL_PRINT, "//VIV PLAT: CSI2: CSI2 handle created at %010p", hndl);
    return 0;
}
