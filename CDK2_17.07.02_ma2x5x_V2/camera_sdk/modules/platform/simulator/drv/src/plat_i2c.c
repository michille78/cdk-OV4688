/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file plat_i2c.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <osal/osal_stdtypes.h>
#include <osal/osal_stdlib.h>
#include <utils/mms_debug.h>

#include <platform/simulator/drv/inc/plat_i2c_resource.h>

#define container_of(PTR,TYPE,FIELD) \
    (TYPE *)((char *)(PTR) - (char *)&(((TYPE *)0)->FIELD))

#define plat_to_dw(PLAT_DEV) container_of((PLAT_DEV), dw_i2c_device_t, plat_dev)

typedef struct {
    hat_cm_socket_i2c_dev_t plat_dev;
    unsigned int slave_addr;
    unsigned int slave_addr_bit_size;
    unsigned int reg_addr_size;
} dw_i2c_device_t;

extern int clp_camera_select;

mmsdbg_define_variable(
        simulator_drv_test_plat_i2c,
        DL_DEFAULT,
        0,
        "simulator_drv_plat_i2c",
        "I2C platform implementation using DevWare API."
    );
#define MMSDEBUGLEVEL mmsdbg_use_variable(simulator_drv_test_plat_i2c)

int dw_i2c_write(
        uint32 bus,
        uint32 slave_addr,
        uint32 reg_addr,
        uint32 reg_addr_size,
        uint32 num,
        uint8 *val
    );
int dw_i2c_read(
        uint32 bus,
        uint32 slave_addr,
        uint32 reg_addr,
        uint32 reg_addr_size,
        uint32 num,
        uint8 *val
    );

static int simulator_i2c_dev_write(
        hat_cm_socket_i2c_dev_t *plat_dev,
        uint32 reg,
        uint32 num,
        uint8 *val
    )
{
    dw_i2c_device_t *dev;
    dev = plat_to_dw(plat_dev);
    mmsdbg(
            DL_MESSAGE,
            "SA=0x%04x, SASZ=%d, RASZ=%d, RA=0x%04x, N=%d",
            dev->slave_addr,
            dev->slave_addr_bit_size,
            dev->reg_addr_size,
            reg,
            num
        );
    mmsdbgdump(DL_MESSAGE, "V", val, num);
    return dw_i2c_write(
            0,                  /* bus */
            dev->slave_addr,    /* slave_addr */
            reg,                /* reg_addr */
            dev->reg_addr_size, /* reg_addr_size */
            num,                /* num */
            val                 /* val */
        );
}

static int simulator_i2c_dev_read(
        hat_cm_socket_i2c_dev_t *plat_dev,
        uint32 reg,
        uint32 num,
        uint8 *val
    )
{
    dw_i2c_device_t *dev;
    int err;

    dev = plat_to_dw(plat_dev);

    err = dw_i2c_read(
            0,                  /* bus */
            dev->slave_addr,    /* slave_addr */
            reg,                /* reg_addr */
            dev->reg_addr_size, /* reg_addr_size */
            num,                /* num */
            val                 /* val */
        );
    if (err) {
        mmsdbg(DL_ERROR, "Failed to read from I2C!");
        /* TODO: return -1; */
        switch(clp_camera_select) {
        case 0:
            val[0] = 0x31; val[1] = 0xFE;
            break;
        case 1:
            val[0] = 0x02; val[1] = 0x14;
            break;
        case 2:
            val[0] = 0xCE; val[1] = 0xCE;
            break;
        default:
            val[0] = 0x31; val[1] = 0xFE;
            break;
        }
    }

    mmsdbg(
            DL_MESSAGE,
            "SA=0x%04x, SASZ=%d, RASZ=%d, RA=0x%04x, N=%d",
            dev->slave_addr,
            dev->slave_addr_bit_size,
            dev->reg_addr_size,
            reg,
            num
        );
    mmsdbgdump(DL_MESSAGE, "V", val, num);

    return 0;
}

static int simulator_i2c_register_device(hat_cm_socket_i2c_dev_t *plat_dev, hat_socket_comp_cfg_t *params_dev)
{

    return 0;
}

hat_cm_socket_i2c_dev_t * simulator_i2c_dev_init(plat_i2c_dev_params_t *params)
{
    dw_i2c_device_t *dev;

    dev = osal_calloc(1, sizeof (*dev));
    if (!dev) {
        mmsdbg(
                DL_ERROR,
                "Failed to allocate memory for new instance (size=%d)!",
                sizeof (*dev)
            );
        goto exit1;
    }

    dev->slave_addr = params->slave_addr;
    dev->slave_addr_bit_size = params->slave_addr_bit_size;
    dev->reg_addr_size = params->reg_addr_size;

    dev->plat_dev.write = simulator_i2c_dev_write;
    dev->plat_dev.read  = simulator_i2c_dev_read;
    dev->plat_dev.regdev= simulator_i2c_register_device;

    return &dev->plat_dev;
exit1:
    return NULL;
}

int simulator_i2c_dev_deinit(hat_cm_socket_i2c_dev_t *plat_dev)
{
    dw_i2c_device_t *dev;

    dev = plat_to_dw(plat_dev);

    if (NULL == dev)
    {
        mmsdbg(DL_ERROR, "Invalid handle pointer");
        return 1;
    }

    osal_free(dev);

    return 0;
}

