/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file plat_simulator.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <hal/hat_types.h>
#include <platform/inc/platform_all.h>
#include <error_handle/include/error_handle.h>

#include <utils/mms_debug.h>
#include "ipipe.h"

mmsdbg_define_variable(
        plat_simulator,
        DL_DEFAULT,
        0,
        "plat_simulator",
        "Platform simulator file"
    );
#define MMSDEBUGLEVEL mmsdbg_use_variable(plat_simulator)

PLAT_PH_SOCKET_T platform_cm_cam_to_sock(uint32 cam_id)
{
    if (cam_id == 1) {
        return PH_SOCKET_2;
    } else if(cam_id == 2){
        return PH_SOCKET_3;
    }

    return PH_SOCKET_1;
}


static int controler_no_to_mipi_rx_device_id_map[] = {
    IC_SIPP_DEVICE0,
    IC_SIPP_DEVICE0, /* TODO */
    IC_SIPP_DEVICE1,
    IC_SIPP_DEVICE2,
    IC_SIPP_DEVICE0, /* TODO */
    IC_SIPP_DEVICE0, /* TODO */
};

int platform_map_ctrlNo_2_rxDevice(uint32_t controllerNo)
{
    return controler_no_to_mipi_rx_device_id_map[controllerNo];
}

#if 0
static plat_i2c_hndl_t i2c_hndl;
static plat_i2c_create_params_t i2c_prms = {
    .num_channels = 3,
};


static plat_i2c_bus_hndl_t bus_hndl;
static plat_i2c_bus_create_params_t bus_prms = {
    .channel_idx = 2,
    .channel_speed = 400000,
};


static plat_i2c_bus_dev_hndl_t dev_hndl;
static plat_i2c_dev_register_param_t dev_reg_prms = {
    .dev_address = 0x23,
    .dev_address_len = 8,
};


void transaction_callback(uint32 reg_addr, uint32 num, void* data)
{
    mmsdbg(DL_MESSAGE, "I2C transaction callback!");
}
plat_bus_callback_t bus_cb;
#endif

#include <platform/inc/plat_hw_resource.h>
extern int clp_camera_select;
static void overwrite_i2c_slave_addr(plat_socket_res_desc_t *socket)
{
    plat_i2c_dev_desc_t *i2c_sen_enable, *i2c_sen_cci,
        *i2c_lens_enable, *i2c_lens_cci;

	if (!socket->sensor_res) {
		mmsdbg(DL_ERROR, "skip SOCK");
		return;
	}

        //i2c_sen_enable  = (*(socket->sensor_res))[5].res_desc->init;
        i2c_sen_cci     = (*(socket->sensor_res))[9].res_desc->init;
        //i2c_lens_enable = (*(socket->lens_res))[3].res_desc->init;
        i2c_lens_cci    = (*(socket->lens_res))[4].res_desc->init;

    switch (clp_camera_select) {
        case 0: /* AR1330 */
          //  i2c_sen_enable->params.slave_addr =
            i2c_sen_cci->params.slave_addr =
                0x6c;
           // i2c_lens_enable->params.slave_addr =
            i2c_lens_cci->params.slave_addr =
                0x6c;
            i2c_lens_cci->params.reg_addr_size = 2;
            break;
        case 1: /* IMX214 */
          //  i2c_sen_enable->params.slave_addr =
            i2c_sen_cci->params.slave_addr =
                0x20;
         //   i2c_lens_enable->params.slave_addr =
            i2c_lens_cci->params.slave_addr =
                0x18;
            i2c_lens_cci->params.reg_addr_size = 1;
            break;
        case 2: /* IMX214 New */
         //   i2c_sen_enable->params.slave_addr =
            i2c_sen_cci->params.slave_addr =
                0x26;
         //   i2c_lens_enable->params.slave_addr =
            i2c_lens_cci->params.slave_addr =
                0xE4;
            i2c_lens_cci->params.reg_addr_size = 1;
            break;
        default:
            mmsdbg(
                    DL_ERROR,
                    "Unknown camera (clp_camera_select=%d)!",
                    clp_camera_select
                );
    }
}

static plat_socket_res_desc_t simulator_socket0_res;
extern plat_socket_res_desc_t simulator_socket1_res;

static plat_socket_obj_t simulator_plat_sockets[PH_SOCKET_MAX];

int board_platform_init(plat_socket_obj_t **sockets)
{
    int err = 0;
    int i;

    //TODO: Need to init platform drivers here
#if 0
    err = plat_i2c_create(&i2c_hndl, i2c_prms);
    err = plat_i2c_bus_create(i2c_hndl, &bus_hndl, bus_prms);
    err = plat_bus_i2c_acquire(bus_hndl);

    err = plat_dev_i2c_dev_create(bus_hndl, plat_i2c_dev_hndl_t* h, plat_i2c_dev_create_param_t param);

    bus_cb = transaction_callback;
    err = plat_bus_i2c_register_dev(bus_hndl, &dev_hndl, dev_reg_prms, bus_cb);
    int plat_dev_i2c_open (plat_i2c_dev_hndl_t* h, plat_dev_callback_t callback, plat_i2c_client_hndl_t client_prv);     // return 0 for success
#endif

    // Init all sockets with dummy socket
    for (i = PH_SOCKET_DUMMY; i < PH_SOCKET_MAX; i++) {
        simulator_plat_sockets[i].soc_res = &simulator_socket0_res;
    }
    // Initialize Platform specific sockets
    simulator_plat_sockets[0].soc_res = &simulator_socket0_res;
    simulator_plat_sockets[1].soc_res = &simulator_socket1_res;


    for (i = PH_SOCKET_DUMMY; i < PH_SOCKET_MAX; i++) {
        mmsdbg(DL_MESSAGE, "\n\n//VIV PLAT: >>>>>>>>>>>>>>>>>> Try to init socket %d", i);
        overwrite_i2c_slave_addr(simulator_plat_sockets[i].soc_res);
        err = plat_socket_init(&simulator_plat_sockets[i]);
        GOTO_EXIT_IF(0 != err, 1);
    }

    *sockets = simulator_plat_sockets;

    return 0;

EXIT_1:
    mmsdbg(DL_ERROR, "//VIV PLAT: Exit Err");
    return -1;
}


int board_platform_deinit(plat_socket_obj_t *sockets)
{
    int err = 0;
    int i;

    for (i = PH_SOCKET_DUMMY; i < PH_SOCKET_MAX; i++) {
        mmsdbg(DL_MESSAGE, "\n\n//VIV PLAT: >>>>>>>>>>>>>>>>>> Try to deinit socket %d", i);
        err = sockets[i].deinit(&sockets[i]);
        GOTO_EXIT_IF(0 != err, 1);
    }

    return 0;
EXIT_1:
    mmsdbg(DL_ERROR, "//VIV PLAT: Exit Err");
    return -1;
}
