/* =============================================================================
* Copyright (c) 2013-2015 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file plat_mv182_ov4188.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <hal/hat_types.h>
#include <platform/inc/platform_all.h>
#include <error_handle/include/error_handle.h>
#include "ipipe.h"

#include <utils/mms_debug.h>

mmsdbg_define_variable(
        plat_mv182_ov4188,
        DL_DEFAULT,
        0,
        "plat_mv182_ov4188",
        "Platform mv182_ov4188"
    );
#define MMSDEBUGLEVEL mmsdbg_use_variable(plat_mv182_ov4188)

#if 0
static plat_i2c_hndl_t i2c_hndl;
static plat_i2c_create_params_t i2c_prms = {
    .num_channels = 3,
};


static plat_i2c_bus_hndl_t bus_hndl;
static plat_i2c_bus_create_params_t bus_prms = {
    .channel_idx = 2,
    .channel_speed = 400000,
};


static plat_i2c_bus_dev_hndl_t dev_hndl;
static plat_i2c_dev_register_param_t dev_reg_prms = {
    .dev_address = 0x23,
    .dev_address_len = 8,
};


void transaction_callback(uint32 reg_addr, uint32 num, void* data)
{
    mmsdbg(DL_MESSAGE, "I2C transaction callback!");
}
plat_bus_callback_t bus_cb;
#endif

#include <platform/inc/plat_hw_resource.h>

static plat_socket_res_desc_t mv182_ov4188_socket0_res;
extern plat_socket_res_desc_t mv182_ov4188_socket1_res;
extern plat_socket_res_desc_t mv182_ov4188_socket2_res;
//extern plat_socket_res_desc_t mv182_ov4188_socket3_res;
//extern plat_socket_res_desc_t mv182_ov4188_socket4_res;

extern uint32 do_not_use_i2c_ch_0;

static plat_socket_obj_t mv182_ov4188_plat_sockets[PH_SOCKET_MAX];


////////////////////////////////////////////////////////////////////////////

///////////////  TODO: V.Petrov - please move me to DTP database

PLAT_PH_SOCKET_T platform_cm_cam_to_sock(uint32 cam_id)
{
    if (cam_id == 1) {
        return PH_SOCKET_2;
    } else if(cam_id == 2){
        return PH_SOCKET_3;
    }

    return PH_SOCKET_1;
}


static int controler_no_to_mipi_rx_device_id_map[] = {
    IC_SIPP_DEVICE0,
    IC_SIPP_DEVICE0, /* TODO */
    IC_SIPP_DEVICE1,
    IC_SIPP_DEVICE2,
    IC_SIPP_DEVICE0, /* TODO */
    IC_SIPP_DEVICE0, /* TODO */
};

int platform_map_ctrlNo_2_rxDevice(uint32_t controllerNo)
{
    return controler_no_to_mipi_rx_device_id_map[controllerNo];
}

int board_platform_init(plat_socket_obj_t **sockets)
{
    int err = 0;
    int i;
printf("mv182_ov4188_plat_sockets!\n");
    //TODO: Need to init platform drivers here
#if 0
    err = plat_i2c_create(&i2c_hndl, i2c_prms);
    err = plat_i2c_bus_create(i2c_hndl, &bus_hndl, bus_prms);
    err = plat_bus_i2c_acquire(bus_hndl);

    err = plat_dev_i2c_dev_create(bus_hndl, plat_i2c_dev_hndl_t* h, plat_i2c_dev_create_param_t param);

    bus_cb = transaction_callback;
    err = plat_bus_i2c_register_dev(bus_hndl, &dev_hndl, dev_reg_prms, bus_cb);
    int plat_dev_i2c_open (plat_i2c_dev_hndl_t* h, plat_dev_callback_t callback, plat_i2c_client_hndl_t client_prv);     // return 0 for success
#endif


    // Init all sockets with dummy socket
    for (i = PH_SOCKET_DUMMY; i < PH_SOCKET_MAX; i++) {
        mv182_ov4188_plat_sockets[i].soc_res = &mv182_ov4188_socket0_res;
    }
    // Initialize Platform specific sockets
    mv182_ov4188_plat_sockets[0].soc_res = &mv182_ov4188_socket0_res;
    mv182_ov4188_plat_sockets[1].soc_res = &mv182_ov4188_socket1_res;

    if (!do_not_use_i2c_ch_0) {
        mv182_ov4188_plat_sockets[2].soc_res = &mv182_ov4188_socket2_res;
    } else {
        mv182_ov4188_plat_sockets[2].soc_res = &mv182_ov4188_socket0_res;
    }
//    mv182_ov4188_plat_sockets[3].soc_res = &mv182_ov4188_socket3_res;
//    mv182_ov4188_plat_sockets[4].soc_res = &mv182_ov4188_socket4_res;

    for (i = PH_SOCKET_DUMMY; i < PH_SOCKET_MAX; i++) {
        mmsdbg(DL_MESSAGE, "\n\n//VIV PLAT: >>>>>>>>>>>>>>>>>> Try to init socket %d", i);
        err = plat_socket_init(&mv182_ov4188_plat_sockets[i]);
        GOTO_EXIT_IF(0 != err, 1);
    }

    *sockets = mv182_ov4188_plat_sockets;

    return 0;

EXIT_1:
    mmsdbg(DL_ERROR, "//VIV PLAT: Exit Err");
    return -1;
}


int board_platform_deinit (plat_socket_obj_t *sockets)
{
    //TODO:
    return 0;
}
