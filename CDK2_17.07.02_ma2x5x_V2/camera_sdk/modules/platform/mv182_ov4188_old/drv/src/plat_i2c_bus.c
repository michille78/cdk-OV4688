/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file plat_i2c_bus.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <osal/osal_stdtypes.h>
#include <osal/osal_stdlib.h>
#include <utils/mms_debug.h>


#define MINOR2ADDR(minor)   ((minor)&((1<<10)-1))
#define MINOR2BUS(minor)    (((minor)>>10)&7)
#define MINOR2DRV(minor)    ((minor)>>13)

#include <platform/mv182_ov4188/drv/inc/plat_i2c_resource.h>
mmsdbg_define_variable(
        mv182_ov4188_drv_plat_i2c_bus,
        DL_DEFAULT,
        0,
        "mv182_ov4188_drv_plat_i2c_bus",
        "I2C_BUS platform implementation for mv182_ov4188."
    );
#define MMSDEBUGLEVEL mmsdbg_use_variable(mv182_ov4188_drv_plat_i2c_bus)

#define plat_to_mv182_ov4188(PLAT_DEV) container_of((PLAT_DEV), mv182_ov4188_i2c_bus_device_t, plat_dev)

typedef struct {
    hat_cm_socket_i2c_bus_dev_t plat_dev;
    unsigned int channel;
    unsigned int speed;
    unsigned int reg_addr_size;
    unsigned int slave_addr;
    unsigned int slave_addr_bit_size;
    int is_open;
    int drv_minor;
} mv182_ov4188_i2c_bus_device_t;

////////////////////            I2C              /////////////////////////////////
#include "i2c/system.h"
#include <stdio.h>
#include <stdlib.h>
#include <bsp.h>
#include <icb_defines.h>
#include "mv_types.h"
// API
#include "OsDrvI2cBus.h"
#include "OsDrvI2cMyr2.h"
#include "DrvTimer.h"
#include "DrvGpio.h"

#include <bsp/irq.h>
#include <rtems/libi2c.h>
#include <rtems/libio.h>
#include "OsDrvCpr.h"

/// check DECLARE_I2C_BUS description befora calling
DECLARE_I2C_BUS(plat_i2c_0, 1, I2C_SPEED_SS, 0, 8);
DECLARE_I2C_BUS(plat_i2c_1, 2, I2C_SPEED_SS, 0, 8);
DECLARE_I2C_BUS(plat_i2c_2, 3, I2C_SPEED_SS, 0, 8);

#define LIBI2C_UPDATE_MINOR(m, i2caddr)    \
        ((m & ~( (1<<10) -1 ) )|((i2caddr)&((1<<10)-1)))

static void myr2_i2c_pin_config(uint32_t scl, uint32_t sda, uint32_t mode)
{
    DrvGpioSetMode(scl, mode);
    DrvGpioSetMode(sda, mode);
}

/*=========================================================================*\
| Function:                                                                 |
\*-------------------------------------------------------------------------*/
int myr2_register_i2c0(unsigned int speed, unsigned int slave_addr)

{
    int ret_code;
    char dev_name[7];
    static int node_count = 0;
    static int bus_is_registred = 0;
    static int i2c_busno = 0;

    if(!bus_is_registred) {
        /*
         * register I2C bus
         */
        ret_code = OsDrvLibI2CRegisterBus("/dev/i2c0", (rtems_libi2c_bus_t *)&plat_i2c_0);

        mmsdbg(DL_MESSAGE, "sDrvLibI2CRegisterBus return:%d\n", ret_code);
        if (ret_code < 0)
        {
            mmsdbg(DL_ERROR, "Could not register the bus\n");
            return ret_code;
        }
        bus_is_registred = 1;
        i2c_busno = ret_code;
    }

    sprintf(dev_name,"i2c0_%d", node_count);
    mmsdbg(DL_MESSAGE, "%s: dev_name:%s", __FUNCTION__, dev_name);
    /*
     * register i2cdev to bus, returns minor
     */
    // Returns minor
    ret_code = OsDrvLibI2CRegisterDevice(dev_name,
                                     i2cMyr2DriverDescriptor,
                                     i2c_busno, slave_addr);

    if (ret_code < 0)
    {
        mmsdbg(DL_ERROR, "Could not register the i2c device\n");
        return ret_code;
    }
    else {
        mmsdbg(DL_MESSAGE, "SUCCESSFUL register the i2c device\n");
        node_count++;
    }

    return ret_code;
}

int myr2_register_i2c1(unsigned int speed, unsigned int slave_addr)

{
    int ret_code;
    char dev_name[7];
    static int node_count = 0;
    static int bus_is_registred = 0;
    static int i2c_busno = 0;

    if(!bus_is_registred) {
        /*
         * register I2C bus
         */
        ret_code = OsDrvLibI2CRegisterBus("/dev/i2c1", (rtems_libi2c_bus_t *)&plat_i2c_1);

        mmsdbg(DL_MESSAGE, "sDrvLibI2CRegisterBus return:%d\n", ret_code);
        if (ret_code < 0)
        {
            mmsdbg(DL_ERROR, "Could not register the bus\n");
            return ret_code;
        }
        bus_is_registred = 1;
        i2c_busno = ret_code;
    }

    sprintf(dev_name,"i2c1_%d", node_count);
    mmsdbg(DL_MESSAGE, "%s: dev_name:%s", __FUNCTION__, dev_name);
    /*
     * register i2cdev to bus, returns minor
     */
    // Returns minor
    ret_code = OsDrvLibI2CRegisterDevice(dev_name,
                                     i2cMyr2DriverDescriptor,
                                     i2c_busno, slave_addr);

    if (ret_code < 0)
    {
        mmsdbg(DL_ERROR, "Could not register the i2c device\n");
        return ret_code;
    }
    else {
        mmsdbg(DL_MESSAGE, "SUCCESSFUL register the i2c device\n");
        node_count++;
    }

    return ret_code;
}

int myr2_register_i2c2(unsigned int speed, unsigned int slave_addr)

{
    int ret_code;
    char dev_name[7];
    static int node_count = 0;
    static int bus_is_registred = 0;
    static int i2c_busno = 0;

    if(!bus_is_registred) {
        /*
         * register I2C bus
         */
        ret_code = OsDrvLibI2CRegisterBus("/dev/i2c2", (rtems_libi2c_bus_t *)&plat_i2c_2);

        mmsdbg(DL_MESSAGE, "sDrvLibI2CRegisterBus return:%d\n", ret_code);
        if (ret_code < 0)
        {
            mmsdbg(DL_ERROR, "Could not register the bus\n");
            return ret_code;
        }
        bus_is_registred = 1;
        i2c_busno = ret_code;
    }

    sprintf(dev_name,"i2c2_%d", node_count);
    mmsdbg(DL_MESSAGE, "%s: dev_name:%s", __FUNCTION__, dev_name);
    /*
     * register i2cdev to bus, returns minor
     */
    // Returns minor
    ret_code = OsDrvLibI2CRegisterDevice(dev_name,
                                     i2cMyr2DriverDescriptor,
                                     i2c_busno, slave_addr);

    if (ret_code < 0)
    {
        mmsdbg(DL_ERROR, "Could not register the i2c device\n");
        return ret_code;
    }
    else {
        mmsdbg(DL_MESSAGE, "SUCCESSFUL register the i2c device\n");
        node_count++;
    }

    return ret_code;
}

static int mv182_ov4188_i2c_bus_dev_regbus(hat_cm_socket_i2c_bus_dev_t *plat_dev, hat_socket_comp_cfg_t *params_dev)
{
    mv182_ov4188_i2c_bus_device_t *dev;
    rtems_status_code status;
    dev = plat_to_mv182_ov4188(plat_dev);

    if (dev->is_open) {
        if ((dev->slave_addr_bit_size != params_dev->slave_addr_bit_size) ||
                (dev->reg_addr_size != params_dev->register_size))
        {
            mmsdbg(DL_ERROR, "DEBUG_GUZZI - I2C parameters mismatch:\n"
                    "\tSlave address: 0x%x(0x%x)\n"
                    "\taddr_size: 0x%x(0x%x)\n"
                    "\treg_addr_size: 0x%x(0x%x)\n",
                    dev->slave_addr, params_dev->address,
                    dev->slave_addr_bit_size, params_dev->slave_addr_bit_size,
                    dev->reg_addr_size, params_dev->register_size);
        }
    }

    dev->slave_addr         = params_dev->address;
    dev->slave_addr_bit_size= params_dev->slave_addr_bit_size;
    dev->reg_addr_size      = params_dev->register_size; /* In number of bytes */

    mmsdbg(DL_MESSAGE, "DEBUG_GUZZI: Init I2C_BUS device channel:%d speed:%d slave_addr:0x%x rtems_libi2c_major:%d drv_minor:%d"
            , dev->channel, dev->speed, dev->slave_addr, rtems_libi2c_major, dev->drv_minor);
    if (dev->is_open)
    {
        dev->drv_minor = LIBI2C_UPDATE_MINOR(dev->drv_minor, dev->slave_addr);
        return 0;
    }

    if(dev->channel == 0) {
        myr2_i2c_pin_config(I2C0_CFG0_SCL, I2C0_CFG0_SDA, I2C0_CFG0_MODE);
        if((dev->drv_minor = myr2_register_i2c0(dev->speed, dev->slave_addr)) < 0) {
            mmsdbg(DL_ERROR,"\n minor version < 0 \n");
        }
        else {
            mmsdbg(DL_MESSAGE,"I2C registered! %d\n", dev->drv_minor);
        }
    }
    else if(dev->channel == 1) {
        myr2_i2c_pin_config(I2C1_CFG0_SCL, I2C1_CFG0_SDA, I2C1_CFG0_MODE);
        if((dev->drv_minor = myr2_register_i2c1(dev->speed, dev->slave_addr)) < 0) {
            mmsdbg(DL_ERROR,"\n minor version < 0 \n");
        }
        else {
            mmsdbg(DL_MESSAGE,"I2C registered! %d\n", dev->drv_minor);
        }
    }
    else if(dev->channel == 2) {
        myr2_i2c_pin_config(I2C2_CFG1_SCL, I2C2_CFG1_SDA, I2C2_CFG1_MODE);
        if((dev->drv_minor = myr2_register_i2c2(dev->speed, dev->slave_addr)) < 0) {
            mmsdbg(DL_ERROR,"\n minor version < 0 \n");
        }
        else {
            mmsdbg(DL_MESSAGE,"I2C registered! %d\n", dev->drv_minor);
        }
    }
    else {
        mmsdbg(DL_ERROR, "Invalid dev->channel index:%d!!!!!", dev->channel);
        return -1;
    }

    // open
    if ((status = rtems_io_open(rtems_libi2c_major, dev->drv_minor, NULL)) != RTEMS_SUCCESSFUL) {
        mmsdbg(DL_ERROR,"rtems_io_open failed with sc %d\n", status);
    }

    dev->is_open = 1;
    return 0;
}

static int mv182_ov4188_i2c_bus_dev_write(hat_cm_socket_i2c_bus_dev_t *plat_dev, uint32 reg, uint32 num, uint8 *val)
{
    mv182_ov4188_i2c_bus_device_t *dev;
    uint8 ulData[20], *cp;
    rtems_libio_rw_args_t wargs;
    rtems_status_code status;
    dev = plat_to_mv182_ov4188(plat_dev);

//    mmsdbg(
//            DL_PRINT,
//            "CH:%d SP:%d SA=0x%04x, SASZ=%d, RA=0x%04x, RASZ=%d, N=%d",
//            dev->channel,
//            dev->speed,
//            dev->slave_addr,
//            dev->slave_addr_bit_size,
//            reg,
//            dev->reg_addr_size,
//            num
//        );
//    mmsdbgdump(DL_PRINT, "V", val, num);

    PROFILE_ADD(PROFILE_ID_I2C_TRANSACTION_WR, reg, (num<<16)|((uint32)val[0]));
    if (num > (sizeof(ulData)-dev->reg_addr_size))
    {
        mmsdbg  (DL_ERROR,"I2C ERROR bus: %d drv: %d addr: 0x%x",
                    MINOR2BUS(dev->drv_minor),
                    MINOR2DRV(dev->drv_minor),
                    MINOR2ADDR(dev->drv_minor)
                );

        mmsdbg(DL_ERROR,"I2C: trying to write too much data: %d Bytes to device addr %d on channel %d, ",
               num, dev->slave_addr, dev->channel);
        mmsdbg(DL_ERROR,"I2C: Truncated to %d\n", (sizeof(ulData)-dev->reg_addr_size));
        num = (sizeof(ulData)-dev->reg_addr_size);
    }
    cp = ulData;
    wargs.count = dev->reg_addr_size + num; // +1 for data value

    if(dev->reg_addr_size == 2) {
        *cp++ = (uint8)(reg>>8);
    }
    *cp++ = (uint8)(reg);

    while (num--)
        *cp++ = *val++;

    wargs.buffer = (char*)ulData;
    //mmsdbgdump(DL_PRINT, "start OsDrvIOWrite data", wargs.buffer, wargs.count);
    status = OsDrvIOWrite(rtems_libi2c_major, dev->drv_minor, &wargs);
    if (status != RTEMS_SUCCESSFUL)
    {
        mmsdbg(DL_ERROR,"not able to write to device (bus: %d drv: %d addr: 0x%x) status:%d\n",
               MINOR2BUS(dev->drv_minor),
               MINOR2DRV(dev->drv_minor),
               MINOR2ADDR(dev->drv_minor),
               status
               );
        return -1;
    }
    if (wargs.bytes_moved != wargs.count)
    {
        mmsdbg(DL_ERROR,"Unable to send all %d bytes. %d sent \n",
                wargs.count,
                wargs.bytes_moved);
        return -1;
    }
    PROFILE_ADD(PROFILE_ID_I2C_TRANSACTION_WR, 0xff, MINOR2ADDR(dev->drv_minor));
    return 0;
}
static int mv182_ov4188_i2c_bus_dev_read(hat_cm_socket_i2c_bus_dev_t *plat_dev, uint32 reg, uint32 num, uint8 *val)
{
    mv182_ov4188_i2c_bus_device_t *dev;
    dev = plat_to_mv182_ov4188(plat_dev);
    rtems_status_code status;
    rtems_libio_rw_args_t wargs;
    uint8 uData[5], *cp = uData;

    //set read address using write operation
    wargs.count = dev->reg_addr_size;
    if(wargs.count == 2) {
        *cp++ = (uint8)(reg>>8);
    }
    *cp++ = (uint8)(reg);
    wargs.buffer = (char*)uData;
    //mmsdbgdump(DL_PRINT, "start OsDrvIOWrite data", wargs.buffer, wargs.count);

    PROFILE_ADD(PROFILE_ID_I2C_TRANSACTION_RD, 0, 0);
    status = OsDrvIOWrite(rtems_libi2c_major, dev->drv_minor, &wargs);
    if (status != RTEMS_SUCCESSFUL)
    {
        mmsdbg(DL_ERROR, "cannot setup device address (bus: %d drv: %d addr: 0x%x) status:%d\n",
               MINOR2BUS(dev->drv_minor),
               MINOR2DRV(dev->drv_minor),
               MINOR2ADDR(dev->drv_minor),
               status);
        return -1;
    }
    if (wargs.bytes_moved != wargs.count)
    {
        mmsdbg(DL_ERROR, "Unable to write all %d bytes. %d sent \n",
                wargs.count,
                wargs.bytes_moved);
        return -1;
    }

    //read required address
    wargs.count = num;
    wargs.buffer = (char*)val;
    //mmsdbg(DL_PRINT, "start OsDrvIORead num:%d", num);
    status = OsDrvIORead(rtems_libi2c_major, dev->drv_minor, &wargs);
    if (status != RTEMS_SUCCESSFUL)
    {
        mmsdbg(DL_ERROR,"not able to read from device status:%d\n", status);
        return -1;
    }
    if (wargs.bytes_moved != wargs.count)
    {
        mmsdbg(DL_ERROR,"Unable to read all %d bytes. %d sent \n",
                wargs.count,
                wargs.bytes_moved);
        return -1;
    }
//    mmsdbg(
//            DL_PRINT,
//            "SA=0x%04x, SASZ=%d, RASZ=%d, RA=0x%04x, N=%d",
//            dev->slave_addr,
//            dev->slave_addr_bit_size,
//            dev->reg_addr_size,
//            reg,
//            num
//        );
//    mmsdbgdump(DL_PRINT, "V", val, num);
    PROFILE_ADD(PROFILE_ID_I2C_TRANSACTION_RD, dev->slave_addr, reg);
    return 0;
}

hat_cm_socket_i2c_bus_dev_t * mv182_ov4188_i2c_bus_dev_init(plat_i2c_bus_dev_params_t *params)
{

    mv182_ov4188_i2c_bus_device_t *dev;
    int ret_code;
    static int bus_init = 0;
    dev = osal_calloc(1, sizeof (*dev));
    if (!dev) {
        mmsdbg(
                DL_ERROR,
                "Failed to allocate memory for new instance (size=%d)!",
                sizeof (*dev)
            );
        goto exit1;
    }
    dev->is_open = 0;
    dev->channel = params->channel;
    dev->speed = params->speed;

    mmsdbg(DL_MESSAGE, "DEBUG_GUZZI: Init I2C_BUS device channel:%d speed:%d slave_addr:0x%x slave_addr_bit_size:%d reg_addr_size:%d"
            , dev->channel, dev->speed, dev->slave_addr, dev->slave_addr_bit_size, dev->reg_addr_size );

    dev->plat_dev.regbus = mv182_ov4188_i2c_bus_dev_regbus;
    dev->plat_dev.write = mv182_ov4188_i2c_bus_dev_write;
    dev->plat_dev.read = mv182_ov4188_i2c_bus_dev_read;

    if(!bus_init) {
        // Init CPR Driver in order to be able to get sys clk
#if 0
        if ((rv = OsDrvCprInit()) != OS_MYR_DRV_SUCCESS)
        {
            mmsdbg(DL_ERROR, "Error: OsDrvCprInit() FAILED\n");
            return NULL;
        }

        // Open CPR Driver in order to be able to get sys clk
        if ((rv = OsDrvCprOpen()) != OS_MYR_DRV_SUCCESS)
        {
            mmsdbg(DL_ERROR, "Error: OsDrvCprOpen() FAILED\n");
            return NULL;
        }
#endif
        /*
         * init I2C library (if not already done)
         */
        if((ret_code = OsDrvLibI2CInitialize()) != RTEMS_SUCCESSFUL)
        {
            mmsdbg(DL_ERROR,"rtems_libi2c_initialize FAILED %d \n", ret_code);
        }
        bus_init = 1;
    }
    return &dev->plat_dev;
exit1:
    return NULL;
}

