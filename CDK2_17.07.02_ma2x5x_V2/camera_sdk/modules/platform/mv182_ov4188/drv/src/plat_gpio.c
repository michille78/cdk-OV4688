/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file plat_gpio.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <DrvGpio.h>
#include <osal/osal_stdtypes.h>
#include <osal/osal_stdlib.h>
#include <utils/mms_debug.h>

#include <platform/mv182_ov4188/drv/inc/plat_gpio_resource.h>

#define plat_to_mv182_ov4188(PLAT_DEV) container_of((PLAT_DEV), mv182_ov4188_gpio_device_t, plat_dev)

typedef struct {
    hat_cm_socket_gpio_dev_t plat_dev;
    plat_gpio_dev_params_t   prms;
} mv182_ov4188_gpio_device_t;


mmsdbg_define_variable(
        mv182_ov4188_drv_test_plat_gpio,
        DL_DEFAULT,
        0,
        "mv182_ov4188_drv_test_plat_gpio",
        "Camera module test application."
    );
#define MMSDEBUGLEVEL mmsdbg_use_variable(mv182_ov4188_drv_test_plat_gpio)

#if 0
typedef struct  {
	int (*drv_gpio_create) 	(void); // return 0 for success
	int (*drv_gpio_open)   	(driver_hndl* h, uint32 gpio_no, uint32 mode); 	// return 0 for success
	int (*drv_gpio_acquire)	(driver_hndl  h, uint32 gpio_no, plat_gpio_direction_t direction);
	int (*drv_gpio_set)  	(driver_hndl h, uint32 gpio_no);
	int (*drv_gpio_clear)  	(driver_hndl h);
	int (*drv_gpio_read)  	(driver_hndl h);
	int (*drv_gpio_debaunce)(driver_hndl h, uint32 deb_value);
	int (*drv_gpio_clock)  	(driver_hndl h, uint32 clk_value );
	int (*drv_gpio_release)	(driver_hndl h);
	int (*drv_gpio_close)  	(driver_hndl h);
	int (*drv_gpio_destroy)	(driver_hndl h);
}plat_gpio_ops;
#endif

static inline void l_gpio_set(uint32 gpio_num, uint32 action)
{
    if(action) {
        DrvGpioSetPinHi(gpio_num);
    }
    else {
        DrvGpioSetPinLo(gpio_num);
    }
}
static int mv182_ov4188_drv_gpio_set(hat_cm_socket_gpio_dev_t *plat_dev, HAT_HW_RESOURCE_ACTION_T action)
{
    mv182_ov4188_gpio_device_t *dev;
    dev = plat_to_mv182_ov4188(plat_dev);

    //mmsdbg(DL_PRINT, "//VIV PLAT: GPIO: GPIO_%d set %d executed!",dev->gpio_num, action);

    //set output mode
    DrvGpioSetMode(dev->prms.gpio_number,  D_GPIO_DIR_OUT | D_GPIO_MODE_7 );
    if(action) {
        l_gpio_set(dev->prms.gpio_number, dev->prms.on_value);
    }
    else {
        l_gpio_set(dev->prms.gpio_number, dev->prms.off_value);
    }

    return 0;
}

hat_cm_socket_gpio_dev_t * mv182_ov4188_drv_gpio_init(plat_gpio_dev_params_t *params)
{
    mv182_ov4188_gpio_device_t *dev;
    dev = osal_calloc(1, sizeof (*dev));
    if (!dev) {
        mmsdbg(
                DL_ERROR,
                "Failed to allocate memory for new instance (size=%d)!",
                sizeof (*dev)
            );
        goto exit1;
    }

    mmsdbg(DL_MESSAGE, "//VIV PLAT: GPIO: Create GPIO with params: %d %d %d %d %d %d",
        params->debounce,
        params->default_value,
        params->direction,
        params->gpio_number,
        params->off_value,
        params->on_value);

    dev->plat_dev.gpio_set = mv182_ov4188_drv_gpio_set;

    dev->prms = *params; // copy all GPIO create parameters
    DrvGpioSetMode(dev->prms.gpio_number,  D_GPIO_DIR_OUT | D_GPIO_MODE_7 );
    l_gpio_set(dev->prms.gpio_number, dev->prms.default_value);

    mmsdbg(DL_MESSAGE, "//VIV PLAT: GPIO: GPIO handle created at %010p", &dev->plat_dev);

    return &dev->plat_dev;
exit1:
    return NULL;
}
