/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file plat_power_resource.h
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! May 4, 2015 : Author aiovtchev
*! Created
* =========================================================================== */



#ifndef mv182_ov4188_DRV_INC_PLAT_POWER_RESOURCE_H_
#define mv182_ov4188_DRV_INC_PLAT_POWER_RESOURCE_H_

#include <platform/inc/plat_hw_resource.h>

#define WM8325_UNIQUE_ID_SIZE (128/8)
unsigned char *wm8325_unique_id_get(void);

typedef enum {
    mv182_ov4188_INVALID_POEWR,  // reserve value 0 as invalid (catch non initialized value)
    mv182_ov4188_LED1_POWER,
    mv182_ov4188_LED2_POWER,
} mv182_ov4188_power_sources;

//TODO: Should be got from the real driver
int mv182_ov4188_drv_power_init(void);
int mv182_ov4188_drv_power_deinit(void);

#endif /* mv182_ov4188_DRV_INC_PLAT_POWER_RESOURCE_H_ */
