/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file plat_csi2.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <platform/dummyplat/drv/inc/plat_csi2_resource.h>

#include <utils/mms_debug.h>

mmsdbg_define_variable(
        test_plat_csi2,
        DL_DEFAULT,
        0,
        "test_plat_csi2",
        "Camera module test application."
    );
#define MMSDEBUGLEVEL mmsdbg_use_variable(test_plat_csi2)

int csi_cfg_temp(hat_cm_socket_csi2_cfg_t* cfg)
{
    mmsdbg(DL_MESSAGE, "//VIV PLAT: CSI2: csi_cfg_temp executed: %d %d", cfg->a, cfg->b);
    return 0;
}

int csi_ctx_cfg_temp(hat_cm_socket_csi2_ctx_cfg_t* ctx_cfg)
{
    mmsdbg(DL_MESSAGE, "//VIV PLAT: CSI2: csi_ctx_cfg_temp executed: %d %d", ctx_cfg->c, ctx_cfg->d);
    return 0;
}

int csi_ctx_start_temp(void)
{
    mmsdbg(DL_MESSAGE, "//VIV PLAT: CSI2: csi_ctx_start_temp executed");
    return 0;
}

int csi_ctx_stop_temp(void)
{
    mmsdbg(DL_MESSAGE, "//VIV PLAT: CSI2: csi_ctx_stop_temp executed");
    return 0;
}

int csi_ctx_imm_stop_temp(void)
{
    mmsdbg(DL_MESSAGE, "//VIV PLAT: CSI2: csi_ctx_imm_stop_temp executed");
    return 0;
}

int csi_ctx_set_buffer_temp(void* buff)
{
    mmsdbg(DL_MESSAGE, "//VIV PLAT: CSI2: csi_ctx_set_buffer_temp executed: %010p", buff);
    return 0;
}

int csi_ctx_enable_event_temp(void)
{
    mmsdbg(DL_MESSAGE, "//VIV PLAT: CSI2: csi_ctx_enable_event_temp executed");
    return 0;
}

int csi_ctx_disable_event_temp(void)
{
    mmsdbg(DL_MESSAGE, "//VIV PLAT: CSI2: csi_ctx_disable_event_temp executed");
    return 0;
}

int csi2_init_temp(plat_res_desc_csi2_t *prms, hat_cm_socket_csi2_hndl_t *hndl)
{
    mmsdbg(DL_MESSAGE, "//VIV PLAT: CSI2: Create CSI2 with params: %s %d",
        prms->name,
        prms->ctx_num);

    //TODO: - The real function should call CSI2 platform driver with
    // these parameters and return handle to all CSI2 operations
    hndl->csi_cfg = csi_cfg_temp;
    hndl->csi_ctx_cfg = csi_ctx_cfg_temp;
    hndl->csi_ctx_start = csi_ctx_start_temp;
    hndl->csi_ctx_stop = csi_ctx_stop_temp;
    hndl->csi_ctx_imm_stop = csi_ctx_imm_stop_temp;
    hndl->csi_ctx_set_buffer = csi_ctx_set_buffer_temp;
    hndl->csi_ctx_enable_event = csi_ctx_enable_event_temp;
    hndl->csi_ctx_disable_event = csi_ctx_disable_event_temp;

    mmsdbg(DL_MESSAGE, "//VIV PLAT: CSI2: CSI2 handle created at %010p", hndl);
    return 0;
}
