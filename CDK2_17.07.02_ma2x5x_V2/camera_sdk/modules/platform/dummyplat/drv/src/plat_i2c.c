/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file plat_i2c.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <osal/osal_stdtypes.h>
#include <osal/osal_stdlib.h>
#include <osal/osal_string.h>
#include <utils/mms_debug.h>

#include <platform/dummyplat/drv/inc/plat_i2c_resource.h>

#define container_of(PTR,TYPE,FIELD) \
    (TYPE *)((char *)(PTR) - (char *)&(((TYPE *)0)->FIELD))

#define plat_to_dummy(PLAT_DEV) container_of((PLAT_DEV), dummy_i2c_device_t, plat_dev)

typedef struct {
    hat_cm_socket_i2c_dev_t plat_dev;
    unsigned int slave_addr;
    unsigned int slave_addr_bit_size;
    unsigned int reg_addr_size;
} dummy_i2c_device_t;

extern int clp_camera_select;

mmsdbg_define_variable(
        test_plat_i2c,
        DL_DEFAULT,
        0,
        "test_plat_i2c",
        "I2C Dummy platform implementation."
    );
#define MMSDEBUGLEVEL mmsdbg_use_variable(test_plat_i2c)

static int dummy_i2c_dev_write(
        hat_cm_socket_i2c_dev_t *plat_dev,
        uint32 reg,
        uint32 num,
        uint8 *val
    )
{
    dummy_i2c_device_t *dev;
    dev = plat_to_dummy(plat_dev);
    mmsdbg(
            DL_MESSAGE,
            "SA=0x%04x, SASZ=%d, RASZ=%d, RA=0x%04x, N=%d",
            dev->slave_addr,
            dev->slave_addr_bit_size,
            dev->reg_addr_size,
            reg,
            num
        );
    mmsdbgdump(DL_MESSAGE, "V", val, num);
    return 0;
}

static int dummy_i2c_dev_read(
        hat_cm_socket_i2c_dev_t *plat_dev,
        uint32 reg,
        uint32 num,
        uint8 *val
    )
{
    dummy_i2c_device_t *dev;
    dev = plat_to_dummy(plat_dev);
    osal_memset(val, 0, num);
    mmsdbg(
            DL_MESSAGE,
            "SA=0x%04x, SASZ=%d, RASZ=%d, RA=0x%04x, N=%d",
            dev->slave_addr,
            dev->slave_addr_bit_size,
            dev->reg_addr_size,
            reg,
            num
        );
    mmsdbgdump(DL_MESSAGE, "V", val, num);
    return 0;
}

hat_cm_socket_i2c_dev_t * dummy_i2c_dev_init(plat_i2c_dev_params_t *params)
{
    dummy_i2c_device_t *dev;

    dev = osal_calloc(1, sizeof (*dev));
    if (!dev) {
        mmsdbg(
                DL_ERROR,
                "Failed to allocate memory for new instance (size=%d)!",
                sizeof (*dev)
            );
        goto exit1;
    }

    dev->slave_addr = params->slave_addr;
    dev->slave_addr_bit_size = params->slave_addr_bit_size;
    dev->reg_addr_size = params->reg_addr_size;

    dev->plat_dev.write = dummy_i2c_dev_write;
    dev->plat_dev.read  = dummy_i2c_dev_read;

    return &dev->plat_dev;
exit1:
    return NULL;
}

int dummy_i2c_dev_deinit(hat_cm_socket_i2c_dev_t *plat_dev)
{
    dummy_i2c_device_t *dev;

    dev = plat_to_dummy(plat_dev);

    if (NULL == dev)
    {
        mmsdbg(DL_ERROR, "Invalid handle pointer");
        return 1;
    }

    osal_free(dev);

    return 0;
}

