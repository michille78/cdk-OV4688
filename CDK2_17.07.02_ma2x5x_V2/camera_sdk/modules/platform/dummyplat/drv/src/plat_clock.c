/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file plat_clock.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <platform/dummyplat/drv/inc/plat_clock_resource.h>

#include <utils/mms_debug.h>

mmsdbg_define_variable(
        test_plat_clock,
        DL_DEFAULT,
        0,
        "test_plat_clock",
        "Camera module test application."
    );
#define MMSDEBUGLEVEL mmsdbg_use_variable(test_plat_clock)

int clock_set_temp(hat_cm_socket_clock_hndl_t *hndl, HAT_HW_RESOURCE_ACTION_T action)
{
    mmsdbg(DL_MESSAGE, "//VIV PLAT: CLOCK: CLOCK set %d executed!", action);

    return 0;
}

int clock_init_temp(plat_res_desc_clock_t *prms, hat_cm_socket_clock_hndl_t *hndl)
{
    mmsdbg(DL_MESSAGE, "//VIV PLAT: CLOCK: Create CLOCK with params: %s %d %d",
        prms->name,
        prms->id,
        prms->freq);

    //TODO: - The real function should call CLOCK platform driver with
    // these parameters and return handle to all CLOCK operations
    hndl->clock_set = clock_set_temp;

    mmsdbg(DL_MESSAGE, "//VIV PLAT: CLOCK: CLOCK handle created at %010p", hndl);
    return 0;
}
