/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file plat_dummy.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <platform/dummyplat/drv/inc/plat_dummy_resource.h>

#include <utils/mms_debug.h>

mmsdbg_define_variable(
        test_plat_dummy,
        DL_DEFAULT,
        0,
        "test_plat_dummy",
        "Camera module test application."
    );
#define MMSDEBUGLEVEL mmsdbg_use_variable(test_plat_dummy)

int dummy_set_temp(HAT_HW_RESOURCE_ACTION_T action)
{
    // mmsdbg(DL_MESSAGE, "//VIV PLAT: DUMMY: DUMMY set %d executed!", action);

    return 0;
}

int dummy_init_temp(plat_res_desc_dummy_t *prms, hat_cm_socket_dummy_hndl_t *hndl)
{
    //mmsdbg(DL_MESSAGE, "//VIV PLAT: DUMMY: Create DUMMY with params: %s %d", prms->name, prms->id);

    //TODO: - The real function should call DUMMY platform driver with
    // these parameters and return handle to all DUMMY operations
    hndl->dummy_set = dummy_set_temp;

    // mmsdbg(DL_MESSAGE, "//VIV PLAT: DUMMY: DUMMY handle created at %010p", hndl);
    return 0;
}
