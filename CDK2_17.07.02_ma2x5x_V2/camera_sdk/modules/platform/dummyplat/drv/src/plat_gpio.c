/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file plat_gpio.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <platform/dummyplat/drv/inc/plat_gpio_resource.h>
#include <osal/osal_stdtypes.h>
#include <osal/osal_stdlib.h>
#include <utils/mms_debug.h>

mmsdbg_define_variable(
        test_plat_gpio,
        DL_DEFAULT,
        0,
        "test_plat_gpio",
        "Camera module test application."
    );
#define MMSDEBUGLEVEL mmsdbg_use_variable(test_plat_gpio)

#define plat_to_dummy_gpio_dev(PLAT_DEV) container_of((PLAT_DEV), dummy_gpio_device_t, plat_dev)


typedef struct {
    hat_cm_socket_gpio_dev_t plat_dev;
    unsigned int gpio_num;
} dummy_gpio_device_t;

#if 0
typedef struct  {
	int (*drv_gpio_create) 	(void); // return 0 for success
	int (*drv_gpio_open)   	(driver_hndl* h, uint32 gpio_no, uint32 mode); 	// return 0 for success
	int (*drv_gpio_acquire)	(driver_hndl  h, uint32 gpio_no, plat_gpio_direction_t direction);
	int (*drv_gpio_set)  	(driver_hndl h, uint32 gpio_no);
	int (*drv_gpio_clear)  	(driver_hndl h);
	int (*drv_gpio_read)  	(driver_hndl h);
	int (*drv_gpio_debaunce)(driver_hndl h, uint32 deb_value);
	int (*drv_gpio_clock)  	(driver_hndl h, uint32 clk_value );
	int (*drv_gpio_release)	(driver_hndl h);
	int (*drv_gpio_close)  	(driver_hndl h);
	int (*drv_gpio_destroy)	(driver_hndl h);
}plat_gpio_ops;
#endif

static int gpio_set_temp(hat_cm_socket_gpio_dev_t *plat_dev, HAT_HW_RESOURCE_ACTION_T action)
{
    mmsdbg(DL_MESSAGE, "//VIV PLAT: GPIO: GPIO set %d executed!", action);

    return 0;
}

hat_cm_socket_gpio_dev_t * dummy_drv_gpio_init(plat_gpio_dev_params_t *params)
{
    dummy_gpio_device_t *dev;
    dev = osal_calloc(1, sizeof (*dev));
    if (!dev) {
        mmsdbg(
                DL_ERROR,
                "Failed to allocate memory for new instance (size=%d)!",
                sizeof (*dev)
            );
        goto exit1;
    }
    mmsdbg(DL_MESSAGE, "//VIV PLAT: GPIO: Create GPIO with params: %d %d %d %d %d %d",
           params->debounce,
           params->default_value,
           params->direction,
           params->gpio_number,
           params->off_value,
           params->on_value);

    //TODO: - The real function should call GPIO platform driver with
    // these parameters and return handle to all GPIO operations

    dev->plat_dev.gpio_set = gpio_set_temp;

    dev->gpio_num = params->gpio_number;

    mmsdbg(DL_PRINT, "//VIV PLAT: GPIO: GPIO handle created at %010p", &dev->plat_dev);

    return &dev->plat_dev;

exit1:
    return NULL;
}

int dummy_drv_gpio_deinit(hat_cm_socket_gpio_dev_t *plat_dev)
{
    dummy_gpio_device_t *dev;

    dev = plat_to_dummy_gpio_dev(plat_dev);

    if (NULL == dev)
    {
        mmsdbg(DL_ERROR, "Invalid handle pointer");
        return 1;
    }

    osal_free(dev);

    return 0;
}
