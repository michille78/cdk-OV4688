/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file plat_dummyplat.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <hal/hat_types.h>
#include <platform/inc/platform_all.h>
#include <error_handle/include/error_handle.h>

#include <utils/mms_debug.h>

mmsdbg_define_variable(
        plat_dummyplat,
        DL_DEFAULT,
        0,
        "plat_dummyplat",
        "Platform dummyplat file"
    );
#define MMSDEBUGLEVEL mmsdbg_use_variable(plat_dummyplat)

#if 0
static plat_i2c_hndl_t i2c_hndl;
static plat_i2c_create_params_t i2c_prms = {
    .num_channels = 3,
};


static plat_i2c_bus_hndl_t bus_hndl;
static plat_i2c_bus_create_params_t bus_prms = {
    .channel_idx = 2,
    .channel_speed = 400000,
};


static plat_i2c_bus_dev_hndl_t dev_hndl;
static plat_i2c_dev_register_param_t dev_reg_prms = {
    .dev_address = 0x23,
    .dev_address_len = 8,
};


void transaction_callback(uint32 reg_addr, uint32 num, void* data)
{
    mmsdbg(DL_MESSAGE, "I2C transaction callback!");
}
plat_bus_callback_t bus_cb;
#endif


static plat_socket_res_desc_t dummyplat_socket0_res;
extern plat_socket_res_desc_t dummyplat_socket1_res;

static plat_socket_obj_t dummyplat_plat_sockets[PH_SOCKET_MAX];

int board_platform_init(plat_socket_obj_t **sockets)
{
    int err = 0;
    int i;

    //TODO: Need to init platform drivers here
#if 0
    err = plat_i2c_create(&i2c_hndl, i2c_prms);
    err = plat_i2c_bus_create(i2c_hndl, &bus_hndl, bus_prms);
    err = plat_bus_i2c_acquire(bus_hndl);

    err = plat_dev_i2c_dev_create(bus_hndl, plat_i2c_dev_hndl_t* h, plat_i2c_dev_create_param_t param);

    bus_cb = transaction_callback;
    err = plat_bus_i2c_register_dev(bus_hndl, &dev_hndl, dev_reg_prms, bus_cb);
    int plat_dev_i2c_open (plat_i2c_dev_hndl_t* h, plat_dev_callback_t callback, plat_i2c_client_hndl_t client_prv);     // return 0 for success
#endif

    // Init all sockets with dummy socket
    for (i = PH_SOCKET_DUMMY; i < PH_SOCKET_MAX; i++) {
        dummyplat_plat_sockets[i].soc_res = &dummyplat_plat_sockets;
    }

    for (i = PH_SOCKET_DUMMY; i < PH_SOCKET_MAX; i++) {
        mmsdbg(DL_MESSAGE, "\n\n//VIV PLAT: >>>>>>>>>>>>>>>>>> Try to init socket %d", i);
        err = plat_socket_init(&dummyplat_plat_sockets[i]);
        GOTO_EXIT_IF(0 != err, 1);
    }

    *sockets = dummyplat_plat_sockets;

    return 0;

EXIT_1:
    mmsdbg(DL_ERROR, "//VIV PLAT: Exit Err");
    return -1;
}


int board_platform_deinit (plat_socket_obj_t *sockets)
{
    int err = 0;
    int i;

    for (i = PH_SOCKET_DUMMY; i < PH_SOCKET_MAX; i++) {
        mmsdbg(DL_MESSAGE, "\n\n//VIV PLAT: >>>>>>>>>>>>>>>>>> Try to deinit socket %d", i);
        err = sockets[i].deinit(&sockets[i]);
        GOTO_EXIT_IF(0 != err, 1);
    }

    return 0;
EXIT_1:
    mmsdbg(DL_ERROR, "//VIV PLAT: Exit Err");
    return -1;
}















#if 0


typedef int (*init_socket_t)(plat_socket_handle_t *h);

typedef struct {
    plat_socket_handle_t h;
    init_socket_t        init;
} plat_socket;

int init_socket1(plat_socket_handle_t *h);
int init_socket2(plat_socket_handle_t *h);

////////////////////// I2C //////////////

typedef struct {
    HAT_SOCKET_STATE_T  state;
    uint32              num_res;
} hat_i2c_object;

typedef hat_i2c_object* hat_i2c_hndl;

typedef int (*init_i2c)(hat_i2c_hndl *h);

typedef struct {
    hat_i2c_hndl    h;
    init_i2c        init;
} plat_i2c;

int init_i2c_1(hat_i2c_hndl *h);
int init_i2c_2(hat_i2c_hndl *h);


////////////////////// GPIO //////////////

typedef struct {
    hat_socket_state_t  state;
    uint32              num_res;
} hat_gpio_object;

typedef hat_gpio_object* hat_gpio_hndl;

typedef int (*init_gpio)(hat_gpio_hndl *h);

typedef struct {
    hat_gpio_hndl   h;
    init_gpio       init;
} plat_gpio;

int plat_init_gpio(hat_gpio_hndl *h)
{
    printf ("%s\n", __func__);
    return 0;
}

////////////////////// sensor //////////////

typedef struct {
    HAT_SOCKET_STATE_T  state;
    uint32              num_res;
} hat_sensor_object;

typedef hat_sensor_object* hat_sensor_hndl;

typedef int (*init_sensor)(hat_sensor_hndl *h, hat_socket_hndl s, hat_i2c_hndl cci);

typedef struct {
    hat_sensor_hndl h;
    init_sensor     init;
} plat_sensor;

int plat_init_sensor(hat_gpio_hndl *h, hat_socket_hndl s, hat_i2c_hndl cci)
{
    printf ("%s\n", __func__);
}

////////////////////// END //////////////


static plat_socket plat_sockets[PH_SOCKET_MAX] = {
    { NULL, &init_socket_dummy},
    { NULL, &init_socket_1},
    { NULL, &init_socket_2},
};


static plat_socket plat_i2c[] = {
        { NULL, &init_i2c_1},
        { NULL, &init_i2c_2},
};

static plat_socket plat_gpio [] = {
        { NULL, &plat_init_gpio},
};
/*
static plat_sensor plat_gpio [] = {
        { NULL, &init_gpio},
};
*/

int platform_init_dummyplat(void)
{
    int i;


#if 0
plat_socket *s;
int ret = 0;

    s = plat_sockets[0];
    while (s->init) {

        ret = s->init(&s->h);

        if (ret)
            return ret;

        s++;
    }
#endif

    /* Try to create all sockets for the platform and
     * store handle for each one */
    for (i = SIM_PHYS_CM_SOCKET_DUMMY; i < SIM_PHYS_CM_SOCKET_MAX; i++) {
        hal_cm_socket_init(i);
        ph_socket_hndls[i] = hal_cm_socket_create(i);
    }
}


int platfor_deinit_dummyplat(void)
{
}


#endif
