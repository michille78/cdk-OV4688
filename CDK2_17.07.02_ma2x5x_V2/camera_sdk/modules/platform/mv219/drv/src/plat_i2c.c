/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file plat_i2c.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <osal/osal_stdtypes.h>
#include <osal/osal_stdlib.h>
#include <utils/mms_debug.h>

#include <platform/mv219/drv/inc/plat_i2c_resource.h>

#define plat_to_mv219(PLAT_DEV) container_of((PLAT_DEV), mv219_i2c_device_t, plat_dev)

typedef struct {
    hat_cm_socket_i2c_dev_t plat_dev;
    unsigned int slave_addr;
    unsigned int slave_addr_bit_size;
    unsigned int reg_addr_size; /* In number of bytes */
    hat_cm_socket_i2c_bus_dev_t * bus_dev_instance;
} mv219_i2c_device_t;

extern int clp_camera_select;

mmsdbg_define_variable(
        mv219_drv_test_plat_i2c,
        DL_DEFAULT,
        0,
        "mv219_drv_plat_i2c",
        "I2C platform implementation for mv219."
    );
#define MMSDEBUGLEVEL mmsdbg_use_variable(mv219_drv_test_plat_i2c)

static int mv219_i2c_dev_write(
        hat_cm_socket_i2c_dev_t *plat_dev,
        uint32 reg,
        uint32 num,
        uint8 *val
    )
{
    mv219_i2c_device_t *dev;
    dev = plat_to_mv219(plat_dev);

    //call write for bus
    return dev->bus_dev_instance->write(dev->bus_dev_instance, reg, num, val);
}

static int mv219_i2c_dev_read(
        hat_cm_socket_i2c_dev_t *plat_dev,
        uint32 reg,
        uint32 num,
        uint8 *val
    )
{
    mv219_i2c_device_t *dev;
    dev = plat_to_mv219(plat_dev);

    //call read for bus
    return dev->bus_dev_instance->read(dev->bus_dev_instance, reg, num, val);
}

static int mv219_i2c_register_device(hat_cm_socket_i2c_dev_t *plat_dev, hat_socket_comp_cfg_t *params_dev)
{
    mv219_i2c_device_t *dev;
    dev = plat_to_mv219(plat_dev);

    return dev->bus_dev_instance->regbus(dev->bus_dev_instance, params_dev);
}

hat_cm_socket_i2c_dev_t * mv219_i2c_dev_init(plat_i2c_dev_params_t *params)
{
    mv219_i2c_device_t *dev;
    hat_socket_comp_cfg_t params_dev;

    mmsdbg(DL_MESSAGE, "DEBUG_GUZZI: call Init I2C_BUS device");

    dev = osal_calloc(1, sizeof (*dev));
    if (!dev) {
        mmsdbg(
                DL_ERROR,
                "Failed to allocate memory for new instance (size=%d)!",
                sizeof (*dev)
            );
        goto exit1;
    }

    dev->slave_addr = params->slave_addr;
    dev->slave_addr_bit_size = params->slave_addr_bit_size;
    dev->reg_addr_size = params->reg_addr_size; /* In number of bytes */

    dev->plat_dev.write = mv219_i2c_dev_write;
    dev->plat_dev.read  = mv219_i2c_dev_read;
    dev->plat_dev.regdev= mv219_i2c_register_device;


    if(params->bus_desc) {
        mmsdbg(DL_MESSAGE, "DEBUG_GUZZI: call Init I2C_BUS device");

        //init bus
        params->bus_desc->instance = params->bus_desc->init(&params->bus_desc->params);

        //save bus device instance inside i2c device
        dev->bus_dev_instance = params->bus_desc->instance;

        //call register bus function (this register bus and device inside bus)
        params_dev.address = params->slave_addr;
        params_dev.slave_addr_bit_size = params->slave_addr_bit_size;
        params_dev.register_size = params->reg_addr_size;

        dev->bus_dev_instance->regbus(dev->bus_dev_instance, &params_dev);
    }
    else {
        mmsdbg(DL_PRINT, "no bus_desc for this i2c device");
    }
    return &dev->plat_dev;
exit1:
    return NULL;
}

