/* =============================================================================
* Copyright (c) 2013-2015 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file plat_socket1.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */
/* IMX208(Left J1) on MV0200 connected to MV182(J13)*/
#include <platform/inc/plat_socket.h>
#include <platform/mv182/drv/inc/plat_dummy_resource.h>
#include <platform/mv182/drv/inc/plat_gpio_resource.h>
#include <platform/mv182/drv/inc/plat_i2c_resource.h>
#include <platform/mv182/drv/inc/plat_csi2_resource.h>
#include <platform/mv182/drv/inc/plat_clock_resource.h>
#include <platform/mv182/drv/inc/plat_i2c_bus_resource.h>

#define CONST_MHz   (1000000)

typedef enum {
    I2C_CH_0,
    I2C_CH_1,
    I2C_CH_2
}i2c_ch_select;

static plat_i2c_bus_dev_desc_t bus0_desc = {
    .params = {
        .channel = I2C_CH_0, //selected i2c channel
        .speed = 0,               //TODO: check this
    },
    .init = mv182_i2c_bus_dev_init,
};

static plat_gpio_dev_desc_t p1 = {
    .params = {
        .gpio_number   = 55,
        .direction     = PLAT_GPIO_OUT,
        .default_value = 0,
        .on_value      = 1,
        .off_value     = 0,
        .debounce      = 0,
    },
    .init = mv182_drv_gpio_init,
};

static plat_gpio_dev_desc_t p2 = {
    .params = {
        .gpio_number   = 55,
        .direction     = PLAT_GPIO_OUT,
        .default_value = 0,
        .on_value      = 1,
        .off_value     = 0,
        .debounce      = 0,  
    },
    .init = mv182_drv_gpio_init,
};

static plat_clock_t sen_clock = {
    .init_data = {
        .name = "mv182 sensor clock",
        .id  = 1,
        .freq = CAM_MCLK2,
    },
    .init_function = mv182_drv_clock_init,
    .hndl = {
        .clock_set =  NULL,
    },
};

static plat_i2c_dev_desc_t sen_cci = {
    .params = {
        .bus_desc = &bus0_desc,
        .slave_addr = (0x6C >> 1),
        .slave_addr_bit_size = 7, //this is not supported
        .reg_addr_size = 2,
    },
    .init = mv182_i2c_dev_init,
};

static plat_csi2_t sen_imgif = {
    .init_data = {
        .name = "CSI_CTX_2",
        .ctx_num = 2,
    },
    .init_function = mv182_drv_csi2_init,
    .hndl = {
        .csi_cfg = NULL,
        .csi_ctx_cfg = NULL,
        .csi_ctx_start = NULL,
        .csi_ctx_stop = NULL,
        .csi_ctx_imm_stop = NULL,
        .csi_ctx_set_buffer = NULL,
        .csi_ctx_enable_event = NULL,
        .csi_ctx_disable_event = NULL,
    }
};

static plat_resource_t sen_res_power_1 = { HW_RSRC_GPIO, PLAT_RES_CLOSE, &p1 };
static plat_resource_t sen_res_power_2 = { HW_RSRC_GPIO, PLAT_RES_CLOSE, &p2 };
static plat_resource_t sen_res_clock   = { HW_RSRC_CLOCK,  PLAT_RES_CLOSE, &sen_clock };
static plat_resource_t sen_res_cci     = { HW_RSRC_I2C,  PLAT_RES_CLOSE, &sen_cci };

static plat_resource_t sen_res_imgif   = { HW_RSRC_CSI2, PLAT_RES_CLOSE, &sen_imgif };

static plat_res_desc_t comp_sen_res[SEN_RSRC_MAX] = {
    [SEN_RSRC_POWER_1 ] = { &sen_res_power_1 },
    [SEN_RSRC_POWER_2 ] = { &sen_res_power_2 },
    [SEN_RSRC_CLOCK   ] = { &sen_res_clock   },
    [SEN_RSRC_CCI     ] = { &sen_res_cci     },
    [SEN_RSRC_IMGIF   ] = { &sen_res_imgif   },
};

plat_socket_res_desc_t mv182_socket2_res = {
    /* SOCKET_COMP_SENSOR */
    .sensor_res = &comp_sen_res,
    /* SOCKET_COMP_LENS */
    .lens_res = NULL,
    /* SOCKET_COMP_FLASH */
    .flash_res = NULL,
    /* SOCKET_COMP_NVM */
    .nvm_res = NULL,
};

