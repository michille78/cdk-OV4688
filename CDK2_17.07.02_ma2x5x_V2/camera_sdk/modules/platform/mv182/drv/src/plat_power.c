/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file plat_power.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! May 4, 2015 : Author aiovtchev
*! Created
* =========================================================================== */

#include <osal/osal_stdtypes.h>
#include <osal/osal_stdlib.h>
#include <platform/mv182/drv/inc/plat_i2c_bus_resource.h>
#include <platform/mv182/drv/inc/plat_i2c_resource.h>
#include <hal/hal_camera_module/hat_cm_socket.h>
#include <utils/mms_debug.h>

#include <platform/mv182/drv/inc/plat_power_resource.h>

mmsdbg_define_variable(
        mv182_drv_plat_power,
        DL_DEFAULT,
        0,
        "mv182_drv_plat_power",
        "Camera module test application."
    );
#define MMSDEBUGLEVEL mmsdbg_use_variable(mv182_drv_plat_power)

#define DL_XXXX    DL_MESSAGE


#define WM8325_REG_LED1         0X404C
#define WM8325_REG_LED2         0X404D

#define MSB_LED_OFF             0x00
#define MSB_LED_ON              0x4000


typedef enum {
    I2C_CH_0,
    I2C_CH_1,
    I2C_CH_2
}i2c_ch_select;

plat_i2c_bus_dev_desc_t power_bus_params = {
    .params = {
        .channel = I2C_CH_2,
        .speed = 0,
    },
    .init = mv182_i2c_bus_dev_init,
};

plat_i2c_dev_params_t hw_power_params = {
    .bus_desc = &power_bus_params,
    .slave_addr = (0x36),
    .slave_addr_bit_size = 7,
    .reg_addr_size = 2,
};

typedef struct {
    int ref_cnt;
} power_device_t;

hat_cm_socket_i2c_dev_t    *power_i2c_dev = NULL;

////////////////
#define WM8325_REG_UNIQUE_ID 0x7800
static unsigned char wm8325_unique_id[WM8325_UNIQUE_ID_SIZE];

#define DBG_WM8325_UNIQUE_ID_PRINT
#ifdef DBG_WM8325_UNIQUE_ID_PRINT
static void wm8325_unique_id_print(
        unsigned char *unique_id,
        int unique_id_size
    )
{
    #define PER_ROW 4
    #define PER_BYTE 3

    int i_id, i_hex;
    char hex[PER_BYTE*PER_ROW+1];

    i_hex = 0;
    for (i_id = 0; i_id < unique_id_size; i_id++) {
        sprintf(hex + PER_BYTE*i_hex, " %02x", unique_id[i_id]);
        i_hex++;
        if (!(i_hex % PER_ROW)) {
            mmsdbg(
                    DL_ERROR,
                    "WM8325_UNIQUE_ID: 0x%04x:%s",
                    WM8325_REG_UNIQUE_ID+i_id-(PER_ROW-1),
                    hex
                );
            i_hex = 0;
        }
    }
    if (i_hex % PER_ROW) {
        mmsdbg(
                DL_ERROR,
                "WM8325_UNIQUE_ID: 0x%04x:%s",
                WM8325_REG_UNIQUE_ID+i_id-(PER_ROW-1),
                hex
            );
    }
}
#else
#define wm8325_unique_id_print(unique_id, unique_id_size) ((void)0)
#endif

unsigned char *wm8325_unique_id_get(void)
{
    static int read_read_once = 0;

    if (!read_read_once) {
        if (!power_i2c_dev) {
            mmsdbg(DL_ERROR, "Power I2C not initialized!");
            goto exit_1;
        }

        power_i2c_dev->read(
                power_i2c_dev,
                WM8325_REG_UNIQUE_ID,
                ARRAY_SIZE(wm8325_unique_id),
                wm8325_unique_id
            );
        wm8325_unique_id_print(
                wm8325_unique_id,
                ARRAY_SIZE(wm8325_unique_id)
            );

        read_read_once = 1;
    }

exit_1:
    return wm8325_unique_id;
}

/*
static uint16_t power_read_reg(uint16_t reg)
{
uint8_t val[2];

    power_i2c_dev->read(power_i2c_dev, reg, 2, val);
    return ((val[0]<<8) | val[1]);
}
*/

static uint16_t power_write_reg(uint16_t reg, uint16_t v)
{
uint8_t val[2];
    val[0] = (v>>8);
    val[1] = (v&0xFF);
    return power_i2c_dev->write(power_i2c_dev, reg, 2, val);
}

/*
static void power_print_reg(uint16_t reg)
{
    printf ("     RRRRRRRRR  0x%04x 0x%04x    \n", reg, power_read_reg(reg));
}
*/

int cam_led_1(HAT_HW_RESOURCE_ACTION_T action)
{
    uint16_t val;

    mmsdbg(DL_XXXX, "//VIV PLAT: %s set %d executed!", __func__, action);

    val = MSB_LED_OFF;
    if (action == HAT_HW_RES_ON) {
        val = MSB_LED_ON;
    }
    power_write_reg(WM8325_REG_LED1, val);

    return 0;
}


void cam_led1_toggle(void)
{
#define CNT_LED1_XXX (8)
    static int led_xxx = 0, cnt_xxx= 0;

    if (!(++cnt_xxx % CNT_LED1_XXX)) {
        led_xxx ^= 1;
        cam_led_1(led_xxx);
    }
}


int cam_led_2(HAT_HW_RESOURCE_ACTION_T action)
{
    uint16_t val;

    mmsdbg(DL_XXXX, "//VIV PLAT: %s set %d executed!", __func__, action);

    val = MSB_LED_OFF;
    if (action == HAT_HW_RES_ON) {
        val = MSB_LED_ON;
    }
    power_write_reg(WM8325_REG_LED2, val);

    return 0;
}


int mv182_drv_power_init(void)
{

/*
    mmsdbg(DL_XXXX, "//VIV PLAT: POWER: Create POWER with params: %d %d %d %d",
        params->default_value,
        params->power_number,
        params->off_value,
        params->on_value);

    switch (params->power_number)
    {
    case CAM_VDDIO_1V8:
        hndl->power_set = cam_vddio_1V8_drv_power_set;
        break;
    case CAM_AVDD_2V8:
        hndl->power_set = cam_avdd_2V8_drv_power_set;
        break;
    case CAM_VDD:
        hndl->power_set = cam_vdd_drv_power_set;
        break;
    case CAM_INVALID_POEWR:
    default:
        mmsdbg(DL_ERROR, "//VIV PLAT: POWER: Invalid POWER_%d selected!",params->power_number);
    }
*/
    if (power_i2c_dev == NULL)
        power_i2c_dev = mv182_i2c_dev_init(&hw_power_params); // Get handle to PMIC

    wm8325_unique_id_get();

    return 0;
}

int mv182_drv_power_deinit(void)
{
    return 0;
}
