/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file plat_clock.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <platform/mv182/drv/inc/plat_clock_resource.h>
#include <platform/mv182/drv/inc/plat_i2c_bus_resource.h>
#include <platform/mv182/drv/inc/plat_i2c_resource.h>
#include <hal/hal_camera_module/hat_cm_socket.h>

#include <utils/mms_debug.h>

mmsdbg_define_variable(
        mv182_drv_test_plat_clock,
        DL_DEFAULT,
        0,
        "mv182_drv_test_plat_clock",
        "Camera module test application."
    );
#define MMSDEBUGLEVEL mmsdbg_use_variable(mv182_drv_test_plat_clock)

typedef enum {
    I2C_CH_0,
    I2C_CH_1,
    I2C_CH_2
}i2c_ch_select;

plat_i2c_bus_dev_desc_t clock_bus_params = {
    .params = {
        .channel = I2C_CH_2,
        .speed = 0,               //TODO: check this
    },
    .init = mv182_i2c_bus_dev_init,
};

plat_i2c_dev_params_t hw_clock_params = {
    .bus_desc = &clock_bus_params,
    .slave_addr = 0x64,
    .slave_addr_bit_size = 7, //this is not supported
    .reg_addr_size = 1,
};

typedef struct {
    int state;
    hat_cm_socket_i2c_dev_t    *clock_i2c_dev;
    uint32  freq; // Frequency Hz
} mv182_clock_device_t;

mv182_clock_device_t clk_dev =
{
     .state = 0,
     .clock_i2c_dev = NULL,
     .freq = 0,
};

static const hat_cm_socket_command_entry_t clock_on_seq_12MHz [] =
 {
//Setup clock start
SENS_INTENT_CMD_CLOCK_I2C_WR(0x02, 5, 0xB4   // Byte 0
                          /*0x83*/, 0x09   // Byte 1
                          /*0x84*/, 0x02   // Byte 2
                          /*0x85*/, 0x48   // Byte 3
                          /*0x86*/, 0x40), // Byte 4
// PLL1 - Y1, Y2, Y3
SENS_INTENT_CMD_CLOCK_I2C_WR(0x10, 16, 0x00 // Byte 0
                          /*0x91*/, 0x00  // Byte 1
                          /*0x92*/, 0x00  // Byte 2
                          /*0x93*/, 0x00  // Byte 3
                          /*0x94*/, 0x0D  // Byte 4
                          /*0x95*/, 0x02  // Byte 5
                          /*0x96*/, 0x00  // Byte 6
                          /*0x97*/, 0x00  // Byte 7
                          /*0x98*/, 0xFF  // Byte 8
                          /*0x99*/, 0x80  // Byte 9
                          /*0x9A*/, 0x02  // Byte 10
                          /*0x9B*/, 0x07  // Byte 11
                          /*0x9C*/, 0x00  // Byte 12
                          /*0x9D*/, 0x40  // Byte 13
                          /*0x9E*/, 0x02  // Byte 14
                          /*0x9F*/, 0x08),// Byte 15
// PLL2 - Y3, Y4
SENS_INTENT_CMD_CLOCK_I2C_WR(0x20, 16, 0x00  // byte 0
                          /*0x21*/, 0x00    // byte 1
                          /*0x22*/, 0x00    // byte 2
                          /*0x23*/, 0x00    // byte 3
                          /*0x24*/, 0x6D    // byte 4
                          /*0x25*/, 0x02    // byte 5
                          /*0x26*/, 0x0c    // byte 6
                          /*0x27*/, 0x0c    // byte 7
                          /*0x28*/, 0xAA    // byte 8
                          /*0x29*/, 0x05    // byte 9
                          /*0x2A*/, 0x52    // byte 10
                          /*0x2B*/, 0xA9    // byte 11
                          /*0x2C*/, 0x00    // byte 12
                          /*0x2D*/, 0x40    // byte 13
                          /*0x2E*/, 0x02    // byte 14
                          /*0x2F*/, 0x08),  // byte 15
CMD_ENTRY_END(),
};

static const hat_cm_socket_command_entry_t clock_on_seq_24MHz [] =
 {
//Setup clock start
SENS_INTENT_CMD_CLOCK_I2C_WR(0x02, 5, 0xB4   // Byte 0
                          /*0x83*/, 0x01   // Byte 1
                          /*0x84*/, 0x02   // Byte 2
                          /*0x85*/, 0x48   // Byte 3
                          /*0x86*/, 0x60), // Byte 4
// PLL1 - Y1, Y2, Y3
SENS_INTENT_CMD_CLOCK_I2C_WR(0x10, 16, 0x00 // Byte 0
                          /*0x91*/, 0x00  // Byte 1
                          /*0x92*/, 0x00  // Byte 2
                          /*0x93*/, 0x00  // Byte 3
                          /*0x94*/, 0x45  // Byte 4
                          /*0x95*/, 0x02  // Byte 5
                          /*0x96*/, 0x00  // Byte 6
                          /*0x97*/, 0x00  // Byte 7
                          /*0x98*/, 0xaF  // Byte 8
                          /*0x99*/, 0x50  // Byte 9
                          /*0x9A*/, 0x02  // Byte 10
                          /*0x9B*/, 0xC9  // Byte 11
                          /*0x9C*/, 0x00  // Byte 12
                          /*0x9D*/, 0x40  // Byte 13
                          /*0x9E*/, 0x02  // Byte 14
                          /*0x9F*/, 0x08),// Byte 15
// PLL2 - Y3, Y4
SENS_INTENT_CMD_CLOCK_I2C_WR(0x20, 16, 0x00  // byte 0
                          /*0x21*/, 0x00    // byte 1
                          /*0x22*/, 0x00    // byte 2
                          /*0x23*/, 0x00    // byte 3
                          /*0x24*/, 0x6D    // byte 4
                          /*0x25*/, 0x02    // byte 5
                          /*0x26*/, 0x06    // byte 6
                          /*0x27*/, 0x06    // byte 7
                          /*0x28*/, 0xAA    // byte 8
                          /*0x29*/, 0x05    // byte 9
                          /*0x2A*/, 0x52    // byte 10
                          /*0x2B*/, 0xA9    // byte 11
                          /*0x2C*/, 0x00    // byte 12
                          /*0x2D*/, 0x40    // byte 13
                          /*0x2E*/, 0x02    // byte 14
                          /*0x2F*/, 0x08),  // byte 15
CMD_ENTRY_END(),
};


static const hat_cm_socket_command_entry_t const*
    ara_mv182_get_clk_data(uint32 freq)
{
    switch(freq) {
    case CAM_MCLK_24MHZ:
        return &clock_on_seq_24MHz[0];
        break;
    case CAM_MCLK_12MHZ:
        return &clock_on_seq_12MHz[0];
        break;
    default:
        return &clock_on_seq_24MHz[0];
        break;
    }
}

int mv182_drv_clock_set(hat_cm_socket_clock_hndl_t *hndl,
    HAT_HW_RESOURCE_ACTION_T action)
{
    const hat_cm_socket_command_entry_t const* entry;

     mmsdbg(DL_PRINT, "//VIV PLAT: CLOCK: CLOCK set %d executed!", action);

    if (clk_dev.state != 1) { // start clock only once
        hndl->freq = clk_dev.freq;
        return 0;
    }

    clk_dev.state = 2;
    hndl->freq = clk_dev.freq;

    entry = ara_mv182_get_clk_data(clk_dev.freq);

    while (entry->type != COMMAND_ENTRY_TYPE_END)
    {
        clk_dev.clock_i2c_dev->write(clk_dev.clock_i2c_dev,
                                      entry->desc.intent.desc.command.cmd.i2c_cmd.reg,
                                      entry->desc.intent.desc.command.cmd.i2c_cmd.num,
                                      entry->desc.intent.desc.command.cmd.i2c_cmd.val);
        entry++;
    }
    return 0;
}

int mv182_drv_clock_init(plat_res_desc_clock_t *prms, hat_cm_socket_clock_hndl_t *hndl)
{
    mmsdbg(DL_PRINT, "//VIV PLAT: CLOCK: Create CLOCK with params: %s %d %d",
        prms->name,
        prms->id,
        prms->freq);
    if (clk_dev.state != 0) { // init once
        hndl->clock_set = mv182_drv_clock_set;
        clk_dev.freq = prms->freq;
        return 0;
    }

    clk_dev.state = 1;
    clk_dev.freq = prms->freq;

    clk_dev.clock_i2c_dev = mv182_i2c_dev_init(&hw_clock_params);

    //TODO: - The real function should call CLOCK platform driver with
    // these parameters and return handle to all CLOCK operations
    hndl->clock_set = mv182_drv_clock_set;

    mmsdbg(DL_PRINT, "//VIV PLAT: CLOCK: CLOCK handle created at %010p", hndl);
    return 0;
}
