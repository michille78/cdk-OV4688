/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file nvmes.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include "nvms.h"
#include "nvm_dummy/inc/nvm_dummy.h"
#include "nvm_M24C16/inc/nvm_M24C16.h"
#include "nvm_M24128/inc/nvm_M24128.h"

#include <utils/mms_debug.h>

mmsdbg_declare_variable(hal_cm_driver);
#define MMSDEBUGLEVEL mmsdbg_use_variable(hal_cm_driver)

const hat_cm_ph_nvm_handle_t nvm_handle_list[] = {
    (hat_cm_ph_nvm_handle_t)&nvm_dummy_nvm_obj,
        (hat_cm_ph_nvm_handle_t)&nvm_M24C16_obj,
        (hat_cm_ph_nvm_handle_t)&nvm_M24128_obj,
};

#ifndef ARR_SIZE
#define ARR_SIZE(a) (sizeof(a)/sizeof(a[0]))
#endif

hat_cm_ph_nvm_handle_t hal_get_nvm_driver(dtp_nvm_id_t nvm_id)
{
int i;
    for (i = 0; i<ARR_SIZE(nvm_handle_list); i++)
    {
        if (nvm_handle_list[i]->nvm_drv_id == nvm_id)
        {
            mmsdbg(DL_MESSAGE, "Mounting nvm drv %d with index %d", nvm_id, i);
            return nvm_handle_list[i];
        }
    }
    mmsdbg(DL_ERROR, "Can't find nvm driver for %. Using index 0", nvm_id);
    return nvm_handle_list[0];
}

