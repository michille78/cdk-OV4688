/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file nvm_phys.h
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#ifndef __NVM_PHYS_H__
#define __NVM_PHYS_H__

#include <hal/hat_types.h>
#include <dtp/dtp_server_defs.h>

typedef struct hat_cm_ph_nvm_obj hat_cm_ph_nvm_obj_t;

typedef hat_cm_ph_nvm_obj_t* hat_cm_ph_nvm_handle_t;

struct hat_cm_ph_nvm_obj {
    dtp_nvm_id_t             nvm_drv_id;
    int (*nvm_power_off)     (void* sock_hndl, const void* feat, void* exec_func);
    int (*nvm_power_on)      (void* sock_hndl, const void* feat, void* exec_func);
    int (*nvm_init)          (void* sock_hndl, const void* feat, void* exec_func);
    int (*nvm_read)          (void* sock_hndl, const void* feat, void* exec_func, void* buff, uint32 rd_start, uint32 rd_size);
    int (*nvm_write)         (void* sock_hndl, const void* feat, void* exec_func, void* buff, uint32 wr_start, uint32 wr_size);
};

#endif // __NVM_PHYS_H__
