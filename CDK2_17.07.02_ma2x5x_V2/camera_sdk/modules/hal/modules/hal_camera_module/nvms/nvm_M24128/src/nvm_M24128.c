/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file nvm_M24128.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <hal/hal_camera_module/hat_cm_socket.h>
#include <hal/hal_camera_module/hat_cm_nvm.h>
#include "../../nvm_phys.h"
#include <osal/osal_stdlib.h>
#include <osal/osal_string.h>
#include <utils/mms_debug.h>

mmsdbg_declare_variable(hal_cm_nvm);
#define MMSDEBUGLEVEL mmsdbg_use_variable(hal_cm_nvm)

#define M24128_PAGE_SIZE  (64)
#define M24128_PAGE_NUM   (256)
#define M24128_SIZE       (M24128_PAGE_NUM * M24128_PAGE_SIZE)

static int nvm_M24128_power_off(void* sock_hndl, const void* feat, void* exec_func)
{
    int err = 0;
    hat_cm_socket_command_entry_t* nvm_M24128_power_off_seq;
    hat_cm_socket_cmd_strip_result_t res;
    hat_cm_socket_cmd_strip_executor_t cmd_strip_executor =
        (hat_cm_socket_cmd_strip_executor_t)exec_func;

    nvm_M24128_power_off_seq = (hat_cm_socket_command_entry_t*)
        ((hat_nvm_features_t *)feat)->operations.pwr_off.oper;

    err = cmd_strip_executor(
        (hat_cm_socket_handle_t)sock_hndl,
        nvm_M24128_power_off_seq,
        &res);

    return err;
}

static int nvm_M24128_power_on(void* sock_hndl, const void* feat, void* exec_func)
{
    int err = 0;
    hat_cm_socket_command_entry_t* nvm_M24128_power_on_seq;
    hat_cm_socket_cmd_strip_result_t res;
    hat_cm_socket_cmd_strip_executor_t cmd_strip_executor =
        (hat_cm_socket_cmd_strip_executor_t)exec_func;

    nvm_M24128_power_on_seq = (hat_cm_socket_command_entry_t*)
        ((hat_nvm_features_t *)feat)->operations.pwr_on.oper;

    err = cmd_strip_executor(
        (hat_cm_socket_handle_t)sock_hndl,
        nvm_M24128_power_on_seq,
        &res);

    return err;
}

static int nvm_M24128_init(void* sock_hndl, const void* feat, void* exec_func)
{
    int err = 0;
    hat_cm_socket_command_entry_t* nvm_M24128_init_seq;
    hat_cm_socket_cmd_strip_result_t res;
    hat_cm_socket_cmd_strip_executor_t cmd_strip_executor =
        (hat_cm_socket_cmd_strip_executor_t)exec_func;

    nvm_M24128_init_seq = (hat_cm_socket_command_entry_t*)
        ((hat_nvm_features_t *)feat)->operations.init.oper;

    if ((M24128_SIZE) < ((hat_nvm_features_t *)feat)->nvm_size_bytes)
    {
        mmsdbg(DL_ERROR, "Invalid NVM features size %d EEPROM %d !",
                ((hat_nvm_features_t *)feat)->nvm_size_bytes,
                M24128_SIZE);
    }

    err = cmd_strip_executor(
        (hat_cm_socket_handle_t)sock_hndl,
        nvm_M24128_init_seq,
        &res);

    return err;
}

static int nvm_M24128_read(void* sock_hndl, const void* feat, void* exec_func, void* buff, uint32 rd_start, uint32 rd_size)
{
    int err = 0;
    hat_cm_socket_cmd_entry_result_t cmd_res;
    hat_cm_socket_cmd_entry_executor_t cmd_entry_executor =
        (hat_cm_socket_cmd_entry_executor_t)exec_func;

    hat_cm_socket_command_entry_t nvm_read_entry [] =
    {
        NVM_INTENT_CMD_CCI_I2C_RD(rd_start, ((hat_nvm_features_t *)feat)->nvm_size_bytes, buff),
        CMD_ENTRY_END()
    };

    if ((rd_start+rd_size) >((hat_nvm_features_t *)feat)->nvm_size_bytes)
    {
        mmsdbg(DL_ERROR, "Trying to read outside EEPROM - start %d size %d EEPROM size %d !",
                rd_start,
                rd_size,
                M24128_SIZE);
        return -1;
    }


    err = cmd_entry_executor(
        (hat_cm_socket_handle_t)sock_hndl,
        nvm_read_entry,
        &cmd_res);

    return err;
}


static int nvm_M24128_write(void* sock_hndl, const void* feat, void* exec_func, void* buff, uint32 wr_start, uint32 wr_size)
{
    int err = 0, len;
    uint32 d;
    uint8_t *s;
    hat_cm_socket_cmd_entry_result_t cmd_res;
    hat_cm_socket_cmd_entry_executor_t cmd_entry_executor =
        (hat_cm_socket_cmd_entry_executor_t)exec_func;

    s = (uint8_t *)buff;
    d = wr_start;
    len = wr_size;

    if ((wr_start+wr_size) >((hat_nvm_features_t *)feat)->nvm_size_bytes)
    {
        mmsdbg(DL_ERROR, "Trying to write outside EEPROM - start %d size %d EEPROM size %d !",
                wr_start,
                wr_size,
                M24128_SIZE);
        return -1;
    }

    while (len) {
        int num = (len > M24128_PAGE_SIZE)?M24128_PAGE_SIZE:len;
        hat_cm_socket_command_entry_t nvm_write_entry [] =
        {
            NVM_INTENT_CMD_CCI_I2C_WR_BUF(d, num, s),
            NVM_DELAY(5500), // Delay 5.5ms - 5 should be enough
            CMD_ENTRY_END()
        };

        err = cmd_entry_executor(
            (hat_cm_socket_handle_t)sock_hndl,
            nvm_write_entry,
            &cmd_res);
        s += num;
        d += num;
        len -= num;
    }
    return err;
}


const hat_cm_ph_nvm_obj_t nvm_M24128_obj =
{
    .nvm_drv_id         = DTP_NVM_M24128,
    .nvm_power_off      = nvm_M24128_power_off,
    .nvm_power_on       = nvm_M24128_power_on,
    .nvm_init           = nvm_M24128_init,
    .nvm_read           = nvm_M24128_read,
    .nvm_write          = nvm_M24128_write,
};

