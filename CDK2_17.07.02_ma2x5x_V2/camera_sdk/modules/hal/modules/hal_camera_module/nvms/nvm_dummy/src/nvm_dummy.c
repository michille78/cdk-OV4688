/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file nvm_dummy.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <hal/hal_camera_module/hat_cm_socket.h>
#include <hal/hal_camera_module/hat_cm_nvm.h>
#include "../../nvm_phys.h"
#include <osal/osal_stdlib.h>
#include <osal/osal_string.h>
#include <utils/mms_debug.h>

mmsdbg_declare_variable(hal_cm_nvm);
#define MMSDEBUGLEVEL mmsdbg_use_variable(hal_cm_nvm)


static int nvm_dummy_power_off(void* sock_hndl, const void* feat, void* exec_func)
{
    int err = -1;
    UNUSED(sock_hndl);
    UNUSED(feat);
    UNUSED(exec_func);

    mmsdbg(DL_ERROR, "NOT IMPLEMENTED!!!");

    return err;
}

static int nvm_dummy_power_on(void* sock_hndl, const void* feat, void* exec_func)
{
    int err = -1;

    UNUSED(sock_hndl);
    UNUSED(feat);
    UNUSED(exec_func);

    mmsdbg(DL_ERROR, "NOT IMPLEMENTED!!!");

    return err;
}

static int nvm_dummy_init(void* sock_hndl, const void* feat, void* exec_func)
{
    int err = -1;
    UNUSED(sock_hndl);
    UNUSED(feat);
    UNUSED(exec_func);

    mmsdbg(DL_ERROR, "NOT IMPLEMENTED!!!");

    return err;
}

static int nvm_dummy_read(void* sock_hndl, const void* feat, void* exec_func, void* buff, uint32 rd_start, uint32 rd_size)
{
    int err = -1;

    UNUSED(sock_hndl);
    UNUSED(feat);
    UNUSED(exec_func);
    UNUSED(rd_start);
    UNUSED(rd_size);

    mmsdbg(DL_ERROR, "NOT IMPLEMENTED!!!");

    return err;
}

static int nvm_dummy_write(void* sock_hndl, const void* feat, void* exec_func, void* buff, uint32 wr_start, uint32 wr_size)
{
    UNUSED(sock_hndl);
    UNUSED(feat);
    UNUSED(exec_func);
    UNUSED(buff);
    UNUSED(wr_start);
    UNUSED(wr_size);

    mmsdbg(DL_ERROR, "NOT IMPLEMENTED!!!");


    return -1;
}


const hat_cm_ph_nvm_obj_t nvm_dummy_nvm_obj = {
    .nvm_drv_id         = DTP_NVM_DUMMY,
    .nvm_power_off      = nvm_dummy_power_off,
    .nvm_power_on       = nvm_dummy_power_on,
    .nvm_init           = nvm_dummy_init,
    .nvm_read           = nvm_dummy_read,
    .nvm_write          = nvm_dummy_write,
};

