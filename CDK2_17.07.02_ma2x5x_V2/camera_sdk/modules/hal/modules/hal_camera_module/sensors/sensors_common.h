
/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file sensors_common.h
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 04-May-2015 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#ifndef __SENSOR_COMMON_H__
#define __SENSOR_COMMON_H__

int sensors_common_init(void* sock_hndl, void* feat, void* exec_func, uint32 init_idx);

int sensors_common_power_off(void* sock_hndl,
    void* feat, void* exec_func);

int sensors_common_power_on(void* sock_hndl, void* feat, void* exec_func);

int sensors_common_set_grped_prm_hold(void* sock_hndl, void* feat,
    void* exec_func, uint32 hold);

int sensors_common_set_mode(void* sock_hndl, void* feat,
    void* exec_func, void *sen_cfg);

int sensors_common_stream_off(void* sock_hndl, void* feat,
    void* exec_func);

int sensors_common_stream_on(void* sock_hndl, void* feat,
    void* exec_func);

int sensors_common_set_lpfr(void* sock_hndl, void* feat,
    void* exec_func, uint32 lpfr);

#endif // __SENSOR_COMMON_H__
