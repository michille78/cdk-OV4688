/* =============================================================================
* Copyright (c) 2013-2015 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file ov4188.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <hal/hal_camera_module/hat_cm_socket.h>
#include <hal/hal_camera_module/hat_cm_sensor.h>
#include "../../sensor_phys.h"
#include "../../sensors_common.h"
#include <utils/mms_debug.h>
#include <DrvTimer.h>
mmsdbg_define_variable(
        ov4188_sen,
        DL_DEFAULT,
        0,
        "ov4188",
        "ov4188"
    );

#define MMSDEBUGLEVEL mmsdbg_use_variable(ov4188_sen)

#define BUFSET16 BUF_SET_BE16
#define GET16BUF GET_LE16_BUF

//Convert float to 8.8 fixed point format
#define FLOAT_TO_FIX88(A) ((int)((A) * 256.0f))

static uint16 ov4188_real_to_register_gain(float gain) {
  uint16 reg_gain;
  if (gain < 1.0)
    gain = 1.0;
  if (gain > 16.0)
    gain = 16.0;
  reg_gain = (uint16_t)(128 * gain);

  return reg_gain;
}



int OV4688_set_shutter(void* sock_hndl, void* exec_func, uint32 shutter)
{
	// write shutter, in number of line period
	int temp;
    int err = 0;
    static uint32 shutter_local=0;

    uint8 expL, expM,expH;
    hat_cm_socket_cmd_strip_result_t res;
    hat_cm_socket_cmd_strip_executor_t cmd_strip_executor = (hat_cm_socket_cmd_strip_executor_t)exec_func;

    /* add by xhwang
     * Max exposure time(0x3500~3) <=VTS-4. Min exposure>2 lines
     */

    if(shutter>1516)   //1516
    	shutter=1516;
    if(shutter<2)
    	shutter =2;

	shutter = shutter & 0xffff;
	temp = shutter & 0x0f;
	expL = temp << 4;
	temp = shutter & 0xfff;
	expM = temp >> 4;
	expH = shutter >> 12;

    if(shutter_local != shutter)
    {
//    	printf("shutter = %d, expl=0x%x, expm=0x%x, exph=0x%x\n",shutter, expL, expM,expH);
    	shutter_local = shutter;
    }
	hat_cm_socket_command_entry_t exp_entry[] = {
	    	SENS_INTENT_CMD_CCI_I2C_WR_BUF(0x3500, 1, &expH),
	        SENS_INTENT_CMD_CCI_I2C_WR_BUF(0x3501, 1, &expM),
			SENS_INTENT_CMD_CCI_I2C_WR_BUF(0x3502, 1, &expL),
	        CMD_ENTRY_END(),
	    };

	    err = cmd_strip_executor(
	        (hat_cm_socket_handle_t)sock_hndl,
	        (hat_cm_socket_command_entry_t *)&exp_entry,
	        &res);

	return err;
}


int OV4688_set_gain128(void* sock_hndl, void* exec_func, float gain128)
{
	// write gain, 128 = 1x

    uint16      gain;
    int         err = 0;
	uint8  gain_regh, gain_regl;
	hat_cm_socket_cmd_strip_result_t res;
	hat_cm_socket_cmd_strip_executor_t cmd_strip_executor =  (hat_cm_socket_cmd_strip_executor_t)exec_func;

//	 return 0;
    // Convert gain
		gain = ov4188_real_to_register_gain(gain128);

		if(gain >= 0x800)
			gain = 0x7ff;

		gain = gain & 0x7ff;
		if (gain < 256) {
			gain_regh = 0;
			gain_regl = gain;
		}
		else if (gain < 512) {
			gain_regh = 1;
			gain_regl = gain / 2 - 8;
		}
		else if (gain < 1024) {
			gain_regh = 3;
			gain_regl = gain / 4 - 12;
		}
		else {
			gain_regh = 7;
			gain_regl = gain / 8 - 8;
		}

//		printf("set gain= %f,  gain_regh=0x%x, gain_regl=0x%x\n",gain128,gain_regh, gain_regl);
		hat_cm_socket_command_entry_t gain_entry[] = {
	        SENS_INTENT_CMD_CCI_I2C_WR_BUF(0x3508, 1, &gain_regh),    /*high*/
			 SENS_INTENT_CMD_CCI_I2C_WR_BUF(0x3509,1, &gain_regl),   /*low*/
	        CMD_ENTRY_END(),
	    };

	    err = cmd_strip_executor(
	        (hat_cm_socket_handle_t)sock_hndl,
	        (hat_cm_socket_command_entry_t *)&gain_entry,
	        &res);

	    return err;
}

static int ov4188_set_lpfr(void* sock_hndl, void* feat,
    void* exec_func, uint32 lpfr)
{
    int err = 0;
    hat_cm_socket_cmd_strip_result_t res;
    hat_cm_socket_cmd_strip_executor_t cmd_strip_executor =
        (hat_cm_socket_cmd_strip_executor_t)exec_func;
    uint8 buf[2];
    return err;

    mmsdbg(DL_ERROR, "%s: input lpfr:%d", __FUNCTION__, lpfr);
    printf("set lpfr= %d\n",lpfr);

    BUFSET16(buf, lpfr);
    hat_cm_socket_command_entry_t lpfr_entry[] = {
        SENS_INTENT_CMD_CCI_I2C_WR_BUF(0x380E, 2, &buf[0]),
        CMD_ENTRY_END(),
    };

    err = cmd_strip_executor(
        (hat_cm_socket_handle_t)sock_hndl,
        (hat_cm_socket_command_entry_t *)&lpfr_entry,
        &res);

    return err;
}


#define SENSORS_4688_ADDR_6C  (0x6c>>1)  //J5
#define SENSORS_4688_ADDR_20  (0x20>>1)  //J6

/* 4688 I2C 读接口 */
extern int ov4688_i2c_read(unsigned int slave_addr, unsigned int reg, unsigned char *val);
/* 4688 I2C 写接口 */
extern int ov4688_i2c_write(unsigned int slave_addr, unsigned int reg, unsigned char val);


static slaveFlag = 0;
int sensors_ov4188_stream_on(void* sock_hndl, void* feat,
    void* exec_func)
{
    int err = 0;
    u8  slaveValue = 0x50;
    u8 statON = 0x01;
    u8 read;
//    hat_cm_socket_command_entry_t* stream_on_seq;
    hat_cm_socket_cmd_strip_result_t res;
    hat_cm_socket_cmd_strip_executor_t cmd_strip_executor =
        (hat_cm_socket_cmd_strip_executor_t)exec_func;

//    stream_on_seq = (hat_cm_socket_command_entry_t*)
//        ((hat_sensor_features_t *)feat)->operations.stream_on.oper;

    if(slaveFlag == 0 )
    {
    	slaveValue = 0;
    	slaveFlag = 1;
    }
    else
    {
     	slaveValue = 0;
        slaveFlag = 0;

//		ov4688_i2c_write(SENSORS_4688_ADDR_6C, 0x0100, 0x01);
//		ov4688_i2c_write(SENSORS_4688_ADDR_20, 0x0100, 0x01);
   }

    hat_cm_socket_command_entry_t stream_on_seq[] = {
	//SENS_INTENT_CMD_CCI_I2C_WR_BUF(0x3823, 1, &slaveValue),
	//SENS_INTENT_CMD_CCI_I2C_WR_BUF(0x0100, 1, &statON),
	CMD_ENTRY_END(),
    };

    if(slaveFlag == 0 )
    {
//		ov4688_i2c_write(SENSORS_4688_ADDR_6C, 0x0100, 0x01);
//		ov4688_i2c_write(SENSORS_4688_ADDR_20, 0x0100, 0x01);
   }

    err = cmd_strip_executor(
        (hat_cm_socket_handle_t)sock_hndl,
        stream_on_seq,
        &res);

    /**/
    hat_cm_socket_command_entry_t readback[] = {
    		SENS_INTENT_CMD_CCI_I2C_RD(0x3823, 1, &read),
            CMD_ENTRY_END(),
        };
        err = cmd_strip_executor(
            (hat_cm_socket_handle_t)sock_hndl,
			readback,
            &res);
      	printf("read back 0x3823 = 0x%x\n",read);


        /**/
         hat_cm_socket_command_entry_t readback2[] = {
                SENS_INTENT_CMD_CCI_I2C_RD(0x380c, 1, &read),
                 CMD_ENTRY_END(),
             };
             err = cmd_strip_executor(
                 (hat_cm_socket_handle_t)sock_hndl,
                 readback2,
                 &res);
            printf("read back 0x380c = 0x%x\n",read);

        /**/
        hat_cm_socket_command_entry_t readback3[] = {
                SENS_INTENT_CMD_CCI_I2C_RD(0x380d, 1, &read),
                CMD_ENTRY_END(),
            };
            err = cmd_strip_executor(
                (hat_cm_socket_handle_t)sock_hndl,
                readback3,
                &res);
            printf("read back 0x380d = 0x%x\n",read);
        /**/
        hat_cm_socket_command_entry_t readback4[] = {
                SENS_INTENT_CMD_CCI_I2C_RD(0x380e, 1, &read),
                CMD_ENTRY_END(),
            };
            err = cmd_strip_executor(
                (hat_cm_socket_handle_t)sock_hndl,
                readback4,
                &res);
            printf("read back 0x380e = 0x%x\n",read);
            /**/
        hat_cm_socket_command_entry_t readback5[] = {
                SENS_INTENT_CMD_CCI_I2C_RD(0x380f, 1, &read),
                CMD_ENTRY_END(),
            };
            err = cmd_strip_executor(
                (hat_cm_socket_handle_t)sock_hndl,
                readback5,
                &res);
            printf("read back 0x380f = 0x%x\n",read);

    return err;
}


static int ov4188_get_temp(void* sock_hndl, void* feat, void* exec_func, float* temp_c)
{
    return 0;
}






const hat_cm_ph_sen_obj_t ov4188_sens_obj = {
    .sen_drv_id         = DTP_SEN_OV4188,
    .power_off          = sensors_common_power_off,
    .power_on           = sensors_common_power_on,
    .init               = sensors_common_init,
    .stream_off         = sensors_common_stream_off,
	  .stream_on          = sensors_ov4188_stream_on,
	  //  .stream_on          = sensors_common_stream_on,
    .set_grped_prm_hold = sensors_common_set_grped_prm_hold,
    .set_mode           = sensors_common_set_mode,
    .set_lpfr           = NULL, /*sensors_common_set_lpfr,*/

    .set_exp            = OV4688_set_shutter,
    .set_gain           = OV4688_set_gain128,
    .get_temperature    = ov4188_get_temp,
};
