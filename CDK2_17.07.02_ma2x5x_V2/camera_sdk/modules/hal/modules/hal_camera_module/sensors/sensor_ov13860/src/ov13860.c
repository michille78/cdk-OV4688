/* =============================================================================
* Copyright (c) 2013-2015 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file ov13860.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <hal/hal_camera_module/hat_cm_socket.h>
#include <hal/hal_camera_module/hat_cm_sensor.h>
#include "../../sensor_phys.h"
#include "../../sensors_common.h"

#include <utils/mms_debug.h>

mmsdbg_define_variable(
        ov13860_sen,
        DL_DEFAULT,
        0,
        "ov13860",
        "ov13860"
    );

#define MMSDEBUGLEVEL mmsdbg_use_variable(ov13860_sen)

#define BUFSET16 BUF_SET_BE16
#define GET16BUF GET_LE16_BUF

//Convert float to 8.8 fixed point format
#define FLOAT_TO_FIX88(A) ((int)((A) * 256.0f)) 

static uint16 ov13860_real_to_register_gain(float gain) {
  uint16 reg_gain;
  if (gain < 1.0)
    gain = 1.0;
  if (gain > 16.0)
    gain = 16.0;
  reg_gain = (uint16_t)(16 * gain);

  return reg_gain;
}

static int ov13860_set_exp(void* sock_hndl, void* exec_func, uint32 exp_val)
{
    int err = 0;
    hat_cm_socket_cmd_strip_result_t res;
    hat_cm_socket_cmd_strip_executor_t cmd_strip_executor =
        (hat_cm_socket_cmd_strip_executor_t)exec_func;
    uint8 buf[2];

    BUFSET16(buf, exp_val);
    hat_cm_socket_command_entry_t exp_entry[] = {
        SENS_INTENT_CMD_CCI_I2C_WR_BUF(0x3501, 2, &buf[0]),
        CMD_ENTRY_END(),
    };

    err = cmd_strip_executor(
        (hat_cm_socket_handle_t)sock_hndl,
        (hat_cm_socket_command_entry_t *)&exp_entry,
        &res);

    return err;
}

static int ov13860_set_gain(void* sock_hndl, void* exec_func, float gain_val)
{
    uint16      gain;
    uint8       buf[2];
    int         err = 0;

    // Convert gain
    gain = ov13860_real_to_register_gain(gain_val);

    hat_cm_socket_cmd_strip_result_t res;
    hat_cm_socket_cmd_strip_executor_t cmd_strip_executor =
        (hat_cm_socket_cmd_strip_executor_t)exec_func;

    BUFSET16(&buf[0], gain);
    hat_cm_socket_command_entry_t gain_entry[] = {
        SENS_INTENT_CMD_CCI_I2C_WR_BUF(0x350A, 2, &buf[0]),
        CMD_ENTRY_END(),
    };

    err = cmd_strip_executor(
        (hat_cm_socket_handle_t)sock_hndl,
        (hat_cm_socket_command_entry_t *)&gain_entry,
        &res);

    return err;
}
/*
static int ov13860_set_lpfr(void* sock_hndl, void* feat,
    void* exec_func, uint32 lpfr)
{
    int err = 0;
    hat_cm_socket_cmd_strip_result_t res;
    hat_cm_socket_cmd_strip_executor_t cmd_strip_executor =
        (hat_cm_socket_cmd_strip_executor_t)exec_func;
    uint8 buf[2];
    return err;

    mmsdbg(DL_ERROR, "%s: input lpfr:%d", __FUNCTION__, lpfr);

    BUFSET16(buf, lpfr);
    hat_cm_socket_command_entry_t lpfr_entry[] = {
        SENS_INTENT_CMD_CCI_I2C_WR_BUF(0x380E, 2, &buf[0]),
        CMD_ENTRY_END(),
    };

    err = cmd_strip_executor(
        (hat_cm_socket_handle_t)sock_hndl,
        (hat_cm_socket_command_entry_t *)&lpfr_entry,
        &res);

    return err;
}
*/
static int ov13860_get_temp(void* sock_hndl, void* feat, void* exec_func, float* temp_c)
{
    return 0;
}


const hat_cm_ph_sen_obj_t ov13860_sens_obj = {
    .sen_drv_id         = DTP_SEN_OV13860,
    .power_off          = sensors_common_power_off,
    .power_on           = sensors_common_power_on,
    .init               = sensors_common_init,
    .stream_off         = sensors_common_stream_off,
    .stream_on          = sensors_common_stream_on,
    .set_grped_prm_hold = sensors_common_set_grped_prm_hold,
    .set_mode           = sensors_common_set_mode,
    .set_lpfr           = NULL, /*sensors_common_set_lpfr,*/

    .set_exp            = ov13860_set_exp,
    .set_gain           = ov13860_set_gain,
    .get_temperature    = ov13860_get_temp,
};
