/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file sensor_phys.h
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#ifndef __SENSOR_PHYS_H__
#define __SENSOR_PHYS_H__

#include <hal/hat_types.h>
#include <hal/hal_camera_module/hat_cm_sensor.h>
#include <dtp/dtp_server_defs.h>

typedef struct hat_cm_ph_sen_obj hat_cm_ph_sen_obj_t;

typedef hat_cm_ph_sen_obj_t* hat_cm_ph_sen_handle_t;

struct hat_cm_ph_sen_obj {
    dtp_sensors_id_t            sen_drv_id;
    int (*power_off)            (void* sock_hndl, void* feat, void* exec_func);
    int (*power_on)             (void* sock_hndl, void* feat, void* exec_func);
    int (*init)                 (void* sock_hndl, void* feat, void* exec_func, uint32 init_idx);
    int (*stream_off)           (void* sock_hndl, void* feat, void* exec_func);
    int (*stream_on)            (void* sock_hndl, void* feat, void* exec_func);
    int (*set_exp)              (void* sock_hndl, void* exec_func, uint32 exp_val);
    int (*set_gain)             (void* sock_hndl, void* exec_func, float gain_val);
    int (*set_grped_prm_hold)   (void* sock_hndl, void* feat, void* exec_func, uint32 hold);
    int (*set_lpfr)             (void* sock_hndl, void* feat, void* exec_func, uint32 lpfr_val);
    int (*set_mode)             (void* sock_hndl, void* feat, void* exec_func, void* sen_cfg);
    int (*get_temperature)      (void* sock_hndl, void* feat, void* exec_func, float* temp_cesl);

    int (*overload_set_exp_gain)(void* sock_hndl, void* prms, void* exec_func, hat_exp_gain_t* exp_gain);
    int (*overload_set_duration)(void* sock_hndl, void* prms, void* exec_func, uint32 duration);
};

#endif // __SENSOR_PHYS_H__
