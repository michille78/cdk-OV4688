/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file imx208.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <hal/hal_camera_module/hat_cm_socket.h>
#include <hal/hal_camera_module/hat_cm_sensor.h>
#include "../../sensor_phys.h"

#include <utils/mms_debug.h>

mmsdbg_define_variable(
        imx208_sen,
        DL_DEFAULT,
        0,
        "imx208",
        "imx208"
    );

#define MMSDEBUGLEVEL mmsdbg_use_variable(imx208_sen)

#define BUFSET16 BUF_SET_BE16
#define GET16BUF GET_LE16_BUF
#define FLOAT_TO_FIX88(A) ((int)((A) * 256.0f)) //Convert float to 8.8 fixed point format



static uint16 imx208_real_to_register_gain(float gain) {
  uint16 reg_gain;
  if (gain < 1.0)
    gain = 1.0;
  if (gain > 8.0)
    gain = 8.0;
  reg_gain = (uint16_t)(256.0 - 256.0 / gain);
  return reg_gain;
}

static int imx208_power_off(void* sock_hndl, void* feat, void* exec_func)
{
    int err = 0;
    hat_cm_socket_command_entry_t* imx208_power_off_seq;
    hat_cm_socket_cmd_strip_result_t res;
    hat_cm_socket_cmd_strip_executor_t cmd_strip_executor =
        (hat_cm_socket_cmd_strip_executor_t)exec_func;

    imx208_power_off_seq = (hat_cm_socket_command_entry_t*)
        ((hat_sensor_features_t *)feat)->operations.pwr_off.oper;

    err = cmd_strip_executor(
        (hat_cm_socket_handle_t)sock_hndl,
        imx208_power_off_seq,
        &res);

    return err;
}

static int imx208_power_on(void* sock_hndl, void* feat, void* exec_func)
{
    int err = 0;
    hat_cm_socket_command_entry_t* imx208_power_on_seq;
    hat_cm_socket_cmd_strip_result_t res;
    hat_cm_socket_cmd_strip_executor_t cmd_strip_executor =
        (hat_cm_socket_cmd_strip_executor_t)exec_func;

//    mmsdbg(DL_ERROR, "%s", __FUNCTION__);

    imx208_power_on_seq = (hat_cm_socket_command_entry_t*)
        ((hat_sensor_features_t *)feat)->operations.pwr_on.oper;

    err = cmd_strip_executor(
        (hat_cm_socket_handle_t)sock_hndl,
        imx208_power_on_seq,
        &res);

    return err;
}

static int imx208_init(void* sock_hndl, void* feat, void* exec_func,
    uint32 init_idx)
{
    int err = 0;
    hat_cm_socket_command_entry_t* imx208_init_seq;
    hat_cm_socket_cmd_strip_result_t res;
    hat_cm_socket_cmd_strip_executor_t cmd_strip_executor =
        (hat_cm_socket_cmd_strip_executor_t)exec_func;

    imx208_init_seq = (hat_cm_socket_command_entry_t*)
        ((hat_sensor_features_t *)feat)->operations.init[init_idx].oper;

    err = cmd_strip_executor(
        (hat_cm_socket_handle_t)sock_hndl,
        imx208_init_seq,
        &res);

    return err;
}

static int imx208_stream_off(void* sock_hndl, void* feat, void* exec_func)
{
    int err = 0;
    hat_cm_socket_command_entry_t* imx208_stream_off_seq;
    hat_cm_socket_cmd_strip_result_t res;
    hat_cm_socket_cmd_strip_executor_t cmd_strip_executor =
        (hat_cm_socket_cmd_strip_executor_t)exec_func;

//    mmsdbg(DL_ERROR, "%s: start", __FUNCTION__);

    imx208_stream_off_seq = (hat_cm_socket_command_entry_t*)
        ((hat_sensor_features_t *)feat)->operations.stream_off.oper;
    err = cmd_strip_executor(
        (hat_cm_socket_handle_t)sock_hndl,
        imx208_stream_off_seq,
        &res);

//    mmsdbg(DL_ERROR, "%s: return", __FUNCTION__);

    return err;
}

static int imx208_stream_on(void* sock_hndl, void* feat, void* exec_func)
{
    int err = 0;
    hat_cm_socket_command_entry_t* imx208_stream_on_seq;
    hat_cm_socket_cmd_strip_result_t res;
    hat_cm_socket_cmd_strip_executor_t cmd_strip_executor =
        (hat_cm_socket_cmd_strip_executor_t)exec_func;

//    mmsdbg(DL_ERROR, "%s: start", __FUNCTION__);
    imx208_stream_on_seq = (hat_cm_socket_command_entry_t*)
        ((hat_sensor_features_t *)feat)->operations.stream_on.oper;

    err = cmd_strip_executor(
        (hat_cm_socket_handle_t)sock_hndl,
        imx208_stream_on_seq,
        &res);

//    mmsdbg(DL_ERROR, "%s: return", __FUNCTION__);
    return err;
}



static int imx208_set_exp(void* sock_hndl, void* exec_func, uint32 exp_val)
{
    int err = 0;
    hat_cm_socket_cmd_strip_result_t res;
    hat_cm_socket_cmd_strip_executor_t cmd_strip_executor =
        (hat_cm_socket_cmd_strip_executor_t)exec_func;
    uint8 buf[2];

//    mmsdbg(DL_ERROR, "%s: input exposure:%d", __FUNCTION__, exp_val);

    BUFSET16(buf, exp_val);
    hat_cm_socket_command_entry_t exp_entry[] = {
        SENS_INTENT_CMD_CCI_I2C_WR_BUF(0x0202, 2, &buf[0]),
        CMD_ENTRY_END(),
    };

    err = cmd_strip_executor(
        (hat_cm_socket_handle_t)sock_hndl,
        (hat_cm_socket_command_entry_t *)&exp_entry,
        &res);

    return err;
}

static int imx208_set_gain(void* sock_hndl, void* exec_func, float gain_val)
{
    uint16      gain,gain_digital;
    float       digital_gain = 1.0;
    uint8       buf1[2], buf2[2*4];
    int         err = 0;


    while (8.0 < gain_val) {
        digital_gain *= 2.0;
        gain_val /= digital_gain;
    }

    // Convert gain
    gain = imx208_real_to_register_gain(gain_val);
    gain_digital = FLOAT_TO_FIX88(digital_gain);

    hat_cm_socket_cmd_strip_result_t res;
    hat_cm_socket_cmd_strip_executor_t cmd_strip_executor =
        (hat_cm_socket_cmd_strip_executor_t)exec_func;

    BUFSET8(&buf1[0], gain);
    BUFSET16(&buf2[0], gain_digital);
    BUFSET16(&buf2[2], gain_digital);
    BUFSET16(&buf2[4], gain_digital);
    BUFSET16(&buf2[6], gain_digital);
    hat_cm_socket_command_entry_t gain_entry[] = {
        SENS_INTENT_CMD_CCI_I2C_WR_BUF(0x0205, 1, &buf1[0]),

        SENS_INTENT_CMD_CCI_I2C_WR_BUF(0x020e, 8, &buf2[0]),

        CMD_ENTRY_END(),
    };

    err = cmd_strip_executor(
        (hat_cm_socket_handle_t)sock_hndl,
        (hat_cm_socket_command_entry_t *)&gain_entry,
        &res);

    return err;
}



static int imx208_set_grped_prm_hold(void* sock_hndl, void* feat,
    void* exec_func, uint32 hold)
{
    int err = 0;
    hat_cm_socket_cmd_strip_result_t res;
    hat_cm_socket_cmd_strip_executor_t cmd_strip_executor =
        (hat_cm_socket_cmd_strip_executor_t)exec_func;
    uint8 buf[1];

    return 0;
    BUFSET8(buf, hold);
    hat_cm_socket_command_entry_t hold_entry[] = {
        SENS_INTENT_CMD_CCI_I2C_WR_BUF(0x104, 1, buf),
        CMD_ENTRY_END(),
    };

    err = cmd_strip_executor(
        (hat_cm_socket_handle_t)sock_hndl,
        (hat_cm_socket_command_entry_t *)&hold_entry,
        &res);

    return err;
}



static int imx208_set_lpfr(void* sock_hndl, void* feat,
    void* exec_func, uint32 lpfr)
{
    int err = 0;
    hat_cm_socket_cmd_strip_result_t res;
    hat_cm_socket_cmd_strip_executor_t cmd_strip_executor =
        (hat_cm_socket_cmd_strip_executor_t)exec_func;
    uint8 buf[2];

//    mmsdbg(DL_ERROR, "%s: input lpfr:%d", __FUNCTION__, lpfr);
    BUFSET16(buf, lpfr);
    hat_cm_socket_command_entry_t lpfr_entry[] = {
        SENS_INTENT_CMD_CCI_I2C_WR_BUF(0x0340, 2, &buf[0]),
        CMD_ENTRY_END(),
    };

    err = cmd_strip_executor(
        (hat_cm_socket_handle_t)sock_hndl,
        (hat_cm_socket_command_entry_t *)&lpfr_entry,
        &res);

    return err;
}



static int imx208_set_mode(void* sock_hndl, void* feat, void* exec_func, void *sen_cfg)
{
    int err = 0;
    int mode_idx = ((hat_sen_config_t *)sen_cfg)->sensor_mode_idx;
    hat_cm_socket_command_entry_t* mode_strip;
    hat_cm_socket_cmd_strip_result_t res;

//    mmsdbg(DL_ERROR, "%s: input mode:%d", __FUNCTION__, mode_idx);
    /* Currently mode is selected depending on the mode index only */
    mode_strip = (hat_cm_socket_command_entry_t*)
        ((hat_sensor_features_t *)feat)->modes.list[mode_idx].mode_settings;

    hat_cm_socket_cmd_strip_executor_t cmd_strip_executor =
        (hat_cm_socket_cmd_strip_executor_t)exec_func;

    err = cmd_strip_executor(
        (hat_cm_socket_handle_t)sock_hndl,
        mode_strip,
        &res);

    return err;
}

static int imx208_get_temp(void* sock_hndl, void* feat, void* exec_func, float* temp_c)
{
    return 0;
}


const hat_cm_ph_sen_obj_t imx208_sens_obj = {
    .sen_drv_id         = DTP_SEN_IMX208,
    .power_off          = imx208_power_off,
    .power_on           = imx208_power_on,
    .init               = imx208_init,
    .stream_off         = imx208_stream_off,
    .stream_on          = imx208_stream_on,
    .set_exp            = imx208_set_exp,
    .set_gain           = imx208_set_gain,
    .set_grped_prm_hold = imx208_set_grped_prm_hold,
    .set_lpfr           = imx208_set_lpfr,
    .set_mode           = imx208_set_mode,
    .get_temperature    = imx208_get_temp,
};
