/* =============================================================================
* Copyright (c) 2013-2015 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file ov5658.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <hal/hal_camera_module/hat_cm_socket.h>
#include <hal/hal_camera_module/hat_cm_sensor.h>
#include "../../sensor_phys.h"
#include "../../sensors_common.h"

#include <utils/mms_debug.h>

mmsdbg_define_variable(
        ov5658_sen,
        DL_DEFAULT,
        0,
        "ov5658",
        "ov5658"
    );

#define MMSDEBUGLEVEL mmsdbg_use_variable(ov5658_sen)

#define BUFSET16 BUF_SET_BE16
#define GET16BUF GET_LE16_BUF

//Convert float to 8.8 fixed point format
#define FLOAT_TO_FIX88(A) ((int)((A) * 256.0f))

//Convert float to integer
#define OV_FLOAT_TO_INT(flt) ((int)((flt) * 512 + 0.5))
//Convert integer to float
#define OV_INT_TO_FLOAT(itg) ((float)((itg) / 512.0))

#define NUM_CAMS    (3)

#define GOLDEN_RG_RATIO (0.7436)
#define GOLDEN_BG_RATIO (0.7050)

// Use this for OTP without proper flags
#define IGNORE_OTP_FLAG

#define OTP_READ_RETRY_MAX  (5)


typedef struct otp_struct {
    int flag; // bit[7]: info
    int module_integrator_id;
    int lens_id;
    int production_year;
    int production_month;
    int production_day;
    int rg_ratio;
    int bg_ratio;
} otp_struct_t;

static otp_struct_t sensor_otp[NUM_CAMS];

static int RG_Ratio_Typical = OV_FLOAT_TO_INT(GOLDEN_RG_RATIO);
static int BG_Ratio_Typical = OV_FLOAT_TO_INT(GOLDEN_BG_RATIO);


static uint16 ov5658_real_to_register_gain(float gain) {
    uint16 reg_gain;

    // sanity
    if (gain < 1.0)
        gain = 1.0;
    if (gain > 15.5)
        gain = 15.5;

    // divide in 4 regions with different steps
    if (gain <= 2.0) {
        // (1 - 2] - 1/16
        reg_gain = ((uint16)(16 * gain) >> 0) << 0;
    } else if (gain <= 4.0) {
        // (2 - 4] - 1/8
        reg_gain = ((uint16)(16 * gain) >> 1) << 1;
    } else if (gain <= 8.0) {
        // (4 - 8] - 1/4
        reg_gain = ((uint16)(16 * gain) >> 2) << 2;
    } else {
        // (8 -16] - 1/2
        reg_gain = ((uint16)(16 * gain) >> 3) << 3;
    }

    return reg_gain;;
}

static int ov5658_set_exp(void* sock_hndl, void* exec_func, uint32 exp_val)
{
    int err = 0;
    hat_cm_socket_cmd_strip_result_t res;
    hat_cm_socket_cmd_strip_executor_t cmd_strip_executor =
        (hat_cm_socket_cmd_strip_executor_t)exec_func;
    uint8 reg[3];
    uint32 x;

    // only bits [19:4], low 4 bits are fraction and should always be 0
    x = (exp_val << 4) & ((1 << 20) - 1);

    //      0x3500  0x3501     0x3502
    // xxxx[19:16]  [15:8]  [7:4]0000
    reg[0] = (uint8)((x >> 16) & 0xFF);
    reg[1] = (uint8)((x >>  8) & 0xFF);
    reg[2] = (uint8)((x >>  0) & 0xFF);

    hat_cm_socket_command_entry_t exp_entry[] = {
        SENS_INTENT_CMD_CCI_I2C_WR_BUF(0x3500, 3, &reg[0]),
        CMD_ENTRY_END(),
    };

    err = cmd_strip_executor(
        (hat_cm_socket_handle_t)sock_hndl,
        (hat_cm_socket_command_entry_t *)&exp_entry,
        &res);

    return err;
}

static int ov5658_set_gain(void* sock_hndl, void* exec_func, float gain_val)
{
    uint16      gain;
    uint8       buf[2];
    int         err = 0;

    // Convert gain
    gain = ov5658_real_to_register_gain(gain_val);

    hat_cm_socket_cmd_strip_result_t res;
    hat_cm_socket_cmd_strip_executor_t cmd_strip_executor =
        (hat_cm_socket_cmd_strip_executor_t)exec_func;

    BUFSET16(&buf[0], gain);
    hat_cm_socket_command_entry_t gain_entry[] = {
        SENS_INTENT_CMD_CCI_I2C_WR_BUF(0x350A, 2, &buf[0]),
        CMD_ENTRY_END(),
    };

    err = cmd_strip_executor(
        (hat_cm_socket_handle_t)sock_hndl,
        (hat_cm_socket_command_entry_t *)&gain_entry,
        &res);

    return err;
}

static int ov5658_set_lpfr(void* sock_hndl, void* feat,
    void* exec_func, uint32 lpfr)
{
    int err = 0;
    hat_cm_socket_cmd_strip_result_t res;
    hat_cm_socket_cmd_strip_executor_t cmd_strip_executor =
        (hat_cm_socket_cmd_strip_executor_t)exec_func;
    uint8 buf[2];
    return err;

    mmsdbg(DL_ERROR, "%s: input lpfr:%d", __FUNCTION__, lpfr);

    BUFSET16(buf, lpfr);
    hat_cm_socket_command_entry_t lpfr_entry[] = {
        SENS_INTENT_CMD_CCI_I2C_WR_BUF(0x380E, 2, &buf[0]),
        CMD_ENTRY_END(),
    };

    err = cmd_strip_executor(
        (hat_cm_socket_handle_t)sock_hndl,
        (hat_cm_socket_command_entry_t *)&lpfr_entry,
        &res);

    return err;
}

static int ov5658_get_temp(void* sock_hndl, void* feat, void* exec_func, float* temp_c)
{
    return 0;
}

// Read from I2C
static int wrapper_rd_i2c(void* sock_hndl, void* exec_func, uint16 addr, uint8 *buff, uint8 num)
{
    hat_cm_socket_cmd_strip_result_t res;
    hat_cm_socket_cmd_strip_executor_t cmd_strip_executor =
        (hat_cm_socket_cmd_strip_executor_t)exec_func;

    hat_cm_socket_command_entry_t seq[] = {
        SENS_INTENT_CMD_CCI_I2C_RD(addr, num, buff),
        CMD_ENTRY_END(),
    };

    return cmd_strip_executor(
        (hat_cm_socket_handle_t)sock_hndl,
        (hat_cm_socket_command_entry_t *)&seq,
        &res);
}

// Write to I2C
static int wrapper_wr_i2c(void* sock_hndl, void* exec_func, uint16 addr, uint8 *buff, uint8 num)
{
    hat_cm_socket_cmd_strip_result_t res;
    hat_cm_socket_cmd_strip_executor_t cmd_strip_executor =
        (hat_cm_socket_cmd_strip_executor_t)exec_func;

    hat_cm_socket_command_entry_t seq[] = {
        SENS_INTENT_CMD_CCI_I2C_WR_BUF(addr, num, buff),
        CMD_ENTRY_END(),
    };

    return cmd_strip_executor(
        (hat_cm_socket_handle_t)sock_hndl,
        (hat_cm_socket_command_entry_t *)&seq,
        &res);
}

// delay, uS
static int wrapper_delay(void* sock_hndl, void* exec_func, uint32 us)
{
    hat_cm_socket_cmd_strip_result_t res;
    hat_cm_socket_cmd_strip_executor_t cmd_strip_executor =
        (hat_cm_socket_cmd_strip_executor_t)exec_func;

    hat_cm_socket_command_entry_t seq[] = {
        SENS_DELAY(us),
        CMD_ENTRY_END(),
    };

    return cmd_strip_executor(
        (hat_cm_socket_handle_t)sock_hndl,
        (hat_cm_socket_command_entry_t *)&seq,
        &res);
}

static int ov5658_get_otp(void* sock_hndl, void* exec_func, otp_struct_t *otp)
{
    int err = 0;
    int addr, i;
    uint8 otp_data[8] = {0};
    uint8 otp_flag;
    uint8 otp_cmd;
    uint8 temp_ratio;
    uint8 temp;

    hat_cm_socket_cmd_strip_result_t res;
    hat_cm_socket_cmd_strip_executor_t cmd_strip_executor =
        (hat_cm_socket_cmd_strip_executor_t)exec_func;

    // Set 0x5002[1] to (Disable OTP)
    // read 0x5002
    err |= wrapper_rd_i2c(sock_hndl, exec_func, 0x5002, &otp_cmd, 1);

    // modify and write back 0x5002
    otp_cmd &=(~0x02);
    err |= wrapper_wr_i2c(sock_hndl, exec_func, 0x5002, &otp_cmd, 1);

    // Read OTP into buffer
    otp_cmd = 0xC0; // bank 0
    err |= wrapper_wr_i2c(sock_hndl, exec_func, 0x3d84, &otp_cmd, 1);

    otp_cmd = 0x01; // load otp into buffer
    err |= wrapper_wr_i2c(sock_hndl, exec_func, 0x3d81, &otp_cmd, 1);

    // Delay 5ms;
    wrapper_delay(sock_hndl, exec_func, 5000);

    // OTP base information
    err |= wrapper_rd_i2c(sock_hndl, exec_func, 0x3d05, &otp_flag, 1);

    addr = 0;
#ifdef IGNORE_OTP_FLAG
    otp_flag = 0x40; // force the otp flag
#endif
    if((otp_flag & 0xc0) == 0x40) {
        addr = 0x3d06; // base address of WB Calibration group 1
    } else if((otp_flag & 0x30) == 0x10) {
        addr = 0x3d0b; // base address of WB Calibration group 2
    } else if((otp_flag & 0x0c) == 0x04) {
        addr = 0x3d10; // base address of info group 3
    }

    if(addr != 0) {
        (*otp).flag |= 0x80;

        err |= wrapper_rd_i2c(sock_hndl, exec_func, addr, &otp_data[0], 5);

        (*otp).module_integrator_id = otp_data[0];
        (*otp).lens_id = otp_data[1];
        (*otp).production_year = otp_data[2];
        (*otp).production_month = otp_data[3];
        (*otp).production_day = otp_data[4];
    } else {
        (*otp).flag = 0x00; // no info in OTP
        (*otp).module_integrator_id = 0;
        (*otp).lens_id = 0;
        (*otp).production_year = 0;
        (*otp).production_month = 0;
        (*otp).production_day = 0;
    }

    // OTP WB Calibration
    err |= wrapper_rd_i2c(sock_hndl, exec_func, 0x3d15, &otp_flag, 1);
    addr = 0;
    if((otp_flag & 0xc0) == 0x40) {
        addr = 0x3d16; // base address of WB Calibration group 1
    } else if((otp_flag & 0x30) == 0x10) {
        addr = 0x3d19; // base address of WB Calibration group 2
    } else if((otp_flag & 0x0c) == 0x04) {
        addr = 0x3d1c; // base address of WB Calibration group 3
    }

    if (addr != 0) {
        (*otp).flag |= 0x40;
        err |= wrapper_rd_i2c(sock_hndl, exec_func, (addr + 2), &temp, 1);
        err |= wrapper_rd_i2c(sock_hndl, exec_func, addr, &temp_ratio, 1);
        (*otp).rg_ratio = (temp_ratio << 2) + ((temp >> 6) & 0x03);
        err |= wrapper_rd_i2c(sock_hndl, exec_func, (addr + 1), &temp_ratio, 1);
        (*otp).bg_ratio = (temp_ratio << 2) + ((temp >> 4) & 0x03);
    } else {
        (*otp).rg_ratio = 0;
        (*otp).bg_ratio = 0;
    }

    if ((!(*otp).rg_ratio) || (!(*otp).bg_ratio)) {
        // RG and BG ratios are not valid
        (*otp).flag &= ~0x80;
    }

    // Clear OTP buffer
    uint8 buff_clr = 0;
    for (i = 0x3d05; i <= 0x3d1e; i++) {
        err |= wrapper_wr_i2c(sock_hndl, exec_func, i, &buff_clr, 1);
    }

    //set 0x5002[1] to (Enable OTP)
    // read 0x5002
    err |= wrapper_rd_i2c(sock_hndl, exec_func, 0x5002, &otp_cmd, 1);
    // modify and write back 0x5002
    otp_cmd |= 0x02;
    err |= wrapper_wr_i2c(sock_hndl, exec_func, 0x5002, &otp_cmd, 1);

    return err;
}

static int ov5658_apply_otp(void* sock_hndl, void* exec_func, otp_struct_t *otp)
{
    int err = 0;
    int rg, bg, R_gain, G_gain, B_gain, Base_gain;
    uint8   buff[2];

    hat_cm_socket_cmd_strip_result_t res;
    hat_cm_socket_cmd_strip_executor_t cmd_strip_executor =
        (hat_cm_socket_cmd_strip_executor_t)exec_func;

    rg = (*otp).rg_ratio;
    bg = (*otp).bg_ratio;

    // apply OTP WB Calibration
    if (((*otp).flag & 0x80) && rg && bg) {

        //calculate G gain
        R_gain = (RG_Ratio_Typical * 1000) / rg;
        B_gain = (BG_Ratio_Typical * 1000) / bg;
        G_gain = 1000;

        if (R_gain < 1000 || B_gain < 1000) {
            if (R_gain < B_gain)
                Base_gain = R_gain;
            else
                Base_gain = B_gain;
        } else {
            Base_gain = G_gain;
        }

        R_gain = 0x400 * R_gain / (Base_gain);
        B_gain = 0x400 * B_gain / (Base_gain);
        G_gain = 0x400 * G_gain / (Base_gain);

        // Enable manual AWB
        buff[0] = 0x01;
        err |= wrapper_wr_i2c(sock_hndl, exec_func, 0x5001, &buff[0], 1);

        // Update sensor WB gain
        // R gain
        if (R_gain > 0x400) {
            buff[0] = R_gain >> 8;
            buff[1] = R_gain & 0x00ff;
            err |= wrapper_wr_i2c(sock_hndl, exec_func, 0x3400, &buff[0], 1);
            err |= wrapper_wr_i2c(sock_hndl, exec_func, 0x3401, &buff[1], 1);
        }
        // G gain
        if (G_gain > 0x400) {
            buff[0] = G_gain >> 8;
            buff[1] = G_gain & 0x00ff;
            err |= wrapper_wr_i2c(sock_hndl, exec_func, 0x3402, &buff[0], 1);
            err |= wrapper_wr_i2c(sock_hndl, exec_func, 0x3403, &buff[1], 1);
        }
        // B gain
        if (B_gain > 0x400) {
            buff[0] = B_gain >> 8;
            buff[1] = B_gain & 0x00ff;
            err |= wrapper_wr_i2c(sock_hndl, exec_func, 0x3404, &buff[0], 1);
            err |= wrapper_wr_i2c(sock_hndl, exec_func, 0x3405, &buff[1], 1);
        }
    } else {
        mmsdbg(DL_ERROR, "%s: No OTP data.", __FUNCTION__);
    }

    return err;
}

static int ov5658_awb_calibration(void* sock_hndl, void* exec_func)
{
    int err = 0;
    static int cam_idx = 0;
    int otp_retry = 0;
    int otp_apply = 0;

    if (NUM_CAMS <= cam_idx) cam_idx = 0;

#if 0
    // Hardcoded Calibration data
    sensor_otp[0].rg_ratio = OV_FLOAT_TO_INT(0.5824);
    sensor_otp[0].bg_ratio = OV_FLOAT_TO_INT(0.7771);

    sensor_otp[1].rg_ratio = OV_FLOAT_TO_INT(0.5583);
    sensor_otp[1].bg_ratio = OV_FLOAT_TO_INT(0.7700);

    sensor_otp[2].rg_ratio = OV_FLOAT_TO_INT(0.5797);
    sensor_otp[2].bg_ratio = OV_FLOAT_TO_INT(0.7683);

    // Force OTP valid flag
    sensor_otp[0].flag |= 0x80;
    sensor_otp[1].flag |= 0x80;
    sensor_otp[2].flag |= 0x80;

    otp_apply = 1;
#else
    do {
        otp_retry++;
        // wait before reading OTP
        wrapper_delay(sock_hndl, exec_func, 20 * 1000);
        // Read sensor OTP
        ov5658_get_otp(sock_hndl, exec_func, &sensor_otp[cam_idx]);
        // check for valid OTP data
        if ((sensor_otp[cam_idx].flag & 0x80) &&
             sensor_otp[cam_idx].rg_ratio &&
             sensor_otp[cam_idx].bg_ratio) {
            otp_apply = 1;
            break;
        }
    } while(OTP_READ_RETRY_MAX > otp_retry);
#endif

    if (otp_apply) {
        // Apply calibration data
        ov5658_apply_otp(sock_hndl, exec_func, &sensor_otp[cam_idx]);
    } else {
        mmsdbg(DL_ERROR, "%s: No OTP data (%d retries).", __FUNCTION__, otp_retry);
    }

    // Next camera
    cam_idx++;

    return err;
}

static int ov5658_enable_single_vsync(void* sock_hndl, void* exec_func)
{
    int err = 0;
    uint8 ctrl_reg;
    uint16 reg_3824_3825;
    uint16 reg_3826_3827;
    uint16 reg_380c_380d;
    uint16 reg_380e_380f;
    static int sync_idx = 0;

    hat_cm_socket_cmd_strip_result_t res;
    hat_cm_socket_cmd_strip_executor_t cmd_strip_executor =
        (hat_cm_socket_cmd_strip_executor_t)exec_func;

    if (NUM_CAMS <= sync_idx) {
        sync_idx = 0;
    }

    if (0 == sync_idx) {
        // set master sensor
        // Set 0x3002[2] to (VSYNC/FSIN - Output)
        // read 0x3002
        err |= wrapper_rd_i2c(sock_hndl, exec_func, 0x3002, &ctrl_reg, 1);
        // modify and write back 0x3002
        ctrl_reg |= (1 << 2);
        err |= wrapper_wr_i2c(sock_hndl, exec_func, 0x3002, &ctrl_reg, 1);
    } else {
        // set slave sensors
        ctrl_reg = 0x0a;
        err |= wrapper_wr_i2c(sock_hndl, exec_func, 0x3001, &ctrl_reg, 1);
        ctrl_reg = 0xc0;
        err |= wrapper_wr_i2c(sock_hndl, exec_func, 0x3002, &ctrl_reg, 1);
        ctrl_reg = 0x50; // <6> FSIN function enable, <4> initialize cs (column) and r (row) counter manually
        err |= wrapper_wr_i2c(sock_hndl, exec_func, 0x3823, &ctrl_reg, 1);

        // Read 0x380c/0x380d and 0x380e/0x380f and save them with the correct endianness
        err |= wrapper_rd_i2c(sock_hndl, exec_func, 0x380c, &reg_380c_380d, 2);
        reg_380c_380d = (reg_380c_380d << 8) | (reg_380c_380d >> 8);
        err |= wrapper_rd_i2c(sock_hndl, exec_func, 0x380e, &reg_380e_380f, 2);
        reg_380e_380f = (reg_380e_380f << 8) | (reg_380e_380f >> 8);

        // The Column Counter (0x3824/0x3825), it shall be set to around (0x380c/0x380d)/8
        reg_3824_3825 = reg_380c_380d / 8;
        // The Row Counter (0x3826/0x3827), OV5658 shall be set 2*(0x380e/0x380f) – 4
        reg_3826_3827 = 2 * reg_380e_380f - 4;

        // write the calculated values
        ctrl_reg = reg_3824_3825 >> 8;
        err |= wrapper_wr_i2c(sock_hndl, exec_func, 0x3824, &ctrl_reg, 1);
        ctrl_reg = reg_3824_3825 & 0xFF;
        err |= wrapper_wr_i2c(sock_hndl, exec_func, 0x3825, &ctrl_reg, 1);

        ctrl_reg = reg_3826_3827 >> 8;
        err |= wrapper_wr_i2c(sock_hndl, exec_func, 0x3826, &ctrl_reg, 1);
        ctrl_reg = reg_3826_3827 & 0xFF;
        err |= wrapper_wr_i2c(sock_hndl, exec_func, 0x3827, &ctrl_reg, 1);
    }

    sync_idx++;

    return err;
}

int ov5658_stream_on(void* sock_hndl, void* feat,
    void* exec_func)
{
    int err = 0;
    hat_cm_socket_command_entry_t* stream_on_seq;
    hat_cm_socket_cmd_strip_result_t res;
    hat_cm_socket_cmd_strip_executor_t cmd_strip_executor =
        (hat_cm_socket_cmd_strip_executor_t)exec_func;

    // Enable VSYNC signal for the first camera only
    ov5658_enable_single_vsync(sock_hndl, exec_func);

    stream_on_seq = (hat_cm_socket_command_entry_t*)
        ((hat_sensor_features_t *)feat)->operations.stream_on.oper;

    err = cmd_strip_executor(
        (hat_cm_socket_handle_t)sock_hndl,
        stream_on_seq,
        &res);

    // Calculate and apply AWB calibration
    ov5658_awb_calibration(sock_hndl, exec_func);

    return err;
}

const hat_cm_ph_sen_obj_t ov5658_sens_obj = {
    .sen_drv_id         = DTP_SEN_OV5658,
    .power_off          = sensors_common_power_off,
    .power_on           = sensors_common_power_on,
    .init               = sensors_common_init,
    .stream_off         = sensors_common_stream_off,
    .stream_on          = sensors_common_stream_on, // ov5658_stream_on,
    .set_grped_prm_hold = sensors_common_set_grped_prm_hold,
    .set_mode           = sensors_common_set_mode,
    .set_lpfr           = NULL, /*sensors_common_set_lpfr,*/

    .set_exp            = ov5658_set_exp,
    .set_gain           = ov5658_set_gain,
    .get_temperature    = ov5658_get_temp,
};
