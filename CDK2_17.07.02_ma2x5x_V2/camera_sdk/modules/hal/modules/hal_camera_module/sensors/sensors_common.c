
/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file sensors_common.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 04-May-2015 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <hal/hal_camera_module/hat_cm_socket.h>
#include <hal/hal_camera_module/hat_cm_sensor.h>
#include "sensors.h"
#include "sensor_dummy/inc/sen_dummy.h"

#include <utils/mms_debug.h>

mmsdbg_declare_variable(hal_cm_driver);
#define MMSDEBUGLEVEL mmsdbg_use_variable(hal_cm_driver)

int sensors_common_init(void* sock_hndl, void* feat, void* exec_func,
    uint32 init_idx)
{
    int err = 0;
    hat_cm_socket_command_entry_t* init_seq;
    hat_cm_socket_cmd_strip_result_t res;
    hat_cm_socket_cmd_strip_executor_t cmd_strip_executor =
        (hat_cm_socket_cmd_strip_executor_t)exec_func;

    init_seq = (hat_cm_socket_command_entry_t*)
        ((hat_sensor_features_t *)feat)->operations.init[init_idx].oper;

    err = cmd_strip_executor(
        (hat_cm_socket_handle_t)sock_hndl,
        init_seq,
        &res);

    return err;
}

int sensors_common_power_off(void* sock_hndl,
    void* feat, void* exec_func)
{
    int err = 0;
    hat_cm_socket_command_entry_t* power_off_seq;
    hat_cm_socket_cmd_strip_result_t res;
    hat_cm_socket_cmd_strip_executor_t cmd_strip_executor =
        (hat_cm_socket_cmd_strip_executor_t)exec_func;

    power_off_seq = (hat_cm_socket_command_entry_t*)
        ((hat_sensor_features_t *)feat)->operations.pwr_off.oper;

    err = cmd_strip_executor(
        (hat_cm_socket_handle_t)sock_hndl,
        power_off_seq,
        &res);

    return err;
}

int sensors_common_power_on(void* sock_hndl, void* feat, void* exec_func)
{
    int err = 0;
    hat_cm_socket_command_entry_t* power_on_seq;
    hat_cm_socket_cmd_strip_result_t res;
    hat_cm_socket_cmd_strip_executor_t cmd_strip_executor =
        (hat_cm_socket_cmd_strip_executor_t)exec_func;

    power_on_seq = (hat_cm_socket_command_entry_t*)
        ((hat_sensor_features_t *)feat)->operations.pwr_on.oper;

    err = cmd_strip_executor(
        (hat_cm_socket_handle_t)sock_hndl,
        power_on_seq,
        &res);

    return err;
}

int sensors_common_set_grped_prm_hold(void* sock_hndl, void* feat,
    void* exec_func, uint32 hold)
{
    int err = 0;
    hat_cm_socket_cmd_strip_result_t res;
    hat_cm_socket_command_entry_t* gph_on_seq;
    hat_cm_socket_cmd_strip_executor_t cmd_strip_executor =
        (hat_cm_socket_cmd_strip_executor_t)exec_func;

    if (!feat) {
        mmsdbg(DL_ERROR, "failed: feat=NULL")
        return -1;
    }

    if (hold) {
        gph_on_seq = (hat_cm_socket_command_entry_t*)
            ((hat_sensor_features_t *)feat)->operations.gph_on.oper;
    } else {
        gph_on_seq = (hat_cm_socket_command_entry_t*)
            ((hat_sensor_features_t *)feat)->operations.gph_off.oper;
    }

    if (!gph_on_seq) {
        mmsdbg(DL_ERROR, "failed: sensor GPH")
        return -1;
    }

    err = cmd_strip_executor(
        (hat_cm_socket_handle_t)sock_hndl,
        gph_on_seq,
        &res);

    return err;
}


int sensors_common_set_mode(void* sock_hndl, void* feat,
    void* exec_func, void *sen_cfg)
{
    int err = 0;
    int mode_idx = ((hat_sen_config_t *)sen_cfg)->sensor_mode_idx;
    hat_cm_socket_command_entry_t* mode_strip;
    hat_cm_socket_cmd_strip_result_t res;

    /* Currently mode is selected depending on the mode index only */
    mode_strip = (hat_cm_socket_command_entry_t*)
        ((hat_sensor_features_t *)feat)->modes.list[mode_idx].mode_settings;

    hat_cm_socket_cmd_strip_executor_t cmd_strip_executor =
        (hat_cm_socket_cmd_strip_executor_t)exec_func;

    err = cmd_strip_executor(
        (hat_cm_socket_handle_t)sock_hndl,
        mode_strip,
        &res);

    return err;
}

int sensors_common_stream_off(void* sock_hndl, void* feat,   
    void* exec_func)
{
    int err = 0;
    hat_cm_socket_command_entry_t* stream_off_seq;
    hat_cm_socket_cmd_strip_result_t res;
    hat_cm_socket_cmd_strip_executor_t cmd_strip_executor =
        (hat_cm_socket_cmd_strip_executor_t)exec_func;
return 0;
    stream_off_seq = (hat_cm_socket_command_entry_t*)
        ((hat_sensor_features_t *)feat)->operations.stream_off.oper;
    err = cmd_strip_executor(
        (hat_cm_socket_handle_t)sock_hndl,
        stream_off_seq,
        &res);

    return err;
}

int sensors_common_stream_on(void* sock_hndl, void* feat,
    void* exec_func)
{
    int err = 0;
    hat_cm_socket_command_entry_t* stream_on_seq;
    hat_cm_socket_cmd_strip_result_t res;
    hat_cm_socket_cmd_strip_executor_t cmd_strip_executor =
        (hat_cm_socket_cmd_strip_executor_t)exec_func;

    stream_on_seq = (hat_cm_socket_command_entry_t*)
        ((hat_sensor_features_t *)feat)->operations.stream_on.oper;

    err = cmd_strip_executor(
        (hat_cm_socket_handle_t)sock_hndl,
        stream_on_seq,
        &res);

    return err;
}

int sensors_common_set_lpfr(void* sock_hndl, void* feat,
    void* exec_func, uint32 lpfr)
{
    int err = 0, i = 0;
    hat_cm_socket_cmd_strip_result_t res;
    hat_cm_socket_cmd_strip_executor_t cmd_strip_executor =
        (hat_cm_socket_cmd_strip_executor_t)exec_func;
    uint8 buf[4] = {0,0,0,0};
    hat_sensor_frame_size_regs_t *frame_size_regs =
        &((hat_sensor_features_t *)feat)->frame_size_regs;
    uint8 *t;

    if ((frame_size_regs->frm_length_lines_size <= 0) ||
        (frame_size_regs->frm_length_lines_size > 4)) {
        mmsdbg(DL_ERROR, "failed : invalid size: %d",
            frame_size_regs->frm_length_lines_size);
        return -1;
    }

    t = (uint8*)&lpfr;
    t = t + frame_size_regs->frm_length_lines_size - 1;
    for (i = 0; i<frame_size_regs->frm_length_lines_size; i++) {
        buf[i] = *t;
        t--;
    }

   // mmsdbg(DL_ERROR, "%s: input lpfr:%d", __FUNCTION__, lpfr);

    hat_cm_socket_command_entry_t lpfr_entry[] = {
        SENS_INTENT_CMD_CCI_I2C_WR_BUF(frame_size_regs->frm_length_lines,
            frame_size_regs->frm_length_lines_size, &buf[0]),
        CMD_ENTRY_END(),
    };

    err = cmd_strip_executor(
        (hat_cm_socket_handle_t)sock_hndl,
        (hat_cm_socket_command_entry_t *)&lpfr_entry,
        &res);

    return err;
}

