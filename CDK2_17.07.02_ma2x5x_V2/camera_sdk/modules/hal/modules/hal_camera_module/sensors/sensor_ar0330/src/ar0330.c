/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file ar0330.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <hal/hal_camera_module/hat_cm_socket.h>
#include <hal/hal_camera_module/hat_cm_sensor.h>
#include "../../sensor_phys.h"

#define BUFSET16 BUF_SET_BE16

typedef struct {
   float real_gain;
   unsigned int reg_value;
}ar0330_again_entry_t;

static const ar0330_again_entry_t ar0330_gain_lookup[] =
{
     {1.0 , 0x00},  // invalid
     {1.0 , 0x00},  // 1
     {2.0 , 0x10},  // 2
     {2.91, 0x1A},  // 3
     {4.0 , 0x20},  // 4
     {4.92, 0x26},  // 5
     {5.80, 0x2A},  // 6
     {6.72, 0x2D},  // 7
     {8.0 , 0x30},  // 8
};

static int ar0330_power_off(void* sock_hndl, void* feat, void* exec_func)
{
    int err = 0;
    hat_cm_socket_command_entry_t* ar0330_power_off_seq;
    hat_cm_socket_cmd_strip_result_t res;
    hat_cm_socket_cmd_strip_executor_t cmd_strip_executor =
        (hat_cm_socket_cmd_strip_executor_t)exec_func;

    ar0330_power_off_seq = (hat_cm_socket_command_entry_t*)
        ((hat_sensor_features_t *)feat)->operations.pwr_off.oper;

    err = cmd_strip_executor(
        (hat_cm_socket_handle_t)sock_hndl,
        ar0330_power_off_seq,
        &res);

    return err;
}

static int ar0330_power_on(void* sock_hndl, void* feat, void* exec_func)
{
    int err = 0;
    hat_cm_socket_command_entry_t* ar0330_power_on_seq;
    hat_cm_socket_cmd_strip_result_t res;
    hat_cm_socket_cmd_strip_executor_t cmd_strip_executor =
        (hat_cm_socket_cmd_strip_executor_t)exec_func;

    ar0330_power_on_seq = (hat_cm_socket_command_entry_t*)
        ((hat_sensor_features_t *)feat)->operations.pwr_on.oper;

    err = cmd_strip_executor(
        (hat_cm_socket_handle_t)sock_hndl,
        ar0330_power_on_seq,
        &res);

    return err;
}

static int ar0330_init(void* sock_hndl, void* feat, void* exec_func,
    uint32 init_idx)
{
    int err = 0;
    hat_cm_socket_command_entry_t* ar0330_init_seq;
    hat_cm_socket_cmd_strip_result_t res;
    hat_cm_socket_cmd_strip_executor_t cmd_strip_executor =
        (hat_cm_socket_cmd_strip_executor_t)exec_func;

    ar0330_init_seq = (hat_cm_socket_command_entry_t*)
        ((hat_sensor_features_t *)feat)->operations.init[init_idx].oper;

    err = cmd_strip_executor(
        (hat_cm_socket_handle_t)sock_hndl,
        ar0330_init_seq,
        &res);

    return err;
}

static int ar0330_stream_off(void* sock_hndl, void* feat, void* exec_func)
{
    int err = 0;
    hat_cm_socket_command_entry_t* ar0330_stream_off_seq;
    hat_cm_socket_cmd_strip_result_t res;
    hat_cm_socket_cmd_strip_executor_t cmd_strip_executor =
        (hat_cm_socket_cmd_strip_executor_t)exec_func;

    ar0330_stream_off_seq = (hat_cm_socket_command_entry_t*)
        ((hat_sensor_features_t *)feat)->operations.stream_off.oper;

    err = cmd_strip_executor(
        (hat_cm_socket_handle_t)sock_hndl,
        ar0330_stream_off_seq,
        &res);

    return err;
}

static int ar0330_stream_on(void* sock_hndl, void* feat, void* exec_func)
{
    int err = 0;
    hat_cm_socket_command_entry_t* ar0330_stream_on_seq;
    hat_cm_socket_cmd_strip_result_t res;
    hat_cm_socket_cmd_strip_executor_t cmd_strip_executor =
        (hat_cm_socket_cmd_strip_executor_t)exec_func;

    ar0330_stream_on_seq = (hat_cm_socket_command_entry_t*)
        ((hat_sensor_features_t *)feat)->operations.stream_on.oper;

    err = cmd_strip_executor(
        (hat_cm_socket_handle_t)sock_hndl,
        ar0330_stream_on_seq,
        &res);

    return err;
}



static int ar0330_set_exp(void* sock_hndl, void* exec_func, uint32 exp_val)
{
    int err = 0;
    hat_cm_socket_cmd_strip_result_t res;
    hat_cm_socket_cmd_strip_executor_t cmd_strip_executor =
        (hat_cm_socket_cmd_strip_executor_t)exec_func;
    uint8 buf[2];

    BUFSET16(buf, exp_val);
    hat_cm_socket_command_entry_t exp_entry[] = {
        SENS_INTENT_CMD_CCI_I2C_WR_BUF(0x3012, 2, buf),
        CMD_ENTRY_END(),
    };

    err = cmd_strip_executor(
        (hat_cm_socket_handle_t)sock_hndl,
        (hat_cm_socket_command_entry_t *)&exp_entry,
        &res);

    return err;
}



static int ar0330_set_gain(void* sock_hndl, void* exec_func, float gain_val)
{
    int err = 0;
    uint8 buf[8];
    uint16 gain;
    uint16 i;

    // Convert gain
    if (gain_val > 128.0)
        gain_val = 128.0;

    if (gain_val < 1.0)
        gain_val = 1.0;

    i = (uint16)gain_val;
    if (i>8)
        i = 8;

    gain = ar0330_gain_lookup[i].reg_value;

    hat_cm_socket_cmd_strip_result_t res;
    hat_cm_socket_cmd_strip_executor_t cmd_strip_executor =
        (hat_cm_socket_cmd_strip_executor_t)exec_func;

    BUFSET16(buf, gain);
    hat_cm_socket_command_entry_t gain_entry[] = {
        SENS_INTENT_CMD_CCI_I2C_WR_BUF(0x3060, 2, buf),
        CMD_ENTRY_END(),
    };

    err = cmd_strip_executor(
        (hat_cm_socket_handle_t)sock_hndl,
        (hat_cm_socket_command_entry_t *)&gain_entry,
        &res);

    gain_val /= ar0330_gain_lookup[i].real_gain;

    gain = 128*gain_val;

    BUFSET16(&buf[0], gain);
    BUFSET16(&buf[2], gain);
    BUFSET16(&buf[4], gain);
    BUFSET16(&buf[6], gain);
    hat_cm_socket_command_entry_t d_gain_entry[] = {
        SENS_INTENT_CMD_CCI_I2C_WR_BUF(0x3056, 8, buf),
        CMD_ENTRY_END(),
    };

    err = cmd_strip_executor(
        (hat_cm_socket_handle_t)sock_hndl,
        (hat_cm_socket_command_entry_t *)&d_gain_entry,
        &res);

    return err;
}



static int ar0330_set_grped_prm_hold(void* sock_hndl, void* feat,
    void* exec_func, uint32 hold)
{
    int err = 0;
/*
    hat_cm_socket_cmd_strip_result_t res;
    hat_cm_socket_cmd_strip_executor_t cmd_strip_executor =
        (hat_cm_socket_cmd_strip_executor_t)exec_func;
    uint8 buf[1];

    BUFSET8(buf, hold);
    hat_cm_socket_command_entry_t hold_entry[] = {
        SENS_INTENT_CMD_CCI_I2C_WR_BUF(0x104, 1, buf),
        CMD_ENTRY_END(),
    };

    err = cmd_strip_executor(
        (hat_cm_socket_handle_t)sock_hndl,
        (hat_cm_socket_command_entry_t *)&hold_entry,
        &res);
*/
    return err;
}



static int ar0330_set_lpfr(void* sock_hndl, void* feat,
    void* exec_func, uint32 lpfr)
{
    int err = 0;
/*
    hat_cm_socket_cmd_strip_result_t res;
    hat_cm_socket_cmd_strip_executor_t cmd_strip_executor =
        (hat_cm_socket_cmd_strip_executor_t)exec_func;
    uint8 buf[2];

    BUFSET16(buf, lpfr);
    hat_cm_socket_command_entry_t lpfr_entry[] = {
        SENS_INTENT_CMD_CCI_I2C_WR_BUF(0x300A, 2, buf),
        CMD_ENTRY_END(),
    };

    err = cmd_strip_executor(
        (hat_cm_socket_handle_t)sock_hndl,
        (hat_cm_socket_command_entry_t *)&lpfr_entry,
        &res);
*/
    return err;
}



static int ar0330_set_mode(void* sock_hndl, void* feat, void* exec_func, void *sen_cfg)
{
    int err = 0;
    int mode_idx = ((hat_sen_config_t *)sen_cfg)->sensor_mode_idx;
    hat_cm_socket_command_entry_t* mode_strip;
    hat_cm_socket_cmd_strip_result_t res;

    /* Currently mode is selected depending on the mode index only */
    mode_strip = (hat_cm_socket_command_entry_t*)
        ((hat_sensor_features_t *)feat)->modes.list[mode_idx].mode_settings;

    hat_cm_socket_cmd_strip_executor_t cmd_strip_executor =
        (hat_cm_socket_cmd_strip_executor_t)exec_func;

    err = cmd_strip_executor(
        (hat_cm_socket_handle_t)sock_hndl,
        mode_strip,
        &res);

    return err;
}

static int ar0330_get_temp(void* sock_hndl, void* feat, void* exec_func, float* temp_c)
{
    int err = 0;

    //TODO Implement this function when we have more info about
    // temperature sensor of ar0330.
    *temp_c = 9999.0f;

    return err;
}



const hat_cm_ph_sen_obj_t ar0330_sens_obj = {
    .sen_drv_id         = DTP_SEN_AR0330,
    .power_off          = ar0330_power_off,
    .power_on           = ar0330_power_on,
    .init               = ar0330_init,
    .stream_off         = ar0330_stream_off,
    .stream_on          = ar0330_stream_on,
    .set_exp            = ar0330_set_exp,
    .set_gain           = ar0330_set_gain,
    .set_grped_prm_hold = ar0330_set_grped_prm_hold,
    .set_lpfr           = ar0330_set_lpfr,
    .set_mode           = ar0330_set_mode,
    .get_temperature    = ar0330_get_temp,
};
