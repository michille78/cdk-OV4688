/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file sensors.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include "sensors.h"
#include "sensor_dummy/inc/sen_dummy.h"

#ifdef SENSOR_MODULE_AR1330
#include "sensor_ar1330/inc/ar1330.h"
#endif
#ifdef SENSOR_MODULE_IMX214
#include "sensor_imx214/inc/imx214.h"
#endif
#ifdef SENSOR_MODULE_IMX208
#include "sensor_imx208/inc/imx208.h"
#endif
#ifdef SENSOR_MODULE_OV13860
#include "sensor_ov13860/inc/ov13860.h"
#endif
#ifdef SENSOR_MODULE_AR0330
#include "sensor_ar0330/inc/ar0330.h"
#endif
#ifdef SENSOR_MODULE_OV7251
#include "sensor_ov7251/inc/ov7251.h"
#endif

#include "sensor_ov5658/inc/ov5658.h"
#include "sensor_ov4188/inc/ov4188.h"

#include <utils/mms_debug.h>

mmsdbg_declare_variable(hal_cm_driver);
#define MMSDEBUGLEVEL mmsdbg_use_variable(hal_cm_driver)

#ifndef ARR_SIZE
#define ARR_SIZE(a) (sizeof(a)/sizeof(a[0]))
#endif

const hat_cm_ph_sen_handle_t sensor_handle_list[] = {
    (hat_cm_ph_sen_handle_t)&sen_dummy_sens_obj,    // 0
#ifdef SENSOR_MODULE_AR1330
    (hat_cm_ph_sen_handle_t)&ar1330_sens_obj,       // 1
#endif
#ifdef SENSOR_MODULE_IMX214
    (hat_cm_ph_sen_handle_t)&imx214_sens_obj,       // 2
#endif
#ifdef SENSOR_MODULE_IMX208
    (hat_cm_ph_sen_handle_t)&imx208_sens_obj,       // 3
#endif
#ifdef SENSOR_MODULE_OV13860
    (hat_cm_ph_sen_handle_t)&ov13860_sens_obj,      // 4
#endif
//#ifdef SENSOR_MODULE_AR0330
//    (hat_cm_ph_sen_handle_t)&ar0330_sens_obj,       // 5
//#endif
//#ifdef SENSOR_MODULE_OV7251
//    (hat_cm_ph_sen_handle_t)&ov7251_sens_obj_1,       // 6
//    (hat_cm_ph_sen_handle_t)&ov7251_sens_obj_2,       // 7
//#endif

    (hat_cm_ph_sen_handle_t)&ov5658_sens_obj,      // 5
 (hat_cm_ph_sen_handle_t)&ov4188_sens_obj,      // 5
};

hat_cm_ph_sen_handle_t hal_get_sensor_driver(dtp_sensors_id_t sen_id)
{
int i;
    for (i = 0; i<ARR_SIZE(sensor_handle_list); i++)
    {
        if (sensor_handle_list[i]->sen_drv_id == sen_id)
        {
            mmsdbg(DL_MESSAGE, "Mounting sen drv %d with index %d", sen_id, i);
            return sensor_handle_list[i];
        }
    }
    mmsdbg(DL_ERROR, "Can't find sensor driver for %d. Using index 0", sen_id);
    return sensor_handle_list[0];
}

