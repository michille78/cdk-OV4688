/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file ov7251.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 29-Feb-2016 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <hal/hal_camera_module/hat_cm_socket.h>
#include <hal/hal_camera_module/hat_cm_sensor.h>
#include "../../sensor_phys.h"

#include <utils/mms_debug.h>

mmsdbg_define_variable(
        ov7251_sen,
        DL_DEFAULT,
        0,
        "ov7251",
        "ov7251"
    );

#define MMSDEBUGLEVEL mmsdbg_use_variable(ov7251_sen)

static uint8_t ov7251_real_to_register_gain(float gain) {

    uint16_t retVal;

    gain = (gain < 1.0) ? 1.0 : (gain > 16.0) ? 16.0 : gain;
    retVal = (uint16_t)(gain * 16.0 + .5);

    return retVal > 0xFF ? 0xFF : (uint8_t)retVal;

}

static int ov7251_power_off(void* sock_hndl, void* feat, void* exec_func){

    int retVal = 0;
    hat_cm_socket_command_entry_t*      ov7251_power_off_seq;
    hat_cm_socket_cmd_strip_result_t    res;
    hat_cm_socket_cmd_strip_executor_t  cmd_strip_executor =
                                        (hat_cm_socket_cmd_strip_executor_t)exec_func;

    ov7251_power_off_seq = (hat_cm_socket_command_entry_t*)
                           ((hat_sensor_features_t *)feat)->operations.pwr_off.oper;

    retVal = cmd_strip_executor((hat_cm_socket_handle_t)sock_hndl,
                                ov7251_power_off_seq,
                                &res);
    return retVal;
}

static int ov7251_power_on(void* sock_hndl, void* feat, void* exec_func){

    int retVal = 0;
    hat_cm_socket_command_entry_t*      ov7251_power_on_seq;
    hat_cm_socket_cmd_strip_result_t    res;
    hat_cm_socket_cmd_strip_executor_t  cmd_strip_executor =
                                        (hat_cm_socket_cmd_strip_executor_t)exec_func;

    ov7251_power_on_seq = (hat_cm_socket_command_entry_t*)
                          ((hat_sensor_features_t *)feat)->operations.pwr_on.oper;

    retVal = cmd_strip_executor((hat_cm_socket_handle_t)sock_hndl,
                                ov7251_power_on_seq,
                                &res);
    return retVal;
}

static int ov7251_init(void* sock_hndl,
                       void* feat,
                       void* exec_func,
                       uint32 init_idx){

    int retVal = 0;
    hat_cm_socket_command_entry_t*      ov7251_init_seq;
    hat_cm_socket_cmd_strip_result_t    res;
    hat_cm_socket_cmd_strip_executor_t  cmd_strip_executor =
                                        (hat_cm_socket_cmd_strip_executor_t)exec_func;

    ov7251_init_seq = (hat_cm_socket_command_entry_t*)
                      ((hat_sensor_features_t *)feat)->operations.init[init_idx].oper;

    retVal = cmd_strip_executor((hat_cm_socket_handle_t)sock_hndl,
                                ov7251_init_seq,
                                &res);
    return retVal;
}

static int ov7251_stream_off(void* sock_hndl, void* feat, void* exec_func){

    int retVal = 0;
    hat_cm_socket_command_entry_t*      ov7251_stream_off_seq;
    hat_cm_socket_cmd_strip_result_t    res;
    hat_cm_socket_cmd_strip_executor_t  cmd_strip_executor =
                                        (hat_cm_socket_cmd_strip_executor_t)exec_func;

    ov7251_stream_off_seq = (hat_cm_socket_command_entry_t*)
                            ((hat_sensor_features_t *)feat)->operations.stream_off.oper;

    retVal = cmd_strip_executor((hat_cm_socket_handle_t)sock_hndl,
                                ov7251_stream_off_seq,
                                &res);
    return retVal;
}

static int ov7251_stream_on(void* sock_hndl, void* feat, void* exec_func){

    int retVal = 0;
    hat_cm_socket_command_entry_t*      ov7251_stream_on_seq;
    hat_cm_socket_cmd_strip_result_t    res;
    hat_cm_socket_cmd_strip_executor_t  cmd_strip_executor =
                                        (hat_cm_socket_cmd_strip_executor_t)exec_func;

    ov7251_stream_on_seq = (hat_cm_socket_command_entry_t*)
                           ((hat_sensor_features_t *)feat)->operations.stream_on.oper;

    retVal = cmd_strip_executor((hat_cm_socket_handle_t)sock_hndl,
                                ov7251_stream_on_seq,
                                &res);
    return retVal;
}

static int ov7251_set_exp(void* sock_hndl, void* exec_func, uint32 exp_val){

    int retVal = 0;
    hat_cm_socket_cmd_strip_result_t        res;
    hat_cm_socket_cmd_strip_executor_t      cmd_strip_executor =
                                            (hat_cm_socket_cmd_strip_executor_t)exec_func;
    uint8 buf[3];

    exp_val &= 0x0000FFFF;
    // Four lower bits are fractional
    BUFSET8(&buf[0], exp_val >> 12 & 0x0000000F);
    BUF_SET_BE16(&buf[1], exp_val << 4 & 0x0000FFF0);
    hat_cm_socket_command_entry_t exp_entry[] = {
        SENS_INTENT_CMD_CCI_I2C_WR_BUF(0x3500, 3, &buf[0]),
        CMD_ENTRY_END(),
    };

    retVal = cmd_strip_executor((hat_cm_socket_handle_t)sock_hndl,
                                (hat_cm_socket_command_entry_t *)&exp_entry,
                                &res);
    return retVal;
}

static int ov7251_set_gain(void* sock_hndl, void* exec_func, float gain_val){

    int retVal = 0;
    hat_cm_socket_cmd_strip_result_t    res;
    hat_cm_socket_cmd_strip_executor_t  cmd_strip_executor =
                                        (hat_cm_socket_cmd_strip_executor_t)exec_func;

    uint8_t gain = ov7251_real_to_register_gain(gain_val);

    hat_cm_socket_command_entry_t gain_entry[] = {
        SENS_INTENT_CMD_CLOCK_I2C_WR(0x350B, gain),
        CMD_ENTRY_END(),
    };

    retVal = cmd_strip_executor((hat_cm_socket_handle_t)sock_hndl,
                                (hat_cm_socket_command_entry_t *)&gain_entry,
                                &res);
    return retVal;
}


static int ov7251_set_grped_prm_hold(void* sock_hndl,
                                     void* feat,
                                     void* exec_func,
                                     uint32 hold){
    int retVal= 0;
    hat_cm_socket_cmd_strip_result_t    res;
    hat_cm_socket_cmd_strip_executor_t  cmd_strip_executor =
                                        (hat_cm_socket_cmd_strip_executor_t)exec_func;

    return retVal;
    hat_cm_socket_command_entry_t hold_entry[] = {
        SENS_INTENT_CMD_CLOCK_I2C_WR(0x3208, hold & 0x000000FF),
        CMD_ENTRY_END(),
    };

    retVal = cmd_strip_executor((hat_cm_socket_handle_t)sock_hndl,
                                (hat_cm_socket_command_entry_t *)&hold_entry,
                                &res);
    return retVal;
}



static int ov7251_set_lpfr(void* sock_hndl,
                           void* feat,
                           void* exec_func,
                           uint32 lpfr){
    int retVal = 0;
    hat_cm_socket_cmd_strip_result_t    res;
    hat_cm_socket_cmd_strip_executor_t  cmd_strip_executor =
                                        (hat_cm_socket_cmd_strip_executor_t)exec_func;
    uint8_t buf[2];

    BUF_SET_BE16(buf, lpfr & 0x0000FFFF);
    hat_cm_socket_command_entry_t lpfr_entry[] = {
        SENS_INTENT_CMD_CCI_I2C_WR_BUF(0x380E, 2, &buf[0]),
        CMD_ENTRY_END(),
    };

    retVal = cmd_strip_executor((hat_cm_socket_handle_t)sock_hndl,
                                (hat_cm_socket_command_entry_t *)&lpfr_entry,
                                &res);
    return retVal;
}

static int ov7251_set_mode(void* sock_hndl,
                           void* feat,
                           void* exec_func,
                           void *sen_cfg){
    int retVal = 0;
    int                                 mode_idx =
                                        ((hat_sen_config_t *)sen_cfg)->sensor_mode_idx;
    hat_cm_socket_command_entry_t       *mode_strip;
    hat_cm_socket_cmd_strip_result_t    res;

    /* Currently mode is selected depending on the mode index only */
    mode_strip = (hat_cm_socket_command_entry_t*)
                 ((hat_sensor_features_t *)feat)->modes.list[mode_idx].mode_settings;

    hat_cm_socket_cmd_strip_executor_t cmd_strip_executor =
                                       (hat_cm_socket_cmd_strip_executor_t)exec_func;

    retVal = cmd_strip_executor((hat_cm_socket_handle_t)sock_hndl,
                                mode_strip,
                                &res);
    return retVal;
}

inline static int ov7251_get_temp(void* sock_hndl,
                                  void* feat,
                                  void* exec_func,
                                  float* temp_c){ return 0; }


const hat_cm_ph_sen_obj_t ov7251_sens_obj_1 =
{
    .sen_drv_id         = DTP_SEN_OV7251_1,
    .power_off          = ov7251_power_off,
    .power_on           = ov7251_power_on,
    .init               = ov7251_init,
    .stream_off         = ov7251_stream_off,
    .stream_on          = ov7251_stream_on,
    .set_exp            = ov7251_set_exp,
    .set_gain           = ov7251_set_gain,
    .set_grped_prm_hold = ov7251_set_grped_prm_hold,
    .set_lpfr           = ov7251_set_lpfr,
    .set_mode           = ov7251_set_mode,
    .get_temperature    = ov7251_get_temp,
};

const hat_cm_ph_sen_obj_t ov7251_sens_obj_2 =
{
    .sen_drv_id         = DTP_SEN_OV7251_2,
    .power_off          = ov7251_power_off,
    .power_on           = ov7251_power_on,
    .init               = ov7251_init,
    .stream_off         = ov7251_stream_off,
    .stream_on          = ov7251_stream_on,
    .set_exp            = ov7251_set_exp,
    .set_gain           = ov7251_set_gain,
    .set_grped_prm_hold = ov7251_set_grped_prm_hold,
    .set_lpfr           = ov7251_set_lpfr,
    .set_mode           = ov7251_set_mode,
    .get_temperature    = ov7251_get_temp,
};
