/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file lights.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include "lights.h"
#include "lights_dummy/lights_dummy.h"

#ifdef LIGHTS_MODULE_AS3645
#include "lights_AS3645/lights_AS3645.h"
#endif

#include <utils/mms_debug.h>

mmsdbg_declare_variable(hal_cm_driver);
#define MMSDEBUGLEVEL mmsdbg_use_variable(hal_cm_driver)

const hat_cm_ph_lights_handle_t lights_handle_list[] = {
        (hat_cm_ph_lights_handle_t)&lights_dummy_lights_obj,
#ifdef LIGHTS_MODULE_AS3645
        (hat_cm_ph_lights_handle_t)&lights_AS3645_lights_obj,
#endif
};


#ifndef ARR_SIZE
#define ARR_SIZE(a) (sizeof(a)/sizeof(a[0]))
#endif

hat_cm_ph_lights_handle_t hal_get_lights_driver(dtp_lights_id_t id)
{
int i;
    for (i = 0; i<ARR_SIZE(lights_handle_list); i++)
    {
        if (lights_handle_list[i]->lights_drv_id == id)
        {
            mmsdbg(DL_MESSAGE, "Mounting lights drv %d with index %d", id, i);
            return lights_handle_list[i];
        }
    }
    mmsdbg(DL_ERROR, "Can't find lights driver for %. Using index 0", id);
    return lights_handle_list[0];
}
