/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file lights_AS3645.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! Jun 10, 2014 : Author aiovtchev
*! Created
* =========================================================================== */

#include "osal/osal_stdtypes.h"
#include <hal/hal_camera_module/hat_cm_socket.h>
#include <error_handle/include/error_handle.h>
#include <hal/modules/hal_camera_module/lights/lights_AS3645/lights_AS3645.h>

#include <utils/mms_debug.h>

mmsdbg_declare_variable(lights_drv);
#define MMSDEBUGLEVEL mmsdbg_use_variable(lights_drv)


typedef enum
{
    LIGHTS_DRV_STATE_CLOSE,
    LIGHTS_DRV_STATE_OPEN
}LIGHTS_DRV_STATE;

typedef struct
{
    LIGHTS_DRV_STATE state;
    osal_timeval on_timestamp;              // [us]
    osal_timeval off_timestamp;             // [us]
    osal_timeval next_possible_timestamp;   // [us] time stamp for next possible fire
}light_state_t;

static hat_cm_socket_command_entry_t AS3645_light_power_seq_on[] =
{
    CMD_ENTRY_DUMMY(),
    CMD_ENTRY_END(),
};

static hat_cm_socket_command_entry_t AS3645_light_power_seq_off[] =
{
    CMD_ENTRY_DUMMY(),
    CMD_ENTRY_END(),
};

static hat_cm_socket_command_entry_t AS3645_light_init[] =
{
    CMD_ENTRY_DUMMY(),
    CMD_ENTRY_END(),
};

static hat_cm_socket_command_entry_t AS3645_light_deinit[] =
{
    CMD_ENTRY_DUMMY(),
    CMD_ENTRY_END(),
};

typedef struct
{
    light_state_t st;
    hat_cm_socket_command_entry_t *pwr_seq_on;
    hat_cm_socket_command_entry_t *pwr_seq_off;
    hat_cm_socket_command_entry_t *init_seq;
    hat_cm_socket_command_entry_t *deinit_seq;
}light_private_t;

static light_private_t AS3645_light_prv =
{
     {
      LIGHTS_DRV_STATE_CLOSE, // state
          0, // on_timestamp;          // [us]
          0, // off_timestamp;         // [us]
          0 // next_possible_timestamp; // [us] time stamp for next possible fire
     },
     AS3645_light_power_seq_on,
     AS3645_light_power_seq_off,
     AS3645_light_init,
     AS3645_light_deinit
};


static  hat_light_entry_t flash_light_entries[] =
{
    {
        80,     // intensity [cd] light intensity
        80,     // power [0-100] Light power (driver specific value/index)
        5000000,// timeout [uSeconds] 0 infinity
        600,    // charge_time; // [us] for LED this is time between two flashes with that power
        .command_script = &AS3645_light_power_seq_on
    },
    {
        100,    // intensity [cd] light intensity
        100,    // power [0-100] Light power (driver specific value/index)
        5000000,// timeout [uSeconds] 0 infinity
        600,    // charge_time; // [us] for LED this is time between two flashes with that power
        .command_script = &AS3645_light_power_seq_on
    },
};

static  hat_light_entry_t torch_light_entries[] =
{
    {
        50,     // intensity [cd] light intensity
        50,     // power [0-100] Light power (driver specific value/index)
        0,      // timeout [uSeconds] 0 infinity
        0,      // charge_time; // [us] for LED this is time between two flashes with that power
        .command_script = &AS3645_light_power_seq_on
    }
};

static  hat_light_entry_t privacy_light_entries[] =
{
    {
        30,     // intensity [cd] light intensity
        30,     // power [0-100] Light power (driver specific value/index)
        0,      // timeout [uSeconds] 0 infinity
        0,      // charge_time; // [us] for LED this is time between two flashes with that power
        .command_script = &AS3645_light_power_seq_on
    }
};

static hat_flash_bounds_t lights_AS3645_bounds =
{
    {//  flash;
       ARRAY_SIZE(flash_light_entries), // uint32 num_light_intensities;
       flash_light_entries, // hat_light_entry_t *entries; // List of supported powers
       { //hat_light_features_t    features;
           0, //uint64 charge_duration; // Time taken before flash can fire again.[ns]
           2500, //uint32 colour_temp;     //
           1, //uint8 available; // Whether this camera has a flash.
           150, //uint8 max_energy; //Max energy output of the flash for a full power single flash [lumen-seconds]
           HAT_FLASH_TYPE_LED, //hat_flash_type_t type;
       },
       &AS3645_light_prv
    },

    {// torch;
        ARRAY_SIZE(torch_light_entries), // uint32 num_light_intensities;
        torch_light_entries, // hat_light_entry_t *entries; // List of supported powers
        { //hat_light_features_t    features;
            0, //uint64 charge_duration; // Time taken before flash can fire again.[ns]
            2500, //uint32 colour_temp;     //
            1, //uint8 available; // Whether this camera has a flash.
            60, //uint8 max_energy; //Max energy output of the flash for a full power single flash [lumen-seconds]
            HAT_FLASH_TYPE_LED, //hat_flash_type_t type;
        },
        &AS3645_light_prv
    },

    {// video;
        ARRAY_SIZE(torch_light_entries), // uint32 num_light_intensities;
        torch_light_entries, // hat_light_entry_t *entries; // List of supported powers
        { //hat_light_features_t    features;
            0, //uint64 charge_duration; // Time taken before flash can fire again.[ns]
            2500, //uint32 colour_temp;     //
            1, //uint8 available; // Whether this camera has a flash.
            60, //uint8 max_energy; //Max energy output of the flash for a full power single flash [lumen-seconds]
            HAT_FLASH_TYPE_LED, //hat_flash_type_t type;
        },
        &AS3645_light_prv
    },

    {// af_assist;
        ARRAY_SIZE(torch_light_entries), // uint32 num_light_intensities;
        torch_light_entries, // hat_light_entry_t *entries; // List of supported powers
        { //hat_light_features_t    features;
            0, //uint64 charge_duration; // Time taken before flash can fire again.[ns]
            2500, //uint32 colour_temp;     //
            1, //uint8 available; // Whether this camera has a flash.
            60, //uint8 max_energy; //Max energy output of the flash for a full power single flash [lumen-seconds]
            HAT_FLASH_TYPE_LED, //hat_flash_type_t type;
        },
        &AS3645_light_prv
    },

    {// privacy;
        ARRAY_SIZE(privacy_light_entries), // uint32 num_light_intensities;
        privacy_light_entries, // hat_light_entry_t *entries; // List of supported powers
        { //hat_light_features_t    features;
            0, //uint64 charge_duration; // Time taken before flash can fire again.[ns]
            6000, //uint32 colour_temp;     //
            0, //uint8 available; // Whether this camera has a flash.
            0, //uint8 max_energy; //Max energy output of the flash for a full power single flash [lumen-seconds]
            HAT_FLASH_TYPE_LED, //hat_flash_type_t type;
        },
        &AS3645_light_prv
    },

    {// red_eye;
        ARRAY_SIZE(flash_light_entries), // uint32 num_light_intensities;
        torch_light_entries, // hat_light_entry_t *entries; // List of supported powers
        { //hat_light_features_t    features;
            0, //uint64 charge_duration; // Time taken before flash can fire again.[ns]
            2500, //uint32 colour_temp;     //
            0, //uint8 available; // Whether this camera has a flash.
            0, //uint8 max_energy; //Max energy output of the flash for a full power single flash [lumen-seconds]
            HAT_FLASH_TYPE_LED, //hat_flash_type_t type;
        },
        &AS3645_light_prv
    },
};

static inline hat_light_capabilities_t* get_bounds_by_src(hat_light_source_type_t src)
{
    hat_light_capabilities_t * ret;

    switch (src) {
        case LIGHT_SRC_TORCH:
            ret = &lights_AS3645_bounds.torch;
            break;
        case LIGHT_SRC_VIDEO_LIGHT:
            ret = &lights_AS3645_bounds.video;
            break;
        case LIGHT_SRC_AF_ASSIST:
            ret = &lights_AS3645_bounds.af_assist;
            break;
        case LIGHT_SRC_PRIVACY:
            ret = &lights_AS3645_bounds.privacy;
            break;
        case LIGHT_SRC_MAIN_FLASH:
            ret = &lights_AS3645_bounds.flash;
            break;
        case LIGHT_SRC_MAIN_RED_EYE:
            ret = &lights_AS3645_bounds.red_eye;
            break;
        default:
            ret = &lights_AS3645_bounds.flash;
            break;
    }

    return ret;
}

static inline const hat_light_entry_t* get_light_by_power_and_time(hat_light_capabilities_t* cap, uint32 pwr, uint32 time)
{
    int i;
    const hat_light_entry_t *ent, *best;

    best = NULL;
    ent = &cap->entries[0];
    for (i = 0; i < cap->num_light_intensities; ++i) {
        if (ent->power >= pwr) {
            if ((!ent->timeout)||   // the light has no time limitation with this power
                (ent->timeout >= time)) { // or it can work longer than requested
                best = ent;
                break; // we found perfect match, so break searching loop
            }
        } else {
            best = ent; // update, but continue to search for better candidate
        }
        ent++;  // move to next entry
    }
    return best;
}

static inline void fill_lights_capabilities(hat_light_capabilities_t *cap,
                                            hat_light_el_features_t *dtp_cap)
{
    light_private_t* prv = (light_private_t*)cap->drv_private;
    cap->num_light_intensities = dtp_cap->num_light_intensities;
    cap->entries = dtp_cap->entries;
    cap->spec_features = dtp_cap->spec_features;
    prv->deinit_seq = (hat_cm_socket_command_entry_t*)dtp_cap->operations.deinit_seq.oper;
    prv->init_seq = (hat_cm_socket_command_entry_t*)dtp_cap->operations.init_seq.oper;
    prv->pwr_seq_on = (hat_cm_socket_command_entry_t*)dtp_cap->operations.pwr_seq_on.oper;
    prv->pwr_seq_off = (hat_cm_socket_command_entry_t*)dtp_cap->operations.pwr_seq_off.oper;

    return;
}
int lights_AS3645_set_power_state (void* sock_hndl, void* exec_func, hat_light_ctrl_entry_t* ctrl);
/*
    LIGHT_SRC_TORCH,
    LIGHT_SRC_VIDEO_LIGHT,
    LIGHT_SRC_AF_ASSIST,
    LIGHT_SRC_PRIVACY,
    LIGHT_SRC_MAIN_FLASH,
    LIGHT_SRC_MAIN_RED_EYE
 */
void drv_test(void* sock_hndl, void* exec_func)
{
    hat_light_ctrl_entry_t ctrl;
//// Torch 50
    ctrl.duration = 5000;
    ctrl.power = 50;
    ctrl.src = LIGHT_SRC_TORCH;
    lights_AS3645_set_power_state (sock_hndl, exec_func, &ctrl);
    osal_sleep(1);
    ctrl.power = 0;
    lights_AS3645_set_power_state (sock_hndl, exec_func, &ctrl);
    osal_sleep(1);
//// LIGHT_SRC_PRIVACY 30
    ctrl.duration = 5000;
    ctrl.power = 30;
    ctrl.src = LIGHT_SRC_PRIVACY;
    lights_AS3645_set_power_state (sock_hndl, exec_func, &ctrl);
    osal_sleep(1);
    ctrl.power = 0;
    lights_AS3645_set_power_state (sock_hndl, exec_func, &ctrl);
    osal_sleep(1);

//// LIGHT_SRC_MAIN_FLASH 100
    ctrl.duration = 5000;
    ctrl.power = 100;
    ctrl.src = LIGHT_SRC_MAIN_FLASH;
    lights_AS3645_set_power_state (sock_hndl, exec_func, &ctrl);
    osal_sleep(1);
    ctrl.power = 0;
    lights_AS3645_set_power_state (sock_hndl, exec_func, &ctrl);
    osal_sleep(1);

//// LIGHT_SRC_VIDEO_LIGHT 50
    ctrl.duration = 5000;
    ctrl.power = 50;
    ctrl.src = LIGHT_SRC_VIDEO_LIGHT;
    lights_AS3645_set_power_state (sock_hndl, exec_func, &ctrl);
    osal_sleep(1);
    ctrl.power = 0;
    lights_AS3645_set_power_state (sock_hndl, exec_func, &ctrl);
    osal_sleep(1);

}

static inline void fill_lights_bounds(hat_lights_features_t* dtp_features)
{
    fill_lights_capabilities(&lights_AS3645_bounds.af_assist, &(dtp_features->af_assist));
    fill_lights_capabilities(&lights_AS3645_bounds.flash, &(dtp_features->flash));
    fill_lights_capabilities(&lights_AS3645_bounds.privacy, &(dtp_features->privacy));
    fill_lights_capabilities(&lights_AS3645_bounds.red_eye, &(dtp_features->red_eye));
    fill_lights_capabilities(&lights_AS3645_bounds.torch, &(dtp_features->torch));
    fill_lights_capabilities(&lights_AS3645_bounds.video, &(dtp_features->video));

    return;
}


int lights_AS3645_set_power_state (void* sock_hndl, void* exec_func, hat_light_ctrl_entry_t* ctrl)
{
    int err;
    hat_light_capabilities_t *cap;
    const hat_light_entry_t *ent;
    light_private_t          *prv;
    osal_timeval              timestamp;
    hat_cm_socket_cmd_strip_result_t res;
    hat_cm_socket_cmd_strip_executor_t cmd_executor =
        (hat_cm_socket_cmd_strip_executor_t)exec_func;

    if ((!sock_hndl) || (!exec_func) || (!ctrl))
    {
        mmsdbg(DL_ERROR, "Null pointer");
        return -1;
    }

    mmsdbg(DL_ERROR, "## SET_POWER_STATE: SRC %d PWR %d Start_Time %d Duration %d",
            ctrl->src,
            ctrl->power,
            ctrl->start_time,
            ctrl->duration);
/* TODO uncomment me
    if (0 == ctrl->power)
    {
        return 0;       // AS3645 driver has nothing to do with power zero
    }
*/
    cap = get_bounds_by_src(ctrl->src);
    GOTO_EXIT_IF(cap == NULL, 1);
    ent = get_light_by_power_and_time(cap, ctrl->power, ctrl->duration);
    GOTO_EXIT_IF(ent == NULL, 1);

    prv = (light_private_t*)cap->drv_private;

    if (prv->st.state != LIGHTS_DRV_STATE_OPEN)
    {
        mmsdbg(DL_ERROR, "Driver isn't open (%d)", prv->st.state);
        GOTO_EXIT_IF(1, 1);
    }

    osal_get_timeval(&timestamp);

    if (timestamp < prv->st.next_possible_timestamp)
    {
        mmsdbg(DL_ERROR, "Skipping flash fire request - too frequently usage");
        mmsdbg(DL_ERROR, " ts %jd next %jd delta %jd",
               timestamp,
               prv->st.next_possible_timestamp,
               prv->st.next_possible_timestamp - timestamp);
        GOTO_EXIT_IF(1, 1);
    }

    prv->st.on_timestamp = timestamp;
    prv->st.off_timestamp = prv->st.on_timestamp + ((osal_timeval)ctrl->duration);
    prv->st.next_possible_timestamp = prv->st.off_timestamp + ((osal_timeval)ent->charge_time);

    mmsdbg(DL_ERROR, "Executing flash fire request ts %jd next %jd delta %jd",
           timestamp,
           prv->st.next_possible_timestamp,
           prv->st.next_possible_timestamp - timestamp);

    err = cmd_executor(sock_hndl, ent->command_script, &res);
    GOTO_EXIT_IF(err, 1);

    return 0;

EXIT_1:
    mmsdbg(DL_ERROR, "Light internal error");
    return -1;

}

int lights_AS3645_read_reg (void* sock_hndl, uint8 addr, uint8* p_val, void* exec_func)
{
    int err = 0;
    hat_cm_socket_cmd_strip_result_t res;
    hat_cm_socket_cmd_strip_executor_t cmd_strip_executor =
        (hat_cm_socket_cmd_strip_executor_t)exec_func;

    hat_cm_socket_command_entry_t rd_entry[] = {
            LIGHT_INTENT_CMD_CCI_I2C_RD(addr, sizeof(*p_val), p_val),
            CMD_ENTRY_END(),
        };

    err = cmd_strip_executor(
        (hat_cm_socket_handle_t)sock_hndl,
        (hat_cm_socket_command_entry_t *)&rd_entry,
        &res);

    return err;
}

int lights_AS3645_write_reg (void* sock_hndl, uint8 addr, uint8* p_val, void* exec_func)
{
    int err = 0;
    hat_cm_socket_cmd_strip_result_t res;
    hat_cm_socket_cmd_strip_executor_t cmd_strip_executor =
        (hat_cm_socket_cmd_strip_executor_t)exec_func;

    hat_cm_socket_command_entry_t rd_entry[] = {
            LIGHT_INTENT_CMD_CCI_I2C_WR_BUF(addr, sizeof(*p_val), p_val),
            CMD_ENTRY_END(),
        };

    err = cmd_strip_executor(
        (hat_cm_socket_handle_t)sock_hndl,
        (hat_cm_socket_command_entry_t *)&rd_entry,
        &res);

    return err;
}

void _AS3645_read_reggs (void* sock_hndl, void* exec_func)
{
    int err = 0, i;
    uint8 val;
    for (i = 0; i < 16; ++i) {
        err = lights_AS3645_read_reg(sock_hndl, i, &val, exec_func);
        if (err) {
            mmsdbg(DL_ERROR, "Error reading AS3645 register %d", i);
            break;
        }
        mmsdbg(DL_ERROR, "AS3645 register %d 0x%02x (%d)", i, val, val);
    }
/* Flash Test
    val = 0x02;     // Flash TO = 200ms
    lights_AS3645_write_reg(sock_hndl, 2, &val, exec_func);

    val = 0xBC;     // Assistant 100 ma
    lights_AS3645_write_reg(sock_hndl, 3, &val, exec_func);

    val = 0xBB;     // Flash mode + ON
    lights_AS3645_write_reg(sock_hndl, 4, &val, exec_func);
    lights_AS3645_read_reg(sock_hndl, 4, &val, exec_func);
    osal_sleep(1);
    val = 0xB2;     // Assistant mode + OFF
    lights_AS3645_write_reg(sock_hndl, 4, &val, exec_func);
    lights_AS3645_read_reg(sock_hndl, 4, &val, exec_func);
*/
}

int lights_AS3645_init (void* sock_hndl, void* dtp_feat, void* exec_func)
{
    int               err;
    light_private_t  *prv;
    hat_cm_socket_cmd_strip_result_t res;
    hat_cm_socket_cmd_strip_executor_t cmd_executor =
        (hat_cm_socket_cmd_strip_executor_t)exec_func;

    if(dtp_feat != NULL)
    {
        fill_lights_bounds((hat_lights_features_t*)dtp_feat);
    } else
    {
        mmsdbg(DL_ERROR, "No DTP! Use default values");
    }


    {   // AF assist
        prv = lights_AS3645_bounds.af_assist.drv_private;

        if (prv->st.state == LIGHTS_DRV_STATE_CLOSE)
        {
            err = cmd_executor(sock_hndl, prv->init_seq, &res);
            GOTO_EXIT_IF(err, 1);
            prv->st.state = LIGHTS_DRV_STATE_OPEN;
        }
    }
    {   // Flash
        prv = lights_AS3645_bounds.flash.drv_private;

        if (prv->st.state == LIGHTS_DRV_STATE_CLOSE)
        {
            err = cmd_executor(sock_hndl, prv->init_seq, &res);
            GOTO_EXIT_IF(err, 1);
            prv->st.state = LIGHTS_DRV_STATE_OPEN;
        }
    }
    {   // Privacy indicator
        prv = lights_AS3645_bounds.privacy.drv_private;
        if (prv->st.state == LIGHTS_DRV_STATE_CLOSE)
        {
            err = cmd_executor(sock_hndl, prv->init_seq, &res);
            GOTO_EXIT_IF(err, 1);
            prv->st.state = LIGHTS_DRV_STATE_OPEN;
        }
    }
    {   // Red Eye
        prv = lights_AS3645_bounds.red_eye.drv_private;

        if (prv->st.state == LIGHTS_DRV_STATE_CLOSE)
        {
            err = cmd_executor(sock_hndl, prv->init_seq, &res);
            GOTO_EXIT_IF(err, 1);
            prv->st.state = LIGHTS_DRV_STATE_OPEN;
        }
    }
    {   // torch
        prv = lights_AS3645_bounds.torch.drv_private;

        if (prv->st.state == LIGHTS_DRV_STATE_CLOSE)
        {
            err = cmd_executor(sock_hndl, prv->init_seq, &res);
            GOTO_EXIT_IF(err, 1);
            prv->st.state = LIGHTS_DRV_STATE_OPEN;
        }
    }
    {   // video
        prv = lights_AS3645_bounds.video.drv_private;

        if (prv->st.state == LIGHTS_DRV_STATE_CLOSE)
        {
            err = cmd_executor(sock_hndl, prv->init_seq, &res);
            GOTO_EXIT_IF(err, 1);
            prv->st.state = LIGHTS_DRV_STATE_OPEN;
        }
    }
//    _AS3645_read_reggs (sock_hndl, exec_func);
//    drv_test(sock_hndl, exec_func);
    return 0;
EXIT_1:
    mmsdbg(DL_ERROR, "Flash internal error");
    return -1;
}
int lights_AS3645_destroy (void* sock_hndl, void* exec_func)
{
    int               err;
    light_private_t  *prv;
    hat_cm_socket_cmd_strip_result_t res;
    hat_cm_socket_cmd_strip_executor_t cmd_executor =
        (hat_cm_socket_cmd_strip_executor_t)exec_func;

    {   // AF assist
        prv = lights_AS3645_bounds.af_assist.drv_private;

        if (prv->st.state == LIGHTS_DRV_STATE_OPEN)
        {
            err = cmd_executor(sock_hndl, prv->deinit_seq, &res);
            GOTO_EXIT_IF(err, 1);
            prv->st.state = LIGHTS_DRV_STATE_CLOSE;
        }
    }
    {   // Flash
        prv = lights_AS3645_bounds.flash.drv_private;

        if (prv->st.state == LIGHTS_DRV_STATE_OPEN)
        {
            err = cmd_executor(sock_hndl, prv->deinit_seq, &res);
            GOTO_EXIT_IF(err, 1);
            prv->st.state = LIGHTS_DRV_STATE_CLOSE;
        }
    }
    {   // Privacy indicator
        prv = lights_AS3645_bounds.privacy.drv_private;
        if (prv->st.state == LIGHTS_DRV_STATE_OPEN)
        {
            err = cmd_executor(sock_hndl, prv->deinit_seq, &res);
            GOTO_EXIT_IF(err, 1);
            prv->st.state = LIGHTS_DRV_STATE_CLOSE;
        }
    }
    {   // Red Eye
        prv = lights_AS3645_bounds.red_eye.drv_private;

        if (prv->st.state == LIGHTS_DRV_STATE_OPEN)
        {
            err = cmd_executor(sock_hndl, prv->deinit_seq, &res);
            GOTO_EXIT_IF(err, 1);
            prv->st.state = LIGHTS_DRV_STATE_CLOSE;
        }
    }
    {   // torch
        prv = lights_AS3645_bounds.torch.drv_private;

        if (prv->st.state == LIGHTS_DRV_STATE_OPEN)
        {
            err = cmd_executor(sock_hndl, prv->deinit_seq, &res);
            GOTO_EXIT_IF(err, 1);
            prv->st.state = LIGHTS_DRV_STATE_CLOSE;
        }
    }
    {   // video
        prv = lights_AS3645_bounds.video.drv_private;

        if (prv->st.state == LIGHTS_DRV_STATE_OPEN)
        {
            err = cmd_executor(sock_hndl, prv->deinit_seq, &res);
            GOTO_EXIT_IF(err, 1);
            prv->st.state = LIGHTS_DRV_STATE_CLOSE;
        }
    }

    return 0;
EXIT_1:
    mmsdbg(DL_ERROR, "Light internal error");
    return -1;
}

int lights_AS3645_get_features (void** features)
{
    *features = &lights_AS3645_bounds;
    return 0;
}

const hat_cm_ph_lights_obj_t lights_AS3645_lights_obj =
{
     .lights_drv_id   = DTP_LIGHTS_AS3645,
     .set_power_state = lights_AS3645_set_power_state,
     .init            = lights_AS3645_init,
     .destroy         = lights_AS3645_destroy,
     .get_features    = lights_AS3645_get_features
};
