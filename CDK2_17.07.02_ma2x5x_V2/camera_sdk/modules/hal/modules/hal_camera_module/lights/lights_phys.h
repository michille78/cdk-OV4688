/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file lights_phys.h
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! Jun 10, 2014 : Author aiovtchev
*! Created
* =========================================================================== */

#ifndef __LIGHTS_PHYS_H__
#define __LIGHTS_PHYS_H__

#include <hal/hat_light.h>
#include <dtp/dtp_server_defs.h>

typedef struct hat_cm_ph_lights_obj hat_cm_ph_lights_obj_t;

typedef hat_cm_ph_lights_obj_t* hat_cm_ph_lights_handle_t;

struct hat_cm_ph_lights_obj
{
    dtp_lights_id_t          lights_drv_id;
    int (*set_power_state)  (void* sock_hndl, void* exec_func, hat_light_ctrl_entry_t* ctrl);
    int (*init)             (void* sock_hndl, void* dtp_feat, void* exec_func);
    int (*destroy)          (void* sock_hndl, void* exec_func);
    int (*get_features)     (void** features);
};

#endif // __LIGHTS_PHYS_H__
