/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file lens_dummy.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <hal/hal_camera_module/hat_cm_socket.h>
#include <hal/hal_camera_module/hat_cm_lens.h>
#include "../../lens_phys.h"

static int lens_dummy_power_off(void* sock_hndl, void* feat, void* exec_func)
{
    int err = 0;
    hat_cm_socket_command_entry_t* lens_dummy_power_off_seq;
    hat_cm_socket_cmd_strip_result_t res;
    hat_cm_socket_cmd_strip_executor_t cmd_strip_executor =
        (hat_cm_socket_cmd_strip_executor_t)exec_func;

    lens_dummy_power_off_seq = (hat_cm_socket_command_entry_t*)
        ((hat_lens_features_t *)feat)->operations.pwr_off.oper;

    err = cmd_strip_executor(
        (hat_cm_socket_handle_t)sock_hndl,
        lens_dummy_power_off_seq,
        &res);

    return err;
}

static int lens_dummy_power_on(void* sock_hndl, void* feat, void* exec_func)
{
    int err = 0;
    hat_cm_socket_command_entry_t* lens_dummy_power_on_seq;
    hat_cm_socket_cmd_strip_result_t res;
    hat_cm_socket_cmd_strip_executor_t cmd_strip_executor =
        (hat_cm_socket_cmd_strip_executor_t)exec_func;

    lens_dummy_power_on_seq = (hat_cm_socket_command_entry_t*)
        ((hat_lens_features_t *)feat)->operations.pwr_on.oper;

    err = cmd_strip_executor(
        (hat_cm_socket_handle_t)sock_hndl,
        lens_dummy_power_on_seq,
        &res);

    return err;
}

static int lens_dummy_init(void* sock_hndl, void* feat, void* exec_func)
{
    int err = 0;
    hat_cm_socket_command_entry_t* lens_dummy_init_seq;
    hat_cm_socket_cmd_strip_result_t res;
    hat_cm_socket_cmd_strip_executor_t cmd_strip_executor =
        (hat_cm_socket_cmd_strip_executor_t)exec_func;

    lens_dummy_init_seq = (hat_cm_socket_command_entry_t*)
        ((hat_lens_features_t *)feat)->operations.init.oper;

    err = cmd_strip_executor(
        (hat_cm_socket_handle_t)sock_hndl,
        lens_dummy_init_seq,
        &res);

    return err;
}

static int lens_dummy_deactivate(void* sock_hndl, void* feat, void* exec_func)
{
    int err = 0;
    hat_cm_socket_command_entry_t* lens_dummy_deactivate_seq;
    hat_cm_socket_cmd_strip_result_t res;
    hat_cm_socket_cmd_strip_executor_t cmd_strip_executor =
        (hat_cm_socket_cmd_strip_executor_t)exec_func;

    lens_dummy_deactivate_seq = (hat_cm_socket_command_entry_t*)
        ((hat_lens_features_t *)feat)->operations.deactivate.oper;

    err = cmd_strip_executor(
        (hat_cm_socket_handle_t)sock_hndl,
        lens_dummy_deactivate_seq,
        &res);

    return err;
}

static int lens_dummy_activate(void* sock_hndl, void* feat, void* exec_func)
{
    int err = 0;
    hat_cm_socket_command_entry_t* lens_dummy_activate_seq;
    hat_cm_socket_cmd_strip_result_t res;
    hat_cm_socket_cmd_strip_executor_t cmd_strip_executor =
        (hat_cm_socket_cmd_strip_executor_t)exec_func;

    lens_dummy_activate_seq = (hat_cm_socket_command_entry_t*)
        ((hat_lens_features_t *)feat)->operations.activate.oper;

    err = cmd_strip_executor(
        (hat_cm_socket_handle_t)sock_hndl,
        lens_dummy_activate_seq,
        &res);

    return err;
}

static int lens_dummy_af_move_to_pos(void* sock_hndl, void* exec_func, float pos_val)
{
    return 0;
}

static int lens_dummy_af_meas_pos(void* sock_hndl, void* exec_func, float *pos_val)
{
    return 0;
}

static int lens_dummy_oz_move_to_pos(void* sock_hndl, void* exec_func, uint32 pos_val)
{
    return 0;
}

static int lens_dummy_oz_meas_pos(void* sock_hndl, void* exec_func, uint32 *pos_val)
{
    return 0;
}

static int lens_dummy_park(void* sock_hndl, void* exec_func)
{
    return 0;
}

static int lens_dummy_set_shutter(void* sock_hndl, void* exec_func, int set)
{
    return 0;
}

static int lens_dummy_set_aperture(void* sock_hndl, void* exec_func, float* apert)
{
    return 0;
}

static int lens_dummy_set_dens_filter(void* sock_hndl, void* exec_func, float* filter)
{
    return 0;
}

static int lens_dummy_set_o_stab(void* sock_hndl, void* exec_func, int mode)
{
    return 0;
}

static int lens_dummy_set_focal_lenght(void* sock_hndl, void* exec_func, float* focal_lenght)
{
    return 0;
}

static int lens_dummy_get_aperture(void* sock_hndl, void* exec_func, float* apert)
{
    return 0;
}
static int lens_dummy_get_dens_filter(void* sock_hndl, void* exec_func, float* filter)
{
    return 0;
}

static int lens_dummy_get_o_stab(void* sock_hndl, void* exec_func, int* mode)
{
    //Optical stabilization - OFF
    *mode = 0;
    return 0;
}

static int lens_dummy_get_focal_lenght(void* sock_hndl, void* exec_func, float* focal_lenght)
{
    return 0;
}


const hat_cm_ph_lens_obj_t lens_dummy_lens_obj = {
    .lens_drv_id    = DTP_LENS_DUMMY,
    .power_off      = lens_dummy_power_off,
    .power_on       = lens_dummy_power_on,
    .init           = lens_dummy_init,
    .deactivate     = lens_dummy_deactivate,
    .activate       = lens_dummy_activate,
    .af_move_to_pos = lens_dummy_af_move_to_pos,
    .af_meas_pos    = lens_dummy_af_meas_pos,
    .oz_move_to_pos = lens_dummy_oz_move_to_pos,
    .oz_meas_pos    = lens_dummy_oz_meas_pos,
    .park           = lens_dummy_park,
    .set_shutter    = lens_dummy_set_shutter,
    .set_aperture   = lens_dummy_set_aperture,
    .set_dens_filter= lens_dummy_set_dens_filter,
    .set_o_stab     = lens_dummy_set_o_stab,
    .set_foc_lenght = lens_dummy_set_focal_lenght,
    .get_aperture   = lens_dummy_get_aperture,
    .get_dens_filter= lens_dummy_get_dens_filter,
    .get_o_stab     = lens_dummy_get_o_stab,
    .get_foc_lenght = lens_dummy_get_focal_lenght,
};
