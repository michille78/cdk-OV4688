/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file lens_aptina.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <hal/hal_camera_module/hat_cm_socket.h>
#include <hal/hal_camera_module/hat_cm_lens.h>
#include "../../lens_phys.h"

#define BUFSET16 BUF_SET_BE16
#define W BE16

#define LENS_APTINA_MAX_POS (1023)

static int lens_aptina_power_off(void* sock_hndl, void* feat, void* exec_func)
{
    int err = 0;
    hat_cm_socket_command_entry_t* lens_aptina_power_off_seq;
    hat_cm_socket_cmd_strip_result_t res;
    hat_cm_socket_cmd_strip_executor_t cmd_strip_executor =
        (hat_cm_socket_cmd_strip_executor_t)exec_func;

    lens_aptina_power_off_seq = (hat_cm_socket_command_entry_t*)
        ((hat_lens_features_t *)feat)->operations.pwr_off.oper;

    err = cmd_strip_executor(
        (hat_cm_socket_handle_t)sock_hndl,
        lens_aptina_power_off_seq,
        &res);

    return err;
}

static int lens_aptina_power_on(void* sock_hndl, void* feat, void* exec_func)
{
    int err = 0;
    hat_cm_socket_command_entry_t* lens_aptina_power_on_seq;
    hat_cm_socket_cmd_strip_result_t res;
    hat_cm_socket_cmd_strip_executor_t cmd_strip_executor =
        (hat_cm_socket_cmd_strip_executor_t)exec_func;

    lens_aptina_power_on_seq = (hat_cm_socket_command_entry_t*)
        ((hat_lens_features_t *)feat)->operations.pwr_on.oper;

    err = cmd_strip_executor(
        (hat_cm_socket_handle_t)sock_hndl,
        lens_aptina_power_on_seq,
        &res);

    return err;
}

static int lens_aptina_init(void* sock_hndl, void* feat, void* exec_func)
{
    int err = 0;
    hat_cm_socket_command_entry_t* lens_aptina_init_seq;
    hat_cm_socket_cmd_strip_result_t res;
    hat_cm_socket_cmd_strip_executor_t cmd_strip_executor =
        (hat_cm_socket_cmd_strip_executor_t)exec_func;

    lens_aptina_init_seq = (hat_cm_socket_command_entry_t*)
        ((hat_lens_features_t *)feat)->operations.init.oper;

    err = cmd_strip_executor(
        (hat_cm_socket_handle_t)sock_hndl,
        lens_aptina_init_seq,
        &res);

    return err;
}

static int lens_aptina_deactivate(void* sock_hndl, void* feat, void* exec_func)
{
    int err = 0;
    hat_cm_socket_command_entry_t* lens_aptina_deactivate_seq;
    hat_cm_socket_cmd_strip_result_t res;
    hat_cm_socket_cmd_strip_executor_t cmd_strip_executor =
        (hat_cm_socket_cmd_strip_executor_t)exec_func;

    lens_aptina_deactivate_seq = (hat_cm_socket_command_entry_t*)
        ((hat_lens_features_t *)feat)->operations.deactivate.oper;

    err = cmd_strip_executor(
        (hat_cm_socket_handle_t)sock_hndl,
        lens_aptina_deactivate_seq,
        &res);

    return err;
}

static int lens_aptina_activate(void* sock_hndl, void* feat, void* exec_func)
{
    int err = 0;
    hat_cm_socket_command_entry_t* lens_aptina_activate_seq;
    hat_cm_socket_cmd_strip_result_t res;
    hat_cm_socket_cmd_strip_executor_t cmd_strip_executor =
        (hat_cm_socket_cmd_strip_executor_t)exec_func;

    lens_aptina_activate_seq = (hat_cm_socket_command_entry_t*)
        ((hat_lens_features_t *)feat)->operations.activate.oper;

    err = cmd_strip_executor(
        (hat_cm_socket_handle_t)sock_hndl,
        lens_aptina_activate_seq,
        &res);

    return err;
}

static int lens_aptina_af_move_to_pos(void* sock_hndl, void* exec_func, float pos_val)
{
    int err = 0;
    hat_cm_socket_cmd_strip_result_t res;
    hat_cm_socket_cmd_strip_executor_t cmd_strip_executor =
        (hat_cm_socket_cmd_strip_executor_t)exec_func;
    uint32 pos;
    uint8 buf[2];

    hat_cm_socket_command_entry_t lens_aptina_prepare_move_to_pos[] = {
        LENS_INTENT_CMD_CCI_I2C_WR(0x30F0, W(0x8000)),
        CMD_ENTRY_END(),
    };

    // min pos 0.0 (0), max pos 1.0 (1023)
    pos = ROUND_FLOAT_X_TO_INT(pos_val * LENS_APTINA_MAX_POS);
    BUFSET16(buf, pos);
    hat_cm_socket_command_entry_t pos_entry[] = {
        LENS_INTENT_CMD_CCI_I2C_WR_BUF(0x30F2, 2, buf),
        CMD_ENTRY_END(),
    };

    err = cmd_strip_executor(
        (hat_cm_socket_handle_t)sock_hndl,
        (hat_cm_socket_command_entry_t *)lens_aptina_prepare_move_to_pos,
        &res);

    err = cmd_strip_executor(
        (hat_cm_socket_handle_t)sock_hndl,
        (hat_cm_socket_command_entry_t *)&pos_entry,
        &res);

    return err;
}

static int lens_aptina_af_meas_pos(void* sock_hndl, void* exec_func, float *pos_val)
{
    // For voice-coil current position is tracked by the high-level driver
    return 0;
}

static int lens_aptina_oz_move_to_pos(void* sock_hndl, void* exec_func, uint32 pos_val)
{
    return 0;
}

static int lens_aptina_oz_meas_pos(void* sock_hndl, void* exec_func, uint32 *pos_val)
{
    return 0;
}

static int lens_aptina_park(void* sock_hndl, void* exec_func)
{
    return 0;
}

static int lens_aptina_set_shutter(void* sock_hndl, void* exec_func, int set)
{
    return 0;
}

static int lens_aptina_set_aperture(void* sock_hndl, void* exec_func, float* apert)
{
    return 0;
}

static int lens_aptina_set_dens_filter(void* sock_hndl, void* exec_func, float* filter)
{
    return 0;
}

static int lens_aptina_set_o_stab(void* sock_hndl, void* exec_func, int mode)
{
    return 0;
}

static int lens_aptina_set_focal_lenght(void* sock_hndl, void* exec_func, float* focal_lenght)
{
    return 0;
}

static int lens_aptina_get_aperture(void* sock_hndl, void* exec_func, float* apert)
{
    return 0;
}
static int lens_aptina_get_dens_filter(void* sock_hndl, void* exec_func, float* filter)
{
    return 0;
}

static int lens_aptina_get_o_stab(void* sock_hndl, void* exec_func, int* mode)
{
    //Optical stabilization - OFF
    *mode = 0;
    return 0;
}

static int lens_aptina_get_focal_lenght(void* sock_hndl, void* exec_func, float* focal_lenght)
{
    return 0;
}




const hat_cm_ph_lens_obj_t lens_aptina_lens_obj = {
    .lens_drv_id    = DTP_LENS_APTINA,
    .power_off      = lens_aptina_power_off,
    .power_on       = lens_aptina_power_on,
    .init           = lens_aptina_init,
    .deactivate     = lens_aptina_deactivate,
    .activate       = lens_aptina_activate,
    .af_move_to_pos = lens_aptina_af_move_to_pos,
    .af_meas_pos    = lens_aptina_af_meas_pos,
    .oz_move_to_pos = lens_aptina_oz_move_to_pos,
    .oz_meas_pos    = lens_aptina_oz_meas_pos,
    .park           = lens_aptina_park,
    .set_shutter    = lens_aptina_set_shutter,
    .set_aperture   = lens_aptina_set_aperture,
    .set_dens_filter= lens_aptina_set_dens_filter,
    .set_o_stab     = lens_aptina_set_o_stab,
    .set_foc_lenght = lens_aptina_set_focal_lenght,
    .get_aperture   = lens_aptina_get_aperture,
    .get_dens_filter= lens_aptina_get_dens_filter,
    .get_o_stab     = lens_aptina_get_o_stab,
    .get_foc_lenght = lens_aptina_get_focal_lenght,
};
