/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file lens_phys.h
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#ifndef __LENS_PHYS_H__
#define __LENS_PHYS_H__

#include <hal/hat_types.h>
#include <dtp/dtp_server_defs.h>

typedef struct hat_cm_ph_lens_obj hat_cm_ph_lens_obj_t;

typedef hat_cm_ph_lens_obj_t* hat_cm_ph_lens_handle_t;

struct hat_cm_ph_lens_obj {
    dtp_lenses_id_t              lens_drv_id;
    int (*power_off)            (void* sock_hndl, void* feat, void* exec_func);
    int (*power_on)             (void* sock_hndl, void* feat, void* exec_func);
    int (*init)                 (void* sock_hndl, void* feat, void* exec_func);
    int (*deactivate)           (void* sock_hndl, void* feat, void* exec_func);
    int (*activate)             (void* sock_hndl, void* feat, void* exec_func);
    int (*af_move_to_pos)       (void* sock_hndl, void* exec_func, float   pos_val);
    int (*af_meas_pos)          (void* sock_hndl, void* exec_func, float*  pos_val);
    int (*oz_move_to_pos)       (void* sock_hndl, void* exec_func, uint32  pos_val);
    int (*oz_meas_pos)          (void* sock_hndl, void* exec_func, uint32* pos_val);
    int (*park)                 (void* sock_hndl, void* exec_func);
    int (*set_shutter)          (void* sock_hndl, void* exec_func, int set);
    int (*set_aperture)         (void* sock_hndl, void* exec_func, float* apert);
    int (*set_dens_filter)      (void* sock_hndl, void* exec_func, float* filter);
    int (*set_o_stab)           (void* sock_hndl, void* exec_func, int mode);
    int (*set_foc_lenght)     (void* sock_hndl, void* exec_func, float* focal_lenght);
    int (*get_aperture)         (void* sock_hndl, void* exec_func, float* apert);
    int (*get_dens_filter)      (void* sock_hndl, void* exec_func, float* filter);
    int (*get_o_stab)           (void* sock_hndl, void* exec_func, int* mode);
    int (*get_foc_lenght)     (void* sock_hndl, void* exec_func, float* focal_lenght);
};

#endif // __LENS_PHYS_H__
