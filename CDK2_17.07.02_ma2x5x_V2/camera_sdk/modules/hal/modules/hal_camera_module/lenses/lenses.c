/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file lenses.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include "lenses.h"
#include "lens_dummy/inc/lens_dummy.h"
#ifdef LENS_MODULE_APTINA
#include "lens_aptina/inc/lens_aptina.h"
#endif
#ifdef LENS_MODULE_DW9714A
#include "lens_dw9714a/inc/lens_dw9714a.h"
#endif
#ifdef LENS_MODULE_LC898212XA
#include "lens_lc898212xa/inc/lens_lc898212xa.h"
#endif

#include <utils/mms_debug.h>

mmsdbg_declare_variable(hal_cm_driver);
#define MMSDEBUGLEVEL mmsdbg_use_variable(hal_cm_driver)

const hat_cm_ph_lens_handle_t lens_handle_list[] = {
    (hat_cm_ph_lens_handle_t)&lens_dummy_lens_obj,
#ifdef LENS_MODULE_APTINA
    (hat_cm_ph_lens_handle_t)&lens_aptina_lens_obj,
#endif
#ifdef LENS_MODULE_DW9714A
    (hat_cm_ph_lens_handle_t)&lens_dw9714a_lens_obj,
#endif
#ifdef LENS_MODULE_LC898212XA
    (hat_cm_ph_lens_handle_t)&lens_lc898212xa_lens_obj,
#endif
};

#ifndef ARR_SIZE
#define ARR_SIZE(a) (sizeof(a)/sizeof(a[0]))
#endif

hat_cm_ph_lens_handle_t hal_get_lens_driver(dtp_lenses_id_t id)
{
int i;
    for (i = 0; i<ARR_SIZE(lens_handle_list); i++)
    {
        if (lens_handle_list[i]->lens_drv_id == id)
        {
            mmsdbg(DL_MESSAGE, "Mounting lens drv %d with index %d", id, i);
            return lens_handle_list[i];
        }
    }
    mmsdbg(DL_ERROR, "Can't find lens driver for %d. Using index 0", id);
    return lens_handle_list[0];
}
