/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file dtp_lens_dummy_db.h
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#ifndef __DTP_LENS_DUMMY_DB_H__
#define __DTP_LENS_DUMMY_DB_H__


// Dummy lens
static const hat_cm_socket_command_entry_t lens_dummy_power_off_seq[] = {
    CMD_ENTRY_DUMMY(),
    CMD_ENTRY_END(),
};

static const hat_cm_socket_command_entry_t lens_dummy_power_on_seq[] = {
    CMD_ENTRY_DUMMY(),
    CMD_ENTRY_END(),
};

static const hat_cm_socket_command_entry_t lens_dummy_init_seq[] = {
    CMD_ENTRY_DUMMY(),
    CMD_ENTRY_END(),
};

static const hat_cm_socket_command_entry_t lens_dummy_deactivate_seq[] = {
    CMD_ENTRY_DUMMY(),
    CMD_ENTRY_END(),
};

static const hat_cm_socket_command_entry_t lens_dummy_activate_seq[] = {
    CMD_ENTRY_DUMMY(),
    CMD_ENTRY_END(),
};

static const float lens_dummy_available_apertures[] = {
    2.8f
};

static const float lens_dummy_available_filt_densities[] = {
    0
};

static const float lens_dummy_available_focal_lengths[] = {
    4.76f
};

static const uint8 lens_dummy_available_optical_st[] = {
    0 //OFF
};

static const float lens_dummy_geom_correction_map[] = {
        0,    0,        0,    0,        0,    0,
     1.0f,    0,     1.0f,    0,     1.0f,    0,
        0, 1.0f,        0, 1.0f,        0, 1.0f,
     1.0f, 1.0f,     1.0f, 1.0f,     1.0f, 1.0f
};


static hat_dtp_lens_desc_t dtp_lens_dummy_db =
{ // Dummy camera module
    .lens_id          = 0,
    .lens_name        = "Dummy lens",
    .lens_features    = {
        0,    //uint16  shutter;
        0,    //uint16  blend;
        0,    //uint16  zoom;
        0,    //uint16  measure_pos;
        0,    //uint16  measure_time_us;
        0.0,  //float  inf_pos;  //infinity position[ 0.0 - 1.0]
        1.0,  //float  mac_pos;  //macro position[ 0.0 - 1.0]
        0.5,  //float  dist_tol; //Optimal distance tolerance [diopter].
        NULL, //int32   *offset;
        NULL, //void    *eeprom_version;
        {   //const hat_lens_operations_t operations;
            .pwr_off = {
                ARR_SIZE(lens_dummy_power_off_seq),
                lens_dummy_power_off_seq,
            },
            .pwr_on = {
                ARR_SIZE(lens_dummy_power_on_seq),
                lens_dummy_power_on_seq,
            },
            .init = {
                ARR_SIZE(lens_dummy_init_seq),
                lens_dummy_init_seq,
            },
            .deactivate = {
                ARR_SIZE(lens_dummy_deactivate_seq),
                lens_dummy_deactivate_seq,
            },
            .activate = {
                ARR_SIZE(lens_dummy_activate_seq),
                lens_dummy_activate_seq,
            },
        },
        // HAL 3.0 Specific
        ARR_SIZE(lens_dummy_available_apertures), //uint16  available_apertures_size;
        lens_dummy_available_apertures, //float  *available_apertures;          // List of supported aperture values.
        ARR_SIZE(lens_dummy_available_filt_densities), //uint16  available_filt_densities_size;
        lens_dummy_available_filt_densities, //float  *available_filt_densities;     // List of supported ND filter values
        ARR_SIZE(lens_dummy_available_focal_lengths), //uint16  available_focal_lengths_size;
        lens_dummy_available_focal_lengths, //float  *available_focal_lengths;      // If fitted with optical zoom, what focal
                                            //                                      // lengths are available. If not, the static focal length
        ARR_SIZE(lens_dummy_available_optical_st), //uint16  available_optical_st_size;
        lens_dummy_available_optical_st, //uint8   *available_optical_st;         // List of supported optical image stabilization modes.
        lens_dummy_geom_correction_map, //float  *geom_correction_map[2][3];    // A low-resolution map for correction of geometric
                                        //                                      // distortions and chromatic aberrations, per color channel.
        {2,2}, //int32  geom_correction_map_size[2];   // Dimensions of geometric correction map
        0.000037074f, //(1/2.6973f) //int32  hyperfocal_distance;           // Hyperfocal distance for this lens; set to 0 if fixed focus [diopters].
        10.0f, //(1 / 0.1f)//float minimum_focus_distance;        // Shortest distance from front most surface of the lens that can be focused correctly [diopters].
        {17, 13}, //int32  shading_map_size[2];           // Dimensions of lens shading map.
        FACING_BACK, //facing_t    facing;                   // Direction the camera faces relative to device screen;
        {0, 0}, //float  optical_axis_angle[2];        // Relative angle of camera optical axis to the perpendicular axis from the display
        {5.0f, 5.0f, 4.7f} //float  position[3];                  // Coordinates of camera optical axis on device in [mm].
    },
};

#define DTP_LENS_DUMMY_DB_SIZE ( \
    sizeof(dtp_lens_dummy_db) \
)

#endif //__DTP_LENS_DUMMY_DB_H__
