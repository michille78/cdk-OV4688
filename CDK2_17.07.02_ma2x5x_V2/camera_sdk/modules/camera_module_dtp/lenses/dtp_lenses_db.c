/* =============================================================================
* Copyright (c) 2013-2015 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file dtp_lenses_db.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 12-May-2015 : Author ( MM Solutions AD ) Evgeniy Borisov
*! Created
* =========================================================================== */

#include <hal/hal_camera_module/hat_cm_dtp_data.h>
#include "dtp_lenses_db.h"

#include "dtp_lens_dummy_db/dtp_lens_dummy.h"

#ifdef LENS_MODULE_APTINA
#include "dtp_lens_aptina_db/dtp_lens_aptina.h"
#endif
#ifdef LENS_MODULE_DW9714A
#include "dtp_lens_dw9714a_db/dtp_lens_dw9714a.h"
#endif
#ifdef LENS_MODULE_LC898212XA
#include "dtp_lens_lc898212xa_db/dtp_lens_lc898212xa.h"
#endif

static const dtp_lens_db_t dtp_lenses_db[] = {
    ADD_LENS_DTP(dummy_lens),
#ifdef LENS_MODULE_APTINA
    ADD_LENS_DTP(aptina),
#endif
#ifdef LENS_MODULE_LC898212XA
    ADD_LENS_DTP(lc898212xa),
#endif
#ifdef LENS_MODULE_DW9714A
    ADD_LENS_DTP(dw9714a),
#endif
};

void* get_lens_dtp_db(int lens_id)
{
    int i;
    hat_dtp_lens_desc_t *dtp_lens_db;

    for (i = 0; i < ARR_SIZE(dtp_lenses_db); i++) {
        dtp_lens_db = dtp_lenses_db[i].dtp_db();
        if (dtp_lens_db->lens_id == lens_id)
            return (void*)dtp_lens_db;
        else
            dtp_lens_db = get_dummy_lens_dtp_db();
    }
    return dtp_lens_db;
}

int get_lens_dtp_db_size(int lens_id)
{
    int i;
    hat_dtp_lens_desc_t *dtp_lens_db;

    for (i = 0; i < ARR_SIZE(dtp_lenses_db); i++) {
        dtp_lens_db = dtp_lenses_db[i].dtp_db();
        if (dtp_lens_db->lens_id == lens_id)
            return dtp_lenses_db[i].dtp_db_size();
    }
    return 0;
}
