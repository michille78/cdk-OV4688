/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file dtp_nvm_dummy_db.h
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#ifndef __DTP_NVM_DUMMY_DB_H__
#define __DTP_NVM_DUMMY_DB_H__

static const hat_cm_socket_command_entry_t nvm_dummy_power_off_seq[] = {
    CMD_ENTRY_DUMMY(),
    CMD_ENTRY_END(),
};

static const hat_cm_socket_command_entry_t nvm_dummy_power_on_seq[] = {
    CMD_ENTRY_DUMMY(),
    CMD_ENTRY_END(),
};

static const hat_cm_socket_command_entry_t nvm_dummy_init_seq[] = {
    CMD_ENTRY_DUMMY(),
    CMD_ENTRY_END(),
};

static hat_dtp_nvm_desc_t dtp_nvm_dummy_db =
{ // Dummy camera module
    .nvm_id          = 0,
    .nvm_name        = "Dummy nvm",
    .nvm_features    = {
        0,                  //uint32  feat1;
        NVM_TYPE_DUMMY_ROM, //HAT_CM_NVM_TYPE_T nvm_type;
        256,                //uint32 nvm_size_bytes;
        {   //const hat_nvm_operations_t operations;
            .pwr_off = {
                ARR_SIZE(nvm_dummy_power_off_seq),
                nvm_dummy_power_off_seq,
            },
            .pwr_on = {
                ARR_SIZE(nvm_dummy_power_on_seq),
                nvm_dummy_power_on_seq,
            },
            .init = {
                ARR_SIZE(nvm_dummy_init_seq),
                nvm_dummy_init_seq,
            },
        },
    },
};

#define DTP_NVM_DUMMY_DB_SIZE ( \
    sizeof(dtp_nvm_dummy_db) \
)

#endif //__DTP_NVM_DUMMY_DB_H__
