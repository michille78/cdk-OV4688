/* =============================================================================
* Copyright (c) 2013-2015 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file dtp_nvms_db.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 12-May-2015 : Author ( MM Solutions AD ) Evgeniy Borisov
*! Created
* =========================================================================== */

#include <hal/hal_camera_module/hat_cm_dtp_data.h>
#include "dtp_nvms_db.h"

#include "dtp_nvm_dummy_db/dtp_nvm_dummy.h"


static const dtp_nvm_db_t dtp_nvms_db[] = {
    ADD_NVM_DTP(dummy_nvm),
};

void* get_nvm_dtp_db(int nvm_id)
{
    int i;
    hat_dtp_nvm_desc_t *dtp_nvm_db;

    for (i = 0; i < ARR_SIZE(dtp_nvms_db); i++) {
        dtp_nvm_db = dtp_nvms_db[i].dtp_db();
        if (dtp_nvm_db->nvm_id == nvm_id)
            return (void*)dtp_nvm_db;
        else
            dtp_nvm_db = get_dummy_nvm_dtp_db();
    }
    return dtp_nvm_db;
}

int get_nvm_dtp_db_size(int nvm_id)
{
    int i;
    hat_dtp_nvm_desc_t *dtp_nvm_db;

    for (i = 0; i < ARR_SIZE(dtp_nvms_db); i++) {
        dtp_nvm_db = dtp_nvms_db[i].dtp_db();
        if (dtp_nvm_db->nvm_id == nvm_id)
            return dtp_nvms_db[i].dtp_db_size();
        else
            dtp_nvm_db = (hat_dtp_nvm_desc_t *)get_dummy_nvm_dtp_db_size();
    }
    return 0;
}
