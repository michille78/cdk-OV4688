/* =============================================================================
* Copyright (c) 2013-2015 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file dtp_nvms_db.h
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 12-May-2015 : Author ( MM Solutions AD ) Evgeniy Borisov
*! Created
* =========================================================================== */

#ifndef __DTP_NVM_DB_H__
#define __DTP_NVM_DB_H__

#define ADD_NVM_DTP(_nvm) \
{\
        .dtp_db         = get_##_nvm##_dtp_db,\
        .dtp_db_size    = get_##_nvm##_dtp_db_size,\
}

typedef void *(*dtp_db_t)(void);
typedef int (*dtp_db_size_t)(void);
typedef struct {
    dtp_db_t        dtp_db;
    dtp_db_size_t   dtp_db_size;
} dtp_nvm_db_t;

void* get_nvm_dtp_db(int nvm_id);
int get_nvm_dtp_db_size(int nvm_id);

#endif //__DTP_NVM_DB_H__
