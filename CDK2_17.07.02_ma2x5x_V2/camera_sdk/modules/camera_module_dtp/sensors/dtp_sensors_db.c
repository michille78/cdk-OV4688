/* =============================================================================
* Copyright (c) 2013-2015 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file dtp_sensor_ar1330.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 12-May-2015 : Author ( MM Solutions AD ) Evgeniy Borisov
*! Created
* =========================================================================== */

#include <hal/hal_camera_module/hat_cm_dtp_data.h>
#include "dtp_sensors_db.h"
#include "dtp_sensor_dummy_db/dtp_sensor_dummy.h"

#ifdef CAMERA_MODULE_DTP_AR1330
#include "dtp_sensor_ar1330_db/dtp_sensor_ar1330.h"
#endif
#ifdef CAMERA_MODULE_DTP_IMX_DUAL_214
#include "dtp_sensor_dual_imx214_db/dtp_sensor_dual_imx214.h"
#endif
#ifdef CAMERA_MODULE_DTP_IMX208
#include "dtp_sensor_imx208_db/dtp_sensor_imx208.h"
#endif
#ifdef CAMERA_MODULE_DTP_IMX214
#include "dtp_sensor_imx214_db/dtp_sensor_imx214.h"
#endif
#ifdef CAMERA_MODULE_DTP_OV13860
#include "dtp_sensor_ov13860_db/dtp_sensor_ov13860.h"
#endif
#ifdef CAMERA_MODULE_DTP_AR0330
#include "dtp_sensor_ar0330_db/dtp_sensor_ar0330.h"
#endif
#ifdef CAMERA_MODULE_DTP_OV7251
#include "dtp_sensor_ov7251_db/dtp_sensor_ov7251.h"
#endif

#include "dtp_sensor_ov5658_db/dtp_sensor_ov5658.h"
#include "dtp_sensor_ov4188_db/dtp_sensor_ov4188.h"

static const dtp_sensor_db_t dtp_sensors_db[] = {
    ADD_SEN_DTP(dummy_sensor),
#ifdef CAMERA_MODULE_DTP_IMX208
    ADD_SEN_DTP(imx208),
#endif

#if (defined(CAMERA_MODULE_DTP_IMX214) || defined(CAMERA_MODULE_DTP_IMX_DUAL_214))
    ADD_SEN_DTP(imx214),
#endif
#ifdef CAMERA_MODULE_DTP_AR1330
    ADD_SEN_DTP(ar1330),
#endif
#ifdef CAMERA_MODULE_DTP_OV13860
    ADD_SEN_DTP(ov13860),
#endif
#ifdef CAMERA_MODULE_DTP_AR0330
    ADD_SEN_DTP(ar0330),
#endif
#ifdef CAMERA_MODULE_DTP_OV7251
    ADD_SEN_DTP(ov7251_1),
    ADD_SEN_DTP(ov7251_2),
#endif
	ADD_SEN_DTP(ov5658),
    ADD_SEN_DTP(ov4188),
};

void* get_sensor_dtp_db(int sensor_id)
{
    int i;
    hat_dtp_sensor_desc_t *dtp_sensor_db;

    for (i = 0; i < ARR_SIZE(dtp_sensors_db); i++) {
        dtp_sensor_db = dtp_sensors_db[i].dtp_db();
        if (dtp_sensor_db->sensor_id == sensor_id)
            return (void*)dtp_sensor_db;
        else
            dtp_sensor_db = get_dummy_sensor_dtp_db();
    }
    return dtp_sensor_db;
}

int get_sensor_dtp_db_size(int sensor_id)
{
    int i;
    hat_dtp_sensor_desc_t *dtp_sensor_db;

    for (i = 0; i < ARR_SIZE(dtp_sensors_db); i++) {
        dtp_sensor_db = dtp_sensors_db[i].dtp_db();
        if (dtp_sensor_db->sensor_id == sensor_id)
            return dtp_sensors_db[i].dtp_db_size();
    }
    return 0;
}
