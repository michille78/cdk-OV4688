/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file dtp_sensor_imx214_db.h
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#ifndef __DTP_SENSOR_IMX214_DB_H__
#define __DTP_SENSOR_IMX214_DB_H__

// ============================ IMX214 camera module =========================

static const hat_cm_socket_command_entry_t imx214_power_off_seq[] = {
    //SENS_INTENT_ACTION(SEN_RSRC_XSHTDWN, HAT_HW_RES_OFF),
    SENS_INTENT_ACTION(SEN_RSRC_POWER_1, HAT_HW_RES_OFF),
    SENS_DELAY(1000),
    CMD_ENTRY_DUMMY(),
    CMD_ENTRY_END(),
};

static const hat_cm_socket_command_entry_t imx214_power_on_seq[] = {
    //SENS_INTENT_ACTION(SEN_RSRC_XSHTDWN, HAT_HW_RES_ON),
    //SENS_DELAY(1000),
    SENS_INTENT_ACTION(SEN_RSRC_POWER_1, HAT_HW_RES_ON),
    SENS_INTENT_ACTION(SEN_RSRC_CLOCK, HAT_HW_RES_ON),  // ENABLE CLOCK
    SENS_INTENT_ACTION(SEN_RSRC_XSHTDWN, HAT_HW_RES_OFF), //XSHDN#
    SENS_DELAY(1000),
    SENS_INTENT_ACTION(SEN_RSRC_XSHTDWN, HAT_HW_RES_ON), //XSHDN#
    CMD_ENTRY_DUMMY(),
    CMD_ENTRY_END(),
};

//imx214_2L_4208x3120_RAW10_10Hz_I2C_regs
static const hat_cm_socket_command_entry_t imx214_12MHz_init_seq[] = {

   SENS_INTENT_CMD_CCI_I2C_WR(0x0100, 0x00
                              /*0x0101 */, 0x00), // image_orientation

    //Preset setting
    //Mode setting
#if (FULL_30FPS == 1)
    SENS_INTENT_CMD_CCI_I2C_WR(0x0114, 0x03), // CSI_LANE_MODE - 4 lanes
#else
    SENS_INTENT_CMD_CCI_I2C_WR(0x0114, 0x01), // CSI_LANE_MODE - 2 lanes
#endif
    SENS_INTENT_CMD_CCI_I2C_WR(0x0220, 0x00  // HDR_mode
                               /*0x0221*/, 0x11 // HDR_resolution_reduction
/*    0x0222*/, 0x01), // Exposure_ratio


//SENS_INTENT_CMD_CCI_I2C_WR(0x3000, 0x3D), // Manufacturer specified register
    SENS_INTENT_CMD_CCI_I2C_WR(0x3000, 0x35), // Manufacturer specified register
    SENS_INTENT_CMD_CCI_I2C_WR(0x3054, 0x01), // Manufacturer specified register
    SENS_INTENT_CMD_CCI_I2C_WR(0x305C, 0x11), // Manufacturer specified register

    //Output format
    SENS_INTENT_CMD_CCI_I2C_WR(0x0112, 0x0A     // CSI_DT_FMT_H [7:0] 0x0A0A - RAW10
                               /*0x0113*/, 0x0A),   // CSI_DT_FMT_L [7:0]
    //Clock setting
    SENS_INTENT_CMD_CCI_I2C_WR(0x0301, 0x05), // VTPXCK_DIV
#if (FULL_30FPS == 1)
    SENS_INTENT_CMD_CCI_I2C_WR(0x0303, 0x02), // VTSYCK_DIV
#else
    SENS_INTENT_CMD_CCI_I2C_WR(0x0303, 0x04), // VTSYCK_DIV
#endif

    SENS_INTENT_CMD_CCI_I2C_WR(0x0305, 0x02    // PREPLLCK_VT_DIV
                               /*0x0306*/, 0x00   // PLL_VT_MPY[10:8]
                               /*0x0307*/, 0xC8), // PLL_VT_MPY[7:0]

    SENS_INTENT_CMD_CCI_I2C_WR(0x0309, 0x0A), // OPPXCK_DIV
    SENS_INTENT_CMD_CCI_I2C_WR(0x030B, 0x01), // OPSYCK_DIV
    SENS_INTENT_CMD_CCI_I2C_WR(0x0310, 0x00), // PLL_MULT_DRIV

#if (FULL_30FPS == 1)
    //Data Rate setting
    SENS_INTENT_CMD_CCI_I2C_WR(0x0820, 0x12     // REQ_LINK_BIT_RATE_MBPS[31:24]
                                /*0x0821*/, 0xC0    // REQ_LINK_BIT_RATE_MBPS[23:16]
                                /*0x0822*/, 0x00    // REQ_LINK_BIT_RATE_MBPS[15:8]
                                /*0x0823*/, 0x00),  // REQ_LINK_BIT_RATE_MBPS[7:0]
#else
    //Data Rate setting
    SENS_INTENT_CMD_CCI_I2C_WR(0x0820, 0x09     // REQ_LINK_BIT_RATE_MBPS[31:24]
                                /*0x0821*/, 0x60    // REQ_LINK_BIT_RATE_MBPS[23:16]
                                /*0x0822*/, 0x00    // REQ_LINK_BIT_RATE_MBPS[15:8]
                                /*0x0823*/, 0x00),  // REQ_LINK_BIT_RATE_MBPS[7:0]
#endif

    //WaterMark setting
    SENS_INTENT_CMD_CCI_I2C_WR(0x3A03, 0x08     // ???
                                /*0x3A04*/, 0x70   // ????
                                /*0x3A05*/, 0x02), // ???

    //Enable setting
    SENS_INTENT_CMD_CCI_I2C_WR(0x0B06, 0x01), // SING_DEF_CORR_EN 1-enable

    //Test setting
    SENS_INTENT_CMD_CCI_I2C_WR(0x30A2, 0x00), // NML_NR_EN[0]-0:LNR OFF NML_NR_EN[1]-0:CNR OFF
    SENS_INTENT_CMD_CCI_I2C_WR(0x30B4, 0x00), // ???

    //HDR setting
    SENS_INTENT_CMD_CCI_I2C_WR(0x3A02, 0xFF), // ???

    //STATS setting
    SENS_INTENT_CMD_CCI_I2C_WR(0x3013, 0x00), // STATS_OUT_EN 0:Stats data is not output

    SENS_INTENT_CMD_CCI_I2C_WR(0x5062, 0x10     // ???
                                /*0x5063*/, 0x70    // ???
                                /*0x5064*/, 0x00),  //???

    // Stop Embedded data
    SENS_INTENT_CMD_CCI_I2C_WR(0x5041, 0x00), // Embedded dataline control Register 0:no EBD

    SENS_INTENT_CMD_CCI_I2C_WR(0x0138, 0x01), // Enable temperature sensor
    CMD_ENTRY_END(),
};

static const hat_cm_socket_command_entry_t imx214_24MHz_init_seq[] = {

   SENS_INTENT_CMD_CCI_I2C_WR(0x0100, 0x00
                              /*0x0101 */, 0x00), // image_orientation

    //Preset setting
    //Mode setting
#if (FULL_30FPS == 1)
    SENS_INTENT_CMD_CCI_I2C_WR(0x0114, 0x03), // CSI_LANE_MODE - 4 lanes
#else
    SENS_INTENT_CMD_CCI_I2C_WR(0x0114, 0x01), // CSI_LANE_MODE - 2 lanes
#endif

    SENS_INTENT_CMD_CCI_I2C_WR(0x0220, 0x00  // HDR_mode
                               /*0x0221*/, 0x11 // HDR_resolution_reduction
/*    0x0222*/, 0x01), // Exposure_ratio


//SENS_INTENT_CMD_CCI_I2C_WR(0x3000, 0x3D), // Manufacturer specified register
    SENS_INTENT_CMD_CCI_I2C_WR(0x3000, 0x35), // Manufacturer specified register
    SENS_INTENT_CMD_CCI_I2C_WR(0x3054, 0x01), // Manufacturer specified register
    SENS_INTENT_CMD_CCI_I2C_WR(0x305C, 0x11), // Manufacturer specified register

    //Output format
    SENS_INTENT_CMD_CCI_I2C_WR(0x0112, 0x0A     // CSI_DT_FMT_H [7:0] 0x0A0A - RAW10
                               /*0x0113*/, 0x0A),   // CSI_DT_FMT_L [7:0]
    //Clock setting
    SENS_INTENT_CMD_CCI_I2C_WR(0x0301, 0x05), // VTPXCK_DIV
#if (FULL_30FPS == 1)
    SENS_INTENT_CMD_CCI_I2C_WR(0x0303, 0x02), // VTSYCK_DIV
#else
     SENS_INTENT_CMD_CCI_I2C_WR(0x0303, 0x04), // VTSYCK_DIV
#endif
    SENS_INTENT_CMD_CCI_I2C_WR(0x0305, 0x03    // PREPLLCK_VT_DIV
                               /*0x0306*/, 0x00   // PLL_VT_MPY[10:8]
                               /*0x0307*/, 0x96), // PLL_VT_MPY[7:0]

    SENS_INTENT_CMD_CCI_I2C_WR(0x0309, 0x0A), // OPPXCK_DIV
    SENS_INTENT_CMD_CCI_I2C_WR(0x030B, 0x01), // OPSYCK_DIV
    SENS_INTENT_CMD_CCI_I2C_WR(0x0310, 0x00), // PLL_MULT_DRIV

#if (FULL_30FPS == 1)
    //Data Rate setting
    SENS_INTENT_CMD_CCI_I2C_WR(0x0820, 0x12     // REQ_LINK_BIT_RATE_MBPS[31:24]
                                /*0x0821*/, 0xC0    // REQ_LINK_BIT_RATE_MBPS[23:16]
                                /*0x0822*/, 0x00    // REQ_LINK_BIT_RATE_MBPS[15:8]
                                /*0x0823*/, 0x00),  // REQ_LINK_BIT_RATE_MBPS[7:0]
#else
    //Data Rate setting
    SENS_INTENT_CMD_CCI_I2C_WR(0x0820, 0x09     // REQ_LINK_BIT_RATE_MBPS[31:24]
                                /*0x0821*/, 0x60    // REQ_LINK_BIT_RATE_MBPS[23:16]
                                /*0x0822*/, 0x00    // REQ_LINK_BIT_RATE_MBPS[15:8]
                                /*0x0823*/, 0x00),  // REQ_LINK_BIT_RATE_MBPS[7:0]
#endif
    //WaterMark setting
    SENS_INTENT_CMD_CCI_I2C_WR(0x3A03, 0x08     // ???
                                /*0x3A04*/, 0x70   // ????
                                /*0x3A05*/, 0x02), // ???

    //Enable setting
    SENS_INTENT_CMD_CCI_I2C_WR(0x0B06, 0x01), // SING_DEF_CORR_EN 1-enable

    //Test setting
    SENS_INTENT_CMD_CCI_I2C_WR(0x30A2, 0x00), // NML_NR_EN[0]-0:LNR OFF NML_NR_EN[1]-0:CNR OFF
    SENS_INTENT_CMD_CCI_I2C_WR(0x30B4, 0x00), // ???

    //HDR setting
    SENS_INTENT_CMD_CCI_I2C_WR(0x3A02, 0xFF), // ???

    //STATS setting
    SENS_INTENT_CMD_CCI_I2C_WR(0x3013, 0x00), // STATS_OUT_EN 0:Stats data is not output

    SENS_INTENT_CMD_CCI_I2C_WR(0x5062, 0x10     // ???
                                /*0x5063*/, 0x70    // ???
                                /*0x5064*/, 0x00),  //???

    // Stop Embedded data
    SENS_INTENT_CMD_CCI_I2C_WR(0x5041, 0x00), // Embedded dataline control Register 0:no EBD

    SENS_INTENT_CMD_CCI_I2C_WR(0x0138, 0x01), // Enable temperature sensor
    CMD_ENTRY_END(),
};

static const hat_cm_socket_command_entry_t imx214_stream_off_seq[] = {
    SENS_INTENT_CMD_CCI_I2C_WR(0x0100, 0),
    CMD_ENTRY_END(),
};

static const hat_cm_socket_command_entry_t imx214_stream_on_seq[] = {
    SENS_INTENT_CMD_CCI_I2C_WR(0x0100, 1),
    CMD_ENTRY_END(),
};




static const hat_cm_socket_command_entry_t imx214_mode0_settings[] = {
    CMD_ENTRY_DUMMY(),
    CMD_ENTRY_END(),
};

static const hat_cm_socket_command_entry_t imx214_mode1_settings[] = {
    CMD_ENTRY_DUMMY(),
    CMD_ENTRY_END(),
};

#if (TEST_SCENARION == 2)
#warning IMX214 BINNING
#include "dtp_sensor_imx214_binning_mode15fps_db.h"
#warning IMX214 FULL
#include "dtp_sensor_imx_mode15fps_db.h"

#elif (TEST_SCENARION == 1)
#warning IMX214 BINNING
#include "dtp_sensor_imx214_binning_mode15fps_db.h"
#else
#warning IMX214 FULL
#include "dtp_sensor_imx_mode15fps_db.h"
#endif

/*
*==============================================================================================================
*               IMX214 timings
*               ---------------
*
*                div            mul                div           div           mul
*              --------       --------           --------      --------      --------
*             |        |     |        |         |        |    |        |    |        |
* In Freq ----| 0x0305 |-----| 0x0306 |----+----| 0x0301 |----| 0x0303 |----|   *4   |--- VTPXCK
*             |        |     | ox0307 |    |    |        |    |        |    |        |
*              --------       --------     |     --------      --------      --------
*                                          |
*                                          |       div           div           mul
*                                          |     --------      --------      --------
*                                          |    |        |    |        |    |        |
*                                          -----| 0x0309 |----| 0x030b |----|   *??  |--- OPPXCK
*                                               |        |    |        |    |        |
*                                                --------      --------      --------
*                                               CSI word len    num lanes??
*
* PPLN (pixel per line)   0x0342:0x0343
* LPFR (lines per frame)  0x0340:0x0341
*
* row_time[uSec] = PPLN / VTPXCK[MHz]
* frame_time[uSec] = row_time * LPFR
*==============================================================================================================
*               15 fps mode settings
*               ---------------------
*
*                 div            mul                div           div           mul
*               --------       --------           --------      --------      --------
*              |        | 8MHz|        | 1200MHz |        | 240|        | 60 |        |
*  24MHz   ----|   /3   |-----|  *150  |----+----|   /5   |----|    /4  |----|   *4   |--- VTPXCK = 240MHz
*              |        |     |        |    |    |        |    |        |    |        |
*               --------       --------     |     --------      --------      --------
*                                           |
*                                           |       div           div           mul
*                                           |     --------      --------      --------
*                                           |    |        | 120|        | 120|        |
*                                           -----|   10   |----|    1   |----|   *??  |--- OPPXCK = 120MHz
*                                                |        |    |        |    |        |
*                                                 --------      --------      --------
*                                                 RAW 10         2 lanes
* PPLN (pixel per line)   [0x0342:0x0343] = 0x1390 (5008)
* LPFR (lines per frame)  [0x0340:0x0341] = 0x0c58 (3160)
*
* row_time   = 5008 / 240 = 20.8666666667 uSec
* frame_time = 20.8666666667 * 3160 = 65,938.6666667 uSec
*
*
**==============================================================================================================
*               15 fps mode settings
*               ---------------------
*
*                 div            mul                div           div           mul
*               --------       --------           --------      --------      --------
*              |        | 6MHz|        | 1200MHz |        | 240|        | 60 |        |
*  12MHz   ----|   /2   |-----|  *200  |----+----|   /5   |----|    /4  |----|   *4   |--- VTPXCK = 240MHz
*              |        |     |        |    |    |        |    |        |    |        |
*               --------       --------     |     --------      --------      --------
*                                           |
*                                           |       div           div           mul
*                                           |     --------      --------      --------
*                                           |    |        | 120|        | 120|        |
*                                           -----|   10   |----|    1   |----|   *??  |--- OPPXCK = 120MHz
*                                                |        |    |        |    |        |
*                                                 --------      --------      --------
*                                                 RAW 10         2 lanes
* PPLN (pixel per line)   [0x0342:0x0343] = 0x1390 (5008)
* LPFR (lines per frame)  [0x0340:0x0341] = 0x0c58 (3160)
*
* row_time   = 5008 / 240 = 20.8666666667 uSec
* frame_time = 20.8666666667 * 3160 = 65,938.6666667 uSec
*
*==============================================================================================================
*/


static const hat_sensor_mode_t imx214_modes[] = {
#if (TEST_SCENARION == 1)
        { // imx214 mode 1
            {0, 0, 2104, 1560}, //hat_rect_t field_of_view;   // Sensor active view area in physical area [pixels]
            {2104, 1560},       //hat_size_t     active;          // Sensor active output pixels [pixels]
            {1, 1},             //hat_size_t     pixels;          // Equivalent of a digital pixel in physical pixels
            {1, 1},             //hat_size_t     aspect;          // Aspect ratio on 1 pixel
#if (FULL_30FPS == 1)
            {1.323, 30.0},
#else
            {1.323, 15.0},     //hat_range_float_t fps;          // sensor mode Min/Max fps
#endif
            {100, 100000},     //hat_range_t       exposure;        // Min & Max exposure time in [us]
            {
                .fmt.order.all = HAT_PORDBYR_R_Gr_Gb_B,
                .fmt.reserved0 = 0,
                .fmt.xydec     = HAT_XYDEC_NO,
                .fmt.uvpkd     = HAT_UVPKD_PACKED,
                .fmt.pkd       = HAT_PIXPKD_PACKED,
                .fmt.fmt       = HAT_PIXCOL_BAYER,
                .bpc           = 10,
                .compression   = HAT_COMPRESSION_NONE,
            }, // hat_pix_fmt_t  format;           // Color format and pattern
            // uint16                  rotate;     // 0, 90, 180, 270 - Rotate TODO should come from somewhere???
            0,             // uint16  v_flip;      // [ON/OFF]
            0,             // uint16  h_mirror;    // [ON/OFF]
            1.0,           // float   sensitivity; //  mode (binning/skipping/average) sensitivity against full mode
            240000000,    // uint32  pixel_clock; // In Hz
#if (FULL_30FPS == 1)
            20866/2,   //TODO       // uint32  row_time;    // [ns] - Readout time of line & blanking
            65938/2,  //TODO       // uint32  frame_time;  // [us] - Readout time of the frame & blanking
#else
            20866,   //TODO       // uint32  row_time;    // [ns] - Readout time of line & blanking
            65938,  //TODO       // uint32  frame_time;  // [us] - Readout time of the frame & blanking
#endif
            5008,          // uint32  ppln;        // Initial number PixelsPerLiNe
            3160,          // uint32  lpfr;        // Initial number LinesPerFRame
            1,             // uint32   num_bad_frames;  // Number of bad frames after mode switch
            imx214_binning_mode15fps_settings,
            ARR_SIZE(imx214_binning_mode15fps_settings),
            .pipeline_id = 0,       // Use pipeline with scale down
            .mipi_rx_cfg = {
#if (FULL_30FPS == 1)
                .lines = 4,
#else
                .lines = 2,
#endif
                .rate = 1200,
                .data_type = 0x2b,  // RAW 10
                .data_mode = 1,
            },
    },

#elif (TEST_SCENARION == 2) // CUSTOM USECASE

    { // imx214 mode Use pipeline for custom use case - still capture (see guzzi_camera3_enum_z_custom_usecase_selection_t)
        {208, 120, 4000, 3000}, //hat_rect_t field_of_view;   // Sensor active view area in physical area [pixels]
        {4000, 3000},       //hat_size_t     active;          // Sensor active output pixels [pixels]
        {1, 1},             //hat_size_t     pixels;          // Equivalent of a digital pixel in physical pixels
        {1, 1},             //hat_size_t     aspect;          // Aspect ratio on 1 pixel
        {1.323, 30.0},     //hat_range_float_t fps;          // sensor mode Min/Max fps
        {100, 1000000},     //hat_range_t       exposure;        // Min & Max exposure time in [us]
        {
            .fmt.order.all = HAT_PORDBYR_R_Gr_Gb_B,
            .fmt.reserved0 = 0,
            .fmt.xydec     = HAT_XYDEC_NO,
            .fmt.uvpkd     = HAT_UVPKD_PACKED,
            .fmt.pkd       = HAT_PIXPKD_PACKED,
            .fmt.fmt       = HAT_PIXCOL_BAYER,
            .bpc           = 10,
            .compression   = HAT_COMPRESSION_NONE,
        }, // hat_pix_fmt_t  format;           // Color format and pattern
        // uint16                  rotate;     // 0, 90, 180, 270 - Rotate TODO should come from somewhere???
        0,             // uint16  v_flip;      // [ON/OFF]
        0,             // uint16  h_mirror;    // [ON/OFF]
        1.0,           // float   sensitivity; //  mode (binning/skipping/average) sensitivity against full mode
        240000000,    // uint32  pixel_clock; // In Hz

        20866/2,   //TODO       // uint32  row_time;    // [ns] - Readout time of line & blanking
        65938/2,  //TODO       // uint32  frame_time;  // [us] - Readout time of the frame & blanking

        5008,          // uint32  ppln;        // Initial number PixelsPerLiNe
        3160,          // uint32  lpfr;        // Initial number LinesPerFRame
        1,             // uint32   num_bad_frames;  // Number of bad frames after mode switch
        imx214_mode15fps_settings,
        ARR_SIZE(imx214_mode15fps_settings),
        .pipeline_id = 0,       // Use pipeline for custom use case - still capture
        .mipi_rx_cfg = {
            .lines = 4,
            .rate = 1200,
            .data_type = 0x2b,  // RAW 10
            .data_mode = 1,
        },
    },
    { // imx214 mode Use pipeline for custom use case - video capture (see guzzi_camera3_enum_z_custom_usecase_selection_t)
        {208, 120, 4000, 3000}, //hat_rect_t field_of_view;   // Sensor active view area in physical area [pixels]
        {4000, 3000},       //hat_size_t     active;          // Sensor active output pixels [pixels]
        {1, 1},             //hat_size_t     pixels;          // Equivalent of a digital pixel in physical pixels
        {1, 1},             //hat_size_t     aspect;          // Aspect ratio on 1 pixel
        {1.323, 30.0},     //hat_range_float_t fps;          // sensor mode Min/Max fps
        {100, 33000},     //hat_range_t       exposure;        // Min & Max exposure time in [us]
        {
            .fmt.order.all = HAT_PORDBYR_R_Gr_Gb_B,
            .fmt.reserved0 = 0,
            .fmt.xydec     = HAT_XYDEC_NO,
            .fmt.uvpkd     = HAT_UVPKD_PACKED,
            .fmt.pkd       = HAT_PIXPKD_PACKED,
            .fmt.fmt       = HAT_PIXCOL_BAYER,
            .bpc           = 10,
            .compression   = HAT_COMPRESSION_NONE,
        }, // hat_pix_fmt_t  format;           // Color format and pattern
        // uint16                  rotate;     // 0, 90, 180, 270 - Rotate TODO should come from somewhere???
        0,             // uint16  v_flip;      // [ON/OFF]
        0,             // uint16  h_mirror;    // [ON/OFF]
        1.0,           // float   sensitivity; //  mode (binning/skipping/average) sensitivity against full mode
        240000000,    // uint32  pixel_clock; // In Hz

        20866/2,   //TODO       // uint32  row_time;    // [ns] - Readout time of line & blanking
        65938/2,  //TODO       // uint32  frame_time;  // [us] - Readout time of the frame & blanking

        5008,          // uint32  ppln;        // Initial number PixelsPerLiNe
        3160,          // uint32  lpfr;        // Initial number LinesPerFRame
        1,             // uint32   num_bad_frames;  // Number of bad frames after mode switch
        imx214_mode15fps_settings,
        ARR_SIZE(imx214_mode15fps_settings),
        .pipeline_id = 1,       // Use pipeline for custom use case - video capture
        .mipi_rx_cfg = {
            .lines = 4,
            .rate = 1200,
            .data_type = 0x2b,  // RAW 10
            .data_mode = 1,
        },
    },

    { // imx214 mode Use pipeline for custom use case - low power video capture (see guzzi_camera3_enum_z_custom_usecase_selection_t)
        {104, 60, 2000, 1500}, //hat_rect_t field_of_view;   // Sensor active view area in physical area [pixels]
        {2000, 1500},       //hat_size_t     active;          // Sensor active output pixels [pixels]
        {1, 1},             //hat_size_t     pixels;          // Equivalent of a digital pixel in physical pixels
        {1, 1},             //hat_size_t     aspect;          // Aspect ratio on 1 pixel
        {1.323, 30.0},     //hat_range_float_t fps;          // sensor mode Min/Max fps
        {100, 33000},     //hat_range_t       exposure;        // Min & Max exposure time in [us]
        {
            .fmt.order.all = HAT_PORDBYR_R_Gr_Gb_B,
            .fmt.reserved0 = 0,
            .fmt.xydec     = HAT_XYDEC_NO,
            .fmt.uvpkd     = HAT_UVPKD_PACKED,
            .fmt.pkd       = HAT_PIXPKD_PACKED,
            .fmt.fmt       = HAT_PIXCOL_BAYER,
            .bpc           = 10,
            .compression   = HAT_COMPRESSION_NONE,
        }, // hat_pix_fmt_t  format;           // Color format and pattern
        // uint16                  rotate;     // 0, 90, 180, 270 - Rotate TODO should come from somewhere???
        0,             // uint16  v_flip;      // [ON/OFF]
        0,             // uint16  h_mirror;    // [ON/OFF]
        1.0,           // float   sensitivity; //  mode (binning/skipping/average) sensitivity against full mode
        240000000,    // uint32  pixel_clock; // In Hz

        20866/2,   //TODO       // uint32  row_time;    // [ns] - Readout time of line & blanking
        65938/2,  //TODO       // uint32  frame_time;  // [us] - Readout time of the frame & blanking
        5008,          // uint32  ppln;        // Initial number PixelsPerLiNe
        3160,          // uint32  lpfr;        // Initial number LinesPerFRame
        1,             // uint32   num_bad_frames;  // Number of bad frames after mode switch
        imx214_binning_mode15fps_settings,
        ARR_SIZE(imx214_binning_mode15fps_settings),
        .pipeline_id = 2,       // Use pipeline with scale down low power video capture (see guzzi_camera3_enum_z_custom_usecase_selection_t)
        .mipi_rx_cfg = {
            .lines = 4,
            .rate = 1200,
            .data_type = 0x2b,  // RAW 10
            .data_mode = 1,
        },
    },
#else
    { // imx214 mode 1
        {0, 0, 4208, 3120}, //hat_rect_t field_of_view;   // Sensor active view area in physical area [pixels]
        {4208, 3120},       //hat_size_t     active;          // Sensor active output pixels [pixels]
        {1, 1},             //hat_size_t     pixels;          // Equivalent of a digital pixel in physical pixels
        {1, 1},             //hat_size_t     aspect;          // Aspect ratio on 1 pixel
#if (FULL_30FPS == 1)
        {1.323, 30.0},
#else
        {1.323, 15.0},     //hat_range_float_t fps;          // sensor mode Min/Max fps
#endif
        {100, 100000},     //hat_range_t       exposure;        // Min & Max exposure time in [us]
        {
            .fmt.order.all = HAT_PORDBYR_R_Gr_Gb_B,
            .fmt.reserved0 = 0,
            .fmt.xydec     = HAT_XYDEC_NO,
            .fmt.uvpkd     = HAT_UVPKD_PACKED,
            .fmt.pkd       = HAT_PIXPKD_PACKED,
            .fmt.fmt       = HAT_PIXCOL_BAYER,
            .bpc           = 10,
            .compression   = HAT_COMPRESSION_NONE,
        }, // hat_pix_fmt_t  format;           // Color format and pattern
        // uint16                  rotate;     // 0, 90, 180, 270 - Rotate TODO should come from somewhere???
        0,             // uint16  v_flip;      // [ON/OFF]
        0,             // uint16  h_mirror;    // [ON/OFF]
        1.0,           // float   sensitivity; //  mode (binning/skipping/average) sensitivity against full mode
        240000000,    // uint32  pixel_clock; // In Hz
#if (FULL_30FPS == 1)
        20866/2,   //TODO       // uint32  row_time;    // [ns] - Readout time of line & blanking
        65938/2,  //TODO       // uint32  frame_time;  // [us] - Readout time of the frame & blanking
#else
        20866,   //TODO       // uint32  row_time;    // [ns] - Readout time of line & blanking
        65938,  //TODO       // uint32  frame_time;  // [us] - Readout time of the frame & blanking
#endif
        5008,          // uint32  ppln;        // Initial number PixelsPerLiNe
        3160,          // uint32  lpfr;        // Initial number LinesPerFRame
        1,             // uint32   num_bad_frames;  // Number of bad frames after mode switch
        imx214_mode15fps_settings,
        ARR_SIZE(imx214_mode15fps_settings),
        .pipeline_id = 1,       // Use pipeline with scale down
        .mipi_rx_cfg = {
#if (FULL_30FPS == 1)
            .lines = 4,
#else
            .lines = 2,
#endif
            .rate = 1200,
            .data_type = 0x2b,  // RAW 10
            .data_mode = 1,
        },
    },
#endif // TEST_SCENARION
};

static const hat_dtp_sensor_desc_t dtp_sensor_imx214_db =
{ // IMX214 sensor
    .sensor_id          = DTP_SEN_IMX214,
    .sensor_name        = "IMX214 sensor",
    .sensor_features    = {
        {4208, 3120},         //hat_size_t          array_size;     // Physical area (valid + black pixels)
        {0, 0, 4208, 3120},   //hat_rect_t          valid_pixels;   // Sensor full area in physical area
         100,                 //uint32              base_ISO;       // real sensor sensitivity with gain = 1 [U32Q16]
        {1.0, 1.0, 16.0, 0.1}, //hat_bounds_float_t again;          // min/max sensor gain analog
        {1.0, 1.0,  1.0, 0.1},// hat_bounds_float_t dgain;          // min/max sensor gain dnalog
        1,                    //uint32              exp_gain_delay; // exposure to gain delay [frames]
        1,                    //uint32              global_delay;   // delay to output frame with applied parameters [frames]
        {   //const hat_sensor_operations_t operations;
            .pwr_off = {
                ARR_SIZE(imx214_power_off_seq),
                imx214_power_off_seq,
            },
            .pwr_on = {
                ARR_SIZE(imx214_power_on_seq),
                imx214_power_on_seq,
            },
            .init = {
                [SENSOR_INIT_1] = {
                    ARR_SIZE(imx214_24MHz_init_seq),
                    imx214_24MHz_init_seq,
                    24000000,
                },
                [SENSOR_INIT_2] = {
                    ARR_SIZE(imx214_12MHz_init_seq),
                    imx214_12MHz_init_seq,
                    12000000,
                },
                [SENSOR_INIT_3] = {},
            },
            .stream_off = {
                ARR_SIZE(imx214_stream_off_seq),
                imx214_stream_off_seq,
            },
            .stream_on = {
                ARR_SIZE(imx214_stream_on_seq),
                imx214_stream_on_seq,
            },
        },
        {
            ARR_SIZE(imx214_modes), // uint32             num;   // number of sensor modes
            imx214_modes // list with sensor modes
        },
        1, //uint32  powerup_bad_frames;  // Number of bad frames after powerup
        1, //uint8 temp_sensor_supp;    // If sensor support temperature sensor - 1. If no - 0.
        //HAL3 specific data.
        {100, 1600}, //int32                   sensitivity_range[2];     // Range of valid sensitivities Min <= 100, Max >= 1600
        {.all = HAT_PORDBYR_R_Gr_Gb_B}, //hat_pix_order_t         color_filter_arrangement; // Arrangement of color filters on sensor TODO we have same info in sens mode descriptor
        {100000, 100000000},//TODO, //int64  exposure_time_range[2];  // Range of valid exposure times in [nanoseconds]
        100000000,//TODO, //int64  max_frame_duration; // Maximum possible frame duration in [nanoseconds].
        {4.71296, 3.4944}, //float                  physical_size[2];         // The physical dimensions of the full pixel array in [mm]
        1023, //int32                   white_level;              // Maximum raw value output by sensor
        {1, 1}, //hat_fract_t             base_gain_factor;         // Gain factor from electrons to raw units when ISO=100
        {42, 42, 42, 42}, //int32                   black_level_pattern[4];   // A fixed black level offset for each of the Bayer mosaic channels.
        {{0, 0}}, //hat_fract_t             calib_transform_1[9];     // Per-device calibration on top of color space transform 1
        {{0, 0}}, //hat_fract_t             calib_transform_2[9];     // Per-device calibration on top of color space transform 2
        {{0, 0}}, //hat_fract_t             color_transform_1[9];     // Linear mapping from XYZ (D50) color space to reference linear sensor color.
        {{0, 0}}, //hat_fract_t             color_transform_2[9];     // Linear mapping from XYZ (D50) color space to reference linear sensor color.
        {{0, 0}}, //hat_fract_t             forward_matrix_1[9];      // Used by DNG for better WB adaptation.
        {{0, 0}}, //hat_fract_t             forward_matrix_2[9];      // Used by DNG for better WB adaptation.
        800, //int32                   max_analog_sensitivity;    // Maximum sensitivity that is implemented purely through analog gain.
        {0.0}, //float                  noise_model_coef[2];      // Estimation of sensor noise characteristics.
        0, //int32                   orientation;              // degrees clockwise rotation TODO we have same info in sens mode descriptor
        REF_ILL_NONE, //reference_illuminant_t  reference_illuminant_1;   // Light source used to define transform 1.
        REF_ILL_NONE //reference_illuminant_t  reference_illuminant_2;   // Light source used to define transform 2.
    }
};

#define DTP_SENSOR_IMX214_DB_SIZE ( \
    sizeof(dtp_sensor_imx214_db) \
)

#endif //__DTP_SENSOR_IMX214_DB_H__
