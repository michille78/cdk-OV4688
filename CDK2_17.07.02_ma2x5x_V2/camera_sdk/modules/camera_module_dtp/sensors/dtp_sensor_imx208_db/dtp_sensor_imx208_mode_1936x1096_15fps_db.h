
//IMX208
//INCK:24MHz
//MIP:2lane
//Data Rate:811Mbps/lane
//Full(1936x1096),15fps,2lane
//H : 1936
//V : 1096
static const hat_cm_socket_command_entry_t imx208_mode1936x1096_15fps_settings[] = {
    SENS_INTENT_CMD_CCI_I2C_WR(0x0103, 0x01), // SW Reset (from Lucian)
    SENS_INTENT_CMD_CCI_I2C_WR(0x0305, 0x04),
    SENS_INTENT_CMD_CCI_I2C_WR(0x0307, 0x87),
    SENS_INTENT_CMD_CCI_I2C_WR(0x303C, 0x4B),
    SENS_INTENT_CMD_CCI_I2C_WR(0x30A4, 0x01),  // 0x02->60fps, 0x00->30fps, 0x01->15fps


    SENS_INTENT_CMD_CCI_I2C_WR(0x0112, 0x0A
                           /*0x0113*/, 0x0A),
    SENS_INTENT_CMD_CCI_I2C_WR(0x0340, 0x04
                           /*0x0341*/, 0xB0
                           /*0x0342*/, 0x04
                           /*0x0343*/, 0x64
                           /*0x0344*/, 0x00
                           /*0x0345*/, 0x00
                           /*0x0346*/, 0x00
                           /*0x0347*/, 0x00
                           /*0x0348*/, 0x07
                           /*0x0349*/, 0x8F
                           /*0x034A*/, 0x04
                           /*0x034B*/, 0x47
                           /*0x034C*/, 0x07
                           /*0x034D*/, 0x90
                           /*0x034E*/, 0x04
                           /*0x034F*/, 0x48),

    SENS_INTENT_CMD_CCI_I2C_WR(0x0381, 0x01),
    SENS_INTENT_CMD_CCI_I2C_WR(0x0383, 0x01),
    SENS_INTENT_CMD_CCI_I2C_WR(0x0385, 0x01),
    SENS_INTENT_CMD_CCI_I2C_WR(0x0387, 0x01),
    SENS_INTENT_CMD_CCI_I2C_WR(0x3048, 0x00),
    SENS_INTENT_CMD_CCI_I2C_WR(0x304E, 0x0A),
    SENS_INTENT_CMD_CCI_I2C_WR(0x3050, 0x01),
    SENS_INTENT_CMD_CCI_I2C_WR(0x309B, 0x00),
    SENS_INTENT_CMD_CCI_I2C_WR(0x30D5, 0x00),
    SENS_INTENT_CMD_CCI_I2C_WR(0x3301, 0x00),
    SENS_INTENT_CMD_CCI_I2C_WR(0x3318, 0x61),

    SENS_INTENT_CMD_CCI_I2C_WR(0x0202, 0x01),
    SENS_INTENT_CMD_CCI_I2C_WR(0x0203, 0x90),
    SENS_INTENT_CMD_CCI_I2C_WR(0x0205, 0x00),

    SENS_INTENT_CMD_CCI_I2C_WR(0x3343,0x03), // Manufacturer prohibited !!?
    SENS_INTENT_CMD_CCI_I2C_WR(0x30f6,0x00), // Manufacturer prohibited !!?
    CMD_ENTRY_END(),
};
