/* =============================================================================
* Copyright (c) 2013-2015 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file dtp_sensor_imx208_db.h
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#ifndef __DTP_SENSOR_IMX208_DB_H__
#define __DTP_SENSOR_IMX208_DB_H__

// ============================ IMX208 camera module =========================

static const hat_cm_socket_command_entry_t imx208_power_off_seq[] = {
    //SENS_INTENT_ACTION(SEN_RSRC_XSHTDWN, HAT_HW_RES_OFF),
    //SENS_INTENT_ACTION(SEN_RSRC_POWER_2, HAT_HW_RES_OFF),
    SENS_INTENT_ACTION(SEN_RSRC_POWER_1, HAT_HW_RES_OFF),
    SENS_DELAY(1000),
    CMD_ENTRY_DUMMY(),
    CMD_ENTRY_END(),
};

static const hat_cm_socket_command_entry_t imx208_power_on_seq[] = {
    //SENS_INTENT_ACTION(SEN_RSRC_XSHTDWN, HAT_HW_RES_ON),
    //SENS_INTENT_ACTION(SEN_RSRC_POWER_2, HAT_HW_RES_OFF),
    SENS_DELAY(1000),
    SENS_INTENT_ACTION(SEN_RSRC_POWER_1, HAT_HW_RES_ON),
    SENS_INTENT_ACTION(SEN_RSRC_POWER_2, HAT_HW_RES_ON),
    SENS_INTENT_ACTION(SEN_RSRC_CLOCK, HAT_HW_RES_ON),  // ENABLE CLOCK
    SENS_INTENT_ACTION(SEN_RSRC_XSHTDWN, HAT_HW_RES_OFF), //XSHDN#
    SENS_DELAY(1000),
    SENS_INTENT_ACTION(SEN_RSRC_XSHTDWN, HAT_HW_RES_ON), //XSHDN#
    CMD_ENTRY_DUMMY(),
    CMD_ENTRY_END(),
};

//
static const hat_cm_socket_command_entry_t imx208_init_seq[] = {

    CMD_ENTRY_END(),
};

static const hat_cm_socket_command_entry_t imx208_stream_off_seq[] = {
    SENS_INTENT_CMD_CCI_I2C_WR(0x0100, 0),
    CMD_ENTRY_END(),
};

static const hat_cm_socket_command_entry_t imx208_stream_on_seq[] = {
    SENS_INTENT_CMD_CCI_I2C_WR(0x0100, 1),
    CMD_ENTRY_END(),
};




static const hat_cm_socket_command_entry_t imx208_mode0_settings[] = {
    CMD_ENTRY_DUMMY(),
    CMD_ENTRY_END(),
};

static const hat_cm_socket_command_entry_t imx208_mode1_settings[] = {
    CMD_ENTRY_DUMMY(),
    CMD_ENTRY_END(),
};

#include "dtp_sensor_imx208_mode60fps_db.h"
#include "dtp_sensor_imx208_mode_1936x1096_30fps_db.h"
#include "dtp_sensor_imx208_mode_1936x1096_15fps_db.h"
/*
*==============================================================================================================
*               IMX208 timings
*               ---------------
*               mode0
*
* PLCK [MHz] = INCK[MHz] * PreDivider ratio setting[0x0305] × PLL multiplier setting[0x0307] × Post Divider ratio setting[0x30A4]
*
* LPFR (lines per frame)  [0x0340:0x0341] = 0x04B0 = 1200
* PPLN (pixel per line)   [0x0342:0x0343] = 0x0464 = 1124
*
* PLCK[Hz] = 24000000 * (1/4) * (135) * (1/4) = 202500000 Hz
*
* row_time[10ns] = ((PPLN * 100000) / (PLCK / 1000))
* row_time[10ns] = ((1124 * 100000) / (202500000 / 1000)) = 555.061728395
* row_time[ns] = 55.5061728395
* row_time[us] = 55506.1728395
*
* frame_time[uSec] = row_time * LPFR
* frame_time[uSec] = (555.061728395/10) * 1200 = 66607.4074074
*
*==============================================================================================================
*/


static const hat_sensor_mode_t imx208_modes[] = {
    { // imx208 mode 0
        {0, 12, 1936, 1096}, //hat_rect_t field_of_view;      // Sensor active view area in physical area [pixels]
        {1936, 1096},       //hat_size_t     active;          // Sensor active output pixels [pixels]
        {1, 1},             //hat_size_t     pixels;          // Equivalent of a digital pixel in physical pixels
        {1, 1},             //hat_size_t     aspect;          // Aspect ratio on 1 pixel
        {1.323, 15.0},     //hat_range_float_t fps;          // sensor mode Min/Max fps
        {100, 100000},     //hat_range_t       exposure;        // Min & Max exposure time in [us]
        {
            .fmt.order.all = HAT_PORDBYR_R_Gr_Gb_B,
            .fmt.reserved0 = 0,
            .fmt.xydec     = HAT_XYDEC_NO,
            .fmt.uvpkd     = HAT_UVPKD_PACKED,
            .fmt.pkd       = HAT_PIXPKD_PACKED,
            .fmt.fmt       = HAT_PIXCOL_BAYER,
            .bpc           = 10,
            .compression   = HAT_COMPRESSION_NONE,
        }, // hat_pix_fmt_t  format;           // Color format and pattern
        // uint16                  rotate;     // 0, 90, 180, 270 - Rotate TODO should come from somewhere???
        0,             // uint16  v_flip;      // [ON/OFF]
        0,             // uint16  h_mirror;    // [ON/OFF]
        1.0,           // float   sensitivity; //  mode (binning/skipping/average) sensitivity against full mode
        240000000,    // uint32  pixel_clock; // In Hz
        55506,        // uint32  row_time;    // [ns] - Readout time of line & blanking
        66607,        // uint32  frame_time;  // [us] - Readout time of the frame & blanking
        1124,          // uint32  ppln;        // Initial number PixelsPerLiNe
        1200,          // uint32  lpfr;        // Initial number LinesPerFRame
        1,             // uint32   num_bad_frames;  // Number of bad frames after mode switch
        imx208_mode1936x1096_15fps_settings,
        ARR_SIZE(imx208_mode1936x1096_15fps_settings),
        .pipeline_id = 0,       // Don't use pipeline with scale down
        .mipi_rx_cfg = {
            .lines = 2,
            .rate = 811,
            .data_type = 0x2b,  // RAW 10
            .data_mode = 1,
        },
    },
    { // imx208 mode 1
        {0, 0, 1936, 1096}, //hat_rect_t field_of_view;   // Sensor active view area in physical area [pixels]
        {1936, 1096},       //hat_size_t     active;          // Sensor active output pixels [pixels]
        {1, 1},             //hat_size_t     pixels;          // Equivalent of a digital pixel in physical pixels
        {1, 1},             //hat_size_t     aspect;          // Aspect ratio on 1 pixel
        {1.323, 30.0},     //hat_range_float_t fps;          // sensor mode Min/Max fps
        {100, 100000},     //hat_range_t       exposure;        // Min & Max exposure time in [us]
        {
            .fmt.order.all = HAT_PORDBYR_R_Gr_Gb_B,
            .fmt.reserved0 = 0,
            .fmt.xydec     = HAT_XYDEC_NO,
            .fmt.uvpkd     = HAT_UVPKD_PACKED,
            .fmt.pkd       = HAT_PIXPKD_PACKED,
            .fmt.fmt       = HAT_PIXCOL_BAYER,
            .bpc           = 10,
            .compression   = HAT_COMPRESSION_NONE,
        }, // hat_pix_fmt_t  format;           // Color format and pattern
        // uint16                  rotate;     // 0, 90, 180, 270 - Rotate TODO should come from somewhere???
        0,             // uint16  v_flip;      // [ON/OFF]
        0,             // uint16  h_mirror;    // [ON/OFF]
        1.0,           // float   sensitivity; //  mode (binning/skipping/average) sensitivity against full mode
        240000000,    // uint32  pixel_clock; // In Hz
        20866,   //TODO       // uint32  row_time;    // [ns] - Readout time of line & blanking
        65938,  //TODO       // uint32  frame_time;  // [us] - Readout time of the frame & blanking
        2248,          // uint32  ppln;        // Initial number PixelsPerLiNe
        1200,          // uint32  lpfr;        // Initial number LinesPerFRame
        1,             // uint32   num_bad_frames;  // Number of bad frames after mode switch
        imx208_mode1936x1096_30fps_settings,
        ARR_SIZE(imx208_mode1936x1096_30fps_settings),
        .pipeline_id = 0,       // Don't use pipeline with scale down
        .mipi_rx_cfg = {
            .lines = 2,
            .rate = 811,
            .data_type = 0x2b,  // RAW 10
            .data_mode = 1,
        },
    },
};

static const hat_dtp_sensor_desc_t dtp_sensor_imx208_db =
{ // IMX208 sensor
    .sensor_id          = DTP_SEN_IMX208,
    .sensor_name        = "IMX208 sensor",
    .sensor_features    = {
        {1936, 1096},         //hat_size_t          array_size;     // Physical area (valid + black pixels)
        {0, 0, 1936, 1096},   //hat_rect_t          valid_pixels;   // Sensor full area in physical area
         100,                 //uint32              base_ISO;       // real sensor sensitivity with gain = 1 [U32Q16]
        {1.0, 1.0, 16.0, 0.1}, //hat_bounds_float_t again;          // min/max sensor gain analog
        {1.0, 1.0,  1.0, 0.1},// hat_bounds_float_t dgain;          // min/max sensor gain dnalog
        0,                    //uint32              exp_gain_delay; // exposure to gain delay [frames]
        2,                    //uint32              global_delay;   // delay to output frame with applied parameters [frames]
        {   //const hat_sensor_operations_t operations;
            .pwr_off = {
                ARR_SIZE(imx208_power_off_seq),
                imx208_power_off_seq,
            },
            .pwr_on = {
                ARR_SIZE(imx208_power_on_seq),
                imx208_power_on_seq,
            },
            .init = {
                [SENSOR_INIT_1] = {
                    ARR_SIZE(imx208_init_seq),
                    imx208_init_seq,
                },
                [SENSOR_INIT_2] = {},
                [SENSOR_INIT_3] = {},
            },
            .stream_off = {
                ARR_SIZE(imx208_stream_off_seq),
                imx208_stream_off_seq,
            },
            .stream_on = {
                ARR_SIZE(imx208_stream_on_seq),
                imx208_stream_on_seq,
            },
        },
        {
            ARR_SIZE(imx208_modes), // uint32             num;   // number of sensor modes
            imx208_modes // list with sensor modes
        },
        1, //uint32  powerup_bad_frames;  // Number of bad frames after powerup
        1, //uint8 temp_sensor_supp;    // If sensor support temperature sensor - 1. If no - 0.
        //HAL3 specific data.
        {100, 1600}, //int32                   sensitivity_range[2];     // Range of valid sensitivities Min <= 100, Max >= 1600
        {.all = HAT_PORDBYR_R_Gr_Gb_B}, //hat_pix_order_t         color_filter_arrangement; // Arrangement of color filters on sensor TODO we have same info in sens mode descriptor
        {100000, 100000000},//TODO, //int64  exposure_time_range[2];  // Range of valid exposure times in [nanoseconds]
        100000000,//TODO, //int64  max_frame_duration; // Maximum possible frame duration in [nanoseconds].
        {4.71296, 3.4944}, //float                  physical_size[2];         // The physical dimensions of the full pixel array in [mm]
        1023, //int32                   white_level;              // Maximum raw value output by sensor
        {1, 1}, //hat_fract_t             base_gain_factor;         // Gain factor from electrons to raw units when ISO=100
        {42, 42, 42, 42}, //int32                   black_level_pattern[4];   // A fixed black level offset for each of the Bayer mosaic channels.
        {{0, 0}}, //hat_fract_t             calib_transform_1[9];     // Per-device calibration on top of color space transform 1
        {{0, 0}}, //hat_fract_t             calib_transform_2[9];     // Per-device calibration on top of color space transform 2
        {{0, 0}}, //hat_fract_t             color_transform_1[9];     // Linear mapping from XYZ (D50) color space to reference linear sensor color.
        {{0, 0}}, //hat_fract_t             color_transform_2[9];     // Linear mapping from XYZ (D50) color space to reference linear sensor color.
        {{0, 0}}, //hat_fract_t             forward_matrix_1[9];      // Used by DNG for better WB adaptation.
        {{0, 0}}, //hat_fract_t             forward_matrix_2[9];      // Used by DNG for better WB adaptation.
        800, //int32                   max_analog_sensitivity;    // Maximum sensitivity that is implemented purely through analog gain.
        {0.0}, //float                  noise_model_coef[2];      // Estimation of sensor noise characteristics.
        0, //int32                   orientation;              // degrees clockwise rotation TODO we have same info in sens mode descriptor
        REF_ILL_NONE, //reference_illuminant_t  reference_illuminant_1;   // Light source used to define transform 1.
        REF_ILL_NONE //reference_illuminant_t  reference_illuminant_2;   // Light source used to define transform 2.
    }
};

#define DTP_SENSOR_IMX208_DB_SIZE ( \
    sizeof(dtp_sensor_imx208_db) \
)

#endif //__DTP_SENSOR_IMX208_DB_H__
