/* =============================================================================
* Copyright (c) 2013-2015 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file dtp_sensor_ov7251_db.h
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 29-Feb-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#ifndef __DTP_SENSOR_OV7251_DB_H__
#define __DTP_SENSOR_OV7251_DB_H__

// ============================ OV7251 camera module =========================

#define OV7251_1_DEV_ADDR   0xD8
#define OV7251_2_DEV_ADDR   0xDA

static const hat_cm_socket_command_entry_t ov7251_power_off_seq[] =
{
    SENS_INTENT_CMD_CCI_I2C_WR(0x0103,  0x01),  // Soft reset
    SENS_DELAY(1000),
    SENS_INTENT_ACTION(SEN_RSRC_XSHTDWN, HAT_HW_RES_OFF),
    SENS_INTENT_ACTION(SEN_RSRC_POWER_1, HAT_HW_RES_OFF),
    SENS_DELAY(1000),
    CMD_ENTRY_DUMMY(),
    CMD_ENTRY_END(),
};

static const hat_cm_socket_command_entry_t ov7251_1_power_on_seq[] = {
    SENS_DELAY(1000),
    SENS_INTENT_ACTION(SEN_RSRC_POWER_1, HAT_HW_RES_ON),
    SENS_INTENT_ACTION(SEN_RSRC_CLOCK, HAT_HW_RES_ON),      // ENABLE CLOCK
    SENS_INTENT_ACTION(SEN_RSRC_XSHTDWN, HAT_HW_RES_OFF),   // XSHDN#
    SENS_DELAY(1000),
    SENS_INTENT_ACTION(SEN_RSRC_XSHTDWN, HAT_HW_RES_ON),    // XSHDN#
    SENS_DELAY(1000),
    SENS_INTENT_CMD_CCI_I2C_CONFIG((0xC0>>1), 2, 7), // Set default address
    SENS_INTENT_CMD_CCI_I2C_WR(0x302B, OV7251_1_DEV_ADDR),   // Change SCCB ID 1 to slave OV7251_1_DEV_ADDR
    SENS_INTENT_CMD_CCI_I2C_WR(0x303B, 0x02),   // Set sccb_id2_nack_en
    SENS_INTENT_CMD_CCI_I2C_WR(0x0109, OV7251_1_DEV_ADDR),   // Change SCCB ID 0 to slave OV7251_1_DEV_ADDR
    SENS_INTENT_CMD_CCI_I2C_CONFIG((OV7251_1_DEV_ADDR>>1), 2, 7), // Config I2C device address to OV7251_1_DEV_ADDR
    SENS_DELAY(1000),
    CMD_ENTRY_END(),
};

static const hat_cm_socket_command_entry_t ov7251_2_power_on_seq[] = {
    SENS_DELAY(1000),
    SENS_INTENT_ACTION(SEN_RSRC_POWER_1, HAT_HW_RES_ON),
    SENS_INTENT_ACTION(SEN_RSRC_CLOCK, HAT_HW_RES_ON),      // ENABLE CLOCK
    SENS_INTENT_ACTION(SEN_RSRC_XSHTDWN, HAT_HW_RES_OFF),   // XSHDN#
    SENS_DELAY(1000),
    SENS_INTENT_ACTION(SEN_RSRC_XSHTDWN, HAT_HW_RES_ON),    // XSHDN#
    SENS_DELAY(1000),
    SENS_INTENT_CMD_CCI_I2C_CONFIG((0xC0>>1), 2, 7), // Set default address
    SENS_INTENT_CMD_CCI_I2C_WR(0x302B, OV7251_2_DEV_ADDR),   // Change SCCB ID 1 to slave OV7251_2_DEV_ADDR
    SENS_INTENT_CMD_CCI_I2C_WR(0x303B, 0x02),   // Set sccb_id2_nack_en
    SENS_INTENT_CMD_CCI_I2C_WR(0x0109, OV7251_2_DEV_ADDR),   // Change SCCB ID 0 to slave OV7251_2_DEV_ADDR
    SENS_INTENT_CMD_CCI_I2C_CONFIG((OV7251_2_DEV_ADDR>>1), 2, 7), // Config I2C device address to OV7251_2_DEV_ADDR
    SENS_DELAY(1000),
    CMD_ENTRY_END(),
};

static const hat_cm_socket_command_entry_t ov7251_init_seq[] = {
    CMD_ENTRY_END(),
};

static const hat_cm_socket_command_entry_t ov7251_stream_off_seq[] = {
    SENS_INTENT_CMD_CCI_I2C_WR(0x0100, 0),
    CMD_ENTRY_END(),
};

static const hat_cm_socket_command_entry_t ov7251_stream_on_seq[] = {
    SENS_INTENT_CMD_CCI_I2C_WR(0x0100, 1),
    CMD_ENTRY_END(),
};

static const hat_cm_socket_command_entry_t ov7251_25fps_settings[] = {
    SENS_INTENT_CMD_CCI_I2C_WR(0x0100,  0x00),  // Stream off

    //=================== System Control Registers
    SENS_INTENT_CMD_CCI_I2C_WR(0x3005,  0x00),  // Strobe output Disable
                                                // PWM output Disable
                                                // VSYNC output Disable
                                                // SIOD output Disable
    SENS_INTENT_CMD_CCI_I2C_WR(0x3012,  0xC0,   // MIPI Phy
                             /*0x3013*/ 0xD2,   // MIPI Phy
                             /*0x3014*/ 0x04),  // MIPI SC Ctrl: Enable
    SENS_INTENT_CMD_CCI_I2C_WR(0x3016,  0xF0,   //*********
                             /*0x3017*/ 0xF0,   //*** SC Clk Rst 0-3
                             /*0x3018*/ 0xF0),  //*********
    SENS_INTENT_CMD_CCI_I2C_WR(0x301A,  0xF0,   //*********
                             /*0x301B*/ 0xF0,   //*** SC Clk Rst 4-6
                             /*0x301C*/ 0xF0),  //*********
    SENS_INTENT_CMD_CCI_I2C_WR(0x3023,  0x07),  // SC Low Pwr Ctrl
    SENS_INTENT_CMD_CCI_I2C_WR(0x3037,  0xF0),  // SC R37
    SENS_INTENT_CMD_CCI_I2C_WR(0x3098,  0x04,   // PLL2 Pre Div
                             /*0x3099*/ 0x28,   // PLL2 Mul
                             /*0x309A*/ 0x05,   // PLL2 Sys Div
                             /*0x309B*/ 0x04),  // PLL2 ADC Div
    SENS_INTENT_CMD_CCI_I2C_WR(0x30B0,  0x0A,   // PLL1 Pix Div
                             /*0x30B1*/ 0x01),  // PLL1 Sys Clk Div
    SENS_INTENT_CMD_CCI_I2C_WR(0x30B3,  0x64,   // PLL1 Mul
                             /*0x30B4*/ 0x03,   // PLL1 Pre Div
                             /*0x30B5*/ 0x05),  // PLL1 Mipi Div

    //=================== SCCB and Group Hold Registers
    SENS_INTENT_CMD_CCI_I2C_WR(0x3106,  0x12),

    //=================== Manual/Auto Exposure/Gain Control Registers
    SENS_INTENT_CMD_CCI_I2C_WR(0x3500,  0x00,   // Exposure [19:16]
                             /*0x3501*/ 0x1F,   // Exposure [15:8]
                             /*0x3502*/ 0x80,   // Exposure [7:0]
                             /*0x3503*/ 0x07),  // Exp/Gain manual Enable

    SENS_INTENT_CMD_CCI_I2C_WR(0x3509,  0x10),  // Exposure gain convert
                                                // Use real gain [0x350B] as linear gain
    SENS_INTENT_CMD_CCI_I2C_WR(0x350B,  0x10),  // Gain = 0x350B / 0x10 for linear

    //=================== Analog Control Registers
    SENS_INTENT_CMD_CCI_I2C_WR(0x3600,  0x1C),
    SENS_INTENT_CMD_CCI_I2C_WR(0x3602,  0x62),
    SENS_INTENT_CMD_CCI_I2C_WR(0x3620,  0xB7),
    SENS_INTENT_CMD_CCI_I2C_WR(0x3622,  0x04),
    SENS_INTENT_CMD_CCI_I2C_WR(0x3626,  0x21,
                             /*0x3627*/ 0x30),
    SENS_INTENT_CMD_CCI_I2C_WR(0x3634,  0x41),
    SENS_INTENT_CMD_CCI_I2C_WR(0x3636,  0x00), // Enable internal regulator
    SENS_INTENT_CMD_CCI_I2C_WR(0x3662,  0x01),
    SENS_INTENT_CMD_CCI_I2C_WR(0x3664,  0xF0),
    SENS_INTENT_CMD_CCI_I2C_WR(0x3669,  0x1A,
                             /*0x366A*/ 0x00,
                             /*0x366B*/ 0x50),

    //=================== Sensor Control Registers
    SENS_INTENT_CMD_CCI_I2C_WR(0x3705,  0xC1),
    SENS_INTENT_CMD_CCI_I2C_WR(0x3709,  0x40),
    SENS_INTENT_CMD_CCI_I2C_WR(0x373C,  0x08),
    SENS_INTENT_CMD_CCI_I2C_WR(0x3742,  0x00),
    SENS_INTENT_CMD_CCI_I2C_WR(0x3788,  0x00),
    SENS_INTENT_CMD_CCI_I2C_WR(0x37A8,  0x01,
                             /*0x37A9*/ 0xC0),

//100fps = 0x0204 LPFR
// 25fps = 0x0810 LPFR
    //=================== Timing Control Registers
    SENS_INTENT_CMD_CCI_I2C_WR(0x3800,  0x00,   // Array Horizontal Start Point MSB
                             /*0x3801*/ 0x04,   // Array Horizontal Start Point LSB
                             /*0x3802*/ 0x00,   // Array Vertical Start Point MSB
                             /*0x3803*/ 0x04,   // Array Vertical Start Point LSB
                             /*0x3804*/ 0x02,   // Array Horizontal End Point MSB
                             /*0x3805*/ 0x8B,   // Array Horizontal End Point LSB
                             /*0x3806*/ 0x01,   // Array Vertical End Point MSB
                             /*0x3807*/ 0xEB,   // Array Vertical End Point LSB
                             /*0x3808*/ 0x02,   // ISP Horizontal Output Width MSB
                             /*0x3809*/ 0x80),  // ISP Horizontal Output Width LSB
    SENS_INTENT_CMD_CCI_I2C_WR(0x380A,  0x01,   // ISP Vertical Output Height MSB
                             /*0x380B*/ 0xE0,   // ISP Vertical Output Height LSB
                             /*0x380C*/ 0x03,   // Total Horizontal Timing Size MSB
                             /*0x380D*/ 0xA0,   // Total Horizontal Timing Size LSB
                             /*0x380E*/ 0x08,   // Total Vertical Timing Size MSB
                             /*0x380F*/ 0x10,   // Total Vertical Timing Size LSB
                             /*0x3810*/ 0x00,   // ISP Horizontal Windowing Offset MSB
                             /*0x3811*/ 0x04,   // ISP Horizontal Windowing Offset LSB
                             /*0x3812*/ 0x00,   // ISP Vertical Windowing Offset MSB
                             /*0x3813*/ 0x05,   // ISP Vertical Windowing Offset LSB
                             /*0x3814*/ 0x11,   // Bit[7:4]: x_odd_inc Bit[3:0]: x_even_inc
                             /*0x3815*/ 0x11),  // Bit[7:4]: y_odd_inc Bit[3:0]: y_even_inc
    SENS_INTENT_CMD_CCI_I2C_WR(0x3820,  0x40,   // Timing Format 1
                             /*0x3821*/ 0x00),  // Timing Format 2
    SENS_INTENT_CMD_CCI_I2C_WR(0x382F,  0xC4),  // Timing Reg 2F
    SENS_INTENT_CMD_CCI_I2C_WR(0x3832,  0xFF,
                             /*0x3833*/ 0xFF,
                             /*0x3834*/ 0x00,
                             /*0x3835*/ 0x05),
    SENS_INTENT_CMD_CCI_I2C_WR(0x3837,  0x00),  // Digital Binning Ctrl

    //=================== PWM and Strobe Control Registers
    SENS_INTENT_CMD_CCI_I2C_WR(0x3B80,  0x00,
                             /*0x3B81*/ 0xA5,
                             /*0x3B82*/ 0x10,
                             /*0x3B83*/ 0x00,
                             /*0x3B84*/ 0x08,
                             /*0x3B85*/ 0x00,
                             /*0x3B86*/ 0x01,
                             /*0x3B87*/ 0x00,
                             /*0x3B88*/ 0x00,
                             /*0x3B89*/ 0x00,
                             /*0x3B8A*/ 0x00,
                             /*0x3B8B*/ 0x05,
                             /*0x3B8C*/ 0x00,
                             /*0x3B8D*/ 0x00,
                             /*0x3B8E*/ 0x00,
                             /*0x3B8F*/ 0x1A),
    SENS_INTENT_CMD_CCI_I2C_WR(0x3B94,  0x05,
                             /*0x3B95*/ 0xF2,
                             /*0x3B96*/ 0x40),
    //=================== Low Power Mode Control Registers
    SENS_INTENT_CMD_CCI_I2C_WR(0x3C00,  0x89,
                             /*0x3C01*/ 0xAB,
                             /*0x3C02*/ 0x01,
                             /*0x3C03*/ 0x00,
                             /*0x3C04*/ 0x00,
                             /*0x3C05*/ 0x03,
                             /*0x3C06*/ 0x00,
                             /*0x3C07*/ 0x05),
    SENS_INTENT_CMD_CCI_I2C_WR(0x3C0C,  0x00,
                             /*0x3C0D*/ 0x00,
                             /*0x3C0E*/ 0x00,
                             /*0x3C0F*/ 0x00),

    //=================== BLC Control Registers
    SENS_INTENT_CMD_CCI_I2C_WR(0x4001,  0xC2),
    SENS_INTENT_CMD_CCI_I2C_WR(0x4004,  0x04,
                             /*0x4005*/ 0x20),
    SENS_INTENT_CMD_CCI_I2C_WR(0x404E,  0x01),

    //=================== Format Control Registers
    SENS_INTENT_CMD_CCI_I2C_WR(0x4300,  0xFF,
                             /*0x4301*/ 0x00),

    //=================== VFIFO Control Registers
    SENS_INTENT_CMD_CCI_I2C_WR(0x4600,  0x00,
                             /*0x4601*/ 0x4E),

    //=================== MIPI Top Control Registers
    SENS_INTENT_CMD_CCI_I2C_WR(0x4801,  0x0F),
    SENS_INTENT_CMD_CCI_I2C_WR(0x4806,  0x0F),
    SENS_INTENT_CMD_CCI_I2C_WR(0x4819,  0xAA),
    SENS_INTENT_CMD_CCI_I2C_WR(0x4823,  0x3E),
    SENS_INTENT_CMD_CCI_I2C_WR(0x4837,  0x19),

    //=================== LVDS Control Registers
    SENS_INTENT_CMD_CCI_I2C_WR(0x4A0D,  0x00),

    //=================== Manual/Auto Exposure/Gain Control Registers
    SENS_INTENT_CMD_CCI_I2C_WR(0x5000,  0x85,   // Gain Format 00
                             /*0x5001*/ 0x80),  // Gain Format 01
    CMD_ENTRY_END(),
};

static const hat_cm_socket_command_entry_t ov7251_mode1_settings[] = {
    CMD_ENTRY_DUMMY(),
    CMD_ENTRY_END(),
};

/*
*==============================================================================================================
* OV7251 timings
* ---------------
*
* Pll1Clk [MHz] = INCK[MHz] / Pll1PreDiv[0x30B4] * Pll1Mul[0x30B3] / Pll1PostDiv[0x30B1]
* MipiClk = Pll1Clk / Pll1MipiDiv[0x30B5]
* PixClk = Pll1Clk / Pll1PixClkDiv[0x30B0]
*
* Pll2Clk [MHz] = INCK[MHz] / Pll1PreDiv[0x3098] * Pll1Mul[0x3099] / Pll1PostDiv[0x309D]
* SysClk = Pll1Clk / Pll1SysDiv[0x309A]
* ADCClk = Pll1Clk / Pll1ADCDiv[0x309B]
* ------------------------------------------------------------------------------
*
* PLL1Clk[MHz] = 24 / 3 * 100 * /1 = 800 MHz
* MipiClk = 800 / 5 = 160 MHz
* PixClk = 800 / 10 = 80 MHz
*
* PLL1Clk[MHz] = 24 / 4 * 40 * /1 = 240 MHz
* SysClk = 240 / 5 = 48 MHz
* ADCClk = 240 / 4 = 60 MHz
* ------------------------------------------------------------------------------
*
* LPFR (lines per frame)  [0x380E:0x380F] = 0x0204 = 516
* PPLN (pixel per line)   [0x380C:0x380D] = 0x03A0 = 928
* ------------------------------------------------------------------------------
*
* frame_time = (PPLN * LPFR) / SysClk
* row_time = PPLN/SCLK
* max_exposure = 502 * row_time
* ------------------------------------------------------------------------------
*
* frame_time = 928 * 516 / 48 = 9976 uSec = 9.976 mSec
* row time = 928 / 48 = 19.333 uSec
* max_exposure = 502 * 19.333 = 9705.333 uSec
*===============================================================================
*/


static const hat_sensor_mode_t ov7251_modes[] = {
    { // ov7251 mode 0
        {0, 0, 640, 480}, //hat_rect_t field_of_view;      // Sensor active view area in physical area [pixels]
        {640, 480},       //hat_size_t     active;          // Sensor active output pixels [pixels]
        {1, 1},             //hat_size_t     pixels;          // Equivalent of a digital pixel in physical pixels
        {1, 1},             //hat_size_t     aspect;          // Aspect ratio on 1 pixel
        {25.0, 25.0},     //hat_range_float_t fps;          // sensor mode Min/Max fps
        {100, 39903},     //hat_range_t       exposure;        // Min & Max exposure time in [us]
        {
            .fmt.order.all = HAT_PORDBYR_B_Gb_Gr_R,
            .fmt.reserved0 = 0,
            .fmt.xydec     = HAT_XYDEC_NO,
            .fmt.uvpkd     = HAT_UVPKD_PACKED,
            .fmt.pkd       = HAT_PIXPKD_PACKED,
            .fmt.fmt       = HAT_PIXCOL_BAYER,
            .bpc           = 10,
            .compression   = HAT_COMPRESSION_NONE,
        }, // hat_pix_fmt_t  format;           // Color format and pattern
        // uint16                  rotate;     // 0, 90, 180, 270 - Rotate TODO should come from somewhere???
        0,             // uint16  v_flip;      // [ON/OFF]
        0,             // uint16  h_mirror;    // [ON/OFF]
        1.0,           // float   sensitivity; //  mode (binning/skipping/average) sensitivity against full mode
        80000000,    // uint32  pixel_clock; // In Hz
        19333,        // uint32  row_time;    // [ns] - Readout time of line & blanking
        39903,        // uint32  frame_time;  // [us] - Readout time of the frame & blanking
        928,          // uint32  ppln;        // Initial number PixelsPerLiNe
        516,          // uint32  lpfr;        // Initial number LinesPerFRame
        1,             // uint32   num_bad_frames;  // Number of bad frames after mode switch
        ov7251_25fps_settings,
        ARR_SIZE(ov7251_25fps_settings),
        .pipeline_id = 0,       // Don't use pipeline with scale down
        .mipi_rx_cfg = {
            .lines = 1,
            .rate = 480,
            .data_type = 0x2b,  // RAW 10
            .data_mode = 1,
        },
    },
};

static const hat_dtp_sensor_desc_t dtp_sensor_ov7251_1_db =
{ // OV7251 sensor
    .sensor_id          = DTP_SEN_OV7251_1,
    .sensor_name        = "OV7251 sensor 1",
    .sensor_features    = {
        {640, 480},         //hat_size_t          array_size;     // Physical area (valid + black pixels)
        {0, 0, 640, 480},   //hat_rect_t          valid_pixels;   // Sensor full area in physical area
         100,                 //uint32              base_ISO;       // real sensor sensitivity with gain = 1 [U32Q16]
        {1.0, 1.0, 16.0, 0.1}, //hat_bounds_float_t again;          // min/max sensor gain analog
        {1.0, 1.0,  1.0, 0.1},// hat_bounds_float_t dgain;          // min/max sensor gain dnalog
        0,                    //uint32              exp_gain_delay; // exposure to gain delay [frames]
        2,                    //uint32              global_delay;   // delay to output frame with applied parameters [frames]
        {   //const hat_sensor_operations_t operations;
            .pwr_off = {
                ARR_SIZE(ov7251_power_off_seq),
                ov7251_power_off_seq,
            },
            .pwr_on = {
                ARR_SIZE(ov7251_1_power_on_seq),
                ov7251_1_power_on_seq,
            },
            .init = {
                [SENSOR_INIT_1] = {
                    ARR_SIZE(ov7251_init_seq),
                    ov7251_init_seq,
                },
                [SENSOR_INIT_2] = {},
                [SENSOR_INIT_3] = {},
            },
            .stream_off = {
                ARR_SIZE(ov7251_stream_off_seq),
                ov7251_stream_off_seq,
            },
            .stream_on = {
                ARR_SIZE(ov7251_stream_on_seq),
                ov7251_stream_on_seq,
            },
        },
        {
            ARR_SIZE(ov7251_modes), // uint32             num;   // number of sensor modes
            ov7251_modes // list with sensor modes
        },
        1, //uint32  powerup_bad_frames;  // Number of bad frames after powerup
        1, //uint8 temp_sensor_supp;    // If sensor support temperature sensor - 1. If no - 0.
        //HAL3 specific data.
        {100, 1600}, //int32                   sensitivity_range[2];     // Range of valid sensitivities Min <= 100, Max >= 1600
        {.all = HAT_PORDBYR_B_Gb_Gr_R}, //hat_pix_order_t         color_filter_arrangement; // Arrangement of color filters on sensor TODO we have same info in sens mode descriptor
        {100000, 39903000},//TODO, //int64  exposure_time_range[2];  // Range of valid exposure times in [nanoseconds]
        100000000,//TODO, //int64  max_frame_duration; // Maximum possible frame duration in [nanoseconds].
        {0.00395, 0.00345}, //float                  physical_size[2];         // The physical dimensions of the full pixel array in [mm]
        1023, //int32                   white_level;              // Maximum raw value output by sensor
        {1, 1}, //hat_fract_t             base_gain_factor;         // Gain factor from electrons to raw units when ISO=100
        {42, 42, 42, 42}, //int32                   black_level_pattern[4];   // A fixed black level offset for each of the Bayer mosaic channels.
        {{0, 0}}, //hat_fract_t             calib_transform_1[9];     // Per-device calibration on top of color space transform 1
        {{0, 0}}, //hat_fract_t             calib_transform_2[9];     // Per-device calibration on top of color space transform 2
        {{0, 0}}, //hat_fract_t             color_transform_1[9];     // Linear mapping from XYZ (D50) color space to reference linear sensor color.
        {{0, 0}}, //hat_fract_t             color_transform_2[9];     // Linear mapping from XYZ (D50) color space to reference linear sensor color.
        {{0, 0}}, //hat_fract_t             forward_matrix_1[9];      // Used by DNG for better WB adaptation.
        {{0, 0}}, //hat_fract_t             forward_matrix_2[9];      // Used by DNG for better WB adaptation.
        800, //int32                   max_analog_sensitivity;    // Maximum sensitivity that is implemented purely through analog gain.
        {0.0}, //float                  noise_model_coef[2];      // Estimation of sensor noise characteristics.
        0, //int32                   orientation;              // degrees clockwise rotation TODO we have same info in sens mode descriptor
        REF_ILL_NONE, //reference_illuminant_t  reference_illuminant_1;   // Light source used to define transform 1.
        REF_ILL_NONE //reference_illuminant_t  reference_illuminant_2;   // Light source used to define transform 2.
    }
};


static const hat_dtp_sensor_desc_t dtp_sensor_ov7251_2_db =
{ // OV7251 sensor
    .sensor_id          = DTP_SEN_OV7251_2,
    .sensor_name        = "OV7251 sensor 2",
    .sensor_features    = {
        {640, 480},         //hat_size_t          array_size;     // Physical area (valid + black pixels)
        {0, 0, 640, 480},   //hat_rect_t          valid_pixels;   // Sensor full area in physical area
         100,                 //uint32              base_ISO;       // real sensor sensitivity with gain = 1 [U32Q16]
        {1.0, 1.0, 16.0, 0.1}, //hat_bounds_float_t again;          // min/max sensor gain analog
        {1.0, 1.0,  1.0, 0.1},// hat_bounds_float_t dgain;          // min/max sensor gain dnalog
        0,                    //uint32              exp_gain_delay; // exposure to gain delay [frames]
        2,                    //uint32              global_delay;   // delay to output frame with applied parameters [frames]
        {   //const hat_sensor_operations_t operations;
            .pwr_off = {
                ARR_SIZE(ov7251_power_off_seq),
                ov7251_power_off_seq,
            },
            .pwr_on = {
                ARR_SIZE(ov7251_2_power_on_seq),
                ov7251_2_power_on_seq,
            },
            .init = {
                [SENSOR_INIT_1] = {
                    ARR_SIZE(ov7251_init_seq),
                    ov7251_init_seq,
                },
                [SENSOR_INIT_2] = {},
                [SENSOR_INIT_3] = {},
            },
            .stream_off = {
                ARR_SIZE(ov7251_stream_off_seq),
                ov7251_stream_off_seq,
            },
            .stream_on = {
                ARR_SIZE(ov7251_stream_on_seq),
                ov7251_stream_on_seq,
            },
        },
        {
            ARR_SIZE(ov7251_modes), // uint32             num;   // number of sensor modes
            ov7251_modes // list with sensor modes
        },
        1, //uint32  powerup_bad_frames;  // Number of bad frames after powerup
        1, //uint8 temp_sensor_supp;    // If sensor support temperature sensor - 1. If no - 0.
        //HAL3 specific data.
        {100, 1600}, //int32                   sensitivity_range[2];     // Range of valid sensitivities Min <= 100, Max >= 1600
        {.all = HAT_PORDBYR_B_Gb_Gr_R}, //hat_pix_order_t         color_filter_arrangement; // Arrangement of color filters on sensor TODO we have same info in sens mode descriptor
        {100000, 39903000},//TODO, //int64  exposure_time_range[2];  // Range of valid exposure times in [nanoseconds]
        100000000,//TODO, //int64  max_frame_duration; // Maximum possible frame duration in [nanoseconds].
        {0.00395, 0.00345}, //float                  physical_size[2];         // The physical dimensions of the full pixel array in [mm]
        1023, //int32                   white_level;              // Maximum raw value output by sensor
        {1, 1}, //hat_fract_t             base_gain_factor;         // Gain factor from electrons to raw units when ISO=100
        {42, 42, 42, 42}, //int32                   black_level_pattern[4];   // A fixed black level offset for each of the Bayer mosaic channels.
        {{0, 0}}, //hat_fract_t             calib_transform_1[9];     // Per-device calibration on top of color space transform 1
        {{0, 0}}, //hat_fract_t             calib_transform_2[9];     // Per-device calibration on top of color space transform 2
        {{0, 0}}, //hat_fract_t             color_transform_1[9];     // Linear mapping from XYZ (D50) color space to reference linear sensor color.
        {{0, 0}}, //hat_fract_t             color_transform_2[9];     // Linear mapping from XYZ (D50) color space to reference linear sensor color.
        {{0, 0}}, //hat_fract_t             forward_matrix_1[9];      // Used by DNG for better WB adaptation.
        {{0, 0}}, //hat_fract_t             forward_matrix_2[9];      // Used by DNG for better WB adaptation.
        800, //int32                   max_analog_sensitivity;    // Maximum sensitivity that is implemented purely through analog gain.
        {0.0}, //float                  noise_model_coef[2];      // Estimation of sensor noise characteristics.
        0, //int32                   orientation;              // degrees clockwise rotation TODO we have same info in sens mode descriptor
        REF_ILL_NONE, //reference_illuminant_t  reference_illuminant_1;   // Light source used to define transform 1.
        REF_ILL_NONE //reference_illuminant_t  reference_illuminant_2;   // Light source used to define transform 2.
    }
};



#define DTP_SENSOR_OV7251_DB_SIZE   ( sizeof(dtp_sensor_ov7251_1_db) )

#endif // __DTP_SENSOR_OV7251_DB_H__
