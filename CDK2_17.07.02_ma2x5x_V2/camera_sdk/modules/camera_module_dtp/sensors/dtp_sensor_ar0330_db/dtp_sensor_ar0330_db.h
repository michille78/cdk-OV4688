/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file dtp_sensor_ar0330_db.h
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#ifndef __DTP_SENSOR_AR0330_DB_H__
#define __DTP_SENSOR_AR0330_DB_H__

// ============================ AR0330 camera module =========================

static const hat_cm_socket_command_entry_t ar0330_power_off_seq[] = {
    CMD_ENTRY_DUMMY(),
    CMD_ENTRY_END(),
};

static const hat_cm_socket_command_entry_t ar0330_power_on_seq[] = {

    CMD_ENTRY_DUMMY(),
    CMD_ENTRY_END(),
};

static const hat_cm_socket_command_entry_t ar0330_init_seq[] =
{
    SENS_INTENT_CMD_CCI_I2C_WR( 0x3052, 0xa1, 0x14 ),
    SENS_INTENT_CMD_CCI_I2C_WR( 0x304a, 0x00, 0x70 ),

//PLL_settings
//STATE = Master Clock, 768000 00
    SENS_INTENT_CMD_CCI_I2C_WR( 0x302A, 0x00, 0x05 ), //VT_PIX_CLK_DIV = 5
    SENS_DELAY(10000),
    SENS_INTENT_CMD_CCI_I2C_WR( 0x302C, 0x00, 0x02 ), //VT_SYS_CLK_DIV = 2
    SENS_INTENT_CMD_CCI_I2C_WR( 0x302E, 0x00, 0x0C ), //PRE_PLL_CLK_DIV = 12
    SENS_INTENT_CMD_CCI_I2C_WR( 0x3030, 0x00, 0xF5 ), //PLL_MULTIPLIER = 245
    SENS_INTENT_CMD_CCI_I2C_WR( 0x3036, 0x00, 0x0A ), //OP_PIX_CLK_DIV = 10
    SENS_INTENT_CMD_CCI_I2C_WR( 0x3038, 0x00, 0x01 ), //OP_SYS_CLK_DIV = 1
    SENS_INTENT_CMD_CCI_I2C_WR( 0x31AC, 0x0A, 0x0A ), //DATA_FORMAT_BITS = 2570 (0x0A0A)
    SENS_INTENT_CMD_CCI_I2C_WR( 0x31AE, 0x02, 0x02 ), //SERIAL_FORMAT = 514

//MIPI Port Timing
    SENS_INTENT_CMD_CCI_I2C_WR( 0x31B0, 0x00, 0x29 ), //0x0024
    SENS_INTENT_CMD_CCI_I2C_WR( 0x31B2, 0x00, 0x10 ), //0x000F
    SENS_INTENT_CMD_CCI_I2C_WR( 0x31B4, 0x3A, 0x43 ), //0x2743
    SENS_DELAY(10000), //
    SENS_INTENT_CMD_CCI_I2C_WR( 0x31B6, 0x21, 0x4D ), //0x114E
    SENS_INTENT_CMD_CCI_I2C_WR( 0x31B8, 0x20, 0x89 ), //0x2049
    SENS_INTENT_CMD_CCI_I2C_WR( 0x31BA, 0x01, 0x86 ), //0x0187
    SENS_INTENT_CMD_CCI_I2C_WR( 0x31BC, 0x80, 0x05 ),


 //Timing_settings
    SENS_INTENT_CMD_CCI_I2C_WR( 0x3002, 0x00, 0xEA ), //Y_ADDR_START = 234
    SENS_INTENT_CMD_CCI_I2C_WR( 0x3004, 0x00, 0xC6 ), //X_ADDR_START = 198
    SENS_INTENT_CMD_CCI_I2C_WR( 0x3006, 0x05, 0x21 ), //Y_ADDR_END = 1313
    SENS_INTENT_CMD_CCI_I2C_WR( 0x3008, 0x08, 0x45 ), //X_ADDR_END = 2117
    SENS_INTENT_CMD_CCI_I2C_WR( 0x300A, 0x07, 0xB4 ), //FRAME_LENGTH_LINES = 1972
    SENS_INTENT_CMD_CCI_I2C_WR( 0x300C, 0x04, 0xDA ), //LINE_LENGTH_PCK = 1242
    SENS_INTENT_CMD_CCI_I2C_WR( 0x3012, 0x05, 0x22 ), //COARSE_INTEGRATION_TIME
    SENS_INTENT_CMD_CCI_I2C_WR( 0x3014, 0x00, 0x00 ), //FINE_INTEGRATION_TIME = 0
    SENS_INTENT_CMD_CCI_I2C_WR( 0x30A2, 0x00, 0x01 ), //X_ODD_INC = 1
    SENS_INTENT_CMD_CCI_I2C_WR( 0x30A6, 0x00, 0x01 ), //Y_ODD_INC = 1

    SENS_INTENT_CMD_CCI_I2C_WR( 0x3040, 0x00, 0x00 ), //READ_MODE = 0
    SENS_INTENT_CMD_CCI_I2C_WR( 0x3042, 0x00, 0x00 ), //EXTRA_DELAY
    SENS_INTENT_CMD_CCI_I2C_WR( 0x30BA, 0x00, 0x2C ), //DIGITAL_CTRL = 44

 //Recommended Configuration
    SENS_INTENT_CMD_CCI_I2C_WR( 0x31E0, 0x03, 0x03 ),
    SENS_INTENT_CMD_CCI_I2C_WR( 0x3064, 0x18, 0x02 ),
    SENS_INTENT_CMD_CCI_I2C_WR( 0x3ED2, 0x01, 0x46 ),
    SENS_INTENT_CMD_CCI_I2C_WR( 0x3ED4, 0x8F, 0x6C ),
    SENS_INTENT_CMD_CCI_I2C_WR( 0x3ED6, 0x66, 0xCC ),
    SENS_INTENT_CMD_CCI_I2C_WR( 0x3ED8, 0x8C, 0x42 ),
    SENS_INTENT_CMD_CCI_I2C_WR( 0x3EDA, 0x88, 0xBC ),
    SENS_INTENT_CMD_CCI_I2C_WR( 0x3EDC, 0xAA, 0x63 ),
    SENS_INTENT_CMD_CCI_I2C_WR( 0x305E, 0x00, 0xA0 ),

    CMD_ENTRY_END(),
};

static const hat_cm_socket_command_entry_t ar0330_stream_off_seq[] = {
    SENS_INTENT_CMD_CCI_I2C_WR( 0x301a, 0x00, 0x58 ),
    CMD_ENTRY_END(),
};

static const hat_cm_socket_command_entry_t ar0330_stream_on_seq[] = {
    SENS_INTENT_CMD_CCI_I2C_WR( 0x301a, 0x02, 0x5c ),
    CMD_ENTRY_END(),
};

static const hat_cm_socket_command_entry_t ar0330_mode0_settings[] = {

    CMD_ENTRY_DUMMY(),
    CMD_ENTRY_END(),
};

static const hat_cm_socket_command_entry_t ar0330_mode1_settings[] = {
    CMD_ENTRY_DUMMY(),
    CMD_ENTRY_END(),
};

static const hat_sensor_mode_t ar0330_modes[] = {
    { // ar0330 mode 0
        {0, 0, 1920, 1080},   //hat_rect_t  field_of_view;   // Sensor active view area in physical area [pixels]
        {1920, 1080},    //hat_size_t        active;          // Sensor active output pixels [pixels]
        {1, 1},        //hat_size_t        pixels;          // Equivalent of a digital pixel in physical pixels
        {1, 1},        //hat_size_t        aspect;          // Aspect ratio on 1 pixel
        {1.058, 30.0}, //hat_range_float_t fps;             // sensor mode Min/Max fps
        {100, 33333},  //hat_range_t       exposure;        // Min & Max exposure time in [us]
        {
            .fmt.order.all = HAT_PORDBYR_Gr_R_B_Gb,
            .fmt.reserved0 = 0,
            .fmt.xydec     = HAT_XYDEC_NO,
            .fmt.uvpkd     = HAT_UVPKD_PACKED,
            .fmt.pkd       = HAT_PIXPKD_PACKED,
            .fmt.fmt       = HAT_PIXCOL_BAYER,
            .bpc           = 10,
            .compression   = HAT_COMPRESSION_NONE,
        },            // hat_pix_fmt_t  format;     // Color format and pattern
        // uint16                  rotate;          // 0, 90, 180, 270 - Rotate TODO should come from somewhere???
        0,             // uint16   v_flip;          // [ON/OFF]
        0,             // uint16   h_mirror;        // [ON/OFF]
        1.0,           // float    sensitivity;     //  mode (binning/skipping/average) sensitivity against full mode
        489979485,     // uint32   pixel_clock;     // In Hz
        25348,         // uint32   row_time;        // [ns] - Readout time of line & blanking
        33333,         // uint32   frame_time;      // [us] - Readout time of the frame & blanking
        1242,          // uint32   ppln;            // Initial number PixelsPerLiNe
        1972,          // uint32   lpfr;            // Initial number LinesPerFRame
        3,             // uint32   num_bad_frames;  // Number of bad frames after mode switch
        ar0330_mode0_settings,
        ARR_SIZE(ar0330_mode0_settings),
        .pipeline_id = 0,       // Don't use pipeline with scale down
        .mipi_rx_cfg = {
            .lines = 2,
            .rate = 512,
            .data_type = 0x2b,  // RAW 10
            .data_mode = 1,
        },
    },
};

static const hat_dtp_sensor_desc_t dtp_sensor_ar0330_db =
{ // Aptina AR0330 sensor
    .sensor_id          = DTP_SEN_AR0330,
    .sensor_name        = "Aptina AR0330 sensor",
    .sensor_features    = {
        {1920, 1080},         //hat_size_t          array_size;     // Physical area (valid + black pixels)
        {0, 0, 1920, 1080},   //hat_rect_t          valid_pixels;   // Sensor full area in physical area
         100,                 //uint32              base_ISO;       // real sensor sensitivity with gain = 1 [U32Q16]
        {1.0, 1.0, 16.0, 0.1},// hat_bounds_float_t again;          // min/max sensor gain analog
        {1.0, 1.0,  1.0, 0.1},// hat_bounds_float_t dgain;          // min/max sensor gain dnalog
        1,                    //uint32              exp_gain_delay; // exposure to gain delay [frames]
        2,                    //uint32              global_delay;   // delay to output frame with applied parameters [frames]
        {   //const hat_sensor_operations_t operations;
            .pwr_off = {
                ARR_SIZE(ar0330_power_off_seq),
                ar0330_power_off_seq,
            },
            .pwr_on = {
                ARR_SIZE(ar0330_power_on_seq),
                ar0330_power_on_seq,
            },
            .init = {
                [SENSOR_INIT_1] = {
                    ARR_SIZE(ar0330_init_seq),
                    ar0330_init_seq,
                },
                [SENSOR_INIT_2] = {},
                [SENSOR_INIT_3] = {},
            },
            .stream_off = {
                ARR_SIZE(ar0330_stream_off_seq),
                ar0330_stream_off_seq,
            },
            .stream_on = {
                ARR_SIZE(ar0330_stream_on_seq),
                ar0330_stream_on_seq,
            },
        },
        {
            ARR_SIZE(ar0330_modes), // uint32             num;   // number of sensor modes
            ar0330_modes // list with sensor modes
        },
        1, //uint32  powerup_bad_frames;  // Number of bad frames after powerup
        0, //uint8 temp_sensor_supp;    // If sensor support temperature sensor - 1. If no - 0.
        //HAL3 specific data.
        {100, 1600}, //int32                   sensitivity_range[2];     // Range of valid sensitivities Min <= 100, Max >= 1600
        {.all = HAT_PORDBYR_Gr_R_B_Gb}, //hat_pix_order_t         color_filter_arrangement; // Arrangement of color filters on sensor TODO we have same info in sens mode descriptor
        {92304, 756131292},//{8 * 11538LL, 65534 * 11538LL}, //int64  exposure_time_range[2];  // Range of valid exposure times in [nanoseconds]
        756142830,//(65534 + 1) * 11538LL, //int64  max_frame_duration; // Maximum possible frame duration in [nanoseconds].
        {4.6288, 3.762}, //float                  physical_size[2];         // The physical dimensions of the full pixel array in [mm]
        1023, //int32                   white_level;              // Maximum raw value output by sensor
        {1, 1}, //hat_fract_t             base_gain_factor;         // Gain factor from electrons to raw units when ISO=100
        {42, 42, 42, 42}, //int32                   black_level_pattern[4];   // A fixed black level offset for each of the Bayer mosaic channels.
        {{0, 0}}, //hat_fract_t             calib_transform_1[9];     // Per-device calibration on top of color space transform 1
        {{0, 0}}, //hat_fract_t             calib_transform_2[9];     // Per-device calibration on top of color space transform 2
        {{0, 0}}, //hat_fract_t             color_transform_1[9];     // Linear mapping from XYZ (D50) color space to reference linear sensor color.
        {{0, 0}}, //hat_fract_t             color_transform_2[9];     // Linear mapping from XYZ (D50) color space to reference linear sensor color.
        {{0, 0}}, //hat_fract_t             forward_matrix_1[9];      // Used by DNG for better WB adaptation.
        {{0, 0}}, //hat_fract_t             forward_matrix_2[9];      // Used by DNG for better WB adaptation.
        800, //int32                   max_analog_sensitivity;    // Maximum sensitivity that is implemented purely through analog gain.
        {0.0}, //float                  noise_model_coef[2];      // Estimation of sensor noise characteristics.
        0, //int32                   orientation;              // degrees clockwise rotation TODO we have same info in sens mode descriptor
        REF_ILL_NONE, //reference_illuminant_t  reference_illuminant_1;   // Light source used to define transform 1.
        REF_ILL_NONE //reference_illuminant_t  reference_illuminant_2;   // Light source used to define transform 2.
    },
};

#define DTP_SENSOR_AR0330_DB_SIZE ( \
    sizeof(dtp_sensor_ar0330_db) \
)

#endif //__DTP_SENSOR_AR0330_DB_H__
