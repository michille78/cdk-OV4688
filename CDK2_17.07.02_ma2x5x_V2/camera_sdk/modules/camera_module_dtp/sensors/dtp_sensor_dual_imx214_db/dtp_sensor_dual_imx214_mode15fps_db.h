
// IMX214, 2 lane config, 810Mbps, 10fps
//const u32 imx214_2L_4208x3120_RAW10_10Hz_I2C_regs

/*
*==============================================================================================================
*               IMX214 timings
*               ---------------
*
*                div            mul                div           div           mul
*              --------       --------           --------      --------      --------
*             |        |     |        |         |        |    |        |    |        |
* In Freq ----| 0x0305 |-----| 0x0306 |----+----| 0x0301 |----| 0x0303 |----|   *4   |--- VTPXCK
*             |        |     | ox0307 |    |    |        |    |        |    |        |
*              --------       --------     |     --------      --------      --------
*                                          |
*                                          |       div           div           mul
*                                          |     --------      --------      --------
*                                          |    |        |    |        |    |        |
*                                          -----| 0x0309 |----| 0x030b |----|   *??  |--- OPPXCK
*                                               |        |    |        |    |        |
*                                                --------      --------      --------
*
*
* PPLN (pixel per line)   0x0342:0x0343
* LPFR (lines per frame)  0x0340:0x0341
*
* row_time[uSec] = PPLN / VTPXCK[MHz]
* frame_time[uSec] = row_time * LPFR
*==============================================================================================================
*               15 fps mode settings
*               ---------------------
*
*                 div            mul                div           div           mul
*               --------       --------           --------      --------      --------
*              |        | 8MHz|        | 1200MHz |        | 240|        | 60 |        |
*  24MHz   ----|   /3   |-----|  *150  |----+----|   /5   |----|    /4  |----|   *4   |--- VTPXCK = 240MHz
*              |        |     |        |    |    |        |    |        |    |        |
*               --------       --------     |     --------      --------      --------
*                                           |
*                                           |       div           div           mul
*                                           |     --------      --------      --------
*                                           |    |        |    |        |    |        |
*                                           -----| 0x0309 |----| 0x030b |----|   *??  |--- OPPXCK
*                                                |        |    |        |    |        |
*                                                 --------      --------      --------
*
* PPLN (pixel per line)   [0x0342:0x0343] = 0x1390 (5008)
* LPFR (lines per frame)  [0x0340:0x0341] = 0x0c58 (3160)
*
* row_time   = 5008 / 240 = 20.8666666667 uSec
* frame_time = 20.8666666667 * 3160 = 65,938.6666667 uSec
*
*==============================================================================================================
*/

static const hat_cm_socket_command_entry_t dual_imx214_mode15fps_settings[] = {
    //External Clock Setting
    SENS_INTENT_CMD_CCI_I2C_WR( 0x0100, 0x00), // Stop streaming
    //Mode setting

    SENS_INTENT_CMD_CCI_I2C_WR(0x0340, 0x0C     // FRM_LENGTH_LINES [15:8]
                               /* 0x0341*/, 0x58   // FRM_LENGTH_LINES [7:0]
                               /* 0x0342*/, 0x13   // LINE_LENGTH_PCK [15:8]
                               /* 0x0343*/, 0x90   // LINE_LENGTH_PCK [7:0]
                               /* 0x0344*/, 0x00   // X_ADD_STA [12:8]
                               /* 0x0345*/, 0x00   // X_ADD_STA [7:0]
                               /* 0x0346*/, 0x00   // Y_ADD_STA [11:8]
                               /* 0x0347*/, 0x00   // Y_ADD_STA [7:0]
                               /* 0x0348*/, 0x10   // X_ADD_END [12:8]
                               /* 0x0349*/, 0x6F   // X_ADD_END [7:0]
                               /* 0x034A*/, 0x0C   // Y_ADD_END [11:8]
                               /* 0x034B*/, 0x2F), // Y_ADD_END [7:0]

    SENS_INTENT_CMD_CCI_I2C_WR(0x0381, 0x01), // X_EVN_INC[2:0]
    SENS_INTENT_CMD_CCI_I2C_WR(0x0383, 0x01), // X_ODD_INC[2:0]
    SENS_INTENT_CMD_CCI_I2C_WR(0x0385, 0x01), // Y_EVN_INC[2:0]
    SENS_INTENT_CMD_CCI_I2C_WR(0x0387, 0x01), // Y_ODD_INC[2:0]

    SENS_INTENT_CMD_CCI_I2C_WR(0x0900, 0x00   // BINNING_MODE 0 - disable
                              /* 0x0901*/, 0x00   // BINNING_TYPE_V[3:0]
                              /* 0x0902*/, 0x00), // BINNING_WEIGHTING[7:0]

//Output Size setting
    SENS_INTENT_CMD_CCI_I2C_WR(0x034C, 0x10     // X_OUT_SIZE[12:8]
                              /* 0x034D*/, 0x70   // X_OUT_SIZE[7:0]   ),
                              /* 0x034E*/, 0x0C   // Y_OUT_SIZE[11:8]  ),
                              /* 0x034F*/, 0x30), // Y_OUT_SIZE[7:0]

    SENS_INTENT_CMD_CCI_I2C_WR(0x0401, 0x00),   // SCALE_MODE[1:0] 0 - No scaling
    SENS_INTENT_CMD_CCI_I2C_WR(0x0404, 0x00     // SCALE_M[8]
                              /*0x0405*/, 0x10),    // SCALE_M[7:0]

    SENS_INTENT_CMD_CCI_I2C_WR(0x0408, 0x00    // DIG_CROP_X_OFFSET[12:8]
                                /*0x0409*/, 0x00   // DIG_CROP_X_OFFSET[7:0]      ,
                                /*0x040A*/, 0x00   // DIG_CROP_Y_OFFSET[11:8]     ,
                                /*0x040B*/, 0x00   // DIG_CROP_Y_OFFSET[7:0]      ,
                                /*0x040C*/, 0x10   // DIG_CROP_IMAGE_WIDTH[12:8]  ,
                                /*0x040D*/, 0x70   // DIG_CROP_IMAGE_WIDTH[7:0]   ,
                                /*0x040E*/, 0x0C   // DIG_CROP_IMAGE_HEIGHT[11:8] ,
                                /*0x040F*/, 0x30), // DIG_CROP_IMAGE_HEIGHT[7:0]

    //Integration Time Setting
    SENS_INTENT_CMD_CCI_I2C_WR(0x0202, 0x0C   // COARSE_INTEG_TIME[15:8]
                               /*0x0203*/, 0x4E), // COARSE_INTEG_TIME[7:0]

    SENS_INTENT_CMD_CCI_I2C_WR(0x0224, 0x01  // ST_COARSE_INTEG_TIME[15:8]
                              /*0x0225*/, 0xF4), // ST_COARSE_INTEG_TIME[7:0]

    //Gain Setting
    SENS_INTENT_CMD_CCI_I2C_WR(0x0204, 0x00     // ANA_GAIN_GLOBAL [8]
                                /*0x0205*/, 0x00), // ANA_GAIN_GLOBAL [7:0]

    SENS_INTENT_CMD_CCI_I2C_WR(0x020E, 0x01    // DIG_GAIN_GR[15:8]
                               /*0x020F*/, 0x00  // DIG_GAIN_GR[7:0]
                               /*0x0210*/, 0x01  // DIG_GAIN_R[15:8]
                               /*0x0211*/, 0x00  // DIG_GAIN_R[7:0]
                               /*0x0212*/, 0x01  // DIG_GAIN_B[15:8]
                               /*0x0213*/, 0x00  // DIG_GAIN_B[7:0]
                               /*0x0214*/, 0x01  // DIG_GAIN_GB[15:8]
                               /*0x0215*/, 0x00  // DIG_GAIN_GB[7:0]
                               /*0x0216*/, 0x00  // short_analogue_gain_global
                               /*0x0217*/, 0x00), // short_analogue_gain_global

    //Analog Setting
    SENS_INTENT_CMD_CCI_I2C_WR(0x4170, 0x00    //???
                               /*0x4171*/, 0x10), // ???

    SENS_INTENT_CMD_CCI_I2C_WR(0x4176, 0x00    // ???
                               /*0x4177*/, 0x3C), // ???

    SENS_INTENT_CMD_CCI_I2C_WR(0xAE20, 0x04   // ???
                              /*0xAE21*/, 0x5C), // ???

    CMD_ENTRY_END(),
};
