/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file dtp_sensor_ov13860_db.h
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#ifndef __DTP_SENSOR_OV13860_DB_H__
#define __DTP_SENSOR_OV13860_DB_H__

// ============================ OV13860 camera module =========================

static const hat_cm_socket_command_entry_t ov13860_power_off_seq[] = {
    SENS_INTENT_ACTION(SEN_RSRC_XSHTDWN, HAT_HW_RES_OFF), //XSHDN#
    SENS_INTENT_ACTION(SEN_RSRC_POWER_1, HAT_HW_RES_OFF), //PRWDN#
    SENS_DELAY(1000),
    CMD_ENTRY_DUMMY(),
    CMD_ENTRY_END(),
};

static const hat_cm_socket_command_entry_t ov13860_power_on_seq[] = {
    //SENS_INTENT_ACTION(SEN_RSRC_XSHTDWN, HAT_HW_RES_ON),
    //SENS_DELAY(1000),
    SENS_INTENT_ACTION(SEN_RSRC_POWER_1, HAT_HW_RES_ON),  //POWER
    SENS_INTENT_ACTION(SEN_RSRC_CLOCK, HAT_HW_RES_ON),  // ENABLE CLOCK
    SENS_INTENT_ACTION(SEN_RSRC_XSHTDWN, HAT_HW_RES_OFF), //XSHDN#
    SENS_DELAY(1000),
    SENS_INTENT_ACTION(SEN_RSRC_XSHTDWN, HAT_HW_RES_ON), //XSHDN#
    SENS_DELAY(1000),
    CMD_ENTRY_END(),
};

//
static const hat_cm_socket_command_entry_t ov13860_init_seq[] = {
        //Reset
        SENS_INTENT_CMD_CCI_I2C_WR(0x0103, 0x01),
        //sl 1 1 ;delay 1ms
        SENS_DELAY(1000),
        SENS_INTENT_CMD_CCI_I2C_WR(0x0302, 0x32//),
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x0303*/, 0x00),

        SENS_INTENT_CMD_CCI_I2C_WR(0x030e, 0x02//),
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x030f*/, 0x03),

        SENS_INTENT_CMD_CCI_I2C_WR(0x0312, 0x03//),
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x0313*/, 0x03),

        SENS_INTENT_CMD_CCI_I2C_WR(0x031e, 0x01),
        SENS_INTENT_CMD_CCI_I2C_WR(0x3010, 0x01),
        SENS_INTENT_CMD_CCI_I2C_WR(0x3012, 0x41), //MIPI ??? lanes ; phy_pad_en
        
        SENS_INTENT_CMD_CCI_I2C_WR(0x340c, 0xff//),
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x340d*/, 0xff),

        SENS_INTENT_CMD_CCI_I2C_WR(0x3501, 0x0d//),
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x3502*/, 0x88//),
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x3503*/, 0x03),

        SENS_INTENT_CMD_CCI_I2C_WR(0x3507, 0x00//),
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x3508*/, 0x00//),
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x3509*/, 0x12//),
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x350a*/, 0x00//),
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x350b*/, 0x40),

        SENS_INTENT_CMD_CCI_I2C_WR(0x350f, 0x10),

        SENS_INTENT_CMD_CCI_I2C_WR(0x3541, 0x02//),
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x3542*/, 0x00//),
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x3543*/, 0x00),

        SENS_INTENT_CMD_CCI_I2C_WR(0x3547, 0x00//),
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x3548*/, 0x00//),
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x3549*/, 0x12),

        SENS_INTENT_CMD_CCI_I2C_WR(0x354b, 0x10),

        SENS_INTENT_CMD_CCI_I2C_WR(0x354f, 0x10),

        SENS_INTENT_CMD_CCI_I2C_WR(0x3600, 0x41//),
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x3601*/, 0xd4),

        SENS_INTENT_CMD_CCI_I2C_WR(0x3603, 0x97//),
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x3604*/, 0x08),

        SENS_INTENT_CMD_CCI_I2C_WR(0x360a, 0x35),

        SENS_INTENT_CMD_CCI_I2C_WR(0x360c, 0xa0//),
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x360d*/, 0x53),

        SENS_INTENT_CMD_CCI_I2C_WR(0x3618, 0x0c),
        SENS_INTENT_CMD_CCI_I2C_WR(0x3620, 0x55),
        SENS_INTENT_CMD_CCI_I2C_WR(0x3622, 0x7c),
        SENS_INTENT_CMD_CCI_I2C_WR(0x3628, 0xc0),
        SENS_INTENT_CMD_CCI_I2C_WR(0x3660, 0xd0),
        SENS_INTENT_CMD_CCI_I2C_WR(0x3662, 0x00),
        SENS_INTENT_CMD_CCI_I2C_WR(0x3664, 0x04),
        SENS_INTENT_CMD_CCI_I2C_WR(0x366b, 0x00),

        SENS_INTENT_CMD_CCI_I2C_WR(0x3701, 0x20//),
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x3702*/, 0x30//),
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x3703*/, 0x3b//),
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x3704*/, 0x26//),
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x3705*/, 0x07//),
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x3706*/, 0x3f),

        SENS_INTENT_CMD_CCI_I2C_WR(0x3709, 0x18//),
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x370a*/, 0x23),

        SENS_INTENT_CMD_CCI_I2C_WR(0x370e, 0x32),
        SENS_INTENT_CMD_CCI_I2C_WR(0x3710, 0x10),
        SENS_INTENT_CMD_CCI_I2C_WR(0x3712, 0x12),
        SENS_INTENT_CMD_CCI_I2C_WR(0x3714, 0x00),

        SENS_INTENT_CMD_CCI_I2C_WR(0x3719, 0x03//),
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x3720*/, 0x18),

        SENS_INTENT_CMD_CCI_I2C_WR(0x3726, 0x22//),
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x3727*/, 0x44//),
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x3728*/, 0x40//),
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x3729*/, 0x00),

        SENS_INTENT_CMD_CCI_I2C_WR(0x372e, 0x2b//),
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x372f*/, 0xa0),

        SENS_INTENT_CMD_CCI_I2C_WR(0x372b, 0x00),

        SENS_INTENT_CMD_CCI_I2C_WR(0x3744, 0x01//),
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x3745*/, 0x5e//),
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x3746*/, 0x01//),
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x3747*/, 0x1f),

        SENS_INTENT_CMD_CCI_I2C_WR(0x374a, 0x4b),

        SENS_INTENT_CMD_CCI_I2C_WR(0x3760, 0xd1//),
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x3761*/, 0x31),

        SENS_INTENT_CMD_CCI_I2C_WR(0x376c, 0x43//),
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x376d*/, 0x01//),
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x376e*/, 0x53),

        SENS_INTENT_CMD_CCI_I2C_WR(0x378c, 0x1f//),
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x378d*/, 0x13),

        SENS_INTENT_CMD_CCI_I2C_WR(0x378f, 0x88),

        SENS_INTENT_CMD_CCI_I2C_WR(0x3790, 0x5a//),
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x3791*/, 0x5a//),
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x3792*/, 0x21),

        SENS_INTENT_CMD_CCI_I2C_WR(0x3794, 0x71),
        SENS_INTENT_CMD_CCI_I2C_WR(0x3796, 0x01),

        SENS_INTENT_CMD_CCI_I2C_WR(0x379f, 0x3e//),
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x37a0*/, 0x44//),
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x37a1*/, 0x00//),
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x37a2*/, 0x44//),
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x37a3*/, 0x41//),
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x37a4*/, 0x88//),
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x37a5*/, 0xa9),

        SENS_INTENT_CMD_CCI_I2C_WR(0x37b3, 0xdc//),
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x37b4*/, 0x0e),

        SENS_INTENT_CMD_CCI_I2C_WR(0x37b7, 0x84//),
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x37b8*/, 0x02//),
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x37b9*/, 0x08),

        //TODO: Make optimization!!!!!!!
        SENS_INTENT_CMD_CCI_I2C_WR(0x3800, 0x00//), // x_start - high byte
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x3801*/, 0x14//), // x_start - low byte
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x3802*/, 0x00//), // y_start - high byte
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x3803*/, 0x0c//), // y_start - low byte
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x3804*/, 0x10//), // x_end - high byte
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x3805*/, 0x8b//), // x_end - low byte
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x3806*/, 0x0c//), // y_end - high byte
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x3807*/, 0x43//), // y_end - low byte
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x3808*/, 0x10//), // x_output - high byte
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x3809*/, 0x80//), // x_output - low byte
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x380a*/, 0x0c//), // y_output - high byte
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x380b*/, 0x30//), // y_output - low byte
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x380c*/, 0x11//), // HTS - high byte - PPLN
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x380d*/, 0xd0//), // HTS - line_lenght
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x380e*/, 0x0d//), // VTS - high byte - LPFR
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x380f*/, 0xa8//), // VTS - frame_lenght
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x3810*/, 0x00//), // ISP horizontal offset
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x3811*/, 0x04), // ISP horizontal offset
        SENS_INTENT_CMD_CCI_I2C_WR(0x3812, 0x00//), // ISP vertical offset
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x3813*/, 0x04//), // ISP vertical offset
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x3814*/, 0x11//), // x binning
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x3815*/, 0x11), // y binning

        SENS_INTENT_CMD_CCI_I2C_WR(0x3820, 0x04//),
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x3821*/, 0x00), // MIRROR

        SENS_INTENT_CMD_CCI_I2C_WR(0x382a, 0x04),

        SENS_INTENT_CMD_CCI_I2C_WR(0x3835, 0x04//),
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x3836*/, 0x0c//),
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x3837*/, 0x02),

        SENS_INTENT_CMD_CCI_I2C_WR(0x382f, 0x84),

        SENS_INTENT_CMD_CCI_I2C_WR(0x383c, 0x88//),
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x383d*/, 0xff),

        SENS_INTENT_CMD_CCI_I2C_WR(0x3842, 0x00),

        SENS_INTENT_CMD_CCI_I2C_WR(0x3845, 0x10),
        SENS_INTENT_CMD_CCI_I2C_WR(0x3d85, 0x16),

        SENS_INTENT_CMD_CCI_I2C_WR(0x3d8c, 0x79//),
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x3d8d*/, 0x7f),

        SENS_INTENT_CMD_CCI_I2C_WR(0x4000, 0x17),

        SENS_INTENT_CMD_CCI_I2C_WR(0x4008, 0x00//),
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x4009*/, 0x13),

        SENS_INTENT_CMD_CCI_I2C_WR(0x400f, 0x80),
        SENS_INTENT_CMD_CCI_I2C_WR(0x4011, 0xfb),
        SENS_INTENT_CMD_CCI_I2C_WR(0x4017, 0x08),
        SENS_INTENT_CMD_CCI_I2C_WR(0x401a, 0x0e),
        SENS_INTENT_CMD_CCI_I2C_WR(0x4019, 0x18),
        SENS_INTENT_CMD_CCI_I2C_WR(0x4020, 0x08),
        SENS_INTENT_CMD_CCI_I2C_WR(0x4022, 0x08),
        SENS_INTENT_CMD_CCI_I2C_WR(0x4024, 0x08),
        SENS_INTENT_CMD_CCI_I2C_WR(0x4026, 0x08),
        SENS_INTENT_CMD_CCI_I2C_WR(0x4028, 0x08),
        SENS_INTENT_CMD_CCI_I2C_WR(0x402a, 0x08),
        SENS_INTENT_CMD_CCI_I2C_WR(0x402c, 0x08),
        SENS_INTENT_CMD_CCI_I2C_WR(0x402e, 0x08),
        SENS_INTENT_CMD_CCI_I2C_WR(0x4030, 0x08),
        SENS_INTENT_CMD_CCI_I2C_WR(0x4032, 0x08),
        SENS_INTENT_CMD_CCI_I2C_WR(0x4034, 0x08),
        SENS_INTENT_CMD_CCI_I2C_WR(0x4036, 0x08),
        SENS_INTENT_CMD_CCI_I2C_WR(0x4038, 0x08),
        SENS_INTENT_CMD_CCI_I2C_WR(0x403a, 0x08),
        SENS_INTENT_CMD_CCI_I2C_WR(0x403c, 0x08),
        SENS_INTENT_CMD_CCI_I2C_WR(0x403e, 0x08),

        SENS_INTENT_CMD_CCI_I2C_WR(0x4051, 0x03//),
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x4052*/, 0x00//),
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x4053*/, 0x80//),
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x4054*/, 0x00//),
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x4055*/, 0x80//),
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x4056*/, 0x00//),
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x4057*/, 0x80//),
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x4058*/, 0x00//),
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x4059*/, 0x80),

        SENS_INTENT_CMD_CCI_I2C_WR(0x4066, 0x04),

        SENS_INTENT_CMD_CCI_I2C_WR(0x4202, 0x00//),
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x4203*/, 0x00),

        SENS_INTENT_CMD_CCI_I2C_WR(0x4b03, 0x80),

        SENS_INTENT_CMD_CCI_I2C_WR(0x4d00, 0x05//),
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x4d01*/, 0x03//),
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x4d02*/, 0xca//),
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x4d03*/, 0xd7//),
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x4d04*/, 0xae//),
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x4d05*/, 0x13),

        SENS_INTENT_CMD_CCI_I2C_WR(0x4813, 0x10),
        SENS_INTENT_CMD_CCI_I2C_WR(0x4815, 0x40),
        SENS_INTENT_CMD_CCI_I2C_WR(0x4837, 0x0d),
        SENS_INTENT_CMD_CCI_I2C_WR(0x486e, 0x03),
        SENS_INTENT_CMD_CCI_I2C_WR(0x4b06, 0x00),
        SENS_INTENT_CMD_CCI_I2C_WR(0x4c01, 0xdf),

        SENS_INTENT_CMD_CCI_I2C_WR(0x5000, 0x99//),
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x5001*/, 0x40//),
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x5002*/, 0x00//),
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x5003*/, 0x00//),
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x5004*/, 0x00//),
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x5005*/, 0x05),

        SENS_INTENT_CMD_CCI_I2C_WR(0x501d, 0x00),
        SENS_INTENT_CMD_CCI_I2C_WR(0x501f, 0x06),

        SENS_INTENT_CMD_CCI_I2C_WR(0x5021, 0x00//),
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x5022*/, 0x13),

        SENS_INTENT_CMD_CCI_I2C_WR(0x5058, 0x00),
        SENS_INTENT_CMD_CCI_I2C_WR(0x5200, 0x00),
        SENS_INTENT_CMD_CCI_I2C_WR(0x5201, 0x80),

        SENS_INTENT_CMD_CCI_I2C_WR(0x5204, 0x01//),
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x5205*/, 0x00),

        SENS_INTENT_CMD_CCI_I2C_WR(0x5209, 0x00//),
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x520a*/, 0x80//),
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x520b*/, 0x04//),
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x520c*/, 0x01),

        SENS_INTENT_CMD_CCI_I2C_WR(0x5210, 0x10//),
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x5211*/, 0xa0),

        SENS_INTENT_CMD_CCI_I2C_WR(0x5280, 0x00),
        SENS_INTENT_CMD_CCI_I2C_WR(0x5292, 0x00),

        SENS_INTENT_CMD_CCI_I2C_WR(0x5c80, 0x05//),
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x5c81*/, 0x90//),
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x5c82*/, 0x09//),
        /*SENS_INTENT_CMD_CCI_I2C_WR(0x5c83*/, 0x5f),

        SENS_INTENT_CMD_CCI_I2C_WR(0x5d00, 0x00),
        SENS_INTENT_CMD_CCI_I2C_WR(0x4001, 0x60),
     SENS_INTENT_CMD_CCI_I2C_WR(0x560f, 0xfc
                            /*0x5610*/, 0xf0        //
                            /*0x5611*/, 0x10),      //

     SENS_INTENT_CMD_CCI_I2C_WR(0x562f, 0xfc
                            /*0x5630*/, 0xf0        //
                            /*0x5631*/, 0x10),      //

     SENS_INTENT_CMD_CCI_I2C_WR(0x564f, 0xfc
                            /*0x5650*/, 0xf0        //
                            /*0x5651*/, 0x10),      //

     SENS_INTENT_CMD_CCI_I2C_WR(0x566f, 0xfc
                            /*0x5670*/, 0xf0        //
                            /*0x5671*/, 0x10),      //

        //;isp setting, not necessary for normal raw output
        SENS_INTENT_CMD_CCI_I2C_WR(0x5292, 0x0a),

        SENS_INTENT_CMD_CCI_I2C_WR(0x5001, 0x40),

     SENS_INTENT_CMD_CCI_I2C_WR(0x5568, 0xf8
                            /*0x5569*/, 0x40        //
                            /*0x556a*/, 0x40        //
                            /*0x556b*/, 0x10        //
                            /*0x556c*/, 0x32        //
                            /*0x556d*/, 0x02        //
                            /*0x556e*/, 0x67        //
                            /*0x556f*/, 0x01        //
                            /*0x5570*/, 0x4c),      //

     SENS_INTENT_CMD_CCI_I2C_WR(0x5940, 0x21
                            /*0x5941*/, 0x02        //
                            /*0x5942*/, 0x04        //
                            /*0x5943*/, 0x06        //
                            /*0x5944*/, 0x08        //
                            /*0x5945*/, 0x0c        //
                            /*0x5946*/, 0x0c        //
                            /*0x5947*/, 0x0c        //
                            /*0x5948*/, 0x0c        //
                            /*0x5949*/, 0x18),      //

     SENS_INTENT_CMD_CCI_I2C_WR(0x5b80, 0x0a
                            /*0x5b81*/, 0x0a        //
                            /*0x5b82*/, 0x14        //
                            /*0x5b83*/, 0x1e        //
                            /*0x5b84*/, 0x44        //
                            /*0x5b85*/, 0x4e        //
                            /*0x5b86*/, 0x07        //
                            /*0x5b87*/, 0x07        //
                            /*0x5b88*/, 0x07        //
                            /*0x5b89*/, 0x08        //
                            /*0x5b8a*/, 0x10        //
                            /*0x5b8b*/, 0x12        //
                            /*0x5b8c*/, 0x07        //
                            /*0x5b8d*/, 0x07        //
                            /*0x5b8e*/, 0x07        //
                            /*0x5b8f*/, 0x08        //
                            /*0x5b90*/, 0x10        //
                            /*0x5b91*/, 0x12),      //

    CMD_ENTRY_END(),
};

static const hat_cm_socket_command_entry_t ov13860_stream_off_seq[] = {
    SENS_INTENT_CMD_CCI_I2C_WR(0x0100, 0),
    CMD_ENTRY_END(),
};

static const hat_cm_socket_command_entry_t ov13860_stream_on_seq[] = {
    SENS_INTENT_CMD_CCI_I2C_WR(0x0100, 1),
    CMD_ENTRY_END(),
};

static const hat_cm_socket_command_entry_t ov13860_gph_on_seq[] = {
    SENS_INTENT_CMD_CCI_I2C_WR(0x3208, 0),
    CMD_ENTRY_END(),
};

static const hat_cm_socket_command_entry_t ov13860_gph_off_seq[] = {
    SENS_INTENT_CMD_CCI_I2C_WR(0x3208, 0x10),
    SENS_INTENT_CMD_CCI_I2C_WR(0x3208, 0xa0),
    CMD_ENTRY_END(),
};


static const hat_cm_socket_command_entry_t ov13860_mode0_settings[] = {
    CMD_ENTRY_DUMMY(),
    CMD_ENTRY_END(),
};

static const hat_cm_socket_command_entry_t ov13860_mode1_settings[] = {
    CMD_ENTRY_DUMMY(),
    CMD_ENTRY_END(),
};

#include "dtp_sensor_ov13860_mode_4208x3120x10fps_db.h"
#include "dtp_sensor_ov13860_mode_4208x3120x15fps_db.h"
#include "dtp_sensor_ov13860_mode_4208x3120x20fps_db.h"
#include "dtp_sensor_ov13860_mode_4208x3120x30fps_db.h"

#include "dtp_sensor_ov13860_mode_2104x1560x30fps_db.h"
#include "dtp_sensor_ov13860_mode_2104x1560x10fps_db.h"
#include "dtp_sensor_ov13860_mode_2104x1560x5fps_db.h"
/*
*==============================================================================================================
*               OV13860 timings
*               ---------------
*               mode0
*                                                                                    ----- m-divider        MIPI divider     PCLK
*                                                                                   |      1+0x0303[3:0]    0x0304/4/5/6/7/8
*  PCLK  prediv0  --  prediv              --   multiplier                   ---|                              [2:0]
*              /1/2        1/1.5/2/2.5/3/4/6/8      {0x0301[1:0], 0x0302[7:0]}
*            0x030A[0]     0x0300[2:0]
*
*  SCLK =    prediv0             prediv                 multiplier                 sys_pre_div         sys divider
*              /1/2           1/1.5/2/2.5/3/4/6/8    {0x030C[1:0], 0x030D[7:0]}     1+0x030F[3:0]       /1/1.5/2/2.5/ PLL2_sys_clk
*             0x0311[0]        0x030B[2:0]
*                                                                       3/3.5/4/5
*  PPLN = 0x380C
*  LPFR = 0x380E
*
*  frame_time = SCLK/(PPLN*LPFR)
*  row_time = PPLN/SCLK
*
*==============================================================================================================
*/
//#define OV13860_FPS_4208x3120_10
#define OV13860_FPS_4208x3120_15
//#define OV13860_FPS_4208x3120_20
//#define OV13860_FPS_4208x3120_30

//#define OV13860_FPS_2104x1560_5
//#define OV13860_FPS_2104x1560_10
//#define OV13860_FPS_2104x1560_30

static const hat_sensor_mode_t ov13860_modes[] = {
#ifdef OV13860_FPS_4208x3120_10
    { // ov13860 mode 0
        {0, 0, 4208, 3120}, //hat_rect_t field_of_view;   // Sensor active view area in physical area [pixels]
        {4208, 3120},       //hat_size_t     active;          // Sensor active output pixels [pixels]
        {1, 1},             //hat_size_t     pixels;          // Equivalent of a digital pixel in physical pixels
        {1, 1},             //hat_size_t     aspect;          // Aspect ratio on 1 pixel
        {1.323, 10.0},     //hat_range_float_t fps;          // sensor mode Min/Max fps
        {100, 100000},     //hat_range_t       exposure;        // Min & Max exposure time in [us]
        {
            .fmt.order.all = HAT_PORDBYR_B_Gb_Gr_R,
            .fmt.reserved0 = 0,
            .fmt.xydec     = HAT_XYDEC_NO,
            .fmt.uvpkd     = HAT_UVPKD_PACKED,
            .fmt.pkd       = HAT_PIXPKD_PACKED,
            .fmt.fmt       = HAT_PIXCOL_BAYER,
            .bpc           = 10,
            .compression   = HAT_COMPRESSION_NONE,
        }, // hat_pix_fmt_t  format;           // Color format and pattern
        // uint16                  rotate;     // 0, 90, 180, 270 - Rotate TODO should come from somewhere???
        0,             // uint16  v_flip;      // [ON/OFF]
        0,             // uint16  h_mirror;    // [ON/OFF]
        1.0,           // float   sensitivity; //  mode (binning/skipping/average) sensitivity against full mode
        160000000,     // uint32  pixel_clock; // In Hz
        28500,         // uint32  row_time;    // [ns] - Readout time of line & blanking
        99636,         // uint32  frame_time;  // [us] - Readout time of the frame & blanking
        4560,          // uint32  ppln;        // Initial number PixelsPerLiNe
        3496,          // uint32  lpfr;        // Initial number LinesPerFRame
        1,             // uint32   num_bad_frames;  // Number of bad frames after mode switch
        ov13860_mode4208x3120_10fps_settings,
        ARR_SIZE(ov13860_mode4208x3120_10fps_settings),
        .pipeline_id = 1,       // Use pipeline with scale down
        .mipi_rx_cfg = {
            .lines = 2,
            .rate = 810,
            .data_type = 0x2b,  // RAW 10
            .data_mode = 1,
        },
    },
#endif
#ifdef OV13860_FPS_4208x3120_15
    { // ov13860 mode 1
        {0, 0, 4208, 3120}, //hat_rect_t field_of_view;   // Sensor active view area in physical area [pixels]
        {4208, 3120},       //hat_size_t     active;          // Sensor active output pixels [pixels]
        {1, 1},             //hat_size_t     pixels;          // Equivalent of a digital pixel in physical pixels
        {1, 1},             //hat_size_t     aspect;          // Aspect ratio on 1 pixel
        {1.323, 15.0},     //hat_range_float_t fps;          // sensor mode Min/Max fps
        {100, 100000},     //hat_range_t       exposure;        // Min & Max exposure time in [us]
        {
            .fmt.order.all = HAT_PORDBYR_B_Gb_Gr_R,
            .fmt.reserved0 = 0,
            .fmt.xydec     = HAT_XYDEC_NO,
            .fmt.uvpkd     = HAT_UVPKD_PACKED,
            .fmt.pkd       = HAT_PIXPKD_PACKED,
            .fmt.fmt       = HAT_PIXCOL_BAYER,
            .bpc           = 10,
            .compression   = HAT_COMPRESSION_NONE,
        }, // hat_pix_fmt_t  format;           // Color format and pattern
        // uint16                  rotate;     // 0, 90, 180, 270 - Rotate TODO should come from somewhere???
        0,             // uint16  v_flip;      // [ON/OFF]
        0,             // uint16  h_mirror;    // [ON/OFF]
        1.0,           // float   sensitivity; //  mode (binning/skipping/average) sensitivity against full mode
        240000000,     // uint32  pixel_clock; // In Hz
        19000,         // uint32  row_time;    // [ns] - Readout time of line & blanking
        66424,         // uint32  frame_time;  // [us] - Readout time of the frame & blanking
        4560,          // uint32  ppln;        // Initial number PixelsPerLiNe
        3496,          // uint32  lpfr;        // Initial number LinesPerFRame
        1,             // uint32   num_bad_frames;  // Number of bad frames after mode switch
        ov13860_mode4208x3120_15fps_settings,
        ARR_SIZE(ov13860_mode4208x3120_15fps_settings),
        .pipeline_id = 1,       // Use pipeline with scale down
        .mipi_rx_cfg = {
            .lines = 2,
            .rate = 1200,
            .data_type = 0x2b,  // RAW 10
            .data_mode = 1,
        },
    },
#endif
#ifdef OV13860_FPS_4208x3120_20
    { // ov13860 mode 2
        {0, 0, 4208, 3120}, //hat_rect_t field_of_view;   // Sensor active view area in physical area [pixels]
        {4208, 3120},       //hat_size_t     active;          // Sensor active output pixels [pixels]
        {1, 1},             //hat_size_t     pixels;          // Equivalent of a digital pixel in physical pixels
        {1, 1},             //hat_size_t     aspect;          // Aspect ratio on 1 pixel
        {1.323, 20.0},     //hat_range_float_t fps;          // sensor mode Min/Max fps
        {100, 100000},     //hat_range_t       exposure;        // Min & Max exposure time in [us]
        {
            .fmt.order.all = HAT_PORDBYR_B_Gb_Gr_R,
            .fmt.reserved0 = 0,
            .fmt.xydec     = HAT_XYDEC_NO,
            .fmt.uvpkd     = HAT_UVPKD_PACKED,
            .fmt.pkd       = HAT_PIXPKD_PACKED,
            .fmt.fmt       = HAT_PIXCOL_BAYER,
            .bpc           = 10,
            .compression   = HAT_COMPRESSION_NONE,
        }, // hat_pix_fmt_t  format;           // Color format and pattern
        // uint16                  rotate;     // 0, 90, 180, 270 - Rotate TODO should come from somewhere???
        0,             // uint16  v_flip;      // [ON/OFF]
        0,             // uint16  h_mirror;    // [ON/OFF]
        1.0,           // float   sensitivity; //  mode (binning/skipping/average) sensitivity against full mode
        320000000,     // uint32  pixel_clock; // In Hz
        14250,          // uint32  row_time;    // [ns] - Readout time of line & blanking
        49818,         // uint32  frame_time;  // [us] - Readout time of the frame & blanking
        4560,          // uint32  ppln;        // Initial number PixelsPerLiNe
        3496,          // uint32  lpfr;        // Initial number LinesPerFRame
        1,             // uint32   num_bad_frames;  // Number of bad frames after mode switch
        ov13860_mode4208x3120_20fps_settings,
        ARR_SIZE(ov13860_mode4208x3120_20fps_settings),
        .pipeline_id = 1,       // Use pipeline with scale down
        .mipi_rx_cfg = {
            .lines = 4,
            .rate = 1200,
            .data_type = 0x2b,  // RAW 10
            .data_mode = 1,
        },
    },
#endif
#ifdef OV13860_FPS_4208x3120_30
    { // ov13860 mode 3
        {0, 0, 4208, 3120}, //hat_rect_t field_of_view;   // Sensor active view area in physical area [pixels]
        {4208, 3120},       //hat_size_t     active;          // Sensor active output pixels [pixels]
        {1, 1},             //hat_size_t     pixels;          // Equivalent of a digital pixel in physical pixels
        {1, 1},             //hat_size_t     aspect;          // Aspect ratio on 1 pixel
        {1.323, 30.0},     //hat_range_float_t fps;          // sensor mode Min/Max fps
        {20, 33000},     //hat_range_t       exposure;        // Min & Max exposure time in [us]
        {
            .fmt.order.all = HAT_PORDBYR_B_Gb_Gr_R,
            .fmt.reserved0 = 0,
            .fmt.xydec     = HAT_XYDEC_NO,
            .fmt.uvpkd     = HAT_UVPKD_PACKED,
            .fmt.pkd       = HAT_PIXPKD_PACKED,
            .fmt.fmt       = HAT_PIXCOL_BAYER,
            .bpc           = 10,
            .compression   = HAT_COMPRESSION_NONE,
        }, // hat_pix_fmt_t  format;           // Color format and pattern
        // uint16                  rotate;     // 0, 90, 180, 270 - Rotate TODO should come from somewhere???
        0,             // uint16  v_flip;      // [ON/OFF]
        0,             // uint16  h_mirror;    // [ON/OFF]
        1.0,           // float   sensitivity; //  mode (binning/skipping/average) sensitivity against full mode
        320000000,     // uint32  pixel_clock; // In Hz
        9500,          // uint32  row_time;    // [ns] - Readout time of line & blanking
        33212,         // uint32  frame_time;  // [us] - Readout time of the frame & blanking
        4560,          // uint32  ppln;        // Initial number PixelsPerLiNe
        3496,          // uint32  lpfr;        // Initial number LinesPerFRame
        1,             // uint32   num_bad_frames;  // Number of bad frames after mode switch
        ov13860_mode4208x3120_30fps_settings,
        ARR_SIZE(ov13860_mode4208x3120_30fps_settings),
        .pipeline_id = 1,       // Use pipeline with scale down
        .mipi_rx_cfg = {
            .lines = 4,
            .rate = 1200,
            .data_type = 0x2b,  // RAW 10
            .data_mode = 1,
        },
    },
#endif // OV13860_FPS_4208x3120_30
#ifdef OV13860_FPS_2104x1560_30
    { // ov13860 mode 3
        {0, 0, 2104, 1560}, //hat_rect_t field_of_view;   // Sensor active view area in physical area [pixels]
        {2104, 1560},       //hat_size_t     active;          // Sensor active output pixels [pixels]
        {1, 1},             //hat_size_t     pixels;          // Equivalent of a digital pixel in physical pixels
        {1, 1},             //hat_size_t     aspect;          // Aspect ratio on 1 pixel
        {1.323, 30.0},     //hat_range_float_t fps;          // sensor mode Min/Max fps
        {100, 100000},     //hat_range_t       exposure;        // Min & Max exposure time in [us]
        {
            .fmt.order.all = HAT_PORDBYR_B_Gb_Gr_R,
            .fmt.reserved0 = 0,
            .fmt.xydec     = HAT_XYDEC_NO,
            .fmt.uvpkd     = HAT_UVPKD_PACKED,
            .fmt.pkd       = HAT_PIXPKD_PACKED,
            .fmt.fmt       = HAT_PIXCOL_BAYER,
            .bpc           = 10,
            .compression   = HAT_COMPRESSION_NONE,
        }, // hat_pix_fmt_t  format;           // Color format and pattern
        // uint16                  rotate;     // 0, 90, 180, 270 - Rotate TODO should come from somewhere???
        0,             // uint16  v_flip;      // [ON/OFF]
        0,             // uint16  h_mirror;    // [ON/OFF]
        1.0,           // float   sensitivity; //  mode (binning/skipping/average) sensitivity against full mode
        240000000,     // uint32  pixel_clock; // In Hz
        19000,         // uint32  row_time;    // [ns] - Readout time of line & blanking
        33288,         // uint32  frame_time;  // [us] - Readout time of the frame & blanking
        4560,          // uint32  ppln;        // Initial number PixelsPerLiNe
        1752    ,          // uint32  lpfr 0x380E;        // Initial number LinesPerFRame
        1,             // uint32   num_bad_frames;  // Number of bad frames after mode switch
        ov13860_mode2104x1560_30fps_settings,
        ARR_SIZE(ov13860_mode2104x1560_30fps_settings),
        .pipeline_id = 1,       // Use pipeline with scale down
        .mipi_rx_cfg = {
            .lines = 2,
            .rate = 1200,
            .data_type = 0x2b,  // RAW 10
            .data_mode = 1,
        },
    },
#endif
#ifdef OV13860_FPS_2104x1560_10
    { // ov13860 mode 4
        {0, 0, 2104, 1560}, //hat_rect_t field_of_view;   // Sensor active view area in physical area [pixels]
        {2104, 1560},       //hat_size_t     active;          // Sensor active output pixels [pixels]
        {1, 1},             //hat_size_t     pixels;          // Equivalent of a digital pixel in physical pixels
        {1, 1},             //hat_size_t     aspect;          // Aspect ratio on 1 pixel
        {1.323, 10.0},     //hat_range_float_t fps;          // sensor mode Min/Max fps
        {100, 100000},     //hat_range_t       exposure;        // Min & Max exposure time in [us]
        {
            .fmt.order.all = HAT_PORDBYR_B_Gb_Gr_R,
            .fmt.reserved0 = 0,
            .fmt.xydec     = HAT_XYDEC_NO,
            .fmt.uvpkd     = HAT_UVPKD_PACKED,
            .fmt.pkd       = HAT_PIXPKD_PACKED,
            .fmt.fmt       = HAT_PIXCOL_BAYER,
            .bpc           = 10,
            .compression   = HAT_COMPRESSION_NONE,
        }, // hat_pix_fmt_t  format;           // Color format and pattern
        // uint16                  rotate;     // 0, 90, 180, 270 - Rotate TODO should come from somewhere???
        0,             // uint16  v_flip;      // [ON/OFF]
        0,             // uint16  h_mirror;    // [ON/OFF]
        1.0,           // float   sensitivity; //  mode (binning/skipping/average) sensitivity against full mode
        240000000,     // uint32  pixel_clock; // In Hz
        19000,         // uint32  row_time;    // [ns] - Readout time of line & blanking
        99864,         // uint32  frame_time;  // [us] - Readout time of the frame & blanking
        4560,          // uint32  ppln 0x380C;        // Initial number PixelsPerLiNe
        5256,          // uint32  lpfr 0x380E;        // Initial number LinesPerFRame
        1,             // uint32   num_bad_frames;  // Number of bad frames after mode switch
        ov13860_mode2104x1560_10fps_settings,
        ARR_SIZE(ov13860_mode2104x1560_10fps_settings),
        .pipeline_id = 1,       // Use pipeline with scale down
        .mipi_rx_cfg = {
            .lines = 2,
            .rate = 1200,
            .data_type = 0x2b,  // RAW 10
            .data_mode = 1,
        },
    },
#endif
#ifdef OV13860_FPS_2104x1560_5
    { // ov13860 mode 4
        {0, 0, 2104, 1560}, //hat_rect_t field_of_view;   // Sensor active view area in physical area [pixels]
        {2104, 1560},       //hat_size_t     active;          // Sensor active output pixels [pixels]
        {1, 1},             //hat_size_t     pixels;          // Equivalent of a digital pixel in physical pixels
        {1, 1},             //hat_size_t     aspect;          // Aspect ratio on 1 pixel
        {1.323, 5.0},     //hat_range_float_t fps;          // sensor mode Min/Max fps
        {100, 100000},     //hat_range_t       exposure;        // Min & Max exposure time in [us]
        {
            .fmt.order.all = HAT_PORDBYR_B_Gb_Gr_R,
            .fmt.reserved0 = 0,
            .fmt.xydec     = HAT_XYDEC_NO,
            .fmt.uvpkd     = HAT_UVPKD_PACKED,
            .fmt.pkd       = HAT_PIXPKD_PACKED,
            .fmt.fmt       = HAT_PIXCOL_BAYER,
            .bpc           = 10,
            .compression   = HAT_COMPRESSION_NONE,
        }, // hat_pix_fmt_t  format;           // Color format and pattern
        // uint16                  rotate;     // 0, 90, 180, 270 - Rotate TODO should come from somewhere???
        0,             // uint16  v_flip;      // [ON/OFF]
        0,             // uint16  h_mirror;    // [ON/OFF]
        1.0,           // float   sensitivity; //  mode (binning/skipping/average) sensitivity against full mode
        240000000,     // uint32  pixel_clock; // In Hz
        19000,         // uint32  row_time;    // [ns] - Readout time of line & blanking
        199728,        // uint32  frame_time;  // [us] - Readout time of the frame & blanking
        4560,          // uint32  ppln[0x380C];        // Initial number PixelsPerLiNe
        10512,         // uint32  lpfr[0x380E];        // Initial number LinesPerFRame
        1,             // uint32   num_bad_frames;  // Number of bad frames after mode switch
        ov13860_mode2104x1560_5fps_settings,
        ARR_SIZE(ov13860_mode2104x1560_5fps_settings),
        .pipeline_id = 1,       // Use pipeline with scale down
        .mipi_rx_cfg = {
            .lines = 2,
            .rate = 1200,
            .data_type = 0x2b,  // RAW 10
            .data_mode = 1,
        },
    },
#endif
};

static const hat_dtp_sensor_desc_t dtp_sensor_ov13860_db =
{ // OV13860 sensor
    .sensor_id          = DTP_SEN_OV13860,
    .sensor_name        = "OV13860 sensor",
    .sensor_features    = {
        {4208, 3120},         //hat_size_t          array_size;     // Physical area (valid + black pixels)
        {0, 0, 4208, 3120},   //hat_rect_t          valid_pixels;   // Sensor full area in physical area
         100,                 //uint32              base_ISO;       // real sensor sensitivity with gain = 1 [U32Q16]
        {1.0, 1.0, 16.0, 0.1}, //hat_bounds_float_t again;          // min/max sensor gain analog
        {1.0, 1.0,  1.0, 0.1},// hat_bounds_float_t dgain;          // min/max sensor gain dnalog
        0,                    //uint32              exp_gain_delay; // exposure to gain delay [frames]
        2,                    //uint32              global_delay;   // delay to output frame with applied parameters [frames]
        {   //const hat_sensor_operations_t operations;
            .pwr_off = {
                ARR_SIZE(ov13860_power_off_seq),
                ov13860_power_off_seq,
            },
            .pwr_on = {
                ARR_SIZE(ov13860_power_on_seq),
                ov13860_power_on_seq,
            },
            .init = {
                [SENSOR_INIT_1] = {
                    ARR_SIZE(ov13860_init_seq),
                    ov13860_init_seq,
                    24000000,
                },
                [SENSOR_INIT_2] = {},
                [SENSOR_INIT_3] = {},
            },
            .stream_off = {
                ARR_SIZE(ov13860_stream_off_seq),
                ov13860_stream_off_seq,
            },
            .stream_on = {
                ARR_SIZE(ov13860_stream_on_seq),
                ov13860_stream_on_seq,
            },
            .gph_on = {
                ARR_SIZE(ov13860_gph_on_seq),
                ov13860_gph_on_seq,
            },
            .gph_off = {
                ARR_SIZE(ov13860_gph_off_seq),
                ov13860_gph_off_seq,
            },
        },
        {
            ARR_SIZE(ov13860_modes), // uint32             num;   // number of sensor modes
            ov13860_modes // list with sensor modes
        },
        1, //uint32  powerup_bad_frames;  // Number of bad frames after powerup
        1, //uint8 temp_sensor_supp;    // If sensor support temperature sensor - 1. If no - 0.
        //HAL3 specific data.
        {100, 1600}, //int32                   sensitivity_range[2];     // Range of valid sensitivities Min <= 100, Max >= 1600
        {.all = HAT_PORDBYR_B_Gb_Gr_R}, //hat_pix_order_t         color_filter_arrangement; // Arrangement of color filters on sensor TODO we have same info in sens mode descriptor
        {100000, 100000000},//TODO, //int64  exposure_time_range[2];  // Range of valid exposure times in [nanoseconds]
        100000000,//TODO, //int64  max_frame_duration; // Maximum possible frame duration in [nanoseconds].
        {5.5541, 4.1134}, //float                  physical_size[2];         // The physical dimensions of the full pixel array in [mm]
        1023, //int32                   white_level;              // Maximum raw value output by sensor
        {1, 1}, //hat_fract_t             base_gain_factor;         // Gain factor from electrons to raw units when ISO=100
        {42, 42, 42, 42}, //int32                   black_level_pattern[4];   // A fixed black level offset for each of the Bayer mosaic channels.
        {{0, 0}}, //hat_fract_t             calib_transform_1[9];     // Per-device calibration on top of color space transform 1
        {{0, 0}}, //hat_fract_t             calib_transform_2[9];     // Per-device calibration on top of color space transform 2
        {{0, 0}}, //hat_fract_t             color_transform_1[9];     // Linear mapping from XYZ (D50) color space to reference linear sensor color.
        {{0, 0}}, //hat_fract_t             color_transform_2[9];     // Linear mapping from XYZ (D50) color space to reference linear sensor color.
        {{0, 0}}, //hat_fract_t             forward_matrix_1[9];      // Used by DNG for better WB adaptation.
        {{0, 0}}, //hat_fract_t             forward_matrix_2[9];      // Used by DNG for better WB adaptation.
        800, //int32                   max_analog_sensitivity;    // Maximum sensitivity that is implemented purely through analog gain.
        {0.0}, //float                  noise_model_coef[2];      // Estimation of sensor noise characteristics.
        0, //int32                   orientation;              // degrees clockwise rotation TODO we have same info in sens mode descriptor
        REF_ILL_NONE, //reference_illuminant_t  reference_illuminant_1;   // Light source used to define transform 1.
        REF_ILL_NONE, //reference_illuminant_t  reference_illuminant_2;   // Light source used to define transform 2.
        .frame_size_regs = {
            .frm_length_lines = 0x380e,
            .frm_length_lines_size = 2,
            .line_length_pck = 0x380c,
            .line_length_pck_size = 2,
        },
    }
};

#define DTP_SENSOR_OV13860_DB_SIZE ( \
    sizeof(dtp_sensor_ov13860_db) \
)

#endif //__DTP_SENSOR_OV13860_DB_H__
