
//OV13860
//INCK:24MHz
//MIPI:2lane
//Data Rate:810Mbps/lane
//Full(4208x3120),10fps,2lane
//H : 4208
//V : 3120
static const hat_cm_socket_command_entry_t ov13860_mode4208x3120_10fps_settings[] = {
    SENS_INTENT_CMD_CCI_I2C_WR(0x0300, 0x05),
    
    SENS_INTENT_CMD_CCI_I2C_WR(0x0302, 0x87//),
    /*SENS_INTENT_CMD_CCI_I2C_WR(0x0303*/, 0x00), //init
    
    SENS_INTENT_CMD_CCI_I2C_WR(0x030f, 0x0b),
    SENS_INTENT_CMD_CCI_I2C_WR(0x3012, 0x21),  //MIPI 2 lanes ; phy_pad_en
    
    SENS_INTENT_CMD_CCI_I2C_WR(0x3501, 0x0d//), //init
    /*SENS_INTENT_CMD_CCI_I2C_WR(0x3502*/, 0x88), //init

    SENS_INTENT_CMD_CCI_I2C_WR(0x370a, 0x23), //init
    SENS_INTENT_CMD_CCI_I2C_WR(0x372f, 0xa0), //init
    
    SENS_INTENT_CMD_CCI_I2C_WR(0x3808, 0x10//), //init
    /*SENS_INTENT_CMD_CCI_I2C_WR(0x3809*/, 0x70//),
    /*SENS_INTENT_CMD_CCI_I2C_WR(0x380a*/, 0x0c//), //init
    /*SENS_INTENT_CMD_CCI_I2C_WR(0x380b*/, 0x30), //init
    
    SENS_INTENT_CMD_CCI_I2C_WR(0x380e, 0x0d//), //init
    /*SENS_INTENT_CMD_CCI_I2C_WR(0x380f*/, 0xa8), //init
    
    SENS_INTENT_CMD_CCI_I2C_WR(0x3813, 0x04), //init
    SENS_INTENT_CMD_CCI_I2C_WR(0x3815, 0x11), //init
    SENS_INTENT_CMD_CCI_I2C_WR(0x3842, 0x00), //init

    SENS_INTENT_CMD_CCI_I2C_WR(0x4008, 0x00//), //init
    /*SENS_INTENT_CMD_CCI_I2C_WR(0x4009*/, 0x13), //init
    
    SENS_INTENT_CMD_CCI_I2C_WR(0x4019, 0x18), //init

    SENS_INTENT_CMD_CCI_I2C_WR(0x4051, 0x03), //init
    SENS_INTENT_CMD_CCI_I2C_WR(0x4066, 0x04), //init
    SENS_INTENT_CMD_CCI_I2C_WR(0x4837, 0x14),
    
    SENS_INTENT_CMD_CCI_I2C_WR(0x5000, 0x99), //init
    SENS_INTENT_CMD_CCI_I2C_WR(0x5201, 0x80), //init
    
    SENS_INTENT_CMD_CCI_I2C_WR(0x5204, 0x01//), //init
    /*SENS_INTENT_CMD_CCI_I2C_WR(0x5205*/, 0x00), //init
    CMD_ENTRY_END(),
};
