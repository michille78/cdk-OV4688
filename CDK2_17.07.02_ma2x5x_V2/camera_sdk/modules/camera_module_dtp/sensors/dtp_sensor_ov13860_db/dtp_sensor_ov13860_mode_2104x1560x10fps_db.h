
//OV13860
//INCK:24MHz
//MIPI:2lane
//Data Rate:1200Mbps/lane
//Full(2104x1560),10fps,2lane
//H : 2104
//V : 1560
static const hat_cm_socket_command_entry_t ov13860_mode2104x1560_10fps_settings[] = {                
    SENS_INTENT_CMD_CCI_I2C_WR(0x0300, 0x00), //default
    
    SENS_INTENT_CMD_CCI_I2C_WR(0x0302, 0x32//), //init
    /*SENS_INTENT_CMD_CCI_I2C_WR(0x0303*/, 0x00),
    
    SENS_INTENT_CMD_CCI_I2C_WR(0x030f, 0x07),
    SENS_INTENT_CMD_CCI_I2C_WR(0x3012, 0x21),
    
    SENS_INTENT_CMD_CCI_I2C_WR(0x3501, 0x06//),
    /*SENS_INTENT_CMD_CCI_I2C_WR(0x3502*/, 0xb8),
    
    SENS_INTENT_CMD_CCI_I2C_WR(0x370a, 0x63),
    SENS_INTENT_CMD_CCI_I2C_WR(0x372f, 0x90),
    
    SENS_INTENT_CMD_CCI_I2C_WR(0x3808, 0x08//),
    /*SENS_INTENT_CMD_CCI_I2C_WR(0x3809*/, 0x38//),
    /*SENS_INTENT_CMD_CCI_I2C_WR(0x380a*/, 0x06//),
    /*SENS_INTENT_CMD_CCI_I2C_WR(0x380b*/, 0x18),
    
    SENS_INTENT_CMD_CCI_I2C_WR(0x380e, 0x14//),
    /*SENS_INTENT_CMD_CCI_I2C_WR(0x380f*/, 0x88),
    
    SENS_INTENT_CMD_CCI_I2C_WR(0x3813, 0x06),
    SENS_INTENT_CMD_CCI_I2C_WR(0x3815, 0x31),
    SENS_INTENT_CMD_CCI_I2C_WR(0x3842, 0x40),
    
    SENS_INTENT_CMD_CCI_I2C_WR(0x4008, 0x02//),
    /*SENS_INTENT_CMD_CCI_I2C_WR(0x4009*/, 0x09),
    
    SENS_INTENT_CMD_CCI_I2C_WR(0x4019, 0x0c),
    SENS_INTENT_CMD_CCI_I2C_WR(0x4051, 0x01),
    SENS_INTENT_CMD_CCI_I2C_WR(0x4066, 0x02),
    SENS_INTENT_CMD_CCI_I2C_WR(0x4837, 0x0d),
    
    SENS_INTENT_CMD_CCI_I2C_WR(0x5000, 0xd9),
    SENS_INTENT_CMD_CCI_I2C_WR(0x5201, 0x71),
    
    SENS_INTENT_CMD_CCI_I2C_WR(0x5204, 0x00//),
    /*SENS_INTENT_CMD_CCI_I2C_WR(0x5205*/, 0x80),
    CMD_ENTRY_END(),
};
