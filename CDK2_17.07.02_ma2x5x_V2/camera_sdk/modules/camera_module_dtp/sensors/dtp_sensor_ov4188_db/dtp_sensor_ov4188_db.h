/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file dtp_sensor_ov4188_db.h
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#ifndef __DTP_SENSOR_OV4188_DB_H__
#define __DTP_SENSOR_OV4188_DB_H__

#include "dtp_sensor_ov4188_mode_2688x1520x30fps_db.h"
#include "dtp_sensor_ov4188_mode_2688x1520x60fps_db.h"
#include "dtp_sensor_ov4188_mode_2688x1520x90fps_db.h"

#include "dtp_sensor_ov4188_mode_2016x1520x30fps_db.h"
#include "dtp_sensor_ov4188_mode_2016x1520x60fps_db.h"

#include "dtp_sensor_ov4188_mode_2120x1196x30fps_db.h"
#include "dtp_sensor_ov4188_mode_2120x1196x75fps_db.h"
#include "dtp_sensor_ov4188_mode_2120x1196x120fps_db.h"

#include "dtp_sensor_ov4188_mode_1920x1080x30fps_db.h"
#include "dtp_sensor_ov4188_mode_1920x1080x60fps_db.h"
//
#include "dtp_sensor_ov4188_mode_1344x760x58.5fps_db.h"

//#define OV4188_FPS_2688x1520_30 //OK
//#define OV4188_FPS_2688x1520_60
//#define OV4188_FPS_2688x1520_90

//#define OV4188_FPS_2016x1520_30 //OK
//#define OV4188_FPS_2016x1520_60
//#define OV4188_FPS_2016x1520_90

//#define OV4188_FPS_2120x1196_37 //OK
//#define OV4188_FPS_2120x1196_4LANE_75P_48P
//#define OV4188_FPS_2120x1196_120

#define OV4188_FPS_1920x1080_30 //OK
//#define OV4188_FPS_1920x1080_60_2lane
//#define OV4188_FPS_1920x1080_120

//#define OV4188_FPS_1344x760_58_5

// ============================ OV13860 camera module =========================

static const hat_cm_socket_command_entry_t ov4188_power_off_seq[] = {
    SENS_INTENT_ACTION(SEN_RSRC_XSHTDWN, HAT_HW_RES_OFF), //XSHDN#
    SENS_DELAY(1000),
    CMD_ENTRY_END(),
};

static const hat_cm_socket_command_entry_t ov4188_power_on_seq[] = {
//    SENS_INTENT_ACTION(SEN_RSRC_CLOCK, HAT_HW_RES_ON),  // ENABLE CLOCK

	SENS_INTENT_ACTION(SEN_RSRC_POWER_1, HAT_HW_RES_ON),  //POWER
	SENS_INTENT_ACTION(SEN_RSRC_CLOCK, HAT_HW_RES_ON),  // ENABLE CLOCK
	SENS_DELAY(1000),
    SENS_INTENT_ACTION(SEN_RSRC_XSHTDWN, HAT_HW_RES_ON), //XSHDN#
    SENS_DELAY(1000),
    CMD_ENTRY_END(),
};

//
static const hat_cm_socket_command_entry_t ov4188_init_seq[] = {
    CMD_ENTRY_END(),
};

static const hat_cm_socket_command_entry_t ov4188_stream_off_seq[] = {
    SENS_INTENT_CMD_CCI_I2C_WR(0x0100, 0),
    CMD_ENTRY_END(),
};

static const hat_cm_socket_command_entry_t ov4188_stream_on_seq[] = {

#ifdef OV4188_FPS_2688x1520_30
		SENS_INTENT_CMD_CCI_I2C_WR(0x3208, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x0103, 0x01),

		SENS_INTENT_CMD_CCI_I2C_WR(0x3638, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x0300, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x0302, 0x2a),
		SENS_INTENT_CMD_CCI_I2C_WR(0x0303, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x0304, 0x03),
		SENS_INTENT_CMD_CCI_I2C_WR(0x030b, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x030d, 0x1e),
		SENS_INTENT_CMD_CCI_I2C_WR(0x030e, 0x04),
		SENS_INTENT_CMD_CCI_I2C_WR(0x030f, 0x01),
		SENS_INTENT_CMD_CCI_I2C_WR(0x0312, 0x01),
		SENS_INTENT_CMD_CCI_I2C_WR(0x031e, 0x00),


		SENS_INTENT_CMD_CCI_I2C_WR(0x3000, 0x00),	//0x00
		SENS_INTENT_CMD_CCI_I2C_WR(0x3002, 0x80), 

		SENS_INTENT_CMD_CCI_I2C_WR(0x3018, 0x32),

		SENS_INTENT_CMD_CCI_I2C_WR(0x3020, 0x93),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3021, 0x03),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3022, 0x01),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3031, 0x0a),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3305, 0xf1),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3307, 0x04),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3309, 0x29),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3500, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3501, 0x60),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3502, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3503, 0x04),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3504, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3505, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3506, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3507, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3508, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3509, 0x80),
		SENS_INTENT_CMD_CCI_I2C_WR(0x350a, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x350b, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x350c, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x350d, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x350e, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x350f, 0x80),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3510, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3511, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3512, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3513, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3514, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3515, 0x80),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3516, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3517, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3518, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3519, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x351a, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x351b, 0x80),
		SENS_INTENT_CMD_CCI_I2C_WR(0x351c, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x351d, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x351e, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x351f, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3520, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3521, 0x80),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3522, 0x08),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3524, 0x08),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3526, 0x08),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3528, 0x08),
		SENS_INTENT_CMD_CCI_I2C_WR(0x352a, 0x08),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3602, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3604, 0x02),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3605, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3606, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3607, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3609, 0x12),
		SENS_INTENT_CMD_CCI_I2C_WR(0x360a, 0x40),
		SENS_INTENT_CMD_CCI_I2C_WR(0x360c, 0x08),
		SENS_INTENT_CMD_CCI_I2C_WR(0x360f, 0xe5),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3608, 0x8f),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3611, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3613, 0xf7),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3616, 0x58),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3619, 0x99),
		SENS_INTENT_CMD_CCI_I2C_WR(0x361b, 0x60),
		SENS_INTENT_CMD_CCI_I2C_WR(0x361c, 0x7a),
		SENS_INTENT_CMD_CCI_I2C_WR(0x361e, 0x79),
		SENS_INTENT_CMD_CCI_I2C_WR(0x361f, 0x02),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3632, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3633, 0x10),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3634, 0x10),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3635, 0x10),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3636, 0x15),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3646, 0x86),
		SENS_INTENT_CMD_CCI_I2C_WR(0x364a, 0x0b),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3700, 0x17),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3701, 0x22),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3703, 0x10),
		SENS_INTENT_CMD_CCI_I2C_WR(0x370a, 0x37),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3705, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3706, 0x63),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3709, 0x3c),
		SENS_INTENT_CMD_CCI_I2C_WR(0x370b, 0x01),
		SENS_INTENT_CMD_CCI_I2C_WR(0x370c, 0x30),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3710, 0x24),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3711, 0x0c),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3716, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3720, 0x28),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3729, 0x7b),
		SENS_INTENT_CMD_CCI_I2C_WR(0x372a, 0x84),
		SENS_INTENT_CMD_CCI_I2C_WR(0x372b, 0xbd),
		SENS_INTENT_CMD_CCI_I2C_WR(0x372c, 0xbc),
		SENS_INTENT_CMD_CCI_I2C_WR(0x372e, 0x52),
		SENS_INTENT_CMD_CCI_I2C_WR(0x373c, 0x0e),
		SENS_INTENT_CMD_CCI_I2C_WR(0x373e, 0x33),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3743, 0x10),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3744, 0x88),
		SENS_INTENT_CMD_CCI_I2C_WR(0x374a, 0x43),
		SENS_INTENT_CMD_CCI_I2C_WR(0x374c, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x374e, 0x23),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3751, 0x7b),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3752, 0x84),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3753, 0xbd),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3754, 0xbc),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3756, 0x52),
		SENS_INTENT_CMD_CCI_I2C_WR(0x375c, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3760, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3761, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3762, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3763, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3764, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3767, 0x04),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3768, 0x04),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3769, 0x08),
		SENS_INTENT_CMD_CCI_I2C_WR(0x376a, 0x08),
		SENS_INTENT_CMD_CCI_I2C_WR(0x376b, 0x20),
		SENS_INTENT_CMD_CCI_I2C_WR(0x376c, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x376d, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x376e, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3773, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3774, 0x51),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3776, 0xbd),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3777, 0xbd),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3781, 0x18),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3783, 0x25),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3800, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3801, 0x08),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3802, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3803, 0x04),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3804, 0x0a),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3805, 0x97),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3806, 0x05),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3807, 0xfb),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3808, 0x0a),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3809, 0x80),
		SENS_INTENT_CMD_CCI_I2C_WR(0x380a, 0x05),
		SENS_INTENT_CMD_CCI_I2C_WR(0x380b, 0xf0),
		SENS_INTENT_CMD_CCI_I2C_WR(0x380c, 0x0a),
		SENS_INTENT_CMD_CCI_I2C_WR(0x380d, 0x0a),
		SENS_INTENT_CMD_CCI_I2C_WR(0x380e, 0x06),
		SENS_INTENT_CMD_CCI_I2C_WR(0x380f, 0x14),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3810, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3811, 0x08),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3812, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3813, 0x04),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3814, 0x01),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3815, 0x01),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3819, 0x01),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3820, 0x00),

		SENS_INTENT_CMD_CCI_I2C_WR(0x3821, 0x06),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3829, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x382a, 0x01),
		SENS_INTENT_CMD_CCI_I2C_WR(0x382b, 0x01),
		SENS_INTENT_CMD_CCI_I2C_WR(0x382d, 0x7f),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3830, 0x04),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3836, 0x01),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3841, 0x02),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3846, 0x08),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3847, 0x07),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3d85, 0x36),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3d8c, 0x71),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3d8d, 0xcb),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3f0a, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x4000, 0x71),
		SENS_INTENT_CMD_CCI_I2C_WR(0x4001, 0x40),
		SENS_INTENT_CMD_CCI_I2C_WR(0x4002, 0x04),
		SENS_INTENT_CMD_CCI_I2C_WR(0x4003, 0x14),
		SENS_INTENT_CMD_CCI_I2C_WR(0x400e, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x4011, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x401a, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x401b, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x401c, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x401d, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x401f, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x4020, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x4021, 0x10),
		SENS_INTENT_CMD_CCI_I2C_WR(0x4022, 0x07),
		SENS_INTENT_CMD_CCI_I2C_WR(0x4023, 0xcf),
		SENS_INTENT_CMD_CCI_I2C_WR(0x4024, 0x09),
		SENS_INTENT_CMD_CCI_I2C_WR(0x4025, 0x60),
		SENS_INTENT_CMD_CCI_I2C_WR(0x4026, 0x09),
		SENS_INTENT_CMD_CCI_I2C_WR(0x4027, 0x6f),
		SENS_INTENT_CMD_CCI_I2C_WR(0x4028, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x4029, 0x02),
		SENS_INTENT_CMD_CCI_I2C_WR(0x402a, 0x06),
		SENS_INTENT_CMD_CCI_I2C_WR(0x402b, 0x04),
		SENS_INTENT_CMD_CCI_I2C_WR(0x402c, 0x02),
		SENS_INTENT_CMD_CCI_I2C_WR(0x402d, 0x02),
		SENS_INTENT_CMD_CCI_I2C_WR(0x402e, 0x0e),
		SENS_INTENT_CMD_CCI_I2C_WR(0x402f, 0x04),
		SENS_INTENT_CMD_CCI_I2C_WR(0x4302, 0xff),
		SENS_INTENT_CMD_CCI_I2C_WR(0x4303, 0xff),
		SENS_INTENT_CMD_CCI_I2C_WR(0x4304, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x4305, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x4306, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x4308, 0x02),
		SENS_INTENT_CMD_CCI_I2C_WR(0x4500, 0x6c),
		SENS_INTENT_CMD_CCI_I2C_WR(0x4501, 0xc4),
		SENS_INTENT_CMD_CCI_I2C_WR(0x4502, 0x40),
		SENS_INTENT_CMD_CCI_I2C_WR(0x4503, 0x02),
		SENS_INTENT_CMD_CCI_I2C_WR(0x4601, 0xA7),
		SENS_INTENT_CMD_CCI_I2C_WR(0x4800, 0x04),
		SENS_INTENT_CMD_CCI_I2C_WR(0x4813, 0x08),
		SENS_INTENT_CMD_CCI_I2C_WR(0x481f, 0x40),
		SENS_INTENT_CMD_CCI_I2C_WR(0x4829, 0x78),
		SENS_INTENT_CMD_CCI_I2C_WR(0x4837, 0x10),
		SENS_INTENT_CMD_CCI_I2C_WR(0x4b00, 0x2a),
		SENS_INTENT_CMD_CCI_I2C_WR(0x4b0d, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x4d00, 0x04),
		SENS_INTENT_CMD_CCI_I2C_WR(0x4d01, 0x42),
		SENS_INTENT_CMD_CCI_I2C_WR(0x4d02, 0xd1),
		SENS_INTENT_CMD_CCI_I2C_WR(0x4d03, 0x93),
		SENS_INTENT_CMD_CCI_I2C_WR(0x4d04, 0xf5),
		SENS_INTENT_CMD_CCI_I2C_WR(0x4d05, 0xc1),
		SENS_INTENT_CMD_CCI_I2C_WR(0x5000, 0xf3),
		SENS_INTENT_CMD_CCI_I2C_WR(0x5001, 0x11),
		SENS_INTENT_CMD_CCI_I2C_WR(0x5004, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x500a, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x500b, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x5032, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x5040, 0x00),  // 0x00, //0x82 //test pattern
		SENS_INTENT_CMD_CCI_I2C_WR(0x5050, 0x0c),
		SENS_INTENT_CMD_CCI_I2C_WR(0x5500, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x5501, 0x10),
		SENS_INTENT_CMD_CCI_I2C_WR(0x5502, 0x01),
		SENS_INTENT_CMD_CCI_I2C_WR(0x5503, 0x0f),
		SENS_INTENT_CMD_CCI_I2C_WR(0x8000, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x8001, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x8002, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x8003, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x8004, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x8005, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x8006, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x8007, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x8008, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3638, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3105, 0x31),
		SENS_INTENT_CMD_CCI_I2C_WR(0x301a, 0xf9),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3508, 0x07),
		SENS_INTENT_CMD_CCI_I2C_WR(0x484b, 0x05),
		SENS_INTENT_CMD_CCI_I2C_WR(0x4805, 0x03),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3601, 0x01),
		SENS_INTENT_CMD_CCI_I2C_WR(0x0100, 0x01),

		SENS_INTENT_CMD_CCI_I2C_WR(0x3105, 0x11),
		SENS_INTENT_CMD_CCI_I2C_WR(0x301a, 0xf1),
		SENS_INTENT_CMD_CCI_I2C_WR(0x4805, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x301a, 0xf0),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3208, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x302a, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x302a, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x302a, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x302a, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x302a, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3601, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3638, 0x00),

		SENS_INTENT_CMD_CCI_I2C_WR(0x3823, 0x50),

		SENS_INTENT_CMD_CCI_I2C_WR(0x0100, 1),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3208, 0x10),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3208, 0xa0),
		CMD_ENTRY_END(),
#endif

#ifdef OV4188_FPS_1920x1080_30
		SENS_INTENT_CMD_CCI_I2C_WR(0x3208, 0x00),
        SENS_INTENT_CMD_CCI_I2C_WR(0x0103, 0x01),  //software reset

		SENS_INTENT_CMD_CCI_I2C_WR(0x3638, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x0300, 0x00),
        SENS_INTENT_CMD_CCI_I2C_WR(0x0302, 0x2a),  //PLL1 divm
		SENS_INTENT_CMD_CCI_I2C_WR(0x0303, 0x00),
        SENS_INTENT_CMD_CCI_I2C_WR(0x0304, 0x03),  //PLL1 div mipi =====
		SENS_INTENT_CMD_CCI_I2C_WR(0x030b, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x030d, 0x1e),
		SENS_INTENT_CMD_CCI_I2C_WR(0x030e, 0x04),
        SENS_INTENT_CMD_CCI_I2C_WR(0x030f, 0x01), //PLL
		SENS_INTENT_CMD_CCI_I2C_WR(0x0312, 0x01),
		SENS_INTENT_CMD_CCI_I2C_WR(0x031e, 0x00),

        SENS_INTENT_CMD_CCI_I2C_WR(0x3000, 0x00),  //FSIN input
        SENS_INTENT_CMD_CCI_I2C_WR(0x3002, 0x80),  //Vsync output, HREF input, FREX input, GPIO0 input

        SENS_INTENT_CMD_CCI_I2C_WR(0x3018, 0x32),  //MIPI 4 lane, Reset MIPI PHY when sleep  *****
		SENS_INTENT_CMD_CCI_I2C_WR(0x3020, 0x93),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3021, 0x03),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3022, 0x01),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3031, 0x0a),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3305, 0xf1),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3307, 0x04),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3309, 0x29),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3500, 0x00),
        SENS_INTENT_CMD_CCI_I2C_WR(0x3501, 0x4c),  //long exposure H
		SENS_INTENT_CMD_CCI_I2C_WR(0x3502, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3503, 0x04),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3504, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3505, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3506, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3507, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3508, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3509, 0x80),
		SENS_INTENT_CMD_CCI_I2C_WR(0x350a, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x350b, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x350c, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x350d, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x350e, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x350f, 0x80),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3510, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3511, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3512, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3513, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3514, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3515, 0x80),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3516, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3517, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3518, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3519, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x351a, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x351b, 0x80),
		SENS_INTENT_CMD_CCI_I2C_WR(0x351c, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x351d, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x351e, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x351f, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3520, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3521, 0x80),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3522, 0x08),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3524, 0x08),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3526, 0x08),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3528, 0x08),
		SENS_INTENT_CMD_CCI_I2C_WR(0x352a, 0x08),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3602, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3604, 0x02),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3605, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3606, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3607, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3609, 0x12),
		SENS_INTENT_CMD_CCI_I2C_WR(0x360a, 0x40),
		SENS_INTENT_CMD_CCI_I2C_WR(0x360c, 0x08),
		SENS_INTENT_CMD_CCI_I2C_WR(0x360f, 0xe5),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3608, 0x8f),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3611, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3613, 0xf7),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3616, 0x58),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3619, 0x99),
		SENS_INTENT_CMD_CCI_I2C_WR(0x361b, 0x60),
		SENS_INTENT_CMD_CCI_I2C_WR(0x361c, 0x7a),
		SENS_INTENT_CMD_CCI_I2C_WR(0x361e, 0x79),
		SENS_INTENT_CMD_CCI_I2C_WR(0x361f, 0x02),
        SENS_INTENT_CMD_CCI_I2C_WR(0x3632, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3633, 0x10),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3634, 0x10),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3635, 0x10),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3636, 0x15),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3646, 0x86),
        SENS_INTENT_CMD_CCI_I2C_WR(0x364a, 0x0b),  //ADC & Anolog
		SENS_INTENT_CMD_CCI_I2C_WR(0x3700, 0x17),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3701, 0x22),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3703, 0x10),
		SENS_INTENT_CMD_CCI_I2C_WR(0x370a, 0x37),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3705, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3706, 0x63),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3709, 0x3c),
		SENS_INTENT_CMD_CCI_I2C_WR(0x370b, 0x01),
		SENS_INTENT_CMD_CCI_I2C_WR(0x370c, 0x30),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3710, 0x24),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3711, 0x0c),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3716, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3720, 0x28),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3729, 0x7b),
		SENS_INTENT_CMD_CCI_I2C_WR(0x372a, 0x84),
		SENS_INTENT_CMD_CCI_I2C_WR(0x372b, 0xbd),
		SENS_INTENT_CMD_CCI_I2C_WR(0x372c, 0xbc),
		SENS_INTENT_CMD_CCI_I2C_WR(0x372e, 0x52),
		SENS_INTENT_CMD_CCI_I2C_WR(0x373c, 0x0e),
		SENS_INTENT_CMD_CCI_I2C_WR(0x373e, 0x33),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3743, 0x10),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3744, 0x88),
		SENS_INTENT_CMD_CCI_I2C_WR(0x374a, 0x43),
		SENS_INTENT_CMD_CCI_I2C_WR(0x374c, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x374e, 0x23),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3751, 0x7b),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3752, 0x84),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3753, 0xbd),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3754, 0xbc),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3756, 0x52),
		SENS_INTENT_CMD_CCI_I2C_WR(0x375c, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3760, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3761, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3762, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3763, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3764, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3767, 0x04),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3768, 0x04),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3769, 0x08),
		SENS_INTENT_CMD_CCI_I2C_WR(0x376a, 0x08),
        SENS_INTENT_CMD_CCI_I2C_WR(0x376b, 0x20),  //Sensor control
		SENS_INTENT_CMD_CCI_I2C_WR(0x376c, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x376d, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x376e, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3773, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3774, 0x51),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3776, 0xbd),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3777, 0xbd),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3781, 0x18),
        SENS_INTENT_CMD_CCI_I2C_WR(0x3783, 0x25),
        SENS_INTENT_CMD_CCI_I2C_WR(0x3800, 0x01),  //H crop start H
        SENS_INTENT_CMD_CCI_I2C_WR(0x3801, 0x88),  //H crop start L
        SENS_INTENT_CMD_CCI_I2C_WR(0x3802, 0x00),  //V crop start H
        SENS_INTENT_CMD_CCI_I2C_WR(0x3803, 0xe0),  //V crop start L
        SENS_INTENT_CMD_CCI_I2C_WR(0x3804, 0x09),  //H crop end H
        SENS_INTENT_CMD_CCI_I2C_WR(0x3805, 0x17),  //H crop end L
        SENS_INTENT_CMD_CCI_I2C_WR(0x3806, 0x05),  //V crop end H
        SENS_INTENT_CMD_CCI_I2C_WR(0x3807, 0x1f),  //V crop end L
        SENS_INTENT_CMD_CCI_I2C_WR(0x3808, 0x07),  //H output size H
        SENS_INTENT_CMD_CCI_I2C_WR(0x3809, 0x80),  //H output size L
        SENS_INTENT_CMD_CCI_I2C_WR(0x380a, 0x04),  //V output size H
        SENS_INTENT_CMD_CCI_I2C_WR(0x380b, 0x38),  //V output size L
        SENS_INTENT_CMD_CCI_I2C_WR(0x380c, 0x0a),
        SENS_INTENT_CMD_CCI_I2C_WR(0x380d, 0x0a),
        SENS_INTENT_CMD_CCI_I2C_WR(0x380e, 0x06),
        SENS_INTENT_CMD_CCI_I2C_WR(0x380f, 0x14),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3810, 0x00),
        SENS_INTENT_CMD_CCI_I2C_WR(0x3811, 0x08),  //H win off L
		SENS_INTENT_CMD_CCI_I2C_WR(0x3812, 0x00),
        SENS_INTENT_CMD_CCI_I2C_WR(0x3813, 0x04),  //V win off L
        SENS_INTENT_CMD_CCI_I2C_WR(0x3814, 0x01),  //H inc odd
		SENS_INTENT_CMD_CCI_I2C_WR(0x3815, 0x01),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3819, 0x01),
        SENS_INTENT_CMD_CCI_I2C_WR(0x3820, 0x00),  //flip off, bin off
        SENS_INTENT_CMD_CCI_I2C_WR(0x3821, 0x06),  //mirror on, bin off
		SENS_INTENT_CMD_CCI_I2C_WR(0x3829, 0x00),
        SENS_INTENT_CMD_CCI_I2C_WR(0x382a, 0x01),  //V inc odd
		SENS_INTENT_CMD_CCI_I2C_WR(0x382b, 0x01),
		SENS_INTENT_CMD_CCI_I2C_WR(0x382d, 0x7f),
        SENS_INTENT_CMD_CCI_I2C_WR(0x3830, 0x04),  //blc use num/2
        SENS_INTENT_CMD_CCI_I2C_WR(0x3836, 0x01),  //rzline use num/2
		SENS_INTENT_CMD_CCI_I2C_WR(0x3841, 0x02),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3846, 0x08),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3847, 0x07),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3d85, 0x36),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3d8c, 0x71),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3d8d, 0xcb),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3f0a, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x4000, 0x71),
        SENS_INTENT_CMD_CCI_I2C_WR(0x4001, 0x40),  //debug mode
		SENS_INTENT_CMD_CCI_I2C_WR(0x4002, 0x04),
		SENS_INTENT_CMD_CCI_I2C_WR(0x4003, 0x14),
		SENS_INTENT_CMD_CCI_I2C_WR(0x400e, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x4011, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x401a, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x401b, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x401c, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x401d, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x401f, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x4020, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x4021, 0x10),
        SENS_INTENT_CMD_CCI_I2C_WR(0x4022, 0x06),  //Anchor left end H
        SENS_INTENT_CMD_CCI_I2C_WR(0x4023, 0x13),  //Anchor left end L
        SENS_INTENT_CMD_CCI_I2C_WR(0x4024, 0x07),  //Anchor right start H
        SENS_INTENT_CMD_CCI_I2C_WR(0x4025, 0x40),  //Anchor right start L
        SENS_INTENT_CMD_CCI_I2C_WR(0x4026, 0x07),  //Anchor right end H
        SENS_INTENT_CMD_CCI_I2C_WR(0x4027, 0x50),  //Anchor right end L
		SENS_INTENT_CMD_CCI_I2C_WR(0x4028, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x4029, 0x02),
		SENS_INTENT_CMD_CCI_I2C_WR(0x402a, 0x06),
		SENS_INTENT_CMD_CCI_I2C_WR(0x402b, 0x04),
		SENS_INTENT_CMD_CCI_I2C_WR(0x402c, 0x02),
		SENS_INTENT_CMD_CCI_I2C_WR(0x402d, 0x02),
		SENS_INTENT_CMD_CCI_I2C_WR(0x402e, 0x0e),
		SENS_INTENT_CMD_CCI_I2C_WR(0x402f, 0x04),
		SENS_INTENT_CMD_CCI_I2C_WR(0x4302, 0xff),
		SENS_INTENT_CMD_CCI_I2C_WR(0x4303, 0xff),
		SENS_INTENT_CMD_CCI_I2C_WR(0x4304, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x4305, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x4306, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x4308, 0x02),
		SENS_INTENT_CMD_CCI_I2C_WR(0x4500, 0x6c),
		SENS_INTENT_CMD_CCI_I2C_WR(0x4501, 0xc4),
        SENS_INTENT_CMD_CCI_I2C_WR(0x4502, 0x40),  //ADC sync control
		SENS_INTENT_CMD_CCI_I2C_WR(0x4503, 0x02),
        SENS_INTENT_CMD_CCI_I2C_WR(0x4601, 0x77),  //V fifo read start  ?******
		SENS_INTENT_CMD_CCI_I2C_WR(0x4800, 0x04),
		SENS_INTENT_CMD_CCI_I2C_WR(0x4813, 0x08),
		SENS_INTENT_CMD_CCI_I2C_WR(0x481f, 0x40),
		SENS_INTENT_CMD_CCI_I2C_WR(0x4829, 0x78),
        SENS_INTENT_CMD_CCI_I2C_WR(0x4837, 0x10), //mipi global timing *******
		SENS_INTENT_CMD_CCI_I2C_WR(0x4b00, 0x2a),
		SENS_INTENT_CMD_CCI_I2C_WR(0x4b0d, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x4d00, 0x04),
		SENS_INTENT_CMD_CCI_I2C_WR(0x4d01, 0x42),
		SENS_INTENT_CMD_CCI_I2C_WR(0x4d02, 0xd1),
		SENS_INTENT_CMD_CCI_I2C_WR(0x4d03, 0x93),
		SENS_INTENT_CMD_CCI_I2C_WR(0x4d04, 0xf5),
		SENS_INTENT_CMD_CCI_I2C_WR(0x4d05, 0xc1),
		SENS_INTENT_CMD_CCI_I2C_WR(0x5000, 0xf3),
		SENS_INTENT_CMD_CCI_I2C_WR(0x5001, 0x11),
		SENS_INTENT_CMD_CCI_I2C_WR(0x5004, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x500a, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x500b, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x5032, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x5040, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x5050, 0x0c),
		SENS_INTENT_CMD_CCI_I2C_WR(0x5500, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x5501, 0x10),
		SENS_INTENT_CMD_CCI_I2C_WR(0x5502, 0x01),
		SENS_INTENT_CMD_CCI_I2C_WR(0x5503, 0x0f),
		SENS_INTENT_CMD_CCI_I2C_WR(0x8000, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x8001, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x8002, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x8003, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x8004, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x8005, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x8006, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x8007, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x8008, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3638, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3105, 0x31),
		SENS_INTENT_CMD_CCI_I2C_WR(0x301a, 0xf9),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3508, 0x07),
		SENS_INTENT_CMD_CCI_I2C_WR(0x484b, 0x05),
		SENS_INTENT_CMD_CCI_I2C_WR(0x4805, 0x03),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3601, 0x01),
		SENS_INTENT_CMD_CCI_I2C_WR(0x0100, 0x01),

		SENS_INTENT_CMD_CCI_I2C_WR(0x3105, 0x11),
		SENS_INTENT_CMD_CCI_I2C_WR(0x301a, 0xf1),
		SENS_INTENT_CMD_CCI_I2C_WR(0x4805, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x301a, 0xf0),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3208, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x302a, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x302a, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x302a, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x302a, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x302a, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3601, 0x00),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3638, 0x00),

		SENS_INTENT_CMD_CCI_I2C_WR(0x3823, 0x50),
		SENS_INTENT_CMD_CCI_I2C_WR(0x0100, 1),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3208, 0x10),
		SENS_INTENT_CMD_CCI_I2C_WR(0x3208, 0xa0),

    CMD_ENTRY_END(),
#endif
};

static const hat_cm_socket_command_entry_t ov4188_gph_on_seq[] = {
   // SENS_INTENT_CMD_CCI_I2C_WR(0x3208, 0),
    CMD_ENTRY_END(),
};

static const hat_cm_socket_command_entry_t ov4188_gph_off_seq[] = {
   // SENS_INTENT_CMD_CCI_I2C_WR(0x3208, 0x10),
   // SENS_INTENT_CMD_CCI_I2C_WR(0x3208, 0xa0),
    CMD_ENTRY_END(),
};


static const hat_cm_socket_command_entry_t ov4188_mode0_settings[] = {
    CMD_ENTRY_DUMMY(),
    CMD_ENTRY_END(),
};

static const hat_cm_socket_command_entry_t ov4188_mode1_settings[] = {
    CMD_ENTRY_DUMMY(),
    CMD_ENTRY_END(),
};

/*
*==============================================================================================================
*               OV4188 timings
*               ---------------
*               mode0
*                                                                                    ----- m-divider        MIPI divider     PCLK
*                                                                                   |      1+0x0303[3:0]    0x0304/4/5/6/7/8
*  PCLK  prediv0  --  prediv              --   multiplier                   ---|                              [2:0]
*              /1/2        1/1.5/2/2.5/3/4/6/8      {0x0301[1:0], 0x0302[7:0]}
*            0x030A[0]     0x0300[2:0]
*
*  SCLK =    prediv0             prediv                 multiplier                 sys_pre_div         sys divider
*              /1/2           1/1.5/2/2.5/3/4/6/8    {0x030C[1:0], 0x030D[7:0]}     1+0x030F[3:0]       /1/1.5/2/2.5/ PLL2_sys_clk
*             0x0311[0]        0x030B[2:0]
*                                                                       3/3.5/4/5
*  PPLN = 0x380C
*  LPFR = 0x380E
*
*  frame_time = SCLK/(PPLN*LPFR)
*  row_time = PPLN/SCLK
*
*==============================================================================================================
*/
/*
*==============================================================================================================
* OV4188 timings
* ---------------
*
* Pll1Clk [MHz] = INCK[MHz] / Pll1PreDivp[0x030A] / Pll1PreDiv[0x0300] * Pll1Mul{0x0301[1:0], 0x0302[7:0]}
* MipiClk = Pll1Clk / Pll1Divm{1+0x0303[3:0]}
* PixClk = Pll1Clk / Pll1Divm{1+0x0303[3:0]} / Pll1_div_mipi[0x0304]
*
* Pll2Clk [MHz] = INCK[MHz] / Pll2PreDivp[0x0311] / Pll2PreDiv[0x030B] * Pll1Mul{0x030C[1:0], 0x030D[7:0]}
* SysClk = Pll2Clk / ppl2_divsp[0x030f] / pll2_divs[0x030e]
* ADCClk = Pll1Clk / Pll1ADCDiv[0x312[3:0]
* ------------------------------------------------------------------------------
*
* PLL1Clk[MHz] = 24 / 1 / 1 * 42  = 1008 MHz
* MipiClk = 1008 / 1 = 1008 MHz
* PixClk = 1008 / 1 / 8 = 126 MHz
*
* Pll2Clk[MHz] = 24 / 1 / 1 * 30  = 720 MHz
* SysClk = 720 / 2 / 3 = 120 MHz
* ADCClk = 720 / 2 = 360 MHz
* ------------------------------------------------------------------------------
*
* LPFR (lines per frame)  [0x380E:0x380F] = 0x0612 = 1554
* PPLN (pixel per line)   [0x380C:0x380D] = 0x0a10 = 2576
* ------------------------------------------------------------------------------
*
* frame_time = (PPLN * LPFR) / SysClk
* row_time = PPLN/SCLK
* max_exposure = 1548 * row_time
* ------------------------------------------------------------------------------
*
* frame_time = 2576 * 1554 / 120 = 33359.2 uSec = 33.3592 mSec
* row time = 2576 / 120 = 21.467 uSec
* max_exposure = 1548 * 21.467 = 33230.9 uSec
*===============================================================================
*/

/*
*==============================================================================================================
*               OV4188 timings
*               ---------------
*
*  PLL1          div0            div            mul                div           div
*              --------       --------       --------           --------       --------
*             |        |     |        |     |        |         |        |     |        |
* In Freq ----| 0x030A |-----| 0x0300 |-----| 0x0301 |----+----|        |-----|        |--- SCLK(option)
*             |        |     |        |     | 0x0302 |    |    |        |     |        |
*              --------       --------       --------     |     --------       --------
*                                                         |
*                                                         |       div            div
*                                                         |     --------       --------
*                                                         |    |        |     |        |
*                                                         -----| 0x0303 |--+--| 0x0304 |--- PCLK
*                                                              |        |  |  |        |
*                                                               --------   |   --------
*                                                                          |
*                                                                          ---------------- MIPI_PHY_CLK
*
*
*  PLL2          div0            div            mul                div           div
*              --------       --------       --------           --------       --------
*             |        |     |        |     |        |         |        |     |        |
* In Freq ----| 0x0311 |-----| 0x030B |-----| 0x030C |----+----| 0x030F |-----| 0x030E |--- SCLK(default)
*             |        |     |        |     | 0x030D |    |    |        |     |        |
*              --------       --------       --------     |     --------       --------
*                                                         |
*                                                         |       div
*                                                         |     --------
*                                                         |    |        |
*                                                         -----|        |----- DAC_CLK
*                                                              |        |
*                                                               --------
*
* PPLN (pixel per line)   0x380C:0x380D
* LPFR (lines per frame)  0x380E:0x380F
*
* row_time[uSec] = PPLN / SCLK[MHz]
* frame_time[uSec] = row_time * LPFR
*==============================================================================================================
*               30 fps mode settings
*               ---------------------
*
*  PLL1          div0            div            mul                div           div
*              --------       --------       --------           --------       --------
*             |        |     |        |     |        |         |        |     |        |
* 24 Mhz  ----|   /1   |-----|   /1   |-----|   42   |----+----|        |-----|        |--- SCLK(option)
*             |        |     |        |     |        |    |    |        |     |        |
*              --------       --------       --------     |     --------       --------
*                                                         |
*                                                         |       div            div
*                                                         |     --------       --------
*                                                         |    |        |     |        |
*                                                         -----|   /1   |--+--|   /8   |--- PCLK = 126 Mhz
*                                                              |        |  |  |        |
*                                                               --------   |   --------
*                                                                          |
*                                                                          ---------------- MIPI_PHY_CLK = 1008 Mhz
*
*
*  PLL2          div0            div            mul                div           div
*              --------       --------       --------           --------       --------
*             |        |     |        |     |        |         |        |     |        |
* 24 Mhz  ----|   /1   |-----|   /1   |-----|   30   |----+----|   /2   |-----|   /3   |--- SCLK(default) = 120 Mhz
*             |        |     |        |     |        |    |    |        |     |        |
*              --------       --------       --------     |     --------       --------
*                                                         |
*                                                         |       div
*                                                         |     --------
*                                                         |    |        |
*                                                         -----|        |----- DAC_CLK
*                                                              |        |
*                                                               --------
*                                                 RAW 10         2 lanes
* LPFR (lines per frame)  [0x380E:0x380F] = 0x0612 = 1554
* PPLN (pixel per line)   [0x380C:0x380D] = 0x0a10 = 2576
*
* frame_time = 2576 * 1554 / 120 = 33359.2 uSec = 33.3592 mSec
* row time = 2576 / 120 = 21.467 uSec
* max_exposure = 1548 * 21.467 = 33230.9 uSec
*
**==============================================================================================================
*/

static const hat_sensor_mode_t ov4188_modes[] = {
#ifdef OV4188_FPS_2688x1520_30
    {
        {0, 0, 2688, 1520}, //hat_rect_t field_of_view;   // Sensor active view area in physical area [pixels]
        {2688, 1520},       //hat_size_t     active;          // Sensor active output pixels [pixels]
        {1, 1},             //hat_size_t     pixels;          // Equivalent of a digital pixel in physical pixels
        {1, 1},             //hat_size_t     aspect;          // Aspect ratio on 1 pixel
        {1.323, 30.0},     //hat_range_float_t fps;          // sensor mode Min/Max fps
       {100,  34000},        //hat_range_t       exposure;        // Min & Max exposure time in [us]
        {
            .fmt.order.all = HAT_PORDBYR_B_Gb_Gr_R,
            .fmt.reserved0 = 0,
            .fmt.xydec     = HAT_XYDEC_NO,
            .fmt.uvpkd     = HAT_UVPKD_PACKED,
            .fmt.pkd       = HAT_PIXPKD_PACKED,
            .fmt.fmt       = HAT_PIXCOL_BAYER,
            .bpc           = 10,
            .compression   = HAT_COMPRESSION_NONE,
        }, // hat_pix_fmt_t  format;           // Color format and pattern
        // uint16                  rotate;     // 0, 90, 180, 270 - Rotate TODO should come from somewhere???
        0,             // uint16  v_flip;      // [ON/OFF]
        0,             // uint16  h_mirror;    // [ON/OFF]
        1.0,           // float   sensitivity; //  mode (binning/skipping/average) sensitivity against full mode
 #if 0
        126000000,     // uint32  pixel_clock; // In Hz
        21467 ,          // uint32  row_time;    // [ns] - Readout time of line & blanking
        33359,         // uint32  frame_time;  // [us] - Readout time of the frame & blanking
        2688,          // uint32  ppln;        // Initial number PixelsPerLiNe
        1520,          // uint32  lpfr;        // Initial number LinesPerFRame
#else
        320000000,     // uint32  pixel_clock; // In Hz
        9500,          // uint32  row_time;    // [ns] - Readout time of line & blanking
        33212,         // uint32  frame_time;  // [us] - Readout time of the frame & blanking
        2688,          // uint32  ppln;        // Initial number PixelsPerLiNe
        1520,          // uint32  lpfr;        // Initial number LinesPerFRame		
#endif
        1,             // uint32   num_bad_frames;  // Number of bad frames after mode switch
		ov4188_mode2688x1520_30fps_settings,
        ARR_SIZE(ov4188_mode2688x1520_30fps_settings),
        .pipeline_id = 0,       // Use pipeline with scale down
        .mipi_rx_cfg = {
            .lines = 2,
            .rate = 1008,   //672, //1008, //840,
            .data_type = 0x2b,  // RAW 10
            .data_mode = 0,
        },
    },
#endif

#ifdef OV4188_FPS_2688x1520_60
    {
        {0, 0, 2688, 1520}, //hat_rect_t field_of_view;   // Sensor active view area in physical area [pixels]
        {2688, 1520},       //hat_size_t     active;          // Sensor active output pixels [pixels]
        {1, 1},             //hat_size_t     pixels;          // Equivalent of a digital pixel in physical pixels
        {1, 1},             //hat_size_t     aspect;          // Aspect ratio on 1 pixel
        {1.323, 60.0},     //hat_range_float_t fps;          // sensor mode Min/Max fps
       {100,  16700},        //hat_range_t       exposure;        // Min & Max exposure time in [us]
        {
            .fmt.order.all = HAT_PORDBYR_B_Gb_Gr_R,
            .fmt.reserved0 = 0,
            .fmt.xydec     = HAT_XYDEC_NO,
            .fmt.uvpkd     = HAT_UVPKD_PACKED,
            .fmt.pkd       = HAT_PIXPKD_PACKED,
            .fmt.fmt       = HAT_PIXCOL_BAYER,
            .bpc           = 10,
            .compression   = HAT_COMPRESSION_NONE,
        }, // hat_pix_fmt_t  format;           // Color format and pattern
        // uint16                  rotate;     // 0, 90, 180, 270 - Rotate TODO should come from somewhere???
        0,             // uint16  v_flip;      // [ON/OFF]
        0,             // uint16  h_mirror;    // [ON/OFF]
        1.0,           // float   sensitivity; //  mode (binning/skipping/average) sensitivity against full mode
        90000000,     // uint32  pixel_clock; // In Hz
        13917,          // uint32  row_time;    // [ns] - Readout time of line & blanking
        16700,         // uint32  frame_time;  // [us] - Readout time of the frame & blanking
        1200,          // uint32  ppln;        // Initial number PixelsPerLiNe 0x4b0
        1670,          // uint32  lpfr;        // Initial number LinesPerFRame 0x686
        1,             // uint32   num_bad_frames;  // Number of bad frames after mode switch
        ov4188_mode2688x1520_60fps_settings,
        ARR_SIZE(ov4188_mode2688x1520_60fps_settings),
        .pipeline_id = 0,       // Use pipeline with scale down
        .mipi_rx_cfg = {
            .lines = 4,
            .rate = 1008,   //840,
            .data_type = 0x2b,  // RAW 10
            .data_mode = 0,
        },
    },
#endif

#ifdef OV4188_FPS_2688x1520_90
    {
        {0, 0, 2688, 1520}, //hat_rect_t field_of_view;   // Sensor active view area in physical area [pixels]
        {2688, 1520},       //hat_size_t     active;          // Sensor active output pixels [pixels]
        {1, 1},             //hat_size_t     pixels;          // Equivalent of a digital pixel in physical pixels
        {1, 1},             //hat_size_t     aspect;          // Aspect ratio on 1 pixel
        {7.5, 74.0},     //hat_range_float_t fps;          // sensor mode Min/Max fps
       {14,  11094},        //hat_range_t       exposure;        // Min & Max exposure time in [us]
        {
            .fmt.order.all = HAT_PORDBYR_B_Gb_Gr_R,
            .fmt.reserved0 = 0,
            .fmt.xydec     = HAT_XYDEC_NO,
            .fmt.uvpkd     = HAT_UVPKD_PACKED,
            .fmt.pkd       = HAT_PIXPKD_PACKED,
            .fmt.fmt       = HAT_PIXCOL_BAYER,
            .bpc           = 10,
            .compression   = HAT_COMPRESSION_NONE,
        }, // hat_pix_fmt_t  format;           // Color format and pattern
        // uint16                  rotate;     // 0, 90, 180, 270 - Rotate TODO should come from somewhere???
        0,             // uint16  v_flip;      // [ON/OFF]
        0,             // uint16  h_mirror;    // [ON/OFF]
        1.0,           // float   sensitivity; //  mode (binning/skipping/average) sensitivity against full mode
        126000000,     // uint32  pixel_clock; // In Hz
        7167 ,          // uint32  row_time;    // [ns] - Readout time of line & blanking
        11137,         // uint32  frame_time;  // [us] - Readout time of the frame & blanking
        1026,          // uint32  ppln;        // Initial number PixelsPerLiNe 0x402
        1580,          // uint32  lpfr;        // Initial number LinesPerFRame 0x612
        1,             // uint32   num_bad_frames;  // Number of bad frames after mode switch
        ov4188_mode2688x1520_90fps_settings,
        ARR_SIZE(ov4188_mode2688x1520_90fps_settings),
        .pipeline_id = 0,       // Use pipeline with scale down
        .mipi_rx_cfg = {
            .lines = 4,
            .rate = 1008,   //840,
            .data_type = 0x2b,  // RAW 10
            .data_mode = 0,
        },
    },
#endif

#ifdef OV4188_FPS_2016x1520_30
    {
        {0, 0, 2016, 1520}, //hat_rect_t field_of_view;   // Sensor active view area in physical area [pixels]
        {2016, 1520},       //hat_size_t     active;          // Sensor active output pixels [pixels]
        {1, 1},             //hat_size_t     pixels;          // Equivalent of a digital pixel in physical pixels
        {1, 1},             //hat_size_t     aspect;          // Aspect ratio on 1 pixel
        {1.323, 30.0},     //hat_range_float_t fps;          // sensor mode Min/Max fps
       {100,  34000},        //hat_range_t       exposure;        // Min & Max exposure time in [us]
        {
            .fmt.order.all = HAT_PORDBYR_B_Gb_Gr_R,
            .fmt.reserved0 = 0,
            .fmt.xydec     = HAT_XYDEC_NO,
            .fmt.uvpkd     = HAT_UVPKD_PACKED,
            .fmt.pkd       = HAT_PIXPKD_PACKED,
            .fmt.fmt       = HAT_PIXCOL_BAYER,
            .bpc           = 10,
            .compression   = HAT_COMPRESSION_NONE,
        }, // hat_pix_fmt_t  format;           // Color format and pattern
        0,             // uint16  v_flip;      // [ON/OFF]
        0,             // uint16  h_mirror;    // [ON/OFF]
        1.0,           // float   sensitivity; //  mode (binning/skipping/average) sensitivity against full mode
        126000000,     // uint32  pixel_clock; // In Hz
        21467 ,          // uint32  row_time;    // [ns] - Readout time of line & blanking
        33359,         // uint32  frame_time;  // [us] - Readout time of the frame & blanking
        2576,          // uint32  ppln;        // Initial number PixelsPerLiNe
        1554,          // uint32  lpfr;        // Initial number LinesPerFRame
        1,             // uint32   num_bad_frames;  // Number of bad frames after mode switch
        ov4188_mode2016x1520_30fps_settings,
        ARR_SIZE(ov4188_mode2016x1520_30fps_settings),
        .pipeline_id = 0,       // Use pipeline with scale down
        .mipi_rx_cfg = {
            .lines = 2,
            .rate = 1008,   //840,
            .data_type = 0x2b,  // RAW 10
            .data_mode = 0,
        },
    },
#endif

#ifdef OV4188_FPS_2016x1520_60
    {
        {0, 0, 2016, 1520}, //hat_rect_t field_of_view;   // Sensor active view area in physical area [pixels]
        {2016, 1520},       //hat_size_t     active;          // Sensor active output pixels [pixels]
        {1, 1},             //hat_size_t     pixels;          // Equivalent of a digital pixel in physical pixels
        {1, 1},             //hat_size_t     aspect;          // Aspect ratio on 1 pixel
        {1.323, 60.0},     //hat_range_float_t fps;          // sensor mode Min/Max fps
       {100,  16615},        //hat_range_t       exposure;        // Min & Max exposure time in [us]
        {
            .fmt.order.all = HAT_PORDBYR_B_Gb_Gr_R,
            .fmt.reserved0 = 0,
            .fmt.xydec     = HAT_XYDEC_NO,
            .fmt.uvpkd     = HAT_UVPKD_PACKED,
            .fmt.pkd       = HAT_PIXPKD_PACKED,
            .fmt.fmt       = HAT_PIXCOL_BAYER,
            .bpc           = 10,
            .compression   = HAT_COMPRESSION_NONE,
        }, // hat_pix_fmt_t  format;           // Color format and pattern
        0,             // uint16  v_flip;      // [ON/OFF]
        0,             // uint16  h_mirror;    // [ON/OFF]
        1.0,           // float   sensitivity; //  mode (binning/skipping/average) sensitivity against full mode
        252000000,     // uint32  pixel_clock; // In Hz
        10728,          // uint32  row_time;    // [ns] - Readout time of line & blanking
        16680,         // uint32  frame_time;  // [us] - Readout time of the frame & blanking
        1288,          // uint32  ppln;        // Initial number PixelsPerLiNe
        1554,          // uint32  lpfr;        // Initial number LinesPerFRame
        1,             // uint32   num_bad_frames;  // Number of bad frames after mode switch
        ov4188_mode2016x1520_60fps_settings,
        ARR_SIZE(ov4188_mode2016x1520_60fps_settings),
        .pipeline_id = 0,       // Use pipeline with scale down
        .mipi_rx_cfg = {
            .lines = 4,
            .rate = 1008,   //840,
            .data_type = 0x2b,  // RAW 10
            .data_mode = 0,
        },
    },
#endif

#ifdef OV4188_FPS_2120x1196_37
    {
        {0, 0, 2120, 1196}, //hat_rect_t field_of_view;   // Sensor active view area in physical area [pixels]
        {2120, 1196},       //hat_size_t     active;          // Sensor active output pixels [pixels]
        {1, 1},             //hat_size_t     pixels;          // Equivalent of a digital pixel in physical pixels
        {1, 1},             //hat_size_t     aspect;          // Aspect ratio on 1 pixel
        {1.323, 37.4},     //hat_range_float_t fps;          // sensor mode Min/Max fps
       {100,  26732},        //hat_range_t       exposure;        // Min & Max exposure time in [us]
        {
            .fmt.order.all = HAT_PORDBYR_B_Gb_Gr_R,
            .fmt.reserved0 = 0,
            .fmt.xydec     = HAT_XYDEC_NO,
            .fmt.uvpkd     = HAT_UVPKD_PACKED,
            .fmt.pkd       = HAT_PIXPKD_PACKED,
            .fmt.fmt       = HAT_PIXCOL_BAYER,
            .bpc           = 10,
            .compression   = HAT_COMPRESSION_NONE,
        }, // hat_pix_fmt_t  format;           // Color format and pattern
        0,             // uint16  v_flip;      // [ON/OFF]
        0,             // uint16  h_mirror;    // [ON/OFF]
        1.0,           // float   sensitivity; //  mode (binning/skipping/average) sensitivity against full mode
        126000000,     // uint32  pixel_clock; // In Hz
        21733,          // uint32  row_time;    // [ns] - Readout time of line & blanking
        26732,         // uint32  frame_time;  // [us] - Readout time of the frame & blanking
        2608,          // uint32  ppln;        // Initial number PixelsPerLiNe 0xa30
        1230,          // uint32  lpfr;        // Initial number LinesPerFRame 0x4ce 0x3be
        1,             // uint32   num_bad_frames;  // Number of bad frames after mode switch
        ov4188_mode2120x1196_30fps_settings,
        ARR_SIZE(ov4188_mode2120x1196_30fps_settings),
        .pipeline_id = 0,       // Use pipeline with scale down
        .mipi_rx_cfg = {
            .lines = 2,
            .rate = 1008,   //840,
            .data_type = 0x2b,  // RAW 10
            .data_mode = 0,
        },
    },
#endif

#ifdef OV4188_FPS_2120x1196_4LANE_75P_48P
    {
        {0, 0, 2120, 1196}, //hat_rect_t field_of_view;   // Sensor active view area in physical area [pixels]
        {2120, 1196},       //hat_size_t     active;          // Sensor active output pixels [pixels]
        {1, 1},             //hat_size_t     pixels;          // Equivalent of a digital pixel in physical pixels
        {1, 1},             //hat_size_t     aspect;          // Aspect ratio on 1 pixel
        {1.323, 48.0/*75.0*/},     //hat_range_float_t fps;          // sensor mode Min/Max fps
       {20,  20822/*16834*/},        //hat_range_t       exposure;        // Min & Max exposure time in [us]
        {
            .fmt.order.all = HAT_PORDBYR_B_Gb_Gr_R,
            .fmt.reserved0 = 0,
            .fmt.xydec     = HAT_XYDEC_NO,
            .fmt.uvpkd     = HAT_UVPKD_PACKED,
            .fmt.pkd       = HAT_PIXPKD_PACKED,
            .fmt.fmt       = HAT_PIXCOL_BAYER,
            .bpc           = 10,
            .compression   = HAT_COMPRESSION_NONE,
        }, // hat_pix_fmt_t  format;           // Color format and pattern
        0,             // uint16  v_flip;      // [ON/OFF]
        0,             // uint16  h_mirror;    // [ON/OFF]
        1.0,           // float   sensitivity; //  mode (binning/skipping/average) sensitivity against full mode
        126000000,     // uint32  pixel_clock; // In Hz
        10833,          // uint32  row_time;    // [ns] - Readout time of line & blanking
        20822/*13324*/,         // uint32  frame_time;  // [us] - Readout time of the frame & blanking
        1300,          // uint32  ppln;        // Initial number PixelsPerLiNe 0x514
        1922/*1230*/,          // uint32  lpfr;        // Initial number LinesPerFRame 0x4ce
        1,             // uint32   num_bad_frames;  // Number of bad frames after mode switch
        ov4188_mode2120x1196_75fps_settings,
        ARR_SIZE(ov4188_mode2120x1196_75fps_settings),
        .pipeline_id = 0,       // Use pipeline with scale down
        .mipi_rx_cfg = {
            .lines = 4,
            .rate = 1008,   //840,
            .data_type = 0x2b,  // RAW 10
            .data_mode = 0,
        },
    },
#endif
#ifdef OV4188_FPS_2120x1196_120
    {
        {0, 0, 2120, 1196}, //hat_rect_t field_of_view;   // Sensor active view area in physical area [pixels]
        {2120, 1196},       //hat_size_t     active;          // Sensor active output pixels [pixels]
        {1, 1},             //hat_size_t     pixels;          // Equivalent of a digital pixel in physical pixels
        {1, 1},             //hat_size_t     aspect;          // Aspect ratio on 1 pixel
        {1.323, 120},     //hat_range_float_t fps;          // sensor mode Min/Max fps
       {20,  8364},        //hat_range_t       exposure;        // Min & Max exposure time in [us]
        {
            .fmt.order.all = HAT_PORDBYR_B_Gb_Gr_R,
            .fmt.reserved0 = 0,
            .fmt.xydec     = HAT_XYDEC_NO,
            .fmt.uvpkd     = HAT_UVPKD_PACKED,
            .fmt.pkd       = HAT_PIXPKD_PACKED,
            .fmt.fmt       = HAT_PIXCOL_BAYER,
            .bpc           = 10,
            .compression   = HAT_COMPRESSION_NONE,
        }, // hat_pix_fmt_t  format;           // Color format and pattern
        0,             // uint16  v_flip;      // [ON/OFF]
        0,             // uint16  h_mirror;    // [ON/OFF]
        1.0,           // float   sensitivity; //  mode (binning/skipping/average) sensitivity against full mode
        126000000,     // uint32  pixel_clock; // In Hz
        6800,          // uint32  row_time;    // [ns] - Readout time of line & blanking
        8364,         // uint32  frame_time;  // [us] - Readout time of the frame & blanking
        816,          // uint32  ppln;        // Initial number PixelsPerLiNe 0x330
        1230,          // uint32  lpfr;        // Initial number LinesPerFRame 0x4ce 0x5c3
        1,             // uint32   num_bad_frames;  // Number of bad frames after mode switch
        ov4188_mode2120x1196_120fps_settings,
        ARR_SIZE(ov4188_mode2120x1196_120fps_settings),
        .pipeline_id = 0,       // Use pipeline with scale down
        .mipi_rx_cfg = {
            .lines = 4,
            .rate = 1008,   //840,
            .data_type = 0x2b,  // RAW 10
            .data_mode = 0,
        },
    },
#endif

#ifdef OV4188_FPS_1920x1080_30
    {
        {0, 0, 1920, 1080}, //hat_rect_t field_of_view;   // Sensor active view area in physical area [pixels]
        {1920, 1080},       //hat_size_t     active;          // Sensor active output pixels [pixels]
        {1, 1},             //hat_size_t     pixels;          // Equivalent of a digital pixel in physical pixels
        {1, 1},             //hat_size_t     aspect;          // Aspect ratio on 1 pixel
        {1.323, 30.0},     //hat_range_float_t fps;          // sensor mode Min/Max fps
       {100,  34000},        //hat_range_t       exposure;        // Min & Max exposure time in [us]
        {
            .fmt.order.all = HAT_PORDBYR_B_Gb_Gr_R,
            .fmt.reserved0 = 0,
            .fmt.xydec     = HAT_XYDEC_NO,
            .fmt.uvpkd     = HAT_UVPKD_PACKED,
            .fmt.pkd       = HAT_PIXPKD_PACKED,
            .fmt.fmt       = HAT_PIXCOL_BAYER,
            .bpc           = 10,
            .compression   = HAT_COMPRESSION_NONE,
        }, // hat_pix_fmt_t  format;           // Color format and pattern
        // uint16                  rotate;     // 0, 90, 180, 270 - Rotate TODO should come from somewhere???
        0,             // uint16  v_flip;      // [ON/OFF]
        0,             // uint16  h_mirror;    // [ON/OFF]
        1.0,           // float   sensitivity; //  mode (binning/skipping/average) sensitivity against full mode
        126000000,     // uint32  pixel_clock; // In Hz
        21467 ,          // uint32  row_time;    // [ns] - Readout time of line & blanking
        33359,         // uint32  frame_time;  // [us] - Readout time of the frame & blanking
        2570,          // uint32  ppln;        // Initial number PixelsPerLiNe 0x0a0a
        1556,          // uint32  lpfr;        // Initial number LinesPerFRame 0x0614
        1,             // uint32   num_bad_frames;  // Number of bad frames after mode switch
        ov4188_mode1920x1080_30fps_settings,
        ARR_SIZE(ov4188_mode1920x1080_30fps_settings),
        .pipeline_id = 0,       // Use pipeline with scale down
        .mipi_rx_cfg = {
            .lines = 2,
            .rate = 1008,   //840,
            .data_type = 0x2b,  // RAW 10
            .data_mode = 0,
        },
    },
#endif
#ifdef OV4188_FPS_1920x1080_60_2lane
    {
        {0, 0, 1920, 1080}, //hat_rect_t field_of_view;   // Sensor active view area in physical area [pixels]
        {1920, 1080},       //hat_size_t     active;          // Sensor active output pixels [pixels]
        {1, 1},             //hat_size_t     pixels;          // Equivalent of a digital pixel in physical pixels
        {1, 1},             //hat_size_t     aspect;          // Aspect ratio on 1 pixel
        {1.323, 60.0},     //hat_range_float_t fps;          // sensor mode Min/Max fps
       {100,  16666},        //hat_range_t       exposure;        // Min & Max exposure time in [us]
        {
            .fmt.order.all = HAT_PORDBYR_B_Gb_Gr_R,
            .fmt.reserved0 = 0,
            .fmt.xydec     = HAT_XYDEC_NO,
            .fmt.uvpkd     = HAT_UVPKD_PACKED,
            .fmt.pkd       = HAT_PIXPKD_PACKED,
            .fmt.fmt       = HAT_PIXCOL_BAYER,
            .bpc           = 10,
            .compression   = HAT_COMPRESSION_NONE,
        }, // hat_pix_fmt_t  format;           // Color format and pattern
        // uint16                  rotate;     // 0, 90, 180, 270 - Rotate TODO should come from somewhere???
        0,             // uint16  v_flip;      // [ON/OFF]
        0,             // uint16  h_mirror;    // [ON/OFF]
        1.0,           // float   sensitivity; //  mode (binning/skipping/average) sensitivity against full mode
        126000000,     // uint32  pixel_clock; // In Hz
        13333 ,          // uint32  row_time;    // [ns] - Readout time of line & blanking
        16666,         // uint32  frame_time;  // [us] - Readout time of the frame & blanking
        1600,          // uint32  ppln;        // Initial number PixelsPerLiNe 0x0640
        1250,          // uint32  lpfr;        // Initial number LinesPerFRame 0x04e2
        1,             // uint32   num_bad_frames;  // Number of bad frames after mode switch
        ov4188_mode1920x1080_60fps_settings,
        ARR_SIZE(ov4188_mode1920x1080_60fps_settings),
        .pipeline_id = 0,       // Use pipeline with scale down
        .mipi_rx_cfg = {
            .lines = 2,
            .rate = 1008,   //840,
            .data_type = 0x2b,  // RAW 10
            .data_mode = 0,
        },
    },
#endif
#ifdef OV4188_FPS_1344x760_58_5
    {
        {0, 0, /*2688, 1520*/1344, 760}, //hat_rect_t field_of_view;   // Sensor active view area in physical area [pixels]
        {/*2688, 1520*/1344, 760},       //hat_size_t     active;          // Sensor active output pixels [pixels]
        {1, 1},             //hat_size_t     pixels;          // Equivalent of a digital pixel in physical pixels
        {1, 1},             //hat_size_t     aspect;          // Aspect ratio on 1 pixel
        {1.323, 58.5},     //hat_range_float_t fps;          // sensor mode Min/Max fps
       {100,  34000},        //hat_range_t       exposure;        // Min & Max exposure time in [us]
        {
            .fmt.order.all = HAT_PORDBYR_B_Gb_Gr_R,
            .fmt.reserved0 = 0,
            .fmt.xydec     = HAT_XYDEC_NO,
            .fmt.uvpkd     = HAT_UVPKD_PACKED,
            .fmt.pkd       = HAT_PIXPKD_PACKED,
            .fmt.fmt       = HAT_PIXCOL_BAYER,
            .bpc           = 10,
            .compression   = HAT_COMPRESSION_NONE,
        }, // hat_pix_fmt_t  format;           // Color format and pattern
        0,             // uint16  v_flip;      // [ON/OFF]
        0,             // uint16  h_mirror;    // [ON/OFF]
        1.0,           // float   sensitivity; //  mode (binning/skipping/average) sensitivity against full mode
        126000000,     // uint32  pixel_clock; // In Hz
        21933 ,          // uint32  row_time;    // [ns] - Readout time of line & blanking
        17481,         // uint32  frame_time;  // [us] - Readout time of the frame & blanking
        2632,          // uint32  ppln;        // Initial number PixelsPerLiNe
        797,          // uint32  lpfr;        // Initial number LinesPerFRame
        1,             // uint32   num_bad_frames;  // Number of bad frames after mode switch
        ov4188_mode1344x760_58_5fps_settings,
        ARR_SIZE(ov4188_mode1344x760_58_5fps_settings),
        .pipeline_id = 0,       // Use pipeline with scale down
        .mipi_rx_cfg = {
            .lines = 2,
            .rate = 1008,   //840,
            .data_type = 0x2b,  // RAW 10
            .data_mode = 0,
        },
    },
#endif

};

static const hat_dtp_sensor_desc_t dtp_sensor_ov4188_db =
{ // OV4188 sensor
    .sensor_id          = DTP_SEN_OV4188,
    .sensor_name        = "OV4188 sensor",
    .sensor_features    = {
        {2688, 1520},         //hat_size_t          array_size;     // Physical area (valid + black pixels)
        {0, 0, 2688, 1520},   //hat_rect_t          valid_pixels;   // Sensor full area in physical area
         100,                 //uint32              base_ISO;       // real sensor sensitivity with gain = 1 [U32Q16]
        {1.0, 1.0, 16.0, 0.1}, //hat_bounds_float_t again;          // min/max sensor gain analog
        {1.0, 1.0,  1.0, 0.1},// hat_bounds_float_t dgain;          // min/max sensor gain dnalog
        0,                    //uint32              exp_gain_delay; // exposure to gain delay [frames]
        2,                    //uint32              global_delay;   // delay to output frame with applied parameters [frames]
        {   //const hat_sensor_operations_t operations;
            .pwr_off = {
                ARR_SIZE(ov4188_power_off_seq),
                ov4188_power_off_seq,
            },
            .pwr_on = {
                ARR_SIZE(ov4188_power_on_seq),
                ov4188_power_on_seq,
            },
            .init = {
                [SENSOR_INIT_1] = {
                    ARR_SIZE(ov4188_init_seq),
                    ov4188_init_seq,
                    24000000,
                },
                [SENSOR_INIT_2] = {},
                [SENSOR_INIT_3] = {},
            },
            .stream_off = {
                ARR_SIZE(ov4188_stream_off_seq),
                ov4188_stream_off_seq,
            },
            .stream_on = {
                ARR_SIZE(ov4188_stream_on_seq),
                ov4188_stream_on_seq,
            },
            .gph_on = {
                ARR_SIZE(ov4188_gph_on_seq),
                ov4188_gph_on_seq,
            },
            .gph_off = {
                ARR_SIZE(ov4188_gph_off_seq),
                ov4188_gph_off_seq,
            },
        },
        {
            ARR_SIZE(ov4188_modes), // uint32             num;   // number of sensor modes
            ov4188_modes // list with sensor modes
        },
        1, //uint32  powerup_bad_frames;  // Number of bad frames after powerup
        1, //uint8 temp_sensor_supp;    // If sensor support temperature sensor - 1. If no - 0.
        //HAL3 specific data.
        {100, 1600}, //int32                   sensitivity_range[2];     // Range of valid sensitivities Min <= 100, Max >= 1600
        {.all = HAT_PORDBYR_B_Gb_Gr_R}, //hat_pix_order_t         color_filter_arrangement; // Arrangement of color filters on sensor TODO we have same info in sens mode descriptor
        {100000, 34000000},//TODO, //int64  exposure_time_range[2];  // Range of valid exposure times in [nanoseconds]
        34000000,//TODO, //int64  max_frame_duration; // Maximum possible frame duration in [nanoseconds].
        {5.5541, 4.1134}, //float                  physical_size[2];         // The physical dimensions of the full pixel array in [mm]
        1023, //int32                   white_level;              // Maximum raw value output by sensor
        {1, 1}, //hat_fract_t             base_gain_factor;         // Gain factor from electrons to raw units when ISO=100
        {42, 42, 42, 42}, //int32                   black_level_pattern[4];   // A fixed black level offset for each of the Bayer mosaic channels.
        {{0, 0}}, //hat_fract_t             calib_transform_1[9];     // Per-device calibration on top of color space transform 1
        {{0, 0}}, //hat_fract_t             calib_transform_2[9];     // Per-device calibration on top of color space transform 2
        {{0, 0}}, //hat_fract_t             color_transform_1[9];     // Linear mapping from XYZ (D50) color space to reference linear sensor color.
        {{0, 0}}, //hat_fract_t             color_transform_2[9];     // Linear mapping from XYZ (D50) color space to reference linear sensor color.
        {{0, 0}}, //hat_fract_t             forward_matrix_1[9];      // Used by DNG for better WB adaptation.
        {{0, 0}}, //hat_fract_t             forward_matrix_2[9];      // Used by DNG for better WB adaptation.
        800, //int32                   max_analog_sensitivity;    // Maximum sensitivity that is implemented purely through analog gain.
        {0.0}, //float                  noise_model_coef[2];      // Estimation of sensor noise characteristics.
        0, //int32                   orientation;              // degrees clockwise rotation TODO we have same info in sens mode descriptor
        REF_ILL_NONE, //reference_illuminant_t  reference_illuminant_1;   // Light source used to define transform 1.
        REF_ILL_NONE, //reference_illuminant_t  reference_illuminant_2;   // Light source used to define transform 2.
        .frame_size_regs = {
            .frm_length_lines = 0x380e,
            .frm_length_lines_size = 2,
            .line_length_pck = 0x380c,
            .line_length_pck_size = 2,
        },
    }
};

#define DTP_SENSOR_OV4188_DB_SIZE ( \
    sizeof(dtp_sensor_ov4188_db) \
)

#endif //__DTP_SENSOR_OV4188_DB_H__
