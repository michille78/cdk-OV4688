/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file dtp_light_AS3645_db.h
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#ifndef __DTP_LIGHT_AS3645_DB_H__
#define __DTP_LIGHT_AS3645_DB_H__

//-------------------
#define AS3645_FLASH_TIMER_REG          2
#define AS3645_FLASH_TIMEOUT_200MS      2
//-------------------
#define AS3645_CURRENT_SET_REG  3
#define AS3645_FLASH_INTENSITY_100     0xBC     // Flash 400ma assist 100ma
#define AS3645_FLASH_INTENSITY_80      0x7C     // Flash 340ma assist 100ma
#define AS3645_ASSISTANT_INTENSITY_50  0xBC     // Flash 400ma assist 100ma
#define AS3645_ASSISTANT_INTENSITY_30  0xB9     // Flash 400ma assist 40ma
//-------------------
#define AS3645_CONTROL_REG      4
#define AS3645_FLASH_MODE_ON           0xBB
#define AS3645_ASSISTANT_MODE_ON       0xBA
#define AS3645_ASSISTANT_MODE_ON       0xBA
#define AS3645_OFF                     0xB2
//-------------------

static const hat_cm_socket_command_entry_t AS3645_flash_light_power_seq_on_100[] =
{
    LIGHT_INTENT_CMD_CCI_I2C_WR(AS3645_FLASH_TIMER_REG,    AS3645_FLASH_TIMEOUT_200MS,
                             /* AS3645_CURRENT_SET_REG */  AS3645_FLASH_INTENSITY_100,
                             /* AS3645_CONTROL_REG */      AS3645_FLASH_MODE_ON),
    CMD_ENTRY_END(),
};

static const hat_cm_socket_command_entry_t AS3645_flash_light_power_seq_on_80[] =
{
        LIGHT_INTENT_CMD_CCI_I2C_WR(AS3645_FLASH_TIMER_REG,    AS3645_FLASH_TIMEOUT_200MS,
                                 /* AS3645_CURRENT_SET_REG */  AS3645_FLASH_INTENSITY_80,
                                 /* AS3645_CONTROL_REG */      AS3645_FLASH_MODE_ON),
    CMD_ENTRY_END(),
};


static const hat_cm_socket_command_entry_t AS3645_torch_light_power_seq_on_50[] =
{
    LIGHT_INTENT_CMD_CCI_I2C_WR(AS3645_CURRENT_SET_REG, AS3645_ASSISTANT_INTENSITY_50,
                             /* AS3645_CONTROL_REG */   AS3645_ASSISTANT_MODE_ON),
    CMD_ENTRY_END(),
};

static const hat_cm_socket_command_entry_t AS3645_torch_light_power_seq_on_30[] =
{
    LIGHT_INTENT_CMD_CCI_I2C_WR(AS3645_CURRENT_SET_REG, AS3645_ASSISTANT_INTENSITY_30,
                             /* AS3645_CONTROL_REG */   AS3645_ASSISTANT_MODE_ON),
    CMD_ENTRY_END(),
};

static const hat_cm_socket_command_entry_t AS3645_light_power_seq_OFF[] =
{
    LIGHT_INTENT_CMD_CCI_I2C_WR(AS3645_CONTROL_REG, AS3645_OFF),
    CMD_ENTRY_END(),
};

static const hat_light_entry_t AS3645_flash_light_entries[] =
{
        {
            0,    // intensity [cd] light intensity
            0,    // power [0-100] Light power (driver specific value/index)
            0,    // timeout [uSeconds] 0 infinity
            0,    // charge_time; // [us] for LED this is time between two flashes with that power
            .command_script = &AS3645_light_power_seq_OFF
        },
        {
        80,     // intensity [cd] light intensity
        80,     // power [0-100] Light power (driver specific value/index)
        5000000,// timeout [uSeconds] 0 infinity
        600,    // charge_time; // [us] for LED this is time between two flashes with that power
        .command_script = &AS3645_flash_light_power_seq_on_80
    },
    {
        100,    // intensity [cd] light intensity
        100,    // power [0-100] Light power (driver specific value/index)
        5000000,// timeout [uSeconds] 0 infinity
        600,    // charge_time; // [us] for LED this is time between two flashes with that power
        .command_script = &AS3645_flash_light_power_seq_on_100
    },
};

static const hat_light_entry_t AS3645_torch_light_entries[] =
{
    {
        0,    // intensity [cd] light intensity
        0,    // power [0-100] Light power (driver specific value/index)
        0,    // timeout [uSeconds] 0 infinity
        0,    // charge_time; // [us] for LED this is time between two flashes with that power
        .command_script = &AS3645_light_power_seq_OFF
    },
    {
        50,     // intensity [cd] light intensity
        50,     // power [0-100] Light power (driver specific value/index)
        0,      // timeout [uSeconds] 0 infinity
        0,       // charge_time; // [us] for LED this is time between two flashes with that power
        .command_script = &AS3645_torch_light_power_seq_on_50
    }
};

static const hat_light_entry_t AS3645_privacy_light_entries[] =
{
    {
        0,    // intensity [cd] light intensity
        0,    // power [0-100] Light power (driver specific value/index)
        0,    // timeout [uSeconds] 0 infinity
        0,    // charge_time; // [us] for LED this is time between two flashes with that power
        .command_script = &AS3645_light_power_seq_OFF
    },
    {
        30,     // intensity [cd] light intensity
        30,     // power [0-100] Light power (driver specific value/index)
        0,      // timeout [uSeconds] 0 infinity
        0,      // charge_time; // [us] for LED this is time between two flashes with that power
        .command_script = &AS3645_torch_light_power_seq_on_30
    }
};

static const hat_cm_socket_command_entry_t AS3645_light_power_seq_on[] =
{
    CMD_ENTRY_DUMMY(),
    CMD_ENTRY_END(),
};

static const hat_cm_socket_command_entry_t AS3645_light_power_seq_off[] =
{
    CMD_ENTRY_DUMMY(),
    CMD_ENTRY_END(),
};

static const hat_cm_socket_command_entry_t AS3645_light_init[] =
{
    CMD_ENTRY_DUMMY(),
    CMD_ENTRY_END(),
};

static const hat_cm_socket_command_entry_t AS3645_light_deinit[] =
{
    CMD_ENTRY_DUMMY(),
    CMD_ENTRY_END(),
};

static const hat_dtp_lights_desc_t dtp_light_AS3645_db =
{ // AS3645 camera module
    .lights_id          = DTP_LIGHTS_AS3645,
    .lights_name        = "AS3645 light",
    .lights_features    = {
    // hat_light_el_features_t flash;
        {
            {
                0, //uint64 charge_duration; // Time taken before flash can fire again.[ns]
                2500, //uint32 colour_temp;     //
                1, //uint8 available; // Whether this camera has a flash.
                150, //uint8 max_energy; //Max energy output of the flash for a full power single flash [lumen-seconds]
                HAT_FLASH_TYPE_LED, //hat_flash_type_t type;
            },
            ARRAY_SIZE(AS3645_flash_light_entries),//uint32 num_light_intensities;   // number of valid entries in the list
            AS3645_flash_light_entries, //const hat_light_entry_t        *entries;
            {//const hat_lights_operations_t  operations;
                .pwr_seq_on = {
                    ARR_SIZE(AS3645_light_power_seq_on),
                    AS3645_light_power_seq_on,
                },
                .pwr_seq_off = {
                    ARR_SIZE(AS3645_light_power_seq_off),
                    AS3645_light_power_seq_off,
                },
                .init_seq = {
                    ARR_SIZE(AS3645_light_init),
                    AS3645_light_init,
                },
                .deinit_seq = {
                    ARR_SIZE(AS3645_light_deinit),
                    AS3645_light_deinit,
                },
            }
        },
    // hat_light_el_features_t torch;
        {
            {
                0, //uint64 charge_duration; // Time taken before flash can fire again.[ns]
                2500, //uint32 colour_temp;     //
                1, //uint8 available; // Whether this camera has a flash.
                60, //uint8 max_energy; //Max energy output of the flash for a full power single flash [lumen-seconds]
                HAT_FLASH_TYPE_LED, //hat_flash_type_t type;
            },
            ARRAY_SIZE(AS3645_torch_light_entries),//uint32 num_light_intensities;   // number of valid entries in the list
            AS3645_torch_light_entries, //const hat_light_entry_t        *entries;
            {//const hat_lights_operations_t  operations;
                .pwr_seq_on = {
                    ARR_SIZE(AS3645_light_power_seq_on),
                    AS3645_light_power_seq_on,
                },
                .pwr_seq_off = {
                    ARR_SIZE(AS3645_light_power_seq_off),
                    AS3645_light_power_seq_off,
                },
                .init_seq = {
                    ARR_SIZE(AS3645_light_init),
                    AS3645_light_init,
                },
                .deinit_seq = {
                    ARR_SIZE(AS3645_light_deinit),
                    AS3645_light_deinit,
                },
            }
        },
    // hat_light_el_features_t video;
        {
            {
                0, //uint64 charge_duration; // Time taken before flash can fire again.[ns]
                2500, //uint32 colour_temp;     //
                1, //uint8 available; // Whether this camera has a flash.
                60, //uint8 max_energy; //Max energy output of the flash for a full power single flash [lumen-seconds]
                HAT_FLASH_TYPE_LED, //hat_flash_type_t type;
            },
            ARRAY_SIZE(AS3645_torch_light_entries),//uint32 num_light_intensities;   // number of valid entries in the list
            AS3645_torch_light_entries, //const hat_light_entry_t        *entries;
            {//const hat_lights_operations_t  operations;
                .pwr_seq_on = {
                    ARR_SIZE(AS3645_light_power_seq_on),
                    AS3645_light_power_seq_on,
                },
                .pwr_seq_off = {
                    ARR_SIZE(AS3645_light_power_seq_off),
                    AS3645_light_power_seq_off,
                },
                .init_seq = {
                    ARR_SIZE(AS3645_light_init),
                    AS3645_light_init,
                },
                .deinit_seq = {
                    ARR_SIZE(AS3645_light_deinit),
                    AS3645_light_deinit,
                },
            }
        },
    // hat_light_el_features_t af_assist;
        {
            {
                0, //uint64 charge_duration; // Time taken before flash can fire again.[ns]
                2500, //uint32 colour_temp;     //
                1, //uint8 available; // Whether this camera has a flash.
                60, //uint8 max_energy; //Max energy output of the flash for a full power single flash [lumen-seconds]
                HAT_FLASH_TYPE_LED, //hat_flash_type_t type;
            },
            ARRAY_SIZE(AS3645_torch_light_entries),//uint32 num_light_intensities;   // number of valid entries in the list
            AS3645_torch_light_entries, //const hat_light_entry_t        *entries;
            {//const hat_lights_operations_t  operations;
                .pwr_seq_on = {
                    ARR_SIZE(AS3645_light_power_seq_on),
                    AS3645_light_power_seq_on,
                },
                .pwr_seq_off = {
                    ARR_SIZE(AS3645_light_power_seq_off),
                    AS3645_light_power_seq_off,
                },
                .init_seq = {
                    ARR_SIZE(AS3645_light_init),
                    AS3645_light_init,
                },
                .deinit_seq = {
                    ARR_SIZE(AS3645_light_deinit),
                    AS3645_light_deinit,
                },
            }
        },
    // hat_light_el_features_t privacy;
        {
            {
                0, //uint64 charge_duration; // Time taken before flash can fire again.[ns]
                2500, //uint32 colour_temp;     //
                0, //uint8 available; // Whether this camera has a flash.
                0, //uint8 max_energy; //Max energy output of the flash for a full power single flash [lumen-seconds]
                HAT_FLASH_TYPE_LED, //hat_flash_type_t type;
            },
            ARRAY_SIZE(AS3645_privacy_light_entries),//uint32 num_light_intensities;   // number of valid entries in the list
            AS3645_privacy_light_entries, //const hat_light_entry_t        *entries;
            {//const hat_lights_operations_t  operations;
                .pwr_seq_on = {
                    ARR_SIZE(AS3645_light_power_seq_on),
                    AS3645_light_power_seq_on,
                },
                .pwr_seq_off = {
                    ARR_SIZE(AS3645_light_power_seq_off),
                    AS3645_light_power_seq_off,
                },
                .init_seq = {
                    ARR_SIZE(AS3645_light_init),
                    AS3645_light_init,
                },
                .deinit_seq = {
                    ARR_SIZE(AS3645_light_deinit),
                    AS3645_light_deinit,
                },
            }
        },
    // hat_light_el_features_t red_eye; // Blink period is HW (driver) specific
        {
            {
                0, //uint64 charge_duration; // Time taken before flash can fire again.[ns]
                2500, //uint32 colour_temp;     //
                0, //uint8 available; // Whether this camera has a flash.
                0, //uint8 max_energy; //Max energy output of the flash for a full power single flash [lumen-seconds]
                HAT_FLASH_TYPE_LED, //hat_flash_type_t type;
            },
            ARRAY_SIZE(AS3645_flash_light_entries),//uint32 num_light_intensities;   // number of valid entries in the list
            AS3645_flash_light_entries, //const hat_light_entry_t        *entries;
            {//const hat_lights_operations_t  operations;
               .pwr_seq_on = {
                   ARR_SIZE(AS3645_light_power_seq_on),
                   AS3645_light_power_seq_on,
               },
               .pwr_seq_off = {
                   ARR_SIZE(AS3645_light_power_seq_off),
                   AS3645_light_power_seq_off,
               },
                .init_seq = {
                    ARR_SIZE(AS3645_light_init),
                    AS3645_light_init,
                },
                .deinit_seq = {
                    ARR_SIZE(AS3645_light_deinit),
                    AS3645_light_deinit,
                },
            }
        },
    }
};

#define DTP_LIGHTS_AS3645_DB_SIZE ( \
    sizeof(dtp_light_AS3645_db) \
)

#endif //__DTP_LIGHT_AS3645_DB_H__
