/* =============================================================================
* Copyright (c) 2013-2015 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file dtp_cm_db.h
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#ifndef __DTP_CM_DB_H__
#define __DTP_CM_DB_H__

#include <hal/hal_camera_module/hat_cm_dtp_data.h>

static const hat_cm_socket_command_entry_t dummy_cm_det_seq[] = {
    CMD_ENTRY_DUMMY(),
    CMD_ENTRY_END(),
};

#define W BE16
static const hat_cm_socket_command_entry_t aptina_cm_det_seq[] = {
    SENS_INTENT_CMD_CCI_I2C_RD_CHECK(0x31FE, W(0x31FE)), // if 0x31FE=0x31FE detect condition is met
    CMD_ENTRY_END(),
};
#undef W

//TODO
#define W BE16
static const hat_cm_socket_command_entry_t imx214_cm_det_seq[] = {
    SENS_INTENT_ACTION(SEN_RSRC_POWER_1, HAT_HW_RES_ON),
    SENS_INTENT_ACTION(SEN_RSRC_CLOCK, HAT_HW_RES_ON),     //XSHDN#
    SENS_DELAY(10000),
    SENS_INTENT_CMD_CCI_I2C_RD_CHECK(0x0016, W(0x0214)), // if 0x0016=0x0214 detect condition is met
    SENS_INTENT_ACTION(SEN_RSRC_POWER_1, HAT_HW_RES_OFF),
    SENS_DELAY(1000),
    CMD_ENTRY_END(),
};
#undef W

//TODO
#define W BE16
static const hat_cm_socket_command_entry_t highlands_cm_det_seq[] = {
    SENS_INTENT_CMD_CCI_I2C_RD_CHECK(0x31FE,  W(0xCECE)), // if 0x31FE=0x31FE detect condition is met
    CMD_ENTRY_END(),
};
#undef W

#define W BE16
static const hat_cm_socket_command_entry_t imx208_cm_det_seq[] = {
    CMD_ENTRY_DUMMY(),
    //Setup clock end
    SENS_INTENT_ACTION(SEN_RSRC_POWER_2, HAT_HW_RES_OFF), //stop Right sensor
    SENS_INTENT_ACTION(SEN_RSRC_POWER_1, HAT_HW_RES_OFF),
    SENS_DELAY(1000),
    SENS_INTENT_ACTION(SEN_RSRC_POWER_1, HAT_HW_RES_ON),
    SENS_INTENT_ACTION(SEN_RSRC_CLOCK, HAT_HW_RES_ON),     //XSHDN#
    SENS_DELAY(1000),
    SENS_INTENT_CMD_CCI_I2C_RD_CHECK(0x0000, W(0x0208)), // if 0x0000=0x0208 detect condition is met
    SENS_INTENT_ACTION(SEN_RSRC_POWER_1, HAT_HW_RES_OFF),
    SENS_DELAY(1000),
    CMD_ENTRY_END(),
};
#undef W

#define W BE16
static const hat_cm_socket_command_entry_t ov13860_cm_det_seq[] = {
    CMD_ENTRY_DUMMY(),
    //Setup clock end
    SENS_INTENT_ACTION(SEN_RSRC_POWER_1, HAT_HW_RES_ON),  //PRWDN#
    SENS_INTENT_ACTION(SEN_RSRC_CLOCK, HAT_HW_RES_ON),    //XSHDN#
    SENS_DELAY(1000),
    SENS_INTENT_ACTION(SEN_RSRC_XSHTDWN, HAT_HW_RES_OFF), //XSHDN#
    SENS_DELAY(1000),
    SENS_INTENT_ACTION(SEN_RSRC_XSHTDWN, HAT_HW_RES_ON), //XSHDN#
    SENS_DELAY(1000),
    SENS_INTENT_CMD_CCI_I2C_RD_CHECK(0x300A, 0x01, 0x38, 0x60), // TODO: check ov13860!!! if 0x0000=0x0000 detect condition is met
    SENS_INTENT_ACTION(SEN_RSRC_POWER_1, HAT_HW_RES_OFF),  //PRWDN#
    SENS_INTENT_ACTION(SEN_RSRC_XSHTDWN, HAT_HW_RES_OFF), //XSHDN#
    CMD_ENTRY_END(),
};
#undef W

#define W BE16
static const hat_cm_socket_command_entry_t ar0330_cm_det_seq[] =
{
    SENS_INTENT_ACTION(SEN_RSRC_RESET, HAT_HW_RES_ON),    //XSHDN#
    SENS_DELAY(1000),
    SENS_INTENT_ACTION(SEN_RSRC_RESET, HAT_HW_RES_OFF),    //XSHDN#
    SENS_DELAY(30000),
    SENS_INTENT_ACTION(SEN_RSRC_CLOCK, HAT_HW_RES_ON),    //XSHDN#
    SENS_INTENT_ACTION(SEN_RSRC_ENABLE, HAT_HW_RES_ON),
    SENS_INTENT_CMD_CCI_I2C_RD_CHECK(0x3000, W(0x2604)), // if 0x3000=0x2604 detect condition is met
    CMD_ENTRY_END(),
};
#undef W

#define W BE16
static const hat_cm_socket_command_entry_t ov7251_cm_det_seq[] = {
    //Setup clock end
    SENS_INTENT_ACTION(SEN_RSRC_POWER_2, HAT_HW_RES_OFF),   // Stop R sensor
    SENS_INTENT_ACTION(SEN_RSRC_POWER_1, HAT_HW_RES_OFF),   // Stop L Sensor
    SENS_DELAY(1000),
    SENS_INTENT_ACTION(SEN_RSRC_POWER_1, HAT_HW_RES_ON),    // Enable only sen 1
    SENS_INTENT_ACTION(SEN_RSRC_CLOCK, HAT_HW_RES_ON),      //XSHDN#
    SENS_DELAY(1000),
    SENS_INTENT_CMD_CCI_I2C_RD_CHECK(0x300A, 0x77, 0x50),   // 0x300A = 0x77 && 0x300B = 0x50 -> detect condition is met
    SENS_INTENT_ACTION(SEN_RSRC_POWER_1, HAT_HW_RES_OFF),    // Enable only sen 1
    SENS_INTENT_ACTION(SEN_RSRC_CLOCK, HAT_HW_RES_OFF),      //XSHDN#
    SENS_DELAY(1000),
    CMD_ENTRY_END(),
};
#undef W

#define W BE16
static const hat_cm_socket_command_entry_t ov4188_cm_det_seq[] = {

#if 0
    SENS_INTENT_ACTION(SEN_RSRC_CLOCK, HAT_HW_RES_ON),    //
	SENS_INTENT_ACTION(SEN_RSRC_POWER_1, HAT_HW_RES_ON),  //PRWDN#
    SENS_DELAY(6000),
    SENS_INTENT_ACTION(SEN_RSRC_XSHTDWN, HAT_HW_RES_OFF), //XSHDN#
    SENS_DELAY(6000),
    SENS_INTENT_ACTION(SEN_RSRC_XSHTDWN, HAT_HW_RES_ON), //XSHDN#
    SENS_DELAY(6000),
    SENS_INTENT_CMD_CCI_I2C_RD_CHECK(0x300A, 0x46),
//    SENS_INTENT_ACTION(SEN_RSRC_XSHTDWN, HAT_HW_RES_OFF), //XSHDN#
    SENS_DELAY(1000),
    CMD_ENTRY_END(),

#else
    SENS_INTENT_ACTION(SEN_RSRC_XSHTDWN, HAT_HW_RES_OFF), //XSHDN#
    SENS_DELAY(6000),
//    SENS_INTENT_ACTION(SEN_RSRC_CLOCK, HAT_HW_RES_ON),    //
//    SENS_DELAY(1000),
    SENS_INTENT_ACTION(SEN_RSRC_XSHTDWN, HAT_HW_RES_ON), //XSHDN#
    SENS_DELAY(6000),
    SENS_INTENT_CMD_CCI_I2C_RD_CHECK(0x300A, 0x46),
//    SENS_INTENT_ACTION(SEN_RSRC_XSHTDWN, HAT_HW_RES_OFF), //XSHDN#
    SENS_DELAY(1000),
    CMD_ENTRY_END(),
#endif
};
#undef W
#define W BE16
static const hat_cm_socket_command_entry_t ov5658_cm_det_seq[] = {

	CMD_ENTRY_DUMMY(),
	//Setup clock end
	SENS_INTENT_ACTION(SEN_RSRC_POWER_1, HAT_HW_RES_ON),  //PRWDN#
	SENS_INTENT_ACTION(SEN_RSRC_CLOCK, HAT_HW_RES_ON),    //XSHDN#
	SENS_DELAY(1000),
    SENS_INTENT_ACTION(SEN_RSRC_XSHTDWN, HAT_HW_RES_OFF), //XSHDN#
    SENS_DELAY(1000),
    SENS_INTENT_ACTION(SEN_RSRC_XSHTDWN, HAT_HW_RES_ON), //XSHDN#
    SENS_DELAY(1000),
	SENS_INTENT_CMD_CCI_I2C_RD_CHECK(0x300A, 0x56),
    SENS_INTENT_ACTION(SEN_RSRC_POWER_1, HAT_HW_RES_OFF),  //PRWDN#
    SENS_INTENT_ACTION(SEN_RSRC_XSHTDWN, HAT_HW_RES_OFF), //XSHDN#
    CMD_ENTRY_END(),

//    SENS_INTENT_ACTION(SEN_RSRC_XSHTDWN, HAT_HW_RES_OFF), //XSHDN#
//    SENS_DELAY(6000),
////    SENS_INTENT_ACTION(SEN_RSRC_CLOCK, HAT_HW_RES_ON),    //
////    SENS_DELAY(1000),
//    SENS_INTENT_ACTION(SEN_RSRC_XSHTDWN, HAT_HW_RES_ON), //XSHDN#
//    SENS_DELAY(6000),
//    SENS_INTENT_CMD_CCI_I2C_RD_CHECK(0x300A, 0x56),
//    SENS_INTENT_ACTION(SEN_RSRC_XSHTDWN, HAT_HW_RES_OFF), //XSHDN#
//    SENS_DELAY(1000),
//    CMD_ENTRY_END(),
};
#undef W
static const hat_dtp_cm_desc_t cm_detect_list[] =
{

    { // Dummy camera module
        .cm_dtp_id      = DTP_CAM_ID_DUMMY,
        .cm_name        = "Dummy camera module",
        .sen_dtp_id     = DTP_SEN_DUMMY,
        .lens_dtp_id    = DTP_LENS_DUMMY,
        .lights_dtp_id  = DTP_LIGHTS_DUMMY,
        .nvm_dtp_id     = DTP_NVM_DUMMY,
        .detect_seq = {
            ARR_SIZE(dummy_cm_det_seq),
            dummy_cm_det_seq,
        },
        .cm_features = {
            .cp = {
                .sensor = {
                    .address = 0,
                    .register_size = 2,
                    .slave_addr_bit_size = 7,
                },
                .lens = {
                    .address = 0,
                    .register_size = 2,
                    .slave_addr_bit_size = 7,
                },
                .light = {
                    .address = 0,
                    .register_size = 2,
                    .slave_addr_bit_size = 7,
                },
                .nvm = {
                    .address = 0,
                    .register_size = 2,
                    .slave_addr_bit_size = 7,
                },
            },
        },
    },
	/*
    { // AR0330 camera module 1
        .cm_dtp_id      = DTP_CAM_ID_AR0330,
        .cm_name        = "AR0330",
        .sen_dtp_id     = DTP_SEN_AR0330,
        .lens_dtp_id    = DTP_LENS_DUMMY,
        .lights_dtp_id  = DTP_LIGHTS_DUMMY,
        .nvm_dtp_id     = DTP_NVM_DUMMY,
        .socket_mask    = ((1 << PH_SOCKET_1)|(1 << PH_SOCKET_3)|(1 << PH_SOCKET_5)),
        .detect_seq = {
            ARR_SIZE(ar0330_cm_det_seq),
            ar0330_cm_det_seq,
        },
        .cm_features = {
            .cp = {
                .sensor = {
                    .address = (0x20>>1),
                    .register_size = 2,
                    .slave_addr_bit_size = 7,
                },
                .lens = {
                    .address = 0,
                    .register_size = 2,
                    .slave_addr_bit_size = 7,
                },
                .light = {
                    .address = 0,
                    .register_size = 2,
                    .slave_addr_bit_size = 7,
                },
                .nvm = {
                    .address = 0,
                    .register_size = 2,
                    .slave_addr_bit_size = 7,
                },
            },
        },
    },
    { // AR0330 camera module 2
        .cm_dtp_id      = DTP_CAM_ID_AR0330,
        .cm_name        = "AR0330",
        .sen_dtp_id     = DTP_SEN_AR0330,
        .lens_dtp_id    = DTP_LENS_DUMMY,
        .lights_dtp_id  = DTP_LIGHTS_DUMMY,
        .nvm_dtp_id     = DTP_NVM_DUMMY,
        .socket_mask    = ((1 << PH_SOCKET_2)|(1 << PH_SOCKET_4)|(1 << PH_SOCKET_6)),
        .detect_seq = {
            ARR_SIZE(ar0330_cm_det_seq),
            ar0330_cm_det_seq,
        },
        .cm_features = {
            .cp = {
                .sensor = {
                    .address = (0x30>>1),
                    .register_size = 2,
                    .slave_addr_bit_size = 7,
                },
                .lens = {
                    .address = 0,
                    .register_size = 2,
                    .slave_addr_bit_size = 7,
                },
                .light = {
                    .address = 0,
                    .register_size = 2,
                    .slave_addr_bit_size = 7,
                },
                .nvm = {
                    .address = 0,
                    .register_size = 2,
                    .slave_addr_bit_size = 7,
                },
            },
        },
    },
    { // Aptina camera module
        .cm_dtp_id      = DTP_CAM_ID_AR1330,
        .cm_name        = "Aptina camera module",
        .sen_dtp_id     = DTP_SEN_AR1330,
        .lens_dtp_id    = DTP_LENS_APTINA,
        .lights_dtp_id  = DTP_LIGHTS_DUMMY,
        .nvm_dtp_id     = DTP_NVM_DUMMY,
        .detect_seq = { // hat_cm_socket_command_entry_t
            ARR_SIZE(aptina_cm_det_seq),
            aptina_cm_det_seq,
        },
        .cm_features = {
            .cp = {
                .sensor = {
                    .address = 0x5C,
                    .register_size = 2,
                    .slave_addr_bit_size = 7,
                },
                .lens = {
                    .address = 0x77,
                    .register_size = 2,
                    .slave_addr_bit_size = 7,
                },
                .light = {
                    .address = 1,
                    .register_size = 2,
                    .slave_addr_bit_size = 7,
                },
                .nvm = {
                    .address = 0x77,
                    .register_size = 2,
                    .slave_addr_bit_size = 7,
                },
            },
        },
    },
    */
    { // IMX214 camera module
        .cm_dtp_id      = DTP_CAM_ID_IMX214,
        .cm_name        = "IMX214 camera module",
        .sen_dtp_id     = DTP_SEN_IMX214,
        .lens_dtp_id    = DTP_LENS_DW9714A,
        .lights_dtp_id  = DTP_LIGHTS_AS3645,
        .nvm_dtp_id     = DTP_NVM_DUMMY,
        .detect_seq = { // hat_cm_socket_command_entry_t
            ARR_SIZE(imx214_cm_det_seq),
            imx214_cm_det_seq,
        },
        .cm_features = {
            .cp = {
                .sensor = {
                    .address = 0x10,
                    .register_size = 2,
                    .slave_addr_bit_size = 7,
                },
                .lens = {
                    .address = 0x0C,
                    .register_size = 1,
                    .slave_addr_bit_size = 7,
                },
                .light = {
                    .address = 0x30,
                    .register_size = 1,
                    .slave_addr_bit_size = 7,
                },
                .nvm = {
                    .address = 0,
                    .register_size = 2,
                    .slave_addr_bit_size = 7,
                },
            },
        },
    },
    { // HIGHLANDS(IMX214) camera module
        .cm_dtp_id      = DTP_CAM_ID_HIGHLANDS,
        .cm_name        = "HIGHLANDS camera module",
        .sen_dtp_id     = DTP_SEN_IMX214,
        .lens_dtp_id    = DTP_LENS_LC898212XA,
        .lights_dtp_id  = DTP_LIGHTS_DUMMY,
        .nvm_dtp_id     = DTP_NVM_DUMMY,
        .detect_seq = { // hat_cm_socket_command_entry_t
            ARR_SIZE(highlands_cm_det_seq),
            highlands_cm_det_seq,
        },
        .cm_features = {
            .cp = {
                .sensor = {
                    .address = 0x10,
                    .register_size = 2,
                    .slave_addr_bit_size = 7,
                },
                .lens = {
                    .address = 0,
                    .register_size = 2,
                    .slave_addr_bit_size = 7,
                },
                .light = {
                    .address = 0,
                    .register_size = 2,
                    .slave_addr_bit_size = 7,
                },
                .nvm = {
                    .address = 0,
                    .register_size = 2,
                    .slave_addr_bit_size = 7,
                },
            },
        },
    },
    { // IMX208 camera module
        .cm_dtp_id      = DTP_CAM_ID_IMX208,
        .cm_name        = "IMX208 camera module 1",
        .sen_dtp_id     = DTP_SEN_IMX208,
        .lens_dtp_id    = DTP_LENS_DUMMY,
        .lights_dtp_id  = DTP_LIGHTS_DUMMY,
        .nvm_dtp_id     = DTP_NVM_DUMMY,
        .socket_mask    = 1 << PH_SOCKET_3,
        .detect_seq = { // hat_cm_socket_command_entry_t
            ARR_SIZE(imx208_cm_det_seq),
            imx208_cm_det_seq,
        },
        .cm_features = {
            .cp = {
                .sensor = {
                    //.address = (0x6C >> 1),
                    .address = (0x6E >> 1),
                    .register_size = 2,
                    .slave_addr_bit_size = 7,
                },
                .lens = {
                    .address = 0,
                    .register_size = 2,
                    .slave_addr_bit_size = 7,
                },
                .light = {
                    .address = 0,
                    .register_size = 2,
                    .slave_addr_bit_size = 7,
                },
                .nvm = {
                    .address = 0,
                    .register_size = 2,
                    .slave_addr_bit_size = 7,
                },
            },
        },
    },
    { // IMX208 camera module
        .cm_dtp_id      = DTP_CAM_ID_IMX208,
        .cm_name        = "IMX208 camera module 2",
        .sen_dtp_id     = DTP_SEN_IMX208,
        .lens_dtp_id    = DTP_LENS_DUMMY,
        .lights_dtp_id  = DTP_LIGHTS_DUMMY,
        .nvm_dtp_id     = DTP_NVM_DUMMY,
        .socket_mask    = 1 << PH_SOCKET_1 | 1 << PH_SOCKET_2,
        .detect_seq = { // hat_cm_socket_command_entry_t
            ARR_SIZE(imx208_cm_det_seq),
            imx208_cm_det_seq,
        },
        .cm_features = {
            .cp = {
                .sensor = {
                    .address = (0x6C >> 1),
                    .register_size = 2,
                    .slave_addr_bit_size = 7,
                },
                .lens = {
                    .address = 0,
                    .register_size = 2,
                    .slave_addr_bit_size = 7,
                },
                .light = {
                    .address = 0,
                    .register_size = 2,
                    .slave_addr_bit_size = 7,
                },
                .nvm = {
                    .address = 0,
                    .register_size = 2,
                    .slave_addr_bit_size = 7,
                },
            },
        },
    },
    { // OV13860 camera module
        .cm_dtp_id      = DTP_CAM_ID_OV13860,
        .cm_name        = "OV13860 camera module",
        .sen_dtp_id     = DTP_SEN_OV13860,
        .lens_dtp_id    = DTP_LENS_DUMMY,
        .lights_dtp_id  = DTP_LIGHTS_DUMMY,
        .nvm_dtp_id     = DTP_NVM_DUMMY,
        .detect_seq = { // hat_cm_socket_command_entry_t
            ARR_SIZE(ov13860_cm_det_seq),
            ov13860_cm_det_seq,
        },
        .cm_features = {
            .cp = {
                .sensor = {
                    .address = (0x6C >> 1),
                    .register_size = 2,
                    .slave_addr_bit_size = 7,
                },
                .lens = {
                    .address = 0,
                    .register_size = 2,
                    .slave_addr_bit_size = 7,
                },
                .light = {
                    .address = 0,
                    .register_size = 2,
                    .slave_addr_bit_size = 7,
                },
                .nvm = {
                    .address = 0,
                    .register_size = 2,
                    .slave_addr_bit_size = 7,
                },
            },
        },
    },
    { // OV13860 camera module
        .cm_dtp_id      = DTP_CAM_ID_OV13860,
        .cm_name        = "OV13860 camera module BW",
        .sen_dtp_id     = DTP_SEN_OV13860,
        .lens_dtp_id    = DTP_LENS_DUMMY,
        .lights_dtp_id  = DTP_LIGHTS_DUMMY,
        .nvm_dtp_id     = DTP_NVM_DUMMY,
        .detect_seq = { // hat_cm_socket_command_entry_t
            ARR_SIZE(ov13860_cm_det_seq),
            ov13860_cm_det_seq,
        },
        .cm_features = {
            .cp = {
                .sensor = {
                    .address = (0x20 >> 1),
                    .register_size = 2,
                    .slave_addr_bit_size = 7,
                },
                .lens = {
                    .address = 0,
                    .register_size = 2,
                    .slave_addr_bit_size = 7,
                },
                .light = {
                    .address = 0,
                    .register_size = 2,
                    .slave_addr_bit_size = 7,
                },
                .nvm = {
                    .address = 0,
                    .register_size = 2,
                    .slave_addr_bit_size = 7,
                },
            },
        },
    },
	/*
    { // OV7251 camera module
        .cm_dtp_id      = DTP_CAM_ID_OV7251,
        .cm_name        = "OV7251 camera module 1",
        .sen_dtp_id     = DTP_SEN_OV7251_1,
        .lens_dtp_id    = DTP_LENS_DUMMY,
        .lights_dtp_id  = DTP_LIGHTS_DUMMY,
        .nvm_dtp_id     = DTP_NVM_DUMMY,
        .socket_mask    = 1 << PH_SOCKET_2,
        .detect_seq = { // hat_cm_socket_command_entry_t
            ARR_SIZE(ov7251_cm_det_seq),
            ov7251_cm_det_seq,
        },
        .cm_features = {
            .cp = {
                .sensor = {
                    .address = (0xE0 >> 1),
                    .register_size = 2,
                    .slave_addr_bit_size = 7,
                },
                .lens = {
                    .address = 0,
                    .register_size = 2,
                    .slave_addr_bit_size = 7,
                },
                .light = {
                    .address = 0,
                    .register_size = 2,
                    .slave_addr_bit_size = 7,
                },
                .nvm = {
                    .address = 0,
                    .register_size = 2,
                    .slave_addr_bit_size = 7,
                },
            },
        },
    },
    { // OV7251 camera module
        .cm_dtp_id      = DTP_CAM_ID_OV7251,
        .cm_name        = "OV7251 camera module 2",
        .sen_dtp_id     = DTP_SEN_OV7251_2,
        .lens_dtp_id    = DTP_LENS_DUMMY,
        .lights_dtp_id  = DTP_LIGHTS_DUMMY,
        .nvm_dtp_id     = DTP_NVM_DUMMY,
        .socket_mask    = 1 << PH_SOCKET_3,
        .detect_seq = { // hat_cm_socket_command_entry_t
            ARR_SIZE(ov7251_cm_det_seq),
            ov7251_cm_det_seq,
        },
        .cm_features = {
            .cp = {
                .sensor = {
                    .address = (0xC0 >> 1),
                    .register_size = 2,
                    .slave_addr_bit_size = 7,
                },
                .lens = {
                    .address = 0,
                    .register_size = 2,
                    .slave_addr_bit_size = 7,
                },
                .light = {
                    .address = 0,
                    .register_size = 2,
                    .slave_addr_bit_size = 7,
                },
                .nvm = {
                    .address = 0,
                    .register_size = 2,
                    .slave_addr_bit_size = 7,
                },
            },
        },
    },
    */
	{ // OV5658 camera module
		.cm_dtp_id      = DTP_CAM_ID_OV5658,
		.cm_name        = "OV5658 camera module 0x6c",
		.sen_dtp_id     = DTP_SEN_OV5658,
		.lens_dtp_id    = DTP_LENS_DUMMY,
		.lights_dtp_id  = DTP_LIGHTS_DUMMY,
		.nvm_dtp_id     = DTP_NVM_DUMMY,
		.socket_mask    = 0,// << PH_SOCKET_1 | 1 << PH_SOCKET_2 | 1 << PH_SOCKET_3,
		.detect_seq = { // hat_cm_socket_command_entry_t
			ARR_SIZE(ov5658_cm_det_seq),
			ov5658_cm_det_seq,
		},
		.cm_features = {
			.cp = {
				.sensor = {
					.address = (0x6C >> 1),
					.register_size = 2,
					.slave_addr_bit_size = 7,
				},
				.lens = {
					.address = 0,
					.register_size = 1,
					.slave_addr_bit_size = 7,
				},
				.light = {
					.address = 0,
					.register_size = 1,
					.slave_addr_bit_size = 7,
				},
				.nvm = {
					.address = 0,
					.register_size = 1,
					.slave_addr_bit_size = 7,
				},
			},
		},
	},
	/*ov4188*/
	{ // OV4188 camera module
		.cm_dtp_id      = DTP_CAM_ID_OV4188,
		.cm_name        = "OV4188 camera module1 0x20",
		.sen_dtp_id     = DTP_SEN_OV4188,
		.lens_dtp_id    = DTP_LENS_DUMMY,
		.lights_dtp_id  = DTP_LIGHTS_DUMMY,
		.nvm_dtp_id     = DTP_NVM_DUMMY,
	   .socket_mask    = 1 << PH_SOCKET_1 ,
		.detect_seq = { // hat_cm_socket_command_entry_t
			ARR_SIZE(ov4188_cm_det_seq),
			ov4188_cm_det_seq,
		},
		.cm_features = {
			.cp = {
				.sensor = {
					.address = (0x20>> 1),
					.register_size = 2,
					.slave_addr_bit_size = 7,
				},
				.lens = {
					.address = 0,
					.register_size = 1,
					.slave_addr_bit_size = 7,
				},
				.light = {
					.address = 0,
					.register_size = 1,
					.slave_addr_bit_size = 7,
				},
				.nvm = {
					.address = 0,
					.register_size = 1,
					.slave_addr_bit_size = 7,
				},
			},
		},
	},
 { // OV4188 camera module
					.cm_dtp_id      = DTP_CAM_ID_OV4188,
					.cm_name        = "OV4188 camera module2 0x6c",
					.sen_dtp_id     = DTP_SEN_OV4188,
					.lens_dtp_id    = DTP_LENS_DUMMY,
					.lights_dtp_id  = DTP_LIGHTS_DUMMY,
					.nvm_dtp_id     = DTP_NVM_DUMMY,
				    .socket_mask    = 1 << PH_SOCKET_2,
					.detect_seq = { // hat_cm_socket_command_entry_t
						ARR_SIZE(ov4188_cm_det_seq),
						ov4188_cm_det_seq,
					},
					.cm_features = {
						.cp = {
							.sensor = {
								.address = (0x6C >> 1),
								.register_size = 2,
								.slave_addr_bit_size = 7,
							},
							.lens = {
								.address = 0,
								.register_size = 1,
								.slave_addr_bit_size = 7,
							},
							.light = {
								.address = 0,
								.register_size = 1,
								.slave_addr_bit_size = 7,
							},
							.nvm = {
								.address = 0,
								.register_size = 1,
								.slave_addr_bit_size = 7,
							},
						},
					},
				},
};

static hat_dtp_cm_db_t dtp_cm_db = {
    //uint32 cm_list_size;
    (sizeof(cm_detect_list)/sizeof(cm_detect_list[0])),
    //hat_dtp_cm_desc_t (*cm_list);
    cm_detect_list,
};

#define DTP_CAMERA_MODULES_DB_SIZE ( \
    sizeof(dtp_cm_db) \
)

#endif //__DTP_CM_DB_H__
