/* =============================================================================
* Copyright (c) 2013-2015 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file dtp_camera.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 12-May-2015 : Author ( MM Solutions AD ) Evgeniy Borisov
*! Created
* =========================================================================== */

#include <hal/hal_camera_module/hat_cm_dtp_data.h>
#include "dtp_camera.h"

/* camera DTP */
#include <camera_module_dtp/cameras/dtp_cm_db.h>

/* sensors DTP */
#include <camera_module_dtp/sensors/dtp_sensors_db.h>

/* lenses DTP */
#include <camera_module_dtp/lenses/dtp_lenses_db.h>

/* lights DTP */
#include <camera_module_dtp/lights/dtp_lights_db.h>

/* NVM DTP */
#include "camera_module_dtp/nvms/dtp_nvms_db.h"

int dtp_camera_query(dtpdb_cd_id_t dtp_id, int mod_id, dtp_camera_query_t *dtp_camera_query)
{
    switch (dtp_id) {
        case DTP_ID_CD_CAMERA_MODULE:
            dtp_camera_query->leaf_size = DTP_CAMERA_MODULES_DB_SIZE;
            dtp_camera_query->leaf_ver  = 0x100;
            dtp_camera_query->leaf_private_data = NULL;
            dtp_camera_query->cur_db_ptr = &dtp_cm_db;
            break;
        case DTP_ID_CD_SENSOR:
            dtp_camera_query->leaf_size = get_sensor_dtp_db_size(mod_id);
            dtp_camera_query->leaf_ver  = 0x100;
            dtp_camera_query->leaf_private_data = NULL;
            dtp_camera_query->cur_db_ptr = get_sensor_dtp_db(mod_id);
            break;
        case DTP_ID_CD_LENS:
            dtp_camera_query->leaf_size = get_lens_dtp_db_size(mod_id);
            dtp_camera_query->leaf_ver  = 0x100;
            dtp_camera_query->leaf_private_data = NULL;
            dtp_camera_query->cur_db_ptr = get_lens_dtp_db(mod_id);
            break;
        case DTP_ID_CD_LIGHTS:
            dtp_camera_query->leaf_size = get_light_dtp_db_size(mod_id);
            dtp_camera_query->leaf_ver  = 0x100;
            dtp_camera_query->leaf_private_data = NULL;
            dtp_camera_query->cur_db_ptr = get_light_dtp_db(mod_id);
            break;
        case DTP_ID_CD_NVM:
            dtp_camera_query->leaf_size = get_nvm_dtp_db_size(mod_id);
            dtp_camera_query->leaf_ver  = 0x100;
            dtp_camera_query->leaf_private_data = NULL;
            dtp_camera_query->cur_db_ptr = get_nvm_dtp_db(mod_id);
            break;
        default:
            dtp_camera_query->leaf_size = 0;
            dtp_camera_query->leaf_ver  = 0;
            dtp_camera_query->leaf_private_data = NULL;
            dtp_camera_query->cur_db_ptr = NULL;
            break;
    }

    return 0;
}

