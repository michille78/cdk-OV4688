MAKE_TARGET_OS=RTEMS

# SENSOR_MODULES="IMX214 AR1330 IMX208 OV13860 AR0330 OV7251"
SENSOR_MODULES="IMX214 AR1330 IMX208 OV13860 OV5658"

# LENS_MODULES="APTINA DW9714A LC898212XA"
LENS_MODULES="APTINA DW9714A LC898212XA"

# LIGHTS_MODULES="AS3645"
LIGHTS_MODULES="AS3645"

# CAMERA_MODULE_DTP="AR1330 IMX214 IMX208 OV13860 AR0330 IMX_DUAL_214 OV7251"
CAMERA_MODULE_DTP="AR1330 IMX214 IMX208 OV13860 AR0330 IMX_DUAL_214 OV5658"

# C Flags
#CFLAGS += -Wall -Wextra -Wcast-align -Wno-multichar -Wno-unused -Wstrict-aliasing=2
#CFLAGS += -Wall -Wcast-align -Wno-multichar -Wno-unused -Wstrict-aliasing=2

#CFLAGS += -Wstrict-overflow=1 -Wswitch -Wtrigraphs -Wuninitialized -Wunknown-pragmas
#CFLAGS += -Wunused-function -Wunused-label -Wunused-value -Wunused-variable
#CFLAGS += -Wsign-compare -Wmissing-prototypes -Wmissing-declarations
#CFLAGS += -Wunused-parameter -Wsign-conversion

DEFAULT_OPTIMIZATION_LEVEL ?= 0

#suppress some warnings until we remove old code
CFLAGS += -Wall -Wcast-align -Wno-multichar -Wno-unused -Wstrict-aliasing=2
CFLAGS += -Wstrict-overflow=1 -Wswitch -Wtrigraphs -Wuninitialized -Wunknown-pragmas
CFLAGS += -Wunused-function -Wunused-label -Wunused-value -Wunused-variable
CFLAGS += -Wformat=0
#CFLAGS += -Wmissing-prototypes -Wmissing-declarations
CFLAGS += -Wno-unused-parameter -Wno-cast-align
# -Wsign-conversion
CFLAGS += -fno-strict-aliasing -pipe
CFLAGS += -O$(DEFAULT_OPTIMIZATION_LEVEL) -g

CPPFLAGS += -Wenum-compare

CFLAGS += \
    -DMYRIAD2 \
    -DLEON_MVT \
    -D__RTEMS__ \
    -DDRAM_SIZE_MB=64 \
	--sysroot=. 	\
	-mcpu=myriad 	\
	-fno-inline-functions-called-once \
	-ffunction-sections \
	-fdata-sections \
	-fno-builtin-isinff \
	-fno-inline-small-functions  \
	-gdwarf-2 \
	-gstrict-dwarf \
	-g3 \

#	-fno-common \

# Shared Flags
SHRFLAGS +=

# Linker Flags
LDFLAGS += \
    -EL \
    -nostdlib \

LDFLAGS_APP += \
    -Wl,-EL \
    -O9 \
    -Wl,-M \
    -Wl,--gc-sections \
    -Wl,--allow-multiple-definition \
    -L $(MV_MDK_DIR)/common/scripts/ld/myriad2collection/ \
    -T $(MV_MDK_DIR)/common/scripts/ld/myriad2collection/myriad2_default_memory_map_elf.ldscript \

#    -Wl,-warn-common \

CPPFLAGS +=  \
    -fno-rtti \
    -fno-exceptions \


CRT_BEG += \
    $(SPARC_TOOLS_DIR)/lib/gcc/sparc-myriad-elf/4.8.2/le/crti.o \
    $(SPARC_TOOLS_DIR)/lib/gcc/sparc-myriad-elf/4.8.2/le/crtbegin.o \

CRT_MDK += \
    $(MV_MDK_DIR)/common/drivers/myriad2/system/rtems_asm/crt0.S \
    $(MV_MDK_DIR)/common/drivers/myriad2/system/rtems_asm/memmap.S \
    $(MV_MDK_DIR)/common/drivers/myriad2/system/rtems_asm/mp_rom.S \
    $(MV_MDK_DIR)/common/drivers/myriad2/system/rtems_asm/utilities.S \

CRT_END += \
    $(SPARC_TOOLS_DIR)/lib/gcc/sparc-myriad-elf/4.8.2/le/crtend.o \
    $(SPARC_TOOLS_DIR)/lib/gcc/sparc-myriad-elf/4.8.2/le/crtn.o \

LIB +=  rtemsbsp rtemscpu stdc++ gcc c supc++ m g ssp gcov ssp_nonshared
LIBDIR += $(DIRINSLIB)
LIBDIR += $(RTEMS_PREBUILD_DIR)

LIBDIR += $(MV_MDK_DIR)/common/scripts/ld/myriad2collection
LIBDIR += $(SPARC_TOOLS_DIR)/lib/gcc/sparc-myriad-elf/4.8.2/le
LIBDIR += $(SPARC_TOOLS_DIR)/sparc-myriad-elf/lib/le

# External Include folders
INCDIR += $(MOV_INCDIR_OSAL)
INCDIR += $(MOV_INCDIR_ALGOS)/aca/
INCDIR += $(MOV_INCDIR_ALGOS)/sg/algs/

INCDIR += $(MOV_INCDIR_SDK)
INCDIR += $(MOV_INCDIR_HAL)
INCDIR += $(MOV_INCDIR_HAL)/../..
INCDIR += $(MOV_INCDIR_DTP_SRV)
INCDIR += $(MOV_INCDIR_GZZ)
INCDIR += $(MOV_INCDIR_GZZ)/..
INCDIR += $(MOV_INCDIR_GZZ)/../libs
INCDIR += $(GUZZI_INC_DIR)/guzzi/modules
INCDIR += $(GUZZI_INC_DIR)/guzzi/modules/libs
INCDIR += $(GUZZI_INC_DIR)/guzzi/modules/include


INCDIR += $(MV_MDK_DIR)/common/drivers/myriad2/socDrivers/leon/bm/include
INCDIR += $(MV_MDK_DIR)/common/drivers/myriad2/socDrivers/leon/bm/arch/$(MYRIAD_VER)/include
INCDIR += $(MV_MDK_DIR)/common/swCommon/include
INCDIR += $(MV_MDK_DIR)/common/drivers/myriad2/socDrivers/leon/rtems/include
INCDIR += $(MV_MDK_DIR)/common/drivers/myriad2/socDrivers/leon/hgl/include
INCDIR += $(MV_MDK_DIR)/common/drivers/myriad2/socDrivers/leon/hgl/arch/$(MYRIAD_VER)/include
INCDIR += $(MV_MDK_DIR)/common/drivers/myriad2/socDrivers/shared/arch/$(MYRIAD_VER)/sgl/include

DEFAULT_TIMEOUT_MS ?= 100000

ifeq ($(MYRIAD_VER), ma2x5x)
    CFLAGS += -DMA2150
else
    CFLAGS += -DMA2100
endif

CFLAGS +=-DDEFAULT_ALLOC_TIMEOUT_MS=$(DEFAULT_TIMEOUT_MS)

CFLAGS += -DCAMERA_INTERFACE_ANDROID_CAMERA3

# Flash simulator
CFLAGS += -DGZZ_FLASH_SIM

# List of excluding include folders when make check
#CHKDIR += $(TOPDIR)/external

CONFIG_OSAL_MSG = FIFO

