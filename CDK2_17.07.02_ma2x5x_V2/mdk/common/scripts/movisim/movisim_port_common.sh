
if [[ ! -z "$JENKINS_HOME" ]]; then
  WORKING_DIR=$JENKINS_HOME/tmp
else
  WORKING_DIR=$HOME/tmp
fi

[[ ! -d "$WORKING_DIR" ]] && mkdir -p $WORKING_DIR

MOVISIM_PORT_LOCK_FILE=$WORKING_DIR/get_movisim_port.lock
PORT_LOCK_BASENAME=movisim_port

