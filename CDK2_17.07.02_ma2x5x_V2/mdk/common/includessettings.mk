###################################################################
#       Building up list of Leon low level include folders        #
###################################################################

# BR - may want to include shave code!!
ifneq ($(MV_SOC_REV),$(filter $(MV_SOC_REV),ma2480 ma2485))
# On MA2x8x Architecture we don't currently include the common base
CC_INCLUDE       += $(addprefix -I,$(wildcard     $(MV_DRIVERS_BASE)/$(MV_SOC_PLATFORM)/socDrivers/leon/bm/include \
                                                  $(MV_DRIVERS_BASE)/$(MV_SOC_PLATFORM)/socDrivers/leon/bm/include \
                                                  $(MV_DRIVERS_BASE)/$(MV_SOC_PLATFORM)/socDrivers/leon/bm/src \
                                                  $(MV_DRIVERS_BASE)/$(MV_SOC_PLATFORM)/socDrivers/leon/hgl/src \
                                                  $(MV_DRIVERS_BASE)/$(MV_SOC_PLATFORM)/socDrivers/leon/hgl/include \
                                                  $(MV_DRIVERS_BASE)/$(MV_SOC_PLATFORM)/socDrivers/shared/include \
                                                  $(MV_DRIVERS_BASE)/$(MV_SOC_PLATFORM)/brdDrivers/include \
                                                  $(MV_DRIVERS_BASE)/$(MV_SOC_PLATFORM)/brdDrivers/include/brdGpioCfgs ))
endif

# Added options for both leons so that we don't want to grant access to bare-metal drivers from RTEMS in the future
CC_INCLUDE       += $(addprefix -I,$(wildcard     $(DirSparcDefaultLibs)/include \
                                                  $(MV_LEON_LIBC_BASE)/include \
                                                  $(MV_SWCOMMON_BASE)/include \
                                                  $(MV_SWCOMMON_IC)/include \
                                                  $(MV_SHARED_BASE)/include \
                                                  $(MV_SWCOMMON_BASE)/shave_code/$(MV_SOC_PLATFORM)/include \
                                                  $(DirAppRoot)/leon \
                                                  $(DirAppRoot)/shared \
                                                  $(MV_TOOLS_BASE)/$(DETECTED_PLATFORM)/$(SPARC_DIR)/lib/gcc/$(MV_GCC_TOOLS)/$(GCCVERSION)/include \
                                                  $(MV_TOOLS_BASE)/$(DETECTED_PLATFORM)/$(SPARC_DIR)/lib/gcc/$(MV_GCC_TOOLS)/$(GCCVERSION)/include/ssp \
                                                  $(MV_TOOLS_BASE)/$(DETECTED_PLATFORM)/$(SPARC_DIR)/$(MV_GCC_TOOLS)/include \
                                                  $(MV_TOOLS_BASE)/$(DETECTED_PLATFORM)/$(SPARC_DIR)/$(MV_GCC_TOOLS)/include/c++/$(GCCVERSION)))


CC_INCLUDE  += $(foreach var, $(SOC_REV_DIR), $(addprefix -I,$(MV_DRIVERS_BASE)/$(MV_SOC_PLATFORM)/socDrivers/leon/bm/arch/$(var)/include \
                                                  $(MV_DRIVERS_BASE)/$(MV_SOC_PLATFORM)/socDrivers/leon/bm/arch/$(var)/src \
                                                  $(MV_DRIVERS_BASE)/$(MV_SOC_PLATFORM)/socDrivers/leon/hgl/arch/$(var)/include \
                                                  $(MV_DRIVERS_BASE)/$(MV_SOC_PLATFORM)/socDrivers/leon/hgl/arch/$(var)/src \
                                                  $(MV_DRIVERS_BASE)/$(MV_SOC_PLATFORM)/brdDrivers/arch/$(var)/include ))

$(call DEBUG_PRINT_VAR,CC_INCLUDE)

ifneq ($(MV_SOC_REV),$(filter $(MV_SOC_REV),ma2480 ma2485))
# On MA2x8x Architecture we don't currently include the common base
CC_INCLUDE_LRT   += $(patsubst %,-I%,$(wildcard   $(MV_DRIVERS_BASE)/$(MV_SOC_PLATFORM)/socDrivers/leon/bm/include \
						  $(MV_DRIVERS_BASE)/$(MV_SOC_PLATFORM)/socDrivers/leon/bm/include \
						  $(MV_DRIVERS_BASE)/$(MV_SOC_PLATFORM)/socDrivers/leon/bm/src \
						  $(MV_DRIVERS_BASE)/$(MV_SOC_PLATFORM)/socDrivers/leon/hgl/src \
						  $(MV_DRIVERS_BASE)/$(MV_SOC_PLATFORM)/socDrivers/leon/hgl/include \
						  $(MV_DRIVERS_BASE)/$(MV_SOC_PLATFORM)/socDrivers/shared/include \
						  $(MV_DRIVERS_BASE)/$(MV_SOC_PLATFORM)/brdDrivers/include/brdGpioCfgs \
						  $(MV_DRIVERS_BASE)/$(MV_SOC_PLATFORM)/brdDrivers/include ))
endif

CC_INCLUDE_LRT   += $(patsubst %,-I%,$(wildcard   $(DirSparcDefaultLibs)/include \
                                                  $(MV_SWCOMMON_BASE)/include \
                                                  $(MV_SWCOMMON_IC)/include \
                                                  $(MV_SHARED_BASE)/include \
                                                  $(DirAppRoot)/leon $(DirAppRoot)/shared \
                                                  $(MV_SWCOMMON_BASE)/shave_code/$(MV_SOC_PLATFORM)/include \
                                                  $(DirAppRoot)/leon \
                                                  $(DirAppRoot)/shared \
                                                  $(MV_TOOLS_BASE)/$(DETECTED_PLATFORM)/$(SPARC_DIR)/lib/gcc/$(MV_GCC_TOOLS)/$(GCCVERSION)/include \
                                                  $(MV_TOOLS_BASE)/$(DETECTED_PLATFORM)/$(SPARC_DIR)/lib/gcc/$(MV_GCC_TOOLS)/$(GCCVERSION)/include/ssp \
                                                  $(MV_TOOLS_BASE)/$(DETECTED_PLATFORM)/$(SPARC_DIR)/$(MV_GCC_TOOLS)/include \
                                                  $(MV_TOOLS_BASE)/$(DETECTED_PLATFORM)/$(SPARC_DIR)/$(MV_GCC_TOOLS)/include/c++/$(GCCVERSION)))


CC_INCLUDE_LRT  += $(foreach var, $(SOC_REV_DIR), $(addprefix -I, $(MV_DRIVERS_BASE)/$(MV_SOC_PLATFORM)/socDrivers/leon/bm/arch/$(var)/include \
                                                  $(MV_DRIVERS_BASE)/$(MV_SOC_PLATFORM)/socDrivers/leon/bm/arch/$(var)/src \
                                                  $(MV_DRIVERS_BASE)/$(MV_SOC_PLATFORM)/socDrivers/leon/hgl/arch/$(var)/include \
                                                  $(MV_DRIVERS_BASE)/$(MV_SOC_PLATFORM)/socDrivers/leon/hgl/arch/$(var)/src \
                                                  $(MV_DRIVERS_BASE)/$(MV_SOC_PLATFORM)/brdDrivers/arch/$(var)/include ))

# If we are using RTEMS we should not use MDK Libc definitions LOS
ifneq ($(MV_SOC_OS), rtems)
  CC_INCLUDE      += $(patsubst %,-I%,$(wildcard $(MV_COMMON_BASE)/libc/$(MV_SOC_PLATFORM)/leon/include))
ifneq ($(MV_SOC_REV),$(filter $(MV_SOC_REV),ma2480 ma2485))
  CC_INCLUDE      += $(patsubst %,-I%,$(wildcard $(MV_DRIVERS_BASE)/$(MV_SOC_PLATFORM)/socDrivers/leon/rtems/include))
endif
  CC_INCLUDE      += $(patsubst %,-I%,$(wildcard $(MV_DRIVERS_BASE)/$(MV_SOC_PLATFORM)/socDrivers/leon/rtems/arch/$(MV_SOC_REV)/include))
  CC_INCLUDE      += $(patsubst %,-I%,$(wildcard $(MV_DRIVERS_BASE)/$(MV_SOC_PLATFORM)/socDrivers/leon/cross/include))
else
ifneq ($(MV_SOC_REV),$(filter $(MV_SOC_REV),ma2480 ma2485))
  CC_INCLUDE      += $(patsubst %,-I%,$(wildcard $(MV_DRIVERS_BASE)/$(MV_SOC_PLATFORM)/socDrivers/leon/rtems/include))
endif
  CC_INCLUDE      += $(patsubst %,-I%,$(wildcard $(MV_DRIVERS_BASE)/$(MV_SOC_PLATFORM)/socDrivers/leon/rtems/arch/$(MV_SOC_REV)/include))
  CC_INCLUDE      += $(patsubst %,-I%,$(wildcard $(MV_DRIVERS_BASE)/$(MV_SOC_PLATFORM)/brdDrivers/osDrivers/include))
  CC_INCLUDE      += $(patsubst %,-I%,$(wildcard $(MV_DRIVERS_BASE)/$(MV_SOC_PLATFORM)/icDrivers/osDrivers/include))
  USB_INCLUDE     += $(patsubst %,$(MV_PACKAGES_BASE)/usb/include/%,$(MV_USB_COMPONENTS))
  USB_INCLUDE     += $(MV_PACKAGES_BASE)/usb/include
  CC_INCLUDE      += $(patsubst %,-I%,$(wildcard $(USB_INCLUDE)))
endif

# If we are using RTEMS we should not use MDK Libc definitions LRT
ifneq ($(MV_SOC_OS_LRT), rtems)
  CC_INCLUDE_LRT      += $(patsubst %,-I%,$(wildcard $(MV_COMMON_BASE)/libc/$(MV_SOC_PLATFORM)/leon/include))
ifneq ($(MV_SOC_REV),$(filter $(MV_SOC_REV),ma2480 ma2485))
  CC_INCLUDE_LRT      += $(patsubst %,-I%,$(wildcard $(MV_DRIVERS_BASE)/$(MV_SOC_PLATFORM)/socDrivers/leon/rtems/include))
endif
  CC_INCLUDE_LRT      += $(patsubst %,-I%,$(wildcard $(MV_DRIVERS_BASE)/$(MV_SOC_PLATFORM)/socDrivers/leon/rtems/arch/$(MV_SOC_REV)/include))
  CC_INCLUDE_LRT      += $(patsubst %,-I%,$(wildcard $(MV_DRIVERS_BASE)/$(MV_SOC_PLATFORM)/socDrivers/leon/cross/include))
else
ifneq ($(MV_SOC_REV),$(filter $(MV_SOC_REV),ma2480 ma2485))
  CC_INCLUDE      += $(patsubst %,-I%,$(wildcard $(MV_DRIVERS_BASE)/$(MV_SOC_PLATFORM)/socDrivers/leon/rtems/include))
endif
  CC_INCLUDE      += $(patsubst %,-I%,$(wildcard $(MV_DRIVERS_BASE)/$(MV_SOC_PLATFORM)/socDrivers/leon/rtems/arch/$(MV_SOC_REV)/include))
  CC_INCLUDE      += $(patsubst %,-I%,$(wildcard $(MV_DRIVERS_BASE)/$(MV_SOC_PLATFORM)/brdDrivers/osDrivers/include))
  CC_INCLUDE      += $(patsubst %,-I%,$(wildcard $(MV_DRIVERS_BASE)/$(MV_SOC_PLATFORM)/icDrivers/osDrivers/include))
  USB_INCLUDE_LRT     += $(patsubst %,$(MV_PACKAGES_BASE)/usb/include/%,$(MV_USB_COMPONENTS_LRT))
  USB_INCLUDE_LRT     += $(MV_PACKAGES_BASE)/usb/include
  CC_INCLUDE_LRT      += $(patsubst %,-I%,$(wildcard $(USB_INCLUDE_LRT)))
endif

#And Leon include dirs LOS
LEON_INCLUDE_PATH   += $(filter-out $(MV_SWCOMMON_IC)/include $(MV_SWCOMMON_BASE)/include, $(patsubst %/,%,$(sort $(dir $(LEON_HEADERS)))))
#And blindly add all component paths
LEON_INCLUDE_PATH += $(patsubst %,$(MV_COMPONENTS_DIR)/%,$(ComponentList_LOS))
LEON_INCLUDE_PATH += $(patsubst %,$(MV_COMPONENTS_DIR)/%/include,$(ComponentList_LOS))

#And Leon include dirs LRT
LEON_INCLUDE_PATH_LRT   += $(filter-out $(MV_SWCOMMON_IC)/include $(MV_SWCOMMON_BASE)/include, $(patsubst %/,%,$(sort $(dir $(LEON_HEADERS_LRT)))))
#And blindly add all component paths
LEON_INCLUDE_PATH_LRT += $(patsubst %,$(MV_COMPONENTS_DIR)/%,$(ComponentList_LRT))
LEON_INCLUDE_PATH_LRT += $(patsubst %,$(MV_COMPONENTS_DIR)/%/include,$(ComponentList_LRT))

CC_INCLUDE     += $(addprefix -I,$(wildcard $(LEON_INCLUDE_PATH)))
CC_INCLUDE_LRT += $(addprefix -I,$(wildcard $(LEON_INCLUDE_PATH_LRT)))


#Add the include folders to the CCOPT variable
CCOPT += $(CC_INCLUDE)
CCOPT_LRT += $(CC_INCLUDE_LRT)

###################################################################
#       Building up list of RTEMS required include folders        #
###################################################################
## LOS
ifeq ($(MV_SOC_OS), rtems)
  #TODO: figure out after resolution of internal Bugzilla 24623 if isystem here can be changed to just -I
  CCOPT += -isystem $(MV_RTEMS_LOS_LIB)/include
  CCOPT += -I $(MV_TOOLS_BASE)/$(DETECTED_PLATFORM)/$(SPARC_DIR)/$(MV_GCC_TOOLS)/include
  CCOPT += -I $(MV_TOOLS_BASE)/$(DETECTED_PLATFORM)/$(SPARC_DIR)
  CCOPT += -I $(MV_TOOLS_BASE)/$(DETECTED_PLATFORM)/$(SPARC_DIR)/lib/gcc
endif
## LRT
ifeq ($(MV_SOC_OS_LRT), rtems)
  #TODO: figure out after resolution of internal Bugzilla 24623 if isystem here can be changed to just -I
  CCOPT_LRT += -isystem $(MV_RTEMS_LRT_LIB)/include
  CCOPT_LRT += -I $(MV_TOOLS_BASE)/$(DETECTED_PLATFORM)/$(SPARC_DIR)/$(MV_GCC_TOOLS)/include
  CCOPT_LRT += -I $(MV_TOOLS_BASE)/$(DETECTED_PLATFORM)/$(SPARC_DIR)
  CCOPT_LRT += -I $(MV_TOOLS_BASE)/$(DETECTED_PLATFORM)/$(SPARC_DIR)/lib/gcc
endif


###################################################################
#       Building up list of Shave assembly include folders        #
###################################################################

MVASM_INCLUDE     += $(patsubst %,-i:%,$(wildcard $(MV_COMMON_BASE)/swCommon/shave_code/$(MV_SOC_PLATFORM)/include \
                                                  $(MV_COMMON_BASE)/swCommon/shave_code/$(MV_SOC_PLATFORM)/asm))

ifeq ($(MV_SOC_REV),$(filter $(MV_SOC_REV),ma2480 ma2485))
MVASM_INCLUDE     += $(patsubst %,-i:%,$(wildcard  $(MV_SWCOMMON_BASE)/shave_code/$(MV_SOC_PLATFORM)/include/arch/ma2x8x \
                                                   $(MV_DRIVERS_BASE)/$(MV_SOC_PLATFORM)/socDrivers/shave/arch/ma2x8x/include))
else
MVASM_INCLUDE     += $(patsubst %,-i:%,$(wildcard  $(MV_SWCOMMON_BASE)/shave_code/$(MV_SOC_PLATFORM)/include/arch/ma2x5x \
                                                   $(MV_DRIVERS_BASE)/$(MV_SOC_PLATFORM)/socDrivers/shave/arch/ma2x5x/include))
endif

#Adding common folder includes
MVASMOPT		 += $(MVASM_INCLUDE)

###################################################################
#       Building up list of Shave C/C++ include folders           #
###################################################################

MVCC_INCLUDE      += $(patsubst %,-I %, $(MV_TOOLS_BASE)/common/moviCompile/include \
						   $(MV_PACKAGES_BASE)/pcModel/moviCompile/compilerIntrinsics/include \
						   $(MV_PACKAGES_BASE)/pcModel/moviCompile/compilerVectorFunctions/include \
                           $(MV_COMMON_BASE)/swCommon/include \
                           $(MV_COMMON_BASE)/swCommon/shave_code/$(MV_SOC_PLATFORM)/include \
                           $(MV_SHARED_BASE)/include \
                           $(DirAppRoot)/shave \
                           $(DirAppRoot)/shared)

ifeq ($(MV_SOC_REV),$(filter $(MV_SOC_REV),ma2480 ma2485))
MVCC_INCLUDE     += $(patsubst %, -I%,$(wildcard  $(MV_SWCOMMON_BASE)/shave_code/$(MV_SOC_PLATFORM)/include/arch/ma2x8x ))
else
MVCC_INCLUDE     += $(patsubst %, -I%,$(wildcard  $(MV_SWCOMMON_BASE)/shave_code/$(MV_SOC_PLATFORM)/include/arch/ma2x5x ))
endif

ifneq ($(MV_SOC_REV),$(filter $(MV_SOC_REV),ma2480 ma2485))
MVCC_INCLUDE      += $(patsubst %,-I %, \
                           $(MV_DRIVERS_BASE)/$(MV_SOC_PLATFORM)/socDrivers/leon/bm/include \
                           $(MV_DRIVERS_BASE)/$(MV_SOC_PLATFORM)/socDrivers/leon/hgl/include \
                           $(MV_DRIVERS_BASE)/$(MV_SOC_PLATFORM)/socDrivers/shared/include \
                           $(MV_DRIVERS_BASE)/$(MV_SOC_PLATFORM)/socDrivers/shave/include)
endif

MVCC_INCLUDE      += $(foreach var, $(SOC_REV_DIR), $(patsubst %,-I %, \
                           $(MV_DRIVERS_BASE)/$(MV_SOC_PLATFORM)/socDrivers/leon/bm/arch/$(var)/include \
                           $(MV_DRIVERS_BASE)/$(MV_SOC_PLATFORM)/socDrivers/leon/hgl/arch/$(var)/include \
                           $(MV_DRIVERS_BASE)/$(MV_SOC_PLATFORM)/socDrivers/shave/arch/$(var)/include \
                           $(MV_DRIVERS_BASE)/$(MV_SOC_PLATFORM)/socDrivers/shared/arch/$(var)/include \
                           $(MV_DRIVERS_BASE)/$(MV_SOC_PLATFORM)/socDrivers/shared/arch/$(var)/sgl/include \
                           $(MV_DRIVERS_BASE)/$(MV_SOC_PLATFORM)/socDrivers/shared/arch/$(var)/hgl/include))

#And Components include directories
SHAVE_COMPONENT_INCLUDE_PATHS  += $(patsubst %/,%,$(sort $(dir $(SHAVE_COMPONENT_HEADERS))))
#And blindly add all component paths
SHAVE_COMPONENT_INCLUDE_PATHS += $(patsubst %,$(MV_COMPONENTS_DIR)/%,$(ComponentList_SVE))

SHAVE_APP_HEADER_PATHS += $(patsubst %/,%,$(sort $(dir $(SHAVE_HEADERS))))

MVCC_INCLUDE += $(patsubst %,-I %,$(sort $(SHAVE_COMPONENT_INCLUDE_PATHS) $(SHAVE_APP_HEADER_PATHS)))

MVCCOPT += $(MVCC_INCLUDE)
