#ifndef SWCFIFO_H__
#define SWCFIFO_H__
#include <stdint.h>

typedef struct swcFifo_t {
    uint8_t *memory;
    int32_t size;

    int32_t unreadSize;

    int32_t writeIndex;
    int32_t activeWriteSize;

    int32_t readIndex;
    int32_t activeReadSize;

} swcFifo_t;

int32_t swcFifoInit(struct swcFifo_t * hndl, void* buffer, int32_t size);

int32_t swcFifoPush32Bit(struct swcFifo_t * hndl, uint32_t data);
int32_t swcFifoPush16Bit(struct swcFifo_t * hndl, uint16_t data);
int32_t swcFifoPush8Bit (struct swcFifo_t * hndl, uint8_t  data);

int32_t swcFifoPop32Bit(struct swcFifo_t * hndl, uint32_t *data);
int32_t swcFifoPop16Bit(struct swcFifo_t * hndl, uint16_t *data);
int32_t swcFifoPop8Bit (struct swcFifo_t * hndl, uint8_t  *data);

int32_t swcFifoGetWritePtr(struct swcFifo_t * hndl, void ** ptr, uint32_t reqLen);
int32_t swcFifoMarkWriteDone(struct swcFifo_t * hndl);

int32_t swcFifoGetReadPtr(struct swcFifo_t * hndl, void ** ptr, uint32_t reqLen);
int32_t swcFifoMarkReadDone(struct swcFifo_t * hndl);

uint32_t swcFifoAvailable(struct swcFifo_t * hndl);
uint32_t swcFifoContigAvailable(struct swcFifo_t * hndl);

uint32_t swcFifoLength(struct swcFifo_t * hndl);

#endif
