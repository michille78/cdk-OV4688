
#ifndef _THE_EXEC_CONTEXT_H_
#define _THE_EXEC_CONTEXT_H_ (1)

/* ----------------------------------------------------------------------------------------
 * System Execution:
 *   Extended interface for managing the initialization, finalization and termination
 *   of SHAVE programs.
 */
typedef void (*_TorFn_t)(void);

/*
 * This structure describes the elements of the Dynamic Loading Context that are handling by the CRT
 * initialisation and finalisation routines.  All "Entry-Points" defined on SHAVE with
 * '__attribute__((dllexport))' capture the following values:
 *
 *    IRF 19  - This is the stack pointer, and used to measure stack usage
 *    IRF 20  - This is the lowest allowed value for the stack pointer (the stack grows towards zero)
 *              If exceeded, the stack has overflowed
 *    IRF 21  - This is the pointer to the '_ExecutionContext_t' described below
 *              Only the CRT routines '_EP_start', '_EP_crtinit', '_EP_crtfini', '__crtinit' and
 *              '__crtfini' use this value
 */
typedef struct _ExecutionContext_t {
    uint32_t  version;        /* The version number for this definition of '_ExecutionContext_t' */

    _TorFn_t* ctors_start;    /* '__init_array_start' to allow loading the application constructors */
    _TorFn_t* ctors_end;      /* '__init_array_end' symbol marks the end of the application .ctors section */

    _TorFn_t* dtors_start;    /* '__fini_array_start' to allow loading the application destructors */
    _TorFn_t* dtors_end;      /* '__fini_array_end' symbol marks the end of the application .dtors section */

    void*     heap_address;   /* The address of the heap - do not initialise the heap if NULL */
    uint32_t  heap_size;      /* This field specifies the heap size in Bytes */
    uint32_t  stack_size;     /* This field specifies the stack size required in Bytes - not used by the CRT v1.00 */
} _ExecutionContext_t;


#endif //_THE_EXEC_CONTEXT_H_
