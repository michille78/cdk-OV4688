#include <stdio.h>
#include <stdlib.h>
#include <svuCommonShave.h>
#include <sys/shave_system.h>
#include "swcWhoAmI.h"
#include <assert.h>

/* The heap must be set before anything else happens on the Shave. */
static void initHeap(_ExecutionContext_t* Context)
{
    /*Only reinitialize heap if it is required to do so*/
    if (Context->heap_address!=(void*)0x0){
        /*Don't really want printfs normally but it helps to keep this here*/
        if (0)
        {
            scMutexRequest(4);
            printf("I'm initializing shave heap memory now at 0x%p, size %d bytes\n", Context->heap_address, Context->heap_size);
            scMutexRelease(4);
        }
        __setheap((void*)Context->heap_address, Context->heap_size);
    }
}


/* Overload CRT0init functions and arrays.
   These are not used for the moment, but required to be able to links against ANSI compliant libraries. */
int __attribute__((noinline)) main(void)
{
    return 0;
}

extern unsigned int __init_array_start[];
extern unsigned int __init_array_end[];

void __attribute__((noinline)) masterEntry(_ExecutionContext_t *Context)
{
    initHeap(Context);

    typedef void (*pfunc)();

    pfunc *p;

    // Call global classes constructors defined in common area
    pfunc *CTORS_BEGIN  = (pfunc*)__init_array_start;
    pfunc *CTORS_END    = (pfunc*)__init_array_end;

    for (p = CTORS_BEGIN; p < CTORS_END; p++)
        (*p)();

    // Call module's global classes constructors
    CTORS_BEGIN = (pfunc*)Context->ctors_start;
    CTORS_END   = (pfunc*)Context->ctors_end;

    for (p = CTORS_BEGIN; p < CTORS_END; p++)
        (*p)();
}

extern unsigned int __fini_array_start[];
extern unsigned int __fini_array_end[];

void __attribute__((noinline)) masterExit(_ExecutionContext_t *Context)
{
    typedef void (*pfunc)();

    pfunc *p;

    // Call global classes destructors defined in common area
    pfunc *DTORS_BEGIN  = (pfunc*)__fini_array_start;
    pfunc *DTORS_END    = (pfunc*)__fini_array_end;

    for(p = DTORS_BEGIN; p < DTORS_END; p++)
        (*p)();

    // Call module's global classes constructors
    DTORS_BEGIN = (pfunc*)Context->dtors_start;
    DTORS_END   = (pfunc*)Context->dtors_end;

    for(p = DTORS_BEGIN; p < DTORS_END; p++)
        (*p)();

}

