/*
 * Add logging capabilities over simple printf.
 * Allows 5 different logging levels:
 *
 * MVLOG_DEBUG = 0
 * MVLOG_INFO = 1
 * MVLOG_WARN = 2
 * MVLOG_ERROR = 3
 * MVLOG_FATAL = 4
 * Before including header, a unit name can be set, otherwise defaults to global. eg:
 *
 * #define MVLOG_UNIT_NAME unitname
 * #include <mvLog.h>
 * Setting log level through debugger can be done in the following way:
 * mset mvLogLevel_unitname 2
 * Will set log level to warnings and above
 */
#ifndef MVLOG_H__
#define MVLOG_H__

#include <stdio.h>

#ifndef MVLOG_UNIT_NAME
#define MVLOG_UNIT_NAME global
#endif

#define _MVLOGLEVEL(UNIT_NAME)  mvLogLevel_ ## UNIT_NAME
#define  MVLOGLEVEL(UNIT_NAME) _MVLOGLEVEL(UNIT_NAME)

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_WHITE   "\x1b[37m"
#define ANSI_COLOR_RESET   "\x1b[0m"

#ifndef MVLOG_DEBUG_COLOR
#define MVLOG_DEBUG_COLOR ANSI_COLOR_WHITE
#endif

#ifndef MVLOG_INFO_COLOR
#define MVLOG_INFO_COLOR ANSI_COLOR_CYAN
#endif

#ifndef MVLOG_WARN_COLOR
#define MVLOG_WARN_COLOR ANSI_COLOR_YELLOW
#endif

#ifndef MVLOG_ERROR_COLOR
#define MVLOG_ERROR_COLOR ANSI_COLOR_MAGENTA
#endif

#ifndef MVLOG_FATAL_COLOR
#define MVLOG_FATAL_COLOR ANSI_COLOR_RED
#endif

typedef enum mvLog_t{
    MVLOG_DEBUG = 0,
    MVLOG_INFO,
    MVLOG_WARN,
    MVLOG_ERROR,
    MVLOG_FATAL,
    MVLOG_LAST,
} mvLog_t;

unsigned int __attribute__ ((weak)) MVLOGLEVEL(MVLOG_UNIT_NAME) = MVLOG_INFO;

unsigned int __attribute__ ((weak)) MVLOGLEVEL(default) = MVLOG_INFO;

#define cprintf(color, format, ...) \
    printf( color format ANSI_COLOR_RESET "\n", ##__VA_ARGS__)

#define flprintf(format, ...)                             \
    printf( "%s:%d " format, __func__, __LINE__, ##__VA_ARGS__)

#define cflprintf(color, format, ...)                            \
    cprintf(color, "%s:%d\t" format, __func__, __LINE__, ##__VA_ARGS__)

#define mvLog(lvl, format, ...)                                         \
    if(lvl >= MVLOGLEVEL(MVLOG_UNIT_NAME) ||                            \
       lvl >= MVLOGLEVEL(default)){                                     \
        if(lvl == MVLOG_DEBUG)                                          \
            cflprintf(MVLOG_DEBUG_COLOR "D:\t", format, ##__VA_ARGS__); \
        else if(lvl == MVLOG_INFO)                                      \
            cflprintf(MVLOG_INFO_COLOR  "I:\t", format, ##__VA_ARGS__); \
        else if(lvl == MVLOG_WARN)                                      \
            cflprintf(MVLOG_WARN_COLOR  "W:\t", format, ##__VA_ARGS__); \
        else if(lvl == MVLOG_ERROR)                                     \
            cflprintf(MVLOG_ERROR_COLOR "E:\t", format, ##__VA_ARGS__); \
        else if(lvl == MVLOG_FATAL)                                     \
            cflprintf(MVLOG_FATAL_COLOR "F:\t", format, ##__VA_ARGS__); \
    }

// Set log level for the current unit. Note that the level must be smaller than the global default
#define mvLogLevelSet(lvl) if(lvl < MVLOG_LAST){ MVLOGLEVEL(MVLOG_UNIT_NAME) = lvl; }
// Set the global log level. Can be used to prevent modules from hiding messages (enable all of them with a single change)
// This should be an application setting, not a per module one
#define mvLogDefaultLevelSet(lvl) if(lvl < MVLOG_LAST){ MVLOGLEVEL(default) = lvl; }

#endif
