#ifndef ADDRESS_MAPPINGS_MA2X5X_H
#define ADDRESS_MAPPINGS_MA2X5X_H

// Generated file ../../drivers/myriad2/socDrivers/leon/bm/arch/ma2x5x/include/DrvDdrAddrMapMa2x5x.h having controller version 2.40a using generate_address_mappings.py

#ifdef __cplusplus
extern "C" {
#endif



// Address mapping for spec "R12..R0 , B2..B0 , C8..C0" with 32-bit die
//------------------------------------------------------------------

// INFO: C1 matches HIF address bit 0
// INFO: C0 matches byte address bit 2

static const int address_map_R12_R0__B2_B0__C8_C0_32_bit_die[12] = {
    0x001f1f1f,
    0x00060606,
    0x00000000,
    0x0f0f0000,
    0x00000f0f,
    0x05050505,
    0x0f0f0f05,
    0x00000f0f,
    0x00001f00,
    0x00000000,
    0x00000000,
    0x00000000,
};


// Address mapping for spec "B2..B0 , R12..R0 , C8..C0" with 32-bit die
//------------------------------------------------------------------

// INFO: C1 matches HIF address bit 0
// INFO: C0 matches byte address bit 2

static const int address_map_B2_B0__R12_R0__C8_C0_32_bit_die[12] = {
    0x001f1f1f,
    0x00131313,
    0x00000000,
    0x0f0f0000,
    0x00000f0f,
    0x02020202,
    0x0f0f0f02,
    0x00000f0f,
    0x00001f00,
    0x00000000,
    0x00000000,
    0x00000000,
};


// Address mapping for spec "B2..B1 , R12..R0 , B0 , C8..C0" with 32-bit die
//------------------------------------------------------------------

// INFO: C1 matches HIF address bit 0
// INFO: C0 matches byte address bit 2

static const int address_map_B2_B1__R12_R0__B0__C8_C0_32_bit_die[12] = {
    0x001f1f1f,
    0x00131306,
    0x00000000,
    0x0f0f0000,
    0x00000f0f,
    0x03030303,
    0x0f0f0f03,
    0x00000f0f,
    0x00001f00,
    0x00000000,
    0x00000000,
    0x00000000,
};


// Address mapping for spec "B2 , R12..R0 , B1..B0 , C8..C0" with 32-bit die
//------------------------------------------------------------------

// INFO: C1 matches HIF address bit 0
// INFO: C0 matches byte address bit 2

static const int address_map_B2__R12_R0__B1_B0__C8_C0_32_bit_die[12] = {
    0x001f1f1f,
    0x00130606,
    0x00000000,
    0x0f0f0000,
    0x00000f0f,
    0x04040404,
    0x0f0f0f04,
    0x00000f0f,
    0x00001f00,
    0x00000000,
    0x00000000,
    0x00000000,
};


// Address mapping for spec "R13..R0 , B2..B0 , C9..C0" with 32-bit die
//------------------------------------------------------------------

// INFO: C1 matches HIF address bit 0
// INFO: C0 matches byte address bit 2

static const int address_map_R13_R0__B2_B0__C9_C0_32_bit_die[12] = {
    0x001f1f1f,
    0x00070707,
    0x00000000,
    0x0f000000,
    0x00000f0f,
    0x06060606,
    0x0f0f0606,
    0x00000f0f,
    0x00001f00,
    0x00000000,
    0x00000000,
    0x00000000,
};


// Address mapping for spec "B2..B0 , R13..R0 , C9..C0" with 32-bit die
//------------------------------------------------------------------

// INFO: C1 matches HIF address bit 0
// INFO: C0 matches byte address bit 2

static const int address_map_B2_B0__R13_R0__C9_C0_32_bit_die[12] = {
    0x001f1f1f,
    0x00151515,
    0x00000000,
    0x0f000000,
    0x00000f0f,
    0x03030303,
    0x0f0f0303,
    0x00000f0f,
    0x00001f00,
    0x00000000,
    0x00000000,
    0x00000000,
};


// Address mapping for spec "B2..B1 , R13..R0 , B0 , C9..C0" with 32-bit die
//------------------------------------------------------------------

// INFO: C1 matches HIF address bit 0
// INFO: C0 matches byte address bit 2

static const int address_map_B2_B1__R13_R0__B0__C9_C0_32_bit_die[12] = {
    0x001f1f1f,
    0x00151507,
    0x00000000,
    0x0f000000,
    0x00000f0f,
    0x04040404,
    0x0f0f0404,
    0x00000f0f,
    0x00001f00,
    0x00000000,
    0x00000000,
    0x00000000,
};


// Address mapping for spec "B2 , R13..R0 , B1..B0 , C9..C0" with 32-bit die
//------------------------------------------------------------------

// INFO: C1 matches HIF address bit 0
// INFO: C0 matches byte address bit 2

static const int address_map_B2__R13_R0__B1_B0__C9_C0_32_bit_die[12] = {
    0x001f1f1f,
    0x00150707,
    0x00000000,
    0x0f000000,
    0x00000f0f,
    0x05050505,
    0x0f0f0505,
    0x00000f0f,
    0x00001f00,
    0x00000000,
    0x00000000,
    0x00000000,
};

#ifdef __cplusplus
}
#endif

#endif
