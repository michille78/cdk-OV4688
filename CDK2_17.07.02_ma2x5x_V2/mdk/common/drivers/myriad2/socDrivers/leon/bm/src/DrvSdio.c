///   
/// @file                             
/// @copyright All code copyright Movidius Ltd 2012, all rights reserved. 
///            For License Warranty see: common/license.txt   
///
/// @brief     Driver to control Clocks, Power and Reset
/// 
/// 
/// 

//#define DRVSDIO_DEBUG

#ifdef DRVSDIO_DEBUG
#define DPRINTF1(...) printf(__VA_ARGS__)
#else
#define DPRINTF1(...)
#endif

//#define DRVSDIO_DPRINTF1(...) printf(__VA_ARGS__)
#define DRVSDIO_DPRINTF1(...)
//#define DRVSDIO_DPRINTF2(...) printf(__VA_ARGS__)
#define DRVSDIO_DPRINTF2(...)
//#define DRVSDIO_RW_DPRINTF1(...) printf(__VA_ARGS__)
#define DRVSDIO_RW_DPRINTF1(...)

// 1: Includes
// ----------------------------------------------------------------------------
#include <mv_types.h>
#include <registersMyriad.h>
#include <DrvRegUtils.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "DrvSdio.h"
#include "DrvSdioDefines.h"
#include "assert.h"
#include "swcLeonUtils.h"
#include "DrvIcb.h"
#include "DrvIcbDefines.h"
#include "DrvCommon.h"
#include <assert.h>
#include <DrvTimer.h>
#include <DrvLeonL2C.h>
#include <DrvGpio.h>

// 2:  Source Specific #defines and types  (typedef,enum,struct)
// ----------------------------------------------------------------------------

#define SDIO_BUS_WIDTH 4 // 4 or 8. GPIOs should be configured accordingly

#if 1
#define SDIO_CMD1_ARG  0x40ff8000  // high voltage 3.3V
#else
#define SDIO_CMD1_ARG  0x40000080  // low  voltage 1.8V
#endif

#define CARD_TRANSFER_SPEED_KHZ   50000  // card transfer speed

/* FIFO Watermarks  */ 
#define DRVSDIO_RX_WMARK                        (DRVSDIO_FIFO_DEPTH/2 - 1)
#define DRVSDIO_TX_WMARK                        (DRVSDIO_FIFO_DEPTH/2)
/* DMA burst size 64 */
#define DRVSDIO_DMASIZE                         5
/* Only for Dual Buffer, distance between two unchained descriptors in bus-width units */
#define DRVSDIO_DMASKIP                         0
/* Control for amount of phase shift on cclk_in_drv clock */
#define DRVSDIO_DEFAULT_CLK_DRV_PHASE_CTRL      0x02
/* Control for amount of phase shift on cclk_in_sample clock */ 
#define DRVSDIO_DEFAULT_CLK_SMPL_PHASE_CTRL     0x00

// TIMEOUTS TBD
#define DRVSDIO_WAITCOMMANDDONETIMEOUTUNITS     10
// Default Block Size
#define DRVSDIO_DEFAULTBLOCKSIZE                512
// Increase descriptor buffer size
#define DRVSDIO_INCREASEBLKSIZE                 8

// Masks
#ifndef DRV_DDRMASK
    #define DRV_DDRMASK                         0x80000000
#endif
#ifndef DRV_DDRBYPASSMASK
    #define DRV_DDRBYPASSMASK                   0x40000000
#endif

#ifndef DRV_CMXBYPASSMASK
    #define DRV_CMXBYPASSMASK                   0x08000000
#endif


#define MMC_CODE
#define USE_DDR
//#define TUNING_ENABLED

#define EMMC_POWER_CLASS 10
volatile u32 DrvSdio_RInterrupts = 0 ;

#define TUNING_NUM_BLOCKS 1
#define TUNING_BUFFER_SIZE (TUNING_NUM_BLOCKS * DRVSDIO_DEFAULTBLOCKSIZE)

u8 tuningTempBuff[TUNING_BUFFER_SIZE];
u8 tuningWriteBlock [TUNING_BUFFER_SIZE];
u8 tuningReadBlock [TUNING_BUFFER_SIZE];

// 3: Global Data (Only if absolutely necessary)
// ----------------------------------------------------------------------------
// Structure to control cards 
static tyDrvSdioCardConfig drvsdioCards[DRVSDIO_MAXDRIVERCARDS];

// 4: Static Local Data 
// ----------------------------------------------------------------------------
// Driver Status
static struct {
    u8 status;
    u8 reserved_1;
    u16 reserved_2;
    u32 cclk_in_khz;
}tyDrvSdio_Driver;

// 5: Static Function Prototypes
// ----------------------------------------------------------------------------
#ifndef MMC_CODE // hush the compiler warning.
static void drvSdioSwitchToHighSpeedMode(u32 cardslot, u32 *errorflags);
#endif
// 6: Functions Implementation
// ----------------------------------------------------------------------------

/// Sleeps for a number of ticks.
///
/// @param[in] ticks Number of clock ticks to pass
/// @return none
static inline void drvSdiodelay(u64 ticks)
{
    volatile u32 i;
    for (i = 0; i < (ticks >> 2); i ++);            
}

/// Allows to wait for any flag no interrupts
///
/// This function waits till the card returns the specified flags 
///
/// @param[in] flags These are the flags that the card should return.
/// @return resp The value of the RINTSTS (Raw Interrupt Status Register)
u32 __attribute__((weak)) DrvSdioWaitForFlags(u32 flag)
{
    u32 resp = 0;
    
    // This macros gets the value of the RINTSTS register
    resp = DRVSDIO_GETFLAGS;
    // Checck if the flags provided as input matches the flags set by the card in RINTSTS
    while (!(resp & flag)) {
        resp |= DRVSDIO_GETFLAGS;
    }
    // Clear Flags, only those which were expected
    // The flags are cleared whene you write a 1 to the flag
    DRVSDIO_CLEARFLAGS(flag);
    // Return the value of the RINTSTS register
    return resp;
}

/// This function sends a command to the card if the card is not busy
///
/// The command is not send until the card is busy. After the command
/// is sent to the CIU the function waits for the start_cmd bit to be
/// lowered. When the CIU send the command to the card it lowers the 
/// start_cmd bit. Sending the command to card does not mean it is DONE!
/// Command DONE is indicated by the card in the RINTSTS and it is not
/// handled here.
///
/// @param[in] command The command that need to be placed in the CMD register
/// @param[in] checkbusy Indicates whether to wait for the card or not
/// @param[in] card This is the card number
/// @return rints The value of the RINTSTS (Raw Interrupt Status Register)
static u32 drvSdioLoadCommand(u32 command, u32 checkbusy, u32 card)
{
    u32 read_data = 0, rints = 0, load_data = 0;
    
    DPRINTF1("\ndrvSdioLoadCommand Command 0x%x CardSlot %d\n", command, card);
    // If we need to wait until the card is busy enter the Wait routine
    if(checkbusy > 0) {
        // Wait routine that check for the busy flag in the status register
        read_data = GET_REG_WORD_VAL(SDIOH1_STATUS_S1);
        while(read_data & (1 << DRVSDIO_STATUS_DATABUSY))  
            read_data = GET_REG_WORD_VAL(SDIOH1_STATUS_S1);
    }           
    // Prepare the command that will be send
    // Take into account card number and the use of hold register 
    load_data = (command & (~((1 << DRVSDIO_CMD_USE_HOLDREGISTER) | (DRVSDIO_CMD_CARD_NUMBER_MASK << DRVSDIO_CMD_CARD_NUMBER))));
    if (drvsdioCards[card].useHoldRegister)
        load_data |= (1 << DRVSDIO_CMD_USE_HOLDREGISTER);
    load_data |= ((card & DRVSDIO_CMD_CARD_NUMBER_MASK) << DRVSDIO_CMD_CARD_NUMBER);
    DPRINTF1("\nProgramming CMD reg with 0x%x\n", load_data);  
    // After the command is put into the CMD register it is executed
    SET_REG_WORD(SDIOH1_CMD_S1, load_data);
    // Wait until the start_cmd flag is lowered or an error flag is raised. 
    // Lowering the start_cmd bit in the CMD register indicates that the command was
    // sent to the card
    while((GET_REG_WORD_VAL(SDIOH1_CMD_S1) & (1 << DRVSDIO_CMD_START_CMD)) && (!rints)) {
        // Get the value of the RINTSTS register to check for error flags (bits)
        rints = (GET_REG_WORD_VAL(SDIOH1_RINTSTS_S1) & (1 << DRVSDIO_RAWINTSTATUS_HLE));
    }   
    DPRINTF1("\nDone Loading 0x%x\n", rints);
    // Returns whether HLE has ocurred or not
    return rints;
}

/// This function loads a command having an argument 
///
/// After command is loaded we wait for the command to 
/// complete (handling the the Command DONE flag in the RINTSTS).
///
/// @param[in] command The command that needs to be loaded
/// @param[in] arg The argument that should be used with this command
/// @param[in] checkbusy Indicates whether to wait for the card or not
/// @param[in] card This is the card number
/// @return flags The value of the RINTSTS (Raw Interrupt Status Register)
static u32 drvSdioLoadAndWait(u32 command, u32 arg, u32 checkbusy, u32 card)
{
    u32 flags = 0;
    // Place the argument into the argument register
    SET_REG_WORD(SDIOH1_CMDARG_S1, arg);
    // Load the command
    flags = drvSdioLoadCommand(command, checkbusy, card);
    // Not HLE error
    if (flags == 0) {
        // Waits the command to complete or the specified error flags to be raised
        flags |= DrvSdioWaitForFlags((1 << DRVSDIO_RAWINTSTATUS_CDONE) | DRVSDIO_COMMANDDONE_ERROR_FLAGS);
    }
    DPRINTF1("\ndrvSdioLoadAndWait Command 0x%x Arg 0x%x Check Busy %d Card ErrorFlags 0x%x\n", command, arg, checkbusy, card, flags);
    return flags;
}

/// This function loads a command with an argument 
///
/// It returns the response from the card (the value of RESPONSE register - 0)
///
/// @param[in] command The command that needs to be loaded
/// @param[in] arg The argument that should be used with this command
/// @param[in] checkbusy Indicates whether to wait for the card or not
/// @param[in] card This is the card number
/// @param[in] errorflags The value of the RINTSTS (Raw Interrupt Status Register)
/// @return resp The value of RESPONSE register 0
static u32 drvSdioLoadAndWait_ShortResponse(u32 command, u32 arg, u32 checkbusy, u32 card, u32 *errorflags)
{
    u32 resp = 0, flags = 0;
    // Load Arg 
    SET_REG_WORD(SDIOH1_CMDARG_S1, arg);
    // Load Command 
    flags = drvSdioLoadCommand(command, checkbusy, card);
    // Not HLE error
    if (flags == 0) {
        // Waits the command to complete or the specified error flags to be raised
        flags |= DrvSdioWaitForFlags((1 << DRVSDIO_RAWINTSTATUS_CDONE) | DRVSDIO_COMMANDDONE_ERROR_FLAGS);
        if ((flags & DRVSDIO_COMMANDDONE_ERROR_FLAGS) == 0) {
            // No errors, get short response
            resp = GET_REG_WORD_VAL(SDIOH1_RESP0_S1);
        }       
    }
    *errorflags = flags;
    DPRINTF1("\ndrvSdioLoadAndWait_ShortResponse Command 0x%x Arg 0x%x Check Busy %d Card %d ErrorFlags 0x%x Resp 0x%x\n", command, arg, checkbusy, card, flags, resp);
    return resp;
}

/// This function loads a command with an argument
///
/// It returns the response from the card (the value of ALL the RESPONSE registers)
///
/// @param[in] command The command that needs to be loaded
/// @param[in] arg The argument that should be used with this command
/// @param[in] checkbusy Indicates whether to wait for the card or not
/// @param[in] card This is the card number
/// @param[in] resp The values of RESPONSE registers
/// @return flags The value of the RINTSTS (Raw Interrupt Status Register)
static u32 drvSdioLoadAndWait_LongResponse(u32 command, u32 arg, u32 checkbusy, u32 card, u32 *resp)
{
    u32 flags = 0;

    // Load Arg 
    SET_REG_WORD(SDIOH1_CMDARG_S1, arg);
    // Load Command 
    flags = drvSdioLoadCommand(command, checkbusy, card);
    // Not HLE error
    if (flags == 0) {
        // Command done? Errors ?
        flags |= DrvSdioWaitForFlags((1 << DRVSDIO_RAWINTSTATUS_CDONE) | DRVSDIO_COMMANDDONE_ERROR_FLAGS);
        if (((flags & DRVSDIO_COMMANDDONE_ERROR_FLAGS) == 0) && (resp)) {
            // No errors, get Long response
            resp[3] = swcLeonSwapU32(GET_REG_WORD_VAL(SDIOH1_RESP0_S1));
            resp[2] = swcLeonSwapU32(GET_REG_WORD_VAL(SDIOH1_RESP1_S1));
            resp[1] = swcLeonSwapU32(GET_REG_WORD_VAL(SDIOH1_RESP2_S1));
            resp[0] = swcLeonSwapU32(GET_REG_WORD_VAL(SDIOH1_RESP3_S1));
        }       
    }   
    DPRINTF1("\ndrvSdioLoadAndWait_LongResponse Command 0x%x Arg 0x%x Check Busy %d Card ErrorFlags 0x%x Resp 0x%x\n", command, arg, checkbusy, card, flags);
    return flags;
}

/// This function allows to select a card.
///
/// Returns response to CMD7 and Flags contains the interrupts to be checked for errors CMD7
///
/// @param[in] relativecardaddress The relative card address
/// @param[in] cardslot This is the card number
/// @param[in] flags The value of the RINTSTS (Raw Interrupt Status Register)
/// @return resp0 The value of RESPONSE register 0
static u32 drvSdioCardSelect(u32 relativecardaddress, u32 cardslot, u32 *flags)
{
    u32 resp0 = 0, tmp = 0;
    DPRINTF1("\nSend CMD7 \n");
    // Prepare the command
    tmp = ((1 << DRVSDIO_CMD_START_CMD) | (1 << DRVSDIO_CMD_RESP_EXPECT) | (DRVSDIO_CMD7 << DRVSDIO_CMD_CMD_INDEX));
    // Send command and wait
    resp0 = drvSdioLoadAndWait_ShortResponse(tmp, (relativecardaddress << 16), TRUE, cardslot, flags);
    DPRINTF1("\ndrvSdioCardSelect RCA 0x%x Card Slot %d Flags 0x%x Resp 0x%x\n", relativecardaddress, cardslot, *flags, resp0);
    // Return response to CMD7
    return resp0;
}

/// This function allows to deselect a card
///
/// It returns the interrupts to be checked for errors CMD7
///
/// @param[in] cardslot This is the card number
/// @return flags The value of the RINTSTS (Raw Interrupt Status Register)
static u32 drvSdioCardDeSelect(u32 cardslot)
{
    // In any case wait till card is not busy. If not when erasing we may have problems. We could use polling though TBD
    u32 flags = drvSdioLoadAndWait(((1 << DRVSDIO_CMD_START_CMD) | (DRVSDIO_CMD7 << DRVSDIO_CMD_CMD_INDEX)), 1, TRUE, cardslot);
    DPRINTF1("\ndrvSdioCardDeSelect Flags 0x%x\n", flags);
    return flags;
}

/// This function width of bus only for SD/SDIO in bits ACMD6
///
/// @param[in] width The bus width
/// @param[in] relativecardaddress The relative card address
/// @param[in] cardslot This is the card number
/// @param[in] flags The value of the RINTSTS (Raw Interrupt Status Register)
/// @return resp0 The value of RESPONSE register 0
static u32 drvSdioSelectBusWidth(u32 width, u32 relativecardaddress, u32 cardslot, u32 *flags,u8 ddr)
{
    UNUSED(relativecardaddress); // hush the compiler warning.

    u32 resp = 0, tmp = 0, iflags = 0, command = 0, arg = 0, hostwidth = 0;
    u32 ddrOff = 0;
#ifdef MMC_CODE
     UNUSED(tmp); // hush the compiler warning.
     UNUSED(flags); // hush the compiler warning.
    /* Write BUS_WIDTH [183] in EXT_CSD = 1 (4BIT mode) */
    DRVSDIO_DPRINTF1("\nCMD6\n");
    // Prepare command
    command = ((1 << DRVSDIO_CMD_START_CMD) | (DRVSDIO_CMD6 << DRVSDIO_CMD_CMD_INDEX) | (1 << DRVSDIO_CMD_RESP_EXPECT) | (1 << DRVSDIO_CMD_CHECK_RESP_CRC));
    /* cmd set = 0 */

    arg  =  DRVSDIO_INTMASK_ACCESS;    /* access = 3 (write) */
    arg |=  DRVSDIO_INTMASK_INDEX; /* index = 183 */

    if(ddr)
        ddrOff = 4;

    if (width == 8)
    {
        arg |= (2 + ddrOff) << 8;    /* value = 2 for SDR, 6 for DDR */
        hostwidth = 1 << 16;  // first bit on the upper half of the register if width = 8;
    }
    else if (width == 4)
    {
        arg |=  (1 + ddrOff) << 8;    /* value = 1 */
        hostwidth = 1;
    }
    else
    {
        if(ddr)
            return 0xFFFFFFFF; //DDR is invalid for bus width of '1'
        arg |=  DRVSDIO_INTMASK_VALUE0;    /* value = 0 */

    }
    hostwidth <<= cardslot;
    SET_REG_WORD(SDIOH1_CTYPE_S1,hostwidth); // need to set the width on the host side


    // Load command
    resp = drvSdioLoadAndWait_ShortResponse(command, arg, TRUE, cardslot, &iflags);
    drvSdiodelay(20*SYSTEMFREQ_CONFIG);
    DRVSDIO_DPRINTF1("\nCMD6 (Write HS timing to EXT_CSD) flags=%x arg=%x resp=%x done_error=%x\n", iflags, arg, resp, DRVSDIO_ISTHERE_COMMAND_DONE_ERROR(iflags));
    if ((DRVSDIO_ISTHERE_COMMAND_DONE_ERROR(iflags) == 0))
    {
        DRVSDIO_DPRINTF1("\nCMD6 success arg =%x resp=%x done_error=%x\n",  arg, resp);
        do
        {
            // SEND_STATUS
            command = ((1 << DRVSDIO_CMD_START_CMD) | (DRVSDIO_CMD13 << DRVSDIO_CMD_CMD_INDEX) | (1 << DRVSDIO_CMD_RESP_EXPECT) | (1 << DRVSDIO_CMD_CHECK_RESP_CRC));
            arg = relativecardaddress << DRVSDIO_R6_RCA;
            resp = drvSdioLoadAndWait_ShortResponse(command, arg, TRUE, cardslot, &iflags);
            DRVSDIO_DPRINTF1("\nCMD13 (get status) flags=%x arg=%x resp=%x done_error=%x\n", iflags, arg, resp, DRVSDIO_ISTHERE_COMMAND_DONE_ERROR(iflags));
            if ((DRVSDIO_ISTHERE_COMMAND_DONE_ERROR(iflags) == 0))
            {
                DRVSDIO_DPRINTF1("\nCMD13 success arg =%x resp=%x done_error=%x\n",  arg, resp);
            }
        } while (DRVSDIO_TMOUT_EMMC1 != (resp & DRVSDIO_TMOUT_EMMC2));

        if(ddr == 1)
        {
            if( (resp & 0x80))
            {
                printf("SWTICH error at changing to DDR \n");
                printf("check if card support exists\n");
            }
            else
            {
//                SET_REG_BITS_MASK(SDIOH1_UHS_S1,1 << (cardslot + DRVSDIO_UHS_REG_DDR_REG));
//                SET_REG_BITS_MASK(SDIOH1_EMMC_DDR_REG_S1, 1 << (cardslot));
                SET_REG_WORD(SDIOH1_UHS_S1,(1 << (cardslot + 16)) | (1 << cardslot));
                SET_REG_WORD(SDIOH1_EMMC_DDR_REG_S1, 1 << (cardslot));
//                SET_REG_WORD(SDIOH1_ENABLE_SHIFT_S1, 0x0 << (cardslot));
//                drvsdioCards[cardslot].useHoldRegister;
                printf("CHANGED TO DDR\n");
            }
        }
        resp = 0;
    }
    else
    {
        printf("command error\n");
    }

   #else
      UNUSED(arg); // hush the compiler warning.
      UNUSED(hostwidth); // hush the compiler warning.
      UNUSED(command); // hush the compiler warning.
    // Prepare CMD55
    // This command is executed prior CMD6 to force send ACMD6 and not CMD6
    tmp = ((1 << DRVSDIO_CMD_START_CMD) | (DRVSDIO_CMD55 << DRVSDIO_CMD_CMD_INDEX) | (1 << DRVSDIO_CMD_RESP_EXPECT) | (1 << DRVSDIO_CMD_CHECK_RESP_CRC));
    DPRINTF1("\nSend CMD55 \n");
    // Load Command 
    iflags = drvSdioLoadAndWait(tmp, (drvsdioCards[cardslot].relativeCardAddress << DRVSDIO_R6_RCA), TRUE, cardslot);
    if ((iflags & (DRVSDIO_COMMANDDONE_ERROR_FLAGS | (1 << DRVSDIO_RAWINTSTATUS_HLE))) == 0) {
        DPRINTF1("\nCMD6\n");
        // Prepare CMD6
        tmp = ((1 << DRVSDIO_CMD_START_CMD) | (DRVSDIO_ACMD6 << DRVSDIO_CMD_CMD_INDEX) | (1 << DRVSDIO_CMD_RESP_EXPECT) | (1 << DRVSDIO_CMD_CHECK_RESP_CRC));
        // Load Command 
        resp = drvSdioLoadAndWait_ShortResponse(tmp, width == 1? 0x00:0x2, TRUE, cardslot, &iflags);
    }
    *flags = iflags;
    DPRINTF1("\ndrvSdioSelectBusWidth Width %d RCA 0x%x ErrorFlags 0x%x Resp 0x%x\n", width, relativecardaddress, iflags, resp);
#endif
    return resp;
}

/// This function Set Block length by SDIO registers and CMD16 
///
/// @param[in] blocklength The block length
/// @param[in] bytecount The bytecount that will be used
/// @param[in] card This is the card number
/// @param[in] errorflags The value of the RINTSTS (Raw Interrupt Status Register)
/// @return resp0 The value of RESPONSE register 0
static u32 drvSdioSetBlockLength(u32 blocklength, u32 bytecount, u32 card, u32 *errorflags)
{
    u32 resp0 = 0;
    // Setting block length to register
    SET_REG_WORD(SDIOH1_BLKSIZ_S1, blocklength);
    // Setting byte count to register
    SET_REG_WORD(SDIOH1_BYTCNT_S1, bytecount);
    // Load Command 
//    resp0 = drvSdioLoadAndWait_ShortResponse(((1 << DRVSDIO_CMD_START_CMD) | (1 << DRVSDIO_CMD_RESP_EXPECT) | (DRVSDIO_CMD16 << DRVSDIO_CMD_CMD_INDEX)), blocklength, TRUE, card, errorflags);
    DPRINTF1("\ndrvSdioSetBlockLength BlockLength 0x%x Byte Count 0x%x Card %d ErrorFlags 0x%x Resp 0x%x\n", blocklength, bytecount, card, *errorflags, resp0);
    return resp0;
}

/// This function allows to reset FIFO 
///
/// @param[in] none
/// @return none
static void drvSdioResetFIFO(void)
{
    u32 controlreg = 0;
    DPRINTF1("\nPerforming FIFO Reset\n");
    // Get the value of the Contlor register
    controlreg = GET_REG_WORD_VAL(SDIOH1_CTRL_S1);
    // Set the fifo reset bit in the Control register
    controlreg |= (0x1 << DRVSDIO_CTRL_FIFO_RESET);
    // Set back the Control register
    SET_REG_WORD(SDIOH1_CTRL_S1, controlreg);
    // Wait for reset to be performed 
    while(GET_REG_WORD_VAL(SDIOH1_CTRL_S1) & (0x1 << DRVSDIO_CTRL_FIFO_RESET));
    DPRINTF1("\nFIFO Reset Done \n");
}

/// This function fills the descriptors
///
///
/// @param[in] count Number of descriptors with given blocksize
/// @param[in] descpointer The address used for descriptor generation
/// @param[in] blksize The size of a block that will be read/write
/// @param[in] addr The address where data will be stored in descriptors
/// @return count The number of descriptors
static u32 drvSdioFillDescriptors(u32 count, DrvSdio_Descriptors *descpointer, u32 blksize, u32 addr)
{
    u32 i = 0;
    DrvSdio_Descriptors *bdescpointer = NULL;
    
    // Write to bypassed addresses
    if ((u32) descpointer & DRV_DDRMASK)
        bdescpointer = (DrvSdio_Descriptors *) ((u32 )descpointer | DRV_DDRBYPASSMASK);
    else
        bdescpointer = (DrvSdio_Descriptors *) ((u32) descpointer | DRV_CMXBYPASSMASK);
        
    // Make sure buffer addresses are not bypassed
    if (addr & DRV_DDRMASK)
        addr &= ~DRV_DDRBYPASSMASK;
    else
        addr &= ~DRV_CMXBYPASSMASK;
        
    for (i = 0; i < count ; i ++) {
        bdescpointer[i].descriptor0 = ((1 << DRVSDIO_DES0_OWN) | (1 << DRVSDIO_DES0_CH));
        bdescpointer[i].descriptor1 = (blksize << DRVSDIO_DES1_BS1);
        bdescpointer[i].descriptor2 = addr + i * blksize;
        bdescpointer[i].descriptor3 = (u32) &descpointer[i + 1];
        DPRINTF1("\nDes %d 0: 0x%x 1: 0x%x 2: 0x%x 3: 0x%x\n", i, descpointer[i].descriptor0, descpointer[i].descriptor1, descpointer[i].descriptor2, descpointer[i].descriptor3);
    }
    DPRINTF1("\nDescriptors %d Start Addr 0x%x Transfer Size %d\n", i, addr, blksize);
    return count;
} 

/// This function marks the start and end of the descriptor chain
///
///
/// @param[in] descpointer Pointer to descriptors in memory
/// @param[in] descriptors number of descriptors 
static void drvSdioCompleteChainedDescriptor(DrvSdio_Descriptors *descpointer, u32 descriptors)
{
    DrvSdio_Descriptors *bdescpointer = NULL;
    // Write to bypassed addresses
    if ((u32) descpointer & DRV_DDRMASK)
        bdescpointer = (DrvSdio_Descriptors *) ((u32 )descpointer | DRV_DDRBYPASSMASK);
    else
        bdescpointer = (DrvSdio_Descriptors *) ((u32) descpointer | DRV_CMXBYPASSMASK);
    
    if (descriptors) {
        bdescpointer[0].descriptor0 |= (1 << DRVSDIO_DES0_FS);
        bdescpointer[descriptors - 1].descriptor0 |= (1 << DRVSDIO_DES0_LD);
        bdescpointer[descriptors - 1].descriptor3 = (u32) &descpointer[0];
    }
}


/// This function allows to build chained descriptors
///
/// Sizes must be integer multiple of blksize
///
/// @param[in] descriptorbaseaddress The address used for descriptor generation
/// @param[in] transactionList List of buffers and sizes to be used consecutively
/// @param[in] transCount Length of the transaction list
/// @param[in] blksize The size of a block that will be read/write
/// @return total The number of descriptors
static u32 drvSdioBuildChainedDescriptor(u32 descriptorbaseaddress, tyDrvSdio_Transaction *transactionList, u32 transCount, u32 blksize)
{
    UNUSED(blksize); // hush the compiler warning.

    DrvSdio_Descriptors *descpointer = NULL;
    u32 count = 0, dmatransize = DRVSDIO_MAXDMATRANSACTIONSIZE, total = 0, j = 0, size = 0, addr = 0;
            
    descpointer = (DrvSdio_Descriptors *) descriptorbaseaddress;
    if (transCount == 1) {
         while ((transactionList[0].size <= dmatransize) && (transactionList[0].size > DRVSDIO_MINDMATRANSACTIONSIZE))
            dmatransize = (dmatransize >> 1);
    }   

    for (j = 0; j < transCount; j ++) {
        // Must be 32-bit aligned 
        transactionList[j].buffer &= 0xFFFFFFFC;
        // Must be multiple of 512
        transactionList[j].size &= 0xFFFFFF00;
        size = transactionList[j].size;
        addr = transactionList[j].buffer;
        do {
            count = drvSdioFillDescriptors(size / dmatransize, (DrvSdio_Descriptors *)&descpointer[total], dmatransize, addr);
            DPRINTF1("\ncount %d size %d dmatransize %d\n", size / dmatransize, size, dmatransize);         
            total += count;
            size -= count * dmatransize;
            addr += count * dmatransize;
            dmatransize = (dmatransize >> 1);
        } while (dmatransize >= DRVSDIO_MINDMATRANSACTIONSIZE);
        dmatransize = DRVSDIO_MAXDMATRANSACTIONSIZE;
    }
    // Handle first and last descriptors
    drvSdioCompleteChainedDescriptor(descpointer, total);    
    DPRINTF1("\ndrvSdioBuildChainedDescriptor Desc Addr 0x%x List Length %d Descriptors %d\n", descpointer, transCount, total);
    return total;
} 


/// This function allows to work out the value to be used in div x for a desired frequency
///
/// @param[in] cclk_in_khz System clock in KHz
/// @param[in] sdfreq_khz Desired card clock frequency in KHz
/// @return res Desired value for the clock frequency devider
static u32 drvSdioCalculateDivValue(u32 cclk_in_khz, u32 sdfreq_khz)
{
    u32 res = 0;

    // Just bypass the divider if input clock is slower
    if(cclk_in_khz > sdfreq_khz)
    {
        // Calculate the divider
        sdfreq_khz = (sdfreq_khz << 1);
        res = (cclk_in_khz/ sdfreq_khz);
        if ((cclk_in_khz % sdfreq_khz) > (u32) ((1 << res) * DRVSDIO_MAXDEVIATIONKHZ))
            res++;
    }
    return res;
}


/// This function allows to change the frequency of operation in any of the 4 possible dividers
///
/// @param[in] cardslot This is the card number
/// @param[in] cclk_in_khz System clock in KHz
/// @param[in] sdfreq_khz Desired card clock frequency in KHz
/// @return none
static u32 drvSdioChangeFrequency(u32 cardslot, u32 div, u32 cclk_in_khz, u32 sdfreq_khz)
{
    u32 tmp = 0;
    u32 res = 0;
    // Work out the value for enumeration process
    // drvSdioCalculateDivValue expects sdfrequency in Hz that's why we are converting
    DPRINTF1("cclk_in_khz %d sdfreq_khz %d\n", cclk_in_khz, sdfreq_khz);

    printf("cclk_in_khz %d sdfreq_khz %d\n", cclk_in_khz, sdfreq_khz);

    tmp = drvSdioCalculateDivValue(cclk_in_khz, sdfreq_khz);
    // Disable Clock for all Cards
    SET_REG_WORD(SDIOH1_CLKENA_S1, DRVSDIO_CLKENA_DISABLE_ALL);
    // Execute change by sending a command to updtate clock registers
    res = drvSdioLoadCommand((1 << DRVSDIO_CMD_START_CMD) | (1 << DRVSDIO_CMD_UPDATE_CLK_REG), TRUE, cardslot);  
    if (res & DRVSDIO_COMMANDDONE_ERROR_FLAGS)
    {
        // There is an error updating clocks
        DPRINTF1("\ndrvSdioChangeFrequency failed to update clocks\n");
        return res;
    }
    // Set new value into the rigth divider 
    SET_REG_WORD(SDIOH1_CLKDIV_S1, (tmp << (div << 3)));
    DPRINTF1("\nDIV %d: %d sdfreq_khz %d\n",div , tmp, sdfreq_khz);
    printf("\nDIV %d: %d sdfreq_khz %d\n",div , tmp, sdfreq_khz);

    // Cardslot is using divider x
    SET_REG_WORD(SDIOH1_CLKSRC_S1, (div << (cardslot << 1)));
    /* Enable clock for all 16 cards and Non-low-power-mode */
    SET_REG_WORD(SDIOH1_CLKENA_S1, DRVSDIO_CLKENA_ENABLE_ALL);
    // Execute change by sending a command to updtate clock registers
    res = drvSdioLoadCommand((1 << DRVSDIO_CMD_START_CMD) | (1 << DRVSDIO_CMD_UPDATE_CLK_REG), FALSE, cardslot);  
    if (res & DRVSDIO_COMMANDDONE_ERROR_FLAGS)
    {
        // There is an error updating clocks
        DPRINTF1("\ndrvSdioChangeFrequency failed to update clocks\n");
        return res;
    }
    DPRINTF1("\ndrvSdioChangeFrequency Card %d Div %d SD Freq (KHz) %d\n", cardslot, div, sdfreq_khz);
    drvsdioCards[cardslot].statusFlags |= (1 << DRVSDIO_INTERNALSD_STATUS_FREQ_SWITCHED);
    return 0;
}

#ifdef MMC_CODE
//static void print_csd()
//{
//    u32 i;
//    DRVSDIO_DPRINTF1("\nCSD:");
//    for (i = 0 ; i < 16; i++)
//    {
//       DRVSDIO_DPRINTF1("%02x:%02x ",i,pucCsd[i]);
//    }
//    DRVSDIO_DPRINTF1("\n");
//    DRVSDIO_DPRINTF1("DRVSDIO_CSD_GET_CSD_STRUCTURE = %x\n", DRVSDIO_CSD_GET_CSD_STRUCTURE(pucCsd));
//    DRVSDIO_DPRINTF1("DRVSDIO_CSD_GET_SPEC_VERS = %x\n",DRVSDIO_CSD_GET_SPEC_VERS(pucCsd));
//    DRVSDIO_DPRINTF1("DRVSDIO_CSD_GET_TAAC = %x\n",DRVSDIO_CSD_GET_TAAC(pucCsd));
//    DRVSDIO_DPRINTF1("DRVSDIO_CSD_GET_NSAC = %x\n",DRVSDIO_CSD_GET_NSAC(pucCsd));
//    DRVSDIO_DPRINTF1("DRVSDIO_CSD_GET_TRAN_SPEED = %x\n",DRVSDIO_CSD_GET_TRAN_SPEED(pucCsd));
//    DRVSDIO_DPRINTF1("DRVSDIO_CSD_GET_C_SIZE = %x\n",DRVSDIO_CSD_GET_C_SIZE(pucCsd));
//    DRVSDIO_DPRINTF1("DRVSDIO_CSD_GET_READ_BLK_LEN = %x\n",DRVSDIO_CSD_GET_READ_BLK_LEN(pucCsd));
//    DRVSDIO_DPRINTF1("DRVSDIO_CSD_GET_WRITE_BLK_LEN = %x\n",DRVSDIO_CSD_GET_WRITE_BLK_LEN(pucCsd));
//    DRVSDIO_DPRINTF1("DRVSDIO_CSD_1_GET_C_SIZE = %x\n",DRVSDIO_CSD_1_GET_C_SIZE(pucCsd));
//    DRVSDIO_DPRINTF1("\n");
//}

#ifdef __RTEMS__
#include <rtems.h>
#else
#include <DrvTimer.h>
#endif

#define EMMC_HS_TIMNG_REG 185
#define SWITCH_INDEX 16

DRV_RETURN_TYPE DrvSdioEnumerate(u32 cardslot, u32 *errorflags)
{
    u32  command = 0, arg = 0, resp = 0, flags = 0, cardTransferSpeed = 0,tmp = 0;
    u32 relativecardaddress = 1;
   UNUSED(tmp);

    // Just in case
    assert(cardslot < DRVSDIO_MAXDRIVERCARDS);
    if ((tyDrvSdio_Driver.status & (1 << DRVSDIO_INTERNALSD_STATUS_FREQ_SWITCHED)))
    {
        memset(&drvsdioCards[cardslot], 0x00, sizeof(tyDrvSdioCardConfig));
        // Use Hold register by default
        drvsdioCards[cardslot].useHoldRegister = TRUE;
        DRVSDIO_DPRINTF1("\nEnumerating Card in Slot %d \n", cardslot);
        // Clear any pending interrpts
        SET_REG_WORD(SDIOH1_RINTSTS_S1, DRVSDIO_RAWINTSTATUS_ALL);

        DRVSDIO_DPRINTF1("\nCMD0\n");
        // Prepare command (send 80 cycles, note DRVSDIO_CMD_SEND_INIT option)
        command = (1 << DRVSDIO_CMD_START_CMD) | (DRVSDIO_CMD0 << DRVSDIO_CMD_CMD_INDEX) | (1 << DRVSDIO_CMD_SEND_INIT);
        // No args for CMD0
        arg = 0;
        // Load command
        tmp = drvSdioLoadAndWait(command, arg, FALSE, cardslot);
        command = (1 << DRVSDIO_CMD_START_CMD) | (DRVSDIO_CMD0 << DRVSDIO_CMD_CMD_INDEX);
        // Go to pre-idle state
        arg = DRVSDIO_CARD_PRE_IDLE_STATE;
        tmp = drvSdioLoadAndWait(command, arg, FALSE, cardslot);
        // Go to idle state
        arg = 0;
        tmp = drvSdioLoadAndWait(command, arg, FALSE, cardslot);

        // Set the host voltage range
        DRVSDIO_DPRINTF1("\nCMD1\n");
        u32 i;

        for (i = 0; i < 20; i++)
        {
            command = (1 << DRVSDIO_CMD_START_CMD) | (1 << DRVSDIO_CMD_RESP_EXPECT) | (DRVSDIO_CMD1 << DRVSDIO_CMD_CMD_INDEX);
            arg = SDIO_CMD1_ARG;
            resp = drvSdioLoadAndWait_ShortResponse(command, arg, FALSE, cardslot, &flags);
            // If successfull CMD1 should ...
            DRVSDIO_DPRINTF1("\nCMD1[%d] flags=%x arg=%x resp=%x done_error=%x\n", i, flags, arg, DRVSDIO_ISTHERE_COMMAND_DONE_ERROR(flags));
            if (DRVSDIO_ISTHERE_COMMAND_DONE_ERROR(flags) == 0)
            {
                if (resp & 0x80000000)
                {
                    DRVSDIO_DPRINTF1("\nCMD1 success %s:%s:%d\n",  __FILE__, __FUNCTION__, __LINE__);
                    break;
                }
                else
                {
                    DRVSDIO_DPRINTF1("\nBusy %s:%s:%d\n", __FILE__, __FUNCTION__, __LINE__);
                }
            }
            else
            {
                DRVSDIO_DPRINTF1("\nNO response %s:%s:%d\n", __FILE__, __FUNCTION__, __LINE__);
                //break;
            }
#ifdef __RTEMS__
            rtems_task_wake_after(500);
#else
            DrvTimerSleepMs(500);
#endif
        }

        while ((drvsdioCards[cardslot].relativeCardAddress == 0))
        {
            DRVSDIO_DPRINTF1("\nCMD2\n");
            // Prepare command
            command = ((1 << DRVSDIO_CMD_START_CMD) | (DRVSDIO_CMD2 << DRVSDIO_CMD_CMD_INDEX) | (1 << DRVSDIO_CMD_CHECK_RESP_CRC) | (1 << DRVSDIO_CMD_RESP_LENGTH) | (1 << DRVSDIO_CMD_RESP_EXPECT));
            // No args for CMD2
            arg = 0;
            // Load command
            flags = drvSdioLoadAndWait_LongResponse(command, arg, TRUE, cardslot, drvsdioCards[cardslot].cid);
            // Check for errors
            if (DRVSDIO_ISTHERE_COMMAND_DONE_ERROR(flags) == 0)
            {
                DRVSDIO_DPRINTF1("%s:%d cid = %08x , %08x, %08x, %08x\n",
                         __FUNCTION__, __LINE__, drvsdioCards[cardslot].cid[0], drvsdioCards[cardslot].cid[1], drvsdioCards[cardslot].cid[2], drvsdioCards[cardslot].cid[3]);
                // We should do something with this information TBD
                if ((drvsdioCards[cardslot].relativeCardAddress == 0) && (DRVSDIO_ISTHERE_COMMAND_DONE_ERROR(flags) == 0))
                {
                    DRVSDIO_DPRINTF1("\nCMD3\n");
                    // Prepare command
                    command = ((1 << DRVSDIO_CMD_START_CMD) | (DRVSDIO_CMD3 << DRVSDIO_CMD_CMD_INDEX) | (1 << DRVSDIO_CMD_RESP_EXPECT) | (1 << DRVSDIO_CMD_CHECK_RESP_CRC));
                    // Select relative card address for CMD3
                    arg = relativecardaddress << DRVSDIO_R6_RCA;
                    // Load command
                    resp = drvSdioLoadAndWait_ShortResponse(command, arg, TRUE, cardslot, &flags);
                    DRVSDIO_DPRINTF1("\nCMD3 (Set RCA) flags=%x arg=%x resp=%x done_error=%x\n", flags, arg, resp, DRVSDIO_ISTHERE_COMMAND_DONE_ERROR(flags));
                    if ((DRVSDIO_ISTHERE_COMMAND_DONE_ERROR(flags) == 0))
                    {
                        DRVSDIO_DPRINTF1("\nCMD3 success arg =%x resp=%x done_error=%x\n",  arg, resp);
                        drvsdioCards[cardslot].relativeCardAddress = relativecardaddress;
                        DRVSDIO_DPRINTF1("\nCMD3 success arg =%x resp=%x RCA=%d\n",  arg, resp, relativecardaddress);
                        break;
                    }
                }
            }
#ifdef __RTEMS__
            rtems_task_wake_after(10);
#else
            DrvTimerSleepMs(10);
#endif
            relativecardaddress++;
        }
        DRVSDIO_DPRINTF1("\nCMD9, i=%d\n",i);
        // Prepare command
        command = ((1 << DRVSDIO_CMD_START_CMD) | (DRVSDIO_CMD9 << DRVSDIO_CMD_CMD_INDEX) | (1 << DRVSDIO_CMD_CHECK_RESP_CRC) | (1 << DRVSDIO_CMD_RESP_LENGTH) | (1 << DRVSDIO_CMD_RESP_EXPECT));
        // Relative address as arg
        arg = (drvsdioCards[cardslot].relativeCardAddress << DRVSDIO_R6_RCA);
        // Load command
        flags = drvSdioLoadAndWait_LongResponse(command, arg, FALSE, cardslot, drvsdioCards[cardslot].csd);
        if (DRVSDIO_ISTHERE_COMMAND_DONE_ERROR(flags) == 0)
        {
            DRVSDIO_DPRINTF1("%s:%d csd = %08x , %08x, %08x, %08x\n",
                     __FUNCTION__, __LINE__,
                     drvsdioCards[cardslot].csd[0], drvsdioCards[cardslot].csd[1], drvsdioCards[cardslot].csd[2], drvsdioCards[cardslot].csd[3]);

//            print_csd(&drvsdioCards[cardslot].csd[0]);

        }
        else
        {
            DRVSDIO_DPRINTF1("%s:%d CMD9 failed, flags = %08x\n", __FUNCTION__, __LINE__, flags);
        }


    if (DRVSDIO_CSD_GET_SPEC_VERS((u8 *)drvsdioCards[cardslot].csd) >= 4)
    {


        /* The host baud rate and bus width have to be changed before the following command*/

        resp = drvSdioCardSelect(relativecardaddress, cardslot, &flags);
        DRVSDIO_DPRINTF1("\n%s:%d resp = %x,flags = %x\n", __FILE__,__LINE__,resp, flags);


       /* Write HS_TIMING [185] in EXT_CSD = 1 */
        DRVSDIO_DPRINTF1("\nCMD6\n");
        // Prepare command
        command = ((1 << DRVSDIO_CMD_START_CMD) | (DRVSDIO_CMD6 << DRVSDIO_CMD_CMD_INDEX) | (1 << DRVSDIO_CMD_RESP_EXPECT) | (1 << DRVSDIO_CMD_CHECK_RESP_CRC));
        // Select relative card address for CMD3
        arg  =  DRVSDIO_INTMASK_ACCESS;    /* access = 3 */
        arg |=  (EMMC_HS_TIMNG_REG << SWITCH_INDEX); /* index = 183 */
//        arg |= DRVSDIO_INTMASK_INDEX;
        arg |=  DRVSDIO_INTMASK_VALUE1;    /* value = 1 */

        // Load command
        resp = drvSdioLoadAndWait_ShortResponse(command, arg, TRUE, cardslot, &flags);
//        printf("\nCMD6 (Write HS timing to EXT_CSD) flags=%x arg=%x resp=%x done_error=%x\n", flags, arg, resp, DRVSDIO_ISTHERE_COMMAND_DONE_ERROR(flags));
        DRVSDIO_DPRINTF1("\nCMD6 (Write HS timing to EXT_CSD) flags=%x arg=%x resp=%x done_error=%x\n", flags, arg, resp, DRVSDIO_ISTHERE_COMMAND_DONE_ERROR(flags));
        if ((DRVSDIO_ISTHERE_COMMAND_DONE_ERROR(flags) == 0))
        {
            DRVSDIO_DPRINTF1("\nCMD6 success arg =%x resp=%x done_error=%x\n",  arg, resp);
        }
       do
       {
           // SEND_STATUS
           command = ((1 << DRVSDIO_CMD_START_CMD) | (DRVSDIO_CMD13 << DRVSDIO_CMD_CMD_INDEX) | (1 << DRVSDIO_CMD_RESP_EXPECT) | (1 << DRVSDIO_CMD_CHECK_RESP_CRC));
           arg = relativecardaddress << DRVSDIO_R6_RCA;
           resp = drvSdioLoadAndWait_ShortResponse(command, arg, TRUE, cardslot, &flags);
           DRVSDIO_DPRINTF1("\nCMD13 (get status) flags=%x arg=%x resp=%x done_error=%x\n", flags, arg, resp, DRVSDIO_ISTHERE_COMMAND_DONE_ERROR(flags));

           if ((DRVSDIO_ISTHERE_COMMAND_DONE_ERROR(flags) == 0))
           {
               DRVSDIO_DPRINTF1("\nCMD13 success arg =%x resp=%x done_error=%x\n",  arg, resp);
           }
       } while (0x000000900 != (resp & 0x00001F00));

//       resp = drvSdioCardSelect(relativecardaddress, cardslot, &flags);
//        //Select Power Class
//        DRVSDIO_DPRINTF1("\nCMD6\n");
//        // Prepare command
//        command = ((1 << DRVSDIO_CMD_START_CMD) | (DRVSDIO_CMD6 << DRVSDIO_CMD_CMD_INDEX) | (1 << DRVSDIO_CMD_RESP_EXPECT) | (1 << DRVSDIO_CMD_CHECK_RESP_CRC));
//        // Select relative card address for CMD3
//        arg  =  DRVSDIO_INTMASK_ACCESS;    /* access = 3 */
//        arg |=  (187 << SWITCH_INDEX); /* index = 183 */
////        arg |= DRVSDIO_INTMASK_INDEX;
//        arg |=  (EMMC_POWER_CLASS << 8);    /* value = 1 */
//
//        // Load command
//        resp = drvSdioLoadAndWait_ShortResponse(command, arg, TRUE, cardslot, &flags);
//        printf("\nCMD6 (Write power class) flags=%x arg=%x resp=%x done_error=%x\n", flags, arg, resp, DRVSDIO_ISTHERE_COMMAND_DONE_ERROR(flags));
//        if ((DRVSDIO_ISTHERE_COMMAND_DONE_ERROR(flags) == 0))
//        {
//            DRVSDIO_DPRINTF1("\nCMD6 success arg =%x resp=%x done_error=%x\n",  arg, resp);
//        }
//       do
//       {
//           // SEND_STATUS
//           command = ((1 << DRVSDIO_CMD_START_CMD) | (DRVSDIO_CMD13 << DRVSDIO_CMD_CMD_INDEX) | (1 << DRVSDIO_CMD_RESP_EXPECT) | (1 << DRVSDIO_CMD_CHECK_RESP_CRC));
//           arg = relativecardaddress << DRVSDIO_R6_RCA;
//           resp = drvSdioLoadAndWait_ShortResponse(command, arg, TRUE, cardslot, &flags);
//           DRVSDIO_DPRINTF1("\nCMD13 (get status) flags=%x arg=%x resp=%x done_error=%x\n", flags, arg, resp, DRVSDIO_ISTHERE_COMMAND_DONE_ERROR(flags));
//           printf("\nCMD13 (get status) flags=%x arg=%x resp=%x done_error=%x\n", flags, arg, resp, DRVSDIO_ISTHERE_COMMAND_DONE_ERROR(flags));
//
//           if ((DRVSDIO_ISTHERE_COMMAND_DONE_ERROR(flags) == 0))
//           {
//               DRVSDIO_DPRINTF1("\nCMD13 success arg =%x resp=%x done_error=%x\n",  arg, resp);
//           }
//       } while (0x000000900 != (resp & 0x00001F00));


       cardTransferSpeed = 25000;//CARD_TRANSFER_SPEED_KHZ; //@@ KHz MLR was 26 MHz
       {
           tmp = drvSdioChangeFrequency(cardslot, 0, tyDrvSdio_Driver.cclk_in_khz, cardTransferSpeed);
           if (DRVSDIO_ISTHERE_COMMAND_DONE_ERROR(flags) != 0)
           {
               DRVSDIO_DPRINTF1("\nError changing frequency\n");
               *errorflags = flags;
               return DRVSDIO_ERROR;
           }
       }

       drvsdioCards[cardslot].busWidth = 8;
       drvsdioCards[cardslot].sdhc = 1;
       drvsdioCards[cardslot].blockSize = DRVSDIO_DEFAULTBLOCKSIZE;

//       resp = drvSdioSetBlockLength(drvsdioCards[cardslot].blockSize, drvSdioGetScatteredTransactionSize(transactionList,count), cardslot, &flags);

//       resp = drvSdioCardSelect(relativecardaddress, cardslot, &flags);
//       resp = drvSdioSelectBusWidth(drvsdioCards[cardslot].busWidth,
//               drvsdioCards[cardslot].relativeCardAddress, cardslot, &flags,0);
//       resp = drvSdioCardSelect(relativecardaddress, cardslot, &flags);

//       drvSdioCardDeSelect(cardslot);
#ifdef USE_DDR
       resp = drvSdioSelectBusWidth(drvsdioCards[cardslot].busWidth,
               drvsdioCards[cardslot].relativeCardAddress, cardslot, &flags,1);
#endif

       DRVSDIO_DPRINTF1("\n%s:%d resp=%x\n",  __FILE__,__LINE__,resp);
       }

        drvSdioCardDeSelect(cardslot);

#ifdef TUNING_ENABLED
        //########START TUNING HERE ###############
        //save the blocks we are going to overwrite
        tyDrvSdio_Transaction transactionList;
        int ret;

        transactionList.size = TUNING_BUFFER_SIZE;
        transactionList.buffer = tuningTempBuff;
        ret = DrvSdioReadDataBlockList(0,&transactionList,1,cardslot,&flags);
        if(ret != 0) printf("ERROR at fristRead %d\n",ret);
        if (DRVSDIO_ISTHERE_COMMAND_DONE_ERROR(flags))
            printf("flags 0x%x\n",flags);

        for(int i = 0; i < TUNING_BUFFER_SIZE; i++)
            tuningWriteBlock[i] = i;
#ifdef __RTEMS__
        rtems_cache_flush_multiple_data_lines(tuningWriteBlock,TUNING_BUFFER_SIZE);
#else
        DrvLL2CFlushOpOnAddrRange(LL2C_OPERATION_WRITE_BACK,0,tuningWriteBlock,
                TUNING_BUFFER_SIZE);
#endif
//        tmp = GET_REG_WORD_VAL(SDIOH1_UHS_REG_EXT_S1);
//        tmp |= ((DRVSDIO_DEFAULT_CLK_DRV_PHASE_CTRL << DRVSDIO_UHS_REG_EXT_CLK_DRV_PHASE_CTRL) | (DRVSDIO_DEFAULT_CLK_SMPL_PHASE_CTRL << DRVSDIO_UHS_REG_EXT_CLK_SMPL_PHASE_CTRL));
//        SET_REG_WORD(SDIOH1_UHS_REG_EXT_S1, tmp);

        u32 ok = 0;
        transactionList.buffer = tuningWriteBlock;
        ret = DrvSdioWriteDataBlockList(0,&transactionList,1,cardslot,&flags);
        if(ret != 0) printf("ERROR at fristRead %d\n",ret);
        if (DRVSDIO_ISTHERE_COMMAND_DONE_ERROR(flags))
            printf("flags 0x%x\n",flags);

        transactionList.buffer = tuningReadBlock;
        ret =DrvSdioReadDataBlockList(0,&transactionList,1,cardslot,&flags);
        if(ret != 0) printf("ERROR at fristRead %d\n",ret);
        if (DRVSDIO_ISTHERE_COMMAND_DONE_ERROR(flags) )
            printf("flags 0x%x\n",flags);

#ifdef __RTEMS__
        rtems_cache_invalidate_multiple_data_lines(tuningReadBlock,TUNING_BUFFER_SIZE);
#else
        DrvLL2CFlushOpOnAddrRange(LL2C_OPERATION_INVALIDATE,0,tuningReadBlock,
                TUNING_BUFFER_SIZE);
#endif
        for(int i = 0; i < TUNING_BUFFER_SIZE; i++)
            if(tuningReadBlock[i] != tuningWriteBlock[i])
                ok = 1;
        if(ok)
        {

            printf(" default config not working!!!!\n WILL START TUNING\n");

            u32 sample_phase = 0;
            u32 drive_phase = 0;

            ok = 0;
            for(int j = 0; j < 8; j++)
            {

                //set different PHASE shift in sample clock.
//                tmp = GET_REG_WORD_VAL(SDIOH1_UHS_REG_EXT_S1);
                sample_phase = j;

                for(int k = 0; k < 8; k++)
                {

                    drive_phase = k;
                    tmp = ((drive_phase << DRVSDIO_UHS_REG_EXT_CLK_DRV_PHASE_CTRL) |
                            (sample_phase << DRVSDIO_UHS_REG_EXT_CLK_SMPL_PHASE_CTRL));
                    SET_REG_WORD(SDIOH1_UHS_REG_EXT_S1, tmp);

                    transactionList.buffer = tuningWriteBlock;
                    ret = DrvSdioWriteDataBlockList(0,&transactionList,1,cardslot,&flags);
                    if(ret != 0) printf("ERROR at fristRead %d\n",ret);
                    if (DRVSDIO_ISTHERE_COMMAND_DONE_ERROR(flags))
                        printf("flags 0x%x\n",flags);

                    transactionList.buffer = tuningReadBlock;
                    ret =DrvSdioReadDataBlockList(0,&transactionList,1,cardslot,&flags);
                    if(ret != 0) printf("ERROR at fristRead %d\n",ret);
                    if (DRVSDIO_ISTHERE_COMMAND_DONE_ERROR(flags) )
                        printf("flags 0x%x\n",flags);

#ifdef __RTEMS__
                    rtems_cache_invalidate_multiple_data_lines(tuningReadBlock,TUNING_BUFFER_SIZE);
#else
        DrvLL2CFlushOpOnAddrRange(LL2C_OPERATION_INVALIDATE,0,tuningReadBlock,
                TUNING_BUFFER_SIZE);
#endif

                    for(int i = 0; i < TUNING_BUFFER_SIZE; i++)
                        if(tuningReadBlock[i] != tuningWriteBlock[i])
                            ok = 1;

                    if(ok == 0)
                        break;

                }
            }
            if(ok)
            {
                printf("could not tune correctly...\n");
                tmp = ((DRVSDIO_DEFAULT_CLK_DRV_PHASE_CTRL << DRVSDIO_UHS_REG_EXT_CLK_DRV_PHASE_CTRL) | (DRVSDIO_DEFAULT_CLK_SMPL_PHASE_CTRL << DRVSDIO_UHS_REG_EXT_CLK_SMPL_PHASE_CTRL));
                SET_REG_WORD(SDIOH1_UHS_REG_EXT_S1, tmp);
            }else
                printf("tuning ok\n tuned with sample_phase %d drive_phase %d\n",sample_phase,drive_phase);

        }
        else
        {
            printf("default values ar fine, no need for tuning\n");
        }

        transactionList.size = TUNING_NUM_BLOCKS * DRVSDIO_DEFAULTBLOCKSIZE;
        transactionList.buffer = tuningTempBuff;
        ret = DrvSdioWriteDataBlockList(0,&transactionList,1,cardslot,&flags);
        if(ret != 0) printf("ERROR at fristRead %d\n",ret);
        if (DRVSDIO_ISTHERE_COMMAND_DONE_ERROR(flags))
            printf("flags 0x%x\n",flags);
#endif


        *errorflags = 0;
#ifndef MMC_CODE // hush the compiler warning.
done:
    DRVSDIO_DPRINTF1("\nDrvSdioEnumerate Slot %d RCA 0x%x Flags 0x%x resp 0x%x\n", cardslot, drvsdioCards[cardslot].relativeCardAddress, flags, resp);
#endif
    return DRVSDIO_SUCCESS;
}
return DRVSDIO_ERROR;
}

/// Start a Read transaction on a card which has been previously configured
/// @param[in] blocknumber Number of blocks to read
/// @param[in] cardslot This is the card number
/// @param[in] errorflags The value of the RINTSTS (Raw Interrupt Status Register)
/// @return resp The value of RESPONSE register 0
static u32 drvSdioStartSingleBlockRead(u32 blocknumber, u32 cardslot, u32 *errorflags)
{
	u32 command = 0, arg = 0, flags = 0, resp = 0, mask = 0;
    DRVSDIO_RW_DPRINTF1("%s blocknumber=%d\n",__FUNCTION__,blocknumber);
	DRVSDIO_RW_DPRINTF1("\nCMD17\n");
    // Prepare command CMD17 for a single block read
	command = ((1 << DRVSDIO_CMD_START_CMD) | (1 << DRVSDIO_CMD_RESP_EXPECT) | (1 << DRVSDIO_CMD_CHECK_RESP_CRC) | (1 << DRVSDIO_CMD_DATA_EXPECTED) | (DRVSDIO_CMD17 << DRVSDIO_CMD_CMD_INDEX) | (1 << DRVSDIO_CMD_WAIT_PREVDATA_CMPL) | (1 << DRVSDIO_CMD_READ_CEATA_DEVICE));
    // Set the number of blocks to read as argumnet to the read command
	arg = blocknumber;
    // Load command
	resp = drvSdioLoadAndWait_ShortResponse(command, arg, TRUE, cardslot, &flags);

    DRVSDIO_RW_DPRINTF1("\n%s:%d Block 0x%x, arg 0x%x\n", __FUNCTION__,__LINE__,blocknumber,arg);
    // Check for standard errors
	if (DRVSDIO_ISTHERE_COMMAND_DONE_ERROR(flags) == 0) {
	// Wait for Data Transfer errors or data transfer over only if response was ok, otherwise we may wait for ever
    // TBD If we need to check for more errors
        mask = (1 << DRVSDIO_CARD_STATUS_OUT_OF_RANGE)  |
               (1 << DRVSDIO_CARD_STATUS_ADDRESS_ERROR) |
               (1 << DRVSDIO_CARD_STATUS_CARD_LOCKED)   |
               (1 << DRVSDIO_CARD_STATUS_ILLEGAL_COMMAND);
    DRVSDIO_DPRINTF1("\n%s:%d Block 0x%x ErrorFlags 0x%x Res 0x%x, mask 0x%x\n", __FUNCTION__,__LINE__,blocknumber, flags, resp,mask);
	if ((resp & mask) == 0)
	{
        flags |= DrvSdioWaitForFlags(DRVSDIO_DATATRANSFER_RD_ERROR_FLAGS | (1 << DRVSDIO_RAWINTSTATUS_DTO));
    }
		else
			flags |= (1 << DRVSDIO_R1_ERROR_FLAG);
    }
	DRVSDIO_DPRINTF1("\n%s:%d Block 0x%x ErrorFlags 0x%x Res 0x%x\n", __FUNCTION__,__LINE__,blocknumber, flags, resp);
	*errorflags = flags;
	return resp;
}

/// Start a Single Write transaction on a card which has been previously configured
///
/// @param[in] blocknumber Number of blocks to write
/// @param[in] cardslot This is the card number
/// @param[in] errorflags The value of the RINTSTS (Raw Interrupt Status Register)
/// @return resp The value of RESPONSE register 0
static u32 drvSdioStartSingleBlockWrite(u32 blocknumber, u32 cardslot, u32 *errorflags)
{
	u32 command = 0, arg = 0, flags = 0, resp = 0;
    DRVSDIO_RW_DPRINTF1("%s blocknumber=%d\n",__FUNCTION__,blocknumber);

	DRVSDIO_RW_DPRINTF1("\nCMD24\n");
	// Prepare command CMD24 for a single block write
	command = ((1 << DRVSDIO_CMD_START_CMD) | (1 << DRVSDIO_CMD_RESP_EXPECT) | (1 << DRVSDIO_CMD_WAIT_PREVDATA_CMPL) | (1 << DRVSDIO_CMD_CHECK_RESP_CRC) | (1 << DRVSDIO_CMD_DATA_EXPECTED) | (1 << DRVSDIO_CMD_READ_WRITE) | (DRVSDIO_CMD24 << DRVSDIO_CMD_CMD_INDEX));
	// Set the number of blocks to read as argumnet to the read command
	arg = blocknumber;
	// Load command
    DRVSDIO_RW_DPRINTF1("\n%s:%d block = %d, arg = %x\n", __FUNCTION__,__LINE__,blocknumber,arg);
	resp = drvSdioLoadAndWait_ShortResponse(command, arg, TRUE, cardslot, &flags);
	// Check for standard errors
	if (DRVSDIO_ISTHERE_COMMAND_DONE_ERROR(flags) == 0) {
		// Wait for Data Transfer errors or data transfer over only if response was ok, otherwise we may wait for ever
		// TBD If we need to check for more errors
		if ((resp & ((1 << DRVSDIO_CARD_STATUS_OUT_OF_RANGE) | (1 << DRVSDIO_CARD_STATUS_ADDRESS_ERROR) | (1 << DRVSDIO_CARD_STATUS_CARD_LOCKED) | (1 << DRVSDIO_CARD_STATUS_ILLEGAL_COMMAND))) == 0) {
			flags |= DrvSdioWaitForFlags(DRVSDIO_DATATRANSFER_WR_ERROR_FLAGS | (1 << DRVSDIO_RAWINTSTATUS_DTO));
			// Need to wait for Busy Clear Interrupt after DTO
			if ((flags & (1 << DRVSDIO_RAWINTSTATUS_DTO)) && ((flags & DRVSDIO_DATATRANSFER_WR_ERROR_FLAGS) == 0))
				flags |= DrvSdioWaitForFlags((1 << DRVSDIO_RAWINTSTATUS_BCI));
		}
		else
			flags |= ( 1 << DRVSDIO_R1_ERROR_FLAG);
	}
	DRVSDIO_RW_DPRINTF1("\n%s:%d Block 0x%x ErrorFlags 0x%x Res 0x%x\n", __FUNCTION__,__LINE__,blocknumber, flags, resp);
	*errorflags = flags;
	return resp;
}


/// Start a Multiple Write transaction on a card which has been previously configured
///
/// @param[in] startblock Offset in blocks where the writing will start on the card
/// @param[in] blockcount Number of blocks to write
/// @param[in] cardslot This is the card number
/// @param[in] errorflags The value of the RINTSTS (Raw Interrupt Status Register)
/// @return resp The value of RESPONSE register 0
static u32 drvSdioStartMultipleBlockWrite(u32 startblock, u32 blockcount, u32 cardslot, u32 *errorflags)
{   UNUSED(blockcount); //hush the compiler warning.
	u32 command = 0, arg = 0, flags = 0, resp = 0;
    DRVSDIO_RW_DPRINTF1("%s startblock=%d, blockcount=%d\n",__FUNCTION__,startblock,blockcount);



		DRVSDIO_RW_DPRINTF1("\nCMD25\n");
		// Prepare command for multi block writing
        command = ((1 << DRVSDIO_CMD_SEND_AUTO_STOP) | (1 << DRVSDIO_CMD_START_CMD) | (1 << DRVSDIO_CMD_WAIT_PREVDATA_CMPL) | (1 << DRVSDIO_CMD_RESP_EXPECT) | (1 << DRVSDIO_CMD_CHECK_RESP_CRC) | (1 << DRVSDIO_CMD_DATA_EXPECTED) | (1 << DRVSDIO_CMD_READ_WRITE) | (DRVSDIO_CMD25 << DRVSDIO_CMD_CMD_INDEX));
        command = ((1 << DRVSDIO_CMD_SEND_AUTO_STOP) | (1 << DRVSDIO_CMD_START_CMD) | (1 << DRVSDIO_CMD_WAIT_PREVDATA_CMPL) | (1 << DRVSDIO_CMD_RESP_EXPECT) | (1 << DRVSDIO_CMD_DATA_EXPECTED) | (1 << DRVSDIO_CMD_READ_WRITE) | (DRVSDIO_CMD25 << DRVSDIO_CMD_CMD_INDEX));
		// Set the start block as argument
		arg = startblock;
		// Load command
		resp = drvSdioLoadAndWait_ShortResponse(command, arg, TRUE, cardslot, &flags);
		if (DRVSDIO_ISTHERE_COMMAND_DONE_ERROR(flags) == 0) {
			// Wait for Data Transfer errors or data transfer over only if response is OK, otherwise we may wait for ever
			// TBD If we need to check for more errors
			if ((resp & ((1 << DRVSDIO_CARD_STATUS_OUT_OF_RANGE) | (1 << DRVSDIO_CARD_STATUS_ADDRESS_ERROR) | (1 << DRVSDIO_CARD_STATUS_CARD_LOCKED) | (1 << DRVSDIO_CARD_STATUS_ILLEGAL_COMMAND))) == 0) {
				flags |= DrvSdioWaitForFlags(DRVSDIO_DATATRANSFER_WR_ERROR_FLAGS | (1 << DRVSDIO_RAWINTSTATUS_DTO) | (1 << DRVSDIO_RAWINTSTATUS_HTO));
				// Need to wait for Busy Clear Interrupt after DTO
				if (flags & (DRVSDIO_DATATRANSFER_WR_ERROR_FLAGS | (1 << DRVSDIO_RAWINTSTATUS_DTO))) {
					flags |= DrvSdioWaitForFlags(DRVSDIO_DATATRANSFER_WR_ERROR_FLAGS | (1 << DRVSDIO_RAWINTSTATUS_BCI));
					if ((flags & (1 << DRVSDIO_RAWINTSTATUS_BCI)) && ((flags & DRVSDIO_DATATRANSFER_WR_ERROR_FLAGS) == 0))
						flags |= DrvSdioWaitForFlags((1 << DRVSDIO_RAWINTSTATUS_ACD));
				}
			}
			else
				flags |= (1 << DRVSDIO_R1_ERROR_FLAG);
		}

	*errorflags = flags;
	DRVSDIO_RW_DPRINTF1("\ndrvSdioStartMultipleBlockWrite Start %d Count %d Card %d Error Flags 0x%x Resp 0x%x\n", startblock, blockcount, cardslot, flags, resp);
	return resp;
}

///  Start a Multiple Read transaction on a card which has been previously configured
///
/// @param[in] startblock Offset in blocks where the writing will start on the card
/// @param[in] blockcount Number of blocks to write
/// @param[in] cardslot This is the card number
/// @param[in] errorflags The value of the RINTSTS (Raw Interrupt Status Register)
/// @return resp The value of RESPONSE register 0
static u32 drvSdioStartMultipleBlockRead(u32 startblock, u32 blockcount, u32 cardslot, u32 *errorflags)
{
	UNUSED(blockcount); //hush the compiler warning.
	u32 command = 0, arg = 0, flags = 0, resp = 0;
    DRVSDIO_RW_DPRINTF1("%s:%d startblock=%d, blockcount=%d\n",__FUNCTION__,__LINE__,startblock,blockcount);

        DRVSDIO_RW_DPRINTF1("\nCMD18\n");
	// Prepare command
	command = ((1 << DRVSDIO_CMD_SEND_AUTO_STOP) | (1 << DRVSDIO_CMD_START_CMD) | (1 << DRVSDIO_CMD_RESP_EXPECT) | (1 << DRVSDIO_CMD_CHECK_RESP_CRC) | (1 << DRVSDIO_CMD_DATA_EXPECTED) | (1 << DRVSDIO_CMD_WAIT_PREVDATA_CMPL) | (DRVSDIO_CMD18 << DRVSDIO_CMD_CMD_INDEX));
	// Set the start block as argument
	arg = startblock;
	// Load command
	resp = drvSdioLoadAndWait_ShortResponse(command, arg, TRUE, cardslot, &flags);


	if (DRVSDIO_ISTHERE_COMMAND_DONE_ERROR(flags) == 0) {
		// TBD If we need to check for more errors
		if ((resp & ((1 << DRVSDIO_CARD_STATUS_OUT_OF_RANGE) | (1 << DRVSDIO_CARD_STATUS_ADDRESS_ERROR) | (1 << DRVSDIO_CARD_STATUS_CARD_LOCKED) | (1 << DRVSDIO_CARD_STATUS_ILLEGAL_COMMAND))) == 0) {
			// Wait for Data Transfer errors or data transfer over
			flags |= DrvSdioWaitForFlags(DRVSDIO_DATATRANSFER_RD_ERROR_FLAGS | (1 << DRVSDIO_RAWINTSTATUS_DTO));
			if ((flags & (1 << DRVSDIO_RAWINTSTATUS_DTO)) && ((flags & DRVSDIO_DATATRANSFER_RD_ERROR_FLAGS) == 0)) {
				// Wait for AC Done
			    flags |= DrvSdioWaitForFlags(DRVSDIO_DATATRANSFER_RD_ERROR_FLAGS | (1 << DRVSDIO_RAWINTSTATUS_ACD));
			}
		}
		else
			flags |= (1 << DRVSDIO_R1_ERROR_FLAG);
	}
	*errorflags = flags;
	DRVSDIO_RW_DPRINTF1("\n%s:%d Start %d Count %d Card %d Error Flags 0x%x Resp 0x%x\n", __FUNCTION__,__LINE__,startblock, blockcount, cardslot, flags, resp);
	return resp;
}
#else // end of MMC_CODE, beginning of original code
DRV_RETURN_TYPE DrvSdioEnumerate(u32 cardslot, u32 *errorflags)
{
    u32 tmp = 0, command = 0, arg = 0, resp = 0, flags = 0, cardTransferSpeed = 0, i = 0;
    // Just in case  
    assert(cardslot < DRVSDIO_MAXDRIVERCARDS);
    if ((tyDrvSdio_Driver.status & (1 << DRVSDIO_INTERNALSD_STATUS_FREQ_SWITCHED))) {           
        memset(&drvsdioCards[cardslot], 0x00, sizeof(tyDrvSdioCardConfig));
        // Use Hold register by default
        drvsdioCards[cardslot].useHoldRegister = TRUE;
        DPRINTF1("\nEnumerating Card in Slot %d \n", cardslot);
        // Clear any pending interrpts
        SET_REG_WORD(SDIOH1_RINTSTS_S1, DRVSDIO_RAWINTSTATUS_ALL);

        DPRINTF1("\nCMD0\n");
        // Prepare command
        command = (1 << DRVSDIO_CMD_START_CMD) | (DRVSDIO_CMD0 << DRVSDIO_CMD_CMD_INDEX);       
        // No args for CMD0
        arg = 0;
        // Load command
        tmp = drvSdioLoadAndWait(command, arg, FALSE, cardslot);

        // Check for errors
        if (DRVSDIO_ISTHERE_COMMAND_DONE_ERROR(tmp) == 0)
        {
            // CMD8 may fail sometimes when fast cards are used. That's why we need to repeated again
            for (i = 0; i < 4; i++)
            {
                DPRINTF1("\nCMD8\n");
                // Prepare command
                command = (1 << DRVSDIO_CMD_START_CMD) | (1 << DRVSDIO_CMD_RESP_EXPECT) | (1 << DRVSDIO_CMD_CHECK_RESP_CRC) | (DRVSDIO_CMD8 << DRVSDIO_CMD_CMD_INDEX);
                // Pass voltage window as parameter to CMD8
                arg = ((DRVSDIO_CMD8_ARG_DEFAULT_PATTERN << DRVSDIO_CMD8_ARG_PATTERN) | (DRVSDIO_CMD8_ARG_V_2_7__3_6 << DRVSDIO_CMD8_ARG_VOLTAGE));
                // Load command
                resp = drvSdioLoadAndWait_ShortResponse(command, arg, FALSE, cardslot, &flags); 
                // If successfull CMD8 should echo the voltage window passed as argument to it
                DPRINTF1("\nCMD8 flags 0x%x   arg 0x%x  tinonino 0x%x\n", flags, arg, DRVSDIO_ISTHERE_COMMAND_DONE_ERROR(flags));
                if ((DRVSDIO_ISTHERE_COMMAND_DONE_ERROR(flags) == 0) && (resp == arg))
                    break;
            }
            // Check for Errors
            if ((DRVSDIO_ISTHERE_COMMAND_DONE_ERROR(flags) == 0) && (resp == arg)) {        
                resp = 0;

                DPRINTF1("\nCMD55 11\n");
                // Prepare command. Need to be performed prior CMD41
                command = ((1 << DRVSDIO_CMD_START_CMD) | (DRVSDIO_CMD55 << DRVSDIO_CMD_CMD_INDEX) | (1 << DRVSDIO_CMD_RESP_EXPECT) | (1 << DRVSDIO_CMD_CHECK_RESP_CRC));                   
                // No args for CMD55
                arg = 0;
                // Load command
                resp = drvSdioLoadAndWait_ShortResponse(command, arg, FALSE, cardslot, &flags);
                // Check for Errors                                     
                if (DRVSDIO_ISTHERE_COMMAND_DONE_ERROR(flags) == 0) {
                    DPRINTF1("\nACMD41  11\n");
                    // Prepare command
                    // DO NOT USE CRC BECAUSE IT WILL ALWAYS FAIL !!!!!!!!! 
                    command = ((1 << DRVSDIO_CMD_START_CMD) | (DRVSDIO_CMD41 << DRVSDIO_CMD_CMD_INDEX) | (1 << DRVSDIO_CMD_RESP_EXPECT));
                    // Arg should be 0
                    arg = 0x0000;   
                    // Load command         
                    resp = drvSdioLoadAndWait_ShortResponse(command, arg, FALSE, cardslot, &flags); 
                }

                // Need a proper response to ACMD41 as well a no errors 
                while(((resp & (1 << DRVSDIO_ACMD41_ARG_BUSY)) == 0) && (DRVSDIO_ISTHERE_COMMAND_DONE_ERROR(flags) == 0)) {                                                              
                    DPRINTF1("\nCMD55\n");
                    // Prepare command. Need to be performed prior CMD41
                    command = ((1 << DRVSDIO_CMD_START_CMD) | (DRVSDIO_CMD55 << DRVSDIO_CMD_CMD_INDEX) | (1 << DRVSDIO_CMD_RESP_EXPECT) | (1 << DRVSDIO_CMD_CHECK_RESP_CRC));                   
                    // No args for CMD55
                    arg = 0;
                    // Load command
                    resp = drvSdioLoadAndWait_ShortResponse(command, arg, FALSE, cardslot, &flags);
                    // Check for Errors                                     
                    if (DRVSDIO_ISTHERE_COMMAND_DONE_ERROR(flags) == 0) {
                        DPRINTF1("\nACMD41\n");
                        // DO NOT USE CRC BECAUSE IT WILL ALWAYS FAIL !!!!!!!!! 
                        // Prepare command.
                        command = ((1 << DRVSDIO_CMD_START_CMD) | (DRVSDIO_CMD41 << DRVSDIO_CMD_CMD_INDEX) | (1 << DRVSDIO_CMD_RESP_EXPECT));
                        // Set flags for High Speed, Voltage switch to check if those options are supported by the card
                        arg = ((1 << DRVSDIO_ACMD41_ARG_HCS)  | (1 << DRVSDIO_ACMD41_ARG_OCR_3_2__3_3) | (1 << DRVSDIO_ACMD41_ARG_XPC) | (1 << DRVSDIO_ACMD41_ARG_S18R) | (DRVSDIO_HOST_VOLT_RANGE));               
                        // Load command
                        resp = drvSdioLoadAndWait_ShortResponse(command, arg, FALSE, cardslot, &flags);             
                    }
                }           
                // Check for Errors 
                if (DRVSDIO_ISTHERE_COMMAND_DONE_ERROR(flags) == 0) {
                // Use 4-bit width by default
                    drvsdioCards[cardslot].busWidth = 4;                                                        
                    if (resp & (1 << DRVSDIO_R3_CCS)) {
                        DPRINTF1("\nThe CCS bit set in response for ACMD 41 shows that card is a High Density SD-MEM card %d  resp: 0x%x\n", cardslot, resp);                       
                        // If bit S18A is set then the card supports voltage switch
                        if (resp & (1 << DRVSDIO_R3_S18A)) {                        
                            DPRINTF1("\nCard supports voltage switching to 1.8V, but it is not supported by the board for now\n");
                            // At this moment board does not supports voltage switch 
                            //drvSdioVoltageSwitch(cardslot);
                        } else {
                            DPRINTF1("\nCard does not support voltage switch to 1.8V\n");
                        }
                        drvsdioCards[cardslot].sdhc = TRUE;                                                                 
                    }
                    else {
                        drvsdioCards[cardslot].sdhc = FALSE;
                        DPRINTF1("\nThe CCS bit set to 0 in response for ACMD 41 shows that card is a Low Density SD-MEM card %d\n", cardslot); 
                    }
                    DPRINTF1("\nCMD2\n");
                    // Prepare command
                    command = ((1 << DRVSDIO_CMD_START_CMD) | (DRVSDIO_CMD2 << DRVSDIO_CMD_CMD_INDEX) | (1 << DRVSDIO_CMD_CHECK_RESP_CRC) | (1 << DRVSDIO_CMD_RESP_LENGTH) | (1 << DRVSDIO_CMD_RESP_EXPECT));
                    // No args for CMD2
                    arg = 0;
                    // Load command
                    flags = drvSdioLoadAndWait_LongResponse(command, arg, FALSE, cardslot, drvsdioCards[cardslot].cid);                 
                    // Check for errors
                    if (DRVSDIO_ISTHERE_COMMAND_DONE_ERROR(flags) == 0) {
                        // We should do something with this information TBD                     
                        while ((drvsdioCards[cardslot].relativeCardAddress == 0) && (DRVSDIO_ISTHERE_COMMAND_DONE_ERROR(flags) == 0)) {
                            DPRINTF1("\nCMD3\n");
                            // Prepare command
                            command = ((1 << DRVSDIO_CMD_START_CMD) | (DRVSDIO_CMD3 << DRVSDIO_CMD_CMD_INDEX) | (1 << DRVSDIO_CMD_RESP_EXPECT) | (1 << DRVSDIO_CMD_CHECK_RESP_CRC));
                            // No args for CMD3
                            arg = 0;
                            // Load command
                            resp = drvSdioLoadAndWait_ShortResponse(command, arg, FALSE, cardslot, &flags); 
                            if ((DRVSDIO_ISTHERE_COMMAND_DONE_ERROR(flags) == 0) && (resp >> DRVSDIO_R6_RCA))
                                drvsdioCards[cardslot].relativeCardAddress = (resp >> DRVSDIO_R6_RCA);                      
                        }
                        if (drvsdioCards[cardslot].relativeCardAddress > 0) {                           
                            drvSdioSwitchToHighSpeedMode(cardslot, &flags);                             
                            if ((DRVSDIO_ISTHERE_COMMAND_DONE_ERROR(flags) != 0)) {
                                DPRINTF1("\nCMD6 failed to switch to High-Speed mode\n");
                            }
                        }

                        if ((drvsdioCards[cardslot].relativeCardAddress > 0) && (DRVSDIO_ISTHERE_COMMAND_DONE_ERROR(flags) == 0)) {
                            DPRINTF1("\nCMD9\n");
                            // Prepare command
                            command = ((1 << DRVSDIO_CMD_START_CMD) | (DRVSDIO_CMD9 << DRVSDIO_CMD_CMD_INDEX) | (1 << DRVSDIO_CMD_CHECK_RESP_CRC) | (1 << DRVSDIO_CMD_RESP_LENGTH) | (1 << DRVSDIO_CMD_RESP_EXPECT));
                            // Relative address as arg
                            arg = (drvsdioCards[cardslot].relativeCardAddress << DRVSDIO_R6_RCA);
                            // Load command
                            flags = drvSdioLoadAndWait_LongResponse(command, arg, FALSE, cardslot, drvsdioCards[cardslot].csd);                                                                                                         
                            // Force it here TBD                            
                            drvsdioCards[cardslot].blockSize = DRVSDIO_DEFAULTBLOCKSIZE;
                            // The card is now initialized and enumerated
                            // Switch to working clock frequency. The working speed is get from the CSD card register.
                            tmp = DrvSdioGetCardTransferSpeed(cardslot, &cardTransferSpeed);                            
                            if (DRVSDIO_ISTHERE_COMMAND_DONE_ERROR(tmp) == 0)
                            {                                       
                                tmp = drvSdioChangeFrequency(cardslot, 0, tyDrvSdio_Driver.cclk_in_khz, cardTransferSpeed);                             
                                if (DRVSDIO_ISTHERE_COMMAND_DONE_ERROR(flags) != 0)
                                {
                                    DPRINTF1("\nError changing frequency\n");
                                    *errorflags = flags;
                                    return DRVSDIO_ERROR;
                                }
                            }
                        }
                        else {
                            DPRINTF1("\nError in CMD3 !  relCardAddr 0x%X\n", drvsdioCards[cardslot].relativeCardAddress);
                            *errorflags = flags;
                            return DRVSDIO_ERROR;
                        }
                    }
                    else {
                        DPRINTF1("\nError in CMD2 !\n");
                        *errorflags = flags;
                        return DRVSDIO_ERROR;
                    }   
                }
                else {
                    DPRINTF1("\nError in CMD41 !\n");
                    *errorflags = flags;
                    return DRVSDIO_ERROR;
                }   
            }       
            else {
                // 
                DPRINTF1("\nNO ANSWER TO CMD8 !!!!!!\n");
                *errorflags = flags;
                return DRVSDIO_ERROR;
            }           
        }
        else {
            DPRINTF1("\nError in CMD0 !\n");
            *errorflags = flags;
            return DRVSDIO_ERROR;
        }
        *errorflags = flags;    
        DPRINTF1("\nDrvSdioEnumerate Slot %d RCA 0x%x Flags 0x%x resp 0x%x\n", cardslot, drvsdioCards[cardslot].relativeCardAddress, flags, resp);
        return DRVSDIO_SUCCESS;
    }
    return DRVSDIO_ERROR;
}

/// Start a Read transaction on a card which has been previously configured
///
/// @param[in] blocknumber Number of blocks to read
/// @param[in] cardslot This is the card number
/// @param[in] errorflags The value of the RINTSTS (Raw Interrupt Status Register)
/// @return resp The value of RESPONSE register 0
static u32 drvSdioStartSingleBlockRead(u32 blocknumber, u32 cardslot, u32 *errorflags)
{
    u32 command = 0, arg = 0, flags = 0, resp = 0;
    DPRINTF1("\nCMD17\n");  
    // Prepare command CMD17 for a single block read
    command = ((1 << DRVSDIO_CMD_START_CMD) | (1 << DRVSDIO_CMD_RESP_EXPECT) | (1 << DRVSDIO_CMD_CHECK_RESP_CRC) | (1 << DRVSDIO_CMD_DATA_EXPECTED) | (DRVSDIO_CMD17 << DRVSDIO_CMD_CMD_INDEX) | (1 << DRVSDIO_CMD_WAIT_PREVDATA_CMPL) | (1 << DRVSDIO_CMD_READ_CEATA_DEVICE));
    // Set the number of blocks to read as argumnet to the read command
    arg = blocknumber;
    // Load command
    resp = drvSdioLoadAndWait_ShortResponse(command, arg, TRUE, cardslot, &flags);
    
    // Check for standard errors    
    if (DRVSDIO_ISTHERE_COMMAND_DONE_ERROR(flags) == 0) {
        // Wait for Data Transfer errors or data transfer over only if response was ok, otherwise we may wait for ever
        // TBD If we need to check for more errors
        if ((resp & ((1 << DRVSDIO_CARD_STATUS_OUT_OF_RANGE) | (1 << DRVSDIO_CARD_STATUS_ADDRESS_ERROR) | (1 << DRVSDIO_CARD_STATUS_CARD_LOCKED) | (1 << DRVSDIO_CARD_STATUS_ILLEGAL_COMMAND))) == 0)
            flags |= DrvSdioWaitForFlags(DRVSDIO_DATATRANSFER_RD_ERROR_FLAGS | (1 << DRVSDIO_RAWINTSTATUS_DTO));
        else
            flags |= (1 << DRVSDIO_R1_ERROR_FLAG);
    }       
    DPRINTF1("\ndrvSdioStartSingleBlockRead Block 0x%x ErrorFlags 0x%x Res 0x%x\n", blocknumber, flags, resp);
    *errorflags = flags;
    return resp;
}

/// Start a Single Write transaction on a card which has been previously configured
///
/// @param[in] blocknumber Number of blocks to write
/// @param[in] cardslot This is the card number
/// @param[in] errorflags The value of the RINTSTS (Raw Interrupt Status Register)
/// @return resp The value of RESPONSE register 0
static u32 drvSdioStartSingleBlockWrite(u32 blocknumber, u32 cardslot, u32 *errorflags)
{
    u32 command = 0, arg = 0, flags = 0, resp = 0;
    DPRINTF1("\nCMD24\n");  
    // Prepare command CMD24 for a single block write
    command = ((1 << DRVSDIO_CMD_START_CMD) | (1 << DRVSDIO_CMD_RESP_EXPECT) | (1 << DRVSDIO_CMD_WAIT_PREVDATA_CMPL) | (1 << DRVSDIO_CMD_CHECK_RESP_CRC) | (1 << DRVSDIO_CMD_DATA_EXPECTED) | (1 << DRVSDIO_CMD_READ_WRITE) | (DRVSDIO_CMD24 << DRVSDIO_CMD_CMD_INDEX));
    // Set the number of blocks to read as argumnet to the read command
    arg = blocknumber;
    // Load command
    resp = drvSdioLoadAndWait_ShortResponse(command, arg, TRUE, cardslot, &flags);
    // Check for standard errors 
    if (DRVSDIO_ISTHERE_COMMAND_DONE_ERROR(flags) == 0) {   
        // Wait for Data Transfer errors or data transfer over only if response was ok, otherwise we may wait for ever
        // TBD If we need to check for more errors
        if ((resp & ((1 << DRVSDIO_CARD_STATUS_OUT_OF_RANGE) | (1 << DRVSDIO_CARD_STATUS_ADDRESS_ERROR) | (1 << DRVSDIO_CARD_STATUS_CARD_LOCKED) | (1 << DRVSDIO_CARD_STATUS_ILLEGAL_COMMAND))) == 0) {
            flags |= DrvSdioWaitForFlags(DRVSDIO_DATATRANSFER_WR_ERROR_FLAGS | (1 << DRVSDIO_RAWINTSTATUS_DTO));
            // Need to wait for Busy Clear Interrupt after DTO 
            if ((flags & (1 << DRVSDIO_RAWINTSTATUS_DTO)) && ((flags & DRVSDIO_DATATRANSFER_WR_ERROR_FLAGS) == 0))
                flags |= DrvSdioWaitForFlags((1 << DRVSDIO_RAWINTSTATUS_BCI));  
        }
        else
            flags |= ( 1 << DRVSDIO_R1_ERROR_FLAG);
    }   
    DPRINTF1("\ndrvSdioStartSingleBlockWrite Block 0x%x ErrorFlags 0x%x Res 0x%x\n", blocknumber, flags, resp);
    *errorflags = flags;
    return resp;
}

/// Start a Multiple Write transaction on a card which has been previously configured
///
/// @param[in] startblock Offset in blocks where the writing will start on the card
/// @param[in] blockcount Number of blocks to write
/// @param[in] cardslot This is the card number
/// @param[in] errorflags The value of the RINTSTS (Raw Interrupt Status Register)
/// @return resp The value of RESPONSE register 0
static u32 drvSdioStartMultipleBlockWrite(u32 startblock, u32 blockcount, u32 cardslot, u32 *errorflags)
{
    u32 command = 0, arg = 0, flags = 0, resp = 0;

    // Performing CMD55 prior CMD23 makes CMD23 -> ACMD23
    DPRINTF1("\nSend CMD55 \n");
    // Prepare command
    command = ((1 << DRVSDIO_CMD_START_CMD) | (DRVSDIO_CMD55 << DRVSDIO_CMD_CMD_INDEX) | (1 << DRVSDIO_CMD_RESP_EXPECT) | (1 << DRVSDIO_CMD_CHECK_RESP_CRC));
    // No arg for CMD55
    arg = 0;
    // Load command
    flags = drvSdioLoadAndWait(command, (drvsdioCards[cardslot].relativeCardAddress << DRVSDIO_R6_RCA), FALSE, cardslot);
    // Check for Errors                                     
    if (DRVSDIO_ISTHERE_COMMAND_DONE_ERROR(flags) == 0) {
        DPRINTF1("\nACMD23\n"); 
        // Prepare command. ACMD23 will pre-erase blocks before writing. This should speed-up the writing procedure for multiple blocks
        command = ((1 << DRVSDIO_CMD_START_CMD) | (1 << DRVSDIO_CMD_RESP_EXPECT) | (1 << DRVSDIO_CMD_CHECK_RESP_CRC) | (DRVSDIO_CMD23 << DRVSDIO_CMD_CMD_INDEX));
        // Set number of blocks that will be pre-erased
        arg = blockcount;
        // Load command
        resp = drvSdioLoadAndWait_ShortResponse(command, arg, TRUE, cardslot, &flags);
    }
    if (DRVSDIO_ISTHERE_COMMAND_DONE_ERROR(flags) == 0) {
        DPRINTF1("\nCMD25\n");  
        // Prepare command for multi block writing
        command = ((1 << DRVSDIO_CMD_SEND_AUTO_STOP) | (1 << DRVSDIO_CMD_START_CMD) | (1 << DRVSDIO_CMD_WAIT_PREVDATA_CMPL) | (1 << DRVSDIO_CMD_RESP_EXPECT) | (1 << DRVSDIO_CMD_CHECK_RESP_CRC) | (1 << DRVSDIO_CMD_DATA_EXPECTED) | (1 << DRVSDIO_CMD_READ_WRITE) | (DRVSDIO_CMD25 << DRVSDIO_CMD_CMD_INDEX));
        // Set the start block as argument
        arg = startblock;
        // Load command
        resp = drvSdioLoadAndWait_ShortResponse(command, arg, TRUE, cardslot, &flags);
        if (DRVSDIO_ISTHERE_COMMAND_DONE_ERROR(flags) == 0) {
            // Wait for Data Transfer errors or data transfer over only if response is OK, otherwise we may wait for ever
            // TBD If we need to check for more errors
            if ((resp & ((1 << DRVSDIO_CARD_STATUS_OUT_OF_RANGE) | (1 << DRVSDIO_CARD_STATUS_ADDRESS_ERROR) | (1 << DRVSDIO_CARD_STATUS_CARD_LOCKED) | (1 << DRVSDIO_CARD_STATUS_ILLEGAL_COMMAND))) == 0) {         
                flags |= DrvSdioWaitForFlags(DRVSDIO_DATATRANSFER_WR_ERROR_FLAGS | (1 << DRVSDIO_RAWINTSTATUS_DTO) | (1 << DRVSDIO_RAWINTSTATUS_HTO));
                // Need to wait for Busy Clear Interrupt after DTO 
                if (flags & (DRVSDIO_DATATRANSFER_WR_ERROR_FLAGS | (1 << DRVSDIO_RAWINTSTATUS_DTO))) {
                    flags |= DrvSdioWaitForFlags(DRVSDIO_DATATRANSFER_WR_ERROR_FLAGS | (1 << DRVSDIO_RAWINTSTATUS_BCI));    
                    if ((flags & (1 << DRVSDIO_RAWINTSTATUS_BCI)) && ((flags & DRVSDIO_DATATRANSFER_WR_ERROR_FLAGS) == 0))
                        flags |= DrvSdioWaitForFlags((1 << DRVSDIO_RAWINTSTATUS_ACD));  
                }
            }
            else
                flags |= (1 << DRVSDIO_R1_ERROR_FLAG);
        }   
    }   
    
    *errorflags = flags;
    DPRINTF1("\ndrvSdioStartMultipleBlockWrite Start %d Count %d Card %d Error Flags 0x%x Resp 0x%x\n", startblock, blockcount, cardslot, flags, resp);
    return resp;
}

///  Start a Multiple Read transaction on a card which has been previously configured
///
/// @param[in] startblock Offset in blocks where the writing will start on the card
/// @param[in] blockcount Number of blocks to write
/// @param[in] cardslot This is the card number
/// @param[in] errorflags The value of the RINTSTS (Raw Interrupt Status Register)
/// @return resp The value of RESPONSE register 0
static u32 drvSdioStartMultipleBlockRead(u32 startblock, u32 blockcount, u32 cardslot, u32 *errorflags)
{
    UNUSED(blockcount); // hush the compiler warning.

    u32 command = 0, arg = 0, flags = 0, resp = 0;      
    DPRINTF1("\nCMD18\n");  
    // Prepare command  
    command = ((1 << DRVSDIO_CMD_SEND_AUTO_STOP) | (1 << DRVSDIO_CMD_START_CMD) | (1 << DRVSDIO_CMD_RESP_EXPECT) | (1 << DRVSDIO_CMD_CHECK_RESP_CRC) | (1 << DRVSDIO_CMD_DATA_EXPECTED) | (1 << DRVSDIO_CMD_WAIT_PREVDATA_CMPL) | (DRVSDIO_CMD18 << DRVSDIO_CMD_CMD_INDEX));        
    // Set the start block as argument
    arg = startblock;
    // Load command
    resp = drvSdioLoadAndWait_ShortResponse(command, arg, TRUE, cardslot, &flags);  
    if (DRVSDIO_ISTHERE_COMMAND_DONE_ERROR(flags) == 0) {       
        // TBD If we need to check for more errors
        if ((resp & ((1 << DRVSDIO_CARD_STATUS_OUT_OF_RANGE) | (1 << DRVSDIO_CARD_STATUS_ADDRESS_ERROR) | (1 << DRVSDIO_CARD_STATUS_CARD_LOCKED) | (1 << DRVSDIO_CARD_STATUS_ILLEGAL_COMMAND))) == 0) {                         
            // Wait for Data Transfer errors or data transfer over 
            flags |= DrvSdioWaitForFlags(DRVSDIO_DATATRANSFER_RD_ERROR_FLAGS | (1 << DRVSDIO_RAWINTSTATUS_DTO));                
            if ((flags & (1 << DRVSDIO_RAWINTSTATUS_DTO)) && ((flags & DRVSDIO_DATATRANSFER_RD_ERROR_FLAGS) == 0)) {
                // Wait for AC Done
                flags |= DrvSdioWaitForFlags(DRVSDIO_DATATRANSFER_RD_ERROR_FLAGS | (1 << DRVSDIO_RAWINTSTATUS_ACD));    
            }
        }
        else
            flags |= (1 << DRVSDIO_R1_ERROR_FLAG);
    }
    *errorflags = flags;
    DPRINTF1("\ndrvSdioStartMultipleBlockRead Start %d Count %d Card %d Error Flags 0x%x Resp 0x%x\n", startblock, blockcount, cardslot, flags, resp);
    return resp;
}
#endif  // end of if/else MMC_CODE
/// Allows to erase blocks from the card
///
///
/// Card must be selected (transfer mode )!!!! CMD 32 CMD33 and CMD38 to execute operations.
/// Resp contains the sd card status (R1/R1b)
///
///
/// @param[in] startblock Offset in blocks where the writing will start on the card
/// @param[in] blockcount Number of blocks to write
/// @param[in] cardslot This is the card number
/// @param[in] errorflags The value of the RINTSTS (Raw Interrupt Status Register)
/// @return resp Contains the sd card status (R1/R1b)
static u32 drvSdioEraseBlocks(u32 firstblock, u32 lastblock, u32 card, u32 *errorflags)
{    
    u32 iflags = 0, tmp = 0, resp = 0; 
    DPRINTF1("\nCMD32\n");
    // Support for SD cards
    if (drvsdioCards[card].sdhc == 0) {
        firstblock *= drvsdioCards[card].blockSize;
        lastblock *= drvsdioCards[card].blockSize;
    }
    // Prepare and Load command
    tmp = ((1 << DRVSDIO_CMD_START_CMD) | (1 << DRVSDIO_CMD_RESP_EXPECT) | (1 << DRVSDIO_CMD_CHECK_RESP_CRC) | (DRVSDIO_CMD32 << DRVSDIO_CMD_CMD_INDEX));
    resp = drvSdioLoadAndWait_ShortResponse(tmp, firstblock, TRUE, card, &iflags);
    // Simple check for errors
    if (((iflags & (DRVSDIO_COMMANDDONE_ERROR_FLAGS | (1 << DRVSDIO_RAWINTSTATUS_HLE))) == 0) && ((resp & DRVSDIO_CARD_STATUS_ERASESEQ_FLAGS_MASK) == 0)) { 
        DPRINTF1("\nCMD33\n");
        // Prepare and Load command
        tmp = ((1 << DRVSDIO_CMD_START_CMD) | (1 << DRVSDIO_CMD_RESP_EXPECT) | (1 << DRVSDIO_CMD_CHECK_RESP_CRC) | (DRVSDIO_CMD33 << DRVSDIO_CMD_CMD_INDEX));
        resp = drvSdioLoadAndWait_ShortResponse(tmp, lastblock, TRUE, card, &iflags);
        // Simple check for errors
        if (((iflags & (DRVSDIO_COMMANDDONE_ERROR_FLAGS | (1 << DRVSDIO_RAWINTSTATUS_HLE))) == 0) && ((resp & DRVSDIO_CARD_STATUS_ERASESEQ_FLAGS_MASK) == 0)) {
            DPRINTF1("\nCMD38\n");
            // Prepare and Load command
            tmp = ((1 << DRVSDIO_CMD_START_CMD) | (1 << DRVSDIO_CMD_RESP_EXPECT) | (1 << DRVSDIO_CMD_CHECK_RESP_CRC) | (DRVSDIO_CMD38 << DRVSDIO_CMD_CMD_INDEX));
            resp = drvSdioLoadAndWait_ShortResponse(tmp, 0, TRUE, card, &iflags);           
        }   
    }   
    *errorflags = iflags;
    return resp;
}

/// Allows to work out the total size of a scattered transfer
///
///
///
/// @param[in] transactionList Transaction list
/// @param[in] count Length of the transaction list
/// @return Size of the transfer
static u32 drvSdioGetScatteredTransactionSize(tyDrvSdio_Transaction *transactionList, u32 count)
{
    u32 i, size = 0;
    for (i = 0; i < count ; i ++)
        size += transactionList[i].size;    
    return size;
}

DRV_RETURN_TYPE DrvSdioWriteDataBlockList(u32 sdblocknumber, tyDrvSdio_Transaction *transactionList, u32 count, u32 cardslot, u32 *errorflags)
{
    u32 flags = 0, resp = 0, descriptors_base_address = 0;
    
    assert(cardslot < DRVSDIO_MAXDRIVERCARDS);
    // Select a card for writing
    resp = drvSdioCardSelect(drvsdioCards[cardslot].relativeCardAddress, cardslot, &flags);
    // Simple error flags check
    if (DRVSDIO_ISTHERE_COMMAND_DONE_ERROR(flags) == 0) {
        // Set bus width
//        resp = drvSdioSelectBusWidth(drvsdioCards[cardslot].busWidth, drvsdioCards[cardslot].relativeCardAddress, cardslot, &flags);
        // Simple error flags check
        if (DRVSDIO_ISTHERE_COMMAND_DONE_ERROR(flags) == 0) {
            // Set block length
            resp = drvSdioSetBlockLength(drvsdioCards[cardslot].blockSize, drvSdioGetScatteredTransactionSize(transactionList,count), cardslot, &flags);
            // Simple error flags check
            if (DRVSDIO_ISTHERE_COMMAND_DONE_ERROR(flags) == 0) {
                // Fifo needs to be reset before writing
                drvSdioResetFIFO();             
                // Get Descriptors base address
                descriptors_base_address = GET_REG_WORD_VAL(SDIOH1_DBADDR_S1);
                // Create the necessary number of descriptors (depending on data size)  
                resp = drvSdioBuildChainedDescriptor(descriptors_base_address, transactionList, count, drvsdioCards[cardslot].blockSize);               
                // Support for SD cards
                if (drvsdioCards[cardslot].sdhc == 0) {
                    sdblocknumber *= drvsdioCards[cardslot].blockSize;                  
                }
                // Work out the number of blocks
                resp = drvSdioGetScatteredTransactionSize(transactionList,count) / drvsdioCards[cardslot].blockSize;                    
                if (resp == 0x01)
                {
                    // If descriptor is only one then perform Single block write
                    resp = drvSdioStartSingleBlockWrite(sdblocknumber, cardslot,&flags);
                }
                else {                  
                    // If descriptors are more than one then perform Multiple block write
                    resp = drvSdioStartMultipleBlockWrite(sdblocknumber, resp, cardslot, &flags);           
                }               

            }           
        }           
        // Deselect the card
        resp = drvSdioCardDeSelect(cardslot);           
    }
    *errorflags = flags;
    if (DRVSDIO_VERIFY_WR_BLOCK_RESULT(flags) != TRUE)      
        resp = DRVSDIO_ERROR;
    else
        resp  = DRVSDIO_SUCCESS;    
    DPRINTF1("\nDrvSdioWriteDataBlockList Block 0x%x BlockSize 0x%x Data Size 0x%x ErrorFlags 0x%x Res 0x%x descriptors_base_address 0x%x\n", sdblocknumber, drvsdioCards[cardslot].blockSize, drvSdioGetScatteredTransactionSize(transactionList,count), flags, resp, descriptors_base_address);       
    //printf("\nDrvSdioWriteDataBlockList Block 0x%x BlockSize 0x%x Data Size 0x%x ErrorFlags 0x%x Res 0x%x descriptors_base_address 0x%x\n", sdblocknumber, drvsdioCards[cardslot].blockSize, drvSdioGetScatteredTransactionSize(transactionList,count), flags, resp, descriptors_base_address);

    return resp;
}

DRV_RETURN_TYPE DrvSdioReadDataBlockList(u32 sdblocknumber, tyDrvSdio_Transaction *transactionList, u32 count, u32 cardslot, u32 *errorflags)
{
    u32 flags = 0, resp = 0, descriptors_base_address = 0;  
    assert(cardslot < DRVSDIO_MAXDRIVERCARDS);      
    // Select a card for reading
    resp = drvSdioCardSelect(drvsdioCards[cardslot].relativeCardAddress, cardslot, &flags);                 
    // Simple error flags check 
    if (DRVSDIO_ISTHERE_COMMAND_DONE_ERROR(flags) == 0) {
        // Set bus width
//        resp = drvSdioSelectBusWidth(drvsdioCards[cardslot].busWidth, drvsdioCards[cardslot].relativeCardAddress, cardslot, &flags);
        // Simple error flags check
        if (DRVSDIO_ISTHERE_COMMAND_DONE_ERROR(flags) == 0) {
            // Set block length
            resp = drvSdioSetBlockLength(drvsdioCards[cardslot].blockSize, drvSdioGetScatteredTransactionSize(transactionList,count), cardslot, &flags);
            // Simple error flags check
            if (DRVSDIO_ISTHERE_COMMAND_DONE_ERROR(flags) == 0) {
                // Fifo needs to be reset before reading
                drvSdioResetFIFO();                                     
                // Get Descriptors base address
                descriptors_base_address = GET_REG_WORD_VAL(SDIOH1_DBADDR_S1);
                // Create the necessary number of descriptors (depending on data size)
                resp = drvSdioBuildChainedDescriptor(descriptors_base_address, transactionList, count, drvsdioCards[cardslot].blockSize);                   
                // Support for SD cards
                if (drvsdioCards[cardslot].sdhc == 0)
                    sdblocknumber *= drvsdioCards[cardslot].blockSize;
                // Work out the number of blocks
                resp = drvSdioGetScatteredTransactionSize(transactionList,count) / drvsdioCards[cardslot].blockSize;                    
                if (resp == 0x01){
                    // If descriptor is only one then perform Single block read
                    resp = drvSdioStartSingleBlockRead(sdblocknumber, cardslot,&flags);
                }
                else{                   
                    // If descriptors are more than one then perform Multiple block read
                    resp = drvSdioStartMultipleBlockRead(sdblocknumber, resp, cardslot, &flags);                    
                }               
            }           
        }           
        // Deselect the card
        resp = drvSdioCardDeSelect(cardslot);               
    }



    *errorflags = flags;
    if (DRVSDIO_VERIFY_RD_BLOCK_RESULT(resp) != TRUE)
        resp = DRVSDIO_ERROR;
    else
        resp = DRVSDIO_SUCCESS;



    //printf("\nDrvSdioReadDataBlockList List Length %d Block 0x%x Block Size 0x%x DataSize 0x%x ErrorFlags 0x%x Resp 0x%x descriptors_base_address 0x%x\n", count, sdblocknumber, drvsdioCards[cardslot].blockSize, drvSdioGetScatteredTransactionSize(transactionList,count), flags, resp, descriptors_base_address);

    return resp;
}

DRV_RETURN_TYPE DrvSdioWriteDataBlock(u32 sdblocknumber, u32 sourceaddress, u32 datasize, u32 cardslot, u32 *errorflags)
{
    tyDrvSdio_Transaction transactionList[1];
    transactionList[0].size = datasize;
    transactionList[0].buffer = sourceaddress;
    return DrvSdioWriteDataBlockList(sdblocknumber, &transactionList[0], 1, cardslot, errorflags);  
}

DRV_RETURN_TYPE DrvSdioReadDataBlock(u32 destaddress, u32 sdblocknumber, u32 datasize, u32 cardslot, u32 *errorflags)
{
    tyDrvSdio_Transaction transactionList[1];
    transactionList[0].size = datasize;
    transactionList[0].buffer = destaddress;
    return DrvSdioReadDataBlockList(sdblocknumber, &transactionList[0], 1, cardslot, errorflags);
}

DRV_RETURN_TYPE DrvSdioEraseDataBlock(u32 firstblock, u32 lastblock, u32 cardslot, u32 *errorflags)
{
    u32 flags = 0;
    assert(cardslot < DRVSDIO_MAXDRIVERCARDS);
    // Select a card
    drvSdioCardSelect(drvsdioCards[cardslot].relativeCardAddress, cardslot, &flags);
    // Check for errors 
    if (DRVSDIO_ISTHERE_COMMAND_DONE_ERROR(flags) == 0) {
        // Perform Block erase  
        drvSdioEraseBlocks(firstblock, lastblock, cardslot, &flags);    
        if (DRVSDIO_ISTHERE_COMMAND_DONE_ERROR(flags) == 0) {                           
            flags |= drvSdioCardDeSelect(cardslot); 
        }
    }
    *errorflags = flags;
    if (DRVSDIO_ISTHERE_COMMAND_DONE_ERROR(flags) != 0) {
        return DRVSDIO_ERROR;
    }
    DPRINTF1("\nDrvSdioEraseDataBlock Start Block 0x%x Last Block 0x%x CardSlot %d Error Flags 0x%x Resp 0x%x\n", firstblock, lastblock, cardslot, flags, flags);
    return DRVSDIO_SUCCESS; 
}

/// Program FIFO 
///
///
///
/// @param[in] dma_Msize Burst size of multiple transaction
/// @param[in] rx_wmark FIFO threshold watermark level when receiving data to card
/// @param[in] tx_wmark FIFO threshold watermark level when transmitting data to card
/// @return none
static void drvSdioProgramFIFO(u32 dma_Msize, u32 rx_wmark, u32 tx_wmark)
{
   u32 write_val;   

   write_val = (((dma_Msize & DRVSDIO_FIFOTH_DMA_MULTIPLETRAN_MASK) << DRVSDIO_FIFOTH_DMA_MULTIPLETRAN) | 
                ((rx_wmark & DRVSDIO_FIFOTH_RX_WATERMARK_MASK) << DRVSDIO_FIFOTH_RX_WATERMARK) | 
                ((tx_wmark << DRVSDIO_FIFOTH_TX_WATERMARK) & DRVSDIO_FIFOTH_TX_WATERMARK_MASK));   
   SET_REG_WORD(SDIOH1_FIFOTH_S1, write_val);
   DPRINTF1("\ndrvSdioProgramFIFO DMA Size 0x%x RXWM 0x%x TXWM 0x%x FIFOTH 0x%x\n", dma_Msize, rx_wmark, tx_wmark, write_val);
}

DRV_RETURN_TYPE DrvSdioGetCardBlockNumber(u32 cardslot, u64 *cardblocknum)
{
    u64 capacity = 0;
    u32 res = 0;

    assert(cardslot < DRVSDIO_MAXDRIVERCARDS);
    if (drvsdioCards[cardslot].relativeCardAddress) {
        // Get the capacity from the CSD card register
        res = DrvSdioGetCardCapacity(cardslot, &capacity);
        capacity = capacity >> 9;
    }   
    else {
        DPRINTF1("\nNo relative card address assigned. Card is probably not initialized\n");
        res = DRVSDIO_ERROR;
    }
    DPRINTF1("\nDrvSdioGetCardBlockNumber CardSlot %d Res %llu\n", cardslot, capacity);
    *cardblocknum = capacity;
    return res;
}

#ifdef MMC_CODE
DRV_RETURN_TYPE DrvSdioGetCardCapacity(u32 cardslot, u64 *cardcapacity)
{
    (void)cardslot;
    u64 blocks = 3751900;
    *cardcapacity =  (u64)blocks << (u64)9;
     return DRVSDIO_SUCCESS;
}
#else
DRV_RETURN_TYPE DrvSdioGetCardCapacity(u32 cardslot, u64 *cardcapacity)
{
    u64 res = 0;
    u8 *pointer = NULL;

    assert(cardslot < DRVSDIO_MAXDRIVERCARDS);
    if (drvsdioCards[cardslot].relativeCardAddress) {
        // Get the capacity from the CSD card register
        pointer = (u8 *) drvsdioCards[cardslot].csd;
        res = (DRVSDIO_CSD_1_GET_C_SIZE(pointer) + 1) * 512 * 1024;     
    }   
    else {
        DPRINTF1("\nNo relative card address assigned. Card is probably not initialized\n");
        return DRVSDIO_ERROR;
    }
    DPRINTF1("\nDrvSdioGetCardCapacity Cardslot %d Capacity %llu\n", cardslot, res);
}
#endif
DRV_RETURN_TYPE DrvSdioGetCardTransferSpeed(u32 cardslot, u32 *cardtransferspeed)
{
    u32 s = 0, e = 0, m = 0;
    u8 *pointer = NULL;

    assert(cardslot < DRVSDIO_MAXDRIVERCARDS);
    if (drvsdioCards[cardslot].relativeCardAddress) {
        // Get the value of the CSD card register
        pointer = (u8 *) drvsdioCards[cardslot].csd;
        // Parse the Card transfer speed
        s = DRVSDIO_CSD_GET_TRAN_SPEED(pointer);
        e = s & 0x7;
        m = s >> 3; 
        switch (e) {
            case 0: s = 10000; break;
            case 1: s = 100000; break;
            case 2: s = 1000000; break;
            case 3: s = 10000000; break;
            default: s = 0; break;
        }
        switch (m) {
            case 1: s *= 10; break;
            case 2: s *= 12; break;
            case 3: s *= 13; break;
            case 4: s *= 15; break;
            case 5: s *= 20; break;
            case 6: s *= 25; break;
            case 7: s *= 30; break;
            case 8: s *= 35; break;
            case 9: s *= 40; break;
            case 10: s *= 45; break;
            case 11: s *= 50; break;
            case 12: s *= 55; break;
            case 13: s *= 60; break;
            case 14: s *= 70; break;
            case 15: s *= 80; break;
            default: s *= 0; break;
        }
    }
    else {
        DPRINTF1("\nNo relative card address assigned. Card is probably not initialized\n");
        return DRVSDIO_NOT_INITIALIZED;
    }
    s = s / 1000;
    DPRINTF1("\nDrvSdioGetCardTransferSpeed CardSlot %d Res %d Khz \n", cardslot, s);
    *cardtransferspeed = s;
    return DRVSDIO_SUCCESS;
}

DRV_RETURN_TYPE DrvSdioGetCardAccessTime(u32 cardslot, u32 *cardaccesstime)
{
    u32 ac = 0, e = 0, m = 0;
    u8 *pointer = NULL;

    assert(cardslot < DRVSDIO_MAXDRIVERCARDS);
    if (drvsdioCards[cardslot].relativeCardAddress){
        // Get the value of the CSD card register
        pointer = (u8 *) drvsdioCards[cardslot].csd;
        // Parse the Card access time
        ac = DRVSDIO_CSD_GET_TAAC(pointer);
        e = ac & 0x7;
        m = ac >> 3;
        switch (e) {
            case 0: ac = 1; break;
            case 1: ac = 10; break;
            case 2: ac = 100; break;
            case 3: ac = 1000; break;
            case 4: ac = 10000; break;
            case 5: ac = 100000; break;
            case 6: ac = 1000000; break;
            case 7: ac = 10000000; break;
            default: ac = 0; break;
        }
        switch (m) {
            case 1: ac *= 10; break;
            case 2: ac *= 12; break;
            case 3: ac *= 13; break;
            case 4: ac *= 15; break;
            case 5: ac *= 20; break;
            case 6: ac *= 25; break;
            case 7: ac *= 30; break;
            case 8: ac *= 35; break;
            case 9: ac *= 40; break;
            case 10: ac *= 45; break;
            case 11: ac *= 50; break;
            case 12: ac *= 55; break;
            case 13: ac *= 60; break;
            case 14: ac *= 70; break;
            case 15: ac *= 80; break;
            default: ac *= 0; break;
        }
        ac = ac/10;
    }
    else {
        DPRINTF1("\nNo relative card address assigned. Card is probably not initialized\n");
        return DRVSDIO_NOT_INITIALIZED;
    }
    DPRINTF1("\nDrvSdioGetCardAccessTime CardSlot %d Res %d \n", cardslot, ac); 
    *cardaccesstime = ac;
    return DRVSDIO_SUCCESS;
}

DRV_RETURN_TYPE DrvSdioGetCardMaxAccessTime(u32 cardslot, u32 transfer_speed, u32 *cardmaxaccesstime)
{
    u64 ac = 0;
    u32 n = 0, res = 0, maxacctime = 0;
    u8 *pointer = NULL;
    u32 ret = 0;

    assert(cardslot < DRVSDIO_MAXDRIVERCARDS);
    if (drvsdioCards[cardslot].relativeCardAddress){
        // Get the value of the CSD card register
        pointer = (u8 *) drvsdioCards[cardslot].csd;
        // Parse the Card access time
        ret = DrvSdioGetCardAccessTime(cardslot, &maxacctime);
        if (ret == DRVSDIO_SUCCESS)
        {
            ac = maxacctime;
            n = DRVSDIO_CSD_GET_NSAC(pointer);
            ac = (ac * transfer_speed) / 8000000000ULL;
            res = n + (u32) ac;
        }
    }
    else {
        DPRINTF1("\nNo relative card address assigned. Card is probably not initialized\n");
        ret = DRVSDIO_NOT_INITIALIZED;
    }
    DPRINTF1("\nDrvSdioGetCardAccessTime Cardslot %d Transfer Speed %d Res %d \n", cardslot, transfer_speed, res);
    *cardmaxaccesstime = res;
    return ret; 
}


// ISR in charge of handling SD interrupts and Internal DMA 
static void drvSdioIrq_Handler(u32 source)
{   
    UNUSED(source); // hush the compiler warning.

    u32 tmp = GET_REG_WORD_VAL(SDIOH1_RINTSTS_S1);
    DrvSdio_RInterrupts |= tmp; 
    SET_REG_WORD(SDIOH1_RINTSTS_S1, tmp);
    GET_REG_WORD_VAL(SDIOH1_RINTSTS_S1); // make sure that the clearing of the interrupt has completed before we clear the ICB level-sensitive interrupt bit
    // Clear the interrupt
    DrvIcbIrqClear(IRQ_SDIO);
}


DRV_RETURN_TYPE DrvSdioSetupIrq(u32 interruptPriority)
{
    /* Install interrupt handler */
    DrvIcbSetIrqHandler(IRQ_SDIO, drvSdioIrq_Handler); 
    DrvIcbConfigureIrq( IRQ_SDIO, interruptPriority, POS_LEVEL_INT);
    DrvIcbEnableIrq(IRQ_SDIO);  
    return DRVSDIO_SUCCESS;
}

void DrvSdioReset(u32 hardware)
{   
    if (hardware) {
        // Set back the Control register
        SET_REG_WORD(SDIOH1_CTRL_S1, (1 << DRVSDIO_CTRL_FIFO_RESET) | (1 << DRVSDIO_CTRL_DMA_RESET) | (1 << DRVSDIO_CTRL_CONTROLLER_RESET));
        // Wait for reset to be performed 
        while(GET_REG_WORD_VAL(SDIOH1_CTRL_S1) & ((1 << DRVSDIO_CTRL_FIFO_RESET) | (1 << DRVSDIO_CTRL_DMA_RESET) | (1 << DRVSDIO_CTRL_CONTROLLER_RESET)));
        // Delay just in case 
        drvSdiodelay(200000);
    }
    // Reset all control structures
    memset(&drvsdioCards, 0x00, sizeof(drvsdioCards));
    memset(&tyDrvSdio_Driver, 0x00, sizeof(tyDrvSdio_Driver));
    DrvSdio_RInterrupts = 0;
    DPRINTF1("\nDrvSdioReset\n");
}
            
DRV_RETURN_TYPE DrvSdioInit(u32 cclk_in, u32 descriptors_base_address)
{
    u32 tmp = 0;
        
    if ((tyDrvSdio_Driver.status & (1 << DRVSDIO_INTERNALSD_STATUS_FREQ_SWITCHED)) == 0) {
        if ((cclk_in == 0) || (cclk_in > DRVSDIO_MAXINPUTFREQUENCYKHZ) || (descriptors_base_address == 0))
        {
            DPRINTF1("\nDrvSdioInit FAIL. \nBad clock frequency or descriptors address\n");
            return DRVSDIO_ERROR;
        }
        // Make sure descriptors base address is not passed bypassed
        if (descriptors_base_address & DRV_DDRMASK)
            descriptors_base_address &= ~DRV_DDRBYPASSMASK;
        else
            descriptors_base_address &= ~DRV_CMXBYPASSMASK;
        tyDrvSdio_Driver.cclk_in_khz = cclk_in;
        DPRINTF1("Configuring cclk_in %d\n", cclk_in);      
        /* Enable power to cards, PWREN reg  - enable power to all cards */
        SET_REG_WORD(SDIOH1_PWREN_S1, DRVSDIO_MAXNUMCARDS_MASK);
        /* Access raw interrupt register to clear any pending interrupt */
        SET_REG_WORD(SDIOH1_RINTSTS_S1, DRVSDIO_RAWINTSTATUS_ALL);  
        /* Set global interrupt enable bit 0x00000010 */
        SET_REG_WORD(SDIOH1_CTRL_S1, (1 << DRVSDIO_CTRL_INT_ENABLE));
        // Work out the value for enumeration process 
        tmp = DRVSDIO_GET_DIV_VALUE(cclk_in, DRVSDIO_STARTFREQ);
        DPRINTF1("Div:%d\n",tmp);
        printf("Div:%d\n",tmp);
        /* Reduce operating frequency to near 400Khz for Div 0, the only one being used  */ 
        SET_REG_WORD(SDIOH1_CLKDIV_S1, (tmp << DRVSDIO_CLKSOURCE_DIV0));        
        /* Clock source dedicated to each card. 00 Div 0, 01 Div 1, 10 Div 2 and 11 Div 3. MMC-Ver3.3-only controller, only Div 0 is enabled. 
        Select Div 0 for all cards */
        SET_REG_WORD(SDIOH1_CLKSRC_S1, 0x00000000);
        /* Enable clock for all 16 cards and Non-low-power-mode */
        SET_REG_WORD(SDIOH1_CLKENA_S1, DRVSDIO_CLKENA_ENABLE_ALL);
        /* For SDIOH1_CLKDIV_S1, SDIOH1_CLKDIV_S1 and SDIOH1_CLKENA_S1 to be loaded bits CMD_START_CMD and CMD_UPDATE_CLK_REG must
        be set in CMD register 0x80200000*/
        drvSdioLoadCommand((1 << DRVSDIO_CMD_START_CMD) | (1 << DRVSDIO_CMD_UPDATE_CLK_REG), FALSE, 0);    
        /* Program Data timeout and response timeout Using default values 0xFFFFFF, 0x40 . 0xFFFFFF40*/ 
        SET_REG_WORD(SDIOH1_TMOUT_S1, (DRVSDIO_TMOUT_DEFAULT_DATATIMEOUT << DRVSDIO_TMOUT_DATATIMEOUT) | (DRVSDIO_TMOUT_DEFAULT_RESPONSETIMEOUT << DRVSDIO_TMOUT_RESPONSETIMEOUT));
        // By default 4 bit bus for all cards
        SET_REG_WORD(SDIOH1_CTYPE_S1, DRVSDIO_CTYPE_SD0_SD15_4BITS);
        /* Program FIFO threshold to default values and DMA_SIZE burst size for multiple transactions to 1 (The same as DW-DMA controller multiple-transaction-size) */
        drvSdioProgramFIFO(DRVSDIO_DMASIZE, DRVSDIO_RX_WMARK, DRVSDIO_TX_WMARK);    
        /* Set CTRL_INT_ENABLE and CTRL_USE_IDMAC in CTRL, 0x02000010 */
//        SET_REG_WORD(SDIOH1_CTRL_S1, (1 << DRVSDIO_CTRL_USE_IDMAC) | (1 << DRVSDIO_CTRL_INT_ENABLE) | (1 << DRVSDIO_CTRL_SEND_AUTO_STOP_CCSD) | (1 << DRVSDIO_CTRL_SEND_CCSD));
        SET_REG_WORD(SDIOH1_CTRL_S1, (1 << DRVSDIO_CTRL_USE_IDMAC) | (1 << DRVSDIO_CTRL_INT_ENABLE));

        /* Program IDMAC */ 
        /* Set PBL with same value as FIFO DMA Transaction Size DRVSDIO_DMASIZE */
        tmp = ((1 << DRVSDIO_BMOD_FB) | (1 << DRVSDIO_BMOD_DE) | (DRVSDIO_DMASKIP << DRVSDIO_BMOD_DSL) | (DRVSDIO_DMASIZE << DRVSDIO_BMOD_PBL));        
        /* 0 * HWORD/WORD/DWORD to be skipped between 2 unchained descriptors == dmac_skip_length, IDMAC enable and Fixed bursts */ 
        SET_REG_WORD(SDIOH1_BMOD_S1, tmp);
        /* Enable all interrupts except Receive/Transmit FIFO data request, 0x0000ffcf */   
        tmp = (DRVSDIO_INTMASK_EVENTALL & ~((1 << DRVSDIO_INTMASK_RXDR) | (1 << DRVSDIO_INTMASK_TXDR)));    
        SET_REG_WORD(SDIOH1_INTMASK_S1, tmp);   
        /* Program descriptor base address aligned to 32 - bit */
        SET_REG_WORD(SDIOH1_DBADDR_S1, descriptors_base_address & 0xFFFFFFFC);
        /* Disable all interrupts for the Internal DMAC */
        SET_REG_WORD(SDIOH1_IDINTEN_S1, 0x00);
        /* Enable Busy Interrupt !!! */
        /* Enable Card Read Threshold and set its value */
        SET_REG_WORD(SDIOH1_CARDTHRCTL_S1,((1 << DRVSDIO_CARDTHRCTL_BSYCLRINTEN) | (1 << DRVSDIO_CARDTHRCTL_CARDRDTHREN) | (DRVSDIO_CARDTHRCTL_CARDRDTHRESHOLD_MASK << DRVSDIO_CARDTHRCTL_CARDRDTHRESHOLD)));
        /* Clock Shift */   
        tmp = GET_REG_WORD_VAL(SDIOH1_UHS_REG_EXT_S1);
        tmp |= ((DRVSDIO_DEFAULT_CLK_DRV_PHASE_CTRL << DRVSDIO_UHS_REG_EXT_CLK_DRV_PHASE_CTRL) | (DRVSDIO_DEFAULT_CLK_SMPL_PHASE_CTRL << DRVSDIO_UHS_REG_EXT_CLK_SMPL_PHASE_CTRL)); 
        SET_REG_WORD(SDIOH1_UHS_REG_EXT_S1, tmp);
        /* DELAY TO BE REPLACED BY A PROPER ROUTINE */
        drvSdiodelay(100*cclk_in);        
        /* Reset all cards */   
        //SET_REG_WORD(SDIOH1_RST_S1, DRVSDIO_RST_N_RESETCARDALL);
		DrvGpioSetPinLo(79);/*change by guanxing, 由于迈外迪项目sdio中reset管脚硬件设计为79，暂时不考虑正常sdio管脚的情况*/
        /* DELAY TO BE REPLACED BY A PROPER ROUTINE */    
        drvSdiodelay(40*cclk_in);
        /* Activate all the cards   */  
        //SET_REG_WORD(SDIOH1_RST_S1, DRVSDIO_RST_N_ACTIVECARDALL);
		DrvGpioSetPinHi(79);/*change by guanxing, 由于迈外迪项目sdio中reset管脚硬件设计为79，暂时不考虑正常sdio管脚的情况*/
        tyDrvSdio_Driver.status |= (1 << DRVSDIO_INTERNALSD_STATUS_FREQ_SWITCHED);          
        DPRINTF1("\nDrvSdioInit Done\n");
    } 
    else {
        DPRINTF1("\nDrvSdioInit FAIL\n");
        return DRVSDIO_ERROR;
    }   
    return DRVSDIO_SUCCESS;
}

/// Switch to High-Speed mode
///
///
/// First check if the card is spec version compatible. 
/// After that check if the card supports switch to High-Speed function and if availabel
/// perform the switch.
///
///
/// @param[in] cardslot This is the card number
/// @param[in] errorflags The value of the RINTSTS (Raw Interrupt Status Register)
/// @return none

#ifndef MMC_CODE // hush the compiler warning.
static void drvSdioSwitchToHighSpeedMode(u32 cardslot, u32 *errorflags)
{
    u32 command = 0, arg = 0, flags = 0;
    u32 resp = 0; UNUSED(resp);
    resp = drvSdioCardSelect(drvsdioCards[cardslot].relativeCardAddress, cardslot, &flags);

    //  Mode 1 Operation - Set Function
    DPRINTF1("\nCMD6\n");
    // Prepare CMD6
    command = ((1 << DRVSDIO_CMD_START_CMD) | (DRVSDIO_CMD6 << DRVSDIO_CMD_CMD_INDEX) | (1 << DRVSDIO_CMD_RESP_EXPECT) | (1 << DRVSDIO_CMD_CHECK_RESP_CRC) | (1 << DRVSDIO_CMD_WAIT_PREVDATA_CMPL));
    // Set args for CMD6
    // Set bit 31 to 1 to perform Set function; set 0 to the reserved bits; set F to the not changed groupd; set 1 for bit 1 to enable High-Speed mode
    arg = 0x80FFFFF1;
    // Load Command
    resp = drvSdioLoadAndWait_ShortResponse(command, arg, TRUE, cardslot, &flags);
    // Wait for a while after performing the command
    drvSdiodelay(20*SYSTEMFREQ_CONFIG);
    if (DRVSDIO_ISTHERE_COMMAND_DONE_ERROR(flags) != 0) {
        DPRINTF1("\nCMD6 Fail to switch to High-Speed mode\n");
    }
    DPRINTF1("\nCMD6 2 flags 0x%x   resp 0x%x\n", flags, resp);
    // Deselect the card (return to previous state)
    resp = drvSdioCardDeSelect(cardslot);
    *errorflags = flags;
}
#endif

