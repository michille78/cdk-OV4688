///  
/// @file
/// @copyright All code copyright Movidius Ltd 2012, all rights reserved 
///            For License Warranty see: common/license.txt              
///
/// @brief     Definitions and types needed by GPIO Driver
/// 
#ifndef DRV_GPIO_DEF_H
#define DRV_GPIO_DEF_H 

#include "mv_types.h"
#include "registersMyriad.h"


#if defined(MA2150) || defined(MA2155) || defined(MA2450) || defined(MA2455)
 #include "DrvGpioDefinesMa2x5x.h"
#endif

#endif //DRV_GPIO_DEF_H 

