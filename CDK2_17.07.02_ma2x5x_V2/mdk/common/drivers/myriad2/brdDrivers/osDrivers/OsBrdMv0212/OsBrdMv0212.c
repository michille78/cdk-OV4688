/// @file
/// @copyright All code copyright Movidius Ltd 2014, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     rtems driver for board mv0212
///
#include <OsBrdMv0212.h>
#include <OsDrvGpio.h>
#include <OsDrvI2cBus.h>
#include <OsDrvCpr.h>
#include <brdGpioCfgs/brdMv0212GpioDefaults.h>
#include <rtems.h>
#include <OsDrvI2cBus.h>
#include <OsDrvI2cMyr2.h>

#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <rtems.h>
#include <rtems/libio.h>

// Driver dependencies
#include <OsWm8325.h>
#include <OsCDCEL.h>
#include <OsADV7513.h>
#include <OsEEPROM.h>

#if 0
#define MV212PRINTK(...) printk(__VA_ARGS__)
#else
#define MV212PRINTK(...)
#endif

#if 0
#define MV212PRINTF(...) printf(__VA_ARGS__)
#else
#define MV212PRINTF(...)
#endif
// 1: Includes
// ----------------------------------------------------------------------------

// 2:  Source Specific #defines and types  (typedef,enum,struct)
// ----------------------------------------------------------------------------
//used to recover i2c2 bus sda pin
#define I2C2_SDA_GPIO                           (80)
#define I2C2_SCL_GPIO                           (79)
#define RECOVERY_TRIES                          (100) // Try 100 SCL clocks to recover SDA stuck low
#define NUM_CLOCKS_I2C_RESET                    (16)  // Try at least 16 clocks to recover SDA stuck low

// Read size for EEPROM
#define EEPROM_READ_SIZE                        0x23
// EEPROM offset to read the PCB revision
#define EEPROM_PCB_REVISION_OFFSET              0x0D
// I2C address
#define EEPROM_SLAVE_ADDRESS                    0x50
// EEPROM device name
#define EEPROM_NAME                             "24AA32A"

#define GPIO_HIGH               1
#define GPIO_LOW                0

// Default I2C Bus name for LibI2C
#define DEFAULT_I2C_BUSNAME                      "/dev/i2c"

// Maximum number of buses to describe devices
#define MAX_I2C_BUSES_PER_DEV                   (3)
#define MAX_I2C_ADDRESSES_PER_DEV               (8)

typedef enum
{
    DRV_OS_BRD_212_UNINITIALIZED  = 0,         // Driver status not initialized
    DRV_OS_BRD_212_INITIALIZED,                // Driver initialized
    DRV_OS_BRD_212_INITIALIZATION_IN_PROGRESS, // Driver opened, ready to be used
} tyOsBrd212StatusCode;

typedef struct
{
    const char* name;
    const rtems_libi2c_drv_t* protocolDesc;
    int32_t busIds[MAX_I2C_BUSES_PER_DEV];
    uint16_t addresses[MAX_I2C_ADDRESSES_PER_DEV];
}tyOsBrd212DevList;

typedef int tyOsI2cbusHandlers;

// 3: Global Data (Only if absolutely necessary)
// ----------------------------------------------------------------------------


// 4: Static Local Data
// ----------------------------------------------------------------------------
static tyOsI2cbusHandlers i2cbusHandlers[3];

/// check DECLARE_I2C_BUS description before calling
DECLARE_I2C_BUS(myr2_mv212_i2c_0, 1, I2C_SPEED_FS, 0, 8);
DECLARE_I2C_BUS(myr2_mv212_i2c_1, 2, I2C_SPEED_FS, 0, 8);
DECLARE_I2C_BUS(myr2_mv212_i2c_2, 3, I2C_SPEED_FS, 0, 8);

tyOsBrd212DevList brdMv212i2cDeviceListFirstPhase[] =
{
    {
        .name = WM8325_NAME,
        .protocolDesc = &wm8325ProtocolDrvTbl,
        .busIds = {2, -1, -1},
        .addresses = {WM8325_SLAVE_ADDR, 0, 0, 0, 0, 0, 0},
    },
    {
        .name = NULL,
        .protocolDesc = NULL,
        .busIds = {-1, -1, -1},
        .addresses = {0, 0, 0, 0, 0, 0, 0, 0},
    }
};

tyOsBrd212DevList brdMv212i2cDeviceListSecondPhase[] =
{
    {
        .name = "imx208_right",
        .protocolDesc = &genericProtocolDrvTbl,
        .busIds = {0, -1, -1},
        .addresses = {(0x6c >> 1), 0, 0, 0, 0, 0, 0},
    },
    {
        .name = "imx208_left",
        .protocolDesc = &genericProtocolDrvTbl,
        .busIds = {0, -1, -1},
        .addresses = {(0x6e >> 1), 0, 0, 0, 0, 0, 0},
    },
    {
        .name = CDCEL_NAME,
        .protocolDesc = &cdcelProtocolDrvTbl,
        .busIds = {2, -1, -1},
        .addresses = {CDCEL913_SLAVE_ADDR, CDCEL925_SLAVE_ADDR, 0, 0, 0, 0, 0},
    },
    {
        .name = ADV7513_NAME,
        .protocolDesc = &adv7513ProtocolDrvTbl,
        .busIds = {2, -1, -1},
        .addresses = {ADV7513_SLAVE_ADDRESS, 0, 0, 0, 0, 0, 0},
    },
    {
        .name = EEPROM_NAME,
        .protocolDesc = &eepromDrvTbl,
        .busIds = {2, -1, -1},
        .addresses = {EEPROM_SLAVE_ADDRESS, 0, 0, 0, 0, 0, 0},
    },
    {
        .name = NULL,
        .protocolDesc = NULL,
        .busIds = {-1, -1, -1},
        .addresses = {0, 0, 0, 0, 0, 0, 0, 0},
    }
};
// 5: Static Function Prototypes
// ----------------------------------------------------------------------------
static tyOsBrd212ErrorCode osBrdMv0212InitialiseI2C();
static tyOsBrd212ErrorCode osBrdMv0212InitialiseGpioDriver();

// 6: Functions Implementation
// ----------------------------------------------------------------------------

static int rtemsGetMajorMinor(const char * name,
                              rtems_device_major_number *major,
                              rtems_device_minor_number *minor)
{
    rtems_status_code sc = RTEMS_SUCCESSFUL;
    struct stat st;
    sc = stat(name, &st);
    if (sc)
    {
        return -sc;
    }

    if(S_ISCHR( st.st_mode ))
    {
        if (major)
            *major = rtems_filesystem_dev_major_t( st.st_rdev );
        if (minor)
            *minor = rtems_filesystem_dev_minor_t( st.st_rdev );
        return sc;
    }
    else
    {
        return RTEMS_UNSATISFIED;
    }
}

// EEPROM CRC Checking function
static u32 eepromCRCCheck(u8 *src, u32 size)
{
    u32 sum = 0, index  = 0;

    for(index = 0;index < size;index++)
        sum = sum + *(src+index);
    sum = sum & 0xFF;
    return sum;
}

// Read all EEPROM data and verify CRC
static s32 readAllEEPROMData(const char *devName, u8 *data, u32 size)
{
    int status = 0, fd = 0;
    fd = open(devName, O_RDONLY,
              S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH );
    if (fd < 0)
        return fd;
    // Set offsset at the beginning
    status = lseek(fd, 0, SEEK_SET);
    if (status < 0)
        goto error;
    // Read
    status = read(fd, data, size);
    if (status < 0)
        goto error;
    status = close(fd);
    if (status < 0)
        return status;
    // Check CRC
    if (eepromCRCCheck(data, size) != 0)
        return -1;
    else
        return 0;
error:
    close(fd);
    return status;
}

// Retrieve PCB Revision
static rtems_status_code readPCBRevisionFromEEPROM(char *devName, u32 readSize, u32 revOffset, u32 *rev)
{
    rtems_status_code sc = RTEMS_SUCCESSFUL;

    u8 *buff = (u8 *) malloc(readSize);

    if (!buff)
        return RTEMS_NO_MEMORY;

    memset(buff, 0, readSize);

    // Read and verify
    if (readAllEEPROMData(devName, buff, readSize) == 0)
        *rev = buff[revOffset];

    // deallocate memory
    free(buff);
    return sc;
}

static tyOsBrd212ErrorCode osBrdMv0212RegisterI2CDevices(tyOsBrd212DevList* i2cDeviceList)
{
    MV212PRINTF("Registering i2c devices");
    uint32_t devInd = 0;
    int32_t i2cDrvMinor;
    tyOsBrd212DevList* device = &i2cDeviceList[devInd];
    while(device->name != NULL)
    {
        MV212PRINTF("Registering i2c device %s\n", device->name);
        int32_t i = 0, j = 0;
        while (i < MAX_I2C_BUSES_PER_DEV && device->busIds[i] != -1)
        {
            j = 0;
            while (j < MAX_I2C_ADDRESSES_PER_DEV && device->addresses[j] != 0)
            {
                MV212PRINTF("Polling address 0x%x on bus %d\n", device->addresses[j], device->busIds[i]);
                i2cDrvMinor = rtems_libi2c_register_drv(device->name,
                                                        (rtems_libi2c_drv_t*) device->protocolDesc,
                                                        i2cbusHandlers[device->busIds[i]],
                                                        device->addresses[j]);
                if (i2cDrvMinor >= 0)
                {
                    MV212PRINTF("i2c device %s registered at bus %d with address 0x%x\n",
                           device->name, device->busIds[i], device->addresses[j]);
                    break;
                }
                j++;
            }

            if (j < MAX_I2C_ADDRESSES_PER_DEV && device->addresses[j] != 0)
            {
                // The device was found. No need to poll it on the other buses
                break;
            }
            i++;
        }
        if (i >= MAX_I2C_BUSES_PER_DEV || device->busIds[i] == -1 )
        {
            // This isn't an error, we can use this feature to autodetect devices (i.e  what camera is connected to the board)
            MV212PRINTF("Device not found\n");
        }
        MV212PRINTF("----------------------------------------\n");
        device = &i2cDeviceList[++devInd];
    }
    return DRV_OS_BRD_212_DRV_SUCCESS;
}


static tyOsBrd212ErrorCode osBrdMv0212InitialiseI2C(tyOsBoard0212Configuration* config)
{
    int32_t retCode;
    rtems_status_code sc = RTEMS_SUCCESSFUL;

    // init I2C library
    if ((retCode = OsDrvLibI2CInitialize()) != RTEMS_SUCCESSFUL)
    {
        return DRV_OS_BRD_212_I2C_DRIVER_ERROR;
    }

    int32_t confInd = 0;
    while ((config[confInd].type != MV212_END) && (config[confInd].type != MV212_I2C0_STATE))
        confInd++;
    // if the user didn't specify any config for i2c0, by default it's master.
    if (((config[confInd].type == MV212_I2C0_STATE) && (config[confInd].value == MV212_I2C_MASTER)) || config[confInd].type == MV212_END)
    {
        //register I2C0 bus as master
        retCode = rtems_libi2c_register_bus(DEFAULT_I2C_BUSNAME,(rtems_libi2c_bus_t *) &myr2_mv212_i2c_0);
        if (retCode < 0)
        {
            return DRV_OS_BRD_212_I2C_DRIVER_ERROR;
        }
        else
        {
            i2cbusHandlers[0] = retCode;
        }
    }
    else
    {
        // TODO: Configure i2c0 as slave. Right now the user will have to take care of it.
        MV212PRINTK("WARNING: i2c0 slave mode configuration is not implemented\n");
    }
    retCode = OsDrvLibI2CRegisterBus(DEFAULT_I2C_BUSNAME,(rtems_libi2c_bus_t *) &myr2_mv212_i2c_1);
    if (retCode < 0)
    {
        return DRV_OS_BRD_212_I2C_DRIVER_ERROR;
    }
    else
    {
        i2cbusHandlers[1] = retCode;
    }

    //register I2C2 bus
    retCode = OsDrvLibI2CRegisterBus(DEFAULT_I2C_BUSNAME,(rtems_libi2c_bus_t *) &myr2_mv212_i2c_2);

    if (retCode < 0)
    {
        return DRV_OS_BRD_212_I2C_DRIVER_ERROR;
    }
    else
    {
        i2cbusHandlers[2] = retCode;
    }

    // Register I2C devices first phase
    retCode = osBrdMv0212RegisterI2CDevices(brdMv212i2cDeviceListFirstPhase);

    if (retCode < 0)
    {
        return retCode;
    }
    confInd = 0;
    while ((config[confInd].type != MV212_END) && config[confInd].type != MV212_CAM_B_I2C_BUS)
            confInd++;
    tyOsMv0212CamBBus wmPinVal = MV212_I2C0;

    if (config[confInd].type == MV212_CAM_B_I2C_BUS)
    {
        // if the config was found, set the value requested by user
        wmPinVal = config[confInd].value;
    }
    else
    {
        // if the config was not found, set the default value
        wmPinVal = MV212_I2C0;
    }
    //Set the I2C bus for CAMB. To do this we need to configure WM8325_I2C_SELECT_PIN_NO pin of wm
    rtems_device_major_number wmMajor = 0;
    rtems_device_minor_number wmMinor = 0;
    rtemsGetMajorMinor("/dev/i2c."WM8325_NAME, &wmMajor, &wmMinor);
    sc = rtems_io_open(wmMajor, wmMinor, NULL);
    if(sc)
        return DRV_OS_BRD_212_DRV_ERROR;
    wm8325GpioStruct_t gpioConf;
    rtems_libio_ioctl_args_t ctl;
    ctl.command = WM8325_MODE_SET_GPIO;
    ctl.buffer = (void*) &gpioConf;
    gpioConf.gpioMode = WM8325_OUTPUT;
    gpioConf.gpioNr = WM8325_I2C_SELECT_PIN_NO;
    gpioConf.gpioVal = wmPinVal;
    sc = rtems_io_control(wmMajor, wmMinor, &ctl);
    if(sc)
        return DRV_OS_BRD_212_DRV_ERROR;
    ctl.command = WM8325_SET_GPIO;
    sc = rtems_io_control(wmMajor, wmMinor, &ctl);
    if(sc)
        return DRV_OS_BRD_212_DRV_ERROR;
    sc = rtems_io_close(wmMajor, wmMinor, NULL);
    if(sc)
        return DRV_OS_BRD_212_DRV_ERROR;

    // Register I2C devices second phase
    retCode = osBrdMv0212RegisterI2CDevices(brdMv212i2cDeviceListSecondPhase);

    return retCode;
}

#ifdef NEVER
static tyOsBrd212ErrorCode recoverI2c2SdaLine(tyOsI2cbusHandlers i2cbusHandlers){
    /*
     * After one run, MCP3424 (ADC) can remain in a state that
     * hold down the sda line - when suddenly stopped.
     *  When is tried a second run, the i2c2 bus (sda line)
     *  remain low (adc wait for clock to terminate unfinished transaction) .
     *  This function check and and unlock sda line.
     */

    gpioVal_t sdaPinValue;
    gpioVal_t sclPinValue;
    u8 i;
    rtems_libio_ioctl_args_t ctl_setMode;
    rtems_status_code status;

    // request
    status = rtems_io_open(gpioMajor, I2C2_SDA_GPIO, NULL);
    if (status != RTEMS_SUCCESSFUL)
        return DRV_OS_BRD_212_GPIO_DRIVER_ERROR;

    //read
    status = rtems_io_read(gpioMajor, I2C2_SDA_GPIO, &sdaPinValue);
    if (status != RTEMS_SUCCESSFUL)
        return DRV_OS_BRD_212_GPIO_DRIVER_ERROR;

    //Release
    status = rtems_io_close(gpioMajor, I2C2_SDA_GPIO, NULL);
    if (status != RTEMS_SUCCESSFUL)
        return DRV_OS_BRD_212_GPIO_DRIVER_ERROR;

    if (sdaPinValue == GPIO_LOW)
    {
        for (i = 0; i < RECOVERY_TRIES; i++)
        {
            // request
            status = rtems_io_open(gpioMajor, I2C2_SCL_GPIO, NULL);
            if (status != RTEMS_SUCCESSFUL)
                return DRV_OS_BRD_212_GPIO_DRIVER_ERROR;

            //write low
            sclPinValue = GPIO_LOW;
            status = rtems_io_write(gpioMajor, I2C2_SCL_GPIO, &sclPinValue);
            if (status != RTEMS_SUCCESSFUL)
                return DRV_OS_BRD_212_GPIO_DRIVER_ERROR;

            //write high
            sclPinValue = GPIO_HIGH;
            status = rtems_io_write(gpioMajor, I2C2_SCL_GPIO, &sclPinValue);
            if (status != RTEMS_SUCCESSFUL)
                return DRV_OS_BRD_212_GPIO_DRIVER_ERROR;

            //Release
            status = rtems_io_close(gpioMajor, I2C2_SCL_GPIO, NULL);
            if (status != RTEMS_SUCCESSFUL)
                return DRV_OS_BRD_212_GPIO_DRIVER_ERROR;

            //check sda value
            if(i > NUM_CLOCKS_I2C_RESET)
            {
                // request
                status = rtems_io_open(gpioMajor, I2C2_SDA_GPIO, NULL);
                if (status != RTEMS_SUCCESSFUL)
                    return DRV_OS_BRD_212_GPIO_DRIVER_ERROR;

                //read
                status = rtems_io_read(gpioMajor, I2C2_SDA_GPIO, &sdaPinValue);
                if (status != RTEMS_SUCCESSFUL)
                    return DRV_OS_BRD_212_GPIO_DRIVER_ERROR;

                //Release
                status = rtems_io_close(gpioMajor, I2C2_SDA_GPIO, NULL);
                if (status != RTEMS_SUCCESSFUL)
                    return DRV_OS_BRD_212_GPIO_DRIVER_ERROR;

                if (I2C2_SDA_GPIO != GPIO_LOW)
                    break;
            }
        }

        if(i == RECOVERY_TRIES)
        {
            //Recovery Failed: i2c2 SDA PIN still stuck low
            return DRV_OS_BRD_212_I2C_SLAVE_ERROR;
        }
    }

    //config i2c2 bus lines to i2c mode (mode 2)
    ctl_setMode.command = GPIO_ARRAY_CONFIG;
    ctl_setMode.buffer = &osBrdMv0212RevDetectConfig;

    status = rtems_io_open(gpioMajor, 0, NULL);
    if (status != OS_MYR_DRV_SUCCESS)
    {
        return DRV_OS_BRD_212_GPIO_DRIVER_ERROR;
    }

    status = rtems_io_control(gpioMajor, 0, &ctl_setMode);
    if (status != OS_MYR_DRV_SUCCESS)
    {
        return DRV_OS_BRD_212_GPIO_DRIVER_ERROR;
    }

    status = rtems_io_close(gpioMajor, 0, NULL);
    if (status != OS_MYR_DRV_SUCCESS)
    {
        return DRV_OS_BRD_212_GPIO_DRIVER_ERROR;
    }

    return DRV_OS_BRD_212_DRV_SUCCESS;
}
#endif

static tyOsBrd212ErrorCode osBrdMv0212InitialiseGpioDriver(void)
{
    OS_MYRIAD_DRIVER_STATUS_CODE retCode;
    uint32_t gpioMajor;

    retCode = rtems_io_register_driver( 0, &osDrvGpioTblName, &gpioMajor );
    if (retCode != OS_MYR_DRV_SUCCESS)
    {
        MV212PRINTK("gpio register rc = %d\n", retCode);
        return DRV_OS_BRD_212_GPIO_DRIVER_ERROR;
    }

    retCode = rtems_io_initialize(gpioMajor, 0,
                                  (void*)brdMV0212GpioCfgDefault);
    if (retCode != OS_MYR_DRV_SUCCESS)
    {
        MV212PRINTK("gpio initialzie %d\n", retCode);
        return DRV_OS_BRD_212_GPIO_DRIVER_ERROR;
    }
    return DRV_OS_BRD_212_DRV_SUCCESS;
}

tyOsBrd212ErrorCode OsBoard0212Initialise(tyOsBoard0212Configuration* config, tyOsBoard0212Information *info)
{
    int32_t rv;
    tyOsBrd212ErrorCode returnCode;
    MV212PRINTF("Starting mv0212 board initialize\n");
    static volatile tyOsBrd212StatusCode boardStatus = DRV_OS_BRD_212_UNINITIALIZED;

    if(boardStatus == DRV_OS_BRD_212_UNINITIALIZED)
    {
        if (config == NULL || info == NULL)
            return DRV_OS_BRD_212_DRV_ERROR;

        boardStatus = DRV_OS_BRD_212_INITIALIZATION_IN_PROGRESS;

        // Init CPR Driver in order to be able to get sys clk
        rv = OsDrvCprInit();
        switch (rv)
        {
        case OS_MYR_DRV_SUCCESS:
        case OS_MYR_DRV_ALREADY_INITIALIZED:
        case OS_MYR_DRV_ALREADY_OPENED:
            break;
        default:
            boardStatus = DRV_OS_BRD_212_UNINITIALIZED;
            return DRV_OS_BRD_212_CPR_INIT_ERROR;
        }

        // Open CPR Driver in order to be able to get sys clk
        rv = OsDrvCprOpen();
        switch (rv)
        {
        case OS_MYR_DRV_SUCCESS:
        case OS_MYR_DRV_ALREADY_INITIALIZED:
        case OS_MYR_DRV_ALREADY_OPENED:
            break;
        default:
            boardStatus = DRV_OS_BRD_212_UNINITIALIZED;
            return DRV_OS_BRD_212_CPR_OPEN_ERORR;
        }

        // Initialize and start GPIO driver
        returnCode = osBrdMv0212InitialiseGpioDriver();
        if(returnCode)
        {
            boardStatus = DRV_OS_BRD_212_UNINITIALIZED;
            return returnCode;
        }

        // Initialize I2C busses
        returnCode = osBrdMv0212InitialiseI2C(config);
        if(returnCode != DRV_OS_BRD_212_DRV_SUCCESS)
        {
            boardStatus = DRV_OS_BRD_212_UNINITIALIZED;
            return returnCode;
        }

        // Get board revision
        returnCode = readPCBRevisionFromEEPROM(DEFAULT_I2C_BUSNAME "." EEPROM_NAME, EEPROM_READ_SIZE,
                                               EEPROM_PCB_REVISION_OFFSET, &(info->revision));
        if(returnCode != DRV_OS_BRD_212_DRV_SUCCESS)
        {
            boardStatus = DRV_OS_BRD_212_UNINITIALIZED;
            return returnCode;
        }

        boardStatus = DRV_OS_BRD_212_INITIALIZED;
        MV212PRINTF("mv0212 board initialized successfully\n");
    }
    else
    {
        returnCode = DRV_OS_BRD_212_ALREADY_INITIALIZED;
        MV212PRINTF("mv0212 board already initialized \n");
    }

    return returnCode;
}
