///  
/// @file
/// @copyright All code copyright Movidius Ltd 2012, all rights reserved 
///            For License Warranty see: common/license.txt              
///
/// @brief     Definitions and types needed by the MV0212 Board Driver API
///
#ifndef _OS_BRD_MV_0212_DEFS_H
#define _OS_BRD_MV_0212_DEFS_H

typedef enum {
    DRV_OS_BRD_212_DRV_SUCCESS           = RTEMS_SUCCESSFUL, // = OS_MYR_DRV_SUCCESS,
    DRV_OS_BRD_212_DRV_ERROR             = 0x8000 , //= OS_MYR_DRV_ERROR,
    DRV_OS_BRD_212_I2C_SLAVE_ERROR,
    DRV_OS_BRD_212_I2C_DRIVER_ERROR,
    DRV_OS_BRD_212_GPIO_DRIVER_ERROR,
    DRV_OS_BRD_212_CPR_INIT_ERROR,
    DRV_OS_BRD_212_CPR_OPEN_ERORR,
    DRV_OS_BRD_212_INVALID_CONFIG,
    DRV_OS_BRD_212_ALREADY_INITIALIZED,
    DRV_OS_BRD_212_ERROR_BRD_HANDLER_NULL,
} tyOsBrd212ErrorCode;

typedef enum tyOsMv0212I2CConfigType
{
    MV212_I2C_SLAVE = 0,
    MV212_I2C_MASTER,
} tyOsMv0212I2CConfigType;

typedef enum tyOsMv0212CamBBus
{
    MV212_I2C0 = 0,
    MV212_I2C1,
} tyOsMv0212CamBBus;

typedef enum tyOsBoard0212ConfigType
{
    MV212_END = 0,
    MV212_I2C0_STATE, // slave or master: tyOsMv0212I2CConfigType
    MV212_CAM_B_I2C_BUS, // i2c0 or i2c1: tyOsMv0212CamBBus
    // to be continued
} tyOsBoard0212ConfigType;

typedef struct tyOsBoard0212Configuration
{
    tyOsBoard0212ConfigType type;
    int value;
}tyOsBoard0212Configuration;

typedef struct tyOsBoard0212Information
{
    u32 revision;
}tyOsBoard0212Information;

#endif
