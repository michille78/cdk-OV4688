/// @file
/// @copyright All code copyright Movidius Ltd 2014, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     rtems driver for board mv0212
///
#ifndef _OS_BRD_MV_0212_H
#define _OS_BRD_MV_0212_H

// 1: Includes
// ----------------------------------------------------------------------------
#include <mv_types.h>
#include <rtems.h>
#include "OsBrdMv0212Defines.h"

#ifdef __cplusplus
extern "C" {
#endif

// 2: Typedefs (types, enums, structs)
// ----------------------------------------------------------------------------

// 3:  Exported Functions
// ----------------------------------------------------------------------------

/// This function initialize the basic functions of MV0212 board: I2C busses,
/// external clock generator and sets up all GPIOS and detect board revision
///
/// @param[in]  config pointer to the preinitialised handle for this MV0212 of type tyOsBoard0212Configuration.
//  Contains i2c buses configurations and external clock configuration.
/// @param[out] info   pointer to tsOsBoard0212Information type, which if set, returns initialisation information.
/// @return @ref tyOsBrd212ErrorCode
///
tyOsBrd212ErrorCode OsBoard0212Initialise(tyOsBoard0212Configuration* config, tyOsBoard0212Information *info);

#ifdef __cplusplus
}
#endif

#endif //_OS_BRD_MV_0212_H
