///
/// @file
/// @copyright All code copyright Movidius Ltd 2012, all rights reserved
///            For License Warranty see: common/license.txt
///
/// @brief     API for the MV0212 Board Driver
///
///
///
///
///
#ifndef BRD_MV0212_H
#define BRD_MV0212_H

// 1: Includes
// ----------------------------------------------------------------------------
#include <brdMv0212Defines.h>
#include <DrvI2cMaster.h>

#ifdef __cplusplus
extern "C" {
#endif

// 2:  Exported Global Data (generally better to avoid)
// ----------------------------------------------------------------------------

// 3:  Exported Functions (non-inline)
// ----------------------------------------------------------------------------

/// @brief Configures the External PLL to a given frequency
/// @param[in] config_index (See "DrvCDCEL.h" for usable indexes)
/// @return    BRD_212_DRV_SUCCESS on success, non-zero on fail
///
s32 Brd212ExternalPllConfigure(u32 configIndex);

/// @brief Helper function to control LEDS on MV0212
/// @param[in] *I2CM_Device Handle for I2C Device 2
/// @param[in] BRD_LED1 or BRD_LED2
/// @param[in] LED_ON or LED_OFF
/// @return  0 on Success
///
s32 Brd212SetLed(I2CM_Device * i2cDevice, tyBrdLedId ledNum, tyLedState ledState);

/// @brief This function initialize the basic functions of MV0182 board: I2C busses,
/// external clock generator and sets up all GPIOS and detect board revision
/// @param[in] config - The board Configuration structure
/// @param[out] info - The board Information structure
/// @return
///         - BRD_212_DRV_SUCCESS
///         - BRD_212_DRV_ERROR
///         - BRD_212_I2C_SLAVE_ERROR
///         - BRD_212_I2C_DRIVER_ERROR
///         - BRD_212_EXTERNAL_PLL_ERROR
///
brd212ErrorCode Brd212Initialise(brd212Configuration *config, brd212Information *info);

#ifdef __cplusplus
}
#endif

#endif // BRD_MV0212_H
