#include <brdMv0212.h>
#include <DrvGpio.h>
#include <assert.h>
#include "mv_types.h"
#include <stdio.h>
#include <stdlib.h>
#include "DrvCDCEL.h"
#include "DrvWm8325.h"
#include <DrvTimer.h>
#include "DrvRegUtilsDefines.h"
#include "brdMv0212GpioDefaults.h"

// 2:  Source Specific #defines and types  (typedef,enum,struct)
// ----------------------------------------------------------------------------
#define IRQ_SRC_0   0
#define IRQ_SRC_1   1
#define IRQ_SRC_2   2
#define IRQ_SRC_3   3

#define NUM_I2C_DEVICES                   (3)
#define ONE_BYTE_SIZE                    0x01
#define TWO_BYTES_SIZE                   0x02

#define PLL_STATUS_OK   0
#define I2C_STATUS_OK   0

#define EEPROM_MEMORY_SIZE               0x23
#define EEPROM_I2C_SLAVE_ADDRESS         0x50
#define EEPROM_PART_1_SIZE                 32 // only 32 bytes can be read or written at a time from eeprom
#define EEPROM_PART_2_SIZE                  3
#define EEPROM_PART_1_OFFSET             0x00
#define EEPROM_PART_2_OFFSET             0x20
#define PCB_REVISION_OFFSET              0x0D

#ifndef WM8325_SLAVE_ADDRESS
#define WM8325_SLAVE_ADDRESS       0x36
#endif
#define WM8325_GPIOS_LEVEL_LOW   0XF000
#define GPIO_DEFAULT_CFG_VALUE   0xA400  //gpio's default value



// #define DRV_BOARD_DEBUG  //Debug Messages
#ifdef DRV_BOARD_DEBUG
#define DPRINTF_BOARD(...) printf(__VA_ARGS__)
#else
#define DPRINTF_BOARD(...)
#endif

// 3: Global Data (Only if absolutely necessary)
// ----------------------------------------------------------------------------

// 4: Static Local Data
// ----------------------------------------------------------------------------
// This function declaration is needed by the data initialised below
static I2CM_Device *i2c0Hndl;
static I2CM_Device *i2c1Hndl;
static I2CM_Device *i2c2Hndl;
static u32 commonI2CErrorHandler(I2CM_StatusType error, u32 param1, u32 param2);
static u8 protocolReadSample2[] = I2C_PROTO_READ_16BA;
static u8 protocolWriteSample2[] = I2C_PROTO_WRITE_16BA;

static I2CM_Device i2cDevHandle[NUM_I2C_DEVICES];

static tyI2cConfig i2c0DefaultConfiguration =
    {
     .device                = IIC1_BASE_ADR,
     .sclPin                = MV0212_I2C0_SCL_PIN,
     .sdaPin                = MV0212_I2C0_SDA_PIN,
     .speedKhz              = MV0212_I2C0_SPEED_KHZ_DEFAULT,
     .addressSize           = MV0212_I2C0_ADDR_SIZE_DEFAULT,
     .errorHandler          = &commonI2CErrorHandler,
    };

static tyI2cConfig i2c1DefaultConfiguration =
    {
     .device                = IIC2_BASE_ADR,
     .sclPin                = MV0212_I2C1_SCL_PIN,
     .sdaPin                = MV0212_I2C1_SDA_PIN,
     .speedKhz              = MV0212_I2C1_SPEED_KHZ_DEFAULT,
     .addressSize           = MV0212_I2C1_ADDR_SIZE_DEFAULT,
     .errorHandler          = &commonI2CErrorHandler,
    };

static tyI2cConfig i2c2DefaultConfiguration =
    {
     .device                = IIC3_BASE_ADR,
     .sclPin                = MV0212_I2C2_SCL_PIN,
     .sdaPin                = MV0212_I2C2_SDA_PIN,
     .speedKhz              = MV0212_I2C2_SPEED_KHZ_DEFAULT,
     .addressSize           = MV0212_I2C2_ADDR_SIZE_DEFAULT,
     .errorHandler          = &commonI2CErrorHandler,
    };

static tyI2cConfig i2c2RevisionDetectConfiguration =
    {
     .device                = IIC3_BASE_ADR,
     .sclPin                = MV0212_I2C2_SCL_PIN,
     .sdaPin                = MV0212_I2C2_SDA_PIN,
     .speedKhz              = MV0212_I2C2_SPEED_KHZ_DEFAULT,
     .addressSize           = MV0212_I2C2_ADDR_SIZE_DEFAULT,
    };

static const drvGpioInitArrayType brdMV0212RevDetectConfig =
{
    // -----------------------------------------------------------------------
    // PCB Revision detect, set weak pullups on the necessary pins
    // -----------------------------------------------------------------------
    {9,  9  , ACTION_UPDATE_PAD          // Only updating the PAD configuration
            ,
            PIN_LEVEL_LOW              // Don't Care, not updated
            ,
            D_GPIO_MODE_0            // Don't Care, not updated
            ,
            D_GPIO_PAD_PULL_DOWN       | // Enable weak pullups so that we can detect revision
            D_GPIO_PAD_DRIVE_2mA     |
            D_GPIO_PAD_SLEW_SLOW     |
            D_GPIO_PAD_SCHMITT_OFF   |
            D_GPIO_PAD_RECEIVER_ON   |
            D_GPIO_PAD_LOCALCTRL_OFF |
            D_GPIO_PAD_LOCALDATA_LO  |
            D_GPIO_PAD_LOCAL_PIN_IN

    },
    // -----------------------------------------------------------------------
    // Finally we terminate the Array
    // -----------------------------------------------------------------------
    {0,0    , ACTION_TERMINATE_ARRAY      // Do nothing but simply termintate the Array
            ,
              PIN_LEVEL_LOW               // Won't actually be updated
            ,
              D_GPIO_MODE_0               // Won't actually be updated
            ,
              D_GPIO_PAD_DEFAULTS         // Won't actually be updated
    }
};
/******************************************************************************
 5: Functions Implementation
******************************************************************************/

// -----------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------
// Static Function Implementations
// -----------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------

static u32 commonI2CErrorHandler(I2CM_StatusType i2cCommsError, u32 slaveAddr, u32 regAddr)
{
    UNUSED(slaveAddr); // hush the compiler warning.
    UNUSED(regAddr);

    if(i2cCommsError != I2CM_STAT_OK)
    {
        DPRINTF_BOARD("\nI2C Error (%d) Slave (%02X) Reg (%02X)",i2cCommsError,slaveAddr,regAddr);
        assert(i2cCommsError == 0);
    }
    return i2cCommsError; // Because we haven't really handled the error, pass it back to the caller
}

static void cleanBuffer(u8 *src, u32 size)
{
    u32 index;
    for(index = 0; index < size; index++)
    {
        *(src+index)=0;
    }
}

static u8 verifyCheckSum(u8 *src,u8 size)
{
    u32 sum = 0;
    u32    index;

    for(index = 0;index < size;index++)
    {
        sum = sum + *(src+index);
    }

    sum = sum & 0xFF;

    if(sum == 0)
    {
        return BRD_212_DRV_SUCCESS; //checksum is ok
    }
    else
    {
       
        return BRD_212_DRV_ERROR; //checksum is not ok
    }
}

static s32 Brd212InitialiseI2C(tyI2cConfig *i2c0Cfg, tyI2cConfig *i2c1Cfg, tyI2cConfig *i2c2Cfg,
                        I2CM_Device **i2c0Dev,I2CM_Device **i2c1Dev, I2CM_Device **i2c2Dev)
{
    s32 ret;

    // Unless the user specifies otherwise we use the default configuration
    if (i2c0Cfg == NULL)
        i2c0Cfg = &i2c0DefaultConfiguration;

    if (i2c1Cfg == NULL)
        i2c1Cfg = &i2c1DefaultConfiguration;

    if (i2c2Cfg == NULL)
        i2c2Cfg = &i2c2DefaultConfiguration;

    // Initialise the I2C device to use the I2C0 Hardware block
    ret = DrvI2cMInitFromConfig(&i2cDevHandle[0], i2c0Cfg);
    if (ret != I2CM_STAT_OK)
    {
        return BRD_212_I2C_DRIVER_ERROR;
    }
    *i2c0Dev = &i2cDevHandle[0]; // Return the handle

    // Initialise the I2C device to use the I2C1 Hardware block
    ret = DrvI2cMInitFromConfig(&i2cDevHandle[1], i2c1Cfg);
    if (ret != I2CM_STAT_OK)
    {
        return BRD_212_I2C_DRIVER_ERROR;
    }
    *i2c1Dev = &i2cDevHandle[1]; // Return the handle

    // Initialise the I2C device to use the I2C1 Hardware block
    ret = DrvI2cMInitFromConfig(&i2cDevHandle[2], i2c2Cfg);
    if (ret != I2CM_STAT_OK)
    {
        return BRD_212_I2C_DRIVER_ERROR;
    }
    *i2c2Dev = &i2cDevHandle[2]; // Return the handle

    // Also setup a common error handler for I2C
    if (i2c0Cfg->errorHandler)
        DrvI2cMSetErrorHandler(&i2cDevHandle[0], i2c0Cfg->errorHandler);

    if (i2c1Cfg->errorHandler)
        DrvI2cMSetErrorHandler(&i2cDevHandle[1], i2c1Cfg->errorHandler);

    if (i2c2Cfg->errorHandler)
        DrvI2cMSetErrorHandler(&i2cDevHandle[2], i2c2Cfg->errorHandler);

    return BRD_212_DRV_SUCCESS;
}
  
static s32 Brd212GetPcbRevision(u32 *brdRev)
{  
    u8 eepromData[EEPROM_MEMORY_SIZE];
    I2CM_Device i2c2Dev;
    I2CM_StatusType status;
    tyI2cConfig * i2c2Cfg;
    i2c2Cfg = &i2c2RevisionDetectConfiguration;

    status = DrvI2cMInitFromConfig(&i2c2Dev, i2c2Cfg);
    if (status != I2CM_STAT_OK)
    {
        DPRINTF_BOARD("I2C initialization error\n");
        return (s32)BRD_212_I2C_DRIVER_ERROR;
    }

    // read revision details from eeprom
    cleanBuffer(&eepromData[0],EEPROM_MEMORY_SIZE);
    status = DrvI2cMTransaction(&i2c2Dev, EEPROM_I2C_SLAVE_ADDRESS,
                    EEPROM_PART_1_OFFSET,
                    protocolReadSample2,
                    (u8 *) &eepromData[EEPROM_PART_1_OFFSET],
                    EEPROM_PART_1_SIZE);
    
    if(status != I2CM_STAT_OK)
    {
        //check other I2C error
        DPRINTF_BOARD("I2C error\n");
        return (s32)BRD_212_I2C_SLAVE_ERROR;
    }
    else
    {
        //read rest of EEPROM
        status = DrvI2cMTransaction(&i2c2Dev, EEPROM_I2C_SLAVE_ADDRESS,
                EEPROM_PART_2_OFFSET,
                protocolReadSample2,
                (u8 *) &eepromData[EEPROM_PART_2_OFFSET],
                EEPROM_PART_2_SIZE);
       
        if(status != I2CM_STAT_OK)
        {
            DPRINTF_BOARD("I2C error\n");
            return (s32)BRD_212_I2C_SLAVE_ERROR;
        }

        if (verifyCheckSum(&eepromData[0], EEPROM_MEMORY_SIZE) == BRD_212_DRV_SUCCESS)
        {
            
            *brdRev = eepromData[PCB_REVISION_OFFSET];
        }
        else
        {
            printf("Error: checksum from EEPROM is invalid\n");
            return (s32)BRD_212_DRV_ERROR;
        }
    }

    return (s32)BRD_212_DRV_SUCCESS;
}

s32 Brd212SetLed(I2CM_Device * i2cDevice,tyBrdLedId ledNum,tyLedState ledState)
{
    const u32 LEN_TWO_BYTES=2;
    const u32 MSB_LED_OFF  =0x00;
    const u32 MSB_LED_ON   =0x40;
    const u32 LSB_LED      =0x00;
    int retVal;
    u8 protocolWriteSample2[] = I2C_PROTO_WRITE_16BA;

    u16 regAddr;
    u8 controlValue[2];

    if (ledNum == BRD_LED1)
        regAddr = WM8325_STATUS_LED_1;
    else if (ledNum == BRD_LED2)
        regAddr = WM8325_STATUS_LED_2; 
    else 
        assert(FALSE);

    if (ledState == LED_ON)
        controlValue[0] = MSB_LED_ON;
    else 
        controlValue[0] = MSB_LED_OFF;

    controlValue[1] = LSB_LED;
    retVal = DrvI2cMTransaction(i2cDevice, MV0212_WM8325_I2C_ADDR_7BIT,
                                regAddr,
                                protocolWriteSample2,
                                (u8 *) &controlValue[0],
                                LEN_TWO_BYTES);

    return retVal;
}

static s32 cfgWM8325GpiosDefault(I2CM_Device* dev)
{
    s32 result;
    u8 controlValue[2];

    // Set all PMIC GPIOs to default state
    controlValue[0] = ((GPIO_DEFAULT_CFG_VALUE & 0xFF00) >> 8);
    controlValue[1] = (GPIO_DEFAULT_CFG_VALUE & 0x00FF);
    for (int i = 0; i < 12; ++i)
    {
        result = DrvI2cMTransaction(dev, WM8325_SLAVE_ADDRESS,
                                    (WM8325_GPIO1_CONTROL + i),
                                    protocolWriteSample2,
                                    (u8 *) &controlValue[0],
                                    TWO_BYTES_SIZE);
    
            if (result != I2CM_STAT_OK)
            {
                DPRINTF_BOARD("Transaction write failed reg 0x%x \n",  (WM8325_GPIO1_CONTROL + i));
                return (s32)BRD_212_I2C_SLAVE_ERROR;
            }
    }

    return (s32)BRD_212_DRV_SUCCESS;
}

// ---------------------------------------------------------------------------------
// External PLL
// ---------------------------------------------------------------------------------
s32 Brd212ExternalPllConfigure(u32 configIndex)
{
    I2CM_Device *   pllI2cHandle;
    int             retVal;

    // Get handle for the I2C
    pllI2cHandle = &i2cDevHandle[2];

    DrvGpioSetPin(58, 1); //enable the CDCEL chip
   // Configure the PLL
    if ((retVal = CDCE925Configure(pllI2cHandle,configIndex)))
        return retVal;

    return (s32)BRD_212_DRV_SUCCESS; // Success
}

static void brdMv0212InitialiseGpioDriver(void)
{
    DrvGpioIrqSrcDisable(IRQ_SRC_0);
    DrvGpioIrqSrcDisable(IRQ_SRC_1);
    DrvGpioIrqSrcDisable(IRQ_SRC_2);
    DrvGpioIrqSrcDisable(IRQ_SRC_3);

    DrvGpioInitialiseRange(brdMV0212GpioCfgDefault);
}

brd212ErrorCode Brd212Initialise(brd212Configuration *config, brd212Information *info)
{
    s32 status = BRD_212_DRV_ERROR;
    if (config == NULL)
        return BRD_212_DRV_ERROR;

    brdMv0212InitialiseGpioDriver();

    status = Brd212InitialiseI2C(NULL,NULL,NULL,     // Use Default I2C configuration
                                    &i2c0Hndl,
                                    &i2c1Hndl,
                                    &i2c2Hndl);

    if(status != BRD_212_DRV_SUCCESS)
        return BRD_212_I2C_DRIVER_ERROR;

    // Setting all PMIC GPIOs to default configuration
    status = cfgWM8325GpiosDefault(i2c2Hndl);
    if(status != BRD_212_DRV_SUCCESS)
        return BRD_212_I2C_SLAVE_ERROR;

    if (config->clockConfiguration != 0)
    {
        status = Brd212ExternalPllConfigure(config->clockConfiguration);
        if (status != PLL_STATUS_OK)
            return BRD_212_EXTERNAL_PLL_ERROR;
    }

    if (info)
    {
        status = Brd212GetPcbRevision(&info->revision);
        if (status != BRD_212_DRV_SUCCESS)
            return BRD_212_DRV_ERROR;

        info->i2c0Hndl=i2c0Hndl;
        info->i2c1Hndl=i2c1Hndl;
        info->i2c2Hndl=i2c2Hndl;
    }

    return BRD_212_DRV_SUCCESS;
}
