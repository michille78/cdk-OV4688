#include <swcLeonUtilsDefines.h>
#include <registersMyriad.h>
#include <DrvIcbDefines.h>
       
!;============================================================================

		.section ".text.swcLeonSetPIL", "ax", @progbits
		.type   swcLeonSetPIL, #function
		.global swcLeonSetPIL
		.align 4
swcLeonSetPIL:
                !; input is in %o0, output in %o0 
                !; extract current PIL / form new PSR
                rd      %psr, %o1
                sll     %o0, POS_PSR_PIL, %o2 ! %o2 = new_pil<<PIL_POS
                and     %o1, MASK_PSR_PIL, %o0 ! %o0 = psr&PIL_MASK
                bclr    MASK_PSR_PIL, %o1 ! %o1 = psr&~PIL_MASK
                and     %o2, MASK_PSR_PIL, %o2 ! %o3 = (new_pil<<PIL_POS)&PIL_MASK
                wr      %o2, %o1, %psr
                 srl    %o0, POS_PSR_PIL, %o0 ! return the old PIL
                 retl
                  nop
                .size   swcLeonSetPIL, . - swcLeonSetPIL

!;============================================================================

		.section ".text.swcLeonDisableTraps", "ax", @progbits
		.type	swcLeonDisableTraps, #function
		.global swcLeonDisableTraps
		.align 4
swcLeonDisableTraps:
                rd      %psr, %o0
                andn    %o0, PSR_ET, %o1
                wr      %o1, %psr
                 and    %o0, PSR_ET, %o0
                 retl
                  srl   %o0, 5, %o0 ! return the old ET
                .size   swcLeonDisableTraps, . - swcLeonDisableTraps

!;============================================================================

		.section ".text.swcLeonEnableTraps", "ax", @progbits
		.type	swcLeonEnableTraps, #function
		.global swcLeonEnableTraps
		.align 4
swcLeonEnableTraps:
                rd      %psr, %o0
                or      %o0, PSR_ET, %o1
                wr      %o1, %psr
                 and    %o0, PSR_ET, %o0
                 retl
                  srl   %o0, 5, %o0 ! return the old ET
                .size   swcLeonEnableTraps, . - swcLeonEnableTraps
