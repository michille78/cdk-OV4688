#commonbuilds.mk - generic.mk addition to be used with the Movidius elf build flow
#                  in order to add build rules for commonly used libraries
#
# Created on: Jul 17, 2013
#     Author: Cristian-Gavril Olar

#----------------------[ Software common library files ]---------------------------#
ifeq ($(MV_SOC_REV),$(filter $(MV_SOC_REV),ma2480 ma2485))
# MA2x8x doesn't support the Cdma Driver yet, this needs to move into an arch specific system soon
# for now we just specifically build swcShaveInit.c, ANSIWriteOverload.c and ignore swcCdmaShaveM2.c
# We cannot safely compile all code from socDrivers/shave and socDrivers/shared for ma2x8x.
 SH_SWCOMMON_C_SOURCES  = $(wildcard $(MV_COMMON_BASE)/swCommon/shave_code/$(MV_SOC_PLATFORM)/*/swcShaveInit.c)
 SH_SWCOMMON_C_SOURCES += $(MV_COMMON_BASE)/swCommon/shave_code/$(MV_SOC_PLATFORM)/src/overloads/ANSIWriteOverload.c
else
 SH_SWCOMMON_C_SOURCES  = $(wildcard $(MV_COMMON_BASE)/swCommon/shave_code/$(MV_SOC_PLATFORM)/*/*.c)
 SH_SWCOMMON_C_SOURCES += $(wildcard $(MV_DRIVERS_BASE)/$(MV_SOC_PLATFORM)/socDrivers/shave/src/*.c)
 SH_SWCOMMON_C_SOURCES += $(wildcard $(MV_DRIVERS_BASE)/$(MV_SOC_PLATFORM)/socDrivers/shared/src/*.c)
endif

SH_SWCOMMON_C_SOURCES += $(foreach var, $(SOC_REV_DIR), $(wildcard \
                           $(MV_DRIVERS_BASE)/$(MV_SOC_PLATFORM)/socDrivers/shave/arch/$(var)/src/*.c \
                           $(MV_DRIVERS_BASE)/$(MV_SOC_PLATFORM)/socDrivers/shared/arch/$(var)/sgl/src/*.c \
                           $(MV_DRIVERS_BASE)/$(MV_SOC_PLATFORM)/socDrivers/shared/arch/$(var)/hgl/src/*.c ))

ifeq ($(MV_SOC_REV),$(filter $(MV_SOC_REV),ma2480 ma2485))
# For now, MA2x8x support doesn't use the common SHAVE assembly as they do not use ma2x8x assembly mnemonics
SH_SWCOMMON_ASM_SOURCES = $(wildcard $(MV_COMMON_BASE)/swCommon/shave_code/$(MV_SOC_PLATFORM)/asm/ma2x8x/*.asm)
else
SH_SWCOMMON_ASM_SOURCES = $(wildcard $(MV_COMMON_BASE)/swCommon/shave_code/$(MV_SOC_PLATFORM)/asm/ma2x5x/*.asm)
endif

SH_SWCOMMON_CPP_SOURCES = $(wildcard $(MV_COMMON_BASE)/swCommon/shave_code/$(MV_SOC_PLATFORM)/*/*.cpp)

SH_SWCOMMON_GENASMS  = $(patsubst %.c,$(DirAppObjBase)%.asmgen,$(SH_SWCOMMON_C_SOURCES))
SH_SWCOMMON_GENASMS += $(patsubst %.cpp,$(DirAppObjBase)%.asmgen,$(SH_SWCOMMON_CPP_SOURCES))

SH_SWCOMMON_OBJS  = $(patsubst %.asm,$(DirAppObjBase)%_shave.o,$(SH_SWCOMMON_ASM_SOURCES))
SH_SWCOMMON_OBJS += $(patsubst %.asmgen,%_shave.o,$(SH_SWCOMMON_GENASMS))

SH_SWCOMMON_MBINOBJS  = $(patsubst %.asm,$(DirAppObjBase)%.mobj,$(SH_SWCOMMON_ASM_SOURCES))
SH_SWCOMMON_MBINOBJS += $(patsubst %.asmgen,%.mobj,$(SH_SWCOMMON_GENASMS))

#----------------------[ Components library files ]---------------------------#
SHAVE_COMPONENT_C_SOURCES = $(sort $(wildcard $(patsubst %,%/*.c,$(SH_COMPONENTS_PATHS))))
SHAVE_COMPONENT_CPP_SOURCES = $(sort $(wildcard $(patsubst %,%/*.cpp,$(SH_COMPONENTS_PATHS))))
SHAVE_COMPONENT_ASM_SOURCES = $(sort $(wildcard $(patsubst %,%/*.asm,$(SH_COMPONENTS_PATHS))))

SHAVE_COMPONENT_GENASMS  = $(patsubst %.c,$(DirAppObjBase)%.asmgen,$(SHAVE_COMPONENT_C_SOURCES))
SHAVE_COMPONENT_GENASMS += $(patsubst %.cpp,$(DirAppObjBase)%.asmgen,$(SHAVE_COMPONENT_CPP_SOURCES))

SHAVE_COMPONENT_OBJS  = $(patsubst %.asm,$(DirAppObjBase)%_shave.o,$(SHAVE_COMPONENT_ASM_SOURCES))
SHAVE_COMPONENT_OBJS += $(patsubst %.asmgen,%_shave.o,$(SHAVE_COMPONENT_GENASMS))
#----------------------[ Actual build rules ]---------------------------#
# swCommon mlib
$(CommonMlibFile).mvlib: $(SH_SWCOMMON_OBJS)
	@echo "Shave Common LIB      : $@"
	@mkdir -p $(dir $@)
	$(ECHO) $(AR) rs $@ $(SH_SWCOMMON_OBJS)

#The apps own lib
$(ProjectShaveLib) : $(SHAVE_COMPONENT_OBJS)
	@echo "Shave Component LIB   : $@"
	@mkdir -p $(dir $@)
	$(ECHO)  $(AR) rs $@ $(SHAVE_COMPONENT_OBJS)

#---------------------[ Listing file builds ]----------------------------#
# These are not files built for executing however they are "common" and
# not related to the build flow. So instead of creating a new *.mk file
# or poluting the generik.mk file that contains build flow rules
# I'm adding these targets here

.INTERMEDIATE: $(DirAppOutput)/lst/$(APPNAME).elf.nodebug

# Generating a binutils generated version of the project
# This file contains valid leon disassembly, but doesn't decode SHAVE sections
LEON_LST_FILE=$(DirAppOutput)/lst/$(APPNAME)_leon.lst
# Movidius Object dump of project elf file
# This file contains full disasembly of leon and shave sections
SHAVE_LST_FILE=$(DirAppOutput)/lst/$(APPNAME)_shave.lst

ifeq "$(JENKINS_HOME)" ""
$(LEON_LST_FILE) : $(DirAppOutput)/lst/$(APPNAME).elf.nodebug
	@echo "Leon Dbg Listing      : $(DirAppOutput)/lst/$(APPNAME)_leon.lst"
	$(ECHO) $(OBJDUMP) -xsrtd $< > $@

$(SHAVE_LST_FILE) : $(DirAppOutput)/lst/$(APPNAME).elf.nodebug
	@echo "Shave Dbg Listing     : $(DirAppOutput)/lst/$(APPNAME)_shave.lst"
#TODO: revert this workaround once the tools team removes bss section clashing checks
ifneq ($(shaveXApps),)
	$(ECHO) $(MVDUMP) -cv:$(MV_SOC_REV) -shave -nosegmentcheck $< -o:$@  $(DUMP_NULL)
else
	$(ECHO) $(MVDUMP) -cv:$(MV_SOC_REV) $< -o:$@  $(DUMP_NULL)
endif
else
$(LEON_LST_FILE) $(SHAVE_LST_FILE) :
	@echo $@ LST file generation is disabled on Jenkins environment
endif

lst: $(LEON_LST_FILE) $(SHAVE_LST_FILE)
	@echo > /dev/null

# This file contains all symbols from the elf file
$(DirAppOutput)/$(APPNAME)_leon.sym : $(DirAppOutput)/$(APPNAME).elf
	@echo "Leon Symobls Listing  : $(DirAppOutput)/$(APPNAME)_leon.sym"
	@test -d $(@D) || mkdir -p $(@D)
	$(ECHO) $(OBJDUMP) -t $< > $@

ifneq ($(shaveXApps),)

define cGroupVars_templateMK
#This target creates the dyanmical data loading infrastructure requires makefile include dependencies
appsGrpDynContextRules$(1).mk : Makefile
	@echo "" > $$@
	@echo "define cGroupAppVars_template$(1)" >> $$@
	@echo "" >> $$@
	@echo "cDynContextDefs_""$$$$""(1) += -D'APPGROUPDYNDATASECTION=""$(1)grpdyndata'" >> $$@
	@echo "cDynContextDefs_""$$$$""(1) += -D'GRPENTRY=""$(1)____AllTimeEntryPoint'" >> $$@
	@echo "" >> $$@
	@echo "$$$$""(1)_GROUP_DATA := $$($(1)).shvZdata" >> $$@
	@echo "" >> $$@
	@echo "endef" >> $$@
	@echo "$$$$""(foreach appX,""$$$$""(MV_$(1)_APPS),""$$$$""(eval ""$$$$""(call cGroupAppVars_template""$(1)"",""$$$$""(lastword ""$$$$""(subst /, ,""$$$$""(appX))))))" >> $$@
	@echo "	" >> $$@
endef

$(foreach groupMK,$(MV_SHAVE_GROUPS),$(eval $(call cGroupVars_templateMK,$(groupMK))))

#generate a master C file holding the list of all applications in the project. Needed to get state of project.
$(DirAppObjBase)dynContextMaster.c : Makefile
	@mkdir -p $(dir $(call RESOLVE_TARGET_PATH,$@))
	@echo "//  Generated file with all application content data" > $(call RESOLVE_TARGET_PATH,$@)
	@echo "#include \"theDynContext.h\"" >> $(call RESOLVE_TARGET_PATH,$@)
	@echo "" >> $(call RESOLVE_TARGET_PATH,$@)
	@echo "#define ___APPS_DETECTED " $(words $(shaveXApps))  >> $(call RESOLVE_TARGET_PATH,$@)
	@echo ""  >> $(call RESOLVE_TARGET_PATH,$@)
	@echo -e "$(foreach app,$(shaveXApps),extern DynamicContext_t $(lastword $(subst /, ,$(app)))X_ModuleData;\n)" >> $(call RESOLVE_TARGET_PATH,$@)
	@echo ""  >>  $(call RESOLVE_TARGET_PATH,$@)
	@echo "DynamicContextInfo_t GlobalArray[___APPS_DETECTED]=" >> $(call RESOLVE_TARGET_PATH,$@)
	@echo "{"  >> $(call RESOLVE_TARGET_PATH,$@)
	@echo -e "   $(foreach app,$(shaveXApps), "{" &$(lastword $(subst /, ,$(app)))X_ModuleData, "\""$(lastword $(subst /, ,$(app)))"\"" "}," \n  )" >> $(call RESOLVE_TARGET_PATH,$@)
	@echo "};"  >> $(call RESOLVE_TARGET_PATH,$@)
	@echo ""  >> $(call RESOLVE_TARGET_PATH,$@)
	@echo "DynamicContextGlobal_t GlobalContextData="  >> $(call RESOLVE_TARGET_PATH,$@)
	@echo "{"   >> $(call RESOLVE_TARGET_PATH,$@)
	@echo "     ___APPS_DETECTED, (DynamicContextInfo_t*)GlobalArray"  >> $(call RESOLVE_TARGET_PATH,$@)
	@echo "};"  >> $(call RESOLVE_TARGET_PATH,$@)
	@echo "" >> $(call RESOLVE_TARGET_PATH,$@)

endif

#Automatic rules for classic builds
define mvlibTemplate

$$(DirAppOutput)/$(1).mvlib : $$(SHAVE_$(1)_AUTOSTAT_OBJS) $$(PROJECT_SHAVE_LIBS)
	@echo "Building statically allocated shave lib $$@"
	@test -d $$(@D) || mkdir -p $$(@D)
	$$(ECHO) $$(LD) $$(MVLIBOPT) $$(DefaultStaticProjectShaveEntryPoints) --gc-sections $$(SHAVE_$(1)_AUTOSTAT_OBJS) $$(PROJECT_SHAVE_LIBS) $$(CompilerANSILibs) -o $$(call RESOLVE_TARGET_PATH,$$@)

endef

$(foreach app,$(AUTO_DETECTED_SHAVE_APPS),$(eval $(call mvlibTemplate,$(lastword $(subst /, ,$(app))))))


#Adding rule to build mvlib dynamic files
define cAppDynamicMvlibBuildRule_template

$$(DirAppOutput)/$$($(1)App).mvlib :  $$(SHAVE_$(1)_AUTODYN_OBJS)
	@echo "Building dynamic data allocated shave lib $$@"
	@test -d $$(@D) || mkdir -p $$(@D)
	$$(ECHO) $$(LD) -e $$($(1)_MainEntry) --gc-sections  $$(MVLIBOPT) $$(SHAVE_$(1)_AUTODYN_OBJS)  $$(SHAVE_$(1)_DYN_APP_LIB) -o $$(call RESOLVE_TARGET_PATH,$$@)
endef

$(foreach app,$(shaveXApps),$(eval $(call cAppDynamicMvlibBuildRule_template,$(lastword $(subst /, ,$(app))),$(app))))

# Intermediate copy of the elf file with the key debug sections removed
# Manually selecting the debug sections to remove as I found the --strip-debug
# option too agressive
# This intermediate target is automatically deleted when make completes
$(DirAppOutput)/lst/$(APPNAME).elf.nodebug : $(DirAppOutput)/$(APPNAME).elf
	@mkdir -p $(DirAppOutput)/lst
	$(ECHO) $(OBJCOPY) -R .debug_aranges \
			   -R .debug_ranges \
			   -R .debug_pubnames \
			   -R .debug_info \
			   -R .debug_abbrev \
			   -R .debug_asmline \
			   -R .debug_line \
			   -R .debug_str \
			   -R .debug_loc \
			   -R .debug_macinfo \
			$< $@


###################################################################
# Dependencies
###################################################################

ifeq ($(MAKEDEPEND),yes)

-include $(LEON_APP_DEPS)
-include $(LEON_RT_DEPS)
-include $(SHAVE_APP_DEPS)

endif
