#generic.mk - simple generic.mk to be used with the Movidius one stage build flow
#
# Created on: Jul 17, 2013
#     Author: Cristian-Gavril Olar


# Need some tentative definitions first and we need to detect the tools to be used

###################################################################
#       Using settings makefiles for wider system settings        #
###################################################################
include $(MV_COMMON_BASE)/generalsettings.mk
include $(MV_COMMON_BASE)/toolssettings.mk
include $(MV_COMMON_BASE)/includessettings.mk
include $(MV_COMMON_BASE)/commonsources.mk
include $(MV_COMMON_BASE)/saferemove.mk
###################################################################
#       And finally listing all of the build rules                #
###################################################################

# detect and correctly handle clean target when parrallel building
ifneq ($(filter clean,$(MAKECMDGOALS)),)
OTHERGOALS := $(filter-out clean,$(MAKECMDGOALS))

ifneq ($(OTHERGOALS),)

# this is the (weird) way of defining line breaks in Makefiles please do not
# modify
define n


endef

$(error You specified 'clean' target along with '$(OTHERGOALS)' target(s).$n \
 Please note this is incorrect as 'clean' will remove objects the '$(OTHERGOALS)' target(s) need.$n \
 Please rewrite your command like this 'make clean && make $(OTHERGOALS)')


endif

else
# the rest of the makefile can go parallel

ELF_FILE = $(DirAppOutput)/$(APPNAME).elf

MV_AUTO_LST ?=yes
ifeq ("$(MV_AUTO_LST)","yes")
BINARY_LST_TARGET-yes=lst
endif

#For external users we have an extra check here that we must do to ensure they have MV_TOOLS_DIR set
#as environment variable
ifeq ($(CHECK_TOOLS_VAR),yes)

CHECKINSTALLOUTPUT = $(shell cd $(MV_COMMON_BASE)/utils && ./mincheck.sh)

all :
	@echo $(CHECKINSTALLOUTPUT)
ifeq ($(CHECKINSTALLOUTPUT),MV_TOOLS_DIR set)
	+$(MAKE) trueall
endif
#Output files and listings files
.PHONY: trueall
trueall :
	$(ECHO) ${MAKE} all_binaries || ${MAKE} delete_elf

.PHONY: all_binaries
all_binaries : print_configuration  $(ELF_FILE) $(DirAppOutput)/$(APPNAME).mvcmd $(BINARY_LST_TARGET-yes) $(DirAppOutput)/$(APPNAME)_leon.sym
	@echo "Finished building $(MV_BUILD_CONFIG) configuration for $(MV_SOC_REV)"

else
MV_PR_LOCAL_TOOLS_EXIST = $(shell if [ ! -d "$(MV_TOOLS_DIR)/$(MV_TOOLS_VERSION)" ];then echo 0;else echo 1;fi)
.PHONY: all
all :
	$(ECHO) ${MAKE} all_binaries || ${MAKE} delete_elf

.PHONY: all_binaries
all_binaries : checktools_locally print_configuration $(ELF_FILE) $(DirAppOutput)/$(APPNAME).mvcmd $(BINARY_LST_TARGET-yes) $(DirAppOutput)/$(APPNAME)_leon.sym
	@echo "Finished building '${MV_BUILD_CONFIG}' configuration for $(MV_SOC_REV)"
# Check if tools exist, output files and listings

checktools_locally :
ifeq ($(MV_PR_LOCAL_TOOLS_EXIST), 0)
	$(error "You do not have $(MV_TOOLS_VERSION) tools locally.")
endif

print_configuration:
	@echo "Building '${MV_BUILD_CONFIG}' configuration for ${MV_SOC_REV}"
endif

delete_elf :
	@echo "Removing ELF file '$(ELF_FILE)'"
	$(foreach item,$(ELF_FILE),$(call SAFE_RM,$(item)))
	exit 1 # take care to fail the overall build as this target is called upon failure

LEON_ALL_OBJECTS=$(sort $(LEON_APP_OBJECTS_REQUIRED) $(LEON_SHARED_OBJECTS_REQUIRED))
$(DirAppOutput)/$(APPNAME).map $(ELF_FILE) : \
		$(LEON_RT_APPS) $(LEON_ALL_OBJECTS) $(ALL_SHAVE_APPS) \
		$(ALL_SHAVE_DYN_APPS) $(SHVDYNSCRIPT) $(AllLibs) $(LinkerScript)
	@echo "Application ELF       : $(call RESOLVE_TARGET_PATH,$@)"
	@mkdir -p $(dir $(call RESOLVE_TARGET_PATH,$(ELF_FILE)))
	$(ECHO) $(LD) $(LDOPT) -o $(call RESOLVE_TARGET_PATH,$(ELF_FILE))         \
		$(MV_RTEMS_PROLOGUE) $(LEON_RT_APPS) $(LEON_ALL_OBJECTS) \
		$(AllLibs) $(ALL_SHAVE_APPS) $(ALL_SHAVE_DYN_APPS)       \
		--start-group $(DefaultSparcRTEMSLibs) --end-group       \
		$(DefaultSparcGccLibs)  $(MV_RTEMS_EPILOGUE) > $(DirAppOutput)/$(APPNAME).map
	@echo "Application MAP File  : $(DirAppOutput)/$(APPNAME).map"

$(DirAppObjBase)%.o : %.c $(LEON_HEADERS) Makefile
	$(call PRINT_USING_CCACHE)
	@echo "Leon CC   : $<"
	@test -d $(@D) || mkdir -p $(@D)
	$(ECHO) $(CC) -c $(CONLY_OPT) $(CCOPT) $(call RESOLVE_TARGET_PATH,$<) -o $(call RESOLVE_TARGET_PATH,$@)

$(DirAppObjBase)%.o: %.cpp $(LEON_HEADERS) Makefile
	$(call PRINT_USING_CCACHE)
	@echo "Leon CPP  : $<"
	@test -d $(@D) || mkdir -p $(@D)
	$(ECHO) $(CXX) -c $(CPPONLY_OPT) $(filter-out -Werror-implicit-function-declaration,$(CCOPT)) $(CPPOPT) -fno-rtti -fno-exceptions  $(call RESOLVE_TARGET_PATH,$<) -o $(call RESOLVE_TARGET_PATH,$@)

$(DirAppObjBase)%.o : %.S $(LEON_HEADERS) Makefile
	$(call PRINT_USING_CCACHE)
	@echo "Leon ASM  : $<"
	@test -d $(@D) || mkdir -p $(@D)
	$(ECHO) $(CC) -c $(CCOPT) -DASM $(call RESOLVE_TARGET_PATH,$<) -o $(call RESOLVE_TARGET_PATH,$@)

%.vero: %.versum Makefile
	@echo "Leon VEROBJ :  $(@:%.vero=%.verres)"
	@test -d $(@D) || mkdir -p $(@D)
	$(ECHO) $(LD) -EL -nostdlib -r -b binary -o $@ $(@:%.vero=%.verres)
	$(ECHO) $(OBJCOPY) --strip-all --rename-section .data=version_info $@

.SECONDARY:
$(DirAppObjBase)%.versum: FORCE
	@echo "Leon VERTMPL:  $(@:%.versum=%.ver)"
	@test -d $(@D) || mkdir -p $(@D)
	$(ECHO) $(TEMPLPROC) $(VERVARS) $*.ver > $(@:%.versum=%.verres)
	$(ECHO) md5sum -b $(@:%.versum=%.verres) > $@
	$(ECHO) md5sum -c --status $@ 2> /dev/null

.PHONY: FORCE
FORCE: ;

###################################################################
#   Shave Build Rules for all common shave code
###################################################################

ifeq ($(MVCC_SKIP_ASM),yes)

ifeq ($(MVCC_DISABLE_CODE_ANALYSER),yes)
 MV_DISABLE_ANALYSER ?=,-a
endif

# The following line may be optionally enabled to see the
# invocation of moviAsm within moviCompile
#MVCC_DEBUGFLAGS=-v -save-temps

# The following assembly options are passed to the assembler to match the pre-existing settings used by the asmgen flow
# In the medium term effors should be made to remove the need for these flags to the assemble
MVCCASMOPT ?= -Wa,-no6thSlotCompression,-i:$(MV_COMMON_BASE)/swCommon/shave_code/myriad2/include,-i:$(MV_COMMON_BASE)/swCommon/shave_code/myriad2/asm$(MV_DISABLE_ANALYSER)

$(DirAppObjBase)%_shave.o : %.c $(SHAVE_C_HEADERS) Makefile
	$(call PRINT_USING_CCACHE)
	@echo "Shave CC  : $<"
	@test -d $(@D) || mkdir -p $(@D)
	$(ECHO) $(MVCC) $(MVCCOPT) $(MVCCASMOPT) $(MVCC_DEBUGFLAGS) $(call RESOLVE_TARGET_PATH,$<) -o $(call RESOLVE_TARGET_PATH,$@) $(DUMP_NULL)

$(DirAppObjBase)%_shave.o : %.cpp $(SHAVE_C_HEADERS) Makefile
	$(call PRINT_USING_CCACHE)
	@echo "Shave CPP : $<"
	@test -d $(@D) || mkdir -p $(@D)
	$(ECHO) $(MVCC) $(MVCCOPT) $(MVCCASMOPT) $(MVCC_DEBUGFLAGS) $(MVCCPPOPT) $(call RESOLVE_TARGET_PATH,$<) -o $(call RESOLVE_TARGET_PATH,$@)  $(DUMP_NULL)

else
$(DirAppObjBase)%_shave.o : $(DirAppObjBase)%.asmgen $(SHAVE_ASM_HEADERS) Makefile
	@echo "Shave CC  : $<"
	@test -d $(@D) || mkdir -p $(@D)
	$(ECHO) $(MVASM) $(MVASMOPT) -elf $(call RESOLVE_TARGET_PATH,$<) -o:$(call RESOLVE_TARGET_PATH,$@) $(DUMP_NULL)

$(DirAppObjBase)%.asmgen : %.c $(SHAVE_C_HEADERS) Makefile
	$(call PRINT_USING_CCACHE)
	@echo "Shave CC  : $<"
	@test -d $(@D) || mkdir -p $(@D)
	$(ECHO) $(MVCC) $(MVCCOPT) $(call RESOLVE_TARGET_PATH,$<) -o $(call RESOLVE_TARGET_PATH,$@) $(DUMP_NULL)

$(DirAppObjBase)%.asmgen : %.cpp $(SHAVE_C_HEADERS) Makefile
	$(call PRINT_USING_CCACHE)
	@echo "Shave CPP : $<"
	@test -d $(@D) || mkdir -p $(@D)
	$(ECHO) $(MVCC) $(MVCCOPT) $(MVCCPPOPT) $(call RESOLVE_TARGET_PATH,$<) -o $(call RESOLVE_TARGET_PATH,$@)  $(DUMP_NULL)

endif

$(DirAppObjBase)%_shave.o : %.asm $(SHAVE_ASM_HEADERS) Makefile
	@echo "Shave ASM : $<"
	@test -d $(@D) || mkdir -p $(@D)
	$(ECHO) $(MVASM) $(MVASMOPT) $(mvAsmAsmfilesOpt) -elf $(call RESOLVE_TARGET_PATH,$<) -o:$(call RESOLVE_TARGET_PATH,$@)  $(DUMP_NULL)

$(DirAppObjBase)%.i : %.c $(SHAVE_C_HEADERS) Makefile
	@echo "Shave pre-process c   : $<"
	@test -d $(@D) || mkdir -p $(@D)
	$(ECHO) $(MVCC) $(MVCCOPT) -E $(call RESOLVE_TARGET_PATH,$<) -o $(call RESOLVE_TARGET_PATH,$@) $(DUMP_NULL)

$(DirAppObjBase)%.ipp : %.cpp $(SHAVE_C_HEADERS) Makefile
	$(call PRINT_USING_CCACHE)
	@echo "Shave pre-process c++ : $(call RESOLVE_TARGET_PATH,$<)"
	@test -d $(@D) || mkdir -p $(@D)
	$(ECHO) $(MVCC) $(MVCCOPT) $(MVCCPPOPT) -E $(call RESOLVE_TARGET_PATH,$<) -o $(call RESOLVE_TARGET_PATH,$@) $(DUMP_NULL)


#########################################################################
#########################################################################

#Move section prefixes for shave0 selected applications. This is the section describing
#ABSOLUTE relocations
#That filter line there is just taking out the application name by removing the ".mvlib" extension
#and the leading words

.SECONDARYEXPANSION:
define MOVE_SHAVE_PREFIX_FOR_LIB

$(2).shv$(1)lib : $(2).shv$(1)templib
	$$(ECHO) $$(OBJCOPY) --redefine-sym $$(lastword $$(filter-out shv%templib,$$(subst /, ,$$(subst ., ,$$<))))$(1)___SglResMgrGlobal=__SglResMgrGlobal -W __SglResMgrGlobal --redefine-sym $$(lastword $$(filter-out shv%templib,$$(subst /, ,$$(subst ., ,$$<))))$(1)_mvShavePipePrintQueueGet=mvShavePipePrintQueueGet  --redefine-sym $$(lastword $$(filter-out shv%templib,$$(subst /, ,$$(subst ., ,$$<))))$(1)_mvShavePipePrintQueueAdd=mvShavePipePrintQueueAdd  --redefine-sym $$(lastword $$(filter-out shv%templib,$$(subst /, ,$$(subst ., ,$$<))))$(1)____globalTail=___globalTail -W ___globalTail $$< $$(call RESOLVE_TARGET_PATH,$$@)
$(2).shv$(1)templib : $(2).mvlib
	$$(ECHO) $$(OBJCOPY) --prefix-alloc-sections=.shv$(1). --prefix-symbols=$$(lastword $$(filter-out mvlib,$$(subst /, ,$$(subst ., ,$$<))))$(1)_ $$< $$(call RESOLVE_TARGET_PATH,$$@)

#And one section to place contents in windowed addresses if we so choose
$(2).shv$(1)wndlib : $(2).mvlib
	$$(ECHO) $$(OBJCOPY) --prefix-alloc-sections=.wndshv$(1). --prefix-symbols=$$(lastword $$(filter-out mvlib,$$(subst /, ,$$(subst ., ,$$<))))$(1)_ $$< $$(call RESOLVE_TARGET_PATH,$$@)

endef

define MOVE_SHAVE_PREFIX

%.shv$(1)lib : %.shv$(1)templib
	$$(ECHO) $$(OBJCOPY) --redefine-sym $$(lastword $$(filter-out shv%templib,$$(subst /, ,$$(subst ., ,$$<))))$(1)___SglResMgrGlobal=__SglResMgrGlobal -W __SglResMgrGlobal  --redefine-sym $$(lastword $$(filter-out shv%templib,$$(subst /, ,$$(subst ., ,$$<))))$(1)_mvShavePipePrintQueueGet=mvShavePipePrintQueueGet --redefine-sym $$(lastword $$(filter-out shv%templib,$$(subst /, ,$$(subst ., ,$$<))))$(1)_mvShavePipePrintQueueAdd=mvShavePipePrintQueueAdd --redefine-sym $$(lastword $$(filter-out shv%templib,$$(subst /, ,$$(subst ., ,$$<))))$(1)____globalTail=___globalTail -W ___globalTail $$< $$(call RESOLVE_TARGET_PATH,$$@)
%.shv$(1)templib : %.mvlib
	$$(ECHO) $$(OBJCOPY) --prefix-alloc-sections=.shv$(1). --prefix-symbols=$$(lastword $$(filter-out mvlib,$$(subst /, ,$$(subst ., ,$$<))))$(1)_ $$< $$(call RESOLVE_TARGET_PATH,$$@)

#And one section to place contents in windowed addresses if we so choose
%.shv$(1)wndlib : %.mvlib
	$$(ECHO) $$(OBJCOPY) --prefix-alloc-sections=.wndshv$(1). --prefix-symbols=$$(lastword $$(filter-out mvlib,$$(subst /, ,$$(subst ., ,$$<))))$(1)_ $$< $$(call RESOLVE_TARGET_PATH,$$@)

endef

$(foreach idx,$(SHAVES_IDX),$(eval $(call MOVE_SHAVE_PREFIX,$(idx))))

#Targets for dynamically loadable objects
%.shvdlib : %.mvlib
	@echo "Creating dynamically loadable library : $(call RESOLVE_TARGET_PATH,$@)"
	$(ECHO) $(LD) $(LD_ENDIAN_OPT) $(LDDYNOPT) $(call RESOLVE_TARGET_PATH,$<) -o $(call RESOLVE_TARGET_PATH,$@)

#this gives out all symbols too
%.shvdcomplete : %.mvlib
	@echo "Creating windowed library for symbol extraction $(call RESOLVE_TARGET_PATH,$@)"
	$(ECHO) $(LD) $(LD_ENDIAN_OPT) $(LDSYMOPT) $(call RESOLVE_TARGET_PATH,$<) -o $(call RESOLVE_TARGET_PATH,$@)

#this creates the required symbols file with only the symbols for the dynamic section
%_sym.o : %.shvdcomplete
	@echo "Creating symbols file for the dynamic section $(call RESOLVE_TARGET_PATH,$@)"
	$(ECHO) $(OBJCOPY) --prefix-symbols=$(lastword $(filter-out shvdcomplete,$(subst /, ,$(subst ., ,$<))))_ --extract-symbol $(call RESOLVE_TARGET_PATH,$<) $(call RESOLVE_TARGET_PATH,$@)

#Some generic targets for raw objects
%_raw.o : %.shvdlib
	$(ECHO) $(OBJCOPY)  -I binary --rename-section .data=.ddr_direct.data \
	--redefine-sym  _binary_$(subst /,_,$(subst .,_,$<))_start=$(lastword $(filter-out shvdlib,$(subst /, ,$(subst ., ,$<))))Mbin \
	-O elf32-littlesparc -B sparc $(call RESOLVE_TARGET_PATH,$<) $(call RESOLVE_TARGET_PATH,$@)

# General rule to make MVcmd from a elf
$(DirAppOutput)/%.mvcmd : $(DirAppOutput)/%.elf
	@echo "MVCMD boot image      : $(call RESOLVE_TARGET_PATH,$@)"
	@mkdir -p $(dir $(call RESOLVE_TARGET_PATH,$@))
	$(ECHO) $(MVCONV) $(MVCONVOPT) $(MVCMDOPT) $(^) -mvcmd:$(@)  $(DUMP_NULL)

ifeq ($(DefaultLinkerGenerateRule),yes)
$(LinkerScript) : $(LinkerScriptSource)
	@echo "Generate Linker Script: $(<F) -> $(call RESOLVE_TARGET_PATH,$@)"
	$(ECHO) $(CC) -E -P -C -I $(DirAppRoot)/scripts -I $(DirLDScript) -I $(DirLDScrCommonPP)/$(MV_SOC_PLATFORM) -I $(DirLDScrCommon)/$(MV_SOC_PLATFORM) $(LinkerScriptSource) -o $(LinkerScript)
endif
###################################################################
# Shave dynamic loading specific rules
###################################################################

###################################################################
#  START of Group handling part
###################################################################
define cGroupRules_template
#Get list of symbols to mark -u based on all apps using this library group
$$($(1)).grpsyms : $$(patsubst %,%.mvlib,$$(MV_$(1)_APPS))
	@echo "Creating list of symbols to be trapped from the library group $$@"
	test -d $$(@D) || mkdir -p $$(@D)
	$$(ECHO) $$(READELF) --syms --wide $$(patsubst %,%.mvlib,$$(MV_$(1)_APPS)) | grep -v Symbol | grep -v Vis | grep -v File | grep -v FILE | grep UND | sed 's/\s\s*/ /g' | cut -d ' ' -f 9 | { grep . || true;} | sort -n | uniq >> $$(call RESOLVE_TARGET_PATH,$$@)
	@echo "Adding the AllTimeEntry point to the symbols to take from the group $$@"
	$$(ECHO) echo ___AllTimeEntryPoint >> $$(call RESOLVE_TARGET_PATH,$$@)

#Get list of all possible symbols provided by the Group
$(1)_FILT_LIBS=$$(filter-out --start-group --end-group --whole-archive --no-whole-archive,$$($(1)_LIBS))
$$($(1)).grpallsyms : $$($(1)_FILT_LIBS)
	@echo "Generate list of all symbols defined by the group $$@"
	@test -d $$(@D) || mkdir -p $$(@D)
	$$(ECHO) $$(READELF) --syms --wide $$($(1)_FILT_LIBS) | grep -v Symbol | grep -v Vis | grep -v File | grep -v FILE | grep -v UND | sed 's/\s\s*/ /g' | cut -d ' ' -f 9 | { grep . || true;} | sort -n | uniq  > $$(call RESOLVE_TARGET_PATH,$$@)

#Group symbols to redefine. Make sure to take out of the redefinition rules the globalTail and ResMgr
$$($(1)).grpredefinesyms : $$($(1)).grpallsyms
	@echo "Generate redefinitions for groups $$@"
	@test -d $$(@D) || mkdir -p $$(@D)
	$$(ECHO) cat $$< | sort -n | uniq  | grep -v "__SglResMgrGlobal\|___globalTail" | sed 's/.*/& $(1)_&/g'> $$@

$$($(1)).lnkcmds_tmp : $$($(1)).grpsyms $$($(1)_FILT_LIBS)
	@test -d $$(@D) || mkdir -p $$(@D)
	$$(ECHO) echo $$(LdCommandLineFileHeader) --start-group $$($(1)_LIBS) --end-group > $$@
	$$(ECHO) cat $$< | sort -n | uniq | sed 's/.*/-u &/g' >> $$@

$$($(1)).lnkcmds : $$($(1)).lnkcmds_tmp
	$$(ECHO) tr '\n' ' ' < $$< > $$@

$$($(1)).mvlibG : $$($(1)).lnkcmds $$($(1)_FILT_LIBS)
	@test -d $$(@D) || mkdir -p $$(@D)
	$$(ECHO) $$(LD) @$$< -o $$(call RESOLVE_TARGET_PATH,$$@)

#Creating library group and making sure the global ResMgr and globalTail symbols are weakened so that the Leon ones are strongest
$$($(1)).libgroup : $$($(1)).mvlibG $$($(1)).grpredefinesyms
	@test -d $$(@D) || mkdir -p $$(@D)
	$$(ECHO) $$(OBJCOPY) --prefix-alloc-sections=.shvZ.$(1). --redefine-syms=$$($(1)).grpredefinesyms -W __SglResMgrGlobal -W ___globalTail \
	$$< $$(call RESOLVE_TARGET_PATH,$$@)

$$($(1)).shvZdata : $$($(1)).mvlibG
	@echo "Creating dynamically loadable library group data : $$@"
	@test -d $$(@D) || mkdir -p $$(@D)
	$$(ECHO) $$(LD) $$(LD_ENDIAN_OPT) $$(V_GRP_LDDYNOPT) $$< -o $$@

$$($(1))_shvZdata.o : $$($(1)).shvZdata
	@test -d $$(@D) || mkdir -p $$(@D)
	$$(ECHO) $$(OBJCOPY)  -I binary --rename-section .data=.ddr.data \
	--redefine-sym  _binary_$$(subst /,_,$$(subst .,_,$$<))_start=$$(subst .shvZdata,,$$(notdir $$<))grpdyndata \
	-O elf32-littlesparc -B sparc $$< $$@

#$$($(1)).shvZdatacomplete : $$($(1)).mvlibG
#	@echo "Creating windowed library for symbol extraction $$@"
#	$$(ECHO) $$(LD) $$(LD_ENDIAN_OPT) $$(V_GRP_LDSYNOPT) $$< -o $$@
#
#$$($(1))_shvZdata_sym.o : $$($(1)).shvZdatacomplete
#	@echo "Creating symbols file for shave group dynamic loading section $$(call RESOLVE_TARGET_PATH,$$@)"
#	$$(ECHO) $$(OBJCOPY) --prefix-symbols=$$(lastword $$(filter-out shvZdatacomplete,$$(subst /, ,$$(subst ., ,$$<))))_ --extract-symbol $$(call RESOLVE_TARGET_PATH,$$<) $$(call RESOLVE_TARGET_PATH,$$@)

PROJECTCLEAN += $$($(1)).grpsyms $$($(1)).grpallsyms
PROJECTCLEAN += $$($(1)).lnkcmds_tmp $$($(1)).lnkcmds
PROJECTCLEAN += $$($(1)).mvlibG $$($(1)).libgrouptemp
PROJECTCLEAN += $$($(1)).libgroup $$($(1)).shvZdata
PROJECTCLEAN += $$($(1)).shvZdatacomplete $$($(1))_shvZdata.o
PROJECTCLEAN += $$($(1))_shvZdata_sym.o

endef

$(foreach group,$(MV_SHAVE_GROUPS),$(eval $(call cGroupRules_template,$(group))))

###################################################################
#  END of Group handling part
###################################################################

###################################################################
#  START of App handling part -> but still using group name to reference things
###################################################################
#The mvlib rule is available from the main application file, so starting here from that point onward

#Creation of the application dyncontext object

define DYNCONTEXT_template
$$(DirAppObjBase)$(1)_dyncontext.o : $$(MV_COMMON_BASE)/swCommon/dynamicStructure/theDynContext.c $$($(1)_GROUP_DATA) $(2).shvXdata
	@echo "Create dyncontext for the current application $@"
	@test -d $$(@D) || mkdir -p $$(@D)
	$$(ECHO) $$(CC) -c $$(CONLY_OPT) $$(CCOPT) $$(cDynContextDefs_$(1)) \
	-D'APPDYNCONTEXTNAME=$(1)X_ModuleData' -D'APPDYNCONTEXTPRIVATENAME=$(1)X_ModuleDataPrivate'\
	-D'APPHEAPDYNCONTEXTADR=$(1)X_heap'  -D'APPGRPDATADYNCONTEXTADR=$(1)X_grpData'\
	-D'CTORSARRAY=$(1)X___init_array_start'\
	-D'INITARRAYEND=$(1)X___init_array_end'\
    -D'DTORSARRAY=$(1)X___fini_array_start'\
    -D'FINIARRAYEND=$(1)X___fini_array_end'\
	-D'APPGROUPDYNDATASECTIONSIZE=0x$$(shell $$(OBJDUMP) -xsrtd $$($(1)_GROUP_DATA) | grep dyn.data | grep -v section | cut -d ' ' -f 9)' \
	-D'APPCRITCMXTEXTSECTIONSIZE=0x0$$(shell $$(OBJDUMP) -xsrtd $(2).shvXdata | grep dyn.textCrit | grep -v section | grep -v : | cut -d ' ' -f 5)ul' \
	$(call RESOLVE_TARGET_PATH,$$<) -o $(call RESOLVE_TARGET_PATH,$$@)

endef

$(foreach app,$(shaveXApps),$(eval $(call DYNCONTEXT_template,$(lastword $(subst /, ,$(app))),$(app))))

#Have a special build rule for building the global array for dyncontexts as in the future we may want to use special rules here
$(DirAppObjBase)dynContextMaster.o : $(DirAppObjBase)dynContextMaster.c
	@echo "Create global dyncontext array for the current application $@"
	@test -d $(@D) || mkdir -p $(@D)
	$(ECHO) $(CC) -c $(CONLY_OPT) $(CCOPT) $(call RESOLVE_TARGET_PATH,$<) -o $(call RESOLVE_TARGET_PATH,$@)

#Create list of local application symbols
%.appsyms : %.mvlib
		$(ECHO) $(READELF) --syms --wide $< | grep -v Symbol | grep -v Vis | grep -v File | grep -v FILE | grep -v UND | sed 's/\s\s*/ /g' | cut -d ' ' -f 9 | grep . | sort -n | uniq  >> $(call RESOLVE_TARGET_PATH,$@)

#App symbols to redefine. Make sure to take out the global __SglResMgrGlobal and ___globalTail symbols, just in case they get their way in the .shvXlib too
%.appredefinesyms : %.appsyms
	$(ECHO) cat $< | sort -n | uniq | grep -v "__SglResMgrGlobal\|___globalTail" | sed 's/.*/& $(lastword $(filter-out appsyms,$(subst /, ,$(subst ., ,$<))))X_&/g'> $(call RESOLVE_TARGET_PATH,$@)

define cGroupAppXLibRules_template
#Create a shvXlib*redefsyms file which is a collection of appredefine syms and grpredefinesyms
#To take note: the main purpose this is a separate rule is to handle linkonce symbols. These
#are for example static functions declared in headers: they are allowed but when header is included in different
#C files, they will be linked twice. In C this may look slightly odd but in C++ this is actually very much used
#because many libraries wil declare template classes in headers for example
#So what we are doing is: we are keeping the local application present linkonce data.
#"Why not the library one?" -> one might ask. Because the library one is a mirror of whoever
#built the libraries, and they may have build them with flags different than your own apps. So:
#staying with the application one. Note: this doesn't affect cases where the library itself has a need
#to call it's own linkonce cases because this does not affect libgroup creation
%.shvXlib$(1)redefsyms : %.mvlib $$(patsubst %,%.appredefinesyms,$$(MV_$(1)_APPS)) $$($(1)).grpredefinesyms
	cat $$($(1)).grpredefinesyms $$(patsubst %.mvlib,%.appredefinesyms,$$<) | \
	sed 's/$$(lastword $$(filter-out mvlib,$$(subst /, ,$$(subst ., ,$$<))))/AA$$(lastword $$(filter-out mvlib,$$(subst /, ,$$(subst ., ,$$<))))/g' \
	| sed 's/$(1)/ZZ$(1)/g' \
	| sort | rev | uniq -f 1 | rev | sed 's/ZZ$(1)/$(1)/g' \
	| sed 's/AA$$(lastword $$(filter-out mvlib,$$(subst /, ,$$(subst ., ,$$<))))/$$(lastword $$(filter-out mvlib,$$(subst /, ,$$(subst ., ,$$<))))/g' > $$(call RESOLVE_TARGET_PATH,$$@)


#shvXlib creation by using the redefine syms list for the current application and the ones from the group. Make sure the GlobalTail and SglResMgrGlobal are weakened
%.shvXlib$(1) : %.shvXlib$(1)redefsyms
	@echo "Moving SHAVE application to its own group application type $$@"
	$$(ECHO) $$(OBJCOPY) --prefix-alloc-sections=.shvX.$$(lastword $$(filter-out shvXlib$(1)redefsyms,$$(subst /, ,$$(subst ., ,$$<)))). \
	--redefine-syms=$$(call RESOLVE_TARGET_PATH,$$<) \
	-W __SglResMgrGlobal -W ___globalTail \
	$$(patsubst %.shvXlib$(1),%.mvlib,$$@) $$(call RESOLVE_TARGET_PATH,$$@)
endef

$(foreach group,$(MV_SHAVE_GROUPS),$(eval $(call cGroupAppXLibRules_template,$(group))))

#Get the Application data
%.shvXdata : %.mvlib
	@echo "Creating dynamically loadable shave application data : $@"
	$(ECHO) $(LD) $(LD_ENDIAN_OPT) $(V_APP_LDDYNOPT) $(call RESOLVE_TARGET_PATH,$<) -o $(call RESOLVE_TARGET_PATH,$@)

%_shvXdata.o : %.shvXdata
	$(ECHO) $(OBJCOPY)  -I binary --rename-section .data=.ddr.data \
	--redefine-sym  _binary_$(subst /,_,$(subst .,_,$<))_start=$(subst .shvXdata,,$(notdir $<))appdyndata \
	-O elf32-littlesparc -B sparc $< $(call RESOLVE_TARGET_PATH,$@)

%.shvXdatacomplete : %.mvlib
	@echo "Creating windowed library for symbol extraction of loadable shave application $@"
	$(ECHO) $(LD) $(LD_ENDIAN_OPT) $(V_APP_LDSYNOPT) $(call RESOLVE_TARGET_PATH,$<) -o $(call RESOLVE_TARGET_PATH,$@)

%_shvXdata_sym.o : %.shvXdatacomplete
	@echo "Creating symbols file for shave app dynamic loading section $(call RESOLVE_TARGET_PATH,$@)"
	$(ECHO) $(OBJCOPY) --prefix-symbols=$(lastword $(filter-out shvXdatacomplete,$(subst /, ,$(subst ., ,$<))))_ --extract-symbol $(call RESOLVE_TARGET_PATH,$<) $(call RESOLVE_TARGET_PATH,$@)

#Add list of current group apps for cleaning
define cGroupAppXLibCleanRules_template
PROJECTCLEAN += $$(patsubst %,%.appsyms,$$(MV_$(1)_APPS))
PROJECTCLEAN += $$(patsubst %,%.appredefinesyms,$$(MV_$(1)_APPS))
PROJECTCLEAN += $$(patsubst %,%.shvXlib$(1),$$(MV_$(1)_APPS))
endef

$(foreach group,$(MV_SHAVE_GROUPS),$(eval $(call cGroupAppXLibCleanRules_template,$(group))))

PROJECTCLEAN += $(foreach group,$(MV_SHAVE_GROUPS),$(patsubst %,%.shvXdata,$(MV_$(group)_APPS)))
PROJECTCLEAN += $(foreach group,$(MV_SHAVE_GROUPS),$(patsubst %,%.shvXdatacomplete,$(MV_$(group)_APPS)))
PROJECTCLEAN += $(foreach group,$(MV_SHAVE_GROUPS),$(patsubst %,%_shvXdata.o,$(MV_$(group)_APPS)))
PROJECTCLEAN += $(foreach group,$(MV_SHAVE_GROUPS),$(patsubst %,%_shvXdata_sym.o,$(MV_$(group)_APPS)))
PROJECTCLEAN += $(foreach group,$(MV_SHAVE_GROUPS),$(patsubst %,%_dyncontext.o,$(MV_$(group)_APPS)))

###################################################################
#  END of App handling part
###################################################################

ifneq ($(shaveXApps),)
#Rule to create the required linkerscript for building the dynamically loaded libs
./$(DirAppOutput)/shvDynInfrastructureBase.ldscript :
	@echo "Create custom Shave Dyn. Data Loading linker script rules $@"
	@test -d $(@D) || mkdir -p $(@D)
	for appName in $(foreach app,$(shaveXApps),$(lastword $(subst /, ,$(subst ., ,$(app))))); \
	do cat $(MV_COMMON_BASE)/scripts/ld/shaveDynamicLoad/shaveDynLoadTemplate_App.ldscript | sed s/ZZZZZZZZZ/"$$appName"/g | sed s/YYYYYYYYY/"$$previous"/g >> $(DirAppOutput)/shvDynInfrastructureTemp.ldscript ; \
	previous=$$file;\
	done
	for grpName in $(foreach group,$(MV_SHAVE_GROUPS),$(lastword $(subst /, ,$(subst ., ,$(group))))); \
	do cat $(MV_COMMON_BASE)/scripts/ld/shaveDynamicLoad/shaveDynLoadTemplate_Group.ldscript | sed s/ZZZZZZZZZ/"$$grpName"/g | sed s/YYYYYYYYY/"$$previous"/g >> $(DirAppOutput)/shvDynInfrastructureTemp.ldscript ; \
	previous=$$file;\
	done
	$(ECHO) cat $(DirAppOutput)/shvDynInfrastructureTemp.ldscript | sed s/"LOADADDR (S.lrt.shvCOM.cmx.data.Shave) + SIZEOF(S.lrt.shvCOM.cmx.data.Shave)"/"0xE0000000"/ > $(DirAppOutput)/shvDynInfrastructureBase.ldscript
	@rm -rf $(DirAppOutput)/shvDynInfrastructureTemp.ldscript
endif

###################################################################
# LeonRT Build Rules (Myriad2 Specific)
###################################################################
# Now we have to provide crti, crtbegin, crtn and crtend for RTEMS 4.10.99 MV_RTEMS_PROLOGUE and MV_RTEMS_EPILOGUE
# As both Leons may have different compiling options they must use different objects
$(LEON_RT_LIB_NAME).mvlib : $(LEON_RT_APP_OBJS) $(LEON_SHARED_OBJECTS_REQUIRED_LRT) $(ALL_SHAVE_APPS) $(DefaultSparcRTEMSLibsRT)
	$(ECHO) $(LD) $(LD_ENDIAN_OPT) $(LRT_PROJECT_LIBS) $(patsubst %,-L %,$(DirSparcDefaultLibs)) -Ur $(MV_RTEMS_PROLOGUE_LRT) $(LEON_RT_APP_OBJS) $(ALL_SHAVE_APPS) $(LEON_SHARED_OBJECTS_REQUIRED_LRT) --start-group $(DefaultSparcRTEMSLibsRT) $(MV_RTEMS_EPILOGUE_LRT) --end-group $(DefaultSparcGccLibs) -o $(call RESOLVE_TARGET_PATH,$@)

$(DirAppObjBase)%_lrt.o : %.c $(LEON_HEADERS_LRT) Makefile
	$(call PRINT_USING_CCACHE)
	@echo "LeonRT CC : $<"
	@test -d $(@D) || mkdir -p $(@D)
	$(ECHO) $(CC) -c $(CONLY_OPT) $(CCOPT_LRT) $(call RESOLVE_TARGET_PATH,$<) -o $(call RESOLVE_TARGET_PATH,$@)

$(DirAppObjBase)%_lrt.o: %.cpp $(LEON_HEADERS_LRT) Makefile
	$(call PRINT_USING_CCACHE)
	@echo "LeonRT CC : $<"
	@test -d $(@D) || mkdir -p $(@D)
	$(ECHO) $(CC) -c $(CPPONLY_OPT) $(filter-out -Werror-implicit-function-declaration,$(CCOPT_LRT)) -fno-rtti -fno-exceptions  $(call RESOLVE_TARGET_PATH,$<) -o $(call RESOLVE_TARGET_PATH,$@)

$(DirAppObjBase)%_lrt.o : %.S $(LEON_HEADERS_LRT) Makefile
	$(call PRINT_USING_CCACHE)
	@echo "LeonRT ASM: $<"
	@test -d $(@D) || mkdir -p $(@D)
	$(ECHO) $(CC) -c $(CCOPT_LRT) -DASM $(call RESOLVE_TARGET_PATH,$<) -o $(call RESOLVE_TARGET_PATH,$@)

include $(MV_COMMON_BASE)/individual_file_options.mk

ifneq ($(shaveXApps),)
# If there are dynamicaly loaded applications then
# redefine symbols that have to be the same for LOS and LRT processors, by removing the "lrt_" prefix

#For each app, create the relevant symuniq separate list
%.lrtappsyms : %.appsyms
	@echo "Generate LeonRT symbol uniquificator for application $@"
	$(ECHO) cat $< | sort -n | uniq | sed 's/.*/lrt_$(lastword $(filter-out appsyms,$(subst /, ,$(subst ., ,$<))))X_& $(lastword $(filter-out appsyms,$(subst /, ,$(subst ., ,$<))))X_&/g' > $(call RESOLVE_TARGET_PATH,$@)
	$(ECHO) echo lrt_$(lastword $(filter-out appsyms,$(subst /, ,$(subst ., ,$<))))appdyndata $(lastword $(filter-out appsyms,$(subst /, ,$(subst ., ,$<))))appdyndata >> $(call RESOLVE_TARGET_PATH,$@)
	$(ECHO) echo lrt_$(lastword $(filter-out appsyms,$(subst /, ,$(subst ., ,$<))))grpdyndata $(lastword $(filter-out appsyms,$(subst /, ,$(subst ., ,$<))))grpdyndata >> $(call RESOLVE_TARGET_PATH,$@)
	$(ECHO) echo lrt_$(lastword $(filter-out appsyms,$(subst /, ,$(subst ., ,$<))))X_ModuleData $(lastword $(filter-out appsyms,$(subst /, ,$(subst ., ,$<))))X_ModuleData >> $(call RESOLVE_TARGET_PATH,$@)

%.lrtgrpsyms : %.grpsyms
	@echo "Generate LeonRT symbol uniquificator for group $@"
	$(ECHO) cat $< | sort -n | uniq | sed 's/.*/lrt_$(lastword $(filter-out grpsyms,$(subst /, ,$(subst ., ,$<))))_& $(lastword $(filter-out grpsyms,$(subst /, ,$(subst ., ,$<))))_&/g' > $(call RESOLVE_TARGET_PATH,$@)
	$(ECHO) echo lrt_$(lastword $(filter-out grpsyms,$(subst /, ,$(subst ., ,$<))))grpdyndata $(lastword $(filter-out grpsyms,$(subst /, ,$(subst ., ,$<))))grpdyndata >> $(call RESOLVE_TARGET_PATH,$@)

$(DirAppOutput)/shaveXApps.symuniq : $(patsubst %,%.lrtappsyms,$(foreach group,$(MV_SHAVE_GROUPS),$(MV_$(group)_APPS))) $(patsubst %,%.lrtgrpsyms,$(foreach group,$(MV_SHAVE_GROUPS),$($(group))))
	@echo "Generate LeonRT master symbol uniquificator $@"
	@test -d $(@D) || mkdir -p $(@D)
	cat $(patsubst %,%.lrtappsyms,$(foreach group,$(MV_SHAVE_GROUPS),$(MV_$(group)_APPS))) > $(call RESOLVE_TARGET_PATH,$@)
	cat $(patsubst %,%.lrtgrpsyms,$(foreach group,$(MV_SHAVE_GROUPS),$($(group)))) >> $(call RESOLVE_TARGET_PATH,$@)

%.rtlib : %.rtlibtemp $(DirAppOutput)/shaveXApps.symuniq
	@echo "Generating final LeonRT library $@"
	$(ECHO) $(OBJCOPY) --redefine-syms=$(DirAppOutput)/shaveXApps.symuniq --redefine-sym lrt___SglResMgrGlobal=__SglResMgrGlobal -W __SglResMgrGlobal --redefine-sym lrt_GlobalContextData=GlobalContextData -W GlobalDynContextData --redefine-sym lrt____globalTail=___globalTail -W ___globalTail $(call RESOLVE_TARGET_PATH,$<) $(call RESOLVE_TARGET_PATH,$@)
else
# Redefine symbols that have to be the same for LOS and LRT processors, by removing the "lrt_" prefix
%.rtlib : %.rtlibtemp
	$(ECHO) $(OBJCOPY) --redefine-sym lrt___SglResMgrGlobal=__SglResMgrGlobal -W __SglResMgrGlobal --redefine-sym lrt_mvShavePipePrintQueueAdd=mvShavePipePrintQueueAdd -W mvShavePipePrintQueueAdd  --redefine-sym lrt_mvShavePipePrintQueueGet=mvShavePipePrintQueueGet -W mvShavePipePrintQueueGet  --redefine-sym lrt____globalTail=___globalTail -W ___globalTail $(call RESOLVE_TARGET_PATH,$<) $(call RESOLVE_TARGET_PATH,$@)
endif
%.rtlibtemp : %.mvlib
	@echo Generating unique instance of leon_rt application: $(call RESOLVE_TARGET_PATH,$@)
	$(ECHO) $(OBJCOPY) --prefix-alloc-sections=.lrt --prefix-symbols=lrt_ $(call RESOLVE_TARGET_PATH,$<) $(call RESOLVE_TARGET_PATH,$@)

endif # ifneq ($(filter clean,$(MAKECMDGOALS))

###################################################################
#       Including some specific build rules we need for all       #
###################################################################
-include $(MV_COMMON_BASE)/setupbinaries.mk
include $(MV_COMMON_BASE)/commonbuilds.mk
include $(MV_COMMON_BASE)/mbinflow.mk
include $(MV_COMMON_BASE)/functional_targets.mk
-include $(MV_COMMON_BASE)/inhousetargets.mk
ifeq    ($(findstring PwrManager,$(ComponentList)),PwrManager)
include $(MV_COMMON_BASE)/pwr_manager.mk
endif

localclean:
	@echo "Cleaning project specific built files."
	$(foreach item,$(LEON_APP_OBJECTS_REQUIRED),$(call SAFE_RM,$(item)))
	$(foreach item,$(LEON_RT_APPS),$(call SAFE_RM,$(item)))
	$(foreach item,$(LEON_APP_URC_C_REQUIRED),$(call SAFE_RM,$(item)))
	$(foreach item,$(LEON_APP_URC_H_REQUIRED),$(call SAFE_RM,$(item)))
	$(foreach item,$(LEON_RT_APP_OBJS),$(call SAFE_RM,$(item)))
	$(foreach item,$(PROJECTCLEAN),$(call SAFE_RM,$(item)))
	$(foreach item,$(ALL_SHAVE_APPS),$(call SAFE_RM,$(item)))
	$(foreach item,$(ALL_SHAVE_DYN_APPS),$(call SAFE_RM,$(item)))
	$(foreach item,$(DirAppObjBase)dynContextMaster.c $(DirAppObjBase)dynContextMaster.o,$(call SAFE_RM,$(item)))
	$(call SAFE_RM, $(foreach groupMK,$(MV_SHAVE_GROUPS),appsGrpDynContextRules$(groupMK).mk))
	$(foreach item,$(LEON_RT_LIB_NAME).rtlib,$(call SAFE_RM,$(item)))
	$(foreach item,$(LEON_RT_LIB_NAME).rtlibtail,$(call SAFE_RM,$(item)))
	$(foreach item,$(LEON_RT_LIB_NAME).rtlibtemp,$(call SAFE_RM,$(item)))
	$(foreach item,$(SHAVE_APP_LIBS),$(call SAFE_RM,$(item)))
	$(foreach item,$(LEON_RT_LIB_NAME).mvlib,$(call SAFE_RM,$(item)))
	$(foreach item,$(DirAppOutput)/$(APPNAME).map,$(call SAFE_RM,$(item)))
	$(foreach item,$(CommonMlibFile).mlib,$(call SAFE_RM,$(item)))
	$(foreach item,$(CommonMlibFile).mvlib,$(call SAFE_RM,$(item)))
	$(foreach item,$(ELF_FILE),$(call SAFE_RM,$(item)))
	$(foreach item,$(DirAppOutput)/$(APPNAME).mvcmd,$(call SAFE_RM,$(item)))
	$(foreach item,$(DirAppOutput)/lst $(DirAppOutput)/$(APPNAME)_leon.sym,$(call SAFE_RM,$(item)))

# Clean split into different lines to avoid "execvp: /bin/bash: Argument list too long"
clean:
	@echo "Cleaning all built files from the MDK distribution and all project built files."
	$(foreach item,$(SHAVE_COMPONENT_OBJS),$(call SAFE_RM,$(item)))
	$(foreach item,$(LEON_SHARED_OBJECTS_REQUIRED),$(call SAFE_RM,$(item)))
	$(foreach item,$(LEON_SHARED_OBJECTS_REQUIRED_LRT),$(call SAFE_RM,$(item)))
	$(foreach item,$(PROJECTCLEAN),$(call SAFE_RM,$(item)))
ifeq ($(MAKEDEPEND),yes)
	$(foreach item,$(LEON_APP_DEPS),$(call SAFE_RM,$(item)))
	$(foreach item,$(LEON_RT_DEPS),$(call SAFE_RM,$(item)))
	$(foreach item,$(SHAVE_APP_DEPS),$(call SAFE_RM,$(item)))
endif
	$(foreach item,$(LEON_APP_OBJECTS_REQUIRED),$(call SAFE_RM,$(item)))
	$(foreach item,$(LEON_RT_APP_OBJS),$(call SAFE_RM,$(item)))
	$(foreach item,$(LEON_RT_APPS),$(call SAFE_RM,$(item)))
	$(foreach item,$(LEON_APP_URC_C_REQUIRED),$(call SAFE_RM,$(item)))
	$(foreach item,$(LEON_APP_URC_H_REQUIRED),$(call SAFE_RM,$(item)))
	$(call SAFE_RM, $(foreach groupMK,$(MV_SHAVE_GROUPS),appsGrpDynContextRules$(groupMK).mk))
	$(foreach item,$(LEON_RT_LIB_NAME).mvlib,$(call SAFE_RM,$(item)))
	$(foreach item,$(LEON_RT_LIB_NAME).rtlib,$(call SAFE_RM,$(item)))
	$(foreach item,$(LEON_RT_LIB_NAME).rtlibtail,$(call SAFE_RM,$(item)))
	$(foreach item,$(LEON_RT_LIB_NAME).rtlibtemp,$(call SAFE_RM,$(item)))
	$(foreach item,$(SH_SWCOMMON_OBJS),$(call SAFE_RM,$(item)))
	$(foreach item,$(SH_SWCOMMON_GENASMS),$(call SAFE_RM,$(item)))
	$(foreach item,$(SHAVE_APP_LIBS),$(call SAFE_RM,$(item)))
	$(foreach item,$(ALL_SHAVE_APPS),$(call SAFE_RM,$(item)))
	$(foreach item,$(ALL_SHAVE_DYN_APPS),$(call SAFE_RM,$(item)))
	$(foreach item,$(DirAppObjBase)dynContextMaster.c $(DirAppObjBase)dynContextMaster.o,$(call SAFE_RM,$(item)))
	$(call SAFE_RM,$(patsubst %,%.lrtgrpsyms,$(foreach group,$(MV_SHAVE_GROUPS),$($(group)))))
	$(call SAFE_RM,$(patsubst %,%.lrtappsyms,$(foreach group,$(MV_SHAVE_GROUPS),$(MV_$(group)_APPS))))
	$(foreach item,$(ALL_SHAVE_TEMP_APPS),$(call SAFE_RM,$(item)))
	$(foreach item,$(DirAppOutput)/$(APPNAME).map,$(call SAFE_RM,$(item)))
	$(foreach item,$(ELF_FILE),$(call SAFE_RM,$(item)))
	$(foreach item,$(DirAppOutput)/$(APPNAME).mvcmd,$(call SAFE_RM,$(item)))
	$(foreach item,$(CommonMlibFile).mlib,$(call SAFE_RM,$(item)))
	$(foreach item,$(CommonMlibFile).mvlib,$(call SAFE_RM,$(item)))
	$(foreach item,$(ProjectShaveLib),$(call SAFE_RM,$(item)))
	$(foreach item,$(DirAppOutput)/*,$(call SAFE_RM,$(item)))
	$(foreach item,$(DirAppOutput)/$(APPNAME)_leon.sym,$(call SAFE_RM,$(item)))


.INTERMEDIARY: $(SH_SWCOMMON_GENASMS) $(PROJECTINTERM)
