#include <Port.hpp>
#include <Node.hpp>
#include <stdio.h>


using namespace DataFlowFramework;
using namespace std;
#define printf(...)

Node::Node(void *context, int priority) : context_(context), priority_(priority), runCondition_(false), tid_(0)
{
    in.clear();
    out.clear();
}

Port * Node::getPortByName(std::vector<Port*> &p, const std::string &name)
{
    printf("GetPortByName\n");
    vector<Port*>::iterator it;
    for(it=p.begin() ; it < p.end(); it++)
    {
        if ((*it)->checkName(name))
            return &(**it);
    }
    printf("GetPortByName not found\n");
    return nullptr;
}

void Node::getPortByName(InPort ** p, const std::string &name)
{
    printf("GetInPortByName\n");

    *p = (InPort* )getPortByName( in, name);
}

void Node::getPortByName(OutPort ** p, const std::string &name)
{
    printf("GetOutPortByName\n");

    *p = (OutPort* )getPortByName( out, name);
}

void* Node::DefaultFuncStub(void* _self) {
    Node* self = (Node*)(_self);
    int rc = self->run(self->context_, &self->runCondition_);
    return (void*)rc;

}


int Node::start()
{
    pthread_attr_t attr;
    int sc;

    runCondition_ = true;
    printf("Starting Thread\n");
    if(pthread_attr_init(&attr) !=0) {
        printf("pthread_attr_init error\n");
    }
    if(pthread_attr_setinheritsched(&attr, PTHREAD_EXPLICIT_SCHED) != 0) {
        printf("pthread_attr_setinheritsched error\n");
    }
    if(pthread_attr_setschedpolicy(&attr, SCHED_RR) != 0) {
        printf("pthread_attr_setschedpolicy error\n");
    }
    sc=pthread_create( &this->tid_, &attr, &DefaultFuncStub, this);
    if (sc) {
        printf("Control Thread creation failed\n");
    }
    struct sched_param schedparam;
    schedparam.sched_priority = priority_;
    if(pthread_setschedparam(this->tid_, SCHED_RR, &schedparam) != 0) {
        printf("pthread_attr_setschedpolicy error\n");
    }
    return 0;
}

int Node::stop()
{
    int sc;
    void *rc;
    printf("Waiting Thread\n");

    runCondition_ = false;
    sc = pthread_join(this->tid_, &rc);
    if (sc) {
        printf("Control Thread join failed\n");
    }
    printf("Waiting Thread\n");

    return 0;
}
