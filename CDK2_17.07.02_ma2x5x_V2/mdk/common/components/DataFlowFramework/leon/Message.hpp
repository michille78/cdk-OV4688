#ifndef __MESSAGE_HPP__
#define __MESSAGE_HPP__

#include <cstddef>
#include <stdio.h>
#include <vector>

#include <Port.hpp>
#include <rtems.h>
#include <assert.h>

typedef enum PayloadType_t
{
    FRAME_PUMP_BUFF = 0,
    FRAME_BUFF,
    META,
    IMU,
}PayloadType_t;

namespace DataFlowFramework {
    class OutPort;
    class Payload
    {
    private:
        OutPort * parent_;
        int refcount;

    public:
        void* data_;
        size_t size_;
        PayloadType_t _type;
        Payload(void* data, size_t size,  OutPort* parent, PayloadType_t type)
           :
             parent_(parent),refcount(0), data_(data),size_(size),  _type(type){
        }
        ~Payload(){};
        // this port will be created when the message is created.
        // The plugin which will accept the free Message will have it's input port linked to this Port
        // when releasing this message back to it's parent we need to push into this port
        // pushing back through this port will increase the refCount. Means that the message is used by the source plugin.
        //sending back the current message trough the parent port.
        int returnToParent();
        // we may extend these with flags in the future
        int decRefCount();
        int incRefCount();
    };
    class AbstractMessage
    {

    };
    class Message :AbstractMessage{
    private:
        std::vector<Payload*> msgList;
        std::vector<Payload*>::iterator currentPayload;

        // we may extend these with flags in the future
        int decRefCount();
        int incRefCount();
        std::vector<Payload*>::iterator releasePayload(std::vector<Payload*>::iterator it);
        Message* dup();
        Message(){
            msgList.clear();
            currentPayload = msgList.begin();
        }

    public:
        Message(void* data, size_t size,  OutPort* parent, PayloadType_t type){
            msgList.clear();
            Payload* pl = new Payload(data, size, parent, type);
            assert(pl);
            msgList.push_back(pl);
            currentPayload = msgList.begin();
        }
        Message(Payload* pl){
            msgList.clear();
            msgList.push_back(pl);
            currentPayload = msgList.begin();
        }
        ~Message(){
        };

        Payload* getNextPayload();
        int compose(Message* m);
        int remove(Message* m);
        // releasing a message
        int release();
        // blocking a message (usually done by Port)
        Message* acquire();

    };

}
#endif
