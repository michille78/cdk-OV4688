#ifndef __PORT_HPP__
#define __PORT_HPP__
#include <Message.hpp>
#include <CommQueue.hpp>

#include <string>
#include <vector>

namespace DataFlowFramework {

    class CommQueue;
    class InPort;
    class OutPort;
    class Message;

    class Port {
    protected:
        std::string name_;
        std::vector<CommQueue *>link_;
        //TODO: the link should get a parameter to select the link type. It may also detect it from the PortType
        // imagine a RemoteProcessorPort
        int link(Port &remote, unsigned int size);


    public:
        Port(const std::string &name) : name_(name){
            link_.clear();
        }
        ~Port(){};
        bool checkName(const std::string &name) { return name == this->name_;}
    };


    class InPort : public Port {
    public:
        InPort(const std::string &name) : Port(name){}
        //TODO: the link should get a parameter to select the link type. It may also detect it from the PortType
        // imagine a RemoteProcessorPort
        int link(OutPort &remote, unsigned int size);
        virtual Message *pop(void);
        Message *top(void);
        virtual ~InPort(){}
    };
    class SerialInPort : public InPort {
        public:
            SerialInPort(const std::string &name) : InPort(name){
                currentQueue = 0;
            }
            //TODO: the link should get a parameter to select the link type. It may also detect it from the PortType
            // imagine a RemoteProcessorPort
            int link(OutPort &remote, unsigned int size);
            virtual Message *pop(void);
            Message *top(void);
            virtual ~SerialInPort(){}
        private:
            unsigned int currentQueue; //round-robin input queue priority
        };
    class OutPort : public Port {
    private:
        std::string name_;

    public:
        OutPort(const std::string &name) : Port(name){}
        int link(InPort &remote, unsigned int size);
        int push (Message *m);
    };

    class ReleasePort : public Port {
    private:
        std::string name_;

    public:
        int push (Message *m);
    };

}


#endif
