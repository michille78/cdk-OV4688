#include <CommQueue.hpp>
#include <OneToOneCommQueue.hpp>
#include <mqueue.h>
#include <fcntl.h>           /* For O_* constants */
#include <sys/stat.h>        /* For mode constants */
#include <time.h>

using namespace DataFlowFramework;

int CommQueue::refcount_ = 0;
#define printf(...)

OneToOneCommQueue::OneToOneCommQueue(unsigned int size) : msgq_(0)
{
    refcount_++;
    name ="CommQueue";
    name += refcount_;
    //TODO: protect this;
    createMessageQueue(size);
}


int OneToOneCommQueue::createMessageQueue(int count)
{
    struct mq_attr m_attr;
    m_attr.mq_maxmsg = count;
    m_attr.mq_msgsize = sizeof(Message *);
    msgq_ = mq_open(name.c_str(), O_RDWR | O_CREAT,
                    S_IRWXU | S_IRWXG, &m_attr);
    if (msgq_ == (mqd_t) -1)
    {
        return -1;
    }

    return 0;
}

int OneToOneCommQueue::link_input(Port *in)
{
    (void)in;
    return 0;

}

int OneToOneCommQueue::link_output(Port *out)
{
    (void)out;
    return 0;

}

int OneToOneCommQueue::push (Message *m)
{
    int sc;
    printf("CommQueue push %x %x\n", this, msgq_);
    struct timespec timeout;
    //set a timeout which already expired. If queue is full, we will drop the frame
    timeout.tv_sec = 0;
    timeout.tv_nsec = 0;
    //TODO: implement proper drop
    Message* localMsg;
    sc = mq_timedsend(msgq_, (const char *)&m, sizeof(Message*), 0, &timeout);
    if (sc)
    {
        printf("Buffer full....................... Let's release\n");
        sc = mq_timedreceive(msgq_, (char *)&localMsg, sizeof(Message*), 0, &timeout);
        if (sc)
        {
            localMsg->release();
            sc = mq_timedsend(msgq_, (const char *)&m, sizeof(Message*), 0, &timeout);
            if (sc)
            {
                m->release();
            }
        }
        //mvLog(MVLOG_ERROR, "cannot send fisheye to DoG FREAK thread %d", errno);
        return -1;
    }

    return 0;
}

Message * OneToOneCommQueue::pop(void)
{
    printf("CommQueue pop\n");

    Message *m = nullptr;
    int sc;
    struct timespec timeout;
    timeout.tv_sec = 0;
    clock_gettime(CLOCK_REALTIME, &timeout);
    timeout.tv_nsec += 200*1000*1000;
    if (timeout.tv_nsec > 1000*1000*1000)
    {
        timeout.tv_sec+=1;
        timeout.tv_nsec-=1000*1000*1000;
    }
    sc = mq_timedreceive(msgq_, (char*)&m, sizeof(Message*), 0, &timeout);
    if (sc < 0)
    {
        printf("CommQueue pop failed\n");
        //continue;
        return nullptr;
    }

    return m;
}

Message * OneToOneCommQueue::trypop(void)
{
    printf("CommQueue pop\n");

    Message *m = nullptr;
    int sc;
    struct timespec timeout;
    //set a timeout which already expired. We just want to try to pop
    timeout.tv_sec = 0;
    timeout.tv_nsec = 0;

    sc = mq_timedreceive(msgq_, (char*)&m, sizeof(Message*), 0, &timeout);
    if (sc < 0)
    {
        //continue;
        return nullptr;
    }

    return m;
}
Message * OneToOneCommQueue::top(void)
{
    return nullptr;
}
