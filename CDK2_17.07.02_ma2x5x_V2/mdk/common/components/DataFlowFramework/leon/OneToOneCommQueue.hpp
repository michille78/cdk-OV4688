#ifndef __ONE_TO_ONE_COMM_QUEUE_HPP__
#define __ONE_TO_ONE_COMM_QUEUE_HPP__

#include <Message.hpp>
#include <CommQueue.hpp>

#include <mqueue.h>
#include <fcntl.h>           /* For O_* constants */
#include <sys/stat.h>        /* For mode constants */

namespace DataFlowFramework {

    class OneToOneCommQueue : public CommQueue{
    private:
        mqd_t msgq_;

        int createMessageQueue(int count);

    public:
        OneToOneCommQueue(unsigned int size);
        ~OneToOneCommQueue(){};

        int push (Message *m);
        Message* pop(void);
        Message* trypop(void);

        Message* top(void);

        int link_input(Port *in);
        int link_output(Port *out);
    };
}

#endif
