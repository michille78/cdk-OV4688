#include <OneToOneCommQueue.hpp>
#include <Port.hpp>

#include <string>
#include <vector>
#include <assert.h>
#include <rtems.h>

using namespace DataFlowFramework;
#define printf(...)
int OutPort::push (Message *m)
{
    printf("Port push, nr of queues %d\n", link_.size());
    if(link_.empty())
    {
        printf("Port release\n");

//        m->release();
        return 0;
    }
    else
    {
        std::vector<CommQueue*>::iterator it;
        for(it=link_.begin() ; it < link_.end(); it++)
        {
            (*it)->push( m->acquire());
        }
    }
    return 0;
}

Message * InPort::pop (void)
{
    printf("Port pop, nr of queues %d \n", link_.size());
    printf("Port pop nr of queues %d %lx!\n", link_.size(), rtems_task_self());

    if(link_.empty())
        return nullptr;

    Message* msg = nullptr;
    std::vector<CommQueue*>::iterator it;
    for(it=link_.begin() ; it < link_.end(); it++)
    {
        Message* newm = (*it)->pop();
        printf("Port pop %lx!\n", newm);
        if (msg == nullptr)
            msg = newm;
        else
        {
            msg->compose(newm);
            newm->release();
        }

    }

    return msg;
}

Message * InPort::top (void)
{
    //TODO: implement
    return nullptr;
}

Message * SerialInPort::pop (void)
{
    printf("SerialPort pop, nr of queues %d \n", link_.size());
    printf("SerialPort pop nr of queues %d %lx!\n", link_.size(), rtems_task_self());

    if(link_.empty())
        return nullptr;

    while(1)
    {
        if (currentQueue >= link_.size())
            currentQueue = 0;
        while(currentQueue < link_.size())
        {
            Message* newm = link_[currentQueue]->trypop();
            currentQueue++;
            if (newm)
                return newm;
        }
        //TODO: proper wait of messages
        usleep(1000);
    }
    return nullptr;
}

Message * SerialInPort::top (void)
{
    //TODO: implement
    return nullptr;
}
int Port::link(Port &remote, unsigned int size)
{
    printf("Port link\n");
//    if(link_.empty() &&
//       remote.link_.empty())
//    {
        printf("creating link\n");
        OneToOneCommQueue* q = new OneToOneCommQueue(size);
        link_.push_back(q);
        remote.link_.push_back(q);
//    }
//    else if (link_.empty() && !remote.link_ .empty())
//    {
//        assert(0);
////        link_ = remote.link_;
//    }
//    else if (!link_.empty() && remote.link_.empty())
//    {
//        assert(0);
////        remote.link_ = link_;
//    }
    return 0;
}

int OutPort::link(InPort &remote, unsigned int size)
{
    Port::link(remote, size);

//    remote.link_->link_input(this);
//    link_->link_output(this);
    return 0;
}

int InPort::link(OutPort &remote, unsigned int size)
{
    Port::link(remote, size);

//    link_->link_input(this);
//    remote.link_->link_output(this);
    return 0;
}
