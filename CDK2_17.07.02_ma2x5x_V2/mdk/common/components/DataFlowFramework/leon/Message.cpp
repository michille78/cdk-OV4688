#include <Message.hpp>
#include  <algorithm>
#include  <rtems.h>

//#include <Port.hpp>
using namespace DataFlowFramework;

#define printf(...)

std::vector<Payload*>::iterator Message::releasePayload(std::vector<Payload*>::iterator it)
{

    if ((*it)->decRefCount()){
        // the message must be removed from list if decRefCount returned true
        return msgList.erase(it);

    }

    return it+1;
}
int Message::decRefCount()
{
    std::vector<Payload*>::iterator it;
    for(it=msgList.begin() ; it < msgList.end(); )
    {
        it = releasePayload(it);
    }
    currentPayload = msgList.begin();
    return 0;

}
int Message::incRefCount()
{
    std::vector<Payload*>::iterator it;
    for(it=msgList.begin() ; it < msgList.end(); it++)
    {
        (*it)->incRefCount();
    }
    return 0;
}
Payload* Message::getNextPayload(){

    if (currentPayload < msgList.end()){
        printf("giving next payload\n");
        Payload* pl = *currentPayload;
        currentPayload++;
        return pl;
    }
    printf("giving next NULL payload %d\n", msgList.size());
    return nullptr;
}

int Message::compose(Message* msg)
{
    printf("Message compose\n");

    if (!msg)
    {
        printf("Can't compose\n");

        return 0;
    }
    std::vector<Payload*>::iterator it;
    for(it=msg->msgList.begin() ; it < msg->msgList.end();it++)
    {
        if (std::find(msgList.begin(), msgList.end(),*it)!=msgList.end())
        {
            printf("releasing\n");
        }
        else
        {
            printf("composing\n");

            msgList.push_back(*it);
            (*it)->incRefCount();
        }
    }

    // TODO: this is ugly. our iterator becomes invalid once we push_back
    currentPayload = msgList.begin();
    return 0;
}

int Message::remove(Message* msg)
{
    //TODO:
    (void)msg;
    return 0;
}

int Message::release()
{
    printf("Message release\n");
    decRefCount();
    delete this;
    return 0;
}
#include <assert.h>
Message* Message::dup(){
    Message* cpy = new Message();

    assert(cpy);

    std::vector<Payload*>::iterator it;
    for(it=msgList.begin() ; it < msgList.end(); it++)
    {
        printf("having a payload\n");
        assert(*it);
        cpy->msgList.push_back(*it);
    }
    cpy->currentPayload = cpy->msgList.begin();

    return cpy;
}

Message* Message::acquire()
{
    printf("Message acquire\n");
    incRefCount();
    Message* cpy = dup();

    return cpy;
}


int Payload::returnToParent()
{
    printf("Message return\n");

    //TODO:
    if (refcount)
    {
        //shouldn't reach this. Encapsulation of the message class was broken
        return -1;
    }
    if (parent_)
    {
        Message* msg = new Message(this);
        parent_->push(msg);
        delete msg;
    }
    else
    {
        delete this;
    }
    return 0;
}
int Payload::decRefCount()
{
    int pil;
    rtems_interrupt_disable(pil);
    refcount--;
    printf("Payload refcount decreased to %d\n", refcount);
    if (refcount == 0){
        rtems_interrupt_enable(pil);

        returnToParent();
        return 1;
    }
    else{
        rtems_interrupt_enable(pil);
        return 0;

    }
}

int Payload::incRefCount()
{
    int pil;
    rtems_interrupt_disable(pil);
    refcount++;
    rtems_interrupt_enable(pil);

    printf("Payload refcount increased to %d\n", refcount);
    return 0;
}

