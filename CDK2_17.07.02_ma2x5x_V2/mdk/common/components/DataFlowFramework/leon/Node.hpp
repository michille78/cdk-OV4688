#ifndef __NODE_HPP__
#define __NODE_HPP__
#include <Port.hpp>
#include <vector>
#include <pthread.h>

namespace DataFlowFramework {

    class Node {
    private:

        void *context_;
        int priority_;
        bool runCondition_;
        pthread_t tid_;
        virtual int run(void *context, bool *runCondition) = 0;

        static void* DefaultFuncStub(void* _self);
        Port * getPortByName(std::vector<Port*> &p, const std::string &name);
    protected:
        std::vector<Port*> in;
        std::vector<Port*> out;
    public:
        Node(void *context, int priority);
        virtual ~Node(){};

        int start();
        int stop();
        void addInPort(const std::string &name){
            in.push_back(new InPort(name));
        }
        void addInPort(InPort* Port){
            in.push_back(Port);
        }
        void addOutPort(const std::string &name){
            out.push_back(new OutPort(name));
        }
        void addOutPort(OutPort* Port){
            out.push_back(Port);
        }
        //TODO: node should abstract the Port. Application should not know about Ports.
        void getPortByName(InPort ** p, const std::string &name);
        void getPortByName(OutPort ** p, const std::string &name);
        // TODO: These are the set of functions that we would need to abstract the Port from application.
        void link(Node* remote,
                  const std::string &localName, const std::string &remoteName, int linkSize, // in an ideal world the framework would do autowiring without these parameters
                  int linkType // having a CommQueue factory would let us choosing the link type when linking. "autowiring" could also do this, but that's maybe a too ideal world
                  );
        void sendAndRelease(Message* msg, const std::string &portName); // portName could be detected automatically by the Msg type
        void send(Message* msg, const std::string &portName); // portName could be detected automatically by the Msg type
        void receive(Message** msg, const std::string &portName);  // portName could be detected automatically by the Msg type

    };
}
#endif
