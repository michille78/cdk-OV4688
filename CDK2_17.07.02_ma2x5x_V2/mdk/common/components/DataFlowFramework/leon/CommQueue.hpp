#ifndef __COMM_QUEUE_HPP__
#define __COMM_QUEUE_HPP__

#include <Port.hpp>
#include <Message.hpp>
#include <string>

namespace DataFlowFramework {
    class Port;
    class Message;

    class CommQueue {
    private:

    protected:
        std::string name;
        static int refcount_;

    public:
        CommQueue(){};
        virtual ~CommQueue(){};

        virtual int push (Message *m) = 0;
        virtual Message *pop(void) = 0;
        virtual Message *top(void) = 0;
        virtual Message* trypop(void) = 0;

        virtual int link_input(Port *in) = 0;
        virtual int link_output(Port *out) = 0;
    };
}


#endif
