#ifndef __PORTAB_H__
#define __PORTAB_H__

#include "stdlib.h"
#include "stdio.h"
#ifdef MYRIAD2
 #include "swcCdma.h"
#endif

#if (defined MYRIAD1)

#define DMA_CREATE_TRANSACTION(_TaskList, _TaskNr, _NewTaskList, _ReqId, _Src, _Dst, _ByteLength)                       \
{                                                                                                                       \
    _TaskNr++;                                                                                                          \
    scDmaSetup(_TaskNr, (u8*)(_Src), (u8*)(_Dst), (_ByteLength));                                                       \
}
#define DMA_CREATE_TRANSACTION_SRC_STRIDE(_TaskList, _TaskNr, _NewTaskList, _ReqId, _Src, _Dst, _ByteLength, _LineWidth, _SrcStride)\
{                                                                                                                       \
    _TaskNr++;                                                                                                          \
    scDmaSetupStrideSrc(_TaskNr, (u8*)(_Src), (u8*)(_Dst), (_ByteLength), (_LineWidth), (_SrcStride));                  \
}
#define DMA_CREATE_TRANSACTION_DST_STRIDE(_TaskList, _TaskNr, _NewTaskList, _ReqId, _Src, _Dst, _ByteLength, _LineWidth, _DstStride)\
{                                                                                                                       \
    _TaskNr++;                                                                                                          \
    scDmaSetupStrideDst(_TaskNr, (u8*)(_Src), (u8*)(_Dst), (_ByteLength), (_LineWidth), (_DstStride));                  \
}
#define DMA_LINK_TWO_TASKS(_TaskList, _TaskNr)
#define DMA_START_LIST_TASK(_Task, _TaskNr)     scDmaStart(_TaskNr);
#define DMA_WAIT_TASK(_Task)                    scDmaWaitFinished();

#elif (defined MYRIAD2)

#define DMA_CREATE_TRANSACTION(_TaskList, _TaskNr, _NewTaskList, _ReqId, _Src, _Dst, _ByteLength)                       \
{                                                                                                                       \
    _TaskNr++;                                                                                                          \
    _TaskList[_TaskNr] = dmaCreateTransaction(_ReqId, &_NewTaskList[_TaskNr], (u8*)(_Src), (u8*)(_Dst), (_ByteLength)); \
}
#define DMA_CREATE_TRANSACTION_SRC_STRIDE(_TaskList, _TaskNr, _NewTaskList, _ReqId, _Src, _Dst, _ByteLength, _LineWidth, _SrcStride)\
{                                                                                                                       \
    _TaskNr++;                                                                                                          \
    _TaskList[_TaskNr] = dmaCreateTransactionSrcStride(_ReqId, &_NewTaskList[_TaskNr], (u8*)(_Src), (u8*)(_Dst), (_ByteLength), (_LineWidth), (_SrcStride)); \
}
#define DMA_CREATE_TRANSACTION_DST_STRIDE(_TaskList, _TaskNr, _NewTaskList, _ReqId, _Src, _Dst, _ByteLength, _LineWidth, _DstStride)\
{                                                                                                                       \
    _TaskNr++;                                                                                                          \
    _TaskList[_TaskNr] = dmaCreateTransactionDstStride(_ReqId, &_NewTaskList[_TaskNr], (u8*)(_Src), (u8*)(_Dst), (_ByteLength), (_LineWidth), (_DstStride)); \
}

#define DMA_LINK_TWO_TASKS(_TaskList, _TaskNr)  dmaLinkTasks(_TaskList[_TaskNr - 1], 1, _TaskList[_TaskNr]);
#define DMA_START_LIST_TASK(_Task, _TaskNr)     dmaStartListTask(_Task);
#define DMA_WAIT_TASK(_Task)                    dmaWaitTask(_Task);

#else
#error "Platform was not selected!!!"
#endif

#endif /*__PORTAB_H__*/
