#!/bin/bash

# this script is intended to be run in the directory containing the common
# components. That's $(MDK_INSTALL_DIR_ABS)/common/components

LOCAL_KCNF=kcnf

COMPONENTS_DIR=$(ls -d */ | grep -v "kernelLib")

for d in $COMPONENTS_DIR ; do
d=${d%%/}
d_UPPER=${d^^}
pushd $d > /dev/null
truncate -s 0 $LOCAL_KCNF
cat >> $LOCAL_KCNF <<KCNF_ENTRY
config USE_COMPONENT_$d_UPPER
  bool "$d"
  default n
  help
    Use component $d

KCNF_ENTRY

popd > /dev/null
done

# vim: set tw=0: set wrapmargin=0 :

