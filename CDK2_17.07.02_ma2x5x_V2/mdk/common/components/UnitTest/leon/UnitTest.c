///
/// @file
/// @copyright All code copyright Movidius Ltd 2012, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Unit Test Framework
///
/// Basic utility functions for unit testing
///
///

// 1: Includes
// ----------------------------------------------------------------------------
#include <assert.h>
#include <stdio.h>
#include <registersMyriad.h>
#include <DrvTimer.h>
#include "string.h"

// 2:  Source Specific #defines and types  (typedef,enum,struct)
// ----------------------------------------------------------------------------

// 3: Global Data (Only if absolutely necessary)
// ----------------------------------------------------------------------------
// This is a global flag that is used in some testBench code as a way to determine
// that the test has reached completion
// Also have a global variable that allows us to check number of failures from debugger
// after execution
volatile u32 unitTestTestComplete = 0;
volatile u32 unitTestNumFailures  = 8888; // This intial default should always be ovewritten by code before being read, otherwise assume failure


// 4: Static Local Data
// ----------------------------------------------------------------------------
static u32 numTestsPassed;
static u32 numTestsRan;

// 5: Static Function Prototypes
// ----------------------------------------------------------------------------

// 6: Functions Implementation
// ----------------------------------------------------------------------------

int unitTestInit(void)
{
    numTestsPassed = 0;
    numTestsRan=0;
    return 0;
}

int unitTestLogPass(void)
{
    numTestsRan++;
    numTestsPassed++;
    return 0;
}

int unitTestLogFail(void)
{
    numTestsRan++;
    return 0;
}

int unitTestAssert(int value)
{
    numTestsRan++;
    if (value)
        numTestsPassed++;

    return 0;
}

int unitTestExecutionWithinRange(float actual, float expected, float percentageError)
{
    float minVal;
    float maxVal;
    numTestsRan++;

    //Early exit if we have negative numbers
    if (actual<0.0f){
        return 0;
    }

    minVal = expected * ((100.0 - percentageError) / 100.0);
    maxVal = expected * ((100.0 + percentageError) / 100.0);
    //Also, if actual number is 0.0f then this means the program ran tocompletion very fast
    //which probably means it passed :)
    if ( ((actual > minVal) && (actual < maxVal)) || (actual==0.0f) )
        numTestsPassed++;

    return 0;
}

int unitTestFloatWithinRange(float actual, float expected, float percentageError)
{
    float difference,absErrorAccepted;
    numTestsRan++;
    difference=actual-expected;
    if (difference<0) difference=-difference;

    //Considering that a deviation of 1.0f is 100% error
    absErrorAccepted=percentageError/100.0f;
    if (difference<=absErrorAccepted){
        numTestsPassed++;
    }

    return 0;
}

int unitTestFloatAbsRangeCheck(float actual, float expected, float AbsError)
{
    float difference;
    difference=actual-expected;
    numTestsRan++;
    if (difference<0) difference=-difference;
    if (difference<=AbsError){
        numTestsPassed++;
    }
    return 0;
}

void unitTestMemCompare(const void * pActualStart,const void * pExpectedStart, u32 lengthBytes)
{
    int does_it_pass = 0;
    numTestsRan++;

    does_it_pass = memcmp(pActualStart, pExpectedStart, lengthBytes);
    if(does_it_pass == 0)
        numTestsPassed++;
}

void unitTestMemCompareDeltaU8(u8 * pActualStart,u8 * pExpectedStart, u32 lengthBytes, u8 delta)
{
    unsigned int i;
    int does_it_pass = 0;
    int negDelta = -delta, diff;
    numTestsRan++;

    for(i=0; i < lengthBytes; i++)
    {
        diff = pActualStart[i] - pExpectedStart[i];
       if( ( diff > delta) || ( diff < negDelta) )
       {
            does_it_pass++;
            break;
       }
    }
    if(does_it_pass == 0)
        numTestsPassed++;
}

void unitTestCompare(u32 actualValue, u32 expectedValue)
{
    numTestsRan++;
    if (actualValue == expectedValue)
    {
        numTestsPassed++;
    }
}

int unitTestLogResults(int passes,int fails)
{
    numTestsRan += passes;
    numTestsRan += fails;
    numTestsPassed += passes;
    return 0;
}

u32 unitTestCheckSectionFail(char *sectionName)
{
    static u32 lastFailNb = 0;
    u32 failNb = numTestsRan - numTestsPassed - lastFailNb;
    if (failNb)
    {
        if (sectionName)
            printf("moviUnitTest: %ld failure(s) in %s section\n", failNb, sectionName);
        lastFailNb += failNb;
    }
    return failNb;
}

int unitTestFinalReport(void)
{
    if (numTestsPassed == numTestsRan)
    {
        printf("\nmoviUnitTest:PASSED\n");
    }
    else
    {
        printf("\nmoviUnitTest:FAILED : %ld failures\n",numTestsRan - numTestsPassed);
    }

    // Some testbenches make use of this global flag to signal the end of
    // test
    unitTestTestComplete = TRUE;
    unitTestNumFailures  = numTestsRan - numTestsPassed;

    return 0;
}
