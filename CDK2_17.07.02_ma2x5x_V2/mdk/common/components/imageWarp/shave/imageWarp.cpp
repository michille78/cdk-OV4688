///
/// @file
/// @copyright All code copyright Movidius Ltd 2012, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Image warp component
///

// 1: Includes
// ----------------------------------------------------------------------------

#include <math.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>

#include <svuCommonShave.h>
#include "warpMeshExpand.h"
#include "warpMeshSample8bit.h"
#include "imageWarpDefines.h"
#include "imageWarp.h"

#include "swcCdma.h"
#include "swcFrameTypes.h"
#include "moviVectorUtils.h"

// 2:  Source Specific #defines and types  (typedef,enum,struct)
// ----------------------------------------------------------------------------
#define NO_DDR_OUTPUT   ((u32)0xFFFFFFFF)

#ifdef IMAGEWARP_USE_DMA_ENGINE
#define USE_DMA
#endif

#ifndef IMAGEWARP_DMA_AGENT
#define IMAGEWARP_DMA_AGENT (0)
#endif

typedef struct meshPortion
{
    tileList* head;
}meshPortion;
// 3: Global Data (Only if absolutely necessary)
// ----------------------------------------------------------------------------
// Sections decoration is required here for downstream tool


// 4: Static Local Data
// ----------------------------------------------------------------------------
// Buffers


__attribute__((aligned(16))) unsigned char inputBufferMem[CMX_BUFFER_SIZE];
__attribute__((aligned(16))) unsigned char outBufferMem[2][OUT_TILE_SIZE];

dmaTransactionList_t list[2];
dmaTransactionList_t *ref[2];

meshPortion meshLUT[MESH_LUT_LENGTH + 1];
u32 tileLutElemCount;
reTileEntry tiles[2];
reTileEntry referenceTile;
__attribute__((aligned(16))) half mx[OUT_TILE_WIDTH * OUT_TILE_WIDTH];
__attribute__((aligned(16))) half my[OUT_TILE_WIDTH * OUT_TILE_WIDTH];
// 5: Static Function Prototypes
// ----------------------------------------------------------------------------

// 6: Functions Implementation
// ----------------------------------------------------------------------------
static inline float min4Elem(float4 vec)
{
    float xmin;
    xmin = __builtin_shave_cmu_min_f32_rr_float(vec[0], vec[1]);
    xmin = __builtin_shave_cmu_min_f32_rr_float(xmin, vec[2]);
    return __builtin_shave_cmu_min_f32_rr_float(xmin, vec[3]);
}

static inline float max4Elem(float4 vec)
{
    float xmax;
    xmax = __builtin_shave_cmu_max_f32_rr_float(vec[0], vec[1]);
    xmax = __builtin_shave_cmu_max_f32_rr_float(xmax, vec[2]);
    return __builtin_shave_cmu_max_f32_rr_float(xmax, vec[3]);
}

static void initTileBuffers(reTileEntry* currTile, u8* buffer, u32 lineLength)
{
    u32 ix;
    for(ix = 0; ix < VERTICAL_PAD; ix++)
    {
        currTile->cmxInBuffP[ix] = (u8*)buffer + CMX_BUFFER_LINES * lineLength + HORIZONTAL_PAD;
        currTile->cmxInBuffP[ix + CMX_BUFFER_LINES + VERTICAL_PAD] = (u8*)buffer + CMX_BUFFER_LINES * lineLength + HORIZONTAL_PAD;
    }
    for(ix = 0; ix < CMX_BUFFER_LINES; ix++)
    {
        currTile->cmxInBuffP[ix + VERTICAL_PAD] = (u8*)buffer + ix * lineLength + HORIZONTAL_PAD;
    }
    currTile->swapP = (u8*)buffer + (CMX_BUFFER_LINES + 1) * lineLength + HORIZONTAL_PAD;

}
static inline void rotateBufferPointers(reTileEntry* currTile)
{
    // TODO: optimize
    u32 i;
    u8* swapP = currTile->cmxInBuffP[VERTICAL_PAD];

    for(i = 0; i < CMX_BUFFER_LINES - 1 ; i++)
    {
        currTile->cmxInBuffP[i + VERTICAL_PAD] = currTile->cmxInBuffP[i + VERTICAL_PAD + 1];
    }
    currTile->cmxInBuffP[CMX_BUFFER_LINES + VERTICAL_PAD - 1] = currTile->swapP;
    currTile->swapP = swapP;
}

static inline void adjustBufferPointers(reTileEntry* currTile, reTileEntry* refTile, int amount)
{
    // TODO: optimize
    u32 i;
    uint4* lineP = (uint4*)&currTile->cmxInBuffP[VERTICAL_PAD];
    uint4* lineP2 = (uint4*)&refTile->cmxInBuffP[VERTICAL_PAD];

    for(i = 0; i < CMX_BUFFER_LINES >> 2 ; i++)
    {
        lineP[i] = lineP2[i] + (uint4)amount;
    }
    currTile->cmxInBuffP[CMX_BUFFER_LINES + VERTICAL_PAD - 1] =
                    refTile->cmxInBuffP[CMX_BUFFER_LINES + VERTICAL_PAD - 1] + amount;
}

static inline  void prepareMesh(meshStruct* mesh, frameSpec* frSpec, tileList* tileNodes)
{
    unsigned int i, j, k;
    unsigned int meshH = mesh->meshHeight;
    unsigned int meshW = mesh->meshWidth;
    float *my = mesh->meshY;
    for (j = 0; j < frSpec->height; j++ )
    {
        meshLUT[j].head = NULL;
    }

    meshLUT[MESH_LUT_LENGTH].head = NULL;
    tileLutElemCount = 0;
    for (i = 0; i < (meshH - 1); i++ )
    {
        for (j = 0; j < meshW - 3; j += 3 )
        {
            u32 ind = i * meshW + j;
            float4* first = (float4*)&my[ind];
            float4* second = (float4*)&my[ind + meshW];
            float4 min = __builtin_shave_cmu_min_f32_rr_float4(first[0], second[0]);
            float4 max = __builtin_shave_cmu_max_f32_rr_float4(first[0], second[0]);

            float4 min2 = __builtin_shave_cmu_alignvec_rri_float4(min, min, 4);
            float4 max2 = __builtin_shave_cmu_alignvec_rri_float4(max, max, 4);

            min = __builtin_shave_cmu_min_f32_rr_float4(min, min2);
            max = __builtin_shave_cmu_max_f32_rr_float4(max, max2);
            int4 minInt = mvuConvert_int4(min);
            int4 maxInt = mvuConvert_int4(max);
            int4 diff = maxInt - minInt;
            for(k = 0; k < 3; k++)
            {
                if ((diff[k] <= CMX_BUFFER_LINES) && (minInt[k] < (int)frSpec->height)
                                && (maxInt[k] >= 0) && (minInt[k] > - VERTICAL_PAD))
                {
                    if (minInt[k] > 0)
                    {
                        tileNodes[tileLutElemCount].next = meshLUT[minInt[k]].head;
                        tileNodes[tileLutElemCount].x = j + k;
                        tileNodes[tileLutElemCount].y = i;
                        meshLUT[minInt[k]].head = &tileNodes[tileLutElemCount++];
                    }
                    else
                    {
                        tileNodes[tileLutElemCount].next = meshLUT[0].head;
                        tileNodes[tileLutElemCount].x = j + k;
                        tileNodes[tileLutElemCount].y = i;
                        meshLUT[0].head = &tileNodes[tileLutElemCount++];
                    }
                }
                else
                {
                    tileNodes[tileLutElemCount].next = meshLUT[MESH_LUT_LENGTH].head;
                    tileNodes[tileLutElemCount].x = j + k;
                    tileNodes[tileLutElemCount].y = i;
                    meshLUT[MESH_LUT_LENGTH].head = &tileNodes[tileLutElemCount++];
                }
            }
        }
        /// Working the last elements
        if( j < meshW - 1)
        {
            u32 ind = i * meshW + j;
            float4* first = (float4*)&my[ind];
            float4* second = (float4*)&my[ind + meshW];
            float4 min = __builtin_shave_cmu_min_f32_rr_float4(first[0], second[0]);
            float4 max = __builtin_shave_cmu_max_f32_rr_float4(first[0], second[0]);

            float4 min2 = __builtin_shave_cmu_alignvec_rri_float4(min, min, 4);
            float4 max2 = __builtin_shave_cmu_alignvec_rri_float4(max, max, 4);

            min = __builtin_shave_cmu_min_f32_rr_float4(min, min2);
            max = __builtin_shave_cmu_max_f32_rr_float4(max, max2);
            int4 minInt = mvuConvert_int4(min);
            int4 maxInt = mvuConvert_int4(max);
            int4 diff = maxInt - minInt;
            for(k = 0; ((j+k) < (meshW -1)); k++)
            {
                if ((diff[k] <= CMX_BUFFER_LINES) && (minInt[k] < (int)frSpec->height)
                                && (maxInt[k] >= 0) && (minInt[k] > - VERTICAL_PAD))
                {
                    if (minInt[k] > 0)
                    {
                        tileNodes[tileLutElemCount].next = meshLUT[minInt[k]].head;
                        tileNodes[tileLutElemCount].x = j + k;
                        tileNodes[tileLutElemCount].y = i;
                        meshLUT[minInt[k]].head = &tileNodes[tileLutElemCount++];
                    }
                    else
                    {
                        tileNodes[tileLutElemCount].next = meshLUT[0].head;
                        tileNodes[tileLutElemCount].x = j + k;
                        tileNodes[tileLutElemCount].y = i;
                        meshLUT[0].head = &tileNodes[tileLutElemCount++];
                    }
                }
                else
                {
                    tileNodes[tileLutElemCount].next = meshLUT[MESH_LUT_LENGTH].head;
                    tileNodes[tileLutElemCount].x = j + k;
                    tileNodes[tileLutElemCount].y = i;
                    meshLUT[MESH_LUT_LENGTH].head = &tileNodes[tileLutElemCount++];
                }
            }
        }
    }

}

static inline void prepareTile(reTileEntry* tile,
                        meshStruct* mesh,
                        frameBuffer *inputFb,
                        frameBuffer *outputFb,
                        tileList* coord,
                        unsigned int cmxY)
{
    float4 x,y;
    unsigned int out_width = outputFb->spec.width;
    unsigned int in_width = mesh->coord_max_x - mesh->coord_min_x;//inputFb->spec.width;

    float minX = (float)mesh->coord_min_x;
    float maxX = (float)mesh->coord_max_x;

    unsigned int meshW = mesh->meshWidth;
    float *mx = mesh->meshX;
    float *my = mesh->meshY;
    unsigned int tileIndex = coord->y * meshW + coord->x;

    float x_offset = (float)mesh->coord_min_x;

    x[0] = mx[tileIndex];
    x[1] = mx[tileIndex + 1];
    x[2] = mx[tileIndex + meshW];
    x[3] = mx[tileIndex + meshW + 1];
    y[0] = my[tileIndex] - cmxY;
    y[1] = my[tileIndex + 1] - cmxY;
    y[2] = my[tileIndex + meshW] - cmxY;
    y[3] = my[tileIndex + meshW + 1] - cmxY;

    float min = min4Elem(x);
    float max = max4Elem(x);
    float minFloor = (int)(min - __builtin_shave_sau_frac_f32_r(min));
    x -= minFloor;
    tile->dstOffsetDDR = coord->y * OUT_TILE_HEIGHT * out_width + coord->x  * OUT_TILE_WIDTH;
    if (min < minX || max > maxX)
    {
        tile->isInsideImg = 0;
        // insert in the list which will be filled with padding
    }
    else
    {
        x[0] -= x_offset;
        x[1] -= x_offset;
        x[2] -= x_offset;
        x[3] -= x_offset;

        tile->isInsideImg = 1;

        adjustBufferPointers(tile, &referenceTile, (unsigned int) minFloor);

        tile->xCoords = mvuConvert_half4(x);
        tile->yCoords = mvuConvert_half4(y);
    }
}



static inline void processTile(reTileEntry* tile,
                        frameBuffer *inputFb,
                        frameBuffer *outputFb,
                        unsigned short paddingvalue,
                        uint32_t nr_shaves_vsplit,
                        dmaRequesterId dmaId)
{
    UNUSED(outputFb);
    UNUSED(paddingvalue);
    UNUSED(dmaId);

    mvcvWarpMeshExpand_asm((half*)&tile->xCoords, (half*)&tile->yCoords, mx, my);
    mvcvWarpMeshSample8bit_asm(&tile->cmxInBuffP[VERTICAL_PAD], tile->cmxOutBuff, mx, my,
                               (inputFb->spec.width / MAX_SHAVES_ON_VSPLIT) + 2 * HORIZONTAL_PAD,
                               CMX_BUFFER_LINES);

}

extern "C" void imageWarp(meshStruct* mesh,
                          frameBuffer *inputFb,
                          frameBuffer *outputFb,
                          tileList* tileNodes,
                          uint32_t nr_shaves_vsplit,
                          uint32_t proc_shave_idx,
                          unsigned short paddingvalue)
{
    unsigned int lineId;
    unsigned int out_width = outputFb->spec.width;
    unsigned int out_height = outputFb->spec.height;
    unsigned int in_height = mesh->coord_max_y;

    unsigned int in_dma_size = mesh->coord_max_x - mesh->coord_min_x;
    unsigned int in_stride = inputFb->spec.width;

    unsigned int in_width_pad = in_dma_size + 2* HORIZONTAL_PAD;

    unsigned char *img = inputFb->p1;
    unsigned char *out_img = outputFb->p1;

    reTileEntry* currTile, *nextTile, *swapTile;

#if 0 // original repo code
    static dmaRequesterId id1 = dmaInitRequesterWithAgent(3, IMAGEWARP_DMA_AGENT);
#else // workaround
    static dmaRequesterId id1;
    if(scGetShaveNumber() == 4)
        id1 = dmaInitRequesterWithAgent(3, IMAGEWARP_DMA_AGENT);
    if(scGetShaveNumber() == 5)
        id1 = dmaInitRequesterWithAgent(2, IMAGEWARP_DMA_AGENT + 1);
    if(scGetShaveNumber() == 6)
        id1 = dmaInitRequesterWithAgent(1, IMAGEWARP_DMA_AGENT + 2);
#endif
    assert((CMX_BUFFER_LINES - 1) % 4 == 0);

    currTile = &tiles[0];
    nextTile = &tiles[1];
    initTileBuffers(currTile, inputBufferMem, in_width_pad);
    initTileBuffers(nextTile, inputBufferMem, in_width_pad);
    initTileBuffers(&referenceTile, inputBufferMem, in_width_pad);

    prepareMesh(mesh, &inputFb->spec, tileNodes);
    lineId = mesh->coord_min_y;

    while (meshLUT[lineId].head == NULL)
        lineId++;
    ref[0] = dmaCreateTransactionFullOptions(
                    id1, &list[0], img + lineId * in_stride,
                    currTile->cmxInBuffP[VERTICAL_PAD],
                    in_dma_size * CMX_BUFFER_LINES,
                    in_dma_size,
                    in_dma_size,
                    in_stride,
                    in_width_pad);
    dmaStartListTask(ref[0]);

    // create output transaction with dummy addresses. We will fill it later
    ref[1] = dmaCreateTransactionFullOptions(
                    id1, &list[1], NULL,
                    out_img + NO_DDR_OUTPUT, OUT_TILE_WIDTH * OUT_TILE_HEIGHT,
                    OUT_TILE_WIDTH, OUT_TILE_WIDTH, OUT_TILE_WIDTH,
                    out_width);
    dmaWaitTask(ref[0]);
    u8* dmaSrcAddress = img + in_stride * (lineId + CMX_BUFFER_LINES);
    ref[0] = dmaCreateTransaction(
                    id1, &list[0], dmaSrcAddress,
                    referenceTile.swapP, in_dma_size);

    currTile->cmxOutBuff = outBufferMem[0];
    nextTile->cmxOutBuff = outBufferMem[1];

    //main loop
    for (; lineId + 1 < in_height; lineId++)
    {
        u32 i;
        ref[0]->src = img + in_stride * (lineId + CMX_BUFFER_LINES);
        ref[0]->dst = referenceTile.swapP;
        ref[0]->linkAddress = NULL;
        dmaStartListTask(ref[0]);
        tileList *listP2, * listP = meshLUT[lineId].head;
        ref[1]->dst = (void*)NO_DDR_OUTPUT;
        if (listP != NULL)
        {
            dmaWaitTask(ref[0]);
            while (listP != NULL)
            {
#ifdef USE_DMA
                if (ref[1]->dst != (void*)NO_DDR_OUTPUT)
                {
                    dmaWaitTask(ref[1]);
                }
#endif
                //prepare tile may reorganize the list
                listP2 = listP;
                listP = listP->next;
                prepareTile(currTile, mesh, inputFb, outputFb, listP2, lineId);

                if (currTile->isInsideImg)
                {
                    processTile(currTile, inputFb, outputFb, paddingvalue, nr_shaves_vsplit, id1);

                    u8* dstAddress = outputFb->p1 + currTile->dstOffsetDDR;
                    u8* srcAddress = currTile->cmxOutBuff;
#ifdef USE_DMA
                    ref[1]->src = srcAddress;
                    ref[1]->dst = dstAddress;
                    ref[1]->linkAddress = NULL;
                    dmaStartListTask(ref[1]);
#else
                    for(i = 0; i < OUT_TILE_HEIGHT; ++i)
                    {
                        memcpy(dstAddress, srcAddress, OUT_TILE_WIDTH);
                        dstAddress += out_width;
                        srcAddress += OUT_TILE_WIDTH;
                    }
#endif
                }
#ifdef USE_DMA
                else
                {
                    ref[1]->dst = (void*)NO_DDR_OUTPUT;
                }
#endif
                swapTile = currTile;
                currTile = nextTile;
                nextTile = swapTile;
            }
#ifdef USE_DMA
            if ( (u32)ref[1]->dst != NO_DDR_OUTPUT)
                dmaWaitTask(ref[1]);
#endif
        }
        else
        {
            dmaWaitTask(ref[0]);
        }
        rotateBufferPointers(&referenceTile);
    }

    SHAVE_HALT;
}

