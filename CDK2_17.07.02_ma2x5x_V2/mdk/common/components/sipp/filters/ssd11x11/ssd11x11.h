/// @file
/// @copyright All code copyright Movidius Ltd 2014, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     This file contains the declaration of the @ref ssd11x11 SIPP filter API.
///

#ifndef __SIPP_SSD11x11_H__
#define __SIPP_SSD11x11_H__ 

#include <sipp.h>

/// @sf_definition ssd11x11 Sum of Squared Differences 11x11
/// @sf_description This filter performs sum of squared differences (SSD), the differences are squared and aggregated within a square window (11x11 in this case).
/// @sf_group Arithmetic
/// @sf_outdatatype UInt8
/// @sf_inputs
/// 	- name: input1; datatypes: UInt8; kernels: 11x11
/// 	- name: input2; datatypes: UInt8; kernels: 11x11
/// @sf_extasm $(MV_COMMON_BASE)/components/kernelLib/MvCV/kernels/sumOfSquaredDiff11x11/arch/ma2x5x/shave/src/sumOfSquaredDiff11x11.asm
/// @{

//Shave symbols that need to be understood by leon need to be declared through "SVU_SYM" MACRO,
//as moviCompile adds a leading _ to symbol exported
/// @brief Shave function of the @ref ssd11x11 filter.
void SVU_SYM(svuSSD11x11)(SippFilter *fptr);

/// @}

#endif // __SIPP_SSD11x11_H__ 