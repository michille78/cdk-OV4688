///
/// @file
/// @copyright All code copyright Movidius Ltd 10154, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     This file contains the declaration of the @ref censusMatching32 SIPP filter API.
///

#ifndef __SIPP_CENSUSMATCHING32_H__
#define __SIPP_CENSUSMATCHING32_H__

#include <sipp.h>

/// @sf_definition censusMatching32 censusMatching32
/// @sf_description  mvcvCensusMatching32 - performs an XOR operation between pixel one pixel in *in1 and 16 pixels from *in2 and counts up how many values of 1 are in the result
/// @sf_group Arithmetic
/// @sf_preserve numPlanes
/// @sf_outdatatype UInt8
/// @sf_inputs
/// 	- datatypes: UInt32; kernels: 1x1
/// 	- datatypes: UInt32; kernels: 1x64
/// @sf_extasm $(MV_COMMON_BASE)/components/kernelLib/MvCV/kernels/censusMatching32/arch/ma2x5x/shave/src/censusMatching32.asm
/// @{

/// @brief Parameter structure of the @ref censusMatching32 filter.
typedef struct
{
    /// @sf_pfdesc flag that enables right crossing (by default left crossing)
    UInt32 flag;
}
CensusMatching32Param;

//Shave symbols that need to be understood by leon need to be declared through "SVU_SYM" MACRO,
//as moviCompile adds a leading _ to symbol exported
/// @brief Shave function of the @ref censusMatching32 filter.
void SVU_SYM(svuCensusMatching32)(SippFilter *fptr);

/// @}

#endif // __SIPP_CENSUSMATCHING32_H__
