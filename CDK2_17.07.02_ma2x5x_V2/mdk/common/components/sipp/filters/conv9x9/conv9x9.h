///
/// @file
/// @copyright All code copyright Movidius Ltd 2014, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     This file contains the declaration of the @ref conv9x9 SIPP filter API.
///

#ifndef __SIPP_CONV9x9_H__
#define __SIPP_CONV9x9_H__ 

#include <sipp.h>

/// @sf_definition conv9x9 Convolution 9x9
/// @sf_description This filter performs a convolution on the input image using the given 9x9 matrix.
/// @sf_group Arithmetic
/// @sf_outdatatype UInt8
/// @sf_inputs
/// 	- datatypes: UInt8; kernels: 9x9
/// @sf_extasm $(MV_COMMON_BASE)/components/kernelLib/MvCV/kernels/convolution9x9/arch/ma2x5x/shave/src/convolution9x9.asm
/// @{

/// @brief Parameter structure of the @ref conv9x9 filter.
typedef struct
{
    /// @sf_pfdesc 81 element array with fp16 values containing the 9x9 convolution matrix.
	/// @sf_pfdefvalue 0x2252, 0x2252, 0x2252, 0x2252, 0x2252, 0x2252, 0x2252, 0x2252, 0x2252, 0x2252, 0x2252, 0x2252, 0x2252, 0x2252, 0x2252, 0x2252, 0x2252, 0x2252, 0x2252, 0x2252, 0x2252, 0x2252, 0x2252, 0x2252, 0x2252, 0x2252, 0x2252, 0x2252, 0x2252, 0x2252, 0x2252, 0x2252, 0x2252, 0x2252, 0x2252, 0x2252, 0x2252, 0x2252, 0x2252, 0x2252, 0x2252, 0x2252, 0x2252, 0x2252, 0x2252, 0x2252, 0x2252, 0x2252, 0x2252, 0x2252, 0x2252, 0x2252, 0x2252, 0x2252, 0x2252, 0x2252, 0x2252, 0x2252, 0x2252, 0x2252, 0x2252, 0x2252, 0x2252, 0x2252, 0x2252, 0x2252, 0x2252, 0x2252, 0x2252, 0x2252, 0x2252, 0x2252, 0x2252, 0x2252, 0x2252, 0x2252, 0x2252, 0x2252, 0x2252, 0x2252, 0x2252
	/// @sf_pfarraysize 81
    UInt16* cMat; //fp16 9x9 matrix
}
Conv9x9Param;

//Shave symbols that need to be understood by leon need to be declared through "SVU_SYM" MACRO,
//as moviCompile adds a leading _ to symbol exported
/// @brief Shave function of the @ref conv9x9 filter.
void SVU_SYM(svuConv9x9)(SippFilter *fptr);

/// @}

#endif // __SIPP_CONV9x9_H__ 
