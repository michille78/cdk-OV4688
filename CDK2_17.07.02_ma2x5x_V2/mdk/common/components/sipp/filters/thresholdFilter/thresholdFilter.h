/// @file
/// @copyright All code copyright Movidius Ltd 2014, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     This file contains the declaration of the @ref thresholdFilter SIPP filter API.
///

#ifndef __SIPP_THRESHOLD_FILTER_H__
#define __SIPP_THRESHOLD_FILTER_H__

#include <sipp.h>

/// @sf_definition thresholdFilter ThresholdFilter
/// @sf_description This filter computes the output image based on a thresholdFilter value 
/// @sf_group CV
/// @sf_outdatatype UInt16
/// @sf_preserve numPlanes
/// @sf_inputs
/// 	- datatypes: float; kernels: 1x1
/// @sf_extasm $(MV_COMMON_BASE)/components/kernelLib/MvCV/kernels/thresholdFilter/arch/ma2x5x/shave/src/thresholdFilter.asm
/// @{

/// @brief Parameter structure of the @ref thresholdFilter filter.
typedef struct
{
	float threshold;
	UInt32 posOffset;
}
ThresholdFilterParam;


//Shave symbols that need to be understood by leon need to be declared through "SVU_SYM" MACRO,
//as moviCompile adds a leading _ to symbol exported
/// @brief Shave function of the @ref thresholdFilter filter
void SVU_SYM(svuThresholdFilter)(SippFilter *fptr);

/// @}

#endif //__SIPP_THRESHOLD_FILTER_H__