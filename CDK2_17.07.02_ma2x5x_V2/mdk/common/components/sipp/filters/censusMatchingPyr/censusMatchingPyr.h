///
/// @file
/// @copyright All code copyright Movidius Ltd 10154, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     This file contains the declaration of the @ref censusMatchingPyr SIPP filter API.
///

#ifndef __SIPP_CENSUSMATCHINGPYR_H__
#define __SIPP_CENSUSMATCHINGPYR_H__ 

#include <sipp.h>

/// @sf_definition censusMatchingPyr censusMatchingPyr
/// @sf_description  mvcvCensusMatchingPyr - performs an XOR operation between pixel one pixel in *in1 and 16 pixels from *in2 and counts up how many values of 1 are in the result
/// @sf_group Arithmetic
/// @sf_preserve numPlanes
/// @sf_outdatatype UInt8
/// @sf_inputs
/// 	- datatypes: UInt32; kernels: 1x1
/// 	- datatypes: UInt32; kernels: 1x15
/// @sf_extasm $(MV_COMMON_BASE)/components/kernelLib/MvCV/kernels/censusMatchingPyr/arch/ma2x5x/shave/src/censusMatchingPyr.asm
/// @{

/// @brief Parameter structure of the @ref censusMatchingPyr filter.
typedef struct 
{
	//pointer to predicted disparities 
	UInt8* predicted;
}
CensusMatchingPyrParam;

//Shave symbols that need to be understood by leon need to be declared through "SVU_SYM" MACRO,
//as moviCompile adds a leading _ to symbol exported
/// @brief Shave function of the @ref censusMatchingPyr filter.
void SVU_SYM(svuCensusMatchingPyr)(SippFilter *fptr);

/// @}

#endif // __SIPP_CENSUSMATCHINGPYR_H__ 