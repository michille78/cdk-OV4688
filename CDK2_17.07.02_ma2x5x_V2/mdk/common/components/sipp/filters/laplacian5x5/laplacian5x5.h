///
/// @file
/// @copyright All code copyright Movidius Ltd 2014, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     This file contains the declaration of the @ref laplacian5x5 SIPP filter API.
///    

#ifndef __SIPP_LAPLACIAN5x5_H__
#define __SIPP_LAPLACIAN5x5_H__ 

#include <sipp.h>

/// @sf_definition laplacian5x5 Laplacian 5x5
/// @sf_description  The filter applies a Laplacian filter with custom size.
/// @sf_group Arithmetic
/// @sf_outdatatype UInt8
/// @sf_inputs
/// 	- datatypes: UInt8; kernels: 5x5
/// @sf_extasm $(MV_COMMON_BASE)/components/kernelLib/MvCV/kernels/sLaplacian5x5/arch/ma2x5x/shave/src/sLaplacian5x5.asm

/// @{
//Shave symbols that need to be understood by leon need to be declared through "SVU_SYM" MACRO,
//as moviCompile adds a leading _ to symbol exported
/// @brief Shave function of the @ref laplacian5x5 filter.
void SVU_SYM(svuLaplacian5x5)(SippFilter *fptr);

/// @}

#endif //__SIPP_LAPLACIAN5x5_H__
