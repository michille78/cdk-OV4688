#include <sipp.h>
///
/// @file
/// @copyright All code copyright Movidius Ltd 2014, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     This file contains the declaration of the @ref histogram SIPP filter API.
///    

#ifndef __SIPP_HISTOGRAM_H__
#define __SIPP_HISTOGRAM_H__ 

#include <sipp.h>

/// @sf_definition histogram Histogram
/// @sf_description  This filter computes a histogram on a given line to be applied to all lines of an image.
/// @sf_group ISP
/// @sf_outdatatype UInt8
/// @sf_inputs
/// 	- datatypes: UInt8; kernels: 1x1
/// @sf_extasm $(MV_COMMON_BASE)/components/kernelLib/MvCV/kernels/histogram/arch/ma2x5x/shave/src/histogram.asm

/// @{

/// @brief Parameter structure of the @ref histogram filter.
typedef struct
{
	/// @sf_desc array of values from histogram
    UInt32* hist; 
}
HistogramParam;

//Shave symbols that need to be understood by leon need to be declared through "SVU_SYM" MACRO,
//as moviCompile adds a leading _ to symbol exported
/// @brief Shave function of the @ref histogram filter.
void SVU_SYM(svuHistogram)(SippFilter *fptr);

/// @}

#endif //__SIPP_HISTOGRAM_H__