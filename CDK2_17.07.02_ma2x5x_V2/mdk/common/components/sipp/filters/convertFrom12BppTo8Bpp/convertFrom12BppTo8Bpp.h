///
/// @file
/// @copyright All code copyright Movidius Ltd 10154, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     This file contains the declaration of the @ref convertFrom12BppTo8Bpp SIPP filter API.
///

#ifndef __SIPP_CONVERTFROM12BPPTO8BPP_H__
#define __SIPP_CONVERTFROM12BPPTO8BPP_H__ 

#include <sipp.h>

/// @sf_definition convertFrom12BppTo8Bpp 12Bpp to 8Bpp conversion
/// @sf_description This filter performs a conversion on the input image from 12bpp to 8bpp
/// @sf_group Arithmetic
/// @sf_preserve numPlanes, dataType
/// @sf_outdatatype UInt8
/// @sf_inputs
/// 	- datatypes: UInt8; kernels: 1x1
/// @sf_extasm $(MV_COMMON_BASE)/components/kernelLib/MvCV/kernels/convertFrom12BppTo8Bpp/arch/ma2x5x/shave/src/convertFrom12BppTo8Bpp.asm
/// @{

//Shave symbols that need to be understood by leon need to be declared through "SVU_SYM" MACRO,
//as moviCompile adds a leading _ to symbol exported
/// @brief Shave function of the @ref convertFrom12BppTo8Bpp filter.
void SVU_SYM(svuConvertFrom12BppTo8Bpp)(SippFilter *fptr);
/// @}

#endif // __SIPP_CONVERTFROM12BPPTO8BPP_H__ 