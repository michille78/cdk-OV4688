///
/// @file
/// @copyright All code copyright Movidius Ltd 2014, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     This file contains the declaration of the @ref copy SIPP filter API.
///

#ifndef __SIPP_COPY_H__
#define __SIPP_COPY_H__ 


#include <sipp.h>

/// @sf_definition copy Copy
/// @sf_description This filter copies input image to output.
/// @sf_group Arithmetic
/// @sf_outdatatype UInt8
/// @sf_inputs
/// 	- datatypes: UInt8; kernels: 1x1
/// @{


//Shave symbols that need to be understood by leon need to be declared through "SVU_SYM" MACRO,
//as moviCompile adds a leading _ to symbol exported
/// @brief Shave function of the @ref copy filter.
void SVU_SYM(svuCopy)(SippFilter *fptr);

/// @}

#endif // __SIPP_COPY_H__ 
