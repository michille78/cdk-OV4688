///
/// @file
/// @copyright All code copyright Movidius Ltd 10154, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     This file contains the declaration of the @ref censusMin64 SIPP filter API.
///

#ifndef __SIPP_CENSUSMin64_H__
#define __SIPP_CENSUSMin64_H__ 

#include <sipp.h>

/// @sf_definition censusMin64 censusMin64
/// @sf_description  mvcvCensusMin64 - computes minimum of 64 disparity costs values
/// @sf_group Arithmetic
/// @sf_preserve numPlanes
/// @sf_outdatatype UInt8
/// @sf_inputs
/// 	- datatypes: UInt8; kernels: 1x1
/// @sf_extasm $(MV_COMMON_BASE)/components/kernelLib/MvCV/kernels/censusMin64/arch/ma2x5x/shave/src/censusMin64.asm
/// @{

/// @brief Parameter structure of the @ref censusMin64 filter.

//Shave symbols that need to be understood by leon need to be declared through "SVU_SYM" MACRO,
//as moviCompile adds a leading _ to symbol exported
/// @brief Shave function of the @ref censusMin64 filter.
void SVU_SYM(svuCensusMin64)(SippFilter *fptr);

/// @}

#endif // __SIPP_CENSUSMin64_H__ 