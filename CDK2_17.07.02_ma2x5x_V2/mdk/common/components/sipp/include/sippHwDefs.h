///
/// @file
/// @copyright All code copyright Movidius Ltd 2015, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     SIPP engine
///

#ifndef __SIPP_HWDEFS_H__
#define __SIPP_HWDEFS_H__


#if defined(MA2150) || defined(MA2155) || defined(MA2450) || defined(MA2455)
#include "sippHwDefs_ma2x5x.h"
#endif

#if defined(MA2480)
#include "sippISPDefs_ma2x8x.h"
#include "sippCVDefs_ma2x8x.h"
#endif

//===================================================================
//Dma params

/// @defgroup dma DMA
/// @brief DMA In/Out filter.
/// @ingroup SIPP_Input-Output_Filters
/// @par Output data type(s):\n
///      UInt8, UInt16, UInt32, UInt64, Int8, Int16, Int32, half, fp16, fp32
/// @par Filter function:\n
///      SIPP_DMA_ID
/// @par Inputs:\n
/// 	- datatypes: UInt8, UInt16, UInt32, UInt64, Int8, Int16, Int32, half, fp16, fp32; kernels: 1x1
/// @{

/// @brief Parameter structure of the @ref dma filter.
typedef struct
{
    /// @brief Private member. Myriad2 DMA 2D-chunked descriptor, as in CMXDMA_controller.doc.
    DmaDesc dmaDsc;

    /// @brief User level params to customize transfer.
    UInt32 ddrAddr;

    /// @brief Private member.
    UInt32 dstChkW;
    /// @brief Private member.
    UInt32 srcChkW;
    /// @brief Private member.
    UInt32 dstChkS;
    /// @brief Private member.
    UInt32 srcChkS;
    /// @brief Private member.
    UInt32 dstPlS;
    /// @brief Private member.
    UInt32 srcPlS;
    /// @brief Private member. Full line strides
    UInt32 dstLnS;
    /// @brief Private member. Full line strides
    UInt32 srcLnS;

    /// @brief Private member. Myriad2 DMA 2D-chunked descriptor pointer used when multiple lines per iteration, as in CMXDMA_controller.doc.
    DmaDesc *      pLineDesList;

    #if defined MA2480
    /// @brief Allow dma filter to be configured for standard and smart modes
    eDmaMode       dmaMode;
    /// @brief dmaMode specific config information
    DMAExtCfg      extCfg;
    /// @brief Private member. Total number of descriptors in the linked list for this filter
    UInt32         numDescriptors;
    #endif
    /// @brief Private member. A transaction list used to feed CDMA driver with direct streaming runtime
    #if defined SIPP_SCRT_ENABLE_OPIPE
    DMATransDesc * pTransList;
    #endif
    #if defined SIPP_USE_CMXDMA_DRIVER
    #if defined MA2480
    DrvCmxDmaTransaction    * drvDesc;
    DrvCmxDmaTransactionHnd * handle;
    #else
    dmaTransactionList * drvDesc;
    #endif
    #endif

}DmaParam;
/// @}


//===================================================================
//Mipi-RX  params
/// @defgroup mipirx MIPI Rx
/// @brief Flexible streaming processing of input directly from MIPI Rx including windowing, sub-sampling, data selection, black level subtraction (for RAW input) and data format conversion.
/// @ingroup SIPP_Input-Output_Filters
/// @par Output data type(s):\n
///	UInt8, UInt16, UInt32, half, fp16
/// @par Preserve:\n
/// @par Filter type:\n
///	 hw
/// @par   Filter functions:\n
/// 	SIPP_MIPI_RX0_ID, SIPP_MIPI_RX1_ID, SIPP_MIPI_RX2_ID, SIPP_MIPI_RX3_ID
/// @par Inputs:\n
/// @par Madatory inputs:\n
///	0
/// @{

/// @brief Parameter structure of the @ref mipirx filter.
typedef struct
{
    /// @par Private:\n
    ///	yes
    UInt32 frmDim;            //see SIPP_MIPI_RX*_FRM_DIM_ADR (Private)
    /// @brief configuration bit field(see SIPP_MIPI_RX*_CFG_ADR)
    UInt32 cfg;               //see SIPP_MIPI_RX*_CFG_ADR
    /// @brief Window x co-ordinate configuration
    UInt32 winX[4];           //see SIPP_MIPI_RX*_X*_ADR
    /// @brief Window y co-ordinate configuration
    UInt32 winY[4];           //see SIPP_MIPI_RX*_Y*_ADR
    /// @brief Selection enables and least significant bit for windows 0 and 1
    UInt32 sel01;             //see SIPP_MIPI_RX*_SEL01_ADR
    /// @brief Selection enables and least significant bit for windows 2 and 3
    UInt32 sel23;             //see SIPP_MIPI_RX*_SEL23_ADR
    /// @brief Selection mask
    UInt32 selMask[4];        //see SIPP_MIPI_RX*_MASK*_ADR
    /// @brief Black levels for windows 0 and 1 or even lines of RAW Bayer data
    UInt32 black01;           //see SIPP_MIPI_RX*_BLACK01_ADR
    /// @brief Black levels for windows 2 and 3 or odd lines of RAW Bayer data
    UInt32 black23;           //see SIPP_MIPI_RX*_BLACK23_ADR
    /// @brief Vertical back porch and private slice stride
    UInt32 vbp;               //see SIPP_MIPI_RX*_VBP_ADR

} MipiRxParam;
/// @}

//===================================================================
//Mipi-TX  params
/// @defgroup mipitx MIPI Tx
/// @brief Timing generation for MIPI Tx controller parallel interface for CSI-2/DSI output.
/// @ingroup SIPP_Input-Output_Filters
///  @par Output data type(s):\n
///@par Filter type:\n
///	 hw
/// @par   Filter functions:\n
///	 SIPP_MIPI_TX0_ID, SIPP_MIPI_TX1_ID
/// @par Inputs:\n
/// 	- datatypes: UInt8, UInt16, UInt32, half, fp16; kernels: 1x1
/// @{

/// @brief Parameter structure of the @ref mipirx filter.
typedef struct
{
    /// @par Private:\n
    /// yes
    UInt32 frmDim;             //see SIPP_MIPI_TX*_FRM_DIM_ADR (Private)
    /// @brief configuration bit field
    UInt32 cfg;                //see SIPP_MIPI_TX*_CFG_ADR

    //UInt32 lineCount;//RO
    /// @brief Line count at which to generate line compare interrupt

    UInt32 lineCompare;        //see SIPP_MIPI_TX*_LINE_COMP_ADR
    //UInt32 vStatus;  //RO
    /// @brief Vertical interval in which to generate vertical interval interrupt
    UInt32 vCompare;           //see SIPP_MIPI_TX*_VCOMP_ADR

    //Horizontal timings
    /// @brief Specifies the width, in PCLK clock periods, of the horizontal sync pulse (value programmed is HSW-1)
    UInt32 hSyncWidth;         //see SIPP_MIPI_TX*_HSYNC_WIDTH_ADR
    /// @brief Specifies the number of PCLK clocks from the end of the horizontal sync pulse to the start of horizontal active (value programmed is HBP so a back porch of 0 cycles can be set)
    UInt32 hBackPorch;         //see SIPP_MIPI_TX*_H_BACKPORCH_ADR
    /// @brief Specifies the number of PCLK clocks in the horizontal active section (value programmed is AVW-1)
    UInt32 hActiveWidth;       //see SIPP_MIPI_TX*_H_ACTIVEWIDTH_ADR
    /// @brief Specifies the number of PCLK clocks from end of active video to the start of horizontal sync (value programmed is HFP)
    UInt32 hFrontPorch;        //see SIPP_MIPI_TX*_H_FRONTPORCH_ADR

    // Vertical timings (there are 2 sets for interlaced,
    // but for now deal with progressive case)
    /// @brief Specifies the width in lines of the vertical sync pulse (value programmed is VSW-1).
    UInt32 vSyncWidth;         //see SIPP_MIPI_TX*_VSYNC_WIDTH_ADR
    /// @brief Specifies the number of lines from the end of the vertical sync pulse to the start of vertical active (value programmed is VBP)
    UInt32 vBackPorch;         //see SIPP_MIPI_TX*_V_BACKPORCH_ADR
    /// @brief Specifies the number of lines in the vertical active section (value programmed is AVH-1)
    UInt32 vActiveHeight;      //see SIPP_MIPI_TX*_V_ACTIVEHEIGHT_ADR
    /// @brief Specifies the number of lines from the end of active data to the start of vertical sync pulse (value programmed is VFP).
    UInt32 vFrontPorch;        //see SIPP_MIPI_TX*_V_FRONTPORCH_ADR
    /// @brief Number of PCLKs from the start of the last horizontal sync pulse in the Vertical Front Porch to the start of the vertical sync pulse.
    UInt32 vSyncStartOff;      //see SIPP_MIPI_TX*_VSYNC_START_OFFSET_ADR
    /// @brief Number of PCLKs from the end of the last horizontal sync pulse in the Vertical Sync Active to the end of the vertical sync pulse
    UInt32 vSyncEndOff;        //see SIPP_MIPI_TX*_VSYNC_END_OFFSET_ADR

} MipiTxParam;
/// @}

//===================================================================
//Mipi-TX loopback debug params
typedef struct
{
    UInt32 txID;               //SIPP_MIPI_TX0_ID or SIPP_MIPI_TX1_ID
    UInt8* imgAddr;            //full image base address
    UInt32 imgW;               //full image width
    UInt32 imgH;               //full image height
    UInt32 bpp;                //bytes per pixel
    UInt32 hbp;                //timing: horizontal back porch [pclk]
    UInt32 hfp;                //timing: horizontal front porch [pclk]
    UInt32 hsync;              //timing: horizontal sync [pclk]
    UInt32 vsync;              //timing: vertical sync [lines]

} MipiTxLoopbackParam;

//===================================================================
//Mipi-RX loopback debug params
typedef struct
{
    UInt32 rxID;               //SIPP_MIPI_RX1_ID or SIPP_MIPI_RX3_ID
    UInt8* imgAddr;            //full image base address
    UInt32 imgW;               //full image width
    UInt32 imgH;               //full image height
    UInt32 bpp;                //bytes per pixel              //bytes per pixel

} MipiRxLoopbackParam;

//===================================================================
//Utils that rely on structures defined above
void cfgMipiRxLoopback  (MipiRxLoopbackParam *cfg);
void cfgMipiTxLoopback  (MipiTxLoopbackParam *cfg);
void startMipiTxLoopback(MipiTxLoopbackParam *cfg);

//===================================================================
// DMA In/Out filter description for the SIPP Graph Designer Eclipse plugin
// DMA filter has to be described in a special way (not as the rest of the filters) as it has special behaviour
/*{
filter({
 id : "dmaIn",
 name : "DMA In",
 description : "DMA in filter",
 image : "51",

 color : "9090b0",
 shape : "rectangle",
 group : "Input-Output Filters",

 type : "hw",
 symbol :"SIPP_DMA_ID",
 preserve : "",
 dataType : "UInt8, UInt16, UInt32, UInt64, Int8, Int16, Int32, half, fp16, fp32, float",
 structure :"DmaParam",
 flags : "0x00",

 sourceConstraint : "assert(true,'')",
 targetConstraint : "assert(false,'No input')",
 mandatoryInputs : 0,

 parameters : [ {
  id : "internal0",
  name : "ddrAddr",
  description : "Buffer address to DMA into. If defined as 'auto', the buffer will be automatically generated in DDR.",
  value : "auto",
  type : "u32",
  constraint : "",
 },{
  id : "internal1",
  name : "File",
  description : "File name to DMA from. If specified than the image is automatically loaded into the DMA buffer specified by ddrAddr. Can be also left empty, in this case the application is responsible to fill the DMA buffer.",
  value : "",
  type : "file",
  constraint : "",
 } ]
});

}*/
/*{

filter({
 id : "dmaOut",
 name : "Dma Out",
 description : "DMA out filter",
 image : "52",

 color : "9090b0",
 shape : "rectangle",
 group : "Input-Output Filters",

 type : "hw",
 symbol :"SIPP_DMA_ID",
 preserve : "imgSize, numPlanes, dataType",
 dataType : "UInt8, UInt16, UInt32, UInt64, Int8, Int16, Int32, half, fp16, fp32, float",
 structure :"DmaParam",
 flags : "0x00",

 sourceConstraint : "assert(false,'No output')",
 targetConstraint : "assert(target.getTargetConnections().size() == 0,'Only 1 input allowed')",

 mandatoryInputs : 1,
 inputs : [ {
       name : "",
       dataTypesOptions : ["UInt8", "UInt16", "UInt32", "UInt64", "Int8", "Int16", "Int32", "half", "fp16", "fp32","float"],
       kernelOptions : [[1,1]]
 } ],

 parameters : [  {
  id : "internal0",
  name : "ddrAddr",
  description : "Buffer address to DMA from. If defined as 'auto', the buffer will be automatically generated in DDR.",
  value : "auto",
  type : "u32",
  constraint : "",
 },{
  id : "internal1",
  name : "File",
  description : "File name to DMA into. If specified than the DMA buffer (ddrAddr) content is automatically saved into the specified file. Can be also left empty.",
  value : "output.raw",
  type : "file",
  constraint : "",
 } ],
});

}*/

#endif // __SIPP_HWDEFS_H__
