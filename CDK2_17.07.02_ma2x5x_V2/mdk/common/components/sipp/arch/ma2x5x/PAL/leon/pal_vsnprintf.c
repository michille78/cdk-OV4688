/***************************************************
  Copyright (c) 2012 Entropic Communications, Inc. 
                All rights reserved.               
 ***************************************************
 
  Filename    :    pal_vsnprintf.c
  Component   :    Provide fast alternative to clib implementations
                   of some string management calls
  Author      :    Media IP FW team (Belfast) 
                   Modified from a version by Georges Menie (www.menie.org)
   
 ******************************************************************************/

/////////////////////////////////////////////////////////////////////////////////
//  Header files                                                             
/////////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdarg.h>
#include <string.h>

/////////////////////////////////////////////////////////////////////////////////
//  Private macros
/////////////////////////////////////////////////////////////////////////////////

#define PAL_VSNP_PRINT_BUF_LEN 12

#define PAL_VSNP_PAD_ZERO      (1 << 0)
#define PAL_VSNP_PAD_RIGHT     (1 << 1)

/////////////////////////////////////////////////////////////////////////////////
//  Code
/////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////
//  Private functions

static int _pal_snputs ( char       *str, 
                         int         size, 
                         int        *pfull, 
                         const char *src, 
                         int         width, 
                         int         pad
                       )
{
  int pc = 0;
  char padchar = ' ';

  if ( width > 0 ) 
  {
    int len = 0;
    const char *ptr;
  
    for ( ptr = src; *ptr; ++ptr ) ++len;
    
    if ( len >= width )     width = 0;
    else                    width -= len;
    
    if ( pad & PAL_VSNP_PAD_ZERO ) padchar = '0';
  }

  if (!(pad & PAL_VSNP_PAD_RIGHT)) 
  {
    for ( ; (pc < size) && (width > 0); --width)
      str[pc++] = padchar;
    
    if (width)
    {
      *pfull = 1;
      return pc;
    }
  }

  for (; pc < size; pc++)
  {
    if (*src)
      str[pc] = *src++;
    else
      return pc;
  }
  
  if ( *src )
  {
    *pfull = 1;
    return pc;
  }

  /* Padding on the right */
  for ( ; ( pc < size ) && ( width > 0 ); --width )
    str[pc++] = padchar;
  
  if ( width )
    *pfull = 1;

  return pc;
}


static int _pal_snputi ( char *str, 
                         int   size, 
                         int  *pfull,
                         int   i, 
                         int   b, 
                         int   sg, 
                         int   width, 
                         int   pad, 
                         int   letbase
                       )
{
  char print_buf[PAL_VSNP_PRINT_BUF_LEN];
  char *s;
  int t, neg = 0, pc = 0;
  unsigned int u = i;

  if (i == 0) 
  {
    print_buf[0] = '0';
    print_buf[1] = '\0';
    return _pal_snputs (str, size, pfull, print_buf, width, pad);
  }

  if ( sg && b == 10 && i < 0 ) 
  {
    neg = 1;
    u = -i;
  }

  s = print_buf + PAL_VSNP_PRINT_BUF_LEN-1;
  *s = '\0';

  if ( b == 16 )
  {
    while ( u ) 
    {
      t = u & 0xf;
      if( t >= 10 )
        t += letbase - '0' - 10;
      *--s = t + '0';
      u >>= 4;
    }
  }
  else
  {
    while ( u ) 
    {
      t = u / 10;
      *--s = (u-(t*10)) + '0';
      u = t;
    }
  }

  if ( neg )
  {
    if( width && (pad & PAL_VSNP_PAD_ZERO))
    {
      if ( pc < size )
        str[pc++] = '-';
      else
      {
        *pfull = 1;
        return pc;
      }
    }
    else
      *--s = '-';
  }

  return pc + _pal_snputs(&str[pc], (size-pc), pfull, s, width, pad);
}

/////////////////////////////////////////////////////////////////////////////////
//  API functions

////////////////////////////////////////////////////////////////////////////////
//  FUNCTION:    pal_vsnprintf                                                //
//                                                                            //
//  DESCRIPTION: Minimal vsnprintf                                            //
//                                                                            //
//  INPUTS:      size - Size of string container in chars                     //
//               psz_format - Print formating string (as for printf).         //
//               args       - Standard va_list set of args                    //
//                                                                            //
//  OUTPUTS:     str  - pointer to string container                           //
//                                                                            //
//  RETURNS:     Number of characters added to string                         //
//                                                                            //
//  NOTES:       Supports %%, field width, string alignment, pad by 0.        //
//               %c %s %d %i %u %x and %X                                     //
//               Arguments assumed to be int sized                            //
//                                                                            //
//  CONTEXT:     None                                                         //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

int pal_vsnprintf ( char       *str, 
                    int         size, 
                    const char *format, 
                    va_list     args
                  )
{
  int  width, pad;
  int  pc = 0;
  char scr[2];
  int  full = 0;

  for (; format && *format; ++format)
  {
    if (*format == '%')
    {
      ++format;
      width = pad = 0;
      
      if (*format == '\0') break;
      
      if (*format == '%') pc += _pal_snputs(&str[pc], (size-pc), &full, "%", 0, 0);

      if (*format == '-') 
      {
        ++format;
        pad = PAL_VSNP_PAD_RIGHT;
      }
      while (*format == '0') 
      {
        ++format;
        pad |= PAL_VSNP_PAD_ZERO;
      }
      
      for ( ; *format >= '0' && *format <= '9'; ++format) 
      {
        width *= 10;
        width += *format - '0';
      }

      /* Roughly in order of frequent use */
      if( *format == 'x' ) 
      {
         pc += _pal_snputi (&str[pc], (size-pc), &full, va_arg( args, int ), 16, 0, width, pad, 'a');
         continue;
      }
      else if ( *format == 'd' ) 
      {
        pc += _pal_snputi (&str[pc], (size-pc), &full, va_arg( args, int ), 10, 1, width, pad, 'a');
        continue;
      }
      else if ( *format == 'i' ) 
      {
        pc += _pal_snputi (&str[pc], (size-pc), &full, va_arg( args, int ), 10, 1, width, pad, 'a');
        continue;
      }
      else if ( *format == 's' ) 
      {
        char *s = (char *)va_arg( args, int );
        pc += _pal_snputs(&str[pc], (size-pc), &full, s?s:"(null)", width, pad);
        continue;
      }
      else if ( *format == 'c' ) 
      {
        /* char are converted to int then pushed on the stack */
        scr[0] = (char)va_arg( args, int );
        scr[1] = '\0';
        pc += _pal_snputs(&str[pc], (size-pc), &full, scr, width, pad);
        continue;
      }
      else if ( *format == 'X' ) 
      {
        pc += _pal_snputi (&str[pc], (size-pc), &full, va_arg( args, int ), 16, 0, width, pad, 'A');
        continue;
      }
      else if ( *format == 'u' ) 
      {
        pc += _pal_snputi (&str[pc], (size-pc), &full, va_arg( args, int ), 10, 0, width, pad, 'a');
        continue;
      }
      
      /* Unrecognised formats dropped silently */
    }
    else
    {
      if ( pc < size )
      {
        str[pc++] = *format;
      }
      else
      {
        full = 1;
      }
    }

    if ( full )
    {
      pc = size-1;
      break;
    }
  }

  str[pc] = '\0';
  
  va_end( args );
  
  return pc;
}


////////////////////////////////////////////////////////////////////////////////
//  FUNCTION:    pal_sprintf                                                  //
//                                                                            //
//  DESCRIPTION: Provide embedded version replacement for sprintf             //
//                                                                            //
//  INPUTS:      str  - pointer to string container                           //
//               size - Size of string container in chars                     //
//               psz_format - Print formating string (as for printf).         //
//               ...    - Additional parameters depending upon the contents   //
//                        of string.                                          //
//                                                                            //
//  OUTPUTS:     None.                                                        //
//                                                                            //
//  RETURNS:     Number of characters added to string                         //
//                                                                            //
//  NOTES:       None.                                                        //
//                                                                            //
//  CONTEXT:     None                                                         //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

int pal_sprintf ( char *str, int size, const char *psz_format, ...)
{
  va_list arg;
  int     nRetVal = 0;

  va_start( arg, psz_format);

  nRetVal = pal_vsnprintf ( str, size, ( const char *)psz_format, arg );
  
  va_end(arg);
  
  return nRetVal;
}

/* End of file */
