///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     SIPP engine
///

#ifdef SIPP_SCRT_ENABLE_OPIPE

/////////////////////////////////////////////////////////////////////////////////
//  Header Files
/////////////////////////////////////////////////////////////////////////////////

#include <sipp.h>
#include <sippInternal.h>

#ifndef SIPP_PC
#include <DrvLeonL2C.h>
#include <DrvCpr.h>
#include <DrvIcb.h>
#include <DrvCmxDma.h>
#endif

/////////////////////////////////////////////////////////////////////////////////
//  Local Macros
/////////////////////////////////////////////////////////////////////////////////

//#define SIPP_OPIPE_RT_DBG_ENABLE

#ifdef SIPP_OPIPE_RT_DBG_ENABLE

#define LOCAL_DBG_ARR_HIST_SIZE 1024
u32 localOPipeDbrArr[LOCAL_DBG_ARR_HIST_SIZE];
u32 localOPipeDbrArrIdx = 0;

#define SIPP_OPIPE_RT_DBG(x) {\
                                 localOPipeDbrArr[localOPipeDbrArrIdx++] = x;\
                                 if (localOPipeDbrArrIdx == LOCAL_DBG_ARR_HIST_SIZE) localOPipeDbrArrIdx = 0;\
                             }
#else
#define SIPP_OPIPE_RT_DBG(x)
#endif

#define USE_NEW_IBFL
#define USE_NEW_LOOP
#define USE_NEW_OBFL

/////////////////////////////////////////////////////////////////////////////////
//  Global Variables
/////////////////////////////////////////////////////////////////////////////////

extern pSIPP_HW_SESSION pgSippHW;

/////////////////////////////////////////////////////////////////////////////////
//  Function Prototypes
/////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////
//  FUNCTION:    sippOPipeRuntimeFrameReset                                       //
//                                                                                //
//  DESCRIPTION: Reset runtime variables                                          //
//                                                                                //
//  INPUTS:      pipeLine - Pipeline                                              //
//                                                                                //
//  OUTPUTS:     None.                                                            //
//                                                                                //
//  RETURNS:     None.                                                            //
//                                                                                //
//  NOTES:       None.                                                            //
//                                                                                //
//  CONTEXT:     This function may be called from any context                     //
//                                                                                //
////////////////////////////////////////////////////////////////////////////////////

void sippOPipeRuntimeFrameReset (SippPipeline * pl)
{
    u32 i, o;

    for (i = 0; i < pl->numManagedBufs; i++)
    {
        pSippOPipeBuf managedBuf            = &pl->managedBufList[i];
        managedBuf->pBuf->internalFillLevel = 0;
        managedBuf->linesNextFill           = 0;

        #ifdef SIPP_ADD_OPIPE_RT_CHECKS
        managedBuf->lastOutLineServiced     = 0x0;
        #endif

        for (o = 0; o < managedBuf->numClients; o++)
        {
            managedBuf->clientCount[o]      = 0x0;
            managedBuf->sinkLineCount[o]    = 0x0;
            #ifdef SIPP_ADD_OPIPE_RT_CHECKS
            managedBuf->lastLineServiced[o] = 0x0;
            #endif
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////
//  FUNCTION:    sippOPipeFinished                                                //
//                                                                                //
//  DESCRIPTION: Check for pipeline frame completion                              //
//                                                                                //
//  INPUTS:      pipeLine - Pipeline                                              //
//                                                                                //
//  OUTPUTS:     None.                                                            //
//                                                                                //
//  RETURNS:     None.                                                            //
//                                                                                //
//  NOTES:       A pipe is considered finished when all its dma sinks have copied //
//               all lines to destination.                                        //
//                                                                                //
//  CONTEXT:     This function may be called from any context                     //
//                                                                                //
////////////////////////////////////////////////////////////////////////////////////

u32 sippOPipeFinished (SippPipeline * pPipe)
{
    u32 i, clientIdx;

    for (i = 0; i < pPipe->numManagedBufs; i++)
    {
        pSippOPipeBuf managedBuf = &pPipe->managedBufList[i];

        if (managedBuf->pFilt->unit != SIPP_DMA_ID)
        {
            for (clientIdx = 0; clientIdx < managedBuf->numClients; clientIdx++)
            {
                if (managedBuf->clientCountMap[clientIdx] == (0x1 << SIPP_DMA_ID))
                {
                    if (managedBuf->sinkLineCount[clientIdx] < managedBuf->pFilt->outputH)
                    {
                        return 0;
                    }
                }
            }
        }
    }

    return 0x1;
}

////////////////////////////////////////////////////////////////////////////////////
//  FUNCTION:    sippOpipeIbflDecAllClients                                       //
//                                                                                //
//  DESCRIPTION: Check that all clients of a managed buffer have completed a      //
//               certain amount of consumption                                    //
//                                                                                //
//  INPUTS:      pManagedBuf - Pointger to managed buffer                         //
//                                                                                //
//  OUTPUTS:     None.                                                            //
//                                                                                //
//  RETURNS:     The number of lines to be made free in a buffer.                 //
//                                                                                //
//  NOTES:       For simplicity, this function only triggers calls when all       //
//               clients have onsumed a number of lines equal to the irq rate of  //
//               the producing filter.                                            //
//                                                                                //
//  CONTEXT:     Only called in irq context (assuming dma callbacks are in irq    //
//               context?)                                                        //
//                                                                                //
////////////////////////////////////////////////////////////////////////////////////

u32  sippOpipeIbflDecAllClients (pSippOPipeBuf pManagedBuf)
{
    u32 retVal;
    u32 triggerVal;
    u32 clientIdx;

    retVal = triggerVal = (1 << pManagedBuf->pFilt->irqRatePow);

    for (clientIdx = 0x0; (clientIdx < pManagedBuf->numClients) && (retVal); clientIdx++)
    {
        if (pManagedBuf->clientCount[clientIdx] < triggerVal) retVal = 0x0;
    }

    if (retVal)
    {
        for (clientIdx = 0x0; clientIdx < pManagedBuf->numClients; clientIdx++)
        {
            pManagedBuf->clientCount[clientIdx] -= triggerVal;
        }
    }

    return retVal;
}

#ifndef SIPP_PC

////////////////////////////////////////////////////////////////////////////////////
//  FUNCTION:    sippGetNextDmaDsc                                                //
//                                                                                //
//  DESCRIPTION: Return next descriptor & handler in cicular fashion              //
//                                                                                //
//  INPUTS:      ptrFilt - DMA filter                                             //
//                                                                                //
//  OUTPUTS:     None.                                                            //
//                                                                                //
//  RETURNS:     dmaTransactionList * - pointer to head of a linked list of       //
//               transactions.                                                    //
//                                                                                //
//  NOTES:       None.                                                            //
//                                                                                //
//  CONTEXT:     This function may be called from any context                     //
//                                                                                //
////////////////////////////////////////////////////////////////////////////////////

dmaTransactionList * getNextDmaDsc (SippFilter  * ptrFilt)
{
    DmaParam *           param      = (DmaParam *)ptrFilt->params;
    DMATransDesc *       pTransList = param->pTransList;
    dmaTransactionList * ret        = &pTransList->sippDmaDsc[pTransList->curDsc].dmaDsc;

    pTransList->curDsc++;
    if (pTransList->curDsc == SIPP_NUM_DESCS_PER_CDMA)
    {
       pTransList->curDsc  = 0;
    }

    return ret;
}

#endif

#ifndef SIPP_PC

////////////////////////////////////////////////////////////////////////////////////
//  FUNCTION:    dmaSourceIrqHandler                                              //
//                                                                                //
//  DESCRIPTION: CMX DMA driver transaction complete callback for source filters  //
//                                                                                //
//  INPUTS:      transHnd - DMA transaction handle                                //
//                                                                                //
//  OUTPUTS:     None.                                                            //
//                                                                                //
//  RETURNS:     None.                                                            //
//                                                                                //
//  NOTES:       None.                                                            //
//                                                                                //
//  CONTEXT:     This function may be called from any context                     //
//                                                                                //
////////////////////////////////////////////////////////////////////////////////////

void dmaSourceIrqHandler (dmaTransactionList* transHnd, void* userContext)
{
    UNUSED (transHnd);

    u32 i;
    pSippOPipeBuf pManagedBuf  = (pSippOPipeBuf)userContext;

    for(i = 0; i < pManagedBuf->linesNextFill; i++)
    {
        SET_REG_WORD(SIPP_IBFL_INC_ADR, pManagedBuf->pBuf->hwInputBufId);
    }

    SIPP_OPIPE_RT_DBG (0xEE00);
}

#endif

////////////////////////////////////////////////////////////////////////////////////
//  FUNCTION:    sippOPipeIbflDecIsr                                              //
//                                                                                //
//  DESCRIPTION: SIPP input buffer decrement ISR handler                          //
//                                                                                //
//  INPUTS:      irqSrc - current Interupt source                                 //
//                                                                                //
//  OUTPUTS:     None.                                                            //
//                                                                                //
//  RETURNS:     None.                                                            //
//                                                                                //
//  NOTES:       Needs multi-stream capability added                              //
//                                                                                //
//  CONTEXT:     This function may be called from any context                     //
//                                                                                //
////////////////////////////////////////////////////////////////////////////////////

void sippOPipeIbflDecIsr(u32 irqSrc)
{
    SippFilter *   fptr;
    u32            idx;
    SippPipeline * pPipe;
    u32            pipeIdx;

    u32 status = GET_REG_WORD_VAL(SIPP_INT0_STATUS_ADR);

    SIPP_OPIPE_RT_DBG (0xEE01);

    for (pipeIdx = 0; pipeIdx < SIPP_MAX_SUPPORTED_HW_PIPELINES; pipeIdx++)
    {
        if (pgSippHW->pSippLoadedHWPipe[pipeIdx])
        {
            pPipe    = pgSippHW->pSippLoadedHWPipe[pipeIdx];

            for (idx = 0; idx < pPipe->numManagedBufs; idx++)
            {
                pSippOPipeBuf managedBuf   = &pPipe->managedBufList[idx];
                u32           bufsToHandle = status & managedBuf->pBuf->hwInputBufId;

                if (bufsToHandle)
                {
                    u32 clientIdx;
                    fptr =  managedBuf->pFilt;

                    // Clear flag
                    SET_REG_WORD(SIPP_INT0_CLEAR_ADR, bufsToHandle);

                    // Analyse each entry in turn in the client count map list
                    // If it is signalled now, increment the client count by the appropriate amount
                    // (the irq rate for the generating filter)

                    // *** Note this assumes that we are able to uniquely service every ibfl interrupt
                    // *** from the same interrupt source - if there was a delay somehow a filter may be able
                    // *** to say decrement my input buffer twice. This algo would not catch that situation
                    for (clientIdx = 0x0; clientIdx < managedBuf->numClients; clientIdx++)
                    {
                        if (managedBuf->clientCountMap[clientIdx] & bufsToHandle)
                        {
                            #ifdef SIPP_ADD_OPIPE_RT_CHECKS
                            #ifdef USE_NEW_IBFL
                            // We should read the IBUF.CTX.LI bits and compare with last good read
                            // to establish if we have potentially had > 1 interrupt since we last ram this
                            // check - This can happen if the buffer size is >= 2 * irq rate of teh consumer
                            u16 lastLineCount = (u16)(*managedBuf->iBufCtxReg[clientIdx] & 0xFFFF);
                            u16 linesToService;

                            // Rnd this down to a multiple of irq rate power
                            lastLineCount &= (~(managedBuf->clientCountIncr[clientIdx] - 0x1));

                            if (lastLineCount >= managedBuf->lastLineServiced[clientIdx])
                            {
                                linesToService                          = lastLineCount - managedBuf->lastLineServiced[clientIdx];
                                managedBuf->lastLineServiced[clientIdx] = lastLineCount;
                            }
                            else
                            {
                                linesToService                          = fptr->outputH - managedBuf->lastLineServiced[clientIdx];
                                managedBuf->lastLineServiced[clientIdx] = fptr->outputH;
                            }

                            while (linesToService)
                            {
                                managedBuf->clientCount[clientIdx] += managedBuf->clientCountIncr[clientIdx];
                                linesToService                     -= managedBuf->clientCountIncr[clientIdx];
                            }

                            #ifdef SIPP_OPIPE_RT_DBG_ENABLE
                            if (linesToService > (u32)managedBuf->clientCountIncr[clientIdx])
                            {
                                printf ("linesToService 0x%04x 0x%04lx pipeIdx %lu\n", linesToService, managedBuf->clientCountIncr[clientIdx], (u32)pipeIdx);
                            }
                            #endif

                            #else
                            managedBuf->clientCount[clientIdx] += managedBuf->clientCountIncr[clientIdx];
                            #endif
                            #else
                            managedBuf->clientCount[clientIdx] += managedBuf->clientCountIncr[clientIdx];
                            #endif
                        }
                    }

                    // Call decrement check function
                    // for simplicity, this function only should trigger calls when all clients have
                    // consumed a number of lines equal to the irq rate of the producing filter
                    #ifdef SIPP_ADD_OPIPE_RT_CHECKS
                    #ifdef USE_NEW_LOOP
                    while (sippOpipeIbflDecAllClients (managedBuf))
                    #else
                    if (sippOpipeIbflDecAllClients (managedBuf))
                    #endif
                    #else
                    if (sippOpipeIbflDecAllClients (managedBuf))
                    #endif
                    {
                        // DMA-in output buffer: DDR->CMX transfer
                        if (fptr->nParents == 0)
                        {
                            #ifndef SIPP_PC
                            u32        srcAddr       = 0xDEADBEEF;
                            u32        dstAddr       = 0xDEADBEEF;
                            u32        fillStartLine = managedBuf->pBuf->internalFillLevel % fptr->nLines[managedBuf->oBufIdx];
                            DmaParam * param         = (DmaParam *)fptr->params;

                            if (managedBuf->pBuf->internalFillLevel < fptr->outputH)
                            {
                                {
                                    managedBuf->linesNextFill = fptr->outputH - managedBuf->pBuf->internalFillLevel;
                                    if (managedBuf->linesNextFill > (1 << fptr->irqRatePow))
                                    {
                                        managedBuf->linesNextFill = 1 << fptr->irqRatePow;
                                    }
                                }

                                srcAddr = (u32)(param->ddrAddr + ((managedBuf->pBuf->internalFillLevel * fptr->outputW) << fptr->bytesPerPix));
                                dstAddr = (u32)fptr->outputBuffer[managedBuf->oBufIdx] + ((fillStartLine * fptr->lineStride[managedBuf->oBufIdx]) << fptr->bytesPerPix);

                                dmaTransactionList *dmaD = getNextDmaDsc (fptr);

                                dmaD->length = ((fptr->outputW * managedBuf->linesNextFill) << fptr->bytesPerPix); // This should use bpp and calculate properly
                                dmaD->src    = (void *)srcAddr;
                                dmaD->dst    = (void *)dstAddr;

                                managedBuf->pBuf->internalFillLevel += managedBuf->linesNextFill;
                                DrvCmxDmaStartTaskAsync (dmaD, dmaSourceIrqHandler, managedBuf);
                            }
                            #endif
                        }
                        else
                        {
                            for (int j = 0; j < (1 << fptr->irqRatePow); j++)
                            {
                                SET_REG_WORD(SIPP_OBFL_DEC_ADR, managedBuf->pBuf->hwOutputBufId);
                            }
                        }
                    }
                }
            }
        }
    }

    #ifndef SIPP_PC
    DrvIcbIrqClear(irqSrc);
    #endif
}

#ifndef SIPP_PC

////////////////////////////////////////////////////////////////////////////////////
//  FUNCTION:    dmaSinkIrqHandler                                                //
//                                                                                //
//  DESCRIPTION: CMX DMA driver transaction complete callback for sink filters    //
//                                                                                //
//  INPUTS:      transHnd - DMA transaction handle                                //
//                                                                                //
//  OUTPUTS:     None.                                                            //
//                                                                                //
//  RETURNS:     None.                                                            //
//                                                                                //
//  NOTES:       None.                                                            //
//                                                                                //
//  CONTEXT:     This function may be called from any context                     //
//                                                                                //
////////////////////////////////////////////////////////////////////////////////////

void dmaSinkIrqHandler (dmaTransactionList * transHnd, void * userContext)
{
    u32 i;
    pSippOPipeBuf managedBuf   = (pSippOPipeBuf)userContext;
    // Re-cast transHnd in order to get access to some additional info
    SippDmaDesc * pSippDmaDesc = (SippDmaDesc *)transHnd;
    SippFilter *  ptrFilt      = (SippFilter *)managedBuf->pFilt;

    SIPP_OPIPE_RT_DBG (0xEE03);

    // Increment the completes for all transactions just completed
    while (pSippDmaDesc)
    {
        managedBuf->clientCount[pSippDmaDesc->managedBufClientIdx]   += pSippDmaDesc->linesInTransaction;
        managedBuf->sinkLineCount[pSippDmaDesc->managedBufClientIdx] += pSippDmaDesc->linesInTransaction;

        // Move to next
        pSippDmaDesc = pSippDmaDesc->dmaDsc.linkAddress;
    }

    #ifdef SIPP_ADD_OPIPE_RT_CHECKS
    #ifdef USE_NEW_LOOP
    while (sippOpipeIbflDecAllClients (managedBuf))
    #else
    if (sippOpipeIbflDecAllClients (managedBuf))
    #endif
    #else
    if (sippOpipeIbflDecAllClients (managedBuf))
    #endif
    {
        for(i = 0; i < (u32)(1 << ptrFilt->irqRatePow); i++)
        {
            SET_REG_WORD(SIPP_OBFL_DEC_ADR, managedBuf->pBuf->hwOutputBufId);
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////
//  FUNCTION:    dmaLastSinkIrqHandler                                            //
//                                                                                //
//  DESCRIPTION: CMX DMA driver transaction complete callback for sink filters    //
//               when carrying out their final transactions for a frame           //
//                                                                                //
//  INPUTS:      transHnd - DMA transaction handle                                //
//                                                                                //
//  OUTPUTS:     None.                                                            //
//                                                                                //
//  RETURNS:     None.                                                            //
//                                                                                //
//  NOTES:       None.                                                            //
//                                                                                //
//  CONTEXT:     This function may be called from any context                     //
//                                                                                //
////////////////////////////////////////////////////////////////////////////////////

void dmaLastSinkIrqHandler(dmaTransactionList * transHnd, void * userContext)
{
    u32 i;
    pSippOPipeBuf managedBuf   = (pSippOPipeBuf)userContext;
    // Re-cast transHnd in order to get access to some additional info
    SippDmaDesc * pSippDmaDesc = (SippDmaDesc *)transHnd;
    SippFilter *  ptrFilt      = (SippFilter *)managedBuf->pFilt;

    SIPP_OPIPE_RT_DBG (0xEE04);

    // Increment the counts for all transactions just completed
    while (pSippDmaDesc)
    {
        managedBuf->clientCount[pSippDmaDesc->managedBufClientIdx]   += pSippDmaDesc->linesInTransaction;
        managedBuf->sinkLineCount[pSippDmaDesc->managedBufClientIdx] += pSippDmaDesc->linesInTransaction;

        // Move to next
        pSippDmaDesc = pSippDmaDesc->dmaDsc.linkAddress;
    }

    #ifdef SIPP_ADD_OPIPE_RT_CHECKS
    #ifdef USE_NEW_LOOP
    while (sippOpipeIbflDecAllClients (managedBuf))
    #else
    if (sippOpipeIbflDecAllClients (managedBuf))
    #endif
    #else
    if (sippOpipeIbflDecAllClients (managedBuf))
    #endif
    {
        for(i = 0; i < (u32)(1 << ptrFilt->irqRatePow); i++)
        {
            SET_REG_WORD(SIPP_OBFL_DEC_ADR, managedBuf->pBuf->hwOutputBufId);
        }
    }

    // If current sink finished, check for pipe finish
    if (sippOPipeFinished(ptrFilt->pPipe))
    {
        SIPP_OPIPE_RT_DBG (0xEE06);

        ptSippPipelineSuper pLocSPipe = (ptSippPipelineSuper)ptrFilt->pPipe;
        // End of frame callback
        // Could return some stats as the data
        sippEventNotify (ptrFilt->pPipe,
                         eSIPP_PIPELINE_FRAME_DONE,
                         (SIPP_PIPELINE_EVENT_DATA *)0);

        // Remove from the loaded list
        sippHWSessionRemoveLoadedPipe (ptrFilt->pPipe);
        sippHWSessionRemoveActiveLists (ptrFilt->pPipe,
                                        pLocSPipe->uHWPipeID);

    }

    // transHnd may now be a linked list of two transactions - reset it to one
    // for the next frame
    transHnd->linkAddress = 0;
}

#endif

////////////////////////////////////////////////////////////////////////////////////
//  FUNCTION:    sippOPipeEOFTransaction                                          //
//                                                                                //
//  DESCRIPTION:                                                                  //
//                                                                                //
//  INPUTS:                                                                       //
//                                                                                //
//  OUTPUTS:     None.                                                            //
//                                                                                //
//  RETURNS:     None.                                                            //
//                                                                                //
//  NOTES:       None.                                                            //
//                                                                                //
//  CONTEXT:     This function may be called from any context                     //
//                                                                                //
////////////////////////////////////////////////////////////////////////////////////

void sippOPipeEOFTransaction (SippFilter * fptr,
                              SippFilter * cons,
                              pSippOPipeBuf managedBuf)
{
    #ifdef SIPP_PC
    UNUSED(fptr);
    UNUSED(cons);
    UNUSED(managedBuf);
    #else

    u32 firstTrans = 0;
    u32 lastTrans  = 0;
    u32 obufIdx       = managedBuf->oBufIdx;
    u32 fillStartLine = managedBuf->pBuf->internalFillLevel % fptr->nLines[obufIdx];

    u32 srcAddr = 0xDEADBEEF;
    u32 dstAddr = 0xDEADBEEF;

    SIPP_OPIPE_RT_DBG (0xEE08);

    firstTrans = fptr->nLines[obufIdx] - fillStartLine;

    if (firstTrans > (fptr->outputH - managedBuf->pBuf->internalFillLevel))
    {
        firstTrans = fptr->outputH - managedBuf->pBuf->internalFillLevel;
    }

    if (firstTrans)
    {
        DmaParam *    param = (DmaParam *)cons->params;
        SippDmaDesc * pSippDmaDesc;

        srcAddr = (u32)fptr->outputBuffer[obufIdx] + ((fillStartLine * fptr->lineStride[managedBuf->oBufIdx]) << fptr->bytesPerPix);
        dstAddr = (u32)param->ddrAddr + ((managedBuf->pBuf->internalFillLevel * fptr->outputW) << fptr->bytesPerPix);

        dmaTransactionList *dmaD0 = getNextDmaDsc (cons);

        dmaD0->length = ((cons->outputW * firstTrans) << cons->bytesPerPix); // This should use bpp and calculate properly
        dmaD0->src    = (void *)srcAddr;
        dmaD0->dst    = (void *)dstAddr;

        pSippDmaDesc                     = (SippDmaDesc *)dmaD0;
        pSippDmaDesc->linesInTransaction = firstTrans;

        managedBuf->pBuf->internalFillLevel += firstTrans;
        fillStartLine = managedBuf->pBuf->internalFillLevel % fptr->nLines[obufIdx];

        // Increment src/dst buffers' address for last transfer
        srcAddr = (u32)fptr->outputBuffer[obufIdx] + ((fillStartLine * fptr->lineStride[managedBuf->oBufIdx]) << fptr->bytesPerPix);
        dstAddr += ((fptr->outputW * firstTrans) << fptr->bytesPerPix);

        lastTrans = fptr->outputH - managedBuf->pBuf->internalFillLevel;

        if (lastTrans)
        {
            dmaTransactionList * dmaD1 = getNextDmaDsc (cons);

            dmaD1->length = ((cons->outputW * lastTrans) << cons->bytesPerPix); // This should use bpp and calculate properly
            dmaD1->src    = (void *)srcAddr;
            dmaD1->dst    = (void *)dstAddr;

            pSippDmaDesc                     = (SippDmaDesc *)dmaD1;
            pSippDmaDesc->linesInTransaction = lastTrans;

            DrvCmxDmaLinkTasks (dmaD0, 1, dmaD1);
        }

        DrvCmxDmaStartTaskAsync (dmaD0, dmaLastSinkIrqHandler, managedBuf);

        managedBuf->pBuf->internalFillLevel += lastTrans;
    }
    #endif
}

////////////////////////////////////////////////////////////////////////////////////
//  FUNCTION:    sippOpipeHandleObflInc                                           //
//                                                                                //
//  DESCRIPTION: Main handler for output buffer fill level increment interrupt    //
//                                                                                //
//  INPUTS:      pPipe  - Pipeline to consider                                    //
//               status - SIPP INT1 status                                        //
//               eof    - called from an end of frame interrupt                   //
//                                                                                //
//  OUTPUTS:     None.                                                            //
//                                                                                //
//  RETURNS:     None.                                                            //
//                                                                                //
//  NOTES:       This is a multi-pipe capable function                            //
//                                                                                //
//  CONTEXT:     ISR context                                                      //
//                                                                                //
////////////////////////////////////////////////////////////////////////////////////

void sippOpipeHandleObflInc (SippPipeline * pPipe, u32 status, u32 eof)
{
    SippFilter *    fptr;
    psSchLineBuffer pBuf;
    u32             idx, consIdx;

    SIPP_OPIPE_RT_DBG (0xEE02);
    SIPP_OPIPE_RT_DBG (eof);
    SIPP_OPIPE_RT_DBG (status);

    for (idx = 0; idx < pPipe->numManagedBufs; idx++)
    {
        pSippOPipeBuf managedBuf   = &pPipe->managedBufList[idx];
        u32           bufsToHandle = status & managedBuf->pBuf->hwOutputBufId;

        if (status & bufsToHandle)
        {
            fptr = managedBuf->pFilt;
            pBuf = managedBuf->pBuf;

            #ifdef SIPP_ADD_OPIPE_RT_CHECKS
            #ifdef USE_NEW_OBFL
            // We should read the OBUF.CTX.LO bits and compare with last good read
            // to establish if we have potentially had > 1 interrupt since we last ram this
            // check - This can happen if the buffer size is >= 2 * irq rate of the consumer
            u16 lastLineCount = (u16)(((*managedBuf->oBufCtxReg) >> 16) & 0x3FF) ;
            u32 numLineIncs;

            if (lastLineCount > managedBuf->lastOutLineServiced)
            {
                numLineIncs = (lastLineCount - managedBuf->lastOutLineServiced);
            }
            else if (lastLineCount < managedBuf->lastOutLineServiced)
            {
                // Wrapped buffer
                numLineIncs = (pBuf->numLines - managedBuf->lastOutLineServiced) + lastLineCount;
            }
            else
            {
                // Equal - check OBFL - if we are full - assume we wrapped, if we are empty
                // this must be a residual interrupt already serviced
                u32 obflRegAddr = (u32)((u32)managedBuf->oBufCtxReg - 0x4);
                u32 notEmpty    = (u32)((*(u32 *)obflRegAddr) & 0x3FF);

                if (notEmpty)
                {
                    numLineIncs = pBuf->numLines;
                }
                else
                {
                    numLineIncs = 0;
                }
            }

            // Rnd this down to a multiple of irq rate power
            numLineIncs &= (~((0x1 << fptr->irqRatePow) - 0x1));

            #ifdef SIPP_OPIPE_RT_DBG_ENABLE
            if (numLineIncs > (u32)(1 << fptr->irqRatePow))
            {
                printf ("lastLineCount 0x%04x 0x%04x 0x%08lx, numLineIncs %lu\n", lastLineCount, managedBuf->lastOutLineServiced, (u32)(*managedBuf->oBufCtxReg), numLineIncs);
            }
            #endif

            managedBuf->lastOutLineServiced += numLineIncs;

            if (managedBuf->lastOutLineServiced >= pBuf->numLines)
            {
                managedBuf->lastOutLineServiced -= pBuf->numLines;
            }

            #else
            u32 numLineIncs = 1 << fptr->irqRatePow;
            #endif
            #else
            u32 numLineIncs = 1 << fptr->irqRatePow;
            #endif

            // Clear flag
            if (eof == 0) SET_REG_WORD(SIPP_INT1_CLEAR_ADR, bufsToHandle);

            // Adding this check for full belt and braces approach
            // in case we also manmage to get a numLineIncs == 0 in all this
            if (numLineIncs)
            {
                for (consIdx = 0; consIdx < fptr->nCons; consIdx++)
                {
                    SippFilter * cons = fptr->cons[consIdx];

                    // Consumer is a DMA out buffer: CMX->DDR transfer
                    // Also check DMA consumer is actually consuming from the buffer under inspection
                    if ((cons->nCons == 0) &&
                        (pBuf == cons->iBufs[0x0]))
                    {
                        bool bLastTrans = FALSE;
                        managedBuf->linesNextFill = 0;

                        // Check if data is still to be fed
                        if (pBuf->internalFillLevel < fptr->outputH)
                        {
                            if (eof == 0x0)
                            {
                                managedBuf->linesNextFill = fptr->outputH - pBuf->internalFillLevel;

                                if (managedBuf->linesNextFill > (numLineIncs))
                                {
                                    managedBuf->linesNextFill = numLineIncs;
                                }
                                else if (managedBuf->linesNextFill + pBuf->internalFillLevel == fptr->outputH)
                                {
                                    bLastTrans = TRUE;
                                }
                            }
                            else
                            {
                                // At end of frame, handle all the data still left
                                sippOPipeEOFTransaction (fptr, cons, managedBuf);
                                return;
                            }
                        }

                        if (managedBuf->linesNextFill)
                        {
                            #ifndef SIPP_PC
                            u32           srcAddr;
                            u32           dstAddr;
                            SippDmaDesc * pSippDmaDesc;
                            DmaParam *    param = (DmaParam *)cons->params;

                            srcAddr = (u32)fptr->outputBuffer[managedBuf->oBufIdx] + (((pBuf->internalFillLevel % fptr->nLines[managedBuf->oBufIdx]) * fptr->lineStride[managedBuf->oBufIdx]) << fptr->bytesPerPix);
                            dstAddr = (u32)param->ddrAddr + ((cons->outputW * pBuf->internalFillLevel) << cons->bytesPerPix);

                            dmaTransactionList * dmaD = getNextDmaDsc (cons);

                            dmaD->length = ((cons->outputW * managedBuf->linesNextFill) << cons->bytesPerPix); // This should use bpp and calculate properly
                            dmaD->src    = (void *)srcAddr;
                            dmaD->dst    = (void *)dstAddr;

                            pSippDmaDesc                     = (SippDmaDesc *)dmaD;
                            pSippDmaDesc->linesInTransaction = managedBuf->linesNextFill;

                            DrvCmxDmaStartTaskAsync (dmaD,
                                                     bLastTrans ? dmaLastSinkIrqHandler : dmaSinkIrqHandler,
                                                     managedBuf);

                            pBuf->internalFillLevel += managedBuf->linesNextFill;

                            #endif
                        }
                    }
                }
            }

            // Buffer between HW filters: increment the other HW filter's input buffer level
            if (pBuf->hwInputBufId)
            {
                if (eof)
                {
                    numLineIncs = (fptr->outputH > pBuf->internalFillLevel) ? fptr->outputH - pBuf->internalFillLevel : 0x0;
                }

                for(u32 j = 0; j < numLineIncs; j++)
                {
                    SET_REG_WORD(SIPP_IBFL_INC_ADR, pBuf->hwInputBufId);
                }

                pBuf->internalFillLevel += numLineIncs;
            }
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////
//  FUNCTION:    sippOPipeObflIncIsr                                              //
//                                                                                //
//  DESCRIPTION: Output buffer fill level increment ISR                           //
//                                                                                //
//  INPUTS:      irqSrc                                                           //
//                                                                                //
//  OUTPUTS:     None.                                                            //
//                                                                                //
//  RETURNS:     None.                                                            //
//                                                                                //
//  NOTES:       This is a multi-pipe capable function                            //
//                                                                                //
//  CONTEXT:     ISR context                                                      //
//                                                                                //
////////////////////////////////////////////////////////////////////////////////////

void sippOPipeObflIncIsr (u32 irqSrc)
{
    SippPipeline * pPipe;
    u32            pipeIdx;

    u32 status = GET_REG_WORD_VAL(SIPP_INT1_STATUS_ADR);

    for (pipeIdx = 0; pipeIdx < SIPP_MAX_SUPPORTED_HW_PIPELINES; pipeIdx++)
    {
        if (pgSippHW->pSippLoadedHWPipe[pipeIdx])
        {
            pPipe    = pgSippHW->pSippLoadedHWPipe[pipeIdx];

            sippOpipeHandleObflInc (pPipe, status, 0x0);
        }
    }

    #ifndef SIPP_PC
    DrvIcbIrqClear (irqSrc);
    #endif
}

////////////////////////////////////////////////////////////////////////////////////
//  FUNCTION:    sippOPipeEofIsr                                                  //
//                                                                                //
//  DESCRIPTION: End of frame ISR                                                 //
//                                                                                //
//  INPUTS:      irqSrc                                                           //
//                                                                                //
//  OUTPUTS:     None.                                                            //
//                                                                                //
//  RETURNS:     None.                                                            //
//                                                                                //
//  NOTES:       This is a multi-pipe capable function                            //
//                                                                                //
//  CONTEXT:     ISR context                                                      //
//                                                                                //
////////////////////////////////////////////////////////////////////////////////////

void sippOPipeEofIsr(u32 irqSrc)
{
    SippPipeline * pPipe;
    u32            pipeIdx;

    u32 status = GET_REG_WORD_VAL(SIPP_INT2_STATUS_ADR);

    for (pipeIdx = 0; pipeIdx < SIPP_MAX_SUPPORTED_HW_PIPELINES; pipeIdx++)
    {
        if (pgSippHW->pSippLoadedHWPipe[pipeIdx])
        {
            pPipe    = pgSippHW->pSippLoadedHWPipe[pipeIdx];

            sippOpipeHandleObflInc (pPipe, status, 0x1);
        }
    }

    // Clearly not muti-stream suitable but for now...
    SET_REG_WORD(SIPP_INT2_CLEAR_ADR, status);

    #ifndef SIPP_PC
    DrvIcbIrqClear (irqSrc);
    #endif
}

////////////////////////////////////////////////////////////////////////////////////
//  FUNCTION:    sippOPipeRuntimeProcessIters                                     //
//                                                                                //
//  DESCRIPTION: Load pipeline to runtime and HW                                  //
//                                                                                //
//  INPUTS:      pPipe - pipeline to 'load' to runtime                            //
//                                                                                //
//  OUTPUTS:     None.                                                            //
//                                                                                //
//  RETURNS:     None.                                                            //
//                                                                                //
//  NOTES:                                                                        //
//                                                                                //
//  CONTEXT:     Thread context                                                   //
//                                                                                //
////////////////////////////////////////////////////////////////////////////////////

void sippOPipeRuntimeProcessIters (SippPipeline *pPipe, u32 numIters)
{
    u32                 filtIdx;
    SippFilter  *       fptr;
    pSippOPipeBuf       managedBuf;
    SIPP_PAL_CRIT_STATE uCS;

    UNUSED (numIters);

    for (filtIdx = 0; filtIdx < pPipe->nFiltersDMA; filtIdx++)
    {
        fptr = pPipe->filtersDMA[filtIdx];

        // DMA-in case: DDR->CMX transaction
        if (fptr->nParents == 0)
        {
            #ifndef SIPP_PC
            u32 srcAddr;
            u32 dstAddr;
            u32 transLength;

            DmaParam * param = (DmaParam *)fptr->params;

            srcAddr     = (u32)param->ddrAddr;
            dstAddr     = (u32)fptr->outputBuffer[0x0];
            transLength = ((fptr->outputW * fptr->nLines[0x0]) << fptr->bytesPerPix);

            // First run: fill in whole input buffer
            managedBuf = fptr->oBufs[0]->pManagedBuf;
            managedBuf->linesNextFill = fptr->nLines[0x0];

            dmaTransactionList *dmaD = getNextDmaDsc (fptr);

            dmaD->length = transLength;
            dmaD->src    = (void *)srcAddr;
            dmaD->dst    = (void *)dstAddr;

            sippPalCriticalSectionBegin (&uCS);

            managedBuf->pBuf->internalFillLevel += managedBuf->linesNextFill;
            DrvCmxDmaStartTaskAsync (dmaD, dmaSourceIrqHandler, managedBuf);

            sippPalCriticalSectionEnd (uCS);

            #endif
        }
    }
}

///////////////////////////////////////////////////////////////////////
// Public API
//
// The following functions are the common hook functions to be implemented by all
// compatible runtimes called by the session controller

void sippOPipeRuntimeClaimHWResource (pSippPipeline pPipe)
{
    u32 regVal, intMask0, intMask12, idx;
    SIPP_PAL_CRIT_STATE uCS;

    // Loop thru managed buffer list
    for (idx = 0, intMask0 = 0, intMask12 = 0; idx < pPipe->numManagedBufs; idx++)
    {
        pSippOPipeBuf managedBuf   = &pPipe->managedBufList[idx];

        intMask0  |= managedBuf->pBuf->hwInputBufId;
        intMask12 |= managedBuf->pBuf->hwOutputBufId;

    }

    sippPalCriticalSectionBegin (&uCS);

    SET_REG_WORD(SIPP_INT0_CLEAR_ADR, intMask0);
    SET_REG_WORD(SIPP_INT1_CLEAR_ADR, intMask12);
    SET_REG_WORD(SIPP_INT2_CLEAR_ADR, intMask12);

    regVal = GET_REG_WORD_VAL(SIPP_INT0_ENABLE_ADR);
    SET_REG_WORD(SIPP_INT0_ENABLE_ADR, (regVal | intMask0));

    regVal = GET_REG_WORD_VAL(SIPP_INT1_ENABLE_ADR);
    SET_REG_WORD(SIPP_INT1_ENABLE_ADR, (regVal | intMask12));

    regVal = GET_REG_WORD_VAL(SIPP_INT2_ENABLE_ADR);
    SET_REG_WORD(SIPP_INT2_ENABLE_ADR, (regVal | intMask12));

    sippPalCriticalSectionEnd (uCS);
}


void sippOPipeRuntime (pSippPipeline                    pPipe,
                       eSIPP_ACCESS_SCHEDULER_EVENT     eEvent,
                       SIPP_ACCESS_SCHEDULER_EVENT_DATA pData)
{
    switch (eEvent)
    {
        case eSIPP_ACCESS_SCHEDULER_CMD_PROCESS_ITERS :
        {
            // Copy over the number of iterations
            u32 uNumIters = *(u32 *)pData;

            sippOPipeRuntimeProcessIters (pPipe,
                                          uNumIters);

        } break;
        default : break;
    }
}

////////////////////////////////////////////
// Async runtime servicing functions

u32 sippOPipeRunIterDone (SippPipeline * pPipe)
{
    UNUSED (pPipe);

    return 0;
}

void sippOPipeRunNextIter (pSippPipeline pPipe)
{
    UNUSED (pPipe);
}

#endif /* SIPP_SCRT_ENABLE_OPIPE */
