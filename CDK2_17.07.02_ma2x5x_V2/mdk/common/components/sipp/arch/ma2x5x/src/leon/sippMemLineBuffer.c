///
/// @file
/// @copyright All code copyright Movidius Ltd 2014, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     SIPP engine
///

////////////////////////////////////////////////////
// Header files
////////////////////////////////////////////////////

#include <sipp.h>
#include <sippInternal.h>

////////////////////////////////////////////////////
// Local prototypes
////////////////////////////////////////////////////

u32 sippMapRegionMapAddrToSliceZero (SippPipeline * pipe,
                                        u32         addrOffset);

////////////////////////////////////////////////////
// Globals
////////////////////////////////////////////////////
extern u8 *sippCmxBase;

////////////////////////////////////////////////////
// Code
////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////
//  FUNCTION:    sippMapRegionToCmx                                               //
//                                                                                //
//  DESCRIPTION: Taking an input memory region - map it to a CMX slice and add it //
//               to a list for the current pipe                                   //
//                                                                                //
//  INPUTS:      pipe - Pointer to relevant pipeline                              //
//               nextMemRegion - Pointer to memory region definition              //
//                                                                                //
//  OUTPUTS:     None.                                                            //
//                                                                                //
//  RETURNS:     None.                                                            //
//                                                                                //
//  NOTES:                                                                        //
//                                                                                //
////////////////////////////////////////////////////////////////////////////////////

void sippMapRegionToCmx (SippPipeline *  pipe,
                         SippMemRegion * nextMemRegion)
{
    pSippMemRegionListNode pListNode;
    pSippMemRegionListNode pPrev;
    u32                    cmxSliceNum;

    // Which CMX slice does this region lie in
    cmxSliceNum = nextMemRegion->regionOffset >> 0x11;   // assumes 128kB sized CMX slices
    pPrev       = pipe->pCmxMap->pCmxSliceRegionList[cmxSliceNum];

    // Before allocating anything - validate that the memory region does not straddle CMX slices
    if ((nextMemRegion->regionOffset + nextMemRegion->regionSize) > ((cmxSliceNum + 1) << 0x11))
    {
        // The region straddles two slices - what error should I return here?
    }

    // Allocate a list node from the CMX pool
    pListNode = (pSippMemRegionListNode)sippMemAlloc (&pipe->tHeapMCB,
                                                      vPoolPipeStructs,
                                                      sizeof(SippMemRegionListNode));

    if (pListNode)
    {
        // Populate the node
        pListNode->pNext         = (void *)0x0;
        pListNode->pNextChunkReg = (void *)0x0;
        pListNode->chainStart    = false;
        pListNode->chainLinked   = false;
        pListNode->regionAddr    = (u32)sippCmxBase + nextMemRegion->regionOffset;
        pListNode->slice0Addr    = sippMapRegionMapAddrToSliceZero (pipe,
                                                                    nextMemRegion->regionOffset);
        pListNode->regionSize    = nextMemRegion->regionSize;
        pListNode->regionUsed    = 0;
        pListNode->regionUsedPtr = &nextMemRegion->regionUsed;

        // Add to list for relevant CMX slice
        if (pPrev)
        {
            pSippMemRegionListNode pNext = (pSippMemRegionListNode)pipe->pCmxMap->pCmxSliceRegionList[cmxSliceNum]->pNext;
            pSippMemRegionListNode pPrev = pNext;

            // Add to tail
            while (pNext)
            {
                pPrev = pNext;
                pNext = (pSippMemRegionListNode)pipe->pCmxMap->pCmxSliceRegionList[cmxSliceNum]->pNext;
            }

            pPrev->pNext = pListNode;

        }
        else
        {
            pipe->pCmxMap->pCmxSliceRegionList[cmxSliceNum] = pListNode;
        }
    }
    else
    {
        // What error do I raise here?
    }
}

////////////////////////////////////////////////////////////////////////////////////
//  FUNCTION:    sippGetNextMemRegion                                             //
//                                                                                //
//  DESCRIPTION: Get the next memory region from input list                       //
//                                                                                //
//  INPUTS:      pipe - Pointer to relevant pipeline                              //
//               memRegList - Pointer to null terminated memory region list       //
//                                                                                //
//  OUTPUTS:     ppNextMemRegion -                                                //
//                                                                                //
//  RETURNS:     TRUE if output region valid, FALSE if no further regions in list //
//                                                                                //
//  NOTES:                                                                        //
//                                                                                //
////////////////////////////////////////////////////////////////////////////////////

u8 sippGetNextMemRegion (SippPipeline *   pipe,
                            SippMemRegion *  memRegList,
                            SippMemRegion ** ppNextMemRegion)
{
    u8 retVal = true;

    if ((memRegList[pipe->numMemRegions].regionOffset == 0x0) &&
        (memRegList[pipe->numMemRegions].regionSize == 0x0) &&
        (memRegList[pipe->numMemRegions].regionUsed == 0x0))
    {
        retVal = false;
    }
    else
    {
        *ppNextMemRegion = &memRegList[pipe->numMemRegions++];
    }

    return retVal;
}

////////////////////////////////////////////////////////////////////////////////////
//  FUNCTION:    sippGetNextMemRegion                                             //
//                                                                                //
//  DESCRIPTION: Get the next memory region from input list                       //
//                                                                                //
//  INPUTS:      pipe - Pointer to relevant pipeline                              //
//               memRegList - Pointer to null terminated memory region list       //
//                                                                                //
//  OUTPUTS:     ppNextMemRegion -                                                //
//                                                                                //
//  RETURNS:     TRUE if output region valid, FALSE if no further regions in list //
//                                                                                //
//  NOTES:       The HW filters share the slice config register which means they  //
//               all share a start and end slice. Consider a mixed pipeline of HW //
//               filters with say 2 shaves for the SW filters. The HW will assume //
//               that all of the output buffers addresses we write down are slice //
//               0 addresses. And also that all the filters in the pipe have      //
//               their actual locations in the slices specified in the slice      //
//               config register. Of-course this is exactly what we wish not to   //
//               be the case since the slice config start/end slices will reflect //
//               the shaves that are in use only. So we need to adjust the        //
//               addresses written down so that if for example a region started   //
//               in slice 7 but the pipline's start/end slice setup was 4 & 5 we  //
//               will write down an address modified by only 4 slice widths       //
//                                                                                //
//               When HW does its 'slice correction' on the address, it will end  //
//               up at the correct place                                          //
//                                                                                //
//               This is the same principle as that employed by                   //
//               sippComputeSwOutCt - This looks upon all the addresses as slice  //
//               0 addresses - and corects them to give the slice addresses       //
//               within the active slice ranges - so this is doing for the SW     //
//               filters what HW will do itself for the HW filters                //
//                                                                                //
//               If  SIPP_SLICE0_ADDRESS is not defined then we do not do this    //
//               correction - instead we write down the actual address and        //
//               instead tell HW that the first slice in use is slice 0. This     //
//               avoids a MoviSim issue with corrected addresses lying outside    //
//               the CMX address map                                              //
//                                                                                //
////////////////////////////////////////////////////////////////////////////////////

u32 sippMapRegionMapAddrToSliceZero (SippPipeline * pipe,
                                     u32            addrOffset)
{
    UNUSED (pipe);

	u32 sliceZeroAddr;
    #ifdef SIPP_SLICE0_ADDRESS
    u32 startSlice = pipe->gi.sliceFirst;
    u32 sliceSize  = pipe->gi.sliceSize;

    sliceZeroAddr = (u32)sippCmxBase + addrOffset - startSlice * sliceSize;
    #else
    sliceZeroAddr = (u32)sippCmxBase + addrOffset;
    printf("sliceZeroAddr = %08x\n", (int)sliceZeroAddr);
    #endif

    return sliceZeroAddr;
}


////////////////////////////////////////////////////////////////////////////////////
//  FUNCTION:    sippMemRegionAllocLineBuffer                                     //
//                                                                                //
//  DESCRIPTION: Attempt to allocate a line buffer within regions mapped to the   //
//               specified slice                                                  //
//                                                                                //
//  INPUTS:      pCmxMap - CMX Map for all the allocated memory regions           //
//               cmxSlice - CMX slice number in which line buffer should be       //
//                          allocated                                             //
//               ptrFilt - pointer to the filter whose line buffer is to be       //
//                         allocated                                              //
//                                                                                //
//  OUTPUTS:     TRUE if line buffer could be allocated in the specified slice    //
//                                                                                //
//  RETURNS:     None.                                                            //
//                                                                                //
//  NOTES:       none.                                                            //
//                                                                                //
////////////////////////////////////////////////////////////////////////////////////

u8 sippMemRegionAllocLineBuffer (pSippCmxBufferMap        pCmxMap,
                                 u32                      cmxSlice,
                                 SippFilter *             ptrFilt,
                                 u32                      oBufIdx,
                                 pSippMemRegionListNode * ppStartOfChainNode)
{
    pSippMemRegionListNode pMemNode     = pCmxMap->pCmxSliceRegionList[cmxSlice];
    u8                     done         = false;
    u32                    totalBits    = (ptrFilt->lineStride[oBufIdx] * ptrFilt->nLines[oBufIdx] * ptrFilt->nPlanes[oBufIdx]) * ptrFilt->bpp[oBufIdx];
                           totalBits   += 0x7;
                           totalBits   &= 0xFFFFFFF8;
    u32                    requiredSize = totalBits >> 3;

    while (done == false)
    {
        if (pMemNode)
        {
            if (pMemNode->chainStart)
            {
                u32 padBytes = (pMemNode->slice0Addr + pMemNode->regionUsed) & 0x7;

                padBytes = (padBytes) ? 0x8 - padBytes : padBytes;

                if ((pMemNode->regionSize - (pMemNode->regionUsed + padBytes)) > requiredSize)
                {
                    ptrFilt->outputBuffer[oBufIdx] = (u8 *)(pMemNode->slice0Addr + pMemNode->regionUsed + padBytes);
                    ptrFilt->oBufAlloc[oBufIdx]    = 0x1;
                    pMemNode->regionUsed          += (padBytes + requiredSize);
                    done                           = true;
                    *ppStartOfChainNode            = pMemNode;
                }
                else
                {
                    pMemNode = (pSippMemRegionListNode)pMemNode->pNext;
                }
            }
            else
            {
                pMemNode = (pSippMemRegionListNode)pMemNode->pNext;
            }
        }
        else
        {
            return false;
        }
    }

    return true;
}

////////////////////////////////////////////////////////////////////////////////////
//  FUNCTION:    sippMemAllocChainChunk                                           //
//                                                                                //
//  DESCRIPTION: Once the first chunk of a line buffer has been allocated, this   //
//               function allocates the remaining chunks in the memory region     //
//               'chain' previously identified by the framework                   //
//                                                                                //
//  INPUTS:      pStartOfChainNode - memory Region in which first chunk was       //
//                                   allocated                                    //
//               numChunks - the number fo chunks in use for the pipeline         //
//                                                                                //
//  OUTPUTS:     TRUE if line buffer could be allocated in the specified chain    //
//                                                                                //
//  RETURNS:     None.                                                            //
//                                                                                //
//  NOTES:       none.                                                            //
//                                                                                //
////////////////////////////////////////////////////////////////////////////////////

u8 sippMemAllocChainChunk (pSippMemRegionListNode pStartOfChainNode,
                           u32                    numChunks,
                           SippFilter *           ptrFilt,
                           u32                    oBufIdx)
{
    u8 retVal                              = true;
    pSippMemRegionListNode pChainNode      = (pSippMemRegionListNode)pStartOfChainNode->pNextChunkReg;
    u32                    chunksRemaining = numChunks - 1;
    u32                    totalBits       = (ptrFilt->lineStride[oBufIdx] * ptrFilt->nLines[oBufIdx] * ptrFilt->nPlanes[oBufIdx]) * ptrFilt->bpp[oBufIdx];
                           totalBits      += 0x7;
                           totalBits      &= 0xFFFFFFF8;
    u32                    requiredSize    = totalBits >> 3;

    while (chunksRemaining)
    {
        u32 padBytes = (pChainNode->slice0Addr + pChainNode->regionUsed) & 0x7;

        padBytes = (padBytes) ? 0x8 - padBytes : padBytes;

        if ((pChainNode->regionSize - (pChainNode->regionUsed + padBytes)) > requiredSize)
        {
            pChainNode->regionUsed += (padBytes + requiredSize);
            chunksRemaining--;

            if (chunksRemaining == 0)
            {
                break;
            }
            else
            {
                pChainNode = (pSippMemRegionListNode)pChainNode->pNextChunkReg;
            }
        }
        else
        {
            retVal = false;
            break;
        }

    }

    return retVal;
}

////////////////////////////////////////////////////////////////////////////////////
//  FUNCTION:    sippConfirmChunkChain                                            //
//                                                                                //
//  DESCRIPTION: Called once a full chain of memory regions is established to     //
//               mark all members appropriately                                   //
//                                                                                //
//  INPUTS:      nodeChain - array of nodes correpsoniding to the chian in order  //                                      //
//               chainLength - Number of nodes in the chain                       //
//                                                                                //
//  OUTPUTS:     None.                                                            //
//                                                                                //
//  RETURNS:     None                                                             //
//                                                                                //
//  NOTES:       Called from sippMatchRegionsToChunks only                        //
//                                                                                //
////////////////////////////////////////////////////////////////////////////////////

void sippConfirmChunkChain (pSippMemRegionListNode * nodeChain,
                            u32                      chainLength)
{
    u32 chainIdx;

    if (chainLength)
    {
        nodeChain[0]->chainStart = true;
    }

    for (chainIdx = 0; chainIdx < chainLength; chainIdx++)
    {
        nodeChain[chainIdx]->chainLinked = true;
    }

}

////////////////////////////////////////////////////////////////////////////////////
//  FUNCTION:    sippMatchRegionsToChunks                                         //
//                                                                                //
//  DESCRIPTION: If chunking is used, this function decides if the memory regions //
//               provided are suitable for use (are there sufficient regions      //
//               spaced at slice stride from each other?)                         //
//                                                                                //
//  INPUTS:      pCmxMap - cmx region map                                         //
//               chunkStride - This is the slice stride for the system - it is    //
//                             the distance apart in memory that the start of one //
//                             chunk should be form teh start of the next chunk   //
//                                                                                //
//  OUTPUTS:     None.                                                            //
//                                                                                //
//  RETURNS:     true on success (regions are suitable for a chunking system)     //
//                                                                                //
//  NOTES:       Called from main API (sippAllocCmxMemRegion) only                //
//                                                                                //
////////////////////////////////////////////////////////////////////////////////////

u8 sippMatchRegionsToChunks (pSippCmxBufferMap pCmxMap,
                             u32               chunkStride,
                             u32               numChunks)
{
    u32 chainLinks = 0;
    u32 suitableChunkChains = 0;
    u32 idx;
    pSippMemRegionListNode confirmedNodes[CMX_NSLICES];

    // Look at all the regions provided and ascertain if they form part of a group
    // which may be used in a system employing chunkning

    // The rule is that the next region must start exactly one stride away from the start address
    // of the current region
    for (idx = 0; idx < CMX_NSLICES; idx++)
    {
        pSippMemRegionListNode pNode = pCmxMap->pCmxSliceRegionList[idx];

        while (pNode)
        {
            // There is at least one region in this CMX slice
            // Check if it is already part of a chain -
            if (pNode->chainLinked == false)
            {
                u8                  chainComplete = false;
                pSippMemRegionListNode pChainNode = pNode;

                confirmedNodes[chainLinks++]      = pChainNode;

                // If the chain reaches a length of numChunks, confirm it as a usable
                // chain and go no further
                if (chainLinks == numChunks)
                {
                    // Set cmxSliceUsageBitMask
                    if (!(pCmxMap->cmxSliceUsageBitMask & (0x1 << idx)))
                    {
                        pCmxMap->numCmxSlicesAvail++;
                    }

                    pCmxMap->cmxSliceUsageBitMask |= (0x1 << idx);

                    sippConfirmChunkChain (confirmedNodes,
                                           chainLinks);
                    chainLinks    = 0;
                    chainComplete = true;
                    suitableChunkChains++;
                }

                while (chainComplete == false)
                {
                    u32                    nextRegCmxLocation, nextRegAddr;
                    u8                     foundNextChunkReg = false;
                    pSippMemRegionListNode pSearchNode;

                    // Which CMX slice will the next chunk start in?
                    nextRegAddr        = pChainNode->regionAddr + chunkStride;
                    nextRegCmxLocation = (nextRegAddr - (u32)sippCmxBase) >> 0x11;

                    pSearchNode = pCmxMap->pCmxSliceRegionList[nextRegCmxLocation];

                    while (pSearchNode)
                    {
                        if (pSearchNode->regionAddr == nextRegAddr)
                        {
                            pChainNode->pNextChunkReg    = pSearchNode;
                            foundNextChunkReg            = true;
                            pChainNode                   = pSearchNode;

                            confirmedNodes[chainLinks++] = pChainNode;

                            // If the chain reaches a length of numChunks, confirm it as a usable
                            // chain and go no further
                            if (chainLinks == numChunks)
                            {
                                // Set cmxSliceUsageBitMask
                                if (!(pCmxMap->cmxSliceUsageBitMask & (0x1 << idx)))
                                {
                                    pCmxMap->numCmxSlicesAvail++;
                                }

                                pCmxMap->cmxSliceUsageBitMask |= (0x1 << idx);

                                sippConfirmChunkChain (confirmedNodes,
                                                       chainLinks);
                                chainLinks    = 0;
                                chainComplete = true;
                                suitableChunkChains++;
                            }
                            break;
                        }
                        else
                        {
                            pSearchNode = (pSippMemRegionListNode)pSearchNode->pNext;
                        }
                    }

                    if (foundNextChunkReg == false)
                    {
                        if (chainComplete == false)
                        {
                            // The chain is not long enough but is now completed
                            chainLinks    = 0;
                            break;
                        }
                    }
                }
            }

            pNode = (pSippMemRegionListNode)pNode->pNext;

        }
    }

    return (suitableChunkChains) ? true : false;
}

////////////////////////////////////////////////////////////////////////////////////
//  FUNCTION:    sippAssignCmxMemRegion                                           //
//                                                                                //
//  DESCRIPTION: Run through the memory region list allocated, mapping each to    //
//               CMX slices and associate all this info with the relevant pipe    //
//                                                                                //
//  INPUTS:      pipe - pipeline to which regions are to be assigned              //
//               memRegList - NULL terminated memory region list                  //
//                                                                                //
//  OUTPUTS:     None.                                                            //
//                                                                                //
//  RETURNS:     0 on success (only a failed sippMemAlloc call can prevent this)  //
//                                                                                //
//  NOTES:       Called from main API (sippAllocCmxMemRegion) only                //
//               If chunking is employed in this system i.e. more than one shave  //
//               is in use then each allocated line buffer must be split up among //
//               a number of regions (number equal to number chunks) and so the   //
//               regions are dealt with in terms of chains of regions. In this    //
//               way a non-chunking system is simply a chain of regions having a  //
//               chain length of 1                                                //
//                                                                                //
////////////////////////////////////////////////////////////////////////////////////

s32 sippAssignCmxMemRegion (SippPipeline *  pipe,
                            SippMemRegion * memRegList)
{
    SippMemRegion * nextMemRegion;

    pipe->pCmxMap = (pSippCmxBufferMap)sippMemAlloc (&pipe->tHeapMCB,
                                                     vPoolPipeStructs,
                                                     sizeof(SippCmxBufferMap));

    if (pipe->pCmxMap)
    {
        u32 numChunks;

        sippPalMemset ((void *)pipe->pCmxMap,
                       0x0,
                       sizeof(SippCmxBufferMap));

        while (sippGetNextMemRegion (pipe,
                                     memRegList,
                                     &nextMemRegion))
        {
            sippMapRegionToCmx (pipe,
                                nextMemRegion);
        }

        // If chunking is employed - match up the regions
        numChunks = pipe->gi.sliceLast - pipe->gi.sliceFirst + 0x1;

        sippMatchRegionsToChunks (pipe->pCmxMap,
                                  pipe->gi.sliceSize,
                                  numChunks);
    }
    else
    {
        return -1;
    }

    return 0;
}


////////////////////////////////////////////////////////////////////////////////////
//  FUNCTION:    sippAllocCmxLineBuffers                                          //
//                                                                                //
//  DESCRIPTION: Allocate the output line buffers for all suitable filters in the //
//               input pipeline                                                   //
//                                                                                //
//  INPUTS:      pipe - input pipeline                                            //
//                                                                                //
//  OUTPUTS:     None.                                                            //
//                                                                                //
//  RETURNS:     0 on success, -1 on failure                                      //
//                                                                                //
//  NOTES:       Called by sippBuildLnBuffs                                       //
//                                                                                //
////////////////////////////////////////////////////////////////////////////////////

s32 sippAllocCmxLineBuffers (SippPipeline * pipe)
{
    SippFilter *      ptrFilt;
    u32               idx, oBufIdx;
    s32               retVal    = 0x0;  /* Default to success */
    pSippCmxBufferMap pCmxMap   = pipe->pCmxMap;
    u8                done      = false;
    u32               numChunks = pipe->gi.sliceLast - pipe->gi.sliceFirst + 1;
    u32               candidateFilterList[SIPP_MAX_BUFFERS_PER_PIPELINE];
    u32               candidateFilterBufIdx[SIPP_MAX_BUFFERS_PER_PIPELINE];
    cmxRegUsage       availCmxRegions[CMX_NSLICES];
    cmxRegUsage *     sortedCmxRegions[CMX_NSLICES];
    u32               cmxRegionUsedCount[CMX_NSLICES];
    u32               cmxRegionsIdx = 0, numCmxRegions;
    u32               candidateListIndex = 0, numCandidates, totalCMXBuffers;

    // Identify HW filters with only HW consumers
    //
    // Note: In future maybe SW filters with only HW consumers would also be suitable candidates??
    // It would not be too bad to go 'off-piste' for the 1 output pixel per operation would it?
    // This would mean that sippComputePaddingOffsets would have to change a little though? Or would it?

    for (idx = 0, numCandidates = 0, totalCMXBuffers = 0; idx < pipe->nFilters; idx++)
    {
        // Ref to current filter
        ptrFilt = pipe->filters[idx];

        for (oBufIdx = 0; oBufIdx < ptrFilt->numOBufs; oBufIdx++)
        {
            if (ptrFilt->nLines[oBufIdx])
            {
                totalCMXBuffers++;

                if (ptrFilt->unit <= SIPP_MAX_ID)
                {
                    if (ptrFilt->oBufs[oBufIdx]->numSWConsumers == 0)
                    {
                        candidateFilterBufIdx[numCandidates] = oBufIdx;
                        candidateFilterList[numCandidates++] = idx;
                    }
                }
            }
        }
    }

    for (idx = 0, numCmxRegions = 0; idx < CMX_NSLICES; idx++)
    {
        // This means that at least one chain of regions starts in this cmx slice
        if ((pCmxMap->cmxSliceUsageBitMask >> idx) & 0x1)
        {
            sortedCmxRegions[numCmxRegions]             = &availCmxRegions[numCmxRegions];
            availCmxRegions[numCmxRegions].cmxRegionIdx = idx;
            availCmxRegions[numCmxRegions].fullCmxSlice = false;
            availCmxRegions[numCmxRegions++].usedCount  = 0;
        }
    }

    // Now add the full CMX slices - actually i should only add the chains
    // so if there are 2 shaves but sw filters - only add the first slice
    // if for 2x5x there were 2 slices but no SW filters - assume we would not chunk and so add both
    if ((pCmxMap->cmxSliceUsageBitMask >> pipe->gi.sliceFirst) & 0x1)
    {
        u8 sortRequired = false;

        for (idx = 0; idx < numCmxRegions; idx++)
        {
            if (availCmxRegions[idx].cmxRegionIdx == pipe->gi.sliceFirst)
            {
                availCmxRegions[numCmxRegions].usedCount  = totalCMXBuffers - numCandidates;
                sortRequired                              = (totalCMXBuffers - numCandidates) ? true : false;
                break;
            }
        }

        if (sortRequired)
        {
            // The entry currently at index needs to go to the end of teh list with all other entries being moved up
            if (idx < numCmxRegions)
            {
                cmxRegUsage * temp =  sortedCmxRegions[idx];

                for (; idx < (numCmxRegions - 1); idx++)
                {
                    sortedCmxRegions[idx] = sortedCmxRegions[idx+1];
                }
                sortedCmxRegions[idx] = temp;
            }
        }
    }
    else
    {
        sortedCmxRegions[numCmxRegions]             = &availCmxRegions[numCmxRegions];
        availCmxRegions[numCmxRegions].cmxRegionIdx = pipe->gi.sliceFirst;
        availCmxRegions[numCmxRegions].fullCmxSlice = true;
        availCmxRegions[numCmxRegions++].usedCount  = totalCMXBuffers - numCandidates;
    }

    // The sorted list contains the full CMX slices at its tail as clearly they are the only
    // regions which can be effectively assigned filter output buffers by this stage - all the
    // filters which are not candidates in this process will have their output line buffers
    // assigned there

    if (numCandidates == 0)
    {
        done = true;
    }

    // Attempt to assign the candidate filter line buffers
    while ( done == false )
    {
        SippFilter *           ptrFilt         = pipe->filters[candidateFilterList[candidateListIndex]];
        u32                    candidateBufIdx = candidateFilterBufIdx[candidateListIndex];
//        psSchLineBuffer        ptrBuf          = ptrFilt->oBufs[candidateBufIdx];
        u8                     success         = false;
        pSippMemRegionListNode pStartOfChainNode;

        if (sortedCmxRegions[cmxRegionsIdx]->fullCmxSlice)
        {
            u32 planeSize = ptrFilt->lineStride[candidateBufIdx] * ptrFilt->nLines[candidateBufIdx];

            u32                         totalBits  = (ptrFilt->nPlanes[candidateBufIdx] * planeSize) * ptrFilt->bpp[candidateBufIdx];
                                        totalBits += 0x7;
                                        totalBits &= 0xFFFFFFF8;
            ptrFilt->outputBuffer[candidateBufIdx] = (u8 *)sippMemAlloc (&pipe->tHeapMCB,
                                                                         vPoolFilterLineBuf,
                                                                         totalBits >> 3);
            ptrFilt->oBufAlloc[candidateBufIdx]    = 0x1;
            success                                = true;
        }
        else
        {
            if (sippMemRegionAllocLineBuffer (pCmxMap,
                                              sortedCmxRegions[cmxRegionsIdx]->cmxRegionIdx,
                                              ptrFilt,
                                              candidateBufIdx,
                                              &pStartOfChainNode))
            {
                success = true;
            }
        }

        if (success == false)
        {
            cmxRegionsIdx++;
            if (cmxRegionsIdx == numCmxRegions)
            {
                // Unable to find a home for the current filter's output buffer
                // This means the full CMX slice failed in this case also - so we are a bit screwed!!
                retVal = -1;
                done = true;
            }
        }
        else
        {
            // Technically we should look at chunking now -
            // if it is in place for this pipeline need to find other spaces at exact multiples
            // of stride away
            // Of-course this only needs to be done for regions allocated via the sippAllocCmxMemRegion
            // API, a line buffer in a region allocated in one of teh fuill CMX slices assigned to teh
            // pipline will inherently be covered in terms of chunking needs in the other slices which
            // effectively set the parameters for chunking (number of full CMX slices == number of chunks)
            if (sortedCmxRegions[cmxRegionsIdx]->fullCmxSlice == false)
            {
                if (numChunks > 0x1)
                {
                    // Need to ensure the remianing links in the chain have sufficient
                    sippMemAllocChainChunk (pStartOfChainNode,
                                            numChunks,
                                            ptrFilt,
                                            candidateBufIdx);

                    // TODO - if this fails need to undo the initial call to sippMemRegionAllocLineBuffer
                }
            }


            candidateListIndex++;

            sortedCmxRegions[cmxRegionsIdx]->usedCount++;

            if (candidateListIndex == numCandidates)
            {
                done = true;
            }
            else
            {
                for (idx = 0; idx < numCmxRegions; idx++)
                {
                    sortedCmxRegions[idx]   = &availCmxRegions[idx];
                    cmxRegionUsedCount[idx] = availCmxRegions[idx].usedCount;
                }

                // Sort the list based on the number of times they have been used
                sippListSort ((s32 *)sortedCmxRegions,
                              (s32 *)cmxRegionUsedCount,
                              numCmxRegions,
                              false);

                cmxRegionsIdx = 0;
            }
        }
    }

    // Update all the used pointers in the original SippMemRegion list (for analysis by the client)
    for (idx = 0; idx < CMX_NSLICES; idx++)
    {
        if ((pCmxMap->cmxSliceUsageBitMask >> idx) & 0x1)
        {
            pSippMemRegionListNode pMemNode = pCmxMap->pCmxSliceRegionList[idx];

            while (pMemNode)
            {
                *pMemNode->regionUsedPtr = pMemNode->regionUsed;
                pMemNode                 = (pSippMemRegionListNode)pMemNode->pNext;
            }
        }
    }

    return retVal;
}

////////////////////////////////////////////////////////////////////////////////////
//  FUNCTION:    sippAllocCmxMemRegion                                            //
//                                                                                //
//  DESCRIPTION: Run through the memory region list allocated, mapping each to    //
//               CMX slices and associate all this info with the relevant pipe    //
//                                                                                //
//  INPUTS:      pipe - pipeline to which regions are to be assigned              //
//               memRegList - NULL terminated memory region list                  //
//                                                                                //
//  OUTPUTS:                                                                      //
//                                                                                //
//  RETURNS:                                                                      //
//                                                                                //
//  NOTES:       This function is a main SIPP API                                 //
//                                                                                //
////////////////////////////////////////////////////////////////////////////////////

s32 sippAllocCmxMemRegion (SippPipeline *  pipe,
                           SippMemRegion * memRegList)
{
    s32 retVal;

    if (0 == sippAssignCmxMemRegion (pipe,
                                     memRegList))
    {
        pipe->useCmxRegMap = 1;

        // Call to finalise the pipe
        sippFinalizePipeline (pipe);

        // Then a check on the pipeline code?
        retVal = pipe->cmxMapResult;
    }
    else
    {
        retVal = -3;
    }

    return retVal;
}
