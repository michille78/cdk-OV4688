///
/// @file
/// @copyright All code copyright Movidius Ltd 2014, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     SIPP engine
///

#include <sipp.h>

#ifdef SIPP_TEST_APP

#if defined(__sparc) && defined(MYRIAD2)

#include <registersMyriad.h>
#include <UnitTestApi.h>
#include <VcsHooksApi.h>
#include <DrvTimer.h>
#include <DrvDdr.h>
#include <DrvLeonL2C.h>
#include <swcLeonUtils.h>

#ifdef SIPP_SCRT_ENABLE_OPIPE
#include <DrvCmxDma.h>
#endif

#elif defined(SIPP_PC)
#include <stdint.h>
u8 * mbinImgSipp = 0; // dummy (unused on PC)
#endif

#ifndef SIPP_PC

void sippCmxDmaDoneIrqHandler ( );
extern void sippPalTrace ( u32 uFlags, const char *psz_format, ...);

//#############################################################################
// This stays in app space, as there's a single DMA interrupt
void dummyCmxDmaIrqHandler (u32 irqSource)
{
    u32 intState;

    #ifdef MYRIAD2
    DrvIcbIrqClear (irqSource);
    #endif

    // Read interrupt status bits
    intState = GET_REG_WORD_VAL (CDMA_INT_STATUS_ADR);

    if (intState & (0x1 << SIPP_CDMA_INT_NO))
    {
        // Checks the INT-bit associated to SIPP, and only treats that one !
        sippCmxDmaDoneIrqHandler ();
    }
}

#endif
#endif // SIPP_TEST_APP

//##########################################################################
// Myriad initialization function
// Undocumented - for internal test cases only!
void sippPlatformInit ()
{

    #ifdef SIPP_TEST_APP
    #if defined(MA2150) || defined(MA2155) || defined(MA2450) || defined(MA2455)
    DrvCprStartAllClocks ();
    DrvCprInit ();
    DrvTimerInit ();
    unitTestInit ();
    DrvLL2CInitWriteThrough ();
    // Need this with current heap init code which will attempt to init the DDR heap on setup
    DrvDdrInitialise(NULL);

    #endif
    #endif  // SIPP_TEST_APP

    sippInitialize ();

}


//##########################################################################
// Myriad initialization function
// Undocumented - for internal test cases only!
void sippPlatformInitAsync ( )
{
    #ifndef SIPP_PC

    #ifndef SIPP_USE_CMXDMA_DRIVER
    // Configure CMXDMA interrupt
    DrvIcbSetupIrq (IRQ_CMXDMA, 5, POS_EDGE_INT,  dummyCmxDmaIrqHandler);
    #endif

    // Enable interrupts
    swcLeonSetPIL (0);
    #endif
}
