///
/// @file
/// @copyright All code copyright Movidius Ltd 2013, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     minMax kernel function call for unitary test
///
#include "localMaxMin3x3_s16.h"
#include <stdio.h>
#include "mv_types.h"
#include "svuCommonShave.h"

#define PADDING 16
#define TEST_FRAME_WIDTH 640
#define MAX_WIDTH (TEST_FRAME_WIDTH + PADDING)

#define MAX_NUMBER_OF_EXTREMAS 322
short input[MAX_WIDTH * 3];
u32 candidateLocationIn[TEST_FRAME_WIDTH];
u32 minLocationList[MAX_NUMBER_OF_EXTREMAS];
u32 maxLocationList[MAX_NUMBER_OF_EXTREMAS];
u32 countIn;
u32 minCount;
u32 maxCount;
short* line[3];
short** inLines;

int main( void )
{
    line[0] = input + PADDING/2;
    line[1] = input + MAX_WIDTH + PADDING/2;
    line[2] = input + MAX_WIDTH * 2 + PADDING/2;
    inLines = (short**)line;
	#ifdef UNIT_TEST_USE_C_VERSION
    mvcvLocalMaxMin3x3_s16(inLines, candidateLocationIn, countIn, minLocationList, maxLocationList, &minCount, &maxCount);
    #else
    mvcvLocalMaxMin3x3_s16_asm(inLines, candidateLocationIn, countIn, minLocationList, maxLocationList, &minCount, &maxCount);
    #endif
    SHAVE_HALT;
    return 0;
}
