#include "cvtColorRGBfp16ToLumaU8.h"

void mvcvCvtColorRGBfp16ToLumaU8(half* inRGB[3], u8* yOut, u32 width)
{
	u32 i;
	
	half *r   = inRGB[0];
	half *g   = inRGB[1];
	half *b   = inRGB[2];
		
	half y;
	
	for (i = 0; i < width; i++)
    {
		y = 0.299f * r[i] + 0.587f * g[i] + 0.114f * b[i];
       
		if (y >= (half)1.0f)
			y = (half)1.0f;
		if (y <= (half)0.0f)
			y = (half)0.0f;
	yOut[i] = (u8)((float)y * 255.0f); 
	}
}
