#include "gtest/gtest.h"
#include "localMaxMin3x3_fp16_asm_test.h"
#include "mv_types.h"

#include "RandomGenerator.h"
#include "InputGenerator.h"
#include "UniformGenerator.h"
#include "FunctionInfo.h"
#include "ArrayChecker.h"


#include <ctime>
#include <memory>
#define CAND_X    9
#define PADDING 16

class localMaxMin3x3Test : public ::testing::Test {
protected:

    virtual void SetUp()
    {
        int i;
        minCount = 0;
        maxCount = 0;
        for (i = 0; i<320; i++)
        {
            maxLocationList[i] = 0;
            minLocationList[i] = 0;
        }
        randGen.reset(new RandomGenerator);
        uniGen.reset(new UniformGenerator);
        inputGen.AddGenerator(std::string("random"), randGen.get());
        inputGen.AddGenerator(std::string("uniform"), uniGen.get());
    }



    InputGenerator inputGen;
    ArrayChecker arrCheck;
    std::auto_ptr<RandomGenerator>  randGen;
    std::auto_ptr<UniformGenerator>  uniGen;
	half* inLines[3];
	


    u32 width;
    unsigned int maxLocationList[320];
    unsigned int minLocationList[320];
    unsigned int minCount;
    unsigned int maxCount;
    //virtual void TearDown() {}
protected:
    unsigned int checkIfExists(unsigned int* values, unsigned int x, unsigned int len)
    {
        unsigned int i;
        for (i = 0; i < len ; i++)
            if (values[i] == x)
                return 1;
        return 0;
    }
};

TEST_F(localMaxMin3x3Test, TestUniformInputLine)
{
	width = 320;
    inputGen.SelectGenerator("uniform");
	
	inLines[0] = inputGen.GetLineFloat16(width + PADDING, 0.5);
    inLines[1] = inputGen.GetLineFloat16(width + PADDING, 0.5);
    inLines[2] = inputGen.GetLineFloat16(width + PADDING, 0.5);
	
    localMaxMin3x3_fp16_asm_test(inLines, width/2, maxLocationList, minLocationList, &minCount, &maxCount);
    RecordProperty("CycleCount", cycleCount);

    EXPECT_EQ((unsigned int)0, minCount);
    EXPECT_EQ((unsigned int)0, maxCount);
    EXPECT_EQ((unsigned int)0,minLocationList[0]);
    EXPECT_EQ((unsigned int)0,maxLocationList[0]);
}

TEST_F(localMaxMin3x3Test, TestOneMinimum)
{
	width = 320;
    inputGen.SelectGenerator("uniform");
	
	inLines[0] = inputGen.GetLineFloat16(width + PADDING, 0.5);
    inLines[1] = inputGen.GetLineFloat16(width + PADDING, 0.5);
    inLines[2] = inputGen.GetLineFloat16(width + PADDING, 0.5);
	
	inLines[1] = inLines[1] + PADDING / 2;
    inLines[1][1] = -0.3;
	inLines[1] = inLines[1] - PADDING / 2;

    localMaxMin3x3_fp16_asm_test(inLines, width, maxLocationList, minLocationList, &minCount, &maxCount);
    RecordProperty("CycleCount", cycleCount);

    EXPECT_EQ((unsigned int)1, minCount);
    EXPECT_EQ((unsigned int)0, maxCount);
    EXPECT_EQ((unsigned int)1,minLocationList[0]);
    EXPECT_EQ((unsigned int)0,minLocationList[1]);
    EXPECT_EQ((unsigned int)0,maxLocationList[0]);
}

TEST_F(localMaxMin3x3Test, TestOneMinimumMiddle)
{
	width = 320;	
    inputGen.SelectGenerator("uniform");
	inLines[0] = inputGen.GetLineFloat16(width + PADDING, 0.5);
    inLines[1] = inputGen.GetLineFloat16(width + PADDING, 0.5);
    inLines[2] = inputGen.GetLineFloat16(width + PADDING, 0.5);

	inLines[1] = inLines[1] + PADDING / 2;
	
    for (int i = CAND_X; i < CAND_X + 9; i++)
    {
        inLines[1][i] = -0.3;
		inLines[1] = inLines[1] - PADDING / 2;
        localMaxMin3x3_fp16_asm_test(inLines, width, maxLocationList, minLocationList, &minCount, &maxCount);
        RecordProperty("CycleCount", cycleCount);
		inLines[1] = inLines[1] + PADDING / 2;

        EXPECT_EQ((unsigned int)1, minCount);
        EXPECT_EQ((unsigned int)0, maxCount);
        EXPECT_EQ((unsigned int)i,minLocationList[0]);
        EXPECT_EQ((unsigned int)0,minLocationList[1]);
        EXPECT_EQ((unsigned int)0,maxLocationList[0]);
        inLines[1][i] = 0.5;
    }
}

TEST_F(localMaxMin3x3Test, TestOneMinimumMiddle1)
{
	width = 320;
    inputGen.SelectGenerator("uniform");
	inLines[0] = inputGen.GetLineFloat16(width + PADDING, 0.5);
    inLines[1] = inputGen.GetLineFloat16(width + PADDING, 0.5);
    inLines[2] = inputGen.GetLineFloat16(width + PADDING, 0.5);
	
	inLines[0] = inLines[0] + PADDING / 2;
	inLines[1] = inLines[1] + PADDING / 2;
	
    for (int i = CAND_X; i < CAND_X + 9; i++)
    {
        inLines[0][i - 1] = -1;
        inLines[1][i] = -0.3;

		inLines[0] = inLines[0] - PADDING / 2;
		inLines[1] = inLines[1] - PADDING / 2;	
		
        localMaxMin3x3_fp16_asm_test(inLines, width, maxLocationList, minLocationList, &minCount, &maxCount);
        RecordProperty("CycleCount", cycleCount);

		inLines[0] = inLines[0] + PADDING / 2;
		inLines[1] = inLines[1] + PADDING / 2;	
		
		
        EXPECT_EQ((unsigned int)0, minCount);
        EXPECT_EQ((unsigned int)0, maxCount);
        EXPECT_EQ((unsigned int)0,minLocationList[0]);
        EXPECT_EQ((unsigned int)0,maxLocationList[0]);
        inLines[0][i - 1] = 0.5;
        inLines[1][i] = 0.5;
    }
}


TEST_F(localMaxMin3x3Test, TestOneMinimumMiddle2)
{
	width = 320;
    inputGen.SelectGenerator("uniform");
	inLines[0] = inputGen.GetLineFloat16(width + PADDING, 0.5);
    inLines[1] = inputGen.GetLineFloat16(width + PADDING, 0.5);
    inLines[2] = inputGen.GetLineFloat16(width + PADDING, 0.5);
	
	inLines[0] = inLines[0] + PADDING / 2;
	inLines[1] = inLines[1] + PADDING / 2;
	
    for (int i = CAND_X; i < CAND_X + 9; i++)
    {
        inLines[0][i] = -1;
        inLines[1][i] = -0.3;

		inLines[0] = inLines[0] - PADDING / 2;
		inLines[1] = inLines[1] - PADDING / 2;	
		
        localMaxMin3x3_fp16_asm_test(inLines, width, maxLocationList, minLocationList, &minCount, &maxCount);
        RecordProperty("CycleCount", cycleCount);

		inLines[0] = inLines[0] + PADDING / 2;
		inLines[1] = inLines[1] + PADDING / 2;	
		
        EXPECT_EQ((unsigned int)0, minCount);
        EXPECT_EQ((unsigned int)0, maxCount);
        EXPECT_EQ((unsigned int)0,minLocationList[0]);
        EXPECT_EQ((unsigned int)0,maxLocationList[0]);
        inLines[0][i] = 0.5;
        inLines[1][i] = 0.5;
    }
}


TEST_F(localMaxMin3x3Test, TestOneMinimumMiddle3)
{
	width = 320;
    inputGen.SelectGenerator("uniform");
	inLines[0] = inputGen.GetLineFloat16(width + PADDING, 0.5);
    inLines[1] = inputGen.GetLineFloat16(width + PADDING, 0.5);
    inLines[2] = inputGen.GetLineFloat16(width + PADDING, 0.5);
	
	inLines[0] = inLines[0] + PADDING / 2;
	inLines[1] = inLines[1] + PADDING / 2;

    for (int i = CAND_X; i < CAND_X + 9; i++)
    {
        inLines[0][i + 1] = -1;
        inLines[1][i] = -0.3;

		inLines[0] = inLines[0] - PADDING / 2;
		inLines[1] = inLines[1] - PADDING / 2;
		
        localMaxMin3x3_fp16_asm_test(inLines, width, maxLocationList, minLocationList, &minCount, &maxCount);
        RecordProperty("CycleCount", cycleCount);

		inLines[0] = inLines[0] + PADDING / 2;
		inLines[1] = inLines[1] + PADDING / 2;
		
		
        EXPECT_EQ((unsigned int)0, minCount);
        EXPECT_EQ((unsigned int)0, maxCount);
        EXPECT_EQ((unsigned int)0,minLocationList[0]);
        EXPECT_EQ((unsigned int)0,maxLocationList[0]);
        inLines[0][i + 1] = 0.5;
        inLines[1][i] = 0.5;
    }
}

TEST_F(localMaxMin3x3Test, TestOneMinimumMiddle4)
{
	width = 320;
    inputGen.SelectGenerator("uniform");
	inLines[0] = inputGen.GetLineFloat16(width + PADDING, 0.5);
    inLines[1] = inputGen.GetLineFloat16(width + PADDING, 0.5);
    inLines[2] = inputGen.GetLineFloat16(width + PADDING, 0.5);

	inLines[1] = inLines[1] + PADDING / 2;
	
    for (int i = CAND_X; i < CAND_X + 9; i++)
    {
        inLines[1][i - 1] = -0.3;
        inLines[1][i] = -0.3;

		inLines[1] = inLines[1] - PADDING / 2;
        localMaxMin3x3_fp16_asm_test(inLines, width, maxLocationList, minLocationList, &minCount, &maxCount);
        RecordProperty("CycleCount", cycleCount);

		inLines[1] = inLines[1] + PADDING / 2;
		
        EXPECT_EQ((unsigned int)0, minCount);
        EXPECT_EQ((unsigned int)0, maxCount);
        EXPECT_EQ((unsigned int)0,minLocationList[0]);
        EXPECT_EQ((unsigned int)0,maxLocationList[0]);
        inLines[1][i - 1] = 0.5;
        inLines[1][i] = 0.5;
    }
}



TEST_F(localMaxMin3x3Test, TestOneMinimumMiddle5)
{
	width = 320;
    inputGen.SelectGenerator("uniform");
	inLines[0] = inputGen.GetLineFloat16(width + PADDING, 0.5);
    inLines[1] = inputGen.GetLineFloat16(width + PADDING, 0.5);
    inLines[2] = inputGen.GetLineFloat16(width + PADDING, 0.5);

	inLines[1] = inLines[1] + PADDING / 2;
	
    for (int i = CAND_X; i < CAND_X + 9; i++)
    {
        inLines[1][i + 1] = -0.3;
        inLines[1][i] = -0.3;

		inLines[1] = inLines[1] - PADDING / 2;	
		
        localMaxMin3x3_fp16_asm_test(inLines, width, maxLocationList, minLocationList, &minCount, &maxCount);
        RecordProperty("CycleCount", cycleCount);

		inLines[1] = inLines[1] + PADDING / 2;	
		
        EXPECT_EQ((unsigned int)0, minCount);
        EXPECT_EQ((unsigned int)0, maxCount);
        EXPECT_EQ((unsigned int)0,minLocationList[0]);
        EXPECT_EQ((unsigned int)0,maxLocationList[0]);
        inLines[1][i + 1] = 0.5;
        inLines[1][i] = 0.5;
    }
}


TEST_F(localMaxMin3x3Test, TestOneMinimumMiddle6)
{
	width = 320;
    inputGen.SelectGenerator("uniform");
	inLines[0] = inputGen.GetLineFloat16(width + PADDING, 0.5);
    inLines[1] = inputGen.GetLineFloat16(width + PADDING, 0.5);
    inLines[2] = inputGen.GetLineFloat16(width + PADDING, 0.5);

	inLines[1] = inLines[1] + PADDING / 2;
	inLines[2] = inLines[2] + PADDING / 2;
	
	for (int i = CAND_X; i < CAND_X + 9; i++)
    {
        inLines[2][i - 1] = -1;
        inLines[1][i] = -0.3;

		inLines[2] = inLines[2] - PADDING / 2;
		inLines[1] = inLines[1] - PADDING / 2;
		
        localMaxMin3x3_fp16_asm_test(inLines, width, maxLocationList, minLocationList, &minCount, &maxCount);
        RecordProperty("CycleCount", cycleCount);

		inLines[2] = inLines[2] + PADDING / 2;
		inLines[1] = inLines[1] + PADDING / 2;
		
		
        EXPECT_EQ((unsigned int)0, minCount);
        EXPECT_EQ((unsigned int)0, maxCount);
        EXPECT_EQ((unsigned int)0,minLocationList[0]);
        EXPECT_EQ((unsigned int)0,maxLocationList[0]);
        inLines[2][i - 1] = 0.5;
        inLines[1][i] = 0.5;
    }
}




TEST_F(localMaxMin3x3Test, TestOneMinimumMiddle7)
{
	width = 320;
    inputGen.SelectGenerator("uniform");
	inLines[0] = inputGen.GetLineFloat16(width + PADDING, 0.5);
    inLines[1] = inputGen.GetLineFloat16(width + PADDING, 0.5);
    inLines[2] = inputGen.GetLineFloat16(width + PADDING, 0.5);

	inLines[1] = inLines[1] + PADDING / 2;
	inLines[2] = inLines[2] + PADDING / 2;
	
	
    for (int i = CAND_X; i < CAND_X + 9; i++)
    {
        inLines[2][i] = -1;
        inLines[1][i] = -0.3;

		inLines[2] = inLines[2] - PADDING / 2;		
		inLines[1] = inLines[1] - PADDING / 2;		
		
		
        localMaxMin3x3_fp16_asm_test(inLines, width, maxLocationList, minLocationList, &minCount, &maxCount);
        RecordProperty("CycleCount", cycleCount);
		
		inLines[2] = inLines[2] + PADDING / 2;		
		inLines[1] = inLines[1] + PADDING / 2;	

        EXPECT_EQ((unsigned int)0, minCount);
        EXPECT_EQ((unsigned int)0, maxCount);
        EXPECT_EQ((unsigned int)0,minLocationList[0]);
        EXPECT_EQ((unsigned int)0,maxLocationList[0]);
        inLines[2][i] = 0.5;
        inLines[1][i] = 0.5;
    }
}


TEST_F(localMaxMin3x3Test, TestOneMinimumMiddle8)
{
	width = 320;
    inputGen.SelectGenerator("uniform");
	inLines[0] = inputGen.GetLineFloat16(width + PADDING, 0.5);
    inLines[1] = inputGen.GetLineFloat16(width + PADDING, 0.5);
    inLines[2] = inputGen.GetLineFloat16(width + PADDING, 0.5);

	inLines[1] = inLines[1] + PADDING / 2;
	inLines[2] = inLines[2] + PADDING / 2;
		
    for (int i = CAND_X; i < CAND_X + 9; i++)
    {
        inLines[2][i + 1] = -1;
        inLines[1][i] = -0.3;

		inLines[2] = inLines[2] - PADDING / 2;	
		inLines[1] = inLines[1] - PADDING / 2;	
		
        localMaxMin3x3_fp16_asm_test(inLines, width, maxLocationList, minLocationList, &minCount, &maxCount);
        RecordProperty("CycleCount", cycleCount);

		inLines[2] = inLines[2] + PADDING / 2;	
		inLines[1] = inLines[1] + PADDING / 2;	
		
		
        EXPECT_EQ((unsigned int)0, minCount);
        EXPECT_EQ((unsigned int)0, maxCount);
        EXPECT_EQ((unsigned int)0,minLocationList[0]);
        EXPECT_EQ((unsigned int)0,maxLocationList[0]);
        inLines[2][i + 1] = 0.5;
        inLines[1][i] = 0.5;
    }
}


TEST_F(localMaxMin3x3Test, TestTwoMaximums)
{
	width = 320;
    inputGen.SelectGenerator("uniform");
	
	
	inLines[0] = inputGen.GetLineFloat16(width + PADDING, -0.5);
    inLines[1] = inputGen.GetLineFloat16(width + PADDING, -0.5);
    inLines[2] = inputGen.GetLineFloat16(width + PADDING, -0.5);

	inLines[1] = inLines[1] + PADDING / 2;
	
    inLines[1][40] = 0.3;
    inLines[1][1] = 1;

	inLines[1] = inLines[1] - PADDING / 2;	

    localMaxMin3x3_fp16_asm_test(inLines, width, maxLocationList, minLocationList, &minCount, &maxCount);
    RecordProperty("CycleCount", cycleCount);

    EXPECT_EQ((unsigned int)0, minCount);
    EXPECT_EQ((unsigned int)2, maxCount);
    EXPECT_EQ((unsigned int)1,maxLocationList[0]);
    EXPECT_EQ((unsigned int)40,maxLocationList[1]);
    EXPECT_EQ((unsigned int)0,maxLocationList[2]);
    EXPECT_EQ((unsigned int)0,minLocationList[0]);
}




TEST_F(localMaxMin3x3Test, TestOneMaximum)
{
	width = 320;
    inputGen.SelectGenerator("uniform");
	inLines[0] = inputGen.GetLineFloat16(width + PADDING, 0);
    inLines[1] = inputGen.GetLineFloat16(width + PADDING, 0);
    inLines[2] = inputGen.GetLineFloat16(width + PADDING, 0);

	inLines[1] = inLines[1] + PADDING / 2;
    inLines[1][CAND_X] = 1;
	inLines[1] = inLines[1] - PADDING / 2;	

    localMaxMin3x3_fp16_asm_test(inLines, width, maxLocationList, minLocationList, &minCount, &maxCount);
    RecordProperty("CycleCount", cycleCount);

    EXPECT_EQ((unsigned int)0, minCount);
    EXPECT_EQ((unsigned int)1, maxCount);
    EXPECT_EQ((unsigned int)CAND_X,maxLocationList[0]);
    EXPECT_EQ((unsigned int)0,maxLocationList[1]);
    EXPECT_EQ((unsigned int)0,minLocationList[0]);
}


TEST_F(localMaxMin3x3Test, TestOneMaxMiddle1)
{
	width = 320;
    inputGen.SelectGenerator("uniform");
	inLines[0] = inputGen.GetLineFloat16(width + PADDING, 0);
    inLines[1] = inputGen.GetLineFloat16(width + PADDING, 0);
    inLines[2] = inputGen.GetLineFloat16(width + PADDING, 0);

	inLines[0] = inLines[0] + PADDING / 2;
	inLines[1] = inLines[1] + PADDING / 2;

    for (int i = CAND_X; i < CAND_X + 9; i++)
    {
        inLines[1][i] = 1;
        inLines[0][i - 1] = 1;
		
		inLines[0] = inLines[0] - PADDING / 2;	
		inLines[1] = inLines[1] - PADDING / 2;
		
        localMaxMin3x3_fp16_asm_test(inLines, width, maxLocationList, minLocationList, &minCount, &maxCount);
        RecordProperty("CycleCount", cycleCount);

		inLines[0] = inLines[0] + PADDING / 2;	
		inLines[1] = inLines[1] + PADDING / 2;
		
		
        EXPECT_EQ((unsigned int)0, minCount);
        EXPECT_EQ((unsigned int)0, maxCount);
        EXPECT_EQ((unsigned int)0,maxLocationList[0]);
        EXPECT_EQ((unsigned int)0,minLocationList[0]);
        inLines[1][i] = 0;
        inLines[0][i - 1] = 0;
    }
}


TEST_F(localMaxMin3x3Test, TestOneMaxMiddle2)
{
	width = 320;
    inputGen.SelectGenerator("uniform");
	inLines[0] = inputGen.GetLineFloat16(width + PADDING, 0);
    inLines[1] = inputGen.GetLineFloat16(width + PADDING, 0);
    inLines[2] = inputGen.GetLineFloat16(width + PADDING, 0);

	inLines[0] = inLines[0] + PADDING / 2;
	inLines[1] = inLines[1] + PADDING / 2;

    for (int i = CAND_X; i < CAND_X + 9; i++)
    {
        inLines[1][i] = 1;
        inLines[0][i] = 1;
		
		inLines[0] = inLines[0] - PADDING / 2;	
		inLines[1] = inLines[1] - PADDING / 2;
		
        localMaxMin3x3_fp16_asm_test(inLines, width, maxLocationList, minLocationList, &minCount, &maxCount);
        RecordProperty("CycleCount", cycleCount);

		inLines[0] = inLines[0] + PADDING / 2;	
		inLines[1] = inLines[1] + PADDING / 2;
		
        EXPECT_EQ((unsigned int)0, minCount);
        EXPECT_EQ((unsigned int)0, maxCount);
        EXPECT_EQ((unsigned int)0,maxLocationList[0]);
        EXPECT_EQ((unsigned int)0,minLocationList[0]);
        inLines[1][i] = 0;
        inLines[0][i] = 0;
    }
}


TEST_F(localMaxMin3x3Test, TestOneMaxMiddle3)
{
	width = 320;
    inputGen.SelectGenerator("uniform");
	inLines[0] = inputGen.GetLineFloat16(width + PADDING, 0);
    inLines[1] = inputGen.GetLineFloat16(width + PADDING, 0);
    inLines[2] = inputGen.GetLineFloat16(width + PADDING, 0);

	inLines[0] = inLines[0] + PADDING / 2;
	inLines[1] = inLines[1] + PADDING / 2;
	
    for (int i = CAND_X; i < CAND_X + 9; i++)
    {
        inLines[1][i] = 1;
        inLines[0][i + 1] = 1;
		
		inLines[0] = inLines[0] - PADDING / 2;	
		inLines[1] = inLines[1] - PADDING / 2;
		
        localMaxMin3x3_fp16_asm_test(inLines, width, maxLocationList, minLocationList, &minCount, &maxCount);
        RecordProperty("CycleCount", cycleCount);

		inLines[0] = inLines[0] + PADDING / 2;	
		inLines[1] = inLines[1] + PADDING / 2;
		
        EXPECT_EQ((unsigned int)0, minCount);
        EXPECT_EQ((unsigned int)0, maxCount);
        EXPECT_EQ((unsigned int)0,maxLocationList[0]);
        EXPECT_EQ((unsigned int)0,minLocationList[0]);
        inLines[1][i] = 0;
        inLines[0][i + 1] = 0;
    }
}



TEST_F(localMaxMin3x3Test, TestOneMaxMiddle4)
{
	width = 320;
    inputGen.SelectGenerator("uniform");
	inLines[0] = inputGen.GetLineFloat16(width + PADDING, 0);
    inLines[1] = inputGen.GetLineFloat16(width + PADDING, 0);
    inLines[2] = inputGen.GetLineFloat16(width + PADDING, 0);

	inLines[1] = inLines[1] + PADDING / 2;

	
    for (int i = CAND_X; i < CAND_X + 9; i++)
    {
        inLines[1][i] = 1;
        inLines[1][i - 1] = 1;
		inLines[1] = inLines[1] - PADDING / 2;
        localMaxMin3x3_fp16_asm_test(inLines, width, maxLocationList, minLocationList, &minCount, &maxCount);
        RecordProperty("CycleCount", cycleCount);

		inLines[1] = inLines[1] + PADDING / 2;
		
        EXPECT_EQ((unsigned int)0, minCount);
        EXPECT_EQ((unsigned int)0, maxCount);
        EXPECT_EQ((unsigned int)0,maxLocationList[0]);
        EXPECT_EQ((unsigned int)0,minLocationList[0]);
        inLines[1][i] = 0;
        inLines[1][i - 1] = 0;
    }
}



TEST_F(localMaxMin3x3Test, TestOneMaxMiddle5)
{
	width = 320;
    inputGen.SelectGenerator("uniform");
	inLines[0] = inputGen.GetLineFloat16(width + PADDING, 0);
    inLines[1] = inputGen.GetLineFloat16(width + PADDING, 0);
    inLines[2] = inputGen.GetLineFloat16(width + PADDING, 0);

	inLines[1] = inLines[1] + PADDING / 2;

    for (int i = CAND_X; i < CAND_X + 9; i++)
    {
        inLines[1][i] = 1;
        inLines[1][i + 1] = 1;
		inLines[1] = inLines[1] - PADDING / 2;
        localMaxMin3x3_fp16_asm_test(inLines, width, maxLocationList, minLocationList, &minCount, &maxCount);
        RecordProperty("CycleCount", cycleCount);
		inLines[1] = inLines[1] + PADDING / 2;
        EXPECT_EQ((unsigned int)0, minCount);
        EXPECT_EQ((unsigned int)0, maxCount);
        EXPECT_EQ((unsigned int)0,maxLocationList[0]);
        EXPECT_EQ((unsigned int)0,minLocationList[0]);
        inLines[1][i] = 0;
        inLines[1][i + 1] = 0;
    }
}


TEST_F(localMaxMin3x3Test, TestOneMaxMiddle6)
{
	width = 320;
    inputGen.SelectGenerator("uniform");
	inLines[0] = inputGen.GetLineFloat16(width + PADDING, 0);
    inLines[1] = inputGen.GetLineFloat16(width + PADDING, 0);
    inLines[2] = inputGen.GetLineFloat16(width + PADDING, 0);

	inLines[1] = inLines[1] + PADDING / 2;
	inLines[2] = inLines[2] + PADDING / 2;

    for (int i = CAND_X; i < CAND_X + 9; i++)
    {
        inLines[1][i] = 1;
        inLines[2][i - 1] = 1;
		inLines[1] = inLines[1] - PADDING / 2;
		inLines[2] = inLines[2] - PADDING / 2;
        localMaxMin3x3_fp16_asm_test(inLines, width, maxLocationList, minLocationList, &minCount, &maxCount);
        RecordProperty("CycleCount", cycleCount);
		inLines[1] = inLines[1] + PADDING / 2;
		inLines[2] = inLines[2] + PADDING / 2;
        EXPECT_EQ((unsigned int)0, minCount);
        EXPECT_EQ((unsigned int)0, maxCount);
        EXPECT_EQ((unsigned int)0,maxLocationList[0]);
        EXPECT_EQ((unsigned int)0,minLocationList[0]);
        inLines[1][i] = 0;
        inLines[2][i - 1] = 0;
    }
}


TEST_F(localMaxMin3x3Test, TestOneMaxMiddle7)
{
	width = 320;
    inputGen.SelectGenerator("uniform");
	inLines[0] = inputGen.GetLineFloat16(width + PADDING, 0);
    inLines[1] = inputGen.GetLineFloat16(width + PADDING, 0);
    inLines[2] = inputGen.GetLineFloat16(width + PADDING, 0);
		
	inLines[1] = inLines[1] + PADDING / 2;
	inLines[2] = inLines[2] + PADDING / 2;

    for (int i = CAND_X; i < CAND_X + 9; i++)
    {
        inLines[1][i] = 1;
        inLines[2][i] = 1;
		inLines[1] = inLines[1] - PADDING / 2;
		inLines[2] = inLines[2] - PADDING / 2;
        localMaxMin3x3_fp16_asm_test(inLines, width, maxLocationList, minLocationList, &minCount, &maxCount);
        RecordProperty("CycleCount", cycleCount);
		inLines[1] = inLines[1] + PADDING / 2;
		inLines[2] = inLines[2] + PADDING / 2;
        EXPECT_EQ((unsigned int)0, minCount);
        EXPECT_EQ((unsigned int)0, maxCount);
        EXPECT_EQ((unsigned int)0,maxLocationList[0]);
        EXPECT_EQ((unsigned int)0,minLocationList[0]);
        inLines[1][i] = 0;
        inLines[2][i] = 0;
    }
}

TEST_F(localMaxMin3x3Test, TestOneMaxMiddle8)
{
	width = 320;
    inputGen.SelectGenerator("uniform");
	inLines[0] = inputGen.GetLineFloat16(width + PADDING, 0);
    inLines[1] = inputGen.GetLineFloat16(width + PADDING, 0);
    inLines[2] = inputGen.GetLineFloat16(width + PADDING, 0);
	
	inLines[1] = inLines[1] + PADDING / 2;
	inLines[2] = inLines[2] + PADDING / 2;

    for (int i = CAND_X; i < CAND_X + 9; i++)
    {
        inLines[1][i] = 1;
        inLines[2][i + 1] = 1;
		inLines[1] = inLines[1] - PADDING / 2;
		inLines[2] = inLines[2] - PADDING / 2;
        localMaxMin3x3_fp16_asm_test(inLines, width, maxLocationList, minLocationList, &minCount, &maxCount);
        RecordProperty("CycleCount", cycleCount);
		inLines[1] = inLines[1] + PADDING / 2;
		inLines[2] = inLines[2] + PADDING / 2;
        EXPECT_EQ((unsigned int)0, minCount);
        EXPECT_EQ((unsigned int)0, maxCount);
        EXPECT_EQ((unsigned int)0,maxLocationList[0]);
        EXPECT_EQ((unsigned int)0,minLocationList[0]);
        inLines[1][i] = 0;
        inLines[2][i + 1] = 0;
    }
}

TEST_F(localMaxMin3x3Test, TestNoMaxNoMin)
{
	width = 320;
    inputGen.SelectGenerator("uniform");
    inLines[0] = inputGen.GetLineFloat16(width + PADDING, -0.3);
    inLines[1] = inputGen.GetLineFloat16(width + PADDING, -0.3);
    inLines[2] = inputGen.GetLineFloat16(width + PADDING, -0.3);

	inLines[0] = inLines[0] + PADDING / 2;
	inLines[2] = inLines[2] + PADDING / 2;
	
    inLines[2][1] = 0.7;
    inLines[0][1] = -0.2;
    inLines[0][310] = 0;
	
	inLines[0] = inLines[0] - PADDING / 2;
	inLines[2] = inLines[2] - PADDING / 2;
	
    localMaxMin3x3_fp16_asm_test(inLines, width, maxLocationList, minLocationList, &minCount, &maxCount);
    RecordProperty("CycleCount", cycleCount);

    EXPECT_EQ((unsigned int)0, minCount);
    EXPECT_EQ((unsigned int)0, maxCount);
    EXPECT_EQ((unsigned int)0,maxLocationList[0]);
    EXPECT_EQ((unsigned int)0,minLocationList[0]);
}

TEST_F(localMaxMin3x3Test, TestOneMinOneMax)
{
	width = 320;
    inputGen.SelectGenerator("uniform");
    inLines[0] = inputGen.GetLineFloat16(width + PADDING, 0);
    inLines[1] = inputGen.GetLineFloat16(width + PADDING, 0);
    inLines[2] = inputGen.GetLineFloat16(width + PADDING, 0);

	inLines[1] = inLines[1] + PADDING / 2;

    inLines[1][315] = 1;
    inLines[1][316] = -1;
	
	inLines[1] = inLines[1] - PADDING / 2;

    localMaxMin3x3_fp16_asm_test(inLines, width, maxLocationList, minLocationList, &minCount, &maxCount);
    RecordProperty("CycleCount", cycleCount);

    EXPECT_EQ((unsigned int)1, minCount);
    EXPECT_EQ((unsigned int)1, maxCount);
    EXPECT_EQ((unsigned int)315,maxLocationList[0]);
    EXPECT_EQ((unsigned int)316,minLocationList[0]);
    EXPECT_EQ((unsigned int)0,maxLocationList[1]);
    EXPECT_EQ((unsigned int)0,minLocationList[1]);
}


TEST_F(localMaxMin3x3Test, TestMaxMin)
{
	width = 320;
    inputGen.SelectGenerator("uniform");
    inLines[0] = inputGen.GetLineFloat16(width + PADDING, 0);
    inLines[1] = inputGen.GetLineFloat16(width + PADDING, 0);
    inLines[2] = inputGen.GetLineFloat16(width + PADDING, 0);

	inLines[1] = inLines[1] + PADDING / 2;
	
    unsigned int i;
    for (i = 0; i<320 ;i += 2)
    {
        inLines[1][i] = 1;
        inLines[1][i+1] = -1;
    }

	inLines[1] = inLines[1] - PADDING / 2;
	
    localMaxMin3x3_fp16_asm_test(inLines, width, maxLocationList, minLocationList, &minCount, &maxCount);
    RecordProperty("CycleCount", cycleCount);

    EXPECT_EQ((unsigned int)160, minCount);
    EXPECT_EQ((unsigned int)159, maxCount);


    for (i = 0; i < minCount;i++)
    {
        EXPECT_EQ((unsigned int)1,checkIfExists(minLocationList, 2 * i + 1, minCount));
    }
    for (i = 0; i < maxCount;i ++)
    {
        EXPECT_EQ((unsigned int)1,checkIfExists(maxLocationList, 2 * i + 2, maxCount));
    }

}


TEST_F(localMaxMin3x3Test, TestMaxMin2)
{
	
	int width = 320;
    inputGen.SelectGenerator("uniform");
    inLines[0] = inputGen.GetLineFloat16(width + PADDING, 0);
    inLines[1] = inputGen.GetLineFloat16(width + PADDING, 0);
    inLines[2] = inputGen.GetLineFloat16(width + PADDING, 0);
	
	inLines[0] = inLines[0] + PADDING / 2;
	inLines[1] = inLines[1] + PADDING / 2;
	
    int i;
    
    for (i = 0; i<width ;i += 2)
    {
      inLines[1][i] = 1;
      inLines[1][i+1] = -1;
    }
    
    inLines[0][100]= -1;
	
	inLines[0] = inLines[0] - PADDING / 2;
	inLines[1] = inLines[1] - PADDING / 2;
	
	localMaxMin3x3_fp16_asm_test(inLines, width, maxLocationList, minLocationList, &minCount, &maxCount);
    RecordProperty("CycleCount", cycleCount);
    EXPECT_EQ((unsigned int)158, minCount);
    EXPECT_EQ((unsigned int)159, maxCount);
    // The value 99 and 101 should not be in the minimum list because they are not minimums
    EXPECT_EQ((unsigned int)0,checkIfExists(minLocationList, 99, minCount));
    EXPECT_EQ((unsigned int)0,checkIfExists(minLocationList, 101, minCount));
    
    EXPECT_EQ((unsigned int)1,checkIfExists(minLocationList, 109, minCount));
    EXPECT_EQ((unsigned int)1,checkIfExists(minLocationList, 107, minCount));
    EXPECT_EQ((unsigned int)1,checkIfExists(minLocationList, 105, minCount));
    EXPECT_EQ((unsigned int)1,checkIfExists(minLocationList, 103, minCount));
    EXPECT_EQ((unsigned int)1,checkIfExists(minLocationList, 97, minCount));
    EXPECT_EQ((unsigned int)1,checkIfExists(minLocationList, 95, minCount));
    EXPECT_EQ((unsigned int)1,checkIfExists(minLocationList, 93, minCount));
    EXPECT_EQ((unsigned int)1,checkIfExists(minLocationList, 91, minCount));
    EXPECT_EQ((unsigned int)1,checkIfExists(minLocationList, 89, minCount));
}
