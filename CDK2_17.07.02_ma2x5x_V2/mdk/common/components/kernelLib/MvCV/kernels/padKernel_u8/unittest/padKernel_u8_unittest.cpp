//padKernel_u8 kernel test

//Asm function prototype:
//	void mvcvPadKernel_u8_asm(u8* iBuf, u32 iBufLen, u8* oBuf, 
//                        u32 padSz, u32 padMode, u32 padType)

//Asm test function prototype:
//	void mvcvPadKernel_u8_asm_test(unsigned char* iBuf, unsigned int iBufLen,
//                        unsigned char* oBuf, unsigned int padSz, unsigned int padMode, unsigned int padType);

//C function prototype:
//	void mvcvpadKernel_u8(u8* iBuf, u32 iBufLen, u8* oBuf, 
//                         u32 padSz, u32 padMode, u32 padType)

#include "gtest/gtest.h"
#include "padKernel_u8_asm_test.h"
#include "padKernel_u8.h"
#include "RandomGenerator.h"
#include "InputGenerator.h"
#include "UniformGenerator.h"
#include "TestEventListener.h"
#include "ArrayChecker.h"
#include <ctime>
#include <float.h>

#define MAX_PADDING 32

class padKernel_u8Test : public ::testing::Test {
protected:

	virtual void SetUp()
	{
		randGen.reset(new RandomGenerator);
		uniGen.reset(new UniformGenerator);
		inputGen.AddGenerator(std::string("random"), randGen.get());
		inputGen.AddGenerator(std::string("uniform"), uniGen.get());
	}	
	
	unsigned char* iBuf;
	unsigned char* oBufC;
	unsigned char* oBufAsm;
	unsigned int iBufLen;
	unsigned int padSz;
	unsigned int padMode;
	unsigned int padType;
	unsigned char pixValue;
	
	InputGenerator inputGen;
	RandomGenerator randomGen;
	ArrayChecker outputCheck;	
	std::auto_ptr<RandomGenerator>  randGen;
	std::auto_ptr<UniformGenerator>  uniGen;

	virtual void TearDown() {}
};


//-------------------------------------------------------------------------------------------
//						case 1 Left Padding
//-------------------------------------------------------------------------------------------

TEST_F(padKernel_u8Test, TestLeftModeAllZeroType)
{
	iBufLen = 320;	
	padSz = randomGen.GenerateUInt(1, 16, 1);	
	padMode = Left;
	padType = AllZero;
	
	inputGen.SelectGenerator("random");
	iBuf = inputGen.GetLine(iBufLen, 0, 255);
	oBufC = inputGen.GetLine(iBufLen + MAX_PADDING, 0);
	oBufAsm = inputGen.GetLine(iBufLen + MAX_PADDING, 0);
		
	padKernel_u8_asm_test(iBuf, iBufLen, oBufAsm, padSz, padMode, padType, 0);
	RecordProperty("CyclePerPixel", padKernel_u8CycleCount / (padSz+iBufLen));
	mvcvPadKernel_u8(iBuf, iBufLen, oBufC, padSz, padMode, padType, 0);
	
	outputCheck.CompareArrays(oBufC, oBufAsm, iBufLen + padSz);
}

TEST_F(padKernel_u8Test, TestLeftModeAllOneType)
{
	iBufLen = 1280;	
	padSz = randomGen.GenerateUInt(1, 16, 1);	
	padMode = Left;
	padType = AllOne;
	
	inputGen.SelectGenerator("random");
	iBuf = inputGen.GetLine(iBufLen, 0, 255);
	oBufC = inputGen.GetLine(iBufLen + MAX_PADDING, 0);
	oBufAsm = inputGen.GetLine(iBufLen + MAX_PADDING, 0);
		
	padKernel_u8_asm_test(iBuf, iBufLen, oBufAsm, padSz, padMode, padType, 0);
	RecordProperty("CyclePerPixel", padKernel_u8CycleCount / (padSz+iBufLen));
	mvcvPadKernel_u8(iBuf, iBufLen, oBufC, padSz, padMode, padType, 0);
	
	outputCheck.CompareArrays(oBufC, oBufAsm, iBufLen + padSz);
}

TEST_F(padKernel_u8Test, TestLeftModeMirrorType)
{
	iBufLen = 16;	
	padSz = randomGen.GenerateUInt(1, 16, 1);	
	padMode = Left;
	padType = Mirror;
	
	inputGen.SelectGenerator("random");
	iBuf = inputGen.GetLine(iBufLen, 0, 255);
	oBufC = inputGen.GetLine(iBufLen + MAX_PADDING, 0);
	oBufAsm = inputGen.GetLine(iBufLen + MAX_PADDING, 0);
		
	padKernel_u8_asm_test(iBuf, iBufLen, oBufAsm, padSz, padMode, padType, 0);
	RecordProperty("CyclePerPixel", padKernel_u8CycleCount / (padSz+iBufLen));
	mvcvPadKernel_u8(iBuf, iBufLen, oBufC, padSz, padMode, padType, 0);
	
	outputCheck.CompareArrays(oBufC, oBufAsm, iBufLen + padSz);
}

TEST_F(padKernel_u8Test, TestLeftModeBlackPixelType)
{
	iBufLen = 240;	
	padSz = randomGen.GenerateUInt(1, 16, 1);	
	padMode = Left;
	padType = BlackPixel;
	
	inputGen.SelectGenerator("random");
	iBuf = inputGen.GetLine(iBufLen, 0, 255);
	oBufC = inputGen.GetLine(iBufLen + MAX_PADDING, 0);
	oBufAsm = inputGen.GetLine(iBufLen + MAX_PADDING, 0);
		
	padKernel_u8_asm_test(iBuf, iBufLen, oBufAsm, padSz, padMode, padType, 0);
	RecordProperty("CyclePerPixel", padKernel_u8CycleCount / (padSz+iBufLen));
	mvcvPadKernel_u8(iBuf, iBufLen, oBufC, padSz, padMode, padType, 0);
	
	outputCheck.CompareArrays(oBufC, oBufAsm, iBufLen + padSz);
}

TEST_F(padKernel_u8Test, TestLeftModeWhitePixelType)
{
	iBufLen = randomGen.GenerateUInt(0, 1920, 16);	
	padSz = randomGen.GenerateUInt(1, 16, 1);	
	padMode = Left;
	padType = WhitePixel;
	
	inputGen.SelectGenerator("random");
	iBuf = inputGen.GetLine(iBufLen, 0, 255);
	oBufC = inputGen.GetLine(iBufLen + MAX_PADDING, 0);
	oBufAsm = inputGen.GetLine(iBufLen + MAX_PADDING, 0);
		
	padKernel_u8_asm_test(iBuf, iBufLen, oBufAsm, padSz, padMode, padType, 0);
	RecordProperty("CyclePerPixel", padKernel_u8CycleCount / (padSz+iBufLen));
	mvcvPadKernel_u8(iBuf, iBufLen, oBufC, padSz, padMode, padType, 0);
	
	outputCheck.CompareArrays(oBufC, oBufAsm, iBufLen + padSz);
}

TEST_F(padKernel_u8Test, TestLeftModePixelValueType)
{
	iBufLen = randomGen.GenerateUInt(0, 1920, 16);	
	padSz = randomGen.GenerateUInt(1, 16, 1);	
	padMode = Left;
	padType = PixelValue;
	pixValue = randomGen.GenerateUInt(0, 255, 1);
	
	inputGen.SelectGenerator("random");
	iBuf = inputGen.GetLine(iBufLen, 0, 255);
	oBufC = inputGen.GetLine(iBufLen + MAX_PADDING, 0);
	oBufAsm = inputGen.GetLine(iBufLen + MAX_PADDING, 0);
		
	padKernel_u8_asm_test(iBuf, iBufLen, oBufAsm, padSz, padMode, padType, pixValue);
	RecordProperty("CyclePerPixel", padKernel_u8CycleCount / (padSz+iBufLen));
	mvcvPadKernel_u8(iBuf, iBufLen, oBufC, padSz, padMode, padType, pixValue);
	
	outputCheck.CompareArrays(oBufC, oBufAsm, iBufLen + padSz);
}


//-------------------------------------------------------------------------------------------
//						case 2 Right Padding
//-------------------------------------------------------------------------------------------

TEST_F(padKernel_u8Test, TestRightModeAllZeroType)
{
	iBufLen = 640;	
	padSz = randomGen.GenerateUInt(1, 16, 1);	
	padMode = Right;
	padType = AllZero;
	
	inputGen.SelectGenerator("random");
	iBuf = inputGen.GetLine(iBufLen, 0, 255);
	oBufC = inputGen.GetLine(iBufLen + MAX_PADDING, 0);
	oBufAsm = inputGen.GetLine(iBufLen + MAX_PADDING, 0);
		
	padKernel_u8_asm_test(iBuf, iBufLen, oBufAsm, padSz, padMode, padType, 0);
	RecordProperty("CyclePerPixel", padKernel_u8CycleCount / (padSz+iBufLen));
	mvcvPadKernel_u8(iBuf, iBufLen, oBufC, padSz, padMode, padType, 0);
	
	outputCheck.CompareArrays(oBufC, oBufAsm, iBufLen + padSz);
}

TEST_F(padKernel_u8Test, TestRightModeAllOneType)
{
	iBufLen = 240;	
	padSz = randomGen.GenerateUInt(1, 16, 1);	
	padMode = Right;
	padType = AllOne;
	
	inputGen.SelectGenerator("random");
	iBuf = inputGen.GetLine(iBufLen, 0, 255);
	oBufC = inputGen.GetLine(iBufLen + MAX_PADDING, 0);
	oBufAsm = inputGen.GetLine(iBufLen + MAX_PADDING, 0);
		
	padKernel_u8_asm_test(iBuf, iBufLen, oBufAsm, padSz, padMode, padType, 0);
	RecordProperty("CyclePerPixel", padKernel_u8CycleCount / (padSz+iBufLen));
	mvcvPadKernel_u8(iBuf, iBufLen, oBufC, padSz, padMode, padType, 0);
	
	outputCheck.CompareArrays(oBufC, oBufAsm, iBufLen + padSz);
}

TEST_F(padKernel_u8Test, TestRightModeMirrorType)
{
	iBufLen = 1920;	
	padSz = randomGen.GenerateUInt(1, 16, 1);	
	padMode = Right;
	padType = Mirror;
	
	inputGen.SelectGenerator("random");
	iBuf = inputGen.GetLine(iBufLen, 0, 255);
	oBufC = inputGen.GetLine(iBufLen + MAX_PADDING, 0);
	oBufAsm = inputGen.GetLine(iBufLen + MAX_PADDING, 0);
		
	padKernel_u8_asm_test(iBuf, iBufLen, oBufAsm, padSz, padMode, padType, 0);
	RecordProperty("CyclePerPixel", padKernel_u8CycleCount / (padSz+iBufLen));
	mvcvPadKernel_u8(iBuf, iBufLen, oBufC, padSz, padMode, padType, 0);
	
	outputCheck.CompareArrays(oBufC, oBufAsm, iBufLen + padSz);
}

TEST_F(padKernel_u8Test, TestRightModeBlackPixelType)
{
	iBufLen = 1200;	
	padSz = randomGen.GenerateUInt(1, 16, 1);	
	padMode = Right;
	padType = BlackPixel;
	
	inputGen.SelectGenerator("random");
	iBuf = inputGen.GetLine(iBufLen, 0, 255);
	oBufC = inputGen.GetLine(iBufLen + MAX_PADDING, 0);
	oBufAsm = inputGen.GetLine(iBufLen + MAX_PADDING, 0);
		
	padKernel_u8_asm_test(iBuf, iBufLen, oBufAsm, padSz, padMode, padType, 0);
	RecordProperty("CyclePerPixel", padKernel_u8CycleCount / (padSz+iBufLen));
	mvcvPadKernel_u8(iBuf, iBufLen, oBufC, padSz, padMode, padType, 0);
	
	outputCheck.CompareArrays(oBufC, oBufAsm, iBufLen + padSz);
}

TEST_F(padKernel_u8Test, TestRightModeWhitePixelType)
{
	iBufLen = randomGen.GenerateUInt(0, 1920, 16);	
	padSz = randomGen.GenerateUInt(1, 16, 1);	
	padMode = Right;
	padType = WhitePixel;
	
	inputGen.SelectGenerator("random");
	iBuf = inputGen.GetLine(iBufLen, 0, 255);
	oBufC = inputGen.GetLine(iBufLen + MAX_PADDING, 0);
	oBufAsm = inputGen.GetLine(iBufLen + MAX_PADDING, 0);
		
	padKernel_u8_asm_test(iBuf, iBufLen, oBufAsm, padSz, padMode, padType, 0);
	RecordProperty("CyclePerPixel", padKernel_u8CycleCount / (padSz+iBufLen));
	mvcvPadKernel_u8(iBuf, iBufLen, oBufC, padSz, padMode, padType, 0);
	
	outputCheck.CompareArrays(oBufC, oBufAsm, iBufLen + padSz);
}

TEST_F(padKernel_u8Test, TestRightModePixelValueType)
{
	iBufLen = randomGen.GenerateUInt(0, 1920, 16);	
	padSz = randomGen.GenerateUInt(1, 16, 1);	
	padMode = Right;
	padType = PixelValue;
	pixValue = randomGen.GenerateUInt(0, 255, 1);
	
	inputGen.SelectGenerator("random");
	iBuf = inputGen.GetLine(iBufLen, 0, 255);
	oBufC = inputGen.GetLine(iBufLen + MAX_PADDING, 0);
	oBufAsm = inputGen.GetLine(iBufLen + MAX_PADDING, 0);
		
	padKernel_u8_asm_test(iBuf, iBufLen, oBufAsm, padSz, padMode, padType, pixValue);
	RecordProperty("CyclePerPixel", padKernel_u8CycleCount / (padSz+iBufLen));
	mvcvPadKernel_u8(iBuf, iBufLen, oBufC, padSz, padMode, padType, pixValue);
	
	outputCheck.CompareArrays(oBufC, oBufAsm, iBufLen + padSz);
}

//-------------------------------------------------------------------------------------------
//						case 3 Left and Right Padding
//-------------------------------------------------------------------------------------------


TEST_F(padKernel_u8Test, TestLeftAndRightModeAllZeroType)
{
	iBufLen = 1280;	
	padSz = randomGen.GenerateUInt(1, 16, 1);	
	padMode = LeftAndRight;
	padType = AllZero;
	
	inputGen.SelectGenerator("random");
	iBuf = inputGen.GetLine(iBufLen, 0, 255);
	oBufC = inputGen.GetLine(iBufLen + MAX_PADDING, 0);
	oBufAsm = inputGen.GetLine(iBufLen + MAX_PADDING, 0);
		
	padKernel_u8_asm_test(iBuf, iBufLen, oBufAsm, padSz, padMode, padType, 0);
	RecordProperty("CyclePerPixel", padKernel_u8CycleCount / (padSz*2+iBufLen));
	mvcvPadKernel_u8(iBuf, iBufLen, oBufC, padSz, padMode, padType, 0);
	
	outputCheck.CompareArrays(oBufC, oBufAsm, iBufLen + padSz * 2 );
}

TEST_F(padKernel_u8Test, TestLeftAndRightModeAllOneType)
{
	iBufLen = 240;	
	padSz = randomGen.GenerateUInt(1, 16, 1);	
	padMode = LeftAndRight;
	padType = AllOne;
	
	inputGen.SelectGenerator("random");
	iBuf = inputGen.GetLine(iBufLen, 0, 255);
	oBufC = inputGen.GetLine(iBufLen + MAX_PADDING, 0);
	oBufAsm = inputGen.GetLine(iBufLen + MAX_PADDING, 0);
		
	padKernel_u8_asm_test(iBuf, iBufLen, oBufAsm, padSz, padMode, padType, 0);
	RecordProperty("CyclePerPixel", padKernel_u8CycleCount / (padSz*2+iBufLen));
	mvcvPadKernel_u8(iBuf, iBufLen, oBufC, padSz, padMode, padType, 0);
	
	outputCheck.CompareArrays(oBufC, oBufAsm, iBufLen + padSz * 2);
}

TEST_F(padKernel_u8Test, TestLeftAndRightModeMirrorType)
{
	iBufLen = 1920;	
	padSz = randomGen.GenerateUInt(1, 16, 1);	
	padMode = LeftAndRight;
	padType = Mirror;
	
	inputGen.SelectGenerator("random");
	iBuf = inputGen.GetLine(iBufLen, 0, 255);
	oBufC = inputGen.GetLine(iBufLen + MAX_PADDING, 0);
	oBufAsm = inputGen.GetLine(iBufLen + MAX_PADDING, 0);
		
	padKernel_u8_asm_test(iBuf, iBufLen, oBufAsm, padSz, padMode, padType, 0);
	RecordProperty("CyclePerPixel", padKernel_u8CycleCount / (padSz*2+iBufLen));
	mvcvPadKernel_u8(iBuf, iBufLen, oBufC, padSz, padMode, padType, 0);
	
	outputCheck.CompareArrays(oBufC, oBufAsm, iBufLen + padSz * 2);
}

TEST_F(padKernel_u8Test, TestLeftAndRightModeBlackPixelType)
{
	iBufLen = 640;	
	padSz = randomGen.GenerateUInt(1, 16, 1);	
	padMode = LeftAndRight;
	padType = BlackPixel;
	
	inputGen.SelectGenerator("random");
	iBuf = inputGen.GetLine(iBufLen, 0, 255);
	oBufC = inputGen.GetLine(iBufLen + MAX_PADDING, 0);
	oBufAsm = inputGen.GetLine(iBufLen + MAX_PADDING, 0);
		
	padKernel_u8_asm_test(iBuf, iBufLen, oBufAsm, padSz, padMode, padType, 0);
	RecordProperty("CyclePerPixel", padKernel_u8CycleCount / (padSz*2+iBufLen));
	mvcvPadKernel_u8(iBuf, iBufLen, oBufC, padSz, padMode, padType, 0);
	
	outputCheck.CompareArrays(oBufC, oBufAsm, iBufLen + padSz * 2);
}

TEST_F(padKernel_u8Test, TestLeftAndRightModeWhitePixelType)
{
	iBufLen = randomGen.GenerateUInt(0, 1920, 16);	
	padSz = randomGen.GenerateUInt(1, 16, 1);	
	padMode = LeftAndRight;
	padType = WhitePixel;
	
	inputGen.SelectGenerator("random");
	iBuf = inputGen.GetLine(iBufLen, 0, 255);
	oBufC = inputGen.GetLine(iBufLen + MAX_PADDING, 0);
	oBufAsm = inputGen.GetLine(iBufLen + MAX_PADDING, 0);
		
	padKernel_u8_asm_test(iBuf, iBufLen, oBufAsm, padSz, padMode, padType, 0);
	RecordProperty("CyclePerPixel", padKernel_u8CycleCount / (padSz*2+iBufLen));
	mvcvPadKernel_u8(iBuf, iBufLen, oBufC, padSz, padMode, padType, 0);
	
	outputCheck.CompareArrays(oBufC, oBufAsm, iBufLen + padSz * 2);
}

TEST_F(padKernel_u8Test, TestLeftAndRightModePixelValueType)
{
	iBufLen = randomGen.GenerateUInt(0, 1920, 16);	
	padSz = randomGen.GenerateUInt(1, 16, 1);	
	padMode = LeftAndRight;
	padType = PixelValue;
	pixValue = randomGen.GenerateUInt(0, 255, 1);
	
	inputGen.SelectGenerator("random");
	iBuf = inputGen.GetLine(iBufLen, 0, 255);
	oBufC = inputGen.GetLine(iBufLen + MAX_PADDING, 0);
	oBufAsm = inputGen.GetLine(iBufLen + MAX_PADDING, 0);
		
	padKernel_u8_asm_test(iBuf, iBufLen, oBufAsm, padSz, padMode, padType, pixValue);
	RecordProperty("CyclePerPixel", padKernel_u8CycleCount / (padSz*2+iBufLen));
	mvcvPadKernel_u8(iBuf, iBufLen, oBufC, padSz, padMode, padType, pixValue);
	
	outputCheck.CompareArrays(oBufC, oBufAsm, iBufLen + padSz * 2);
}
