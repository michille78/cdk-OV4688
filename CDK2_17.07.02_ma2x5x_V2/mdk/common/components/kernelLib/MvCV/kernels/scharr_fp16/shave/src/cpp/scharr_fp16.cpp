#include <math.h>
#include "scharr_fp16.h"
#include <stdio.h>

void mvcvScharr_fp16(half** in, half** out, u32 width)
{
    int i;
    half* lines[3];
    fp32 sx=0, sy=0,s=0;
    half* aux;
    aux=(*out);

    //scharr matrix
    float VertScharr[3][3]={
        {-3, 0, 3},
        {-10, 0, 10},
        {-3, 0, 3}
    };

    float HorzScharr[3][3]={
        {-3, -10, -3},
        { 0,  0,  0},
        { 3,  10,  3}
    };


    //Initialize lines pointers
    lines[0]=*in;
    lines[1]=*(in+1);
    lines[2]=*(in+2);


    //Go on the whole line
    for (i=0;i<(int)width;i++){
        sx = VertScharr[0][0]*lines[0][i-1]+ VertScharr[0][1]*lines[0][i] +VertScharr[0][2]*lines[0][i+1]+
                        VertScharr[1][0]*lines[1][i-1]+ VertScharr[1][1]*lines[1][i] +VertScharr[1][2]*lines[1][i+1]+
                        VertScharr[2][0]*lines[2][i-1]+ VertScharr[2][1]*lines[2][i] +VertScharr[2][2]*lines[2][i+1];

        sy = HorzScharr[0][0]*lines[0][i-1]+ HorzScharr[0][1]*lines[0][i] +HorzScharr[0][2]*lines[0][i+1]+
                        HorzScharr[1][0]*lines[1][i-1]+ HorzScharr[1][1]*lines[1][i] +HorzScharr[1][2]*lines[1][i+1]+
                        HorzScharr[2][0]*lines[2][i-1]+ HorzScharr[2][1]*lines[2][i] +HorzScharr[2][2]*lines[2][i+1];
        
        s= sqrtf(sx*sx+ sy*sy);
        aux[i] = (half)s;
    }
    return;
}
