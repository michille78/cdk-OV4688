/// @file
/// @copyright All code copyright Movidius Ltd 2013, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief
///

.version 00.51.04

.data .data.MinKernel64
.align 4

.code .text.MinKernel64

.lalign
//void mvcvMinKernel64_asm_test(unsigned char *input1, unsigned char *input2, unsigned char *output, unsigned char min2, unsigned char penaltyP2)//
//                                                  i18                  i17                     i16                   i15                    i14
mvcvMinKernel64_asm:

lsu0.ldo.64.l v0 i18  0 || lsu1.ldo.64.h v0 i18  8 
lsu0.ldo.64.l v1 i18 16 || lsu1.ldo.64.h v1 i18 24 
lsu0.ldo.64.l v2 i18 32 || lsu1.ldo.64.h v2 i18 40 
lsu0.ldo.64.l v3 i18 48 || lsu1.ldo.64.h v3 i18 56 
nop 5

cmu.min.u8 v4 v0 v1 
cmu.min.u8 v5 v2 v3 
nop
cmu.min.u8 v6 v4 v5 
nop
cmu.alignvec v7 v6 v6 8
cmu.min.u8 v8 v6 v7 
nop
cmu.alignvec v9 v8 v8 4
cmu.min.u8 v10 v9 v8 
nop
cmu.alignvec v11 v10 v10 2
cmu.min.u8 v12 v11 v10 
BRU.jmp i30
cmu.alignvec v13 v12 v12 1
cmu.min.u8 v14 v12 v13 
nop
lsu0.cp i0 v14.0
lsu0.st.8 i0 i17
nop
.end
