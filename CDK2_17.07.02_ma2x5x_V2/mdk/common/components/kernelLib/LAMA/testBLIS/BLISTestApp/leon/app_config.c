///
/// @file
/// @copyright All code copyright Movidius Ltd 2012, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Application configuration Leon file
///


// 1: Includes
// ----------------------------------------------------------------------------
#include "app_config.h"
#include "DrvCpr.h"
#include "DrvDdr.h"
#include "assert.h"
#include "registersMyriad.h"

#if (defined MYRIAD1)
 #include <DrvL2Cache.h>
 #include "sysClkCfg/sysClk180_180.h"
#elif (defined MYRIAD2)
 #include "DrvRegUtils.h"
 #include "DrvGpioDefines.h"
 #include "DrvGpio.h"
 #include "brdGpioCfgs/brdMv0182GpioDefaults.h"
 #include "DrvShaveL2Cache.h"
 #include "DrvTimer.h"
#endif

// 2:  Source Specific #defines and types  (typedef,enum,struct)
// ----------------------------------------------------------------------------
#if (defined MYRIAD1)
 #define CMX_CONFIG                 (0x66666666)
#elif (defined MYRIAD2)
 #define SYS_CLK_KHZ 12000 // 20MHz
 #define CMX_CONFIG_SLICE_7_0       (0x11111111)
 #define CMX_CONFIG_SLICE_15_8      (0x11111111)
 #ifndef BUP_PLL0_FREQ
  // no PLL by default
  #define BUP_PLL0_FREQ 300000
 #endif
 #ifndef BUP_PLL1_FREQ
  // no PLL by default
  #define BUP_PLL1_FREQ 300000
 #endif
 #define APP_UPA_CLOCKS (DEV_UPA_SH0      | \
                         DEV_UPA_SH1      | \
                         DEV_UPA_SH2      | \
                         DEV_UPA_SH3      | \
                         DEV_UPA_SH4      | \
                         DEV_UPA_SH5      | \
                         DEV_UPA_SH6      | \
                         DEV_UPA_SH7      | \
                         DEV_UPA_SH8      | \
                         DEV_UPA_SH9      | \
                         DEV_UPA_SH10     | \
                         DEV_UPA_SH11     | \
                         DEV_UPA_SHAVE_L2 | \
                         DEV_UPA_CDMA     | \
                         DEV_UPA_CTRL         )
#endif
#define L2CACHE_CFG     (SHAVE_L2CACHE_NORMAL_MODE)
                         
// 3: Global Data (Only if absolutely necessary)
// ----------------------------------------------------------------------------
// Sections decoration is require here for downstream tools
u32 __l2_config   __attribute__((section(".l2.mode")))  = L2CACHE_CFG;
#if (defined MYRIAD1)
 u32 __cmx_config  __attribute__((section(".cmx.ctrl"))) = CMX_CONFIG;
#elif (defined MYRIAD2)
 CmxRamLayoutCfgType __attribute__((section(".cmx.ctrl"))) __cmx_config = {CMX_CONFIG_SLICE_7_0, CMX_CONFIG_SLICE_15_8};
#endif

// 4: Static Local Data 
// ----------------------------------------------------------------------------

#if (defined MYRIAD2)
static tyAuxClkDividerCfg auxClk[] =
{
        {
            .auxClockEnableMask = AUX_CLK_MASK_LCD | CSS_AUX_MEDIA,
            .auxClockSource = CLK_SRC_SYS_CLK,
            .auxClockDivNumerator = 1,
            .auxClockDivDenominator = 1
        },    // media clock and LCD
        {0, 0, 0, 0}, // Null Terminated List
    };


static tySocClockConfig clocksConfig =
{
    .refClk0InputKhz         = SYS_CLK_KHZ      /* 12 MHz */,
    .refClk1InputKhz         = 0                /* refClk1 not enabled for now */,
    .targetPll0FreqKhz       = BUP_PLL0_FREQ,   /* PLL0 target freq = 100 MHz */
    .targetPll1FreqKhz       = 0,               /* set in DDR driver */
    .clkSrcPll1              = CLK_SRC_REFCLK0, /* refClk1 is also not enabled for now */
    .masterClkDivNumerator   = 1,
    .masterClkDivDenominator = 1,
    .cssDssClockEnableMask   = DEFAULT_CORE_CSS_DSS_CLOCKS,
    .upaClockEnableMask      = DEFAULT_UPA_CLOCKS, /* no UPA clocks enabled */
    .mssClockEnableMask      = 0xFFFFFFFF, /* MSS clocks enabled */
    .sippClockEnableMask     = 0,
    .pAuxClkCfg              = auxClk,
};
#endif

// 5: Static Function Prototypes
// ----------------------------------------------------------------------------

// 6: Functions Implementation
// ----------------------------------------------------------------------------

#if (defined MYRIAD2)
void configureMss(void)
{
    SET_REG_WORD(MSS_MIPI_CFG_ADR,     0X00000000);    //DISABLE MIPI CHANNELS
    SET_REG_WORD(MSS_MIPI_CIF_CFG_ADR, 0X00000000);    //DISABLE MIPI->CIF CONNECTION
    SET_REG_WORD(MSS_LCD_MIPI_CFG_ADR, 0X00000000);    //DISABLE LCD->MIPI CONNECTION
    SET_REG_WORD(MSS_LOOPBACK_CFG_ADR, 0X00000000);    //LCD-CIF internal loopback not needed in this test-case.

    /* By default, the LCD and CIF modules are connected to MIPI.
     * So, in order to connect them to GPIOs, the MSSGPIOCfg register has to be set:
     * bit 0 = 1 => CIF connected to GPIO
     * bit 1 = 1 => LCD connected to GPIO */
    SET_REG_WORD(MSS_GPIO_CFG_ADR, 0x03);
    return;
}
#endif

int initClocksAndMemory(void)
{
#if (defined MYRIAD1)

    // Initialise the Clock Power reset module
    retVal = DrvCprInit(NULL,NULL);
    if (retVal)
        return retVal;

    retVal = DrvCprSetupClocks(&appClockConfig180_180);
    if (retVal)
        return retVal;

    // Configure all CMX RAM layouts
    SET_REG_WORD(LHB_CMX_RAMLAYOUT_CFG, CMX_CONFIG);

    // Initialise DDR
    DrvDdrInitialise(DrvCprGetClockFreqKhz(AUX_CLK_DDR,NULL));

    // Invalidate L2 Cache
    SET_REG_WORD( L2C_MODE_ADR, __l2_config );
    DrvL2CachePartitionInvalidate(0);

#elif (defined MYRIAD2)

    int i;

    // Configure the system
    DrvCprInit();
    DrvCprSetupClocks(&clocksConfig);
    DrvCprSysDeviceAction(MSS_DOMAIN, DEASSERT_RESET, 0xFFFFFFFF);
    DrvCprSysDeviceAction(UPA_DOMAIN, DEASSERT_RESET, APP_UPA_CLOCKS);

    configureMss();

    DrvTimerInit(); //needed to initialize LeonOS timer
    DrvDdrInitialise(NULL);

    //Set Shave L2 cache partitions
    DrvShaveL2CacheSetupPartition(SHAVEPART256KB);

    //Allocate Shave L2 cache set partitions
    DrvShaveL2CacheAllocateSetPartitions();

    //Assign the one partition defined to all shaves
    for (i = 0; i < 12; i++)
    {
        DrvShaveL2CacheSetLSUPartId(i, 0);
    }

    // Configure GPIOs
    DrvGpioInitialiseRange(brdMV0182GpioCfgDefault);
#endif

    return 0;
}

