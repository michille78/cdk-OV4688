#!/bin/bash

# this script is intended to be run in the directory containing the mdk
# components. That's $(MDK_INSTALL_DIR_ABS)/common/components

SHAVE_DIR=( "shave" "detector/shave_code" )
SHAVE_ARCH_DIR=( "arch/ma2x8x/src/shave" )
LOS_DIR=( "leon" "LOS" "leon_code" "leon/rtems" "dogPipeApi/leon" )
LEON_ARCH_DIR=( "arch/ma2x8x/PAL/leon" "arch/ma2x8x/src/leon" "arch/ma2x5x/leon" )
LRT_DIR=( "leon_rt" "LRT" )
SHARED_DIR=( "shared" "detector/shared" )
INCLUDE_DIR=( "include" )
INCLUDE_ARCH_DIR=( "arch/ma2x8x/include" )
SOC_LIST=( "ma2x5x" "ma2x8x" )
TARGET_ma2x5x_SOC_LIST=( "MA2150" "MA2450" )
TARGET_ma2x8x_SOC_LIST=( "MA2480" )
COMPONENT_KCNF=kcnf
COMPONENT_MAKEFILE=subdir.mk
LOCAL_SHAVE_MAKEFILE=subdir.mk
LOCAL_LOS_MAKEFILE=subdir.mk
LOCAL_LRT_MAKEFILE=subdir.mk

INCLUDE_COMPONENTS=( "libGlossTests" "mipiCam" )

REMAINING=("vTrack/ vTrack.kconfig/ mipiCam/ libGlossTests/ imageWarpDyn/ JpegM2EncoderDyn/ Debug/ DumpPins/ /CameraModules ")

SIMPLE_COMPONENTS=( "bicubicWarp" "BLIS" "CamGeneric" "CifGeneric" "DataCollector" "eventLoop" "Fp16Convert" \
	"functionProfiler" "H264Encoder" "HdmiTxIte" "HdmiTxIteFull" "imageWarp" "ipcSMM" "I2CSlave" "JpegM2EncoderParallel" \
	"LcdGeneric" "LeonIPC" "LeonL1Cache" "MemLogger" "MessageProtocol" "MV0212" "PatternGenerator" "PipePrint" "PMDSensor" \
	"PwrManager" "sampleProfiler" "shaveFifoComm" "StlportTest" "TestUtils" "UnitTest" "UnitTestVcs" "USBLink" \
	"USB_VSC" "VcsHooks" "sippDD" "sippNoWrap" "MV0212" "Opipe" "FramePump" "DoGDetector" )

for c in "${SIMPLE_COMPONENTS[@]}" ; do
	pushd "$c" > /dev/null
	c_UPPER=${c^^}
	KCNF_OPTION=USE_COMPONENT_$c_UPPER

	truncate -s 0 $COMPONENT_MAKEFILE
	truncate -s 0 $COMPONENT_KCNF

	cat >>$COMPONENT_KCNF <<-KCNF_ENTRY
	config $KCNF_OPTION
	 bool "Use mdk component $c"
	 default n
	 help
	  Check this if you want to compile the $c mdk component into your application

	KCNF_ENTRY

	for i in "${INCLUDE_DIR[@]}" ; do
		[ -d "$i" ] && echo "include-dirs-los-y+=\$(MDK_INSTALL_DIR_ABS)/components/$c/$i" >> $COMPONENT_MAKEFILE
	done
	for shrd in "${SHARED_DIR[@]}" ; do
		[ -d "$shrd" ] && echo "include-dirs-los-y+=\$(MDK_INSTALL_DIR_ABS)/components/$c/$shrd" >> $COMPONENT_MAKEFILE
	done

	echo "include-dirs-shave-y+=\$(include-dirs-los-y)" >> $COMPONENT_MAKEFILE
	echo "include-dirs-lrt-y+=\$(include-dirs-los-y)" >> $COMPONENT_MAKEFILE
	echo "" >> $COMPONENT_MAKEFILE

	for los in "${LOS_DIR[@]}" ; do
		if [ -d "$los" ]; then
			echo "subdirs-los-y+=$los" >> $COMPONENT_MAKEFILE
			pushd "$los" > /dev/null

			truncate -s 0 $LOCAL_LOS_MAKEFILE
			HEADER_FILES=($(find * -type f -name "*.h" -o -name "*.hpp"))
			SOURCE_FILES=($(find * -type f -name "*.c" -o -name "*.cpp" -o -name "*.asm"))

			if [ ! "${#HEADER_FILES[@]}" -eq 0 ]; then
				echo -n "include-dirs-los-y+=" >> $LOCAL_LOS_MAKEFILE
				FOLDERS=()
				HEADER_FILES=($(find * -type f -name "*.h" -o -name "*.hpp"))
				for files in "${HEADER_FILES[@]}"; do FOLDERS+=("$(dirname "$files")") ; done
				UNIQ_FOLDERS=($(echo "${FOLDERS[@]}" | tr ' ' '\n' | sort -u | tr '\n' ' '))
				for folders in "${UNIQ_FOLDERS[@]}" ; do
					#use readlink for the case path/to/headers/. to normalize the path
					LINK=$(readlink -f -m /components/$c/$los/$folders)
					echo -en "\\\\\n  \$(MDK_INSTALL_DIR_ABS)$LINK " >> $LOCAL_LOS_MAKEFILE
				done
			fi
			echo "" >> $LOCAL_LOS_MAKEFILE

			if [ ! "${#SOURCE_FILES[@]}" -eq 0 ]; then echo -n "srcs-los-y+=" >> $LOCAL_LOS_MAKEFILE ; fi
			for f in "${SOURCE_FILES[@]}" ; do
				echo -en "\\\\\n $f " >> $LOCAL_LOS_MAKEFILE
			done
			echo "" >> $LOCAL_LOS_MAKEFILE

			popd > /dev/null
		fi
	done

	for shv in "${SHAVE_DIR[@]}" ; do
		if [ -d "$shv" ]; then
			echo "subdirs-shave-y+=$shv" >> $COMPONENT_MAKEFILE
			pushd "$shv" > /dev/null

			truncate -s 0 $LOCAL_SHAVE_MAKEFILE
			HEADER_FILES=($(find * -type f -name "*.h" -o -name "*.hpp"))
			SOURCE_FILES=($(find * -type f -name "*.c" -o -name "*.cpp" -o -name "*.asm"))

			if [ ! "${#HEADER_FILES[@]}" -eq 0 ]; then
				echo -n "include-dirs-shave-y+=" >> $LOCAL_SHAVE_MAKEFILE
				FOLDERS=()
				HEADER_FILES=($(find * -type f -name "*.h" -o -name "*.hpp"))
				for files in "${HEADER_FILES[@]}"; do FOLDERS+=("$(dirname "$files")") ; done
				UNIQ_FOLDERS=($(echo "${FOLDERS[@]}" | tr ' ' '\n' | sort -u | tr '\n' ' '))
				for folders in "${UNIQ_FOLDERS[@]}" ; do
					LINK=$(readlink -f -m /components/$c/$shv/$folders)
					echo -en "\\\\\n  \$(MDK_INSTALL_DIR_ABS)$LINK " >> $LOCAL_SHAVE_MAKEFILE
				done
			fi
			echo "" >> $LOCAL_SHAVE_MAKEFILE

			if [ ! "${#SOURCE_FILES[@]}" -eq 0 ]; then echo -n "srcs-shave-y+=" >> $LOCAL_SHAVE_MAKEFILE ; fi
			for f in "${SOURCE_FILES[@]}" ; do
				echo -en "\\\\\n $f " >> $LOCAL_SHAVE_MAKEFILE
			done
			echo "" >> $LOCAL_SHAVE_MAKEFILE

			popd > /dev/null
		fi
	done

	for lrt in "${LRT_DIR[@]}" ; do
		if [ -d "$lrt" ]; then
			echo "subdirs-lrt-y+=$lrt" >> $COMPONENT_MAKEFILE
			pushd "$lrt" > /dev/null

			truncate -s 0 $LOCAL_LRT_MAKEFILE
			HEADER_FILES=($(find * -type f -name "*.h" -o -name "*.hpp"))
			SOURCE_FILES=($(find * -type f -name "*.c" -o -name "*.cpp" -o -name "*.asm"))

			if [ ! "${#HEADER_FILES[@]}" -eq 0 ]; then
				echo -n "include-dirs-lrt-y+=" >> $LOCAL_LRT_MAKEFILE
				FOLDERS=()
				HEADER_FILES=($(find * -type f -name "*.h" -o -name "*.hpp"))
				for files in "${HEADER_FILES[@]}"; do FOLDERS+=("$(dirname "$files")") ; done
				UNIQ_FOLDERS=($(echo "${FOLDERS[@]}" | tr ' ' '\n' | sort -u | tr '\n' ' '))
				for folders in "${UNIQ_FOLDERS[@]}" ; do
					LINK=$(readlink -f -m /components/$c/$lrt/$folders)
					echo -en "\\\\\n  \$(MDK_INSTALL_DIR_ABS)$LINK " >> $LOCAL_LRT_MAKEFILE
				done
			fi
			echo "" >> $LOCAL_LRT_MAKEFILE

			if [ ! "${#SOURCE_FILES[@]}" -eq 0 ]; then echo -n "srcs-lrt-y+=" >> $LOCAL_LRT_MAKEFILE ; fi
			for f in "${SOURCE_FILES[@]}" ; do
				echo -en "\\\\\n $f " >> $LOCAL_LRT_MAKEFILE
			done
			echo "" >> $LOCAL_LRT_MAKEFILE

			popd > /dev/null
		fi
	done

	for soc in "${SOC_LIST[@]}" ; do
		LOS_SOC_DIR=($(printf -- '%s\n' "${LEON_ARCH_DIR[@]}" | grep $soc))
		SHAVE_SOC_DIR=($(printf -- '%s\n' "${SHAVE_ARCH_DIR[@]}" | grep $soc))
		INCLUDE_SOC_DIR=($(printf -- '%s\n' "${INCLUDE_ARCH_DIR[@]}" | grep $soc))
		losflag=0
		shaveflag=0
		includeflag=0

		for i in "${INCLUDE_SOC_DIR[@]}" ; do
			if [ -d "$i" ]; then
				echo "include-dirs-los-$soc-y+=\$(MDK_INSTALL_DIR_ABS)/components/$c/$i" >> $COMPONENT_MAKEFILE
				includeflag=1
			fi
		done

		if [ $includeflag -ne 0 ] ; then
			arrAlias=TARGET_${soc}_SOC_LIST[@]
			for t in ${!arrAlias} ; do
				echo "include-dirs-los-\$(CONFIG_TARGET_SOC_$t) += include-dirs-los-$soc-y" >> $COMPONENT_MAKEFILE
				echo "include-dirs-shave-\$(CONFIG_TARGET_SOC_$t) += include-dirs-los-$soc-y" >> $COMPONENT_MAKEFILE
				echo "include-dirs-lrt-\$(CONFIG_TARGET_SOC_$t) += include-dirs-los-$soc-y" >> $COMPONENT_MAKEFILE
				echo "" >> $COMPONENT_MAKEFILE
			done
		fi


		for los in "${LOS_SOC_DIR[@]}" ; do
		if [ -d "$los" ]; then
			losflag=1
			echo "subdirs-los-$soc-y+=$los" >> $COMPONENT_MAKEFILE

			pushd "$los" > /dev/null

			truncate -s 0 $LOCAL_LOS_MAKEFILE
			HEADER_FILES=($(find * -type f -name "*.h" -o -name "*.hpp"))
			SOURCE_FILES=($(find * -type f -name "*.c" -o -name "*.cpp" -o -name "*.asm"))

			if [ ! "${#HEADER_FILES[@]}" -eq 0 ]; then
				echo -n "include-dirs-los-$soc-y+=" >> $LOCAL_LOS_MAKEFILE
				FOLDERS=()
				HEADER_FILES=($(find * -type f -name "*.h" -o -name "*.hpp"))
				for files in "${HEADER_FILES[@]}"; do FOLDERS+=("$(dirname "$files")") ; done
				UNIQ_FOLDERS=($(echo "${FOLDERS[@]}" | tr ' ' '\n' | sort -u | tr '\n' ' '))
				for folders in "${UNIQ_FOLDERS[@]}" ; do
					#use readlink for the case path/to/headers/. to normalize the path
					LINK=$(readlink -f -m /components/$c/$los/$folders)
					echo -en "\\\\\n  \$(MDK_INSTALL_DIR_ABS)$LINK " >> $LOCAL_LOS_MAKEFILE
				done
			fi
			echo "" >> $LOCAL_LOS_MAKEFILE

			arrAlias=TARGET_${soc}_SOC_LIST[@]
			for t in ${!arrAlias} ; do
				echo "include-dirs-los-\$(CONFIG_TARGET_SOC_$t) += include-dirs-los-$soc-y" >> $COMPONENT_MAKEFILE
			done
			echo "" >> $LOCAL_LOS_MAKEFILE

			if [ ! "${#SOURCE_FILES[@]}" -eq 0 ]; then echo -n "srcs-los-$soc-y+=" >> $LOCAL_LOS_MAKEFILE ; fi
			for f in "${SOURCE_FILES[@]}" ; do
				echo -en "\\\\\n $f " >> $LOCAL_LOS_MAKEFILE
			done
			echo "" >> $LOCAL_LOS_MAKEFILE

			arrAlias=TARGET_${soc}_SOC_LIST[@]
			for t in ${!arrAlias} ; do
				echo "srcs-los-\$(CONFIG_TARGET_SOC_$t) += srcs-los-$soc-y" >> $COMPONENT_MAKEFILE
			done

			popd > /dev/null
		fi
		done

		if [ $losflag -ne 0 ] ; then
			arrAlias=TARGET_${soc}_SOC_LIST[@]
			for t in ${!arrAlias} ; do
				echo "subdirs-los-\$(CONFIG_TARGET_SOC_$t) += subdirs-los-$soc-y" >> $COMPONENT_MAKEFILE
			done
		fi

		for shv in "${SHAVE_SOC_DIR[@]}" ; do
		if [ -d "$shv" ]; then
			shaveflag=1
			echo "subdirs-shave-$soc-y+=$shv" >> $COMPONENT_MAKEFILE

			pushd "$shv" > /dev/null

			truncate -s 0 $LOCAL_SHAVE_MAKEFILE
			HEADER_FILES=($(find * -type f -name "*.h" -o -name "*.hpp"))
			SOURCE_FILES=($(find * -type f -name "*.c" -o -name "*.cpp" -o -name "*.asm"))

			if [ ! "${#HEADER_FILES[@]}" -eq 0 ]; then
				echo -n "include-dirs-shave-$soc-y+=" >> $LOCAL_SHAVE_MAKEFILE
				FOLDERS=()
				HEADER_FILES=($(find * -type f -name "*.h" -o -name "*.hpp"))
				for files in "${HEADER_FILES[@]}"; do FOLDERS+=("$(dirname "$files")") ; done
				UNIQ_FOLDERS=($(echo "${FOLDERS[@]}" | tr ' ' '\n' | sort -u | tr '\n' ' '))
				for folders in "${UNIQ_FOLDERS[@]}" ; do
					#use readlink for the case path/to/headers/. to normalize the path
					LINK=$(readlink -f -m /components/$c/$shv/$folders)
					echo -en "\\\\\n  \$(MDK_INSTALL_DIR_ABS)$LINK " >> $LOCAL_SHAVE_MAKEFILE
				done
			fi
			echo "" >> $LOCAL_SHAVE_MAKEFILE

			arrAlias=TARGET_${soc}_SOC_LIST[@]
			for t in ${!arrAlias} ; do
				echo "include-dirs-shave-\$(CONFIG_TARGET_SOC_$t) += include-dirs-shave-$soc-y" >> $COMPONENT_MAKEFILE
			done
			echo "" >> $LOCAL_SHAVE_MAKEFILE

			if [ ! "${#SOURCE_FILES[@]}" -eq 0 ]; then echo -n "srcs-shave-$soc-y+=" >> $LOCAL_SHAVE_MAKEFILE ; fi
			for f in "${SOURCE_FILES[@]}" ; do
				echo -en "\\\\\n $f " >> $LOCAL_SHAVE_MAKEFILE
			done
			echo "" >> $LOCAL_SHAVE_MAKEFILE

			arrAlias=TARGET_${soc}_SOC_LIST[@]
			for t in ${!arrAlias} ; do
				echo "srcs-shave-\$(CONFIG_TARGET_SOC_$t) += srcs-shave-$soc-y" >> $COMPONENT_MAKEFILE
			done

			popd > /dev/null
		fi
		done

		if [ $shaveflag -ne 0 ] ; then
			arrAlias=TARGET_${soc}_SOC_LIST[@]
			for t in ${!arrAlias} ; do
				echo "subdirs-shave-\$(CONFIG_TARGET_SOC_$t) += subdirs-shave-$soc-y" >> $COMPONENT_MAKEFILE
			done
		fi

	done

	echo "" >> $COMPONENT_MAKEFILE

	popd > /dev/null
done

# vim: set tw=0: set wrapmargin=0 :

