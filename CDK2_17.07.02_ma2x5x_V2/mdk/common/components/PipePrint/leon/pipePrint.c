/// =====================================================================================
///
///        @file:      pipePrint.c
///        @brief:     IO Console over moviDebug pipe (see ""help pipe")
///        @author:    alseh alexandru.horin@movidius.com
///        @copyright: All code copyright Movidius Ltd 2014, all rights reserved.
///                  For License Warranty see: common/license.txt
/// =====================================================================================
///
#ifdef __RTEMS__
#include <rtems.h>
#include <rtems/bspIo.h>
#else
#include <sys/types.h>
#include <sys/reent.h>
#endif
#include <mv_types.h>

#ifndef PIPEPRINT_SECTION
#define PIPEPRINT_SECTION ".ddr_direct.data"
#endif

#ifndef PIPEPRINT_SIZE
#define PIPEPRINT_SIZE (50*1024)
#endif

typedef struct {
    volatile u32  canaryStart;   // Used to detect corruption of queue
    volatile int  in;
    volatile int  out;
    volatile int  queueSize;
    volatile u32  canaryEnd;     // Used to detect corruption of queue
    volatile u8   buffer[PIPEPRINT_SIZE];
} tyMvConsoleQueue;

tyMvConsoleQueue mvConsoleTxQueue __attribute__((section(PIPEPRINT_SECTION)))=
{
    .canaryStart = 0x11223344,
    .in          = 0,
    .out         = 0,
    .queueSize   = PIPEPRINT_SIZE,
    .canaryEnd   = 0xAABBCCDD,
    .buffer      = {0},
};

tyMvConsoleQueue mvConsoleRxQueue __attribute__((section(PIPEPRINT_SECTION)))=
{
    .canaryStart = 0x11223344,
    .in          = 0,
    .out         = 0,
    .queueSize   = PIPEPRINT_SIZE,
    .canaryEnd   = 0xAABBCCDD,
    .buffer      = {0},
};

static inline void * convertToUncachedAddr(void * addr)
{
    if ((u32)addr & 0x80000000)
        addr = (void*)((u32)addr | 0x40000000);
    else // Assume CMX
        addr = (void*)((u32)addr | 0x08000000);
    return addr;
}

// Blocking Queue Add
void mvQueueAdd  (tyMvConsoleQueue * qPtr, u8 val)
{
    tyMvConsoleQueue * q = convertToUncachedAddr(qPtr);

#ifdef PIPEPRINT_WAIT_DEBUG
    do // Wait for space in the Q
    {
    } while (q->in == (( q->out - 1 + q->queueSize) % q->queueSize)  );
#endif

    q->buffer[q->in] = val;
    q->in = (q->in + 1) % q->queueSize;
    return;
}

// Blocking Queue Get
int mvQueueGet  (tyMvConsoleQueue *qPtr)
{
    u8 val;
    tyMvConsoleQueue * q = convertToUncachedAddr(qPtr);

    if(q->in == q->out)
    {
        return -1;
    }
    else
    {
        val = q->buffer[q->out];
        q->out = (q->out + 1) % q->queueSize;
        return val;
    }
}

#ifdef __RTEMS__

static int buff_debug_init(void)
{
    return 0;
}

static void buff_out_char(char chr)
{
    mvQueueAdd(&mvConsoleTxQueue, chr);
}

static int buff_in_char(void)
{
    return mvQueueGet(&mvConsoleRxQueue);
}
BSP_debug_configuration_type  BSP_debug_configuration = buff_debug_init;
BSP_output_char_function_type BSP_output_char         = buff_out_char;
BSP_polling_getchar_function_type BSP_poll_char       = buff_in_char;

#else

int _write_r(struct _reent * unused,int fildes, const void *buf, size_t nbyte)
{
    char *str = (char *)buf;
    size_t i;

    UNUSED(unused);
    UNUSED(fildes);

    for(i = 0 ; i < nbyte ; i++)
    {
        mvQueueAdd(&mvConsoleTxQueue, str[i]);
    }

    return i;
}

int _read_r(struct _reent * unused,int fildes, void *buf, size_t nbyte)
{
    UNUSED(unused); // hush the compiler warning.
    UNUSED(fildes); // hush the compiler warning.

    size_t i;

    for(i = 0 ; i < nbyte ; i++)
    {
        ((char*)buf)[i] = mvQueueGet(&mvConsoleRxQueue);
        if(((char*)buf)[i] == '\0')
            break;
    }

    return i;
}

#endif
