// -----------------------------------------------------------------------------
// Copyright (C) 2016 Movidius Ltd. All rights reserved
//
// Company            Movidius
// Author             Titus BIRAU (titus.birau@movidius.com)
//
// Description        Stereo API header file
//                    API interface for Stereo HW filter
//
// Usage              Steps for using this API in your application:
//                    - include API header file:
//                      #include <hwStereoApi.h>
//                    - declare global variable like:
//                      hwiStereoConfig_t stereoConfig;
//                    - initialize HW filter:
//                      hwiStereoInit( &stereoConfig );
//                    - configure stereoConfig parameters; e.g.:
//                      stereoConfig.operationMode = SCFG_MODE__SGMB;
//                    - configure HW filter with your stereo configuration:
//                      hwiStereoConfig( &stereoConfig );
//                    - start HW filter:
//                      hwiStereoStart();
//                    - wait for HW filter to finish:
//                      hwiStereoWait();
//                    - handle return status from API functions
// -----------------------------------------------------------------------------

#ifndef __STEREO_API__
#define __STEREO_API__

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>


// -----------------------------------------------------------------------------
//
//                    Register:  STEREO_CFG
//
// -----------------------------------------------------------------------------

// Operation Mode
typedef enum {
    SCFG_MODE__CT               = 0  ,  // Census Transform only
    SCFG_MODE__SGMB             = 1  ,  // Cost Aggregation only
    SCFG_MODE__CT_CM            = 2  ,  // Census Transform AND Cost Computation only
    SCFG_MODE__CT_CM_SGMB       = 3     // run full pipe on image data
}   scfgOperationMode_t;

// Image Input Mode
typedef enum {
    SCFG_IN_M__8BPP             = 0  ,  // u8  pixel intensity
    SCFG_IN_M__10BPP            = 1     // u10 pixel intensity
}   scfgImageInputMode_t;

// Output Mode
typedef enum {
    SCFG_OUT_M__DSP_MAP         = 0  ,  // ( d0, min1, min2 )      the disparity and the two minima
    SCFG_OUT_M__DSP_MC          = 1  ,  // ( do, r(p) )            the disparity and the confidence (ratio)
    SCFG_OUT_M__DSP_CD          = 2  ,  // ( disp(p) )             just the conditioned disparity
    SCFG_OUT_M__DSP_SINGLE      = 3  ,  // ( d0 )                  just the disparity
    SCFG_OUT_M__DSP_COMB        = 4  ,  // ( disp(p), r(p), min1 ) conditioned disparity, ratio and min value

    SCFG_OUT_M__NOT_CFGD        = 5
}   scfgOutputMode_t;

// Census Transform Kernel Size
typedef enum {
    SCFG_CT_KER__5X5            = 0  ,  // 5x5
    SCFG_CT_KER__7X7            = 1  ,  // 7x7
    SCFG_CT_KER__7X9            = 2     // 7x9
}   scfgCTKernelSize_t;

// Census Transform Descriptor Type
typedef enum {
    SCFG_CT_TYPE__THRESHOLD     = 0  ,  // enable threshold
    SCFG_CT_TYPE__CT_MEAN       = 1  ,  // enable CT-MEAN
    SCFG_CT_TYPE__CT_MASK       = 2     // enable CT-MASK
}   scfgCTDescriptorType_t;

// Number Of Disparities
typedef enum {
    SCFG_DSP_WD__64             = 0  ,  // 64 disparities
    SCFG_DSP_WD__96             = 1     // 96 disparities
}   scfgDisparities_t;

// Descriptor Format
typedef enum {
    SCFG_CT_FORMAT__PV8_CT24    = 0  ,
    SCFG_CT_FORMAT__PV0_CT32    = 1  ,
    SCFG_CT_FORMAT__PV0_CT64    = 2  ,
    SCFG_CT_FORMAT__PV8_CT56    = 3  ,
    SCFG_CT_FORMAT__PV10_CT48   = 4
}   scfgCTDescriptorFormat_t;

// Aggregation Division Factor
typedef enum {
    SCFG_DIV_FACTOR__00         = 0  ,
    SCFG_DIV_FACTOR__01         = 1  ,
    SCFG_DIV_FACTOR__10         = 2  ,
    SCFG_DIV_FACTOR__11         = 3  ,

    SCFG_DIV_FACTOR__NOT_CFGD   = 4
}   scfgAggDivFactor_t;

// Companding Enable
typedef enum {
    SCFG_CME__DISABLED          = 0  ,
    SCFG_CME__ENABLED           = 1
}   scfgCompandingEnable_t;

// Force Descriptor Dumping
typedef enum {
    SCFG_CTD__DISABLED          = 0  ,
    SCFG_CTD__ENABLED           = 1
}   scfgForceDescrDump_t;

// Debug Cost Dump
typedef enum {
    SCFG_DCD__DISABLED          = 0  ,
    SCFG_DCD__ENABLED           = 1
}   scfgDebugCostDump_t;

// Invalid Disparity
typedef uint8_t scfgInvalidDisp_t;


// -----------------------------------------------------------------------------
//
//                    Register:  STEREO_PARAMS
//
// -----------------------------------------------------------------------------

// CM Alfa Value
typedef uint8_t sparamsCMAlfa_t;

// CM Beta Value
typedef uint8_t sparamsCMBeta_t;

// CM Max Disparity Value
typedef uint8_t sparamsCMThreshold_t;

// CT Threshold Value
typedef uint8_t sparamsCTThreshold_t;

// Conditioned Disparity Threshold
typedef uint8_t sparamsRatioThreshold_t;


// -----------------------------------------------------------------------------
//
//                    Register:  STEREO_RWLUT_x                   where x: H, V
//
// -----------------------------------------------------------------------------

// parameters used for computing P1 and P2 parameters from the aggregation equation
typedef int32_t srwlutParams_t;


// -----------------------------------------------------------------------------
//
//                    Register:  STEREO_CTMASKx                   where x: L, H
//
// -----------------------------------------------------------------------------

// Census Transform masks
typedef uint32_t sctMask_t;


// -----------------------------------------------------------------------------
//
//                    CONFIGURATION STRUCTURE
//
// -----------------------------------------------------------------------------

// structure used for Stereo HW filter configuration
typedef struct                  hwiStereoParams
{
    // default values - set in hwiStereoInit() -->                      DEFAULT VALUES

    // Register: STEREO_CFG                               // B I T S
    scfgOperationMode_t         operationMode           ; //  2 -  0    SCFG_MODE__CT
    scfgImageInputMode_t        imageInputMode          ; //  4 -  3    SCFG_IN_M__8BPP
    scfgOutputMode_t            outputMode              ; //  7 -  5
    scfgCTKernelSize_t          ctKernelSize            ; //  9 -  8    SCFG_CT_KER__5X5
    scfgCTDescriptorType_t      ctDescriptorType        ; // 12 - 10    SCFG_CT_TYPE__THRESHOLD
    scfgDisparities_t           disparities             ; // 14 - 13    SCFG_DSP_WD__64
    scfgCTDescriptorFormat_t    ctDescriptorFormat      ; // 17 - 15    SCFG_CT_FORMAT__PV8_CT24
    scfgAggDivFactor_t          aggDivFactor            ; // 19 - 18
    scfgCompandingEnable_t      compandingEnable        ; // 20         SCFG_CME__DISABLED
    scfgForceDescrDump_t        forceDescrDump          ; // 21         SCFG_CTD__DISABLED
    scfgDebugCostDump_t         debugCostDump           ; // 22         SCFG_DCD__DISABLED
    scfgInvalidDisp_t           invalidDisp             ; // 31 - 24

    // Register: STEREO_PARAMS                            // B I T S
    sparamsCMAlfa_t             cmAlfa                  ; //  3 -  0
    sparamsCMBeta_t             cmBeta                  ; //  7 -  4
    sparamsCMThreshold_t        cmThreshold             ; // 15 -  8
    sparamsCTThreshold_t        ctThreshold             ; // 23 - 16
    sparamsRatioThreshold_t     ratioThreshold          ; // 31 - 24

    // Register:  STEREO_CTMASKx
    sctMask_t                   ctMaskL                 ;
    sctMask_t                   ctMaskH                 ;

    // Register: STEREO_RWLUT_x
    srwlutParams_t              P1_H                    ;
    srwlutParams_t              P2_H                    ;
    srwlutParams_t              P1_V                    ;
    srwlutParams_t              P2_V                    ;

    // IMAGE PARAMETERS
    uint32_t                    imageWidth              ;
    uint32_t                    imageHeight             ;
    uint32_t                    imageStride             ;

    // INPUT BUFFERS
    void     *                  inputLBuffer            ;
    void     *                  inputRBuffer            ;
    uint8_t  *                  inputPreCCostsBuffer    ;

    // OUTPUT BUFFERS
    uint32_t *                  outputLCTDescrBuffer    ;
    uint32_t *                  outputRCTDescrBuffer    ;
    uint8_t  *                  outputDebugCostsBuffer  ;
    void     *                  outputDisparityBuffer   ;

}   hwiStereoConfig_t;


// -----------------------------------------------------------------------------
//
//                    E R R O R   H A N D L E R
//
// -----------------------------------------------------------------------------

typedef enum {
    SCFG_STS__NO_ERROR                  =  0  ,  // no error - STEREO filter configured properly

    SCFG_STS__IMAGE_PARAMS_NOT_CFGD     =  1  ,  // image parameters             not configured
    SCFG_STS__IMAGE_BUFS_NOT_CFGD       =  2  ,  // image buffers                not configured
    SCFG_STS__PRECCOSTS_BUFS_NOT_CFGD   =  3  ,  // Pre-Computed Costs buffers   not configured
    SCFG_STS__CTDESCR_BUFS_NOT_CFGD     =  4  ,  // CT Descriptors buffers       not configured
    SCFG_STS__DISPARITY_BUFS_NOT_CFGD   =  5  ,  // Disparity buffers            not configured
    SCFG_STS__OUT_M_NOT_CFGD            =  6  ,  // Output Mode                  not configured
    SCFG_STS__CM_PARAMS_NOT_CFGD        =  7  ,  // CM Parameters                not configured
    SCFG_STS__CTD_BUFS_NOT_CFGD         =  8  ,  // ForceDescriptorDump buffers  not configured
    SCFG_STS__DCD_BUFS_NOT_CFGD         =  9  ,  // DebugCostDump       buffers  not configured
    SCFG_STS__RWLUT_PARAMS_NOT_CFGD     = 10  ,  // RWLUT parameters             not configured
    SCFG_STS__CT_MASKS_NOT_CFGD         = 11  ,  // CT masks                     not configured

    SCFG_STS__INIT_NOT_CALLED           = 71  ,  // hwiStereoInit()              not called
    SCFG_STS__CONFIG_NOT_CALLED         = 72  ,  // hwiStereoConfig()            not called
    SCFG_STS__START_NOT_CALLED          = 73  ,  // hwiStereoStart()             not called

    SCFG_STS__DFLT_ERROR                = 99     // default/generic error
}   scfgStatus_t;


// -----------------------------------------------------------------------------
//
//                    F U N C T I O N S
//
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
//
// API Function       hwiStereoInit
//
// Description        initializes Stereo Hw filter configuration parameters
//                    to default OR not configured values
//
// Parameters         stereoConfig - contains address of configuration
//                                   structure which need to be initialized
//
// Return             none
//
// Constraints        none
//
// -----------------------------------------------------------------------------
extern void           hwiStereoInit  ( hwiStereoConfig_t * );

// -----------------------------------------------------------------------------
//
// API Function       hwiStereoConfig
//
// Description        configures Stereo Filter based on provided configuration
//
// Parameters         stereoConfig - structure containing configuration
//                                   parameters provided by API user
//
// Return             configuration error status (scfgStatus_t)
//
// Constraints        hwiStereoInit() need to be called before
//
// -----------------------------------------------------------------------------
extern scfgStatus_t   hwiStereoConfig( hwiStereoConfig_t * );

// -----------------------------------------------------------------------------
//
// API Function       hwiStereoStart
//
// Description        starts Stereo Filter
//
// Parameters         none
//
// Return             configuration error status (scfgStatus_t)
//
// Constraints        hwiStereoConfig() need to be called before
//
// -----------------------------------------------------------------------------
extern scfgStatus_t   hwiStereoStart();

// -----------------------------------------------------------------------------
//
// API Function       hwiStereoWait
//
// Description        waits for Stereo Filter to finish processing
//
// Parameters         none
//
// Return             configuration error status (scfgStatus_t)
//
// Constraints        hwiStereoStart() need to be called before
//
// -----------------------------------------------------------------------------
extern scfgStatus_t   hwiStereoWait();



#ifdef __cplusplus
}
#endif

#endif // __STEREO_API__
