///
/// @file
/// @copyright All code copyright Movidius Ltd 2013, all rights reserved
///            For License Warranty see: common/license.txt
///
/// @defgroup EdgeApiPriv    EDGE HW Module Api
/// @ingroup  HwModInt
/// @{
/// @brief     I2C Slave Functions API.
///
///  @par Component Usage
///
///  In order to use the component the following steps are ought to be done:
///  TBD
///

#ifndef _HWI_EDGE_API_H_
#define _HWI_EDGE_API_H_

// System Includes
// ----------------------------------------------------------------------------

// Application Includes
// ----------------------------------------------------------------------------

// Source Specific #defines and types  (typedef,enum,struct)
// ----------------------------------------------------------------------------

// Api function execution return status and error reporting:
#define HWI_EDGE_COMMAND_EXECUTED_SUCCESFULLY                                                 ( 0)
#define HWI_EDGE_FILTER_ALREADY_IN_USE                                                        ( 1)
#define HWI_EDGE_FILTER_CONFIGURATION_NOT_INITIALIZED                                         ( 2)
#define HWI_EDGE_ERROR_CONFIGURATION_NOT_WRITTEN                                              ( 3)
#define HWI_EDGE_FILTER_CFG_ALREADY_INITIALIZED                                               ( 4)
#define HWI_EDGE_SOBEL_DATA_UNCONFIGURED_ERROR                                                ( 5)
#define HWI_EDGE_OP_MODE_NOT_CONFIGURED_ERROR                                                 ( 6)
#define HWI_EDGE_INPUT_IMAGE_DIMENSION_NOT_CONFIGURED_ERROR                                   ( 7)
#define HWI_EDGE_INPUT_BUFFER_TYPE_INCORECTLY_SET_FOR_NORMAL_INPUT_CFG_ERROR                  ( 8)
#define HWI_EDGE_INPUT_BUFFER_TYPE_INCORECTLY_SET_FOR_FP16_GRAD_INPUT_CFG_ERROR               ( 9)
#define HWI_EDGE_INPUT_BUFFER_NOT_INITIALISED_ERROR                                           (10)
#define HWI_EDGE_INPUT_BUFFER_TYPE_INCORECTLY_SET_FOR_U8_GRAD_INPUT_CFG_ERROR                 (11)
#define HWI_EDGE_CFG_NOT_INITIALISED_BEFORE_CFG_CALL_ERROR                                    (12)
#define HWI_EDGE_OUTPUT_BUFFER_ALREADY_ALLOCATED_POINTING_TO_MEM_LOCATION_ERROR               (13)
#define HWI_EDGE_OUTPUT_BUFFER_TYPE_INCORECTLY_SET_FOR_ORIENT_8BIT_CFG_ERROR                  (14)
#define HWI_EDGE_OUTPUT_BUFFER_TYPE_INCORECTLY_SET_FOR_SCAL_MAGN_16BIT_CFG_ERROR              (15)
#define HWI_EDGE_OUTPUT_BUFFER_TYPE_INCORECTLY_SET_FOR_MAGN_ORIENT_16BIT_CFG_ERROR            (16)
#define HWI_EDGE_OUTPUT_BUFFER_TYPE_INCORECTLY_SET_FOR_SCALED_GRADIENTS_16BIT_CFG_ERROR       (17)
#define HWI_EDGE_OUTPUT_BUFFER_TYPE_INCORECTLY_SET_FOR_PLANAR_GRADS_AND_MAGN_CFG_ERROR        (18)
#define HWI_EDGE_OUTPUT_BUFFER_TYPE_INCORECTLY_SET_FOR_SCAL_MAGN_8BIT_CFG_ERROR               (19)
#define HWI_EDGE_OUTPUT_BUFFER_TYPE_INCORECTLY_SET_FOR_DOMINANT_HIST_BIN_CFG_ERROR            (20)
#define HWI_EDGE_OUTPUT_BUFFER_INIT_FAIL                                                      (21)
#define HWI_EDGE_INPUT_BUFFER_INIT_FAIL                                                       (22)
#define HWI_EDGE_ERROR_FILTER_NOT_INITIALIZED                                                 (23)
#define HWI_EDGE_8BIT_INTERNAL_OUTPUT_BUFFER_FOR_ORIENT_8BIT_IS_NULL_ERROR                    (24)
#define HWI_EDGE_16BIT_INTERNAL_OUTPUT_BUFFER_FOR_ORIENT_8BIT_IS_NULL_ERROR                   (25)
#define HWI_EDGE_8BIT_INTERNAL_OUTPUT_BUFFER_FOR_SCALED_MAGN_8BIT_IS_NULL_ERROR               (26)
#define HWI_EDGE_16BIT_INTERNAL_OUTPUT_BUFFER_FOR_SCALED_MAGN_16BIT_IS_NULL_ERROR             (27)
#define HWI_EDGE_16BIT_INTERNAL_OUTPUT_BUFFER_FOR_MAGN_ORIENT_16BIT_IS_NULL_ERROR             (28)
#define HWI_EDGE_16BIT_INTERNAL_OUTPUT_BUFFER_FOR_MAGN_SCALED_GRADIENTS_16BIT_IS_NULL_ERROR   (29)
#define HWI_EDGE_16BIT_INTERNAL_OUTPUT_BUFFER_FOR_PLANAR_GRADS_AND_MAGN_IS_NULL_ERROR         (30)
#define HWI_EDGE_16BIT_INTERNAL_OUTPUT_BUFFER_FOR_SCALED_GRADIENTS_32BIT_IS_NULL_ERROR        (31)
#define HWI_EDGE_16BIT_INTERNAL_OUTPUT_BUFFER_FOR_DOMINANT_HIST_BIN_IS_NULL_ERROR             (32)
#define HWI_EDGE_16BIT_INTERNAL_OUTPUT_BUFFER_FOR_HOG_MODE_IS_NULL_ERROR                      (33)
#define HWI_EDGE_16BIT_USER_INPUT_BUFFER_NULL_NORMAL_MODE_ERROR                               (34)
#define HWI_EDGE_8BIT_USER_INPUT_BUFFER_NULL_NORMAL_MODE_ERROR                                (35)
#define HWI_EDGE_32BIT_USER_INPUT_BUFFER_NULL_PRE_FP16_GRAD_ERROR                             (36)
#define HWI_EDGE_16BIT_USER_INPUT_BUFFER_NULL_PRE_U8_GRAD_ERROR                               (37)
#define HWI_EDGE_PLANE_MULTIPLE_PARAMETER_NOT_IN_RANGE                                        (38)
#define HWI_EDGE_OUTPUT_IMAGE_SIZE_INCORRECT_FOR_DOMINANT_HIST_BIN_OUTPUT                     (39)
#define HWI_EDGE_OUTPUT_IMAGE_SIZE_INCORRECT_FOR_HOG_MODE_OUTPUT                              (40)

// Edge API reconfigurability related defines
#define MAX_INPUT_PLANE_NO   3
#define MAX_OUTPUT_PLANE_NO  5

#define EDGE_API_HOG_KERNEL_SIZE         4

#define EDGE_API_SOBEL_DISABLED 0

// Edge operator modes
typedef enum {
    EDGE_INPUT_NORMAL_MODE   = 0,
    EDGE_INPUT_PRE_FP16_GRAD = 1,
    EDGE_INPUT_PRE_U8_GRAD   = 2,
} e_EdgeOpInputModes;

typedef enum {
    EDGE_THETA_NORMAL_THETA  = 0,
    EDGE_THETA_X_AXIS_REFL   = 1,
    EDGE_THETA_XY_AXIS_REFL  = 2,
    EDGE_THETA_ORIGIN_REFL   = 3,
} e_EdgeOpThetaModes;

typedef enum {
    EDGE_OUTPUT_SCALED_MAGN_16BIT      = 0,   // output clipping (mask) is =0xFFFF
    EDGE_OUTPUT_SCALED_MAGN_8BIT       = 1,   // output clipping (mask) is =0xFF
    EDGE_OUTPUT_MAGN_ORIENT_16BIT      = 2,   // output clipping (mask) is =0xFF
    EDGE_OUTPUT_ORIENT_8BIT            = 3,   // output clipping (mask) is =0xFF
    EDGE_OUTPUT_SCALED_GRADIENTS_16BIT = 4,   // output clipping (mask) is =0xFFFF
    EDGE_OUTPUT_SCALED_GRADIENTS_32BIT = 5,   // output clipping (mask) is =0xFFFF
    EDGE_OUTPUT_PLANAR_GRADS_AND_MAGN  = 6,   // output clipping (mask) is =0xFFFF
    EDGE_OUTPUT_HOG_MODE               = 8,   // output clipping (mask) is =0xFFFF
    EDGE_OUTPUT_DOMINANT_HIST_BIN      = 9,   // output clipping (mask) is =0xFFFF
} e_EdgeOpOutputModes;

// Edge operator input and output buffer types
typedef enum {
    UNDEFINED_TYPE = 0,
    UNSIGNED_INT_8_BIT = 8,
    UNSIGNED_INT_16_BIT = 16,
    UNSIGNED_INT_32_BIT = 32,
    UNSIGNED_INT_64_BIT = 64,
    FLOATING_POINT_FP16_BIT = 0xF6,
} e_EdgeApiBufferTypes;


// Edge Api configuration structure: - variable needs to be allocated in user app.
typedef struct {
    int XSobelCoefficients;
    int YSobelCoefficients;

    unsigned int  SetInputImageWidth;   // input variable
    unsigned int  SetInputImageHeight;  // input variable

    unsigned int  GetOutputImageWidth;  // output variable, set by the API
    unsigned int  GetOutputImageHeight; // output variable, set by the API

    e_EdgeApiBufferTypes  e_InputBufferType;
    e_EdgeApiBufferTypes  e_OutputBufferType;

    unsigned int  NumberOfInputPlanes;
    unsigned int  NumberOfOutputPlanes;

    e_EdgeOpInputModes           EdgeOpInputMode;          // Can take values from  e_EdgeOpInputModes
    e_EdgeOpThetaModes           EdgeOpThetaMode;          // Can take values from  e_EdgeOpThetaModes
    e_EdgeOpOutputModes          EdgeOpOutputMode;         // Can take values from  e_EdgeOpOutputModes
    bool          THETA_OVX_enabled;

    float     magnitudeScaleFactor;              // is floating point value and needs to be =1.[x]f where x=any number
    int       planeMultiple;                     //can be 0,1,2,3

    void *inputBuffer[MAX_INPUT_PLANE_NO];             // Pointer to the input buffer
    void *outputBuffer[MAX_OUTPUT_PLANE_NO];            // Pointer to the output buffer
} hwEdgePcModelCfgTable;

#ifdef __cplusplus
extern "C" {
#endif

// Global function prototypes
// ----------------------------------------------------------------------------

// Function that allocates and initializes in memory the configuration structure.
//
// F. parameters:
//        hwEdgePcModelCfgTable ** EdgeFilterUserCfg     :    Input-Output par.
//              This parameter takes as an input value, the address of the
//              configuration structure pointer, that had been defined in the
//              users application, in order to update the address it's pointing to.
// F. the function returns returns HWI_EDGE_COMMAND_EXECUTED_SUCCESFULLY if executed
// successfully, otherwise and error message that need to be forwarded to Movidius for
// troubleshooting
extern int hwiEdgeGetInitTheCfgStruct(hwEdgePcModelCfgTable **EdgeFilterUserCfg);

// Function that configures and allocates resources needed by the filter. The user
// needs to make sure he configured all the needed parameters of the filter before
// calling this function
//
// F. parameters:
//        hwEdgePcModelCfgTable * EdgeFilterUserCfg     :    Input-Output par.
//              This parameter takes as an input value, the address of the
//              configuration structure in memory, it updates certain values
// F. the function returns returns HWI_EDGE_COMMAND_EXECUTED_SUCCESFULLY if executed
// successfully, otherwise and error message that need to be forwarded to Movidius for
// troubleshooting
extern int hwiEdgeCfgAndAllocatePcModel( hwEdgePcModelCfgTable *EdgeFilterUserCfg );

// Function that will start the filter image processing if configuration is finished.
// F. parameters:
//        hwEdgePcModelCfgTable * EdgeFilterUserCfg     :    Input-Output par.
//              This parameter takes as an input value, the address of the
//              configuration structure in memory, it updates certain values
// F. the function returns returns HWI_EDGE_COMMAND_EXECUTED_SUCCESFULLY if executed
// successfully, otherwise and error message that need to be forwarded to Movidius for
// troubleshooting
extern int hwiEdgeStart(hwEdgePcModelCfgTable *EdgeFilterUserCfg);


// Function that cleans up allocated buffers and resets configuration flags.
// F. parameters:
//        hwEdgePcModelCfgTable ** EdgeFilterUserCfg     :    Input-Output par.
//              This parameter takes as an input value, the address of the
//              configuration structure pointer, that had been defined in the
//              users application, in order to update the address it's pointing to
// F. the function returns returns HWI_EDGE_COMMAND_EXECUTED_SUCCESFULLY if executed
// successfully, otherwise and error message that need to be forwarded to Movidius for
// troubleshooting
extern int hwiEdgeCleanUp(hwEdgePcModelCfgTable **EdgeFilterUserCfg);

/// }@
#ifdef __cplusplus
}
#endif


#endif
