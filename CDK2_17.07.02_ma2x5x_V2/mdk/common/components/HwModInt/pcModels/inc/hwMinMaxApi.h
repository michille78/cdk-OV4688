#ifndef __MinMaxApiH__
#define __MinMaxApiH__



typedef enum minMaxFrmTypeEnum {
  FixedPoint16,
  FloatingPoint16,
  U32,
  NONE
} minMaxFrmType;

typedef struct minMaxDebugStruct {
  uint32_t enable;
  uint32_t lbrc;
  uint32_t pixel;
} minMaxDebug;

typedef struct minMaxFrameSpecsStruct {
     minMaxFrmType  type;
     unsigned int   width;
     unsigned int   height;
     unsigned int   lineStride;
     unsigned int   planes;
} minMaxSpec;

typedef struct minMaxBufferStruct {
     minMaxSpec spec;
     unsigned char *plane;
     unsigned char *outPlanes[3];
} minMaxBuffer;

typedef struct minMaxCfgStruct {
  uint16_t extremaThreshold;
  uint16_t scoreThreshold;
  uint16_t maxExtrema;
  uint32_t roundingEnable;
  uint32_t outDenseEnable0;
  uint32_t outDenseEnable1;
  minMaxDebug debug;
} minMaxCfg;


int32_t MinMaxSetup(minMaxBuffer in[3], minMaxBuffer out[2], minMaxCfg cfg);

int32_t MinMaxStart();

int32_t MinMaxWait();

int32_t MinMaxCleanup();


#endif
