///
/// @file
/// @copyright All code copyright Movidius Ltd 2012, all rights reserved
///            For License Warranty see: common/license.txt
///
/// @defgroup CNN   Convolutional Neural Network
/// @ingroup  CNN
/// @{
/// @brief     Definitions and types needed by the CNN library
///
/// This file contains all the definitions of constants, typedefs, structures, enums and exported variables for the CNN library
///

#ifndef HWCNN_API_DEFINES_H_
#define HWCNN_API_DEFINES_H_

/// @brief Structure of a CNN layer (elements are hidden)
typedef struct
{
    uint64_t config[16];
} cnnLayer;

/// @brief Operation Mode enum
typedef enum
{
    MODE_1_256 = 0,
    MODE_2_128 = 1,
    MODE_4_64  = 2,
    MODE_8_32  = 3,
    MODE_16_16 = 4
} cnnOperationMode;

/// @brief Coefficient Mode enum
typedef enum
{
    FP16_COEFF = 0,
    U8F_COEFF  = 1,
    FOUR_BIT_PLLTZD = 2,
    TWO_BIT_PLLTZD = 3,
    ONE_BIT_PLLTZD = 4,
    ONE_BIT_DIRECT = 5
} cnnCoefficientMode;

/// @brief Input data Mode enum
typedef enum
{
    MODE_FP16 = 0,
    MODE_U8F  = 1
} cnnDataMode;

/// @brief Pad Mode enum
typedef enum
{
    PAD_WITH_ZEROS = 0x00,
    PAD_REPEAT_RIGHT_EDGE = 0x01,
    PAD_REPEAT_LEFT_EDGE = 0x08,
    PAD_REPEAT_TOP_EDGE = 0x04,
    PAD_REPEAT_BOTTOM_EDGE = 0x02
} cnnPadMode;

/// @brief Pool Type enum
typedef enum
{
    POOL_MAX = 0,
    POOL_AVERAGE = 1
} cnnPoolType;

/// @brief CNN Lane enum
typedef enum
{
    CNN_LANE0 = 0,
    CNN_LANE1 = 1,
} cnnLane;

/// @brief CNN Block enum
typedef enum
{
    CNN_BLOCK0 = 0,
    CNN_BLOCK1 = 1,
}cnnBlock;
#endif /* HWCNN_API_DEFINES_H_ */
