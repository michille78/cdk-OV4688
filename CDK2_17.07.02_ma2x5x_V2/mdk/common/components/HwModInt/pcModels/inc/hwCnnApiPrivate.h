///
/// @file
/// @copyright All code copyright Movidius Ltd 2012, all rights reserved
///            For License Warranty see: common/license.txt
///
/// @defgroup CNN   Convolutional Neural Network
/// @defgroup CNNApi Convolutional Neural Network API
/// @ingroup  CNN
/// @{
/// @brief     Convolutional Neural Network API
///
/// This is the API for the Convolutional Neural Network hardware module.
/// It provides functionality for building and configuring a CNN from the standard blocks:
/// convolution, pooling and fully connected layers. Additionally it allows the configuration
/// of optional elements such as ReLu, bias and offset.
/// Layers can be linked together in a form of simple linked list.
///

#ifndef __HWCNN_API_PRIVATE_H__
#define __HWCNN_API_PRIVATE_H__

// 1: Includes
// ----------------------------------------------------------------------------
#include <stdint.h>

#ifdef __cplusplus
extern "C"
{
#endif

#include "hwCnnApiDefines.h"

// 2:  Exported Global Data (generally better to avoid)
// ----------------------------------------------------------------------------


// 3:  Exported Functions (non-inline)
// ----------------------------------------------------------------------------

/// @brief Initializes the basic elements of a CNN layer
/// @param[in] layer - pointer to the layer that will be configured
/// @param[in] dataMode     - type of the input data (fp16 or u8f)
/// @param[in] Id - the Id of the layer
/// @param[in] disableInt - disables or enables interrupt for this layer ( 0 = enabled, 1 = disabled)
/// @param[in] interruptTrigger - the interrupt that will be triggered on completion of this layer (if interrupts are enabled)
/// @param[in] linkTo - pointer to another layer to which the current layer will be linked to (can also be NULL in case of the root node)
/// @return    void
/// @note      This function must be called prior to any other function
void cnnInitDescriptor(cnnLayer *layer, cnnDataMode dataMode, uint32_t Id, uint32_t disableInt, uint32_t interruptTrigger, cnnLayer *linkTo);

/// @brief Configures the input parameters for a Convolution or Pooling layer
/// @param[in] layer        - pointer to the layer that will be configured
/// @param[in] inputData    - pointer to the input data
/// @param[in] noOfChannels - number of input channels
/// @param[in] chStride     - input data channel stride
/// @param[in] lineStride   - input data line stride
/// @param[in] dataWidth    - input data width
/// @param[in] dataHeight   - input data height
/// @return    void
void cnnSetupInput(cnnLayer *layer, void *inputData, uint32_t noOfChannels, uint32_t chStride, uint32_t lineStride, uint32_t dataWidth, uint32_t dataHeight);

/// @brief Configures the input parameters for a Convolution or Pooling layer (strides are automatically computed)
/// @param[in] layer        - pointer to the layer that will be configured
/// @param[in] inputData    - pointer to the input data
/// @param[in] noOfChannels - number of input channels
/// @param[in] dataWidth    - input data width
/// @param[in] dataHeight   - input data height
/// @return    void
void cnnSetupInputBrief(cnnLayer *layer, void *inputData, uint32_t noOfChannels, uint32_t dataWidth, uint32_t dataHeight);

/// @brief Configures the output parameters for a Convolution or Pooling layer
/// @param[in] layer        - pointer to the layer that will be configured
/// @param[in] outputData   - pointer to the output data
/// @param[in] noOfChannels - number of output channels
/// @param[in] chStride     - output data channel stride
/// @param[in] lineStide    - output data line stride
/// @return    void
void cnnSetupOutput(cnnLayer *layer, void *outputData, uint32_t noOfChannels, uint32_t chStride, uint32_t lineStride);

/// @brief Configures the output parameters for a Convolution or Pooling layer (strides are automatically computed)
/// @param[in] layer        - pointer to the layer that will be configured
/// @param[in] outputData   - pointer to the output data
/// @param[in] noOfChannels - number of output channels
/// @return    void
/// @note The cnnSetupInput/cnnSetupInputBrief, cnnSetupConvolutionFeatureMaps/cnnSetupConvolutionFeatureMapsBrief, cnnSetupConvolutionPooling(optionally) and cnnSetupPadding(optionally) must be called first
void cnnSetupOutputBrief(cnnLayer *layer, void *outputData, uint32_t noOfChannels);

/// @brief Configures the convolution feature maps for a Convolution layer
/// @param[in] layer             - pointer to the layer that will be configured
/// @param[in] mode              - coefficient mode (fp16, u8f, palletized)
/// @param[in] coefficientData   - pointer to the coefficient data structure (see documentation for the coefficient structure format)
/// @param[in] chStrideIn        - coefficient data channel stride in (see documentation)
/// @param[in] chStrideOut       - coefficient data channel stride out (see documentation)
/// @param[in] kernelWidth       - feature kernel width
/// @param[in] kernelHeight      - feature kernel height
/// @param[in] convolutionStride - kernel stride for convolution
/// @param[in] palette           - coefficient palette - FP16 stored as uint16 (NULL in case of FP16, U8F or one bit direct mode)
/// @return    void
void cnnSetupConvolutionCoefficients(cnnLayer *layer, cnnCoefficientMode mode, void *coefficientData, uint32_t chStrideIn, uint32_t chStrideOut, uint32_t kernelWidth, uint32_t kernelHeight, uint32_t convolutionStride, uint16_t *palette);

/// @brief Configures the convolution feature maps for a Convolution layer (strides are automatically computed)
/// @param[in] layer             - pointer to the layer that will be configured
/// @param[in] mode              - coefficient mode (fp16, u8f, palletized)
/// @param[in] coefficientData   - pointer to the coefficient data structure (see documentation for the coefficient structure format)
/// @param[in] kernelWidth       - feature kernel width
/// @param[in] kernelHeight      - feature kernel height
/// @param[in] convolutionStride - kernel stride for convolution
/// @param[in] palette           - coefficient palette - FP16 stored as uint16 (NULL in case of FP16, U8F or one bit direct mode)
/// @return    void
/// @note      the cnnSetupInput or cnnSetupInputBrief must be called first
void cnnSetupConvolutionCoefficientsBrief(cnnLayer *layer, cnnCoefficientMode mode, void *coefficientData, uint32_t kernelWidth, uint32_t kernelHeight, uint32_t convolutionStride, uint16_t *palette);

/// @brief Configures the optional max pooling on a convolution layer
/// @param[in] layer - pointer to the convolution layer that will be configured with the optional max pooling
/// @param[in] kernelWidth - pool kernel width
/// @param[in] kernelHeight - pool kernel height
/// @return    void
/// @note      This function can only be called on a Convolution type layer
void cnnSetupConvolutionPooling(cnnLayer *layer, uint32_t kernelWidth, uint32_t kernelHeight);

/// @brief Configures a pooling only layer
/// @param[in] layer - pointer to a layer that will be configured
/// @param[in] type - pool type (max or average pooling)
/// @param[in] kernelWidth - pool kernel width
/// @param[in] kernelHeight - pool kernel height
/// @param[in] poolStride - kernel stride for pooling
/// @return    void
void cnnSetupPoolingLayer(cnnLayer *layer, cnnPoolType type, uint32_t kernelWidth, uint32_t kernelHeight, uint32_t poolStride);

/// @brief Configures the input parameters for a fully connected layer
/// @param[in] layer - pointer to a layer that will be configured
/// @param[in] inputData - pointer to the input data
/// @param[in] inputWidht - width of the input data
/// @param[in] lineStride - input data line stride (see documentation)
/// @param[in] blockStride - input data block stride (see documentation)
/// @return    void
void cnnSetupInputFullyConnectedLayer(cnnLayer *layer, void *inputData, uint32_t inputWidth, uint32_t lineStride, uint32_t blockStride);

/// @brief Configures the input parameters for a fully connected layer (strides are automatically computed)
/// @param[in] layer - pointer to a layer that will be configured
/// @param[in] inputData - pointer to the input data
/// @param[in] inputWidht - width of the input data
/// @return    void
void cnnSetupInputFullyConnectedLayerBrief(cnnLayer *layer, void *inputData, uint32_t inputWidth);

/// @brief Configures the output parameters for a fully connected layer
/// @param[in] layer - pointer to a layer that will be configured
/// @param[in] outputData - pointer to the output data
/// @param[in] lineStride - output data line stride (see documentation)
/// @return    void
void cnnSetupOutputFullyConnectedLayer(cnnLayer *layer, void *outputData, uint32_t lineStride);

/// @brief Configures the output parameters for a fully connected layer (strides are automatically computed)
/// @param[in] layer - pointer to a layer that will be configured
/// @param[in] outputData - pointer to the output data
/// @return    void
void cnnSetupOutputFullyConnectedLayerBrief(cnnLayer *layer, void *outputData);

/// @brief Configures the vector data for a fully connected layer
/// @param[in] layer - pointer to a layer that will be configured
/// @param[in] mode  - coefficient mode (fp16, u8f, palletized)
/// @param[in] vectorData - pointer to the vector data structure (see documentation for the vector structure format)
/// @param[in] noOfVectors - number of vectors
/// @param[in] strideIn - vector data stride in (see documentation)
/// @param[in] strideOut - vector data stride out (see documentation)
/// @return    void
void cnnSetupVectorsFullyConnectedLayer(cnnLayer *layer, cnnCoefficientMode mode, void *vectorData, uint32_t noOfVectors, uint32_t strideIn, uint32_t strideOut);

/// @brief Configures the vector data for a fully connected layer (strides are automatically computed)
/// @param[in] layer - pointer to a layer that will be configured
/// @param[in] mode  - coefficient mode (fp16, u8f, palletized)
/// @param[in] vectorData - pointer to the vector data structure (see documentation for the vector structure format)
/// @param[in] noOfVectors - number of vectors
/// @return    void
void cnnSetupVectorsFullyConnectedLayerBrief(cnnLayer *layer, cnnCoefficientMode mode, void *vectorData, uint32_t noOfVectors);

/// @brief Configures the optional padding for a layer
/// @param[in] layer - pointer to the layer that will be configured
/// @param[in] mode - padding mode (see documentation)
/// @return    void
void cnnSetupPadding(cnnLayer *layer, uint32_t mode);

/// @brief Configures the optional ReLu function for a convolution or fully connected layer
/// @param[in] layer - pointer to the layer that will be configured
/// @param[in] t0 - T0 argument for ReLu function - 10 bit signed integer converted to FP16
/// @param[in] a0 - A0 argument for ReLu function - 10 bit signed integer converted to FP16
/// @param[in] a1 - A1 argument for ReLu function - 10 bit signed integer converted to FP16
/// @return    void
void cnnSetupReLu(cnnLayer *layer, uint16_t t0, uint16_t a0, uint16_t a1);

/// @brief Configure the optional ReLuX function for a convolution or fully connected layer
/// @param[in] layer - pointer to the layer that will be configured
/// @param[in] x - the X parameter for the ReLuX function - FP16 stored as uint16
/// @return    void
void cnnSetupReLuX(cnnLayer *layer, uint16_t x);

/// @brief Configures the optional bias parameter for a convolution or fully connected layer
/// @param[in] layer - pointer to the layer that will be configured
/// @param[in] biasData - pointer to the bias data
/// @return    void
void cnnSetupBias(cnnLayer *layer, void *biasData);

/// @brief Configures the optional scale parameter for a convolution or fully connected layer
/// @param[in] layer - pointer to the layer that will be configured
/// @param[in] scaleData - pointer to the scale data
/// @return    void
void cnnSetupScale(cnnLayer *layer, void *scaleData);

/// @brief Finalizes the configuration of a cnn layer
/// @param[in] layer - pointer to a layer that will be configured
/// @return    0             - the layer is valid
///            anything else - the layer has an invalid configuration
/// @note      This function must be called at the end of every layer configuration
int cnnFinalizeLayer(cnnLayer *layer);

/// @brief Unlinks a layer from it's child nodes
/// @param[in] layer - pointer to a layer descriptor
/// @return    pointer to the child layer (or NULL if the current node has no child)
/// @note      This function detaches the child node from a parent node and returns the child node address
cnnLayer *cnnUnlink(cnnLayer *layer);

/// @brief Links all layer nodes given as parameters between them
/// @param[in] noOfLayers - number of layer descriptors to be linked together
/// @param[in] ...        - list of layer pointers that will be linked together; the order or the input parameters determines the link order
/// @return    void
void cnnLinkList(uint32_t noOfLayers, ...);

/// @brief Links together an array of layer descriptors
/// @param[in] noOfLayers - number of layer descriptors to be linked together
/// @param[in] layers - pointer to the first layer descriptor in the array
/// @return    void
void cnnLinkArray(uint32_t noOfLayers, cnnLayer *layers);

/// @brief Links two layers together
/// @param[in] parent - the parent node (parent points to child)
/// @param[in] child - the child node (child follows parent in the linked list)
/// @return    void
void cnnLinkLayers(cnnLayer *parent, cnnLayer *child);

/// @brief Starts the CNN hardware on a given list
/// @param[in] lane - CNN hardware lane to use
/// @param[in] layers - pointer to a linked list of layer descriptors
/// @param[in] callback - callback function (called each time a descriptor is finished)
/// @return    void
void cnnStart(cnnBlock block, cnnLane lane, cnnLayer *layers);

// 4: inline function implementations
// ----------------------------------------------------------------------------

#ifdef __cplusplus
}
#endif

/// @}
#endif //__HWCNN_API_PRIVATE_H__
