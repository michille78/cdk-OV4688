// -----------------------------------------------------------------------------
// Copyright (C) 2016 Movidius Ltd. All rights reserved
//
// Company            Movidius
// Author             Titus BIRAU (titus.birau@movidius.com)
//
// Description        Warp API header file
//                    API interface for Warp HW filter
//
// Usage              Steps for using this API in your application:
//                    - include API header file:
//                      #include <hwWarpApi.h>
//                    - declare global variable like: 
//                      hwiWarpConfig_t warpCfg;
//                    - initialize HW filter:
//                      hwiWarpInit( &warpCfg );
//                    - configure warpCfg parameters; e.g.:
//                      warpCfg.edgeColorMode = WARP_EDGECOLOUR__REPLICATE;
//                    - configure HW filter with your warp configuration:
//                      hwiWarpConfig( &warpCfg );
//                    - start HW filter:
//                      hwiWaitStart();
//                    - wait for HW filter to finish:
//                      hwiWarpWait();
//                    - handle return status from API functions
// -----------------------------------------------------------------------------

#ifndef __WARP_API__
#define __WARP_API__

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>


// -----------------------------------------------------------------------------
//
//                    M A C R O S
//
// -----------------------------------------------------------------------------


// Register: WARP_EDGECOLOUR
//           Edge Color Mode
#define      WARP_EDGECOLOUR__REPLICATE     (        0  )           // replicate pixel
#define      WARP_EDGECOLOUR__COLOR(color)  ( (1 << 16) \
                                            | ( color ) )           // use clear-color
#define      COLOR__BLACK                   (        0  )           // can use own color


// Register: WARP_MESH
//           Mesh Configuration/Mode
#define      WARP_MESH__SPARSE_F32          (  3 <<  6  )           // sparse.f32
#define      WARP_MESH__DENSE_I16(relative, bits_of_fraction) \
                                 ( (                 2 << 6 ) \
                                 | (                 1 << 5 ) \
                                 | (  bits_of_fraction << 1 ) \
                                 | (          relative << 0 ) )     // dense.i16
#define      RELATIVE                          1                    // - relative to the output pixel
#define      NOT_RELATIVE                      0                    // - not relative to the output pixel
#define      WARP_MESH__BYPASS              ( (1 <<  8) \
                                            | (1 <<  5) \
                                            | (1 <<  0) )           // mesh bypass



// -----------------------------------------------------------------------------
//
//                    D A T A   T Y P E S
//
// -----------------------------------------------------------------------------

typedef enum {
    WARP__0  =  0 ,
    WARP__1  =  1 ,
    WARP__2  =  2
}   warpInstance_t;

typedef enum {
    WARP_MODE__BILINEAR =  2  ,     // bit 9
    WARP_MODE__BICUBIC  =  1  ,     // bit 8
    WARP_MODE__NONE     =  0
}   warpInterpMode_t;



// -----------------------------------------------------------------------------
//
//                    CONFIGURATION STRUCTURE
//
// -----------------------------------------------------------------------------

// structure used for Warp HW filter configuration
typedef struct          hwiWarpParams
{
    // WARP instance
    //                  - WARP__0                           --  DEFAULT VALUE (set in hwiWarpInit())
    //                  - WARP__1
    //                  - WARP__2
    warpInstance_t      warpInstance           ;

    // MESH             Mesh   Memory - Pointer & Size
    void *              mesh                   ;   // mesh          [address]
    uint32_t            meshWidth              ;   // mesh   width  [meshpoints]
    uint32_t            meshHeight             ;   // mesh   width  [meshpoints]

    // INPUT            Image  Memory - Pointer & Size
    void *              inputBuffer            ;   // input  buffer [address]
    uint32_t            inputWidth             ;   // input  width  [pixels]
    uint32_t            inputHeight            ;   // input  height [pixels]

    // OUTPUT           Output Memory - Pointer & Size
    void *              outputBuffer           ;   // output buffer [address]
    uint32_t            outputWidth            ;   // output width  [pixels]
    uint32_t            outputHeight           ;   // output height [pixels]]

    // Register:        WARP_MODE
    //                  Interpolation Mode
    //                  - WARP_MODE__BILININEAR
    //                  - WARP_MODE__BICUBIC                --  DEFAULT VALUE (set in hwiWarpInit())
    //                  - WARP_MODE__NONE
    warpInterpMode_t    interpMode             ;

    // Register:        WARP_EDGECOLOUR
    //                  Edge Color Mode
    //                  - WARP_EDGECOLOUR__REPLICATE
    //                  - WARP_EDGECOLOUR__COLOR(color)     --  DEFAULT VALUE (set in hwiWarpInit())
    uint32_t            edgeColorMode          ;

    // Register:        WARP_MESH
    //                  Mesh Configuration/Mode
    //                  - WARP_MESH__SPARSE_F32             --  DEFAULT VALUE (set in hwiWarpInit())
    //                  - WARP_MESH__DENSE_I16(relative, bits_of_fraction)
    //                  - WARP_MESH__BYPASS
    uint32_t            meshMode               ;

    // Register:        WARP_MATx       x = 0..8
    //                  Coordonations Tranform Matrix
    //                  - can be NULL (by-passed)           --  DEFAULT VALUE (set in hwiWarpInit())
    float *             matrix3x3              ;

}   hwiWarpConfig_t;


// -----------------------------------------------------------------------------
//
//                    E R R O R   H A N D L E R
//
// -----------------------------------------------------------------------------

typedef enum {
    WCFG_STS__NO_ERROR                  =  0  ,  // no error - WARP filter configured properly

    WCFG_STS__MESH_SIZE_NOT_CFGD        = 11  ,  // mesh   memory size    not configured

    WCFG_STS__INPUT_BUFF_NOT_CFGD       = 20  ,  // image  memory pointer not configured
    WCFG_STS__INPUT_SIZE_NOT_CFGD       = 21  ,  // image  memory size    not configured

    WCFG_STS__OUTPUT_BUFF_NOT_CFGD      = 30  ,  // output memory pointer not configured
    WCFG_STS__OUTPUT_SIZE_NOT_CFGD      = 31  ,  // output memory size    not configured

    WCFG_STS__INIT_NOT_CALLED           = 71  ,  // hwiWarpInit()         not called
    WCFG_STS__CONFIG_NOT_CALLED         = 72  ,  // hwiWarpConfig()       not called
    WCFG_STS__START_NOT_CALLED          = 73  ,  // hwiWarpStart()        not called

    WCFG_STS__DFLT_ERROR                = 99     // default/generic error
}   wcfgStatus_t;


// -----------------------------------------------------------------------------
//
//                    F U N C T I O N S
//
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
//
// API Function       hwiWarpInit
//
// Description        initializes Warp Hw filter configuration parameters
//                    to default OR not configured values
//
// Parameters         warpConfig - contains address of configuration
//                                 structure which need to be initialized
//
// Return             none
//
// Constraints        none
//
// -----------------------------------------------------------------------------
extern void           hwiWarpInit  ( hwiWarpConfig_t * );

// -----------------------------------------------------------------------------
//
// API Function       hwiWarpConfig
//
// Description        configures Warp Filter based on provided configuration
//
// Parameters         warpConfig - structure containing configuration
//                                 parameters provided by API user
//
// Return             configuration error status (wcfgStatus_t)
//
// Constraints        hwiWarpInit() need to be called before
//
// -----------------------------------------------------------------------------
extern wcfgStatus_t   hwiWarpConfig( hwiWarpConfig_t * );

// -----------------------------------------------------------------------------
//
// API Function       hwiWarpStart
//
// Description        starts Warp Filter
//
// Parameters         none
//
// Return             configuration error status (wcfgStatus_t)
//
// Constraints        hwiWarpConfig() need to be called before
//
// -----------------------------------------------------------------------------
extern wcfgStatus_t   hwiWarpStart ();

// -----------------------------------------------------------------------------
//
// API Function       hwiWarpWait
//
// Description        waits for Warp Filter to finish
//
// Parameters         none
//
// Return             configuration error status (wcfgStatus_t)
//
// Constraints        hwiWarpStart() need to be called before
//
// -----------------------------------------------------------------------------
extern wcfgStatus_t   hwiWarpWait  ();



#ifdef __cplusplus
}
#endif

#endif // __WARP_API__
