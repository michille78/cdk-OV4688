///
/// @file
/// @copyright All code copyright Movidius Ltd 2013, all rights reserved
///            For License Warranty see: common/license.txt
///
/// @defgroup EdgeApiPriv    EDGE HW Module Api
/// @ingroup  HwModInt
/// @{
/// @brief     I2C Slave Functions API.
///
///  @par Component Usage
///
///  In order to use the component the following steps are ought to be done:
///   -# Declare a variable of i2cSlaveAddrCfg_t type
///   -# Initialize the members of the variable above declared
///   -# Declare a variable of i2cSlaveHandle_t type (handler)
///   -# Initialize a member of the handler declared, i2cConfig_t
///   -# Call I2CSlaveSetupCallbacks() function, having as parameters address of handler declared,
///                       result of i2cRead function, result of i2cRead function
///   -# Call I2CSlaveInit() function, having as parameters the address of the handler declared,
///                       I2C block, and the address of I2CSlaveAddrCfg variable
///
/// i2cReadAction() callback is called every time a RD_REQ interrupt is triggered, meaning
/// that the master has issued a read request and it is waiting for data. The function
/// can be used to prepare the data to be send to the master.
///
/// i2cWriteAction() callback is called when the slave has finished reading the information sent
/// by the master and also a stop bit has been received, signaling that the master has finished
/// the transfer. It provides the data that was received from master.
///

#ifndef _HWI_MODELS_API_H_
#define _HWI_MODELS_API_H_

// System Includes
// ----------------------------------------------------------------------------
#include "hwCnnApi.h"
#include "hwCnnApiPrivate.h"
#include "hwEdgeApi.h"
#include "hwStereoApi.h"
#include "hwWarpApi.h"
#include "hwMinMaxApi.h"
#include "hwMotEstApi.h"
// Application Includes
// ----------------------------------------------------------------------------

#ifdef __cplusplus
extern "C" {
#endif

// Source Specific #defines and types  (typedef,enum,struct)
// ----------------------------------------------------------------------------

// Global function prototypes
// ----------------------------------------------------------------------------


/// }@
#ifdef __cplusplus
}
#endif


#endif
