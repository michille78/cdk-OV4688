// -----------------------------------------------------------------------------
// Copyright (C) 2016 Movidius Ltd. All rights reserved
//
// Company            Movidius
// Author             Titus BIRAU (titus.birau@movidius.com)
//
// Description        MotEst API header file
//                    API interface for MotEst HW filter
//
// Usage              Steps for using this API in your application:
//                    - include API header file:
//                      #include <hwMotEstApi.h>
//                    - declare global variable like:
//                      hwiMotEstConfig_t motEstCfg;
//                    - initialize HW filter:
//                      hwiMotEstInit( &motEstCfg );
//                    - configure motEstCfg parameters; e.g.:
//                      motEstCfg.disableHalfPel = DISABLE_H_PEL;
//                    - configure HW filter with your MotEst configuration:
//                      hwiMotEstConfig( &motEstCfg );
//                    - start HW filter:
//                      hwiWaitStart();
//                    - wait for HW filter to finish:
//                      hwiMotEstWait();
//                    - handle return status from API functions
// -----------------------------------------------------------------------------

#ifndef __MOTEST_API__
#define __MOTEST_API__

#ifdef __cplusplus
extern "C" {
#endif


#include <stdint.h>

// -----------------------------------------------------------------------------
//
//                    M A C R O S
//
// -----------------------------------------------------------------------------


#define  MOTEST_IMG_MAX_WIDTH                               8192  // maximum width  accepted by the hw filter
#define  MOTEST_IMG_MAX_HEIGHT                              4096  // maximum height accepted by the hw filter

#define  MOTEST_MAX_BLK_SIZE                                32    // maximum block size to be searched

#define  RASTER_STEP_SIZE__DFLT                             8
#define  RASTER_STEP_SIZE__MAX                              MOTEST_MAX_BLK_SIZE

#define  SRCH_MODE__DIRECTED__MAX                           \
         MOTEST_IMG_MAX_WIDTH  / MOTEST_MAX_BLK_SIZE *      \
         MOTEST_IMG_MAX_HEIGHT / MOTEST_MAX_BLK_SIZE        // maximum possible number of blocks
                                                            // that can be searched when in directed mode

#define  IMG_RESOLUTION__MAX                                \
         MOTEST_IMG_MAX_WIDTH * MOTEST_IMG_MAX_HEIGHT       // maximum image resolution


// -----------------------------------------------------------------------------
//
//                    D A T A   T Y P E S
//
// -----------------------------------------------------------------------------

typedef enum {
    SRCH_MODE__DIRECTED     = 0 ,   // coords direct kernel location
    SRCH_MODE__RASTER       = 1 ,   // full search, all 8x8s in raster order
}   motestSrchMode_t;

typedef enum {
    OUT_MODE__ONLY_MVS      = 0 ,   // only motion vectors: {X[15:8], Y[7:0]}
    OUT_MODE__SAD_MVS       = 1     // SAD and MVs: {SAD[31:16], X[15:8], Y[7:0]}
}   motestOutMode_t;

typedef struct  pixelCoords         // coordinates of the top left pel of SRC_BLK
{
    uint32_t    xpel ;
    uint32_t    ypel ;
}   motestPelCoords_t;

// -----------------------------------------------------------------------------
//
//                    CONFIGURATION STRUCTURE
//
// -----------------------------------------------------------------------------

// structure used for MotEst HW filter configuration
typedef struct              hwiMotEstParams
{
    // reference image      REF_IMG (Luma only)
    uint8_t *               refImgBuffer            ;
    // source    image      SRC_IMG (Luma only)
    uint8_t *               srcImgBuffer            ;

    // image width  (max. 8192)
    uint32_t                imgWidth                ;
    // image height (max. 4096)
    uint32_t                imgHeight               ;
    // image stride (shall be the same with imgWidth)
    uint32_t                imgStride               ;

    // results buffer
    uint32_t *              results                 ;

    // raster step size (multiple of 8, max. 32)    - default value: RASTER_STEP_SIZE__DFLT
    uint8_t                 rasterStepSize          ;

    // Search Mode                                  - default value: SRCH_MODE__RASTER
    motestSrchMode_t        srchMode                ;
    // coordinates of the top pel of SRC_BLKs
    motestPelCoords_t *     srchCoords              ;
    // number of blocks to search
    uint32_t                srchNumBlks             ;

    // Out Mode                                     - default value: OUT_MODE__ONLY_MVS
    motestOutMode_t         outMode                 ;

    // disable Half/Quarter pel                     - default value: false (for both)
    bool                    disableHalfPel          ;
    bool                    disableQuarterPel       ;

}   hwiMotEstConfig_t;


// -----------------------------------------------------------------------------
//
//                    E R R O R   H A N D L E R
//
// -----------------------------------------------------------------------------

typedef enum {
    MCFG_STS__NO_ERROR                  =  0  ,  // no error - MotEst filter configured properly

    MCFG_STS__REF_IMG_NOT_CFGD          = 11  ,  // reference      image not configured
    MCFG_STS__SRC_IMG_NOT_CFGD          = 12  ,  // source         image not configured

    MCFG_STS__RESULTS_BUFF_NOT_CFGD     = 21  ,  // results       buffer not configured

    MCFG_STS__IMG_PARAMS_NOT_CFGD       = 31  ,  // image     parameters not configured
    MCFG_STS__IMG_PARAMS_INVALID        = 32  ,  // image parameters wrongly configured
    MCFG_STS__RASTER_STEP_INVALID       = 33  ,  // raster step      wrongly configured
    MCFG_STS__SRCH_COORDS_NOT_CFGD      = 34  ,  // search coords        not configured

    MCFG_STS__INIT_NOT_CALLED           = 71  ,  // hwiMotEstInit()      not called
    MCFG_STS__CONFIG_NOT_CALLED         = 72  ,  // hwiMotEstConfig()    not called
    MCFG_STS__START_NOT_CALLED          = 73  ,  // hwiMotEstStart()     not called

    MCFG_STS__DFLT_ERROR                = 99     // default/generic error
}   mcfgStatus_t;


// -----------------------------------------------------------------------------
//
//                    F U N C T I O N S
//
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
//
// API Function       hwiMotEstInit
//
// Description        initializes MotEst Hw filter configuration parameters
//                    to default OR not configured values
//
// Parameters         motEstConfig - contains address of configuration
//                                   structure which need to be initialized
//
// Return             none
//
// Constraints        none
//
// -----------------------------------------------------------------------------
extern void           hwiMotEstInit( hwiMotEstConfig_t * );

// -----------------------------------------------------------------------------
//
// API Function       hwiMotEstConfig
//
// Description        configures MotEst Filter based on provided configuration
//
// Parameters         motEstConfig - structure containing configuration
//                                   parameters provided by API user
//
// Return             configuration error status (mcfgStatus_t)
//
// Constraints        hwiMotEstInit() need to be called before
//
// -----------------------------------------------------------------------------
extern mcfgStatus_t   hwiMotEstConfig( hwiMotEstConfig_t * );

// -----------------------------------------------------------------------------
//
// API Function       hwiMotEstStart
//
// Description        starts MotEst Filter
//
// Parameters         none
//
// Return             configuration error status (mcfgStatus_t)
//
// Constraints        hwiMotEstConfig() need to be called before
//
// -----------------------------------------------------------------------------
extern mcfgStatus_t   hwiMotEstStart();

// -----------------------------------------------------------------------------
//
// API Function       hwiMotEstWait
//
// Description        waits for WaMotEst Filter to finish
//
// Parameters         none
//
// Return             configuration error status (mcfgStatus_t)
//
// Constraints        hwiMotEstStart() need to be called before
//
// -----------------------------------------------------------------------------
extern mcfgStatus_t   hwiMotEstWait();



#ifdef __cplusplus
}
#endif

#endif // __MOTEST_API__
