LIST_OF_API_NAMES := Warp Cnn Stereo Edge MinMax
MDK_PROJ_STRING ?= "MDK_FALSE"

# define safe remove to be able to safely clean all generated output
define SAFE_RM
	@SHOW_RM="$(ECHO)" ; \
	THE_RM_ALLOWED_IN=`readlink -f $(MV_COMMON_BASE)`; \
	for d in $(1) ; do \
	[ "$${d##$$THE_RM_ALLOWED_IN}" != "$$THE_RM_PATH" ] && rm -rf $$d ; \
	[ "$$SHOW_RM" != "@" ] && echo "rm -rf $$d" || true ; \
	done ;
endef

OS_Name:=$(shell cat /etc/*-release | grep '^ID=' | sed 's/ID=//;')

ifeq (fedora,$(OS_Name))
  # this works only on ubuntu by default:
  OS:=$(shell cat /etc/*-release | grep '^NAME' | sed 's/NAME=//;')
  VER:=$(shell cat /etc/*-release | grep '^VERSION_ID' | sed 's/VERSION_ID=//;')
endif
ifeq (ubuntu,$(OS_Name))
  # this works only on ubuntu by default:
  OS:=$(shell lsb_release -si)
  VER:=$(shell lsb_release -sr)
endif

ifneq ($(findstring $(firstword $(subst ., ,$(VER))),12 14 15 16 25),)
LINUX_VARIANT := $(OS)$(firstword $(subst ., ,$(VER)))
endif

ifeq ($(LINUX_VARIANT),"")
$(error "not suported Linux version")
endif

-include $(MV_COMMON_BASE)/inhousetargets.mk
-include $(MV_COMMON_BASE)/HwModApi.mk

hwModelsApiLib = libHwModulesfull.a
hwModelsApiIncDir = $(MV_COMMON_BASE)/components/HwModInt/pcModels/inc
hwModelsApiDir = $(MV_COMMON_BASE)/components/HwModInt/pcModels/src
hwModelsApiLibDir = $(MV_COMMON_BASE)/../packages/movidius/pcModel/ma2x8x/libs/$(LINUX_VARIANT)LTS

Cnn_Api_CPP_FLAGS = -std=c++0x -Wall -O0 -g -fopenmp -fpermissive -Wno-unknown-pragmas -Wno-comment -Wno-reorder -m32
MinMax_Api_CPP_FLAGS = -std=c++0x -Wall -O0 -g -fopenmp -fpermissive -Wno-unknown-pragmas -Wno-comment -Wno-reorder -m32
Stereo_Api_CPP_FLAGS = -std=c++0x -Wall -O0 -g -fopenmp -fpermissive -Wno-unknown-pragmas -Wno-comment -Wno-reorder -m32
Warp_Api_CPP_FLAGS = -std=c++0x -Wall -O0 -g -fopenmp -fpermissive -Wno-unknown-pragmas -Wno-comment -Wno-reorder
Edge_Api_CPP_FLAGS = -std=c++0x -Wall -O0 -g -fopenmp -fpermissive -Wno-unknown-pragmas -Wno-comment -Wno-reorder -m32
MotEst_Api_CPP_FLAGS =   -O2 -g -m32

Cnn_Api_LD_FLAGS = -m elf_i386
MinMax_Api_LD_FLAGS = -m elf_i386
Stereo_Api_LD_FLAGS =  -m elf_i386
Warp_Api_LD_FLAGS = -m elf_x86_64
Edge_Api_LD_FLAGS = -m elf_i386
MotEst_Api_LD_FLAGS = -m elf_i386

Cnn_Api_CPP = g++
MinMax_Api_CPP = g++
Stereo_Api_CPP = g++
Warp_Api_CPP = g++
Edge_Api_CPP = g++
MotEst_Api_CPP = g++

Cnn_Api_LD = ld
MinMax_Api_LD = ld
Stereo_Api_LD = ld
Warp_Api_LD = ld
Edge_Api_LD = ld
MotEst_Api_LD = ld
