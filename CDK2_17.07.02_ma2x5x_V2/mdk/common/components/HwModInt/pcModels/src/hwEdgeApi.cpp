///
/// @file
/// @copyright All code copyright Movidius Ltd 2012, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     main leon file
///

#include <fp16.h>
#include <stdint.h>
#include <memory>
#include <thread>
#include <chrono>
#include <mutex>

#include <cstdio>

// 1: Includes
// ----------------------------------------------------------------------------
#include "hwEdgeApi.h"
#include <registersMyriad2.h>
#include <sippHwDefines.h>
#include <sippHwFactory.h>
#include <sippEdgeAdaptiveMedian.h>
#include <sippEdgeOperatorAgg.h>
#include <sippEdgeOperator.h>
#include <sippUnitTestProducer.h>
#include <sippUnitTestConsumer.h>
#include <sippImageFileRW.h>
#include <measure.h>

#include "sippHwM2Factory.h"
#include "sippUnitTestIsr.h"
#include "sippUnitTestConfigurer.h"

// 2:  Source Specific #defines and types  (typedef, enum, struct)
// ----------------------------------------------------------------------------
#define SIPP_ID            SIPP_EDGE_OP_ID
#define SIPP_ID_MASK       (1 << SIPP_ID)

// Front-end of the mini-pipe
#define SIPP_IBUF_BASE_ADR BUF_BASE_REG(SIPP,EDGE_OP,I,0)
#define SIPP_IBUF_CFG_ADR  BUF_CFG_REG(SIPP,EDGE_OP,I,0)
#define SIPP_IBUF_LS_ADR   BUF_LS_REG(SIPP,EDGE_OP,I,0)
#define SIPP_IBUF_PS_ADR   BUF_PS_REG(SIPP,EDGE_OP,I,0)
#define SIPP_IBUF_IR_ADR   BUF_IR_REG(SIPP,EDGE_OP,I,0)
#define SIPP_IBUF_FC_ADR   BUF_FC_REG(SIPP,EDGE_OP,I,0)
#define SIPP_ICTX_ADR      CTX_REG(SIPP,EDGE_OP,I,0)

// Back-end of the mini-pipe
#define SIPP_OBUF_BASE_ADR  BUF_BASE_REG(SIPP,EDGE_OP,O,0)
#define SIPP_OBUF_CFG_ADR   BUF_CFG_REG(SIPP,EDGE_OP,O,0)
#define SIPP_OBUF_LS_ADR    BUF_LS_REG(SIPP,EDGE_OP,O,0)
#define SIPP_OBUF_PS_ADR    BUF_PS_REG(SIPP,EDGE_OP,O,0)
#define SIPP_OBUF_IR_ADR    BUF_IR_REG(SIPP,EDGE_OP,O,0)
#define SIPP_OBUF_FC_ADR    BUF_FC_REG(SIPP,EDGE_OP,O,0)
#define SIPP_OCTX_ADR       CTX_REG(SIPP,EDGE_OP,O,0)

#define THETA_OVX           ENABLED
// 3: Global Data (Only if absolutely necessary)
// ----------------------------------------------------------------------------

// 4: Static Local Data
// ----------------------------------------------------------------------------
class ApbSlaveInterface;

static bool b_EdgeModelInUse;
static bool b_EdgeCfgInitDone;
static bool b_EdgeCfgDone;
static bool b_EdgeInputInitDone;
static bool b_EdgeOutputInitDone;
static bool b_EdgeStarted;
static bool b_EdgeFinished;
static bool b_EdgeOutputBuffCfgDone;

const bool b_VerbosityEnabled = false;

// Input buffers for holding the image data
uint8_t   * edgeApi_inbuf_8normal[MAX_INPUT_PLANE_NO];
uint16_t  * edgeApi_inbuf_16normal[MAX_INPUT_PLANE_NO];
uint16_t  * edgeApi_inbuf_8Grad[MAX_INPUT_PLANE_NO];
uint32_t  * edgeApi_inbuf_16Grad[MAX_INPUT_PLANE_NO];

// Output buffers for holding the output data
std::shared_ptr<uint16_t> edgeApi_outbuf_SCALED_MAGN_16BIT[MAX_OUTPUT_PLANE_NO];
std::shared_ptr<uint8_t > edgeApi_outbuf_SCALED_MAGN_8BIT[MAX_OUTPUT_PLANE_NO];
std::shared_ptr<uint16_t> edgeApi_outbuf_MAGN_ORIENT_16BIT[MAX_OUTPUT_PLANE_NO];
std::shared_ptr<uint8_t > edgeApi_outbuf_ORIENT8_8BIT[MAX_OUTPUT_PLANE_NO];
std::shared_ptr<uint16_t> edgeApi_outbuf_ORIENT8_16BIT[MAX_OUTPUT_PLANE_NO];
std::shared_ptr<uint16_t> edgeApi_outbuf_SCALED_GRADIENTS_16BIT[MAX_OUTPUT_PLANE_NO];
std::shared_ptr<uint32_t> edgeApi_outbuf_SCALED_GRADIENTS_32BIT[MAX_OUTPUT_PLANE_NO];
std::shared_ptr<uint16_t> edgeApi_outbuf_PLANAR_GRADS_AND_MAGN[MAX_OUTPUT_PLANE_NO];
std::shared_ptr<fp16    > edgeApi_outbuf_HOG_MODE[MAX_OUTPUT_PLANE_NO];
std::shared_ptr<uint32_t> edgeApi_outbuf_DOMINANT_HIST_BIN[MAX_OUTPUT_PLANE_NO];

static const uint32_t  edgeApi_inAddr[] = { SIPP_IBUF_BASE_ADR, SIPP_IBUF_CFG_ADR, SIPP_IBUF_LS_ADR, SIPP_IBUF_PS_ADR, SIPP_IBUF_FC_ADR, SIPP_ICTX_ADR };
static const uint32_t edgeApi_outAddr[] = { SIPP_OBUF_BASE_ADR, SIPP_OBUF_CFG_ADR, SIPP_OBUF_LS_ADR, SIPP_OBUF_PS_ADR, SIPP_OBUF_FC_ADR, SIPP_OCTX_ADR };

// Establish test case interrupts, name and verbosity
static int  idMask = SIPP_ID_MASK;
static int aIdMask = 0;
static int irqMask = 0x7;
static std::string tcstr("Edge Filter");

Semaphore_sp edgeApi_inputSem[IsrHandler::MAX_IBUFS];
Semaphore_sp edgeApi_outputSem[IsrHandler::MAX_OBUFS];

// These objects are responsible with configuring the I/O buffers and
// image resolution. They will be used within the producer/consumer threads
static SippUnitTestConfigurer edgeApi_inputCfg[IsrHandler::MAX_IBUFS];
static SippUnitTestConfigurer edgeApi_outputCfg[IsrHandler::MAX_OBUFS];

// Pointers to the threads
static    SippUnitTestProcess::thrd_t *p_edgeApi_pthrd;
static    SippUnitTestProcess::thrd_t *p_edgeApi_cthrd;

// Pointer to the buffers
SippUnitTestBufferAlloc<uint8_t> *edgeApi_ibuf_8;
SippUnitTestBufferAlloc<uint8_t> *edgeApi_obuf_8;
SippUnitTestBufferAlloc<uint16_t> *edgeApi_ibuf_16;
SippUnitTestBufferAlloc<uint16_t> *edgeApi_obuf_16;
SippUnitTestBufferAlloc<uint32_t> *edgeApi_ibuf_32;
SippUnitTestBufferAlloc<uint32_t> *edgeApi_obuf_32;

SippUnitTestBufferAlloc<fp16> *edgeApi_obuf_fp16;

static SippUnitTestProcess *p_edgeApi_prod_proc;
static SippUnitTestProcess *p_edgeApi_cons_proc;

SippUnitTestConfigurer::ApbSlaveInterface_sp edgeApi_sippHwAcc;

typedef struct {
    unsigned int InputKernelSize;           // By default 3 for edge filter, currently hard coded...
    unsigned int OutputKernelSize;
    unsigned int InputLineStride;
    unsigned int OutputLineStride;

} InternalCfgStruct_t;

static InternalCfgStruct_t gs_IntCfgStruct;

// 5: Static Function Prototypes
// ----------------------------------------------------------------------------

// 6: Functions Implementation
// ----------------------------------------------------------------------------

bool hwiIsEdgeInUse(void)
{
    return b_EdgeModelInUse;
}

void hwiSetEdgeInUse(void)
{
    b_EdgeModelInUse = true;
}

void hwiClrEdgeInUse(void)
{
    b_EdgeModelInUse = false;
}



static int hwiConfigureSobelCoefficients(ApbSlaveInterface * sippHwAcc, hwEdgePcModelCfgTable *EdgeFilterUserCfg)
{
    int retVal = HWI_EDGE_COMMAND_EXECUTED_SUCCESFULLY;

    if (   (EdgeFilterUserCfg->XSobelCoefficients != (int)0xCDCDCDCD)
        && (EdgeFilterUserCfg->YSobelCoefficients != (int)0xCDCDCDCD))
    {
        sippHwAcc->ApbWrite(SIPP_EDGE_OP_XCOEFF_ADR, EdgeFilterUserCfg->XSobelCoefficients);
        sippHwAcc->ApbWrite(SIPP_EDGE_OP_YCOEFF_ADR, EdgeFilterUserCfg->YSobelCoefficients);
    }
    else
    {
        retVal = HWI_EDGE_SOBEL_DATA_UNCONFIGURED_ERROR;
    }

    return retVal;
}



static int hwiConfigureEdgeOperator(ApbSlaveInterface * sippHwAcc, hwEdgePcModelCfgTable *EdgeFilterUserCfg)
{
    int retVal = HWI_EDGE_COMMAND_EXECUTED_SUCCESFULLY;
    int data = 0;

    // Set I/O modes
    if (   (EdgeFilterUserCfg->EdgeOpInputMode  != (int)0xCDCDCDCD)
        && (EdgeFilterUserCfg->EdgeOpOutputMode != (int)0xCDCDCDCD)
        && (EdgeFilterUserCfg->EdgeOpThetaMode  != (int)0xCDCDCDCD))
    {
        data = EdgeFilterUserCfg->EdgeOpInputMode        |
               EdgeFilterUserCfg->EdgeOpOutputMode << 2  |
               EdgeFilterUserCfg->EdgeOpThetaMode  << 8;

        if (EdgeFilterUserCfg->THETA_OVX_enabled)
        {
            data |= (int)0x1000;  // enable theta angle calculations (1<<12)
        }

        //factor in the magnification scale
        if ((int)EdgeFilterUserCfg->magnitudeScaleFactor != (int)0xCDCDCDCD)
        {
            switch(EdgeFilterUserCfg->EdgeOpOutputMode)
            {
                case EDGE_OUTPUT_SCALED_GRADIENTS_16BIT:
                case EDGE_OUTPUT_SCALED_GRADIENTS_32BIT:
                case EDGE_OUTPUT_MAGN_ORIENT_16BIT     :
                case EDGE_OUTPUT_SCALED_MAGN_8BIT      :
                case EDGE_OUTPUT_SCALED_MAGN_16BIT     :
                case EDGE_OUTPUT_HOG_MODE              :
                case EDGE_OUTPUT_DOMINANT_HIST_BIN     :
                {
                    data |= fp16(EdgeFilterUserCfg->magnitudeScaleFactor).getPackedValue() << 16;
                    break;
                }
                case EDGE_OUTPUT_ORIENT_8BIT           :
                case EDGE_OUTPUT_PLANAR_GRADS_AND_MAGN :
                {
                    if ((0 < EdgeFilterUserCfg->planeMultiple) && (EdgeFilterUserCfg->planeMultiple >= 3))
                    {
                        data |= (EdgeFilterUserCfg->planeMultiple - 1) << 14; //mask uninteresting data
                        data |= fp16(EdgeFilterUserCfg->magnitudeScaleFactor).getPackedValue() << 16;
                    }
                    else
                        retVal = HWI_EDGE_PLANE_MULTIPLE_PARAMETER_NOT_IN_RANGE;
                    break;
                }
                default:
                {
                    break;
                }
            }
        }
        sippHwAcc->ApbWrite(SIPP_EDGE_OP_CFG_ADR, data);

        // Sobel coefficients
        retVal = hwiConfigureSobelCoefficients(sippHwAcc, EdgeFilterUserCfg);

        if (retVal == HWI_EDGE_COMMAND_EXECUTED_SUCCESFULLY)
        {
            // Set magnitude scale factor
            if (   (EdgeFilterUserCfg->SetInputImageHeight != 0xCDCDCDCD)
                && (EdgeFilterUserCfg->SetInputImageWidth  != 0xCDCDCDCD))
            {
                // Image resolution
                sippHwAcc->ApbWrite(SIPP_EDGE_OP_FRM_DIM_ADR, ((EdgeFilterUserCfg->SetInputImageHeight & SIPP_IMGDIM_MASK) << SIPP_IMGDIM_SIZE) |
                                                               (EdgeFilterUserCfg->SetInputImageWidth  & SIPP_IMGDIM_MASK));
            }
            else
            {
                retVal = HWI_EDGE_INPUT_IMAGE_DIMENSION_NOT_CONFIGURED_ERROR;
            }
        }
    }
    else
    {
        retVal = HWI_EDGE_OP_MODE_NOT_CONFIGURED_ERROR;
    }

    return retVal;
}



static int hwiConfigurePipe(ApbSlaveInterface *sippHwAcc,  hwEdgePcModelCfgTable *EdgeFilterUserCfg)
{
    int retVal = HWI_EDGE_COMMAND_EXECUTED_SUCCESFULLY;

    if (b_EdgeCfgInitDone)
    {
        retVal = hwiConfigureEdgeOperator(sippHwAcc, EdgeFilterUserCfg);
        if (retVal == HWI_EDGE_COMMAND_EXECUTED_SUCCESFULLY)
        {
            b_EdgeCfgDone = true;
        }
    }
    else
    {
        retVal = HWI_EDGE_CFG_NOT_INITIALISED_BEFORE_CFG_CALL_ERROR;
    }

    return retVal;
}



static int hwiEdgeConfigureInputBuffer(hwEdgePcModelCfgTable *EdgeFilterUserCfg, SippUnitTestConfigurer * p_incfg)
{
    int retStat = HWI_EDGE_COMMAND_EXECUTED_SUCCESFULLY;
    const unsigned long inputBufferPixelSize = (unsigned long)(EdgeFilterUserCfg->SetInputImageHeight * gs_IntCfgStruct.InputLineStride);

    if (!hwiIsEdgeInUse() && b_EdgeCfgInitDone && b_EdgeCfgDone)
    {
        // Input buffer needs to point towards a memory location containing image data
        retStat = HWI_EDGE_INPUT_BUFFER_NOT_INITIALISED_ERROR;

        if (EdgeFilterUserCfg->inputBuffer != NULL)
        {
            retStat = HWI_EDGE_COMMAND_EXECUTED_SUCCESFULLY;
        }
        if (HWI_EDGE_COMMAND_EXECUTED_SUCCESFULLY == retStat)
        {
            switch(EdgeFilterUserCfg->EdgeOpInputMode)
            {
                case EDGE_INPUT_NORMAL_MODE:
                {
                    switch(EdgeFilterUserCfg->e_InputBufferType)
                    {
                        case UNSIGNED_INT_8_BIT:
                        {
                            for (unsigned int pl =0; pl < EdgeFilterUserCfg->NumberOfInputPlanes; pl++)
                            {
                                edgeApi_inbuf_8normal[pl] = (uint8_t *)malloc(inputBufferPixelSize);  // 1 byte for each pixel
                                if (EdgeFilterUserCfg->inputBuffer[pl] != NULL)
                                    memcpy(edgeApi_inbuf_8normal[pl], (uint8_t *)EdgeFilterUserCfg->inputBuffer[pl], inputBufferPixelSize);
                                else
                                    retStat = HWI_EDGE_8BIT_USER_INPUT_BUFFER_NULL_NORMAL_MODE_ERROR;
                            }
                            p_edgeApi_prod_proc = new SippUnitTestProcess(circularProducer< uint8_t , edgeApi_inbuf_8normal, false>,  *p_incfg, 60000);
                            break;
                        }
                        case UNSIGNED_INT_16_BIT:
                        {
                            for (unsigned int pl =0; pl < EdgeFilterUserCfg->NumberOfInputPlanes; pl++)
                            {
                                edgeApi_inbuf_16normal[pl] = (uint16_t *)malloc(inputBufferPixelSize);  // 2 byte for each pixel
                                if (EdgeFilterUserCfg->inputBuffer[pl] != NULL)
                                    memcpy(edgeApi_inbuf_16normal[pl], EdgeFilterUserCfg->inputBuffer[pl], inputBufferPixelSize);
                                else
                                    retStat = HWI_EDGE_16BIT_USER_INPUT_BUFFER_NULL_NORMAL_MODE_ERROR;
                            }
                            p_edgeApi_prod_proc = new SippUnitTestProcess(circularProducer<uint16_t, edgeApi_inbuf_16normal, false>,  *p_incfg, 60000);
                            break;
                        }
                        case UNSIGNED_INT_32_BIT:
                        case UNSIGNED_INT_64_BIT:
                        case FLOATING_POINT_FP16_BIT:
                        case UNDEFINED_TYPE:
                        default:
                        {
                            retStat = HWI_EDGE_INPUT_BUFFER_TYPE_INCORECTLY_SET_FOR_NORMAL_INPUT_CFG_ERROR;
                            break;
                        }
                    }
                    break;
                }
                case EDGE_INPUT_PRE_FP16_GRAD:
                {
                    switch(EdgeFilterUserCfg->e_InputBufferType)
                    {
                        case UNSIGNED_INT_32_BIT:
                        {
                            for (unsigned int pl =0; pl < EdgeFilterUserCfg->NumberOfInputPlanes; pl++)
                            {
                                edgeApi_inbuf_16Grad[pl] = (uint32_t *)malloc(inputBufferPixelSize);
                                if (EdgeFilterUserCfg->inputBuffer[pl] != NULL)
                                    memcpy(edgeApi_inbuf_16Grad[pl], EdgeFilterUserCfg->inputBuffer[pl], inputBufferPixelSize);
                                else
                                    retStat = HWI_EDGE_32BIT_USER_INPUT_BUFFER_NULL_PRE_FP16_GRAD_ERROR;
                            }
                            p_edgeApi_prod_proc = new SippUnitTestProcess(circularProducer<uint32_t, edgeApi_inbuf_16Grad, false>,  *p_incfg, 60000);
                            break;
                        }
                        case UNSIGNED_INT_16_BIT:
                        case UNSIGNED_INT_8_BIT:
                        case UNSIGNED_INT_64_BIT:
                        case FLOATING_POINT_FP16_BIT:
                        case UNDEFINED_TYPE:
                        default:
                        {
                            retStat = HWI_EDGE_INPUT_BUFFER_TYPE_INCORECTLY_SET_FOR_FP16_GRAD_INPUT_CFG_ERROR;
                            break;
                        }
                    }
                    break;
                }
                case EDGE_INPUT_PRE_U8_GRAD:
                {
                    switch(EdgeFilterUserCfg->e_InputBufferType)
                    {
                        case UNSIGNED_INT_16_BIT:
                        {
                            for (unsigned int pl =0; pl < EdgeFilterUserCfg->NumberOfInputPlanes; pl++)
                            {
                                edgeApi_inbuf_8Grad[pl] = (uint16_t *)malloc(inputBufferPixelSize);  // 2 byte for each pixel
                                if (EdgeFilterUserCfg->inputBuffer[pl] != NULL)
                                    memcpy(edgeApi_inbuf_8Grad[pl], EdgeFilterUserCfg->inputBuffer[pl], inputBufferPixelSize);
                                else
                                    retStat = HWI_EDGE_16BIT_USER_INPUT_BUFFER_NULL_PRE_U8_GRAD_ERROR;
                            }
                            p_edgeApi_prod_proc = new SippUnitTestProcess(circularProducer<uint16_t, edgeApi_inbuf_8Grad, false>,  *p_incfg, 60000);
                            break;
                        }
                        case UNSIGNED_INT_32_BIT:
                        case UNSIGNED_INT_8_BIT:
                        case UNSIGNED_INT_64_BIT:
                        case FLOATING_POINT_FP16_BIT:
                        case UNDEFINED_TYPE:
                        default:
                        {
                            retStat = HWI_EDGE_INPUT_BUFFER_TYPE_INCORECTLY_SET_FOR_U8_GRAD_INPUT_CFG_ERROR;
                            break;
                        }
                    }
                    break;
                }
            }
        }
    }
    else
    {
        retStat = HWI_EDGE_FILTER_ALREADY_IN_USE;
    }

    return retStat;
}



static int hwiEdgeConfigureOutputBuffer(hwEdgePcModelCfgTable *EdgeFilterUserCfg, SippUnitTestConfigurer * p_outcfg)
{
    int retStat = HWI_EDGE_COMMAND_EXECUTED_SUCCESFULLY;

    if (!hwiIsEdgeInUse() && b_EdgeCfgInitDone && b_EdgeCfgDone)
    {
        if (retStat == HWI_EDGE_COMMAND_EXECUTED_SUCCESFULLY)
        {
            switch(EdgeFilterUserCfg->EdgeOpOutputMode)
            {
                case EDGE_OUTPUT_ORIENT_8BIT:
                {
                    switch(EdgeFilterUserCfg->e_OutputBufferType)
                    {
                        case UNSIGNED_INT_8_BIT:
                        {
                            p_edgeApi_cons_proc = new SippUnitTestProcess(circularConsumer<uint8_t, std::shared_ptr<uint8_t>, edgeApi_outbuf_ORIENT8_8BIT, false>,  *p_outcfg, 60000);
                            break;
                        }
                        case UNSIGNED_INT_16_BIT:
                        {
                            p_edgeApi_cons_proc = new SippUnitTestProcess(circularConsumer<uint16_t,std::shared_ptr<uint16_t>, edgeApi_outbuf_ORIENT8_16BIT, false>,  *p_outcfg, 60000);
                            break;
                        }
                        case UNSIGNED_INT_32_BIT:
                        case UNSIGNED_INT_64_BIT:
                        case FLOATING_POINT_FP16_BIT:
                        case UNDEFINED_TYPE:
                        default:
                        {
                            retStat = HWI_EDGE_OUTPUT_BUFFER_TYPE_INCORECTLY_SET_FOR_ORIENT_8BIT_CFG_ERROR;
                            break;
                        }
                    }
                    break;
                }
                case EDGE_OUTPUT_SCALED_MAGN_8BIT:
                {
                    switch(EdgeFilterUserCfg->e_OutputBufferType)
                    {
                        case UNSIGNED_INT_8_BIT:
                        {
                            p_edgeApi_cons_proc = new SippUnitTestProcess(circularConsumer<uint8_t, std::shared_ptr<uint8_t>, edgeApi_outbuf_SCALED_MAGN_8BIT, false>,  *p_outcfg, 60000);
                            break;
                        }
                        case UNSIGNED_INT_16_BIT:
                        case UNSIGNED_INT_32_BIT:
                        case UNSIGNED_INT_64_BIT:
                        case FLOATING_POINT_FP16_BIT:
                        case UNDEFINED_TYPE:
                        default:
                        {
                            retStat = HWI_EDGE_OUTPUT_BUFFER_TYPE_INCORECTLY_SET_FOR_SCAL_MAGN_8BIT_CFG_ERROR;
                            break;
                        }
                    }
                    break;
                }
                case EDGE_OUTPUT_SCALED_MAGN_16BIT:
                {
                    switch(EdgeFilterUserCfg->e_OutputBufferType)
                    {
                        case UNSIGNED_INT_16_BIT:
                        {
                            p_edgeApi_cons_proc = new SippUnitTestProcess(circularConsumer<uint16_t, std::shared_ptr<uint16_t>, edgeApi_outbuf_SCALED_MAGN_16BIT, false>,  *p_outcfg, 60000);
                            break;
                        }
                        case UNSIGNED_INT_8_BIT:
                        case UNSIGNED_INT_32_BIT:
                        case UNSIGNED_INT_64_BIT:
                        case FLOATING_POINT_FP16_BIT:
                        case UNDEFINED_TYPE:
                        default:
                        {
                            retStat = HWI_EDGE_OUTPUT_BUFFER_TYPE_INCORECTLY_SET_FOR_SCAL_MAGN_16BIT_CFG_ERROR;
                            break;
                        }
                    }
                    break;
                }
                case EDGE_OUTPUT_MAGN_ORIENT_16BIT:
                {
                    switch(EdgeFilterUserCfg->e_OutputBufferType)
                    {
                        case UNSIGNED_INT_16_BIT:
                        {
                            p_edgeApi_cons_proc = new SippUnitTestProcess(circularConsumer<uint16_t, std::shared_ptr<uint16_t>, edgeApi_outbuf_MAGN_ORIENT_16BIT, false>,  *p_outcfg, 60000);
                            break;
                        }
                        case UNSIGNED_INT_8_BIT:
                        case UNSIGNED_INT_32_BIT:
                        case UNSIGNED_INT_64_BIT:
                        case FLOATING_POINT_FP16_BIT:
                        case UNDEFINED_TYPE:
                        default:
                        {
                            retStat = HWI_EDGE_OUTPUT_BUFFER_TYPE_INCORECTLY_SET_FOR_MAGN_ORIENT_16BIT_CFG_ERROR;
                            break;
                        }
                    }
                    break;
                }
                case EDGE_OUTPUT_SCALED_GRADIENTS_16BIT:
                {
                    switch(EdgeFilterUserCfg->e_OutputBufferType)
                    {
                        case UNSIGNED_INT_16_BIT:
                        {
                            p_edgeApi_cons_proc = new SippUnitTestProcess(circularConsumer<uint16_t, std::shared_ptr<uint16_t>, edgeApi_outbuf_SCALED_GRADIENTS_16BIT, false>,  *p_outcfg, 60000);
                            break;
                        }
                        case UNSIGNED_INT_8_BIT:
                        case UNSIGNED_INT_32_BIT:
                        case UNSIGNED_INT_64_BIT:
                        case FLOATING_POINT_FP16_BIT:
                        case UNDEFINED_TYPE:
                        default:
                        {
                            retStat = HWI_EDGE_OUTPUT_BUFFER_TYPE_INCORECTLY_SET_FOR_SCALED_GRADIENTS_16BIT_CFG_ERROR;
                            break;
                        }
                    }
                    break;
                }
                case EDGE_OUTPUT_PLANAR_GRADS_AND_MAGN:
                {
                    switch(EdgeFilterUserCfg->e_OutputBufferType)
                    {
                        case UNSIGNED_INT_16_BIT:
                        {
                            p_edgeApi_cons_proc = new SippUnitTestProcess(circularConsumer<uint16_t, std::shared_ptr<uint16_t>, edgeApi_outbuf_PLANAR_GRADS_AND_MAGN, false>,  *p_outcfg, 60000);
                            break;
                        }
                        case UNSIGNED_INT_8_BIT:
                        case UNSIGNED_INT_32_BIT:
                        case UNSIGNED_INT_64_BIT:
                        case FLOATING_POINT_FP16_BIT:
                        case UNDEFINED_TYPE:
                        default:
                        {
                            retStat = HWI_EDGE_OUTPUT_BUFFER_TYPE_INCORECTLY_SET_FOR_PLANAR_GRADS_AND_MAGN_CFG_ERROR;
                            break;
                        }
                    }
                    break;
                }
                case EDGE_OUTPUT_SCALED_GRADIENTS_32BIT:
                {
                    switch(EdgeFilterUserCfg->e_OutputBufferType)
                    {
                        case UNSIGNED_INT_32_BIT:
                        {
                            p_edgeApi_cons_proc = new SippUnitTestProcess(circularConsumer<uint32_t, std::shared_ptr<uint32_t>, edgeApi_outbuf_SCALED_GRADIENTS_32BIT, false>,  *p_outcfg, 60000);
                            break;
                        }
                        case UNSIGNED_INT_8_BIT:
                        case UNSIGNED_INT_16_BIT:
                        case UNSIGNED_INT_64_BIT:
                        case FLOATING_POINT_FP16_BIT:
                        case UNDEFINED_TYPE:
                        default:
                        {
                            retStat = HWI_EDGE_OUTPUT_BUFFER_TYPE_INCORECTLY_SET_FOR_PLANAR_GRADS_AND_MAGN_CFG_ERROR;
                            break;
                        }
                    }
                    break;
                }
                case EDGE_OUTPUT_HOG_MODE:
                {
                    switch(EdgeFilterUserCfg->e_OutputBufferType)
                    {
                        case FLOATING_POINT_FP16_BIT:
                        {
                            p_edgeApi_cons_proc = new SippUnitTestProcess(circularConsumer<fp16, std::shared_ptr<fp16>, edgeApi_outbuf_HOG_MODE, false>,  *p_outcfg, 60000);
                            break;
                        }
                        case UNSIGNED_INT_8_BIT:
                        case UNSIGNED_INT_16_BIT:
                        case UNSIGNED_INT_32_BIT:
                        case UNSIGNED_INT_64_BIT:
                        case UNDEFINED_TYPE:
                        default:
                        {
                            retStat = HWI_EDGE_OUTPUT_BUFFER_TYPE_INCORECTLY_SET_FOR_PLANAR_GRADS_AND_MAGN_CFG_ERROR;
                            break;
                        }
                    }
                    break;
                }
                case EDGE_OUTPUT_DOMINANT_HIST_BIN:
                {
                    switch(EdgeFilterUserCfg->e_OutputBufferType)
                    {
                        case UNSIGNED_INT_32_BIT:
                        {
                            p_edgeApi_cons_proc = new SippUnitTestProcess(circularConsumer<uint32_t, std::shared_ptr<uint32_t>, edgeApi_outbuf_DOMINANT_HIST_BIN, false>,  *p_outcfg, 60000);
                            break;
                        }
                        case UNSIGNED_INT_8_BIT:
                        case UNSIGNED_INT_16_BIT:
                        case UNSIGNED_INT_64_BIT:
                        case FLOATING_POINT_FP16_BIT:
                        case UNDEFINED_TYPE:
                        default:
                        {
                            retStat = HWI_EDGE_OUTPUT_BUFFER_TYPE_INCORECTLY_SET_FOR_DOMINANT_HIST_BIN_CFG_ERROR;
                            break;
                        }
                    }
                    break;
                }
            }
            if (retStat == HWI_EDGE_COMMAND_EXECUTED_SUCCESFULLY) b_EdgeOutputBuffCfgDone = true;
        }
    }
    else
    {
        retStat = HWI_EDGE_FILTER_ALREADY_IN_USE;
    }

    return retStat;
}


static int hwiEdgeCopyDataToOutputBuffer(hwEdgePcModelCfgTable *EdgeFilterUserCfg)
{
    int retStat = HWI_EDGE_COMMAND_EXECUTED_SUCCESFULLY;

    const unsigned long outputBufferSize = (unsigned long)(EdgeFilterUserCfg->GetOutputImageHeight * gs_IntCfgStruct.OutputLineStride);

    if (hwiIsEdgeInUse() && b_EdgeCfgInitDone && b_EdgeCfgDone && b_EdgeOutputBuffCfgDone && b_EdgeFinished)
    {
        for( unsigned int pl = 0; pl < EdgeFilterUserCfg->NumberOfOutputPlanes; pl++)
        {
            if (retStat == HWI_EDGE_COMMAND_EXECUTED_SUCCESFULLY)
            {
                switch(EdgeFilterUserCfg->EdgeOpOutputMode)
                {
                    case EDGE_OUTPUT_ORIENT_8BIT:
                    {
                        switch(EdgeFilterUserCfg->e_OutputBufferType)
                        {
                            case UNSIGNED_INT_8_BIT:
                            {
                                if (edgeApi_outbuf_ORIENT8_8BIT[pl].get() == NULL)
                                {
                                    retStat = HWI_EDGE_8BIT_INTERNAL_OUTPUT_BUFFER_FOR_ORIENT_8BIT_IS_NULL_ERROR;
                                }
                                else
                                {
                                    if (EdgeFilterUserCfg->outputBuffer[pl] == NULL)
                                    {
                                        EdgeFilterUserCfg->outputBuffer[pl] = malloc(outputBufferSize);
                                    }
                                    memcpy(EdgeFilterUserCfg->outputBuffer[pl], edgeApi_outbuf_ORIENT8_8BIT[pl].get(), outputBufferSize);
                                }
                                break;
                            }
                            case UNSIGNED_INT_16_BIT:
                            {
                                if (edgeApi_outbuf_ORIENT8_16BIT[pl].get() == NULL)
                                {
                                    retStat = HWI_EDGE_16BIT_INTERNAL_OUTPUT_BUFFER_FOR_ORIENT_8BIT_IS_NULL_ERROR;
                                }
                                else
                                {
                                    if (EdgeFilterUserCfg->outputBuffer[pl] == NULL)
                                    {
                                        EdgeFilterUserCfg->outputBuffer[pl] = malloc(outputBufferSize);
                                    }
                                    memcpy(EdgeFilterUserCfg->outputBuffer[pl], edgeApi_outbuf_ORIENT8_16BIT[pl].get(), outputBufferSize);
                                }
                                break;
                            }
                            case UNSIGNED_INT_32_BIT:
                            case UNSIGNED_INT_64_BIT:
                            case FLOATING_POINT_FP16_BIT:
                            case UNDEFINED_TYPE:
                            default:
                            {
                                retStat = HWI_EDGE_OUTPUT_BUFFER_TYPE_INCORECTLY_SET_FOR_ORIENT_8BIT_CFG_ERROR;
                                break;
                            }
                        }
                        break;
                    }
                    case EDGE_OUTPUT_SCALED_MAGN_8BIT:
                    {
                        switch(EdgeFilterUserCfg->e_OutputBufferType)
                        {
                            case UNSIGNED_INT_8_BIT:
                            {
                                if (edgeApi_outbuf_SCALED_MAGN_8BIT[pl].get() == NULL)
                                {
                                    retStat = HWI_EDGE_8BIT_INTERNAL_OUTPUT_BUFFER_FOR_SCALED_MAGN_8BIT_IS_NULL_ERROR;
                                }
                                else
                                {
                                    if (EdgeFilterUserCfg->outputBuffer[pl] == NULL)
                                    {
                                        EdgeFilterUserCfg->outputBuffer[pl] = malloc(outputBufferSize);
                                    }
                                    memcpy(EdgeFilterUserCfg->outputBuffer[pl], edgeApi_outbuf_SCALED_MAGN_8BIT[pl].get(), outputBufferSize);
                                }
                                break;
                            }
                            case UNSIGNED_INT_16_BIT:
                            case UNSIGNED_INT_32_BIT:
                            case UNSIGNED_INT_64_BIT:
                            case FLOATING_POINT_FP16_BIT:
                            case UNDEFINED_TYPE:
                            default:
                            {
                                retStat = HWI_EDGE_OUTPUT_BUFFER_TYPE_INCORECTLY_SET_FOR_SCAL_MAGN_8BIT_CFG_ERROR;
                                break;
                            }
                        }
                        break;
                    }
                    case EDGE_OUTPUT_SCALED_MAGN_16BIT:
                    {
                        switch(EdgeFilterUserCfg->e_OutputBufferType)
                        {
                            case UNSIGNED_INT_16_BIT:
                            {
                                if (edgeApi_outbuf_SCALED_MAGN_16BIT[pl].get() == NULL)
                                {
                                    retStat = HWI_EDGE_16BIT_INTERNAL_OUTPUT_BUFFER_FOR_SCALED_MAGN_16BIT_IS_NULL_ERROR;
                                }
                                else
                                {
                                    if (EdgeFilterUserCfg->outputBuffer[pl] == NULL)
                                    {
                                        EdgeFilterUserCfg->outputBuffer[pl] = malloc(outputBufferSize);
                                    }
                                    memcpy(EdgeFilterUserCfg->outputBuffer[pl], edgeApi_outbuf_SCALED_MAGN_16BIT[pl].get(), outputBufferSize);
                                }
                                break;
                            }
                            case UNSIGNED_INT_8_BIT:
                            case UNSIGNED_INT_32_BIT:
                            case UNSIGNED_INT_64_BIT:
                            case FLOATING_POINT_FP16_BIT:
                            case UNDEFINED_TYPE:
                            default:
                            {
                                retStat = HWI_EDGE_OUTPUT_BUFFER_TYPE_INCORECTLY_SET_FOR_SCAL_MAGN_16BIT_CFG_ERROR;
                                break;
                            }
                        }
                        break;
                    }
                    case EDGE_OUTPUT_MAGN_ORIENT_16BIT:
                    {
                        switch(EdgeFilterUserCfg->e_OutputBufferType)
                        {
                            case UNSIGNED_INT_16_BIT:
                            {
                                if (edgeApi_outbuf_MAGN_ORIENT_16BIT[pl].get() == NULL)
                                {
                                    retStat = HWI_EDGE_16BIT_INTERNAL_OUTPUT_BUFFER_FOR_MAGN_ORIENT_16BIT_IS_NULL_ERROR;
                                }
                                else
                                {
                                    if (EdgeFilterUserCfg->outputBuffer[pl] == NULL)
                                    {
                                        EdgeFilterUserCfg->outputBuffer[pl] = malloc(outputBufferSize);
                                    }
                                    memcpy(EdgeFilterUserCfg->outputBuffer[pl], edgeApi_outbuf_MAGN_ORIENT_16BIT[pl].get(), outputBufferSize);
                                }
                                break;
                            }
                            case UNSIGNED_INT_8_BIT:
                            case UNSIGNED_INT_32_BIT:
                            case UNSIGNED_INT_64_BIT:
                            case FLOATING_POINT_FP16_BIT:
                            case UNDEFINED_TYPE:
                            default:
                            {
                                retStat = HWI_EDGE_OUTPUT_BUFFER_TYPE_INCORECTLY_SET_FOR_MAGN_ORIENT_16BIT_CFG_ERROR;
                                break;
                            }
                        }
                        break;
                    }
                    case EDGE_OUTPUT_SCALED_GRADIENTS_16BIT:
                    {
                        switch(EdgeFilterUserCfg->e_OutputBufferType)
                        {
                            case UNSIGNED_INT_16_BIT:
                            {
                                if (edgeApi_outbuf_SCALED_GRADIENTS_16BIT[pl].get() == NULL)
                                {
                                    retStat = HWI_EDGE_16BIT_INTERNAL_OUTPUT_BUFFER_FOR_MAGN_SCALED_GRADIENTS_16BIT_IS_NULL_ERROR;
                                }
                                else
                                {
                                    if (EdgeFilterUserCfg->outputBuffer[pl] == NULL)
                                    {
                                        EdgeFilterUserCfg->outputBuffer[pl] = malloc(outputBufferSize);
                                    }
                                    memcpy(EdgeFilterUserCfg->outputBuffer[pl], edgeApi_outbuf_SCALED_GRADIENTS_16BIT[pl].get(), outputBufferSize);
                                }
                                break;
                            }
                            case UNSIGNED_INT_8_BIT:
                            case UNSIGNED_INT_32_BIT:
                            case UNSIGNED_INT_64_BIT:
                            case FLOATING_POINT_FP16_BIT:
                            case UNDEFINED_TYPE:
                            default:
                            {
                                retStat = HWI_EDGE_OUTPUT_BUFFER_TYPE_INCORECTLY_SET_FOR_SCALED_GRADIENTS_16BIT_CFG_ERROR;
                                break;
                            }
                        }
                        break;
                    }
                    case EDGE_OUTPUT_PLANAR_GRADS_AND_MAGN:
                    {
                        switch(EdgeFilterUserCfg->e_OutputBufferType)
                        {
                            case UNSIGNED_INT_16_BIT:
                            {
                                if (edgeApi_outbuf_PLANAR_GRADS_AND_MAGN[pl].get() == NULL)
                                {
                                    retStat = HWI_EDGE_16BIT_INTERNAL_OUTPUT_BUFFER_FOR_PLANAR_GRADS_AND_MAGN_IS_NULL_ERROR;
                                }
                                else
                                {
                                    if (pl < EdgeFilterUserCfg->NumberOfInputPlanes)
                                    {
                                        if (EdgeFilterUserCfg->outputBuffer[pl] == NULL)
                                        {
                                            EdgeFilterUserCfg->outputBuffer[pl] = malloc(outputBufferSize);
                                        }
                                        memcpy(EdgeFilterUserCfg->outputBuffer[pl], edgeApi_outbuf_PLANAR_GRADS_AND_MAGN[pl].get(), outputBufferSize);
                                    }
                                    else
                                    {
                                        if (EdgeFilterUserCfg->outputBuffer[pl] == NULL)
                                        {
                                            EdgeFilterUserCfg->outputBuffer[pl] = malloc(outputBufferSize);
                                        }
                                        memcpy(EdgeFilterUserCfg->outputBuffer[pl], edgeApi_outbuf_PLANAR_GRADS_AND_MAGN[pl].get(), outputBufferSize);
                                    }
                                }
                                break;
                            }
                            case UNSIGNED_INT_8_BIT:
                            case UNSIGNED_INT_32_BIT:
                            case UNSIGNED_INT_64_BIT:
                            case FLOATING_POINT_FP16_BIT:
                            case UNDEFINED_TYPE:
                            default:
                            {
                                retStat = HWI_EDGE_OUTPUT_BUFFER_TYPE_INCORECTLY_SET_FOR_PLANAR_GRADS_AND_MAGN_CFG_ERROR;
                                break;
                            }
                        }
                        break;
                    }
                    case EDGE_OUTPUT_SCALED_GRADIENTS_32BIT:
                    {
                        switch(EdgeFilterUserCfg->e_OutputBufferType)
                        {
                            case UNSIGNED_INT_32_BIT:
                            {
                                if (edgeApi_outbuf_SCALED_GRADIENTS_32BIT[pl].get() == NULL)
                                {
                                    retStat = HWI_EDGE_16BIT_INTERNAL_OUTPUT_BUFFER_FOR_SCALED_GRADIENTS_32BIT_IS_NULL_ERROR;
                                }
                                else
                                {
                                    if (EdgeFilterUserCfg->outputBuffer[pl] == NULL)
                                    {
                                        EdgeFilterUserCfg->outputBuffer[pl] = malloc(outputBufferSize);
                                    }
                                    memcpy(EdgeFilterUserCfg->outputBuffer[pl], edgeApi_outbuf_SCALED_GRADIENTS_32BIT[pl].get(), outputBufferSize);
                                }
                                break;
                            }
                            case UNSIGNED_INT_8_BIT:
                            case UNSIGNED_INT_16_BIT:
                            case UNSIGNED_INT_64_BIT:
                            case FLOATING_POINT_FP16_BIT:
                            case UNDEFINED_TYPE:
                            default:
                            {
                                retStat = HWI_EDGE_OUTPUT_BUFFER_TYPE_INCORECTLY_SET_FOR_PLANAR_GRADS_AND_MAGN_CFG_ERROR;
                                break;
                            }
                        }
                        break;
                    }
                    case EDGE_OUTPUT_HOG_MODE:
                    {
                        switch(EdgeFilterUserCfg->e_OutputBufferType)
                        {
                            case FLOATING_POINT_FP16_BIT:
                            {
                                if (edgeApi_outbuf_HOG_MODE[pl].get() == NULL)
                                {
                                    retStat = HWI_EDGE_16BIT_INTERNAL_OUTPUT_BUFFER_FOR_HOG_MODE_IS_NULL_ERROR;
                                }
                                else
                                {
                                    if (EdgeFilterUserCfg->outputBuffer[pl] == NULL)
                                    {
                                        EdgeFilterUserCfg->outputBuffer[pl] = malloc(outputBufferSize);
                                    }
                                    memcpy(EdgeFilterUserCfg->outputBuffer[pl], edgeApi_outbuf_HOG_MODE[pl].get(), outputBufferSize);
                                }
                                break;
                            }
                            case UNSIGNED_INT_8_BIT:
                            case UNSIGNED_INT_16_BIT:
                            case UNSIGNED_INT_32_BIT:
                            case UNSIGNED_INT_64_BIT:
                            case UNDEFINED_TYPE:
                            default:
                            {
                                retStat = HWI_EDGE_OUTPUT_BUFFER_TYPE_INCORECTLY_SET_FOR_PLANAR_GRADS_AND_MAGN_CFG_ERROR;
                                break;
                            }
                        }
                        break;
                    }
                    case EDGE_OUTPUT_DOMINANT_HIST_BIN:
                    {
                        switch(EdgeFilterUserCfg->e_OutputBufferType)
                        {
                            case UNSIGNED_INT_32_BIT:
                            {
                                if (edgeApi_outbuf_DOMINANT_HIST_BIN[pl].get() == NULL)
                                {
                                    retStat = HWI_EDGE_16BIT_INTERNAL_OUTPUT_BUFFER_FOR_DOMINANT_HIST_BIN_IS_NULL_ERROR;
                                }
                                else
                                {
                                    if (EdgeFilterUserCfg->outputBuffer[pl] == NULL)
                                    {
                                        EdgeFilterUserCfg->outputBuffer[pl] = malloc(outputBufferSize);
                                    }
                                    memcpy(EdgeFilterUserCfg->outputBuffer[pl], edgeApi_outbuf_DOMINANT_HIST_BIN[pl].get(), outputBufferSize);
                                }
                                break;
                            }
                            case UNSIGNED_INT_8_BIT:
                            case UNSIGNED_INT_16_BIT:
                            case UNSIGNED_INT_64_BIT:
                            case FLOATING_POINT_FP16_BIT:
                            case UNDEFINED_TYPE:
                            default:
                            {
                                retStat = HWI_EDGE_OUTPUT_BUFFER_TYPE_INCORECTLY_SET_FOR_DOMINANT_HIST_BIN_CFG_ERROR;
                                break;
                            }
                        }
                        break;
                    } // case EDGE_OUTPUT_DOMINANT_HIST_BIN:
                    default:
                    retStat = HWI_EDGE_OUTPUT_BUFFER_INIT_FAIL;
                } //switch(EdgeFilterUserCfg->EdgeOpOutputMode)
            } // if (retStat == HWI_EDGE_COMMAND_EXECUTED_SUCCESFULLY)
        } // for( unsigned int pl = 0; pl < EdgeFilterUserCfg->NumberOfOutputPlanes; pl++)
    } //if (hwiIsEdgeInUse() && b_EdgeCfgInitDone && b_EdgeCfgDone && b_EdgeOutputBuffCfgDone)
    else
    {
        retStat = HWI_EDGE_FILTER_ALREADY_IN_USE;
    }

    return retStat;
}


static int hwiEdgeGetInputStride(hwEdgePcModelCfgTable *EdgeFilterUserCfg)
{
    int retStat = HWI_EDGE_COMMAND_EXECUTED_SUCCESFULLY;

    switch(EdgeFilterUserCfg->e_InputBufferType)
    {
        case UNSIGNED_INT_8_BIT:
        {
            gs_IntCfgStruct.InputLineStride = EdgeFilterUserCfg->SetInputImageWidth;
            break;
        }
        case UNSIGNED_INT_16_BIT:
        {
            gs_IntCfgStruct.InputLineStride = EdgeFilterUserCfg->SetInputImageWidth * sizeof(uint16_t);
            break;
        }
        case UNSIGNED_INT_32_BIT:
        {
            gs_IntCfgStruct.InputLineStride = EdgeFilterUserCfg->SetInputImageWidth * sizeof(uint32_t);
            break;
        }
        case UNSIGNED_INT_64_BIT:
        {
            gs_IntCfgStruct.InputLineStride = EdgeFilterUserCfg->SetInputImageWidth * sizeof(uint64_t);
            break;
        }
        case FLOATING_POINT_FP16_BIT:
        case UNDEFINED_TYPE:
        default:
        {
            retStat = HWI_EDGE_INPUT_BUFFER_TYPE_INCORECTLY_SET_FOR_U8_GRAD_INPUT_CFG_ERROR;
            break;
        }
    }
    return retStat;
}

static int hwiEdgeGetOutputStride(hwEdgePcModelCfgTable *EdgeFilterUserCfg)
{
    int retStat = HWI_EDGE_COMMAND_EXECUTED_SUCCESFULLY;

    switch(EdgeFilterUserCfg->e_OutputBufferType)
    {
        case UNSIGNED_INT_8_BIT:
        {
            gs_IntCfgStruct.OutputLineStride = EdgeFilterUserCfg->GetOutputImageWidth;
            break;
        }
        case UNSIGNED_INT_16_BIT:
        {
            gs_IntCfgStruct.OutputLineStride = EdgeFilterUserCfg->GetOutputImageWidth * sizeof(uint16_t);
            break;
        }
        case UNSIGNED_INT_32_BIT:
        {
            gs_IntCfgStruct.OutputLineStride = EdgeFilterUserCfg->GetOutputImageWidth * sizeof(uint32_t);
            break;
        }
        case UNSIGNED_INT_64_BIT:
        {
            gs_IntCfgStruct.OutputLineStride = EdgeFilterUserCfg->GetOutputImageWidth * sizeof(uint64_t);
            break;
        }
        case FLOATING_POINT_FP16_BIT:
        {
            gs_IntCfgStruct.OutputLineStride = EdgeFilterUserCfg->GetOutputImageWidth * sizeof(fp16);
            break;
        }
        case UNDEFINED_TYPE:
        default:
        {
            retStat = HWI_EDGE_OUTPUT_BUFFER_TYPE_INCORECTLY_SET_FOR_DOMINANT_HIST_BIN_CFG_ERROR;
            break;
        }
    }
    return retStat;
}

// Function resets pcmodel status variables and allocates memory for the configuration structure
int hwiEdgeGetInitTheCfgStruct(hwEdgePcModelCfgTable **EdgeFilterUserCfg)
{
    int retStat = HWI_EDGE_COMMAND_EXECUTED_SUCCESFULLY;

    if (!hwiIsEdgeInUse())
    {
        b_EdgeModelInUse = false;
        b_EdgeCfgDone = false;
        b_EdgeStarted = false;
        b_EdgeFinished = false;
        b_EdgeOutputInitDone = false;
        b_EdgeOutputInitDone = false;

        // if configuration pointer is null, allocate memory for it.
        if (NULL == *EdgeFilterUserCfg)
        {
            // allocate new data
            *EdgeFilterUserCfg = (hwEdgePcModelCfgTable *) malloc(sizeof(hwEdgePcModelCfgTable));
        }

        // init struct variable values.
        memset(*EdgeFilterUserCfg, 0xCD, sizeof(hwEdgePcModelCfgTable));

        // init internal cfg structure
        gs_IntCfgStruct.InputKernelSize = 4;
        gs_IntCfgStruct.OutputKernelSize = 2;
        gs_IntCfgStruct.OutputLineStride = 0;
        gs_IntCfgStruct.InputLineStride = 0;

        (*EdgeFilterUserCfg)->THETA_OVX_enabled = false;

        (*EdgeFilterUserCfg)->e_InputBufferType = UNDEFINED_TYPE;
        (*EdgeFilterUserCfg)->e_OutputBufferType = UNDEFINED_TYPE;

        for(int pl=0; pl < MAX_INPUT_PLANE_NO; pl++)
        {
            (*EdgeFilterUserCfg)->inputBuffer[pl] = NULL;
        }
        for(int pl=0; pl < MAX_OUTPUT_PLANE_NO; pl++)
        {
            (*EdgeFilterUserCfg)->outputBuffer[pl] = NULL;
        }

        //TODO:   pointers need to be set to null ?

        b_EdgeCfgInitDone = true;
    }
    else
    {
        retStat = HWI_EDGE_FILTER_ALREADY_IN_USE;
    }

    return retStat;
}


int hwiEdgeCfgAndAllocatePcModel( hwEdgePcModelCfgTable *EdgeFilterUserCfg)
{
    int retVal = HWI_EDGE_COMMAND_EXECUTED_SUCCESFULLY;
    ApbSlaveInterface *p_sippHwAcc;

    if (!hwiIsEdgeInUse() && b_EdgeCfgInitDone)
    {
        if (EdgeFilterUserCfg->EdgeOpOutputMode == EDGE_OUTPUT_DOMINANT_HIST_BIN)
        {
            EdgeFilterUserCfg->GetOutputImageWidth = EdgeFilterUserCfg->SetInputImageWidth / HOG_KERNEL_SIZE;
            EdgeFilterUserCfg->GetOutputImageHeight = EdgeFilterUserCfg->SetInputImageHeight / HOG_KERNEL_SIZE;
        }
        else
        if (EdgeFilterUserCfg->EdgeOpOutputMode == EDGE_OUTPUT_HOG_MODE)
        {
            EdgeFilterUserCfg->GetOutputImageWidth = EdgeFilterUserCfg->SetInputImageWidth / HOG_KERNEL_SIZE * 16;
            EdgeFilterUserCfg->GetOutputImageHeight = EdgeFilterUserCfg->SetInputImageHeight / HOG_KERNEL_SIZE;
        }
        else
        {
            EdgeFilterUserCfg->GetOutputImageWidth = EdgeFilterUserCfg->SetInputImageWidth;
            EdgeFilterUserCfg->GetOutputImageHeight = EdgeFilterUserCfg->SetInputImageHeight;
        }

        if (retVal == HWI_EDGE_COMMAND_EXECUTED_SUCCESFULLY)
        {
            // create filter instance. and allocate memory for it, return this pointer in a parameter.
            edgeApi_sippHwAcc =  SippUnitTestConfigurer::ApbSlaveInterface_sp(new SippHwM2Factory(IsrHandler::separateRegIsr));
            SippUnitTestConfigurer::SetApbSlaveInterface(edgeApi_sippHwAcc);

            p_sippHwAcc = edgeApi_sippHwAcc.get();

            // Set verbosity level for the filter in this unit test
            if(b_VerbosityEnabled) p_sippHwAcc->SetVerbose(idMask);

            // Establish test case interrupts, interrupt registers and name
            IsrHandler::SetTestInfo(irqMask, aIdMask, tcstr);
            IsrHandler::SetIrqRegs(SIPP_EDGE_OP_INT_STATUS_ADR,
                                SIPP_EDGE_OP_INT_ENABLE_ADR,
                                SIPP_EDGE_OP_INT_CLEAR_ADR);

            // Enable ibfl_inc, obfl_dec and eof irqs
            IsrHandler::EnableSeparateIrqs(p_sippHwAcc);

            p_sippHwAcc->ApbWrite(SIPP_EDGE_OP_ENABLE_ADR, ENABLED);

            // Get the image stride based on the input and output buffer configuration
            hwiEdgeGetInputStride(EdgeFilterUserCfg);
            hwiEdgeGetOutputStride(EdgeFilterUserCfg);

            // Pipe specific configuration
            hwiConfigurePipe(p_sippHwAcc, EdgeFilterUserCfg);

            // Thread-safe mechanism for preventing
            // I/O buffer over/underflow, respectively.
            // Initially, all the buffer are empty
            edgeApi_inputSem[0] = Semaphore_sp(new Semaphore(gs_IntCfgStruct.InputKernelSize, gs_IntCfgStruct.InputKernelSize));
            edgeApi_outputSem[0] = Semaphore_sp(new Semaphore(0, gs_IntCfgStruct.OutputKernelSize));

            IsrHandler:: SetInputSemaphore( edgeApi_inputSem[0], 0);
            IsrHandler::SetOutputSemaphore(edgeApi_outputSem[0], 0);

            SippUnitTestConfigurer::addr_idx_t  edgeApi_inAddrV( edgeApi_inAddr,  edgeApi_inAddr + sizeof( edgeApi_inAddr)/sizeof(* edgeApi_inAddr));
            SippUnitTestConfigurer::addr_idx_t edgeApi_outAddrV(edgeApi_outAddr, edgeApi_outAddr + sizeof(edgeApi_outAddr)/sizeof(* edgeApi_outAddr));

            // Buffer allocation and configuration
            switch(EdgeFilterUserCfg->e_InputBufferType)
            {
                case UNSIGNED_INT_8_BIT:
                {
                    edgeApi_ibuf_8 = new SippUnitTestBufferAlloc<uint8_t>(DEF_SLICE_SIZE * CMX_NSLICES);
                    edgeApi_inputCfg[0] = SippUnitTestConfigurer(
                                                                 *edgeApi_ibuf_8,
                                                                 gs_IntCfgStruct.InputKernelSize,
                                                                 EdgeFilterUserCfg->SetInputImageWidth,
                                                                 EdgeFilterUserCfg->NumberOfInputPlanes ,
                                                                 sizeof(uint8_t),  edgeApi_inAddrV,  edgeApi_inputSem[0]);
                    break;
                }
                case UNSIGNED_INT_16_BIT:
                {
                    edgeApi_ibuf_16 = new SippUnitTestBufferAlloc<uint16_t>(DEF_SLICE_SIZE * CMX_NSLICES);
                    edgeApi_inputCfg[0] = SippUnitTestConfigurer(
                                                                 *edgeApi_ibuf_16,
                                                                 gs_IntCfgStruct.InputKernelSize,
                                                                 EdgeFilterUserCfg->SetInputImageWidth,
                                                                 EdgeFilterUserCfg->NumberOfInputPlanes ,
                                                                 sizeof(uint16_t),  edgeApi_inAddrV,  edgeApi_inputSem[0]);
                    break;
                }
                case UNSIGNED_INT_32_BIT:
                {
                    edgeApi_ibuf_32 = new SippUnitTestBufferAlloc<uint32_t>(DEF_SLICE_SIZE * CMX_NSLICES);
                    edgeApi_inputCfg[0] = SippUnitTestConfigurer(
                                                                 *edgeApi_ibuf_32,
                                                                 gs_IntCfgStruct.InputKernelSize,
                                                                 EdgeFilterUserCfg->SetInputImageWidth,
                                                                 EdgeFilterUserCfg->NumberOfInputPlanes ,
                                                                 sizeof(uint32_t),  edgeApi_inAddrV,  edgeApi_inputSem[0]);
                    break;
                }
                case FLOATING_POINT_FP16_BIT:
                case UNSIGNED_INT_64_BIT:
                case UNDEFINED_TYPE:
                default:
                    retVal = HWI_EDGE_ERROR_FILTER_NOT_INITIALIZED;
            }


            switch(EdgeFilterUserCfg->e_OutputBufferType)
            {
                case UNSIGNED_INT_8_BIT:
                {
                    edgeApi_obuf_8 = new SippUnitTestBufferAlloc<uint8_t>(DEF_SLICE_SIZE * CMX_NSLICES);
                    edgeApi_outputCfg[0] = SippUnitTestConfigurer(
                                                                  *edgeApi_obuf_8,
                                                                  gs_IntCfgStruct.OutputKernelSize,
                                                                  EdgeFilterUserCfg->GetOutputImageWidth,
                                                                  EdgeFilterUserCfg->NumberOfOutputPlanes,
                                                                  sizeof(uint8_t), edgeApi_outAddrV, edgeApi_outputSem[0]);
                    break;
                }
                case UNSIGNED_INT_16_BIT:
                {
                    edgeApi_obuf_16 = new SippUnitTestBufferAlloc<uint16_t>(DEF_SLICE_SIZE * CMX_NSLICES);
                    edgeApi_outputCfg[0] = SippUnitTestConfigurer(
                                                                  *edgeApi_obuf_16,
                                                                  gs_IntCfgStruct.OutputKernelSize,
                                                                  EdgeFilterUserCfg->GetOutputImageWidth,
                                                                  EdgeFilterUserCfg->NumberOfOutputPlanes,
                                                                  sizeof(uint16_t), edgeApi_outAddrV, edgeApi_outputSem[0]);
                    break;
                }
                case UNSIGNED_INT_32_BIT:
                {
                    edgeApi_obuf_32 = new SippUnitTestBufferAlloc<uint32_t>(DEF_SLICE_SIZE * CMX_NSLICES);
                    edgeApi_outputCfg[0] = SippUnitTestConfigurer(
                                                                  *edgeApi_obuf_32,
                                                                  gs_IntCfgStruct.OutputKernelSize,
                                                                  EdgeFilterUserCfg->GetOutputImageWidth,
                                                                  EdgeFilterUserCfg->NumberOfOutputPlanes,
                                                                  sizeof(uint32_t), edgeApi_outAddrV, edgeApi_outputSem[0]);
                    break;
                }
                case FLOATING_POINT_FP16_BIT:
                {
                    edgeApi_obuf_fp16 = new SippUnitTestBufferAlloc<fp16>(DEF_SLICE_SIZE * CMX_NSLICES);
                    edgeApi_outputCfg[0] = SippUnitTestConfigurer(
                                                                  *edgeApi_obuf_fp16,
                                                                  gs_IntCfgStruct.OutputKernelSize,
                                                                  EdgeFilterUserCfg->GetOutputImageWidth,
                                                                  EdgeFilterUserCfg->NumberOfOutputPlanes,
                                                                  sizeof(fp16), edgeApi_outAddrV, edgeApi_outputSem[0]);
                    break;
                }
                case UNSIGNED_INT_64_BIT:
                case UNDEFINED_TYPE:
                default:
                    retVal = HWI_EDGE_ERROR_FILTER_NOT_INITIALIZED;
            }

            edgeApi_inputCfg[0].SetInDimensions (EdgeFilterUserCfg->SetInputImageWidth, EdgeFilterUserCfg->SetInputImageHeight);
            edgeApi_outputCfg[0].SetOutDimensions(EdgeFilterUserCfg->GetOutputImageWidth, EdgeFilterUserCfg->GetOutputImageHeight);

            // See where the filter's left off from the last
            // run. This is performed here instead of in the
            // producer consumer themselves as the following
            // scenario could've happened:
            // - producer thread started, produced sufficient IBFL_INC events
            // - filter would have run, increased its ocbl
            // - consumer thread started, read erroneous ocbl that
            //   has just been updated (not the one from the last run)
            edgeApi_inputCfg[0].DetermineCurrentBufferIdx();
            edgeApi_outputCfg[0].DetermineCurrentBufferIdx();

            // Pipe specific configuration
            hwiConfigurePipe(p_sippHwAcc, EdgeFilterUserCfg);

            retVal = hwiEdgeConfigureInputBuffer(EdgeFilterUserCfg, &edgeApi_inputCfg[0]);
            if (HWI_EDGE_COMMAND_EXECUTED_SUCCESFULLY == retVal)
            {
                b_EdgeInputInitDone = true;
                retVal = hwiEdgeConfigureOutputBuffer(EdgeFilterUserCfg, &edgeApi_outputCfg[0]);
                if (HWI_EDGE_COMMAND_EXECUTED_SUCCESFULLY == retVal)
                {
                    b_EdgeOutputInitDone = true;
                    // Enable filters in this pipe
                    hwiSetEdgeInUse();

                }
            }
        }
    }
    else
    {
        retVal = HWI_EDGE_FILTER_CONFIGURATION_NOT_INITIALIZED;
    }

    return retVal;
}




int hwiEdgeStart(hwEdgePcModelCfgTable *EdgeFilterUserCfg)
{
    int retVal = HWI_EDGE_COMMAND_EXECUTED_SUCCESFULLY;

    if(hwiIsEdgeInUse() && b_EdgeOutputInitDone && b_EdgeOutputInitDone)
    {
        p_edgeApi_pthrd = new SippUnitTestProcess::thrd_t;
        p_edgeApi_cthrd = new SippUnitTestProcess::thrd_t;

        *p_edgeApi_pthrd = p_edgeApi_prod_proc->StartProcess();
        *p_edgeApi_cthrd = p_edgeApi_cons_proc->StartProcess();
        b_EdgeStarted = true;

        p_edgeApi_prod_proc->WaitForProcess(*p_edgeApi_pthrd);
        p_edgeApi_cons_proc->WaitForProcess(*p_edgeApi_cthrd);

        b_EdgeFinished = true;

        hwiEdgeCopyDataToOutputBuffer(EdgeFilterUserCfg);

        delete(p_edgeApi_pthrd);
        delete(p_edgeApi_cthrd);

        hwiClrEdgeInUse();
    }
    else
    {
        retVal = HWI_EDGE_ERROR_FILTER_NOT_INITIALIZED;
    }

    return retVal;
}


// Function resets pcmodel status variables and allocates memory for the configuration structure
int hwiEdgeCleanUp(hwEdgePcModelCfgTable **EdgeFilterUserCfg)
{
    if (!hwiIsEdgeInUse() && b_EdgeStarted && b_EdgeFinished)
    {
        for (unsigned int pl=0; pl < (*EdgeFilterUserCfg)->NumberOfInputPlanes; pl++)
        {
            // Input buffers for holding the image data
            free(edgeApi_inbuf_8normal[pl]);
            free(edgeApi_inbuf_16normal[pl]);
            free(edgeApi_inbuf_8Grad[pl]);
            free(edgeApi_inbuf_16Grad[pl]);
        }

        // pointer to producer and consumer threads
        delete(p_edgeApi_prod_proc);
        delete(p_edgeApi_cons_proc);

        // Pointer to the buffers
        delete(edgeApi_ibuf_8);
        delete(edgeApi_obuf_8);
        delete(edgeApi_ibuf_16);
        delete(edgeApi_obuf_16);
        delete(edgeApi_ibuf_32);
        delete(edgeApi_obuf_32);

        for (unsigned int pl=0; pl < (*EdgeFilterUserCfg)->NumberOfOutputPlanes; pl++)
        {
            free((*EdgeFilterUserCfg)->outputBuffer[pl]);
        }

        free((void *)(*EdgeFilterUserCfg));

        b_EdgeModelInUse = false;
        b_EdgeCfgDone = false;
        b_EdgeStarted = false;
        b_EdgeFinished = false;
        b_EdgeOutputInitDone = false;
        b_EdgeOutputInitDone = false;
        b_EdgeCfgInitDone = false;

        return 0;
    }

    return -1;
}
