// -----------------------------------------------------------------------------
// Copyright (C) 2016 Movidius Ltd. All rights reserved
//
// Company            Movidius
// Author             Titus BIRAU (titus.birau@movidius.com)
//
// Description        Stereo HW filter API source code
// -----------------------------------------------------------------------------

#include "hwStereoApi.h"

#include <StereoDisparitySoftwareModel.h>

#include <registersMyriad2.h>

#include <sippHwM2Factory.h>
#include <sippHwDefines.h>

#include <sippUnitTestProducer.h>
#include <sippUnitTestConsumer.h>
#include <sippUnitTestIsr.h>


// -----------------------------------------------------------------------------
//
//                    M A C R O S
//
// -----------------------------------------------------------------------------

// verbosity - update this in order to change verbosity
#define  VERBOSITY   (bool)false

// NULL pointer definition
#ifndef NULL
#define NULL                        ( (   void * ) 0 )
#endif

// used for configuration initialization
#define SCFG_PARAM__U8__NOT_CFGD    ( (  uint8_t ) 0xCD       )
#define SCFG_PARAM_U16__NOT_CFGD    ( ( uint16_t ) 0xCDCD     )
#define SCFG_PARAM_U32__NOT_CFGD    ( ( uint32_t ) 0xCDCDCDCD )

#define SCFG_PARAM__S8__NOT_CFGD    ( (   int8_t ) 0xCD       )
#define SCFG_PARAM_S16__NOT_CFGD    ( (  int16_t ) 0xCDCD     )
#define SCFG_PARAM_S32__NOT_CFGD    ( (  int32_t ) 0xCDCDCDCD )


// -----------------------------------------------------------------------------
//
//                    Stereo PC Model registers
//
// -----------------------------------------------------------------------------

// front-end of the mini-pipe
#define IBUF0_BASE_ADR          BUF_BASE_REG    (CV, STEREO, I, 0)
#define IBUF0_CFG_ADR           BUF_CFG_REG     (CV, STEREO, I, 0)
#define IBUF0_LS_ADR            BUF_LS_REG      (CV, STEREO, I, 0)
#define IBUF0_PS_ADR            BUF_PS_REG      (CV, STEREO, I, 0)
#define IBUF0_IR_ADR            BUF_IR_REG      (CV, STEREO, I, 0)
#define IBUF0_FC_ADR            BUF_FC_REG      (CV, STEREO, I, 0)
#define ICTX0_ADR               CTX_REG         (CV, STEREO, I, 0)
#define IBUF1_BASE_ADR          BUF_BASE_REG    (CV, STEREO, I, 1)
#define IBUF1_CFG_ADR           BUF_CFG_REG     (CV, STEREO, I, 1)
#define IBUF1_LS_ADR            BUF_LS_REG      (CV, STEREO, I, 1)
#define IBUF1_PS_ADR            BUF_PS_REG      (CV, STEREO, I, 1)
#define IBUF1_IR_ADR            BUF_IR_REG      (CV, STEREO, I, 1)
#define IBUF1_FC_ADR            BUF_FC_REG      (CV, STEREO, I, 1)
#define ICTX1_ADR               CTX_REG         (CV, STEREO, I, 1)

// back-end of the mini-pipe
#define OBUF0_BASE_ADR          BUF_BASE_REG    (CV, STEREO, O, 0)
#define OBUF0_CFG_ADR           BUF_CFG_REG     (CV, STEREO, O, 0)
#define OBUF0_LS_ADR            BUF_LS_REG      (CV, STEREO, O, 0)
#define OBUF0_PS_ADR            BUF_PS_REG      (CV, STEREO, O, 0)
#define OBUF0_IR_ADR            BUF_IR_REG      (CV, STEREO, O, 0)
#define OBUF0_FC_ADR            BUF_FC_REG      (CV, STEREO, O, 0)
#define OCTX0_ADR               CTX_REG         (CV, STEREO, O, 0)
#define OBUF1_BASE_ADR          BUF_BASE_REG    (CV, STEREO, O, 1)
#define OBUF1_CFG_ADR           BUF_CFG_REG     (CV, STEREO, O, 1)
#define OBUF1_LS_ADR            BUF_LS_REG      (CV, STEREO, O, 1)
#define OBUF1_PS_ADR            BUF_PS_REG      (CV, STEREO, O, 1)
#define OBUF1_IR_ADR            BUF_IR_REG      (CV, STEREO, O, 1)
#define OBUF1_FC_ADR            BUF_FC_REG      (CV, STEREO, O, 1)
#define OCTX1_ADR               CTX_REG         (CV, STEREO, O, 1)

#define INPUT0_BUFFER_LINES     STEREO_V_KERNEL_SIZE + 1  // R & L images
#define INPUT1_BUFFER_LINES     1                         // Pre-Computed Costs
#define OUTPUT_BUFFER_LINES     2


// -----------------------------------------------------------------------------
//
//                    Sipp Unit Test Configuration
//
// -----------------------------------------------------------------------------

static ApbSlaveInterface *          apbSlave;

// These objects are responsible with:
// - configuring the I/O buffers
// - configuring image resolution
// They will be used within the producer/consumer threads.
static SippUnitTestConfigurer       inputCfg [IsrHandler::MAX_IBUFS];
static SippUnitTestConfigurer       outputCfg[IsrHandler::MAX_OBUFS];

static SippUnitTestProcess *        prod0;      // R & L images
static SippUnitTestProcess *        prod1;      // Pre-Computed Costs
static SippUnitTestProcess *        cons0;      // CT Descriptors OR DebugCostDump
static SippUnitTestProcess *        cons1;      // Disparity

static SippUnitTestProcess::thrd_t  pthrd0;
static SippUnitTestProcess::thrd_t  pthrd1;
static SippUnitTestProcess::thrd_t  cthrd0;
static SippUnitTestProcess::thrd_t  cthrd1;


// establish test case interrupts, name and verbosity
static int                          irqMask = 0x3f;
static bool                         verbose = VERBOSITY;
static std::string                  tcstr("STEREO FILTER");


// -----------------------------------------------------------------------------
//
//                    INPUT & OUTPUT - DATA TYPES & BUFFERS
//
// -----------------------------------------------------------------------------

// Image Input Mode
typedef uint8_t                                     stereo__8BPP__itype          ;
typedef uint16_t                                    stereo__10BPP__itype         ;
// Pre-Computed Costs
typedef uint8_t                                     stereo__PreCCosts__itype     ;

// Output Mode
typedef uint32_t                                    stereo__CT_DESCR__otype      ;
typedef std::shared_ptr<stereo__CT_DESCR__otype>    stereo__CT_DESCR__otype_sp   ;
typedef uint8_t                                     stereo__DBG_COSTS__otype     ;
typedef std::shared_ptr<stereo__DBG_COSTS__otype>   stereo__DBG_COSTS__otype_sp  ;
typedef uint32_t                                    stereo__DSP_MAP__otype       ;
typedef std::shared_ptr<stereo__DSP_MAP__otype>     stereo__DSP_MAP__otype_sp    ;
typedef uint16_t                                    stereo__DSP_MC__otype        ;
typedef std::shared_ptr<stereo__DSP_MC__otype>      stereo__DSP_MC__otype_sp     ;
typedef uint8_t                                     stereo__DSP_CD__otype        ;
typedef std::shared_ptr<stereo__DSP_CD__otype>      stereo__DSP_CD__otype_sp     ;
typedef uint8_t                                     stereo__DSP_SINGLE__otype    ;
typedef std::shared_ptr<stereo__DSP_SINGLE__otype>  stereo__DSP_SINGLE__otype_sp ;
typedef uint32_t                                    stereo__DSP_COMB__otype      ;
typedef std::shared_ptr<stereo__DSP_COMB__otype>    stereo__DSP_COMB__otype_sp   ;

// INPUT BUFFERS
stereo__8BPP__itype      *                          inputBuffer__8BPP        [2] ;  // 2 planes for L & R images (greyscale)
stereo__10BPP__itype     *                          inputBuffer__10BPP       [2] ;  // 2 planes for L & R images (greyscale)
stereo__PreCCosts__itype *                          inputBuffer__PreCCosts   [1] ;  // 1 plane  for Pre-Computed Costs

// OUTPUT BUFFERS
stereo__CT_DESCR__otype_sp                          outputBuffer__CT_DESCR   [2] ;  // 2 planes for CT Descriptors (CT Descriptor per L/R image)
stereo__DBG_COSTS__otype_sp                         outputBuffer__DBG_COSTS  [1] ;  // 1 plane  for Debug-Cost-Dump
stereo__DSP_MAP__otype_sp                           outputBuffer__DSP_MAP    [1] ;  // 1 plane  for disparity
stereo__DSP_MC__otype_sp                            outputBuffer__DSP_MC     [1] ;  // 1 plane  for disparity
stereo__DSP_CD__otype_sp                            outputBuffer__DSP_CD     [1] ;  // 1 plane  for disparity
stereo__DSP_SINGLE__otype_sp                        outputBuffer__DSP_SINGLE [1] ;  // 1 plane  for disparity
stereo__DSP_COMB__otype_sp                          outputBuffer__DSP_COMB   [1] ;  // 1 plane  for disparity


// -----------------------------------------------------------------------------
//
//                    GLOBAL VARIABLES
//
// -----------------------------------------------------------------------------

// DATA TYPES

typedef enum {
    CFG__INPUT__8BPP  ,
    CFG__INPUT__10BPP
}   cfgInput_t;

typedef enum {
    CFG__OUTPUT__CT_DESCR   ,
    CFG__OUTPUT__DSP_MAP    ,
    CFG__OUTPUT__DSP_MC     ,
    CFG__OUTPUT__DSP_CD     ,
    CFG__OUTPUT__DSP_SINGLE ,
    CFG__OUTPUT__DSP_COMB
}   cfgOutput_t;

typedef enum {
    HW_API__NONE   ,
    HW_API__INIT   ,
    HW_API__CONFIG ,
    HW_API__START  ,
    HW_API__WAIT
}   hwApiCalls_t;

// VARIABLES

// image parameters
static uint32_t        stereoImageWidth              ;
static uint32_t        stereoImageHeight             ;
static uint32_t        stereoImageStride             ;

// input buffers
static void     *      stereoInputLBuffer            ;
static void     *      stereoInputRBuffer            ;
static uint8_t  *      stereoInputPreCCostsBuffer    ;

// output buffers
static uint32_t *      stereoOutputLCTDescrBuffer    ;
static uint32_t *      stereoOutputRCTDescrBuffer    ;
static uint8_t  *      stereoOutputDebugCostsBuffer  ;
static void     *      stereoOutputDisparityBuffer   ;

// Force Descriptor Dump was/wasn't requested
static bool            stereoForceDescrDumpRequested ;
// Debug Costs Dump was/wasn't' requested
static bool            stereoDebugCostDumpRequested  ;
// Pre-Computed Costs are required by Stereo HW filter
static bool            stereoPreCCostsRequired       ;
// store configured number of disparities
static uint32_t        stereoDisparities             ;

// used to create producers according to input configuration
static cfgInput_t       configInputType              ;
// used to create consumers based on expected output configuration
static cfgOutput_t      configOutputType             ;

// used for API calling sequence error handler
static hwApiCalls_t     lastApiCalled = HW_API__NONE ;


// -----------------------------------------------------------------------------
//
//                    F U N C T I O N S
//
// -----------------------------------------------------------------------------


// -----------------------------------------------------------------------------
//
// API Function       hwiStereoInit
//
// Description        initializes Stereo Hw filter configuration parameters
//                    to default OR not configured values
//
// Parameters         stereoConfig - contains address of configuration
//                                   structure which need to be initialized
//
// Return             none
//
// Constraints        none
//
// -----------------------------------------------------------------------------
void hwiStereoInit( hwiStereoConfig_t * stereoConfig )
{
    // Register: STEREO_CFG
    stereoConfig->operationMode          = SCFG_MODE__CT                  ; // default value
    stereoConfig->imageInputMode         = SCFG_IN_M__8BPP                ; // default value
    stereoConfig->outputMode             = SCFG_OUT_M__NOT_CFGD           ;
    stereoConfig->ctKernelSize           = SCFG_CT_KER__5X5               ; // default value
    stereoConfig->ctDescriptorType       = SCFG_CT_TYPE__THRESHOLD        ; // default value
    stereoConfig->disparities            = SCFG_DSP_WD__64                ; // default value
    stereoConfig->ctDescriptorFormat     = SCFG_CT_FORMAT__PV8_CT24       ; // default value
    stereoConfig->aggDivFactor           = SCFG_DIV_FACTOR__NOT_CFGD      ;
    stereoConfig->compandingEnable       = SCFG_CME__DISABLED             ; // default value
    stereoConfig->forceDescrDump         = SCFG_CTD__DISABLED             ; // default value
    stereoConfig->debugCostDump          = SCFG_DCD__DISABLED             ; // default value
    stereoConfig->invalidDisp            = SCFG_PARAM__U8__NOT_CFGD       ;

    // Register: STEREO_PARAMS
    stereoConfig->cmAlfa                 = SCFG_PARAM__U8__NOT_CFGD       ;
    stereoConfig->cmBeta                 = SCFG_PARAM__U8__NOT_CFGD       ;
    stereoConfig->cmThreshold            = SCFG_PARAM__U8__NOT_CFGD       ;
    stereoConfig->ctThreshold            = SCFG_PARAM__U8__NOT_CFGD       ;
    stereoConfig->ratioThreshold         = SCFG_PARAM__U8__NOT_CFGD       ;

    // Register: STEREO_CTMASKx
    stereoConfig->ctMaskL                = SCFG_PARAM_U32__NOT_CFGD       ;
    stereoConfig->ctMaskH                = SCFG_PARAM_U32__NOT_CFGD       ;

    // Register: STEREO_RWLUT_x
    stereoConfig->P1_H                   = SCFG_PARAM_S32__NOT_CFGD       ;
    stereoConfig->P2_H                   = SCFG_PARAM_S32__NOT_CFGD       ;
    stereoConfig->P1_V                   = SCFG_PARAM_S32__NOT_CFGD       ;
    stereoConfig->P2_V                   = SCFG_PARAM_S32__NOT_CFGD       ;

    // IMAGE PARAMETERS
    stereoConfig->imageWidth             = SCFG_PARAM_S32__NOT_CFGD       ;
    stereoConfig->imageHeight            = SCFG_PARAM_S32__NOT_CFGD       ;
    stereoConfig->imageStride            = SCFG_PARAM_S32__NOT_CFGD       ;

    // INPUT BUFFERS
    stereoConfig->inputLBuffer           = NULL                           ;
    stereoConfig->inputRBuffer           = NULL                           ;
    stereoConfig->inputPreCCostsBuffer   = NULL                           ;

    // OUTPUT BUFFERS
    stereoConfig->outputLCTDescrBuffer   = NULL                           ;
    stereoConfig->outputLCTDescrBuffer   = NULL                           ;
    stereoConfig->outputDebugCostsBuffer = NULL                           ;
    stereoConfig->outputDisparityBuffer  = NULL                           ;


    // save last valid API call for error handling
    lastApiCalled = HW_API__INIT;

    return;
}


// -----------------------------------------------------------------------------
//
// Local Function     checkStereoConfig
//
// Description        implements basic error handler for provided configuration
//                    parameters
//
// Parameters         stereoConfig - structure containing configuration
//                                   parameters that need to be checked
//
// Return             configuration error status (scfgStatus_t)
//
// Constraints        shall be called only from hwiStereoConfig() function
//
// -----------------------------------------------------------------------------
scfgStatus_t checkStereoConfig( hwiStereoConfig_t * stereoConfig )
{
    // Force Descriptor Dump      - will be taken into consideration only when:
    //                              OperationMode is different from SCFG_MODE__CT
    stereoForceDescrDumpRequested = false;
    // Debug Cost Dump            - will be taken into consideration only when:
    //                              OperationMode is different from SCFG_MODE__CT
    //                              AND
    //                              when ForceDescriptorDump was not requested
    stereoDebugCostDumpRequested  = false;
    // Pre-Computed Costs         - are used only when:
    //                              OperationMode is SCFG_MODE__SGMB
    stereoPreCCostsRequired       = false;

    // get/compute number of disparities based on provided configuration
    if  ( SCFG_DSP_WD__64 == stereoConfig->disparities )
        stereoDisparities = 64;
    else
        stereoDisparities = 96;

    // check image configuration
    if  (  ( SCFG_PARAM_U32__NOT_CFGD == stereoConfig->imageWidth  )
        || ( SCFG_PARAM_U32__NOT_CFGD == stereoConfig->imageHeight )
        || ( SCFG_PARAM_U32__NOT_CFGD == stereoConfig->imageStride ) )
        return SCFG_STS__IMAGE_PARAMS_NOT_CFGD;

    // check input buffers
    if  (  ( NULL == stereoConfig->inputLBuffer )
        || ( NULL == stereoConfig->inputRBuffer ) )
        return SCFG_STS__IMAGE_BUFS_NOT_CFGD;

    switch ( stereoConfig->operationMode )
    {
        case SCFG_MODE__CT:         // Census Transform only
            // check CT Descriptors buffers
            if  (  ( NULL == stereoConfig->outputLCTDescrBuffer )
                || ( NULL == stereoConfig->outputRCTDescrBuffer ) )
                return SCFG_STS__CTDESCR_BUFS_NOT_CFGD;
            break;

        case SCFG_MODE__SGMB:       // Cost Aggregation only
            // Pre-Computed Costs are needed by Stereo HW filter
            stereoPreCCostsRequired = true;
            // check Pre-Computed Costs buffer
            if  ( NULL == stereoConfig->inputPreCCostsBuffer )
                return SCFG_STS__PRECCOSTS_BUFS_NOT_CFGD;
            // check Disparity buffer
            if  ( NULL == stereoConfig->outputDisparityBuffer )
                return SCFG_STS__DISPARITY_BUFS_NOT_CFGD;
            // check Output Mode
            if  ( SCFG_OUT_M__NOT_CFGD == stereoConfig->outputMode )
                return SCFG_STS__OUT_M_NOT_CFGD;
            // check CM Parameters
            if  (  ( SCFG_PARAM__U8__NOT_CFGD == stereoConfig->cmAlfa      )
                || ( SCFG_PARAM__U8__NOT_CFGD == stereoConfig->cmBeta      )
                || ( SCFG_PARAM__U8__NOT_CFGD == stereoConfig->cmThreshold ) )
                return SCFG_STS__CM_PARAMS_NOT_CFGD; // TODO are these params needed in this SGMB mode ?
            // check RWLUT parameters (needed for LUT configuration)
            if  (  ( SCFG_PARAM_S32__NOT_CFGD == stereoConfig->P1_H )
                || ( SCFG_PARAM_S32__NOT_CFGD == stereoConfig->P2_H )
                || ( SCFG_PARAM_S32__NOT_CFGD == stereoConfig->P1_V )
                || ( SCFG_PARAM_S32__NOT_CFGD == stereoConfig->P2_V ) )
                return SCFG_STS__RWLUT_PARAMS_NOT_CFGD;
            // TODO is CA_DIV_FACTOR param needed in this SGMB mode ?
            break;

        case SCFG_MODE__CT_CM:      // Census Transform AND Cost Computation
            // check Disparity buffer
            if  ( NULL == stereoConfig->outputDisparityBuffer )
                return SCFG_STS__DISPARITY_BUFS_NOT_CFGD;
            // check Output Mode
            if  ( SCFG_OUT_M__NOT_CFGD == stereoConfig->outputMode )
                return SCFG_STS__OUT_M_NOT_CFGD;
            // check CM Parameters
            if  (  ( SCFG_PARAM__U8__NOT_CFGD == stereoConfig->cmAlfa      )
                || ( SCFG_PARAM__U8__NOT_CFGD == stereoConfig->cmBeta      )
                || ( SCFG_PARAM__U8__NOT_CFGD == stereoConfig->cmThreshold ) )
                return SCFG_STS__CM_PARAMS_NOT_CFGD;
            break;

        case SCFG_MODE__CT_CM_SGMB: // run full pipe on image data
            // check Disparity buffer
            if  ( NULL == stereoConfig->outputDisparityBuffer )
                return SCFG_STS__DISPARITY_BUFS_NOT_CFGD;
            // check Output Mode
            if  ( SCFG_OUT_M__NOT_CFGD == stereoConfig->outputMode )
                return SCFG_STS__OUT_M_NOT_CFGD;
            // check CM Parameters
            if  (  ( SCFG_PARAM__U8__NOT_CFGD == stereoConfig->cmAlfa      )
                || ( SCFG_PARAM__U8__NOT_CFGD == stereoConfig->cmBeta      )
                || ( SCFG_PARAM__U8__NOT_CFGD == stereoConfig->cmThreshold ) )
                return SCFG_STS__CM_PARAMS_NOT_CFGD;
            // check RWLUT parameters (needed for LUT configuration)
            if  (  ( SCFG_PARAM_S32__NOT_CFGD == stereoConfig->P1_H )
                || ( SCFG_PARAM_S32__NOT_CFGD == stereoConfig->P2_H )
                || ( SCFG_PARAM_S32__NOT_CFGD == stereoConfig->P1_V )
                || ( SCFG_PARAM_S32__NOT_CFGD == stereoConfig->P2_V ) )
                return SCFG_STS__RWLUT_PARAMS_NOT_CFGD;
            // TODO is CA_DIV_FACTOR param  needed in this CT_CM_SGMB mode ?
            break;

        default:
            return SCFG_STS__DFLT_ERROR;
    }

    // check if ForceDescriptorDump was requested
    if  ( SCFG_CTD__ENABLED == stereoConfig->forceDescrDump )
    {
        stereoForceDescrDumpRequested = true;
        // check CT Descriptors buffers
        if  (  ( NULL == stereoConfig->outputLCTDescrBuffer )
            || ( NULL == stereoConfig->outputRCTDescrBuffer ) )
            return SCFG_STS__CTD_BUFS_NOT_CFGD;
    } else

    // check if DebugCostDump was requested
    if  ( SCFG_DCD__ENABLED == stereoConfig->debugCostDump )
    {
        stereoDebugCostDumpRequested = true;
        // check Debug Costs buffer
        if  ( NULL == stereoConfig->outputDebugCostsBuffer )
            return SCFG_STS__DCD_BUFS_NOT_CFGD;
    }

    // check CT Masks
    if  ( SCFG_CT_TYPE__CT_MASK == stereoConfig->ctDescriptorType )
    {
        if  (  ( SCFG_PARAM_U32__NOT_CFGD == stereoConfig->ctMaskL )
            || ( SCFG_PARAM_U32__NOT_CFGD == stereoConfig->ctMaskH ) )
            return SCFG_STS__CT_MASKS_NOT_CFGD;
    }

    // TODO if OutputMode = SCFG_OUT_M__DSP_CD || SCFG_OUT_M__DSP_COMB --> ? INVALID_DISP

    // TODO if CT_TYPE = SCFG_CT_TYPE__THRESHOLD --> ? CT_THRESHOLD (STEREO_PARAMS register)

    return SCFG_STS__NO_ERROR;
}


// -----------------------------------------------------------------------------
//
// Local Function     configureLut
//
// Description        computes P1 and P2 parameters from the aggregation
//                    equation AND configures STEREO_RWLUT_x registers
//
// Parameters         stereoConfig - structure containing configuration params
//                                   that are used for computing P1 and P2
//
// Return             none
//
// Constraints        shall be called only from configureStereoFilter() function
//
// -----------------------------------------------------------------------------
void configureLut( hwiStereoConfig_t * stereoConfig )
{
    int hP1, hP2, vP1, vP2;
    int data = 0;

    // write to all locations of the LUT
    for ( int adr = 0; adr < 256; ++adr )
    {
        hP1 = (0 == adr) ? stereoConfig->P1_H : stereoConfig->P1_H/adr;
        hP2 = (0 == adr) ? stereoConfig->P2_H : stereoConfig->P2_H/adr;
        hP1 = CLIP(hP1, 0xFF);
        hP2 = CLIP(hP2, 0xFF);

        data = ( 1           << 26) |
               ( 1           << 25) |
               ( 1           << 24) |
               ((adr & 0xFF) << 16) |
               ((hP2 & 0xFF) <<  8) |
               ((hP1 & 0xFF) <<  0) ;

        apbSlave->ApbWrite(CV_STEREO_RWLUT0_ADR, data);

        vP1 = (0 == adr) ? stereoConfig->P1_V : stereoConfig->P1_V/adr;
        vP2 = (0 == adr) ? stereoConfig->P2_V : stereoConfig->P2_V/adr;
        vP1 = CLIP(vP1, 0xFF);
        vP2 = CLIP(vP2, 0xFF);

        data = ( 1           << 26) |
               ( 1           << 25) |
               ( 1           << 24) |
               ((adr & 0xFF) << 16) |
               ((vP2 & 0xFF) <<  8) |
               ((vP1 & 0xFF) <<  0) ;

        apbSlave->ApbWrite(CV_STEREO_RWLUT1_ADR, data);
    }
}


// -----------------------------------------------------------------------------
//
// Local Function     configureStereoFilter
//
// Description        enables Stereo HW filter (STEREO_EN register)
//                    configures registers
//                        - STEREO_CFG
//                        - STEREO_PARAMS
//                        - STEREO_CTMASKx
//                        based on configuration parameters
//                    performs LUT configuration - configureLut()
//                    sets image resolution/parameters
//
// Parameters         stereoConfig - structure containing provided
//                                   configuration parameters
//
// Return             none
//
// Constraints        shall be called only from hwiStereoConfig() function
//
// -----------------------------------------------------------------------------
void configureStereoFilter( hwiStereoConfig_t * stereoConfig )
{
    int data;


    // Register: STEREO_EN - enable Stereo HW filter in this pipe
    apbSlave->ApbWrite(CV_STEREO_EN_ADR, ENABLED | (ENABLED << 4));


    // Register: STEREO_CFG - set filter configuration
    // default configuration
    data =  stereoConfig->operationMode       <<  0  |
            stereoConfig->imageInputMode      <<  3  |
            stereoConfig->ctKernelSize        <<  8  |
            stereoConfig->ctDescriptorType    << 10  |
            stereoConfig->disparities         << 13  |
            stereoConfig->ctDescriptorFormat  << 15  |
            stereoConfig->compandingEnable    << 20  |
            stereoConfig->forceDescrDump      << 21  |
            stereoConfig->debugCostDump       << 22  ;

    // add other configured parameters
    if  ( SCFG_OUT_M__NOT_CFGD      != stereoConfig->outputMode   )
        data |= stereoConfig->outputMode    <<  5 ;
    if  ( SCFG_DIV_FACTOR__NOT_CFGD != stereoConfig->aggDivFactor )
        data |= stereoConfig->aggDivFactor  << 18 ;
    if  ( SCFG_PARAM__U8__NOT_CFGD  != stereoConfig->invalidDisp  )
        data |= stereoConfig->invalidDisp   << 24 ;

    apbSlave->ApbWrite(CV_STEREO_CFG_ADR, data);


    // Register: STEREO_PARAMS - set cost matching parameters
    if  ( SCFG_MODE__CT != stereoConfig->operationMode )
    {
        data =  stereoConfig->cmAlfa       <<  0  |
                stereoConfig->cmBeta       <<  4  |
                stereoConfig->cmThreshold  <<  8  ;

        // add other configured parameters
        if  ( SCFG_PARAM__U8__NOT_CFGD != stereoConfig->ctThreshold    )
            data |= stereoConfig->ctThreshold     << 16;
        if  ( SCFG_PARAM__U8__NOT_CFGD != stereoConfig->ratioThreshold )
            data |= stereoConfig->ratioThreshold  << 24;

        apbSlave->ApbWrite(CV_STEREO_PARAMS_ADR, data);
    }


    // Registers: STEREO_CTMASKx - set CT masks
    if  ( SCFG_CT_TYPE__CT_MASK == stereoConfig->ctDescriptorType )
    {
        apbSlave->ApbWrite(CV_STEREO_CTMASKL_ADR, stereoConfig->ctMaskL);
        apbSlave->ApbWrite(CV_STEREO_CTMASKH_ADR, stereoConfig->ctMaskH);
    }


    // Registers: STEREO_RWLUT_x - LUT configuration
    if  (  ( SCFG_MODE__SGMB       == stereoConfig->operationMode )
        || ( SCFG_MODE__CT_CM_SGMB == stereoConfig->operationMode ) )
        configureLut( stereoConfig );


    // set image resolution
    apbSlave->ApbWrite(CV_STEREO_FRM_DIM_ADR,
        ( (stereoConfig->imageHeight & SIPP_IMGDIM_MASK) << SIPP_IMGDIM_SIZE) |
          (stereoConfig->imageWidth  & SIPP_IMGDIM_MASK)                      );
}


template <typename TI, typename TO>
void configSippUnitTest();


// -----------------------------------------------------------------------------
//
// API Function       hwiStereoConfig
//
// Description        creates new Stereo PC Model
//                    performs SippUnitTest configuration based on provided
//                    configuration parameters - configSippUnitTest():
//                    - creates SippUnitTest input and output buffers
//                    - creates needed SippUnitTest producers
//                    - creates needed SippUnitTest consumers
//
// Parameters         stereoConfig - structure containing configuration
//                                   parameters provided by API user
//
// Return             configuration error status (scfgStatus_t)
//
// Constraints        hwiStereoInit() need to be called before
//
// -----------------------------------------------------------------------------
scfgStatus_t hwiStereoConfig( hwiStereoConfig_t * stereoConfig )
{
    // --------- ERROR HANDLER -------------------------------------------------

    // check if hwiStereoInit() was called before this
    if  ( HW_API__INIT != lastApiCalled )
        return SCFG_STS__INIT_NOT_CALLED;

    // check provided stereo configuration
    scfgStatus_t configStatus = checkStereoConfig( stereoConfig );
    if  ( SCFG_STS__NO_ERROR != configStatus )
        return configStatus;


    // --------- NEW STEREO PC MODEL -------------------------------------------

    // create new Stereo PC Model
    SippUnitTestConfigurer::ApbSlaveInterface_sp stereo(
        new StereoDisparitySoftwareModel(IsrHandler::separateRegIsr, STEREO_H_KERNEL_SIZE, STEREO_V_KERNEL_SIZE));
    SippUnitTestConfigurer::SetApbSlaveInterface(stereo);
    apbSlave = stereo.get();

    // configure Stereo Filter/Registers
    configureStereoFilter(stereoConfig);

    // set verbosity level for the filter in this unit test
    apbSlave->SetVerbose(verbose);


    // establish test case interrupts and name
    IsrHandler::SetTestInfo(irqMask, 0, tcstr);
    IsrHandler::SetIrqRegs (CV_STEREO_INT_STATUS_ADR, CV_STEREO_INT_ENABLE_ADR, CV_STEREO_INT_CLEAR_ADR);
    IsrHandler::SetIbflIrqSlot(0, 0); IsrHandler::SetIbflIrqSlot(1, 1);
    IsrHandler::SetObflIrqSlot(2, 0); IsrHandler::SetObflIrqSlot(3, 1);
    IsrHandler::SetEofIrqSlot (4);

    // enable ibfl_inc, obfl_dec and eof irqs
    IsrHandler::EnableSeparateIrqs(apbSlave);


    // --------- PROCESS CONFIGURATION PARAMETERS ------------------------------

    // process provided configuration parameters (stereoConfig)
    stereoImageWidth                        =                                stereoConfig->imageWidth             ;
    stereoImageHeight                       =                                stereoConfig->imageHeight            ;
    stereoImageStride                       =                                stereoConfig->imageStride            ;

    switch ( stereoConfig->imageInputMode )
    {
        case SCFG_IN_M__8BPP:
            stereoInputLBuffer              = (stereo__8BPP__itype *)        stereoConfig->inputLBuffer           ;
            stereoInputRBuffer              = (stereo__8BPP__itype *)        stereoConfig->inputRBuffer           ;
            break;
        case SCFG_IN_M__10BPP:
            stereoInputLBuffer              = (stereo__10BPP__itype *)       stereoConfig->inputLBuffer           ;
            stereoInputRBuffer              = (stereo__10BPP__itype *)       stereoConfig->inputRBuffer           ;
            break;
        default:
            return SCFG_STS__DFLT_ERROR;
    }

    stereoInputPreCCostsBuffer              =                                stereoConfig->inputPreCCostsBuffer   ;

    stereoOutputLCTDescrBuffer              =                                stereoConfig->outputLCTDescrBuffer   ;
    stereoOutputRCTDescrBuffer              =                                stereoConfig->outputRCTDescrBuffer   ;

    if  ( SCFG_MODE__CT != stereoConfig->operationMode )
        switch ( stereoConfig->outputMode )
        {
            case SCFG_OUT_M__DSP_MAP:
                stereoOutputDisparityBuffer = (stereo__DSP_MAP__otype * )    stereoConfig->outputDisparityBuffer  ;
                break;
            case SCFG_OUT_M__DSP_MC:
                stereoOutputDisparityBuffer = (stereo__DSP_MC__otype * )     stereoConfig->outputDisparityBuffer  ;
                break;
            case SCFG_OUT_M__DSP_CD:
                stereoOutputDisparityBuffer = (stereo__DSP_CD__otype * )     stereoConfig->outputDisparityBuffer  ;
                break;
            case SCFG_OUT_M__DSP_SINGLE:
                stereoOutputDisparityBuffer = (stereo__DSP_SINGLE__otype * ) stereoConfig->outputDisparityBuffer  ;
                break;
            case SCFG_OUT_M__DSP_COMB:
                stereoOutputDisparityBuffer = (stereo__DSP_COMB__otype * )   stereoConfig->outputDisparityBuffer  ;
                break;
            default:
                return SCFG_STS__DFLT_ERROR;
        }
    else
        stereoOutputDisparityBuffer         =                                stereoConfig->outputDisparityBuffer  ;

    stereoOutputDebugCostsBuffer            =                                stereoConfig->outputDebugCostsBuffer ;


    // --------- SIPP UNIT TEST CONFIGURATION ----------------------------------

    if  ( SCFG_MODE__CT == stereoConfig->operationMode )
    {
        switch (stereoConfig->imageInputMode)
        {
            case SCFG_IN_M__8BPP:
                configInputType  = CFG__INPUT__8BPP;
                configOutputType = CFG__OUTPUT__CT_DESCR;
                configSippUnitTest<stereo__8BPP__itype, stereo__CT_DESCR__otype>();
                break;
            case SCFG_IN_M__10BPP:
                configInputType  = CFG__INPUT__10BPP;
                configOutputType = CFG__OUTPUT__CT_DESCR;
                configSippUnitTest<stereo__10BPP__itype, stereo__CT_DESCR__otype>();
                break;
            default:
                return SCFG_STS__DFLT_ERROR;
        }
    }
    else
    {
        switch ( stereoConfig->imageInputMode )
        {
            case SCFG_IN_M__8BPP:
                switch (stereoConfig->outputMode )
                {
                    case SCFG_OUT_M__DSP_MAP:
                        configInputType  = CFG__INPUT__8BPP;
                        configOutputType = CFG__OUTPUT__DSP_MAP;
                        configSippUnitTest<stereo__8BPP__itype, stereo__DSP_MAP__otype>();
                        break;
                    case SCFG_OUT_M__DSP_MC:
                        configInputType  = CFG__INPUT__8BPP;
                        configOutputType = CFG__OUTPUT__DSP_MC;
                        configSippUnitTest<stereo__8BPP__itype, stereo__DSP_MC__otype>();
                        break;
                    case SCFG_OUT_M__DSP_CD:
                        configInputType  = CFG__INPUT__8BPP;
                        configOutputType = CFG__OUTPUT__DSP_CD;
                        configSippUnitTest<stereo__8BPP__itype, stereo__DSP_CD__otype>();
                        break;
                    case SCFG_OUT_M__DSP_SINGLE:
                        configInputType  = CFG__INPUT__8BPP;
                        configOutputType = CFG__OUTPUT__DSP_SINGLE;
                        configSippUnitTest<stereo__8BPP__itype, stereo__DSP_SINGLE__otype>();
                        break;
                    case SCFG_OUT_M__DSP_COMB:
                        configInputType  = CFG__INPUT__8BPP;
                        configOutputType = CFG__OUTPUT__DSP_COMB;
                        configSippUnitTest<stereo__8BPP__itype, stereo__DSP_COMB__otype>();
                        break;
                    default:
                        return SCFG_STS__DFLT_ERROR;
                }
                break;
            case SCFG_IN_M__10BPP:
                switch ( stereoConfig->outputMode )
                {
                    case SCFG_OUT_M__DSP_MAP:
                        configInputType  = CFG__INPUT__10BPP;
                        configOutputType = CFG__OUTPUT__DSP_MAP;
                        configSippUnitTest<stereo__10BPP__itype, stereo__DSP_MAP__otype>();
                        break;
                    case SCFG_OUT_M__DSP_MC:
                        configInputType  = CFG__INPUT__10BPP;
                        configOutputType = CFG__OUTPUT__DSP_MC;
                        configSippUnitTest<stereo__10BPP__itype, stereo__DSP_MC__otype>();
                        break;
                    case SCFG_OUT_M__DSP_CD:
                        configInputType  = CFG__INPUT__10BPP;
                        configOutputType = CFG__OUTPUT__DSP_CD;
                        configSippUnitTest<stereo__10BPP__itype, stereo__DSP_CD__otype>();
                        break;
                    case SCFG_OUT_M__DSP_SINGLE:
                        configInputType  = CFG__INPUT__10BPP;
                        configOutputType = CFG__OUTPUT__DSP_SINGLE;
                        configSippUnitTest<stereo__10BPP__itype, stereo__DSP_SINGLE__otype>();
                        break;
                    case SCFG_OUT_M__DSP_COMB:
                        configInputType  = CFG__INPUT__10BPP;
                        configOutputType = CFG__OUTPUT__DSP_COMB;
                        configSippUnitTest<stereo__10BPP__itype, stereo__DSP_COMB__otype>();
                        break;
                    default:
                        return SCFG_STS__DFLT_ERROR;
                }
                break;
            default:
                return SCFG_STS__DFLT_ERROR;
        }
    }


    // save last valid API call for error handling
    lastApiCalled = HW_API__CONFIG;

    return SCFG_STS__NO_ERROR;
}


// -----------------------------------------------------------------------------
//
// Local Function     configSippUnitTest
//
// Description        creates SippUnitTest input and output buffers
//                    creates needed SippUnitTest producers
//                    creates needed SippUnitTest consumers
//
// Parameters         none
//
// Template Types     TI - input data type based on configured imageInputMode:
//                         scfgImageInputMode_t: 8bpp, 10bpp
//                    TO - output data type based on configured outputMode:
//                         scfgOutputMode_t: MAP, MC, CD, SINGLE, COMB
//
// Return             none
//
// Constraints        shall be called only from hwiStereoConfig() function
//
// -----------------------------------------------------------------------------
template <typename TI, typename TO>
void configSippUnitTest()
{
    // Thread-safe mechanism for preventing  I/O buffer over/underflow, respectively.
    // Initially, all the buffer are empty.
    Semaphore_sp  inputSem[IsrHandler::MAX_IBUFS];
    Semaphore_sp outputSem[IsrHandler::MAX_OBUFS];

    // Pre-Computed Costs are needed by Stereo PC Model
    if  ( stereoPreCCostsRequired )
    {
        // Configure SippUniTest for:
        // - Pre-Computed Costs - INPUT

        inputSem[1] = Semaphore_sp(new Semaphore(INPUT1_BUFFER_LINES, INPUT0_BUFFER_LINES));

        IsrHandler::SetInputSemaphore (inputSem[1], 1);

        static SippUnitTestBufferAlloc<stereo__PreCCosts__itype> ibuf1(DEF_SLICE_SIZE * CMX_NSLICES);
        uint32_t inAddr1[] = { IBUF1_BASE_ADR, IBUF1_CFG_ADR, IBUF1_LS_ADR, IBUF1_PS_ADR, IBUF1_FC_ADR, ICTX1_ADR };

        SippUnitTestConfigurer::addr_idx_t inAddrV1( inAddr1, inAddr1 + sizeof( inAddr1)/sizeof(* inAddr1));

        inputCfg[1] = SippUnitTestConfigurer(
            ibuf1,
            INPUT1_BUFFER_LINES,
            stereoImageStride * stereoDisparities,
            1, // 1 plane for Pre-Computed Costs
            sizeof(stereo__PreCCosts__itype),
            inAddrV1,
            inputSem[1]
        );

        inputCfg[1].SetInDimensions (stereoImageWidth * stereoDisparities, stereoImageHeight);
        inputCfg[1].DetermineCurrentBufferIdx();

        // Pre-Computed Costs PRODUCER
        inputBuffer__PreCCosts[0] = stereoInputPreCCostsBuffer;
        prod1 = new SippUnitTestProcess(circularProducer<stereo__PreCCosts__itype, inputBuffer__PreCCosts, VERBOSITY>, inputCfg[1], INFINITE);
    }

    // Configure SippUniTest for:
    // - R & L images                       - INPUT
    // - CT Descriptors OR DebugCostsDump   - OUTPUT
    // - Disparity                          - OUTPUT

     inputSem[0] = Semaphore_sp(new Semaphore(INPUT0_BUFFER_LINES, INPUT0_BUFFER_LINES)); // R & L images
    outputSem[0] = Semaphore_sp(new Semaphore(0                  , OUTPUT_BUFFER_LINES)); // CT Descriptors OR DebugCostDump
    outputSem[1] = Semaphore_sp(new Semaphore(0                  , OUTPUT_BUFFER_LINES)); // Disparity

    IsrHandler::SetInputSemaphore ( inputSem[0], 0);
    IsrHandler::SetOutputSemaphore(outputSem[0], 0);
    IsrHandler::SetOutputSemaphore(outputSem[1], 1);

    // buffer allocation and configuration
    static SippUnitTestBufferAlloc<TI>                       ibuf0(DEF_SLICE_SIZE * CMX_NSLICES);
    static SippUnitTestBufferAlloc<stereo__CT_DESCR__otype>  obuf0(DEF_SLICE_SIZE * CMX_NSLICES);
    static SippUnitTestBufferAlloc<TO>                       obuf1(DEF_SLICE_SIZE * CMX_NSLICES);

    uint32_t  inAddr0[] = { IBUF0_BASE_ADR, IBUF0_CFG_ADR, IBUF0_LS_ADR, IBUF0_PS_ADR, IBUF0_FC_ADR, ICTX0_ADR };
    uint32_t outAddr0[] = { OBUF0_BASE_ADR, OBUF0_CFG_ADR, OBUF0_LS_ADR, OBUF0_PS_ADR, OBUF0_FC_ADR, OCTX0_ADR };
    uint32_t outAddr1[] = { OBUF1_BASE_ADR, OBUF1_CFG_ADR, OBUF1_LS_ADR, OBUF1_PS_ADR, OBUF1_FC_ADR, OCTX1_ADR };

    SippUnitTestConfigurer::addr_idx_t  inAddrV0( inAddr0,  inAddr0 + sizeof( inAddr0)/sizeof(* inAddr0));
    SippUnitTestConfigurer::addr_idx_t outAddrV0(outAddr0, outAddr0 + sizeof(outAddr0)/sizeof(*outAddr0));
    SippUnitTestConfigurer::addr_idx_t outAddrV1(outAddr1, outAddr1 + sizeof(outAddr1)/sizeof(*outAddr1));

    inputCfg[0] = SippUnitTestConfigurer(   // R & L images
        ibuf0,
        stereoPreCCostsRequired ? INPUT0_BUFFER_LINES + stereoImageHeight : INPUT0_BUFFER_LINES,
        stereoImageStride,
        2, // 2 planes for R & L images
        sizeof(TI),
        inAddrV0,
        inputSem[0]
    );
    outputCfg[0] = SippUnitTestConfigurer(  // CT Descriptors OR DebugCostDump
        obuf0,
        OUTPUT_BUFFER_LINES,
        stereoDebugCostDumpRequested ? stereoImageStride * stereoDisparities : stereoImageStride,
        stereoDebugCostDumpRequested ? 1 : 2, // 1 plane - DebugCostDump, 2 planes - CT Descriptors
        stereoDebugCostDumpRequested ? sizeof(stereo__DBG_COSTS__otype) : sizeof(stereo__CT_DESCR__otype),
        outAddrV0,
        outputSem[0]
    );
    outputCfg[1] = SippUnitTestConfigurer(  // Disparity
        obuf1,
        OUTPUT_BUFFER_LINES,
        stereoImageStride,
        1, // 1 plane for Disparity
        sizeof(TO),
        outAddrV1,
        outputSem[1]
    );

     inputCfg[0].SetInDimensions (stereoImageWidth, stereoImageHeight);
    outputCfg[0].SetOutDimensions(
        stereoDebugCostDumpRequested ? stereoImageWidth * stereoDisparities : stereoImageWidth,
        stereoImageHeight);
    outputCfg[1].SetOutDimensions(stereoImageWidth, stereoImageHeight);

    // See where the filter's left off from the last run.
    // This is performed here instead of in the producer consumer themselves as the following scenario could've happened:
    // - producer thread started, produced sufficient IBFL_INC events
    // - filter would have run, increased its ocbl
    // - consumer thread started, read erroneous ocbl that has just been updated (not the one from the last run)
     inputCfg[0].DetermineCurrentBufferIdx();
    outputCfg[0].DetermineCurrentBufferIdx();
    outputCfg[1].DetermineCurrentBufferIdx();

    // R & L images PRODUCER
    switch ( configInputType )
    {
        case CFG__INPUT__8BPP:
            inputBuffer__8BPP[0] = (stereo__8BPP__itype *) stereoInputLBuffer;
            inputBuffer__8BPP[1] = (stereo__8BPP__itype *) stereoInputRBuffer;

            prod0 = new SippUnitTestProcess(circularProducer<stereo__8BPP__itype, inputBuffer__8BPP, VERBOSITY>, inputCfg[0], INFINITE);
            break;

        case CFG__INPUT__10BPP:
            inputBuffer__10BPP[0] = (stereo__10BPP__itype *) stereoInputLBuffer;
            inputBuffer__10BPP[1] = (stereo__10BPP__itype *) stereoInputRBuffer;

            prod0 = new SippUnitTestProcess(circularProducer<stereo__10BPP__itype, inputBuffer__10BPP, VERBOSITY>, inputCfg[0], INFINITE);
            break;

        default:
            // n/a
            break;
    }

    // check if ForceDescriptorDump was requested
    if  ( stereoForceDescrDumpRequested )
        // CT Descriptors CONSUMER
        cons0 = new SippUnitTestProcess(circularConsumer<stereo__CT_DESCR__otype,  stereo__CT_DESCR__otype_sp,  outputBuffer__CT_DESCR,  VERBOSITY>, outputCfg[0], INFINITE);

    // otherwhise, check if DebugCostDump was requested
    if  ( stereoDebugCostDumpRequested )
        // Debug Costs Dump CONSUMER
        cons0 = new SippUnitTestProcess(circularConsumer<stereo__DBG_COSTS__otype, stereo__DBG_COSTS__otype_sp, outputBuffer__DBG_COSTS, VERBOSITY>, outputCfg[0], INFINITE);

    // CT Descriptor CONSUMER
    // AND
    // Disparity     CONSUMER
    switch ( configOutputType )
    {
        case CFG__OUTPUT__CT_DESCR:
            // This case is executed only when OperationMode is SCFG_MODE__CT.
            // Therefore, stereoForceDescrDumpRequested is FALSE AND stereoDebugCostDumpRequested is FALSE
            // because they are set on TRUE only when OperationMode is different from SCFG_MODE__CT.
            cons0 = new SippUnitTestProcess(circularConsumer<stereo__CT_DESCR__otype,   stereo__CT_DESCR__otype_sp,   outputBuffer__CT_DESCR,   VERBOSITY>, outputCfg[0], INFINITE);
            break;
        case CFG__OUTPUT__DSP_MAP:
            cons1 = new SippUnitTestProcess(circularConsumer<stereo__DSP_MAP__otype,    stereo__DSP_MAP__otype_sp,    outputBuffer__DSP_MAP,    VERBOSITY>, outputCfg[1], INFINITE);
            break;
        case CFG__OUTPUT__DSP_MC:
            cons1 = new SippUnitTestProcess(circularConsumer<stereo__DSP_MC__otype,     stereo__DSP_MC__otype_sp,     outputBuffer__DSP_MC,     VERBOSITY>, outputCfg[1], INFINITE);
            break;
        case CFG__OUTPUT__DSP_CD:
            cons1 = new SippUnitTestProcess(circularConsumer<stereo__DSP_CD__otype,     stereo__DSP_CD__otype_sp,     outputBuffer__DSP_CD,     VERBOSITY>, outputCfg[1], INFINITE);
            break;
        case CFG__OUTPUT__DSP_SINGLE:
            cons1 = new SippUnitTestProcess(circularConsumer<stereo__DSP_SINGLE__otype, stereo__DSP_SINGLE__otype_sp, outputBuffer__DSP_SINGLE, VERBOSITY>, outputCfg[1], INFINITE);
            break;
        case CFG__OUTPUT__DSP_COMB:
            cons1 = new SippUnitTestProcess(circularConsumer<stereo__DSP_COMB__otype,   stereo__DSP_COMB__otype_sp,   outputBuffer__DSP_COMB,   VERBOSITY>, outputCfg[1], INFINITE);
            break;
        default:
            // n/a
            break;
    }

    return;
}


// -----------------------------------------------------------------------------
//
// API Function       hwiStereoStart
//
// Description        starts producer and consumer processes
//
// Parameters         none
//
// Return             configuration error status (scfgStatus_t)
//
// Constraints        hwiStereoConfig() need to be called before
//
// -----------------------------------------------------------------------------
scfgStatus_t hwiStereoStart()
{
    // check if hwiStereoConfig() was called before this
    if  ( HW_API__CONFIG != lastApiCalled )
        return SCFG_STS__CONFIG_NOT_CALLED;

    if  ( verbose )
        // "Fill level" mode - start producer and consumer threads and wait for them to terminate
        std::cout << tcstr
                  << " - starting producer/consumer threads: filtering "
                  << stereoImageHeight << " lines of "
                  << stereoImageWidth  << " pixels..."
                  << std::endl;


    // start PRODUCER and CONSUMER processes
    if  ( NULL != prod0 )
        pthrd0  = prod0->StartProcess();
    if  ( NULL != prod1 )
        pthrd1  = prod1->StartProcess();
    if  ( NULL != cons0 )
        cthrd0  = cons0->StartProcess();
    if  ( NULL != cons1 )
        cthrd1  = cons1->StartProcess();


    // save last valid API call for error handling
    lastApiCalled = HW_API__START;

    return SCFG_STS__NO_ERROR;
}


// -----------------------------------------------------------------------------
//
// API Function       hwiStereoWait
//
// Description        wait for producer/consumer threads to finish
//                    extract results from SippUnitTest output buffers
//
// Parameters         none
//
// Return             configuration error status (scfgStatus_t)
//
// Constraints        hwiStereoStart() need to be called before
//
// -----------------------------------------------------------------------------
scfgStatus_t hwiStereoWait()
{
    // check if hwiStereoStart() was called before this
    if  ( HW_API__START != lastApiCalled )
        return SCFG_STS__START_NOT_CALLED;


    // wait for producer/consumer threads to finish
    if  ( NULL != prod0 )
        prod0->WaitForProcess(pthrd0);
    if  ( NULL != cons0 )
        cons0->WaitForProcess(cthrd0);
    if  ( NULL != prod1 )
        prod1->WaitForProcess(pthrd1);
    if  ( NULL != cons1 )
        cons1->WaitForProcess(cthrd1);

    // extract results from SippUnitTest buffers to corresponding provided output buffers
    if  ( stereoForceDescrDumpRequested )
    {
        memcpy(stereoOutputLCTDescrBuffer, outputBuffer__CT_DESCR[0].get(), stereoImageWidth * stereoImageHeight );
        memcpy(stereoOutputRCTDescrBuffer, outputBuffer__CT_DESCR[1].get(), stereoImageWidth * stereoImageHeight );
    }

    switch ( configOutputType )
    {
        case CFG__OUTPUT__CT_DESCR:
            // This case is executed only when OperationMode is SCFG_MODE__CT.
            // Therefore, stereoForceDescrDumpRequested is FALSE AND stereoDebugCostDumpRequested is FALSE
            // because they are set on TRUE only when OperationMode is different from SCFG_MODE__CT
            memcpy(stereoOutputLCTDescrBuffer, outputBuffer__CT_DESCR[0].get(), stereoImageWidth * stereoImageHeight * sizeof(stereo__CT_DESCR__otype) );
            memcpy(stereoOutputRCTDescrBuffer, outputBuffer__CT_DESCR[1].get(), stereoImageWidth * stereoImageHeight * sizeof(stereo__CT_DESCR__otype) );
            break;
        case CFG__OUTPUT__DSP_MAP:
            memcpy(stereoOutputDisparityBuffer, outputBuffer__DSP_MAP[0].get(), stereoImageWidth * stereoImageHeight * sizeof(stereo__DSP_MAP__otype) );
            break;
        case CFG__OUTPUT__DSP_MC:
            memcpy(stereoOutputDisparityBuffer, outputBuffer__DSP_MC[0].get(), stereoImageWidth * stereoImageHeight * sizeof(stereo__DSP_MC__otype) );
            break;
        case CFG__OUTPUT__DSP_CD:
            memcpy(stereoOutputDisparityBuffer, outputBuffer__DSP_CD[0].get(), stereoImageWidth * stereoImageHeight * sizeof(stereo__DSP_CD__otype) );
            break;
        case CFG__OUTPUT__DSP_SINGLE:
            memcpy(stereoOutputDisparityBuffer, outputBuffer__DSP_SINGLE[0].get(), stereoImageWidth * stereoImageHeight * sizeof(stereo__DSP_SINGLE__otype) );
            break;
        case CFG__OUTPUT__DSP_COMB:
            memcpy(stereoOutputDisparityBuffer, outputBuffer__DSP_COMB[0].get(), stereoImageWidth * stereoImageHeight * sizeof(stereo__DSP_COMB__otype) );
            break;
        default:
            // n/a
            break;
    }

    if  ( stereoDebugCostDumpRequested )
    {
        memcpy(stereoOutputDebugCostsBuffer, outputBuffer__DBG_COSTS[0].get(), stereoImageWidth * stereoDisparities * stereoImageHeight);
    }

    // delete producers and consumers
    if  ( NULL != prod0 )
        delete prod0;
    if  ( NULL != prod1 )
        delete prod1;
    if  ( NULL != cons0 )
        delete cons0;
    if  ( NULL != cons1 )
        delete cons1;


    // save last valid API call for error handling
    lastApiCalled = HW_API__WAIT;

    return SCFG_STS__NO_ERROR;
}

