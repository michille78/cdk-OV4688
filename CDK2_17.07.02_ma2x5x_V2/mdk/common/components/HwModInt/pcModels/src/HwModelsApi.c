///
/// @file
/// @copyright All code copyright Movidius Ltd 2012, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     main leon file
///


// 1: Includes
// ----------------------------------------------------------------------------
#include <stdint.h>
#include <stdbool.h>
#include "HwModelsApi.h"

// 2:  Source Specific #defines and types  (typedef, enum, struct)
// ----------------------------------------------------------------------------

// 3: Global Data (Only if absolutely necessary)
// ----------------------------------------------------------------------------

// 4: Static Local Data
// ----------------------------------------------------------------------------

// 5: Static Function Prototypes
// ----------------------------------------------------------------------------

// 6: Functions Implementation
// ----------------------------------------------------------------------------


// status  getHwModule(enum <module name>, hwModHandle)
//     - return value: HW_INTERF_MODULE_ALREADY_IN_USE
//                          HW_INTERF_ALLOCATION_SUCCESS
//                          HW_INTERF_ALLOCATION_ERROR
//     - this function should be very simple and efficient to allow for a quick execution and in case it dosnt return success then the application should continue execution of other activities,
//               OPTIONALY: it could also allow a wait until the resource is available.

// status initCfgHwModule(hwModHandle, struct <configuration>)
//     - return value : HW_INTERF_CFG_SUCCESS
//                           HW_INTERF_CFG_ERROR
//     - this function should init all module configuration variables, it should be possible to start the HWModule after this call.

// status adjHwModuleCfg(hwModHandle, struct <dyn_configuration>)
//     - return value : HW_INTERF_CFG_ADJ_SUCCESS
//                           HW_INTERF_CFG_ADJ_ERROR -- this will be returned also in case the initCfgHwModule was not called before this function call, it is questionable if it should return error if the hw module was not started. (probably it should not)
//                           HW_INTERF_CFG_ADJ_NOT_POSSIBLE
//     - this function should allow configuration of parameters then can be changed while the HW module is running , if there are such parameters

// status startHwModule(hwModHandle);

// status waitHwModule(hwModHandle);

// status resetHwModule(hwModHandle)   sau  status releaseHwModule(hwModHandle) sau status freeHwModule(hwModHandle)


