///
/// @file
/// @copyright All code copyright Movidius Ltd 2012, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Convolutional Neural Network Application Programming Interface
///
/// This is the implementation of the CNN API functions.
/// The module contains functions for setting up the 3 types of CNN functions:
/// convolution, pooling and fully connected layers.
/// Additional functions allow the configuration of ReLu function, bias and scale.
/// The CNN hardware module accepts a linked list of descriptors (each descriptor
/// represents an operation). Different functions exist to facilitate the creation of
/// this linked lists.
///

// 1: Includes
// ----------------------------------------------------------------------------

#include <registersMyriad2.h>
#include "CNNSoftwareModel.h"
#include <stddef.h>
#include <cstdio>
#include <string.h>
#include <stdarg.h>
#include "hwCnnApi.h"
#include "hwCnnApiPrivate.h"
#include "CNNIrq.h"

// 2:  Source Specific #defines and types  (typedef,enum,struct)
// ----------------------------------------------------------------------------

/// @brief Coefficient/Vectors line size
#define COEFF_LINE_SIZE (8 * sizeof(uint16_t))
#define LOCAL_RAM_SIZE (128 * 1024)
#define CMX_DATA_WIDTH 128

/// @brief Operation type enum
typedef enum
{
    TYPE_CONV     = 0,
    TYPE_CONVPOOL = 1,
    TYPE_FULLCONN = 2,
    TYPE_POOL     = 4
} ten_OperationType;

/// @brief Common part for CNN layer descriptor
typedef struct
{
    uint32_t linkAddress : 32;
    ten_OperationType type  : 3;
    cnnOperationMode mode  : 3;
    uint32_t rsvd0 : 2;
    uint32_t id : 4;
    uint32_t it : 4;
    cnnCoefficientMode cm : 3;
    cnnDataMode dm : 1;
    uint32_t disInt : 1;
    uint32_t rsvd1 : 11;
} GeneralLinkStructure;

/// @brief Common CNN layer descriptor
typedef struct
{
    GeneralLinkStructure Line0;
    union
    {
        struct
        {
            uint32_t inputHeight : 12;
            uint32_t rsvd0 : 4;
            uint32_t inputWidth  : 12;
            uint32_t rsvd1 : 4;
            uint32_t inputChannels  : 11;
            uint32_t rsvd2 : 5;
            uint32_t outputChannels : 11;
            uint32_t rsvd3 : 5;
        } ConvolutionPooling;
        struct
        {
            uint32_t inputWidth : 12;
            uint32_t rsvd0 : 20;
            uint32_t vectors : 8;
            uint32_t rsvd1 : 24;
        } FullyConnected;
    } Line1;

    union
    {
        struct
        {
            uint32_t chPerRamBlock : 11;
            uint32_t rsvd0  : 5;
            uint32_t chStride : 4;
            uint32_t rsvd1  : 12;
            uint32_t kernelWidth  : 4;  //not used for Pooling
            uint32_t kernelHeight : 4;  //not used for Pooling
            uint32_t rsvd2 : 19;
            cnnPadMode padType : 4;
            uint32_t padEn : 1;
        } ConvolutionPooling;

        struct
        {
            uint32_t dataPerRamBlock : 9;
            uint32_t rsvd0 : 23;
            uint32_t rsvd1 : 32;
        } FullyConnected;
    } Line2;

    union
    {
        struct
        {
            uint32_t poolEn : 1;
            uint32_t rsvd0  : 15;
            uint32_t poolKernelHeight : 8;
            uint32_t poolKernelWidth  : 8;
            uint32_t avgPoolX : 16;   //Average value for pooling OR X value for ReLuX
            uint32_t rsvd2 : 15;
            cnnPoolType poolType : 1;
        } ConvolutionPooling;

        struct
        {
            uint32_t rsvd0 : 32;
            uint32_t X : 16;
            uint32_t rsvd1 : 16;
        } FullyConnected;

    } Line3;

    struct
    {
        uint32_t dataBaseAddr : 32;
        uint32_t t0 : 10;               // not used for Pooling
        uint32_t a0 : 10;               // not used for Pooling
        uint32_t a1 : 10;               // not used for Pooling
        uint32_t reluxEn : 1;           // not used for Pooling
        uint32_t reluEn  : 1;           // not used for Pooling
    } Line4;

    struct
    {
        uint32_t dataChStr : 32;
        uint32_t dataLnStr : 32;
    } Line5;


    union
    {
        struct
        {
            uint32_t coeffBaseAddr : 32;
            uint32_t coeffChStrOut : 32;
        } Convolution;

        struct
        {
            uint32_t vectorBaseAddr : 32;
            uint32_t vectorStrOut : 32;
        } FullyConnected;
    } Line6;

    union
    {
        struct
        {
            uint32_t coeffChStrIn : 32;  //not used for Pooling
            uint32_t outLnStr : 32;
        } ConvolutionPooling;

        struct
        {
            uint32_t vectorStrIn : 32;
            uint32_t outLnStr : 32;
        } FullyConnected;
    } Line7;


    struct
    {
        uint32_t outBaseAddr : 32;
        uint32_t outChStr : 32;
    } Line8;

    union
    {
        struct
        {
            uint32_t localLs : 9;
            uint32_t rsvd0 : 7;
            uint32_t localCs : 13;
            uint32_t rsvd1 : 3;
            uint32_t linesPerCh : 9;
            uint32_t rsvd2 : 22;
            uint32_t rud : 1;
        } ConvolutionPooling;

        struct
        {
            uint32_t localLs : 9;
            uint32_t rsvd0 : 7;
            uint32_t localBs : 13;
            uint32_t rsvd1 : 3;
            uint32_t rsvd2 : 31;
            uint32_t rud : 1;
        } FullyConnected;
    } Line9;

    union
    {
        struct
        {
            uint32_t minLines : 9;
            uint32_t rsvd0 : 23;
            uint32_t coeffLpb : 8; //not used for Pooling
            uint32_t css : 8;      //not used for Pooling
            uint32_t outputX : 12;
            uint32_t rsvd1 : 4;
        } ConvolutionPooling;

        struct
        {
            uint32_t rsvd0 : 16;
            uint32_t acc : 1;
            uint32_t rsvd1 : 15;
            uint32_t vectorLPB : 8;
            uint32_t rsvd2 : 24;
        } FullyConnected;
    } Line10;

    struct
    {
        uint32_t  biasBaseAddr : 32;
        uint32_t scaleBaseAddr : 32;
    } Line11;
    struct
    {
        uint32_t p0  : 16;
        uint32_t p1  : 16;
        uint32_t p2  : 16;
        uint32_t p3  : 16;
    } Line12;
    struct
    {
        uint32_t p4  : 16;
        uint32_t p5  : 16;
        uint32_t p6  : 16;
        uint32_t p7  : 16;
    } Line13;
    struct
    {
        uint32_t p8  : 16;
        uint32_t p9  : 16;
        uint32_t p10 : 16;
        uint32_t p11 : 16;
    } Line14;
    struct
    {
        uint32_t p12 : 16;
        uint32_t p13 : 16;
        uint32_t p14 : 16;
        uint32_t p15 : 16;
    } Line15;
} cnnDescriptorStructure;


// 5: Static Function Prototypes
// ----------------------------------------------------------------------------

// 6: Functions Implementation
// ----------------------------------------------------------------------------

// Returns the optimum mode for a given input size
uint32_t getMode(uint32_t inputSize)
{
    uint32_t mode = 0; //Default mode is 0 (1:256)
    if(0 == (inputSize % 8))
    {
        if(0 == (inputSize % 16))
        {
            mode = 4;
        }
        else if(0 == (inputSize % 8))
        {
            mode = 3;
        }
        else if(0 == (inputSize % 4))
        {
            mode = 2;
        }
        else if(0 == (inputSize % 2))
        {
            mode = 1;
        }
    }
    return mode;
}

// Configures the basic elements of a cnn descriptor
void cnnInitDescriptor(cnnLayer *layer, cnnDataMode dataMode, uint32_t Id, uint32_t disableInt, uint32_t interruptTrigger, cnnLayer *linkTo)
{
    cnnDescriptorStructure *desc = reinterpret_cast<cnnDescriptorStructure*>(layer);
    GeneralLinkStructure *link = reinterpret_cast<GeneralLinkStructure*>(linkTo);
    memset(layer, 0, sizeof(cnnLayer));
    desc->Line0.disInt = disableInt & 0x01;
    desc->Line0.dm = dataMode;
    desc->Line0.it = interruptTrigger & 0x0F;
    desc->Line0.id = Id;
    if(NULL != link)
    {
        link->linkAddress = (reinterpret_cast<uint32_t>(desc));
    }
}

// Configures the input parameters of a cnn descriptor (only for convolution or pooling layers)
void cnnSetupInput(cnnLayer *layer, void *inputData, uint32_t noOfChannels, uint32_t chStride, uint32_t lineStride, uint32_t dataWidth, uint32_t dataHeight)
{
    cnnDescriptorStructure *desc = reinterpret_cast<cnnDescriptorStructure*>(layer);    
    desc->Line1.ConvolutionPooling.inputHeight = dataHeight;
    desc->Line1.ConvolutionPooling.inputWidth = dataWidth;
    desc->Line1.ConvolutionPooling.inputChannels = noOfChannels;    
    desc->Line4.dataBaseAddr = (reinterpret_cast<uint32_t>(inputData));
    desc->Line5.dataChStr = chStride;
    desc->Line5.dataLnStr = lineStride;
}

// Configures the input parameters of a cnn descriptor (only for convolution or pooling layers); stides are automatically computed
void cnnSetupInputBrief(cnnLayer *layer, void *inputData, uint32_t noOfChannels, uint32_t dataWidth, uint32_t dataHeight)
{
    cnnSetupInput(layer, inputData, noOfChannels, 0, 0, dataWidth, dataHeight);
}

// Configures the output parameters of a cnn descriptor (only for convolution or pooling layers)
void cnnSetupOutput(cnnLayer *layer, void *outputData, uint32_t noOfChannels, uint32_t chStride, uint32_t lineStride)
{
    cnnDescriptorStructure *desc = reinterpret_cast<cnnDescriptorStructure*>(layer);
    desc->Line1.ConvolutionPooling.outputChannels = noOfChannels;
    desc->Line7.ConvolutionPooling.outLnStr = lineStride;
    desc->Line8.outChStr = chStride;
    desc->Line8.outBaseAddr = (reinterpret_cast<uint32_t>(outputData));
}

// Configures the output parameters of a cnn descriptor (only for convolution or pooling layers); stides are automatically computed
void cnnSetupOutputBrief(cnnLayer *layer, void *outputData, uint32_t noOfChannels)
{
    cnnSetupOutput(layer, outputData, noOfChannels, 0, 0);
}

// Configures the coefficients parameters of a cnn descriptor (only for convolution layer)
void cnnSetupConvolutionCoefficients(cnnLayer *layer, cnnCoefficientMode mode, void *coefficientData, uint32_t chStrideIn, uint32_t chStrideOut, uint32_t kernelWidth, uint32_t kernelHeight, uint32_t convolutionStride, uint16_t *palette)
{
    cnnDescriptorStructure *desc = reinterpret_cast<cnnDescriptorStructure*>(layer);
    desc->Line6.Convolution.coeffBaseAddr = (reinterpret_cast<uint32_t>(coefficientData));
    desc->Line6.Convolution.coeffChStrOut = chStrideOut;
    desc->Line7.ConvolutionPooling.coeffChStrIn = chStrideIn;
    desc->Line2.ConvolutionPooling.kernelHeight = kernelHeight;
    desc->Line2.ConvolutionPooling.kernelWidth = kernelWidth;
    desc->Line2.ConvolutionPooling.chStride = convolutionStride;
    desc->Line0.cm = mode;
    desc->Line0.type = TYPE_CONV;
    if(NULL != palette)
    {
        desc->Line12.p0 = *(palette + 0);
        desc->Line12.p1 = *(palette + 1);
        desc->Line12.p2 = *(palette + 2);
        desc->Line12.p3 = *(palette + 3);
        desc->Line13.p4 = *(palette + 4);
        desc->Line13.p5 = *(palette + 5);
        desc->Line13.p6 = *(palette + 6);
        desc->Line13.p7 = *(palette + 7);
        desc->Line14.p8 = *(palette + 8);
        desc->Line14.p9 = *(palette + 9);
        desc->Line14.p10 = *(palette + 10);
        desc->Line14.p11 = *(palette + 11);
        desc->Line15.p12 = *(palette + 12);
        desc->Line15.p13 = *(palette + 13);
        desc->Line15.p14 = *(palette + 14);
        desc->Line15.p15 = *(palette + 15);
    }
}

// Configures the coefficients parameters of a cnn descriptor (only for convolution layer); stides are automatically computed
void cnnSetupConvolutionCoefficientsBrief(cnnLayer *layer, cnnCoefficientMode mode, void *coefficientData, uint32_t kernelWidth, uint32_t kernelHeight, uint32_t convolutionStride, uint16_t *palette)
{
    cnnSetupConvolutionCoefficients(layer, mode, coefficientData, 0, 0, kernelWidth, kernelHeight, convolutionStride, palette);
}

// Configures the optional pooling parameters for a cnn descriptor (only for convolution layers)
void cnnSetupConvolutionPooling(cnnLayer *layer, uint32_t kernelWidth, uint32_t kernelHeight)
{
    cnnDescriptorStructure *desc = reinterpret_cast<cnnDescriptorStructure*>(layer);
    desc->Line0.type = TYPE_CONVPOOL;
    desc->Line3.ConvolutionPooling.poolEn = 1;
    desc->Line3.ConvolutionPooling.poolKernelHeight = kernelHeight;
    desc->Line3.ConvolutionPooling.poolKernelWidth = kernelWidth;
    desc->Line3.ConvolutionPooling.poolType = POOL_MAX;
}

// Configures the pooling parameters for a cnn descriptor (only for a pooling layer)
void cnnSetupPoolingLayer(cnnLayer *layer, cnnPoolType type, uint32_t kernelWidth, uint32_t kernelHeight, uint32_t poolStride)
{
    cnnDescriptorStructure *desc = reinterpret_cast<cnnDescriptorStructure*>(layer);    
    desc->Line0.type = TYPE_POOL;
    desc->Line3.ConvolutionPooling.poolEn = 1;
    desc->Line3.ConvolutionPooling.poolKernelHeight = kernelHeight;
    desc->Line3.ConvolutionPooling.poolKernelWidth = kernelWidth;
    desc->Line2.ConvolutionPooling.chStride = poolStride;
    desc->Line3.ConvolutionPooling.poolType = type;
    if(POOL_AVERAGE == type)
    {
        float average = (float)(1) / (float)(kernelWidth * kernelHeight);
        fp16 avg = fp16(average);
        desc->Line3.ConvolutionPooling.avgPoolX = avg.getPackedValue();
    }    
}

// Configures the input parameters for a cnn descriptor (only for a fully connected layer)
void cnnSetupInputFullyConnectedLayer(cnnLayer *layer, void *inputData, uint32_t inputWidth, uint32_t lineStride, uint32_t blockStride)
{
    cnnDescriptorStructure *desc = reinterpret_cast<cnnDescriptorStructure*>(layer);
    desc->Line0.type = TYPE_FULLCONN;
    desc->Line1.FullyConnected.inputWidth = inputWidth;
    desc->Line4.dataBaseAddr = (reinterpret_cast<uint32_t>(inputData));
    desc->Line5.dataLnStr = lineStride;
    desc->Line5.dataChStr = blockStride;
}

// Configures the input parameters for a cnn descriptor (only for a fully connected layer); stides are automatically computed
void cnnSetupInputFullyConnectedLayerBrief(cnnLayer *layer, void *inputData, uint32_t inputWidth)
{
    cnnSetupInputFullyConnectedLayer(layer, inputData, inputWidth, 0, 0);
}

// Configures the output parameters for a cnn descriptor (only for a fully connected layer)
void cnnSetupOutputFullyConnectedLayer(cnnLayer *layer, void *outputData, uint32_t lineStride)
{
    cnnDescriptorStructure *desc = reinterpret_cast<cnnDescriptorStructure*>(layer);
    desc->Line0.type = TYPE_FULLCONN;
    desc->Line7.FullyConnected.outLnStr = lineStride;
    desc->Line8.outBaseAddr = (reinterpret_cast<uint32_t>(outputData));
}

// Configures the output parameters for a cnn descriptor (only for a fully connected layer); stides are automatically computed
void cnnSetupOutputFullyConnectedLayerBrief(cnnLayer *layer, void *outputData)
{
    cnnSetupOutputFullyConnectedLayer(layer, outputData, 0);
}

// Configures the vector parameters for a cnn descriptor (only for a fully connected layer)
void cnnSetupVectorsFullyConnectedLayer(cnnLayer *layer, cnnCoefficientMode mode, void *vectorData, uint32_t noOfVectors, uint32_t strideIn, uint32_t strideOut)
{
    cnnDescriptorStructure *desc = reinterpret_cast<cnnDescriptorStructure*>(layer);
    desc->Line0.type = TYPE_FULLCONN;
    desc->Line0.cm = mode;
    desc->Line1.FullyConnected.vectors = noOfVectors;
    desc->Line6.FullyConnected.vectorBaseAddr = (reinterpret_cast<uint32_t>(vectorData));
    desc->Line6.FullyConnected.vectorStrOut = strideOut;
    desc->Line7.FullyConnected.vectorStrIn = strideIn;
}

// Configures the vector parameters for a cnn descriptor (only for a fully connected layer); stides are automatically computed
void cnnSetupVectorsFullyConnectedLayerBrief(cnnLayer *layer, cnnCoefficientMode mode, void *vectorData, uint32_t noOfVectors)
{    
    cnnSetupVectorsFullyConnectedLayer(layer, mode, vectorData, noOfVectors, 0, 0);
}

// Configures the optional padding parameters for a cnn descriptor
void cnnSetupPadding(cnnLayer *layer, uint32_t mode)
{
    cnnDescriptorStructure *desc = reinterpret_cast<cnnDescriptorStructure*>(layer);
    desc->Line2.ConvolutionPooling.padEn = 1;
    desc->Line2.ConvolutionPooling.padType = static_cast<cnnPadMode>(mode);
}

// Configures the optional ReLu parameters for a cnn descriptor (only for convolution and fully connected layers)
void cnnSetupReLu(cnnLayer *layer, uint16_t t0, uint16_t a0, uint16_t a1)
{
    cnnDescriptorStructure *desc = reinterpret_cast<cnnDescriptorStructure*>(layer);
    desc->Line4.reluEn = 1;
    desc->Line4.t0 = t0;
    desc->Line4.a0 = a0;
    desc->Line4.a1 = a1;
}

// Configures the optional ReLuX parameters for a cnn descriptor (only for convolution and fully connected layers)
void cnnSetupReLuX(cnnLayer *layer, uint16_t x)
{
    cnnDescriptorStructure *desc = reinterpret_cast<cnnDescriptorStructure*>(layer);
    desc->Line4.reluxEn = 1;
    desc->Line3.ConvolutionPooling.avgPoolX = x;
}

// Configures the optional bias parameter for a cnn descriptor (only for convolution and fully connected layers)
void cnnSetupBias(cnnLayer *layer, void *biasData)
{
    cnnDescriptorStructure *desc = reinterpret_cast<cnnDescriptorStructure*>(layer);
    desc->Line11.biasBaseAddr = reinterpret_cast<uint32_t>(biasData);
}

// Configures the optional scale parameter for a cnn descriptor (only for convolution and fully connected layers)
void cnnSetupScale(cnnLayer *layer, void *scaleData)
{
    cnnDescriptorStructure *desc = reinterpret_cast<cnnDescriptorStructure*>(layer);
    desc->Line11.scaleBaseAddr = reinterpret_cast<uint32_t>(scaleData);
}

// Computes the output dimension (x or y) based on the input size, kernel size, stride and padding 
uint32_t calcOutputSize(uint32_t inputSize, uint32_t kernelSize, uint32_t stride, uint32_t pad)
{
    if(0 == pad)
    {
        inputSize -= (kernelSize >> 1) << 1;
    }
    if(stride > 1)
    {
        if(0 != inputSize % stride)
        {
            inputSize = (inputSize / stride) + 1;
        }
        else
        {
            inputSize = inputSize / stride;
        }
    }
    return inputSize;
}

// Configure strides and dependent variables for a convolution layer and checks for validity
int cnnFinalizeConvolutionLayer(cnnDescriptorStructure *desc)
{
    uint32_t mode = 0;
    uint32_t noOfBlocks = 0;
    uint32_t sizeOfBlock = LOCAL_RAM_SIZE;
    uint32_t chanPerBlock = 0;
    uint32_t availableBytesPerChan = 0;
    uint32_t linesPerChan = 0;
    uint32_t bytesPerPixel = 1 << (1 - desc->Line0.dm);
    uint32_t bytesPerLine = 0;
    uint32_t pixelsPerCMXLine = CMX_DATA_WIDTH / bytesPerPixel;
    uint32_t localLineStride = 0;
    uint32_t localChanStride = 0;
    uint32_t minLines = 0;
    uint32_t coeffLPB = 0;
    uint32_t coeffSetSize = 0;
    uint32_t outputX = 0;

    //Calculate input strides
    uint32_t inDataLineStr = desc->Line5.dataLnStr;
    uint32_t inDataChanStr = desc->Line5.dataChStr;
    if(0 == inDataLineStr)
    {
        //Input data strides were not given by the user -> compute them
        inDataLineStr = bytesPerPixel * desc->Line1.ConvolutionPooling.inputWidth;
        inDataChanStr = inDataLineStr * desc->Line1.ConvolutionPooling.inputHeight;
    }

    //Calculate local line stride
    localLineStride = desc->Line1.ConvolutionPooling.inputWidth / pixelsPerCMXLine;
    if(0 != desc->Line1.ConvolutionPooling.inputWidth % pixelsPerCMXLine)
    {
        localLineStride++;
    }

    //Get the optimal operation mode
    mode = getMode(desc->Line1.ConvolutionPooling.inputChannels);
    noOfBlocks = (1 << mode);
    sizeOfBlock >>= mode;
    chanPerBlock = desc->Line1.ConvolutionPooling.inputChannels / noOfBlocks;
    availableBytesPerChan = sizeOfBlock / chanPerBlock;
    bytesPerLine = localLineStride * pixelsPerCMXLine * bytesPerPixel;

    //Calculate lines per channel
    linesPerChan =  availableBytesPerChan / bytesPerLine;
    if(linesPerChan > desc->Line1.ConvolutionPooling.inputHeight)
    {
        linesPerChan = desc->Line1.ConvolutionPooling.inputHeight;
    }
    if(linesPerChan > 512)
    {
        return 1; //Unsupported configuration
    }

    localChanStride = linesPerChan * localLineStride;
    minLines = desc->Line2.ConvolutionPooling.kernelHeight + desc->Line3.ConvolutionPooling.poolKernelHeight;  //or is it max between the two ???
    coeffLPB = chanPerBlock * desc->Line2.ConvolutionPooling.kernelHeight * desc->Line2.ConvolutionPooling.kernelWidth;
    if(coeffLPB > 256)
    {
    	return 1; //Unsupported configuration
    }
    coeffSetSize = desc->Line2.ConvolutionPooling.kernelHeight * desc->Line2.ConvolutionPooling.kernelWidth;
    outputX = calcOutputSize(desc->Line1.ConvolutionPooling.inputWidth, desc->Line2.ConvolutionPooling.kernelWidth, desc->Line2.ConvolutionPooling.chStride, desc->Line2.ConvolutionPooling.padEn);

    //Calculate output strides
    uint32_t outDataLineStr = desc->Line7.ConvolutionPooling.outLnStr;
    uint32_t outDataChanStr = desc->Line8.outChStr;
    if(0 == outDataLineStr)
    {
        //Output data strides were not given by the user -> compute them
        uint32_t outputY = calcOutputSize(desc->Line1.ConvolutionPooling.inputHeight, desc->Line2.ConvolutionPooling.kernelHeight, desc->Line2.ConvolutionPooling.chStride, desc->Line2.ConvolutionPooling.padEn);
        outDataLineStr = bytesPerPixel * outputX;
        outDataChanStr = outDataLineStr * outputY;
    }

    //Calculate coefficient strides
    uint32_t coeffChStrideIn = desc->Line7.ConvolutionPooling.coeffChStrIn;
    uint32_t coeffChStrideOut = desc->Line6.Convolution.coeffChStrOut;
    uint32_t bytesPerCoeffSet = coeffSetSize;
    if(0 == coeffChStrideIn)
    {
        //Coefficient strides were not given by the user -> compute them        
        switch(desc->Line0.cm)
        {
            case FP16_COEFF:
                //Coeff stride in is the distance between 2 input channels
                bytesPerCoeffSet *= sizeof(uint16_t);
                coeffChStrideIn = bytesPerCoeffSet * 8; 
                coeffChStrideOut = coeffChStrideIn * desc->Line1.ConvolutionPooling.inputChannels;
                break;
            case U8F_COEFF:
                //Coeff stride in is the distance between 2 ram blocks
                bytesPerCoeffSet *= chanPerBlock;
                if(0 != bytesPerCoeffSet % 2)
                {
                    bytesPerCoeffSet++;
                }                
                coeffChStrideIn = bytesPerCoeffSet * 8; 
                coeffChStrideOut = coeffChStrideIn * noOfBlocks;
                break;
            case FOUR_BIT_PLLTZD:                          
                break;
            case TWO_BIT_PLLTZD:
                break;
            case ONE_BIT_PLLTZD:
            case ONE_BIT_DIRECT:
                break;
        }
    }

    //Update cnn structure with computed data
    desc->Line0.mode = static_cast<cnnOperationMode>(mode);
    desc->Line1.ConvolutionPooling.inputHeight -= 1;
    desc->Line1.ConvolutionPooling.inputWidth -= 1;
    desc->Line1.ConvolutionPooling.inputChannels -= 1;
    desc->Line1.ConvolutionPooling.outputChannels -= 1;
    desc->Line2.ConvolutionPooling.chPerRamBlock = chanPerBlock - 1;
    desc->Line2.ConvolutionPooling.chStride -= 1;
    desc->Line2.ConvolutionPooling.kernelHeight -= 1;
    desc->Line2.ConvolutionPooling.kernelWidth -= 1;
    desc->Line3.ConvolutionPooling.poolKernelHeight -= 1;
    desc->Line3.ConvolutionPooling.poolKernelWidth -= 1;
    desc->Line5.dataLnStr = inDataLineStr;
    desc->Line5.dataChStr = inDataChanStr;
    desc->Line7.ConvolutionPooling.outLnStr = outDataLineStr;
    desc->Line7.ConvolutionPooling.coeffChStrIn = coeffChStrideIn;
    desc->Line6.Convolution.coeffChStrOut = coeffChStrideOut;
    desc->Line8.outChStr = outDataChanStr;
    desc->Line9.ConvolutionPooling.linesPerCh = linesPerChan - 1;
    desc->Line9.ConvolutionPooling.localLs = localLineStride;
    desc->Line9.ConvolutionPooling.localCs = localChanStride;
    desc->Line10.ConvolutionPooling.minLines = minLines;
    desc->Line10.ConvolutionPooling.coeffLpb = coeffLPB - 1;
    desc->Line10.ConvolutionPooling.css = coeffSetSize - 1;
    desc->Line10.ConvolutionPooling.outputX = outputX;
    return 0;
}

// Configure strides and dependent variables for a pooling layer and checks for validity
int cnnFinalizePoolingLayer(cnnDescriptorStructure *desc)
{
    uint32_t mode = 0;
    uint32_t noOfBlocks = 0;
    uint32_t sizeOfBlock = LOCAL_RAM_SIZE;
    uint32_t chanPerBlock = 0;
    uint32_t availableBytesPerChan = 0;
    uint32_t linesPerChan = 0;
    uint32_t bytesPerPixel = 1 << (1 - desc->Line0.dm);
    uint32_t bytesPerLine = 0;
    uint32_t pixelsPerCMXLine = CMX_DATA_WIDTH / bytesPerPixel;
    uint32_t localLineStride = 0;
    uint32_t localChanStride = 0;
    uint32_t minLines = 0;
    uint32_t outputX = 0;

    //Calculate input strides
    uint32_t inDataLineStr = desc->Line5.dataLnStr;
    uint32_t inDataChanStr = desc->Line5.dataChStr;
    if(0 == inDataLineStr)
    {
        //Input data strides were not given by the user -> compute them
        inDataLineStr = bytesPerPixel * desc->Line1.ConvolutionPooling.inputWidth;
        inDataChanStr = inDataLineStr * desc->Line1.ConvolutionPooling.inputHeight;
    }

    //Calculate local line stride
    localLineStride = desc->Line1.ConvolutionPooling.inputWidth / pixelsPerCMXLine;
    if(0 != desc->Line1.ConvolutionPooling.inputWidth % pixelsPerCMXLine)
    {
        localLineStride++;
    }

    //Get the optimal operation mode
    mode = getMode(desc->Line1.ConvolutionPooling.inputChannels);
    noOfBlocks = (1 << mode);
    sizeOfBlock >>= mode;
    chanPerBlock = desc->Line1.ConvolutionPooling.inputChannels / noOfBlocks;
    availableBytesPerChan = sizeOfBlock / chanPerBlock;
    bytesPerLine = localLineStride * pixelsPerCMXLine * bytesPerPixel;

    //Calculate lines per channel
    linesPerChan =  availableBytesPerChan / bytesPerLine;
    if(linesPerChan > desc->Line1.ConvolutionPooling.inputHeight)
    {
        linesPerChan = desc->Line1.ConvolutionPooling.inputHeight;
    }
    if(linesPerChan > 512)
    {
        return 1; //Unsupported configuration
    }

    localChanStride = linesPerChan * localLineStride;

    minLines = desc->Line3.ConvolutionPooling.poolKernelHeight;

    outputX = calcOutputSize(desc->Line1.ConvolutionPooling.inputWidth, desc->Line3.ConvolutionPooling.poolKernelWidth, desc->Line2.ConvolutionPooling.chStride, desc->Line2.ConvolutionPooling.padEn);

    //Calculate output strides
    uint32_t outDataLineStr = desc->Line7.ConvolutionPooling.outLnStr;
    uint32_t outDataChanStr = desc->Line8.outChStr;
    if(0 == outDataLineStr)
    {
        //Output data strides were not given by the user -> compute them
        uint32_t outputY = calcOutputSize(desc->Line1.ConvolutionPooling.inputHeight, desc->Line3.ConvolutionPooling.poolKernelHeight, desc->Line2.ConvolutionPooling.chStride, desc->Line2.ConvolutionPooling.padEn);
        outDataLineStr = bytesPerPixel * outputX;
        outDataChanStr = outDataLineStr * outputY;
    }

    //Update cnn structure with computed data
    desc->Line0.mode = static_cast<cnnOperationMode>(mode);
    desc->Line1.ConvolutionPooling.inputHeight -= 1;
    desc->Line1.ConvolutionPooling.inputWidth -= 1;
    desc->Line1.ConvolutionPooling.inputChannels -= 1;
    desc->Line1.ConvolutionPooling.outputChannels -= 1;
    desc->Line2.ConvolutionPooling.chPerRamBlock = chanPerBlock - 1;
    desc->Line2.ConvolutionPooling.chStride -= 1;
    desc->Line2.ConvolutionPooling.kernelHeight -= 1;
    desc->Line2.ConvolutionPooling.kernelWidth -= 1;
    desc->Line3.ConvolutionPooling.poolKernelHeight -= 1;
    desc->Line3.ConvolutionPooling.poolKernelWidth -= 1;
    desc->Line5.dataLnStr = inDataLineStr;
    desc->Line5.dataChStr = inDataChanStr;
    desc->Line7.ConvolutionPooling.outLnStr = outDataLineStr;
    desc->Line8.outChStr = outDataChanStr;
    desc->Line9.ConvolutionPooling.linesPerCh = linesPerChan - 1;
    desc->Line9.ConvolutionPooling.localLs = localLineStride;
    desc->Line9.ConvolutionPooling.localCs = localChanStride;
    desc->Line10.ConvolutionPooling.minLines = minLines;
    desc->Line10.ConvolutionPooling.outputX = outputX;
    return 0;
}

// Configure strides and dependent variables for a fully connected layer and checks for validity
int cnnFinalizeFullyConnectedLayer(cnnDescriptorStructure *desc)
{
    uint32_t mode = 0;
    uint32_t noOfBlocks = 0;
    uint32_t dataPerBlock = 0;
    uint32_t bytesPerPixel = 1 << (1 - desc->Line0.dm);
    uint32_t pixelsPerCMXLine = CMX_DATA_WIDTH / bytesPerPixel;
    uint32_t localLineStr = 0;
    uint32_t localBlkStr = 0;
    uint32_t vectLPB = 0;
    //uint32_t coeffsPer2Bytes = 1;

    //Get the optimal operation mode
    mode = getMode(desc->Line1.FullyConnected.inputWidth);
    noOfBlocks = (1 << mode);
    dataPerBlock = desc->Line1.FullyConnected.inputWidth / noOfBlocks;

    //Calculate input strides
    uint32_t inDataLineStr = desc->Line5.dataLnStr;
    uint32_t inDataBlkStr = desc->Line5.dataChStr;
    if(0 == inDataLineStr)
    {
        //Input data strides were not given by the user -> compute them
        inDataLineStr = bytesPerPixel;
        inDataBlkStr = inDataLineStr * dataPerBlock;
    }

    //Calculate output strides
	uint32_t outDataLineStr = desc->Line7.FullyConnected.outLnStr;
	uint32_t outDataBlkStr = desc->Line8.outChStr;
	if(0 == outDataLineStr)
	{
		//Output data strides were not given by the user -> compute them
		outDataLineStr = bytesPerPixel;
		outDataBlkStr = outDataLineStr * dataPerBlock;
	}

    //Calculate local line stride
    localLineStr = desc->Line1.FullyConnected.inputWidth / pixelsPerCMXLine;
    if(0 != desc->Line1.FullyConnected.inputWidth % pixelsPerCMXLine)
    {
        localLineStr++;
    }

    // uint32_t coeff_mode = desc->Line0.cm;
    // if(ONE_BIT_DIRECT == coeff_mode)
    // {
    //     coeff_mode = ONE_BIT_PLLTZD;
    // }
    // coeffsPer2Bytes = 1 << coeff_mode;

    localBlkStr = localLineStr;

    vectLPB = desc->Line1.FullyConnected.inputWidth / noOfBlocks;
    // if(0 != vectLPB % coeffsPer2Bytes)
	// {
	// 	vectLPB = (vectLPB / coeffsPer2Bytes) + 1;
	// }
	// else
	// {
	// 	vectLPB = vectLPB / coeffsPer2Bytes;
	// }
    if(vectLPB > 256)
    {
    	return 1; //Unsupported configuration
    }

    //Calculate vector strides
    uint32_t vectStrIn = desc->Line7.FullyConnected.vectorStrIn;
    uint32_t vectStrOut = desc->Line6.FullyConnected.vectorStrOut;
    if(0 == vectStrIn)
    {
        //Vector strides were not given by the user -> compute them
        switch(desc->Line0.cm)
        {
            case FP16_COEFF:
                vectStrIn = inDataBlkStr * 8;
    	        vectStrOut = vectStrIn * noOfBlocks;
                break;
            case U8F_COEFF:
                vectStrIn = inDataBlkStr * (8 / sizeof(uint16_t));
    	        vectStrOut = vectStrIn * noOfBlocks;
                break;
            case FOUR_BIT_PLLTZD:                          
                break;
            case TWO_BIT_PLLTZD:
                break;
            case ONE_BIT_PLLTZD:
            case ONE_BIT_DIRECT:
                break;
        }    	
    }

    //Update cnn structure with computed data
    desc->Line0.mode = static_cast<cnnOperationMode>(mode);
    desc->Line1.FullyConnected.inputWidth -= 1;
    desc->Line1.FullyConnected.vectors -= 1;
    desc->Line2.FullyConnected.dataPerRamBlock = dataPerBlock - 1;
    desc->Line5.dataLnStr = inDataLineStr;
    desc->Line5.dataChStr = inDataBlkStr;
    desc->Line6.FullyConnected.vectorStrOut = vectStrOut;
    desc->Line7.FullyConnected.vectorStrIn = vectStrIn;
    desc->Line7.FullyConnected.outLnStr = outDataLineStr;
    desc->Line8.outChStr = outDataBlkStr;
    desc->Line9.FullyConnected.localLs = localLineStr;
    desc->Line9.FullyConnected.localBs = localBlkStr;
    desc->Line10.FullyConnected.vectorLPB = vectLPB - 1;
    return 0;
}

// Configures the dependent values from the cnn descriptor and checks for validity
int cnnFinalizeLayer(cnnLayer *layer)
{
    cnnDescriptorStructure *desc = reinterpret_cast<cnnDescriptorStructure*>(layer);
    switch(desc->Line0.type)
    {
    case TYPE_CONV:
    case TYPE_CONVPOOL:
    	return cnnFinalizeConvolutionLayer(desc);
    case TYPE_POOL:
    	return cnnFinalizePoolingLayer(desc);
    case TYPE_FULLCONN:
    	return cnnFinalizeFullyConnectedLayer(desc);
    }
    return 1;
}

// Unlinks a cnn descriptor from it's child and returns the child's address
cnnLayer *cnnUnlink(cnnLayer *layer)
{
    cnnDescriptorStructure *desc = reinterpret_cast<cnnDescriptorStructure*>(layer);
    cnnLayer *prev = reinterpret_cast<cnnLayer*>(desc->Line0.linkAddress);
    desc->Line0.linkAddress = 0;
    return prev;
}

// Links all cnn descriptors given as parameters
void cnnLinkList(uint32_t noOfLayers, ...)
{
    uint32_t index = 0;
    cnnDescriptorStructure *prev = NULL;
    cnnDescriptorStructure *next = NULL;
    va_list layers;
    va_start(layers, noOfLayers);
    if(noOfLayers > 0)
    {
        prev = va_arg(layers, cnnDescriptorStructure*);
        for(index = 0; index < (noOfLayers - 1); index++)
        {
            next = va_arg(layers, cnnDescriptorStructure*);
            prev->Line0.linkAddress = reinterpret_cast<uint32_t>(next);
            prev = next;
        }
    }
    va_end(layers);
}

// Links all cnn descriptors in an array of descriptors
void cnnLinkArray(uint32_t noOfLayers, cnnLayer *layers)
{
    uint32_t index = 0;
    cnnDescriptorStructure *desc = reinterpret_cast<cnnDescriptorStructure*>(layers);

    if(noOfLayers > 0)
    {
        for(index = 0; index < (noOfLayers - 1); index++)
        {
            desc->Line0.linkAddress = reinterpret_cast<uint32_t>(desc);
            desc += 1;
        }
        desc->Line0.linkAddress = 0;
    }
}

// Links two cnn descriptors together (parent points to child - the child is executed after the parent)
void cnnLinkLayers(cnnLayer *parent, cnnLayer *child)
{
    GeneralLinkStructure *root = reinterpret_cast<GeneralLinkStructure*>(parent);
    root->linkAddress = reinterpret_cast<uint32_t>(child);
}

// ISR simulation function for CNN
void isr(ApbSlaveInterface *pObj) {
    pObj = pObj;
}

// Starts the CNN simulation on a linked cnn descriptor list
void cnnStart(cnnBlock block, cnnLane lane, cnnLayer *layers)
{
    block = block;  //The SW model does not care about the block or lane
    lane = lane;
    size_t list_address = (reinterpret_cast<size_t>(layers));
    CNNSoftwareModel CNNSoftwareModelObj(isr);

    CNNSoftwareModelObj.SetCmxBaseAddr((uint8_t*)0x70000000);
    CNNSoftwareModelObj.SetDdrBaseAddr((uint8_t*)0x80000000);
    CNNSoftwareModelObj.SetVerbose(1);    
    CNNSoftwareModelObj.ApbWrite(CNN0_CNN_INT_EN_ADR, 0x3FFFF);
    CNNSoftwareModelObj.ApbWrite(CNN0_CNN_LINK_SETUP_CFG0_ADR, list_address);
    CNNSoftwareModelObj.ApbWrite(CNN0_CNN_LINK_CFG0_ADR, 1);
}

// Waits for the completion on both lanes of a cnn block
void cnnWaitForBlock(cnnBlock block)
{
    //not used on the PC simulation
}

// Computes the number of needed temporary buffers and it's maximum size
void cnnGetMaxBufferSizeFromBlob(uint8_t *blobData, uint32_t *noOfBuffers, uint32_t *bufferSize)
{
    cnnDescriptorStructure *desc = reinterpret_cast<cnnDescriptorStructure*>(blobData);
    uint32_t blobStartAddr = reinterpret_cast<uint32_t>(blobData);
    uint32_t maxBufferIndex = 0;
    uint32_t maxBufferSize = 0;
    uint32_t inputBufferIndex = 0;
    uint32_t outputBufferIndex = 0;
    uint32_t inputBufferSize = 0;
    uint32_t outputBufferSize = 0;
    while(NULL != desc)
    {
        inputBufferIndex = desc->Line4.dataBaseAddr >> 24; //The inputBufferIndex is the MSB byte of the address; It specifies which temp buffer to use as input
        outputBufferIndex = desc->Line8.outBaseAddr >> 24; //The outputBufferIndex is the MSB byte of the address; It specifies which temp buffer to use as output

        switch(desc->Line0.type)
        {
        case TYPE_CONV:
            /* fall through */
        case TYPE_POOL:
            /* fall through */
        case TYPE_CONVPOOL:
            inputBufferSize = desc->Line5.dataChStr * (desc->Line1.ConvolutionPooling.inputChannels + 1);
            outputBufferSize = desc->Line8.outChStr * (desc->Line1.ConvolutionPooling.outputChannels + 1);
            break;
        case TYPE_FULLCONN:
            inputBufferSize = desc->Line5.dataChStr * (desc->Line1.FullyConnected.inputWidth + 1);
            outputBufferSize = desc->Line8.outChStr * (desc->Line1.FullyConnected.vectors + 1);
            break;
        }

        if(maxBufferIndex < inputBufferIndex)
        {
            maxBufferIndex = inputBufferIndex;
        }
        if(maxBufferIndex < outputBufferIndex)
        {
            maxBufferIndex = outputBufferIndex;
        }
        if(maxBufferSize < inputBufferSize)
        {
            maxBufferSize = inputBufferSize;
        }
        if(maxBufferSize < outputBufferSize)
        {
            maxBufferSize = outputBufferSize;
        }

        //Advance to next cnn struct (only if it's not the last one)
        if(0 == desc->Line0.linkAddress)
        {
            desc = NULL;
        }
        else
        {
            desc = reinterpret_cast<cnnDescriptorStructure*>(blobStartAddr + desc->Line0.linkAddress);
        }
    }
	*noOfBuffers = maxBufferIndex - 2; //first 2 buffer indexes (1 & 2) are used as input and output
    *bufferSize = maxBufferSize;
}

// Binds the blob data to a place in memory and links the input, output and temporary buffers to the cnn structures
void cnnConfigureBlobBuffers(uint8_t *blobData, uint8_t *inputBuffer, uint8_t *outputBuffer, uint8_t **tempBuffers)
{
    cnnDescriptorStructure *desc = reinterpret_cast<cnnDescriptorStructure*>(blobData);
    uint32_t blobStartAddr = reinterpret_cast<uint32_t>(blobData);
    while(NULL != desc)
    {
        //Rewrite input and output buffers
        uint32_t inputBufferIndex = desc->Line4.dataBaseAddr >> 24; //The inputBufferIndex is the MSB byte of the address; It specifies which temp buffer to use as input
        uint32_t outputBufferIndex = desc->Line8.outBaseAddr >> 24; //The outputBufferIndex is the MSB byte of the address; It specifies which temp buffer to use as output
        uint32_t outputBufferOffset = desc->Line8.outBaseAddr & 0xFFFFFF; //Last 3 bytes of the address is the output offset (used for merging data layers)

		if(1 == inputBufferIndex)
        {
            desc->Line4.dataBaseAddr = reinterpret_cast<uint32_t>(inputBuffer);
        }
        else
        {
			desc->Line4.dataBaseAddr = reinterpret_cast<uint32_t>(tempBuffers[inputBufferIndex - 3]);
        }
		if(2 == outputBufferIndex)
        {
			desc->Line8.outBaseAddr = reinterpret_cast<uint32_t>(outputBuffer) + outputBufferOffset;
        }
        else
        {
			desc->Line8.outBaseAddr = reinterpret_cast<uint32_t>(tempBuffers[outputBufferIndex - 3]) + outputBufferOffset;
        }

        //Rewrite weights & bias addresses
        switch(desc->Line0.type)
        {
        case TYPE_CONV:
            /* fall through */
        case TYPE_CONVPOOL:
            /* fall through */
        case TYPE_POOL:
            desc->Line6.Convolution.coeffBaseAddr += blobStartAddr;
            break;
        case TYPE_FULLCONN:
            desc->Line6.FullyConnected.vectorBaseAddr += blobStartAddr;
            break;
        }
        if(0 != desc->Line11.biasBaseAddr)
        {
            desc->Line11.biasBaseAddr += blobStartAddr;
        }

        //Rewrite the link address (only if it's not the last one)
        if(0 != desc->Line0.linkAddress)
        {
            desc->Line0.linkAddress += blobStartAddr;
        }
        //Advance to next cnn structure
        desc = reinterpret_cast<cnnDescriptorStructure*>(desc->Line0.linkAddress);
    }
}

// Starts the cnn hardware
void cnnStartBlob(cnnBlock block, cnnLane lane, uint8_t *blobData)
{
   cnnLayer *layers = reinterpret_cast<cnnLayer*>(blobData);
   cnnStart(block, lane, layers);
}

/// @}
