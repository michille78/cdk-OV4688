// -----------------------------------------------------------------------------
// Copyright (C) 2016 Movidius Ltd. All rights reserved
//
// Company            Movidius
// Author             Titus BIRAU (titus.birau@movidius.com)
//
// Description        MotEst HW filter API source code
// -----------------------------------------------------------------------------

#include "hwMotEstApi.h"

#include <stdio.h>

#include <MotEstAccelerator.h>
#include <registersMyriad2.h>


// -----------------------------------------------------------------------------
//
//                    MACROS
//
// -----------------------------------------------------------------------------

// NULL pointer definition
#ifndef  NULL
#define  NULL                               (void *  ) 0
#endif

// verbosity - update this in order to change verbosity
#define  VERBOSITY                          (bool    ) false

#define  MOTEST_CFG_PARAM_U32__NOT_CFGD     (uint32_t) 0xCDCDCDCD


// -----------------------------------------------------------------------------
//
//                    DATA TYPES
//
// -----------------------------------------------------------------------------

typedef enum {
    HW_API__NONE   ,
    HW_API__INIT   ,
    HW_API__CONFIG ,
    HW_API__START  ,
    HW_API__WAIT
}   hwiApiCalls_t;


// -----------------------------------------------------------------------------
//
//                    GLOBAL VARIABLES
//
// -----------------------------------------------------------------------------

// used for API calling sequence error handle
static bool                 motestInitCalled     = false                    ;
static bool                 motestConfigCalled   = false                    ;
static bool                 motestStartCalled    = false                    ;
static mcfgStatus_t         motestConfigStatus   = MCFG_STS__NO_ERROR       ;

// verbosity
static bool                 motestVerbose        = VERBOSITY                ;

// configured number coords that have been processed and output which is
// used to generate BLK_NUM_COORDS_DONE interrupt (substract -1)
static uint32_t             motestBlockVectors                              ;
// total number of coordinates to be processed in this run
static uint32_t             motestTotalVectors                              ;

// coords buffer when Search Mode DIRECTED
static uint32_t             motestCoordsDirected [SRCH_MODE__DIRECTED__MAX] ;

// reference to the current MotEst configuration
static MotEstAccelerator *  motEst                                          ;


// -----------------------------------------------------------------------------
//
//                    LOCAL FUNCTIONS
//
// -----------------------------------------------------------------------------

// Interrupt Service Routine - this is a callback called by the MotEst model
//                             object when any enabled interrupt occurs.
static void motestISRCallback( ApbSlaveInterface * motEst )
{
    uint32_t data = motEst->ApbRead (MOTEST0_INT_STATUS_ADR     );
                    motEst->ApbWrite(MOTEST0_INT_CLEAR_ADR, data);

    if  ( motestVerbose )
    {
        if  ( data & (1 << MotEstIrq::BLK_NUM_MVS_DONE_BIT) )
            printf("Computed %d motion vectors.\n", motestBlockVectors);
        if  ( data & (1 << MotEstIrq::TOT_NUM_MVS_DONE_BIT) )
            printf("Computed all the %d motion vectors.\n", motestTotalVectors);
    }
}

// list the provided MotEst configuration
static void listMotEstConfig( hwiMotEstConfig_t * motestCfg )
{
    printf("MotEst Configuration                                 \n");
    printf("-------------------------------------------          \n");
    printf("refImgBuffer        %p\n", motestCfg->refImgBuffer      );
    printf("srcImgBuffer        %p\n", motestCfg->srcImgBuffer      );
    printf("imgWidth            %d\n", motestCfg->imgWidth          );
    printf("imgHeight           %d\n", motestCfg->imgHeight         );
    printf("imgStride           %d\n", motestCfg->imgStride         );
    printf("results             %p\n", motestCfg->results           );
    printf("rasterStepSize      %d\n", motestCfg->rasterStepSize    );
    printf("srchMode            %d\n", motestCfg->srchMode          );
    printf("srchCoords          %p\n", motestCfg->srchCoords        );
    //for ( uint32_t i = 0; i < motestCfg->srchNumBlks; i++ )
    //    printf("srchCoords[%2d]      <%2d:%2d>\n", i,
    //                                 motestCfg->srchCoords[i].xpel,
    //                                 motestCfg->srchCoords[i].ypel);
    printf("srchNumBlks         %d\n", motestCfg->srchNumBlks       );
    printf("outMode             %d\n", motestCfg->outMode           );
    printf("disableHalfPel      %d\n", motestCfg->disableHalfPel    );
    printf("disableQuarterPel   %d\n", motestCfg->disableQuarterPel );
    printf("-------------------------------------------          \n");

    return;
}

// -----------------------------------------------------------------------------
//
//                    F U N C T I O N S
//
// -----------------------------------------------------------------------------


// -----------------------------------------------------------------------------
//
// API Function       hwiMotEstInit
//
// Description        initializes MotEst Hw filter configuration parameters
//                    to default OR not configured values
//
// Parameters         motEstConfig - contains address of configuration
//                                   structure which need to be initialized
//
// Return             none
//
// Constraints        none
//
// -----------------------------------------------------------------------------
void hwiMotEstInit( hwiMotEstConfig_t * motestCfg )
{
    // --------- INITIALIZATION ------------------------------------------------

    // REF_IMG
    motestCfg->refImgBuffer         = NULL                                  ;
    // SRC_IMG
    motestCfg->srcImgBuffer         = NULL                                  ;

    // image parameters
    motestCfg->imgWidth             = MOTEST_CFG_PARAM_U32__NOT_CFGD        ;
    motestCfg->imgHeight            = MOTEST_CFG_PARAM_U32__NOT_CFGD        ;
    motestCfg->imgStride            = MOTEST_CFG_PARAM_U32__NOT_CFGD        ;

    // results buffer
    motestCfg->results              = NULL                                  ;

    // raster step
    motestCfg->rasterStepSize       = RASTER_STEP_SIZE__DFLT                ; // default

    // search mode
    motestCfg->srchMode             = SRCH_MODE__RASTER                     ; // default
    motestCfg->srchCoords           = NULL                                  ;
    motestCfg->srchNumBlks          = MOTEST_CFG_PARAM_U32__NOT_CFGD        ;

    // out mode
    motestCfg->outMode              = OUT_MODE__ONLY_MVS                    ; // default


    // disable Half/Quarter pel
    motestCfg->disableHalfPel       = false                                 ; // default
    motestCfg->disableQuarterPel    = false                                 ; // default



    // --------- CALLING SEQUENCE & ERROR HANDLER ------------------------------

    // calling sequence error handling
    motestInitCalled   = true ;
    motestConfigCalled = false;
    motestStartCalled  = false;

    // current MotEst configuration status
    motestConfigStatus = MCFG_STS__NO_ERROR;


    return;
}


// -----------------------------------------------------------------------------
//
// Local Function     checkMotEstConfig
//
// Description        implements basic error handler for provided configuration
//                    parameters
//
// Parameters         motEstConfig - structure containing configuration
//                                   parameters that need to be checked
//
// Return             configuration error status (mcfgStatus_t)
//
// Constraints        shall be called only from hwiMotEstConfig() function
//
// -----------------------------------------------------------------------------
mcfgStatus_t checkMotEstConfig( hwiMotEstConfig_t * motestCfg )
{
    // check REF_IMG
    if  ( motestCfg->refImgBuffer == NULL )
        return MCFG_STS__REF_IMG_NOT_CFGD;

    // check SRC_IMG
    if  ( motestCfg->srcImgBuffer == NULL )
        return MCFG_STS__SRC_IMG_NOT_CFGD;

    // check image parameters/rezolution
    if  (  ( motestCfg->imgWidth  == MOTEST_CFG_PARAM_U32__NOT_CFGD )
        || ( motestCfg->imgHeight == MOTEST_CFG_PARAM_U32__NOT_CFGD )
        || ( motestCfg->imgStride == MOTEST_CFG_PARAM_U32__NOT_CFGD ) )
        return MCFG_STS__IMG_PARAMS_NOT_CFGD;

    // check image size
    if  (  ( motestCfg->imgWidth  > MOTEST_IMG_MAX_WIDTH  )
        || ( motestCfg->imgHeight > MOTEST_IMG_MAX_HEIGHT ) )
        return MCFG_STS__IMG_PARAMS_INVALID;

    // check results buffer
    if  ( motestCfg->results == NULL )
        return MCFG_STS__RESULTS_BUFF_NOT_CFGD;

    // check raster step size to be multiple of 8 and less than 32
    if  (  ( motestCfg->rasterStepSize > RASTER_STEP_SIZE__MAX  )
        || ( motestCfg->rasterStepSize % RASTER_STEP_SIZE__DFLT ) )
        return MCFG_STS__RASTER_STEP_INVALID;

    // check search mode
    if  ( SRCH_MODE__DIRECTED == motestCfg->srchMode )
    {
        if  (  ( motestCfg->srchCoords  == NULL                           )
            || ( motestCfg->srchNumBlks == MOTEST_CFG_PARAM_U32__NOT_CFGD ) )
            return MCFG_STS__SRCH_COORDS_NOT_CFGD;
    }
    else    // SRCH_MODE__RASTER
    {
        // compute total number of search blocks
        motestCfg->srchNumBlks = motestCfg->imgWidth  / motestCfg->rasterStepSize
                               * motestCfg->imgHeight / motestCfg->rasterStepSize;
    }

    // list configuration
    if  ( motestVerbose )
        listMotEstConfig(motestCfg);

    return MCFG_STS__NO_ERROR;
}


// -----------------------------------------------------------------------------
//
// API Function       hwiMotEstConfig
//
// Description        configures MotEst Filter based on provided configuration
//
// Parameters         motEstConfig - structure containing configuration
//                                   parameters provided by API user
//
// Return             configuration error status (mcfgStatus_t)
//
// Constraints        hwiMotEstInit() need to be called before
//
// -----------------------------------------------------------------------------
mcfgStatus_t hwiMotEstConfig( hwiMotEstConfig_t * motestCfg )
{
    // --------- ERROR HANDLER -------------------------------------------------

    motestConfigCalled = true;

    // check if hwiMotEstInit() was called before this
    if  ( !motestInitCalled )
        return MCFG_STS__INIT_NOT_CALLED;

    // check provided MotEst configuration
    motestConfigStatus = checkMotEstConfig( motestCfg );
    if  ( MCFG_STS__NO_ERROR != motestConfigStatus )
        return motestConfigStatus;


    // --------- MOTEST CONFIGURATION ------------------------------------------

    size_t data        = 0;
    motestBlockVectors = motestCfg->imgWidth       /
                         motestCfg->rasterStepSize ;
    motestTotalVectors = motestCfg->srchNumBlks    ;


    // create new MotEst PC model
    motEst = new MotEstAccelerator(motestISRCallback);


    // enable interrupts
    motEst->ApbWrite(MOTEST0_INT_MASK_ADR, 0x3);


    // convert srchCoords to uint32_t buffer
    if  ( motestCfg->srchMode == SRCH_MODE__DIRECTED )
        for ( uint32_t i = 0; i < motestCfg->srchNumBlks; i++ )
            motestCoordsDirected[i] = motestCfg->srchCoords[i].xpel << 16 |
                                      motestCfg->srchCoords[i].ypel <<  0 ;


    // Register: CFG                                              B I T S
    data =  motestTotalVectors                       <<  0 |   // 17:0     TOT_NUM_COORDS
            motestCfg->srchMode                      << 28 |   // 28       MODE     0 - Directed, 1 - Raster
          ((motestCfg->disableHalfPel   ) ? 0x0 :              // 30:29    VEC_RES  00  HPel disabled
           (motestCfg->disableQuarterPel) ? 0x1 :              //                   01  HPel enabled  & QPel disabled
                                            0x3 )    << 29 ;   //                   11  HPel enabled  & QPel enabled
    motEst->ApbWrite(MOTEST0_CFG_ADR,                 data);

    // Register: IMAGE_SIZE                                       B I T S
    data =  motestCfg->imgWidth                      <<  0 |   // 15:0     V_SIZE (vertical   image size)
            motestCfg->imgHeight                     << 16 ;   // 31:16    H_SIZE (horizontal image size)
    motEst->ApbWrite(MOTEST0_IMAGE_SIZE_ADR,          data);

    // Register: LINE_STRIDE                                      B I T S
    data =  motestCfg->imgStride;                              // 31:4     LINE_STRIDE
    motEst->ApbWrite(MOTEST0_LINE_STRIDE_ADR,         data);

    // Register: REF_BASE                                         B I T S
    data = (size_t)motestCfg->refImgBuffer;                    // 31:4     REF_BASE
    motEst->ApbWrite(MOTEST0_REF_BASE_ADR,            data);

    // Register: SRC_BASE                                         B I T S
    data = (size_t)motestCfg->srcImgBuffer;                    // 31:4     SRC_BASE
    motEst->ApbWrite(MOTEST0_SRC_BASE_ADR,            data);

    // Register: COORDS_BASE                                      B I T S
    data = (size_t)motestCoordsDirected;                       // 31:4     COORDS_BASE
    motEst->ApbWrite(MOTEST0_COORDS_BASE_ADR,         data);

    // Register: RES_BASE                                         B I T S
    data = (size_t)motestCfg->results;                         // 31:4     RES_BASE
    motEst->ApbWrite(MOTEST0_RES_BASE_ADR,            data);

    // Register: BLOCK_NUMBER_COORDS                              B I T S
    data = (motestBlockVectors - 1)                  <<  0 |   // 17:0     BLK_NUM_COORDS_M1 - line-by-line basis IRQ
            motestCfg->outMode                       << 23 |   // 23       OUTPUT_MODE
            motestCfg->rasterStepSize                << 24 ;   // 30:24    RASTER_STEP_SIZE
    motEst->ApbWrite(MOTEST0_BLOCK_NUMBER_COORDS_ADR, data);


    return MCFG_STS__NO_ERROR;
}


// -----------------------------------------------------------------------------
//
// API Function       hwiMotEstStart
//
// Description        starts MotEst Filter
//
// Parameters         none
//
// Return             configuration error status (mcfgStatus_t)
//
// Constraints        hwiMotEstConfig() need to be called before
//
// -----------------------------------------------------------------------------
mcfgStatus_t hwiMotEstStart()
{
    // --------- ERROR HANDLER -------------------------------------------------

    motestStartCalled = true;

    // check if hwiMotEstInit() was called before this
    if  ( !motestInitCalled )
        return MCFG_STS__INIT_NOT_CALLED;

    // check if hwiMotEstConfig() was called before this
    if  ( !motestConfigCalled )
        return MCFG_STS__CONFIG_NOT_CALLED;

    // check provided MotEst configuration status
    if  ( MCFG_STS__NO_ERROR != motestConfigStatus )
        return motestConfigStatus;


    // --------- START MOTEST FILTER -------------------------------------------

    motEst->ApbWrite(MOTEST0_CFG_ADR                    ,
                     motEst->ApbRead(MOTEST0_CFG_ADR)   |
                     MotEstAccelerator::MOT_EST_RUN_MASK);


    return MCFG_STS__NO_ERROR;
}


// -----------------------------------------------------------------------------
//
// API Function       hwiMotEstWait
//
// Description        waits for WaMotEst Filter to finish
//
// Parameters         none
//
// Return             configuration error status (mcfgStatus_t)
//
// Constraints        hwiMotEstStart() need to be called before
//
// -----------------------------------------------------------------------------
mcfgStatus_t hwiMotEstWait()
{
    // --------- ERROR HANDLER -------------------------------------------------

    // check if hwiMotEstInit() was called before this
    if  ( !motestInitCalled )
        return MCFG_STS__INIT_NOT_CALLED;

    // check if hwiMotEstConfig() was called before this
    if  ( !motestConfigCalled )
        return MCFG_STS__CONFIG_NOT_CALLED;    

    // check provided MotEst configuration status
    if  ( MCFG_STS__NO_ERROR != motestConfigStatus )
        return motestConfigStatus;

    // check if hwiMotEstStart() was called before this
    if  ( !motestStartCalled )
        return MCFG_STS__START_NOT_CALLED;


    // --------- WAIT FOR MOTEST FILTER  -----------------------------------------


    uint32_t nSearches = motEst->GetNumberOfSearches();

    printf(" Done: completed %d searches Average MV=%.2f:%.2f\n" ,
        nSearches                                                ,
        motEst->GetSumOfXvectors() / nSearches                   ,
        motEst->GetSumOfYvectors() / nSearches                   );


    // delete the instance in the end
    delete motEst;

    return MCFG_STS__NO_ERROR;
}
