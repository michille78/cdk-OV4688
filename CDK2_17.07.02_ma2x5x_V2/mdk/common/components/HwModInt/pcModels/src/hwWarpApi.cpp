// -----------------------------------------------------------------------------
// Copyright (C) 2016 Movidius Ltd. All rights reserved
//
// Company            Movidius
// Author             Titus BIRAU (titus.birau@movidius.com)
//
// Description        Warp HW filter API source code
// -----------------------------------------------------------------------------

#include "hwWarpApi.h"

#include <stdio.h>

#include <cwarp.h>
#include <config.h>

#include <registersMyriad2.h>


// -----------------------------------------------------------------------------
//
//                    MACROS
//
// -----------------------------------------------------------------------------

// NULL pointer definition
#ifndef NULL
#define NULL                                ( (   void * ) 0 )
#endif

// verbosity - update this in order to change verbosity
#define  VERBOSITY   (bool)false

// used for configuration initialization
#define WCFG_PARAM_U32__NOT_CFGD            ( ( uint32_t ) 0xCDCDCDCD )

// data alignment - to 16 Bytes target
#define DATA_ALIGN_16(x)                    ( ((x) + 15) & (~15) )

// warp IO indices (as in HW)
#define W_MESH                              0
#define W_IN                                1
#define W_OUT                               2

#ifndef BPP
#define BPP(x)                              ( x )                   // bytes per pixel
#endif

#ifndef NOP
#define NOP                                 ;                       // asm("nop")
#endif


// Register: WARP_START
//           Start Configuration
#define      WARP_START__1x_SUPER_RUN       ( (1 <<  1) \
                                            | (1 <<  0) )           // do 1x Super-Run

// Register: WARP_RUNS
//           Runs and Number of Tiles configuration
#define      WARP_RUNS__FULL_FRAME          0                       // full-frame


// -----------------------------------------------------------------------------
//
//                    DATA TYPES
//
// -----------------------------------------------------------------------------

typedef enum {
    HW_API__NONE   ,
    HW_API__INIT   ,
    HW_API__CONFIG ,
    HW_API__START  ,
    HW_API__WAIT
}   hwiApiCalls_t;


// -----------------------------------------------------------------------------
//
//                    GLOBAL VARIABLES
//
// -----------------------------------------------------------------------------

// reference to current configuration
static void *           warpApi                               ;

// Warp HW filter Base Address
static uint32_t         warpBaseAddress                       ;

// used for API calling sequence error handler
static bool             warpInitCalled   = false              ;
static bool             warpConfigCalled = false              ;
static bool             warpStartCalled  = false              ;
static wcfgStatus_t     warpConfigStatus = WCFG_STS__NO_ERROR ;

// verbosity
static bool             verbose          = VERBOSITY          ;

// -----------------------------------------------------------------------------
//
//                    WARP PC MODEL
//
// -----------------------------------------------------------------------------

static inline void SET_REG_WORD(uint32_t addr, uint32_t data)
{
    if  ( verbose ) {
        printf("SET_REG_WORD(0x%08x, 0x%08x);\n", addr, data);
        fflush(stdout);
    }

    apbWarpWrite(warpApi, addr, data);
}

static inline uint32_t GET_REG_WORD_VAL(uint32_t addr)
{
    uint32_t data = apbWarpRead(warpApi, addr);

    if  ( verbose ) {
        printf("GET_REG_WORD_VAL(0x%08x, 0x%08x);\n", addr, data);
        fflush(stdout);
    }

    return data;
}

static void eor_callback (int) {}
static void eosr_callback(int) {}


// -----------------------------------------------------------------------------
//
//                    F U N C T I O N S
//
// -----------------------------------------------------------------------------


// -----------------------------------------------------------------------------
//
// API Function       hwiWarpInit
//
// Description        initializes Warp Hw filter configuration parameters
//                    to default OR not configured values
//
// Parameters         warpConfig - contains address of configuration
//                                 structure which need to be initialized
//
// Return             none
//
// Constraints        none
//
// -----------------------------------------------------------------------------
void hwiWarpInit( hwiWarpConfig_t * warpConfig )
{
    // --------- INITIALIZATION ------------------------------------------------
    
    // WARP INSTANCE
    warpConfig->warpInstance    = WARP__0                               ;   // default

    // MESH
    warpConfig->mesh            = NULL                                  ;
    warpConfig->meshWidth       = WCFG_PARAM_U32__NOT_CFGD              ;
    warpConfig->meshHeight      = WCFG_PARAM_U32__NOT_CFGD              ;

    // INPUT
    warpConfig->inputBuffer     = NULL                                  ;
    warpConfig->inputWidth      = WCFG_PARAM_U32__NOT_CFGD              ;
    warpConfig->inputHeight     = WCFG_PARAM_U32__NOT_CFGD              ;

    // OUTPUT
    warpConfig->outputBuffer    = NULL                                  ;
    warpConfig->outputWidth     = WCFG_PARAM_U32__NOT_CFGD              ;
    warpConfig->outputHeight    = WCFG_PARAM_U32__NOT_CFGD              ;

    // Register: WARP_MODE
    warpConfig->interpMode      = WARP_MODE__BICUBIC                    ;   // default

    // Register: WARP_EDGECOLOUR
    warpConfig->edgeColorMode   = WARP_EDGECOLOUR__COLOR(COLOR__BLACK)  ;   // default

    // Register: WARP_MESH
    warpConfig->meshMode        = WARP_MESH__SPARSE_F32                 ;   // default

    // Register: WARP_MATx
    warpConfig->matrix3x3       = NULL                                  ;   // default


    // --------- CALLING SEQUENCE & ERROR HANDLER ------------------------------

    // calling sequence error handling
    warpInitCalled   = true ;
    warpConfigCalled = false;
    warpStartCalled  = false;

    // current warp configuration status
    warpConfigStatus = WCFG_STS__NO_ERROR;

    warpBaseAddress  = WCFG_PARAM_U32__NOT_CFGD;

    return;
}


// -----------------------------------------------------------------------------
//
// Local Function     checkWarpConfig
//
// Description        implements basic error handler for provided configuration
//                    parameters
//
// Parameters         warpConfig - structure containing configuration
//                                 parameters that need to be checked
//
// Return             configuration error status (wcfgStatus_t)
//
// Constraints        shall be called only from hwiWarpConfig() function
//
// -----------------------------------------------------------------------------
wcfgStatus_t checkWarpConfig( hwiWarpConfig_t * warpConfig )
{
    // check MESH configuration
    if  ( NULL == warpConfig->mesh ) {
        // mesh bypass
        warpConfig->meshWidth  = 0;
        warpConfig->meshHeight = 0;
        warpConfig->meshMode   = WARP_MESH__BYPASS;
    } else {
        if  (  ( WCFG_PARAM_U32__NOT_CFGD == warpConfig->meshWidth  )
            || ( WCFG_PARAM_U32__NOT_CFGD == warpConfig->meshHeight ) )
            return WCFG_STS__MESH_SIZE_NOT_CFGD;
    }

    // check INPUT configuration
    if  ( NULL == warpConfig->inputBuffer )
        return WCFG_STS__INPUT_BUFF_NOT_CFGD;
    
    if  (  ( WCFG_PARAM_U32__NOT_CFGD == warpConfig->inputWidth  )
        || ( WCFG_PARAM_U32__NOT_CFGD == warpConfig->inputHeight ) )
        return WCFG_STS__INPUT_SIZE_NOT_CFGD;

    // check OUTPUT configuration
    if  ( NULL == warpConfig->outputBuffer )
        return WCFG_STS__OUTPUT_BUFF_NOT_CFGD;
    
    if  (  ( WCFG_PARAM_U32__NOT_CFGD == warpConfig->outputWidth  )
        || ( WCFG_PARAM_U32__NOT_CFGD == warpConfig->outputHeight ) )
        return WCFG_STS__OUTPUT_SIZE_NOT_CFGD;


    return WCFG_STS__NO_ERROR;
}


// -----------------------------------------------------------------------------
//
// Local Function     configureWarpFilter
//
// Description        configures Warp Filter registers
//
// Parameters         warpConfig - structure containing provided
//                                 configuration parameters
//
// Return             none
//
// Constraints        shall be called only from hwiWarpConfig() function
//
// -----------------------------------------------------------------------------
void configureWarpFilter( hwiWarpConfig_t * warpConfig )
{
    uint32_t byp3x3         =  1;
    uint32_t meshBpp        = (warpConfig->meshMode == WARP_MESH__SPARSE_F32) ? 4 : 2;

    uint32_t stride[3];                                     // stride [bytes] { mesh, in, out }

    uint32_t warpTileWPwr2  = 4                         ;   // Tile Size:
    uint32_t warpTileHPwr2  = 4                         ;   //    16x16


    // compute line strides
    stride[W_MESH] = DATA_ALIGN_16(warpConfig->meshWidth    * 2 * meshBpp); // 2Coords x 4Bytes/float element
    stride[W_IN  ] = DATA_ALIGN_16(warpConfig->inputWidth   * BPP(1)     );
    stride[W_OUT ] = DATA_ALIGN_16(warpConfig->outputWidth  * BPP(1)     );


    // setting base address
    warpApi = initialiseWarpAPB(
        warpConfig->mesh        , warpConfig->meshWidth   * warpConfig->meshHeight   * meshBpp,
        warpConfig->inputBuffer , warpConfig->inputWidth  * warpConfig->inputHeight           ,
        warpConfig->outputBuffer, warpConfig->outputWidth * warpConfig->outputHeight          ,
        0, 0, 0, 0, 0,
        eor_callback ,
        eosr_callback,
        0, 0
    );

    SET_REG_WORD(warpBaseAddress + WARP_TILESTART_OFFSET       ,
        (0 << 16) | (0 << 0)                                   );
    SET_REG_WORD(warpBaseAddress + WARP_INTMESHSTARTXY_OFFSET  ,
        (0 << 16) | (0 << 0)                                )  ;
    SET_REG_WORD(warpBaseAddress + WARP_FRACMESHSTARTX_OFFSET  ,
        0x00000                                                );
    SET_REG_WORD(warpBaseAddress + WARP_FRACMESHSTARTY_OFFSET  ,
        0x00000                                                );

    SET_REG_WORD(warpBaseAddress + WARP_GRECIPX_OFFSET         ,
        ( (warpConfig->meshWidth   - 1) * 0x100000)            /
          (warpConfig->outputWidth - 1)                        );
    SET_REG_WORD(warpBaseAddress + WARP_GRECIPY_OFFSET         ,
        ( (warpConfig->meshHeight   - 1) * 0x100000)           /
          (warpConfig->outputHeight - 1)                       );

    SET_REG_WORD(warpBaseAddress + WARP_OUTFRAMELIM_OFFSET     ,
        ( (warpConfig->outputHeight - 1) << 16)                |
        ( (warpConfig->outputWidth  - 1) <<  0)                );

    SET_REG_WORD(warpBaseAddress + WARP_EDGECOLOUR_OFFSET      ,
        warpConfig->edgeColorMode                              ); // replicate | clearColor modes


    // BurstSize | Stride
    SET_REG_WORD(warpBaseAddress + WARP_MEMSETUP0_OFFSET       ,
        (0 << 25) | (stride[W_MESH])                           );
    SET_REG_WORD(warpBaseAddress + WARP_MEMSETUP1_OFFSET       ,
        (0 << 25) | (stride[W_IN  ])                           );
    SET_REG_WORD(warpBaseAddress + WARP_MEMSETUP2_OFFSET       ,
        (0 << 25) | (stride[W_OUT ])                           );


    // LIMIT_LO | LIMIT_HI
    SET_REG_WORD(warpBaseAddress + WARP_INFRAMELIMIT_X_OFFSET  ,
        (0 << 16) | (warpConfig->inputWidth  - 1) << 0         );
    SET_REG_WORD(warpBaseAddress + WARP_INFRAMELIMIT_Y_OFFSET  ,
        (0 << 16) | (warpConfig->inputHeight - 1) << 0         );

    SET_REG_WORD(warpBaseAddress + WARP_MESHLIMIT_OFFSET       ,
        ( (warpConfig->meshHeight - 1) << 16)                  |
        ( (warpConfig->meshWidth  - 1) <<  0)                  );
    SET_REG_WORD(warpBaseAddress + WARP_PFBC_OFFSET            ,
        (1 << 4)                                               ); // CLR_CACHE
    SET_REG_WORD(warpBaseAddress + WARP_MESH_OFFSET            ,
        warpConfig->meshMode                                   );

    SET_REG_WORD(warpBaseAddress + WARP_PIXELFORMATS_OFFSET    ,
        ( (8 - 1) << 0)                                        |  // out bit-width
        ( (8 - 1) << 4)                                        ); // in  bit-width


    if  ( NULL != warpConfig->matrix3x3 )
    {
        byp3x3 = 0; // don't bypass the 3x3 coord-transform matrix

        // 0  3  6
        // 1  4  7
        // 2  6  8
        SET_REG_WORD(warpBaseAddress + WARP_MAT0_OFFSET        ,
            *((uint32_t *) &warpConfig->matrix3x3[ 0 ])        );
        SET_REG_WORD(warpBaseAddress + WARP_MAT1_OFFSET        ,
            *((uint32_t *) &warpConfig->matrix3x3[ 3 ])        );
        SET_REG_WORD(warpBaseAddress + WARP_MAT2_OFFSET        ,
            *((uint32_t *) &warpConfig->matrix3x3[ 6 ])        );
        SET_REG_WORD(warpBaseAddress + WARP_MAT3_OFFSET        ,
            *((uint32_t *) &warpConfig->matrix3x3[ 1 ])        );
        SET_REG_WORD(warpBaseAddress + WARP_MAT4_OFFSET        ,
            *((uint32_t *) &warpConfig->matrix3x3[ 4 ])        );
        SET_REG_WORD(warpBaseAddress + WARP_MAT5_OFFSET        ,
            *((uint32_t *) &warpConfig->matrix3x3[ 7 ])        );
        SET_REG_WORD(warpBaseAddress + WARP_MAT6_OFFSET        ,
            *((uint32_t *) &warpConfig->matrix3x3[ 2 ])        );
        SET_REG_WORD(warpBaseAddress + WARP_MAT7_OFFSET        ,
            *((uint32_t *) &warpConfig->matrix3x3[ 5 ])        );
        SET_REG_WORD(warpBaseAddress + WARP_MAT8_OFFSET        ,
            *((uint32_t *) &warpConfig->matrix3x3[ 8 ])        );
   }


    SET_REG_WORD(warpBaseAddress + WARP_MODE_OFFSET            ,
        (warpConfig->interpMode  <<  8)                        |  // bit8 = bicubic, bit9 = bilinear
        (                byp3x3  <<  7)                        |  // bypass 3x3 coord transform
        (                     1  <<  6)                        |  // 2nd filter path
        (          warpTileHPwr2 <<  3)                        |  // tileH power_of_2
        (          warpTileWPwr2 <<  0)                        ); // tileW power_of_2
}

// -----------------------------------------------------------------------------
//
// API Function       hwiWarpConfig
//
// Description        configures Warp Filter based on provided configuration
//
// Parameters         warpConfig - structure containing configuration
//                                 parameters provided by API user
//
// Return             configuration error status (wcfgStatus_t)
//
// Constraints        hwiWarpInit() need to be called before
//
// -----------------------------------------------------------------------------
wcfgStatus_t hwiWarpConfig( hwiWarpConfig_t * warpConfig )
{
    // --------- ERROR HANDLER -------------------------------------------------

    warpConfigCalled = true;

    // check if hwiWarpInit() was called before this
    if  ( !warpInitCalled )
        return WCFG_STS__INIT_NOT_CALLED;

    // check provided warp configuration
    warpConfigStatus = checkWarpConfig( warpConfig );
    if  ( WCFG_STS__NO_ERROR != warpConfigStatus )
        return warpConfigStatus;


    // --------- WARP CONFIGURATION --------------------------------------------

    // NOTE Warp PC Model seem to have only instance 0
    warpBaseAddress  = WARP0_BASE_ADR;
    // warpBaseAddress  = WARP0_BASE_ADR + ((uint32_t)warpConfig->warpInstance * 0x10000);

    // configure Warp filter
    configureWarpFilter( warpConfig );


    return WCFG_STS__NO_ERROR;
}


// -----------------------------------------------------------------------------
//
// API Function       hwiWarpStart
//
// Description        starts Warp Filter
//
// Parameters         none
//
// Return             configuration error status (wcfgStatus_t)
//
// Constraints        hwiWarpConfig() need to be called before
//
// -----------------------------------------------------------------------------
wcfgStatus_t hwiWarpStart()
{
    // --------- ERROR HANDLER -------------------------------------------------

    warpStartCalled = true;

    // check if hwiWarpInit() was called before this
    if  ( !warpInitCalled )
        return WCFG_STS__INIT_NOT_CALLED;

    // check if hwiWarpConfig() was called before this
    if  ( !warpConfigCalled )
        return WCFG_STS__CONFIG_NOT_CALLED;

    // check provided warp configuration status
    if  ( WCFG_STS__NO_ERROR != warpConfigStatus )
        return warpConfigStatus;


    // --------- START WARP FILTER ---------------------------------------------

    // enable End Of Run interrupt
    SET_REG_WORD(warpBaseAddress + WARP_INT_ENABLE_OFFSET, 1 << 0   );


    // start warp filter
    uint32_t warpStart      = WARP_START__1x_SUPER_RUN  ;   // do 1x SuperRun
    uint32_t warpRuns       = WARP_RUNS__FULL_FRAME     ;   //    full-frame

    SET_REG_WORD(warpBaseAddress + WARP_RUNS_OFFSET      , warpRuns );
    SET_REG_WORD(warpBaseAddress + WARP_START_OFFSET     , warpStart);


    return WCFG_STS__NO_ERROR;
}


// -----------------------------------------------------------------------------
//
// API Function       hwiWarpWait
//
// Description        waits for Warp Filter to finish
//
// Parameters         none
//
// Return             configuration error status (wcfgStatus_t)
//
// Constraints        hwiWarpStart() need to be called before
//
// -----------------------------------------------------------------------------
wcfgStatus_t hwiWarpWait()
{
    // --------- ERROR HANDLER -------------------------------------------------

    // check if hwiWarpInit() was called before this
    if  ( !warpInitCalled )
        return WCFG_STS__INIT_NOT_CALLED;

    // check if hwiWarpConfig() was called before this
    if  ( !warpConfigCalled )
        return WCFG_STS__CONFIG_NOT_CALLED;    

    // check provided warp configuration status
    if  ( WCFG_STS__NO_ERROR != warpConfigStatus )
        return warpConfigStatus;

    // check if hwiWarpStart() was called before this
    if  ( !warpStartCalled )
        return WCFG_STS__START_NOT_CALLED;


    // --------- WAIT FOR WARP FILTER  -----------------------------------------

    uint32_t status;

    do {
        NOP;NOP;NOP;NOP;NOP;   NOP;NOP;NOP;NOP;NOP;
        NOP;NOP;NOP;NOP;NOP;   NOP;NOP;NOP;NOP;NOP;

        status = GET_REG_WORD_VAL(warpBaseAddress + WARP_INT_STATUS_OFFSET);

    }   while ((status & 1) != 1); // End Of Run

    // clear End Of Run
    SET_REG_WORD(warpBaseAddress + WARP_INT_CLEAR_OFFSET, 1);


    // --------- CLEAN UP  -----------------------------------------------------

    // delete the instance in the end
    delete (WarpConfig *) warpApi;
    // clear base address
    warpBaseAddress  = WCFG_PARAM_U32__NOT_CFGD;


    return WCFG_STS__NO_ERROR;
}
