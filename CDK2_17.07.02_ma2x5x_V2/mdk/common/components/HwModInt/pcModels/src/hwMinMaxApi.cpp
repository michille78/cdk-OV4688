#include "sippUnitTestIsr.h"
#include "sippUnitTestConfigurer.h"
#include "minMaxAccelerator.h"
#include "apbSlaveInterface.h"
#include <registersMyriad2.h>
#include <sippHwDefines.h>
#include <sippUnitTestProducer.h>
#include <sippUnitTestConsumer.h>
#include <measure.h>
#include "hwMinMaxApi.h"
#include "fp16.h"

//#define MINMAX_DEBUG

static std::string tcstr("MinMax filter");
// Front-end of the mini-pipe
#define TEST_IBUF0_BASE_ADR BUF_BASE_REG(CV,MIN_MAX,I,0)
#define TEST_IBUF0_CFG_ADR  BUF_CFG_REG(CV,MIN_MAX,I,0)
#define TEST_IBUF0_LS_ADR   BUF_LS_REG(CV,MIN_MAX,I,0)
#define TEST_IBUF0_PS_ADR   BUF_PS_REG(CV,MIN_MAX,I,0)
#define TEST_IBUF0_IR_ADR   BUF_IR_REG(CV,MIN_MAX,I,0)
#define TEST_IBUF0_FC_ADR   BUF_FC_REG(CV,MIN_MAX,I,0)
#define TEST_ICTX0_ADR      CTX_REG(CV,MIN_MAX,I,0)
#define TEST_IBUF1_BASE_ADR BUF_BASE_REG(CV,MIN_MAX,I,1)
#define TEST_IBUF1_CFG_ADR  BUF_CFG_REG(CV,MIN_MAX,I,1)
#define TEST_IBUF1_LS_ADR   BUF_LS_REG(CV,MIN_MAX,I,1)
#define TEST_IBUF1_PS_ADR   BUF_PS_REG(CV,MIN_MAX,I,1)
#define TEST_IBUF1_IR_ADR   BUF_IR_REG(CV,MIN_MAX,I,1)
#define TEST_IBUF1_FC_ADR   BUF_FC_REG(CV,MIN_MAX,I,1)
#define TEST_ICTX1_ADR      CTX_REG(CV,MIN_MAX,I,1)
#define TEST_IBUF2_BASE_ADR BUF_BASE_REG(CV,MIN_MAX,I,2)
#define TEST_IBUF2_CFG_ADR  BUF_CFG_REG(CV,MIN_MAX,I,2)
#define TEST_IBUF2_LS_ADR   BUF_LS_REG(CV,MIN_MAX,I,2)
#define TEST_IBUF2_PS_ADR   BUF_PS_REG(CV,MIN_MAX,I,2)
#define TEST_IBUF2_IR_ADR   BUF_IR_REG(CV,MIN_MAX,I,2)
#define TEST_IBUF2_FC_ADR   BUF_FC_REG(CV,MIN_MAX,I,2)
#define TEST_ICTX2_ADR      CTX_REG(CV,MIN_MAX,I,2)

// Back-end of the mini-pipe
#define TEST_OBUF0_BASE_ADR BUF_BASE_REG(CV,MIN_MAX,O,0)
#define TEST_OBUF0_CFG_ADR  BUF_CFG_REG(CV,MIN_MAX,O,0)
#define TEST_OBUF0_LS_ADR   BUF_LS_REG(CV,MIN_MAX,O,0)
#define TEST_OBUF0_PS_ADR   BUF_PS_REG(CV,MIN_MAX,O,0)
#define TEST_OBUF0_IR_ADR   BUF_IR_REG(CV,MIN_MAX,O,0)
#define TEST_OBUF0_FC_ADR   BUF_FC_REG(CV,MIN_MAX,O,0)
#define TEST_OCTX0_ADR      CTX_REG(CV,MIN_MAX,O,0)
#define TEST_OBUF1_BASE_ADR BUF_BASE_REG(CV,MIN_MAX,O,1)
#define TEST_OBUF1_CFG_ADR  BUF_CFG_REG(CV,MIN_MAX,O,1)
#define TEST_OBUF1_LS_ADR   BUF_LS_REG(CV,MIN_MAX,O,1)
#define TEST_OBUF1_PS_ADR   BUF_PS_REG(CV,MIN_MAX,O,1)
#define TEST_OBUF1_IR_ADR   BUF_IR_REG(CV,MIN_MAX,O,1)
#define TEST_OBUF1_FC_ADR   BUF_FC_REG(CV,MIN_MAX,O,1)
#define TEST_OCTX1_ADR      CTX_REG(CV,MIN_MAX,O,1)

#define MIN_MAX_KERNEL_SIZE     3

#define INPUT_BUFFER_LINES  MIN_MAX_KERNEL_SIZE + 1
#define OUTPUT_BUFFER_LINES 2
#define STATS_BUFFER_LINES  2


#ifdef MINMAX_DEBUG
#include<stdarg.h>
void minmaxDebug(const char* format, ...)
{
   va_list argptr;
   va_start(argptr, format);
   vprintf(format, argptr);
   va_end(argptr);
}
#define MINMAX_VERBOSE true

#else

#define MINMAX_VERBOSE false
#define minmaxDebug(...)
#endif


typedef std::shared_ptr<fp16>        fp16_t_sp;
typedef std::shared_ptr<uint16_t>  uint16_t_sp;
typedef std::shared_ptr<uint32_t>  uint32_t_sp;

SippUnitTestBufferAlloc<fp16>    * minmax_ibuff16[3];
SippUnitTestBufferAlloc<uint16_t>* minmax_ibufu16[3];
SippUnitTestBufferAlloc<uint32_t>* minmax_ibufu32[3];

SippUnitTestBufferAlloc<fp16>    * minmax_obuff16[2];
SippUnitTestBufferAlloc<uint16_t>* minmax_obufu16[2];
SippUnitTestBufferAlloc<uint32_t>* minmax_obufu32[2];

//typedef std::shared_ptr<ApbSlaveInterface> ApbSlaveInterface_sp
static SippUnitTestConfigurer::ApbSlaveInterface_sp minMax;

fp16     *minmax_inPlanes0_fp16[3];
fp16     *minmax_inPlanes1_fp16[3];
fp16     *minmax_inPlanes2_fp16[3];

uint16_t *minmax_inPlanes0_ui16[3];
uint16_t *minmax_inPlanes1_ui16[3];
uint16_t *minmax_inPlanes2_ui16[3];

uint16_t_sp minmax_outPlanes0_ui16[3];
  fp16_t_sp minmax_outPlanes0_fp16[3];
uint32_t_sp minmax_outPlanes1[3];

minMaxBuffer *minmax_pOutBuf[2];

static SippUnitTestConfigurer *minmax_inputCfg [3];
static SippUnitTestConfigurer *minmax_outputCfg[2];

uint32_t  minmax_inAddr[3][6] = {
  { TEST_IBUF0_BASE_ADR, TEST_IBUF0_CFG_ADR, TEST_IBUF0_LS_ADR, TEST_IBUF0_PS_ADR, TEST_IBUF0_FC_ADR, TEST_ICTX0_ADR },
  { TEST_IBUF1_BASE_ADR, TEST_IBUF1_CFG_ADR, TEST_IBUF1_LS_ADR, TEST_IBUF1_PS_ADR, TEST_IBUF1_FC_ADR, TEST_ICTX1_ADR },
  { TEST_IBUF2_BASE_ADR, TEST_IBUF2_CFG_ADR, TEST_IBUF2_LS_ADR, TEST_IBUF2_PS_ADR, TEST_IBUF2_FC_ADR, TEST_ICTX2_ADR }
};
uint32_t minmax_outAddr[2][6] = {
  { TEST_OBUF0_BASE_ADR, TEST_OBUF0_CFG_ADR, TEST_OBUF0_LS_ADR, TEST_OBUF0_PS_ADR, TEST_OBUF0_FC_ADR, TEST_OCTX0_ADR },
  { TEST_OBUF1_BASE_ADR, TEST_OBUF1_CFG_ADR, TEST_OBUF1_LS_ADR, TEST_OBUF1_PS_ADR, TEST_OBUF1_FC_ADR, TEST_OCTX1_ADR }
};

static SippUnitTestConfigurer::addr_idx_t  *minmax_inAddrV[3];
static SippUnitTestConfigurer::addr_idx_t *minmax_outAddrV[2];

Semaphore_sp minmax_inputSem[IsrHandler::MAX_IBUFS], minmax_outputSem[IsrHandler::MAX_OBUFS];

minMaxCfg minmax_cfg;
uint32_t  minmax_denseExtremaCount;

static SippUnitTestProcess *minmax_prod[3]={0,0,0};
static SippUnitTestProcess *minmax_cons[2]={0,0};

static SippUnitTestProcess::thrd_t minmax_pthrd[3];
static SippUnitTestProcess::thrd_t minmax_cthrd[2];

int32_t MinMaxCleanup()
{
  for(int i=0; i<3; i++){
    if( minmax_prod[i])     delete minmax_prod[i];
    if( minmax_inputCfg[i]) delete minmax_inputCfg[i];
    if( minmax_inAddrV[i])  delete minmax_inAddrV[i];
    minmax_inputSem[i].reset();
    if(minmax_ibuff16[i])   delete minmax_ibuff16[i];
    if(minmax_ibufu16[i])   delete minmax_ibufu16[i];
    if(minmax_ibufu32[i])   delete minmax_ibufu32[i];
  }

  for(int i=0; i<2; i++){
    if(minmax_cons[i] )     delete minmax_cons[i];
    if(minmax_outputCfg[i]) delete minmax_outputCfg[i];
    if(minmax_outAddrV[i])  delete minmax_outAddrV[i];
    minmax_outputSem[i].reset();
    if(minmax_obuff16[i])  delete minmax_obuff16[i];
    if(minmax_obufu16[i])  delete minmax_obufu16[i];
    if(minmax_obufu32[i])  delete minmax_obufu32[i];
  }

  return 0;
}

static int32_t MinMaxErrorCheck(minMaxBuffer in[3], minMaxBuffer out[2])
{
  // TODO check if exactly half/double else erro
  if(in[0].spec.type!=NONE && in[0].spec.type!=in[1].spec.type) return -2;
  if(in[2].spec.type!=NONE && in[2].spec.type!=in[1].spec.type) return -3;
  if(out[0].spec.type==NONE) return -4;
  if(out[0].spec.type!=in[1].spec.type) return -5;
  if(out[1].spec.type!=NONE && out[1].spec.type!=U32) return -6;
  if(in[1].spec.type==NONE || (in[1].spec.type!=FixedPoint16 && in[1].spec.type!=FloatingPoint16)) return -7;

  return 0;
}


int32_t MinMaxSetup(minMaxBuffer in[3], minMaxBuffer out[2], minMaxCfg cfg)
{
  minmaxDebug("MinMaxStart()\n");

  minmax_cfg=cfg;
  minmax_denseExtremaCount=in[1].spec.planes*in[1].spec.height;

  for(int i=0;i<3;i++){
    if(in[0].spec.type==FixedPoint16)     minmax_inPlanes0_ui16[i] = (uint16_t *)((uint16_t *)(in[0].plane)+(in[0].spec.width*in[0].spec.height*i));
    if(in[0].spec.type==FloatingPoint16)  minmax_inPlanes0_fp16[i] = (fp16*)((fp16*)(in[0].plane)+(in[0].spec.width*in[0].spec.height*i));
  }
  for(int i=0;i<3;i++){
    if(in[1].spec.type==FixedPoint16)     minmax_inPlanes1_ui16[i] = (uint16_t *)((uint16_t *)(in[1].plane)+(in[1].spec.width*in[1].spec.height*i));
    if(in[1].spec.type==FloatingPoint16)  minmax_inPlanes1_fp16[i] = (fp16*)((fp16*)(in[1].plane)+(in[1].spec.width*in[1].spec.height*i));
  }
  for(int i=0;i<3;i++){
    if(in[2].spec.type==FixedPoint16)     minmax_inPlanes2_ui16[i] = (uint16_t *)((uint16_t *)(in[2].plane)+(in[2].spec.width*in[2].spec.height*i));
    if(in[2].spec.type==FloatingPoint16)  minmax_inPlanes2_fp16[i] = (fp16*)((fp16*)(in[2].plane)+(in[2].spec.width*in[2].spec.height*i));
  }

  uint32_t cfgInEnable  = 0;

  int32_t error = MinMaxErrorCheck(in, out);
  if(error<0) return error;

  if(!minMax){
    minMax= std::make_shared<MinMaxAccelerator>(IsrHandler::separateRegIsr, MIN_MAX_KERNEL_SIZE);
    minMax.get()->SetVerbose(MINMAX_VERBOSE);
    SippUnitTestConfigurer::SetApbSlaveInterface(minMax);
  }

  IsrHandler::SetTestInfo(0x3F, 0, tcstr);
  IsrHandler::SetIrqRegs(CV_MIN_MAX_INT_STATUS_ADR, CV_MIN_MAX_INT_ENABLE_ADR, CV_MIN_MAX_INT_CLEAR_ADR);
  IsrHandler::SetIbflIrqSlot(0, 0); IsrHandler::SetIbflIrqSlot(1, 1); IsrHandler::SetIbflIrqSlot(2, 2);
  IsrHandler::SetObflIrqSlot(3, 0); IsrHandler::SetObflIrqSlot(4, 1); IsrHandler:: SetEofIrqSlot(5);

  IsrHandler::EnableSeparateIrqs(minMax.get());

  for(int i=0;i<3;i++)
  {
    if(in[i].spec.type!=NONE)
    {
      minmax_inputSem[i]  = Semaphore_sp(new Semaphore(INPUT_BUFFER_LINES, INPUT_BUFFER_LINES));
      IsrHandler:: SetInputSemaphore( minmax_inputSem[i], i);
      minmax_inAddrV[i] = new SippUnitTestConfigurer::addr_idx_t( minmax_inAddr[i],  minmax_inAddr[i] + sizeof( minmax_inAddr[i])/sizeof(* minmax_inAddr[i]));
      if(in[i].spec.type==FixedPoint16)     {
        minmax_ibufu16[i] = new SippUnitTestBufferAlloc<uint16_t>(DEF_SLICE_SIZE * CMX_NSLICES);
        minmax_inputCfg[i] = new SippUnitTestConfigurer(*minmax_ibufu16[i],  INPUT_BUFFER_LINES, in[i].spec.lineStride, in[i].spec.planes, sizeof(uint16_t),  *minmax_inAddrV[i],  minmax_inputSem[i]);
      }
      if(in[i].spec.type==FloatingPoint16)  {
        minmax_ibuff16[i] = new SippUnitTestBufferAlloc<    fp16>(DEF_SLICE_SIZE * CMX_NSLICES);
        minmax_inputCfg[i] = new SippUnitTestConfigurer(*minmax_ibuff16[i],  INPUT_BUFFER_LINES, in[i].spec.lineStride, in[i].spec.planes, sizeof(uint16_t),  *minmax_inAddrV[i],  minmax_inputSem[i]);
      }
      minmax_inputCfg[i]->SetInDimensions (in[i].spec.width, in[i].spec.height);
      minmax_inputCfg[i]->DetermineCurrentBufferIdx();

      cfgInEnable |= 1<<i;
    }
  }

  if(in[0].spec.type==FixedPoint16)     minmax_prod[0]     = new SippUnitTestProcess(circularProducer< uint16_t, minmax_inPlanes0_ui16, MINMAX_VERBOSE>, *minmax_inputCfg[0], INFINITE);
  if(in[0].spec.type==FloatingPoint16)  minmax_prod[0]     = new SippUnitTestProcess(circularProducer<     fp16, minmax_inPlanes0_fp16, MINMAX_VERBOSE>, *minmax_inputCfg[0], INFINITE);
  if(in[1].spec.type==FixedPoint16)     minmax_prod[1]     = new SippUnitTestProcess(circularProducer< uint16_t, minmax_inPlanes1_ui16, MINMAX_VERBOSE>, *minmax_inputCfg[1], INFINITE);
  if(in[1].spec.type==FloatingPoint16)  minmax_prod[1]     = new SippUnitTestProcess(circularProducer<     fp16, minmax_inPlanes1_fp16, MINMAX_VERBOSE>, *minmax_inputCfg[1], INFINITE);
  if(in[2].spec.type==FixedPoint16)     minmax_prod[2]     = new SippUnitTestProcess(circularProducer< uint16_t, minmax_inPlanes2_ui16, MINMAX_VERBOSE>, *minmax_inputCfg[2], INFINITE);
  if(in[2].spec.type==FloatingPoint16)  minmax_prod[2]     = new SippUnitTestProcess(circularProducer<     fp16, minmax_inPlanes2_fp16, MINMAX_VERBOSE>, *minmax_inputCfg[2], INFINITE);

  for(int i=0;i<2;i++)
  {
    if(out[i].spec.type!=NONE)
    {
      minmax_outputSem[i] = Semaphore_sp(new Semaphore(0, OUTPUT_BUFFER_LINES));
      IsrHandler::SetOutputSemaphore(minmax_outputSem[i], i);
      minmax_outAddrV[i] = new SippUnitTestConfigurer::addr_idx_t( minmax_outAddr[i],  minmax_outAddr[i] + sizeof( minmax_outAddr[i])/sizeof(* minmax_outAddr[i]));

     if(out[i].spec.type==FloatingPoint16) minmax_obuff16[i] = new SippUnitTestBufferAlloc<fp16>    (DEF_SLICE_SIZE * CMX_NSLICES);
     if(out[i].spec.type==FixedPoint16)    minmax_obufu16[i] = new SippUnitTestBufferAlloc<uint16_t>(DEF_SLICE_SIZE * CMX_NSLICES);
     if(out[i].spec.type==U32)             minmax_obufu32[i] = new SippUnitTestBufferAlloc<uint32_t>(DEF_SLICE_SIZE * CMX_NSLICES);
    }
  }

  for(int i=0;i<2;i++)
  {
    if(out[i].spec.type!=NONE)
    {
      if(out[i].spec.type==FloatingPoint16)
        minmax_outputCfg[i] = new SippUnitTestConfigurer(*minmax_obuff16[i],  OUTPUT_BUFFER_LINES, out[i].spec.lineStride, out[i].spec.planes, sizeof(fp16),  *minmax_outAddrV[i],  minmax_outputSem[i]);

      if(out[i].spec.type==FixedPoint16)
        minmax_outputCfg[i] = new SippUnitTestConfigurer(*minmax_obufu16[i],  OUTPUT_BUFFER_LINES, out[i].spec.lineStride, out[i].spec.planes, sizeof(uint16_t),  *minmax_outAddrV[i],  minmax_outputSem[i]);

      if(out[i].spec.type==U32)
        minmax_outputCfg[i] = new SippUnitTestConfigurer(*minmax_obufu32[i],  OUTPUT_BUFFER_LINES, out[i].spec.lineStride, out[i].spec.planes, sizeof(uint32_t),  *minmax_outAddrV[i],  minmax_outputSem[i]);
    }
  }

  if (out[0].spec.type!=NONE )
  {
	  if(cfg.outDenseEnable0){
		minmax_outputCfg[0]->SetOutDimensions (in[1].spec.width*in[1].spec.planes, 1);
		minmax_outputCfg[0]->DetermineCurrentBufferIdx();
	  }
	  else
	  {
		minmax_outputCfg[0]->SetOutDimensions (out[0].spec.width, out[0].spec.height);
		minmax_outputCfg[0]->DetermineCurrentBufferIdx();
	  }
  }
  if (out[1].spec.type!=NONE )
  {
	  if(cfg.outDenseEnable1){
		minmax_outputCfg[1]->SetOutDimensions (in[1].spec.width*in[1].spec.planes, 1);
		minmax_outputCfg[1]->DetermineCurrentBufferIdx();
	  }
	  else
	  {
		minmax_outputCfg[1]->SetOutDimensions (out[1].spec.width, out[1].spec.height);
		minmax_outputCfg[1]->DetermineCurrentBufferIdx();
	  }
  }

  if(out[0].spec.type==FixedPoint16)    minmax_cons[0] = new SippUnitTestProcess(circularConsumer<uint16_t, uint16_t_sp, minmax_outPlanes0_ui16, MINMAX_VERBOSE>, *minmax_outputCfg[0], INFINITE);
  if(out[0].spec.type==FloatingPoint16) minmax_cons[0] = new SippUnitTestProcess(circularConsumer<fp16,       fp16_t_sp, minmax_outPlanes0_fp16, MINMAX_VERBOSE>, *minmax_outputCfg[0], INFINITE);
  if(out[1].spec.type==U32) minmax_cons[1] = new SippUnitTestProcess(circularConsumer<uint32_t, uint32_t_sp, minmax_outPlanes1, MINMAX_VERBOSE>, *minmax_outputCfg[1], INFINITE);

  uint32_t cfgFp16     = in[1].spec.type == FloatingPoint16 ? 1 : 0;
  minmaxDebug("Floating bit is %d\n", cfgFp16);
  uint32_t cfgScaling  = (in[1].spec.width == in[0].spec.width ? 0:1) | (in[1].spec.width == in[2].spec.width ? 0:2);
  minmaxDebug("Scaling bits are %d\n", cfgScaling);
  uint32_t cfgRounding = cfg.roundingEnable ? 0x8 : 0;
  minmaxDebug("Rounding bit is %d\n", cfgRounding);
  minmaxDebug("Dense bits are %d %d\n", cfg.outDenseEnable0, cfg.outDenseEnable1);

  // Set filter configuration
  int data = (cfgScaling << 0) | (cfgFp16 << 2) | (cfg.outDenseEnable0 << 4) | (cfg.outDenseEnable1 << 5) | (cfg.maxExtrema <<16) | cfgRounding;

  minMax->ApbWrite(CV_MIN_MAX_CFG_ADR, data);

  // Set thresholds
  data = cfg.scoreThreshold <<  0 | cfg.extremaThreshold << 16;
  minMax->ApbWrite(CV_MIN_MAX_THRESHOLD_ADR, data);

  // Image resolution
  minMax->ApbWrite(CV_MIN_MAX_FRM_DIM_ADR, ((in[1].spec.height & SIPP_IMGDIM_MASK) << SIPP_IMGDIM_SIZE) | (in[1].spec.width & SIPP_IMGDIM_MASK));

  if(out[1].spec.type !=NONE){
    cfgInEnable |=  0x8; // enable status output
    minmaxDebug("Output 1 enabled\n");
    minmaxDebug("Stats width is %u \n", out[1].spec.width & SIPP_IMGDIM_MASK);
    // Apparently CV_MIN_MAX_STATUS_IMG_WIDTH_ADR might be changed on CV_MIN_MAX_FRM_DIM_ADR write so better keep it after it
    minMax->ApbWrite(CV_MIN_MAX_STATUS_IMG_WIDTH_ADR, out[1].spec.width & SIPP_IMGDIM_MASK);
  }

  // Enable filters in this pipe
  minmaxDebug("MinMax inputs configuration is %u\n",cfgInEnable);

  for(int i=0; i<2; i++){
    minmax_pOutBuf[i]=&out[i];
  }

  // Kernel debugging
  if(cfg.debug.enable) {
    minmaxDebug("MinMax Debug config enabled\n");
    minMax->ApbWrite(CV_MIN_MAX_DEBUG_KERNELS_ADR, (1 << 6) | (cfg.debug.lbrc << 4) | (cfg.debug.pixel));
  }


  minmaxDebug("MinMax enable cfg is 0x%08X \n",cfgInEnable);
  minMax->ApbWrite(CV_MIN_MAX_ENABLE_ADR, cfgInEnable);
  minmaxDebug("MinMax End of setup\n");
  return 0;
}


int32_t MinMaxStart()
{
  minmaxDebug("MinMaxStart()\n");
  for(int i=0;i<3;i++){
    if(minmax_prod[i]){
      minmax_pthrd[i] = minmax_prod[i]->StartProcess();
      minmaxDebug("MinMax Starting producer %d\n", i);
    }
  }
  for(int i=0;i<2 ;i++){
    if(minmax_cons[i]){
      minmax_cthrd[i] = minmax_cons[i]->StartProcess();
      minmaxDebug("MinMax Starting consumer %d\n", i);
    }
  }
  return 0;
}


int32_t MinMaxWait()
{
  minmaxDebug("MinMaxWait()\n");
  for(int i=0;i<3;i++){
    if(minmax_prod[i]){
      minmax_prod[i]->WaitForProcess(minmax_pthrd[i]);
      minmaxDebug("MinMax Waiting for producer %d\n", i);
    }
  }
  for(int i=0;i<2;i++){
    if(minmax_cons[i]){
      minmax_cons[i]->WaitForProcess(minmax_cthrd[i]);
      minmaxDebug("MinMax Waiting for consumer %d\n", i);
    }
  }

  if(minmax_pOutBuf[0]->spec.type==FixedPoint16){
    for(int i=0;i<3;i++) {
      minmax_pOutBuf[0]->outPlanes[i]=(uint8_t*)minmax_outPlanes0_ui16[i].get();
      minmaxDebug("1Outbuf 0 plane %d is %p and the size is: %d \n", i, minmax_pOutBuf[0]->outPlanes[i], minmax_pOutBuf[0]->spec.height * minmax_pOutBuf[0]->spec.width);
    }
  }
  if(minmax_pOutBuf[0]->spec.type==FloatingPoint16){
    for(int i=0;i<3;i++) {
      minmax_pOutBuf[0]->outPlanes[i]=(uint8_t*)minmax_outPlanes0_fp16[i].get();
      minmaxDebug("2Outbuf 0 plane %d is %p\n", i, minmax_pOutBuf[0]->outPlanes[i]);
    }
  }

  if(minmax_cfg.outDenseEnable0){
    minmax_pOutBuf[0]->spec.height=1;
    minmax_pOutBuf[0]->spec.lineStride=1;
    minmax_pOutBuf[0]->spec.planes=1;

    uint16_t *p= (uint16_t*)minmax_pOutBuf[0]->outPlanes[0];
    uint32_t count=0;
    uint32_t lineWidth=0;
    uint32_t watchdog=2048*2048*3; // maximum sized input with 3 planes
    minmaxDebug("3MinMax dense extrema count is %u. Starting search..\n", minmax_denseExtremaCount);
    for(;count<minmax_denseExtremaCount;p++){
      lineWidth++;
      if(*p==0xFFFF){
        count++;
        minmaxDebug("4MinMax found line %4u finish descriptor after %4u bytes\n", count, lineWidth);
        lineWidth=0;
      }
      if(!watchdog--) {
        minmaxDebug("5MinMax dense mode outside of bounds error\n");
        return -1;
      }
    }

    minmax_pOutBuf[0]->spec.width = ((uint8_t*)p - minmax_pOutBuf[0]->outPlanes[0])/2;
    minmaxDebug("6Outbuf 0 dense [%p -> %p]\n", minmax_pOutBuf[0]->outPlanes[0], p);
    minmaxDebug("7Output 0 dense width = %u, height = %u\n", minmax_pOutBuf[0]->spec.width,  minmax_pOutBuf[0]->spec.height,  minmax_pOutBuf[0]->spec.lineStride);
  }

  for(int i=0;i<3;i++) {
    if (1)
    minmax_pOutBuf[1]->outPlanes[i]=(uint8_t*)minmax_outPlanes1[i].get();
    minmaxDebug("8Outbuf 1 plane %d is %p size is: %d\n", i, minmax_pOutBuf[1]->outPlanes[i], minmax_pOutBuf[1]->spec.height * minmax_pOutBuf[1]->spec.width * 4);
  }

  if(minmax_cfg.outDenseEnable1){
    // This should be the same and you should select the proper size anyway
  }
  return 0;
}

















