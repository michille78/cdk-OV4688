///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Application configuration Leon header
///
#include "stdio.h"
#include "stdint.h"
#include "stdlib.h"
#include "string.h"

#include <assert.h>
#include <stdlib.h>

#include <pthread.h>
#include <semaphore.h>

#include "UsbLinkDispatcher.h"
#include "UsbLinkPrivateDefines.h"

typedef enum{
    EVENT_PENDING,
    EVENT_BLOCKED,
    EVENT_READY,
    EVENT_SERVED,

}usbLinkEventState_t;
typedef struct usbLinkEventPriv_t{
    usbLinkEvent_t packet;
    usbLinkEventState_t isServed;
    usbLinkEventOrigin_t origin;
    sem_t* sem;
    void* data;
    uint32_t pad;

}usbLinkEventPriv_t;
#define CIRCULAR_INCREMENT(x,maxVal) \
    { \
         x++; \
         if (x == maxVal) \
             x = 0; \
    }
//avoid problems with unsigned. first compare and then give the nuw value
#define CIRCULAR_DECREMENT(x,maxVal) \
{ \
    if (x == 0) \
        x = maxVal; \
    else \
        x--; \
}
#define printf2(...)
#define printf(...)

static void* eventSchedulerRun(void*);
static usbLinkEventPriv_t* searchForReadyEvent(int remove);

pthread_t UsbLinkThreadId;
sem_t addEventSem;
sem_t notifyDispatcherSem;

struct dispatcherControlFunctions* glControlFunc;
uint32_t resetUsbLink = 0;
static void* eventReader(void* ctx)
{
    (void)ctx;
    usbLinkEvent_t event;
    event.header.id = -1;
    while(!resetUsbLink){
        printf2("Reading\n");
        int sc = glControlFunc->eventReceive(&event);
        if (sc){
            break;
        }
    }
    return 0;
}

//TODO: these will be message queues
volatile int remoteEventNum;
volatile int localEventNum;
volatile int responseEventNum;

volatile int remoteEventProcCount;
volatile int localEventProcCount;

__attribute__((aligned(8))) usbLinkEventPriv_t remoteEventQueue[MAX_EVENTS];
__attribute__((aligned(8))) usbLinkEventPriv_t localEventQueue[MAX_EVENTS];
__attribute__((aligned(8))) usbLinkEventPriv_t responseEventQueue[MAX_EVENTS];

int unservedReqNo = 0;
__attribute__((aligned(8))) usbLinkEventPriv_t* unservedRequests[MAX_EVENTS];
uint32_t semaphores;
typedef struct {
    sem_t sem;
    pthread_t threadId;
}localSem_t;
localSem_t eventSemaphores[MAXIMUM_SEMAPHORES];
static sem_t* getCurrentSem(pthread_t threadId)
{
    localSem_t* sem = eventSemaphores;
    while (sem < eventSemaphores + semaphores){
        if (sem->threadId == threadId){
            return &sem->sem;
        }
        sem++;
    }
    return NULL;
}
static sem_t* createSem(){
    sem_t* sem = getCurrentSem(pthread_self());
    if (sem) // it already exists, error
        return NULL;
    else
    {
        //allocate space
        if (semaphores < MAXIMUM_SEMAPHORES)
            semaphores++;
        else
            return NULL;
        sem = &eventSemaphores[semaphores - 1].sem;
        eventSemaphores[semaphores - 1].threadId = pthread_self();
        if (sem_init(sem, 0, 0))
            perror("Can't create semaphore\n");
        return sem;
    }

}

static int isEventTypeRequest(usbLinkEventType_t type)
{
    if (type < USB_REQUEST_LAST)
        return 1;
    else
        return 0;
}

static int dispatcherEventServe(usbLinkEventPriv_t * event){
    int i=0;
    if (isEventTypeRequest(event->packet.header.type))
    {
        if (event->packet.header.flags.bitField.ack == 1 && event->packet.header.flags.bitField.nack == 0)
        {
            event->isServed = EVENT_PENDING;
            unservedRequests[unservedReqNo++] = event;
            printf2("------------------------UNserved %x %d\n", event->packet.type, unservedReqNo);
            if(MAX_EVENTS <= unservedReqNo){
                for(i = 0; i < MAX_EVENTS; i++)
                    printf2("unserved type %x\n", unservedRequests[i]->packet.type);
                ASSERT_USB_LINK(0);
            }
        }else{
            event->isServed = EVENT_SERVED;
        }
    }else
    {
        int found = 0;
        for (i = 0 ; i< unservedReqNo; i++)
        {

            if(!found && unservedRequests[i]->packet.header.id == event->packet.header.id && unservedRequests[i]->packet.header.type == event->packet.header.type - USB_REQUEST_LAST -1)
            {
                found = 1;
                event->isServed = EVENT_SERVED;
                if (event->packet.header.flags.bitField.nack){
                    printf2("----------------------ISblocked %x %d\n", unservedRequests[i]->packet.type, unservedReqNo);

                    unservedRequests[i]->isServed = EVENT_BLOCKED;
                    return 0;
                }else
                {
                    printf2("----------------------ISserved %x %d\n", unservedRequests[i]->packet.type, unservedReqNo-1);
                    unservedRequests[i]->isServed = EVENT_SERVED;
                }
                //propagate back flags
                unservedRequests[i]->packet.header.flags = event->packet.header.flags;
                if(unservedRequests[i]->sem)
                {
                    if (sem_post(unservedRequests[i]->sem)) {
                        printf2("can't post semaphore\n");
                    }
                }
            }
            if(found){
                printf("delete %d %d\n", unservedRequests[i]->packet.header.type, i);
                unservedRequests[i] = unservedRequests[i+1];
            }
        }
        if(found)
            unservedReqNo--;
        else{
            printf2("no request for this response: %d\n", event->packet.type);
            ASSERT_USB_LINK(0);
        }
    }
    return 0;
}

static usbLinkEventPriv_t* searchForReadyEvent(int remove)
{
    int i;
    usbLinkEventPriv_t* ev = NULL;
    for (i = 0 ; i< unservedReqNo; i++)
    {
        {
            if (unservedRequests[i]->isServed == EVENT_READY){

                ev = unservedRequests[i];
                break;
            }
        }
    }
    if (ev && remove){
        for (; i< unservedReqNo; i++)
        {
            unservedRequests[i] = unservedRequests[i+1];
        }
        unservedReqNo--;
    }
    if (ev) {
        printf("ready %d %d \n", (int)ev->packet.header.type, (int)ev->packet.header.id);
    }
    return ev;
}

static usbLinkEventPriv_t* dispatcherGetNextEvent()
{
     usbLinkEventPriv_t* event = NULL;
     event = searchForReadyEvent(1);
     if (event) {
         return event;
     }
     if (sem_wait(&notifyDispatcherSem)) {
         printf2("can't post semaphore\n");
     }
     if(localEventNum != localEventProcCount){
         while(localEventQueue[localEventProcCount].isServed != EVENT_SERVED)
             CIRCULAR_INCREMENT(localEventProcCount, MAX_EVENTS);
         event = &localEventQueue[localEventProcCount];
         event->origin = EVENT_LOCAL;
         CIRCULAR_INCREMENT(localEventProcCount, MAX_EVENTS);
     }else if(remoteEventNum != remoteEventProcCount){
         while(remoteEventQueue[remoteEventProcCount].isServed != EVENT_SERVED)
             CIRCULAR_INCREMENT(remoteEventProcCount, MAX_EVENTS);

         event = &remoteEventQueue[remoteEventProcCount];
         event->origin = EVENT_REMOTE;
         CIRCULAR_INCREMENT(remoteEventProcCount, MAX_EVENTS);
     }
     return event;
}

void dispatcherFindCorrespondingWriteRtsAndFillData(usbLinkEvent_t* event) {
    int i;
    event->data = 0;
    for (i = 0 ; i< unservedReqNo; i++)
    {
        if(unservedRequests[i]->packet.header.id == event->header.id && unservedRequests[i]->packet.header.type == USB_WRITE_RTS)
        {
           event->data = unservedRequests[i]->packet.data;
        }
    }

}

static void dispatcherReset() {
    int i;
    glControlFunc->closeLink();
     if (sem_post(&notifyDispatcherSem)){
         printf2("can't post semaphore\n"); //to allow us to get a NULL event
     }
     usbLinkEventPriv_t* event = dispatcherGetNextEvent();
     while (event != NULL) {
        event = dispatcherGetNextEvent();
     }
    for(i=0; i<unservedReqNo; i++){
        if(unservedRequests[i]->sem)
        {
            if (sem_post(unservedRequests[i]->sem)){
                printf2("can't post semaphore\n");
            }
        }
    }
    glControlFunc->resetDevice();
    printf("Reset Successfully\n");
}
static void* eventSchedulerRun(void* ctx)
{
    (void)ctx;
    pthread_t readerThreadId;

    // create thread for reader. This thread will notify the dispatcher of any incoming packets
    pthread_attr_t attr;
    int sc;
    if(pthread_attr_init(&attr) !=0) {
        printf2("pthread_attr_init error");
    }
#ifndef __PC__
    if(pthread_attr_setinheritsched(&attr, PTHREAD_EXPLICIT_SCHED) != 0) {
        printf2("pthread_attr_setinheritsched error");
    }
    if(pthread_attr_setschedpolicy(&attr, SCHED_RR) != 0) {
        printf2("pthread_attr_setschedpolicy error");
    }
#endif
    sc = pthread_create( &readerThreadId, &attr, eventReader, NULL);
    if (sc) {
        perror("Thread creation failed");
    }
    usbLinkEventPriv_t* event;
    usbLinkEventPriv_t response;

    while(!resetUsbLink){
        event = dispatcherGetNextEvent();
        if (event->origin == EVENT_LOCAL)
        {
           if(glControlFunc->localGetResponse(&event->packet, &response.packet))
           {
               dispatcherEventServe(event);
               glControlFunc->eventSend(&event->packet);
           }else{
               // reads will be served locally.
               dispatcherEventServe(event);
               dispatcherEventServe(&response);
           }
       }else{
           if(glControlFunc->remoteGetResponse(&event->packet, &response.packet)){
               responseEventQueue[responseEventNum].packet = response.packet;
               dispatcherEventServe(event);
               dispatcherEventServe(&responseEventQueue[responseEventNum]);
               glControlFunc->eventSend(&response.packet);
               CIRCULAR_INCREMENT(responseEventNum, MAX_EVENTS);
               if(event->packet.header.type == USB_RESET_REQ) {
                    resetUsbLink = 1;
               }
           }
           else
           {
               dispatcherEventServe(event);
               if(event->packet.header.type == USB_RESET_RESP) {
                    resetUsbLink = 1;
               }
           }
       }
    }
    pthread_join(readerThreadId, NULL);
    dispatcherReset();
    return NULL;
}

static int createUniqueID()
{
    static int id = 0xa;
    return id++;
}
//adds a new event with parameters and returns event id
usbLinkEvent_t* dispatcherAddEvent(usbLinkEventOrigin_t origin, usbLinkEvent_t *event)
{
    if(resetUsbLink) {
        return NULL;
    }
    printf2("receiving event %d %d\n",event->header.type, origin );
    if (sem_wait(&addEventSem)){
        printf2("can't wait semaphore\n");
    }
     sem_t *sem = NULL;
     usbLinkEvent_t* ev;
     if (origin == EVENT_LOCAL){
         event->header.id = createUniqueID();
         sem = getCurrentSem(pthread_self());
         if (!sem){
             sem = createSem();
         }
         if (!sem){
             return NULL;
         }

         event->header.flags.raw= 0;
         event->header.flags.bitField.ack = 1;
         while(localEventQueue[localEventNum].isServed != EVENT_SERVED)
             CIRCULAR_INCREMENT(localEventNum, MAX_EVENTS);
         printf2("received event %d %d\n",event->header.type, origin );

         ev = &localEventQueue[localEventNum].packet;
         localEventQueue[localEventNum].sem = sem;
         localEventQueue[localEventNum].packet=*event;
         localEventQueue[localEventNum].origin = origin;
         CIRCULAR_INCREMENT(localEventNum, MAX_EVENTS);
     }
     else{
         while(remoteEventQueue[remoteEventNum].isServed != EVENT_SERVED)
             CIRCULAR_INCREMENT(remoteEventNum, MAX_EVENTS);
         ev = &remoteEventQueue[remoteEventNum].packet;

         remoteEventQueue[remoteEventNum].sem = NULL;
         remoteEventQueue[remoteEventNum].packet=*event;
         remoteEventQueue[remoteEventNum].origin=origin;
         CIRCULAR_INCREMENT(remoteEventNum, MAX_EVENTS);
     }
     if (sem_post(&addEventSem)) {
         printf2("can't post semaphore\n");
     }
     if (sem_post(&notifyDispatcherSem)) {
         printf2("can't post semaphore\n");
     }
    return ev;
}
 int dispatcherWaitEventComplete()
{
    sem_t* id = getCurrentSem(pthread_self());
    if (id == NULL) {
        return -1;
    }
    return sem_wait(id);
}

 int dispatcherUnblockEvent(eventId_t id, usbLinkEventType_t type, streamId_t stream)
 {
     printf("unblock\n");
     usbLinkEventPriv_t* ev = NULL;
     int i;
     for (i = 0 ; i< unservedReqNo; i++)
     {
         if(((unservedRequests[i]->packet.header.id == id && unservedRequests[i]->packet.header.type == type && unservedRequests[i]->packet.header.streamId == stream) ||
                         (unservedRequests[i]->packet.header.type == type && id == -1 && unservedRequests[i]->packet.header.streamId == stream)))
         {
             if(unservedRequests[i]->isServed == EVENT_BLOCKED){
                 ev = unservedRequests[i];
                 ev->isServed = EVENT_READY;
                 printf("unblocked**************** %d %d\n", (int)unservedRequests[i]->packet.header.id, (int)unservedRequests[i]->packet.header.type);

                 return 1;
             }else{}
//                 return 2;
         }
         else{
             printf("%d %d\n", (int)unservedRequests[i]->packet.header.id, (int)unservedRequests[i]->packet.header.type);
         }
     }
     return 0;
 }
 void dispatcherBlockEvent(eventId_t id, usbLinkEventType_t type)
 {
     usbLinkEventPriv_t* ev = NULL;
     int i;
     for (i = 0 ; i< unservedReqNo; i++)
     {
         if(((unservedRequests[i]->packet.header.id == id && unservedRequests[i]->packet.header.type == type) ||
                         (unservedRequests[i]->packet.header.type == type && id == -1)) && unservedRequests[i]->isServed == EVENT_PENDING)
         {
             ev = unservedRequests[i];
             ev->isServed = EVENT_BLOCKED;
             break;
         }
     }
     ASSERT_USB_LINK(i!=unservedReqNo);
 }
 int dispatcherInitialize(struct dispatcherControlFunctions* controlFunc){
     // create thread which will communicate with the pc

     semaphores = 0;
     remoteEventNum = 0;
     localEventNum = 0;
     responseEventNum = 0;

     remoteEventProcCount = 0;
     localEventProcCount = 0;

     if (!controlFunc || !controlFunc->eventReceive ||
                     !controlFunc->eventSend ||
                     !controlFunc->localGetResponse ||
                     !controlFunc->remoteGetResponse){
         return -1;
     }
     int i;
     for (i = 0 ; i < MAX_EVENTS; i++)
     {
         remoteEventQueue[i].isServed = EVENT_SERVED;
         localEventQueue[i].isServed = EVENT_SERVED;
         responseEventQueue[i].isServed = EVENT_SERVED;
     }
     glControlFunc = controlFunc;
     pthread_attr_t attr;
     int sc;
     if (sem_init(&addEventSem, 0, 1)){
         perror("Can't create semaphore\n");
         return -1;
     }
     if (sem_init(&notifyDispatcherSem, 0, 0)){
         perror("Can't create semaphore\n");
     }

     if(pthread_attr_init(&attr) !=0) {
         printf2("pthread_attr_init error");
     }
#ifndef __PC__
     if(pthread_attr_setinheritsched(&attr, PTHREAD_EXPLICIT_SCHED) != 0) {
         printf2("pthread_attr_setinheritsched error");
     }
     if(pthread_attr_setschedpolicy(&attr, SCHED_RR) != 0) {
         printf2("pthread_attr_setschedpolicy error");
     }
#endif
     sc = pthread_create( &UsbLinkThreadId, &attr, eventSchedulerRun, NULL);
     if (sc) {
         perror("Thread creation failed");
     }
     return 0;
 }

/* end of file */
