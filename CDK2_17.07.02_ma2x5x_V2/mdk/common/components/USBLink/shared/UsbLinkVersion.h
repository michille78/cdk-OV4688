///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Application configuration Leon header
///

#define USB_LINK_VERSION_MAJOR 1
#define USB_LINK_VERSION_MINOR 0
#define USB_LINK_VERSION_PATCH 0
