///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Application configuration Leon header
///
#ifndef _USBLINK_H
#define _USBLINK_H
#include "UsbLinkPublicDefines.h"

#ifdef __cplusplus
extern "C"
{
#endif

// Initializes USB communication and pings the remote
USBLinkError_t UsbLinkInitialize(USBLinkHandler_t* handler);

// Opens a stream in the remote that can be written to by the local
// Allocates stream_write_size for that stream
streamId_t UsbLinkOpenStream(char* name, int stream_write_size);

// Currently useless
USBLinkError_t UsbLinkGetAvailableStreams();

// Send a package to initiate the writing of data to a remote stream
USBLinkError_t UsbLinkWriteData(int streamId, void* buffer, int size);

// Currently useless
USBLinkError_t UsbLinkAsyncWriteData();

// Read data from local stream. Will only have something if it was written
// to by the remote
USBLinkError_t UsbLinkReadData(int streamId, streamPacketDesc_t** packet);

// Release data from stream - This should be called after ReadData
USBLinkError_t UsbLinkReleaseData(int streamId);

// Boot the remote (This is intended as an interface to boot the Myriad
// from PC)
USBLinkError_t UsbLinkBootRemote(char* bootUtility, char* binaryPath);

// Reset the remote and reset local as well
USBLinkError_t UsbLinkResetRemote();

// Profiling funcs
USBLinkError_t USBLinkProfStart();
USBLinkError_t USBLinkProfStop();
USBLinkError_t USBLinkProfPrint();
#ifdef __cplusplus
}
#endif

#endif
