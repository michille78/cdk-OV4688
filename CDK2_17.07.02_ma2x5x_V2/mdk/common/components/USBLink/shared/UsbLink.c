///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Application configuration Leon header
///

#include "UsbLink.h"

#include "stdio.h"
#include "stdint.h"
#include "string.h"

#include <assert.h>
#include <stdlib.h>

#include <pthread.h>
#include <semaphore.h>
#include "mvMacros.h"
#include "UsbLinkPlatform.h"
#include "UsbLinkDispatcher.h"
#define _USBLINK_ENABLE_PRIVATE_INCLUDE_
#include "UsbLinkPrivateDefines.h"

#define CIRCULAR_INCREMENT(x,maxVal) \
    { \
         x++; \
         if (x == maxVal) \
             x = 0; \
    }
//avoid problems with unsigned. first compare and then give the nuw value
#define CIRCULAR_DECREMENT(x,maxVal) \
{ \
    if (x == 0) \
        x = maxVal; \
    else \
        x--; \
}

#define printf2(...)
int dispatcherLocalEventGetResponse(usbLinkEvent_t* event, usbLinkEvent_t* response);
int dispatcherRemoteEventGetResponse(usbLinkEvent_t* event, usbLinkEvent_t* response);
//adds a new event with parameters and returns event id
int dispatcherEventSend(usbLinkEvent_t* event);
streamDesc_t* getStreamById(streamId_t id);
void releaseStream();
int addNewPacketToStream(streamDesc_t* stream, void* buffer, uint32_t size);

USBLinkHandler_t* glHandler;
//streams
int numberOfStreams=0;
streamDesc_t availableStreams[USB_LINK_MAX_STREAMS];
sem_t  pingSem;
usbLinkState_t peerState = USB_LINK_NOT_INIT;
static float timespec_diff(struct timespec *start, struct timespec *stop)
{
    if ((stop->tv_nsec - start->tv_nsec) < 0) {
        start->tv_sec = stop->tv_sec - start->tv_sec - 1;
        start->tv_nsec = stop->tv_nsec - start->tv_nsec + 1000000000;
    } else {
        start->tv_sec = stop->tv_sec - start->tv_sec;
        start->tv_nsec = stop->tv_nsec - start->tv_nsec;
    }

    return start->tv_nsec/ 1000000000.0 + start->tv_sec;
}
 int handleIncomingEvent(usbLinkEvent_t* event){
    //this function will be dependent whether this is a client or a Remote
    //specific actions to this peer
    void* buffer ;
    streamDesc_t* stream ;
    switch (event->header.type){
    case USB_WRITE_RTS:
        break;
    case USB_WRITE_DATA:
        // If we got here, we will read the data no matter what happens.
        // If we encounter any problems we will still read the data to keep the communication working but send a NACK.
        stream = getStreamById(event->header.streamId);
        int32_t sc = swcFifoGetWritePtr(&stream->fifo, &buffer, ALIGN_UP(event->header.size, CACHE_LINE_SIZE));
        //end of fifo. We need contiguous space, so get new buffer from the beginning
        if (sc < ALIGN_UP((int32_t)event->header.size, CACHE_LINE_SIZE)){
            swcFifoMarkWriteDone(&stream->fifo);
            sc = swcFifoGetWritePtr(&stream->fifo, &buffer, ALIGN_UP(event->header.size, CACHE_LINE_SIZE));
        }
        printf2("fifo available size %d\n",swcFifoAvailable(&stream->fifo));
        if (sc != ALIGN_UP((int32_t)event->header.size, CACHE_LINE_SIZE)){
            printf2("allocated data not equal to requested%d %d\n", sc, ALIGN_UP((int32_t)event->header.size, CACHE_LINE_SIZE));
            ASSERT_USB_LINK(0);
        }
        USBLinkRead(buffer, event->header.size);
        event->data = buffer;
        swcFifoMarkWriteDone(&stream->fifo);
        addNewPacketToStream(stream, buffer, event->header.size);
        releaseStream(stream);
        break;
    case USB_READ_REQ:
        break;
    case USB_READ_REL_REQ:
        break;
    case USB_CREATE_STREAM_REQ:
        break;
    case USB_PING_REQ:
        break;
    case USB_RESET_REQ:
        break;
    case USB_WRITE_CTS:
        break;
    case USB_WRITE_DATA_ACK:
        break;
    case USB_READ_RESP:
        break;
    case USB_READ_REL_RESP:
        break;
    case USB_CREATE_STREAM_RESP:
        break;
    case USB_PING_RESP:
        break;
    case USB_RESET_RESP:
        break;
    default:
        ASSERT_USB_LINK(0);
    }
    //adding event for the scheduler. We let it know that this is a remote event
    dispatcherAddEvent(EVENT_REMOTE, event);
    return 0;
}
 int dispatcherEventReceive(usbLinkEvent_t* event){
     static usbLinkEvent_t prevEvent;
     int sc = USBLinkRead(&event->header, sizeof(event->header));
     if(sc < 0)
         return sc;
     printf2("Incoming event %d %d %d %d\n", event->header.type,  event->header.id, prevEvent.header.id, prevEvent.header.type);
     if (prevEvent.header.id == event->header.id && prevEvent.header.type == event->header.type){
         //TODO: now this is ugly... it seems to be a concurrency issue in VSC
         printf2("UGLY bugbugbugbugubugbugubugubgubugubugub\n");
         ASSERT_USB_LINK(0);
     }
     prevEvent = *event;
     handleIncomingEvent(event);
     return 0;
 }

streamDesc_t* getStreamById(streamId_t id){
   int stream;
   for(stream = 0; stream < numberOfStreams; stream++)
   {
       if (availableStreams[stream].id == id){
           sem_wait(&availableStreams[stream].sem);
           return &availableStreams[stream];
       }
   }
   return NULL;
}
streamDesc_t* getStreamByName(char* name){
   int stream;
   for(stream = 0; stream < numberOfStreams; stream++)
   {
       if (strcmp(availableStreams[stream].name, name) == 0){
           sem_wait(&availableStreams[stream].sem);
           return &availableStreams[stream];
       }
   }
   return NULL;
}
void releaseStream(streamDesc_t* stream){
    sem_post(&stream->sem);
}
streamId_t getStreamIdByName(char* name){
    streamDesc_t* stream = getStreamByName(name);
    if (stream) {
        releaseStream(stream);
        return stream->id;
    }
    else
        return INVALID_STREAM_ID;
}
streamPacketDesc_t* getPacketFromStream(streamDesc_t* stream)
{
    streamPacketDesc_t* ret = NULL;
    if (stream->availablePackets)
    {
        ret = &stream->packets[stream->firstPacketUnused];
        stream->availablePackets--;
        CIRCULAR_INCREMENT(stream->firstPacketUnused, USB_LINK_MAX_PACKETS_PER_STREAM);
        stream->blockedPackets++;
    }
    return ret;
}
int releasePacketFromStream(streamDesc_t* stream)
{
    streamPacketDesc_t* currPack = &stream->packets[stream->firstPacket];
    void* ptr;
    int32_t bytes = swcFifoGetReadPtr(&stream->fifo, &ptr, ALIGN_UP(currPack->length, CACHE_LINE_SIZE));
    if (bytes < ALIGN_UP((int32_t)currPack->length, CACHE_LINE_SIZE))
    {
        //end of fifo. this was dropped at write. Get the real data from the begiining of the fifo
        swcFifoMarkReadDone(&stream->fifo);
        bytes = swcFifoGetReadPtr(&stream->fifo, &ptr, ALIGN_UP(currPack->length, CACHE_LINE_SIZE));
    }
    ASSERT_USB_LINK(bytes == (int32_t)ALIGN_UP(currPack->length, CACHE_LINE_SIZE));
    ASSERT_USB_LINK(ptr == (void*)currPack->data);
    swcFifoMarkReadDone(&stream->fifo);

    CIRCULAR_INCREMENT(stream->firstPacket, USB_LINK_MAX_PACKETS_PER_STREAM);
    stream->blockedPackets--;
    return 0;
}
int isStreamSpaceEnoughFor(streamDesc_t* stream, uint32_t size)
{
    if(stream->availablePackets + stream->blockedPackets >= USB_LINK_MAX_PACKETS_PER_STREAM ||
                    swcFifoContigAvailable(&stream->fifo) < size)
        return 0;
    else
        return 1;
}
int addNewPacketToStream(streamDesc_t* stream, void* buffer, uint32_t size){
    if (stream->availablePackets + stream->blockedPackets < USB_LINK_MAX_PACKETS_PER_STREAM)
    {
        stream->packets[stream->firstPacketFree].data = buffer;
        stream->packets[stream->firstPacketFree].length = size;
        CIRCULAR_INCREMENT(stream->firstPacketFree, USB_LINK_MAX_PACKETS_PER_STREAM);
        stream->availablePackets++;
        return 0;
    }
    return -1;
}
 streamId_t allocateNewStream(char* name, uint32_t writeSize, uint32_t readSize, streamId_t forcedId)
{
    uint64_t streamId;
    streamDesc_t* stream;
    stream = getStreamByName(name);
    if (stream != NULL){
        //the stream already exists
        if ((writeSize > stream->writeSize && stream->writeSize != 0) ||
            (readSize > stream->readSize && stream->readSize != 0))
        {
            return INVALID_STREAM_ID;
        }
    }else
    {
        stream = &availableStreams[numberOfStreams];
        if (forcedId == INVALID_STREAM_ID)
            stream->id = numberOfStreams;
        else
            stream->id = forcedId;
        strncpy(stream->name, name, MAX_NAME_LENGTH);
        numberOfStreams++;
        stream->readSize = 0;
        stream->writeSize = 0;
        sem_init(&stream->sem, 0, 0);
    }
    if (readSize && !stream->readSize)
    {
        stream->readSize = readSize;
        void* buff = allocateData(readSize, CACHE_LINE_SIZE);
        if(!buff) {
            return INVALID_STREAM_ID;
        }
        swcFifoInit(&stream->fifo, buff, readSize);
    }
    if (writeSize && !stream->writeSize)
    {
        stream->writeSize = writeSize;
    }
    streamId = stream->id;
    sem_post(&stream->sem);
    return streamId;
}

//this function should be called only for remote requests
int dispatcherLocalEventGetResponse(usbLinkEvent_t* event, usbLinkEvent_t* response)
{
    streamDesc_t* stream;
    response->header.id = event->header.id;
    switch (event->header.type){
    case USB_WRITE_RTS:
        // TODO: check here if streamId and other input data is correct.
        // Fill the response with WRITE_DATA_ACK type and NACK to propagate the error back to tha API function
        // return -1 to don't even send it to the remote
        return 1;
        break;
    case USB_WRITE_CTS:
        // shouldn't happen
        return 1;
        break;
    case USB_READ_REQ:
        stream = getStreamById((event->header.streamId));
        ASSERT_USB_LINK(stream);
        streamPacketDesc_t* packet = getPacketFromStream(stream);
        *response = *event;
        if (packet){
            //the read can be served with this packet
            streamPacketDesc_t** pack = (streamPacketDesc_t**)event->data;
            *pack = packet;
            response->header.type = USB_READ_RESP;
            response->header.flags.bitField.ack = 1;
            response->header.flags.bitField.nack = 0;
        }
        else{
            response->header.type = USB_READ_RESP;
            response->header.flags.bitField.ack = 0;
            response->header.flags.bitField.nack = 1;
        }
        releaseStream(stream);
        return 0;
        break;
    case USB_READ_REL_REQ:
        stream = getStreamById((event->header.streamId));
        releasePacketFromStream(stream);
        releaseStream(stream);
        return 1;
        break;
    case USB_CREATE_STREAM_REQ:
        return 1;
        break;
    case USB_PING_REQ:
        return 1;
        break;
    case USB_RESET_REQ:
        return 1;
        break;
    case USB_WRITE_DATA:
        return 1;
        break;
    case USB_WRITE_DATA_ACK:
        return 1;
        break;
    case USB_READ_RESP:
        return 1;
        break;
    case USB_READ_REL_RESP:
        return 1;
        break;
    case USB_CREATE_STREAM_RESP:
        return 1;
        break;
    case USB_PING_RESP:
        return 1;
        break;
    case USB_RESET_RESP:
        //should not happen
        return 0;
        break;
    default:
        ASSERT_USB_LINK(0);
    }
    return 0;
}
//this function should be called only for remote requests
int dispatcherRemoteEventGetResponse(usbLinkEvent_t* event, usbLinkEvent_t* response)
{
    response->header.id = event->header.id;
    response->header.flags.raw = 0;
    switch (event->header.type){
    case USB_WRITE_RTS:
        //let remote write immediately as we have a local buffer for the data
        response->header.type = USB_WRITE_CTS;
        response->header.size = event->header.size;
        response->header.streamId = event->header.streamId;
        response->data = 0;
        //we need to check if we have enough space in the stream, if not we will try to serve this after the next host read.
        streamDesc_t* stream = getStreamById(event->header.streamId);
        if (!stream)
        {
            response->header.flags.bitField.nack = 1;
            response->header.flags.bitField.noSuchStream = 1;
        }else if (!isStreamSpaceEnoughFor(stream, event->header.size)){
            event->header.flags.bitField.nack = 1;
            event->header.flags.bitField.bufferFull = 1;

            response->header.flags.bitField.nack = 1;
            response->header.flags.bitField.bufferFull = 1;
        }else{
            response->header.flags.bitField.ack = 1;
        }
        releaseStream(stream);
        return 1;
        break;
    case USB_WRITE_DATA:

        response->header.type = USB_WRITE_DATA_ACK;
        response->header.size = event->header.size;
        response->header.streamId = event->header.streamId;
        response->header.flags.bitField.ack = 1;
        // we got some data. We should unblock a blocked read
        int xxx = dispatcherUnblockEvent(-1, USB_READ_REQ, response->header.streamId);
	    (void) xxx;
	printf2("unblocked from stream %d %d\n", response->header.streamId, xxx);
        return 1;
        break;
    case USB_READ_REQ:
        return 0;
        break;
    case USB_READ_REL_REQ:
        response->header.flags.bitField.ack = 1;
        response->header.type = USB_READ_REL_RESP;

        dispatcherUnblockEvent(-1, USB_WRITE_RTS, event->header.streamId);
        return 1;
        break;
    case USB_CREATE_STREAM_REQ:
        // TODO: concurrency issue. We may need to add some handshake to avoid it
        response->header.flags.bitField.ack = 1;
        response->header.type = USB_CREATE_STREAM_RESP;
        //write size from remote means read size for this peer
        response->header.streamId = allocateNewStream(event->header.streamName,
                                                                 0, event->header.size,
                                                                 INVALID_STREAM_ID);
        strncpy(response->header.streamName, event->header.streamName, MAX_NAME_LENGTH);
        response->header.size = event->header.size;
        printf2("creating stream %d\n", response->header.streamId);
        return 1;
        break;
    case USB_WRITE_CTS:
        if (event->header.flags.bitField.nack){
            printf2("nack\n");
            printf2("%x\n", event->header.flags.raw);
            dispatcherBlockEvent(event->header.id, USB_WRITE_RTS);
            return 0;
        }
        response->header.size = event->header.size;
        response->header.flags.bitField.ack = 1;
        response->header.type = USB_WRITE_DATA;
        response->header.size = event->header.size;
        response->header.streamId = event->header.streamId;
        dispatcherFindCorrespondingWriteRtsAndFillData(response);
        return 1;
        break;
    case USB_PING_REQ:
        response->header.type = USB_PING_RESP;
        response->header.flags.bitField.ack = 1;
        sem_post(&pingSem);

        return 1;
        break;
    case USB_RESET_REQ:
        printf2("reset request\n");
        response->header.flags.bitField.ack = 1;
        response->header.flags.bitField.nack = 0;
        response->header.type = USB_RESET_RESP;
        // need to send the response, serve the event and then reset
        return 1;
        break;
    case USB_WRITE_DATA_ACK:
        break;
    case USB_READ_RESP:
        break;
    case USB_READ_REL_RESP:
        break;
    case USB_CREATE_STREAM_RESP:
    {
        // write_size from the response the size of the buffer from the remote
        response->header.streamId = allocateNewStream(event->header.streamName,
                                                                 event->header.size,0,
                                                                 event->header.streamId);
        break;
    }
    case USB_PING_RESP:
        break;
    case USB_RESET_RESP:
        return 0;
        break;
    default:
        ASSERT_USB_LINK(0);
    }
    return 0;
}
//adds a new event with parameters and returns event id
int dispatcherEventSend(usbLinkEvent_t *event)
{
    printf2 ("sending %d %d\n", event->header.type,  event->header.id);
    USBLinkWrite(&event->header, sizeof(event->header));

    if (event->header.type == USB_WRITE_DATA)
    {
        //write requested data
        USBLinkWrite(event->data, event->header.size);
    }
    // this function will send events to the remote node
    return 0;
}
struct dispatcherControlFunctions controlFunctionTbl;
static usbLinkState_t getUsbLinkState()
{
    return peerState;
}
void dispatcherCloseUsbLink(){
    peerState = USB_LINK_COMMUNICATION_NOT_OPEN;
}
void dispatcherResetDevice(){
    USBLinkPlatformResetRemote();
}

USBLinkError_t UsbLinkInitialize(USBLinkHandler_t* handler)
{
    ASSERT_USB_LINK(USB_LINK_MAX_STREAMS <= MAX_POOLS_ALLOC);
    peerState = USB_LINK_NOT_INIT;
    glHandler = handler;
    sem_init(&pingSem,0,0);

    int sc = UsbLinkPlatformInit( handler->devicePath2, handler->devicePath);
    if (sc){
       return USB_LINK_COMMUNICATION_NOT_OPEN;
    }
    controlFunctionTbl.eventReceive = &dispatcherEventReceive;
    controlFunctionTbl.eventSend = &dispatcherEventSend;
    controlFunctionTbl.localGetResponse = &dispatcherLocalEventGetResponse;
    controlFunctionTbl.remoteGetResponse = &dispatcherRemoteEventGetResponse;
    controlFunctionTbl.closeLink = &dispatcherCloseUsbLink;
    controlFunctionTbl.resetDevice = &dispatcherResetDevice;
    dispatcherInitialize(&controlFunctionTbl);
#ifdef __PC__
    usbLinkEvent_t event;
    event.header.type = USB_PING_REQ;
    dispatcherAddEvent(EVENT_LOCAL, &event);
    dispatcherWaitEventComplete();
#else
    printf2("waiting\n");
    sem_wait(&pingSem);
    printf2("********************************************************\n");

#endif
    peerState = USB_LINK_UP;
    return USB_LINK_SUCCESS;
}

streamId_t UsbLinkOpenStream(char* name, int stream_write_size)
{
    if (getUsbLinkState() != USB_LINK_UP)
    {
        return INVALID_STREAM_ID;
    }
    if(strlen(name) > MAX_NAME_LENGTH){
        printf2("name too long\n");
        return INVALID_STREAM_ID;
    }
    if(stream_write_size > 0)
    {
        usbLinkEvent_t event;
        event.header.type = USB_CREATE_STREAM_REQ;
        strncpy(event.header.streamName, name, MAX_NAME_LENGTH);
        event.header.size = stream_write_size;
        event.header.streamId = INVALID_STREAM_ID;
        dispatcherAddEvent(EVENT_LOCAL, &event);
        dispatcherWaitEventComplete();
    }
    return getStreamIdByName(name);
}
USBLinkError_t UsbLinkGetAvailableStreams()
{
    if (getUsbLinkState() != USB_LINK_UP)
    {
        return USB_LINK_COMMUNICATION_NOT_OPEN;
    }
    return USB_LINK_SUCCESS;
}
USBLinkError_t UsbLinkWriteData(int streamId, void* buffer, int size)
{
    if (getUsbLinkState() != USB_LINK_UP)
    {
        return USB_LINK_COMMUNICATION_NOT_OPEN;
    }
    struct timespec start, end;
    clock_gettime(CLOCK_REALTIME, &start);

    usbLinkEvent_t event;
    event.header.type = USB_WRITE_RTS;
    event.header.size = size;
    event.header.streamId = streamId;
    event.data = buffer;
    usbLinkEvent_t* ev = dispatcherAddEvent(EVENT_LOCAL, &event);
    dispatcherWaitEventComplete();
    clock_gettime(CLOCK_REALTIME, &end);
    if( glHandler->profEnable){
        glHandler->profilingData.totalWriteBytes += size;
        glHandler->profilingData.totalWriteTime += timespec_diff(&start, &end);

    }
    if (ev->header.flags.bitField.ack == 1)
        return USB_LINK_SUCCESS;
    else
        return USB_LINK_COMMUNICATION_FAIL;
}
USBLinkError_t UsbLinkAsyncWriteData()
{
    if (getUsbLinkState() != USB_LINK_UP)
    {
        return USB_LINK_COMMUNICATION_NOT_OPEN;
    }
    return USB_LINK_SUCCESS;
}
USBLinkError_t UsbLinkReadData(int streamId, streamPacketDesc_t** packet)
{
    if (getUsbLinkState() != USB_LINK_UP)
    {
        return USB_LINK_COMMUNICATION_NOT_OPEN;
    }
    usbLinkEvent_t event;
    event.header.type = USB_READ_REQ;
    event.header.size = 0;
    event.header.streamId = streamId;
    event.data = (void*)packet;
    struct timespec start, end;
    clock_gettime(CLOCK_REALTIME, &start);

    usbLinkEvent_t* ev = dispatcherAddEvent(EVENT_LOCAL, &event);
    dispatcherWaitEventComplete();
    clock_gettime(CLOCK_REALTIME, &end);

    if( glHandler->profEnable){
        glHandler->profilingData.totalReadBytes += (*packet)->length;
        glHandler->profilingData.totalReadTime += timespec_diff(&start, &end);
    }
    if (ev->header.flags.bitField.ack == 1)
        return USB_LINK_SUCCESS;
    else
        return USB_LINK_COMMUNICATION_FAIL;
}

USBLinkError_t UsbLinkReleaseData(int streamId)
{
    if (getUsbLinkState() != USB_LINK_UP)
    {
        return USB_LINK_COMMUNICATION_NOT_OPEN;
    }
    usbLinkEvent_t event;
    event.header.type = USB_READ_REL_REQ;
    event.header.streamId = streamId;
    usbLinkEvent_t* ev = dispatcherAddEvent(EVENT_LOCAL, &event);
    dispatcherWaitEventComplete();
    if (ev->header.flags.bitField.ack == 1)
        return USB_LINK_SUCCESS;
    else
        return USB_LINK_COMMUNICATION_FAIL;
}

USBLinkError_t UsbLinkBootRemote(char* bootUtility, char* binaryPath){
    if (UsbLinkPlatformBootRemote(bootUtility, binaryPath) == 0)
        return USB_LINK_SUCCESS;
    else
        return USB_LINK_COMMUNICATION_FAIL;
}
USBLinkError_t UsbLinkResetRemote(){
    if (getUsbLinkState() != USB_LINK_UP)
    {
        USBLinkPlatformResetRemote();
        return USB_LINK_COMMUNICATION_NOT_OPEN;
    }
    usbLinkEvent_t event;
    event.header.type = USB_RESET_REQ;
    dispatcherAddEvent(EVENT_LOCAL, &event);
    dispatcherWaitEventComplete();
    return USB_LINK_SUCCESS;

}
USBLinkError_t USBLinkProfStart()
{
    glHandler->profEnable = 1;
    glHandler->profilingData.totalReadBytes = 0;
    glHandler->profilingData.totalWriteBytes = 0;
    glHandler->profilingData.totalWriteTime = 0;
    glHandler->profilingData.totalReadTime = 0;
    glHandler->profilingData.totalBootCount = 0;
    glHandler->profilingData.totalBootTime = 0;

    return USB_LINK_SUCCESS;

}
USBLinkError_t USBLinkProfStop()
{
    glHandler->profEnable = 0;
    return USB_LINK_SUCCESS;

}
USBLinkError_t USBLinkProfPrint()
{

    printf("USB Link profiling results:\n");
    if (glHandler->profilingData.totalWriteTime)
    {
        printf("Average write speed: %f MB/Sec\n",
               glHandler->profilingData.totalWriteBytes / glHandler->profilingData.totalWriteTime / 1024.0 / 1024.0 );
    }
    if (glHandler->profilingData.totalReadTime)
    {
        printf("Average read speed: %f MB/Sec\n",
               glHandler->profilingData.totalReadBytes / glHandler->profilingData.totalReadTime / 1024.0 / 1024.0);
    }
    if (glHandler->profilingData.totalBootCount)
    {
        printf("Average boot speed: %f sec\n",
               glHandler->profilingData.totalBootTime / glHandler->profilingData.totalBootCount);
    }
    return USB_LINK_SUCCESS;
}


/* end of file */
