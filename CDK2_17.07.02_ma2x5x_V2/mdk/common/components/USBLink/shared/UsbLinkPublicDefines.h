///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Application configuration Leon header
///
#ifndef _USBLINKPUBLICDEFINES_H
#define _USBLINKPUBLICDEFINES_H
#include <stdint.h>
#ifdef __cplusplus
extern "C"
{
#endif

#define USB_LINK_MAX_STREAMS 8
#define USB_LINK_MAX_PACKETS_PER_STREAM 64

typedef enum{
    USB_LINK_SUCCESS = 0,
    USB_LINK_ALREADY_OPEN,
    USB_LINK_COMMUNICATION_NOT_OPEN,
    USB_LINK_COMMUNICATION_FAIL,
    USB_LINK_COMMUNICATION_UNKNOWN_ERROR,
}USBLinkError_t;

#define USB_LINK_INVALID_FD  (-314)

#define INVALID_STREAM_ID 0xDEADDEAD

typedef uint32_t streamId_t;

typedef struct streamPacketDesc_t
{
    uint8_t* data;
    uint32_t length;

}streamPacketDesc_t;


typedef struct USBLinkProf_t
{
    float totalReadTime;
    float totalWriteTime;
    unsigned long totalReadBytes;
    unsigned long totalWriteBytes;
    unsigned long totalBootCount;
    float totalBootTime;

}USBLinkProf_t;

typedef struct USBLinkHandler_t
{
    char* bootUtility;
    char* devicePath;
    char* devicePath2;

    int profEnable;
    USBLinkProf_t profilingData;
}USBLinkHandler_t;

#ifdef __cplusplus
}
#endif

#endif

/* end of include file */
