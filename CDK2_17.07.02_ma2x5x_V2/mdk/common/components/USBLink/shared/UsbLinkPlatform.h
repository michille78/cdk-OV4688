///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Application configuration Leon header
///
#ifndef _USBLINKPLATFORM_H
#define _USBLINKPLATFORM_H
#include <stdint.h>
#ifdef __cplusplus
extern "C"
{
#endif

#define MAX_POOLS_ALLOC 8
#define PACKET_LENGTH (64*1024)

int USBLinkWrite(void* data, int size);
int USBLinkRead(void* data, int size);
int UsbLinkPlatformInit(char* devPathRead, char* devPathWrite);
void* allocateData(uint32_t size, uint32_t alignment);
int UsbLinkPlatformBootRemote(char* devPathRead, char* devPathWrite);
int USBLinkPlatformResetRemote();




#ifdef __cplusplus
}
#endif

#endif

/* end of include file */
