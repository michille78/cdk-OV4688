///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Application configuration Leon header
///
#ifndef _USBLINKCOMMONDEFINES_H
#define _USBLINKCOMMONDEFINES_H

#ifdef _USBLINK_ENABLE_PRIVATE_INCLUDE_

#include <stdint.h>
#include <semaphore.h>
#include <swcFifo.h>
#include <UsbLinkPublicDefines.h>

#ifdef __cplusplus
extern "C"
{
#endif
#define MAX_NAME_LENGTH 16
#ifdef USE_USB_VSC
#define HEADER_SIZE (64-12 -8)
#else
#define HEADER_SIZE (64-12 -8)
#endif
#define MAXIMUM_SEMAPHORES 8
#define CACHE_LINE_SIZE 64

#define ASSERT_USB_LINK(x)   if(!(x)) { abort(); }


typedef int32_t eventId_t;

typedef enum {
    USB_LINK_NOT_INIT,
    USB_LINK_UP,
    USB_LINK_DOWN,
}usbLinkState_t;

typedef struct{
    char name[MAX_NAME_LENGTH];
    streamId_t id;
    uint32_t writeSize;
    uint32_t readSize;//no need of read buffer. It's on remote, will read it directly to the requested buffer
    streamPacketDesc_t packets[USB_LINK_MAX_PACKETS_PER_STREAM];
    uint32_t availablePackets;
    uint32_t blockedPackets;

    uint32_t firstPacket;
    uint32_t firstPacketUnused;
    uint32_t firstPacketFree;

    swcFifo_t fifo;
    sem_t sem;
}streamDesc_t;

//events which are coming from remote
typedef enum
{
    USB_WRITE_RTS,
    USB_WRITE_CTS,
    USB_READ_REQ,
    USB_READ_REL_REQ,
    USB_CREATE_STREAM_REQ,
    USB_PING_REQ,
    USB_RESET_REQ,
    USB_REQUEST_LAST,

    //note that is important to separate request and response
    USB_WRITE_DATA_ACK, // rts is served when we finished writing the data
    USB_WRITE_DATA, // cts is served when we sent the data
    USB_READ_RESP,
    USB_READ_REL_RESP,
    USB_CREATE_STREAM_RESP,
    USB_PING_RESP,
    USB_RESET_RESP,

    USB_RESP_LAST,


}usbLinkEventType_t;
typedef enum
{
    EVENT_LOCAL,
    EVENT_REMOTE,
}usbLinkEventOrigin_t;


#define MAX_EVENTS 64
//static int eventCount;
//static dispEventType_t eventQueue[MAX_EVENTS];


typedef struct usbLinkEventHeader_t{
    eventId_t           id;
    usbLinkEventType_t  type;
    char                streamName[MAX_NAME_LENGTH];
    streamId_t          streamId;
    uint32_t            size;
    union{
        uint32_t raw;
        struct{
            uint32_t ack : 1;
            uint32_t nack : 1;
            uint32_t terminate : 1;
            uint32_t bufferFull : 1;
            uint32_t sizeTooBig : 1;
            uint32_t noSuchStream : 1;
        }bitField;
    }flags;
}usbLinkEventHeader_t;

typedef struct usbLinkEvent_t {
    usbLinkEventHeader_t header;
    void* data;
}usbLinkEvent_t;


#ifdef __cplusplus
}
#endif
#endif
#endif

/* end of include file */
