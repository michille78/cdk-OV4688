///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Application configuration Leon header
///

#include "UsbLinkPlatform.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <sys/timeb.h>
#include <sys/un.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <termios.h>
#ifdef USE_LINK_JTAG
#include <sys/types.h>          /* See NOTES */
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#endif
#define USB_LINK_SOCKET_PORT 5678

#ifdef USE_USB_VSC
#include "fastmemDevice.hpp"
#define USB_ENDPOINT_IN     (0x81)   /* endpoint address */
#define USB_ENDPOINT_OUT (0x01)   /* endpoint address */
#define USB_TIMEOUT         10000        /* Connection timeout (in ms) */
#endif

#define USBLINK_ERROR_PRINT
#ifdef USBLINK_ERROR_PRINT
#define USBLINK_ERROR(...) printf(__VA_ARGS__)
#else
#define USBLINK_ERROR(...) (void)0
#endif

#ifdef USBLINKDEBUG
#define USBLINK_PRINT(...) printf(__VA_ARGS__)
#else
#define USBLINK_PRINT(...) (void)0
#endif


#ifndef USE_USB_VSC
int usbFdWrite = -1;
int usbFdRead = -1;
#else
fastmemDevice* dev;
#endif



void* poolsAllocated[MAX_POOLS_ALLOC];
uint32_t numPoolsAllocated = 0;
void freeAllData();

 int USBLinkWrite(void* data, int size)
{
    int byteCount = 0;
#ifndef USE_USB_VSC
#ifdef USE_LINK_JTAG
    while (byteCount < size){
        byteCount += write(usbFdWrite, &((char*)data)[byteCount], size - byteCount);
        printf("write %d %d\n", byteCount, size);
    }
#else
    if(usbFdWrite < 0)
    {
        return -1;
    }
    while(byteCount < size)
    {
       int toWrite = (PACKET_LENGTH && (size - byteCount > PACKET_LENGTH))? PACKET_LENGTH:size - byteCount;
       int wc = write(usbFdWrite, ((char*)data) + byteCount, toWrite);
//       printf("wwrite %x %d %x %d\n", wc, handler->commFd, ((char*)data) + byteCount, toWrite);

       if ( wc != toWrite)
       {
           return -2;
       }

       byteCount += toWrite;
       unsigned char acknowledge;
       int rc;
       rc = read(usbFdWrite, &acknowledge, sizeof(acknowledge));
       if ( rc < 0)
       {
           return -2;
       }
       if (acknowledge == 0xEF)
       {
//         printf("read %x\n", acknowledge);
       }
       else
       {
//         printf("read err %x %d\n", acknowledge, rc);
           return -2;
       }
    }
#endif
#else
    while (byteCount < size){
        byteCount += fastmemWrite(dev, USB_ENDPOINT_OUT, &((char*)data)[byteCount],
                                  size - byteCount, USB_TIMEOUT);
    }
#endif
    return 0;
}

 int USBLinkRead(void* data, int size)
{
    int nread =  0;
#ifndef USE_USB_VSC
#ifdef USE_LINK_JTAG
    while (nread < size){
        nread += read(usbFdWrite, &((char*)data)[nread], size - nread);
        printf("read %d %d\n", nread, size);
    }
#else
    if(usbFdRead < 0)
    {
        return -1;
    }

    while(nread < size)
    {

        int toRead = (PACKET_LENGTH && (size - nread > PACKET_LENGTH)) ? PACKET_LENGTH : size - nread;
        int rc = 0;

        while(toRead > 0)
        {

            rc = read(usbFdRead, &((char*)data)[nread], toRead);
//          printf("read %x %d\n", *(int*)data, nread);
            if ( rc < 0)
            {
                return -2;
            }
            toRead -=rc;
            nread += rc;
        }
        unsigned char acknowledge = 0xEF;
        int wc = write(usbFdRead, &acknowledge, sizeof(acknowledge));
        if (wc == sizeof(acknowledge))
        {
//          printf("write %x %d\n", acknowledge, wc);
        }
        else
        {
            return -2;
        }
    }
#endif
#else
    while (nread < size){
        nread += fastmemRead(dev, USB_ENDPOINT_IN, &((char*)data)[nread],
                                  size - nread, USB_TIMEOUT);
    }
#endif
    return 0;
}

int UsbLinkPlatformBootRemote(char* bootUtility, char* binaryPath)
{
#ifndef USE_USB_VSC

    if (usbFdRead != -1){
        close(usbFdRead);
        usbFdRead = -1;
    }
    if (usbFdWrite != -1){
        close(usbFdWrite);
        usbFdWrite = -1;
    }
#endif
    enum localExitModes{
        BOOT_SUCCESS = 0,
        UNKNOWN_BOOT_ERROR = 1, // This is the value returned by moviUsbBoot (1) Do not change
        CANT_CREATE_CHILD_PROCESS = 2,
        CANT_BOOT_MYRIAD = 3,
        CANT_EXIT_CHILD_PROCESS = 4
    };

    int status;
    int pid = fork();
    if (pid == 0){
        int err;
        char *env[1] = { 0 };
        char *argv[4] = { bootUtility,
                          binaryPath, (char*)"-q", 0 };
        err = execve(bootUtility, argv, env);
        (void)err;
        printf("Not able to start the boot binary. Configure correct path to moviUsbBoot\n");
        printf("Current path is set to: %s\n", bootUtility);
        exit(CANT_BOOT_MYRIAD); //if it got here, it's an error
    } else if(pid < 0) {
        exit(CANT_CREATE_CHILD_PROCESS);
    }
    wait(&status); //simplest one, man wait for others

    localExitModes err = BOOT_SUCCESS; //init to boot_success
    if (status)
    {
        //wasn't able to boot
        if(WIFEXITED(status)) {
            switch (WEXITSTATUS(status)) {
                case 0:
                    err = BOOT_SUCCESS;
                    break;
                case 1:
                    err = UNKNOWN_BOOT_ERROR;
                    break;
                case 2:
                    err = CANT_CREATE_CHILD_PROCESS;
                    break;
                case 3:
                    err = CANT_BOOT_MYRIAD;
                break;
                case 4:
                    err = CANT_EXIT_CHILD_PROCESS;
                break;
            }
        }
        else {
            err = CANT_EXIT_CHILD_PROCESS;
        }
    }
    // Workaround for situations when the Myriad is booted but still returns an error
    if(err == BOOT_SUCCESS || err == UNKNOWN_BOOT_ERROR) {
        return 0;
    }
    return 1;
}


int USBLinkPlatformResetRemote()
{
    freeAllData();
#ifndef USE_USB_VSC
#ifdef USE_LINK_JTAG

#else
    if (usbFdRead != -1){
        close(usbFdRead);
        usbFdRead = -1;
    }
    if (usbFdWrite != -1){
        close(usbFdWrite);
        usbFdWrite = -1;
    }
#endif
#else
    if(dev) {
        delete dev;
    }
#endif
    return -1;
}



#include <pthread.h>
#include <assert.h>

pthread_t readerThreadId;

int UsbLinkPlatformInit(char* devPathRead, char* devPathWrite)
{
#ifndef USE_USB_VSC
#ifdef USE_LINK_JTAG
    struct sockaddr_in serv_addr;
    usbFdWrite = socket(AF_INET, SOCK_STREAM, 0);
    usbFdRead = socket(AF_INET, SOCK_STREAM, 0);
    assert(usbFdWrite >=0);
    assert(usbFdRead >=0);
    memset(&serv_addr, '0', sizeof(serv_addr));

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = inet_addr("127.0.0.1");
    serv_addr.sin_port = htons(USB_LINK_SOCKET_PORT);
    if (connect(usbFdWrite, (struct sockaddr*)&serv_addr, sizeof(serv_addr)) < 0) {
        perror("ERROR connecting");
        exit(1);
     }
    printf("this is working\n");
    return 0;

#else
    usbFdRead= open(devPathRead, O_RDWR);
    if(usbFdRead < 0)
    {
        return -1;
    }
    // set tty to raw mode
    struct termios  tty;
    speed_t     spd;
    int rc;
    rc = tcgetattr(usbFdRead, &tty);
    if (rc < 0) {
        usbFdRead = -1;
        return -2;
    }

    spd = B115200;
    cfsetospeed(&tty, (speed_t)spd);
    cfsetispeed(&tty, (speed_t)spd);

    cfmakeraw(&tty);

    rc = tcsetattr(usbFdRead, TCSANOW, &tty);
    if (rc < 0) {
        usbFdRead = -1;
        return -2;
    }


    usbFdWrite= open(devPathWrite, O_RDWR);
    if(usbFdWrite < 0)
    {
        usbFdWrite = -1;
        return -2;
    }
    // set tty to raw mode
    rc = tcgetattr(usbFdWrite, &tty);
    if (rc < 0) {
        usbFdWrite = -1;
        return -2;
    }

    spd = B115200;
    cfsetospeed(&tty, (speed_t)spd);
    cfsetispeed(&tty, (speed_t)spd);

    cfmakeraw(&tty);

    rc = tcsetattr(usbFdWrite, TCSANOW, &tty);
    if (rc < 0) {
        usbFdWrite = -1;
        return -2;
    }
    return 0;
#endif
#else
    dev = new fastmemDevice("usb");
    int usbInit = dev->fastmemInit();
    return usbInit;
#endif
}

void* allocateData(uint32_t size, uint32_t alignment)
{
   void* ret = NULL;
   if(numPoolsAllocated >= MAX_POOLS_ALLOC) {
       return ret;
   }
   ret = malloc(size + alignment -1);
   poolsAllocated[numPoolsAllocated++] = ret;

   ret = (void*)((uint64_t)(ret + alignment - 1) & ~((uint64_t)alignment-1));
   return ret;
}

void freeAllData(){
    for(uint32_t i = 0; i<numPoolsAllocated; i++) {
        if(poolsAllocated[i] != 0){
            free(poolsAllocated[i]);
        }
    }
}
