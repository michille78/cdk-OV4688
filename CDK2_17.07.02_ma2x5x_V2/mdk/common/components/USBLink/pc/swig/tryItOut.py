import UsbLink
import numpy as np
handler = UsbLink.USBLinkHandler_t()
handler.bootUtility = "/home/zsolt/WORK/Tools/Releases/General/00.50.74.2/linux64/bin/moviUsbBoot"
handler.devicePath = "/dev/ttyACM0"
handler.commFd = UsbLink.USB_LINK_INVALID_FD
rc = UsbLink.BootMyriad(handler, "/home/zsolt/workspace/git/CV_Devrepo/mdk/projects/USBLink/myriad/ImageProcApp/output/ImageProcApp.mvcmd")
if rc==UsbLink.USB_LINK_SUCCESS:
  print("Success")

status = UsbLink.MYRIAD_NOT_INIT
myriadState = UsbLink.GetMyriadStatus(handler)
if rc==UsbLink.USB_LINK_SUCCESS:
  print(myriadState)
status = UsbLink.SetData(handler, "arrayName1", np.arange(10))
A = np.zeros(10)
status = UsbLink.SendHostReady(handler)
A = UsbLink.GetData(handler, "arrayName1", A, 0)

while myriadState !=  UsbLink.MYRIAD_FINISHED:
  myriadState = UsbLink.GetMyriadStatus(handler)
A = UsbLink.GetData(handler, "arrayName1", A, 0)
  