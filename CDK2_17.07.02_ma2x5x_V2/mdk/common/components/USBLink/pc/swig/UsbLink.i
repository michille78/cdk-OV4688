///
/// @file
/// @copyright All code copyright Movidius Ltd 2012, all rights reserved.
///            For License Warranty see: common/license.txt
///

%module UsbLink

%include "typemaps.i"

#include "UsbLink.h"
#include "stdio.h"

%{
    typedef int bool;

    #define SWIG_FILE_WITH_INIT
    #include "UsbLink.h"
%}

%include "numpy.i"

%init
%{
    import_array();
%}

#define USB_LINK_INVALID_FD  (-314)

typedef enum{
    USB_LINK_SUCCESS = 0,
    USB_LINK_ALREADY_OPEN,
    USB_LINK_COMMUNICATION_NOT_OPEN,
    USB_LINK_COMMUNICATION_FAIL,
    USB_LINK_COMMUNICATION_UNKNOWN_ERROR,
}USBLinkError_t;

typedef struct USBLinkHandler_t
{
    char* bootUtility;
    char* devicePath;
    char* devicePath2;

    int profEnable;
    USBLinkProf_t profilingData;
}USBLinkHandler_t;

%apply (int DIM1, unsigned char* IN_ARRAY1) {(int sz, unsigned char* arr)};
%apply (int DIM1, unsigned char* INPLACE_ARRAY1)  {(int sz, unsigned char* arr_out)};

%apply (int DIM1, unsigned int* IN_ARRAY1) {(int sz, unsigned int* arr)};
%apply (int DIM1, unsigned int* INPLACE_ARRAY1)  {(int sz, unsigned int* arr_out)};

%apply (int DIM1, unsigned short* IN_ARRAY1) {(int sz, unsigned short* arr)};
%apply (int DIM1, unsigned short* INPLACE_ARRAY1)  {(int sz, unsigned short* arr_out)};

typedef unsigned int streamId_t;

USBLinkError_t ResetMyriad(USBLinkHandler_t* handler);
USBLinkError_t BootMyriad(char* bootUtility, char* binaryPath);
USBLinkError_t InitMyriad(USBLinkHandler_t* handler);
streamId_t OpenStream(char* name, int stream_write_size);
USBLinkError_t SetData(streamId_t streamId, int sz, unsigned char* arr);
USBLinkError_t SetData2Byte(streamId_t streamId, int sz, unsigned short* arr);
USBLinkError_t SetData4Byte(streamId_t streamId, int sz, unsigned int* arr);
USBLinkError_t GetDataPacket(streamId_t streamId, streamPacketDesc_t** packet);
USBLinkError_t GetData(streamId_t streamId, int sz, unsigned char* arr);
USBLinkError_t GetData2Byte(streamId_t streamId, int sz, unsigned short* arr);
USBLinkError_t GetData4Byte(streamId_t streamId, int sz, unsigned int* arr);


%inline
%{

USBLinkError_t ResetMyriad(){
	return UsbLinkResetRemote();
}
USBLinkError_t BootMyriad(char* bootUtility, char* binaryPath){
	return UsbLinkBootRemote(bootUtility, binaryPath);
}
USBLinkError_t InitMyriad(USBLinkHandler_t* handler){
	return UsbLinkInitialize(handler);
}
streamId_t OpenStream(char* name, int stream_write_size){
	return UsbLinkOpenStream(name, stream_write_size);
}
USBLinkError_t SetData(streamId_t streamId, int sz, unsigned char* arr)
{
  	USBLinkError_t err = UsbLinkWriteData(streamId, (void*)arr, sz);
  	return err;
}
USBLinkError_t SetData2Byte(streamId_t streamId, int sz, unsigned short* arr)
{
	return UsbLinkWriteData(streamId, (void*)arr, sz*sizeof(unsigned short));
}
USBLinkError_t SetData4Byte(streamId_t streamId, int sz, unsigned int* arr)
{
	return UsbLinkWriteData(streamId, (void*)arr, sz*sizeof(unsigned int));
}
USBLinkError_t GetDataPacket(streamId_t streamId, streamPacketDesc_t** packet)
{
	USBLinkError_t a = UsbLinkReadData(streamId, packet);
  	return a;
}

USBLinkError_t GetDataNoRelease(streamId_t streamId, int sz, unsigned char* arr)
{
  int processed = 0;
  streamPacketDesc_t *pc_dsc;
  while (sz > processed){
    GetDataPacket(streamId, &pc_dsc);
    memcpy(arr+processed, (pc_dsc)->data, (pc_dsc)->length);
    processed += (pc_dsc)->length;
  }
  return 0;
}

USBLinkError_t DataRelease(streamId_t streamId)
{
  USBLinkError_t a = UsbLinkReleaseData(streamId);
  return a;
}

USBLinkError_t GetData(streamId_t streamId, int sz, unsigned char* arr)
{
  int processed = 0;
  streamPacketDesc_t *pc_dsc;
  while (sz > processed){
  	GetDataPacket(streamId, &pc_dsc);  // TODO: check for sucess
    memcpy(arr+processed, (pc_dsc)->data, (pc_dsc)->length);
    processed += (pc_dsc)->length;
  }
  return UsbLinkReleaseData(streamId);
}
USBLinkError_t GetData2Byte(streamId_t streamId, int sz, unsigned short* arr)
{
  int processed = 0;
  streamPacketDesc_t *pc_dsc;
  while (sz > processed){
  	GetDataPacket(streamId, &pc_dsc);
    memcpy(arr+processed, (pc_dsc)->data, (pc_dsc)->length);
    processed += (pc_dsc)->length;
  }
  return UsbLinkReleaseData(streamId);
}
USBLinkError_t GetData4Byte(streamId_t streamId, int sz, unsigned int* arr)
{
  int processed = 0;
  streamPacketDesc_t *pc_dsc;
  while (sz > processed){
  	GetDataPacket(streamId, &pc_dsc);
    memcpy(arr+processed, (pc_dsc)->data, (pc_dsc)->length);
    processed += (pc_dsc)->length;
  }
  return UsbLinkReleaseData(streamId);
}
%}

%pythoncode
%{
import numpy as np

def SetData(streamID, npy_arr):
    npy_arr = npy_arr.flatten()
    if npy_arr.dtype in [np.uint32, np.int32]:
        npy_arr = npy_arr.astype(dtype = np.uint32)
        return _UsbLink.SetData4Byte(streamID, npy_arr)
    elif npy_arr.dtype in [np.float16]:
        bytes = npy_arr.tobytes()
        dt = np.dtype(np.ushort)
        dt = dt.newbyteorder('<')
        a = np.frombuffer(bytes, dtype=dt)
        return _UsbLink.SetData2Byte(streamID, a)
    elif npy_arr.dtype in [np.ushort]:
        npy_arr = npy_arr.astype(dtype = np.ushort)
        return _UsbLink.SetData2Byte(streamID, npy_arr)
    else:
        npy_arr = npy_arr.astype(dtype = np.uint8)
        a = _UsbLink.SetData(streamID, npy_arr)
        return a

def GetData(streamID, npy_arr, no_release=False):

  if no_release:  # Temp Hack.
      npy_arr = npy_arr.flatten().astype(dtype = np.uint8)
      _UsbLink.GetDataNoRelease(streamID, npy_arr)
      return npy_arr

  if npy_arr.dtype in [ np.uint32, np.int32 ]:
      org_type = npy_arr.dtype
      npy_arr = npy_arr.flatten().astype(dtype = np.uint32)
      _UsbLink.GetData4Byte(streamID, npy_arr)
      npy_arr = npy_arr.astype(org_type)
  elif npy_arr.dtype in [ np.float32 ]:
      org_type = npy_arr.dtype
      npy_arr = npy_arr.flatten().astype(dtype = np.uint32)
      _UsbLink.GetData4Byte(streamID, npy_arr)
      bytes = npy_arr.tobytes()
      dt = np.dtype(np.float32)
      dt = dt.newbyteorder('<')
      npy_arr = np.frombuffer(bytes, dtype=dt)
  elif npy_arr.dtype in [ np.ushort ]:
      org_type = npy_arr.dtype
      npy_arr = npy_arr.flatten().astype(dtype = np.ushort)
      _UsbLink.GetData2Byte(streamID, npy_arr)
      npy_arr = npy_arr.astype(dtype = org_type)
  elif npy_arr.dtype in [ np.float16 ]:
      org_type = npy_arr.dtype
      npy_arr = npy_arr.flatten().astype(dtype = np.uint16)
      _UsbLink.GetData2Byte(streamID, npy_arr)
      bytes = npy_arr.tobytes()
      dt = np.dtype(np.float16)
      dt = dt.newbyteorder('<')
      npy_arr = np.frombuffer(bytes, dtype=dt)
  else:
      npy_arr = npy_arr.flatten().astype(dtype = np.uint8)
      _UsbLink.GetData(streamID, npy_arr)
  return npy_arr
%}
