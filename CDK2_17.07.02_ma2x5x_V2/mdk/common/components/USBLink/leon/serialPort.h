
///
/// @file
/// @copyright All code copyright Movidius Ltd 2014, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Video demo application context.
///

#ifndef _SERIALPORT_H_
#define _SERIALPORT_H_
#ifndef USE_USB_VSC
#ifndef USE_LINK_JTAG

// 1: Includes
// ----------------------------------------------------------------------------
#include "usbpump_usbseri.h"
#include "rtems.h"

// 2:  Source Specific #defines and types  (typedef,enum,struct)
// ----------------------------------------------------------------------------
//#define MAX_RX_SIZE 512
//#define MAX_TX_SIZE 512
#define SERIALPORT_INSTANCE_NO 1
#define SERIALPORT_EVENT RTEMS_EVENT_5

typedef struct {
    rtems_id taskId;
} RtemsEvent_t;
__TMS_TYPE_DEF_STRUCT(SERIALPORT_CONTEXT);

// VideoDemo Application Context
typedef struct
{
    __TMS_UPLATFORM *pPlatform;

    void *portHandle;
    uint32_t portInstanceNo;
    uint8_t portIndex;
} SerialPortContext;

SerialPortContext *SerialPortInit(UPLATFORM *pPlatform, rtems_device_minor_number minor);

#endif
#endif
#endif

