///
/// @file
/// @copyright All code copyright Movidius Ltd 2012, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     shave Fifo communication init functions
///

// 1: Includes
// ----------------------------------------------------------------------------

#include <stdlib.h>
#include <stdio.h>
#include "fifoCommInitApi.h"

// 2:  Source Specific #defines and types  (typedef, enum, struct)
// ----------------------------------------------------------------------------

// 3: Global Data (Only if absolutely necessary)
// ----------------------------------------------------------------------------
// 4: Static Local Data
// ----------------------------------------------------------------------------
// 5: Static Function Prototypes
// ----------------------------------------------------------------------------
// 6: Functions Implementation
// ----------------------------------------------------------------------------

void fifoCommMasterInit(fifoCommMasterHandler_t* handler, fifoCommMasterCallback_t taskHandler)
{
    handler->masterHandler = taskHandler;
    handler->taskTypeList = NULL;
}
void fifoCommSlaveInit(fifoCommSlaveHandler_t* handler)
{
    handler->taskTypeList = NULL;
    handler->stopRequest = 0;
}


void fifoCommMasterRegisterTaskType(fifoCommMasterHandler_t* masterHandler, fifoCommTask_t* taskType,
                                    u32 taskFifoNr, u32 responseFifoNr, u32 mutexNr)
{
    taskType->taskFifoNr = taskFifoNr;
    taskType->responseFifoNr = responseFifoNr;
    taskType->mutexNr = mutexNr;
    taskType->next = NULL;
    taskType->initHandler = NULL;
    taskType->taskHandler = NULL;
    if (masterHandler->taskTypeList == NULL)
    {
        masterHandler->taskTypeList = taskType;
    }
    else
    {
        taskType->next = masterHandler->taskTypeList;
        masterHandler->taskTypeList = taskType;
    }
}

void fifoCommSlaveRegisterTaskType(fifoCommSlaveHandler_t* slaveHandler, fifoCommTask_t* taskType,
                                   fifoCommTask_t* masterTask, fifoCommTaskCallback_t initHandler,
                                   fifoCommTaskCallback_t taskHandler)
{
    if (slaveHandler == NULL || taskType == NULL || masterTask ==NULL)
    {
        return;
    }
    taskType->taskFifoNr = masterTask->taskFifoNr;
    taskType->responseFifoNr = masterTask->responseFifoNr;
    taskType->mutexNr = masterTask->mutexNr;
    taskType->next = NULL;
    taskType->initHandler = initHandler;
    taskType->taskHandler = taskHandler;
    if (slaveHandler->taskTypeList == NULL)
    {
        slaveHandler->taskTypeList = taskType;
    }
    else
    {
        slaveHandler->taskTypeList->next = taskType;
    }
}

