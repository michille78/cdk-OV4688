///
/// @file
/// @copyright All code copyright Movidius Ltd 2012, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     shave Fifo communication init functions
///

#ifndef FIFOCOMMINITAPI_H_
#define FIFOCOMMINITAPI_H_

// 1: Includes
// ----------------------------------------------------------------------------
#include "fifoCommApiDefines.h"
// 2:  Source Specific #defines and types  (typedef, enum, struct)
// ----------------------------------------------------------------------------
// 3: Static Local Data
// ----------------------------------------------------------------------------

// 4:  Exported Functions (non-inline)
// ----------------------------------------------------------------------------
void fifoCommMasterInit(fifoCommMasterHandler_t* handler, fifoCommMasterCallback_t taskHandler);
void fifoCommSlaveInit(fifoCommSlaveHandler_t* handler);
void fifoCommMasterRegisterTaskType(fifoCommMasterHandler_t* masterHandler, fifoCommTask_t* taskType,
                                    u32 taskFifoNr, u32 responseFifoNr, u32 mutexNr);
void fifoCommSlaveRegisterTaskType(fifoCommSlaveHandler_t* slaveHandler, fifoCommTask_t* taskType,
                                   fifoCommTask_t* masterTask, fifoCommTaskCallback_t initHandler,
                                   fifoCommTaskCallback_t taskHandler);
#endif
