///
/// @file
/// @copyright All code copyright Movidius Ltd 2012, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     shave Fifo communication function implementations
///

// 1: Includes
// ----------------------------------------------------------------------------
#include <stdio.h>
#include <string.h>
#include <svuCommonShave.h>
#include <swcWhoAmI.h>

#include "fifoCommApi.h"
#include "fifoCommApiDefines.h"

// 2:  Source Specific #defines and types  (typedef,enum,struct)
// ----------------------------------------------------------------------------
// 3: Global Data (Only if absolutely necessary)
// ----------------------------------------------------------------------------
// 4: Static Local Data
// ----------------------------------------------------------------------------
// 5: Static Function Prototypes
// ----------------------------------------------------------------------------
// 6: Functions Implementation
// ----------------------------------------------------------------------------

void fifoCommMasterAddTask(fifoCommTask_t* taskType, void* taskParameters)
{
    scFifoWrite(taskType->taskFifoNr, (u32)taskParameters);
}

void* fifoCommMasterWaitTask(fifoCommTask_t* taskType)
{
    while(scFifoIsEmpty(taskType->responseFifoNr))
    {
        asm volatile("nop");
    }
    return (void*) scFifoReadShave(taskType->responseFifoNr);
}

u32 fifoCommSlaveReadTask(fifoCommTask_t* taskType, u32* result)
{
    u32 retVal = 0;
    scMutexRequest(taskType->mutexNr);
    if (!scFifoIsEmpty(taskType->taskFifoNr))
    {
        *result = scFifoReadShave(taskType->taskFifoNr);
        retVal = 1;
    }
    scMutexRelease(taskType->mutexNr);
    return retVal;
}

void fifoCommSlaveNotifyTaskCompletion(fifoCommTask_t* taskType, void* taskParameters)
{
    scFifoWrite(taskType->responseFifoNr, (u32)taskParameters);

}

void fifoCommMasterRun(fifoCommMasterHandler_t* handler, void* params)
{
    handler->masterHandler(handler, params);
    SHAVE_HALT;
}

void fifoCommSlaveRun(fifoCommSlaveHandler_t* handler)
{
    fifoCommTask_t* task = handler->taskTypeList;
    handler->stopRequest = 0;
    while (task != NULL)
    {
        if(task->initHandler != NULL)
        {
            task->initHandler(NULL);
        }
        task = task->next;
    }
    while(1)
    {

        task = handler->taskTypeList;
        while (task != NULL)
        {
            u32 taskParams;
            if (fifoCommSlaveReadTask(task,&taskParams))
            {
                task->taskHandler((void*)taskParams);
                fifoCommSlaveNotifyTaskCompletion(task, (void*)taskParams);
            }
            task = task->next;
        }
        if (handler->stopRequest)
        {
            break;
        }
    }
    SHAVE_HALT;
}

