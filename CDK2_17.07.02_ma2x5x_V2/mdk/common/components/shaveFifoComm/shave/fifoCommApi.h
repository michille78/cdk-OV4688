///
/// @file
/// @copyright All code copyright Movidius Ltd 2012, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Application configuration Leon header
///

#ifndef _FIFO_COMM_API_H_
#define _FIFO_COMM_API_H_

// 1: Includes
// ----------------------------------------------------------------------------
#include "fifoCommApiDefines.h"
// 2:  Source Specific #defines and types  (typedef, enum, struct)
// ----------------------------------------------------------------------------

// 3: Static Local Data
// ----------------------------------------------------------------------------

// 4:  Exported Functions (non-inline)
// ----------------------------------------------------------------------------
#ifdef __cplusplus
extern "C"
#endif
void fifoCommMasterAddTask(fifoCommTask_t* taskType, void* taskParameters);
#ifdef __cplusplus
extern "C"
#endif
void* fifoCommMasterWaitTask(fifoCommTask_t* taskType);
#ifdef __cplusplus
extern "C"
#endif
u32 fifoCommSlaveReadTask(fifoCommTask_t* taskType, u32* result);
#ifdef __cplusplus
extern "C"
#endif
void fifoCommSlaveNotifyTaskCompletion(fifoCommTask_t* taskType, void* taskParameters);
#ifdef __cplusplus
extern "C"
#endif
void fifoCommMasterRun(fifoCommMasterHandler_t* handler, void* params);
#ifdef __cplusplus
extern "C"
#endif
void fifoCommSlaveRun(fifoCommSlaveHandler_t* handler);

#endif
