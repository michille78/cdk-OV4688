#ifndef USB_VSC_H
#define USB_VSC_H

#include "usbpump_application_rtems_api.h"
#include <stddef.h>

# define USBPUMP_MDK_CACHE_ENABLE 1
#define ALIGNED(x) __attribute__((aligned(x)))

#ifdef __cplusplus
extern "C"
{
#endif
extern int VSCRead(void*, size_t);
extern int VSCWrite(void*, size_t);
extern void* usb_vsc_init(void);

#ifdef __cplusplus
}
#endif

#endif
