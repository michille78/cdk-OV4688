
#include "usb_vsc.h"

static unsigned char sg_DataPump_MemoryPool[4 * 1024 * 1024] ALIGNED(128) __attribute__((section(".ddr_direct.bss")));

static USBPUMP_APPLICATION_RTEMS_CONFIGURATION sg_DataPump_AppConfig =
    USBPUMP_APPLICATION_RTEMS_CONFIGURATION_INIT_V1(
        /* nEventQueue */       64,
        /* pMemoryPool */       sg_DataPump_MemoryPool,
        /* nMemoryPool */       sizeof(sg_DataPump_MemoryPool),
        /* DataPumpTaskPriority */  100,
        /* DebugTaskPriority */     200,
        /* UsbInterruptPriority */  10,
        /* pDeviceSerialNumber */   NULL,
        /* pUseBusPowerFn */        NULL,
        /* fCacheEnabled */     USBPUMP_MDK_CACHE_ENABLE,
        /* DebugMask */         3
    );

void* usb_vsc_init()
{
    return UsbPump_Rtems_DataPump_Startup(&sg_DataPump_AppConfig);
}
