#ifndef __PROF_MEMORY_H__
#define __PROF_MEMORY_H__

// TODO: replace with ProfileBufferEntry_t
//typedef struct ProfileEntry {
//    int headBufNumber;
//    struct ProfileEntry* next;
//    char name[25];
//} ProfileEntry;

// TODO: every master record will hold a pointer to a ProfileBufferEntry_t
//typedef struct MasterRecord {
//    ProfileEntry* firstEntry;
//} MasterRecord;


// TODO: #include <whatever.h>
// TODO: define those outside
typedef struct ProfileEntry_t {
    int i;
    char c;
    float f;
    char str[10];
} ProfileEntry_t;
typedef struct __attribute__((packed)) ProfileBufferEntry_t {
    const void* const end;
    struct ProfileEntry_t* /*restrict*/ head;
    struct ProfileBufferEntry_t* next;
} ProfileBufferEntry_t;
#define NB_ELEMENTS (5)
#define NB_BUFFERS (6)

void lock();
void unlock();
void disableProfiler();
int profilerEnabled();
//////////////////////////////


void init(ProfileBufferEntry_t* entry, const char* name);

//void write(ProfileEntry* pEntry, ProfileEntry_t * data);


#endif
