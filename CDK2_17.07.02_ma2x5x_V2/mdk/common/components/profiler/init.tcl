puts "Profiling buffers will be collected at exit"
namespace path [concat [namespace path] ::mdbg]

breset
displaystate verbose
cli::InternalErrorInfo

# collect the buffers and interpret them after the program finishes
dll::atexit {
    if {[catch {

	if { ${timeout} != -1 } {
		# minesweeper
		after cancel ${bomb}
	}
	puts "Collecting buffers ..."
	# stop everything to read consistent data
	catch { halt -a }
    set elf "[pwd]/$elf"
    cd [file dirname ${elf}]
	if { ! [info exists SAMPLE_PERIOD_MICRO] } {
		set SAMPLE_PERIOD_MICRO "<undefined>"
	}

	set profArgs [string map {; ;\n} $profArgs]
	append profArgs "platform = \"[getPlatform]\";\n"

    decodeBuffers ${elf} ${moviProf} ${profArgs} ${SAMPLE_PERIOD_MICRO}
    samplePrint ${elf}
    }]} {
        puts stderr $::errorInfo
    }
}

# When hit, download buffer and continue execution. in parralel or not
# FIXME: figure out a solution to inhibit runw termination when this particular
# breakpoint is hit.
#bp add collectProfileData { code to run when this is hit }
#collect function is present for each core...

proc samplePrint {elf} {
    catch {
    foreach { gpfile } [glob -type f *.gprof] {
        puts "=================== ${gpfile} =================="
        # TODO: diff shall be done using gprof -u
        catch {
            # TODO: add back the --inline-file-names when even Ubuntu will support it
            eval% exec gprof -bpQ ${elf} ${gpfile}
        }

    }}
}

proc decodeBuffers { elf moviProf profArgs samplePeriod } {
    # search for all master resords in elf
    set pipe [list "nm $elf"]
    lappend pipe {grep __profileBufferMasterRecord__}
    lappend pipe {awk '{print $3}'}
    set masterRecords [exec sh -c [join $pipe " | "]]
    set sampleFiles ""
    foreach { masterRecord } $masterRecords {
        puts "\n===========================\nFound master record ${masterRecord} at address [sym addr ${masterRecord}]"
        set magic [mget ((ProfileBufferMasterRecord_t)${masterRecord}).magic -value]
        if { $magic != 0x666f7270 } {
            puts "Invalid magic number on master record: $magic"
            continue
        }
        set version [mget ((ProfileBufferMasterRecord_t)${masterRecord}).version -value]
        set coreId [mget ((ProfileBufferMasterRecord_t)${masterRecord}).coreId -value]
        set startTime [mget ((ProfileBufferMasterRecord_t)${masterRecord}).startTime -value]
	    puts "startTime = $startTime"
        if { $coreId == 0xFF } {
            puts "No data available. Skipping..."
            continue
        }
        set myriadChipVersion [mget *((ProfileBufferMasterRecord_t)${masterRecord}).myriadChipVersion -value -binary -size 6 -scan a* ]
        set fProf [mget ((ProfileBufferMasterRecord_t)${masterRecord}).fProf -value]
        set tProf [mget ((ProfileBufferMasterRecord_t)${masterRecord}).tProf -value]
        set sProf [mget ((ProfileBufferMasterRecord_t)${masterRecord}).sProf -value]
        foreach { pType pprof } "trace $tProf function $fProf sample $sProf" {
            if { !$pprof } {
                puts "The $pType profiling type is unused"
                continue
            }
            set head [mget ((ProfileBufferEntry_t*)${pprof})->head -value -type pointer]
            set bufferEnd [mget ((ProfileBufferEntry_t*)${pprof})->end -value -type pointer]
            set startBuffer [mget ((ProfileBufferEntry_t*)${pprof})->buffer -value -type pointer]

            set bufferSize [expr $bufferEnd - $startBuffer]

            puts "${pType} profile buffer v.[format %d $version] CoreID: $coreId ($myriadChipVersion)\n\
                \[${startBuffer} .. *${head} .. [format 0x%X [expr $startBuffer + $bufferSize]]\] (size: $bufferSize)"
            if { ${head} < ${startBuffer} || ${startBuffer} + ${bufferSize} <= ${head} } {
                puts "ERROR: Buffer appears to be invalid. Skipping..."
                continue
            }

            close [file tempfile fileName "${pType}${masterRecord}${coreId}.bin"]
            # if we have data at head on a length of 4, then we have two chunks of data
            if { [lsearch -not -exact -integer [mget $head -value -count 4] 0] == -1 } {
                #is zero, then only one
                eval% savefile $fileName $startBuffer [expr {${head} - $startBuffer}]
            } else {
                puts "two chunks because we have data at write pointer: [mget $head -value -count 4]"
                # two chunks
                eval% savefile $fileName $head [expr {$bufferEnd - ${head}}]
                eval% savefile -append $fileName $startBuffer [expr {${head} - $startBuffer}]
            }
           if { $pType == "sample" } {
                # the core in this case is not important
                set sampleFiles [concat $sampleFiles $fileName]
            } {
                if { [catch { exec {*}${moviProf} --streamVersion=${version} --arg=${profArgs} -t${pType} ${fileName} -c${coreId} --verbose --startTime=${startTime} --elf=$elf } msg] } {
                    puts "${moviProf} -t${pType} ${fileName} -c${coreId} --verbose --startTime=${startTime} --elf=$elf"
                    puts "${msg}"
                    continue
                }
                puts "${msg}"
            }
        }
	}
    puts "Sample Decoding ..."
    if { $sampleFiles != "" && [catch { exec {*}${moviProf} --streamVersion=${version} --arg=${profArgs} -tsample --intervalSample=${samplePeriod} -c1 --verbose --elf=$elf {*}$sampleFiles } msg] } {
        puts $msg
    }

	# TODO: enhance profiler with cache hit/miss info
	#puts "get register SVU_PCCn - 04_02_Shave_DCU.odt"
	#puts "get reg L2C_MISS_CNT, L2C_HIT_CNT, L2C_LEON_HIT_CNTR, L2C_LEON_ACCESS_CNTR"

	puts "Done."
}

# stop the application by force after timeout ms
if { ${timeout} != -1 } {
	set bomb [after ${timeout} {
        puts "Stopping Myriad Application because timeout (${timeout} ms) expired!"
        mdbg::halt -all -async
    }]
}

