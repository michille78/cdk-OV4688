TRACE_BUFFER_SIZES ?= 1024
PROFILER_ARGUMENTS += traceProfileBufferSize = \"$(TRACE_BUFFER_SIZES)\";

TRACE_PROF_DEFINES += -DTRACE_PROFILER_ENABLED \
					  -DTRACE_BUFFER_SIZES=$(TRACE_BUFFER_SIZES)

ComponentList_LOS += profiler
ComponentList_LRT += profiler
#ComponentList_SVE += profiler
#SHAVE_COMPONENTS = yes

# enable trace profiling code, otherwise only stubs are in place
CCOPT_EXTRA += $(TRACE_PROF_DEFINES)
CCOPT_EXTRA_LRT += $(TRACE_PROF_DEFINES)
MVCCOPT += $(TRACE_PROF_DEFINES)

$(info Trace profiling activated)
