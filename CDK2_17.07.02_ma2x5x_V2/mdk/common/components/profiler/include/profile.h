#ifndef _PROFILE_H_
#define _PROFILE_H_

struct ProfileEntry_t;

#define MV_PROF_VERSION         (4u)

#ifdef __cplusplus
#include <cstdint>
extern "C" {
#else
#include <stdint.h>
#endif

#include <profilerAPI.h>


// TODO: drop buffer name configuration support. no one will ever need to provide it
// TODO:

typedef struct __attribute__((packed)) ProfileBufferEntry_t {
    struct ProfileEntry_t* buffer;
    void* end;
    struct ProfileEntry_t* /*restrict*/ head;
} ProfileBufferEntry_t;

// master record
typedef struct __attribute__((packed)) ProfileBufferMasterRecord_t {
    const uint32_t magic; // always 0x666f7270 (prof)
    const uint32_t version; // > 1
    ProfileBufferEntry_t* fProf;
    ProfileBufferEntry_t* sProf;
    ProfileBufferEntry_t* tProf;
    uint64_t startTime;
    uint8_t coreId; // not const because shave core is retrieved at runtime during init
    const char* myriadChipVersion;
    uint32_t overhead; // time spent in hooks
} ProfileBufferMasterRecord_t;

extern ProfileBufferMasterRecord_t __profileBufferMasterRecord__;

// when the debugger starts the application sets a breakpoint on this particular function. Whenever
// runw is halted due to this breakpoint, the buffer is collected and interpreted. The execution is resumed
// afterwards
void collectProfileData(struct ProfileBufferEntry_t* p);

#ifdef PROFILE_ENTRY_DEFINED

#define unlikely(x) __builtin_expect(!!(x), 0)

// TODO: in BUFFER_FULL allocate a new buffer; depending on the
// allocator type can be the same one reused.
// TODO: check if it is more conveninet to make this as a function
#define I_FOUND_HOW_TO_INHIBIT_RUNW_HALT_WHEN_MYBP_IS_HIT 0
#if I_FOUND_HOW_TO_INHIBIT_RUNW_HALT_WHEN_MYBP_IS_HIT
#define BUFFER_FULL(entry) collectProfileData(&entry)
#else
#define BUFFER_FULL(entry) (entry).head = (struct ProfileEntry_t*) (entry).buffer
#endif

#ifdef FP_32BIT_TIMESTAMP_OPT_MODE
#define UPDATE_START_TIME(entry) __profileBufferMasterRecord__.startTime= (uint64_t)(__profileBufferMasterRecord__.startTime + *(uint32_t*)(entry).head)
#else
#define UPDATE_START_TIME(entry)
#endif

#define profilePushEntry(entry, ...) \
    *(entry).head++ = (struct ProfileEntry_t) {__VA_ARGS__}; \
    if (unlikely((entry).head == (entry).end)) { \
        BUFFER_FULL(entry); \
        UPDATE_START_TIME(entry); \
    }


#endif


#ifdef __cplusplus
} // extern "C"
#endif

#endif
