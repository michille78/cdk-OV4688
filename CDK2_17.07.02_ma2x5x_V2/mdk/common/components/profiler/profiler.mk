ifdef PROFILE_INFO

ifeq ($(OS),Windows_NT)
PROFILE_DECODER =  $(shell cygpath -m $(realpath $(MV_TOOLS_BASE)/$(DETECTED_PLATFORM)/bin/moviProf.exe))
else
PROFILE_DECODER = $(shell realpath $(MV_TOOLS_BASE)/$(DETECTED_PLATFORM)/bin/moviProf)
endif

# Usage: $(call mkrule,list,var,value)
# for each file from list, var += value
# used to set compile flags per file
define PROFILER_CODE_RULE
$(1): $(2) += $(3)
endef
define mkrule
$(eval $(foreach f,$(1),$(eval $(call PROFILER_CODE_RULE,$(f),$(2),$(3)))))
endef

SELF_PATH := $(abspath $(dir $(lastword $(MAKEFILE_LIST))))
COMPONENT_PATH = $(MV_COMMON_BASE)/components/profiler
PROFILER_INIT_HOOK_SCRIPT ?= $(COMPONENT_PATH)/init.tcl
PROFILER_EXEC_TIME := $(shell date "+%Y.%m.%d-%H.%M.%S")
PROFILER_OUTPUT_DIR ?= $(DirAppOutput)/profile
PROFILER_DURATION_MS ?= 60000

MDBG_INIT_HOOK = --script $(PROFILER_INIT_HOOK_SCRIPT)

# User defined string. Freetext filled by the user
#PROFILER_SESSION_DESCRIPTION ?=

###############################################
## Function Profiling
###############################################
ifneq (,$(findstring function,$(PROFILE_INFO)))
include $(SELF_PATH)/functionProfiler.mk
endif # PROFILE == function

###############################################
## Sample Profiling
###############################################
ifneq (,$(findstring sample,$(PROFILE_INFO)))
include $(SELF_PATH)/sampleProfiler.mk
endif # PROFILE == sample

###############################################
## Trace Profiling
###############################################
ifneq (,$(findstring trace,$(PROFILE_INFO)))
include $(SELF_PATH)/traceProfiler.mk
endif

DirAppOutput := $(PROFILER_OUTPUT_DIR)

# list of arguments separated by ;
PROFILER_ARGUMENTS += profilerDescription = \"$(PROFILER_SESSION_DESCRIPTION)\";
GIT_AVAIL := $(shell command -v git 2>/dev/null 1>&2 && git -C . rev-parse 2>/dev/null && echo "yes" || echo "no")
ifeq ($(GIT_AVAIL),yes)
PROFILER_ARGUMENTS += gitVersion = \"$(shell git rev-parse --short HEAD) ($(shell git symbolic-ref --quiet HEAD))\";
endif
PROFILER_ARGUMENTS += profilerType = \"$(PROFILE_INFO)\";
PROFILER_ARGUMENTS += MV_SOC_REV = \"$(MV_SOC_REV)\";
PROFILER_ARGUMENTS += MV_TOOLS_BASE = \"$(MV_TOOLS_BASE)\";
PROFILER_ARGUMENTS += MV_BUILD_CONFIG = \"$(MV_BUILD_CONFIG)\";

MDBG_INIT_HOOK += -D:moviProf="$(PROFILE_DECODER) --lttng=$(DirAppOutput)/profile$(PROFILER_EXEC_TIME)" \
	-D:timeout=$(PROFILER_DURATION_MS) -D:profArgs="$(PROFILER_ARGUMENTS)"

run debug: MVDBG_SCRIPT_OPT += $(MDBG_INIT_HOOK)
debug: PROFILER_DURATION_MS=-1
profile: run

endif # PROFILE_INFO defined
