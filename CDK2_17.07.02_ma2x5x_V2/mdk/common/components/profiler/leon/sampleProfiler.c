#ifdef SAMPLE_PROFILING
///
/// @file
/// @copyright All code copyright Movidius Ltd 2013, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// Myriad Sampling profiler
///

// System Includes
/// ----------------------------------------------------------------------------
#include <string.h>
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
/// Application Includes
/// ----------------------------------------------------------------------------
#include "DrvTimer.h"
#include "DrvIcb.h"
#include "DrvRegUtilsDefines.h"
#include "DrvSvu.h"
#include "DrvSvuDefines.h"
#include <registersMyriad.h>
#include <swcLeonUtils.h>
#include "theDynContext.h"
#include "DrvDdr.h"
#include <swcWhoAmI.h>

#ifdef __RTEMS__
#include "OsDrvTimer.h"
#include "OsDrvCpr.h"
#include <rtems.h>
#endif

/// Source Specific #defines and types  (typedef,enum,struct)
/// ----------------------------------------------------------------------------

#ifndef SAMPLE_PROFILER_DATA_SECTION
#define SAMPLE_PROFILER_DATA_SECTION __attribute__((section(".ddr_direct.bss")))
#endif

#ifndef SAMPLE_PERIOD_MICRO
#define SAMPLE_PERIOD_MICRO 10
#endif
#ifndef SAMPLE_DURATION_MS
#define SAMPLE_DURATION_MS 2000
#endif

#ifndef SP_TIMER_INTERRUPT_LEVEL
#define SP_TIMER_INTERRUPT_LEVEL 13
#endif

#define MAX_SAMPLE_COUNT (SAMPLE_DURATION_MS / SAMPLE_PERIOD_MICRO * 1000)
#define SAMPLE_BUFFER_SIZE (MAX_SAMPLE_COUNT)

#ifdef __RTEMS__
#define RTEMS_EVENT_STOP_TIMER  RTEMS_EVENT_1
#define RTEMS_EVENT_START_TIMER RTEMS_EVENT_2

typedef enum
{
    SAMPLE_STATUS_UNITIALIZED = 0,
    SAMPLE_STATUS_INITIALIZED = 1
} SAMPLE_STATUS;
#endif

#ifdef SAMPLE_PROF_COMPUTE_WINDOW
static uint32_t ShaveToLeonAddress(uint32_t shaveNumber,uint32_t address);
#define SHV_ADDR(shv, addr) (ShaveToLeonAddress(shv, addr))
#else
#define SHV_ADDR(shv, addr) (addr)
#endif


//#define SAMPLE_DEBUG
#ifdef SAMPLE_DEBUG
#define DPRINTF1(...) printf(__VA_ARGS__)
#else
#define DPRINTF1(...)
#endif

/// Static Local Data
/// ----------------------------------------------------------------------------

volatile u32 spSampleCount = 0;
volatile u32 spSampleCount4___ = 0;
static uint8_t shave_val[TOTAL_NUM_SHAVES], shaveStartedCount = 0;
static irq_handler userCallback[TOTAL_NUM_SHAVES];
typedef u32 swcShaveUnit_t;
__attribute__((unused)) static int spTimerNo;

#ifdef __RTEMS__ //used only if exist rtems
static osDrvTimerHandler_t osSpTimerHandler;
static rtems_id taskSPTimer_id;
static rtems_id taskSPMain_id;
#endif

typedef struct  __attribute__ ((packed)) ProfileEntry_t {
	uint32_t ip;
	uint8_t shaveNumber;
} ProfileEntry_t;
#define PROFILE_ENTRY_DEFINED

SAMPLE_PROFILER_DATA_SECTION ProfileEntry_t newspBuffer___[SAMPLE_BUFFER_SIZE];
// IMPORTANT: ProfileEntry_t must be defined before profile.h is included
#include "profile.h"
static ProfileBufferEntry_t sProfile = {NULL, NULL, NULL};

/// Static Function Prototypes
/// ----------------------------------------------------------------------------
void swcStartShave_original(u32 shave_nr, u32 entry_point);
void swcDynStartShave_original(u32 shave_nr, u32 Context);
s32 swcRunShaveAlgo_original(DynamicContext_t *moduleStData, int * const shaveNumber);
s32 swcRunShaveAlgoCC_original(DynamicContext_t *moduleStData, int * const shaveNumber, const char *fmt, ...);
void swcStartShaveCC_original(u32 shave_num, u32 pc, const char *fmt, ...);
void swcStartShaveAsync_original(u32 shave_num, u32 pc, irq_handler function);
void swcShaveStartAsync_original(u32 shave_num, u32 pc, irq_handler function);
void swcStartShaveAsyncCC_original(u32 shave_num, u32 pc, irq_handler function, const char *fmt, ...);
int swcWaitShave_original(u32 shave_nr);
int swcWaitShaves_original(u32 no_of_shaves, swcShaveUnit_t *shave_list);
void swcAssignShaveCallback_original(u32 shave_num, irq_handler function);


/// Functions Implementation
/// ----------------------------------------------------------------------------
static inline void timerStopOperation()
{
#ifdef __RTEMS__
    rtems_status_code status;
    status = rtems_event_send(taskSPTimer_id,RTEMS_EVENT_STOP_TIMER);
    if(status != RTEMS_SUCCESSFUL)

    {
        DPRINTF1("Error: rtems event send, status = %u\n",status);
    }
#else
    s32 status;
    status = DrvTimerStopOperation(spTimerNo);
    if(status!= MYR_DRV_SUCCESS)
    {
        DPRINTF1("Error: DrvTimerStopOperation, status = %d\n",status);
    }
#endif
}

// TODO: common/components/Debug/SvuDebugMyriad2/leon/SvuDebugMyriad2.c regarding shave window
u32 sampleProfilerTimerCb(u32 timerNumber, u32 param2)
{
    /// TODO: this can be implemented in ASM to speed it up
    UNUSED(timerNumber);
    UNUSED(param2);
    u32 i;
    if ((spSampleCount + shaveStartedCount) <= MAX_SAMPLE_COUNT)
    {
        for(i = 0; i < shaveStartedCount; i++)
        {
            uint32_t ip = GET_REG_WORD_VAL(DCU_SVU_PTR(shave_val[i]));
            if (!ip) continue;
            ip = SHV_ADDR(shave_val[i], ip);
            profilePushEntry(sProfile, ip, shave_val[i]); // shave start from 0, but core id is +2
        }
    }
    else
    {
        timerStopOperation();
    }
    return 0;
}

#ifdef __RTEMS__
rtems_task SampleProfiler_task(rtems_task_argument unused)
{
    UNUSED(unused);
    static SAMPLE_STATUS sampleProfilerStatus = SAMPLE_STATUS_UNITIALIZED;
    rtems_event_set event;
    rtems_status_code rtemsStatusCode;
    s32 intStatus;

    //only for the first call, init cpr, timer and DDR
    if(sampleProfilerStatus == SAMPLE_STATUS_UNITIALIZED)
    {
        sampleProfilerStatus = SAMPLE_STATUS_INITIALIZED;
        intStatus = OsDrvCprInit();
        if (intStatus != OS_MYR_DRV_SUCCESS)
        {
            DPRINTF1("Error: CPR init, status = %d\n", intStatus);
        }

        intStatus = OsDrvCprOpen();
        if (intStatus != OS_MYR_DRV_SUCCESS)
        {
            DPRINTF1("Error: CPR open, status = %d\n", intStatus);
        }

        intStatus = OsDrvTimerInit();
        if (intStatus != OS_MYR_DRV_SUCCESS)
        {
            DPRINTF1("Error: timer init, status = %d\n", intStatus);
        }

        DrvDdrInitialise(NULL);
    }

    taskSPTimer_id = rtems_task_self();

    intStatus = OsDrvTimerOpen(&osSpTimerHandler, 20000, OS_MYR_PROTECTION_SEM);
    if (intStatus != OS_MYR_DRV_SUCCESS)
    {
        DPRINTF1("Error: timer open status %d\n", intStatus);
    }

    intStatus = OsDrvTimerStartTimer(&osSpTimerHandler, SAMPLE_PERIOD_MICRO,
                                  sampleProfilerTimerCb, REPEAT_FOREVER,
                                  SP_TIMER_INTERRUPT_LEVEL);

    if (intStatus != OS_MYR_DRV_SUCCESS)
    {
        DPRINTF1("Error: timer start status = %d\n", intStatus);
    }

    //send event to inform that timer just start
    rtemsStatusCode = rtems_event_send(taskSPMain_id, RTEMS_EVENT_START_TIMER);
    if(rtemsStatusCode != RTEMS_SUCCESSFUL)
    {
        DPRINTF1("Error: rtems event send, status = %u\n", rtemsStatusCode);
    }

    //wait for signal to stop timer
    rtemsStatusCode = rtems_event_receive(
                                 RTEMS_EVENT_STOP_TIMER,
                                 RTEMS_WAIT,
                                 RTEMS_NO_TIMEOUT,
                                 &event
                                 );

    if(rtemsStatusCode != RTEMS_SUCCESSFUL)
    {
        DPRINTF1("Error: event receive, status = %u\n", rtemsStatusCode);
    }

    intStatus = OsDrvTimerStopTimer(&osSpTimerHandler);
    if (intStatus != OS_MYR_DRV_SUCCESS)
    {
        DPRINTF1("Error: timer stop, status = %d\n", intStatus);
    }

    intStatus = OsDrvTimerCloseTimer(&osSpTimerHandler);
    if (intStatus != OS_MYR_DRV_SUCCESS)
    {
        DPRINTF1("Error: timer close, status = %d\n", intStatus);
    }

    rtemsStatusCode = rtems_task_delete(RTEMS_SELF);
    if(rtemsStatusCode != RTEMS_SUCCESSFUL)
    {
        DPRINTF1("Error: event receive, status = %u\n", rtemsStatusCode);
    }
}
#endif

static inline void timerStartOperation()
{
#ifdef __RTEMS__
    rtems_name task_name;
    rtems_status_code status;
    rtems_event_set event;

    // Create New Task
    task_name = rtems_build_name('S', 'A', 'M', 'P');

    // Task creation
    status = rtems_task_create(
                    task_name,
                    1, RTEMS_MINIMUM_STACK_SIZE * 2,
                    RTEMS_DEFAULT_MODES,
                    RTEMS_DEFAULT_ATTRIBUTES, &taskSPTimer_id);

    if(status != RTEMS_SUCCESSFUL)
    {
        DPRINTF1("Error: task create, status = %u\n",status);
    }

    taskSPMain_id = rtems_task_self();

    // Start Task
    status = rtems_task_start(taskSPTimer_id, SampleProfiler_task, 1);
    if(status != RTEMS_SUCCESSFUL)
    {
        DPRINTF1("Error: task start, status =  %u\n",status);
    }

    //wait for task to start timer
    status = rtems_event_receive(
                    RTEMS_EVENT_START_TIMER,
                    RTEMS_WAIT,
                    RTEMS_NO_TIMEOUT,
                    &event
    );

    if(status != RTEMS_SUCCESSFUL)
    {
        DPRINTF1("Error: event recevive, status =  %u\n",status);
    }
#else
    int status;
    status = DrvTimerStartOperation(SAMPLE_PERIOD_MICRO,
                           sampleProfilerTimerCb,
                           REPEAT_FOREVER,
                           SP_TIMER_INTERRUPT_LEVEL,
                           &spTimerNo);
    if (status != MYR_DRV_SUCCESS)
    {
        DPRINTF1("Error: DrvTimerStartOperation, status =  %u\n",status);
    }
#endif
}

static void removeShaveFromList(u32 shave_nr)
{
    u32 i;
    for(i = 0; i < shaveStartedCount; i++)
        if(shave_val[i] == shave_nr)
            break;
    if (i < shaveStartedCount)
    {
        for(; i + 1 < shaveStartedCount; i++)
            shave_val[i] = shave_val[i+1];
        if (shaveStartedCount > 0)
            shaveStartedCount--;
    }
    if(shaveStartedCount == 0)
    {
        timerStopOperation();
    }
}

static void addShaveToList(u32 shave_nr)
{
    int found = 0;
    u32 i;

    for (i = 0; i < shaveStartedCount; i++)
    {
        if (shave_val[i] == shave_nr)
        {
            found = 1;
            break;
        }
    }
    if (!found)
    {
        shave_val[shaveStartedCount] = shave_nr;
        shaveStartedCount++;
        if (swcLeonGetPIL() >= SP_TIMER_INTERRUPT_LEVEL)
        {
            swcLeonSetPIL(SP_TIMER_INTERRUPT_LEVEL - 1);
        }
        if (shaveStartedCount == 1)
            timerStartOperation();
    }
}

static void profilerCallback (u32 source)
{
    removeShaveFromList(source - IRQ_SVE_0);
//  printf("\nSample Profiler was enabled. Comment these printfs if you use shave runtime measurements."
//           "\nMeasurements taken: %lu\n\n", spSampleCount);
    spSampleCount4___ = sizeof(u32) * spSampleCount;
    userCallback[source - IRQ_SVE_0](source);
}

static void initSampleProfiler(u32 shave_nr)
{
    addShaveToList(shave_nr);
    /// TODO: check to don't start twice
    //printf("\nEnabling sampling profiler on shave %d \n", shave_nr);
    //printf("The shave instruction pointer will be checked in every %d microseconds \n",
    //SAMPLE_PERIOD_MICRO);
	if (!__profileBufferMasterRecord__.sProf) {
		sProfile.buffer = sProfile.head = newspBuffer___;
    sProfile.end = &sProfile.buffer[SAMPLE_BUFFER_SIZE];
		__profileBufferMasterRecord__.sProf = &sProfile;
		__profileBufferMasterRecord__.coreId = swcWhoAmI();
		__builtin_memset(newspBuffer___, 0, sizeof(newspBuffer___));
	}
}

void swcStartShave(u32 shave_nr, u32 entry_point)
{
    //printf("sp address %x spSampleCount4___ %u\n",spBuffer___,___spSampleCount4);
    initSampleProfiler(shave_nr);
    swcStartShave_original(shave_nr, entry_point);
}

void swcDynStartShave(u32 shave_nr, u32 Context)
{
    //printf("sp address %x spSampleCount4___ %u\n",spBuffer___,___spSampleCount4);
    initSampleProfiler(shave_nr);
    swcDynStartShave_original(shave_nr, Context);
}

s32 swcRunShaveAlgo(DynamicContext_t *moduleStData, int * const shave_nr)
{
    int status;
    //printf("sp address %x spSampleCount4___ %u\n",spBuffer___,___spSampleCount4);
    status=swcRunShaveAlgo_original(moduleStData, shave_nr);
    if (status != MYR_DYN_INFR_SUCCESS)
        MYR_DYN_INFR_CHECK_CODE(status);
    initSampleProfiler(*shave_nr);
    return status;
}

s32 swcRunShaveAlgoCC(DynamicContext_t *moduleStData, int * const shave_num, const char *fmt, ...)
{
    int status;
    UNUSED(moduleStData);
    u32 argListSize = 1 * sizeof(DynamicContext_t*)
                    + sizeof(int * const)
                    + strlen(fmt) * (sizeof(u32) + sizeof(char));
    void* argList = __builtin_apply_args();
    __builtin_apply((void (*)())swcRunShaveAlgoCC_original, argList, argListSize);
    __builtin_return((void*)&status);
    initSampleProfiler(*shave_num);
    return status;
}

void swcStartShaveCC(u32 shave_num, u32 pc, const char *fmt, ...)
{
    UNUSED(pc);
    initSampleProfiler(shave_num);
    u32 argListSize = 2 * sizeof(u32)
                    + strlen(fmt) * (sizeof(u32) + sizeof(char));
    void* argList = __builtin_apply_args();
    __builtin_apply((void (*)())swcStartShaveCC_original, argList, argListSize);
}

void swcAssignShaveCallback(u32 shave_nr, irq_handler function)
{
    userCallback[shave_nr] = function;
    swcAssignShaveCallback_original(shave_nr, profilerCallback);
}

void swcStartShaveAsync(u32 shave_nr, u32 entry_point, irq_handler function)
{
    userCallback[shave_nr] = function;
    initSampleProfiler(shave_nr);
    swcStartShaveAsync_original(shave_nr, entry_point, profilerCallback);
}

void swcShaveStartAsync(u32 shave_nr, u32 entry_point, irq_handler function)
{
    userCallback[shave_nr] = function;
    initSampleProfiler(shave_nr);
    swcShaveStartAsync_original(shave_nr, entry_point, profilerCallback);
}

void swcStartShaveAsyncCC(u32 shave_num, u32 pc, irq_handler function,
                          const char *fmt, ...)
{
    UNUSED(pc);
    userCallback[shave_num] = function;
    initSampleProfiler(shave_num);
    u32 argListSize = 2 * sizeof(u32) + sizeof(irq_handler)
                      + strlen(fmt) * (sizeof(u32) + sizeof(char));
    void* argList = __builtin_apply_args();
    __builtin_apply((void (*)())swcStartShaveAsyncCC_original, argList, argListSize);
    DrvIcbSetIrqHandler(IRQ_SVE_0 + shave_num, profilerCallback);
}
int swcWaitShave(u32 shave_nr)
{
    int result = swcWaitShave_original(shave_nr);
    removeShaveFromList(shave_nr);
    //printf("\nSample Profiler was enabled. Comment these printfs if you use shave runtime measurements."
    //"\nMeasurements taken: %lu\n\n", spSampleCount);
    spSampleCount4___ = sizeof(u32) * spSampleCount;
    return result;
}

int swcWaitShaves(u32 no_of_shaves, swcShaveUnit_t *shave_list)
{
    int result = swcWaitShaves_original(no_of_shaves, shave_list);
    unsigned i;
    for (i = 0; i < no_of_shaves; i++)
    {
        removeShaveFromList(shave_list[i]);
    }
    //printf("\nSample Profiler was enabled. Comment these printfs if you use shave runtime measurements."
    //"\nMeasurements taken: %lu\n\n", spSampleCount);
    spSampleCount4___ = sizeof(u32) * spSampleCount;
    return result;
}


#ifdef SAMPLE_PROF_COMPUTE_WINDOW
static uint32_t ShaveToLeonAddress(uint32_t shaveNumber,uint32_t address)
{
    uint32_t window;
    uint32_t windowBase;
    uint32_t leonAddress;
    //Calculate address of the WindowA register
    uint32_t base = SHAVE_0_BASE_ADR + SLC_TOP_OFFSET_WIN_A + SVU_SLICE_OFFSET * shaveNumber;

    window = (address >> 24) & 0xFF;

    switch (window) {
    case 0x1c:
        windowBase  = GET_REG_WORD_VAL(base + 0x0);
        leonAddress = windowBase + (address & 0x00FFFFFF);
        break;
    case 0x1d:
        windowBase  = GET_REG_WORD_VAL(base + 0x4);
        leonAddress = windowBase + (address & 0x00FFFFFF);
        break;
    case 0x1e:
        windowBase  = GET_REG_WORD_VAL(base + 0x8);
        leonAddress = windowBase + (address & 0x00FFFFFF);
        break;
    case 0x1f:
        windowBase  = GET_REG_WORD_VAL(base + 0xC);
        leonAddress = windowBase + (address & 0x00FFFFFF);
        break;
    default:
        // If we aren't in a window the address isn't modified
        leonAddress = address;
        break;
    }
    return leonAddress;
}
#endif


#endif
