/* usbmsd10.h	Sat May 23 1998 23:27:58  tmm */

/*

Module:  usbmsd10.h

Function:
	Definitions for the USB-IF Mass-Storage Device Class (wire-level)

Version:
	V1.00a	Sat May 23 1998 23:27:58	tmm	Edit level 1

Copyright notice:
	This file copyright (C) 1998 by

		MCCI Corporation
		3520 Krums Corners Road
		Ithaca, NY  14850

	An unpublished work.  All rights reserved.
	
	This file is proprietary information, and may not be disclosed or
	copied without the prior permission of MCCI Corporation.
 
Author:
	Terry Moore, MCCI Corporation	May 1998

Revision history:
   1.00a  Sat May 23 1998 23:27:58  tmm
	Module created.

*/

#ifndef _USBMSD10_H_		/* prevent multiple includes */
#define _USBMSD10_H_

#ifndef _USBIF_H_
# include "usbif.h"
#endif

/****************************************************************************\
|
|	Class, subclass, and protocol codes.
|
\****************************************************************************/

/* class code */
#define	USB_bInterfaceClass_MSD		0x08	/* the mass-storage subclass */

/* subclass */
#define	USB_bInterfaceSubClass_MSD_General	0x01
#define	USB_bInterfaceSubClass_MSD_CDROM	0x02
#define	USB_bInterfaceSubClass_MSD_Tape		0x03

/* protocols */
/* none */

/****************************************************************************\
|
|	Requests -- printer class is somewhat unusual in its implementation.
|
\****************************************************************************/

/*
|| Accept device-specific command -- data is the SCSI 12-byte command.
*/
#define	USB_bRequest_MSD_ADSC	0x00


/**** end of usbmsd10.h ****/
#endif /* _USBMSD10_H_ */
