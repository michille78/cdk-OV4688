/* usbpumpparams.h	Wed Jan 30 2013 16:21:51 chwon */

/*

Module:  usbpumpparams.h

Function:
	Compile-time tunable parameters for the USB DataPump

Version:
	V3.11b	Wed Jan 30 2013 16:21:51 chwon	Edit level 6

Copyright notice:
	This file copyright (C) 2002, 2011, 2013 by

		MCCI Corporation
		3520 Krums Corners Road
		Ithaca, NY  14850

	An unpublished work.  All rights reserved.
	
	This file is proprietary information, and may not be disclosed or
	copied without the prior permission of MCCI Corporation
 
Author:
	Terry Moore, MCCI Corporation	May 2002

Revision history:
   1.64a  Mon May 27 2002 18:56:12  tmm
	Module created.

   3.01f  Tue Sep 06 2011 16:48:10  chwon
	13272: remove use of DEBUG. Define USBPUMP_DEBUG based on DEBUG
	defintion if USBPUMP_DEBUG symbol was not defined.

   3.11b  Sat Jan 12 2013 05:28:25  tmm
	16458: make sure we have a component ID (USBPUMP_COMPONENT_ID) for
	use by tracing and other facilities.

   3.11b  Sat Jan 12 2013 09:38:02  tmm
	16459: add __TMS_USBPUMP_TRACE and USBPUMP_TRACE to control
	compilation of tracing features.

   3.11b  Wed Jan 16 2013 06:23:06  tmm
	16463: add __TMS_DATAPUMP_COMPAT_V3_10 to cover some of the global
	changes that we're making.

   3.11b  Wed Jan 30 2013 16:21:51  chwon
	16459: fixed typo; __TMS_USBUMP_TRACE should be __TMS_USBPUMP_TRACE.

*/

#ifndef _USBPUMPPARAMS_H_		/* prevent multiple includes */
#define _USBPUMPPARAMS_H_

/****************************************************************************\
|
|	Parameters for tuning declarations
|
\****************************************************************************/

/**** some parameters ****/
#ifndef	WANT_HW_DERIVED_UCONFIG
# define	WANT_HW_DERIVED_UCONFIG	0
#endif

#ifndef WANT_HW_DERIVED_UIFCSET
# define	WANT_HW_DERIVED_UIFCSET	0
#endif

#ifndef WANT_HW_DERIVED_UINTERFACE
# define	WANT_HW_DERIVED_UINTERFACE 0
#endif

#ifndef INLINE_UsbMarkCompletionBusy	/* PARAM */
# define	INLINE_UsbMarkCompletionBusy 1
#endif

#ifndef INLINE_UsbMarkCompletionNotBusy	/* PARAM */
# define	INLINE_UsbMarkCompletionNotBusy 1
#endif

#ifndef __TMS_CLOAKED_NAMES_ONLY
# define __TMS_CLOAKED_NAMES_ONLY 0
#endif

/* MCCI_DEBUG, if defined, forces USBPUMP_DEBUG == 1 */
#ifdef MCCI_DEBUG
# undef		__TMS_USBPUMP_DEBUG
# define	__TMS_USBPUMP_DEBUG 1
#endif

/*
|| Remove use of DEBUG. Switch to __TMS_USBPUMP_DEBUG as the cloaked control
|| symbol, and uncloak it as USBPUMP_DEBUG.
|| If USBPUMP_DEBUG is not defined, need to define USBPUMP_DEBUG symbol based
|| on DEBUG symbol definition.
*/
#ifndef __TMS_USBPUMP_DEBUG
# ifdef DEBUG
#  define	__TMS_USBPUMP_DEBUG_TYPE_0	0
#  define	__TMS_USBPUMP_DEBUG_TYPE_1	1
#  define	__TMS_USBPUMP_DEBUG_TYPE_	2
#  define	__TMS_USBPUMP_DEBUG_TYPE_BLANK	__TMS_USBPUMP_DEBUG_TYPE_
#  define	__TMS_USBPUMP_DEBUG_TYPE_SUB(x)	__TMS_USBPUMP_DEBUG_TYPE_##x
#  define	__TMS_USBPUMP_DEBUG_TYPE(x)	__TMS_USBPUMP_DEBUG_TYPE_SUB(x)
#  if __TMS_USBPUMP_DEBUG_TYPE(DEBUG) == __TMS_USBPUMP_DEBUG_TYPE_BLANK
#   define	__TMS_USBPUMP_DEBUG	1	/* blank means debug on */
#  elif __TMS_USBPUMP_DEBUG_TYPE(DEBUG) == __TMS_USBPUMP_DEBUG_TYPE_0
#   define	__TMS_USBPUMP_DEBUG	0	/* defined zero means debug off */
#  else
#   define	__TMS_USBPUMP_DEBUG	1	/* non-zero value means debug on */
#  endif
# else
#  define	__TMS_USBPUMP_DEBUG	0	/* undefined means debug off */
# endif	/* DEBUG */
#endif

#if !__TMS_CLOAKED_NAMES_ONLY
# define	USBPUMP_DEBUG		__TMS_USBPUMP_DEBUG
#endif

/*
|| Starting with V1.7, we've reorganized the names to make things more
|| sensible.  Defining DATAPUMP_COMPAT_V1_6 will declare macros that
|| allow older code to be recompiled to work with the V1.7 datapumpe.
*/

#ifndef __TMS_DATAPUMP_COMPAT_V1_6
# define	__TMS_DATAPUMP_COMPAT_V1_6	0
#endif /* __TMS_DATAPUMP_COMPAT_V1_6 */

/*

Macro:	DATAPUMP_COMPAT_V3_10

Index:	Macro:	__TMS_DATAPUMP_COMPAT_V3_10

Function:
	Binary compile parameter -- enables inclusion of DataPump V3.10 
	backward compatibility features.

Description:
	Starting after V3.10, we made substantial changes to clean up
	and rationalize code. This parameter, if true, allows code 
	targeting the older DataPump APIs to be compiled.

*/

#ifndef	__TMS_DATAPUMP_COMPAT_V3_10
# define	__TMS_DATAPUMP_COMPAT_V3_10	0
#endif /* __TMS_DATAPUMP_COMPAT_V3_10 */

#if !__TMS_CLOAKED_NAMES_ONLY
# define	DATAPUMP_COMPAT_V3_10	__TMS_DATAPUMP_COMPAT_V3_10
#endif

/*

Macro:	USBPUMP_COMPONENT_ID

Index:	Macro:	__TMS_USBPUMP_COMPONENT_ID

Function:
	Identifies the component containing a given compile unit.

Description:
	The DataPump build system defines USBPUMP_COMPONENT_ID based on the
	name of the library or program that contains the component. This
	can be used by custom trace systems to provide component-specific
	trace control, to tag trace messages with the originating component,
	and so forth.  

	The build system sets this macro to expand to an identifier-like name
	(although it doesn't prevent it from beginning with a digit). Trace
	components that need a string should use the C preprocessor #
	operator to obtain it. Bear in mind that you must doubly expand to
	get a macro value converted to a string, so if you need a string, 
	you'll need something like the following:

		#define STRING(x) #x
		#define STRINGVAL(x) STRING(x)

		static const char skName[] = STRINGVAL(USBPUMP_COMPONENT_ID);

	If the compile environment doens't define __TMS_USBPUMP_COMPONENT_ID,
	then the following default will be enforced by usbpumpparams.h:

		#define __TMS_USBPUMP_COMPONENT_ID none

*/

/*
|| Make sure we have a component ID, and define the uncloaked version
*/

#ifndef __TMS_USBPUMP_COMPONENT_ID
# define __TMS_USBPUMP_COMPONENT_ID	none
#endif

#if !__TMS_CLOAKED_NAMES_ONLY
# define	USBPUMP_COMPONENT_ID	__TMS_USBPUMP_COMPONENT_ID
#endif

/*

Macro:	USBPUMP_TRACE

Index:	Macro:	__TMS_USBPUMP_TRACE

Function:
	Configuration parameter to control inclusion of trace commands.

Description:
	These object-like macros represent simple truth values.
	__TMS_USBPUMP_TRACE will be non-zero if trace capabilities are
	to be compiled in, zero otherwise.  USBPUMP_TRACE, if defined, is
	always the same value as __TMS_USBPUMP_TRACE.

	USBPUMP_TRACE is always true if USBPUMP_DEBUG is true. However, it's
	possible (in customer-specific builds) to set USBPUMP_TRACE true in
	free builds.  This is not a standard MCCI build configuration, and
	must be done via a line in port/{name}/mk/buildset.var, for example:

		CPPFLAGS_PORT += -D__TMS_USBPUMP_TRACE=1

*/

#ifndef __TMS_USBPUMP_TRACE
# define __TMS_USBPUMP_TRACE	__TMS_USBPUMP_DEBUG
#else /* def __TMS_USBPUMP_TRACE */
# if !__TMS_USBPUMP_TRACE && __TMS_USBPUMP_DEBUG
#  error "Unsupported configuration: if debug is on, tracing must be on"
# endif
#endif

#if !__TMS_CLOAKED_NAMES_ONLY
# define USBPUMP_TRACE	__TMS_USBPUMP_TRACE
#endif

/**** end of usbpumpparams.h ****/
#endif /* _USBPUMPPARAMS_H_ */
