/* usbproto.h	Sat Dec  8 2001 20:51:54  tmm */

/*

Module:  usbproto.h

Function:
	Represents all USB protocols.

Version:
	V1.70a	Sat Dec  8 2001 20:51:54	tmm	Edit level 1

Copyright notice:
	This file copyright (C) 2001 by

		MCCI Corporation
		3520 Krums Corners Road
		Ithaca, NY  14850

	An unpublished work.  All rights reserved.
	
	This file is proprietary information, and may not be disclosed or
	copied without the prior permission of MCCI Corporation.
 
Author:
	Terry Moore, MCCI Corporation	December 2001

Revision history:
   1.70a  Sat Dec  8 2001 20:51:54  tmm
	Module created.

*/

#ifndef _USBPROTO_H_		/* prevent multiple includes */
#define _USBPROTO_H_

/****************************************************************************\
|
|	At init time we need to have a list of USB protocols.  This list
|	effectively configures this instance of the datapump by providing
|	the list of protocols.  The initialization scheme in the V1 datapump
|	is dynamic; the config table is logically per UDEVICE, 
|
\****************************************************************************/




/**** end of usbproto.h ****/
#endif /* _USBPROTO_H_ */
