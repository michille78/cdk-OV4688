/* usbvideo11.h	Thu Jan 12 2017 15:26:49 chwon */

/*

Module:  usbvideo11.h

Function:
	USB Video Class constants

Version:
	V3.19b	Thu Jan 12 2017 15:26:49 chwon	Edit level 4

Copyright notice:
	This file copyright (C) 2007-2008, 2012, 2017 by

		MCCI Corporation
		3520 Krums Corners Road
		Ithaca, NY  14850

	An unpublished work.  All rights reserved.

	This file is proprietary information, and may not be disclosed or
	copied without the prior permission of MCCI Corporation

Author:
	ChaoTsung Perng, MCCI Corporation	September 2007

Revision history:
   1.97j  Fri Sep 14 2007 09:59:12  ctpeng
	Module Created

   1.97k  Tue Sep 16 2008 12:36:19  chwon
	Fix doc-xml error

   3.01f  Tue Jul 24 2012 09:53:57  chwon
	Fixed documentation.

   3.19b  Thu Jan 12 2017 15:26:49  chwon
	20422: Add USB_VideoControl_FunctionalDescriptor_OUTPUT_TERMINAL
	structure.

*/

#ifndef _USBVIDEO11_H_		/* prevent multiple includes */
#define _USBVIDEO11_H_

/****************************************************************************\
|
|	Video class, subclass, and protocol codes. (A.1, A.2, A.3)
|
\****************************************************************************/

/* class codes */
#define	USB_bInterfaceClass_Video		0x0E	/* the video class */

/* Interface subclasses */
#define	USB_bInterfaceSubClass_VideoControl	0x01	/* control */
#define	USB_bInterfaceSubClass_VideoStreaming	0x02	/* streaming subclass */
#define	USB_bInterfaceSubClass_VideoInterfaceCollection	0x03	/* Midi subclass */

/* A table initializer */
#define	USB_bInterfaceSubClass_Video_INIT \
	{ USB_bInterfaceSubClass_VideoControl, "Video Control" }, \
	{ USB_bInterfaceSubClass_VideoStreaming, "Video Streaming" }, \
	{ USB_bInterfaceSubClass_VideoInterfaceCollection, "Video Interface Collection" }

/* Interface protocol */
#define	USB_bInterfaceProtocol_VideoUndefined	0x00


/****************************************************************************\
|
|	Video Class-Specific Functional Descriptors (A.4, A.5, A.6)
|
\****************************************************************************/

/* XXX incomplete */
struct USB_Video_FunctionalDescriptor_Generic
	{
	unsigned char	bFunctionLength;
	unsigned char	bDescriptorType;
	unsigned char	bDescriptorSubtype;
	unsigned char	bFunctionSpecifcData[1];
	};

/*
|| Video class specific descriptor Types
*/
#define	USB_bDescriptorType_Video_DEVICE	0x21	/* Class-specific descriptor, Device */
#define	USB_bDescriptorType_Video_CONFIGURATION	0x22	/* Class-specific descriptor, Configuration */
#define	USB_bDescriptorType_Video_STRING	0x23	/* Class-specific descriptor, String */
#define	USB_bDescriptorType_Video_INTERFACE	0x24	/* Class-specific descriptor, Interace */
#define	USB_bDescriptorType_Video_ENDPOINT	0x25	/* Class-specific descriptor, Endpoint */

/*
|| Video control interfce descriptor subtypes
*/
#define USB_bDescriptorSubtype_VideoControl_HEADER		0x01
#define USB_bDescriptorSubtype_VideoControl_INPUT_TERMINAL	0x02
#define USB_bDescriptorSubtype_VideoControl_OUTPUT_TERMINAL	0x03
#define USB_bDescriptorSubtype_VideoControl_SELECTOR_UNIT	0x04
#define USB_bDescriptorSubtype_VideoControl_PROCESSING_UNIT	0x05
#define USB_bDescriptorSubtype_VideoControl_EXTENSION_UNIT	0x06

#define	USB_bDescriptorSubtype_VideoControl__INIT \
	{ USB_bDescriptorSubtype_VideoControl_HEADER, "Header" }, \
	{ USB_bDescriptorSubtype_VideoControl_INPUT_TERMINAL, "InputTerminal" }, \
	{ USB_bDescriptorSubtype_VideoControl_OUTPUT_TERMINAL, "OutputTerminal" }, \
	{ USB_bDescriptorSubtype_VideoControl_SELECTOR_UNIT, "SelectorUnit" }, \
	{ USB_bDescriptorSubtype_VideoControl_PROCESSING_UNIT, "ProcessingUnit" }, \
	{ USB_bDescriptorSubtype_VideoControl_EXTENSION_UNIT, "ExtensionUnit" }

/*
|| Video streamming interfce descriptor subtypes
*/
#define USB_bDescriptorSubtype_VideoStreaming_INPUT_HEADER		0x01
#define USB_bDescriptorSubtype_VideoStreaming_OUTPUT_HEADER		0x02
#define USB_bDescriptorSubtype_VideoStreaming_STILL_IMAGE_FRAME		0x03
#define USB_bDescriptorSubtype_VideoStreaming_FORMAT_UNCOMPRESSED	0x04
#define USB_bDescriptorSubtype_VideoStreaming_FRAME_UNCOMPRESSED	0x05
#define USB_bDescriptorSubtype_VideoStreaming_FORMAT_MJPEQ		0x06
#define USB_bDescriptorSubtype_VideoStreaming_FRAME_MJPEQ		0x07
#define USB_bDescriptorSubtype_VideoStreaming_Reserve8			0x08
#define USB_bDescriptorSubtype_VideoStreaming_Reserve9			0x09
#define USB_bDescriptorSubtype_VideoStreaming_FORMAT_MPEG2TS		0x0A
#define USB_bDescriptorSubtype_VideoStreaming_ReserveB			0x0B
#define USB_bDescriptorSubtype_VideoStreaming_FORMAT_DV			0x0C
#define USB_bDescriptorSubtype_VideoStreaming_COLORFORMAT		0x0D
#define USB_bDescriptorSubtype_VideoStreaming_ReserveE			0x0E
#define USB_bDescriptorSubtype_VideoStreaming_ReserveF			0x0F
#define USB_bDescriptorSubtype_VideoStreaming_FORMAT_FRAME_BASED	0x10
#define USB_bDescriptorSubtype_VideoStreaming_FRAME_FRAME_BASED		0x11
#define USB_bDescriptorSubtype_VideoStreaming_FORMAT_STREAM_BASED	0x12

#define	USB_bDescriptorSubtype_VideoStreaming__INIT \
	{ USB_bDescriptorSubtype_VideoStreaming_INPUT_HEADER, "InputHeader" }, \
	{ USB_bDescriptorSubtype_VideoStreaming_OUTPUT_HEADER, "OutputHeader" }, \
	{ USB_bDescriptorSubtype_VideoStreaming_STILL_IMAGE_FRAME, "StillImageFrame" }, \
	{ USB_bDescriptorSubtype_VideoStreaming_FORMAT_UNCOMPRESSED, "FormatUncompressed" }, \
	{ USB_bDescriptorSubtype_VideoStreaming_FRAME_UNCOMPRESSED, "FrameUncompressed" }, \
	{ USB_bDescriptorSubtype_VideoStreaming_FORMAT_MJPEQ, "FormatMJPEG" }, \
	{ USB_bDescriptorSubtype_VideoStreaming_FRAME_MJPEQ, "FrameMJPEG" }, \
	{ USB_bDescriptorSubtype_VideoStreaming_Reserve8, "Reserve8" }, \
	{ USB_bDescriptorSubtype_VideoStreaming_Reserve9, "Reserve9" }, \
	{ USB_bDescriptorSubtype_VideoStreaming_FORMAT_MPEG2TS, "FormatMPEG2TS" }, \
	{ USB_bDescriptorSubtype_VideoStreaming_ReserveB, "ReserveB" }, \
	{ USB_bDescriptorSubtype_VideoStreaming_FORMAT_DV, "FormatDV" }, \
	{ USB_bDescriptorSubtype_VideoStreaming_COLORFORMAT, "ColorFormat" }, \
	{ USB_bDescriptorSubtype_VideoStreaming_ReserveE, "ReserveE" }, \
	{ USB_bDescriptorSubtype_VideoStreaming_ReserveF, "ReserveE" }, \
	{ USB_bDescriptorSubtype_VideoStreaming_FORMAT_FRAME_BASED, "FormatFrameBased" }, \
	{ USB_bDescriptorSubtype_VideoStreaming_FRAME_FRAME_BASED, "FrametFrameBased" }, \
	{ USB_bDescriptorSubtype_VideoStreaming_FORMAT_STREAM_BASED, "FormatStreamBased" }


/****************************************************************************\
|
|	Video Class-Specific Endpoint Descriptor Subtypes (A.7)
|
\****************************************************************************/

#define USB_bDescriptorSubtype_VideoEP_GENERAL		0x01
#define USB_bDescriptorSubtype_VideoEP_ENDPOINT		0x02
#define USB_bDescriptorSubtype_VideoEP_INTERRUPT	0x03

#define	USB_bDescriptorSubtype_VideoEP__INIT \
	{ USB_bDescriptorSubtype_VideoEP_GENERAL, "General" }, \
	{ USB_bDescriptorSubtype_VideoEP_ENDPOINT, "Endpoint" }, \
	{ USB_bDescriptorSubtype_VideoEP_INTERRUPT, "Interrupt" }


/****************************************************************************\
|
|	Video Class-Specific Request Codes (A.8)
|
\****************************************************************************/

#define	USB_bRequest_Video_UNDEFINED	0x00	/* undefined */
#define	USB_bRequest_Video_SET_CUR	0x01	/* set current */
#define	USB_bRequest_Video_GET_CUR	0x81	/* get current */
#define	USB_bRequest_Video_GET_MIN	0x82	/* get minimum */
#define	USB_bRequest_Video_GET_MAX	0x83	/* get maximum */
#define	USB_bRequest_Video_GET_RES	0x84	/* get resolution */
#define	USB_bRequest_Video_GET_LEN	0x85	/* get length space */
#define	USB_bRequest_Video_GET_INFO	0x86	/* get information space */
#define	USB_bRequest_Video_GET_DEF	0x87	/* get default space */

#define	USB_bRequest_Video__INIT \
	{ USB_bRequest_Video_UNDEFINED, "UNDEFINED" },	\
	{ USB_bRequest_Video_SET_CUR, "SET_CUR" },	\
	{ USB_bRequest_Video_GET_CUR, "GET_CUR"	},	\
	{ USB_bRequest_Video_GET_MIN, "GET_MIN"	},	\
	{ USB_bRequest_Video_GET_MAX, "GET_MAX"	},	\
	{ USB_bRequest_Video_GET_RES, "GET_RES"	},	\
	{ USB_bRequest_Video_GET_LEN, "SET_LEN" },	\
	{ USB_bRequest_Video_GET_INFO, "GET_INFO" },	\
	{ USB_bRequest_Video_GET_DEF, "GET_DEF"}


/****************************************************************************\
|
|	Control Selector Codes (A.9)
|
\****************************************************************************/

/* A.9.1 VideoControl Interface Control Selectors */
#define	USBVIDEO_bControlSelector_VC_CONTROL_UNDEFINED		0x00
#define	USBVIDEO_bControlSelector_VC_VIDEO_POWER_MODE_CONTROL	0x01
#define	USBVIDEO_bControlSelector_VC_REQUEST_ERROR_CODE_CONTROL	0x02

/* A.9.2 Terminal Control Selectors */
#define	USBVIDEO_bControlSelector_TE_CONTROL_UNDEFINED		0x00

/* A.9.3 Selector Unit Control Selectors */
#define	USBVIDEO_bControlSelector_SU_CONTROL_UNDEFINED		0x00
#define	USBVIDEO_bControlSelector_SU_INPUT_SELECT_CONTROL	0x01

/* A.9.4 Camera Terminal Control Selectors */
#define	USBVIDEO_bControlSelector_CT_CONTROL_UNDEFINED		0x00
#define	USBVIDEO_bControlSelector_CT_SCANNING_MODE_CONTROL	0x01
#define	USBVIDEO_bControlSelector_CT_AE_MODE_CONTROL		0x02
#define	USBVIDEO_bControlSelector_CT_AE_PRIORITY_CONTROL	0x03
#define	USBVIDEO_bControlSelector_CT_EXPOSURE_TIME_ABSOLUTE_CONTROL	0x04
#define	USBVIDEO_bControlSelector_CT_EXPOSURE_TIME_RELATIVE_CONTROL	0x05
#define	USBVIDEO_bControlSelector_CT_FOCUS_ABSOLUTE_CONTROL	0x06
#define	USBVIDEO_bControlSelector_CT_FOCUS_RELATIVE_CONTROL	0x07
#define	USBVIDEO_bControlSelector_CT_FOCUS_AUTO_CONTROL		0x08
#define	USBVIDEO_bControlSelector_CT_IRIS_ABSOLUTE_CONTROL	0x09
#define	USBVIDEO_bControlSelector_CT_IRIS_RELATIVE_CONTROL	0x0A
#define	USBVIDEO_bControlSelector_CT_ZOOM_ABSOLUTE_CONTROL	0x0B
#define	USBVIDEO_bControlSelector_CT_ZOOM_RELATIVE_CONTROL	0x0C
#define	USBVIDEO_bControlSelector_CT_PANTILT_ABSOLUTE_CONTROL	0x0D
#define	USBVIDEO_bControlSelector_CT_PANTILT_RELATIVE_CONTROL	0x0E
#define	USBVIDEO_bControlSelector_CT_ROLL_ABSOLUTE_CONTROL	0x0F
#define	USBVIDEO_bControlSelector_CT_ROLL_RELATIVE_CONTROL	0x10
#define	USBVIDEO_bControlSelector_CT_PRIVACY_CONTROL		0x11

/* A.9.5 Processing Unit Control Selectors*/
#define	USBVIDEO_bControlSelector_PU_CONTROL_UNDEFINED		0x00
#define	USBVIDEO_bControlSelector_PU_BACKLIGHT_COMPENSATION_CONTROL	0x01
#define	USBVIDEO_bControlSelector_PU_BRIGHTNESS_CONTROL		0x02
#define	USBVIDEO_bControlSelector_PU_CONTRAST_CONTROL		0x03
#define	USBVIDEO_bControlSelector_PU_GAIN_CONTROL		0x04
#define	USBVIDEO_bControlSelector_PU_POWER_LINE_FREQUENCY_CONTROL	0x05
#define	USBVIDEO_bControlSelector_PU_HUE_CONTROL		0x06
#define	USBVIDEO_bControlSelector_PU_SATURATION_CONTROL		0x07
#define	USBVIDEO_bControlSelector_PU_SHARPNESS_CONTROL		0x08
#define	USBVIDEO_bControlSelector_PU_GAMMA_CONTROL		0x09
#define	USBVIDEO_bControlSelector_PU_WHITE_BALANCE_TEMPERATURE_CONTROL	0x0A
#define	USBVIDEO_bControlSelector_PU_WHITE_BALANCE_TEMPERATURE_AUTO_CONTROL 0x0B
#define	USBVIDEO_bControlSelector_PU_WHITE_BALANCE_COMPONENT_CONTROL	0x0C
#define	USBVIDEO_bControlSelector_PU_WHITE_BALANCE_COMPONENT_AUTO_CONTROL 0x0D
#define	USBVIDEO_bControlSelector_PU_DIGITAL_MULTIPLIER_CONTROL	0x0E
#define	USBVIDEO_bControlSelector_PU_DIGITAL_MULTIPLIER_LIMIT_CONTROL	0x0F
#define	USBVIDEO_bControlSelector_PU_HUE_AUTO_CONTROL		0x10
#define	USBVIDEO_bControlSelector_PU_ANALOG_VIDEO_STANDARD_CONTROL	0x11
#define	USBVIDEO_bControlSelector_PU_ANALOG_LOCK_STATUS_CONTROL	0x12

/* A.9.6 Extension Unit Control Selectors*/
#define	USBVIDEO_bControlSelector_XU_CONTROL_UNDEFINED		0x00

/* A.9.7 VideoStream Interface Control Selectors*/
#define	USBVIDEO_bControlSelector_VS_CONTROL_UNDEFINED		0x00
#define	USBVIDEO_bControlSelector_VS_PROBE_CONTROL		0x01
#define	USBVIDEO_bControlSelector_VS_COMMIT_CONTROL		0x02
#define	USBVIDEO_bControlSelector_VS_STILL_PROBE_CONTROL	0x03
#define	USBVIDEO_bControlSelector_VS_STILL_COMMIT_CONTROL	0x04
#define	USBVIDEO_bControlSelector_VS_STILL_IMAGE_TRIGGER_CONTROL	0x05
#define	USBVIDEO_bControlSelector_VS_STREAM_ERROR_CODE_CONTROL 	0x06
#define	USBVIDEO_bControlSelector_VS_GENERATE_KEY_FRAME_CONTROL 0x07
#define	USBVIDEO_bControlSelector_VS_UPDATE_FRAME_SEGMENT_CONTROL	0x08
#define	USBVIDEO_bControlSelector_VS_SYNCH_DELAY_CONTROL 	0x09

/****************************************************************************\
|
|	Terminal Types [Video Terminal Types] (B)
|
\****************************************************************************/

/* B.1  USB Terminal Types */
#define	USBVIDEO_wTerminalType_TT_VENDOR_SPECIFIC		0x0100
#define	USBVIDEO_wTerminalType_TT_STREAMING			0x0101

/* B.2  Input Terminal Types */
#define	USBVIDEO_wTerminalType_ITT_VENDOR_SPECIFIC		0x0200
#define	USBVIDEO_wTerminalType_ITT_CAMERA			0x0201
#define	USBVIDEO_wTerminalType_ITT_MEDIA_TRANPORT_INPUT		0x0202

/* B.3  Input Terminal Types */
#define	USBVIDEO_wTerminalType_OTT_VENDOR_SPECIFIC		0x0300
#define	USBVIDEO_wTerminalType_OTT_DISPLAY			0x0301
#define	USBVIDEO_wTerminalType_OTT_MEDIA_TRANPORT_OUTPUT	0x0302

/* B.4  External Terminal Types */
#define	USBVIDEO_wTerminalType_EXTERNAL_VENDOR_SPECIFIC		0x0400
#define	USBVIDEO_wTerminalType_COMPOSITE_CONNECTOR		0x0401
#define	USBVIDEO_wTerminalType_SVIDEO_CONNECTOR			0x0402
#define	USBVIDEO_wTerminalType_COMPONENT_CONNECTOR		0x0402

/* symbolic names */
#define USBVIDEO_wTerminalType__INIT \
	{ USBVIDEO_wTerminalType_TT_VENDOR_SPECIFIC, "USBVIDEO_wTerminalType_VENDOR_SPECIFIC" }, \
	{ USBVIDEO_wTerminalType_TT_STREAMING, "USBVIDEO_wTerminalType_TT_STREAMING" }, \
	{ USBVIDEO_wTerminalType_ITT_VENDOR_SPECIFIC, "USBVIDEO_wTerminalType_INPUT_ITT_VENDOR_SPECIFIC" }, \
	{ USBVIDEO_wTerminalType_ITT_CAMERA, "USBVIDEO_wTerminalType_INPUT_ITT_CAMERA" }, \
	{ USBVIDEO_wTerminalType_ITT_MEDIA_TRANPORT_INPUT, "USBVIDEO_wTerminalType_INPUT_ITT_MEDIA_TRANPORT_INPUT" }, \
	{ USBVIDEO_wTerminalType_OTT_VENDOR_SPECIFIC, "USBVIDEO_wTerminalType_OUTPUT_OTT_VENDOR_SPECIFIC" }, \
	{ USBVIDEO_wTerminalType_OTT_DISPLAY, "USBVIDEO_wTerminalType_OUTPUT_OTT_DISPLAY" }, \
	{ USBVIDEO_wTerminalType_OTT_MEDIA_TRANPORT_OUTPUT, "USBVIDEO_wTerminalType_OUTPUT_OTT_MEDIA_TRANPORT_OUTPUT" }, \
	{ USBVIDEO_wTerminalType_EXTERNAL_VENDOR_SPECIFIC, "USBVIDEO_wTerminalType_EXTERNAL_VENDOR_SPECIFIC" }, \
	{ USBVIDEO_wTerminalType_COMPOSITE_CONNECTOR, "USBVIDEO_wTerminalType_EXTERNAL_COMPOSITE_CONNECTOR" }, \
	{ USBVIDEO_wTerminalType_SVIDEO_CONNECTOR, "USBVIDEO_wTerminalType_EXTERNAL_SVIDEO_CONNECTOR" }, \
	{ USBVIDEO_wTerminalType_COMPONENT_CONNECTOR, "USBVIDEO_wTerminalType_EXTERNAL_COMPONENT_CONNECTOR" }


/****************************************************************************\
|
|	Video Format Tags
|
\****************************************************************************/


/* XXX need MPEG control selectors and AC-3 control selectors */

/****************************************************************************\
|
|	The header functional descriptor (3.7.2)
|
\****************************************************************************/

struct USB_VideoControl_FunctionalDescriptor_HEADER
	{
	unsigned char	bFunctionLength;
	unsigned char	bDescriptorType;
	unsigned char	bDescriptorSubtype;
	unsigned char	bcdVDC[2];		/*
						|| the version of the spec.
						||  1.1 == 0x0110
						*/
	unsigned char	wTotalLength[2];	/* total length including
						|| other descriptors
						*/
	unsigned char	dwClockFrequency[4];
	unsigned char	bInCollection;		/* # of interfaces in
						|| collection
						*/
	unsigned char	baInterfaceNr[1];	/* vector of associated ifcs */
	};

/****************************************************************************\
|
|	The generic entity descriptor
|
\****************************************************************************/

struct USB_VideoControl_FunctionalDescriptor_ENTITY_GENERAL
	{
	unsigned char	bFunctionLength;
	unsigned char	bDescriptorType;
	unsigned char	bDescriptorSubtype;
	unsigned char	bEntityId;
	};

/****************************************************************************\
|
|	The input terminal functional descriptor (3.7.2.1)
|
\****************************************************************************/

struct USB_VideoControl_FunctionalDescriptor_INPUT_TERMINAL
	{
	unsigned char	bFunctionLength;
	unsigned char	bDescriptorType;
	unsigned char	bDescriptorSubtype;

	/*
	|| A constant uniqely identifyingthe terminal within the video
	|| function.  This value is used in all requests to address this
	|| Terminal.
	*/
	unsigned char	bTerminalID;

	/*
	|| The type of the input terminal.  See [USBAUDTERM1.0].
	*/
	unsigned char	wTerminalType[2];

	/*
	|| the ID of the output terminal to which the input terminal is
	|| associated.
	*/
	unsigned char	bAssocTerminal;

	/*
	|| string index of the name of the input terminal
	*/
	unsigned char	iTerminal;

	/*
	|| Min Focal Length
	*/
	unsigned char	wObjectiveFocalLengthMin[2];

	/*
	|| Max Focal Length
	*/
	unsigned char	wObjectiveFocalLengthMax[2];

	/*
	|| Focal Length Step
	*/
	unsigned char	wObjectiveFocalLength[2];

	/*
	|| The size of control bitmap
	*/
	unsigned char	bControlSize;

	/*
	|| Bitmap of related attributes
	*/
	unsigned char	wbmControl[2];
	};


/****************************************************************************\
|
|	The output terminal functional descriptor (3.7.2.2)
|
\****************************************************************************/

struct USB_VideoControl_FunctionalDescriptor_OUTPUT_TERMINAL
	{
	unsigned char	bFunctionLength;
	unsigned char	bDescriptorType;
	unsigned char	bDescriptorSubtype;

	/*
	|| A constant uniqely identifying the terminal within the video
	|| function.  This value is used in all requests to address this
	|| Terminal.
	*/
	unsigned char	bTerminalID;

	/*
	|| The type of the output terminal.  See [USBAUDTERM1.0].
	*/
	unsigned char	wTerminalType[2];

	/*
	|| the ID of the input terminal to which the output terminal is
	|| associated.
	*/
	unsigned char	bAssocTerminal;

	/*
	|| the ID of the Unit or Terminal to which this Terminal is
	|| connected
	*/
	unsigned char	bSourceID;

	/*
	|| string index for name of this output terminal
	*/
	unsigned char	iTerminal;
	};


/****************************************************************************\
|
|	The selector unit functional descriptor (3.7.2.4)
|
\****************************************************************************/

struct USB_VideoControl_FunctionalDescriptor_SELECTOR_UNIT
	{
	unsigned char	bFunctionLength;
	unsigned char	bDescriptorType;
	unsigned char	bDescriptorSubtype;

	/*
	|| A constant uniqely identifying the unit within the video
	|| function.  This value is used in all requests to address this
	|| Unit
	*/
	unsigned char	bUnitID;

	/* Number of input pins of this Unit:  [p] */
	unsigned char	bNrInPins;

	/*
	|| ID of each Unit or terminal to which the input pins of this
	|| Selector unit are connected.
	*/
	unsigned char	baSourceID[1];
	};

/*
|| The video descriptor data structures were evidently not designed by
|| C programmers, so the access methods are a bit clunky.
*/
#define	USBVIDEO_SELECTOR_UNIT_iSelector(pDesc) \
	(((unsigned char *)(pDesc))[(pDesc)->bFunctionLength - 1])


/****************************************************************************\
|
|	The Video Streaming Endpoint functional descriptor (3.8.2.2)
|
\****************************************************************************/

struct USB_VideoControl_EndpointDescriptor_GENERAL
	{
	unsigned char	bFunctionLength;
	unsigned char	bDescriptorType;
	unsigned char	bDescriptorSubtype;
	unsigned char	wMaxTransferSize[2];
	};


/****************************************************************************\
|
|	The Video Streaming Input Header Descriptor (3.9.2.1)
|
\****************************************************************************/

struct USB_VideoStreaming_FunctionalDescriptor_INPUT_HEADER
	{
	unsigned char	bFunctionLength;
	unsigned char	bDescriptorType;
	unsigned char	bDescriptorSubtype;

	/*
	|| The number of video payload format descriptors following
	|| for this interface.
	*/
	unsigned char	bNumFormats;

	/*
	|| The total length of bytes returned for this class-specific
	|| VideoStreaming interface descriptors including this header
	|| descriptor.
	*/
	unsigned char	wTotalLength[2];

	/*
	|| The address of the iso/bulk endpoint used for video data.
	*/
	unsigned char	bEndpointAddress;

	/*
	|| Bitmap indicating the capabilities of this VideoStreaming
	|| interface.
	*/
	unsigned char	bmInfo;

	/*
	|| The terminal ID of the Output terminal to which the endpoint
	|| of this interface is connected.
	*/
	unsigned char	bTerminalLink;

	/*
	|| Method of still image capture supported.
	*/
	unsigned char	bStillCaptureMethod;

	/*
	|| Specify if hardware triggering is supported through this interface.
	*/
	unsigned char	bTriggerSupport;

	/*
	|| Specify how the host software shall respond to a hardware trigger
	|| interrupt event from this interface.
	*/
	unsigned char	bTriggerUsage;

	/*
	|| Size of each bmaControls field, in bytes.
	*/
	unsigned char	bControlSize;

	/*
	|| Indicate that named field is supported by Video Probe and Commit
	|| Control when bFormatIndex is 1.
	*/
	unsigned char	bmaControls;
	};


/****************************************************************************\
|
|	The Video Streaming Output Header Descriptor (3.9.2.2)
|
\****************************************************************************/

struct USB_VideoStreaming_FunctionalDescriptor_OUTPUT_HEADER
	{
	unsigned char	bFunctionLength;
	unsigned char	bDescriptorType;
	unsigned char	bDescriptorSubtype;

	/*
	|| The number of video payload format descriptors following
	|| for this interface.
	*/
	unsigned char	bNumFormats;

	/*
	|| The total length of bytes returned for this class-specific
	|| VideoStreaming interface descriptors including this header
	|| descriptor.
	*/
	unsigned char	wTotalLength[2];

	/*
	|| The address of the iso/bulk endpoint used for video data.
	*/
	unsigned char	bEndpointAddress;

	/*
	|| The terminal ID of the Output terminal to which the endpoint
	|| of this interface is connected.
	*/
	unsigned char	bTerminalLink;

	/*
	|| Size of each bmaControls field, in bytes.
	*/
	unsigned char	bControlSize;

	/*
	|| Indicate that named field is supported by Video Probe and Commit
	|| Control when bFormatIndex is 1.
	*/
	unsigned char	bmaControls;
	};


/****************************************************************************\
|
|	XXX other video-class things -- not completed.
|
\****************************************************************************/

/**** end of usbvideo11.h ****/
#endif /* _USBVIDEO11_H_ */
