/* uendpoint.h	Mon Feb 18 2013 15:54:49 chwon */

/*

Module:  uendpoint.h

Function:
	Home for UENDPOINT

Version:
	V3.11c	Mon Feb 18 2013 15:54:49 chwon	Edit level 11

Copyright notice:
	This file copyright (C) 2002-2008, 2010-2011, 2013 by

		MCCI Corporation
		3520 Krums Corners Road
		Ithaca, NY  14850

	An unpublished work.  All rights reserved.
	
	This file is proprietary information, and may not be disclosed or
	copied without the prior permission of MCCI Corporation
 
Author:
	Terry Moore, MCCI Corporation	May 2002

Revision history:
   1.79a  Mon May 27 2002 23:01:26  tmm
	Module created.

   1.79b  Mon Sep 23 2002 17:30:00  maw
   	UENDPOINT_COUNT_PENDING_BYTES corretced. Returned an invalid pLastQe value

   1.79b  Mon Nov 18 2002 15:30:00 maw
	Added cloaking to UENDPOINT_HDR

   1.79b  12/3/2002  chwon
	Get rid of uninitialized variable warning in UENDPOINT_COUNT_PENDING_BYTES.

   1.91c  Tue May 24 2005 12:32:21  chwon
	Add UsbEndpointLockIo() & UsbEndpointUnlockIo()

   1.97k  Tue Apr 22 2008 11:37:19  chwon
	3735: reorganize UBUFQE to allow 32-bit sizes -- change input parameter
	from USHORT to BYTES. Generate uncloaked name using uncloak-def.sh.
	3746:  fix name cloaking for UBUFQEFLAG_xxx
	4040: use __TMS_NULL instead of NULL.

   3.01c  Sun Aug 29 2010 02:44:04  cccho
	10109: Add UENDPOINTSWITCH_INIT_V4 and UENDPOINTSWITCH_QUERYSTATUS_FN
	function prototype.

   3.01c  Wed Sep 01 2010 17:51:38  chwon
	11117: increase uephh_Size field from 8 bits to 10 bits and change
	uephh_siolock and uephh_ucTimeoutFrames to bit field.

   3.01d  Mon Jan 03 2011 12:31:23  cccho
	12155: add UENDPOINT_AUTO_REMOTE_WAKEUP_OK()

   3.01f  Fri Jul 01 2011 14:02:15  maw
   	13233: add endpoint activity flag uep_fActive, 
   	uep_fActivateWhenComplete.

   3.11c  Mon Feb 18 2013 15:54:49  chwon
	16743: Add UENDPOINT_CAN_AUTO_REMOTE_WAKEUP() and
	UENDPOINT_CHECK_AUTO_REMOTE_WAKEUP() macro.

*/

#ifndef _UENDPOINT_H_		/* prevent multiple includes */
#define _UENDPOINT_H_

#ifndef _USBPUMPENV_H_
# include "usbpumpenv.h"
#endif

#ifndef _UDEVICE_H_
# include "udevice.h"
#endif

#ifndef _UPIPE_H_
# include "upipe.h"
#endif

/****************************************************************************\
|
|	The standard endpoint structure -- this is used to manage I/O
|	and so must be a base type that the hardware level can derive
|	from.  
|
\****************************************************************************/

/**** the contents of the embedded header ****/
struct TTUSB_UENDPOINT_HDR
	{
	__TMS_UBUFQE *uephh_pending;	/* queue elements being filled */
	__TMS_UPIPE *uephh_pPipe;	/* pointer to the pipe we work for;
					|| if NULL, then not tied to a pipe,
					|| so this endpoint cannot do I/O.
					|| All of the information about the
					|| current endpoint configuration
					|| is held in the UPIPE.
					*/
	__TMS_VOID *uephh_pExtension;	/* application extension */
	__TMS_CONST __TMS_UENDPOINTSWITCH *uephh_pSwitch;	
					/* pointer to the endpoint switch
					|| struct, which allows dispatch
					|| based on device/endpoint type.
					*/
	__TMS_UINT uephh_stall: 1;	/* stalled if TRUE */
	__TMS_UINT uephh_fChanged: 1;	/* configuration of ep changed */
	__TMS_UINT uephh_fActive: 1;	/* ep is active if TRUE */
	__TMS_UINT uephh_fActivateWhenComplete: 1;
					/* if TRUE, activate endpoints in the
					|| same interface when UBUFQE is
					|| completed
					*/
	__TMS_UINT uephh_Size: 10;	/* size of this endpoint struct */
	__TMS_UINT uephh_siolock: 8;	/* start-I/O lock-out count */
	__TMS_UINT uephh_ucTimeoutFrames: 8; /* timeout down-counter */
	};

/**** the macro to embed in derived structures ****/
#define	__TMS_UENDPOINT_HDR	struct TTUSB_UENDPOINT_HDR uep_hh


/**** the generic structure ****/
struct TTUSB_UENDPOINT
	{
	__TMS_UENDPOINT_HDR;
	};

/**** sane names ****/
#define	uep_pending			uep_hh.uephh_pending
#define	uep_Size			uep_hh.uephh_Size
#define	uep_pSwitch			uep_hh.uephh_pSwitch
#define	uep_pPipe			uep_hh.uephh_pPipe
#define	uep_pExtension			uep_hh.uephh_pExtension
#define	uep_stall			uep_hh.uephh_stall
#define uep_fChanged			uep_hh.uephh_fChanged
#define uep_fActive			uep_hh.uephh_fActive
#define uep_fActivateWhenComplete	uep_hh.uephh_fActivateWhenComplete
#define	uep_siolock			uep_hh.uephh_siolock
#define	uep_ucTimeoutFrames		uep_hh.uephh_ucTimeoutFrames

/**** the initializer ****/
#define	UsbGenericEndpointInit(pep)		\
    do	{					\
    	(pep)->uep_pending = __TMS_NULL;	\
	(pep)->uep_pPipe = __TMS_NULL;		\
	(pep)->uep_Size = sizeof(*(pep));	\
	(pep)->uep_fChanged = __TMS_TRUE;	\
	(pep)->uep_fActive = __TMS_TRUE;	\
	(pep)->uep_fActivateWhenComplete = __TMS_FALSE;	\
	} while (0)

/**** start I/O lock handling ****/
#define UsbEndpointLockIo(pEndpoint)	++((pEndpoint)->uep_siolock)

#define UsbEndpointUnlockIo(pEndpoint)	--((pEndpoint)->uep_siolock)

/**** iterators ****/
#define	UENDPOINT_SIZE(p)	((p)->uep_Size)
#define	UENDPOINT_INDEX(newp, p, i)	\
    do	{ \
	int __i = (i); \
	(newp) = (__TMS_UENDPOINT *) (p); \
	for (; __i > 0; --__i) \
		(newp) = (__TMS_UENDPOINT *) \
			((char *)(newp) + UENDPOINT_SIZE(newp)); \
	} while (0)
#define	UENDPOINT_NEXT(p) \
	((__TMS_UENDPOINT *)((char *)(p) + UENDPOINT_SIZE(p)))


/****************************************************************************\
|
|	The Endpoint Switch contains dispatch functions for the
|	endpoint.
|
\****************************************************************************/

__TMS_BEGIN_DECLS

__TMS_FNTYPE_DEF(
USTARTIOFN,
	__TMS_VOID,
		(
		__TMS_UDEVICE *, 
		__TMS_UENDPOINT *
		)
	);

__TMS_FNTYPE_DEF(
UCANCELIOFN,
	__TMS_VOID,
		(
		__TMS_UDEVICE *, 
		__TMS_UENDPOINT *, 
		__TMS_USTAT /*why*/
		)
	);

__TMS_FNTYPE_DEF(
UPREPIOFN,
	 __TMS_VOID,
		(
		__TMS_UDEVICE *, 
		__TMS_UENDPOINT *, 
		__TMS_UBUFQE *
		)
	);

__TMS_FNTYPE_DEF(
UEPEVENTFN,
	__TMS_VOID,
		(
		__TMS_UDEVICE *, 
		__TMS_UENDPOINT *
		)
	);

__TMS_FNTYPE_DEF(
UTIMEOUTFN,
	__TMS_VOID,
		(
		__TMS_UDEVICE *, 
		__TMS_UENDPOINT *
		)
	);

__TMS_TYPE_DEF_FUNCTION(
UENDPOINTSWITCH_QUERYSTATUS_FN,
	__TMS_BOOL,
		(
		__TMS_UDEVICE *,
		__TMS_UENDPOINT *
		)
	);

/* stub functions */
__TMS_UEPEVENTFN UsbEpswEpEventStub;
__TMS_UTIMEOUTFN UsbEpswTimeoutStub;
__TMS_UENDPOINTSWITCH_QUERYSTATUS_FN	UsbPumpEndpointSwitch_QeuryStatusStub;

__TMS_END_DECLS

struct TTUSB_UENDPOINTSWITCH
	{
	__TMS_USTARTIOFN	*uepsw_StartIo;
	__TMS_UCANCELIOFN	*uepsw_CancelIo;
	__TMS_UPREPIOFN		*uepsw_PrepIo;
	__TMS_UEPEVENTFN	*uepsw_EpEvent;
	__TMS_UTIMEOUTFN	*uepsw_Timeout;
	__TMS_UENDPOINTSWITCH_QUERYSTATUS_FN	*uepsw_QueryStatus;
	};

#define	__TMS_USTARTIO(pDev, pEp)	\
		((*((pEp)->uep_pSwitch)->uepsw_StartIo)((pDev), (pEp)))

#define	__TMS_UCANCELIO(pDev, pEp, why)	\
		((*((pEp)->uep_pSwitch)->uepsw_CancelIo)((pDev), (pEp), (why)))

#define	__TMS_UPREPIO(pDev, pEp, pQe)	\
		((*((pEp)->uep_pSwitch)->uepsw_PrepIo)((pDev), (pEp), (pQe)))

#define __TMS_UEPEVENT(pDev, pEp) \
		((*((pEp)->uep_pSwitch)->uepsw_EpEvent)((pDev), (pEp)))

#define	__TMS_UTIMEOUT(pDev, pEp)	\
		((*((pEp)->uep_pSwitch)->uepsw_Timeout)((pDev), (pEp)))

#define	__TMS_UENDPOINTSWITCH_QUERYSTATUS(pDev, pEp)	\
		((*((pEp)->uep_pSwitch)->uepsw_QueryStatus)(	\
			(pDev), (pEp)))

/* use these for initializing endpoint switches */
#define __TMS_UENDPOINTSWITCH_INIT_V1(StartIo, CancelIo, PrepIo) \
	__TMS_UENDPOINTSWITCH_INIT_V2(StartIo, CancelIo, PrepIo, \
		UsbEpswEpEventStub)

#define	__TMS_UENDPOINTSWITCH_INIT_V2(StartIo, CancelIo, PrepIo, EpEvent) \
	__TMS_UENDPOINTSWITCH_INIT_V3(StartIo, CancelIo, PrepIo, EpEvent, \
		UsbEpswTimeoutStub \
		)

#define	__TMS_UENDPOINTSWITCH_INIT_V3(StartIo, CancelIo, PrepIo, EpEvent, Timeout) \
	__TMS_UENDPOINTSWITCH_INIT_V4(			\
		StartIo,				\
		CancelIo,				\
		PrepIo,					\
		EpEvent,				\
		Timeout,				\
		UsbPumpEndpointSwitch_QeuryStatusStub	\
		)

#define	__TMS_UENDPOINTSWITCH_INIT_V4(	\
	uEndpointSwitch_StartIo,	\
	uEndpoitnSwitch_CancelIo,	\
	uEndpoitnSwitch_PrepIo,		\
	uEndpoitnSwitch_EpEvent,	\
	uEndpoitnSwitch_Timeout,	\
	uEndpointSwtich_QueryStatus	\
	)				\
	{				\
	(uEndpointSwitch_StartIo),	\
	(uEndpoitnSwitch_CancelIo),	\
	(uEndpoitnSwitch_PrepIo),	\
	(uEndpoitnSwitch_EpEvent),	\
	(uEndpoitnSwitch_Timeout),	\
	(uEndpointSwtich_QueryStatus)	\
	}

/*

Name:	UENDPOINT_CANPUTSIMPLE()

Function:
	Macro predicate:  returns true if a given QE can be put directly as
	a single packet.

Definition:
	BOOL UENDPOINT_CANPUTSIMPLE(
		UENDPOINT *pep, 
		UBUFQE *pqe,
		BYTES MaxPacketSize,
		BYTES nAvail
		);

Description:
	pqe is the UBUFQE that is about to be written to the endpoint pep.
	wMaxPacketsize is the current packet size for that endpoint; nAvail
	is the number of bytes in that UBUFQE that haven't yet been written.

Returns:
	TRUE if and only if this UBUFQE can really be written to the 
	fifo and committed as a packet (even if it's short).

Notes:
	This macro takes into account the state of the pending queue, the
	postbreak flags of the current pqe and the prebreak and stall flags
	of the head of the pending queue.

*/

/*
|| This macro tells us whether we can put this qe in a single operation.
*/
#define	__TMS_UENDPOINT_CANPUTSIMPLE(pep, pqe, pktsize, navail) \
	((pktsize) <= (navail) || \
	 (pep)->uep_pending == __TMS_NULL || \
	 ((pqe)->uqe_flags & __TMS_UBUFQEFLAG_POSTBREAK) || \
	 ((pep)->uep_pending->uqe_flags & \
	  (__TMS_UBUFQEFLAG_PREBREAK | __TMS_UBUFQEFLAG_STALL)))

/*

Name:	UENDPOINT_COUNT_PENDING_BYTES()

Function:
	For non-simple packet writes, calculate the packet size and last
	UBUFQE.

Definition:
	VOID UENDPOINT_COUNT_PENDING_BYTES(
		IN CONST UENDPOINT *pep,
		IN CONST BYTES MaxPacketSize,
		IN OUT BYTES *pNavail,
		OUT UENDPOINT **pplastqe
		);

Description:
	On the assumption that UENDPOINT_CANPUTSIMPLE is false, and that
	*pNavail initially holds the number of bytes that will be written
	to the packet from the current UBUFQE, UENDPOINT_COUNT_PENDING_BYTES()
	looks ahead in the pending queue until either (1) UBUFQEs that would
	get the packet size to MaxPacketSize have been seen, (2) the end
	of the pending list is encountered; (3) a UBUFQE is encountered with
	flags that require ending the packet with a short packet.

Returns:
	No explicit result.  (*pNavail) is increased to the actual packet
	size, and *pplastqe is set to point to the last ubufqe in the pending
	list that should be processed into the current packet. 

Notes:
	Assumes that UENDPOINT_CANPUTSIMPLE() has been tested and is in
	fact FALSE.

*/

/*
|| This macro scans through the pending list, and tries to figure out how to
|| assemble fragments into a single packet, assuming that navail is the number
|| of bytes that can be put from the current packet, and that CANPUTSIMPLE
|| was false.
*/
#define	__TMS_UENDPOINT_COUNT_PENDING_BYTES(pep, pktsize, pnavail, pplastqe) \
    do	{ \
	__TMS_PUBUFQE __pthisqe; \
	__TMS_PUBUFQE __pnextqe; \
	\
	__pthisqe = __pnextqe = (pep)->uep_pending; \
	while (__pnextqe && *(pnavail) < (pktsize)) \
		{ \
		\
		__pthisqe   = __pnextqe; \
		*(pnavail) += __pthisqe->uqe_bufsize; \
		if ((__pthisqe->uqe_flags & __TMS_UBUFQEFLAG_POSTBREAK) || \
		    (__pnextqe = __pthisqe->uqe_next) == (pep)->uep_pending || \
		    (__pnextqe->uqe_flags & \
		     (__TMS_UBUFQEFLAG_PREBREAK | __TMS_UBUFQEFLAG_STALL))) \
			break; \
		} \
	\
	if (*(pnavail) > (pktsize)) \
		{ \
		*(pnavail) = (pktsize); \
		} \
	*(pplastqe) = __pthisqe; \
	} while (0)

/*

Name:	UENDPOINT_AUTO_REMOTE_WAKEUP_OK()

Function:
	Return the endpoint's eligibility to issue auto remote wakeup.

Definition:
	BOOL UENDPOINT_AUTO_REMOTE_WAKEUP_OK(
		IN CONST UENDPOINT *pUep,
		IN BOOL	fTrueForExamine
		);

Description:
	Take fTrueForExamine as the feature bit for backwards compatibility,
	If fTrueForExamine is FALSE, bypass the endpoint validation and return
	TRUE.
	If fTrueForExamine is TRUE, examine the endpoint's eligility of auto
	remote wakeup.  Currently BULK/INT IN pipes are eligible to issue auto
	remote wakeup.

Returns:
	TRUE if this endpoint is eligible to issue auto remote wakeup.
	FLASE otherwise.

Notes:
	Don't use this macro. Use UENDPOINT_CAN_AUTO_REMOTE_WAKEUP() or
	UENDPOINT_CHECK_AUTO_REMOTE_WAKEUP().

*/

#define	__TMS_UENDPOINT_AUTO_REMOTE_WAKEUP_OK(pUep, fTrueForExamine)	\
	(! (fTrueForExamine) ||						\
	((pUep)->uep_pPipe != __TMS_NULL &&				\
	(pUep)->uep_fActive &&						\
	(__TMS_UPIPE_SETTING_MASK((pUep)->uep_pPipe->upipe_bmAttributes,\
				  (pUep)->uep_pPipe->upipe_bEndpointAddress) \
			& __TMS_UPIPE_SETTING_MASK_BULKINT_IN)))

/*

Name:	UENDPOINT_CAN_AUTO_REMOTE_WAKEUP()

Function:
	Return the endpoint's eligibility to issue auto remote wakeup.

Definition:
	BOOL UENDPOINT_CAN_AUTO_REMOTE_WAKEUP(
		IN CONST UENDPOINT *pUep
		);

Description:
	This macro examines the endpoint's eligility of auto remote wakeup.
	Currently BULK/INT IN pipes are eligible to issue auto remote wakeup.

Returns:
	TRUE if this endpoint is eligible to issue auto remote wakeup.
	FLASE otherwise.

*/

#define	__TMS_UENDPOINT_CAN_AUTO_REMOTE_WAKEUP(pUep)		\
	((pUep)->uep_pPipe != __TMS_NULL &&			\
	 (pUep)->uep_fActive &&					\
	 (__TMS_UPIPE_GET_SETTING_MASK((pUep)->uep_pPipe) &	\
	  __TMS_UPIPE_SETTING_MASK_BULKINT_IN))

/*

Name:	UENDPOINT_CHECK_AUTO_REMOTE_WAKEUP()

Function:
	Check the endpoint can issue auto remote wakeup.

Definition:
	BOOL UENDPOINT_CHECK_AUTO_REMOTE_WAKEUP(
		IN CONST UENDPOINT *pUep,
		IN CONST UDEVICE *pDevice
		);

Description:
	This macro checks the endpoint need to  auto remote wakeup.
	Currently BULK/INT IN pipes are eligible to issue auto remote wakeup.

Returns:
	TRUE if this endpoint is eligible to issue auto remote wakeup.
	FLASE otherwise.

*/

#define	__TMS_UENDPOINT_CHECK_AUTO_REMOTE_WAKEUP(pUep, pDevice)	\
	(! __TMS_UDEVICE_IS_L0_STATE(pDevice) &&		\
	 /* ! __TMS_UDEVICE_IS_U0_STATE(pDevice) && */		\
	 (pDevice)->udev_fValidateEpforAutoRemoteWakeup &&	\
	 __TMS_UENDPOINT_CAN_AUTO_REMOTE_WAKEUP(pUep))


/****************************************************************************\
|
|	Uncloaked names
|
\****************************************************************************/

/**** uncloaked names generated by uncloak-defs.sh ****/
#if !__TMS_CLOAKED_NAMES_ONLY
# define UENDPOINT_HDR	\
   __TMS_UENDPOINT_HDR
# define USTARTIO(pDev, pEp)	\
   __TMS_USTARTIO(pDev, pEp)
# define UCANCELIO(pDev, pEp, why)	\
   __TMS_UCANCELIO(pDev, pEp, why)
# define UPREPIO(pDev, pEp, pQe)	\
   __TMS_UPREPIO(pDev, pEp, pQe)
# define UEPEVENT(pDev, pEp)	\
   __TMS_UEPEVENT(pDev, pEp)
# define UTIMEOUT(pDev, pEp)	\
   __TMS_UTIMEOUT(pDev, pEp)
# define UENDPOINTSWITCH_QUERYSTATUS(pDev, pEp)	\
   __TMS_UENDPOINTSWITCH_QUERYSTATUS(pDev, pEp)
# define UENDPOINTSWITCH_INIT_V1(StartIo, CancelIo, PrepIo)	\
   __TMS_UENDPOINTSWITCH_INIT_V1(StartIo, CancelIo, PrepIo)
# define UENDPOINTSWITCH_INIT_V2(StartIo, CancelIo, PrepIo, EpEvent)	\
   __TMS_UENDPOINTSWITCH_INIT_V2(StartIo, CancelIo, PrepIo, EpEvent)
# define UENDPOINTSWITCH_INIT_V3(StartIo, CancelIo, PrepIo, EpEvent, Timeout)	\
   __TMS_UENDPOINTSWITCH_INIT_V3(StartIo, CancelIo, PrepIo, EpEvent, Timeout)
# define UENDPOINTSWITCH_INIT_V4(	\
	uEndpointSwitch_StartIo,	\
	uEndpoitnSwitch_CancelIo,	\
	uEndpoitnSwitch_PrepIo,		\
	uEndpoitnSwitch_EpEvent,	\
	uEndpoitnSwitch_Timeout,	\
	uEndpointSwtich_QueryStatus	\
	)	\
	__TMS_UENDPOINTSWITCH_INIT_V4(	\
	uEndpointSwitch_StartIo,	\
	uEndpoitnSwitch_CancelIo,	\
	uEndpoitnSwitch_PrepIo,		\
	uEndpoitnSwitch_EpEvent,	\
	uEndpoitnSwitch_Timeout,	\
	uEndpointSwtich_QueryStatus	\
	)
# define UENDPOINT_CANPUTSIMPLE(pep, pqe, pktsize, navail)	\
   __TMS_UENDPOINT_CANPUTSIMPLE(pep, pqe, pktsize, navail)
# define UENDPOINT_COUNT_PENDING_BYTES(pep, pktsize, pnavail, pplastqe)	\
   __TMS_UENDPOINT_COUNT_PENDING_BYTES(pep, pktsize, pnavail, pplastqe)
# define UENDPOINT_AUTO_REMOTE_WAKEUP_OK(pUep, fTrueForExamine)	\
   __TMS_UENDPOINT_AUTO_REMOTE_WAKEUP_OK(pUep, fTrueForExamine)
# define UENDPOINT_CAN_AUTO_REMOTE_WAKEUP(pUep)	\
   __TMS_UENDPOINT_CAN_AUTO_REMOTE_WAKEUP(pUep)
# define UENDPOINT_CHECK_AUTO_REMOTE_WAKEUP(pUep, pDevice)	\
   __TMS_UENDPOINT_CHECK_AUTO_REMOTE_WAKEUP(pUep, pDevice)
#endif /* !__TMS_CLOAKED_NAMES_ONLY */

/**** end of uendpoint.h ****/
#endif /* _UENDPOINT_H_ */
