/* usbmsc10.h	Fri Jan 22 2010 11:40:33 ctpeng */

/*

Module:  usbmsc10.h

Function:
	USB-IF Mass Storage Class - Bulk Only Transport protocol 
	constants and definitions.

Version:
	V3.01a	Fri Jan 22 2010 11:40:33 ctpeng	Edit level 5

Copyright notice:
	This file copyright (C) 2001-2002, 2009-2010 by

		MCCI Corporation
		3520 Krums Corners Road
		Ithaca, NY  14850

	An unpublished work.  All rights reserved.
	
	This file is proprietary information, and may not be disclosed or
	copied without the prior permission of MCCI Corporation 
 
Author:
	Chris Sawran, MCCI Corporation	July 2001

Revision history:
   1.00a  Fri Jul  6 2001 00:00:01  cds
	Module created.

   1.79b  Fri Oct 4 2002 15:05:00  maw
   	Renamed from proto/usbmass/i/usbmass.h to i/usbmsc10.h
  
   2.01a  Mon Mar 09 2009 16:02:55 ctpeng
   	8265: Add USB_bInterfaceSubClass_MassStorageUasp and
   	USB_Uasp_Descriptor_PipeUsage
  
   2.01a  Tue Dec 01 2009 14:31:32  chwon
	9357: Fixed doc-xml error in revision history. No code change.

   3.01a  Fri Jan 22 2010 11:40:33 ctpeng
   	9784: Remove USB_bInterfaceSubClass_MassStorageUasp and
	add USB_bInterfaceSubclass_ProtocolUasp

*/


#ifndef _USBMSC10_H_		/* prevent multiple includes */
#define _USBMSC10_H_

#ifndef _USBIF_H_
# include "usbif.h"
#endif

/****************************************************************************\
|
|	Class, subclass, and protocol codes.
|
\****************************************************************************/

/* mass storage class device */
#define	USB_bInterfaceClass_MassStorage			0x08

/* Mass Storage class interface constants */
#define	USB_bInterfaceSubClass_MassStorageRbc		0x01
#define	USB_bInterfaceSubClass_MassStorageATAPI		0x02
#define USB_bInterfaceSubClass_MassStorageQIC157	0x03
#define USB_bInterfaceSubClass_MassStorageUFI		0x04
#define USB_bInterfaceSubClass_MassStorageSff8070i	0x05
#define USB_bInterfaceSubClass_MassStorageScsi		0x06

/* A table initializer */
#define	USB_bInterfaceSubClass_MassStorage_INIT \
	{ USB_bInterfaceSubClass_MassStorageRbc, "Reduced Block Commands" }, 	\
	{ USB_bInterfaceSubClass_MassStorageATAPI, "Atapi/MMC-2 Commands" }, 	\
	{ USB_bInterfaceSubClass_MassStorageQIC157, "QIC157 Commands" }, 	\
	{ USB_bInterfaceSubClass_MassStorageUFI, "UFI Commands" }, 		\
	{ USB_bInterfaceSubClass_MassStorageSff8070i, "SFF8070i Commands" }, 	\
	{ USB_bInterfaceSubClass_MassStorageScsi, "SCSI Commands"}

/* Mass Storage class Protocol codes */
#define USB_bInterfaceSubclass_ProtocolCBI		0x00
#define USB_bInterfaceSubclass_ProtocolCB		0x01
#define USB_bInterfaceSubclass_ProtocolBulkOnly		0x50
#define USB_bInterfaceSubclass_ProtocolUasp		0x62

/* A table initializer */
#define	USB_bInterfaceProtocol_MassStorage_INIT \
	{ USB_bInterfaceSubclass_ProtocolCBI, "Control-Bulk-Interrupt Transport" }, 	\
	{ USB_bInterfaceSubclass_ProtocolCB, "Control-Bulk (No Interrupt) Transport" }, \
	{ USB_bInterfaceSubclass_ProtocolBulkOnly, "Bulk-Only Transport" },		\
	{ USB_bInterfaceSubclass_ProtocolUasp, "UASP Transport" }

/* Bulk-only class-specific requests */
#define USB_bRequest_MassStorage_BULK_ONLY_RESET	0xFF
#define USB_bRequest_MassStorage_BULK_ONLY_GET_MAX_LUN	0xFE

/* A table initializer */
#define	USB_bRequest_MassStorage__INIT \
	{ USB_bRequest_MassStorage_BULK_ONLY_RESET, "BULK_ONLY_RESET" }, \
	{ USB_bRequest_MassStorage_BULK_ONLY_GET_MAX_LUN, "BULK_ONLY_GET_MAX_LUN" }

/****************************************************************************\
|
|	Uasp Class-Specific Pipe Usage Descriptor
|
\****************************************************************************/

struct USB_Uasp_Descriptor_PipeUsage
	{
	unsigned char	bFunctionLength;
	unsigned char	bDescriptorType;
	unsigned char	bPipeID;
	unsigned char	bReserved;
	};

#define USB_bDescriptorType_Uasp_PIPE_USAGE	0x24
#define USB_bDescriptorType_Uasp_PIPE_USAGE_COMMAND	0x01
#define USB_bDescriptorType_Uasp_PIPE_USAGE_STATUS	0x02
#define USB_bDescriptorType_Uasp_PIPE_USAGE_DATAIN	0x03
#define USB_bDescriptorType_Uasp_PIPE_USAGE_DATAOUT	0x04

/****************************************************************************\
|
|	Definitions of Mass Storage Class SETUP commands.
|	Bits in various SETUP commands
|
\****************************************************************************/

/*
|| BULK_ONLY_MASS_STORAGE_RESET
||
|+-------------+----------------------------+------------------+------------+---------------+--------------+
||bmRequestType|         bmRequest          |      wValue      |   wIndex   |    wLength    |     Data     |
|+-------------+----------------------------+------------------+------------+---------------+--------------+
||  00100001B  |BULK_ONLY_MASS_STORAGE_RESET|       zero       |  Interface |      zero     |     None     |
|+-------------+----------------------------+------------------+------------+---------------+--------------+
||
|| This request is used to reset the mass storage device and its associated interface
||
|| This class-specific request shall ready the device for the next CBW from the host
||
|| The device shall NAK the status stage of the device request until the reset
|| is complete.
||
+=========================================================================================================+
*/
/*
|| GET_MAX_LUN
||
|+-------------+----------------------------+------------------+------------+---------------+--------------+
||bmRequestType|         bmRequest          |      wValue      |   wIndex   |    wLength    |     Data     |
|+-------------+----------------------------+------------------+------------+---------------+--------------+
||  00100001B  |GET_MAX_LUN					|       zero       |  Interface |    0x0001     |  Max Lun Idx |
|+-------------+----------------------------+------------------+------------+---------------+--------------+
||
|| This command shall return 1 byte which contains the maximum LUN supported by the device, if LUNs are
|| supported by the device at all.  If no LUN is associated with the device, the value shall be 0.
|| If multiple luns are not supported, the device may STALL the command.
||
+=========================================================================================================+
*/



/**** end of usbmsc10.h ****/
#endif /* _USBMSC10_H_ */
