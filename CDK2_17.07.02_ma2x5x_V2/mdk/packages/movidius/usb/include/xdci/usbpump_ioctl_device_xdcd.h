/* usbpump_ioctl_device_xdcd.h	Thu Jul 10 2014 16:55:25 tmm */

/*

Module:  usbpump_ioctl_device_xdcd.h

Function:
	XDCD-specific IOCTLs.

Version:
	V3.13a	Thu Jul 10 2014 16:55:25 tmm	Edit level 1

Copyright notice:
	This file copyright (C) 2014 by

		MCCI Corporation
		3520 Krums Corners Road
		Ithaca, NY  14850

	An unpublished work.  All rights reserved.
	
	This file is proprietary information, and may not be disclosed or
	copied without the prior permission of MCCI Corporation.
 
Author:
	Terry Moore, MCCI Corporation	July 2014

Revision history:
   3.13a  Thu Jul 10 2014 16:55:25  tmm
	18309: Module created.

*/

#ifndef _USBPUMP_IOCTL_DEVICE_XDCD_H_		/* prevent multiple includes */
#define _USBPUMP_IOCTL_DEVICE_XDCD_H_

#ifndef _USBIOCTL_DEVICE_H_
# include "usbioctl_device.h"
#endif

/****************************************************************************\
|
|	XDCD specific device IOCTL definition
|
\****************************************************************************/

#define	__TMS_USBPUMP_XDCD_IOCTL_BASE				\
	__TMS_USBPUMP_IOCTL_DEVICE_IFC_BASE

#define	__TMS_USBPUMP_IOCTL_DEVICE_XDCD_REGISTER_SOF_CLIENT	\
	__TMS_USBPUMP_IOCTL_DEVICE(					\
		__TMS_USBPUMP_XDCD_IOCTL_BASE, W, XDCD_REGISTER_SOF_CLIENT)

/*

IOCTL:	USBPUMP_IOCTL_DEVICE_XDCD_REGISTER_SOF_CLIENT

Index:	Type:	USBPUMP_IOCTL_DEVICE_XDCD_REGISTER_SOF_CLIENT_ARG
	Name:	USBPUMP_IOCTL_DEVICE_XDCD_REGISTER_SOF_CLIENT_ARG_SETUP_V1

Function:
	IOCTL issued by USB chip interface to register SOF client callback 
	function.

Input:
	pInParam	points to a 
			USBPUMP_IOCTL_DEVICE_XDCD_REGISTER_SOF_CLIENT_ARG
			structure.

	pOutParam	None.

	USBPUMP_IOCTL_DEVICE_XDCD_REGISTER_SOF_CLIENT_ARG has
	the following elements:

	UCALLBACKCOMPLETION *	pCallback;
		IN: points to the callback completion structure.

	BOOL		fMicroFrameCallback;
		IN: TRUE if DCD calls callback on every micro-frame event.

Description:
	The client should send this IOCTL to register the callback function to
	recieve SOF event. The client should pass NULL to unregister the
	callback when SOF event is no longer needed.

Setup Macro:
	VOID USBPUMP_IOCTL_DEVICE_XDCD_REGISTER_SOF_CLIENT_ARG_SETUP_V1(
		USBPUMP_IOCTL_DEVICE_XDCD_REGISTER_SOF_CLIENT_ARG *pArg,
			UCALLBACKCOMPLETION *	pCallback,
			BOOL			fMicroFrameCallback
			);

*/

__TMS_TYPE_DEF_STRUCT(USBPUMP_IOCTL_DEVICE_XDCD_REGISTER_SOF_CLIENT_ARG);
struct __TMS_STRUCTNAME(USBPUMP_IOCTL_DEVICE_XDCD_REGISTER_SOF_CLIENT_ARG)
	{
	__TMS_UCALLBACKCOMPLETION *		pCallback;
	__TMS_BOOL				fMicroFrameCallback;
	};

#define __TMS_USBPUMP_IOCTL_DEVICE_XDCD_REGISTER_SOF_CLIENT_ARG_SETUP_V1( \
	pArg,								\
	ARG_pCallback,							\
	ARG_fMicroFrameCallback						\
	)								\
   do	{								\
	__TMS_USBPUMP_IOCTL_DEVICE_XDCD_REGISTER_SOF_CLIENT_ARG * __TMS_CONST \
		__pArg = (pArg);					\
	__pArg->pCallback = (ARG_pCallback);				\
	__pArg->fMicroFrameCallback = (ARG_fMicroFrameCallback);	\
   	} while (0)

/****************************************************************************\
|
|	Uncloaked types
|
\****************************************************************************/

/**** uncloaked names generated by uncloak-defs.sh ****/
#if !__TMS_CLOAKED_NAMES_ONLY
# define USBPUMP_XDCD_IOCTL_BASE	\
   __TMS_USBPUMP_XDCD_IOCTL_BASE
# define USBPUMP_IOCTL_DEVICE_XDCD_REGISTER_SOF_CLIENT	\
   __TMS_USBPUMP_IOCTL_DEVICE_XDCD_REGISTER_SOF_CLIENT
# define USBPUMP_IOCTL_DEVICE_XDCD_REGISTER_SOF_CLIENT_ARG_SETUP_V1( \
	pArg,								\
	ARG_pCallback,							\
	ARG_fMicroFrameCallback						\
	)	\
	__TMS_USBPUMP_IOCTL_DEVICE_XDCD_REGISTER_SOF_CLIENT_ARG_SETUP_V1( \
	pArg,								\
	ARG_pCallback,							\
	ARG_fMicroFrameCallback						\
	)
#endif /* !__TMS_CLOAKED_NAMES_ONLY */


/**** end of usbpump_ioctl_device_xdcd.h ****/
#endif /* _USBPUMP_IOCTL_DEVICE_XDCD_H_ */
