///
/// @file
/// @copyright All code copyright Movidius Ltd 2012, all rights reserved.
///            For License Warranty see: common/license.txt
///

#include "matmul_partitioner.h"

namespace matmul
{

const unsigned Partitioner::DEPTH = 10;
const unsigned Partitioner::MAX_DIVISORS = 512;
const float Partitioner::MAX_WASTE = 0.1;

PartitionerParams::PartitionerParams()
    : m_(0)
    , k_(0)
    , n_(0)
    , s_(0)
    , major_(0)
    , minor_(0)
    , major_len_(0)
    , minor_len_(0)
    , padm_(0)
    , padk_(0)
    , padn_(0)
{
}

Partitioner::Partitioner(unsigned cmx_items, unsigned kernel_width, MatMulAllocator& alloc)
    : cmx_floats_(cmx_items)
    , kernel_width_(kernel_width)
    , major_(0)
    , minor_(0)
    , major_len_(0)
    , minor_len_(0)
    , m_(0)
    , k_(0)
    , n_(0)
    , s_(0)
    , padm_(0)
    , padk_(0)
    , padn_(0)
    , alloc_(alloc)
{
}

Partitioner::~Partitioner()
{
}

bool Partitioner::partition(PartitionerParams& p)
{
    //TODO: MATMUL_PROFILER_START;

    m_ = p.m_;
    k_ = p.k_;
    n_ = p.n_;
    s_ = p.s_;

    m_ % kernel_width_ ? padm_ = kernel_width_ - m_ % kernel_width_ : padm_ = 0;
    k_ % kernel_width_ ? padk_ = kernel_width_ - k_ % kernel_width_ : padk_ = 0;
    n_ % kernel_width_ ? padn_ = kernel_width_ - n_ % kernel_width_ : padn_ = 0;

    if (!is_valid_k()) return false;

    Vector<unsigned> candm(alloc_);
    candm.reserve(DEPTH);
    Vector<unsigned> candn(alloc_);
    candn.reserve(DEPTH);
    Vector<unsigned> divm(alloc_);
    divm.reserve(MAX_DIVISORS);
    Vector<unsigned> divn(alloc_);
    divn.reserve(MAX_DIVISORS);

    for (unsigned idx = 0; idx < DEPTH; idx++)
    {
        unsigned cm = m_ + padm_ + idx * kernel_width_;
        float waste = 1 - (float) m_ / cm;
        if ((waste < MAX_WASTE) or (candm.size() == 0))
            candm.push_back(cm);
        unsigned cn = n_ + padn_ + idx * kernel_width_;
        waste = 1 - (float) n_ / cn;
        if ((waste < MAX_WASTE) or (candn.size() == 0))
            candn.push_back(cn);
    }

    bool solution_found = false;

    for (unsigned indcm = 0; indcm < candm.size(); indcm++)
    {
        unsigned cm = candm[indcm];
        find_divisors(cm, kernel_width_, divm);
        for (unsigned inddm = 0; inddm < divm.size(); inddm++)
        {
            unsigned dm = divm[inddm];
            for (unsigned indcn = 0; indcn < candn.size(); indcn++)
            {
                unsigned cn = candn[indcn];
                find_divisors(cn, kernel_width_, divn);
                for (unsigned inddn = 0; inddn < divn.size(); inddn++)
                {
                    unsigned dn = divn[inddn];
                    unsigned cmx_used = cmx_usage(k_ + padk_, dm, dn);
                    if ((cmx_used <= cmx_floats_) && (is_better_solution(dm, dn)))
                    {
                        solution_found = true;
                        padm_ = cm - m_;
                        unsigned paddedm = padm_ + m_;
                        minor_ = paddedm / dm;
                        minor_len_ = paddedm / minor_;

                        padn_ = cn - n_;
                        unsigned paddedn = padn_ + n_;
                        major_ = paddedn / dn;
                        major_len_ = paddedn / major_;
                    }
                }
            }
        }
    }

    p.major_ = major_;
    p.minor_ = minor_;
    p.major_len_ = major_len_;
    p.minor_len_ = minor_len_;
    p.padm_ = padm_;
    p.padk_ = padk_;
    p.padn_ = padn_;

    //TODO: MATMUL_PROFILER_STOP;

    return solution_found;

}

bool Partitioner::is_valid_k()
{
    bool ret = true;
    unsigned cmx_used = cmx_usage(k_ + padk_, kernel_width_, kernel_width_);
    if (cmx_used > cmx_floats_) ret = false;
    return ret;
}

void Partitioner::find_divisors(unsigned n, unsigned div, Vector<unsigned>& divisors)
{
    divisors.clear();
    unsigned cur_div = n;
    while (cur_div > 0)
    {
        if (n % cur_div == 0)
            divisors.push_back(cur_div);
        cur_div -= div;
    }
}

unsigned Partitioner::cmx_usage(unsigned k, unsigned minor_len, unsigned major_len)
{
    return (2 * k * minor_len) + (2 * k * major_len) + (3 * minor_len * major_len);
}

bool Partitioner::is_better_solution(unsigned minor_len, unsigned major_len)
{
    unsigned C_block_old = minor_len_ * major_len_;
    unsigned C_block_new = minor_len * major_len;

    unsigned major_new = 0;
    unsigned acu = 0;
    while (acu < n_)
    {
        acu += major_len;
        major_new++;
    }

    bool ret = false;
    if ((s_ == 1) || (s_ == 2))
    {
        if (C_block_new > C_block_old) ret = true;
    }
    else
    {
        if (C_block_new > C_block_old)
        {
            if ((major_new < s_) && (s_ % major_new == 0)) ret = true;
            else if ((major_new % s_ == 0) && (major_ % s_ == 0) && (major_new < major_)) ret = true;
            else if ((major_new % s_ == 0) && (major_ % s_ != 0)) ret = true;

            if ((s_ - (major_new % s_)) <= (s_ - (major_ % s_))) ret = true;
        }
    }

    return ret;
}

} /* namespace matmul */

