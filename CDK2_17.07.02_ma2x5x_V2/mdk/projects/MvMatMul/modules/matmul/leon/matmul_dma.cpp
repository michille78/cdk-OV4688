///
/// @file
/// @copyright All code copyright Movidius Ltd 2012, all rights reserved.
///            For License Warranty see: common/license.txt
///

#include <stdio.h>
#include <string.h>
#include <assert.h>

#include "matmul_dma.h"

namespace matmul
{

static dmaTransactionList_t __attribute__((section(".cmx.cdmaDescriptors"))) sTasks[DmaChannel::MAX_INSTANCES];
int __attribute__((section(".cmx.cdmaDescriptors"))) DmaChannel::instances_ = 0;

DmaChannel::DmaChannel(int priority, unsigned int dma_link_agent)
    : m_reqId(0)
    , tasks(NULL) //this is just an init value, storing the reference and incrementing the instance count must be atomic (see below)
    , refs(0)
{
    #if defined(__RTEMS__)
    (void) dma_link_agent;
    OsDrvCmxDmaInitDefault();
    OsDrvCmxDmaInitRequester(priority, &m_reqId);
    u32 pil;
    rtems_interrupt_disable(pil);
    tasks = &sTasks[instances_]; //this must be done here, otherwise two threads can assign the reference to the same task.
    instances_++;
    if(instances_ == DmaChannel::MAX_INSTANCES)
        instances_ = 0;
    rtems_interrupt_enable(pil);
    assert(instances_ >= 0 && instances_ <= MAX_INSTANCES);
    #else
    swcLeonSetPIL(0);
    tasks = &sTasks[instances_];
    DrvCmxDmaInitDefault();
    m_reqId = DrvCmxDmaInitRequesterOnAgent(priority, dma_link_agent);
    #endif
}

DmaChannel::~DmaChannel()
{
    #if defined(__RTEMS__)
    u32 pil;
    rtems_interrupt_disable(pil);
//    instances_--;
    rtems_interrupt_enable(pil);
    assert(instances_ >= 0 && instances_ <= MAX_INSTANCES);
    #endif
}

void DmaChannel::copy(unsigned char* dst, unsigned char* src, int bytes)
{
    #if defined(__RTEMS__)
    OsDrvCmxDmaCreateTransaction(m_reqId, tasks, src, dst, bytes);
    int ret = OsDrvCmxDmaStartListTask(tasks, &ret);
    assert(ret==0);
    #else
    refs = DrvCmxDmaCreateTransaction(m_reqId, tasks, src, dst, bytes);
    DrvCmxDmaStartListTask(refs);
    #endif
}

void DmaChannel::wait()
{
    #if defined(__RTEMS__)
    OsDrvCmxDmaWaitTask(tasks);
    #else
    DrvCmxDmaWaitTask(refs);
    #endif
}

} /* namespace matmul */

