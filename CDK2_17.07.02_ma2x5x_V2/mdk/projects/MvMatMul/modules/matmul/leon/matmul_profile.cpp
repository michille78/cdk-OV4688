///
/// @file
/// @copyright All code copyright Movidius Ltd 2012, all rights reserved.
///            For License Warranty see: common/license.txt
///

#include <stdio.h>
#include <string>
#include <assert.h>
#include "matmul_profile.h"

namespace matmul
{
MatMulProfiler* MatMulProfiler::m_instance = 0;

MatMulProfiler& MatMulProfiler::instance()
{
    static MatMulProfiler m_instance;
    return m_instance;
}

MatMulProfiler::MatMulProfiler()
    : m_stats()
{
    init();
}

MatMulProfiler::~MatMulProfiler()
{
}

void MatMulProfiler::start(const char* str)
{
    u64 t0 = get_ticks();

    std::string s(str);

    stats_t stat = m_stats[s];
    stat.t0 = t0;
    m_stats[s] = stat;
}

void MatMulProfiler::stop(const char* str)
{
    u64 t1 = get_ticks();

    std::string s(str);

    stats_t stat = m_stats[s];
    stat.t1 = t1;
    stat.cycles = stat.t1 - stat.t0;
    #if defined(__RTEMS__)
    stat.ms = 0;
    #else
    stat.ms = DrvTimerTicksToMs(stat.cycles);
    #endif
    m_stats[s] = stat;
}

void MatMulProfiler::log()
{
    for(map_t::iterator iter = m_stats.begin(); iter != m_stats.end(); ++iter)
        printf("%s.cycles = %llu, %s.msec = %0.2f\n",
                iter->first.c_str(),
                iter->second.cycles,
                iter->first.c_str(),
                iter->second.ms);
}

} /* namespace matmul */

