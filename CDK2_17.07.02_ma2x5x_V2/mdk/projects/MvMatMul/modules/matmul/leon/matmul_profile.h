///
/// @file
/// @copyright All code copyright Movidius Ltd 2012, all rights reserved.
///            For License Warranty see: common/license.txt
///

#ifndef MATMUL_PROFILE_H_
#define MATMUL_PROFILE_H_

#include <string>
#include <map>
#include <cassert>
#if defined(__RTEMS__)
#include <OsDrvTimer.h>
#else
#include <DrvTimer.h>
#endif

#ifdef __cplusplus

#ifdef MATMUL_PROFILE
    #define MATMUL_PROFILER_START matmul::MatMulProfiler::instance().start(__FUNCTION__)
    #define MATMUL_PROFILER_STOP  matmul::MatMulProfiler::instance().stop(__FUNCTION__)
    #define MATMUL_PROFILER_LOG   matmul::MatMulProfiler::instance().log()
#else
    #define MATMUL_PROFILER_START
    #define MATMUL_PROFILER_STOP
    #define MATMUL_PROFILER_LOG
#endif


namespace matmul
{

class MatMulProfiler
{
    struct stats_t
    {
        stats_t() : t0(0), t1(0), cycles(0), ms(0.0) {};

        u64 t0;
        u64 t1;
        u64 cycles;
        double ms;
    };

    typedef std::map<std::string, stats_t> map_t;

public:
    static MatMulProfiler& instance();
    ~MatMulProfiler();

    void start(const char* str = 0);
    void stop(const char* str = 0);
    void log();

    inline static void init()
    {
        #if defined(__RTEMS__)
        assert(OsDrvTimerInit() != 0);
        #else
        assert(DrvTimerInit() != 0);
        #endif
    }

    inline static uint64_t get_ticks()
    {
        uint64_t t0;
        #if defined(__RTEMS__)
        OsDrvTimerGetSystemTicks64(&t0);
        #else
        DrvTimerGetSystemTicks64(&t0);
        #endif
        return t0;
    }

private:
    MatMulProfiler();
    static MatMulProfiler* m_instance;

    map_t m_stats;
};

} /* namespace matmul */

#endif /* __cplusplus */

#endif // MVGEMM_H_


