///
/// @file
/// @copyright All code copyright Movidius Ltd 2012, all rights reserved.
///            For License Warranty see: common/license.txt
///

#include "matmul_profile.h"
#include "matmul_trace.h"
#include "matmul_leon.h"

namespace matmul
{

void MatMulTrace::init()
{
    MatMulProfiler::init();
}

uint64_t MatMulTrace::profile_start()
{
    stats_.cycles = MatMulProfiler::get_ticks();
    return stats_.cycles;
}

uint64_t MatMulTrace::profile_stop()
{
    stats_.cycles = MatMulProfiler::get_ticks() - stats_.cycles;
    return stats_.cycles;
}

void MatMulTrace::collect(void* mm)
{
    MvMatMul<half>& mmi = (MvMatMul<half>&) *((MvMatMul<half>*) mm);
    stats_.M = mmi.m_;
    stats_.K = mmi.k_;
    stats_.N = mmi.n_;
    stats_.S = mmi.s_;
    stats_.C = mmi.calls_;
    stats_.S0 = mmi.cfg_.first_shave;
    stats_.major_slices = mmi.major_;
    stats_.minor_slices = mmi.minor_;
    stats_.major_slice_len = mmi.major_len_;
    stats_.minor_slice_len = mmi.minor_len_;
    stats_.A_slice_size = stats_.K * stats_.minor_slice_len * sizeof(half);
    stats_.B_slice_size = stats_.K * stats_.major_slice_len * sizeof(half);
    stats_.C_slice_size = stats_.major_slice_len * stats_.minor_slice_len * sizeof(half);

    MatMulTracePOD* pst = (MatMulTracePOD*) ((uint32_t) mmi.mms_.tracer_stats | 0x08000000);
    for (int i = 0; i < SHAVES_TOUSE_MAX; i++)
    {
        stats_.lsu_stalls[i] = pst->lsu_stalls[i];
        stats_.idc_stalls[i] = pst->idc_stalls[i];
        stats_.dma_transactions[i] = pst->dma_transactions[i];
    }
}

void MatMulTrace::log_short()
{
    printf("M                           = %u\n", (unsigned) stats_.M);
    printf("K                           = %u\n", (unsigned) stats_.K);
    printf("N                           = %u\n", (unsigned) stats_.N);
    printf("S                           = %u\n", (unsigned) stats_.S);
    printf("C                           = %u\n", (unsigned) stats_.C);
    printf("cycles                      = %u\n", (unsigned) stats_.cycles);
}

void MatMulTrace::log_extended()
{
    printf("S0                          = %u\n", (unsigned) stats_.S0);
    printf("major_slices                = %u\n", (unsigned) stats_.major_slices);
    printf("major_slice_len             = %u\n", (unsigned) stats_.major_slice_len);
    printf("minor_slices                = %u\n", (unsigned) stats_.minor_slices);
    printf("minor_slice_len             = %u\n", (unsigned) stats_.minor_slice_len);
    printf("A_slice_size                = %u\n", (unsigned) stats_.A_slice_size);
    printf("B_slice_size                = %u\n", (unsigned) stats_.B_slice_size);
    printf("C_slice_size                = %u\n", (unsigned) stats_.C_slice_size);

    unsigned int lsu_stalls = 0;
    unsigned int idc_stalls = 0;
    unsigned int dma_transactions = 0;
    for (unsigned int i = stats_.S0; i < stats_.S0 + stats_.S; i++)
    {
        if (stats_.lsu_stalls[i] > lsu_stalls)
            lsu_stalls = stats_.lsu_stalls[i];

        if (stats_.idc_stalls[i] > idc_stalls)
            idc_stalls = stats_.idc_stalls[i];

        dma_transactions += stats_.dma_transactions[i];
    }

    float lsup = (float)lsu_stalls / stats_.cycles * 100;
    printf("lsu_stalls (max)            = %u (%.1f%%)\n", (unsigned) lsu_stalls, lsup);
    //for (unsigned int i = stats_.S0; i < stats_.S0 + stats_.S; i++)
    //    printf("    SHV[%d]          = %u\n", i, (unsigned) stats_.lsu_stalls[i]);

    float idcp = (float)idc_stalls / stats_.cycles * 100;
    printf("idc_stalls (max)            = %u (%.1f%%)\n", (unsigned) idc_stalls, idcp);
    //for (unsigned int i = stats_.S0; i < stats_.S0 + stats_.S; i++)
    //    printf("    SHV[%d]          = %u\n", i, (unsigned) stats_.idc_stalls[i]);
    
    printf("dma_transactions (total)    = %u\n", (unsigned) dma_transactions);
    for (unsigned int i = stats_.S0; i < stats_.S0 + stats_.S; i++)
        printf("    SHV[%d]                  = %u\n", i, (unsigned) stats_.dma_transactions[i]);
}

} /* namespace matmul */

