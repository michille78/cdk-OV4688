///
/// @file
/// @copyright All code copyright Movidius Ltd 2012, all rights reserved.
///            For License Warranty see: common/license.txt
///

#ifndef _MATMUL_LEON_H_
#define _MATMUL_LEON_H_

#include "matmul.h"
#include "matmul_iface.h"
#include "matmul_pool.h"
#include "matmul_graph.h"
#include "matmul_trace.h"
#include "matmul_cache.h"

#ifdef __cplusplus

namespace matmul
{

struct MatMulConfig
{
    MatMulType matrix_type;
    kernel_t kernel_type;
    unsigned kernel_width;
    unsigned dma_link_agent;
    unsigned first_shave = 0;
    unsigned scratch_memory_size;
    char* scratch_memory;
    char* error_buffer;
};

template <typename T>
struct MvMatMul
{
    static const int LAST_SHAVE_MAX = 12;

    MvMatMul(MatMulConfig& cfg, MatMulCache& cache);
    virtual ~MvMatMul();

    uint64_t multiply(Tensor& A, Tensor& B, Tensor& C, int s, TensorList& loptions);
    uint64_t multiply(Tensor& A, Tensor& B, Tensor& C, int s, kernel_t kt = GEMM_HHHH_NNN);
    uint64_t multiply(int m, int k, int n, int s, T* pA, T* pB, T* pC,
            int strideA = 0, int strideB = 0, int strideC = 0, kernel_t kt = GEMM_HHHH_NNN);

    void enable_trace();
    void disable_trace();
    MatMulTrace& tracer();

    friend MatMulTrace;

private:

    int multiply_internal(Tensor& A, Tensor& B, Tensor& C, int s);

    void allocate_working_buffers();
    void setup_k_padding(unsigned padK);
    void run_shaves();

    MatMulConfig cfg_;
    int minor_;
    int minor_len_;
    int major_;
    int major_len_;
    int m_;
    int k_;
    int n_;
    int s_;
    unsigned int calls_;
    MatMulAllocator alloc_;
    MatMulPool tensor_pool_;
    MatMulPool command_pool_;
    MatMulGraph graph_;
    MatMulCache& cache_;
    MatMulSolution solution_;
    bool trace_;
    MatMulTrace* tracer_;
    MatMulSharePOD mms_;
    TensorList options_;
};

} /* namespace matmul */

#endif /* __cplusplus */

#endif // _MATMUL_LEON_H_

