///
/// @file
/// @copyright All code copyright Movidius Ltd 2012, all rights reserved.
///            For License Warranty see: common/license.txt
///

#include <string.h>
#include "matmul_cache.h"
#include "matmul_dma.h"
#include "matmul_lock.h"
#if defined(__RTEMS__)
    #include <pthread.h>
#endif

namespace matmul
{

MatMulSolution::MatMulSolution()
    : id_(0)
    , M(0)
    , K(0)
    , N(0)
    , S(0)
    , C(0)
    , major(0)
    , major_len(0)
    , minor(0)
    , minor_len(0)
    , padM(0)
    , padK(0)
    , padN(0)
    , strideA(0)
    , strideB(0)
    , strideC(0)
    , ptrA(0)
    , ptrB(0)
    , ptrC(0)
    , tensor_cnt(0)
    , command_cnt(0)
    , command_pool_cnt(0)
    , tensor_pool_size(0)
    , command_pool_size(0)
    , graph_pool_size(0)
    , pod_size_(sizeof(MatMulSolutionPOD))
    , pod_aligment_(1)
{
    for (int i = 0; i < SINGLE_CALLS_MAX; i++)
    {
        offsetA[i] = 0;
        offsetB[i] = 0;
        offsetC[i] = 0;
        kernel_type[i] = GEMM_HHHH_NNN;
    }
}

void MatMulSolution::serialize(void* ptr)
{
    MatMulSolutionPOD& out = *(MatMulSolutionPOD*) ptr;
    out.id = id_;
    out.M = M;
    out.K = K;
    out.N = N;
    out.S = S;
    out.C = C;
    out.major = major;
    out.major_len = major_len;
    out.minor = minor;
    out.minor_len = minor_len;
    out.padM = padM;
    out.padK = padK;
    out.padN = padN;
    out.strideA = strideA;
    out.strideB = strideB;
    out.strideC = strideC;
    out.ptrA = ptrA;
    out.ptrB = ptrB;
    out.ptrC = ptrC;
    out.tensor_cnt = tensor_cnt;
    out.command_cnt = command_cnt;
    out.command_pool_cnt = command_pool_cnt;
    out.tensor_pool_size = tensor_pool_size;
    out.command_pool_size = command_pool_size;
    out.graph_pool_size = graph_pool_size;
    for (unsigned i = 0; i < C; i++)
    {
        out.offsetA[i] = offsetA[i];
        out.offsetB[i] = offsetB[i];
        out.offsetC[i] = offsetC[i];
        out.kernel_type[i] = kernel_type[i];
    }

    matmul::DmaChannel dma(1, dma_link_agent_);

    u8* src = (u8*) tensor_pool_->pool_array();
    u8* dst = (u8*) out.data;
    dma.copy(dst, src, tensor_pool_size);
    dma.wait();

    src = (u8*) command_pool_->pool_array();
    dst += tensor_pool_size;
    dma.copy(dst, src, command_pool_size);
    dma.wait();

    src =  (u8*) graph_->array();
    dst += command_pool_size;
    dma.copy(dst, src, graph_pool_size);
    dma.wait();
}

void MatMulSolution::unserialize(void* ptr)
{
    MatMulSolutionPOD& in = *(MatMulSolutionPOD*) ptr;
    id_ = in.id;
    M = in.M;
    K = in.K;
    N = in.N;
    S = in.S;
    C = in.C;
    major = in.major;
    major_len = in.major_len;
    minor = in.minor;
    minor_len = in.minor_len;
    padM = in.padM;
    padK = in.padK;
    padN = in.padN;
    strideA = in.strideA;
    strideB = in.strideB;
    strideC = in.strideC;
    ptrA = in.ptrA;
    ptrB = in.ptrB;
    ptrC = in.ptrC;
    tensor_cnt = in.tensor_cnt;
    command_cnt = in.command_cnt;
    command_pool_cnt = in.command_pool_cnt;
    tensor_pool_size = in.tensor_pool_size;
    command_pool_size = in.command_pool_size;
    graph_pool_size = in.graph_pool_size;
    for (unsigned i = 0; i < C; i++)
    {
        offsetA[i] = in.offsetA[i];
        offsetB[i] = in.offsetB[i];
        offsetC[i] = in.offsetC[i];
        kernel_type[i] = in.kernel_type[i];
    }

    tensor_pool_->reserve<Tensor>(tensor_cnt);
    tensor_pool_->resize(tensor_cnt);
    command_pool_->reserve<MatMulCmd>(command_pool_cnt);
    command_pool_->resize(command_pool_cnt);
    graph_->reserve(major, minor, command_pool_cnt, S);

    matmul::DmaChannel dma(1, dma_link_agent_);

    u8* src = (u8*) in.data;
    u8* dst = (u8*) tensor_pool_->pool_array();
    dma.copy(dst, src, tensor_pool_size);
    dma.wait();

    src += tensor_pool_size;
    dst = (u8*) command_pool_->pool_array();
    dma.copy(dst, src, command_pool_size);
    dma.wait();

    src += command_pool_size;
    dst = (u8*) graph_->array();
    dma.copy(dst, src, graph_pool_size);
    dma.wait();
}

MatMulCache instance_arr[2];

MatMulCache& MatMulCache::instance()
{
    MatMulCache* instance_;
    #if defined(__RTEMS__)
    //TODO: revert to multithreaded singleton. Now hardcoded to 2 thread test
    if (pthread_self() % 2 == 0)
    {
        instance_ = &instance_arr[0];
    }
    else
    {
        instance_ = &instance_arr[1];
    }
    #else
    instance_ = &instance_arr[0];
    #endif
    return *instance_;
}

MatMulCache::MatMulCache()
    :  alloc_("cache allocator")
    , pool_(&alloc_)
    , is_configured_(false)
    , cache_memory_ptr_(0)
    , cache_memory_size_(0)
{
}

MatMulCache::~MatMulCache()
{
}

void MatMulCache::config(char* cache_memory_ptr, unsigned cache_memory_size)
{
    cache_memory_ptr_ = cache_memory_ptr;
    cache_memory_size_ = cache_memory_size;
    run_once();
}

void MatMulCache::run_once(void)
{
    if (!is_configured_)
    {
        config_internal(cache_memory_ptr_, cache_memory_size_);
        is_configured_ = true;
    }
}

void MatMulCache::config_internal(char* cache_memory_ptr, unsigned cache_memory_size)
{
    unsigned cache_cnt_est = cache_memory_size / sizeof(MatMulSolutionPOD);
    unsigned cache_pool_size_est = cache_cnt_est * (sizeof(MatMulSolutionPOD) + sizeof(unsigned long));
    while (cache_pool_size_est > cache_memory_size)
    {
        cache_cnt_est -= 1;
        cache_pool_size_est = cache_cnt_est * (sizeof(MatMulSolutionPOD) + sizeof(unsigned long));
    }
    alloc_.reset(cache_memory_ptr, cache_memory_size);
    pool_.reserve<MatMulSolution>(cache_cnt_est);
}

bool MatMulCache::fetch(MatMulSolution& sol, Tensor& A, Tensor& B, Tensor& C, unsigned S, TensorList& offsets)
{
    unsigned M = A.dim0();
    unsigned K = A.dim1();
    unsigned N = B.dim1();
    unsigned calls = offsets.size();
    bool ret = fetch(sol, M, K, N, S, calls, A.st1(), B.st1(), C.st1());
    if (ret)
    {
        assert((M == sol.M) && (K == sol.K) && (N == sol.N) && (S == sol.S) && (calls == sol.C));
        assert((unsigned) A.st1() == sol.strideA);
        assert((unsigned) B.st1() == sol.strideB);
        assert((unsigned) C.st1() == sol.strideC);

        int fixa = (int) A.buffer().data() - (int) sol.ptrA;
        int fixb = (int) B.buffer().data() - (int) sol.ptrB;
        int fixc = (int) C.buffer().data() - (int) sol.ptrC;
        for (unsigned i = 0; i < calls; i++)
        {
            MatMulOptions opt = offsets.get(i);
            sol.offsetA[i] = opt.offset0_ + fixa;
            sol.offsetB[i] = opt.offset1_ + fixb;
            sol.offsetC[i] = opt.offset2_ + fixc;
        }
    }
    return ret;
}

bool MatMulCache::fetch(MatMulSolution& sol, unsigned M, unsigned K, unsigned N,
        unsigned S, unsigned C, unsigned sta, unsigned stb, unsigned stc)
{
    bool ret = false;
    uint64_t id = hash(M, K, N, S, C, sta, stb, stc);
    ret = pool_.look_up_by_id<MatMulSolution>(id, sol);
    return ret;
}

void MatMulCache::write_back(MatMulSolution& sol)
{
    uint64_t id = hash(sol.M, sol.K, sol.N, sol.S, sol.C, sol.strideA, sol.strideB, sol.strideC);
    bool cached = pool_.is_id_in_pool(id);
    if (!cached)
    {
        sol.id_ = id;
        pool_.push<MatMulSolution>(sol);
    }
}

uint64_t MatMulCache::hash(unsigned M, unsigned K, unsigned N, unsigned S, unsigned C,
        unsigned sta, unsigned stb, unsigned stc)
{
    const int HASH_INPUT_SIZE = 8;
    unsigned int p[HASH_INPUT_SIZE] = {M, K, N, S, C, sta, stb, stc};
    uint64_t h = 0;
    for (int i = 0; i < HASH_INPUT_SIZE; i++)
        h ^= p[i] + 0x9e3779b9 + (h << 6) + (h >> 2);
    return h;
}

} /* namespace matmul */

