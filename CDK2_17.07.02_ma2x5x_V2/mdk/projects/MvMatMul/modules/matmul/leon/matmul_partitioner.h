///
/// @file
/// @copyright All code copyright Movidius Ltd 2012, all rights reserved.
///            For License Warranty see: common/license.txt
///

#ifndef _MATMUL_PARTITIONER_H_
#define _MATMUL_PARTITIONER_H_

#include "matmul_vector.h"

#ifdef __cplusplus

namespace matmul
{

struct PartitionerParams
{
    PartitionerParams();

    unsigned m_;
    unsigned k_;
    unsigned n_;
    unsigned s_;
    unsigned major_;
    unsigned minor_;
    unsigned major_len_;
    unsigned minor_len_;
    unsigned padm_;
    unsigned padk_;
    unsigned padn_;
};

class Partitioner
{
    static const unsigned DEPTH;
    static const unsigned MAX_DIVISORS;
    static const float MAX_WASTE;

public:
    Partitioner(unsigned cmx_items, unsigned kernel_width, MatMulAllocator& alloc);
    ~Partitioner();

    bool partition(PartitionerParams& p);

private:
    bool is_valid_k();
    void find_divisors(unsigned n, unsigned div, Vector<unsigned>& divisors);
    unsigned cmx_usage(unsigned k, unsigned minor_len, unsigned major_len);
    bool is_better_solution(unsigned minor_len, unsigned major_len);

    unsigned cmx_floats_;
    unsigned kernel_width_;
    unsigned major_;
    unsigned minor_;
    unsigned major_len_;
    unsigned minor_len_;
    unsigned m_;
    unsigned k_;
    unsigned n_;
    unsigned s_;
    unsigned padm_;
    unsigned padk_;
    unsigned padn_;
    MatMulAllocator& alloc_;
};

} /* namespace matmul */

#endif /* __cplusplus */

#endif // _MATMUL_PARTITIONER_H_

