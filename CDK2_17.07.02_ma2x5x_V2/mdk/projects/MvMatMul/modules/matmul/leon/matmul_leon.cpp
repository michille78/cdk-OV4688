///
/// @file
/// @copyright All code copyright Movidius Ltd 2012, all rights reserved.
///            For License Warranty see: common/license.txt
///

#if defined(__RTEMS__)
    #include <unistd.h>
    #include <pthread.h>
    #include "OsDrvShaveL2Cache.h"
#else
    #include <DrvShaveL2Cache.h>
#endif
#include <stddef.h>
#include <cstring>
#include <math.h>
#include <swcLeonUtils.h>
#include <swcShaveLoaderLocal.h>
#include <mvTensorInternal.h>
#include <mvTensorDebug.h>
#include <DrvLeonL2C.h>
#include "matmul_profile.h"
#include "matmul_leon.h"
#include "matmul_iface.h"
#include "matmul_dma.h"
#include "matmul_scheduler.h"
#include "matmul_partitioner.h"

#if defined(__RTEMS__)
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
#endif
matmul::MatMulTracePOD __attribute__((section(".cmx.data"))) tracer_stats;

swcShaveUnit_t shavesList[matmul::SHAVES_TOUSE_MAX] = {0,1,2,3,4,5,6,7,8,9,10,11};

extern DynamicContext_t localModule[12];

namespace matmul
{

template <typename T>
MvMatMul<T>::MvMatMul(MatMulConfig& cfg, MatMulCache& cache)
    : cfg_(cfg)
    , minor_(0)
    , minor_len_(0)
    , major_(0)
    , major_len_(0)
    , m_(0)
    , k_(0)
    , n_(0)
    , s_(0)
    , calls_(0)
    , alloc_("matmul allocator")
    , tensor_pool_(alloc_)
    , command_pool_(alloc_)
    , graph_(&alloc_)
    , cache_(cache)
    , solution_()
    , trace_(false)
    , tracer_(0)
    , options_()
{
    ErrorManager::Init(cfg.error_buffer);

    alloc_.reset(cfg.scratch_memory, cfg.scratch_memory_size);

    mms_.matrix_type = cfg.matrix_type;
    mms_.kernel_width = cfg.kernel_width;
    mms_.first_shave = cfg.first_shave;
    mms_.dma_link_agent = cfg.dma_link_agent;
    mms_.tracer_stats = &tracer_stats;
    mms_.trace = 0;

    static MatMulTrace SMT(tracer_stats);
    tracer_ = &SMT;
}

template <typename T>
MvMatMul<T>::~MvMatMul()
{
}

template <typename T>
uint64_t MvMatMul<T>::multiply(Tensor& A, Tensor& B, Tensor& C, int s, TensorList& loptions)
{
    // Single call interface
    ErrorManager::Assert(loptions.size() > 0,"Bad Number for options");
    ErrorManager::Assert(loptions.size() <= SINGLE_CALLS_MAX,"Bad Number for options");

    tracer_->profile_start();

    MATMUL_PROFILER_START;

    options_.reset();
    calls_ = loptions.size();
    options_ = loptions;

    multiply_internal(A, B, C, s);

    MATMUL_PROFILER_STOP;

    if (trace_) tracer_->collect(this);

    tracer_->profile_stop();
    return tracer_->stats_.cycles;
}

template <typename T>
uint64_t MvMatMul<T>::multiply(Tensor& A, Tensor& B, Tensor& C, int s, kernel_t kt)
{
    // One call interface
    tracer_->profile_start();

    options_.reset();
    calls_ = 1;
    MatMulOptions opt(0, 0, 0, kt);
    options_.add(opt);

    multiply_internal(A, B, C, s);

    if (trace_) tracer_->collect(this);

    tracer_->profile_stop();
    return tracer_->stats_.cycles;
}

template <typename T>
uint64_t MvMatMul<T>::multiply(int m, int k, int n, int s, T* pA, T* pB, T* pC,
        int strideA, int strideB, int strideC, kernel_t kt)
{
    // Legacy interface from raw pointers
    tracer_->profile_start();

    options_.reset();
    calls_ = 1;
    MatMulOptions opt(0, 0, 0, kt);
    options_.add(opt);

    MatMulBuffer<T> bufA(pA, m * k * sizeof(T));
    Tensor A(&bufA, DataTypeToEnum<T>::v(), m, k, strideA);

    MatMulBuffer<T> bufB(pB, k * n * sizeof(T));
    Tensor B(&bufB, DataTypeToEnum<T>::v(), k, n, strideB);

    MatMulBuffer<T> bufC(pC, m * n * sizeof(T));
    Tensor C(&bufC, DataTypeToEnum<T>::v(), m, n, strideC);

    multiply_internal(A, B, C, s);

    if (trace_) tracer_->collect(this);

    tracer_->profile_stop();
    return tracer_->stats_.cycles;
}

template <typename T>
void MvMatMul<T>::enable_trace()
{
    trace_ = true;
    mms_.trace = 1;
    tracer_->init();
}

template <typename T>
void MvMatMul<T>::disable_trace()
{
    trace_ = false;
    mms_.trace = 0;
}

template <typename T>
MatMulTrace& MvMatMul<T>::tracer()
{
    return *tracer_;
}

template <typename T>
int MvMatMul<T>::multiply_internal(Tensor& A, Tensor& B, Tensor& C, int s)
{
    MATMUL_PROFILER_START;

    s_ = s;
    m_ = A.dim0();
    k_ = A.dim1();
    n_ = B.dim1();

    unsigned padM = 0, padK = 0, padN = 0;

    alloc_.reset(cfg_.scratch_memory, cfg_.scratch_memory_size);
    solution_.tensor_pool_ = &tensor_pool_;
    solution_.command_pool_ = &command_pool_;
    solution_.graph_ = &graph_;
    solution_.dma_link_agent_ = mms_.dma_link_agent;
    bool cached = cache_.fetch(solution_, A, B, C, s_, options_);
    if (!cached)
    {
        PartitionerParams parp;
        parp.m_ = m_;
        parp.k_ = k_;
        parp.n_ = n_;
        parp.s_ = s_;
        Partitioner partitioner(CMX_DATA_SIZE / sizeof(T), cfg_.kernel_width, alloc_);
        if (!partitioner.partition(parp))
            ErrorManager::Assert((major_ != 0) || (minor_ != 0),"Major or Minor Slices of MatMul are zero");
        alloc_.reset(cfg_.scratch_memory, cfg_.scratch_memory_size);

        major_ = parp.major_;
        minor_ = parp.minor_;
        major_len_ = parp.major_len_;
        minor_len_ = parp.minor_len_;
        padM = parp.padm_;
        padK = parp.padk_;
        padN = parp.padn_;

        allocate_working_buffers();

        SchParams schp;
        schp.strideA = A.st1();
        schp.strideB = B.st1();
        schp.strideC = C.st1();
        schp.major_slices = major_;
        schp.minor_slices = minor_;
        schp.major_slice_len = major_len_;
        schp.minor_slice_len = minor_len_;
        schp.M = m_;
        schp.K = k_;
        schp.N = n_;
        schp.S = s_;
        schp.inA = A.buffer().base<T>();
        schp.inB = B.buffer().base<T>();
        schp.inoutC = C.buffer().base<T>();
        schp.first_shave = cfg_.first_shave;
        schp.padM = padM;
        schp.padK = padK;
        schp.padN = padN;
        MatMulScheduler sch;
        sch.schedule(schp, &tensor_pool_, &command_pool_, &graph_);

        solution_.M = m_;
        solution_.K = k_;
        solution_.N = n_;
        solution_.S = s_;
        solution_.C = calls_;
        solution_.major = major_;
        solution_.major_len = major_len_;
        solution_.minor = minor_;
        solution_.minor_len = minor_len_;
        solution_.padM = padM;
        solution_.padK = padK;
        solution_.padN = padN;
        solution_.strideA = A.st1();
        solution_.strideB = B.st1();
        solution_.strideC = C.st1();
        solution_.ptrA = A.buffer().base<T>();
        solution_.ptrB = B.buffer().base<T>();
        solution_.ptrC = C.buffer().base<T>();
        for (unsigned int idx = 0; idx < calls_; idx++)
        {
            MatMulOptions opt = options_.get(idx);
            solution_.offsetA[idx] = opt.offset0_;
            solution_.offsetB[idx] = opt.offset1_;
            solution_.offsetC[idx] = opt.offset2_;
        }
        solution_.tensor_cnt = tensor_pool_.pool_cnt();
        solution_.command_cnt = sch.command_cnt;
        solution_.command_pool_cnt = command_pool_.pool_cnt();
        solution_.tensor_pool_size = tensor_pool_.pool_size();
        solution_.command_pool_size = command_pool_.pool_size();
        solution_.graph_pool_size = graph_.graph_size();
        cache_.write_back(solution_);
    }
    major_ = solution_.major;
    major_len_ = solution_.major_len;
    minor_ = solution_.minor;
    minor_len_ = solution_.minor_len;
    padM = solution_.padM;
    padK = solution_.padK;
    padN = solution_.padN;

    if (padK) setup_k_padding(padK);

    memset(graph_.Addr_in_cmx, 0, solution_.minor * sizeof(T));
    memset(graph_.Bddr_in_cmx, 0, solution_.major * sizeof(T));
    mms_.total_shaves = s_;
    mms_.padK = padK;
    mms_.calls = solution_.C;
    mms_.minor_slices = solution_.minor;
    mms_.major_slices = solution_.major;
    mms_.tensor_cnt = solution_.tensor_cnt;
    mms_.command_cnt = solution_.command_cnt;
    mms_.command_pool_cnt = solution_.command_pool_cnt;
    mms_.tensor_ids = tensor_pool_.ids_array();
    mms_.tensor_arr = tensor_pool_.pool_array();
    mms_.command_ids = command_pool_.ids_array();
    mms_.command_arr = command_pool_.pool_array();
    mms_.graph_arr = graph_.array();
    for (unsigned int idx = 0; idx < calls_; idx++)
    {
        MatMulOptions opt = options_.get(idx);
        mms_.offsetA[idx] = solution_.offsetA[idx];
        mms_.offsetB[idx] = solution_.offsetB[idx];
        mms_.offsetC[idx] = solution_.offsetC[idx];
        mms_.kernel_type[idx] = opt.kernel_type_;
    }

/*
    pthread_mutex_lock(&mutex);
    printf("---------------------------------\n");
    printf("M                       = %d\n", m_);
    printf("K                       = %d\n", k_);
    printf("N                       = %d\n", n_);
    printf("S                       = %d\n", s_);
    printf("calls                   = %d\n", calls_);
    printf("cached                  = %s\n", cached?"YES":"NO");
    printf("ptrA                    = 0x%X\n", (unsigned) A.buffer().data());
    printf("ptrB                    = 0x%X\n", (unsigned) B.buffer().data());
    printf("ptrC                    = 0x%X\n", (unsigned) C.buffer().data());
    printf("A stride                = %d\n", A.st1());
    printf("B stride                = %d\n", B.st1());
    printf("C stride                = %d\n", C.st1());
    for (unsigned i = 0; i < calls_; i++)
        printf("off[%d] A=%d, B=%d, C=%d\n", i, mms_.offsetA[i], mms_.offsetB[i], mms_.offsetC[i]);
    printf("major_slices            = %d\n", major_);
    printf("major_slice_len         = %d\n", major_len_);
    printf("minor_slices            = %d\n", minor_);
    printf("minor_slice_len         = %d\n", minor_len_);
    printf("padM                    = %d\n", padM);
    printf("padK                    = %d\n", padK);
    printf("padN                    = %d\n", padN);
    printf("kernel type             = %u\n", (unsigned) cfg_.kernel_type);
    printf("first_shave             = %d\n", cfg_.first_shave);
    printf("tensor_cnt              = %d\n", mms_.tensor_cnt);
    printf("command_cnt             = %d\n", mms_.command_cnt);
    printf("command_pool_cnt        = %d\n", mms_.command_pool_cnt);
    printf("tensor_arr              = 0x%X\n", (unsigned) mms_.tensor_arr);
    printf("command_arr             = 0x%X\n", (unsigned) mms_.command_arr);
    printf("graph_arr               = 0x%X\n", (unsigned) mms_.graph_arr);
    printf("Tensor allocator memory = %d\n", solution_.tensor_pool_size);
    printf("Cmd allocator memory    = %d\n", solution_.command_pool_size);
    printf("Graph allocator memory  = %d\n", solution_.graph_pool_size);
    printf("Matmul allocator memory = %d\n", alloc_.memory_used());
    printf("---------------------------------\n");
    pthread_mutex_unlock(&mutex);
    for (int idc = 0; idc < mms_.command_cnt; idc++)
    {
        for (int ids = 0; ids < s_; ids++)
        {
            matmul::MatMulCmd cmd;
            command_pool_.look_up_by_index<matmul::MatMulCmd>(graph_.command[idc * s_ + ids], cmd);
            printf("cmd=%d shave=%d A_to_process=%d B_to_process=%d A_loader=%d B_loader=%d, A_to_load=%d, B_to_load=%d, sync=%d\n",
                idc, ids,
                cmd.A_to_process, cmd.B_to_process,
                cmd.A_loader,cmd.B_loader,
                cmd.A_to_load, cmd.B_to_load, cmd.sync);
        }
    }
    for (int idx = 0; idx < tensor_pool_.pool_cnt(); idx++)
    {
        matmul::MatMulBuffer<T> b;
        matmul::Tensor t(b);
        tensor_pool_.look_up_by_index<Tensor>(idx, t);
        printf("tensor[%d] dims[%3d][%3d] .stride=%3d .data=0x%X\n"
            , idx, t.dim0(), t.dim1(), t.st1(), (unsigned) t.buffer().data());
    }
*/

    run_shaves();

/*
    pthread_mutex_lock(&mutex);
    printf("matmul done\n");
    pthread_mutex_unlock(&mutex);
*/

    MATMUL_PROFILER_STOP;

    return 0;
}

template <typename T>
void MvMatMul<T>::allocate_working_buffers()
{
    unsigned tensor_cnt_est = (7 * s_) + 8;
    unsigned command_cnt_est = ceil((double) (minor_ * major_) / s_) * s_;
    unsigned graph_cnt_est_a =  (7 * s_) + minor_ + major_ + command_cnt_est;
    unsigned graph_cnt_est_b =  minor_ + major_ + minor_ * major_;

    unsigned tensor_pool_size_est = tensor_cnt_est * (sizeof(TensorPOD) + sizeof(unsigned int));
    unsigned command_pool_size_est = command_cnt_est * (sizeof(MatMulCmdPOD) + sizeof(unsigned int));
    unsigned graph_size_est = graph_cnt_est_a * sizeof(unsigned short) + graph_cnt_est_b * sizeof(GraphNode);
    unsigned total_to_allocate = tensor_pool_size_est + command_pool_size_est + graph_size_est;

    if (total_to_allocate > alloc_.memory_available())
    {
        char buf[64];
        sprintf(buf, "Matmul scratch memory [%d] lower than required [%d]"
                , alloc_.memory_available()
                , total_to_allocate);
        ErrorManager::Assert(total_to_allocate <= alloc_.memory_available(), buf);
    }

    tensor_pool_.reserve<Tensor>(tensor_cnt_est);
    command_pool_.reserve<MatMulCmd>(command_cnt_est);
    graph_.reserve(major_, minor_, command_cnt_est, s_);
}

template <typename T>
void MvMatMul<T>::setup_k_padding(unsigned padK)
{
    swcLeonDataCacheFlush();
    DrvLL2CFlushOpOnAllLines(LL2C_OPERATION_INVALIDATE, 0);

    MatMulBuffer<T> buf;
    Tensor tensor(buf);
    unsigned char* data;
    unsigned int size;
    for (int i = 0; i < s_; ++i)
    {
        // Acmx
        tensor_pool_.look_up_by_index<Tensor>(graph_.Acmx[i], tensor);
        data = (unsigned char*) tensor.buffer().data() + k_ * sizeof(T);
        size = padK * sizeof(T);
        for (int idx = 0; idx < minor_len_; idx++)
        {
            memset(data, 0, size);
            data += (k_ + padK) * sizeof(T);
        }

        // Acmx_next
        tensor_pool_.look_up_by_index<Tensor>(graph_.Acmx_next[i], tensor);
        data = (unsigned char*) tensor.buffer().data() + k_ * sizeof(T);
        size = padK * sizeof(T);
        for (int idx = 0; idx < minor_len_; idx++)
        {
            memset(data, 0, size);
            data += (k_ + padK) * sizeof(T);
        }

        // Bcmx
        tensor_pool_.look_up_by_index<Tensor>(graph_.Bcmx[i], tensor);
        data = (unsigned char*) tensor.buffer().data() + k_ * major_len_ * sizeof(T);
        size = padK * major_len_ * sizeof(T);
        memset(data, 0, size);

        // Bcmx_next
        tensor_pool_.look_up_by_index<Tensor>(graph_.Bcmx_next[i], tensor);
        data = (unsigned char*) tensor.buffer().data() + k_ * major_len_ * sizeof(T);
        size = padK * major_len_ * sizeof(T);
        memset(data, 0, size);
    }
}

#if defined(__RTEMS__)
template <typename T>
void MvMatMul<T>::run_shaves()
{
    MATMUL_PROFILER_START;
    int last_shave = cfg_.first_shave + s_;
    ErrorManager::Assert(last_shave <= LAST_SHAVE_MAX,"Last Shave Number beyond LAST_SHAVE_MAX");

    swcShaveUnit_t svuList[s_];
    u32 runningShaves;
    u32 shv_start_count = 0;

    int sc;
    for (int i = cfg_.first_shave; i < last_shave; i++)
    {
        svuList[shv_start_count++] = i;
        busyWaitShaveL1(i);
        sc = OsDrvSvuRunShaveAlgoOnAssignedShaveCCNoSetup(&localModule[i], i, "iii",
                (u32)&MODULE_ENTRY(SHVMatGEMM), (u32)&mms_, (int) cfg_.matrix_type);

        ErrorManager::Assert(sc == OS_MYR_DRV_SUCCESS,"MatMul failed to start shaves");
    }

    sc = OsDrvSvuDynWaitShaves(svuList, s_, OS_DRV_SVU_WAIT_FOREVER, &runningShaves);
    ErrorManager::Assert(sc == OS_MYR_DRV_SUCCESS,"Wait shaves failed on MatMul");

    OsDrvShaveL2CachePartitionFlush(0, PERFORM_INVALIDATION);

    MATMUL_PROFILER_STOP;
}
#else
template <typename T>
void MvMatMul<T>::run_shaves()
{
    MATMUL_PROFILER_START;

    int last_shave = cfg_.first_shave + s_;
    ErrorManager::Assert(last_shave <= LAST_SHAVE_MAX,"Last Shave Number beyond LAST_SHAVE_MAX");

    DrvShaveL2CachePartitionInvalidate(0);

    for (int i = cfg_.first_shave; i < last_shave; i++)
    {
        busyWaitShaveL1(i);

        swcRunShaveAlgoOnAssignedShaveCCNoSetup(&localModule[i], i, "iii",
                (u32)&MODULE_ENTRY(SHVMatGEMM), (u32)&mms_, (int) cfg_.matrix_type);
    }
    swcWaitShaves(s_, &shavesList[cfg_.first_shave]);
    DrvShaveL2CachePartitionFlush(0);
    MATMUL_PROFILER_STOP;
}
#endif

template struct MvMatMul<half>;

} /* namespace matmul */

