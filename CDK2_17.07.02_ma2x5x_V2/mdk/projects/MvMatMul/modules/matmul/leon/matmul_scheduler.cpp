///
/// @file
/// @copyright All code copyright Movidius Ltd 2012, all rights reserved.
///            For License Warranty see: common/license.txt
///

#include <cstring>
#include "matmul.h"
#include "matmul_iface.h"
#include "matmul_profile.h"
#include "matmul_scheduler.h"

namespace matmul
{

MatMulScheduler::MatMulScheduler()
    : asini(0)
    , asend(0)
    , bsini(0)
    , bsend(0)
{
}

MatMulScheduler::~MatMulScheduler()
{
}

void MatMulScheduler::schedule(SchParams& p,
        MatMulPool* tensor_pool,
        MatMulPool* command_pool,
        MatMulGraph* graph)
{
    M = p.M;
    K = p.K;
    N = p.N;
    S = p.S;
    padM = p.padM;
    padK = p.padK;
    padN = p.padN;
    strideA = p.strideA;
    strideB = p.strideB;
    strideC = p.strideC;
    major_slices = p.major_slices;
    minor_slices = p.minor_slices;
    minor_slice_len = p.minor_slice_len;
    major_slice_len = p.major_slice_len;
    m_first_shave = p.first_shave;
    inA = (half*) p.inA;
    inB = (half*) p.inB;
    inoutC = (half*) p.inoutC;

    tensor_pool_ = tensor_pool;
    command_pool_ = command_pool;
    graph_ = graph;

    generate_cmd_list();
    schedule_inner<half>();
}

template <typename T>
void MatMulScheduler::schedule_inner()
{
    MATMUL_PROFILER_START;

    int strideA_, strideB_, strideC_;
    strideA ? strideA_ = strideA : strideA_ = K * sizeof(T);
    strideB ? strideB_ = strideB : strideB_ = N * sizeof(T);
    strideC ? strideC_ = strideC : strideC_ = N * sizeof(T);

    MatMulBuffer<T> buf;

    for (int i = 0; i < S; ++i)
    {
        buf.set(get_cmx_data<T>(i + m_first_shave), (K + padK) * major_slice_len * sizeof(T));
        Tensor Bcmx(&buf, DataTypeToEnum<T>::v(), (K + padK), major_slice_len, major_slice_len * sizeof(T));
        graph_->Bcmx[i] = tensor_pool_->push<Tensor>(Bcmx);

        buf.set((T*) Bcmx.buffer().data() + Bcmx.dim0() * Bcmx.dim1(), (K + padK) * major_slice_len * sizeof(T));
        Tensor Bcmx_next(&buf, DataTypeToEnum<T>::v(), (K + padK), major_slice_len, major_slice_len * sizeof(T));
        graph_->Bcmx_next[i] = tensor_pool_->push<Tensor>(Bcmx_next);

        buf.set((T*) Bcmx_next.buffer().data() + Bcmx.dim0() * Bcmx.dim1(), minor_slice_len * (K + padK) * sizeof(T));
        Tensor Acmx(&buf, DataTypeToEnum<T>::v(), minor_slice_len, (K + padK), (K + padK) * sizeof(T));
        graph_->Acmx[i] = tensor_pool_->push<Tensor>(Acmx);

        buf.set((T*) Acmx.buffer().data() + Acmx.dim0() * Acmx.dim1(), minor_slice_len * (K + padK) * sizeof(T));
        Tensor Acmx_next(&buf, DataTypeToEnum<T>::v(), minor_slice_len, (K + padK), (K + padK) * sizeof(T));
        graph_->Acmx_next[i] = tensor_pool_->push<Tensor>(Acmx_next);

        buf.set((T*) Acmx_next.buffer().data() + Acmx.dim0() * Acmx.dim1(), minor_slice_len * major_slice_len * sizeof(T));
        Tensor Ccmx(&buf, DataTypeToEnum<T>::v(), minor_slice_len , major_slice_len, major_slice_len * sizeof(T));
        graph_->Ccmx[i] = tensor_pool_->push<Tensor>(Ccmx);

        buf.set((T*) Ccmx.buffer().data() + Ccmx.dim0() * Ccmx.dim1(), minor_slice_len * major_slice_len * sizeof(T));
        Tensor Ccmx_old(&buf, DataTypeToEnum<T>::v(), minor_slice_len , major_slice_len, major_slice_len * sizeof(T));
        graph_->Ccmx_old[i] = tensor_pool_->push<Tensor>(Ccmx_old);

        buf.set((T*) Ccmx_old.buffer().data() + Ccmx_old.dim0() * Ccmx_old.dim1(), minor_slice_len * major_slice_len * sizeof(T));
        Tensor Ccmx_next(&buf, DataTypeToEnum<T>::v(), minor_slice_len , major_slice_len, major_slice_len * sizeof(T));
        graph_->Ccmx_next[i] = tensor_pool_->push<Tensor>(Ccmx_next);
    }

    unsigned char* Abase = (unsigned char*) inA;
    unsigned char* Bbase = (unsigned char*) inB;
    unsigned char* Cbase = (unsigned char*) inoutC;

    enum pad_t {NOPAD, PADV, PADH, PADVH} atype, btype, ctype;
    enum idx_t {ADDR = 0, ADDRV, BDDR, BDDRH, CDDR, CDDRV, CDDRH, CDDRVH, MTMAX};

    unsigned index_to_tensor[MTMAX];

    {
        // Addr
        MatMulBuffer<T> buf((T*) Abase, minor_slice_len * K * sizeof(T));
        Tensor tensor(&buf, DataTypeToEnum<T>::v(), minor_slice_len , K, strideA_);
        index_to_tensor[ADDR] = tensor_pool_->push<Tensor>(tensor);
    }
    {
        // Addr vertical padding
        MatMulBuffer<T> buf((T*) Abase, (minor_slice_len - padM) * K * sizeof(T));
        Tensor tensor(&buf, DataTypeToEnum<T>::v(), minor_slice_len - padM, K, strideA_);
        index_to_tensor[ADDRV] = tensor_pool_->push<Tensor>(tensor);
    }
    {
        // Bddr
        MatMulBuffer<T> buf((T*) Bbase, K * major_slice_len * sizeof(T));
        Tensor tensor(&buf, DataTypeToEnum<T>::v(), K , major_slice_len, strideB_);
        index_to_tensor[BDDR] = tensor_pool_->push<Tensor>(tensor);
    }
    {
        // Bddr horizontal padding
        MatMulBuffer<T> buf((T*) Bbase, K * (major_slice_len - padN) * sizeof(T));
        Tensor tensor(&buf, DataTypeToEnum<T>::v(), K , major_slice_len - padN, strideB_);
        index_to_tensor[BDDRH] = tensor_pool_->push<Tensor>(tensor);
    }
    {
        // Cddr
        MatMulBuffer<T> buf((T*) Cbase, minor_slice_len * major_slice_len * sizeof(T));
        Tensor tensor(&buf, DataTypeToEnum<T>::v(), minor_slice_len , major_slice_len, strideC_);
        index_to_tensor[CDDR] = tensor_pool_->push<Tensor>(tensor);
    }
    {
        // Cddr vertical padding
        MatMulBuffer<T> buf((T*) Cbase, (minor_slice_len - padM) * major_slice_len * sizeof(T));
        Tensor tensor(&buf, DataTypeToEnum<T>::v(), minor_slice_len - padM, major_slice_len, strideC_);
        index_to_tensor[CDDRV] = tensor_pool_->push<Tensor>(tensor);
    }
    {
        // Cddr vertical padding
        MatMulBuffer<T> buf((T*) Cbase, minor_slice_len * (major_slice_len - padN) * sizeof(T));
        Tensor tensor(&buf, DataTypeToEnum<T>::v(), minor_slice_len , major_slice_len - padN, strideC_);
        index_to_tensor[CDDRH] = tensor_pool_->push<Tensor>(tensor);
    }
    {
        // Cddr vertical and horizontal padding
        MatMulBuffer<T> buf((T*) Cbase, (minor_slice_len - padM) * (major_slice_len - padN) * sizeof(T));
        Tensor tensor(&buf, DataTypeToEnum<T>::v(), minor_slice_len - padM, major_slice_len - padN, strideC_);
        index_to_tensor[CDDRVH] = tensor_pool_->push<Tensor>(tensor);
    }

    Tensor tensor(buf);
    atype = NOPAD;
    for (int a_slice = 0; a_slice < minor_slices; ++a_slice)
    {
        btype = NOPAD;
        ctype = NOPAD;
        if ((a_slice + 1) * minor_slice_len > M)
        {
            atype = PADV;
            ctype = PADV;
        }

        T* dataA = (T*) Abase;
        idx_t aidx;
        switch (atype)
        {
            case NOPAD: aidx = ADDR; break;
            case PADV: aidx = ADDRV; break;
            default: assert(0); break;
        }
        tensor_pool_->look_up_by_index(index_to_tensor[aidx], tensor);
        unsigned int idx = tensor_pool_->get_index_for_id(tensor.id());
        int offset = (int) dataA - (int) tensor.buffer().data();
        GraphNode node(idx, offset);
        graph_->Addr[a_slice] = node;
        Abase += minor_slice_len * strideA_;

        for (int b_slice = 0; b_slice < major_slices; b_slice++)
        {
            if ((b_slice + 1) * major_slice_len > N)
            {
                if (atype == NOPAD) ctype = PADH;
                else ctype = PADVH;
                btype = PADH;
            }

            T* dataB = (T*) Bbase + (b_slice * major_slice_len);
            T* dataC = (T*) Cbase + b_slice * major_slice_len;

            if (!a_slice)
            {
                idx_t bidx;
                switch (btype)
                {
                    case NOPAD: bidx = BDDR; break;
                    case PADH: bidx = BDDRH; break;
                    default: assert(0); break;
                }
                tensor_pool_->look_up_by_index(index_to_tensor[bidx], tensor);
                unsigned int idx = tensor_pool_->get_index_for_id(tensor.id());
                int offset = (int) dataB - (int) tensor.buffer().data();
                GraphNode node(idx, offset);
                graph_->Bddr[b_slice] = node;
            }

            idx_t cidx;
            switch (ctype)
            {
                case NOPAD: cidx = CDDR; break;
                case PADV: cidx = CDDRV; break;
                case PADH: cidx = CDDRH; break;
                case PADVH: cidx = CDDRVH; break;
                default: assert(0); break;
            }
            tensor_pool_->look_up_by_index(index_to_tensor[cidx], tensor);
            unsigned int idx = tensor_pool_->get_index_for_id(tensor.id());
            int offset =  (int) dataC - (int) tensor.buffer().data();
            GraphNode node(idx, offset);
            graph_->Cddr[a_slice * major_slices + b_slice] = node;
        }
        Cbase += minor_slice_len * strideC_;
    }

    MATMUL_PROFILER_STOP;
}

void MatMulScheduler::generate_cmd_list()
{
    MATMUL_PROFILER_START;

    command_cnt = 0;
    int bscurr = S;
    int ascurr = S;

    int bsleft = major_slices;
    bsini = 0;
    bsend = std::min(S, major_slices);
    while (bsleft > 0)
    {
        int asleft = minor_slices;
        asini = 0;
        asend = std::min(S, minor_slices);

        while (asleft > 0)
        {
            set_cmd_list();

            asleft -= ascurr;
            asini = asend;
            asend += ascurr;
            if (asend > minor_slices) asend = minor_slices;
        }

        bsleft -= bscurr;
        bsini = bsend;
        bsend += bscurr;
        if (bsend > major_slices) bsend = major_slices;
    }

    MATMUL_PROFILER_STOP;
}

void MatMulScheduler::clean_last_command()
{
    MatMulCmd empty;
    for (int i = 0; i < S; i++)
        command_last[i] = empty;
}

void MatMulScheduler::set_cmd_list()
{
    bool new_load_area = true;
    int asz = asend - asini;
    int bsz = bsend - bsini;

    clean_last_command();

    char tmat[SHAVES_TOUSE_MAX][SHAVES_TOUSE_MAX];
    for (int i = 0; i < asz; i++)
        for (int j = 0; j < bsz; j++)
            tmat[i][j] = -1;

    int tcnt = 0;
    int scnt = 0;
    int aidx = 0;
    int bidx = 0;
    int aboundary = std::min(S, asz);
    int bboundary = std::min(S, bsz);
    while (tcnt < asz * bsz)
    {
        while (tmat[aidx][bidx] != -1)
        {
            aidx = inc_sat(aidx, aboundary);

            if (tmat[aidx][bidx] != -1)
                bidx = inc_sat(bidx, bboundary);
        }

        tmat[aidx][bidx] = scnt;
        MatMulCmd cmd(asini + aidx, bsini + bidx, aidx, bidx);
        if ((major_slices < S) && (S % major_slices == 0))
        {
            cmd.B_loader = scnt;
        }
        if (S == 1)
        {
            cmd.sync = 0;
        }
        else
        {
            if ((!new_load_area) || (!command_cnt))
                cmd.sync = 0;
        }
        command_last[scnt] = cmd;

        tcnt += 1;

        scnt += 1;
        if (scnt >= S)
        {
            scnt = 0;
            for (int inds = 0; inds < S; inds++)
            {
                if (new_load_area) set_load_cmd(inds);
                graph_->command[command_cnt * S + inds] = command_pool_->push<MatMulCmd>(command_last[inds]);
            }
            clean_last_command();
            new_load_area = false;
            command_cnt += 1;
        }
        aidx = inc_sat(aidx, aboundary);
        bidx = inc_sat(bidx, bboundary);

    }
    if (scnt)
    {
        for (int inds = 0; inds < S; inds++)
        {
            if (new_load_area)
            {
                set_load_cmd(inds);
            }
            else
            {
                command_last[inds].sync = 0;
            }
            graph_->command[command_cnt * S + inds] = command_pool_->push<MatMulCmd>(command_last[inds]);
        }
        command_cnt += 1;
    }
}

void MatMulScheduler::set_load_cmd(int shave)
{
    int A_to_load;
    int B_to_load;
    A_to_load = asini + shave;
    if (A_to_load < minor_slices)
        command_last[shave].A_to_load = A_to_load;

    if (asini == 0)
    {
        if ((major_slices < S) && (S % major_slices == 0))
        {
            command_last[shave].B_to_load = shave % major_slices;
        }
        else
        {
            B_to_load = bsini + shave;
            if ((B_to_load < major_slices))
            {
                command_last[shave].B_to_load = B_to_load;
            }
        }
    }
}

int MatMulScheduler::inc_sat(int n, int s)
{
    n += 1;
    if (n >= s) n = 0;
    return n;
}

template void MatMulScheduler::schedule_inner<half>();

} /* namespace matmul */

