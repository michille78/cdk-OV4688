///
/// @file
/// @copyright All code copyright Movidius Ltd 2012, all rights reserved.
///            For License Warranty see: common/license.txt
///

#ifndef _MATMUL_ALLOC_H_
#define _MATMUL_ALLOC_H_

#include <cstdio>
#include <cstddef>
#include <cassert>
#include <cstdint>

#ifdef __cplusplus

namespace matmul
{

class MatMulAllocator
{
    static const int BLOCKS_MEMORY_MAX = 1;
    static const int MSG_LENGTH = 32;

public:
    MatMulAllocator(const char* msg);
    MatMulAllocator(char* block, size_t size, const char* msg);
    ~MatMulAllocator();

    char* allocate(size_t bytes, unsigned aligment = 1);

    size_t memory_used() const { return memory_used_; }
    size_t memory_available() const { return memory_available_; }
    char* base() const { return base_; }
    void reset();
    void reset(char* block, size_t size);
    void free(void* ptr) const { (void) ptr; }

private:
    char* allocation_ptr_;
    size_t memory_available_;
    size_t memory_used_;
    char* base_;
    size_t base_size_;
    char msg_[MSG_LENGTH];
};

inline char* MatMulAllocator::allocate(size_t bytes, unsigned aligment)
{
    unsigned offset = 0;
    char* aligned_ptr = allocation_ptr_;
    while ((uint64_t) aligned_ptr % aligment)
    {
        offset++;
        aligned_ptr++;
    }
    unsigned aligned_bytes = bytes + offset;

    if (aligned_bytes > memory_available_)
    {
        printf("%s\n", msg_);
        assert(aligned_bytes < memory_available_);
    }

    allocation_ptr_ = aligned_ptr;
    char* result = allocation_ptr_;
    allocation_ptr_ += bytes;
    memory_available_ -= aligned_bytes;
    memory_used_ += aligned_bytes;
    return result;
}

} /* namespace matmul */

#endif /* __cplusplus */

#endif // _MATMUL_ALLOC_H_

