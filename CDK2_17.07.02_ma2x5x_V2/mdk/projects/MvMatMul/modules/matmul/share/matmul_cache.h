///
/// @file
/// @copyright All code copyright Movidius Ltd 2012, all rights reserved.
///            For License Warranty see: common/license.txt
///

#ifndef _MATMUL_CACHE_H_
#define _MATMUL_CACHE_H_

#include "matmul.h"
#include "matmul_pool.h"
#include "matmul_graph.h"

#ifdef __cplusplus

namespace matmul
{

struct MatMulSolutionPOD
{
    uint64_t id;
    unsigned int M;
    unsigned int K;
    unsigned int N;
    unsigned int S;
    unsigned int C;
    unsigned int major;
    unsigned int major_len;
    unsigned int minor;
    unsigned int minor_len;
    unsigned int padM;
    unsigned int padK;
    unsigned int padN;
    unsigned int strideA;
    unsigned int strideB;
    unsigned int strideC;
    void* ptrA;
    void* ptrB;
    void* ptrC;
    int offsetA[SINGLE_CALLS_MAX];
    int offsetB[SINGLE_CALLS_MAX];
    int offsetC[SINGLE_CALLS_MAX];
    kernel_t kernel_type[SINGLE_CALLS_MAX];
    unsigned int tensor_cnt;
    unsigned int command_cnt;
    unsigned int command_pool_cnt;
    unsigned int tensor_pool_size;
    unsigned int command_pool_size;
    unsigned int graph_pool_size;
    char data[SCRATCH_MEMORY];
};

class MatMulSolution
{
public:
    uint64_t id_;
    unsigned int M;
    unsigned int K;
    unsigned int N;
    unsigned int S;
    unsigned int C;
    unsigned int major;
    unsigned int major_len;
    unsigned int minor;
    unsigned int minor_len;
    unsigned int padM;
    unsigned int padK;
    unsigned int padN;
    unsigned int strideA;
    unsigned int strideB;
    unsigned int strideC;
    void* ptrA;
    void* ptrB;
    void* ptrC;
    int offsetA[SINGLE_CALLS_MAX];
    int offsetB[SINGLE_CALLS_MAX];
    int offsetC[SINGLE_CALLS_MAX];
    kernel_t kernel_type[SINGLE_CALLS_MAX];
    unsigned int tensor_cnt;
    unsigned int command_cnt;
    unsigned int command_pool_cnt;
    unsigned int tensor_pool_size;
    unsigned int command_pool_size;
    unsigned int graph_pool_size;
    unsigned int pod_size_;
    unsigned int pod_aligment_;
    unsigned int dma_link_agent_;
    MatMulPool* tensor_pool_;
    MatMulPool* command_pool_;
    MatMulGraph* graph_;

    MatMulSolution();
    void serialize(void* ptr);
    void unserialize(void* ptr);
    uint64_t id() const { return id_; }
    unsigned int pod_size() const { return pod_size_; };
    unsigned int pod_aligment() const { return pod_aligment_; };
};

// Multithreaded singleton: should be re-implemented in C++11 or higher
class MatMulCache
{
public:
    static MatMulCache& instance();
    MatMulCache();
    ~MatMulCache();

    void config(char* cache_memory_ptr, unsigned cache_memory_size);
    bool fetch(MatMulSolution& sol, Tensor& A, Tensor& B, Tensor& C, unsigned S, TensorList& offsets);
    void write_back(MatMulSolution& sol);

private:
    MatMulAllocator alloc_;
    MatMulPool64 pool_;
    bool is_configured_;
    char* cache_memory_ptr_;
    unsigned cache_memory_size_;

    bool fetch(MatMulSolution& sol, unsigned M, unsigned K, unsigned N, unsigned S, unsigned C,
            unsigned sta, unsigned stb, unsigned stc);
    uint64_t hash(unsigned M, unsigned K, unsigned N, unsigned S, unsigned C,
            unsigned sta, unsigned stb, unsigned stc);
    void run_once(void);
    void config_internal(char* cache_memory_ptr, unsigned cache_memory_size);
};

} /* namespace matmul */

#endif /* __cplusplus */

#endif // _MATMUL_LEON_H_

