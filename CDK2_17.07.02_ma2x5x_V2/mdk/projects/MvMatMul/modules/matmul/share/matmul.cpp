///
/// @file
/// @copyright All code copyright Movidius Ltd 2012, all rights reserved.
///            For License Warranty see: common/license.txt
///

#include <assert.h>
#include <cstring>
#include "matmul.h"

namespace matmul
{

TensorSub::TensorSub()
    : start0(0)
    , start1(0)
    , delta0(0)
    , delta1(0)
{
}

TensorSub::TensorSub(int s0, int s1, int d0, int d1)
    : start0(s0)
    , start1(s1)
    , delta0(d0)
    , delta1(d1)
{
}

int Tensor::refid = 0;

Tensor::Tensor()
    : buf_(0)
    , type_(MMT_INVALID)
    , dim0_(0)
    , dim1_(0)
    , stride1_(0)
    , id_(refid)
    , pod_size_(sizeof(TensorPOD))
    , pod_aligment_(sizeof(int))
{
    refid++;
}

Tensor::Tensor(IMatMulBuffer& buf)
    : buf_(&buf)
    , type_(MMT_INVALID)
    , dim0_(0)
    , dim1_(0)
    , stride1_(0)
    , id_(refid)
    , pod_size_(sizeof(TensorPOD))
    , pod_aligment_(sizeof(int))
{
    refid++;
}

Tensor::Tensor(IMatMulBuffer* buf, MatMulType type, int d0, int d1, int st1)
    : buf_(buf)
    , type_(type)
    , dim0_(d0)
    , dim1_(d1)
    , stride1_(st1)
    , id_(refid)
    , pod_size_(sizeof(TensorPOD))
    , pod_aligment_(sizeof(int))
{
    refid++;
}

#define SINGLE_ARG(...) __VA_ARGS__
#define CASE(TYPE, STMTS)                           \
    case DataTypeToEnum<TYPE>::value:               \
    {                                               \
        typedef TYPE T;                             \
        STMTS;                                      \
        break;                                      \
    }
#define CASES(TYPE_ENUM, STMTS)                     \
    switch (TYPE_ENUM)                              \
    {                                               \
        CASE(half, SINGLE_ARG(STMTS))               \
        CASE(float, SINGLE_ARG(STMTS))              \
        case MMT_INVALID:                           \
        assert(0);                                  \
        break;                                      \
        default:                                    \
        assert(0);                                  \
        break;                                      \
    }

Tensor::~Tensor()
{
}

void Tensor::serialize(void* ptr)
{
    TensorPOD& out = *(TensorPOD*) ptr;
    out.id = id_;
    out.type = type_;
    out.dim0 = dim0_;
    out.dim1 = dim1_;
    out.stride0 = stride1_;
    out.buf_size = static_cast<int>(buf_->size());
    out.buf_data = (uint32_t)buf_->data();
}

void Tensor::unserialize(void* ptr)
{
    TensorPOD& in = *(TensorPOD*) ptr;
    id_ = in.id;
    type_ = static_cast<MatMulType>(in.type);
    dim0_ = in.dim0;
    dim1_ = in.dim1;
    stride1_ = in.stride0;
    buf_->set((void*)in.buf_data, in.buf_size);
}

template <typename T0>
Tensor Tensor::operator()(const TensorSub& ts, MatMulBuffer<T0>& buf)
{
    if ((ts.start0 >= dim0_) || (ts.start0 + ts.delta0 > dim0_))
        assert(0);
    if ((ts.start1 >= dim1_) || (ts.start1 + ts.delta1 > dim1_))
        assert(0);

    Tensor tensor;
    tensor.id_ = id_;
    tensor.type_ = type_;
    tensor.dim0_= ts.delta0;
    tensor.dim1_= ts.delta1;
    tensor.stride1_ = stride1_;
    size_t elemsz;
    CASES(type_, elemsz = sizeof(T));
    size_t new_size =  tensor.dim0_ * tensor.dim1_ * elemsz;

    char* new_data = reinterpret_cast<char*>(buf_->data()) + ts.start0 * stride1_ + ts.start1 * elemsz;
    tensor.buf_ = &buf;
    tensor.buf_->set(reinterpret_cast<T0*>(new_data), new_size);

    return tensor;
}

Tensor& Tensor::operator= (const Tensor& rhs)
{
    id_ = rhs.id_;
    type_ = rhs.type_;
    dim0_ = rhs.dim0_;
    dim1_ = rhs.dim1_;
    stride1_ = rhs.stride1_;
    buf_->set(rhs.buf_->data(), rhs.buf_->size());

    return *this;
}

void Tensor::set_data(void* data)
{
    refid++;
    id_++;
    buf_->set_data(data);
}

#undef CASES
#undef CASE


void swap_buffers(Tensor& lhs, Tensor& rhs)
{
    std::swap(lhs.buf_, rhs.buf_);
}

MatMulOptions::MatMulOptions()
    : offset0_(0)
    , offset1_(0)
    , offset2_(0)
    , kernel_type_(GEMM_HHHH_NNN)
{
}

MatMulOptions::MatMulOptions(int off0, int off1, int off2, kernel_t kt)
    : offset0_(off0)
    , offset1_(off1)
    , offset2_(off2)
    , kernel_type_(kt)
{
}

TensorList::TensorList()
    : elem_(0)
    , options_()
{
}

TensorList::TensorList(const TensorList& other)
{
    elem_ = other.elem_;
    for (unsigned i = 0; i < other.elem_; i++)
        options_[i] = other.options_[i];
}

TensorList::~TensorList()
{
}

TensorList& TensorList::operator=(TensorList tmp)
{
    std::swap(elem_, tmp.elem_);
    for (unsigned i = 0; i < std::max(elem_, tmp.elem_); i++)
        std::swap(options_[i], tmp.options_[i]);
    return *this;
}

void TensorList::add(MatMulOptions& opt)
{
    assert(elem_ < MAX_ITEMS);
    options_[elem_] = opt;
    elem_++;
}

size_t TensorList::size()
{
    return elem_;
}

MatMulOptions&  TensorList::get(unsigned int idx)
{
    assert(idx < elem_);
    return options_[idx];
}

void TensorList::reset()
{
    elem_ = 0;
}

template Tensor Tensor::operator()<half>(const TensorSub& ts, MatMulBuffer<half>& buf);

} /* namespace matmul */

