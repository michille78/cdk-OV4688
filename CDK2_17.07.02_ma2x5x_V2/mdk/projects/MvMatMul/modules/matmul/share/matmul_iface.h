///
/// @file
/// @copyright All code copyright Movidius Ltd 2012, all rights reserved.
///            For License Warranty see: common/license.txt
///

#ifndef __MATMUL_IFACE_H__
#define __MATMUL_IFACE_H__

#include <cassert>
#include <mvTensorInternal.h>

#define DDR_TO_DDRCACHE(_address) ((u8*)(((u32)_address)&0xF7FFFFFF))
#define ALIGNED(_value) __attribute__((aligned(_value)))
#if (defined MYRIAD2)
#    ifndef DDR_BSS
#        define DDR_BSS       __attribute__((section(".ddr.bss"))) ALIGNED(16)
#    endif
#define DDR_LEON_HEAP __attribute__((section(".ddr.bss"))) ALIGNED(16)
#    ifndef DDR_BSS_DIRECT
#        define DDR_BSS_DIRECT  __attribute__((section(".ddr_direct.bss"))) ALIGNED(8)
#    endif
#endif

#ifdef __cplusplus
extern "C" {
#endif

#include <mv_types.h>

void gemm_ssss(const float *A, const float *B, float *C, int m, int k, int n,
                int wA, int wB, int wC );

void gemm_hhhh(const half *A, const half *B, half *C, int m, int k, int n,
               int wA, int wB, int wC);

void matvecmul_acc(const float *A, const float *B, float *C, int K, int N);
void matvecmulT_acc(const float *A, const float *B, float *C, int K, int N);

void gemm_ssss_c(const float *A, const float *B, float *C, int m, int k, int n,
                int wA, int wB, int wC);

void gemm_hhhh_c(const half *A, const half *B, half *C, int m, int k, int n,
                int wA, int wB, int wC);

#ifndef __MOVICOMPILE__
MODULE_ENTRY_DECL(SHVMatGEMM);
MODULE_ENTRY_DECL(SHVMatGEMV);
#endif // ifndef __MOVICOMPILE__

#ifdef __cplusplus
}
#endif


#ifdef __cplusplus

namespace matmul
{

static const int CMX_DATA_SIZE = MVTENSOR_HEAP_DATA_SIZE;
static const int SHAVES_TOUSE_MAX = MVTENSOR_MAX_SHAVES;
static const int SCRATCH_MEMORY = 110 * 1024;
static const int CACHE_MEMORY_SIZE = 25 * 1024 * 1024;

enum kernel_t
{
    GEMM_HHHH_NNN = 0,
    GEMM_HHHH_NNN_C,
    GEMM_HHHH_NNN_K8,
    GEMM_HHHH_NNN_K16,
    GEMM_HHHH_NNN_NAC
};

#ifndef __MOVICOMPILE__
template <typename T>
T* get_cmx_data(int shvno)
{
    assert(shvno < SHAVES_TOUSE_MAX);

    return (T*)getCMXSliceDataSection(shvno);
}
#endif // ifndef __MOVICOMPILE__

} /* namespace matmul */

#endif /* __cplusplus */

#endif // #ifndef __MATMUL_IFACE_H__

