///
/// @file
/// @copyright All code copyright Movidius Ltd 2012, all rights reserved.
///            For License Warranty see: common/license.txt
///

#ifndef _DMA_PROFILE_H_
#define _DMA_PROFILE_H_

#ifdef __cplusplus

//#define DMA_PROFILE 1

#ifdef DMA_PROFILE
    #define DMA_PROFILER_REC_TRANSACTION(ps, sl, ss, pd, dl, ds, by) matmul::DmaProfiler::instance().log(ps, sl, ss, pd, dl, ds, by)
    #define DMA_PROFILER_REC_WAIT matmul::DmaProfiler::instance().log()
    #define DMA_PROFILER_LOG matmul::DmaProfiler::instance().print()
#else
    #define DMA_PROFILER_REC_TRANSACTION(ps, sl, ss, pd, dl, ds, by)
    #define DMA_PROFILER_REC_WAIT
    #define DMA_PROFILER_LOG
#endif

namespace matmul
{

static const int DMA_PROFILER_LOG_ITEMS = 1 * 1024;

struct DmaProfilePOD
{
    DmaProfilePOD()
        : timestamp(0)
        , src(0)
        , src_line(0)
        , src_stride(0)
        , dst(0)
        , dst_line(0)
        , dst_stride(0)
        , bytes(0)
    {};

    DmaProfilePOD(unsigned int ts, void* ps = 0, unsigned int sl = 0, unsigned int ss = 0,
        void* pd = 0, unsigned int dl = 0, unsigned int ds = 0, unsigned int by = 0)
        : timestamp(ts)
        , src(ps)
        , src_line(sl)
        , src_stride(ss)
        , dst(pd)
        , dst_line(dl)
        , dst_stride(ds)
        , bytes(by)
    {};

    unsigned int timestamp;
    void* src;
    unsigned int src_line;
    unsigned int src_stride;
    void* dst;
    unsigned int dst_line;
    unsigned int dst_stride;
    unsigned int bytes;
};

class DmaProfiler
{

public:
    static DmaProfiler& instance();
    ~DmaProfiler();

    void log();
    void log(void* ps, unsigned int sl, unsigned int ss, void* pd,
            unsigned int dl, unsigned int ds, unsigned int by);
    void print();

private:
    DmaProfiler();
    void print_internal(const char* msg, unsigned int cnt, DmaProfilePOD* arr);
};

} /* namespace matmul */

#endif /* __cplusplus */

#endif // _DMA_PROFILE_H_

