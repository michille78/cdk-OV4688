///
/// @file
/// @copyright All code copyright Movidius Ltd 2012, all rights reserved.
///            For License Warranty see: common/license.txt
///

#ifndef _MATMUL_H_
#define _MATMUL_H_

#include <algorithm>
#include <map>
#include <vector>
#include <string>
#include <mv_types.h>
#include "matmul_iface.h"
#include "matmul_types.h"
#include "matmul_alloc.h"
#include "matmul_cmd.h"

#ifdef __cplusplus

namespace matmul
{
static const int SINGLE_CALLS_MAX = 3;

class Tensor;

struct TensorPOD
{
    unsigned int id;
    int type;
    int dim0;
    int dim1;
    int stride0;
    int buf_size;
    uint32_t buf_data;
};

class IMatMulBuffer
{
public:
    virtual ~IMatMulBuffer() {}

    virtual void* data() const = 0;
    virtual size_t size() const = 0;

    template <typename T>
    T* base() const { return reinterpret_cast<T*>(data()); }

    virtual void set(void* data, size_t sz) = 0;
    virtual void set_data(void* data) = 0;
};

template <typename T>
class MatMulBuffer : public IMatMulBuffer
{
public:
    MatMulBuffer()
        : data_(0)
        , size_(0)
    {}

    MatMulBuffer(T* data, int size)
        : data_(data)
        , size_(size)
    {}

    ~MatMulBuffer()
    {}

    void* data() const { return data_; }
    size_t size() const { return size_; }

    void set(void* data, size_t sz)
    {
        data_ = reinterpret_cast<T*>(data);
        size_ = sz;
    }

    void set_data(void* data)
    {
        data_ = reinterpret_cast<T*>(data);
    }

    friend void swap(MatMulBuffer& lhs, MatMulBuffer& rhs)
    {
        std::swap(lhs.data_, rhs.data_);
        std::swap(lhs.size_, rhs.size_);
    }

private:
    T* data_;
    size_t size_;
};

class TensorSub
{
public:
    TensorSub();
    TensorSub(int s0, int s1, int d0, int d1);

    int start0;
    int start1;
    int delta0;
    int delta1;
};

class Tensor
{
public:
    Tensor();
    Tensor(IMatMulBuffer& buf);
    Tensor(IMatMulBuffer* buf, MatMulType type, int d0, int d1, int st1 = 0);
    ~Tensor();

    void serialize(void* ptr);
    void unserialize(void* ptr);

    friend void swap_buffers(Tensor& lhs, Tensor& rhs);

    template <typename T0>
    Tensor operator()(const TensorSub& ts, MatMulBuffer<T0>& buf);

    Tensor& operator= (const Tensor& rhs);

    const IMatMulBuffer& buffer() const { return *buf_; }
    unsigned int dim0() const { return dim0_; }
    int dim1() const { return dim1_; }
    int st1() const { return stride1_; }
    unsigned int id() const { return id_; }
    unsigned int pod_size() const { return pod_size_; };
    unsigned int pod_aligment() const { return pod_aligment_; };

    void set_data(void* data);

private:

    static int refid;

    IMatMulBuffer* buf_;
    MatMulType type_;
    int dim0_;
    int dim1_;
    int stride1_;
    unsigned int id_;
    unsigned int pod_size_;
    unsigned int pod_aligment_;
};

void swap_buffers(Tensor& lhs, Tensor& rhs);

class MatMulOptions
{
    // Collection of options
    //   offsets are expressed in bytes from first element of a matrix
public:
    MatMulOptions();
    MatMulOptions(int off0, int off1, int off2, kernel_t kt = GEMM_HHHH_NNN);

    int offset0_;
    int offset1_;
    int offset2_;
    kernel_t kernel_type_;
};

class TensorList
{
public:
    TensorList();
    TensorList(const TensorList& other);
    ~TensorList();

    TensorList& operator=(TensorList tmp);

    void add(MatMulOptions& opt);
    size_t size();
    MatMulOptions&  get(unsigned int idx);
    void reset();

private:
    static const int MAX_ITEMS = 16;
    unsigned int elem_;
    MatMulOptions options_[MAX_ITEMS];
};

struct MatMulSharePOD
{
    unsigned int first_shave;
    unsigned int total_shaves;
	unsigned int kernel_width;
    unsigned int dma_link_agent;
    unsigned int padK;

    unsigned int trace;
    void* tracer_stats;

    void* tensor_ids;
    void* tensor_arr;
    void* command_ids;
    void* command_arr;
    void* graph_arr;

    void* tensor_pages;
    void* command_pages;
    void* graph_pages;

    unsigned int minor_slices;
    unsigned int major_slices;
    unsigned int tensor_cnt;
    unsigned int command_cnt;
    unsigned int command_pool_cnt;
    unsigned int calls;

    int offsetA[SINGLE_CALLS_MAX];
    int offsetB[SINGLE_CALLS_MAX];
    int offsetC[SINGLE_CALLS_MAX];
    kernel_t kernel_type[SINGLE_CALLS_MAX];

    // legacy fields, should be removed once refactoring is done
    MatMulType matrix_type;
};

} /* namespace matmul */

#endif /* __cplusplus */

#endif // _MATMUL_H_

