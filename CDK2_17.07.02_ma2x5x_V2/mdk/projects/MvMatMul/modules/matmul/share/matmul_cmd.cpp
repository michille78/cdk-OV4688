///
/// @file
/// @copyright All code copyright Movidius Ltd 2012, all rights reserved.
///            For License Warranty see: common/license.txt
///

#include "matmul_cmd.h"

namespace matmul
{

unsigned int MatMulCmd::refid = 0;

MatMulCmd::MatMulCmd(short r, char c, char la, char lb)
    : A_to_process(r)
    , B_to_process(c)
    , A_loader(la)
    , B_loader(lb)
    , B_to_load(-1)
    , A_to_load(-1)
    , id_(refid)
    , sync(1)
    , pod_size_(sizeof(MatMulCmdPOD))
    , pod_aligment_(2)
{
    refid++;
}

MatMulCmd& MatMulCmd::operator= (const MatMulCmd& rhs)
{
    A_to_process = rhs.A_to_process;
    B_to_process = rhs.B_to_process;
    A_loader = rhs.A_loader;
    B_loader = rhs.B_loader;
    A_to_load = rhs.A_to_load;
    B_to_load = rhs.B_to_load;
    sync = rhs.sync;

    return *this;
}

void MatMulCmd::serialize(void* ptr)
{
    MatMulCmdPOD& out = *(MatMulCmdPOD*) ptr;
    out.A_to_process = A_to_process;
    out.B_to_process = B_to_process;
    out.A_loader = A_loader;
    out.B_loader = B_loader;
    out.A_to_load = A_to_load;
    out.B_to_load = B_to_load;
    out.sync = sync;
    out.id = id_;
}

void MatMulCmd::unserialize(void* ptr)
{
    MatMulCmdPOD& in = *(MatMulCmdPOD*) ptr;
    A_to_process = in.A_to_process;
    B_to_process = in.B_to_process;
    A_loader = in.A_loader;
    B_loader = in.B_loader;
    A_to_load = in.A_to_load;
    B_to_load = in.B_to_load;
    sync = in.sync;
    id_ = in.id;
}

} /* namespace matmul */

