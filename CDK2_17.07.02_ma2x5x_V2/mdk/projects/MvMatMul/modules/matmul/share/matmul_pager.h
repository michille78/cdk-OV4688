///
/// @file
/// @copyright All code copyright Movidius Ltd 2012, all rights reserved.
///            For License Warranty see: common/license.txt
///

#ifndef _MATMUL_PAGER_H_
#define _MATMUL_PAGER_H_

#ifdef __cplusplus

namespace matmul
{

class MatMulPager
{
public:
    MatMulPager(unsigned int pages, size_t page_sz, void* internal, void* external);
    ~MatMulPager();

    void save_page(unsigned int idx);
    void load_page(unsigned int idx);
    void* internal() const { return internal_; }
    void* external(unsigned int idx) const { return reinterpret_cast<char*>(external_) + (page_sz_ * idx); }
    size_t page_size() const { return page_sz_; }

private:
    unsigned int pages_;
    size_t page_sz_;
    void* internal_;
    void* external_;
};

} /* namespace matmul */

#endif /* __cplusplus */

#endif // _MATMUL_PAGER_H_


