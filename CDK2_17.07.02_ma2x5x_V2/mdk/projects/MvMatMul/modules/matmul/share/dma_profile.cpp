///
/// @file
/// @copyright All code copyright Movidius Ltd 2012, all rights reserved.
///            For License Warranty see: common/license.txt
///

#include <cstdio>
#include <cstring>
#include "dma_profile.h"
#include "matmul_trace.h"

#ifdef DMA_PROFILE
#if !defined(__MOVICOMPILE__)
matmul::DmaProfilePOD __attribute__((section(".ddr_direct.data"))) MatMul0_dma_log[matmul::DMA_PROFILER_LOG_ITEMS];
matmul::DmaProfilePOD __attribute__((section(".ddr_direct.data"))) MatMul1_dma_log[matmul::DMA_PROFILER_LOG_ITEMS];
matmul::DmaProfilePOD __attribute__((section(".ddr_direct.data"))) MatMul2_dma_log[matmul::DMA_PROFILER_LOG_ITEMS];
matmul::DmaProfilePOD __attribute__((section(".ddr_direct.data"))) MatMul3_dma_log[matmul::DMA_PROFILER_LOG_ITEMS];
matmul::DmaProfilePOD __attribute__((section(".ddr_direct.data"))) MatMul4_dma_log[matmul::DMA_PROFILER_LOG_ITEMS];
matmul::DmaProfilePOD __attribute__((section(".ddr_direct.data"))) MatMul5_dma_log[matmul::DMA_PROFILER_LOG_ITEMS];
matmul::DmaProfilePOD __attribute__((section(".ddr_direct.data"))) MatMul6_dma_log[matmul::DMA_PROFILER_LOG_ITEMS];
matmul::DmaProfilePOD __attribute__((section(".ddr_direct.data"))) MatMul7_dma_log[matmul::DMA_PROFILER_LOG_ITEMS];
matmul::DmaProfilePOD __attribute__((section(".ddr_direct.data"))) MatMul8_dma_log[matmul::DMA_PROFILER_LOG_ITEMS];
matmul::DmaProfilePOD __attribute__((section(".ddr_direct.data"))) MatMul9_dma_log[matmul::DMA_PROFILER_LOG_ITEMS];
matmul::DmaProfilePOD __attribute__((section(".ddr_direct.data"))) MatMul10_dma_log[matmul::DMA_PROFILER_LOG_ITEMS];
matmul::DmaProfilePOD __attribute__((section(".ddr_direct.data"))) MatMul11_dma_log[matmul::DMA_PROFILER_LOG_ITEMS];
unsigned int __attribute__((section(".ddr_direct.data"))) MatMul0_dma_log_cnt;
unsigned int __attribute__((section(".ddr_direct.data"))) MatMul1_dma_log_cnt;
unsigned int __attribute__((section(".ddr_direct.data"))) MatMul2_dma_log_cnt;
unsigned int __attribute__((section(".ddr_direct.data"))) MatMul3_dma_log_cnt;
unsigned int __attribute__((section(".ddr_direct.data"))) MatMul4_dma_log_cnt;
unsigned int __attribute__((section(".ddr_direct.data"))) MatMul5_dma_log_cnt;
unsigned int __attribute__((section(".ddr_direct.data"))) MatMul6_dma_log_cnt;
unsigned int __attribute__((section(".ddr_direct.data"))) MatMul7_dma_log_cnt;
unsigned int __attribute__((section(".ddr_direct.data"))) MatMul8_dma_log_cnt;
unsigned int __attribute__((section(".ddr_direct.data"))) MatMul9_dma_log_cnt;
unsigned int __attribute__((section(".ddr_direct.data"))) MatMul10_dma_log_cnt;
unsigned int __attribute__((section(".ddr_direct.data"))) MatMul11_dma_log_cnt;
#else
extern matmul::DmaProfilePOD dma_log[matmul::DMA_PROFILER_LOG_ITEMS];
extern unsigned int dma_log_cnt;
#endif

namespace matmul
{

DmaProfiler& DmaProfiler::instance()
{
    static DmaProfiler instance_;
    return instance_;
}

DmaProfiler::~DmaProfiler()
{
}

void DmaProfiler::log()
{
    #if defined(__MOVICOMPILE__)
    unsigned int ts = trace_cpu_cycles();
    DmaProfilePOD& dst = dma_log[dma_log_cnt];
    DmaProfilePOD src(ts);
    memcpy(&dst, &src, sizeof(DmaProfilePOD));
    dma_log_cnt++;
    #endif
}

void DmaProfiler::log(void* ps, unsigned int sl, unsigned int ss,
    void* pd, unsigned int dl, unsigned int ds, unsigned int by)
{
    #if defined(__MOVICOMPILE__)
    unsigned int ts = trace_cpu_cycles();
    DmaProfilePOD& dst = dma_log[dma_log_cnt];
    DmaProfilePOD src(ts, ps, sl, ss, pd, dl, ds, by);
    memcpy(&dst, &src, sizeof(DmaProfilePOD));
    dma_log_cnt++;
    #endif
}

void DmaProfiler::print()
{
#if !defined(__MOVICOMPILE__)
    printf("DMA PROFILE ----------------------\n");
    if (MatMul0_dma_log_cnt) print_internal("SHV0", MatMul0_dma_log_cnt, MatMul0_dma_log);
    if (MatMul1_dma_log_cnt) print_internal("SHV1", MatMul1_dma_log_cnt, MatMul1_dma_log);
    if (MatMul2_dma_log_cnt) print_internal("SHV2", MatMul2_dma_log_cnt, MatMul2_dma_log);
    if (MatMul3_dma_log_cnt) print_internal("SHV3", MatMul3_dma_log_cnt, MatMul3_dma_log);
    if (MatMul4_dma_log_cnt) print_internal("SHV4", MatMul4_dma_log_cnt, MatMul4_dma_log);
    if (MatMul5_dma_log_cnt) print_internal("SHV5", MatMul5_dma_log_cnt, MatMul5_dma_log);
    if (MatMul6_dma_log_cnt) print_internal("SHV6", MatMul6_dma_log_cnt, MatMul6_dma_log);
    if (MatMul7_dma_log_cnt) print_internal("SHV7", MatMul7_dma_log_cnt, MatMul7_dma_log);
    if (MatMul8_dma_log_cnt) print_internal("SHV8", MatMul8_dma_log_cnt, MatMul8_dma_log);
    if (MatMul9_dma_log_cnt) print_internal("SHV9", MatMul9_dma_log_cnt, MatMul9_dma_log);
    if (MatMul10_dma_log_cnt) print_internal("SHV10", MatMul10_dma_log_cnt, MatMul10_dma_log);
    if (MatMul11_dma_log_cnt) print_internal("SHV11", MatMul11_dma_log_cnt, MatMul11_dma_log);
#endif
}

void DmaProfiler::print_internal(const char* msg, unsigned int cnt, DmaProfilePOD* arr)
{
    printf("%s --------------------------------\n", msg);
    printf("  log count = %d\n", cnt);
    printf("  cnt timestamp src src_line src_stride dst dst_line dst_stride bytes\n");
    for (unsigned int i = 0; i < cnt; i++)
        printf("  %03d %07d 0x%08X %03d %03d 0x%08X %03d %03d %05d\n"
                , i
                , arr[i].timestamp
                , (unsigned) arr[i].src
                , (unsigned) arr[i].src_line
                , (unsigned) arr[i].src_stride
                , (unsigned) arr[i].dst
                , (unsigned) arr[i].dst_line
                , (unsigned) arr[i].dst_stride
                , (unsigned) arr[i].bytes
                );
}

DmaProfiler::DmaProfiler()
{
    #if defined(__MOVICOMPILE__)
    dma_log_cnt = 0;
    #endif
}

} /* namespace matmul */

#endif

