///
/// @file
/// @copyright All code copyright Movidius Ltd 2012, all rights reserved.
///            For License Warranty see: common/license.txt
///

#ifndef _MATMUL_POOL_H_
#define _MATMUL_POOL_H_

#include "matmul_alloc.h"

#ifdef __cplusplus

namespace matmul
{

template <typename T0>
class PoolBase
{
public:
    PoolBase(MatMulAllocator* a)
        : alloc_(a), pool_cnt_(0), max_size_(0), pool_size_(0), pool_array_(0), map_id_(0)
    {
    }

    PoolBase(MatMulAllocator& a)
        : alloc_(&a), pool_cnt_(0), max_size_(0), pool_size_(0), pool_array_(0), map_id_(0)
    {
    }

    PoolBase(T0* idarr, void* tarr, size_t elems)
        : alloc_(0), pool_cnt_(elems), max_size_(elems), pool_size_(0), pool_array_(tarr), map_id_(idarr) {}

    virtual ~PoolBase(){}

    unsigned int pool_cnt() const { return pool_cnt_; }
    void* pool_array() const { return pool_array_; }
    T0* ids_array() const { return map_id_; }
    size_t pool_size() const { return pool_size_; }

    void reset()
    {
        pool_cnt_ = 0;
    }

    void resize(unsigned new_size)
    {
        assert(alloc_);
        assert(new_size <= max_size_);
        pool_cnt_ = new_size;
    }

    template <typename T>
    void reserve(size_t pool_elems)
    {
        assert(alloc_);
        T dummy;
        unsigned ids_sz = pool_elems * sizeof(T0);
        unsigned arr_sz = pool_elems * dummy.pod_size();
        pool_size_ = ids_sz + arr_sz;
        map_id_ = (T0*) alloc_->allocate(ids_sz, sizeof(T0));
        pool_array_ = alloc_->allocate(arr_sz, dummy.pod_aligment());
        pool_cnt_ = 0;
        max_size_ = pool_elems;
    }

    template <typename T>
    unsigned int push(T& item)
    {
        assert(alloc_ != 0);
        assert(pool_cnt_ < max_size_);

        char* ptr = &reinterpret_cast<char*>(pool_array_)[pool_cnt_ * item.pod_size()];
        map_id_[pool_cnt_] = item.id();
        item.serialize(ptr);
        pool_cnt_++;
        return pool_cnt_ - 1;
    }

    template <typename T>
    bool look_up_by_index(unsigned int idx, T& item)
    {
        bool ret = false;
        if (idx < pool_cnt_)
        {
            char* ptr = &reinterpret_cast<char*>(pool_array_)[idx * item.pod_size()];
            item.unserialize(ptr);
            ret = true;
        }
        return ret;
    }

    template <typename T>
    bool look_up_by_id(T0 id, T& item)
    {
        bool found = false;
        unsigned int idx;
        for (idx = 0; idx < pool_cnt_; idx++)
        {
            if (map_id_[idx] == id)
            {
                found = true;
                break;
            }
        }
        if (found)
        {
            char* ptr = &reinterpret_cast<char*>(pool_array_)[idx * item.pod_size()];
            item.unserialize(ptr);
        }
        return found;
    }

    bool is_id_in_pool(T0 id) const
    {
        bool found = false;
        unsigned int idx;
        for (idx = 0; idx < pool_cnt_; idx++)
        {
            if (map_id_[idx] == id)
            {
                found = true;
                break;
            }
        }
        return found;
    }

    unsigned int get_index_for_id(T0 id) const
    {
        unsigned int idx;
        for (idx = 0; idx < pool_cnt_; idx++)
        {
            if (map_id_[idx] == id)
                break;
        }
        return idx;
    }

    MatMulAllocator& allocator() { return *alloc_; };

private:
    MatMulAllocator* alloc_;
    unsigned int pool_cnt_;
    unsigned int max_size_;
    unsigned int pool_size_;
    void* pool_array_;
    T0* map_id_;
};

typedef PoolBase<unsigned int> MatMulPool;
typedef PoolBase<uint64_t> MatMulPool64;

} /* namespace matmul */

#endif /* __cplusplus */

#endif // _MATMUL_POOL_H_

