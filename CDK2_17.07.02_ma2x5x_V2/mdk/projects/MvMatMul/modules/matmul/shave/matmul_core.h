///
/// @file
/// @copyright All code copyright Movidius Ltd 2015, all rights reserved.
///            For License Warranty see: common/license.txt
///

#ifndef __MATMUL_CORE_H__
#define __MATMUL_CORE_H__

extern "C" void SHVMatGEMM(void* pmms, int type);
extern "C" void SHVMatGEMV(void* pmms, int type);

#endif //__MATMUL_CORE_H__

