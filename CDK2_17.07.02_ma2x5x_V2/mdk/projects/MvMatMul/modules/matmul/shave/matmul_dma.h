///
/// @file
/// @copyright All code copyright Movidius Ltd 2012, all rights reserved.
///            For License Warranty see: common/license.txt
///

#ifndef __MATMULDMA_HPP__
#define __MATMULDMA_HPP__

#include <swcCdma.h>
#include "matmul.h"

#ifdef __cplusplus

namespace matmul
{

struct DmaChannel
{
public:
    static const int MAX_TRANSACTIONS = 4;
    static const int TRANSACTION_LIST_HEAD = 0;

private:
    dmaTransactionList* pTransactions[DmaChannel::MAX_TRANSACTIONS];
    dmaTransactionList_t lTransactions[DmaChannel::MAX_TRANSACTIONS];

public:
    enum DmaTransferTypes
    {
        DMA_1D = 0,
        DMA_2D
    };

    DmaChannel(int channel, unsigned int dma_link_agent = 0);
    virtual ~DmaChannel();

    template <typename T>
    void transfer_submat(Tensor& dst, Tensor& src, DmaTransferTypes tt);
    void create(unsigned char* dst, unsigned char* src, int bytes,
                int dst_line = 0, int src_line = 0, int dst_stride = 0, int src_stride = 0);

    void link();
    void start();
    void wait();
    void reset();
    unsigned int transaction_cnt() { return m_transaction_cnt_; }

    bool m_transfer_active;
    int m_transaction;
    int m_reqId;
    unsigned int m_transaction_cnt_;
};

} /* namespace matmul */

#endif /* __cplusplus */

#endif //ifndef __MATMUL_HPP__

