#ifndef _MVMULAB_H_
#define _MVMULAB_H_
#include "mv_types.h"

#ifdef __PC__
#include "half.h"
#include "VectorTypes.h"
#include "builtinFunctions.h"
#else
#include <moviVectorUtils.h>
#endif // __PC__

#include "matmul_common.h"

extern "C"
{

/*
*  C = A * B
*
*      A is m * k
*      B is k * n
*      C is m * n
*
*      Matrixes are row-major order
*
*/
void mvtMulAB_hhhh_c(const half *A, const half *B, half *C, int m, int k, int n,
    int wA, int wB, int wC);
void mvtMulAB_hhhh_c_opt(const half *A, const half *B, half *C, int m, int k, int n,
    int wA, int wB, int wC);
void mvtMulAB_hhhh_c_ref(const float *A, const float *B, float *C, int m, int k, int n,
    int wA, int wB, int wC);

/*
*  C = A * B + bias
*
*      A is m * k
*      B is k * n
*      C is m * n
*      bias is m * n
*
*      Matrixes are row-major order
*
*/
void mvtMulABBias_hhhh_c(const half *A, const half *B, const half* bias, half *C, int m, int k, int n,
    int wA, int wB, int wBias, int wC);
void mvtMulABBias_hhhh_c_opt(const half *A, const half *B, const half* bias, half *C, int m, int k, int n,
    int wA, int wB, int wBias, int wC);
void mvtMulABBias_hhhh_asm(const half *A, const half *B, const half* bias, half *C, int m, int k, int n,
    int wA, int wB, int wBias, int wC);
void mvtMulABBias_hhhh_c_ref(const float *A, const float *B, const float* bias, float *C, int m, int k, int n,
    int wA, int wB, int wBias, int wC);


/*
*  C = CLAMP(A * B + bias, 0, X)
*
*      A is m * k
*      B is k * n
*      C is m * n
*      bias is m * n
*
*      Matrixes are row-major order
*
*/
void mvtMulABBias_hhhh_ReluX_c(const half *A, const half *B, const half* bias, half *C, int m, int k, int n,
    int wA, int wB, int wBias, int wC, half X);
void mvtMulABBias_hhhh_ReluX_c_opt(const half *A, const half *B, const half* bias, half *C, int m, int k, int n,
    int wA, int wB, int wBias, int wC, half X);
void mvtMulABBias_hhhh_ReluX_asm(const half *A, const half *B, const half* bias, half *C, int m, int k, int n,
    int wA, int wB, int wBias, int wC, half X);
void mvtMulABBias_hhhh_ReluX_c_ref(const float *A, const float *B, const float* bias, float *C, int m, int k, int n,
    int wA, int wB, int wBias, int wC, float X);


}


#endif // _MVMULAB_H_
