///
/// @file
/// @copyright All code copyright Movidius Ltd 2015, all rights reserved.
///            For License Warranty see: common/license.txt
///

#include <DrvRegUtilsDefines.h>
#include "matmul_trace.h"

namespace matmul
{

const int SVU_DCR = 0x1800;         // Debug Control Register
const int SVU_PC0 = 0x1830;         // Performance Counter 0
const int SVU_PCC0 = 0x1834;        // Performance Counter 0 Control Register
const int SVU_PC1 = 0x1838;         // Performance Counter 1
const int SVU_PCC1 = 0x183C;        // Performance Counter 1 Control Register
const int SVU_PC2 = 0x1844;         // Performance Counter 2
const int SVU_PCC2 = 0x1848;        // Performance Counter 2 Control Register

const int DEBUG_ENABLE_CFG = 0x1;
const int CYCLE_CNT_CFG = 0x4;
const int STALL_CNT_CFG = 0x3C0001;
const int IDC_STALL_CNT_CFG = 0x400001;

uint32_t trace_cpu_cycles()
{
	uint32_t regSVU_DCR, regSVU_PCCO;

	regSVU_DCR = GET_REG_WORD_VAL(SVU_DCR);
	regSVU_PCCO = GET_REG_WORD_VAL(SVU_PCC0);

	if( ((regSVU_DCR & DEBUG_ENABLE_CFG) == 0)
            || ((regSVU_PCCO & CYCLE_CNT_CFG) == 0) )
	{
		SET_REG_WORD(SVU_DCR, DEBUG_ENABLE_CFG);
		SET_REG_WORD(SVU_PCC0, CYCLE_CNT_CFG);
		SET_REG_WORD(SVU_PC0, 0);
	}

	return GET_REG_WORD_VAL(SVU_PC0);
}

uint32_t trace_lsu_stalls()
{
	uint32_t regSVU_DCR, regSVU_PCC1;
	regSVU_DCR = GET_REG_WORD_VAL(SVU_DCR);
	regSVU_PCC1 = GET_REG_WORD_VAL(SVU_PCC1);

	if( ((regSVU_DCR & DEBUG_ENABLE_CFG) == 0)
            || ((regSVU_PCC1 & STALL_CNT_CFG) == 0) )
	{
		SET_REG_WORD(SVU_DCR, DEBUG_ENABLE_CFG);
		SET_REG_WORD(SVU_PCC1, STALL_CNT_CFG);
		SET_REG_WORD(SVU_PC1, 0);
	}

	return GET_REG_WORD_VAL(SVU_PC1);
}

uint32_t trace_idc_stalls()
{
	uint32_t regSVU_DCR, regSVU_PCC2;
	regSVU_DCR = GET_REG_WORD_VAL(SVU_DCR);
	regSVU_PCC2 = GET_REG_WORD_VAL(SVU_PCC2);

	if( ((regSVU_DCR & DEBUG_ENABLE_CFG) == 0)
            || ((regSVU_PCC2 & IDC_STALL_CNT_CFG) == 0) )
	{
		SET_REG_WORD(SVU_DCR, DEBUG_ENABLE_CFG);
		SET_REG_WORD(SVU_PCC2, IDC_STALL_CNT_CFG);
		SET_REG_WORD(SVU_PC2, 0);
	}

	return GET_REG_WORD_VAL(SVU_PC2);
}

} /* namespace matmul */

