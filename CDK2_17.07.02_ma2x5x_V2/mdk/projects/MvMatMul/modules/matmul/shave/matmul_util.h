///
/// @file
/// @copyright All code copyright Movidius Ltd 2015, all rights reserved.
///            For License Warranty see: common/license.txt
///

#ifndef __MATMUL_UTIL_H__
#define __MATMUL_UTIL_H__

#ifdef __cplusplus

namespace matmul
{

void* convertToUncachedAddr(void * addr);

template <typename T>
void mv_pad_convolution(T* in, T* out, int oldH, int oldW, int newH, int newW)
{
    int i = 0;

    for (i = 0; i < oldH; ++i)
    {
        memcpy(&out[i*newW], &in[i*oldW],  oldW*sizeof(T));
        bzero(&out[(i*newW) + oldW],  (newW - oldW)*sizeof(T));
    }
    bzero(&out[newW*oldH],(newH-oldH)*(newW)*sizeof(T));
}

template <typename T>
void mv_unpad_convolution(T *in, T * out, int oldW, int oldH, int newW, int newH)
{
    int x;
    // unused argument !!!
    (void)oldH;
    for(x=0;x!=newH;x+=1)
    {
        memcpy(&out[x*newW],&in[x*oldW],newW*sizeof(T) );
    }
}

} /* namespace matmul */

#endif /* __cplusplus */

#endif //__MATMUL_UTIL_H__

