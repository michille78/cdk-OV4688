///
/// @file
/// @copyright All code copyright Movidius Ltd 2012, all rights reserved.
///            For License Warranty see: common/license.txt
///

#ifndef MATMUL_IPC_H_
#define MATMUL_IPC_H_

#ifdef __cplusplus

namespace matmul
{

struct IpcAgent
{
    static const int MAX_SLAVES = 12;

    enum IPC_MSG
    {
        SLAVE_START = 0,
        SLAVE_DONE
    };

    IpcAgent(int id);
    ~IpcAgent();

    void write_msg(int dst, int msg) const;
    int read_msg() const;
    int get_queue_fill_level() const;
    void wait_queue_fill_level(int level) const;
    void empty_queue() const;

    void register_slave(int first_shave, int shaves);
    void broadcast_msg(int msg) const;

    int m_id;
    int n_slaves;
    int m_slaves[MAX_SLAVES];
};

} /* namespace matmul */

#endif /* __cplusplus */

#endif // MATMUL_INTERNAL_H_

