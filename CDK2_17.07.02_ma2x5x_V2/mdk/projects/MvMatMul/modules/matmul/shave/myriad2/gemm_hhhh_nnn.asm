///
/// @file
/// @copyright All code copyright Movidius Ltd 2015, all rights reserved.
///            For License Warranty see: common/license.txt
///

; Code for GEMM kernel (C += A * B)
; Authors: radu.vanca@codec-art.com
; Number of instruction cycles: 19 + m*( 12 + n/8*( 11 + 9*k/8 ) )
; Number of stall cycles:       0
; Error from reference output: RMS = , MAX =
; Copyright 2015 Movidius
; Constraints:
;         n%8 = 0, n>=8
;         k%8 = 0, k>=8
;

.version 00.70.00

.code .text
.salign 16
;------------------------------------------------------------------------------------------------------------
;void matmul_acc_asm(const half *A, const half *B, half *C, int m, int k, int n, int wA, int wB, int wC);
;                        (i18)       (i17)          (i16)    (i15) (i14)  (i13)   (i12)   (i11)   (i10)
;------------------------------------------------------------------------------------------------------------
gemm_hhhh_nnn:
  ; ------ IRF usage ------
  .set A            i18
  .set B            i17
  .set C            i16
  .set m            i15
  .set k            i14
  .set n            i13
  .set wA           i12
  .set wB           i11
  .set wC           i10

  .set pA0          i0
  .set pB0          i1
  .set k_x_2        i2
  .set n_x_4        i3
  .set m_x_2        i4
  .set pB1          i5
  .set incr         i6
  .set temp         i7

  .set n_x_2        n
  .set row_out      wA ; overlaps with wA because wA is unused
  .set col_out      wB ; overlaps with wB because wB is unused
  .set common_dim   wC ; overlaps with wC because wC is unused

  ; ------ VRF usage ------
  .set c_half8      v0
  .set half8_out    v1
  .set zero         v2
  .set a0           v3
  .set a1           v4
  .set a2           v5
  .set a3           v6
  .set a4           v7
  .set a5           v8
  .set a6           v9
  .set a7           v10
  .set b0           v11
  .set b1           v12
  .set b2           v13
  .set b3           v14
  .set b4           v15
  .set b5           v16
  .set b6           v17
  .set b7           v18

  LSU0.LDIL incr, 16               || IAU.XOR row_out, row_out, row_out || CMU.CPZV zero, 0
  LSU0.LD.64.L c_half8, C          || LSU1.LDO.64.H c_half8, C, 8    || IAU.SHL n_x_2, n, 1
  LSU0.LD.64.L a0, A               || LSU1.LDO.64.H a0, A, 8         || IAU.ADDSU pB1, B, 8         || CMU.CPII pB0, B
  LSU0.LDI.64.L b0, pB0, n_x_2     || LSU1.LDI.64.H b0, pB1, n_x_2   || IAU.ADDSU pA0, A, incr
  LSU0.LDI.64.L b1, pB0, n_x_2     || LSU1.LDI.64.H b1, pB1, n_x_2   || IAU.ADD col_out, incr, 0
  LSU0.LDI.64.L b2, pB0, n_x_2     || LSU1.LDI.64.H b2, pB1, n_x_2   || IAU.SHL k_x_2, k, 1
  LSU0.LDI.64.L b3, pB0, n_x_2     || LSU1.LDI.64.H b3, pB1, n_x_2   || IAU.SHL n_x_4, n_x_2, 1
  LSU0.LDI.64.L b4, pB0, n_x_2     || LSU1.LDI.64.H b4, pB1, n_x_2   || IAU.ADD row_out, row_out, 1
  LSU0.LDI.64.L b5, pB0, n_x_2     || LSU1.LDI.64.H b5, pB1, n_x_2   || IAU.ADD common_dim, k, 0
  LSU0.LDI.64.L b6, pB0, n_x_2     || LSU1.LDI.64.H b6, pB1, n_x_2   || CMU.VILV.X16 a0, a4, a0, a0
  LSU0.LDI.64.L b7, pB0, n_x_2     || LSU1.LDI.64.H b7, pB1, n_x_2   || CMU.VILV.X32 a0, a2, a0, a0
  LSU0.LDI.64.L a0, pA0, incr      || LSU1.LDO.64.H a0, pA0, 8       || CMU.VILV.X32 a0, a1, a0, a0 || VAU.ACCPZ.F16 c_half8

; _matmul_acc_asm_loop labels (1) _matmul_acc_asm_loop_col_out, (2) _matmul_acc_asm_loop_common_dim and (3)_matmul_acc_asm_loop_row_out
.lalign
_matmul_acc_asm_loop:
  LSU0.LDI.64.L b0, pB0, n_x_2     || LSU1.LDI.64.H b0, pB1, n_x_2   || CMU.VILV.X32 a2, a3, a2, a2  || VAU.MACP.F16 a0, b0
  LSU0.LDI.64.L b1, pB0, n_x_2     || LSU1.LDI.64.H b1, pB1, n_x_2   || CMU.VILV.X32 a4, a6, a4, a4  || VAU.MACP.F16 a1, b1 || IAU.SUBSU common_dim, common_dim, 8
  LSU0.LDI.64.L b2, pB0, n_x_2     || LSU1.LDI.64.H b2, pB1, n_x_2   || BRU.BRA _matmul_acc_asm_loop || VAU.MACP.F16 a2, b2 || PEU.PCIX.NEQ 0x00
  LSU0.LDI.64.L b3, pB0, n_x_2     || LSU1.LDI.64.H b3, pB1, n_x_2   || CMU.VILV.X32 a4, a5, a4, a4  || VAU.MACP.F16 a3, b3
  LSU0.LDI.64.L b4, pB0, n_x_2     || LSU1.LDI.64.H b4, pB1, n_x_2                                   || VAU.MACP.F16 a4, b4
  LSU0.LDI.64.L b5, pB0, n_x_2     || LSU1.LDI.64.H b5, pB1, n_x_2   || CMU.VILV.X32 a6, a7, a6, a6  || VAU.MACP.F16 a5, b5
  LSU0.LDI.64.L b6, pB0, n_x_2     || LSU1.LDI.64.H b6, pB1, n_x_2   || CMU.VILV.X16 a0, a4, a0, a0  || VAU.MACP.F16 a6, b6
  LSU0.LDI.64.L b7, pB0, n_x_2     || LSU1.LDI.64.H b7, pB1, n_x_2   || CMU.VILV.X32 a0, a2, a0, a0  || VAU.MACP.F16 a7, b7
  LSU0.LDI.64.L a0, pA0, incr      || LSU1.LDO.64.H a0, pA0, 8       || CMU.VILV.X32 a0, a1, a0, a0  || VAU.MACPW.F16 half8_out, zero, zero
; ~_matmul_acc_asm_loop_common_dim
  LSU0.LD.64.L a0, A               || LSU1.LDO.64.H a0, A, 8         || IAU.ADDSU pB0, B, col_out
  LSU0.LDO.64.L c_half8, C, 16     || LSU1.LDO.64.H c_half8, C, 24   || IAU.ADDSU pB1, pB0, 8
  LSU0.LDI.64.L b0, pB0, n_x_2     || LSU1.LDI.64.H b0, pB1, n_x_2
  LSU0.LDI.64.L b1, pB0, n_x_2     || LSU1.LDI.64.H b1, pB1, n_x_2
  LSU0.LDI.64.L b2, pB0, n_x_2     || LSU1.LDI.64.H b2, pB1, n_x_2   || IAU.SUBSU  temp, n_x_2, col_out
  LSU0.LDI.64.L b3, pB0, n_x_2     || LSU1.LDI.64.H b3, pB1, n_x_2   || BRU.BRA _matmul_acc_asm_loop || PEU.PCIX.NEQ 0x00
  LSU0.LDI.64.L b4, pB0, n_x_2     || LSU1.LDI.64.H b4, pB1, n_x_2   || IAU.ADDSU pA0, A, 16
  LSU0.LDI.64.L b5, pB0, n_x_2     || LSU1.LDI.64.H b5, pB1, n_x_2   || CMU.VILV.X16 a0, a4, a0, a0   || IAU.ADD common_dim, k, 0
  LSU0.LDI.64.L b6, pB0, n_x_2     || LSU1.LDI.64.H b6, pB1, n_x_2   || CMU.VILV.X32 a0, a2, a0, a0
  LSU0.LDI.64.L b7, pB0, n_x_2     || LSU1.LDI.64.H b7, pB1, n_x_2   || IAU.ADD col_out, col_out, 16
  LSU0.LDI.64.L a0, pA0, incr      || LSU1.LDO.64.H a0, pA0, 8
  LSU0.STI.64.L half8_out, C, incr || LSU1.STO.64.H half8_out, C, 8  || CMU.VILV.X32 a0, a1, a0, a0  || VAU.ACCPZ.F16 c_half8
; ~_matmul_acc_asm_loop_col_out
  LSU0.LD.64.L c_half8, C          || LSU1.LDO.64.H c_half8, C, 24    || IAU.ADDSU A, A, k_x_2
  LSU0.LD.64.L a0, A               || LSU1.LDO.64.H a0, A, 8         || CMU.CPII pB0, B || IAU.ADDSU pB1, B, 8
  LSU0.LDI.64.L b0, pB0, n_x_2     || LSU1.LDI.64.H b0, pB1, n_x_2   || IAU.ADD col_out, incr, 0
  LSU0.LDI.64.L b1, pB0, n_x_2     || LSU1.LDI.64.H b1, pB1, n_x_2   || IAU.SUBSU temp, m, row_out
  LSU0.LDI.64.L b2, pB0, n_x_2     || LSU1.LDI.64.H b2, pB1, n_x_2   || BRU.BRA _matmul_acc_asm_loop || PEU.PCIX.NEQ 0x0
  LSU0.LDI.64.L b3, pB0, n_x_2     || LSU1.LDI.64.H b3, pB1, n_x_2   || IAU.ADD row_out, row_out, 1
  LSU0.LDI.64.L b4, pB0, n_x_2     || LSU1.LDI.64.H b4, pB1, n_x_2   || IAU.ADDSU pA0, A, incr
  LSU0.LDI.64.L b5, pB0, n_x_2     || LSU1.LDI.64.H b5, pB1, n_x_2   || IAU.ADD common_dim, k, 0
  LSU0.LDI.64.L b6, pB0, n_x_2     || LSU1.LDI.64.H b6, pB1, n_x_2   || CMU.VILV.X16 a0, a4, a0, a0
  LSU0.LDI.64.L b7, pB0, n_x_2     || LSU1.LDI.64.H b7, pB1, n_x_2   || CMU.VILV.X32 a0, a2, a0, a0
  LSU0.LDI.64.L a0, pA0, incr      || LSU1.LDO.64.H a0, pA0, 8       || CMU.VILV.X32 a0, a1, a0, a0 || VAU.ACCPZ.F16 c_half8
; _matmul_acc_asm_loop_row_out

  BRU.JMP i30
  NOP 6


