;
; @file
; @copyright All code copyright Movidius Ltd 2016, all rights reserved
;            For License Warranty see: mdk/common/license.txt
;
; Code for matmul kernel (C = CLAMPAB(C + A * B, 0, X))
; Number of instruction cycles: 22 + m*( 12 + n/8* ( 8 + 9*k/8 ) )
; Number of stall cycles:       0
; Error from reference output: RMS = , MAX =
; Constraints:
;         n%8 = 0, n>=8
;         k = 16
;

.version 00.70.00

.code .text
.salign 16
;------------------------------------------------------------------------------------------------------------
;void matmul_k16_ReluX_asm(const half *A, const half *B, half *C, int m, int k, int n, int wA, int wB, int wC);
;                        (i18)       (i17)          (i16)    (i15) (i14)  (i13)   (i12)   (i11)   (stack)
;------------------------------------------------------------------------------------------------------------
matmul_k16_ReluX_asm:
  ; ------ IRF usage ------
  .set A            i18
  .set B            i17
  .set C            i16
  .set m            i15
  .set k            i14
  .set n            i13
  .set wA           i12
  .set wB           i11
  .set wC           i10

  .set pA0          i0
  .set pB0          i1
  .set pC           i2  ; points to the next line of C
  .set pB1          i3
  .set incr         i4
  .set temp         i5

  .set n_x_2        n
  .set row_out      i6
  .set col_out      i7
  .set min_fp16     temp
  .set max_fp16     i8
  .set wB_goback    i9

  ; ------ VRF usage ------
  .set c_half8      v0
  .set half8_out    v1
  .set zero         v2
  .set a0           v3
  .set a1           v4
  .set a2           v5
  .set a3           v6
  .set a4           v7
  .set a5           v8
  .set a6           v9
  .set a7           v10
  .set b0           v11
  .set b1           v12
  .set b2           v13
  .set b3           v14
  .set b4           v15
  .set b5           v16
  .set b6           v17
  .set b7           v18
  .set v_min_fp16   v19
  .set v_max_fp16   v20
  .set a_rd         v21

  ; wB_goback = 16 - 15*wB
  LSU0.LDIL wB_goback, 16
  LSU0.LDO.16 max_fp16, i19, 4  || LSU1.LDIL min_fp16, 0 || CMU.CPII pC, C
  LSU0.LDIL incr, 16            || LSU1.LD.32 wC, i19           || IAU.XOR row_out, row_out, row_out || CMU.CPZV zero, 0
  LSU0.LDI.64.L c_half8, C, incr       || LSU1.LDO.64.H c_half8, C, 8  || IAU.SHL wB, wB, 1          || CMU.CPIVR.X16 v_min_fp16, min_fp16
  LSU0.LD.64.L a_rd, A          || LSU1.LDO.64.H a_rd, A, 8     || IAU.ADDSU pB1, B, 8               || CMU.CPII pB0, B
  LSU0.LDI.64.L b0, pB0, wB     || LSU1.LDI.64.H b0, pB1, wB    || IAU.ADDSU pA0, A, incr            || SAU.ADD.I32 wB_goback, wB_goback, wB
  LSU0.LDI.64.L b1, pB0, wB     || LSU1.LDI.64.H b1, pB1, wB    || IAU.ADD col_out, incr, 0          || SAU.SHL.X32 temp, wB, 4
  LSU0.LDI.64.L b2, pB0, wB     || LSU1.LDI.64.H b2, pB1, wB    || IAU.SHL wA, wA, 1
  LSU0.LDI.64.L b3, pB0, wB     || LSU1.LDI.64.H b3, pB1, wB    || IAU.ADD row_out, row_out, 1
  LSU0.LDI.64.L b4, pB0, wB     || LSU1.LDI.64.H b4, pB1, wB    || IAU.SHL wC, wC, 1
  LSU0.LDI.64.L b5, pB0, wB     || LSU1.LDI.64.H b5, pB1, wB    || IAU.SUB wB_goback, wB_goback, temp 
  LSU0.LDI.64.L b6, pB0, wB     || LSU1.LDI.64.H b6, pB1, wB    || CMU.VILV.X16 a0, a4, a_rd, a_rd || IAU.ADD pC, pC, wC
  LSU0.LDI.64.L b7, pB0, wB     || LSU1.LDI.64.H b7, pB1, wB    || CMU.VILV.X32 a0, a2, a0, a0 || IAU.SHL n_x_2, n, 1 
  LSU0.LDO.64.L a_rd, A, 16     || LSU1.LDO.64.H a_rd, A, 24    || CMU.VILV.X32 a0, a1, a0, a0 
  LSU1.LDIH wB_goback, 0                                        || CMU.CPIVR.X16 v_max_fp16, max_fp16      || VAU.ACCPZ.F16 c_half8
  
; _matmul_k16_ReluX_asm_loop labels (1) _matmul_k16_ReluX_asm_loop_col_out, (2) _matmul_k16_ReluX_asm_loop_common_dim and (3)_matmul_k16_ReluX_asm_loop_row_out
.lalign
_matmul_k16_ReluX_asm_loop:
  LSU0.LDI.64.L b0, pB0, wB    || LSU1.LDI.64.H b0, pB1, wB || CMU.VILV.X32 a2, a3, a2, a2                        || VAU.MACP.F16 a0, b0
  LSU0.LDI.64.L b1, pB0, wB    || LSU1.LDI.64.H b1, pB1, wB || CMU.VILV.X32 a4, a6, a4, a4                        || VAU.MACP.F16 a1, b1
  LSU0.LDI.64.L b2, pB0, wB    || LSU1.LDI.64.H b2, pB1, wB || CMU.VILV.X32 a4, a5, a4, a4                        || VAU.MACP.F16 a2, b2
  LSU0.LDI.64.L b3, pB0, wB    || LSU1.LDI.64.H b3, pB1, wB || CMU.VILV.X32 a6, a7, a6, a6                        || VAU.MACP.F16 a3, b3
  LSU0.LDI.64.L b4, pB0, wB    || LSU1.LDI.64.H b4, pB1, wB                                                       || VAU.MACP.F16 a4, b4
  LSU0.LDI.64.L b5, pB0, wB    || LSU1.LDI.64.H b5, pB1, wB                                                       || VAU.MACP.F16 a5, b5
  LSU0.LDI.64.L b6, pB0, wB    || LSU1.LDI.64.H b6, pB1, wB || CMU.VILV.X16 a0, a4, a_rd, a_rd                    || VAU.MACP.F16 a6, b6
  LSU0.LDI.64.L b7, pB0, wB_goback || LSU1.LDI.64.H b7, pB1, wB_goback || CMU.VILV.X32 a0, a2, a0, a0             || VAU.MACP.F16 a7, b7
  LSU0.LD.64.L a_rd, A         || LSU1.LDO.64.H a_rd, A, 8  || CMU.VILV.X32 a0, a1, a0, a0                        || VAU.MACP.F16 zero, zero

  LSU0.LDI.64.L b0, pB0, wB    || LSU1.LDI.64.H b0, pB1, wB || CMU.VILV.X32 a2, a3, a2, a2                        || VAU.MACP.F16 a0, b0
  LSU0.LDI.64.L b1, pB0, wB    || LSU1.LDI.64.H b1, pB1, wB || CMU.VILV.X32 a4, a6, a4, a4                        || VAU.MACP.F16 a1, b1
  LSU0.LDI.64.L b2, pB0, wB    || LSU1.LDI.64.H b2, pB1, wB || CMU.VILV.X32 a4, a5, a4, a4                        || VAU.MACP.F16 a2, b2
  LSU0.LDI.64.L b3, pB0, wB    || LSU1.LDI.64.H b3, pB1, wB || CMU.VILV.X32 a6, a7, a6, a6                        || VAU.MACP.F16 a3, b3
  LSU0.LDI.64.L b4, pB0, wB    || LSU1.LDI.64.H b4, pB1, wB                                                       || VAU.MACP.F16 a4, b4
  LSU0.LDI.64.L b5, pB0, wB    || LSU1.LDI.64.H b5, pB1, wB                                                       || VAU.MACP.F16 a5, b5
  LSU0.LDI.64.L b6, pB0, wB    || LSU1.LDI.64.H b6, pB1, wB || CMU.VILV.X16 a0, a4, a_rd, a_rd                    || VAU.MACP.F16 a6, b6
  LSU0.LDI.64.L b7, pB0, wB    || LSU1.LDI.64.H b7, pB1, wB || CMU.VILV.X32 a0, a2, a0, a0             || VAU.MACP.F16 a7, b7
  LSU0.LDO.64.L a_rd, A, 16    || LSU1.LDO.64.H a_rd, A, 24 || CMU.VILV.X32 a0, a1, a0, a0                        || VAU.MACPW.F16 half8_out, zero, zero

; ~_matmul_k16_ReluX_asm_loop_common_dim
  
  LSU0.LDI.64.L c_half8, C, incr  || LSU1.LDO.64.H c_half8, C, 8   || IAU.SUBSU  temp, n_x_2, col_out || CMU.CLAMPAB.F16 half8_out, v_min_fp16, v_max_fp16 
                                                                      IAU.SUB temp, col_out, incr     || PEU.PCIX.NEQ 0x00 || BRU.BRA _matmul_k16_ReluX_asm_loop   
  LSU0.STO.64.L half8_out, C, -48 || LSU1.STO.64.H half8_out, C, -40 || IAU.ADD col_out, col_out, 16  || PEU.PCIX.NEQ 0x30
  NOP 4
  VAU.ACCPZ.F16 c_half8
  
  
; ~_matmul_k16_ReluX_asm_loop_col_out
  
  LSU0.LDI.64.L c_half8, pC, wC || LSU1.LDO.64.H c_half8, pC, 8      || IAU.ADDSU A, A, wA
  LSU0.LD.64.L a_rd, A          || LSU1.LDO.64.H a_rd, A, 8          || IAU.ADDSU pB1, B, 8 || CMU.CPII pB0, B
  LSU0.LDI.64.L b0, pB0, wB     || LSU1.LDI.64.H b0, pB1, wB         || IAU.ADD col_out, incr, 0
  LSU0.LDI.64.L b1, pB0, wB     || LSU1.LDI.64.H b1, pB1, wB        
  LSU0.LDI.64.L b2, pB0, wB     || LSU1.LDI.64.H b2, pB1, wB         || IAU.SUBSU temp, m, row_out
  LSU0.LDI.64.L b3, pB0, wB     || LSU1.LDI.64.H b3, pB1, wB         || IAU.ADDSU pA0, A, incr  || BRU.BRA _matmul_k16_ReluX_asm_loop || PEU.PCIX.NEQ 0x0
  LSU0.LDI.64.L b4, pB0, wB     || LSU1.LDI.64.H b4, pB1, wB        
  LSU0.LDI.64.L b5, pB0, wB     || LSU1.LDI.64.H b5, pB1, wB        
  LSU0.LDI.64.L b6, pB0, wB     || LSU1.LDI.64.H b6, pB1, wB         || CMU.VILV.X16 a0, a4, a_rd, a_rd
  LSU0.LDI.64.L b7, pB0, wB     || LSU1.LDI.64.H b7, pB1, wB         || CMU.VILV.X32 a0, a2, a0, a0 || IAU.ADD row_out, row_out, 1
  LSU0.STO.64.L half8_out, C, -32 || LSU1.STO.64.H half8_out, C, -24 || CMU.VILV.X32 a0, a1, a0, a0 || IAU.SUBSU C, pC, wC
  LSU0.LDO.64.L a_rd, A, 16     || LSU1.LDO.64.H a_rd, A, 24         || IAU.ADD C, C, incr || VAU.ACCPZ.F16 c_half8
; _matmul_k16_ReluX_asm_loop_row_out

  BRU.JMP i30
  NOP 6


