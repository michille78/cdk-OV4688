;
; @file
; @copyright All code copyright Movidius Ltd 2016, all rights reserved
;            For License Warranty see: mdk/common/license.txt
;
; Code for mvtMulABBias ReluX kernel (C = A * B + bias)
; Number of instruction cycles: 27 + m*( 12 + n/8*( 11 + 9*k/8 ) )
; Number of stall cycles:       0
; Error from reference output: RMS = , MAX =
; Constraints:
;         n%8 = 0, n>=8
;         k%8 = 0, k>=8
;

.version 00.70.00

.code .text
.salign 16
;------------------------------------------------------------------------------------------------------------
;void mvtMulABBias_hhhh_ReluX_asm(const half *A, const half *B, const half *bias, half *C, int m, int k, int n, int wA, int wB, int wBias, int wC,   half X);
;                                  (i18)             (i17)          (i16)            (i15)  (i14)  (i13)   (i12)  (i11) (stack)   (stack)  (stack)  (stack)
;------------------------------------------------------------------------------------------------------------
mvtMulABBias_hhhh_ReluX_asm:

  ; ------ IRF usage ------
  .set A            i18
  .set B            i17
  .set bias         i16
  .set C            i15
  .set m            i14
  .set k            i13
  .set n            i12
  .set wA           i11
  .set wB           i10
  .set wC           i9

  .set pA0          i0
  .set pB0          i1
  .set pC           i2  ; points to the next line of C
  .set pB1          i3
  .set incr         i4
  .set temp         i5

  .set n_x_2        n
  .set row_out      i6
  .set col_out      i7
  .set common_dim   i8
  .set wBias        i20
  .set pBias        i21
  .set min_fp16     row_out
  .set max_fp16     common_dim

  ; ------ VRF usage ------
  .set bias_half8      v0
  .set half8_out    v1
  .set zero         v2
  .set a0           v3
  .set a1           v4
  .set a2           v5
  .set a3           v6
  .set a4           v7
  .set a5           v8
  .set a6           v9
  .set a7           v10
  .set b0           v11
  .set b1           v12
  .set b2           v13
  .set b3           v14
  .set b4           v15
  .set b5           v16
  .set b6           v17
  .set b7           v18
  .set v_min_fp16   v19
  .set v_max_fp16   v20

  LSU0.LD.32 wB, i19
  LSU0.STO.32  wBias, i19, -4
  LSU0.STO.32  pBias, i19, -8
  LSU0.LDO.32 wBias, i19, 4
  LSU0.LDO.16 max_fp16, i19, 12
  LSU0.LDIL incr, 16            || LSU1.LDIL min_fp16, 0             || CMU.CPZV zero, 0
  LSU1.LDO.32 wC, i19, 8        || IAU.XOR row_out, row_out, row_out || CMU.CPIVR.X16 v_min_fp16, min_fp16
  LSU0.LDI.64.L bias_half8, bias, incr || LSU1.LDO.64.H bias_half8, bias, 8 || IAU.ADDSU pB1, B, 8
  LSU0.LD.64.L a0, A            || LSU1.LDO.64.H a0, A, 8      || IAU.SHL wB, wB, 1               || CMU.CPII pB0, B
  LSU0.LDI.64.L b0, pB0, wB     || LSU1.LDI.64.H b0, pB1, wB   || IAU.ADDSU pA0, A, incr
  LSU0.LDI.64.L b1, pB0, wB     || LSU1.LDI.64.H b1, pB1, wB   || IAU.ADD col_out, incr, 0
  LSU0.LDI.64.L b2, pB0, wB     || LSU1.LDI.64.H b2, pB1, wB   || IAU.SHL wA, wA, 1
  LSU0.LDI.64.L b3, pB0, wB     || LSU1.LDI.64.H b3, pB1, wB   || IAU.ADD row_out, row_out, 1
  LSU0.LDI.64.L b4, pB0, wB     || LSU1.LDI.64.H b4, pB1, wB   || IAU.SHL wC, wC, 1
  LSU0.LDI.64.L b5, pB0, wB     || LSU1.LDI.64.H b5, pB1, wB   || IAU.SHL wBias, wBias, 1
  LSU0.LDI.64.L b6, pB0, wB     || LSU1.LDI.64.H b6, pB1, wB   || IAU.ADD pC, C, wC   || CMU.VILV.X16 a0, a4, a0, a0
  LSU0.LDI.64.L b7, pB0, wB     || LSU1.LDI.64.H b7, pB1, wB   || IAU.SHL n_x_2, n, 1 || CMU.VILV.X32 a0, a2, a0, a0
  LSU0.LDI.64.L a0, pA0, incr   || LSU1.LDO.64.H a0, pA0, 8    || CMU.VILV.X32 a0, a1, a0, a0 || VAU.ACCPZ.F16 bias_half8
  IAU.SUB pBias, bias, incr                                    || CMU.CPIVR.X16 v_max_fp16, max_fp16
  IAU.ADD pBias, pBias, wBias                                  || CMU.CPII common_dim, k

; _mvtMulABBias_hhhh_ReluX_asm_loop labels (1) _mvtMulABBias_hhhh_ReluX_asm_loop_col_out, (2) _mvtMulABBias_hhhh_ReluX_asm_loop_common_dim and (3)_mvtMulABBias_hhhh_ReluX_asm_loop_row_out
.lalign
_mvtMulABBias_hhhh_ReluX_asm_loop:
  LSU0.LDI.64.L b0, pB0, wB     || LSU1.LDI.64.H b0, pB1, wB   || CMU.VILV.X32 a2, a3, a2, a2  || VAU.MACP.F16 a0, b0
  LSU0.LDI.64.L b1, pB0, wB     || LSU1.LDI.64.H b1, pB1, wB   || CMU.VILV.X32 a4, a6, a4, a4  || VAU.MACP.F16 a1, b1 || IAU.SUBSU common_dim, common_dim, 8
  LSU0.LDI.64.L b2, pB0, wB     || LSU1.LDI.64.H b2, pB1, wB   || BRU.BRA _mvtMulABBias_hhhh_ReluX_asm_loop || VAU.MACP.F16 a2, b2 || PEU.PCIX.NEQ 0x00
  LSU0.LDI.64.L b3, pB0, wB     || LSU1.LDI.64.H b3, pB1, wB   || CMU.VILV.X32 a4, a5, a4, a4  || VAU.MACP.F16 a3, b3
  LSU0.LDI.64.L b4, pB0, wB     || LSU1.LDI.64.H b4, pB1, wB                                   || VAU.MACP.F16 a4, b4
  LSU0.LDI.64.L b5, pB0, wB     || LSU1.LDI.64.H b5, pB1, wB   || CMU.VILV.X32 a6, a7, a6, a6  || VAU.MACP.F16 a5, b5
  LSU0.LDI.64.L b6, pB0, wB     || LSU1.LDI.64.H b6, pB1, wB   || CMU.VILV.X16 a0, a4, a0, a0  || VAU.MACP.F16 a6, b6
  LSU0.LDI.64.L b7, pB0, wB     || LSU1.LDI.64.H b7, pB1, wB   || CMU.VILV.X32 a0, a2, a0, a0  || VAU.MACP.F16 a7, b7
  LSU0.LDI.64.L a0, pA0, incr   || LSU1.LDO.64.H a0, pA0, 8    || CMU.VILV.X32 a0, a1, a0, a0  || VAU.MACPW.F16 half8_out, zero, zero
; ~_mvtMulABBias_hhhh_ReluX_asm_loop_common_dim
  LSU0.LD.64.L a0, A            || LSU1.LDO.64.H a0, A, 8         || IAU.ADDSU pB0, B, col_out
  LSU0.LDI.64.L bias_half8, bias, incr  || LSU1.LDO.64.H bias_half8, bias, 8   || IAU.ADDSU pB1, pB0, 8
  LSU0.LDI.64.L b0, pB0, wB     || LSU1.LDI.64.H b0, pB1, wB
  LSU0.LDI.64.L b1, pB0, wB     || LSU1.LDI.64.H b1, pB1, wB
  LSU0.LDI.64.L b2, pB0, wB     || LSU1.LDI.64.H b2, pB1, wB   || IAU.SUBSU  temp, n_x_2, col_out
  LSU0.LDI.64.L b3, pB0, wB     || LSU1.LDI.64.H b3, pB1, wB   || BRU.BRA _mvtMulABBias_hhhh_ReluX_asm_loop || PEU.PCIX.NEQ 0x00
  LSU0.LDI.64.L b4, pB0, wB     || LSU1.LDI.64.H b4, pB1, wB   || IAU.ADDSU pA0, A, 16
  LSU0.LDI.64.L b5, pB0, wB     || LSU1.LDI.64.H b5, pB1, wB   || CMU.VILV.X16 a0, a4, a0, a0   || IAU.ADD common_dim, k, 0
  LSU0.LDI.64.L b6, pB0, wB     || LSU1.LDI.64.H b6, pB1, wB   || CMU.VILV.X32 a0, a2, a0, a0
  LSU0.LDI.64.L b7, pB0, wB     || LSU1.LDI.64.H b7, pB1, wB   || IAU.ADD col_out, col_out, 16  || CMU.CLAMPAB.F16 half8_out, v_min_fp16, v_max_fp16
  LSU0.LDI.64.L a0, pA0, incr      || LSU1.LDO.64.H a0, pA0, 8
  LSU0.STI.64.L half8_out, C, incr || LSU1.STO.64.H half8_out, C, 8  || CMU.VILV.X32 a0, a1, a0, a0  || VAU.ACCPZ.F16 bias_half8
; ~_mvtMulABBias_hhhh_ReluX_asm_loop_col_out
  LSU0.LDI.64.L bias_half8, pBias, wBias || LSU1.LDO.64.H bias_half8, pBias, 8     || IAU.ADDSU A, A, wA                || SAU.ADD.I32 bias, pBias, incr
  LSU0.LD.64.L a0, A            || LSU1.LDO.64.H a0, A, 8           || IAU.ADDSU pB1, B, 8        || CMU.CPII pB0, B || SAU.ADD.I32 C, pC, 0
  LSU0.LDI.64.L b0, pB0, wB     || LSU1.LDI.64.H b0, pB1, wB        || IAU.ADD col_out, incr, 0
  LSU0.LDI.64.L b1, pB0, wB     || LSU1.LDI.64.H b1, pB1, wB        || IAU.SUBSU temp, m, row_out
  LSU0.LDI.64.L b2, pB0, wB     || LSU1.LDI.64.H b2, pB1, wB        || IAU.ADDSU pC, pC, wC || BRU.BRA _mvtMulABBias_hhhh_ReluX_asm_loop || PEU.PCIX.NEQ 0x0
  LSU0.LDI.64.L b3, pB0, wB     || LSU1.LDI.64.H b3, pB1, wB        || IAU.ADD row_out, row_out, 1
  LSU0.LDI.64.L b4, pB0, wB     || LSU1.LDI.64.H b4, pB1, wB        || IAU.ADDSU pA0, A, incr
  LSU0.LDI.64.L b5, pB0, wB     || LSU1.LDI.64.H b5, pB1, wB        || IAU.ADD common_dim, k, 0
  LSU0.LDI.64.L b6, pB0, wB     || LSU1.LDI.64.H b6, pB1, wB        || CMU.VILV.X16 a0, a4, a0, a0
  LSU0.LDI.64.L b7, pB0, wB     || LSU1.LDI.64.H b7, pB1, wB        || CMU.VILV.X32 a0, a2, a0, a0
  LSU0.LDI.64.L a0, pA0, incr      || LSU1.LDO.64.H a0, pA0, 8      || CMU.VILV.X32 a0, a1, a0, a0 || VAU.ACCPZ.F16 bias_half8
; _mvtMulABBias_hhhh_ReluX_asm_loop_row_out

  LSU0.LDO.32  wBias, i19, -4
  LSU0.LDO.32  pBias, i19, -8
  BRU.JMP i30
  NOP 6


