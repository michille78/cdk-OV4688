///
/// @file
/// @copyright All code copyright Movidius Ltd 2012, all rights reserved.
///            For License Warranty see: common/license.txt
///

#include <stdio.h>
#include <string.h>
#include <assert.h>
#include "matmul_dma.h"

namespace matmul
{

void* convertToUncachedAddr(void * addr)
{
    if ((u32)addr & 0x80000000)
        addr = (void*)((u32)addr | 0x40000000);
    else // Assume CMX
        addr = (void*)((u32)addr | 0x08000000);
    return addr;
}

} /* namespace matmul */

