
/**************************************************************************************************

 @File         : main.c
 @Author       : MT
 @Brief        : Contains main debug control for application
 Date          : 01 - March - 2015
 E-mail        : xxx.xxx@movidius.com
 Copyright     : � Movidius Srl 2014, � Movidius Ltd 2015

 Description   : This file allow initialization and control of lrt pipes. Control is made
                 base on debuger interaction. Application can run on Pc or on movidius
                 MV182 board.
 **************************************************************************************************/

/**************************************************************************************************
 ~~~ Included types first then APIs from other modules
 **************************************************************************************************/
#include <stdlib.h>
#include "assert.h"
#include <time.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>
#include <signal.h>
#include <semaphore.h>
// project specific
#include "IcTypes.h"
#include "ipipe.h"
#include "ic_main.h"
#include "rtems.h"
#include "rtems_config.h"
#include "swcLeonUtils.h"
#include "UnitTestApi.h"

// contain all interface necessary for updating isp parameter
#include "IspCfgParamsUp.h"
// configuration headers
#include "app_config.h"
#ifdef MV0212
#include "SourceCfgParamMV0212.h"
#elif defined(MV0182)
#include "SourceCfgParam.h"
#endif

#include "IspCommon.h"
#ifdef POWER_MEASURE_ON
#include "PowerWatch.h"
#endif
#include "IspCfgInstanceApi.h"
#include "sendOutApi.h"

extern uint32_t frmCounterTotal[IPIPE_MAX_OUTPUTS_ALlOWED];

/**************************************************************************************************
 ~~~  Basic typdefs
 **************************************************************************************************/
// available debugger command allowed
typedef enum {
    DBG_NO_COMMAND          = 0,
    DBG_START_EXECUTION     = 1,
    DBG_SETUP_CAM           = 2,
    DBG_COMMIT_CFG          = 3,
    DBG_STOP_EXECUTION      = 4,
    DBG_START_CAM           = 5,
    DBG_STOP_CAM            = 6,
    DBG_LOCK_CAPTURE_CAM    = 7,
    DBG_LOCK_BY_CFG_CAPTURE = 8,
    DBG_LOCK_CAPTURE_BURST  = 9,
    DBG_CHANGE_RESOLUTION   = 10,
    DBG_CHANGE_FINIS_EXECUTION = 11,
} dbgCommand_t;


#ifndef DEFAULT_NR_OF_CAPTURE_IN_BURST_MODE
#define DEFAULT_NR_OF_CAPTURE_IN_BURST_MODE 8
#endif

#define MAX_NR_OF_ISP_CFG_INSTANCES  8

//this value is specific for the frame used here, NOT to be reused
#define FRAME_SIZE_CAM0 16516417

// thresholds for frame counter used in test cases
// numbers are smaller for output on MIPI because of mv214 board usage
#ifdef OUTPUT_UNIT_IS_HDMI
    #define T04_FRM_CTR_THRES_01  (300)
    #define T04_FRM_CTR_THRES_02  (800)
    #define T04_FRM_CTR_THRES_03  (600)
    #define T04_FRM_CTR_THRES_04  (400)
    #define T04_FRM_CTR_THRES_05  (200)
    #define T04_FRM_CTR_THRES_06  (200)
    #define T04_FRM_CTR_THRES_07 (2000)
#endif
#ifdef OUTPUT_UNIT_IS_MIPI
    #define T04_FRM_CTR_THRES_01  (250)
    #define T04_FRM_CTR_THRES_02  (400)
    #define T04_FRM_CTR_THRES_03  (230)
    #define T04_FRM_CTR_THRES_04  (150)
    #define T04_FRM_CTR_THRES_05  (140)
    #define T04_FRM_CTR_THRES_06   (40)
    #define T04_FRM_CTR_THRES_07 (1800)
#endif

/**************************************************************************************************
 ~~~  Local variables
 **************************************************************************************************/

// this will be set in order to config things different
// !!! debug control variable - use debugger set command in order to control them
dbgCommand_t            dbgCommand      = DBG_NO_COMMAND;
uint32_t                dbgCamId        = 0;
uint32_t                dbgCamCfgId     = IMX214_30FPS_CAMA_BINN;
uint32_t                dbgRes_vN_vD_hN_hD = ((1<<24)|(1<<16)|(1<<8)|(1<<0));
IspCfgInstanceType      dbgStilVdoSelect = ISP_CFG_INSTANCE_VIDEO;

//
pthread_t               assincronDebugCmdThread;
IspCfgInstance          *ispCfgInstances[MAX_NR_OF_ISP_CFG_INSTANCES];

uint32_t freeMemForSrc0, noOfZSLframesForCam0;
/**************************************************************************************************
 ~~~ Local File function declarations
 **************************************************************************************************/
// by this thread can be send different commands from debuger
static void* assincronDbgCmdThread(void *unused);
// demo different initialization and run sequence
void t02_3Cams_214Bining_208_208(void);
void t03_1Cams_214QuarterMode(void);
void t04_1Cams_214FULL(void);
void t05_3Cams_208_208_208(void);
void t06_1Cams_214Bining_RescaleTest(void);
void t08_1Cams_208_cif(void);
void t08_cifStartStop(void);
// will run dynamically different scenario. At some point you need to replace sensors as will be print
void tAllTest(void);

/**************************************************************************************************
 ~~~ Functions Implementation
 **************************************************************************************************/
// main function pc/board definition
void POSIX_Init (void *args)
{
    UNUSED(args);
    int rc = 0;
    initClocksAndMemory();
    // turn of unused power islands 12 shaves + usb
    DrvCprPowerTurnOffIsland(POWER_ISLAND_USB);
    for (rc = POWER_ISLAND_SHAVE_0; rc <= POWER_ISLAND_SHAVE_11; rc++)
        DrvCprPowerTurnOffIsland(rc);

    // create user interaction thread, it will read commands from the debugger
    if ((rc = pthread_create(&assincronDebugCmdThread, NULL, assincronDbgCmdThread, NULL)) != 0) {
        printf("pthread_create: %s\n", strerror(rc));
        exit(1);
    }
    sleep(1);
    memset((void*)ispCfgInstances, 0, sizeof(ispCfgInstances));

    sendOutCreate(&sendOut_initCfg);

    unitTestInit();
    tAllTest();
    //t02_3Cams_214Bining_208_208();
    //t04_1Cams_214FULL();
    //t03_1Cams_214QuarterMode();
    //t05_3Cams_208_208_208();
    //t08_1Cams_208_cif(); //
    //t08_cifStartStop();
    //t06_1Cams_214Bining_RescaleTest();
    pthread_join(assincronDebugCmdThread, NULL);

    unitTestFinalReport();
    exit(0);
}

int a = 0;
// debug thread
static void* assincronDbgCmdThread(void *unused) {
    UNUSED(unused);
    printf("Insert commands: \n");
    printf("set dbgCommand 1  - for DBG_START_EXECUTION \n");
    printf("set dbgCommand 2  - for DBG_STOP_EXECUTION  \n");
    printf("set dbgCommand 3  - for DBG_SETUP_CAM  \n");
    printf("set dbgCommand 4  - for DBG_COMMIT_CFG  \n");
    printf("set dbgCommand 5  - for DBG_START_CAM      \n");
    printf("set dbgCommand 6  - for DBG_STOP_CAM       \n");
    printf("set dbgCommand 7  - for DBG_LOCK_CAPTURE_CAM    \n");
    printf("set dbgCommand 8  - for DBG_LOCK_BY_CFG_CAPTURE \n");
    printf("set dbgCommand 9  - for DBG_LOCK_CAPTURE_BURST  \n");
    printf("set dbgCommand 10 - for DBG_CHANGE_RESOLUTION   \n");
    printf("\n set dbgCamId X  - select Source refer to  \n");
    printf(" set dbgCamCfgId X - See in SourceCfgParam.h available cfg.\n");
    printf(" set dbgEnableOutput X  - Out enable display bitmap selection.\n");
    printf(" set dbgStilVdoSelect 0/1 - select resize for video or still output.\n");
    printf(" set dbgRes_vN_vD_hN_hD X - scale factor change, 8 bit each.\n");

    while(1) {
        uint32_t i;
        dbgCommand = swcLeonReadNoCacheU32((uint32_t)&dbgCommand);
        dbgCamId = swcLeonReadNoCacheU32((uint32_t)&dbgCamId);
        dbgCamCfgId = swcLeonReadNoCacheU32((uint32_t)&dbgCamCfgId);
        switch(dbgCommand) {
        // individual start-stop support
        case DBG_START_EXECUTION:
            //searchAndBurnI2cConfigsByType(&sensorConfigurations[IMX214_30FPS_CAMA], CAM_STOP_STREAMING, NULL);
            //searchAndBurnI2cConfigsByType(&sensorConfigurations[IMX208_30FPS_CAMB], CAM_STOP_STREAMING, NULL);
            //searchAndBurnI2cConfigsByType(&sensorConfigurations[IMX208_30FPS_CAMBR_MONO], CAM_STOP_STREAMING, NULL);
            // this is just to ensure that the sensor are reset at start
            setAllGpioByType(sensorConfigurations[IMX214_30FPS_CAMA].gpioConfigDescriptor, RESET_PIN, 1); // assert reset
            setAllGpioByType(sensorConfigurations[IMX208_30FPS_CAMB].gpioConfigDescriptor, RESET_PIN, 1); // assert reset
            setAllGpioByType(sensorConfigurations[IMX208_30FPS_CAMBR_MONO].gpioConfigDescriptor, RESET_PIN, 1); // assert reset

            los_start(NULL);
            dbgCommand = DBG_NO_COMMAND;
            break;
        case DBG_STOP_EXECUTION:
            los_stop();
            //printf("\n sss \n");
            for (i = 0; i < MAX_NR_OF_ISP_CFG_INSTANCES; i++) {
                //printf("Cfg: %p\n",ispCfgInstances[i]);
                IspCfgInstanceDistroy(ispCfgInstances[i]);
            }
            printf("App was stopped. \n"); sleep(1);
            dbgCommand = DBG_NO_COMMAND;
            break;
        case DBG_SETUP_CAM:
            //printf("S %d, %d\n",dbgCamId,dbgCamCfgId);
            los_SetupSource(dbgCamId, &sourceSetup[dbgCamCfgId]);
            dbgCommand = DBG_NO_COMMAND;
            break;
        case DBG_COMMIT_CFG:
            los_SetupSourceCommit();
            dbgCommand = DBG_NO_COMMAND;
            break;
        case DBG_START_CAM:
            //printf("T %d, %d\n",dbgCamId,dbgCamCfgId);
            ispCfgInstances[dbgCamId] = IspCfgInstanceCreate(los_configIsp, los_ipipe_TriggerCapture, dbgCamId);
            los_ConfigureSource(dbgCamId, &srcCfg[dbgCamCfgId]);
            IspCfgInstanceTrigerNewRequest(ispCfgInstances[dbgCamId]);
            los_startSource(dbgCamId);
            startSensor(&sensorConfigurations[dbgCamCfgId]);
            dbgCommand = DBG_NO_COMMAND;
            break;
        case DBG_STOP_CAM:
            los_stopSource(dbgCamId);
            IspCfgInstanceDistroy(ispCfgInstances[dbgCamId]);
            searchAndBurnI2cConfigsByType(&sensorConfigurations[dbgCamCfgId], CAM_STOP_STREAMING, NULL);
            dbgCommand = DBG_NO_COMMAND;
            break;
        case DBG_LOCK_CAPTURE_CAM:
            los_ipipe_LockZSL(dbgCamId, 45, 1);
            dbgCommand = DBG_NO_COMMAND;
            break;
        case DBG_LOCK_BY_CFG_CAPTURE:
            IspCfgInstanceTrigerBurstMode(ispCfgInstances[dbgCamId], 1);
            dbgCommand = DBG_NO_COMMAND;
            break;
        case DBG_LOCK_CAPTURE_BURST:
            IspCfgInstanceTrigerBurstMode(ispCfgInstances[dbgCamId], DEFAULT_NR_OF_CAPTURE_IN_BURST_MODE);
            dbgCommand = DBG_NO_COMMAND;
            break;
        case DBG_CHANGE_RESOLUTION:
            dbgRes_vN_vD_hN_hD = swcLeonReadNoCacheU32((uint32_t)&dbgRes_vN_vD_hN_hD);
            dbgStilVdoSelect = swcLeonReadNoCacheU32((uint32_t)&dbgStilVdoSelect);
            IspCfgInstanceChangeResolution(ispCfgInstances[dbgCamId], dbgStilVdoSelect,
                    (dbgRes_vN_vD_hN_hD >> 24) & 0xFF,
                    (dbgRes_vN_vD_hN_hD >> 16) & 0xFF,
                    (dbgRes_vN_vD_hN_hD >>  8) & 0xFF,
                    (dbgRes_vN_vD_hN_hD >>  0) & 0xFF);
            dbgCommand = DBG_NO_COMMAND;
            break;
        case DBG_CHANGE_FINIS_EXECUTION:
            return NULL;
            break;
        default:
            break;
        }
        rtems_task_wake_after(40);

        //sleep(1);
    }
    return NULL;
}

static void Fatal_extension(Internal_errors_Source  the_source, bool is_internal,
        uint32_t the_error) {
    if (the_source != RTEMS_FATAL_SOURCE_EXIT)
        printk ("\nSource %d Internal %d Error %d\n", the_source, is_internal, the_error);
}



//=================================================================================================
//=================================================================================================
//          ic_main functions apear at start of file as exported. Will be proper implemented
//          by params isp configuration application. Prepared for Guzzi (Mms application)
//=================================================================================================
void inc_cam_ipipe_buff_locked(
        void *p_prv,
        void *userData,
        unsigned int sourceInstance,
        void *buffZsl,
        icTimestamp ts,
        unsigned int seqNo
) {
    UNUSED(p_prv);
    UNUSED(userData);
    UNUSED(ts);
    UNUSED(seqNo);
    // dummy, every time a frame is locked, automatically call trigger capture.
    // Normally can be keep and call at other time.
    // or multiple lock, for maybe different buffers, ....
    IspCfgInstanceTrigerNewStillRequest(ispCfgInstances[sourceInstance], buffZsl, 0);
}
void inc_cam_capture_ready( // this should have a source identification, in order to know source
        void *p_prv,
        unsigned int seqNo,
        void *p_cfg_prv
) {
    // capture was made. Nothing to do in this demo.
    UNUSED(p_prv);
    UNUSED(seqNo);
    UNUSED(p_cfg_prv);
}


// Count stats ready events For Isp 0 and 1 =======================================================
// Dummy version, here normally have to update new parameters updating base on statistics
//=================================================================================================
int incCamIspStartCount[6] = {0, 0, 0, 0, 0, 0};
int incCamIspEndCount  [6] = {0, 0, 0, 0, 0, 0};
int incCamCaptureStartCount[6] = {0, 0, 0, 0, 0, 0};
int incCamCaptureEndCount  [6] = {0, 0, 0, 0, 0, 0};

void incCamIspStart(int32_t ispInstance, int32_t seqNo, uint32_t userData) {
    UNUSED(seqNo);
    UNUSED(userData);
    assert(ispInstance< 6);
    assert(ispInstance>=0);
    incCamIspStartCount[ispInstance]++;
}
void incCamIspEnd(int32_t ispInstance, int32_t seqNo, uint32_t userData) {
    UNUSED(seqNo);
    UNUSED(userData);
    assert(ispInstance< 6);
    assert(ispInstance>=0);
    incCamIspEndCount[ispInstance]++;
}

// Update params for Isp 0 and 1 ==================================================================
// Dummy version, this thread is trigger by read frame end event, and will update params
//with last one, will not use other parameters same params all the time
// ================================================================================================
void incCamSendFrameEnd(int32_t sourceInstance, int32_t seqNo, uint64_t ts) {
    UNUSED(seqNo);
    UNUSED(ts);
    // send new configuration at every eof isp event
    incCamCaptureEndCount[sourceInstance]++;
    IspCfgInstanceTrigerNewRequest(ispCfgInstances[sourceInstance]);
}

void incCamSendFrameStart(int32_t sourceInstance, int32_t seqNo, uint64_t ts) {
    UNUSED(seqNo);
    UNUSED(ts);
    incCamCaptureStartCount[sourceInstance]++;
}

void inc_zsl_add_buff_result(
        uint32_t memFree,
        uint32_t totalNoOfZSLframes,
        icAddZSLStatusCode status)
{
    UNUSED(status);
    freeMemForSrc0 = memFree;
    noOfZSLframesForCam0 = totalNoOfZSLframes;
}

// Test scenarios available======================================================================
//
// ================================================================================================

void t02_3Cams_214Bining_208_208(void) {
    dbgCommand = DBG_START_EXECUTION;
    sleep(1);
    dbgCamId        = 0;
    dbgCamCfgId     = IMX214_30FPS_CAMA_BINN;
    sourceSetup[dbgCamCfgId].appSpecificInfo = 0;
    dbgCommand      = DBG_SETUP_CAM;
    sleep(1);
    dbgCamId        = 1;
    dbgCamCfgId     = IMX208_30FPS_CAMB;
    dbgCommand      = DBG_SETUP_CAM;
    sleep(1);
    dbgCamId        = 2;
    dbgCamCfgId     = IMX208_30FPS_CAMBR_MONO;
    dbgCommand      = DBG_SETUP_CAM;
    sleep(1);
    dbgCommand      = DBG_COMMIT_CFG;
    sleep(2);
    // start all cams
    dbgCamId        = 0;
    dbgCamCfgId     = IMX214_30FPS_CAMA_BINN;
    dbgCommand      = DBG_START_CAM;
    sleep(6);
    dbgCamId        = 1;
    dbgCamCfgId     = IMX208_30FPS_CAMB;
    dbgCommand      = DBG_START_CAM;
    sleep(6);
    dbgCamId        = 2;
    dbgCamCfgId     = IMX208_30FPS_CAMBR_MONO;
    dbgCommand      = DBG_START_CAM;
    sleep(6);

    // capture normal
    dbgEnableOutput = 64;
    dbgCamId        = 0;
    dbgCommand      = DBG_LOCK_CAPTURE_CAM;
    // capture by isp
    sleep(2);
    dbgCommand      = DBG_LOCK_BY_CFG_CAPTURE;
    sleep(2);
    // capture burst
    dbgCommand      = DBG_LOCK_CAPTURE_BURST;
    sleep(2);
    //enable again all outputs show
    dbgEnableOutput = 255;
#ifdef POWER_MEASURE_ON
    initPowerMeasurement();
#endif
}

void t06_1Cams_214Bining_RescaleTest(void) {
    // hack the lcd initialization, it is not dynamically reconfigured
    dbgCommand = DBG_START_EXECUTION;
    sleep(1);
    dbgCamId        = 0;
    dbgCamCfgId     = IMX214_30FPS_CAMA_BINN;
    sourceSetup[dbgCamCfgId].maxHorizN = 2; // allow until at 2x upscale
    sourceSetup[dbgCamCfgId].maxVertN  = 2;
    srcCfg[dbgCamCfgId].cropWindow.x1 = 16; // resolution tested will be 1920, with below scale
    sourceSetup[dbgCamCfgId].appSpecificInfo = 0;
    dbgCommand      = DBG_SETUP_CAM;
    sleep(1);
    dbgCommand      = DBG_COMMIT_CFG;
    sleep(2);
    // start all cams
    dbgCamId        = 0;
    dbgCamCfgId     = IMX214_30FPS_CAMA_BINN;
    dbgCommand      = DBG_START_CAM;
    sleep(10);

    dbgCamId           = 0;
    dbgRes_vN_vD_hN_hD = 0x01040104;
    dbgStilVdoSelect   = ISP_CFG_INSTANCE_VIDEO;
    dbgCommand = DBG_CHANGE_RESOLUTION;
    sleep(10);

    dbgRes_vN_vD_hN_hD = 0x02040204;
    dbgCommand = DBG_CHANGE_RESOLUTION;
    sleep(10);

    dbgRes_vN_vD_hN_hD = 0x03040304;
    dbgCommand = DBG_CHANGE_RESOLUTION;
    sleep(10);

    dbgRes_vN_vD_hN_hD = 0x04040404;
    dbgCommand = DBG_CHANGE_RESOLUTION;
    sleep(10);

    dbgRes_vN_vD_hN_hD = 0x05040504;
    dbgCommand = DBG_CHANGE_RESOLUTION;
    sleep(10);

    dbgRes_vN_vD_hN_hD = 0x06040604;
    dbgCommand = DBG_CHANGE_RESOLUTION;
    sleep(10);

    dbgRes_vN_vD_hN_hD = 0x07040704;
    dbgCommand = DBG_CHANGE_RESOLUTION;
    sleep(10);

    dbgRes_vN_vD_hN_hD = 0x08040804;
    dbgCommand = DBG_CHANGE_RESOLUTION;
    sleep(10);
    //enable again all outputs show
    // revert back changes source configs
    sourceSetup[dbgCamCfgId].maxHorizN = 1; // allow until at 2x upscale
    sourceSetup[dbgCamCfgId].maxVertN  = 1;
    srcCfg[dbgCamCfgId].cropWindow.x1  = 0;
    dbgEnableOutput = 255;
}

void t03_1Cams_214QuarterMode(void) {
    dbgCommand = DBG_START_EXECUTION;
    sleep(1);;
    dbgCamId        = 0;
    dbgCamCfgId     = IMX214_30FPS_CAMA;
    sourceSetup[dbgCamCfgId].appSpecificInfo = QUARTER_MODE_FOR_VIDEO_PIPE;
    dbgCommand      = DBG_SETUP_CAM;
    sleep(1);;
    dbgCommand      = DBG_COMMIT_CFG;
    sleep(1);;
    // start cams
    dbgCamId        = 0;
    dbgCamCfgId     = IMX214_30FPS_CAMA;
    dbgCommand      = DBG_START_CAM;
    sleep(1);
}

void t04_1Cams_214FULL(void) {
    dbgCommand = DBG_START_EXECUTION;
    sleep(1);;
    dbgCamId        = 0;
    dbgCamCfgId     = IMX214_30FPS_CAMA;
    sourceSetup[dbgCamCfgId].appSpecificInfo = 0;
    dbgCommand      = DBG_SETUP_CAM;
    sleep(1);;
    dbgCommand      = DBG_COMMIT_CFG;
    sleep(1);;
    // start cams
    dbgCamId        = 0;
    dbgCamCfgId     = IMX214_30FPS_CAMA;
    dbgCommand      = DBG_START_CAM;
    sleep(1);;
}

void t05_3Cams_208_208_208(void) {
    dbgCommand = DBG_START_EXECUTION;
    sleep(1);;
    dbgCamId        = 0;
    dbgCamCfgId     = IMX208_30FPS_CAMA;
    sourceSetup[dbgCamCfgId].appSpecificInfo = 0;
    dbgCommand      = DBG_SETUP_CAM;
    sleep(1);;
    dbgCamId        = 1;
    dbgCamCfgId     = IMX208_30FPS_CAMB;
    dbgCommand      = DBG_SETUP_CAM;
    sleep(1);;
    dbgCamId        = 2;
    dbgCamCfgId     = IMX208_30FPS_CAMBR_MONO;
    dbgCommand      = DBG_SETUP_CAM;
    sleep(1);;
    dbgCommand      = DBG_COMMIT_CFG;
    sleep(1);;
    // start all cams
    dbgCamId        = 0;
    dbgCamCfgId     = IMX208_30FPS_CAMA;
    dbgCommand      = DBG_START_CAM;
    sleep(1);;
    dbgCamId        = 1;
    dbgCamCfgId     = IMX208_30FPS_CAMB;
    dbgCommand      = DBG_START_CAM;
    sleep(1);;
    dbgCamId        = 2;
    dbgCamCfgId     = IMX208_30FPS_CAMBR_MONO;
    dbgCommand      = DBG_START_CAM;
    sleep(3);
    // capture normal
    //dbgEnableOutput = 64;
    //dbgCamId        = 0;
    //dbgCommand      = DBG_LOCK_CAPTURE_CAM;
    // capture by isp
    //sleep(2);
    //dbgCommand      = DBG_LOCK_BY_CFG_CAPTURE;
    //sleep(2);
    // capture burst
    //dbgCommand      = DBG_LOCK_CAPTURE_BURST;
    //sleep(2);
    //enable again all outputs show
    dbgEnableOutput = 255;
}

void t08_1Cams_208_cif(void) {
    dbgCommand = DBG_START_EXECUTION;
    sleep(1);;
    dbgCamId        = 0;
    dbgCamCfgId     = IMX208_30FPS_CAMA_CIF; //IMX208_30FPS_CAMBR_MONO_CIF IMX208_30FPS_CAMA_CIF IMX208_15FPS_CAMA; //IMX208_15FPS_CAMA_CIF IMX208_15FPS_CAMBR_MONO_CIF
    sourceSetup[dbgCamCfgId].appSpecificInfo = 0;
    dbgCommand      = DBG_SETUP_CAM;
    sleep(1);
    dbgCamId        = 1;
    dbgCamCfgId     = IMX208_30FPS_CAMB;
    dbgCommand      = DBG_SETUP_CAM;
    sleep(1);
    dbgCamId        = 2;
    dbgCamCfgId     = IMX208_30FPS_CAMBR_MONO_CIF;
    dbgCommand      = DBG_SETUP_CAM;
    sleep(1);
    dbgCommand      = DBG_COMMIT_CFG;
    sleep(1);
    dbgCamId        = 0;
    dbgCamCfgId     = IMX208_30FPS_CAMA_CIF;
    dbgCommand      = DBG_START_CAM;
    sleep(8);
    dbgCommand      = DBG_STOP_CAM;
    sleep(5);
    dbgCommand      = DBG_START_CAM;
    sleep(8);
    dbgCommand      = DBG_STOP_CAM;
    sleep(5);
    dbgCommand      = DBG_START_CAM;
    sleep(8);
    dbgCamId        = 1;
    dbgCamCfgId     = IMX208_30FPS_CAMB;
    dbgCommand      = DBG_START_CAM;
    sleep(8);
    dbgCommand      = DBG_STOP_CAM;
    sleep(5);
    dbgCommand      = DBG_START_CAM;
    sleep(8);
    dbgCamId        = 2;
    dbgCamCfgId     = IMX208_30FPS_CAMBR_MONO_CIF;
    dbgCommand      = DBG_START_CAM;
    sleep(8);
    dbgCommand      = DBG_STOP_CAM;
    sleep(5);
    dbgCommand      = DBG_START_CAM;
    sleep(8);
}

void t08_cifStartStop(void) {
    t08_1Cams_208_cif();
    dbgCommand = DBG_STOP_EXECUTION; sleep(5);
    t08_1Cams_208_cif();
}

void tAllTest(void)
{
    printf("\n\n Play 1 cams, imx214 quarter size video output. \n");
    frmCounterTotal[0] = 0;
    t03_1Cams_214QuarterMode();    sleep(8);
    los_addFrameToZSLBuff(0, 0);     //request info about ZSL buffer for Cam0
    sleep(1);
    printf("Current number of ZSL frames in buffer for cam0: %ld \n", noOfZSLframesForCam0);
    if(freeMemForSrc0 > FRAME_SIZE_CAM0){
        printf("Add 1 frame to ZSL buff for cam0. \n");
        los_addFrameToZSLBuff(0, 1);
        sleep(1);
        printf("Current number of ZSL frames in buffer for cam0: %ld \n", noOfZSLframesForCam0);
        sleep(5);
    }
    dbgCommand = DBG_STOP_EXECUTION; sleep(2);
    printf("Nr of frames produced %ld\n", frmCounterTotal[0]);
    unitTestAssert(frmCounterTotal[0] > T04_FRM_CTR_THRES_01);


    printf("\n\n Play 3 cams, imx214 binning, and 2 imx208. Still demo.\n");
    frmCounterTotal[0] = 0; frmCounterTotal[1] = 0; frmCounterTotal[2] = 0;
    t02_3Cams_214Bining_208_208(); sleep(8); dbgCommand = DBG_STOP_EXECUTION; sleep(2);
    printf("Nr of frames produced %ld - %ld - %ld\n", frmCounterTotal[0], frmCounterTotal[1], frmCounterTotal[2]);
    unitTestAssert(frmCounterTotal[0] > T04_FRM_CTR_THRES_02);
    unitTestAssert(frmCounterTotal[1] > T04_FRM_CTR_THRES_03);
    unitTestAssert(frmCounterTotal[2] > T04_FRM_CTR_THRES_04);

    //repeat 1st test (see bug 23222)
    printf("\n\n Play 1 cams, imx214 quarter size video output. \n");
    frmCounterTotal[0] = 0;
    t03_1Cams_214QuarterMode();    sleep(8); dbgCommand = DBG_STOP_EXECUTION; sleep(2);
    printf("Nr of frames produced %ld\n", frmCounterTotal[0]);
    unitTestAssert(frmCounterTotal[0] > T04_FRM_CTR_THRES_05);

    printf("\n\n Play 1 cams, imx214 full size. \n");
    frmCounterTotal[0] = 0;
    t04_1Cams_214FULL();           sleep(8); dbgCommand = DBG_STOP_EXECUTION; sleep(2);
    printf("Nr of frames produced %ld\n", frmCounterTotal[0]);
    unitTestAssert(frmCounterTotal[0] > T04_FRM_CTR_THRES_06);

    printf("\n\n Play 1 cams, imx214 binning, rescaling different factors. \n");
    frmCounterTotal[0] = 0;
    t06_1Cams_214Bining_RescaleTest(); sleep(8); dbgCommand = DBG_STOP_EXECUTION; sleep(2);
    printf("Nr of frames produced %ld\n", frmCounterTotal[0]);
    unitTestAssert(frmCounterTotal[0] > T04_FRM_CTR_THRES_07);

    //TODO: for unknown reason los crash after this scenario
    //printf("\n\n Play forever 3 cams, imx214 binning, and 2 imx208. Still demo.\n");
    //frmCounterTotal[0] = 0; frmCounterTotal[1] = 0; frmCounterTotal[2] = 0; frmCounterTotal[3] = 0; sleep(2);
    //t02_3Cams_214Bining_208_208(); sleep(12); dbgCommand = DBG_STOP_EXECUTION; sleep(2);
    //printf("Nr of frames produced %d - %d - %d\n", frmCounterTotal[0], frmCounterTotal[1], frmCounterTotal[2]);
    //unitTestAssert(frmCounterTotal[0] > 800); unitTestAssert(frmCounterTotal[1] > 600); unitTestAssert(frmCounterTotal[2] > 400);
    sleep(1);
    printf("\n");
    dbgCommand = DBG_CHANGE_FINIS_EXECUTION;
}
