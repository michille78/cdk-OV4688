t04FullIsp

Supported Platform
===================
Myriad2 - This example works on Myriad2 ma2150/ma2450 silicon

Overview
==========
Stream up to 3 sensors depending on Los debuger commands.
Isp output is sent over HDMI (the output is cropped for IMX214 sensor scenario).

Software description
=======================
                
 
Hardware needed
==================
- MV182 or MV212 development board
- HDMI cable connected to a monitor
Depending on selection command over debuger you will need different sensors connected
- MV201, containing 1 imx214 Color sensor Left position(J1), connected to CAM A
- MV200 board, containing 2 imx208 Mono sensors, connected to CAM B

Build
==================
Please type "make help" if you want to learn available targets.

To build the project please select one of the following scenarios:
- 3 cameras streaming: up to 3 sensors running, on 1080p resolution,
  (imx214 sensor is in binning mode and is cropped by sippRx filter).
  "make clean"
  for ma2150: "make all"
  for ma2150: "make all MV_SOC_REV=ma2450"
- 1 camera streaming: 1 imx214 sensor connected, at full 13Mp resolution, 28fps
  (on hdmi will be display just a corner af the image (uper left corner)).
  "make clean"
  "make all IMX214FULL=yes"

Setup
==================
Myriad2 - To run the application:
    - open terminal and type "make start_server"
    - open another terminal and type:
        - for running on MV0182: "make debug"
        - for running on MV0212: "make debug MV_SOC_REV=ma2450"

Expected output
==================
on HDMI, depending on selected dbgCommand (see below).

User interaction
==================

Available options (commands) will be printed in the terminal window where debugger is running.
Enter them using debugger in order to start application.
Insert commands: 
set dbgCommand 100  - for DBG_IND_START_EXECUTION 
set dbgCommand 101  - for DBG_IND_STOP_EXECUTION  
set dbgCommand 110  - for DBG_IND_START_CAM1      
set dbgCommand 111  - for DBG_IND_STOP_CAM1       
set dbgCommand 120  - for DBG_IND_START_CAM2      
set dbgCommand 121  - for DBG_IND_STOP_CAM2       
set dbgCommand 130  - for DBG_IND_START_CAM3      
set dbgCommand 131  - for DBG_IND_STOP_CAM3      

Explanations:
"set dbgCommand 100"
This will start Leon Rt, hdmi block initialization, and do all necesary initializations on Leon Rt side,
"set dbgCommand 101"
This command teardown the processor.

"set dbgCommand 110"
This  will start streaming imx214 sensor. 
"set dbgCommand 111"
This  will stop streaming imx214 sensor. 

"set dbgCommand 120"
This  will start streaming imx208 sensor. Not available in 13Mp 30fps scenario. 
"set dbgCommand 121"
This  will stop streaming imx208 sensor. Not available in 13Mp 30fps  scenario. 

"set dbgCommand 120"
This  will start streaming imx208 sensor. Not available in 13Mp 30fps scenario. 
"set dbgCommand 121"
This  will stop streaming imx208 sensor. Not available in 13Mp 30fps  scenario. 

In case of 3 camera streaming scenario, in order to select which one is displayed over Hdmi, there is one
extra command. This will just show the selected sensor(s), but in the background, all sensors are running with isp.
write in debugger terminal window:
"set dbgEnableOutput 1"  - to see just cam1
"set dbgEnableOutput 2"  - to see just cam2
"set dbgEnableOutput 4"  - to see just cam3

Another available command, in order to see output frame per second for each of the sensors:
"get fpsCams"

