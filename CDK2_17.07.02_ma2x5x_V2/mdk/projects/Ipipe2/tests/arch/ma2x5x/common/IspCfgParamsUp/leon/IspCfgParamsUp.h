/**************************************************************************************************

 @File         : IspCfgParamsUp.h
 @Author       : AL
 @Brief        : Containing Cofiguration params interface
 Date          : 01 - March - 2015
 E-mail        : xxx.xxx@movidius.com
 Copyright     : © Movidius Srl 2014, © Movidius Ltd 2015

 Description :

 **************************************************************************************************/
#ifndef _ISP_CFG_PARAMS_UP_H
#define _ISP_CFG_PARAMS_UP_H

/**************************************************************************************************
 ~~~ Included types first then APIs from other modules
 **************************************************************************************************/
#include <IcTypes.h>

/**************************************************************************************************
 ~~~  Exported Functions
 **************************************************************************************************/
//registerNewIspInstance(int32_t ispPipeIdx);
//sendNewStatistics(int32_t ispPipeIdx, void* configStructure)

void * getIspParams(int32_t ispPipeIdx);


#endif  /* _ISP_CFG_PARAMS_UP_H */
