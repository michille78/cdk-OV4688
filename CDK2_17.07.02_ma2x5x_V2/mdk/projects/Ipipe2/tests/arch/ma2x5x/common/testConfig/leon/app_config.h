
/**************************************************************************************************

 @File         : app_config.h
 @Author       : xx
 @Brief        : Application configuration Leon header
 Date          : 01 - May - 2015
 E-mail        : xxx.xxx@movidius.com
 Copyright     : � Movidius Srl 2014, � Movidius Ltd 2015

 Description :

 **************************************************************************************************/


#ifndef _APP_CONFIG_H_
#define _APP_CONFIG_H_


/**************************************************************************************************
 ~~~ Includes
 **************************************************************************************************/
#ifdef MV0212
#include "MV0212.h"
#else
#include "Board182Api.h"
#endif

/**************************************************************************************************
 ~~~  Specific #defines and types (typedef,enum,struct)
 **************************************************************************************************/
#ifndef DEFAULT_APP_CLOCK_KHZ
#define DEFAULT_APP_CLOCK_KHZ       480000
#endif
#define DEFAULT_OSC_CLOCK_KHZ       12000
#define BIGENDIANMODE               (0x01000786)

#ifdef MV0212
extern I2CM_Device * i2c0Handle;
extern I2CM_Device * i2c1Handle;
extern I2CM_Device * i2c2Handle;
#endif

/**************************************************************************************************
 ~~~  Exported Functions
 **************************************************************************************************/

// ----------------------------------------------------------------------------
/// Setup all the clock configurations needed by this application and also the ddr
///
/// @return    0 on success, non-zero otherwise
int initClocksAndMemory(void);


#endif // _APP_CONFIG_H_
