/**************************************************************************************************

 @File         : IspCfgParamsUp.c
 @Author       : AL
 @Brief        : Containing Cofiguration params interface
 Date          : 01 - March - 2015
 E-mail        : xxx.xxx@movidius.com
 Copyright     : © Movidius Srl 2014, © Movidius Ltd 2015

 Description :

 **************************************************************************************************/

/**************************************************************************************************
 ~~~ Included types first then APIs from other modules
 **************************************************************************************************/
#include <stdio.h>
#include <string.h> //memcpy
#include <Fp16Convert.h>
#include "isp_params_wrap.h"
#include "Opipe.h"
#include "OpipeApps.h"
#include "IspCommon.h"
#include "ipipeUtils.h"
#include "IspCfgParamsUp.h"
#include "IspCfgInstanceApi.h"

/**************************************************************************************************
 ~~~  Specific #defines
 **************************************************************************************************/
#define MAX_NR_OF_ISP_INSTANCES 8
/**************************************************************************************************
 ~~~  Local variables
 **************************************************************************************************/
static IspCfgInstance ispInstances[MAX_NR_OF_ISP_INSTANCES];
/**************************************************************************************************
 ~~~  Exported Functions
 **************************************************************************************************/
IspCfgInstance *IspCfgInstanceCreate(IspCfgInstanceCallback genIspCalback,
                                    IspCfgInstanceCallbackStill getStillIspCalback,
                                    uint32_t ispId)
{
    IspCfgInstance *ispCfgInstance = &ispInstances[ispId]; //malloc(sizeof(IspCfgInstance));
    ispCfgInstance->ispId              = ispId;
    ispCfgInstance->genIspCalback      = genIspCalback;
    ispCfgInstance->getStillIspCalback = getStillIspCalback;
    memset((void*)ispCfgInstance->scale, 1, sizeof(ispCfgInstance->scale));
    ispCfgInstance->nrFutureCapture = 0;
    return ispCfgInstance;
}

void IspCfgInstanceDistroy(IspCfgInstance *ispCfgInstance) {
    if(ispCfgInstance) {
        //free(ispCfgInstance);
        ispCfgInstance = NULL;
    }
}

void IspCfgInstanceChangeResolution(IspCfgInstance *ispCfgInstance, IspCfgInstanceType type,
        uint32_t vN, uint32_t vD, uint32_t hN, uint32_t hD) {
    ispCfgInstance->scale[type].vN = vN;
    ispCfgInstance->scale[type].vD = vD;
    ispCfgInstance->scale[type].hN = hN;
    ispCfgInstance->scale[type].hD = hD;
}

void IspCfgInstanceTrigerNewRequest(IspCfgInstance *ispCfgInstance) {
    icIspConfig *ispCfg = (icIspConfig*) getIspParams(ispCfgInstance->ispId);
    ispCfg->updnCfg0.vN   = ispCfgInstance->scale[ISP_CFG_INSTANCE_VIDEO].vN;
    ispCfg->updnCfg0.vD   = ispCfgInstance->scale[ISP_CFG_INSTANCE_VIDEO].vD;
    ispCfg->updnCfg0.hN   = ispCfgInstance->scale[ISP_CFG_INSTANCE_VIDEO].hN;
    ispCfg->updnCfg0.hD   = ispCfgInstance->scale[ISP_CFG_INSTANCE_VIDEO].hD;
    if(ispCfgInstance->nrFutureCapture) {
        ispCfg->pipeControl    = IC_PIPECTL_ZSL_LOCK;
        ispCfgInstance->nrFutureCapture--;
    }
    else {
        ispCfg->pipeControl    = 0;
    }
    ispCfg->pipeControl    |= IC_PIPECTL_MIPI_ENABLE;
    ispCfgInstance->genIspCalback((void*)ispCfg,ispCfgInstance->ispId);
}

void IspCfgInstanceTrigerBurstMode(IspCfgInstance *ispCfgInstance, uint32_t nrOfFrames) {
    ispCfgInstance->nrFutureCapture = nrOfFrames;
}

void IspCfgInstanceTrigerNewStillRequest(IspCfgInstance *ispCfgInstance,
        void* buff, uint32_t upperLayerFlag) {
    icIspConfig *ispCfg = (icIspConfig*) getIspParams(ispCfgInstance->ispId);
    ispCfg->updnCfg0.vN   = ispCfgInstance->scale[ISP_CFG_INSTANCE_STILL].vN;
    ispCfg->updnCfg0.vD   = ispCfgInstance->scale[ISP_CFG_INSTANCE_STILL].vD;
    ispCfg->updnCfg0.hN   = ispCfgInstance->scale[ISP_CFG_INSTANCE_STILL].hN;
    ispCfg->updnCfg0.hD   = ispCfgInstance->scale[ISP_CFG_INSTANCE_STILL].hD;
    ispCfgInstance->getStillIspCalback(buff,(void*)ispCfg, ispCfgInstance->ispId, upperLayerFlag);
}
