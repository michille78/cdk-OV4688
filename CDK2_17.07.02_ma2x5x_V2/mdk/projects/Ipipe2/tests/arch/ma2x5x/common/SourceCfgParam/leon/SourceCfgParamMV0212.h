///
/// @file
/// @copyright All code copyright Movidius Ltd 2014, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief
///

// 1: Includes
// ----------------------------------------------------------------------------
#ifndef CAMCONFIGPARAMETERS_H
#define CAMCONFIGPARAMETERS_H

#include "ipipe.h"
#if defined(__sparc)
#include "CamSensor.h"
#include "imx208_2L_1936x1096_Raw10_15Hz.h"
#include "imx214_2L_2104x1560_Raw10_30Hz.h"
#include "imx214_2L_4208x3120_Raw10_7d5Hz.h"
#include "imx214_2L_4208x3120_Raw10_15Hz.h"
#include "imx214_4L_4208x3120_Raw10_20Hz.h"
#include "imx214_4L_4208x3120_Raw10_30Hz.h"
#include "imx208_2L_1936x1096_Raw10_30Hz.h"
#include "imx208_2L_1936x1096_Raw10_60Hz.h"
#include "imx214_4L_2104x1560_Raw10_60Hz.h"
#include "DrvGpio.h"
#include "MV0212.h"
#define MV0182_MV0201_SENSOR_RST_GPIO        59
#define MV0182_MV0200_SENSOR_LEFT_RST_GPIO   56
#define MV0182_MV0200_SENSOR_RIGHT_RST_GPIO  15
#endif

// 2:  Source specific #defines and types  (typedef, enum, struct)
// ----------------------------------------------------------------------------


enum {
    IMX208_15FPS_CAMA        = 0,
    IMX208_15FPS_CAMB        = 1,
    IMX208_15FPS_CAMBR       = 2,
    IMX208_15FPS_CAMBR_MONO  = 3,
    IMX214_30FPS_CAMA_BINN   = 4,
    IMX214_7d5FPS_CAMA       = 5,
    IMX214_15FPS_CAMA        = 6,
    IMX214_20FPS_CAMA        = 7,
    IMX214_30FPS_CAMA        = 8,

    IMX208_30FPS_CAMA        = 9,
    IMX208_30FPS_CAMB        = 10,
    IMX208_30FPS_CAMBR       = 11,
    IMX208_30FPS_CAMBR_MONO  = 12,

    IMX208_60FPS_CAMA        = 13,
    IMX208_60FPS_CAMB        = 14,
    IMX208_60FPS_CAMBR       = 15,
    IMX208_60FPS_CAMBR_MONO  = 16,

    IMX214_60FPS_CAMA_BINN   = 17,

    IMX208_15FPS_CAMB_DOWN   = 18,
    IMX208_15FPS_CAMBR_MONO_DOWN  = 19,


    IMX208_30FPS_CAMA_CROP        = 20,
    IMX208_30FPS_CAMB_CROP        = 21,
    IMX208_30FPS_CAMBR_CROP       = 22,
    IMX208_30FPS_CAMBR_MONO_CROP  = 23,

    IMX208_15FPS_CAMA_CIF         = 24,
    IMX208_15FPS_CAMBR_MONO_CIF   = 25,
    IMX208_30FPS_CAMA_CIF         = 26,
    IMX208_30FPS_CAMBR_MONO_CIF   = 27,

    IMX214_30FPS_CAMA_BINN_1080   = 28,
    IMX208_30FPS_CAMB_1080        = 29,
    IMX208_30FPS_CAMBR_1080       = 30,
    IMX208_30FPS_CAMBR_MONO_1080  = 31,


    MAX_SENSOR_CONFIG             = 32
};


const GpioConfigDescriptor imx208CamAGpios[] =
{
        {
                .configType = RESET_PIN,
                .gpioNumber = MV0182_MV0201_SENSOR_RST_GPIO,
                .activeLevel = 1,
                .delayMs = 1,   // 3.41 at 24MHz = 8192 MCLK's
        },
        {
                .configType = RESET_PIN,
                .gpioNumber = MV0182_MV0201_SENSOR_RST_GPIO,
                .activeLevel = 0,
                .delayMs = 1,   // 3.41 at 24MHz = 8192 MCLK's
        },
        {
                .configType = END, 0, 0, 0
        },
};

const GpioConfigDescriptor imx208CamBlGpios[] =
{
        {
                .configType = RESET_PIN,
                .gpioNumber = MV0182_MV0200_SENSOR_RIGHT_RST_GPIO,
                .activeLevel = 1,
                .delayMs = 1,   // 3.41 at 24MHz = 8192 MCLK's
        },
        {
                .configType = RESET_PIN,
                .gpioNumber = MV0182_MV0200_SENSOR_RIGHT_RST_GPIO,
                .activeLevel = 0,
                .delayMs = 1,   // 3.41 at 24MHz = 8192 MCLK's
        },
        {
                .configType = END, 0, 0, 0
        },
};

const GpioConfigDescriptor imx208CamBrGpios[] =
{
        {
                .configType = RESET_PIN,
                .gpioNumber = MV0182_MV0200_SENSOR_LEFT_RST_GPIO,
                .activeLevel = 1,
                .delayMs = 1,   // 3.41 at 24MHz = 8192 MCLK's
        },
        {
                .configType = RESET_PIN,
                .gpioNumber = MV0182_MV0200_SENSOR_LEFT_RST_GPIO,
                .activeLevel = 0,
                .delayMs = 1,   // 3.41 at 24MHz = 8192 MCLK's
        },
        {
                .configType = END, 0, 0, 0
        },
};

const GpioConfigDescriptor imx214CamAGpios[] =
{
        {
                .configType = RESET_PIN,
                .gpioNumber = MV0182_MV0201_SENSOR_RST_GPIO,
                .activeLevel = 1,
                .delayMs = 2,   // 3.41 at 24MHz = 8192 MCLK's
        },
        {
                .configType = RESET_PIN,
                .gpioNumber = MV0182_MV0201_SENSOR_RST_GPIO,
                .activeLevel = 0,
                .delayMs = 2,   // 3.41 at 24MHz = 8192 MCLK's
        },
        {
                .configType = END, 0, 0, 0
        },
};


#if defined(__sparc)
SensorConfiguration sensorConfigurations[MAX_SENSOR_CONFIG] = {
        {&imx208_2L_1936x1096_RAW10_15Hz_camCfg , &i2c1Handle, 0, imx208CamAGpios},
        {&imx208_2L_1936x1096_RAW10_15Hz_camCfg , &i2c0Handle, 0, imx208CamBlGpios},
        {&imx208_2L_1936x1096_RAW10_15Hz_camCfg , &i2c0Handle, 0, imx208CamBrGpios},
        {&imx208_2L_1936x1096_RAW10_15Hz_camCfg , &i2c0Handle, 1, imx208CamBrGpios},

        {&imx214_2L_2104x1560_RAW10_30Hz_camCfg , &i2c1Handle, 0, imx214CamAGpios},

        {&imx214_2L_4208x3120_RAW10_7d5Hz_camCfg , &i2c1Handle, 0, imx214CamAGpios},
        {&imx214_2L_4208x3120_RAW10_15Hz_camCfg , &i2c1Handle, 0, imx214CamAGpios},
        {&imx214_4L_4208x3120_RAW10_20Hz_camCfg , &i2c1Handle, 0, imx214CamAGpios},
        {&imx214_4L_4208x3120_RAW10_30Hz_camCfg , &i2c1Handle, 0, imx214CamAGpios},
        // imx208 30fps
        {&imx208_2L_1936x1096_RAW10_30Hz_camCfg , &i2c1Handle, 0, imx208CamAGpios},
        {&imx208_2L_1936x1096_RAW10_30Hz_camCfg , &i2c0Handle, 0, imx208CamBlGpios},
        {&imx208_2L_1936x1096_RAW10_30Hz_camCfg , &i2c0Handle, 0, imx208CamBrGpios},
        {&imx208_2L_1936x1096_RAW10_30Hz_camCfg , &i2c0Handle, 1, imx208CamBrGpios},
        // imx208 60fps
        {&imx208_2L_1936x1096_RAW10_60Hz_camCfg , &i2c1Handle, 0, imx208CamAGpios},
        {&imx208_2L_1936x1096_RAW10_60Hz_camCfg , &i2c0Handle, 0, imx208CamBlGpios},
        {&imx208_2L_1936x1096_RAW10_60Hz_camCfg , &i2c0Handle, 0, imx208CamBrGpios},
        {&imx208_2L_1936x1096_RAW10_60Hz_camCfg , &i2c0Handle, 1, imx208CamBrGpios},

        {&imx214_2L_2104x1560_RAW10_60Hz_camCfg , &i2c1Handle, 0, imx214CamAGpios},

        {&imx208_2L_1936x1096_RAW10_15Hz_camCfg , &i2c0Handle, 0, imx208CamBlGpios},
        {&imx208_2L_1936x1096_RAW10_15Hz_camCfg , &i2c0Handle, 1, imx208CamBrGpios},

        //IMX208_30FPS_CAMA_CROP        = 19,
        //IMX208_30FPS_CAMB_CROP        = 20,
        //IMX208_30FPS_CAMBR_CROP       = 21,
        //IMX208_30FPS_CAMBR_MONO_CROP  = 22,
        {&imx208_2L_1936x1096_RAW10_30Hz_camCfg , &i2c1Handle, 0, imx208CamAGpios},
        {&imx208_2L_1936x1096_RAW10_30Hz_camCfg , &i2c0Handle, 0, imx208CamBlGpios},
        {&imx208_2L_1936x1096_RAW10_30Hz_camCfg , &i2c0Handle, 0, imx208CamBrGpios},
        {&imx208_2L_1936x1096_RAW10_30Hz_camCfg , &i2c0Handle, 1, imx208CamBrGpios},

        {&imx208_2L_1936x1096_RAW10_15Hz_camCfg , &i2c1Handle, 0, imx208CamAGpios},
        {&imx208_2L_1936x1096_RAW10_15Hz_camCfg , &i2c0Handle, 1, imx208CamBrGpios},

        {&imx208_2L_1936x1096_RAW10_30Hz_camCfg , &i2c1Handle, 0, imx208CamAGpios},
        {&imx208_2L_1936x1096_RAW10_30Hz_camCfg , &i2c0Handle, 1, imx208CamBrGpios},

 	    {&imx214_2L_2104x1560_RAW10_30Hz_camCfg , &i2c1Handle, 0, imx214CamAGpios},
        {&imx208_2L_1936x1096_RAW10_30Hz_camCfg , &i2c0Handle, 0, imx208CamBlGpios},
        {&imx208_2L_1936x1096_RAW10_30Hz_camCfg , &i2c0Handle, 0, imx208CamBrGpios},
        {&imx208_2L_1936x1096_RAW10_30Hz_camCfg , &i2c0Handle, 1, imx208CamBrGpios},
};
#endif

char IN_FILE1[1] = "";
char IN_FILE2[1] = "";
char IN_FILE3[1] = "";

// it is repeated 3 times, in order fit with sensor configuration, no difference between 30fps and 15 fos for mipi configuration
icSourceConfig srcCfg[MAX_SENSOR_CONFIG] = {
        //IMX208_15FPS_CAMA
        {{1936,1110},  {0,12,1936,1108}, IC_BAYER_FORMAT_GRBG, 10,   {IC_MIPI_CTRL_0, 2, 810, IC_IPIPE_RAW_10, 1, IC_SIPP_DEVICE0}, ""},
        //IMX208_15FPS_CAMB
        {{1936,1108},  {0,12,1936,1108}, IC_BAYER_FORMAT_GRBG, 10,   {IC_MIPI_CTRL_2, 2, 810, IC_IPIPE_RAW_10, 1, IC_SIPP_DEVICE1}, ""},
        //IMX208_15FPS_CAMBR
        {{1936,1108},  {0, 12,1936,1108}, IC_BAYER_FORMAT_GRBG, 10,   {IC_MIPI_CTRL_3, 2, 810, IC_IPIPE_RAW_10, 1, IC_SIPP_DEVICE2}, ""},
        //IMX208_15FPS_CAMBR_MONO
        {{1920,1108},  {0, 12,1936,1108}, IC_BAYER_FORMAT_GRBG, 10,   {IC_MIPI_CTRL_3, 2, 810, IC_IPIPE_RAW_10, 1, IC_SIPP_DEVICE2}, ""},
        //IMX214_30FPS_CAMA_BINN
        {{2104,1564},  {0, 468,1936,1564}, IC_BAYER_FORMAT_GRBG, 10,   {IC_MIPI_CTRL_0, 2, 1200, IC_IPIPE_RAW_10, 1, IC_SIPP_DEVICE0}, ""},
        //IMX214_7d5FPS_CAMA
        {{4208,3120},  {16, 8,4208,3128}, IC_BAYER_FORMAT_GRBG, 10,   {IC_MIPI_CTRL_0, 2, 600, IC_IPIPE_RAW_10, 1, IC_SIPP_DEVICE0}, ""},
        //IMX214_15FPS_CAMA
        {{4208,3120},  {16, 8,4208,3128}, IC_BAYER_FORMAT_GRBG, 10,   {IC_MIPI_CTRL_0, 2, 1200, IC_IPIPE_RAW_10, 1, IC_SIPP_DEVICE0}, ""},
        //IMX214_20FPS_CAMA
        {{4208,3120},  {16, 8,4208,3128}, IC_BAYER_FORMAT_GRBG, 10,   {IC_MIPI_CTRL_0, 4, 810, IC_IPIPE_RAW_10, 1, IC_SIPP_DEVICE0}, ""},
        //IMX214_30FPS_CAMA
        {{4208,3120},  {16, 4,4208,3124}, IC_BAYER_FORMAT_GRBG, 10,   {IC_MIPI_CTRL_0, 4, 1200, IC_IPIPE_RAW_10, 1, IC_SIPP_DEVICE0}, ""},
        //IMX208_30FPS_CAMA
        {{1936,1108},  {0,12,1936,1108}, IC_BAYER_FORMAT_GRBG, 10,   {IC_MIPI_CTRL_0, 2, 810, IC_IPIPE_RAW_10, 1, IC_SIPP_DEVICE0}, ""},
        //IMX208_30FPS_CAMB
        {{1936,1108},  {0,12,1936,1108}, IC_BAYER_FORMAT_GRBG, 10,   {IC_MIPI_CTRL_2, 2, 810, IC_IPIPE_RAW_10, 1, IC_SIPP_DEVICE1}, ""},
        //IMX208_30FPS_CAMBR
        {{1936,1108},  {0, 12,1936,1108}, IC_BAYER_FORMAT_GRBG, 10,   {IC_MIPI_CTRL_3, 2, 810, IC_IPIPE_RAW_10, 1, IC_SIPP_DEVICE2}, ""},
        //IMX208_30FPS_CAMBR_MONO
        {{1920,1108},  {0, 12,1936,1108}, IC_BAYER_FORMAT_GRBG, 10,   {IC_MIPI_CTRL_3, 2, 810, IC_IPIPE_RAW_10, 1, IC_SIPP_DEVICE2}, ""},
        //IMX208_60FPS_CAMA
        {{1936,1108},  {0,12,1936,1108}, IC_BAYER_FORMAT_GRBG, 10,   {IC_MIPI_CTRL_0, 2, 810, IC_IPIPE_RAW_10, 1, IC_SIPP_DEVICE0}, ""},
        //IMX208_60FPS_CAMB
        {{1936,1108},  {0,12,1936,1108}, IC_BAYER_FORMAT_GRBG, 10,   {IC_MIPI_CTRL_2, 2, 810, IC_IPIPE_RAW_10, 1, IC_SIPP_DEVICE1}, ""},
        //IMX208_60FPS_CAMBR
        {{1936,1108},  {0, 12,1936,1108}, IC_BAYER_FORMAT_GRBG, 10,   {IC_MIPI_CTRL_3, 2, 810, IC_IPIPE_RAW_10, 1, IC_SIPP_DEVICE2}, ""},
        //IMX208_60FPS_CAMBR_MONO
        {{1920,1108},  {0, 12,1936,1108}, IC_BAYER_FORMAT_GRBG, 10,   {IC_MIPI_CTRL_3, 2, 810, IC_IPIPE_RAW_10, 1, IC_SIPP_DEVICE2}, ""},
        //IMX214_60FPS_CAMA_BINN
        {{2104,1564},  {0, 468,1936,1564}, IC_BAYER_FORMAT_GRBG, 10,   {IC_MIPI_CTRL_0, 4, 1200, IC_IPIPE_RAW_10, 1, IC_SIPP_DEVICE0}, ""},

        //IMX208_15FPS_CAMB_DOWN
        {{1936,1108},  {328,12+68,328+1280,12+68+960}, IC_BAYER_FORMAT_GRBG, 10,   {IC_MIPI_CTRL_2, 2, 810, IC_IPIPE_RAW_10, 1, IC_SIPP_DEVICE1}, ""},
        //IMX208_15FPS_CAMBR_MONO_DOWN
        {{1920,1108},  {328,12+68,328+1280,12+68+960}, IC_BAYER_FORMAT_GRBG, 10,   {IC_MIPI_CTRL_3, 2, 810, IC_IPIPE_RAW_10, 1, IC_SIPP_DEVICE2}, ""},


        //IMX208_30FPS_CAMA_CROP        = 19,
        //IMX208_30FPS_CAMB_CROP        = 20,
        //IMX208_30FPS_CAMBR_CROP       = 21,
        //IMX208_30FPS_CAMBR_MONO_CROP  = 22,
        {{1296,972},  {0,12,1296,972+12}, IC_BAYER_FORMAT_GRBG, 10,   {IC_MIPI_CTRL_0, 2, 810, IC_IPIPE_RAW_10, 1, IC_SIPP_DEVICE0}, ""},
        {{1296,972},  {0,12,1296,972+12}, IC_BAYER_FORMAT_GRBG, 10,   {IC_MIPI_CTRL_2, 2, 810, IC_IPIPE_RAW_10, 1, IC_SIPP_DEVICE1}, ""},
        {{1296,972},  {0, 12,1296,972+12}, IC_BAYER_FORMAT_GRBG, 10,   {IC_MIPI_CTRL_3, 2, 810, IC_IPIPE_RAW_10, 1, IC_SIPP_DEVICE2}, ""},
        {{1296,972},  {0, 12,1296,972+12}, IC_BAYER_FORMAT_GRBG, 10,   {IC_MIPI_CTRL_3, 2, 810, IC_IPIPE_RAW_10, 1, IC_SIPP_DEVICE2}, ""},

        //IMX208_15FPS_CAMA_CIF
        {{1936,1108},  {0,0,1936,1108}, IC_BAYER_FORMAT_GRBG, 10,   {IC_MIPI_CTRL_0, 2, 810, IC_IPIPE_RAW_10, 1, IC_CIF0_DEVICE4}, ""},
        //IMX208_15FPS_CAMBR_MONO_CIF
        {{1936,1108},  {0, 0,1936,1108}, IC_BAYER_FORMAT_GRBG, 10,   {IC_MIPI_CTRL_3, 2, 810, IC_IPIPE_RAW_10, 1, IC_CIF1_DEVICE5}, ""},

        //IMX208_30FPS_CAMA_CIF
        {{1936,1108},  {0,12,1936,1108}, IC_BAYER_FORMAT_GRBG, 10,   {IC_MIPI_CTRL_0, 2, 810, IC_IPIPE_RAW_10, 1, IC_CIF0_DEVICE4}, ""},
        //IMX208_30FPS_CAMBR_MONO_CIF
        {{1936,1108},  {0,12,1936,1108}, IC_BAYER_FORMAT_GRBG, 10,   {IC_MIPI_CTRL_3, 2, 810, IC_IPIPE_RAW_10, 1, IC_CIF1_DEVICE5}, ""},

        //IMX214_30FPS_CAMA_BINN_1080
        {{2104,1564},  {16, 484,1936,1564}, IC_BAYER_FORMAT_GRBG, 10,   {IC_MIPI_CTRL_0, 2, 1200, IC_IPIPE_RAW_10, 1, IC_SIPP_DEVICE0}, ""},
        //IMX208_30FPS_CAMB_1080
        {{1936,1108},  {16,28,1936,1108}, IC_BAYER_FORMAT_GRBG, 10,   {IC_MIPI_CTRL_2, 2, 810, IC_IPIPE_RAW_10, 1, IC_SIPP_DEVICE1}, ""},
        //IMX208_30FPS_CAMBR_1080
        {{1936,1108},  {16,28,1936,1108}, IC_BAYER_FORMAT_GRBG, 10,   {IC_MIPI_CTRL_3, 2, 810, IC_IPIPE_RAW_10, 1, IC_SIPP_DEVICE2}, ""},
        //IMX208_30FPS_CAMBR_MONO_1080
        {{1920,1108},  {16, 28,1936,1108}, IC_BAYER_FORMAT_GRBG, 10,   {IC_MIPI_CTRL_3, 2, 810, IC_IPIPE_RAW_10, 1, IC_SIPP_DEVICE2}, ""},
};

#define CBPP 10 //bits per pixel

icSourceSetup sourceSetup[MAX_SENSOR_CONFIG] = {
        //IMX208_15FPS
        {1936, 1208, CBPP, (1936 * 1208), 1,1,1,1,0},
        {1936, 1208, CBPP, (1936 * 1208), 1,1,1,1,0},
        {1936, 1208, CBPP, (1936 * 1208), 1,1,1,1,0},
        {1936, 1208, CBPP, (1936 * 1208), 1,1,1,1,0},
        //IMX214_30FPS_CAMA_BINN
        {2104, 1564, CBPP, (2104 * 1564), 1,1,1,1,0},
        //IMX214
        {4208, 3140, CBPP, (4208 * 3140), 1,1,1,1,0},
        {4208, 3140, CBPP, (4208 * 3140), 1,1,1,1,0},
        {4208, 3140, CBPP, (4208 * 3140), 1,1,1,1,0},
        {4208, 3140, CBPP, (4208 * 3140), 1,1,1,1,0},
        //IMX208_30FPS_CAMA
        {1936, 1108, CBPP, (1936 * 1108), 1,1,1,1,0},
        {1936, 1108, CBPP, (1936 * 1108), 1,1,1,1,0},
        {1936, 1108, CBPP, (1936 * 1108), 1,1,1,1,0},
        {1936, 1108, CBPP, (1936 * 1108), 1,1,1,1,0},
        //IMX208_60FPS_CAMA
        {1936, 1108, CBPP, (1936 * 1108), 1,1,1,1,0},
        {1936, 1108, CBPP, (1936 * 1108), 1,1,1,1,0},
        {1936, 1108, CBPP, (1936 * 1108), 1,1,1,1,0},
        {1936, 1108, CBPP, (1936 * 1108), 1,1,1,1,0},
        //IMX214_60FPS_CAMA_BINN
        {2104, 1564, CBPP, (2104 * 1564), 1,1,1,1,0},
        //
        {1280, 960, CBPP, (1280 * 960), 1,2,1,2,0},
        {1280, 960, CBPP, (1280 * 960), 1,2,1,2,0},

        //IMX208_30FPS_CAMB_CROP        = 20,
        //IMX208_30FPS_CAMBR_CROP       = 21,
        //IMX208_30FPS_CAMBR_MONO_CROP  = 22,
        {1344, 980, CBPP, (1344 * 980), 1,1,1,1,0},
        {1344, 980, CBPP, (1344 * 980), 1,1,1,1,0},
        {1344, 980, CBPP, (1344 * 980), 1,1,1,1,0},
        {1344, 980, CBPP, (1344 * 980), 1,1,1,1,0},

        //IMX208_15FPS
        {1936, 1208, 16, (1936 * 1208), 1,1,1,1,0},
        {1936, 1208, 16, (1936 * 1208), 1,1,1,1,0},

        //IMX208_30FPS
        {1936, 1208, 16, (1936 * 1208), 1,1,1,1,0},
        {1936, 1208, 16, (1936 * 1208), 1,1,1,1,0},

        {2104, 1564, CBPP, (2104 * 1564), 1,1,1,1,0},
        {1936, 1108, CBPP, (1936 * 1108), 1,1,1,1,0},
        {1936, 1108, CBPP, (1936 * 1108), 1,1,1,1,0},
        {1936, 1108, CBPP, (1936 * 1108), 1,1,1,1,0},

};
#undef CBPP

#endif //CAMCONFIGPARAMETERS_H

