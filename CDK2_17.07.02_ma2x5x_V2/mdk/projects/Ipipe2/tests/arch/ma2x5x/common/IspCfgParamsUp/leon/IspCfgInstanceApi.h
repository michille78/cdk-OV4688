/**************************************************************************************************

 @File         : IspCfgParamsUp.h
 @Author       : AL
 @Brief        : Containing Cofiguration params interface
 Date          : 01 - March - 2015
 E-mail        : xxx.xxx@movidius.com
 Copyright     : © Movidius Srl 2014, © Movidius Ltd 2015

 Description :

 **************************************************************************************************/
#ifndef _ISP_CFG_INSTANCE_API_H
#define _ISP_CFG_INSTANCE_API_H

/**************************************************************************************************
 ~~~ Included types first then APIs from other modules
 **************************************************************************************************/
#include <IcTypes.h>

/**************************************************************************************************
 ~~~  Exported Functions
 **************************************************************************************************/

typedef void (*IspCfgInstanceCallback)(void *iconf, int ispIdx);
typedef void (*IspCfgInstanceCallbackStill)(void* buff, void *iconf,
        uint32_t srcIdx, uint32_t upperLayerFlag);

typedef enum {
    ISP_CFG_INSTANCE_VIDEO,
    ISP_CFG_INSTANCE_STILL,
    ISP_CFG_INSTANCE_MAX_TYPE
} IspCfgInstanceType;

typedef struct {
    uint8_t vN;
    uint8_t vD;
    uint8_t hN;
    uint8_t hD;
}IspCfgInstanceRescaleFactors;

typedef struct {
    uint32_t ispId;
    IspCfgInstanceRescaleFactors scale[ISP_CFG_INSTANCE_MAX_TYPE];
    uint32_t nrFutureCapture;
    IspCfgInstanceCallback genIspCalback;
    IspCfgInstanceCallbackStill getStillIspCalback;
}IspCfgInstance;

IspCfgInstance *IspCfgInstanceCreate(IspCfgInstanceCallback genIspCalback,
        IspCfgInstanceCallbackStill getStillIspCalback, uint32_t ispId);
void IspCfgInstanceDistroy(IspCfgInstance *ispCfgInstance);
void IspCfgInstanceChangeResolution(IspCfgInstance *ispCfgInstance, IspCfgInstanceType type,
        uint32_t vN, uint32_t vD, uint32_t hN, uint32_t hD);
void IspCfgInstanceTrigerNewRequest(IspCfgInstance *ispCfgInstance);
void IspCfgInstanceTrigerNewStillRequest(IspCfgInstance *ispCfgInstance,
        void* buff, uint32_t upperLayerFlag);
void IspCfgInstanceTrigerBurstMode(IspCfgInstance *ispCfgInstance, uint32_t nrOfFrames);

#endif  /* _ISP_CFG_INSTANCE_API_H */
