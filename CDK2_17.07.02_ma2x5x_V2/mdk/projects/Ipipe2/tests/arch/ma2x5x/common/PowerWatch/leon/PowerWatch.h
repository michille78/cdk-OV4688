/**************************************************************************************************

 @File         : PowerWatch.h
 @Author       : MT
 @Brief        : Contains Interface for capture power
 Date          : 01 - October - 2015
 E-mail        : xxx.xxx@movidius.com
 Copyright     : � Movidius Srl 2014, � Movidius Ltd 2015

 Description :


 **************************************************************************************************/

#ifndef _POWER_WATCH_H_
#define _POWER_WATCH_H_

/**************************************************************************************************
 ~~~ Includes
 **************************************************************************************************/
#include "IcTypes.h"
#include "ipipe.h"


/**************************************************************************************************
 ~~~  Exported Functions
 **************************************************************************************************/
void showPower(void);
void initPowerMeasurement(void);

#endif //_POWER_WATCH_H_
