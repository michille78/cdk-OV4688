/**************************************************************************************************

 @File         : ic_main.c
 @Author       : Florin Cotoranu
 @Brief        : Contains main debug control for application
 Date          : 13-April-2016
 E-mail        : florin.cotoranu@movidius.com
 Copyright     : � Movidius Srl 2016, � Movidius Ltd 2016

 Description :
    This file described all allowed message interaction with lrt pipes side

 **************************************************************************************************/

/**************************************************************************************************
 ~~~ Included types first then APIs from other modules
 **************************************************************************************************/
// stl
#include <stdint.h>
#include <stdlib.h>
#include <assert.h>
#include <time.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>
#include <signal.h>
#include <semaphore.h>
//project specific
#include "ipipe.h"
#include "ic_main.h"

#include "ipipeUtils.h"
#include "sendOutApi.h"
/**************************************************************************************************
 ~~~  Specific #defines
**************************************************************************************************/
#ifndef PRINT_LOS_SENT_MESSAGES
#define PRINT_LOS_SENT_MESSAGES(...)
#endif
#ifndef PRINT_LOS_RECEIVED_MESSAGES
#define PRINT_LOS_RECEIVED_MESSAGES(...)
#endif

#define DDR_BUFFER_ALOCATED_MEM_SIZE (4208*3140*78/8)



/*
 * Internal debug code, disabled by default.
 */
//#define ENABLE_DBG_LOCAL
#ifdef ENABLE_DBG_LOCAL
#define DBG_MAX_MESSAGE 8096
static volatile uint32_t dbgMsgBuf[DBG_MAX_MESSAGE];
static volatile uint32_t dbgMsgIdx = 0;
static inline void dbgMAddMes(uint32_t mesage) {
    dbgMsgBuf[dbgMsgIdx] = mesage;
    dbgMsgIdx++;
    assert(dbgMsgIdx < DBG_MAX_MESSAGE);
    //if(dbgMsgIdx == DBG_MAX_MESSAGE) {
    //    dbgMsgIdx = 0;
    //}
}
#else
#define dbgMAddMes(X)
#endif


uint32_t frmCounterTotal[IPIPE_MAX_OUTPUTS_ALlOWED];

/**************************************************************************************************
 ~~~  Local variables
**************************************************************************************************/
uint8_t ddrStaticAlocatedMemory[DDR_BUFFER_ALOCATED_MEM_SIZE] DDR_BUF_DATA;
static icCtrl       *ctrl = (icCtrl*)0;
static pthread_t	eventThread;
int32_t             ip_ready = 0;
sem_t               semWaitForLrtReady;
sem_t               semWaitForSourceCommit;
sem_t               semWaitForSourceReady;
sem_t               semWaitForSourceStoped;

/**************************************************************************************************
 ~~~ Imported Function Declaration
**************************************************************************************************/
// !!! TODO: on integration in different specific application this functions will be replace
// with application necessary functions. And will be added other in conformity with the necessity
// as response to other events sent by Lrt side !!!
// function implemented in other module that will take in consideration Ipipe events
extern void incCamSendFrameEnd(int32_t sourceInstance, int32_t seqNo, uint64_t ts);
extern void incCamSendFrameStart(int32_t sourceInstance, int32_t seqNo, uint64_t ts);
extern void incCamIspStart(int32_t ispInstance, int32_t seqNo, uint32_t userData);
extern void incCamIspEnd(int32_t ispInstance, int32_t seqNo, uint32_t userData);
extern void incCamSendOuputData(FrameT *dataBufStruct, uint32_t outputId);
extern void inc_cam_ipipe_buff_locked(
        void *p_prv,
        void *userData,
        unsigned int sourceInstance,
        void *buffZsl,
        icTimestamp ts,
        unsigned int seqNo
    );
void inc_cam_capture_ready( // this should have a source identification
        void *p_prv,
        unsigned int seqNo,
        void *p_cfg_prv
    );
void inc_zsl_add_buff_result(
        uint32_t memFree,
        uint32_t totalNoOfZSLframes,
        icAddZSLStatusCode status
);
/**************************************************************************************************
 ~~~ Local File function declarations
**************************************************************************************************/
static void *eventLoop(void *vCtrl);


// debug information about fps. Updating just when frame was produced "get fpsCams"
void updateFps(uint32_t outputId);
/**************************************************************************************************
 ~~~ Functions Implementation
**************************************************************************************************/
void los_start(void *arg)
{
    UNUSED(arg);
    PRINT_LOS_SENT_MESSAGES("%s:\n",__func__);
    int        rc;

    /// Initialize the semaphore for waiting for lrt ready
    if((rc = sem_init(&semWaitForLrtReady, 0, 0)) == -1)
        printf("pthread_create: %s\n", strerror(rc));
    ctrl = icSetup(1,(uint32_t)ddrStaticAlocatedMemory,sizeof(ddrStaticAlocatedMemory));

    //Create the Event thread.  This thread blocks waiting for events
    //from the IPIPE client API.
    if ((rc = pthread_create(&eventThread, NULL, eventLoop, (void*)ctrl)) != 0) {
        printf("pthread_create: %s\n", strerror(rc));
        exit(1);
    }
    // wait for lrt ready event, this function is blocking, waiting for that
    if(sem_wait(&semWaitForLrtReady) == -1) {
        printf("sem_wait error\n");
    }
    sem_destroy(&semWaitForLrtReady);

    sendOutInit();

    // printf lrt pipeline descrition
    DBG_DUMP_DOT_FILE_APP_GRAPH(ctrl);
}

void los_stop(void) {
    PRINT_LOS_SENT_MESSAGES("%s:\n",__func__);
    icTeardown((icCtrl*)ctrl);
    /* Stop the event loop thread and wait for it to exit */
    pthread_join(eventThread, NULL);
    sendOutFini();
}

void los_ConfigureSource(int srcIdx, icSourceConfig *sconf)
{
    PRINT_LOS_SENT_MESSAGES("%s:\n",__func__);
    icConfigureSource (ctrl, (icSourceInstance)srcIdx, sconf);
}

void los_SetupSource(int srcIdx, icSourceSetup *sconf) {
    PRINT_LOS_SENT_MESSAGES("%s:\n",__func__);
    icSetupSource (ctrl, (icSourceInstance)srcIdx, sconf);
}

void los_SetupSourceCommit(void) {
    int        rc;
    PRINT_LOS_SENT_MESSAGES("%s:\n",__func__);
    /// Initialize the semaphore for waiting for source commit response
    if((rc = sem_init(&semWaitForSourceCommit, 0, 0)) == -1)
        printf("pthread_create: %s\n", strerror(rc));
    icSetupSourcesCommit(ctrl);
    // wait for lrt ready event, this function is blocking, waiting for that
    if(sem_wait(&semWaitForSourceCommit) == -1) {
        printf("sem_wait error\n");
    }
    sem_destroy(&semWaitForSourceCommit);
}

void los_ipipe_LockZSL(uint32_t srcIdx, uint32_t frameSel, uint32_t enableTimeRelative) {
    PRINT_LOS_SENT_MESSAGES("%s:\n",__func__);
    icLockZSL (ctrl, (icSourceInstance) srcIdx, frameSel, (icLockZSLFlags) enableTimeRelative);
    //PROFILE_ADD(PROFILE_ID_LOS_LOCK_ZSL, 0, 0);
}

void los_ipipe_TriggerCapture(void* buff, void *iconf, uint32_t srcIdx, uint32_t notReleaseZslBuf) {
    PRINT_LOS_SENT_MESSAGES("%s:\n",__func__);
    icTriggerCapture (ctrl, (icSourceInstance) srcIdx, (FrameT*)buff,
            iconf, (icCaptureFlags) notReleaseZslBuf);
    //PROFILE_ADD(PROFILE_ID_LOS_TRIGGER_CAPTURE, buff, 0);
}

void los_configIsp(void *iconf, int ispIdx) {
    PRINT_LOS_SENT_MESSAGES("%s:\n",__func__);
    icConfigureIsp(ctrl, ispIdx, iconf);
}

int  los_startSource (int srcIdx) {
    int        rc;
    PRINT_LOS_SENT_MESSAGES("%s:\n",__func__);
    /// Initialize the semaphore for waiting for source commit response
    if((rc = sem_init(&semWaitForSourceReady, 0, 0)) == -1)
        printf("pthread_create: %s\n", strerror(rc));
    icStartSource(ctrl, (icSourceInstance)srcIdx);
    // wait for source to be ready. Otherwise will not be possible other
    // configuration
    if(sem_wait(&semWaitForSourceReady) == -1) {
        printf("sem_wait error\n");
    }
    sem_destroy(&semWaitForSourceReady);
    return 0;
}

int  los_stopSource (int srcIdx) {
    int        rc;
    PRINT_LOS_SENT_MESSAGES("%s:\n",__func__);
    /// Initialize the semaphore for waiting for source commit response
    if((rc = sem_init(&semWaitForSourceStoped, 0, 0)) == -1)
        printf("pthread_create: %s\n", strerror(rc));
    icStopSource(ctrl, (icSourceInstance)srcIdx);
    // wait for source to be stopped
    if(sem_wait(&semWaitForSourceStoped) == -1) {
        printf("sem_wait error\n");
    }
    sem_destroy(&semWaitForSourceStoped);
    return 0;
}

void los_dataWasSent (FrameT *dataBufStruct, uint32_t outputId, uint32_t frmType) {
    UNUSED(outputId);
    UNUSED(frmType);
    PRINT_LOS_SENT_MESSAGES("%s:\n",__func__);
    icDataReceived(ctrl, dataBufStruct);
}

void los_addFrameToZSLBuff(uint32_t srcIdx, uint32_t ZSLframesToAdd){
    PRINT_LOS_SENT_MESSAGES("%s:\n",__func__);
    icZSLAdd(ctrl, srcIdx, ZSLframesToAdd);
}



static void *eventLoop(void *vCtrl) {
    icEvent	ev;
    unsigned int     evno;
    icCtrl * lpCtrl = (icCtrl *)vCtrl;
    if(0 == vCtrl) {
        return NULL;
    }
    while (1) {
        if(!icGetEvent(lpCtrl, &ev)) {
            evno = ev.ctrl & IC_EVENT_CTRL_TYPE_MASK;
            dbgMAddMes(evno);
            switch (evno) {
                case IC_EVENT_TYPE_LEON_RT_READY:
                    PRINT_LOS_RECEIVED_MESSAGES("IC_EVENT_TYPE_LEON_RT_READY \n");
                    if(sem_post(&semWaitForLrtReady) == -1)  {/// Inform that lrt is ready
                        printf("sem_post error\n");
                    }
                    break;
                case IC_EVENT_TYPE_SETUP_SOURCES_RESULT:
                    PRINT_LOS_RECEIVED_MESSAGES("IC_EVENT_TYPE_CONFIG_SOURCES_RESULT \n");
                    if(sem_post(&semWaitForSourceCommit) == -1)  {/// Inform that lrt is ready
                        printf("sem_post error\n");
                    }
                    break;
                case IC_EVENT_TYPE_SOURCE_READY:
                    PRINT_LOS_RECEIVED_MESSAGES("IC_EVENT_TYPE_SOURCE_READY \n");
                    if(sem_post(&semWaitForSourceReady) == -1)  {/// Inform that lrt is ready
                        printf("sem_post error\n");
                    }
                    break;
                case IC_EVENT_TYPE_READOUT_START:
                    // not implemented
                    incCamSendFrameStart(ev.u.lineEvent.sourceInstance, ev.u.lineEvent.seqNo, ev.u.lineEvent.ts);
                    break;
                case IC_EVENT_TYPE_LINE_REACHED:
                    // not implemented
                    break;
                case IC_EVENT_TYPE_READOUT_END:
                    //PRINT_LOS_RECEIVED_MESSAGES("IC_EVENT_TYPE_READOUT_END \n");
                    // triger updating isp parameters is call at every eof event
                    incCamSendFrameEnd(ev.u.lineEvent.sourceInstance, ev.u.lineEvent.seqNo, ev.u.lineEvent.ts);
                    break;
                case IC_EVENT_TYPE_ISP_CONFIG_ACCEPTED:
                    //PRINT_LOS_RECEIVED_MESSAGES("IC_EVENT_TYPE_ISP_CONFIG_ACCEPTED \n");
                    break;
                case IC_EVENT_TYPE_ISP_START:
                    incCamIspStart(ev.u.ispEvent.ispInstance, ev.u.ispEvent.seqNo, (uint32_t)ev.u.ispEvent.userData);
                    //PRINT_LOS_RECEIVED_MESSAGES("IC_EVENT_TYPE_ISP_START \n");
                    break;
                case IC_EVENT_TYPE_STATS_READY:
                    //PRINT_LOS_RECEIVED_MESSAGES("IC_EVENT_TYPE_STATS_READY \n");
                    //incCamStatsReady(ev.u.statsReady.ispInstance, ev.u.statsReady.seqNo, (uint32_t)ev.u.statsReady.userData);
                    break;
                case IC_EVENT_TYPE_ISP_END:
                    //when query option will be available, it should have to be possible to interrogate
                    // the lrt source plug-ins and found is source have capture possibility and what
                    // is the isp Id is the associated with it. Until then will be hard-coded.
                    if(ev.u.ispEvent.ispInstance >= 3) {// it is a still isp event
                        // in this case, source Id associated with this isp is: (ev.u.ispEvent.ispInstance-3)
                        // probably this function need this parameter
                        inc_cam_capture_ready(
                                NULL,
                                ev.u.ispEvent.seqNo,
                                ev.u.ispEvent.userData
                            );
                    }
                    else {
                        // video isp end event
                        //

                    }
                    incCamIspEnd(ev.u.ispEvent.ispInstance, ev.u.ispEvent.seqNo, (uint32_t)ev.u.ispEvent.userData);
                    //PRINT_LOS_RECEIVED_MESSAGES("IC_EVENT_TYPE_ISP_END \n");
                    break;
                case IC_EVENT_TYPE_ZSL_LOCKED:
                    // not implemented
                    inc_cam_ipipe_buff_locked(
                            NULL,
                            ev.u.buffLockedZSL.userData,
                            ev.u.buffLockedZSL.sourceInstance,
                            ev.u.buffLockedZSL.buffZsl,
                            ev.u.buffLockedZSL.buffZsl->timestamp[0], // time stamp when zsl was arrived in memory
                            //ev.u.buffLockedZSL.buffZsl->timestamp[ev.u.buffLockedZSL.buffZsl->timestampNr-1] // time stamp when frame was processed
                            ev.u.buffLockedZSL.buffZsl->seqNo
                    );
                    break;
                case IC_EVENT_TYPE_ZSL_ADD_RESULT:
                    //send required fields to app
                    inc_zsl_add_buff_result(
                            ev.u.addZSLresult.memFree,
                            ev.u.addZSLresult.totalNoOfZSLframes,
                            ev.u.addZSLresult.status
                    );
                    break;
                case IC_EVENT_TYPE_SOURCE_STOPPED:
                    PRINT_LOS_RECEIVED_MESSAGES("IC_EVENT_TYPE_SOURCE_STOPPED \n");
                    if(sem_post(&semWaitForSourceStoped) == -1)  {/// Inform that lrt is ready
                        printf("sem_post error\n");
                    }
                    break;
                case IC_EVENT_TYPE_SEND_OUTPUT_DATA: {
                    //PRINT_LOS_RECEIVED_MESSAGES("IC_EVENT_TYPE_SEND_OUTPUT_DATA \n");
                    FrameT *frame;
                    uint32_t outId;
                    uint32_t frmType;
                  //uint32_t frmFmt; //unused?
                    frame   = ev.u.sentOutput.dataBufStruct;
                    // guzzi create a dependency between source id and output id, so here will be the source id
                    // output comming from an isp instance
                    if (ctrl->icPipelineDescription.icQueryOutput[ev.u.sentOutput.outputId]->attrs <= IC_OUTPUT_FRAME_DATA_TYPE_METADATA_STILL) {
                        outId  = ctrl->icPipelineDescription.icQueryOutput[ev.u.sentOutput.outputId]->dependentSources;
                        frmType = ctrl->icPipelineDescription.icQueryOutput[ev.u.sentOutput.outputId]->attrs;; //type is still, video ...
                    }
                    else {
                        // it is a app specific output type. In this case as not directly dependent output id by source is not supported,
                        // will be made a hack, depth output will be register as  output id 2, FRAME_DATA_TYPE_STILL, FRAME_DATA_FORMAT_YUV420
                        outId = 2;
                      //frmFmt = FRAME_T_FORMAT_YUV420;
                        frmType = IC_OUTPUT_FRAME_DATA_TYPE_STILL;
                    }
                    sendOutSend(frame, outId, frmType, los_dataWasSent);
                    frmCounterTotal[ev.u.sentOutput.outputId]++;
                    updateFps(ev.u.sentOutput.outputId);
                    dbgMAddMes(ev.u.sentOutput.outputId | 0x10000000);
                    }
                    break;
                case IC_EVENT_TYPE_TORN_DOWN:
                    PRINT_LOS_RECEIVED_MESSAGES("IC_EVENT_TYPE_TORN_DOWN \n");
                    //printf("tm: %llu  \n",lpCtrl->curTime);
                    return NULL;
                    break;
                default:
                    PRINT_LOS_RECEIVED_MESSAGES ("%s: Unhandled evt %d:\n", __func__, evno);
                    break;
            }
        }
        else {
            // no activity in last period, lrt crash
            // or lrt is started, but no camera connected
            // in this case cut down lrt
            printf("Error X\n");
        }
    }
    return NULL;
}

//=================================================================================================
//=================================================================================================
//         debug information. what is the fps for each output.
//          moviDebug command: "get fpsCams"
//=================================================================================================
float fpsCams[IPIPE_MAX_OUTPUTS_ALlOWED];
uint64_t oldTime[IPIPE_MAX_OUTPUTS_ALlOWED];
uint32_t frmContor[IPIPE_MAX_OUTPUTS_ALlOWED];

void updateFps(uint32_t outputId) {

    struct timespec tp;
    clock_gettime(CLOCK_REALTIME, &tp);
    (frmContor[outputId])++;
    uint64_t crtTime = (uint64_t)((uint64_t)tp.tv_nsec/1000 + (uint64_t)tp.tv_sec * 1000000);
    uint64_t difTime = crtTime - oldTime[outputId];
    if(0 == oldTime[outputId]) {
        oldTime[outputId] = crtTime;
        return;
    }
    if(difTime>5000000) {
        fpsCams[outputId] = (float)frmContor[outputId]/5.0f;
        frmContor[outputId] = 0;
        oldTime[outputId] = crtTime;
    }
}
