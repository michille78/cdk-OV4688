/**************************************************************************************************

 @File         : ic_main.h
 @Author       : Florin Cotoranu
 @Brief        : Contains main debug control for application
 Date          : 13-April-2016
 E-mail        : florin.cotoranu@movidius.com
 Copyright     : � Movidius Srl 2016, � Movidius Ltd 2016

 Description :
    This file described all allowed message interaction with lrt pipes side

 **************************************************************************************************/

#ifndef IC_MAIN_H_
#define IC_MAIN_H_

/**************************************************************************************************
 ~~~ Includes
 **************************************************************************************************/
#include "ipipe.h"

#if defined(__sparc)
    #define ALIGNED(x) __attribute__((aligned(x)))
    #define DDR_BUF_DATA     __attribute__((section(".ddr.bss")))
#else // PC world
    #define ALIGNED(x) //nothing
    #define DDR_BUF_DATA
#endif
/**************************************************************************************************
 ~~~  Exported Functions
 **************************************************************************************************/
void los_start(void *);
void los_stop(void);
void los_SetupSource(int srcIdx, icSourceSetup *sconf);
void los_SetupSourceCommit(void);
void los_ConfigureSource(int srcIdx, icSourceConfig *sconf);
int  los_startSource (int srcIdx);
int  los_stopSource (int srcIdx);
void los_configIsp(void *iconf, int ispIdx);

//
void los_ipipe_LockZSL(uint32_t srcIdx, uint32_t frameSel, uint32_t enableTimeRelative) ;
//
void los_ipipe_TriggerCapture(void* buff, void *iconf, uint32_t srcIdx, uint32_t notReleaseZslBuf);
void los_addFrameToZSLBuff(uint32_t srcIdx, uint32_t ZSLframesToAdd);
#endif // IC_MAIN_H_
