/**************************************************************************************************

 @File         : main.c
 @Author       : Florin Cotoranu
 @Brief        : Contains main debug control for application
 Date          : 13-April-2016
 E-mail        : florin.cotoranu@movidius.com
 Copyright     : � Movidius Srl 2016, � Movidius Ltd 2016

 Description   : This file allow initialization and control of lrt pipes. Control is made
                 base on debuger interaction. Application can run on Pc or on movidius
                 MV182 board.
 **************************************************************************************************/

/**************************************************************************************************
 ~~~ Included types first then APIs from other modules
 **************************************************************************************************/
#include <stdlib.h>
#include "assert.h"
#include <time.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>
#include <signal.h>
#include <semaphore.h>
// project specific
#include "IcTypes.h"
#include "ipipe.h"
#include "ic_main.h"
#include "rtems.h"
#include "rtems_config.h"
#include "swcLeonUtils.h"
#include "UnitTestApi.h"

// contain all interface necessary for updating isp parameter
#include "IspCfgParamsUp.h"
// configuration headers
#include "app_config.h"
#ifdef MV0212
#include "SourceCfgParamMV0212.h"
#elif defined(MV0182)
#include "SourceCfgParam.h"
#endif

#include "IspCommon.h"
#ifdef POWER_MEASURE_ON
#include "PowerWatch.h"
#endif
#include "IspCfgInstanceApi.h"
#include "sendOutApi.h"
/**************************************************************************************************
 ~~~  Basic typdefs
 **************************************************************************************************/
// available debugger command allowed
typedef enum {
    DBG_NO_COMMAND          = 0,
    DBG_START_EXECUTION     = 1,
    DBG_SETUP_CAM           = 2,
    DBG_COMMIT_CFG          = 3,
    DBG_STOP_EXECUTION      = 4,
    DBG_START_CAM           = 5,
    DBG_STOP_CAM            = 6,
    DBG_LOCK_CAPTURE_CAM    = 7,
    DBG_LOCK_BY_CFG_CAPTURE = 8,
    DBG_LOCK_CAPTURE_BURST  = 9,
    DBG_CHANGE_FINISH_EXECUTION = 10
} dbgCommand_t;


#ifndef DEFAULT_NR_OF_CAPTURE_IN_BURST_MODE
#define DEFAULT_NR_OF_CAPTURE_IN_BURST_MODE 8
#endif

#define MAX_NR_OF_ISP_CFG_INSTANCES  8
#define MAX_NR_OF_SOURCES            6

/**************************************************************************************************
 ~~~  Local variables
 **************************************************************************************************/

// this will be set in order to config things different
// !!! debug control variable - use debugger set command in order to control them
dbgCommand_t            dbgCommand      = DBG_NO_COMMAND;
uint32_t                dbgCamId        = 0;
uint32_t                dbgCamCfgId     = IMX214_30FPS_CAMA_BINN;

//
pthread_t               assincronDebugCmdThread;
IspCfgInstance          *ispCfgInstances[MAX_NR_OF_ISP_CFG_INSTANCES];

uint32_t freeMemForSrc0, noOfZSLframesForCam0;
/**************************************************************************************************
 ~~~ Local File function declarations
 **************************************************************************************************/
// by this thread can be send different commands from debuger
static void* assincronDbgCmdThread(void *unused);
// demo different initialization and run sequence
void t01_3Cams_214Bining_208_208(void);
// will run dynamically different scenario. At some point you need to replace sensors as will be print
void tAllTest(void);

/**************************************************************************************************
 ~~~ Functions Implementation
 **************************************************************************************************/
// main function pc/board definition
void POSIX_Init (void *args)
{
    UNUSED(args);
    int rc = 0;
    initClocksAndMemory();
    // turn of unused power islands 12 shaves + usb
    DrvCprPowerTurnOffIsland(POWER_ISLAND_USB);
    for (rc = POWER_ISLAND_SHAVE_0; rc <= POWER_ISLAND_SHAVE_11; rc++)
        DrvCprPowerTurnOffIsland(rc);

    // create user interaction thread, it will read commands from the debugger
    if ((rc = pthread_create(&assincronDebugCmdThread, NULL, assincronDbgCmdThread, NULL)) != 0) {
        printf("pthread_create: %s\n", strerror(rc));
        exit(1);
    }
    sleep(1);
    memset((void*)ispCfgInstances, 0, sizeof(ispCfgInstances));
    unitTestInit();

    sendOutCreate(&sendOut_initCfg);

    tAllTest();
    //t01_3Cams_214Bining_208_208();
    pthread_join(assincronDebugCmdThread, NULL);

    unitTestFinalReport();
    exit(0);
}

static void* assincronDbgCmdThread(void *unused) {
    UNUSED(unused);
    printf("Insert commands: \n");
    printf("Insert commands: \n");
    printf("set dbgCommand 1  - for DBG_START_EXECUTION \n");
    printf("set dbgCommand 2  - for DBG_SETUP_CAM \n");
    printf("set dbgCommand 3  - for DBG_COMMIT_CFG \n");
    printf("set dbgCommand 4  - for DBG_STOP_EXECUTION  \n");
    printf("set dbgCommand 5  - for DBG_START_CAM      \n");
    printf("set dbgCommand 6  - for DBG_STOP_CAM       \n");
    printf("set dbgCommand 7  - for DBG_LOCK_CAPTURE_CAM    \n");
    printf("set dbgCommand 8  - for DBG_LOCK_BY_CFG_CAPTURE \n");
    printf("set dbgCommand 9  - for DBG_LOCK_CAPTURE_BURST  \n");
    printf("set dbgCommand 10 - for DBG_CHANGE_FINISH_EXECUTION  \n");
    printf("\n set dbgCamId X  - select Source refer to  \n");
    printf("set dbgCamCfgId X - See in SourceCfgParam.h available cfg.\n");
    printf("set dbgEnableOutput X  - Out enable display bitmap selection.\n");

    while(1) {
        uint32_t i;
        dbgCommand = swcLeonReadNoCacheU32((uint32_t)&dbgCommand);
        dbgCamId = swcLeonReadNoCacheU32((uint32_t)&dbgCamId);
        dbgCamCfgId = swcLeonReadNoCacheU32((uint32_t)&dbgCamCfgId);
        switch(dbgCommand) {
        // individual start-stop support
        case DBG_START_EXECUTION:
            // this is just to ensure that the sensor are reset at start
            setAllGpioByType(sensorConfigurations[IMX214_30FPS_CAMA].gpioConfigDescriptor, RESET_PIN, 1); // assert reset
            setAllGpioByType(sensorConfigurations[IMX208_30FPS_CAMB].gpioConfigDescriptor, RESET_PIN, 1); // assert reset
            setAllGpioByType(sensorConfigurations[IMX208_30FPS_CAMBR_MONO].gpioConfigDescriptor, RESET_PIN, 1); // assert reset

            los_start(NULL);
            dbgCommand = DBG_NO_COMMAND;
            break;
        case DBG_STOP_EXECUTION:
            los_stop();
            //printf("\n sss \n");
            for (i = 0; i < MAX_NR_OF_ISP_CFG_INSTANCES; i++) {
                //printf("Cfg: %p\n",ispCfgInstances[i]);
                IspCfgInstanceDistroy(ispCfgInstances[i]);
            }
            printf("App was stopped. \n"); sleep(1);
            dbgCommand = DBG_NO_COMMAND;
            break;
        case DBG_SETUP_CAM:
            //printf("S %d, %d\n",dbgCamId,dbgCamCfgId);
            los_SetupSource(dbgCamId, &sourceSetup[dbgCamCfgId]);
            dbgCommand = DBG_NO_COMMAND;
            break;
        case DBG_COMMIT_CFG:
            los_SetupSourceCommit();
            dbgCommand = DBG_NO_COMMAND;
            break;
        case DBG_START_CAM:
            //printf("T %d, %d\n",dbgCamId,dbgCamCfgId);
            ispCfgInstances[dbgCamId] = IspCfgInstanceCreate(los_configIsp, los_ipipe_TriggerCapture, dbgCamId);
            los_ConfigureSource(dbgCamId, &srcCfg[dbgCamCfgId]);
            IspCfgInstanceTrigerNewRequest(ispCfgInstances[dbgCamId]);
            los_startSource(dbgCamId);
            startSensor(&sensorConfigurations[dbgCamCfgId]);
            dbgCommand = DBG_NO_COMMAND;
            break;
        case DBG_STOP_CAM:
            los_stopSource(dbgCamId);
            IspCfgInstanceDistroy(ispCfgInstances[dbgCamId]);
            searchAndBurnI2cConfigsByType(&sensorConfigurations[dbgCamCfgId], CAM_STOP_STREAMING, NULL);
            dbgCommand = DBG_NO_COMMAND;
            break;
        case DBG_LOCK_CAPTURE_CAM:
            los_ipipe_LockZSL(dbgCamId, 45, 1);
            dbgCommand = DBG_NO_COMMAND;
            break;
        case DBG_LOCK_BY_CFG_CAPTURE:
            IspCfgInstanceTrigerBurstMode(ispCfgInstances[dbgCamId], 1);
            dbgCommand = DBG_NO_COMMAND;
            break;
        case DBG_LOCK_CAPTURE_BURST:
            IspCfgInstanceTrigerBurstMode(ispCfgInstances[dbgCamId], DEFAULT_NR_OF_CAPTURE_IN_BURST_MODE);
            dbgCommand = DBG_NO_COMMAND;
            break;
        case DBG_CHANGE_FINISH_EXECUTION:
            return NULL;
            break;
        default:
            break;
        }
        rtems_task_wake_after(40);

        //sleep(1);
    }
    return NULL;
}

static void Fatal_extension(Internal_errors_Source  the_source, bool is_internal,
        uint32_t the_error) {
    if (the_source != RTEMS_FATAL_SOURCE_EXIT)
        printk ("\nSource %d Internal %d Error %d\n", the_source, is_internal, the_error);
}



//=================================================================================================
//=================================================================================================
//          ic_main functions apear at start of file as exported. Will be proper implemented
//          by params isp configuration application. Prepared for Guzzi (Mms application)
//=================================================================================================
void inc_cam_ipipe_buff_locked(
        void *p_prv,
        void *userData,
        unsigned int sourceInstance,
        void *buffZsl,
        icTimestamp ts,
        unsigned int seqNo
) {
    UNUSED(p_prv);
    UNUSED(userData);
    UNUSED(ts);
    UNUSED(seqNo);
    // dummy, every time a frame is locked, automatically call trigger capture.
    // Normally can be keep and call at other time.
    // or multiple lock, for maybe different buffers, ....
    IspCfgInstanceTrigerNewStillRequest(ispCfgInstances[sourceInstance], buffZsl, 0);
}
void inc_cam_capture_ready( // this should have a source identification, in order to know source
        void *p_prv,
        unsigned int seqNo,
        void *p_cfg_prv
) {
    // capture was made. Nothing to do in this demo.
    UNUSED(p_prv);
    UNUSED(seqNo);
    UNUSED(p_cfg_prv);
}


// Count stats ready events For Isp 0 and 1 =======================================================
// Dummy version, here normally have to update new parameters updating base on statistics
//=================================================================================================
int incCamIspStartCount[MAX_NR_OF_ISP_CFG_INSTANCES] = {0, 0, 0, 0, 0, 0, 0, 0};
int incCamIspEndCount  [MAX_NR_OF_ISP_CFG_INSTANCES] = {0, 0, 0, 0, 0, 0, 0, 0};
int incCamCaptureStartCount[MAX_NR_OF_ISP_CFG_INSTANCES] = {0, 0, 0, 0, 0, 0, 0, 0};
int incCamCaptureEndCount  [MAX_NR_OF_ISP_CFG_INSTANCES] = {0, 0, 0, 0, 0, 0, 0, 0};

void incCamIspStart(int32_t ispInstance, int32_t seqNo, uint32_t userData) {
    UNUSED(seqNo);
    UNUSED(userData);
    assert(ispInstance< 8);
    assert(ispInstance>=0);
    incCamIspStartCount[ispInstance]++;
}
void incCamIspEnd(int32_t ispInstance, int32_t seqNo, uint32_t userData) {
    UNUSED(seqNo);
    UNUSED(userData);
    assert(ispInstance< MAX_NR_OF_ISP_CFG_INSTANCES);
    assert(ispInstance>=0);
    incCamIspEndCount[ispInstance]++;
}

// Update params for Isp 0 and 1 ==================================================================
// Dummy version, this thread is trigger by read frame end event, and will update params
//with last one, will not use other parameters same params all the time
// ================================================================================================
void incCamSendFrameEnd(int32_t sourceInstance, int32_t seqNo, uint64_t ts) {
    UNUSED(seqNo);
    UNUSED(ts);
    // send new configuration at every eof isp event
    incCamCaptureEndCount[sourceInstance]++;
    IspCfgInstanceTrigerNewRequest(ispCfgInstances[sourceInstance]);
}

void incCamSendFrameStart(int32_t sourceInstance, int32_t seqNo, uint64_t ts) {
    UNUSED(seqNo);
    UNUSED(ts);
    incCamCaptureStartCount[sourceInstance]++;
}

void inc_zsl_add_buff_result(
        uint32_t memFree,
        uint32_t totalNoOfZSLframes,
        icAddZSLStatusCode status)
{
    UNUSED(status);
    freeMemForSrc0 = memFree;
    noOfZSLframesForCam0 = totalNoOfZSLframes;
}

// Test scenarios available======================================================================
//
// ================================================================================================

void t01_3Cams_214Bining_208_208(void) {

    dbgCommand      = DBG_START_EXECUTION;
    sleep(1);
    printf("Setup Cam0\n");
    dbgCamId        = 0;
    dbgCamCfgId     = IMX214_30FPS_CAMA_BINN;
    sourceSetup[dbgCamCfgId].appSpecificInfo = 0;
    dbgCommand      = DBG_SETUP_CAM;
    sleep(1);
    printf("Setup Cam1\n");
    dbgCamId        = 1;
    dbgCamCfgId     = IMX208_30FPS_CAMB;
    dbgCommand      = DBG_SETUP_CAM;
    sleep(1);
    printf("Setup Cam2\n");
    dbgCamId        = 2;
    dbgCamCfgId     = IMX208_30FPS_CAMBR_MONO;
    dbgCommand      = DBG_SETUP_CAM;
    sleep(1);
    dbgCommand      = DBG_COMMIT_CFG;
    sleep(2);
    // start all cams
    printf("Starting Cam0\n");
    dbgCamId        = 0;
    dbgEnableOutput = (1 << dbgCamId);
    dbgCamCfgId     = IMX214_30FPS_CAMA_BINN;
    dbgCommand      = DBG_START_CAM;
    sleep(6);
    printf("Starting Cam1\n");
    dbgCamId        = 1;
    dbgEnableOutput = (1 << dbgCamId);
    dbgCamCfgId     = IMX208_30FPS_CAMB;
    dbgCommand      = DBG_START_CAM;
    sleep(6);
    printf("Starting Cam2\n");
    dbgCamId        = 2;
    dbgEnableOutput = (1 << dbgCamId);
    dbgCamCfgId     = IMX208_30FPS_CAMBR_MONO;
    dbgCommand      = DBG_START_CAM;
    sleep(6);

    // capture normal
    for (dbgCamId = 0; dbgCamId < 3; dbgCamId++){
       printf("Trigger capture Cam%ld\n", dbgCamId);
       dbgEnableOutput = (1 << (dbgCamId + MAX_NR_OF_SOURCES));
       dbgCommand      = DBG_LOCK_CAPTURE_CAM;
       sleep(2);
       printf("Trigger capture by config Cam%ld\n", dbgCamId);
       dbgCommand      = DBG_LOCK_BY_CFG_CAPTURE;
       sleep(2);
       printf("Trigger capture burst Cam%ld\n", dbgCamId);
       dbgCommand      = DBG_LOCK_CAPTURE_BURST;
       sleep(2);
    }

#ifdef POWER_MEASURE_ON
    initPowerMeasurement();
#endif
}


extern uint32_t frmCounterTotal[IPIPE_MAX_OUTPUTS_ALlOWED];
void tAllTest(void)
{
    printf("\n\nPlay 3 cams, imx214 binning, and 2 imx208. Still demo.\n");
    frmCounterTotal[0] = 0;
    frmCounterTotal[1] = 0;
    frmCounterTotal[2] = 0;
    t01_3Cams_214Bining_208_208();
    dbgCommand = DBG_STOP_EXECUTION;
    sleep(2);
    printf("Nr of frames produced %ld - %ld - %ld\n", frmCounterTotal[0], frmCounterTotal[1], frmCounterTotal[2]);
    unitTestAssert(frmCounterTotal[0] > 800);
    unitTestAssert(frmCounterTotal[1] > 600);
    unitTestAssert(frmCounterTotal[2] > 400);

    sleep(1);
    printf("\n");
    dbgCommand = DBG_CHANGE_FINISH_EXECUTION;
}
