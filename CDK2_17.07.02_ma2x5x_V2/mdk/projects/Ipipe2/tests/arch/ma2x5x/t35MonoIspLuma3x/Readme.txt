t35MonoIspLuma3x

Supported Platform
===================
Myriad2 - This example works on Myriad2 ma2150/ma2450 silicon

Overview
==========
Stream up to 3 sensors depending on Los debuger commands.
Mono ISP Luma output is sent over HDMI (the output is cropped for IMX214 sensor scenario).

Software description
=======================
Software tests mono ISP luma output over HDMI and the number of frames produced over an amount of time.
 
Hardware needed
==================
- MV182/MV212
- HDMI cable connected to a monitor
Depending on selection command over debuger you will need different sensors connected
- MV201, containing 1 imx214 Color sensor Left position(J1), connected to CAM A
- MV200 board, containing 2 imx208 Mono sensors, connected to CAM B

Build
==================
Please type "make help" if you want to learn available targets.
Then open terminal and type:
  "make clean"
- on MV0182/ma2150:  
  "make all"
- on MV0212/ma2450:
  "make all MV_SOC_REV=ma2450"

Setup
==================
Myriad2 - To run the application:
- on MV0182/ma2150:
    - open terminal and type "make start_server"
    - open another terminal and type "make debug"
- on MV0212/ma2450:
    - open terminal and type "make start_server"
    - open another terminal and type "make debug MV_SOC_REV=ma2450"

Expected output
==================
- mono ISP output on HDMI
- debugger output:

PIPE:LOS: Insert commands: 
PIPE:LOS: set dbgCommand 1  - for DBG_START_EXECUTION 
PIPE:LOS: set dbgCommand 2  - for DBG_STOP_EXECUTION  
PIPE:LOS: set dbgCommand 3  - for DBG_SETUP_CAM  
PIPE:LOS: set dbgCommand 4  - for DBG_COMMIT_CFG  
PIPE:LOS: set dbgCommand 5  - for DBG_START_CAM      
PIPE:LOS: set dbgCommand 6  - for DBG_STOP_CAM       
PIPE:LOS: set dbgCommand 7  - for DBG_LOCK_CAPTURE_CAM    
PIPE:LOS: set dbgCommand 8  - for DBG_LOCK_BY_CFG_CAPTURE 
PIPE:LOS: set dbgCommand 9  - for DBG_LOCK_CAPTURE_BURST  
PIPE:LOS: 
PIPE:LOS:  set dbgCamId X  - select Source refer to  
PIPE:LOS:  set dbgCamCfgId X - See in SourceCfgParam.h available cfg.
PIPE:LOS:  set dbgEnableOutput X  - Out enable display bitmap selection.
PIPE:LOS: LOS:: unitTestVerbosity()    : VERBOSITY_QUIET
PIPE:LOS: 
PIPE:LOS: 
PIPE:LOS:  Play 3 cams, imx214 binning, and 2 imx208. Still demo.
PIPE:LOS: Setup Cam0
PIPE:LOS: Setup Cam1
PIPE:LOS: Setup Cam2
PIPE:LOS: Starting Cam0
PIPE:LOS: Starting Cam1
PIPE:LOS: Starting Cam2
PIPE:LOS: Trigger capture Cam0
PIPE:LOS: Trigger capture by config Cam0
PIPE:LOS: Trigger capture burst Cam0
PIPE:LOS: Trigger capture Cam1
PIPE:LOS: Trigger capture by config Cam1
PIPE:LOS: Trigger capture burst Cam1
PIPE:LOS: Trigger capture Cam2
PIPE:LOS: Trigger capture by config Cam2
PIPE:LOS: Trigger capture burst Cam2
LRT: LeonRT (P0:ALRT) suspended at 0x701A1158 (Application terminated successfully)
PIPE:LOS: App was stopped. 
PIPE:LOS: Nr of frames produced 1076 - 899 - 720
PIPE:LOS: LOS:: unitTestAssert()       : (value:0x00000001)                            => PASS
PIPE:LOS: LOS:: unitTestAssert()       : (value:0x00000001)                            => PASS
PIPE:LOS: LOS:: unitTestAssert()       : (value:0x00000001)                            => PASS
PIPE:LOS: 
PIPE:LOS: LOS:: moviUnitTest:PASSED
LOS: LeonOS (P0:ALOS) suspended at 0x87D45104 (Application terminated successfully)

User interaction
==================
Insert commands: 
set dbgCommand 1  - for DBG_START_EXECUTION 
set dbgCommand 2  - for DBG_STOP_EXECUTION  
set dbgCommand 3  - for DBG_SETUP_CAM  
set dbgCommand 4  - for DBG_COMMIT_CFG  
set dbgCommand 5  - for DBG_START_CAM      
set dbgCommand 6  - for DBG_STOP_CAM       
set dbgCommand 7  - for DBG_LOCK_CAPTURE_CAM    
set dbgCommand 8  - for DBG_LOCK_BY_CFG_CAPTURE 
set dbgCommand 9  - for DBG_LOCK_CAPTURE_BURST  

set dbgCamId X  - select Source refer to  
set dbgCamCfgId X - See in SourceCfgParam.h available cfg.
set dbgEnableOutput X  - Out enable display bitmap selection.
