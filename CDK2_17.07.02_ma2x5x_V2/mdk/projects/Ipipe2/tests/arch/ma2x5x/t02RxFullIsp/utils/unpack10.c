#include <stdio.h>
#include <stdint.h>

//########################################################################
void UnPackRaw10(uint8_t *in, uint16_t *out, int width)
{
    int x, nBytes;
    uint16_t b[5]; //bytes
    nBytes = width * 5 / 4; //4 pixels get packed in 5 bytes

    for(x=0; x<nBytes; )
    {
      b[0] = in[x];   x++;
      b[1] = in[x];   x++;
      b[2] = in[x];   x++;
      b[3] = in[x];   x++;
      b[4] = in[x];   x++;

      *out = (b[0]<<2) | ((b[4]>>0) & 0x3);  out++;
      *out = (b[1]<<2) | ((b[4]>>2) & 0x3);  out++;
      *out = (b[2]<<2) | ((b[4]>>4) & 0x3);  out++;
      *out = (b[3]<<2) | ((b[4]>>6) & 0x3);  out++;
    }
}

#define IMG_W 4192
#define IMG_H 3120
uint8_t  in [IMG_W*IMG_H*5/4];
uint16_t out[IMG_W*IMG_H];

//########################################################################
void main()
{
    FILE *f;
    f = fopen("../inPacked_4192x3120_PACKED.raw", "rb");
    fread(in, 1, sizeof(in), f);
    fclose(f);

    UnPackRaw10(in, out, IMG_W*IMG_H);

    f = fopen("../flat_4192x3120.raw", "wb");
    fwrite(out, 1, sizeof(out), f);
    fclose(f);
}

