/**************************************************************************************************

 @File         : ic_main.h
 @Author       : MT
 @Brief        : Contains all interaction part with ipipe
 Date          : 01 - March - 2015
 E-mail        : xxx.xxx@movidius.com
 Copyright     : � Movidius Srl 2014, � Movidius Ltd 2015

 Description :
    This file described all allowed message interaction with lrt pipes side

 **************************************************************************************************/

#ifndef _IC_MAIN_H_
#define _IC_MAIN_H_

/**************************************************************************************************
 ~~~ Includes
 **************************************************************************************************/
#include "ipipe.h"

#if defined(__sparc)
    #define ALIGNED(x) __attribute__((aligned(x)))
    #define DDR_BUF_DATA     __attribute__((section(".ddr.bss")))
#else // PC world
    #define ALIGNED(x) //nothing
    #define DDR_BUF_DATA
#endif
/**************************************************************************************************
 ~~~  Exported Functions
 **************************************************************************************************/
void los_start(void *);
void los_stop(void);
void los_SetupSource(int srcIdx, icSourceSetup *sconf);
void los_SetupSourceCommit(void);
void los_ConfigureSource(int srcIdx, icSourceConfig *sconf);
int  los_startSource (int srcIdx);
int  los_stopSource (int srcIdx);
void los_configIsp(void *iconf, int ispIdx);

//
void los_ipipe_LockZSL(uint32_t srcIdx, uint32_t frameSel, uint32_t enableTimeRelative) ;
//
void los_ipipe_TriggerCapture(void* buff, void *iconf, uint32_t srcIdx, uint32_t notReleaseZslBuf);
void los_addFrameToZSLBuff(uint32_t srcIdx, uint32_t ZSLframesToAdd);
#endif //_IC_MAIN_H_
