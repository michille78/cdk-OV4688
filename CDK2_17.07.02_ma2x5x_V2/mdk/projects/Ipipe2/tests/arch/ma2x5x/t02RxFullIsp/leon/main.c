#include <stdlib.h>
#include "assert.h"
#include <time.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>

// project specific
#include "IcTypes.h"
#include "ipipe.h"
#include "ic_main.h"
#include "rtems.h"
#include "rtems_config.h"
#include "swcLeonUtils.h"
#include "UnitTestApi.h"

// contain all interface necessary for updating isp parameter
#include "IspCfgParamsUp.h"
// configuration headers
#include "app_config.h"
#ifdef MV0212
#include "SourceCfgParamMV0212.h"
#elif defined(MV0182)
#include "SourceCfgParam.h"
#endif

#include "IspCommon.h"
#include "IspCfgInstanceApi.h"
#include "sendOutApi.h"

// available debugger command allowed
typedef enum {
    DBG_NO_COMMAND          = 0,
    DBG_START_EXECUTION     = 1,
    DBG_SETUP_CAM           = 2,
    DBG_COMMIT_CFG          = 3,
    DBG_STOP_EXECUTION      = 4,
    DBG_START_CAM           = 5,
    DBG_STOP_CAM            = 6,
    DBG_LOCK_CAPTURE_CAM    = 7,
    DBG_LOCK_BY_CFG_CAPTURE = 8,
    DBG_LOCK_CAPTURE_BURST  = 9,
    DBG_CHANGE_RESOLUTION   = 10,
    DBG_CHANGE_FINIS_EXECUTION = 11,
} dbgCommand_t;

#define MAX_NR_OF_ISP_CFG_INSTANCES  1

// Local variables
dbgCommand_t       dbgCommand  = DBG_NO_COMMAND;
uint32_t           dbgCamId    = 0;
uint32_t           dbgCamCfgId = 0;
uint32_t           dbgSendCfg  = 1; //send configs to LRT

pthread_t       assincronDebugCmdThread;
IspCfgInstance *ispCfgInstances[MAX_NR_OF_ISP_CFG_INSTANCES];

// Local File function declarations =================================================================
static void* assincronDbgCmdThread(void *unused);

void t04_1Cams_214FULL(void);

/**************************************************************************************************
 ~~~ Functions Implementation
 **************************************************************************************************/
// main function pc/board definition
void POSIX_Init (void *args)
{
    UNUSED(args);
    int rc = 0;
    initClocksAndMemory();
    // turn of unused power islands 12 shaves + usb
    DrvCprPowerTurnOffIsland(POWER_ISLAND_USB);
    for (rc = POWER_ISLAND_SHAVE_0; rc <= POWER_ISLAND_SHAVE_11; rc++)
        DrvCprPowerTurnOffIsland(rc);

    // create user interaction thread, it will read commands from the debugger
    if ((rc = pthread_create(&assincronDebugCmdThread, NULL, assincronDbgCmdThread, NULL)) != 0) {
        printf("pthread_create: %s\n", strerror(rc));
        exit(1);
    }
    sleep(1);
    memset((void*)ispCfgInstances, 0, sizeof(ispCfgInstances));

    sendOutCreate(&sendOut_initCfg);

    unitTestInit();

    t04_1Cams_214FULL();

    pthread_join(assincronDebugCmdThread, NULL);
    unitTestFinalReport();
    exit(0);
}

//################################################################################################################
// debug thread
static void* assincronDbgCmdThread(void *unused) {
    UNUSED(unused);

    while(1)
    {
        dbgCommand  = swcLeonReadNoCacheU32((uint32_t)&dbgCommand);
        dbgCamId    = swcLeonReadNoCacheU32((uint32_t)&dbgCamId);
        dbgCamCfgId = swcLeonReadNoCacheU32((uint32_t)&dbgCamCfgId);


        switch(dbgCommand)
        {
           case DBG_START_EXECUTION:
               // this is just to ensure that the sensor are reset at start
               setAllGpioByType(sensorConfigurations[IMX214_30FPS_CAMA      ].gpioConfigDescriptor, RESET_PIN, 1); // assert reset
               setAllGpioByType(sensorConfigurations[IMX208_30FPS_CAMB      ].gpioConfigDescriptor, RESET_PIN, 1); // assert reset
               setAllGpioByType(sensorConfigurations[IMX208_30FPS_CAMBR_MONO].gpioConfigDescriptor, RESET_PIN, 1); // assert reset
               los_start(NULL);
               break;

           case DBG_STOP_EXECUTION:
               los_stop();
               IspCfgInstanceDistroy(ispCfgInstances[0]);
               printf("App was stopped. \n"); sleep(1);
               break;

           case DBG_SETUP_CAM:
               los_SetupSource(dbgCamId, &sourceSetup[dbgCamCfgId]);
               break;

           case DBG_COMMIT_CFG:
               los_SetupSourceCommit();
               break;

           case DBG_START_CAM:
               //printf("T %d, %d\n",dbgCamId,dbgCamCfgId);
               ispCfgInstances[dbgCamId] = IspCfgInstanceCreate(los_configIsp, NULL, dbgCamId);
               los_ConfigureSource(dbgCamId, &srcCfg[dbgCamCfgId]);
               IspCfgInstanceTrigerNewRequest(ispCfgInstances[dbgCamId]);
               los_startSource(dbgCamId);
               startSensor(&sensorConfigurations[dbgCamCfgId]);
               break;
           default:
               break;
        } //switch
       dbgCommand = DBG_NO_COMMAND; //Command processed, mark as NONE

       rtems_task_wake_after(40);
       //sleep(1);
    }
    return NULL;
}

static void Fatal_extension(Internal_errors_Source  the_source, bool is_internal,
        uint32_t the_error) {
    if (the_source != RTEMS_FATAL_SOURCE_EXIT)
        printk ("\nSource %d Internal %d Error %d\n", the_source, is_internal, the_error);
}

// Update params for Isp 0 and 1 ==================================================================
// Dummy version, this thread is trigger by read frame end event, and will update params
//with last one, will not use other parameters same params all the time
// ================================================================================================
void incCamSendFrameEnd(int32_t sourceInstance, int32_t seqNo, uint64_t ts) {
    UNUSED(seqNo);
    UNUSED(ts);

    dbgSendCfg = swcLeonReadNoCacheU32((uint32_t)&dbgSendCfg);
    if(dbgSendCfg){
      // send new configuration at every eof isp event
      IspCfgInstanceTrigerNewRequest(ispCfgInstances[sourceInstance]);
    }
}

//#############################################################################################
void t04_1Cams_214FULL(void)
{
    extern icSourceSetup sourceSetup[MAX_SENSOR_CONFIG];

    dbgCommand  = DBG_START_EXECUTION;
    sleep(1);

    dbgCamId    = 0;
    dbgCamCfgId = IMX214_30FPS_CAMA;
    sourceSetup[dbgCamCfgId].maxBpp          = 16; //change to use flat (i.e. unpacked) output (also fits yuv420)
    sourceSetup[dbgCamCfgId].appSpecificInfo = 0;
    dbgCommand  = DBG_SETUP_CAM;
    sleep(1);

    dbgCommand  = DBG_COMMIT_CFG;
    sleep(1);

    // start cams
    dbgCamId    = 0;
    dbgCamCfgId = IMX214_30FPS_CAMA;
    dbgCommand  = DBG_START_CAM;
    sleep(1);
}
