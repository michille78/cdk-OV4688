
/**************************************************************************************************

 @File         : main_rt.c
 @Author       : MT
 @Brief        : Contains Lrt code starting point
 Date          : 01 - March - 2015
 E-mail        : xxx.xxx@movidius.com
 Copyright     : � Movidius Srl 2014, � Movidius Ltd 2015

 Description :


 **************************************************************************************************/


/**************************************************************************************************
 ~~~ Included types first then APIs from other modules
 **************************************************************************************************/
#include <stdio.h>

#include "DrvShaveL2Cache.h"
#include "DrvLeonL2C.h"
#include "swcLeonUtils.h"
#include "ipipe.h"
#include "IpipeServerApi.h"
#include "ipipeMsgQueue.h"
#include "isp1xVdoPlusBicubic.h"


/**************************************************************************************************
 ~~~  Specific #defines
 **************************************************************************************************/


/**************************************************************************************************
 ~~~ Local File function declarations
 **************************************************************************************************/


/**************************************************************************************************
 ~~~  Exported Functions
 **************************************************************************************************/
int main(void)
{

    uint32_t x;
    DrvLL2CDisable(LL2C_OPERATION_INVALIDATE);
    DrvLL2CInitWriteThrough();
    SET_REG_BITS_MASK(SIPP_INT0_ENABLE_ADR, 0);
    SET_REG_BITS_MASK(SIPP_INT1_ENABLE_ADR, 0);
    SET_REG_BITS_MASK(SIPP_INT2_ENABLE_ADR, 0);//patch for not working obfl_inc



    gServerInfo.cbIcSetup       = app3CamIspCbIcSetup;
    gServerInfo.cbIcTearDown    = app3CamIspCbIcTearDown;
    gServerInfo.cbSourcesCommit = app3CamIspSrcComit;


    setupIpipeServer();

    while (1) {
        app3CamIspMain();
    }
    return 0;
}

/**************************************************************************************************
 ~~~ Local Functions Implementation
 **************************************************************************************************/

