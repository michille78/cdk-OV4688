/**************************************************************************************************
 @File         : isp3xVdoPlusBicubic.c
 @Author       : MT
 @Brief        : Contains 1 Isp video plus Bicubic
 Date          : 01 - March - 2015
 E-mail        : xxx.xxx@movidius.com
 Copyright     : � Movidius Srl 2014, � Movidius Ltd 2015
 Description :
 **************************************************************************************************/


/**************************************************************************************************
 ~~~ Included types first then APIs from other modules
 **************************************************************************************************/
#include "isp1xVdoPlusBicubic.h"

#include <string.h>
#include <stdio.h>
#include <assert.h>
#include "DrvLeonL2C.h"
#include "ipipe.h"
#include "ipipeDbg.h"
#include "FrameMgrUtils.h"
#include "ipipeUtils.h"
#include "ipipeOpipeUtils.h"
#include "FrameMgrApi.h"
#include "IpipeServerApi.h"
#include "ipipeMsgQueue.h"
#include "PlgSourceApi.h"
#include "PlgFifoApi.h"
#include "PlgIspFullApi.h"
#include "PlgBicubicApi.h"

/**************************************************************************************************
 ~~~  Specific #defines
 **************************************************************************************************/
#define NR_OF_BUFFERS_PER_SOURCE  3
#define NR_OF_BUFFERS_PER_ISP_OUT 4
#define NR_OF_BUFFERS_PER_BICUBIC_OUT  3

/**************************************************************************************************
 ~~~  Local variables
 **************************************************************************************************/

static PlgSource       plgSource  SECTION(".cmx.cdmaDescriptors") ALIGNED(8);
static PlgIspFull      plgVdo1    SECTION(".cmx.cdmaDescriptors") ALIGNED(8);
static PlgFifo         plgFifo;
static PlgBicubic      plgBicubic SECTION(".cmx.cdmaDescriptors") ALIGNED(8);

static FramePool       frameMgrPoolC;
static FramePool       frameMgrPoolFifo;
static FramePool       frameMgrPoolP;
static FramePool       frameMgrPoolBicubic;

static FrameT          *frameMgrFrameC;
static FrameT          *frameMgrFrameP;
static FrameT          *frameMgrFrameBicubic;

static uint32_t        startSrcState;
static uint32_t        stopSrcState;
static icSourceConfig  *startSrcLocConfig;
static uint32_t        tearDownEnable = 0;
static void            *nextCfg;
static FrameProducedCB cbOutputList[2];
static FrameProducedCB cbListCam1[2]; // isp output go to send and to Bicubic

/**************************************************************************************************
 ~~~ Local File function declarations
 **************************************************************************************************/
static void     turnOfapp3Cam(void);
static uint32_t checkTurnOfFinalStop(uint32_t *updateVal);
static void     startSourcesLocal(icSourceConfig  *sourceConfig, uint32_t sourceId);
static uint32_t getIspPluginId(void *plg);
static void     app3CamIspStartSrc(uint32_t sourceInstance, icSourceConfig  *sourceConfig);
static void     app3CamIspStopSrc (uint32_t sourceInstance) ;
static void     cbEofSourceEvent(void *plg, FrameT *frame);
static void     cbSofSourceEvent(void *plg, FrameT *frame);
static void     cbConfigIsp    (uint32_t ispInstance, void *iconf);
static void     cbStartIspEvent(void *plg, uint32_t seqNr, void *userData);
static void     cbEndIspEvent  (void *plg, uint32_t seqNr, void *userData);
static void     cbOutput(FrameT *frame, void *pluginObj);


/**************************************************************************************************
 ~~~  Exported Functions
 **************************************************************************************************/
void app3CamIspCbIcSetup(icCtrl *ctrl) {
    uint32_t i;
    OpipeReset(); //general inits
    memset((void*)startSrcState, 0, sizeof(startSrcState));
    memset((void*)stopSrcState, 0, sizeof(stopSrcState));
    memset((void*)startSrcLocConfig, 0, sizeof(startSrcLocConfig));
    memset((void*)nextCfg, 0, sizeof(nextCfg));
    tearDownEnable = 0;
    gServerInfo.cbDataWasSent   = NULL;
    PlgFifoCreate       ((void*)&plgFifo);
    PlgFifoConfig       ((void*)&plgFifo, 1);
    plgFifo.plg.init(&frameMgrPoolFifo, 1, (void*)&plgFifo);
    PlgBicubicCreate    ((void*)&plgBicubic);

    plgBicubic.frmSz.w = 1936;
    plgBicubic.frmSz.h = 1096;
    plgBicubic.plg.init(&frameMgrPoolBicubic, 1, (void*)&plgBicubic);

    gServerInfo.sourceServerCtrl[0].cbStartSource  = NULL;
    gServerInfo.sourceServerCtrl[0].cbStopSource   = NULL;
    gServerInfo.pluginServerCtrl[0].cbConfigPlugin = NULL;
    PlgSourceCreate ((void*)&plgSource, 0);
    PlgIspFullCreate((void*)&plgVdo1);
    plgSource.plg.init(&frameMgrPoolC, 1, (void*)&plgSource);
    plgVdo1.plg.init(&frameMgrPoolP, 1, (void*)&plgVdo1);
    gServerInfo.sourceServerCtrl[0].pool = plgSource.outputPools;
    frameMgrFrameC = ipServerFrameMgrCreateList(NR_OF_BUFFERS_PER_SOURCE);
    frameMgrFrameP = ipServerFrameMgrCreateList(NR_OF_BUFFERS_PER_ISP_OUT);
    // Source output pool
    FrameMgrCreatePool(&frameMgrPoolC, frameMgrFrameC, &plgFifo.plg.callbacks[i], 1);
    // Mux  output pool, special case for serialization plug-in, no frame inside, as it pass the input data out
    FrameMgrCreatePool(&frameMgrPoolFifo, NULL, plgVdo1.plg.callbacks, 1);
    cbOutputList[0 ].callback  = cbOutput;
    cbOutputList[0 ].pluginObj = &plgVdo1;

    frameMgrFrameBicubic     = ipServerFrameMgrCreateList(NR_OF_BUFFERS_PER_BICUBIC_OUT);
    cbListCam1[0].callback  =   plgBicubic.plg.callbacks[0].callback;
    cbListCam1[0].pluginObj =   plgBicubic.plg.callbacks[0].pluginObj;
    cbListCam1[1].callback  =   cbOutputList[0].callback;
    cbListCam1[1].pluginObj =   cbOutputList[0].pluginObj;
    FrameMgrCreatePool(&frameMgrPoolP, frameMgrFrameP, cbListCam1, 2);
    // Bicubic plugin output
    cbOutputList[1 ].callback  = cbOutput;
    cbOutputList[1 ].pluginObj = &plgBicubic;
    FrameMgrCreatePool(&frameMgrPoolBicubic, frameMgrFrameBicubic, &cbOutputList[1], 1);
    // create descriptions for available functionality regarding isp. Los isp side, base on this
    // informations, will properly update parameters and config sensors.
    ipServerRegSourceQuery(0,
            "Source",
            IC_SOURCE_ATTR_HAS_VIDEO_ISP |
            IC_SOURCE_ATTR_HAS_VIDEO_OUT ,
            NR_OF_BUFFERS_PER_SOURCE,
            0, 0, 0, 0);

    ipServerRegIspQuery(0, "IspVdo", IC_ISP_ATTR_VIDEO_LINK, 0);
    ipServerRegUserPlgQuery(0, "Bicubic", 0);
    ipServerRegOutputQuery(0, "Out", IC_OUTPUT_FRAME_DATA_TYPE_PREVIEW  , 0);
    ipServerRegOutputQuery(1, "Out", IC_OUTPUT_FRAME_DATA_TYPE_PREVIEW  , 1);

    ipServerQueryAddChild(ctrl->icPipelineDescription.icQuerySource[0], ctrl->icPipelineDescription.icQueryIsp[0]);
    ipServerQueryAddChild(ctrl->icPipelineDescription.icQueryIsp[0], ctrl->icPipelineDescription.icQueryOutput[0]);
    ipServerQueryAddChild(ctrl->icPipelineDescription.icQueryIsp[0], ctrl->icPipelineDescription.icQueryPlg[0]);
    ipServerQueryAddChild(ctrl->icPipelineDescription.icQueryPlg[0], ctrl->icPipelineDescription.icQueryOutput[1]);
}

//
void app3CamIspCbIcTearDown(void) {
    turnOfapp3Cam();
    tearDownEnable = 1;
}

//
void app3CamIspMain(void) {
    if(tearDownEnable) {
        if(checkTurnOfFinalStop(&tearDownEnable)) {
            ipServerWasTornDown();
            exit(0);
        }
    }

    // triger just if opipe is idle
    if (
            (0 == plgVdo1.plg.status)
    ) {
        // Start Source Command in order to avoid big interrupt time
        if (1 == startSrcState) {
            startSourcesLocal(startSrcLocConfig, 0);
            startSrcState = 0;
        }
        // Stop Source Command in order to avoid big interrupt time
        if (1 == stopSrcState)
        {
            if(plgSource.plg.fini)
                plgSource.plg.fini(&plgSource);
            if(plgVdo1.plg.fini)
                plgVdo1.plg.fini(&plgVdo1);
            if(plgBicubic.plg.fini) plgBicubic.plg.fini(&plgBicubic);
            while ( (PLG_STATS_RUNNING == plgSource.plg.status) ||
                    (PLG_STATS_RUNNING == plgVdo1.plg.status)) {
                NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;
                NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;
            }
            while (PLG_STATS_RUNNING == plgBicubic.plg.status){
                    NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;
                    NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;
            }
            stopSrcState = 0;
            ipServerSourceStopped(0);
        }
        plgFifo.triger((void*)&plgFifo);
        NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;
    }
}

//
icStatusCode app3CamIspSrcComit(icCtrl *ctrl) {
    int32_t x;
    /*ALLOC*/ AllocOpipeReset(); //clear prev alloc
    /*ALLOC*/ AllocOpipeRxCmxBuffs (ctrl);
    /*ALLOC*/
    /*ALLOC*/ PlgIspBase *ispBase; //Isp buffs
    /*ALLOC*/ ispBase = &plgVdo1.base;
    /*ALLOC*/ AllocOpipeIspCmxBuffs(ctrl, 1, &ispBase);

    // Allocate frames buffers memory
    if(IPIPE_SRC_SETUP == ctrl->source[0].sourceStatus)
    {
        //size.w = (inSz.w * hN - 1)/hD + 1;
        //size.h = (inSz.h * vN - 1)/vD + 1;

        PlgBicubicConfig((void*)&plgBicubic, plgBicubic.frmSz, ctrl->source[0].sourceSetup.appSpecificInfo);

        uint32_t maxIspW = ((ctrl->source[0].sourceSetup.maxWidth *
                ctrl->source[0].sourceSetup.maxHorizN - 1) /
                ctrl->source[0].sourceSetup.maxHorizD + 1);
        uint32_t maxIspH = ((ctrl->source[0].sourceSetup.maxHeight *
                ctrl->source[0].sourceSetup.maxVertN - 1) /
                ctrl->source[0].sourceSetup.maxVertD + 1);
        uint32_t videoFrameSize = (maxIspW * maxIspH);
        ipServerFrameMgrAddBuffs(frameMgrFrameP,videoFrameSize,videoFrameSize>>1, 0);
        // this functionality is available just now !!! Important to do that
        gServerInfo.sourceServerCtrl[0].cbStartSource = app3CamIspStartSrc;
        gServerInfo.sourceServerCtrl[0].cbStopSource = app3CamIspStopSrc;
        gServerInfo.pluginServerCtrl[0].cbConfigPlugin = cbConfigIsp;
            ipServerFrameMgrAddBuffs(frameMgrFrameBicubic,
                    videoFrameSize, // div by 8 size in bytes
                    0, 0);
    }

    gServerInfo.cbDataWasSent   = FrameMgrReleaseFrame; // !!! different approach here

    return IC_STATS_SUCCESS;
}




/**************************************************************************************************
 ~~~ Local Functions Implementation
 **************************************************************************************************/

// Output callback linked to plug-ins
static void cbOutput(FrameT *frame, void *pluginObj) {
    uint32_t ispInstance = getIspPluginId(pluginObj);
    ipServerSendData(frame, ispInstance);
}

static void cbSofSourceEvent(void *plg, FrameT *frame) {
    uint32_t idx = 0;
    if(frame) {
        if(nextCfg) {
            frame->appSpecificData = nextCfg;
            nextCfg = NULL;
            ipServerReadoutStart((icSourceInstance)idx,
                    ((icIspConfig*)(frame->appSpecificData))->userData,
                    frame->seqNo, frame->timestamp[0]);
        }
        else {
            frame->appSpecificData = NULL;
            ipServerReadoutStart((icSourceInstance)idx,  NULL, frame->seqNo, frame->timestamp[0]);
        }
    }
    else {
        ipServerReadoutStart((icSourceInstance)idx, NULL, 0, 0);
    }
}

static void cbEofSourceEvent(void *plg, FrameT *frame) {
    uint32_t idx = 0;
    if(frame) {
        if(frame->appSpecificData) {
            ipServerReadoutEnd((icSourceInstance)idx,
                    ((icIspConfig*)(frame->appSpecificData))->userData,
                    frame->seqNo, frame->timestamp[0]);
        }
        else {
            ipServerReadoutEnd((icSourceInstance)idx, 0, frame->seqNo, frame->timestamp[0]);
        }
    }
    else {
        ipServerReadoutEnd((icSourceInstance)idx, NULL, 0, 0);
    }
}

static void cbStartIspEvent(void *plg, uint32_t seqNr, void *userData) {
    uint32_t ispInstance = getIspPluginId(plg);
    ipServerIspStart(ispInstance, seqNr, userData);
}
static void cbEndIspEvent(void *plg, uint32_t seqNr, void *userData) {
    uint32_t ispInstance = getIspPluginId(plg);
    ipServerIspEnd(ispInstance, seqNr, userData);
}

static void cbConfigIsp(uint32_t ispInstance, void *iconf) {
    nextCfg = iconf;
}

void startSourcesLocal(icSourceConfig  *sourceConfig, uint32_t sourceId) {
    icSize       iSize;
    iSize.w  = sourceConfig->cropWindow.x2-sourceConfig->cropWindow.x1;
    iSize.h  = sourceConfig->cropWindow.y2-sourceConfig->cropWindow.y1;
    FrmMgrUtilsInitList(frameMgrFrameC, iSize,
            FrmMgrUtilsGetRawFrm(sourceConfig->bitsPerPixel, 1));
    FrmMgrUtilsInitList(frameMgrFrameP, iSize, FRAME_T_FORMAT_YUV420);
    FrmMgrUtilsInitList(frameMgrFrameBicubic, iSize, FRAME_T_FORMAT_RAW_8);
    // set input frames params
    PlgIspFullConfig(&plgVdo1, iSize,
            GetFrameBppPackFormat(sourceConfig->bitsPerPixel), 0);
    plgSource.eofEvent    = cbEofSourceEvent;
    plgSource.sofEvent    = cbSofSourceEvent;
    plgVdo1.procesStart   = cbStartIspEvent;
    plgVdo1.procesEnd     = cbEndIspEvent;
    PlgSourceStart(&plgSource, sourceConfig,
            GetFrameBppPackFormat(sourceConfig->bitsPerPixel));
    ipServerSourceReady(sourceId);
}

static void app3CamIspStartSrc(uint32_t sourceInstance, icSourceConfig  *sourceConfig) {
    startSrcState = 1;
    startSrcLocConfig = sourceConfig;
}
static void app3CamIspStopSrc(uint32_t sourceInstance) {
    stopSrcState = 1;
}

// Turn off capability ############################################################################
static void turnOfapp3Cam(void)
{
    if(plgVdo1.plg.fini)   plgVdo1.plg.fini(&plgVdo1);
    if(plgSource.plg.fini) plgSource.plg.fini(&plgSource);
    gServerInfo.sourceServerCtrl[0].cbStartSource  = NULL;
    gServerInfo.sourceServerCtrl[0].cbStopSource   = NULL;
    gServerInfo.pluginServerCtrl[0].cbConfigPlugin = NULL;
    if(plgFifo.plg.fini) plgFifo.plg.fini(&plgFifo);
    if(plgBicubic.plg.fini) plgBicubic.plg.fini(&plgBicubic);
    gServerInfo.cbDataWasSent   = NULL;
}

static uint32_t checkTurnOfFinalStop(uint32_t *updateVal) {
    if(     (0 == plgSource.plg.status) &&
            (0 == plgVdo1.plg.status)   &&
            (0 == plgBicubic.plg.status)   &&
            (0 == plgFifo.plg.status)         ) {
        MemMgrReset();
        *updateVal = 0;
        return 1;
    }
    return 0;
}

static uint32_t getIspPluginId(void *plg) {
    if(plg == &plgVdo1) return 0;
    if(plg == &plgBicubic) return 1;
    assert(0); return 0;
}
