
/**************************************************************************************************

 @File         : main_rt.c
 @Author       : MT
 @Brief        : Contains Lrt code starting point
 Date          : 01 - March - 2015
 E-mail        : xxx.xxx@movidius.com
 Copyright     : � Movidius Srl 2014, � Movidius Ltd 2015
 Description   :


 **************************************************************************************************/


/**************************************************************************************************
 ~~~ Included types first then APIs from other modules
 **************************************************************************************************/
#include <stdio.h>
#include "DrvShaveL2Cache.h"
#include "DrvLeonL2C.h"
#include "swcLeonUtils.h"
#include "ipipe.h"
#include "IpipeServerApi.h"
#include "ipipeMsgQueue.h"
#include "ispUpTo6Cams.h"
#include "registersMyriad.h"

/**************************************************************************************************
 ~~~  Specific #defines
 **************************************************************************************************/
#define MXI_PRI_W     2
/**************************************************************************************************
 ~~~ Local File function declarations
 **************************************************************************************************/

void configure_mxi(void)
{
    u32 data;

    //vcsFastPuts("Configuring MXI...");
    SET_REG_WORD(AXM_AXI_ARB_CFG_ADR, 0x00000001);     // Enable AXI master priority-based arbitration

    data  =  0;                                        // Default all masters to lowest priority level
    data |= (3 << (MXI_CIF0_MST*MXI_PRI_W));            // Give LCD highest priority level
    data |= (2 << (MXI_CIF1_MST*MXI_PRI_W));            // Give LCD highest priority level
    SET_REG_WORD(AXM_MXI_PRIORITY_CFG_ADR, data);
}


/**************************************************************************************************
 ~~~  Exported Functions
 **************************************************************************************************/
int main(void)
{
    DrvLL2CDisable(LL2C_OPERATION_INVALIDATE);
    DrvLL2CInitWriteThrough();

    gServerInfo.cbIcSetup       = app3CamIspCbIcSetup;
    gServerInfo.cbIcTearDown    = app3CamIspCbIcTearDown;
    gServerInfo.cbSourcesCommit = app3CamIspSrcComit;


    // Port 0 (Leon OS/RT 128-bit AHB slave)
    SET_REG_WORD(DDRC_PCFGR_0_ADR, 0x0001524a); // Order of reads/writes to same address preserved; Read channel page match enabled; Read channel aging enabled - initial value = 586
    SET_REG_WORD(DDRC_PCFGW_0_ADR, 0x000012ea); // Write channel aging enabled - initial value = 746
    // Port 1 (SHAVE L2 cache 128-bit AXI slave)
    SET_REG_WORD(DDRC_PCFGR_1_ADR, 0x00011195); // Order of reads/writes to same address preserved; Read channel aging enabled - initial value = 405
    SET_REG_WORD(DDRC_PCFGW_1_ADR, 0x00001205); // Write channel aging enabled - initial value = 517
    // Port 2 (AMC 64-bit AXI slave)
    SET_REG_WORD(DDRC_PCFGR_2_ADR, 0x0001420d); // Order of reads/writes to same address preserved; Read channel page match enabled; Read channel aging disabled - highest priority
    SET_REG_WORD(DDRC_PCFGW_2_ADR, 0x0000104a); // Write channel aging enabled - initial value = 74
    // Port 3 (MXI 128-bit AXI slave - CIFs, LCD, NAL, CMXDMA)
    SET_REG_WORD(DDRC_PCFGR_3_ADR, 0x0000018e); // (Order of reads and writes cannot be preserved); Read channel aging disabled - highest priority
    SET_REG_WORD(DDRC_PCFGW_3_ADR, 0x0000101d); // Write channel aging enabled - initial value = 29

    configure_mxi();

    setupIpipeServer();

    while (1) {
        app3CamIspMain();
    }
    return 0;
}

/**************************************************************************************************
 ~~~ Local Functions Implementation
 **************************************************************************************************/
