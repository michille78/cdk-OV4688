/**************************************************************************************************

 @File         : isp3xVdoPlusCv.c
 @Author       : MT
 @Brief        : Contains 3 Isp video plus a dummy CV algo project main control functionality
 Date          : 01 - March - 2015
 E-mail        : xxx.xxx@movidius.com
 Copyright     : � Movidius Srl 2014, � Movidius Ltd 2015

 Description :


 **************************************************************************************************/


/**************************************************************************************************
 ~~~ Included types first then APIs from other modules
 **************************************************************************************************/
#include <string.h>
#include <stdio.h>
#include <assert.h>
#include "DrvLeonL2C.h"
#include "ipipe.h"
#include "ipipeDbg.h"
#include "FrameMgrUtils.h"
#include "ipipeUtils.h"
#include "ipipeOpipeUtils.h"
#include "FrameMgrApi.h"
#include "IpipeServerApi.h"
#include "ipipeMsgQueue.h"
#include "PlgSourceApi.h"
#include "PlgFifoApi.h"
#include "PlgIspFullApi.h"
#include "PlgSadDmApi.h"
#include "isp3xVdoPlusCv.h"
#include "TimeSyncMgr.h"

/**************************************************************************************************
 ~~~  Specific #defines
 **************************************************************************************************/
#define MAX_NR_OF_CAMS      3
#define NR_OF_BUFFERS_PER_SOURCE  3
#define NR_OF_BUFFERS_PER_ISP_OUT 4
#define NR_OF_BUFFERS_PER_DM_OUT  3



/**************************************************************************************************
 ~~~  Local variables
 **************************************************************************************************/

static PlgSource       plgSource[MAX_NR_OF_CAMS] SECTION(".cmx.cdmaDescriptors") ALIGNED(8);
static PlgIspFull      plgVdo1  [MAX_NR_OF_CAMS] SECTION(".cmx.cdmaDescriptors") ALIGNED(8);
static PlgFifo         plgFifo;
static PlgDm           plgDm SECTION(".cmx.cdmaDescriptors") ALIGNED(8);

static FramePool       frameMgrPoolC   [MAX_NR_OF_CAMS];
static FramePool       frameMgrPoolFifo[MAX_NR_OF_CAMS];
static FramePool       frameMgrPoolP   [MAX_NR_OF_CAMS];
static FramePool       frameMgrPoolDm;

static FrameT          *frameMgrFrameC[MAX_NR_OF_CAMS];
static FrameT          *frameMgrFrameP[MAX_NR_OF_CAMS];
static FrameT          *frameMgrFrameDm;

static uint32_t        startSrcState     [MAX_NR_OF_CAMS];
static uint32_t        stopSrcState      [MAX_NR_OF_CAMS];
static icSourceConfig  *startSrcLocConfig[MAX_NR_OF_CAMS];
static uint32_t        tearDownEnable = 0;
static void            *nextCfg [MAX_NR_OF_CAMS];
static uint32_t        dmInit = 0;
static FrameProducedCB cbOutputList[MAX_NR_OF_CAMS+1];

/**************************************************************************************************
 ~~~ Local File function declarations
 **************************************************************************************************/
static void     turnOfapp3Cam(void);
static uint32_t checkTurnOfFinalStop(uint32_t *updateVal);
static void     startSourcesLocal(icSourceConfig  *sourceConfig, uint32_t sourceId);
static uint32_t getSourcePluginId(void *plg);
static uint32_t getIspPluginId(void *plg);
static void     app3CamIspStartSrc (uint32_t sourceInstance, icSourceConfig  *sourceConfig);
static void     app3CamIspCfgDynSrc(uint32_t sourceInstance, icSourceConfigDynamic *dynCfg);
static void     app3CamIspStopSrc (uint32_t sourceInstance);
static void     cbEofSourceEvent(void *plg, FrameT *frame);
static void     cbSofSourceEvent(void *plg, FrameT *frame);
static void     cbHitSourceEvent(void *plg, FrameT *frame);
static void     cbConfigIsp    (uint32_t ispInstance, void *iconf);
static void     cbStartIspEvent(void *plg, uint32_t seqNr, void *userData);
static void     cbEndIspEvent  (void *plg, uint32_t seqNr, void *userData);
static void     cbOutput(FrameT *frame, void *pluginObj);


/**************************************************************************************************
 ~~~  Exported Functions
 **************************************************************************************************/
void app3CamIspCbIcSetup(icCtrl *ctrl) {
    uint32_t i;
    OpipeReset(); //general inits
    memset((void*)startSrcState, 0, sizeof(startSrcState));
    memset((void*)stopSrcState, 0, sizeof(stopSrcState));
    memset((void*)startSrcLocConfig, 0, sizeof(startSrcLocConfig));
    memset((void*)nextCfg, 0, sizeof(nextCfg));
    tearDownEnable = 0;
    gServerInfo.cbDataWasSent   = NULL;
    PlgFifoCreate       ((void*)&plgFifo);
    PlgFifoConfig       ((void*)&plgFifo, (uint32_t)MAX_NR_OF_CAMS);
    plgFifo.plg.init(frameMgrPoolFifo, MAX_NR_OF_CAMS, (void*)&plgFifo);
    PlgDmCreate         ((void*)&plgDm);
    plgDm.plg.init(&frameMgrPoolDm, 1, (void*)&plgDm);
    for(i = 0; i < MAX_NR_OF_CAMS; i++) {
        gServerInfo.sourceServerCtrl[i].cbStartSource  = NULL;
        gServerInfo.sourceServerCtrl[i].cbCfgDynamic   = NULL;
        gServerInfo.sourceServerCtrl[i].cbStopSource   = NULL;
        gServerInfo.pluginServerCtrl[i].cbConfigPlugin = NULL;
        PlgSourceCreate ((void*)&plgSource[i], i);
        PlgIspFullCreate((void*)&plgVdo1[i]);
        plgSource[0].plg.init(&frameMgrPoolC[i], 1, (void*)&plgSource[i]);
        plgVdo1[0].plg.init(&frameMgrPoolP[i], 1, (void*)&plgVdo1[i]);
        gServerInfo.sourceServerCtrl[i].pool = plgSource[i].outputPools;
        frameMgrFrameC[i]       = ipServerFrameMgrCreateList(NR_OF_BUFFERS_PER_SOURCE);
        frameMgrFrameP[i]       = ipServerFrameMgrCreateList(NR_OF_BUFFERS_PER_ISP_OUT);
        // Source output pool
        FrameMgrCreatePool(&frameMgrPoolC[i], frameMgrFrameC[i], &plgFifo.plg.callbacks[i], 1);
        // Mux  output pool, special case for serialization plug-in, no frame inside, as it pass the input data out
        FrameMgrCreatePool(&frameMgrPoolFifo[i],  NULL,                  plgVdo1[i].plg.callbacks, 1);
        cbOutputList[i  ].callback  = cbOutput;
        cbOutputList[i  ].pluginObj = &plgVdo1[i];
        // application description
        ipServerRegSourceQuery(i,
                "Source",
                IC_SOURCE_ATTR_HAS_VIDEO_ISP |
                IC_SOURCE_ATTR_HAS_VIDEO_OUT ,
                NR_OF_BUFFERS_PER_SOURCE,
                i, 0, 0, 0);
        ipServerRegIspQuery(i,   "IspVdo"   , IC_ISP_ATTR_VIDEO_LINK, i);
        ipServerRegOutputQuery(i,"Out"      , IC_OUTPUT_FRAME_DATA_TYPE_PREVIEW  , i); // preview cam output
        ipServerQueryAddChild(ctrl->icPipelineDescription.icQuerySource[i], ctrl->icPipelineDescription.icQueryIsp[i]);
        ipServerQueryAddChild(ctrl->icPipelineDescription.icQueryIsp[i], ctrl->icPipelineDescription.icQueryOutput[i]);
    }
    frameMgrFrameDm     = ipServerFrameMgrCreateList(NR_OF_BUFFERS_PER_DM_OUT);
    // ISP output Pool
    FrameMgrCreatePool(&frameMgrPoolP[0], frameMgrFrameP[0], &cbOutputList[0], 1);

    FrameMgrCreatePool(&frameMgrPoolP[1], frameMgrFrameP[1], &plgDm.plg.callbacks[0], 1);

    FrameMgrCreatePool(&frameMgrPoolP[2], frameMgrFrameP[2],&plgDm.plg.callbacks[1], 1);
    // dm plugin output
    cbOutputList[3  ].callback  = cbOutput;
    cbOutputList[3  ].pluginObj = &plgDm;
    FrameMgrCreatePool(&frameMgrPoolDm, frameMgrFrameDm, &cbOutputList[3], 1);
    dmInit=0;
    // general plugin type, not isp, source or output
    ipServerRegUserPlgQuery(0, "SadDm", 0);
    ipServerRegOutputQuery(3, "Out", IC_OUTPUT_FRAME_DATA_TYPE_USER1  , (2<<8)|(1)); // preview cam output
    ipServerQueryAddChild(ctrl->icPipelineDescription.icQueryIsp[1], ctrl->icPipelineDescription.icQueryPlg[0]);
    ipServerQueryAddChild(ctrl->icPipelineDescription.icQueryIsp[2], ctrl->icPipelineDescription.icQueryPlg[0]);
    ipServerQueryAddChild(ctrl->icPipelineDescription.icQueryPlg[0], ctrl->icPipelineDescription.icQueryOutput[3]);
}

//
void app3CamIspCbIcTearDown(void) {
    turnOfapp3Cam();
    tearDownEnable = 1;
}

//
void app3CamIspMain(void) {
    uint32_t x;
    if(tearDownEnable) {
        if(checkTurnOfFinalStop(&tearDownEnable)) {
            ipServerWasTornDown();
            exit(0);
        }
    }

    // triger just if opipe is idle
    if (
            (0 == plgVdo1[0].plg.status)   &&
            (0 == plgVdo1[1].plg.status)   &&
            (0 == plgVdo1[2].plg.status)
    ) {
        for (x = 0; x < MAX_NR_OF_CAMS; x++) {
            // Start Source Command in order to avoid big interrupt time
            if (1 == startSrcState[x]) {
                startSourcesLocal(startSrcLocConfig[x], x);
                startSrcState[x] = 0;
            }
            // Stop Source Command in order to avoid big interrupt time
            if (1 == stopSrcState[x])
            {
                if(plgSource[x].plg.fini)
                    plgSource[x].plg.fini(&plgSource[x]);
                if(plgVdo1[x].plg.fini)
                    plgVdo1[x].plg.fini(&plgVdo1[x]);

                while ( (PLG_STATS_RUNNING == plgSource[x].plg.status) ||
                        (PLG_STATS_RUNNING == plgVdo1[x].plg.status)) {
                    NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;
                    NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;
                }

                if(0 != x){ // is not the sensor not conected to dm
                    dmInit = dmInit & (~(1<<x));
                }
                stopSrcState[x] = 0;
                ipServerSourceStopped(x);
            }
        }
        plgFifo.triger((void*)&plgFifo);
        NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;
    }
}

//
icStatusCode app3CamIspSrcComit(icCtrl *ctrl) {
    int32_t x;
    /*ALLOC*/ AllocOpipeReset(); //clear prev alloc
    /*ALLOC*/ AllocOpipeRxCmxBuffs (ctrl);
    /*ALLOC*/
    /*ALLOC*/ PlgIspBase *ispBase[MAX_NR_OF_CAMS]; //Isp buffs
    /*ALLOC*/ for(x=0; x<MAX_NR_OF_CAMS; x++)
        /*ALLOC*/   ispBase[x] = &plgVdo1[x].base;
    /*ALLOC*/ AllocOpipeIspCmxBuffs(ctrl, MAX_NR_OF_CAMS, ispBase);

    // Allocate frames buffers memory
    for (x = 0; x < MAX_NR_OF_CAMS; x++)
    {
        if(IPIPE_SRC_SETUP == ctrl->source[x].sourceStatus)
        {
            //size.w = (inSz.w * hN - 1)/hD + 1;
            //size.h = (inSz.h * vN - 1)/vD + 1;
            uint32_t maxIspW = ((ctrl->source[x].sourceSetup.maxWidth *
                    ctrl->source[x].sourceSetup.maxHorizN - 1) /
                    ctrl->source[x].sourceSetup.maxHorizD + 1);
            uint32_t maxIspH = ((ctrl->source[x].sourceSetup.maxHeight *
                    ctrl->source[x].sourceSetup.maxVertN - 1) /
                    ctrl->source[x].sourceSetup.maxVertD + 1);
            uint32_t videoFrameSize = (maxIspW * maxIspH);
            ipServerFrameMgrAddBuffs(frameMgrFrameP[x],videoFrameSize,videoFrameSize>>1, 0);
            // this functionality is available just now !!! Important to do that
            gServerInfo.sourceServerCtrl[x].cbStartSource = app3CamIspStartSrc;
            gServerInfo.sourceServerCtrl[x].cbCfgDynamic  = app3CamIspCfgDynSrc;
            gServerInfo.sourceServerCtrl[x].cbStopSource  = app3CamIspStopSrc;
            gServerInfo.pluginServerCtrl[x].cbConfigPlugin = cbConfigIsp;
            if(1 == x) { // if  source 1 is prepared then dm is prepared to
                ipServerFrameMgrAddBuffs(frameMgrFrameDm,
                        videoFrameSize, // div by 8 size in bytes
                        0, 0);
            }
        }
    }

    gServerInfo.cbDataWasSent   = FrameMgrReleaseFrame; // !!! different approach here

    return IC_STATS_SUCCESS;
}


/**************************************************************************************************
 ~~~ Local Functions Implementation
 **************************************************************************************************/

// Output callback linked to plug-ins
static void cbOutput(FrameT *frame, void *pluginObj) {
    uint32_t ispInstance = getIspPluginId(pluginObj);
    ipServerSendData(frame, ispInstance);
}

static void cbSofSourceEvent(void *plg, FrameT *frame) {
    uint32_t   idx = getSourcePluginId(plg);
    if(frame) {
        if(nextCfg[idx]) {
            frame->appSpecificData = nextCfg[idx];
            nextCfg[idx] = NULL;
            ipServerReadoutStart((icSourceInstance)idx,
                    ((icIspConfig*)(frame->appSpecificData))->userData,
                    frame->seqNo, frame->timestamp[0]);
        }
        else {
            frame->appSpecificData = NULL;
            ipServerReadoutStart((icSourceInstance)idx,  NULL, frame->seqNo, frame->timestamp[0]);
        }
    }
    else {
        ipServerReadoutStart((icSourceInstance)idx, NULL, 0, 0);
    }
}

static void cbHitSourceEvent(void *plg, FrameT *frame) {
    uint32_t   idx = getSourcePluginId(plg);
    icTimestamp ts = TimeSyncMsgGetTimeUs(); //current time !!!

    if(frame) {
        if(frame->appSpecificData) {
            ipServerLineHit((icSourceInstance)idx,
                    ((icIspConfig*)(frame->appSpecificData))->userData,
                    frame->seqNo, ts);
        }
        else {
            ipServerLineHit((icSourceInstance)idx,  NULL, frame->seqNo, ts);
        }
    }
    else {
        ipServerLineHit((icSourceInstance)idx, NULL, 0, 0);
    }
}
static void cbEofSourceEvent(void *plg, FrameT *frame) {
    uint32_t idx = getSourcePluginId(plg);
    if(frame) {
        if(frame->appSpecificData) {
            ipServerReadoutEnd((icSourceInstance)idx,
                    ((icIspConfig*)(frame->appSpecificData))->userData,
                    frame->seqNo, frame->timestamp[0]);
        }
        else {
            ipServerReadoutEnd((icSourceInstance)idx, 0, frame->seqNo, frame->timestamp[0]);
        }
    }
    else {
        ipServerReadoutEnd((icSourceInstance)idx, NULL, 0, 0);
    }
}

static void cbStartIspEvent(void *plg, uint32_t seqNr, void *userData) {
    uint32_t ispInstance = getIspPluginId(plg);
    ipServerIspStart(ispInstance, seqNr, userData);
}
static void cbEndIspEvent(void *plg, uint32_t seqNr, void *userData) {
    uint32_t ispInstance = getIspPluginId(plg);
    ipServerIspEnd(ispInstance, seqNr, userData);
}

static void cbConfigIsp(uint32_t ispInstance, void *iconf) {
    nextCfg[ispInstance] = iconf;
}

void startSourcesLocal(icSourceConfig  *sourceConfig, uint32_t sourceId) {
    icSize       iSize;
    iSize.w  = sourceConfig->cropWindow.x2-sourceConfig->cropWindow.x1;
    iSize.h  = sourceConfig->cropWindow.y2-sourceConfig->cropWindow.y1;
    FrmMgrUtilsInitList(frameMgrFrameC[sourceId], iSize,
            FrmMgrUtilsGetRawFrm(sourceConfig->bitsPerPixel, 1));
    FrmMgrUtilsInitList(frameMgrFrameP[sourceId], iSize, FRAME_T_FORMAT_YUV420);
    if(0 != sourceId){ // is not the sensor not conected to dm
        if(0 == dmInit) { // it is first sensor started
            PlgDmSetParams(&plgDm, iSize, 8, 11);
            PlgSadDmStopSrc(&plgDm);
            FrmMgrUtilsInitList(frameMgrFrameDm, iSize, FRAME_T_FORMAT_RAW_8);
        }
        dmInit=dmInit|(1<<sourceId);
    }
    // set input frames params
    PlgIspFullConfig(&plgVdo1[sourceId], iSize,
            GetFrameBppPackFormat(sourceConfig->bitsPerPixel), 0);
    plgSource[sourceId].eofEvent    = cbEofSourceEvent;
    plgSource[sourceId].sofEvent    = cbSofSourceEvent;
    plgSource[sourceId].hitEvent    = cbHitSourceEvent;
    plgVdo1[sourceId].procesStart   = cbStartIspEvent;
    plgVdo1[sourceId].procesEnd     = cbEndIspEvent;
    PlgSourceStart(&plgSource[sourceId], sourceConfig,
            GetFrameBppPackFormat(sourceConfig->bitsPerPixel));
    ipServerSourceReady(sourceId);
}

static void app3CamIspStartSrc(uint32_t sourceInstance, icSourceConfig  *sourceConfig) {
    startSrcState[sourceInstance] = 1;
    startSrcLocConfig[sourceInstance] = sourceConfig;
}
static void app3CamIspCfgDynSrc(uint32_t sourceInstance, icSourceConfigDynamic *dynCfg){
    PlgSourceSetLineHit(&plgSource[sourceInstance], dynCfg->notificationLine);
}
static void app3CamIspStopSrc(uint32_t sourceInstance) {
    stopSrcState[sourceInstance] = 1;
}

// Turn off capability ############################################################################
static void turnOfapp3Cam(void)
{
    uint32_t i;
    for(i = 0; i < MAX_NR_OF_CAMS; i++) {
        if(plgVdo1[i].plg.fini)   plgVdo1[i].plg.fini(&plgVdo1[i]);
        if(plgSource[i].plg.fini) plgSource[i].plg.fini(&plgSource[i]);
        gServerInfo.sourceServerCtrl[i].cbStartSource  = NULL;
        gServerInfo.sourceServerCtrl[i].cbStopSource   = NULL;
        gServerInfo.pluginServerCtrl[i].cbConfigPlugin = NULL;
    }
    if(plgFifo.plg.fini) plgFifo.plg.fini(&plgFifo);
    gServerInfo.cbDataWasSent   = NULL;
}

static uint32_t checkTurnOfFinalStop(uint32_t *updateVal) {
    if(     (0 == plgSource[0].plg.status) &&
            (0 == plgSource[1].plg.status) &&
            (0 == plgSource[2].plg.status) &&
            (0 == plgVdo1[0].plg.status)   &&
            (0 == plgVdo1[1].plg.status)   &&
            (0 == plgVdo1[2].plg.status)   &&
            (0 == plgFifo.plg.status)         ) {
        MemMgrReset();
        *updateVal = 0;
        return 1;
    }
    return 0;
}

static uint32_t getSourcePluginId(void *plg) {
    uint32_t i;
    for(i = 0; i < MAX_NR_OF_CAMS; i++)
        if(plg == &plgSource[i]) return i;
    assert(0); return 0;
}

static uint32_t getIspPluginId(void *plg) {
    uint32_t i;
    for(i = 0; i < MAX_NR_OF_CAMS; i++)
        if(plg == &plgVdo1[i]) return i;
    if(plg == &plgDm) return MAX_NR_OF_CAMS;
    assert(0); return 0;
}
