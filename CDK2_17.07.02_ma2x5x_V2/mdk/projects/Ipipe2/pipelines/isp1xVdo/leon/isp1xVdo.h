/**************************************************************************************************

 @File         : isp1xVdo.c
 @Author       : AG
 @Brief        : Contains application interface For 1 isp
 Date          : 12 - Jan - 2016
 E-mail        : xxx.xxx@movidius.com
 Copyright     : � Movidius Srl 2014, � Movidius Ltd 2015

 Description : Receive and process image from one single camera; type of sensor is
     selected at application level, on LOS


 **************************************************************************************************/

#ifndef _ISP1XVDO_H_
#define _ISP1XVDO_H_

/**************************************************************************************************
 ~~~ Included types first then APIs from other modules
 **************************************************************************************************/
#include "IcTypes.h"
#include "ipipe.h"

/**************************************************************************************************
 ~~~  Exported Functions
 **************************************************************************************************/
// as lrt start, this function will be called. (Doc. 4.2)
void app1CamIspCbIcSetup(icCtrl *ctrl);

// application specific Tear Down
void app1CamIspCbIcTearDown(void);

//
icStatusCode app1CamIspSrcComit(icCtrl *ctrl);

//
void app1CamIspMain(void);

#endif // _ISP1XVDO_H_


