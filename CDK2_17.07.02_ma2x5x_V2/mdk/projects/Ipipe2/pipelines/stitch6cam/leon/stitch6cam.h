/**************************************************************************************************

 @File         : app3CamIsp.c
 @Author       : MT
 @Brief        : Contains application interface For 3 isp in parallel
 Date          : 01 - March - 2015
 E-mail        : xxx.xxx@movidius.com
 Copyright     : � Movidius Srl 2014, � Movidius Ltd 2015

 Description :


 **************************************************************************************************/

#ifndef _APP6CAMISP_H_
#define _APP6CAMISP_H_

/**************************************************************************************************
 ~~~ Included types first then APIs from other modules
 **************************************************************************************************/
#include "IcTypes.h"
#include "ipipe.h"

/**************************************************************************************************
 ~~~  Exported Functions
 **************************************************************************************************/
// as lrt start, this function will be called. (Doc. 4.2)
void app6CamIspCbIcSetup(icCtrl *ctrl);

// application specific Tear Down
void app6CamIspCbIcTearDown(void);

//
icStatusCode app6CamIspSrcComit(icCtrl *ctrl);

//
void app6CamIspMain(void);

#endif // _APP6CAMISP_H_


