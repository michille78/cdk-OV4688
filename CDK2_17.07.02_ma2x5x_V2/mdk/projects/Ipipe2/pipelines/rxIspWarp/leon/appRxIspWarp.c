#include <string.h>
#include <assert.h>
#include "ipipe.h"
#include "IpipeServerApi.h"
#include "ipipeOpipeUtils.h"
#include "FrameMgrUtils.h"
#include "FrameMgrApi.h"
#include "PlgSrcIspApi.h"
#include "appRxIspWarp.h"

/// Warp
#include "PlgWarpApi.h"
#include "init_mesh_y.h"
#include "init_mesh_uv.h"

#define MAX_NR_OF_CAMS              1
#define NR_OF_BUFFERS_PER_SOURCE    4
#define NR_OF_BUFFERS_PER_WARP_OUT  4

#define IMG_W_IN_WARP (3840)
#define IMG_H_IN_WARP (2160)

#define IMG_W_OUT_WARP (1920)
#define IMG_H_OUT_WARP (1088)

#define MESH_CELL_SIZE 16
#define MESH_WIDTH     MESH_WIDTH_F
#define MESH_HEIGHT    MESH_HEIGHT_F
#define MESH_WIDTH_C   MESH_WIDTH_F_CHROMA
#define MESH_HEIGHT_C  MESH_HEIGHT_F_CHROMA

//in cdmaDescriptor section that bypasses caches !!!
static PlgSrcIsp   plgSrcIsp SECTION(".cmx_direct.data") ALIGNED(8);
static PlgWarp     plgWarp   SECTION(".cmx_direct.data") ALIGNED(8);

static FramePool   frameMgrPoolC;
static FramePool   frameMgrPoolWarp;
static FrameT     *frameMgrFrameC;
static FrameT     *frameMgrFrameWarp;

static uint32_t        startSrcState;
static uint32_t        stopSrcState;
static icSourceConfig  *startSrcLocConfig;
static uint32_t        tearDownEnable;
static void            *nextCfg;
static FrameProducedCB cbOutputList;

static uint32_t getSourcePluginId(void *plg) {UNUSED(plg); return 0;}
static void     startSourcesLocal(icSourceConfig  *sourceConfig, uint32_t sourceId);
static void     appRxIspStartSrc(uint32_t sourceInstance, icSourceConfig  *sourceConfig);
static void     appRxIspStopSrc (uint32_t sourceInstance) ;
static void     cbEofSourceEvent(void *plg, FrameT *frame);
static void     cbSofSourceEvent(void *plg, FrameT *frame);
static void     cbConfigIsp    (uint32_t ispInstance, void *iconf);
static void     cbOutput(FrameT *frame, void *pluginObj);

SECTION(".cmx_direct.data") static volatile meshStruct init_mesh_Y0 =
{
    .meshWidth  = MESH_WIDTH,
    .meshHeight = MESH_HEIGHT,
    .meshX      = (float*)initMeshY_x,
    .meshY      = (float*)initMeshY_y,
    .coord_min_x = 0,
    .coord_max_x = 960,
    .coord_min_y = 0,
    .coord_max_y = 1088
};
SECTION(".cmx_direct.data") static volatile meshStruct init_mesh_Y1 =
{
    .meshWidth  = MESH_WIDTH,
    .meshHeight = MESH_HEIGHT,
    .meshX      = (float*)initMeshY_x,
    .meshY      = (float*)initMeshY_y,
    .coord_min_x = 960,
    .coord_max_x = 1920,
    .coord_min_y = 0,
    .coord_max_y = 1088
};
SECTION(".cmx_direct.data") static volatile meshStruct init_mesh_UV =
{
    .meshWidth  = MESH_WIDTH_C,
    .meshHeight = MESH_HEIGHT_C,
    .meshX      = (float*)initMeshChroma_x,
    .meshY      = (float*)initMeshChroma_y,
    .coord_min_x = 0,
    .coord_max_x = 960,
    .coord_min_y = 0,
    .coord_max_y = 544
};

//#################################################################################################
void appRxIspCbIcSetup(icCtrl *ctrl)
{
    OpipeReset(); //general inits

    memset((void*)startSrcState,     0, sizeof(startSrcState));
    memset((void*)stopSrcState,      0, sizeof(stopSrcState));
    memset((void*)startSrcLocConfig, 0, sizeof(startSrcLocConfig));
    nextCfg                   = NULL;
    tearDownEnable            = 0;
    gServerInfo.cbDataWasSent = NULL;

    {// create plug-ins
        uint32_t i = 0;
        gServerInfo.sourceServerCtrl[i].cbStartSource  = NULL;
        gServerInfo.sourceServerCtrl[i].cbStopSource   = NULL;
        gServerInfo.pluginServerCtrl[i].cbConfigPlugin = NULL;

        /// Isp
        PlgSrcIspCreate ((void*)&plgSrcIsp, i);
        plgSrcIsp.plg.init(&frameMgrPoolC, 1, (void*)&plgSrcIsp);

        /// Eis
        icSize frameSizeIn  = {IMG_W_IN_WARP, IMG_H_IN_WARP};
        icSize frameSizeOut = {IMG_W_OUT_WARP, IMG_H_OUT_WARP};
        PlgWarpCreate ((void*)&plgWarp);
        PlgWarpConfig(&plgWarp, frameSizeIn, frameSizeOut,
                        &init_mesh_Y0, &init_mesh_Y1, &init_mesh_UV,
                        0x80 , i);
        plgWarp.plg.init (&frameMgrPoolWarp, 1, (void*)&plgWarp);

        // source that have associated trigger capture capability
        gServerInfo.sourceServerCtrl[i].pool = plgSrcIsp.outputPools;
        frameMgrFrameC = ipServerFrameMgrCreateList(NR_OF_BUFFERS_PER_SOURCE);
        frameMgrFrameWarp = ipServerFrameMgrCreateList(NR_OF_BUFFERS_PER_WARP_OUT);

        // init callback output, this pluginObj not refer this time to output plugin, 
        /// refere to source
#if 0
        // plugin, in order to identify the source
        cbOutputList.callback  = cbOutput;
        cbOutputList.pluginObj = &plgSrcIsp;

       // SOURCE->OUTPUT link
        FrameMgrCreatePool(&frameMgrPoolC, frameMgrFrameC, &cbOutputList, 1);
#else
        // plugin, in order to identify the source
        cbOutputList.callback  = cbOutput;
        cbOutputList.pluginObj = &plgWarp;

       // SOURCE->EIS->OUTPUT link
        FrameMgrCreatePool(&frameMgrPoolC, frameMgrFrameC, &plgWarp.plg.callbacks[0], 1);
        FrameMgrCreatePool(&frameMgrPoolWarp,  frameMgrFrameWarp,  &cbOutputList, 1);
#endif

       // create descriptions for available functionality regarding isp. Los isp side, base on this
       // informations, will properly update parameters and config sensors.
       ipServerRegSourceQuery(i,
               "Source",
               IC_SOURCE_ATTR_HAS_VIDEO_ISP |
               IC_SOURCE_ATTR_HAS_VIDEO_OUT,
               NR_OF_BUFFERS_PER_SOURCE,
               i, i + MAX_NR_OF_CAMS*1, i + MAX_NR_OF_CAMS*2, 0);

       ipServerRegIspQuery   (i , "IspVdo", IC_ISP_ATTR_VIDEO_LINK, i);
       ipServerRegOutputQuery(i , "Out",    IC_OUTPUT_FRAME_DATA_TYPE_PREVIEW  , i); // preview cam output

       ipServerQueryAddChild(ctrl->icPipelineDescription.icQuerySource[i], ctrl->icPipelineDescription.icQueryIsp[i]);
       ipServerQueryAddChild(ctrl->icPipelineDescription.icQueryIsp   [i], ctrl->icPipelineDescription.icQueryOutput[i]);
    };
}

void appRxIspCbIcTearDown(void) {
    //turnOfappRx();
    tearDownEnable = 1;
}

int sippOpipeResurcesFree(void) {
   return 1;
}

//#################################################################################################
void appRxIspMain()
{
    if(tearDownEnable) {
       ipServerWasTornDown();
       exit(0);
    }

    /// Eis scheduler
    {
        if(plgWarp.meshGenEnd == 1)
        {
            plgWarp.meshGenEnd = 0;
            plgWarp.triggerStartWarp(&plgWarp);
        }

        if(plgWarp.chrEOF == 1)
        {
            plgWarp.chrEOF = 0;
            plgWarp.triggerChroma(&plgWarp);
        }

        if(plgWarp.lumaEOF == 1)
        {
            plgWarp.lumaEOF = 0;
            plgWarp.triggerLuma(&plgWarp);
        }

        if((PLG_STATS_IDLE == plgWarp.plg.status) && (plgWarp.runEnFlag > 0)) {
            plgWarp.runEnFlag--;
            plgWarp.triger(&plgWarp);
        }
    }

    //trigger just if opipe is idle
    if (sippOpipeResurcesFree()) {
        uint32_t x = 0;
        {
            // Start Source Command in order to avoid big interrupt time
            if (1 == startSrcState) {
                startSourcesLocal(startSrcLocConfig, x);
                startSrcState = 0;
            }
        }
    }
}

//#################################################################################################
icStatusCode appRxIspSrcComit(icCtrl *ctrl) {
    int32_t x;

  //Before this CB is called, full-DDR SOURCE buffers are allocated !!!

  //Ajust UV plane pointers within these buffs
    FrameT *cur = &frameMgrFrameC[0];
    do{
     /*DBG*/ //printf("fbPtr = 0x%lx\n", (uint32_t)cur->fbPtr[0]);
      cur->fbPtr[1] = cur->fbPtr[0] + 4192*3120; //TBD: do this properly
      cur = cur->next;
    }while(cur);

    /*ALLOC*/ AllocOpipeReset(); //clear prev alloc
    /*ALLOC*/ PlgSrcIspCmxAlloc(&plgSrcIsp, &ctrl->source[0].sourceSetup, CheckAllocOmemPool());
    /*DBG*/ //printf("Free = %ld\n", MemMgrGetFreeMem(CheckAllocOmemPool()));

    x = 0;
    if(IPIPE_SRC_SETUP == ctrl->source[x].sourceStatus) {
        gServerInfo.sourceServerCtrl[x].cbStartSource  = appRxIspStartSrc;
        gServerInfo.sourceServerCtrl[x].cbStopSource   = appRxIspStopSrc;
        gServerInfo.pluginServerCtrl[x].cbConfigPlugin = cbConfigIsp;
    }

    ipServerFrameMgrAddBuffs(frameMgrFrameWarp,
                IMG_W_OUT_WARP *  (IMG_H_OUT_WARP), // div by 8 size in bytes
                (IMG_W_OUT_WARP *  IMG_H_OUT_WARP)>>1,
                0);

    gServerInfo.cbDataWasSent = FrameMgrReleaseFrame;
    return IC_STATS_SUCCESS;
}

//#################################################################################################
// Local Functions Implementation

// Output callback linked to plug-ins
static void cbOutput(FrameT *frame, void *pluginObj) {
    UNUSED(pluginObj);
    //uint32_t ispInstance = getIspPluginId(pluginObj);
    ipServerSendData(frame, 0);
}

static void cbSofSourceEvent(void *plg, FrameT *frame) {
    uint32_t idx = getSourcePluginId(plg);
    if(frame) {
        if(nextCfg) {
            frame->appSpecificData = nextCfg;
            nextCfg = NULL;
            ipServerReadoutStart((icSourceInstance)idx,
                    ((icIspConfig*)(frame->appSpecificData))->userData,
                    frame->seqNo, frame->timestamp[0]);
        }
        else {
            frame->appSpecificData = NULL;
            //ipServerReadoutStart((icSourceInstance)idx,  NULL, frame->seqNo, frame->timestamp[0]);
        }
    }
}

static void cbEofSourceEvent(void *plg, FrameT *frame) {
    uint32_t idx = getSourcePluginId(plg);
    if(frame) {
        if(frame->appSpecificData) {
            ipServerReadoutEnd((icSourceInstance)idx,
                    ((icIspConfig*)(frame->appSpecificData))->userData,
                    frame->seqNo, frame->timestamp[0]);
        }
    }
    else{ //send READOUT_END to LOS even when frames get skipped,
          //as that triggers test code to send new configs
        ipServerReadoutEnd((icSourceInstance)idx, 0, 0, 0);
    }
}

static void cbConfigIsp(uint32_t ispInstance, void *iconf) {
    UNUSED(ispInstance);
    nextCfg             = iconf;
    plgSrcIsp.nxtIspCfg = nextCfg;

   //===============================================================
   //Patches that are NOT required for metal-fixed ma2150 silicon:
    extern uint16_t cmxGammaLut[512*4];
    plgSrcIsp.nxtIspCfg->gamma.table  = cmxGammaLut; //for bug 22777
    plgSrcIsp.nxtIspCfg->dog.strength = 0; //LTM-Only for better performance on ma2150
   //===============================================================
}

static void startSourcesLocal(icSourceConfig  *sourceConfig, uint32_t sourceId) {
    icSize   iSize;
    iSize.w  = sourceConfig->cropWindow.x2 - sourceConfig->cropWindow.x1;
    iSize.h  = sourceConfig->cropWindow.y2 - sourceConfig->cropWindow.y1;

    icSize frameSizeOutWarp = {IMG_W_OUT_WARP, IMG_H_OUT_WARP};

  //new SrcIsp outputs YUV420 !
    FrmMgrUtilsInitList(frameMgrFrameC, iSize, FRAME_T_FORMAT_YUV420);
    FrmMgrUtilsInitList(frameMgrFrameWarp,  frameSizeOutWarp, FRAME_T_FORMAT_YUV420);

    plgSrcIsp.eofEvent = cbEofSourceEvent;
    plgSrcIsp.sofEvent = cbSofSourceEvent;
    PlgSrcIspStart(&plgSrcIsp, sourceConfig);
    ipServerSourceReady(sourceId);
}

static void appRxIspStartSrc(uint32_t sourceInstance, icSourceConfig  *sourceConfig) {
    UNUSED(sourceInstance);
    startSrcState     = 1;
    startSrcLocConfig = sourceConfig;
}
static void appRxIspStopSrc(uint32_t sourceInstance) {
    UNUSED(sourceInstance);
    stopSrcState = 1;
}
