
/**************************************************************************************************

 @File         : main_rt.c
 @Author       : Florin Cotoranu
 @Brief        : Contains LRT code starting point
 Date          : April 8th 2016
 E-mail        : florin.cotoranu@movidius.com
 Copyright     : � Movidius Srl 2016, � Movidius Ltd 2016
 Description   : LRT code starting point for isp4xVdoMonoBayer pipeline

 **************************************************************************************************/


/**************************************************************************************************
 ~~~ Included types first then APIs from other modules
 **************************************************************************************************/
#include <stdio.h>

#include "app4CamIspMonoBayer.h"
#include "DrvShaveL2Cache.h"
#include "DrvLeonL2C.h"
#include "swcLeonUtils.h"
#include "ipipe.h"
#include "IpipeServerApi.h"
#include "ipipeMsgQueue.h"


/**************************************************************************************************
 ~~~  Specific #defines
 **************************************************************************************************/


/**************************************************************************************************
 ~~~ Local File function declarations
 **************************************************************************************************/


/**************************************************************************************************
 ~~~  Exported Functions
 **************************************************************************************************/
int main(void)
{
   DrvLL2CDisable(LL2C_OPERATION_INVALIDATE);
   DrvLL2CInitWriteThrough();

   gServerInfo.cbIcSetup       = isp4xMonoBayerCbIcSetup;
   gServerInfo.cbIcTearDown    = isp4xMonoBayerCbIcTearDown;
   gServerInfo.cbSourcesCommit = isp4xMonoBayerSrcComit;

   setupIpipeServer();

   while (1) {
      isp4xMonoBayerMain();
   }
   return 0;
}

/**************************************************************************************************
 ~~~ Local Functions Implementation
 **************************************************************************************************/
