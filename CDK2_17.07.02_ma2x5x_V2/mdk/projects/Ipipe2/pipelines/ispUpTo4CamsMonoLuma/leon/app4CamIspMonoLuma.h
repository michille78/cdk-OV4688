/**************************************************************************************************

 @File         : app4CamIspMonoLuma.h
 @Author       : Florin Cotoranu
 @Brief        : Contains application interface For 4 mono Luma isp in parallel
 Date          : 08 - April - 2016
 E-mail        : florin.cotoranu@movidius.com
 Copyright     : � Movidius Srl 2016, � Movidius Ltd 2016

 Description : Pipeline supports up to 4 mono Luma ISP


 **************************************************************************************************/

#ifndef _ISP4MONOLUMA_H_
#define _ISP4MONOLUMA_H_

/**************************************************************************************************
 ~~~ Included types first then APIs from other modules
 **************************************************************************************************/
#include "IcTypes.h"
#include "ipipe.h"

/**************************************************************************************************
 ~~~  Exported Functions
 **************************************************************************************************/
// as lrt start, this function will be called. (Doc. 4.2)
void isp4xMonoLumaCbIcSetup(icCtrl *ctrl);

// application specific Tear Down
void isp4xMonoLumaCbIcTearDown(void);

//
icStatusCode isp4xMonoLumaSrcComit(icCtrl *ctrl);

//
void isp4xMonoLumaMain(void);

#endif // _ISP4MONOLUMA_H_


