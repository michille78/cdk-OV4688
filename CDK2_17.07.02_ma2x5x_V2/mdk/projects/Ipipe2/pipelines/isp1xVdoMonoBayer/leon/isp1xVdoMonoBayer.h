/**************************************************************************************************

 @File         : isp1xVdoMonoBayer.h
 @Author       : Florin Cotoranu
 @Brief        : Contains 1 Mono Bayer ISP plugin
 Date          : March 29th 2016
 E-mail        : florin.cotoranu@movidius.com
 Copyright     : � Movidius Srl 2016, � Movidius Ltd 2016
 Description   : Receive and process image from one single camera; type of sensor is
                 selected at application level, on LOS

 **************************************************************************************************/

#ifndef _ISP1MONOBAYER_H_
#define _ISP1MONOBAYER_H_

/**************************************************************************************************
 ~~~ Included types first then APIs from other modules
 **************************************************************************************************/
#include "IcTypes.h"
#include "ipipe.h"

/**************************************************************************************************
 ~~~  Exported Functions
 **************************************************************************************************/
// as lrt start, this function will be called. (Doc. 4.2)
void isp1xMonoBayerCbIcSetup(icCtrl *ctrl);

// application specific Tear Down
void isp1xMonoBayerCbIcTearDown(void);

//
icStatusCode isp1xMonoBayerSrcComit(icCtrl *ctrl);

//
void isp1xMonoBayerMain(void);

#endif // _ISP1MONOBAYER_H_


