
/**************************************************************************************************

 @File         : main_rt.c
 @Author       : Florin Cotoranu
 @Brief        : Contains LRT code starting point
 Date          : March 29th 2016
 E-mail        : florin.cotoranu@movidius.com
 Copyright     : � Movidius Srl 2016, � Movidius Ltd 2016
 Description   : LRT code starting point for isp1xVdoMonoBayer pipeline

 **************************************************************************************************/


/**************************************************************************************************
 ~~~ Included types first then APIs from other modules
 **************************************************************************************************/
#include <stdio.h>

#include "DrvShaveL2Cache.h"
#include "DrvLeonL2C.h"
#include "swcLeonUtils.h"
#include "ipipe.h"
#include "IpipeServerApi.h"
#include "ipipeMsgQueue.h"
#include "isp1xVdoMonoBayer.h"


/**************************************************************************************************
 ~~~  Specific #defines
 **************************************************************************************************/


/**************************************************************************************************
 ~~~ Local File function declarations
 **************************************************************************************************/


/**************************************************************************************************
 ~~~  Exported Functions
 **************************************************************************************************/
int main(void)
{
    DrvLL2CDisable(LL2C_OPERATION_INVALIDATE);
    DrvLL2CInitWriteThrough();
    SET_REG_BITS_MASK(SIPP_INT0_ENABLE_ADR, 0);
    SET_REG_BITS_MASK(SIPP_INT1_ENABLE_ADR, 0);
    SET_REG_BITS_MASK(SIPP_INT2_ENABLE_ADR, 0);//patch for not working obfl_inc



    gServerInfo.cbIcSetup       = isp1xMonoBayerCbIcSetup;
    gServerInfo.cbIcTearDown    = isp1xMonoBayerCbIcTearDown;
    gServerInfo.cbSourcesCommit = isp1xMonoBayerSrcComit;


    setupIpipeServer();

    while (1) {
       isp1xMonoBayerMain();
    }
    return 0;
}

/**************************************************************************************************
 ~~~ Local Functions Implementation
 **************************************************************************************************/

