/**************************************************************************************************

 @File         : isp3xVdoPlusDepthSgdm.c
 @Author       : MT
 @Brief        : Contains 3 Isp video, bicubic, 2 downscale sw and Sgdm depth algorithm
 Date          : 04 - December - 2015
 E-mail        : xxx.xxx@movidius.com
 Copyright     : � Movidius Srl 2014, � Movidius Ltd 2015

 Description :


 **************************************************************************************************/


/**************************************************************************************************
 ~~~ Included types first then APIs from other modules
 **************************************************************************************************/
#include "isp3xMonoVdoPlusDepthSgdm.h"

#include <string.h>
#include <stdio.h>
#include <assert.h>
#include "DrvLeonL2C.h"
#include "ipipe.h"
#include "ipipeDbg.h"
#include "FrameMgrUtils.h"
#include "ipipeUtils.h"
#include "ipipeOpipeUtils.h"
#include "FrameMgrApi.h"
#include "IpipeServerApi.h"
#include "ipipeMsgQueue.h"
#include "PlgSourceApi.h"
#include "PlgFifoApi.h"
#include "PlgIspMonoLumaApi.h"
#include "PlgBicubicApi.h"
#include "PlgGs2xDApi.h"
#include "PlgSgbmApi.h"

/**************************************************************************************************
 ~~~  Specific #defines
 **************************************************************************************************/
#ifndef APP_CONFIGURATION

#define NR_OF_BUFFERS_PER_SOURCE            3
#define NR_OF_BUFFERS_PER_ISP_OUT           4
#define NR_OF_BUFFERS_PER_BICUBIC_OUT       3
#define NR_OF_BUFFERS_PER_DOWNSCALE_OUT     4
#define NR_OF_BUFFERS_PER_DEPTH_OUT         3

#endif // APP_CONFIGURATION

#define NR_OF_BUFFERS_PER_STILL_OUT         1

// this is hardcoded for current implementation
#define DEPTH_W 640
#define DEPTH_H 480

// this numbers is hardcoded , plug-ins are statically linked in this way. Not change it.
#define MAX_NR_OF_CAMS                      3
#define NR_OF_DOWNSCALE                     2

// shaves allocation
#define G0_DOWN_SHV                         4
#define G1_DOWN_SHV                         5

// shaves 0, 1, 2, 3 used by Sgbm

// slices 6,7,8,9,10,11 allocated for opipe buffers
/**************************************************************************************************
 ~~~  Local variables
 **************************************************************************************************/

static PlgSource       plgSource[MAX_NR_OF_CAMS] SECTION(".cmx.cdmaDescriptors") ALIGNED(8);
static PlgIspMonoLuma      plgVdo1  [MAX_NR_OF_CAMS] SECTION(".cmx.cdmaDescriptors") ALIGNED(8);
static PlgIspMonoLuma      plgStill                  SECTION(".cmx.cdmaDescriptors") ALIGNED(8);
static PlgFifo         plgFifo;
static PlgBicubic      plgBicubic                SECTION(".cmx.cdmaDescriptors") ALIGNED(8);
static PlgGs2xD        plgGs2xD[NR_OF_DOWNSCALE] SECTION(".cmx.cdmaDescriptors") ALIGNED(8);
static PlgSgbm         plgSgbm                   SECTION(".cmx.cdmaDescriptors") ALIGNED(8);

static FramePool       frameMgrPoolC   [MAX_NR_OF_CAMS];
static FramePool       frameMgrPoolFifo[MAX_NR_OF_CAMS];
static FramePool       frameMgrPoolP   [MAX_NR_OF_CAMS];
static FramePool       frameMgrPoolBicubic;
static FramePool       frameMgrPoolStill;
static FramePool       frameMgrPoolDown [NR_OF_DOWNSCALE];
static FramePool       frameMgrPoolSgdm;

static FrameT          *frameMgrFrameC[MAX_NR_OF_CAMS];
static FrameT          *frameMgrFrameP[MAX_NR_OF_CAMS];
static FrameT          *frameMgrFrameBicubic;
static FrameT          *frameMgrFrameStill;
static FrameT          *frameMgrFrameDown [NR_OF_DOWNSCALE];
static FrameT          *frameMgrFrameSgdm;

static uint32_t        startSrcState     [MAX_NR_OF_CAMS];
static uint32_t        stopSrcState      [MAX_NR_OF_CAMS];
static icSourceConfig  *startSrcLocConfig[MAX_NR_OF_CAMS];
static uint32_t        tearDownEnable = 0;
static void            *nextCfg [MAX_NR_OF_CAMS];
static FrameProducedCB cbOutputList[MAX_NR_OF_CAMS+2];
static uint32_t        dmInit = 0;

static FrameProducedCB cbListIsp2[3];
static FrameProducedCB cbListBic1[3];

/**************************************************************************************************
 ~~~ Local File function declarations
 **************************************************************************************************/
static void     turnOfapp3Cam(void);
static uint32_t checkTurnOfFinalStop(uint32_t *updateVal);
static void     startSourcesLocal(icSourceConfig  *sourceConfig, uint32_t sourceId);
static uint32_t getSourcePluginId(void *plg);
static uint32_t getIspPluginId(void *plg);
static void     app3CamIspStartSrc(uint32_t sourceInstance, icSourceConfig  *sourceConfig);
static void     app3CamIspStopSrc (uint32_t sourceInstance) ;
static void     cbEofSourceEvent(void *plg, FrameT *frame);
static void     cbSofSourceEvent(void *plg, FrameT *frame);
static void     cbConfigIsp    (uint32_t ispInstance, void *iconf);
static void     cbStartIspEvent(void *plg, uint32_t seqNr, void *userData);
static void     cbEndIspEvent  (void *plg, uint32_t seqNr, void *userData);
static void     cbOutput(FrameT *frame, void *pluginObj);


/**************************************************************************************************
 ~~~  Exported Functions
 **************************************************************************************************/
void app3CamIspCbIcSetup(icCtrl *ctrl) {
    uint32_t i;
    OpipeReset(); //general inits
    memset((void*)startSrcState, 0, sizeof(startSrcState));
    memset((void*)stopSrcState, 0, sizeof(stopSrcState));
    memset((void*)startSrcLocConfig, 0, sizeof(startSrcLocConfig));
    memset((void*)nextCfg, 0, sizeof(nextCfg));
    tearDownEnable = 0;
    gServerInfo.cbDataWasSent   = NULL;
    PlgFifoCreate       ((void*)&plgFifo);
    PlgFifoConfig       ((void*)&plgFifo, (uint32_t)MAX_NR_OF_CAMS);
    plgFifo.plg.init(frameMgrPoolFifo, MAX_NR_OF_CAMS, (void*)&plgFifo);
    PlgBicubicCreate    ((void*)&plgBicubic);

    plgBicubic.frmSz.w = DEPTH_W;
    plgBicubic.frmSz.h = DEPTH_H;
    plgBicubic.plg.init(&frameMgrPoolBicubic, 1, (void*)&plgBicubic);


    PlgGs2xDCreate(&plgGs2xD[0]);
    PlgGs2xDCreate(&plgGs2xD[1]);
    PlgGs2xDSetParams(&plgGs2xD[0], 4, plgBicubic.frmSz);
    PlgGs2xDSetParams(&plgGs2xD[1], 5, plgBicubic.frmSz);

    PlgSgbmCreate(&plgSgbm);
    plgSgbm.plg.init(&frameMgrPoolSgdm, 1, (void*)&plgSgbm);

    PlgIspMonoLumaCreate((void*)&plgStill);
    plgStill.plg.init(&frameMgrPoolStill, 1, (void*)&plgStill);
    cbOutputList[4].callback  = cbOutput;
    cbOutputList[4].pluginObj = &plgStill;


    for(i = 0; i < MAX_NR_OF_CAMS; i++) {
        gServerInfo.sourceServerCtrl[i].cbStartSource  = NULL;
        gServerInfo.sourceServerCtrl[i].cbStopSource   = NULL;
        gServerInfo.pluginServerCtrl[i].cbConfigPlugin = NULL;
        PlgSourceCreate ((void*)&plgSource[i], i);
        PlgIspMonoLumaCreate((void*)&plgVdo1[i]);
        plgSource[0].plg.init(&frameMgrPoolC[i], 1, (void*)&plgSource[i]);
        plgVdo1[0].plg.init(&frameMgrPoolP[i], 1, (void*)&plgVdo1[i]);
        gServerInfo.sourceServerCtrl[i].pool = plgSource[i].outputPools;
        frameMgrFrameC[i]       = ipServerFrameMgrCreateList(NR_OF_BUFFERS_PER_SOURCE);
        frameMgrFrameP[i]       = ipServerFrameMgrCreateList(NR_OF_BUFFERS_PER_ISP_OUT);
        // Source output pool
        FrameMgrCreatePool(&frameMgrPoolC[i], frameMgrFrameC[i], &plgFifo.plg.callbacks[i], 1);
        // Mux  output pool, special case for serialization plug-in, no frame inside, as it pass the input data out
        FrameMgrCreatePool(&frameMgrPoolFifo[i],  NULL,                  plgVdo1[i].plg.callbacks, 1);
        cbOutputList[i  ].callback  = cbOutput;
        cbOutputList[i  ].pluginObj = &plgVdo1[i];
    }
    // buffers alocation for all extra buffers
    frameMgrFrameBicubic  = ipServerFrameMgrCreateList(NR_OF_BUFFERS_PER_BICUBIC_OUT);
    frameMgrFrameDown[0]  = ipServerFrameMgrCreateList(NR_OF_BUFFERS_PER_DOWNSCALE_OUT);
    frameMgrFrameDown[1]  = ipServerFrameMgrCreateList(NR_OF_BUFFERS_PER_DOWNSCALE_OUT);
    frameMgrFrameSgdm     = ipServerFrameMgrCreateList(NR_OF_BUFFERS_PER_DEPTH_OUT);
    frameMgrFrameStill    = ipServerFrameMgrCreateList(NR_OF_BUFFERS_PER_STILL_OUT);

    // ISP output Pool
    //cam0
    FrameMgrCreatePool(&frameMgrPoolP[0], frameMgrFrameP[0], &cbOutputList[0], 1);
    FrameMgrCreatePool(&frameMgrPoolStill, frameMgrFrameStill, &cbOutputList[4], 1);
    // cam1
    FrameMgrCreatePool(&frameMgrPoolP[1], frameMgrFrameP[1], &plgBicubic.plg.callbacks[0], 1);
    // bicubic

    cbListBic1[0].callback = plgGs2xD[0].plg.callbacks[0].callback;
    cbListBic1[0].pluginObj = plgGs2xD[0].plg.callbacks[0].pluginObj;
    cbListBic1[1].callback = plgSgbm.plg.callbacks[1].callback;
    cbListBic1[1].pluginObj = plgSgbm.plg.callbacks[1].pluginObj;
    cbListBic1[2].callback  =   cbOutput;
    cbListBic1[2].pluginObj =   &plgVdo1[1];
    FrameMgrCreatePool(&frameMgrPoolBicubic, frameMgrFrameBicubic, cbListBic1, 3);
    //cam2
    cbListIsp2[0].callback  =   plgGs2xD[1].plg.callbacks[0].callback;
    cbListIsp2[0].pluginObj =   plgGs2xD[1].plg.callbacks[0].pluginObj;
    cbListIsp2[1].callback  =   plgSgbm.plg.callbacks[0].callback;
    cbListIsp2[1].pluginObj =   plgSgbm.plg.callbacks[0].pluginObj;
    cbListIsp2[2].callback  =   cbOutput;
    cbListIsp2[2].pluginObj =   &plgVdo1[2];
    FrameMgrCreatePool(&frameMgrPoolP[2], frameMgrFrameP[2], cbListIsp2, 3);

    // downscale 0
    FrameMgrCreatePool(&frameMgrPoolDown[0], frameMgrFrameDown[0], &plgSgbm.plg.callbacks[3], 1);
    // downscale 1
    FrameMgrCreatePool(&frameMgrPoolDown[1], frameMgrFrameDown[1], &plgSgbm.plg.callbacks[2], 1);
    // Bicubic plugin output
    cbOutputList[3  ].callback  = cbOutput;
    cbOutputList[3  ].pluginObj = &plgSgbm;
    FrameMgrCreatePool(&frameMgrPoolSgdm, frameMgrFrameSgdm, &cbOutputList[3], 1);
    dmInit=0;

    // Create pipe description structure
    ipServerRegSourceQuery(0, "Source", IC_SOURCE_ATTR_HAS_VIDEO_ISP | IC_SOURCE_ATTR_HAS_VIDEO_OUT ,
            NR_OF_BUFFERS_PER_SOURCE, 0, 0, 0, 0);
    ipServerRegIspQuery(0,   "IspVdo",      IC_ISP_ATTR_VIDEO_LINK, 0);
    ipServerRegIspQuery(4,   "IspStill",    IC_ISP_ATTR_SILL_LINK, 0);
    ipServerRegOutputQuery(0, "Out",        IC_OUTPUT_FRAME_DATA_TYPE_PREVIEW  , 0); // preview cam output
    ipServerRegOutputQuery(4, "Out",        IC_OUTPUT_FRAME_DATA_TYPE_STILL    , 0); // still cam output
    ipServerRegOutputQuery(5, "Out",        IC_OUTPUT_FRAME_DATA_TYPE_STILL_RAW, 0); // raw cam output

    ipServerQueryAddChild(ctrl->icPipelineDescription.icQuerySource[0], ctrl->icPipelineDescription.icQueryIsp[0]);
    ipServerQueryAddChild(ctrl->icPipelineDescription.icQuerySource[0], ctrl->icPipelineDescription.icQueryIsp[4]);
    ipServerQueryAddChild(ctrl->icPipelineDescription.icQueryIsp[0], ctrl->icPipelineDescription.icQueryOutput[0]);
    ipServerQueryAddChild(ctrl->icPipelineDescription.icQueryIsp[4], ctrl->icPipelineDescription.icQueryOutput[4]);

    ipServerRegSourceQuery(1, "Source", IC_SOURCE_ATTR_HAS_VIDEO_ISP , NR_OF_BUFFERS_PER_SOURCE, 0, 0, 0, 0);
    ipServerRegIspQuery(1,   "IspVdo"   , IC_ISP_ATTR_VIDEO_LINK, 1);
    ipServerRegOutputQuery(2,"Out"      , IC_OUTPUT_FRAME_DATA_TYPE_PREVIEW  , 2); // preview cam output
    ipServerQueryAddChild(ctrl->icPipelineDescription.icQuerySource[1], ctrl->icPipelineDescription.icQueryIsp[1]);

    ipServerRegSourceQuery(2, "Source", IC_SOURCE_ATTR_HAS_VIDEO_ISP , NR_OF_BUFFERS_PER_SOURCE, 0, 0, 0, 0);
    ipServerRegIspQuery(2,   "IspVdo"   , IC_ISP_ATTR_VIDEO_LINK, 2);
    ipServerRegOutputQuery(1,"Out"      , IC_OUTPUT_FRAME_DATA_TYPE_PREVIEW  , 1); // preview cam output
    ipServerQueryAddChild(ctrl->icPipelineDescription.icQuerySource[2], ctrl->icPipelineDescription.icQueryIsp[2]);

    // general plugin type, not isp, source or output
    ipServerRegUserPlgQuery(0, "Warp", 0);
    ipServerRegUserPlgQuery(1, "GaussDownScale", 0);
    ipServerRegUserPlgQuery(2, "GaussDownScale", 0);
    ipServerRegUserPlgQuery(3, "SgbmDm", 0);
    ipServerRegOutputQuery(3, "Out", IC_OUTPUT_FRAME_DATA_TYPE_USER1  , (2<<8)|(1)); // preview cam output

    ipServerQueryAddChild(ctrl->icPipelineDescription.icQueryIsp[1], ctrl->icPipelineDescription.icQueryPlg[0]);
    ipServerQueryAddChild(ctrl->icPipelineDescription.icQueryIsp[2], ctrl->icPipelineDescription.icQueryPlg[2]);
    ipServerQueryAddChild(ctrl->icPipelineDescription.icQueryIsp[2], ctrl->icPipelineDescription.icQueryPlg[3]);
    ipServerQueryAddChild(ctrl->icPipelineDescription.icQueryPlg[0], ctrl->icPipelineDescription.icQueryPlg[3]);
    ipServerQueryAddChild(ctrl->icPipelineDescription.icQueryPlg[0], ctrl->icPipelineDescription.icQueryPlg[1]);
    ipServerQueryAddChild(ctrl->icPipelineDescription.icQueryPlg[1], ctrl->icPipelineDescription.icQueryPlg[3]);
    ipServerQueryAddChild(ctrl->icPipelineDescription.icQueryPlg[2], ctrl->icPipelineDescription.icQueryPlg[3]);
    ipServerQueryAddChild(ctrl->icPipelineDescription.icQueryPlg[3], ctrl->icPipelineDescription.icQueryOutput[3]);
}

//
void app3CamIspCbIcTearDown(void) {
    turnOfapp3Cam();
    tearDownEnable = 1;
}

//
void app3CamIspMain(void) {
    uint32_t x;
    if(tearDownEnable) {
        if(checkTurnOfFinalStop(&tearDownEnable)) {
            ipServerWasTornDown();
            exit(0);
        }
    }

    // triger just if opipe is idle
    if (
            (0 == plgVdo1[0].plg.status)   &&
            (0 == plgVdo1[1].plg.status)   &&
            (0 == plgVdo1[2].plg.status)   &&
            (0 == plgStill.plg.status)
    ) {
        for (x = 0; x < MAX_NR_OF_CAMS; x++) {
            // Start Source Command in order to avoid big interrupt time
            if (1 == startSrcState[x]) {
                startSourcesLocal(startSrcLocConfig[x], x);
                startSrcState[x] = 0;
            }
            // Stop Source Command in order to avoid big interrupt time
            if (1 == stopSrcState[x])
            {
                if(plgSource[x].plg.fini)
                    plgSource[x].plg.fini(&plgSource[x]);
                if(plgVdo1[x].plg.fini)
                    plgVdo1[x].plg.fini(&plgVdo1[x]);
                while ( (PLG_STATS_RUNNING == plgSource[x].plg.status) ||
                        (PLG_STATS_RUNNING == plgVdo1[x].plg.status)) {
                    NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;
                    NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;
                }

                dmInit=dmInit&(~(1<<x));

                if (0 == dmInit) {
                    PlgSgbmStopSrc((void*)&plgSgbm);
                }
                stopSrcState[x] = 0;
                ipServerSourceStopped(x);
            }
        }
        // check if new frame can be run inside sgbm, and start if is possible
        plgSgbmTryRun((void*)&plgSgbm);
        // check capture command queue
        TriggerCaptElement *captureDescriptor;
        if(0 == ipServerQueueGet(&captureDescriptor)) {
            // call still isp associated with this source
            plgStill.plg.trigger(captureDescriptor->buffer,
                    captureDescriptor->config, NULL, &plgStill);
        }
        else {
            plgFifo.triger((void*)&plgFifo);
        }
        NOP;NOP;NOP;NOP;NOP;NOP;NOP;NOP;
    }
}

//
icStatusCode app3CamIspSrcComit(icCtrl *ctrl) {
    int32_t x;
    /*ALLOC*/ AllocOpipeReset(); //clear prev alloc
    /*ALLOC*/ AllocOpipeRxCmxBuffs (ctrl);
    /*ALLOC*/ PlgIspBase *ispBase[MAX_NR_OF_CAMS]; //Isp buffs
    /*ALLOC*/ for(x=0; x<MAX_NR_OF_CAMS; x++)
        /*ALLOC*/   ispBase[x] = &plgVdo1[x].base;
    /*ALLOC*/ AllocOpipeIspCmxBuffs(ctrl, MAX_NR_OF_CAMS, ispBase);
    // Allocate frames buffers memory
    for (x = 0; x < MAX_NR_OF_CAMS; x++)
    {
        if(IPIPE_SRC_SETUP == ctrl->source[x].sourceStatus)
        {
            //size.w = (inSz.w * hN - 1)/hD + 1;
            //size.h = (inSz.h * vN - 1)/vD + 1;
            PlgBicubicConfig((void*)&plgBicubic, plgBicubic.frmSz, (float*)ctrl->source[x].sourceSetup.appSpecificInfo);
            uint32_t maxIspW = ((ctrl->source[x].sourceSetup.maxWidth *
                    ctrl->source[x].sourceSetup.maxHorizN - 1) /
                    ctrl->source[x].sourceSetup.maxHorizD + 1);
            uint32_t maxIspH = ((ctrl->source[x].sourceSetup.maxHeight *
                    ctrl->source[x].sourceSetup.maxVertN - 1) /
                    ctrl->source[x].sourceSetup.maxVertD + 1);
            uint32_t videoFrameSize = (maxIspW * maxIspH);
            ipServerFrameMgrAddBuffs(frameMgrFrameP[x],videoFrameSize, 0, 0);
            // this functionality is available just now !!! Important to do that
            gServerInfo.sourceServerCtrl[x].cbStartSource = app3CamIspStartSrc;
            gServerInfo.sourceServerCtrl[x].cbStopSource = app3CamIspStopSrc;
            gServerInfo.pluginServerCtrl[x].cbConfigPlugin = cbConfigIsp;
            if(0 == x) {
                ipServerFrameMgrAddBuffs(frameMgrFrameStill,
                                   maxIspW * maxIspH,
                                   0,
                                   0);
            }
            if(1 == x) { // if  source 1 is prepared then Bicubic is prepared to
                //uint32_t HARDOCED_DEPT_SIZE = (DEPTH_W * DEPTH_H);
                ipServerFrameMgrAddBuffs(frameMgrFrameBicubic,
                        videoFrameSize, // div by 8 size in bytes
                        0, 0);
                ipServerFrameMgrAddBuffs(frameMgrFrameDown[0],
                        videoFrameSize>>2, // div by 8 size in bytes
                        0, 0);
                ipServerFrameMgrAddBuffs(frameMgrFrameDown[1],
                        videoFrameSize>>2, // div by 8 size in bytes
                        0, 0);
                ipServerFrameMgrAddBuffs(frameMgrFrameSgdm,
                        (DEPTH_W * DEPTH_H), // div by 8 size in bytes
                        0, 0);
            }
        }
    }

    gServerInfo.cbDataWasSent   = FrameMgrReleaseFrame; // !!! different approach here

    return IC_STATS_SUCCESS;
}


/**************************************************************************************************
 ~~~ Local Functions Implementation
 **************************************************************************************************/

// Output callback linked to plug-ins
static void cbOutput(FrameT *frame, void *pluginObj) {
    uint32_t ispInstance = getIspPluginId(pluginObj);
    // add possibility to display all isp output by setting 1, in this place
    if(0) {
        ipServerSendData(frame, ispInstance);
    }
    else {
        if((ispInstance == 1) || (ispInstance == 2)) {
            FrameMgrReleaseFrame(frame);
        }
        else {
            ipServerSendData(frame, ispInstance);
        }
    }
}

static void cbSofSourceEvent(void *plg, FrameT *frame) {
    uint32_t idx = getSourcePluginId(plg);
    if(frame) {
        if(nextCfg[idx]) {
            frame->appSpecificData = nextCfg[idx];
            nextCfg[idx] = NULL;
            ipServerReadoutStart((icSourceInstance)idx,
                    ((icIspConfig*)(frame->appSpecificData))->userData,
                    frame->seqNo, frame->timestamp[0]);
        }
        else {
            frame->appSpecificData = NULL;
//            ipServerReadoutStart((icSourceInstance)idx,  NULL, frame->seqNo, frame->timestamp[0]);
        }
    }
    else {
//        ipServerReadoutStart((icSourceInstance)idx, NULL, 0, 0);
    }
}

static void cbEofSourceEvent(void *plg, FrameT *frame) {
    uint32_t idx = getSourcePluginId(plg);
    if(frame) {
        if(frame->appSpecificData) {
            ipServerReadoutEnd((icSourceInstance)idx,
                    ((icIspConfig*)(frame->appSpecificData))->userData,
                    frame->seqNo, frame->timestamp[0]);
        }
        else {
//            ipServerReadoutEnd((icSourceInstance)idx, 0, frame->seqNo, frame->timestamp[0]);
        }
    }
    else {
//        ipServerReadoutEnd((icSourceInstance)idx, NULL, 0, 0);
    }
}

static void cbStartIspEvent(void *plg, uint32_t seqNr, void *userData) {
    uint32_t ispInstance = getIspPluginId(plg);
    ipServerIspStart(ispInstance, seqNr, userData);
}
static void cbEndIspEvent(void *plg, uint32_t seqNr, void *userData) {
    uint32_t ispInstance = getIspPluginId(plg);
    ipServerIspEnd(ispInstance, seqNr, userData);
}

static void cbConfigIsp(uint32_t ispInstance, void *iconf) {
    nextCfg[ispInstance] = iconf;
}

// Error reporting callbacks
void cbErrorIspEvent(void *plg, icSeverity severity, icError errorNo, void *userData) {
    uint32_t ispInstance = getIspPluginId(plg);
    ipServerIspReportError(ispInstance, severity, errorNo, userData);
}

//###########################################################################################
void startSourcesLocal(icSourceConfig  *sourceConfig, uint32_t sourceId) {
    icSize       iSize, oSize;
    iSize.w  = sourceConfig->cropWindow.x2-sourceConfig->cropWindow.x1;
    iSize.h  = sourceConfig->cropWindow.y2-sourceConfig->cropWindow.y1;
    oSize.w = DEPTH_W;
    oSize.h = DEPTH_H;
    FrmMgrUtilsInitList(frameMgrFrameC[sourceId], iSize,
            FrmMgrUtilsGetRawFrm(sourceConfig->bitsPerPixel,  (sourceConfig->mipiRxData.recNrl <= IC_SIPP_DEVICE3 ? 1 : 0)));

    FrmMgrUtilsInitList(frameMgrFrameP[sourceId], iSize, FRAME_T_FORMAT_RAW_8);
    if(0 != sourceId) {
        plgGs2xD[sourceId-1].plg.init(&frameMgrPoolDown[sourceId-1], 1, (void*)&plgGs2xD[sourceId-1]);
        if(0 == dmInit) { // it is first sensor started
            PlgSgbmStopSrc((void*)&plgSgbm); // clear any old FIFO frame values
            FrmMgrUtilsInitList(frameMgrFrameBicubic, iSize, FRAME_T_FORMAT_RAW_8);
            FrmMgrUtilsInitList(frameMgrFrameDown[0], iSize, FRAME_T_FORMAT_RAW_8);
            FrmMgrUtilsInitList(frameMgrFrameDown[1], iSize, FRAME_T_FORMAT_RAW_8);
            FrmMgrUtilsInitList(frameMgrFrameSgdm, oSize, FRAME_T_FORMAT_RAW_8);
        }
        dmInit=dmInit|(1<<sourceId);
    }

    // set input frames params
    uint32_t sourceFmt = SIPP_FMT_16BIT; //CIF default
    if(sourceConfig->mipiRxData.recNrl <= IC_SIPP_DEVICE3)
        sourceFmt = GetFrameBppPackFormat(sourceConfig->bitsPerPixel);
    PlgIspMonoLumaConfig(&plgVdo1[sourceId], iSize,
            sourceFmt, 0);

    if(0 == sourceId){
        FrmMgrUtilsInitList(frameMgrFrameStill, iSize, FRAME_T_FORMAT_RAW_8);
        PlgIspMonoLumaConfig(&plgStill, iSize,  GetFrameBppPackFormat(sourceConfig->bitsPerPixel), 0);
        plgStill.procesStart      = cbStartIspEvent;
        plgStill.procesEnd        = cbEndIspEvent;
        plgStill.procesIspError   = cbErrorIspEvent;
    }
    if(0 != sourceId) {
        if (DEPTH_W != iSize.w){
            // invalid resolution for sgbm. Sgbm algorithm is made for 640x480 resolution
            // no Crop implemented for mono pipeline
            assert(0);
        }
    }
    plgSource[sourceId].eofEvent     = cbEofSourceEvent;
    plgSource[sourceId].sofEvent     = cbSofSourceEvent;
    plgVdo1[sourceId].procesStart    = cbStartIspEvent;
    plgVdo1[sourceId].procesEnd      = cbEndIspEvent;
    plgVdo1[sourceId].procesIspError = cbErrorIspEvent;
    PlgSourceStart(&plgSource[sourceId], sourceConfig,
            GetFrameBppPackFormat(sourceConfig->bitsPerPixel));
    ipServerSourceReady(sourceId);
}

static void app3CamIspStartSrc(uint32_t sourceInstance, icSourceConfig  *sourceConfig) {
    startSrcState[sourceInstance] = 1;
    startSrcLocConfig[sourceInstance] = sourceConfig;
}
static void app3CamIspStopSrc(uint32_t sourceInstance) {
    stopSrcState[sourceInstance] = 1;
}

// Turn off capability ############################################################################
static void turnOfapp3Cam(void)
{
    uint32_t i;
    for(i = 0; i < MAX_NR_OF_CAMS; i++) {
        if(plgVdo1[i].plg.fini)   plgVdo1[i].plg.fini(&plgVdo1[i]);
        if(plgSource[i].plg.fini) plgSource[i].plg.fini(&plgSource[i]);
        gServerInfo.sourceServerCtrl[i].cbStartSource  = NULL;
        gServerInfo.sourceServerCtrl[i].cbStopSource   = NULL;
        gServerInfo.pluginServerCtrl[i].cbConfigPlugin = NULL;
    }
    if(plgFifo.plg.fini) plgFifo.plg.fini(&plgFifo);
    if(plgBicubic.plg.fini) plgBicubic.plg.fini(&plgBicubic);
    if(plgGs2xD[0].plg.fini) plgGs2xD[0].plg.fini(&plgGs2xD[0]);
    if(plgGs2xD[1].plg.fini) plgGs2xD[1].plg.fini(&plgGs2xD[1]);
    if(plgSgbm.plg.fini) plgSgbm.plg.fini(&plgSgbm);
    gServerInfo.cbDataWasSent   = NULL;
}

static uint32_t checkTurnOfFinalStop(uint32_t *updateVal) {
    if(     (0 == plgSource[0].plg.status) &&
            (0 == plgSource[1].plg.status) &&
            (0 == plgSource[2].plg.status) &&
            (0 == plgVdo1[0].plg.status)   &&
            (0 == plgVdo1[1].plg.status)   &&
            (0 == plgVdo1[2].plg.status)   &&
            (0 == plgFifo.plg.status)      &&
            (0 == plgBicubic.plg.status)   &&
            (0 == plgGs2xD[0].plg.status)  &&
            (0 == plgGs2xD[1].plg.status)  &&
            (0 == plgSgbm.plg.status)
    ) {
        MemMgrReset();
        *updateVal = 0;
        return 1;
    }
    return 0;
}

static uint32_t getSourcePluginId(void *plg) {
    uint32_t i;
    for(i = 0; i < MAX_NR_OF_CAMS; i++)
        if(plg == &plgSource[i]) return i;
    assert(0); return 0;
}

static uint32_t getIspPluginId(void *plg) {
    uint32_t i;
    for(i = 0; i < MAX_NR_OF_CAMS; i++)
        if(plg == &plgVdo1[i]) return i;
    if(plg == &plgSgbm)     return MAX_NR_OF_CAMS;
    if(plg == &plgStill)    return 4;
    assert(0); return 0;
}
