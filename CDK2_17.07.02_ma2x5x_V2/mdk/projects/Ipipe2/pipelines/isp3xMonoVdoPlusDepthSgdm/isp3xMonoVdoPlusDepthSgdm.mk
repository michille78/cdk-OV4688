PIPE_NAME = isp3xMonoVdoPlusDepthSgdm
MV_COMMON_BASE 	?= ../../../../../../common
#assumed preexisting define on upper layer
ComponentList_LRT += Opipe      \
					UnitTestVcs \
					VcsHooks    \
					$(IPIPE_COMPONENTS)/bicubicWarp \
					$(IPIPE_COMPONENTS)/IpipeMsgQueue \
					$(IPIPE_COMPONENTS)/FrameMgr      \
					$(IPIPE_COMPONENTS)/MemMgr        \
					$(IPIPE_COMPONENTS)/IpipeServer   \
					$(IPIPE_COMPONENTS)/../common     \
					$(IPIPE_PLUGINS_BASE_ARCH)/common/IspCommon \
					$(IPIPE_PLUGINS_BASE_ARCH)/PlgSource        \
					$(IPIPE_PLUGINS_BASE_ARCH)/PlgFifo          \
					$(IPIPE_PLUGINS_BASE_ARCH)/PlgIspMonoLuma       \
					$(IPIPE_PLUGINS_BASE_ARCH)/PlgBicubic       \
					$(IPIPE_PLUGINS_BASE)/PlgGs2xD \
					$(IPIPE_PLUGINS_BASE)/PlgSgbm \
					$(IPIPE_COMPONENTS)/../pipelines/appUtils     \
					$(IPIPE_COMPONENTS)/../pipelines/$(PIPE_NAME) 

#opipe extra defines
CCOPT_LRT += -D'OPIPE_RUNTIME_CHECKS'

LEON_RT_LIB_NAME       	= $(DirAppObjBase)/leonRTApp
                  
# Bicubic specific defines options
CCOPT   += -DCALIB_FILENAME="\"$(CALIB_FILE)\""


include $(IPIPE_BASE)/plugins/PlgGs2xD/build/myriad2/PlgGs2xD.mk
include $(IPIPE_BASE)/plugins/PlgSgbm/build/myriad2/PlgSgbm.mk
