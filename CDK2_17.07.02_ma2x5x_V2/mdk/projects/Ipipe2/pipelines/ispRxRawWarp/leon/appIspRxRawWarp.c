#include <string.h>
#include <assert.h>
#include "ipipe.h"
#include "IpipeServerApi.h"
#include "ipipeOpipeUtils.h"
#include "FrameMgrUtils.h"
#include "FrameMgrApi.h"
#include "PlgSrcIspApi.h"
#include "PlgSourceApi.h"
#include "appIspRxRawWarp.h"

/// Warp
#include "PlgWarpApi.h"
#include "init_mesh_y.h"
#include "init_mesh_uv.h"

#define MAX_NR_OF_CAMS             2
#define NR_OF_BUFFERS_PER_SOURCE   4
#define NR_OF_BUFFERS_PER_WARP_OUT  4

#define ISP_RX_OUT_ID  0
#define RAW_OUT_ID     1
#define ISP_RX_STILL_OUT_ID  2

// Warp Hardcoded
#define IMG_W_IN_WARP (4000)
#define IMG_H_IN_WARP (3000)
#define IMG_W_OUT_WARP (1920)
#define IMG_H_OUT_WARP (1088)
#define MESH_CELL_SIZE 16
#define MESH_WIDTH     MESH_WIDTH_F
#define MESH_HEIGHT    MESH_HEIGHT_F
#define MESH_WIDTH_C   MESH_WIDTH_F_CHROMA
#define MESH_HEIGHT_C  MESH_HEIGHT_F_CHROMA
//

enum {
    PLG_ISPSRC_VIDEO_MODE_VDO,
    PLG_ISPSRC_STILL_MODE_VDO,
    PLG_ISPSRC_STILL_MODE_STL,
    PLG_ISPSRC_LOWPO_MODE_VDO,
    MAX_NO_OF_PREDEFINED_MODE
};
//in cdmaDescriptor section that bypasses caches !!!
static PlgSrcIsp   plgSrcIsp SECTION(".cmx_direct.data") ALIGNED(8);
static PlgSource   plgSource SECTION(".cmx_direct.data") ALIGNED(8);
static PlgWarp     plgWarp   SECTION(".cmx_direct.data") ALIGNED(8);


static FramePool   frameMgrPoolIsp;
static FramePool   frameMgrPoolRaw;
static FramePool   frameMgrPoolWarp;

static FrameT     *frameMgrFrameIsp;
static FrameT     *frameMgrFrameRaw;
static FrameT     *frameMgrFrameWarp;

static uint32_t        startSrcState[MAX_NR_OF_CAMS];
static uint32_t        stopSrcState[MAX_NR_OF_CAMS];
static icSourceConfig  *startSrcLocConfig[MAX_NR_OF_CAMS];
static uint32_t        tearDownEnable;

static FrameProducedCB cbOutputIsp[2];
static FrameProducedCB cbOutputWarp;
static FrameProducedCB cbOutputRaw;

// Warp Hardcoded
SECTION(".cmx_direct.data") static meshStruct init_mesh_Y0 =
{
        .meshWidth  = MESH_WIDTH,
        .meshHeight = MESH_HEIGHT,
        .meshX      = (float*)initMeshY_x,
        .meshY      = (float*)initMeshY_y,
        .coord_min_x = 0,
        .coord_max_x = 960,
        .coord_min_y = 0,
        .coord_max_y = 1088
};
SECTION(".cmx_direct.data") static meshStruct init_mesh_Y1 =
{
        .meshWidth  = MESH_WIDTH,
        .meshHeight = MESH_HEIGHT,
        .meshX      = (float*)initMeshY_x,
        .meshY      = (float*)initMeshY_y,
        .coord_min_x = 960,
        .coord_max_x = 1920,
        .coord_min_y = 0,
        .coord_max_y = 1088
};
SECTION(".cmx_direct.data") static meshStruct init_mesh_UV =
{
        .meshWidth  = MESH_WIDTH_C,
        .meshHeight = MESH_HEIGHT_C,
        .meshX      = (float*)initMeshChroma_x,
        .meshY      = (float*)initMeshChroma_y,
        .coord_min_x = 0,
        .coord_max_x = 960,
        .coord_min_y = 0,
        .coord_max_y = 544
};
// in order to not recalculate scale factors internally, in interrupt context
UpfirdnCfg upfirdnCfgY[MAX_NO_OF_PREDEFINED_MODE] = {
        {0, 16, 25, 16, 25, NULL, NULL, 0, 0, 0, 0},
        {0,  2,  5,  2,  5, NULL, NULL, 0, 0, 0, 0},
        {0,  1,  1,  1,  1, NULL, NULL, 0, 0, 0, 0},
        {0,  1,  1,  1,  1, NULL, NULL, 0, 0, 0, 0},
};
UpfirdnCfg upfirdnCfgC[MAX_NO_OF_PREDEFINED_MODE];
YuvScale   yuvScale[MAX_NO_OF_PREDEFINED_MODE];
icSize  outWarpModeSizes[MAX_NO_OF_PREDEFINED_MODE] = {
        {1920, 1088},
        {1440, 1088},
        {   0,    0},
        {1280,  736},
};

icSize  outIspModeSizes[MAX_NO_OF_PREDEFINED_MODE] = {
        {2560, 1920},
        {1600, 1200},
        {4000, 3000},
        {2000, 1500},
};

static uint32_t getSourcePluginId(void *plg) {if(plg == &plgSrcIsp) return ISP_RX_OUT_ID; else return RAW_OUT_ID;}
static void     startSourcesLocal(icSourceConfig  *sourceConfig, uint32_t sourceId);
static void     appRxIspStartSrc(uint32_t sourceInstance, icSourceConfig  *sourceConfig);
static void     appRxIspStopSrc (uint32_t sourceInstance) ;
static void     cbEofSourceEvent(void *plg, FrameT *frame);
static void     cbSofSourceEvent(void *plg, FrameT *frame);
static void     cbConfigIsp    (uint32_t ispInstance, void *iconf);

// Output callback linked to plug-ins
static inline void outputIsp(FrameT *frame, void *pluginObj) {
    UNUSED(pluginObj);
    assert(frame);
    assert(frame->appSpecificData);
    if (((icIspConfig*)(frame->appSpecificData))->pipeControl & (IC_PIPECTL_MODE_STILL_STILL)) {
        ipServerSendData(frame, ISP_RX_STILL_OUT_ID);
    }
    else {
        FrameMgrReleaseFrame(frame);
    }

}
static inline void outputWarp(FrameT *frame, void *pluginObj) {
    UNUSED(pluginObj);
    assert(frame);
    assert(frame->appSpecificData);
    if (((icIspConfig*)(frame->appSpecificData))->pipeControl &
            (IC_PIPECTL_MODE_VIDEO | IC_PIPECTL_MODE_STILL_VIDEO | IC_PIPECTL_MODE_LOW_POWER)) {
        ipServerSendData(frame, ISP_RX_OUT_ID);
    }
    else {
        FrameMgrReleaseFrame(frame);
    }
}
static void ispTrigerWarp(FrameT *frame, void *pluginObj) {
    UNUSED(pluginObj);
    assert(frame);
    assert(frame->appSpecificData);
    // other mode that still capture, run warp, else release frame
    if (0 == (((icIspConfig*)(frame->appSpecificData))->pipeControl & (IC_PIPECTL_MODE_STILL_STILL))) {
       plgWarp.plg.callbacks[0].callback(frame, plgWarp.plg.callbacks[0].pluginObj);
    }
    else {
        FrameMgrReleaseFrame(frame);
    }
}
static inline void outputRaw(FrameT *frame, void *pluginObj) { UNUSED(pluginObj); ipServerSendData(frame, RAW_OUT_ID);}

static void userEvendCb(void *eventStruct, uint32_t id);

static void procesIspError (void* plg, PlgSrcIspErrors errorNo) {
    UNUSED(plg);
    if(ERR_PLGSRCISP_NO_OUT_BUF == errorNo) {
        ipServerIspReportError(ISP_RX_OUT_ID, IC_SEVERITY_NORMAL, IC_ERROR_RT_OUT_BUFFERS_NOT_AVAILABLE, (void*)0);
    }
    // if(ERR_PLGSRCISP_NO_ISP_CFG == errorNo) {
    //     ipServerIspReportError(ISP_RX_OUT_ID, IC_SEVERITY_NORMAL, IC_ERROR_RT_CFG_MISSING, (void*)0);
    // }
}


//#################################################################################################
void appRxIspCbIcSetup(icCtrl *ctrl)
{
    uint32_t j;
    OpipeReset(); //general inits

    memset((void*)startSrcState,     0, sizeof(startSrcState));
    memset((void*)stopSrcState,      0, sizeof(stopSrcState));
    memset((void*)startSrcLocConfig, 0, sizeof(startSrcLocConfig));
    tearDownEnable            = 0;
    gServerInfo.cbDataWasSent = NULL;
    gServerInfo.cbIcUserMsg   = userEvendCb;

    {// create plug-ins IspRx
        uint32_t i = ISP_RX_OUT_ID;
        gServerInfo.sourceServerCtrl[i].cbStartSource  = NULL;
        gServerInfo.sourceServerCtrl[i].cbStopSource   = NULL;
        gServerInfo.pluginServerCtrl[i].cbConfigPlugin = NULL;

        PlgSrcIspCreate ((void*)&plgSrcIsp, i);
        // create pre-computed polifir params
        for (j = 0; j < MAX_NO_OF_PREDEFINED_MODE; j++) {
            computePolyfirParamsStruct(&yuvScale[j], &upfirdnCfgY[j]);
            upfirdnCfgY[j]  = yuvScale[j].polyY;
            upfirdnCfgC[j]  = yuvScale[j].polyC;
        }
        plgSrcIsp.plg.init(&frameMgrPoolIsp, 1, (void*)&plgSrcIsp);

        /// Eis
        icSize frameSizeIn  = {IMG_W_IN_WARP, IMG_H_IN_WARP};
        icSize frameSizeOut = {IMG_W_OUT_WARP, IMG_H_OUT_WARP};
        PlgWarpCreate ((void*)&plgWarp);
        PlgWarpConfig(&plgWarp, frameSizeIn, frameSizeOut,
                &init_mesh_Y0, &init_mesh_Y1, &init_mesh_UV,
                0x80 , i);
        plgWarp.plg.init (&frameMgrPoolWarp, 1, (void*)&plgWarp);

        gServerInfo.sourceServerCtrl[i].pool = plgSrcIsp.outputPools;
        frameMgrFrameIsp = ipServerFrameMgrCreateList(NR_OF_BUFFERS_PER_SOURCE);
        frameMgrFrameWarp = ipServerFrameMgrCreateList(NR_OF_BUFFERS_PER_WARP_OUT);

        // init callback output, this pluginObj not refer this time to output plugin, refere to source
#ifndef DISABLE_EIS
        cbOutputIsp[0].callback  = outputIsp;
        cbOutputIsp[0].pluginObj = &plgSrcIsp;
        cbOutputIsp[1].callback  = ispTrigerWarp;
        cbOutputIsp[1].pluginObj = (void*)&plgWarp;
        FrameMgrCreatePool(&frameMgrPoolIsp, frameMgrFrameIsp, cbOutputIsp, 2);

        // plugin, in order to identify the source
        cbOutputWarp.callback  = outputWarp;
        cbOutputWarp.pluginObj = &plgWarp;
        FrameMgrCreatePool(&frameMgrPoolWarp,  frameMgrFrameWarp,  &cbOutputWarp, 1);
#else
        UNUSED(cbOutputWarp);
        cbOutputIsp[0].callback  = outputIsp;
        cbOutputIsp[0].pluginObj = &plgSrcIsp;
        cbOutputIsp[1].callback  = outputWarp;
        cbOutputIsp[1].pluginObj = &plgWarp; // SOURCE->OUTPUT link
        FrameMgrCreatePool(&frameMgrPoolIsp, frameMgrFrameIsp, cbOutputIsp, 2);
#endif
        // create descriptions for available functionality regarding isp. Los isp side, base on this
        // informations, will properly update parameters and config sensors.
        ipServerRegSourceQuery(i,
                "SourceIsp",
                IC_SOURCE_ATTR_HAS_VIDEO_ISP | IC_SOURCE_ATTR_HAS_STILL_ISP|
                IC_SOURCE_ATTR_HAS_VIDEO_OUT | IC_SOURCE_ATTR_HAS_STILL_OUT,
                NR_OF_BUFFERS_PER_SOURCE,
                i, ISP_RX_STILL_OUT_ID, 0, 0);
#ifndef DISABLE_EIS
        ipServerRegOutputQuery(i , "OutVdo",    IC_OUTPUT_FRAME_DATA_TYPE_PREVIEW  , i); // preview cam output
        ipServerRegOutputQuery(ISP_RX_STILL_OUT_ID , "OutStill",    IC_OUTPUT_FRAME_DATA_TYPE_STILL  , i); // preview cam output
        ipServerRegUserPlgQuery(ISP_RX_OUT_ID, "Warp", 0);
        ipServerQueryAddChild(ctrl->icPipelineDescription.icQuerySource[i], ctrl->icPipelineDescription.icQueryPlg[ISP_RX_OUT_ID]);
        ipServerQueryAddChild(ctrl->icPipelineDescription.icQueryPlg[i], ctrl->icPipelineDescription.icQueryOutput[ISP_RX_OUT_ID]);
        ipServerQueryAddChild(ctrl->icPipelineDescription.icQuerySource[i], ctrl->icPipelineDescription.icQueryOutput[ISP_RX_STILL_OUT_ID]);
#else
        ipServerRegOutputQuery(i , "OutVdo",    IC_OUTPUT_FRAME_DATA_TYPE_PREVIEW  , i); // preview cam output
        ipServerRegOutputQuery(ISP_RX_STILL_OUT_ID , "OutStill",    IC_OUTPUT_FRAME_DATA_TYPE_STILL  , i); // preview cam output
        ipServerQueryAddChild(ctrl->icPipelineDescription.icQuerySource[i], ctrl->icPipelineDescription.icQueryOutput[ISP_RX_OUT_ID]);
        ipServerQueryAddChild(ctrl->icPipelineDescription.icQuerySource[i], ctrl->icPipelineDescription.icQueryOutput[ISP_RX_STILL_OUT_ID]);
#endif
    };

    {// create plug-ins IspRx
        uint32_t i = RAW_OUT_ID;
        gServerInfo.sourceServerCtrl[i].cbStartSource  = NULL;
        gServerInfo.sourceServerCtrl[i].cbStopSource   = NULL;
        gServerInfo.pluginServerCtrl[i].cbConfigPlugin = NULL;

        PlgSourceCreate ((void*)&plgSource, i);
        plgSource.plg.init(&frameMgrPoolRaw, 1, (void*)&plgSource);

        gServerInfo.sourceServerCtrl[i].pool = plgSource.outputPools;
        frameMgrFrameRaw = ipServerFrameMgrCreateList(NR_OF_BUFFERS_PER_SOURCE);

        // init callback output, this pluginObj not refer this time to output plugin, refere to source
        // plugin, in order to identify the source
        cbOutputRaw.callback  = outputRaw;
        cbOutputRaw.pluginObj = &plgSource;

        // SOURCE->OUTPUT link
        FrameMgrCreatePool(&frameMgrPoolRaw, frameMgrFrameRaw, &cbOutputRaw, 1);

        // create descriptions for available functionality regarding isp. Los isp side, base on this
        // informations, will properly update parameters and config sensors.
        ipServerRegSourceQuery(i,
                "Source",
                IC_SOURCE_ATTR_HAS_RAW_OUT,
                NR_OF_BUFFERS_PER_SOURCE,
                0, 0, i, 0);
        ipServerRegOutputQuery(i , "Out",    IC_OUTPUT_FRAME_DATA_TYPE_STILL_RAW  , i); // preview cam output
        ipServerQueryAddChild(ctrl->icPipelineDescription.icQuerySource[i], ctrl->icPipelineDescription.icQueryOutput[i]);
    };
}

void appRxIspCbIcTearDown(void) {
    //turnOfappRx();
    tearDownEnable = 1;
}

int sippOpipeResurcesFree(void) {
    return 1;
}

void regenerateIdentityMesh(icSize frameSizeOut) {
    uint32_t meshYWidth  = (frameSizeOut.w>>4)+1;
    uint32_t meshYHeight = (frameSizeOut.h>>4)+1;
    uint32_t x = 0;
    uint32_t y = 0;
    for (y = 0; y < meshYHeight; y++) {
        for (x = 0; x < meshYWidth; x++) {
            init_mesh_Y0.meshX[y*meshYWidth+x] = x<<4;
            init_mesh_Y0.meshY[y*meshYWidth+x] = y<<4;
        }
    }
    /// Update mesh sizes Y0
    init_mesh_Y0.meshWidth  = meshYWidth;
    init_mesh_Y0.meshHeight = meshYHeight;
    init_mesh_Y0.coord_min_x = 0;
    init_mesh_Y0.coord_max_x = (frameSizeOut.w>>1);
    init_mesh_Y0.coord_min_y = 0;
    init_mesh_Y0.coord_max_y = frameSizeOut.h;

    /// Update mesh sizes Y1
    init_mesh_Y1.meshWidth  = meshYWidth;
    init_mesh_Y1.meshHeight = meshYHeight;
    init_mesh_Y1.coord_min_x = (frameSizeOut.w>>1);
    init_mesh_Y1.coord_max_x = frameSizeOut.w;
    init_mesh_Y1.coord_min_y = 0;
    init_mesh_Y1.coord_max_y = frameSizeOut.h;

    meshYWidth  = (frameSizeOut.w>>5)+1;
    meshYHeight = (frameSizeOut.h>>5)+1;
    for (y = 0; y < meshYHeight; y++) {
        for (x = 0; x < meshYWidth; x++) {
            init_mesh_UV.meshX[y*meshYWidth+x] = x<<4;
            init_mesh_UV.meshY[y*meshYWidth+x] = y<<4;
        }
    }
    /// Update mesh sizes UV
    init_mesh_UV.meshWidth  = meshYWidth;
    init_mesh_UV.meshHeight = meshYHeight;
    init_mesh_UV.coord_min_x = 0;
    init_mesh_UV.coord_max_x = (frameSizeOut.w>>1);
    init_mesh_UV.coord_min_y = 0;
    init_mesh_UV.coord_max_y = (frameSizeOut.h>>1);
}
//#################################################################################################
void appRxIspMain()
{
    if((tearDownEnable)&& (PLG_STATS_IDLE == plgWarp.plg.status)) {
        if(plgSrcIsp.plg.fini)
            plgSrcIsp.plg.fini(&plgSrcIsp);
        if(plgWarp.plg.fini)
            plgWarp.plg.fini(&plgWarp);
        if(plgSource.plg.fini)
            plgSource.plg.fini(&plgSource);
        ipServerWasTornDown();
        exit(0);
    }

#ifndef DISABLE_EIS
    /// Eis scheduler
    //TODO: Need better value to track whether plugin is off.
    if (plgWarp.crtStatus != 0)
    {
        if(plgWarp.meshGenEnd == 1)
        {
            /// If meshgen finished -> trigger warp start
            plgWarp.meshGenEnd = 0;
            plgWarp.triggerStartWarp(&plgWarp);
        }

        if(plgWarp.chromaisrcount == 1)
        {
            /// If 1st UV plane finished -> Start warping the second UV Plane
            plgWarp.triggerStartVPlane(&plgWarp);
        }

        if(plgWarp.chrEOF == 1)
        {
            /// If both UV planes have finished -> Trigger Chroma EOF
            plgWarp.chrEOF = 0;
            plgWarp.triggerChroma(&plgWarp);
        }

        if(plgWarp.lumaEOF == 1)
        {
            /// If all Y shaves have finished -> Trigger luma EOF
            plgWarp.lumaEOF = 0;
            plgWarp.triggerLuma(&plgWarp);
        }

        if((PLG_STATS_IDLE == plgWarp.plg.status) && /// if status is idle
           (plgWarp.runEnFlag > 0)) /// if plugin is enabled (by frame interrupt)
        {
            plgWarp.runEnFlag--;
                if(plgWarp.frameInExpectation[plgWarp.nextRunIdx] != 0) {
                    FrameT* currentIFrame = plgWarp.frameInExpectation[plgWarp.nextRunIdx];
                    icSize frameSizeIn;
                    icSize frameSizeOut;
                    frameSizeIn.w = currentIFrame->stride[0];
                    frameSizeIn.h = currentIFrame->height[0];
                if(outIspModeSizes[PLG_ISPSRC_VIDEO_MODE_VDO].w == frameSizeIn.w)
                    frameSizeOut = outWarpModeSizes[PLG_ISPSRC_VIDEO_MODE_VDO];
                else
                    if(outIspModeSizes[PLG_ISPSRC_STILL_MODE_VDO].w == frameSizeIn.w)
                        frameSizeOut = outWarpModeSizes[PLG_ISPSRC_STILL_MODE_VDO];
                    else
                        if(outIspModeSizes[PLG_ISPSRC_LOWPO_MODE_VDO].w == frameSizeIn.w)
                            frameSizeOut = outWarpModeSizes[PLG_ISPSRC_LOWPO_MODE_VDO];
                        else
                            assert(0); // invalid resolution was trigger to warp
                regenerateIdentityMesh(frameSizeOut);
                        PlgWarpReConfig(&plgWarp, frameSizeIn, frameSizeOut,
                                &init_mesh_Y0, &init_mesh_Y1, &init_mesh_UV,
                                0x80);
            plgWarp.triger(&plgWarp);
            }
        }
    }
#endif

    //trigger just if opipe is idle,
    if (sippOpipeResurcesFree()) {
        // IspRx Start Source Command in order to avoid big interrupt time
        if (1 == startSrcState[ISP_RX_OUT_ID]) {
            startSourcesLocal(startSrcLocConfig[ISP_RX_OUT_ID], ISP_RX_OUT_ID);
            startSrcState[ISP_RX_OUT_ID] = 0;
        }
        // Raw Start Source Command in order to avoid big interrupt time
        if (1 == startSrcState[RAW_OUT_ID]) {
            startSourcesLocal(startSrcLocConfig[RAW_OUT_ID], RAW_OUT_ID);
            startSrcState[RAW_OUT_ID] = 0;
            ipServerSendUserMsgToLos((void*)NULL, 8);
        }

        // Stop Source Command in order to avoid big interrupt time
        if ((1 == stopSrcState[ISP_RX_OUT_ID]) && (PLG_STATS_IDLE == plgWarp.plg.status)){
            if(plgSrcIsp.plg.fini)
                plgSrcIsp.plg.fini(&plgSrcIsp);
            if(plgWarp.plg.fini)
                plgWarp.plg.fini(&plgWarp);
            stopSrcState[ISP_RX_OUT_ID] = 0;
            ipServerSourceStopped(ISP_RX_OUT_ID);
        }

        // Stop Source Command in order to avoid big interrupt time
        if (1 == stopSrcState[RAW_OUT_ID]) {
            if(plgSource.plg.fini)
                plgSource.plg.fini(&plgSource);
            stopSrcState[RAW_OUT_ID] = 0;
            ipServerSourceStopped(RAW_OUT_ID);
        }
    }
}

//#################################################################################################
icStatusCode appRxIspSrcComit(icCtrl *ctrl) {
    //Before this CB is called, full-DDR SOURCE buffers are allocated !!!
    /*ALLOC*/ AllocOpipeReset(); //clear prev alloc
    /*ALLOC*/ PlgSrcIspCmxAlloc(&plgSrcIsp, &ctrl->source[0].sourceSetup, CheckAllocOmemPool());
    /*DBG*/ //printf("Free = %ld\n", MemMgrGetFreeMem(CheckAllocOmemPool()));

    if(IPIPE_SRC_SETUP == ctrl->source[ISP_RX_OUT_ID].sourceStatus) {
        gServerInfo.sourceServerCtrl[ISP_RX_OUT_ID].cbStartSource  = appRxIspStartSrc;
        gServerInfo.sourceServerCtrl[ISP_RX_OUT_ID].cbStopSource   = appRxIspStopSrc;
        gServerInfo.pluginServerCtrl[ISP_RX_OUT_ID].cbConfigPlugin = cbConfigIsp;
        ipServerFrameMgrAddBuffs(frameMgrFrameWarp,
                IMG_W_OUT_WARP *  (IMG_H_OUT_WARP), // div by 8 size in bytes
                (IMG_W_OUT_WARP *  IMG_H_OUT_WARP)>>1,
                0);
    }
    if(IPIPE_SRC_SETUP == ctrl->source[RAW_OUT_ID].sourceStatus) {
        gServerInfo.sourceServerCtrl[RAW_OUT_ID].cbStartSource  = appRxIspStartSrc;
        gServerInfo.sourceServerCtrl[RAW_OUT_ID].cbStopSource   = appRxIspStopSrc;
        gServerInfo.pluginServerCtrl[RAW_OUT_ID].cbConfigPlugin = cbConfigIsp;
    }
    gServerInfo.cbDataWasSent = FrameMgrReleaseFrame;
    return IC_STATS_SUCCESS;
}

//#################################################################################################
// Local Functions Implementation



static void cbSofSourceEvent(void *plg, FrameT *frame) {
    uint32_t idx = getSourcePluginId(plg);
    if(ISP_RX_OUT_ID == idx) { // special approach on connected to isp
        // unexpected behavior, sof have to happens just if cfg and bufer exist
        assert(frame);
        assert(frame->appSpecificData);
        ipServerReadoutStart((icSourceInstance)idx,
                ((icIspConfig*)(frame->appSpecificData))->userData,
                frame->seqNo, frame->timestamp[0]);
        ipServerIspStart(idx, frame->seqNo, ((icIspConfig*)(frame->appSpecificData))->userData);
    }
    else { // raw source , not controlled by guzzi
    if(frame) {
            ipServerReadoutStart((icSourceInstance)idx,
                    NULL,
                    frame->seqNo, frame->timestamp[0]);
        }
    }
}

static void cbEofSourceEvent(void *plg, FrameT *frame) {
    uint32_t idx = getSourcePluginId(plg);
    if(frame) {
        if(frame->appSpecificData) {
            ipServerReadoutEnd((icSourceInstance)idx,
                    ((icIspConfig*)(frame->appSpecificData))->userData,
                    frame->seqNo, frame->timestamp[0]);
            if(ISP_RX_OUT_ID == idx) { // special approach on connected to isp
                ipServerIspEnd(idx, frame->seqNo, ((icIspConfig*)(frame->appSpecificData))->userData);
            }
        }
    }
    // else{ //send READOUT_END to LOS even when frames get skipped,
    //     //as that triggers test code to send new configs
    //     ipServerReadoutEnd((icSourceInstance)idx, 0, 0, 0);
    // }
}

static inline uint32_t getModeIdx(uint32_t pipeControl) {
    switch (pipeControl) {
    case IC_PIPECTL_MODE_STILL_STILL:
        return PLG_ISPSRC_STILL_MODE_STL;
        break;
    case IC_PIPECTL_MODE_VIDEO:
        return PLG_ISPSRC_VIDEO_MODE_VDO;
        break;
    case IC_PIPECTL_MODE_STILL_VIDEO:
        return PLG_ISPSRC_STILL_MODE_VDO;
        break;
    case IC_PIPECTL_MODE_LOW_POWER:
        return PLG_ISPSRC_LOWPO_MODE_VDO;
        break;
    default:
        // invalid pipe control parameters. 1 of available modes need to e set.
        //TODO: decide if is not better to have video mode by default, in case of not set flag
        return MAX_NO_OF_PREDEFINED_MODE;
        break;
    }
}
static void cbConfigIsp(uint32_t ispInstance, void *iconf) {
    UNUSED(ispInstance);
    icIspConfig* ispCfg = iconf;
    uint32_t mode = 0;
    if(ispCfg) {
        // other flgs will be ignored as are invalid in this pipeline
        uint32_t pipeControl = ispCfg->pipeControl &
                (IC_PIPECTL_MODE_STILL_STILL | IC_PIPECTL_MODE_VIDEO |
                        IC_PIPECTL_MODE_STILL_VIDEO | IC_PIPECTL_MODE_LOW_POWER);
        mode = getModeIdx(pipeControl);
        if (mode < MAX_NO_OF_PREDEFINED_MODE) {
            // in case of stilll configs, this will not be skipped and will be kept and procesed
            // one by one until will be finished, (as in burst mode scenario)
            if(plgSrcIsp.nxtIspCfg) {
                // next video cfg will be overwrite, Los will be inform to reuse prevision config structure
                ipServerIspReportError(ispInstance, IC_SEVERITY_NORMAL, IC_ERROR_RT_CFG_SKIPPED, (void*)plgSrcIsp.nxtIspCfg->userData);
            }
            ispCfg->updnCfg0  = upfirdnCfgY[mode];
            ispCfg->updnCfg12 = upfirdnCfgC[mode];
            plgSrcIsp.nxtIspCfg = ispCfg;

            }
        else {
            ipServerIspReportError(ispInstance, IC_SEVERITY_NORMAL, IC_ERROR_BAD_PARAMETER, (void*)ispCfg->userData);
        }
    }
    else {
        ipServerIspReportError(ispInstance, IC_SEVERITY_NORMAL, IC_ERROR_RT_CFG_MISSING, (void*)0);
    }

    //===============================================================
    //Patches that are NOT required for metal-fixed ma2150 silicon:
    if(plgSrcIsp.nxtIspCfg)
    plgSrcIsp.nxtIspCfg->dog.strength = 0; //LTM-Only for better performance on ma2150
    //===============================================================
}

static void startSourcesLocal(icSourceConfig  *sourceConfig, uint32_t sourceId) {
    icSize   iSize;
    iSize.w  = sourceConfig->cropWindow.x2 - sourceConfig->cropWindow.x1;
    iSize.h  = sourceConfig->cropWindow.y2 - sourceConfig->cropWindow.y1;

    if(ISP_RX_OUT_ID == sourceId) {
        icSize frameSizeOutWarp = {IMG_W_OUT_WARP, IMG_H_OUT_WARP};
        //new SrcIsp outputs YUV420 !
        FrmMgrUtilsInitList(frameMgrFrameIsp, iSize, FRAME_T_FORMAT_YUV420);
        FrmMgrUtilsInitList(frameMgrFrameWarp,  frameSizeOutWarp, FRAME_T_FORMAT_YUV420);

        plgWarp.plg.init (&frameMgrPoolWarp, 1, (void*)&plgWarp);
        plgSrcIsp.eofEvent = cbEofSourceEvent;
        plgSrcIsp.sofEvent = cbSofSourceEvent;
        plgSrcIsp.procesIspError = procesIspError;
        PlgSrcIspStart(&plgSrcIsp, sourceConfig);
    }

    if(RAW_OUT_ID == sourceId) {
        //new SrcIsp outputs YUV420 !
        FrmMgrUtilsInitList(frameMgrFrameRaw, iSize,
                FrmMgrUtilsGetRawFrm(sourceConfig->bitsPerPixel,
                        (sourceConfig->mipiRxData.recNrl <= IC_SIPP_DEVICE3 ? 1 : 0)));
        plgSource.eofEvent = cbEofSourceEvent;
        plgSource.sofEvent = cbSofSourceEvent;
        PlgSourceStart(&plgSource, sourceConfig, SIPP_FMT_16BIT);
    }
    ipServerSourceReady(sourceId);
}

static void appRxIspStartSrc(uint32_t sourceInstance, icSourceConfig  *sourceConfig) {
    startSrcState[sourceInstance] = 1;
    startSrcLocConfig[sourceInstance] = sourceConfig;
}
static void appRxIspStopSrc(uint32_t sourceInstance) {
    stopSrcState[sourceInstance] = 1;
}

static void userEvendCb(void *eventStruct, uint32_t id)
{
    UNUSED(eventStruct);
    UNUSED(id);
    /// Custom user event
}
