
/*

RTEMS 系统封装，参考POSIX协议
todo 其他封装
ADDBY XHWANG 2016-5-31
*/
#include "os_adapta.h"

/* 创建MQ的flag   */ 
#define MQ_FLAG (O_RDWR | O_CREAT | O_EXCL) 

/* 设定创建MQ的权限  */
#define FILE_MODE (S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH)

/*****************************************/
/*
   thread      :创建线程返回的PID
   attr_arg    :创建线程的属性(默认使用NULL :继承父进程的属性)
   task_entry  :创建的线程函数 
   task_arg    :创建的线程函数的入参
   priority    :创建线程的优先级

	返回值: 0  : 创建成功

	example    : 创建优先级为10的线程，rtos_task_creat(&pid, NULL, task_entry, task_arg, 10)
*/
int rtos_task_creat(pthread_t *thread,  pthread_attr_t *attr_arg, void *(*task_entry)(void*), void *task_arg,  int priority)
{
	pthread_attr_t attr;
	struct sched_param tPriority;
	pthread_t t_thread, *p_thread;

	if(thread)
	{
		p_thread = thread;
	}
	else
	{
		p_thread = &t_thread;
	}
	
	if(pthread_attr_init(&attr) !=0)
	{
		printf("pthread_attr_init error\n");
	}
	
	if(pthread_attr_setinheritsched(&attr, PTHREAD_EXPLICIT_SCHED) != 0) 
	{
		printf("pthread_attr_setinheritsched error\n");
	}
	
	if(pthread_attr_setschedpolicy(&attr, SCHED_RR) != 0) {
		printf("pthread_attr_setschedpolicy error\n");
	}

	if(priority > 0) 
	{
		tPriority.sched_priority = priority;
		if(pthread_attr_setschedparam(&attr, &tPriority) != 0) 
		{
			printf("pthread_attr_setschedparam error\n");
		}
	}
	
	if (pthread_create( p_thread, &attr, task_entry,task_arg) != 0) 
	{
		printf("Write thread creation failed:\n");
	}
}

/*****************************************/
/*
   key         :创建消息队列的关键字
   msg_num     :创建消息队列的个数
   msg_size    :创建消息队列的传输长度 
  
	返回值: -1  : 创建失败
	其他值:       创建的消息队列值

	example    : 创建消息队列的，个数为100,每个消息队列最大长度为4, rtos_msgq_create("/temp", 100, 4);
*/
int rtos_msgq_create (void* key, int msg_num, int msg_size)
{
	struct mq_attr mqattr; 	
	mqd_t posixmq; 

	memset(&mqattr, 0, sizeof(mqattr));
	
	mqattr.mq_maxmsg  = msg_num; 
	mqattr.mq_msgsize = msg_size; 
	
	posixmq = mq_open(key, MQ_FLAG, FILE_MODE, &mqattr); 
	
	if(-1 == posixmq)  
	{  
		 printf("创建MQ失败 %s\n",key);  
		 return -1;  
	} 

	return posixmq;
}

/* 消息队列打开 */
int rtos_msgq_open (void* key)
{
	mqd_t posixmq; 

	posixmq = mq_open(key, O_WRONLY);

	return posixmq;
}

/* 返回实际接收长度 */
int rtos_msgq_recv (int q_id,void *msg,int len) 
{
	int rcv_len;
	struct mq_attr mqattr; 
	unsigned int prio; 

	/* 获取消息队列的属性  */  
   
     if(-1 == mq_getattr((mqd_t)q_id, &mqattr))  
	{  
		printf("获取消息队列属性失败, QID:%d\n", q_id);  
		return -1; 
	}  
	
	/*调用接收消息队列函数*/
	rcv_len =mq_receive((mqd_t)q_id, msg, mqattr.mq_msgsize, &prio);

	return rcv_len;
}

int rtos_msgq_send(int q_id,void *msg,int len, int pri) 
{
	struct mq_attr mqattr; 
	
	/* 获取消息队列的属性  */  

	if(-1 == mq_getattr((mqd_t)q_id, &mqattr))  
	{  
		printf("获取消息队列属性失败, QID:%d\n", q_id);
		return -1; 
	}  
  
	if(mq_send((mqd_t)q_id, msg, len>mqattr.mq_msgsize?mqattr.mq_msgsize:len, pri) < 0)  
	{  
		perror("写入消息队列失败");  
		return -1;  
	}   	
	 
	 return 0;
}

extern mqd_t posixmq;

void myprintf(char* p_fmt, ...)
{
	int              len=0;
	unsigned char    str[200];

	va_list     vArgs;

    va_start(vArgs, p_fmt);

    memset(str, 0, sizeof(str));

   	len =  vsprintf(str, (char const *)p_fmt, vArgs);

    va_end(vArgs);

	printf("%s",str);
	return;
	
	rtos_msgq_send(posixmq,str,len, 1);

}
