#include "stdio.h"
#include "string.h"
#include "DeviceInfo.h"

int GetDeviceInfo(DeviceInfo_t *deviceinfo)
{
	if(deviceinfo)
	{
		deviceinfo->DeviceVersion = 0;
		deviceinfo->DeviceType = 1;
		deviceinfo->DepthFrameWidth = 2688;
		deviceinfo->DepthFrameHeight = 1520;
		deviceinfo->BitsPerPoint = 8;
		deviceinfo->VisibleFrameWidth = 0;
		deviceinfo->VisibleFrameHeight = 0;
		deviceinfo->BitsPerPixel = 12;
		deviceinfo->BlockSizeIn = 1024;
		deviceinfo->BlockSizeOut = 1024;

		deviceinfo->querygainctrla.maximum	=	PU_CONTRAST_CONTROL_MAX;
		deviceinfo->querygainctrla.minimum	=	PU_CONTRAST_CONTROL_MIN;
		deviceinfo->querygainctrla.step		=	PU_CONTRAST_CONTROL_RES;
		deviceinfo->querygainctrla.default_value=	PU_CONTRAST_CONTROL_DEF;

		deviceinfo->queryexptimectrla.maximum	=	PU_BRIGHTNESS_CONTROL_MAX;
		deviceinfo->queryexptimectrla.minimum	=	PU_BRIGHTNESS_CONTROL_MIN;
		deviceinfo->queryexptimectrla.step	=	PU_BRIGHTNESS_CONTROL_RES;
		deviceinfo->queryexptimectrla.default_value=	PU_BRIGHTNESS_CONTROL_DEF;

		deviceinfo->querygainctrlb.maximum	=	PU_HUE_CONTROL_MAX;
		deviceinfo->querygainctrlb.minimum	=	PU_HUE_CONTROL_MIN;
		deviceinfo->querygainctrlb.step		=	PU_HUE_CONTROL_RES;
		deviceinfo->querygainctrlb.default_value=	PU_HUE_CONTROL_DEF;

		deviceinfo->queryexptimectrlb.maximum	=	PU_SATURATION_CONTROL_MAX;
		deviceinfo->queryexptimectrlb.minimum	=	PU_SATURATION_CONTROL_MIN;
		deviceinfo->queryexptimectrlb.step	=	PU_SATURATION_CONTROL_RES;
		deviceinfo->queryexptimectrlb.default_value=	PU_SATURATION_CONTROL_DEF;

		return 0;
	}
	return -1;
}
