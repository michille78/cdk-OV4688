///
/// @file
/// @copyright All code copyright Movidius Ltd 2014, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Test app main()
///

#include <stdlib.h>
#include <time.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>
#include <signal.h>
#include <semaphore.h>
#include <assert.h>

#include <ipipe.h>

#include <utils/mms_debug.h>
#include <utils/profile/profile.h>
#include <osal/osal_stdlib.h>
#include <osal/osal_mutex.h>
#include <osal/osal_time.h>
#include <osal/osal_string.h>

#include <registersMyriad.h>
#include <DrvRegUtils.h>
#include <DrvMipi.h>
#include <DrvMss.h>
#include <DrvTimer.h>
#include "IspCommon.h"

//
#include "usbpumpdebug.h"
#include "usbpump_application_rtems_api.h"

#include "vsc2app_outcall.h"
#include "usbpump_vsc2app.h"


#include "camera_control.h"
#include "DrvGpio.h"

#include "metadata.h"       //add by wzz
#include "user_config.h"    //add by wzz
#include "vsc2app_if.h"    //ADD FOR VSC
#include <rtems.h>   	//add by xw
#include <DrvTimer.h> //add by xw

enum{
	USB_OUTPUT_DEPTH=1,
	USB_OUTPUT_PREVIEW=2,
};

u8  UsbOutputType = USB_OUTPUT_PREVIEW;//0;

extern volatile int   __attribute__((section(".ddr_direct.data"))) capModPth;
extern volatile int   __attribute__((section(".ddr_direct.data"))) g_heartVal;
extern volatile char   __attribute__((section(".ddr_direct.data"))) g_bEnableHeartCheck;
extern volatile CamConfig __attribute__((section(".ddr_direct.data"))) camCfg;
extern volatile char  __attribute__((section(".ddr_direct.data"))) g_camCfg_para_cnt;
extern volatile char  __attribute__((section(".ddr_direct.data"))) g_camCfgOK;
extern volatile AwbGainsConfig __attribute__((section(".ddr_direct.data"))) awbGainsCfg;
extern volatile char  __attribute__((section(".ddr_direct.data"))) g_awbgains_para_cnt;

#include "spi.h" //add by zp
extern struct tFlashParams flash_params;
#define MULTISPECKLE 1

mmsdbg_define_variable(
        vdl_ic,
        DL_DEFAULT,
        0,
        "vdl_ic",
        "Guzzi IC."
    );
#define MMSDEBUGLEVEL mmsdbg_use_variable(vdl_ic)

// scenarios
#define ALL3SENSOR_2MP_VIDEO_PLUS_STILL 0
#define IMX214_FULL_SIZE_VIDEO_PLUS_STILL 1
#define IMX214_QUARTER_SIZE_VIDEO_PLUS_STILL 2


volatile char Speckle_Brightness=0xf;
volatile char Texture_Brightness=0xf;

char  *pUsbStream = NULL;
char  *pUsbStreamA = NULL;
char  *pUsbStreamB = NULL;
char  *pUsbStreamC = NULL;
char  *pUsbStreamD = NULL;
u32  frameCountA = 0;
u32  frameCountB = 0 ;
u32  frameCountC = 0 ;
volatile u32  los_frameC=0;
volatile u32  frame_cnt=0;
volatile u32  flag_read_calib=0;

int app_ready_flag=0;
int start_cap_flg = 0;
int timer_cnt = 0;
int flg_cap_loop = 0;

void write_1byte(u64 spi_data);
void setSpeckleLight(int speckle_light_ctl);

S_MetaData   MetaData_CamA = {
		0, 0, 0,               					    /*version  reserve byte*/
		CAMERA_A ,                          					/*camera number*/
		IC_IMAGE_WIDTH,            /*image width   2688*/
		IC_IMAGE_HEIGHT,           /*image height  1520*/
		YUV_8,                                   /*bpp  bits per pixel*/
		YUV420_I420,                      /*data format*/
		0,                                             /*frameCount  need update*/
		0,                                             /*getFrameTime   need update*/
		0,                                             /*sendFrameTime  need update*/
		META_DATA_SIZE,              /*usb transport head length*/
        0,  //lastCapAFramesError
        0,  //lastCapBFramesError
};

S_MetaData   MetaData_CamB = {
		0, 0, 0,               					    /*version  reserve byte*/
		CAMERA_B,                         					/*camera number*/
		IC_IMAGE_WIDTH,            /*image width   2688*/
		IC_IMAGE_HEIGHT,           /*image height  1520*/
		YUV_8,                                   /*bpp  bits per pixel*/
		YUV420_I420,                      /*data format*/
		0,                                             /*frameCount  need update*/
		0,                                             /*getFrameTime   need update*/
		0,                                             /*sendFrameTime  need update*/
		META_DATA_SIZE,               /*usb transport head length*/
        0,  //lastCapAFramesError
        0,  //lastCapBFramesError
};

S_MetaData   MetaData_CamC = {
		0, 0, 0,               					    /*version  reserve byte*/
		CAMERB_A,                         					/*camera number*/
		IC_IMAGE_WIDTH,            /*image width   2688*/
		IC_IMAGE_HEIGHT,           /*image height  1520*/
		YUV_8,                                   /*bpp  bits per pixel*/
		YUV420_I420,                      /*data format*/
		0,                                             /*frameCount  need update*/
		0,                                             /*getFrameTime   need update*/
		0,                                             /*sendFrameTime  need update*/
		META_DATA_SIZE,               /*usb transport head length*/
        0,  //lastCapAFramesError
        0,  //lastCapBFramesError
};


void cam_led1_toggle (void);
static void SynTriggerThrd(void);
static void TimerCntThrd(void);

void los_ConfigureSource(
        int srcIdx,
        icSourceConfig *sconf,
        int pipeId
    );
void los_SetupSource(
        int srcIdx,
        icSourceSetup *sconf
    );
void los_SetupSourceCommit(void);
void inc_cam_stats_ready(
        void *p_prv,
        unsigned int seqNo,
        void *p_cfg_prv
    );
void inc_cam_capture_ready(
        void *p_prv,
        unsigned int seqNo,
        void *p_cfg_prv
    );
void inc_cam_ipipe_cfg_ready(
        void *p_prv,
        unsigned int seqNo,
        void *p_cfg_prv
    );
void inc_cam_ipipe_buff_locked(
        void *p_prv,
        void *userData,
        unsigned int sourceInstance,
        void *buffZsl,
        icTimestamp ts,
        unsigned int seqNo
    );
void inc_cam_frame_start(

        void *p_prv,
        uint32_t sourceInstance,
        uint32_t seqNo,
        icTimestamp ts,
        void *userData
    );
void inc_cam_frame_line_reached(
        void *p_prv,
        uint32_t sourceInstance,
        uint32_t seqNo,
        icTimestamp ts,
        void *userData
    );
void inc_cam_frame_end(
        void *p_prv,
        uint32_t sourceInstance,
        uint32_t seqNo,
        icTimestamp ts,
        void *userData
    );

void los_dataWasSent(FrameT *dataBufStruct, uint32_t outputId, uint32_t frmType);

void inc_cam_terminate_fr(
        void *p_prv,
        uint32_t sourceInstance,
        void *userData
    );

typedef struct {
    uint32_t    ref_cnt;
    osal_mutex *lock;
    icCtrl     *ctrl;
} g_ipipe_ctx_t;

static g_ipipe_ctx_t gctx;

rtems_id  q_capseq_msg = -1;

sem_t semWaitForLrtReady;
sem_t semWaitForSourceCommit;
sem_t semWaitForSourceReady;
sem_t semWaitForSourceStoped;

static pthread_t eventThread;
static pthread_t SynTriggerThread;
static pthread_t TimerCntThread;


/************* add by xw ********************/
extern volatile int lrt_g_flgCaptureSpec;
extern volatile int lrt_g_flgCaptureTex;
extern volatile int lrt_g_capMod;

extern volatile CaptureT lrt_capFrame;
#define IOFLASHLIGHT          54
#define IOFLOODLIGHT          55

#define SPI0_MOSI          28            //add    spi data out
#define SPI0_SCLK_OUT      55            //add    spi sclk out
#define SPI0_SS_1          59            //add    spi cs out
icSourceSetup srcSet = {
        IC_IMAGE_WIDTH, IC_IMAGE_HEIGHT, 10, (IC_IMAGE_WIDTH * IC_IMAGE_HEIGHT), 1, 1, 1, 1, 0
};

#define INSTANCES   INSTANCES_COUNT_MAX

#define SRC_BUFS  3//  4
#define ISP_BUFS    4//5
#define STILL_BUFS  1

/*WZZ 20160311 add meteDataSize*/
#define DDR_BUFFER_ALOCATED_MEM_SIZE \
( \
      INSTANCES \
    * (  ((5 * USB_IMAGE_WIDTH*USB_IMAGE_HEIGHT / 4) * SRC_BUFS) \
       + ((3 * USB_IMAGE_WIDTH*USB_IMAGE_HEIGHT / 2) * ISP_BUFS) \
       + ((3 * USB_IMAGE_WIDTH*USB_IMAGE_HEIGHT / 2) * STILL_BUFS) \
       +1024 \
      ) \
)


static uint8_t ddrStaticAlocatedMemory[DDR_BUFFER_ALOCATED_MEM_SIZE] __attribute__((
        section(".ddr.bss")
    ));


void ddrStaticAlocatedMemoryInit()
{
		memset(ddrStaticAlocatedMemory, 0x80, DDR_BUFFER_ALOCATED_MEM_SIZE);
}


/*
 * ****************************************************************************
 * ** Misc ********************************************************************
 * ****************************************************************************
 */

//丢帧掩码
static int gSkipMark[6]={1,1,1,1,1,1};

//cap(tex + spec) frames mask
static unsigned char gCapFramesCnt = 0; //counter
static unsigned char gCapAFramesError = 0;
static unsigned char gCapBFramesError = 0;
 	 	 
//丢帧处理
void SetFrameSkipFlag(int fps)
{
    printf("SetFrameSkipFlag %d\n",fps);
 	 	 
    /*1 1 1 1 1 1 */
    if(fps == 30)
    {
        gSkipMark[0] = 1;
        gSkipMark[1] = 1;
        gSkipMark[2] = 1;
        gSkipMark[3] = 1;
        gSkipMark[4] = 1;
        gSkipMark[5] = 1;
    }
    /*1 1 1 1 1 0 */
    else if(fps>=25)
    {
        gSkipMark[0] = 1;
        gSkipMark[1] = 1;
        gSkipMark[2] = 1;
        gSkipMark[3] = 1;
        gSkipMark[4] = 1;
        gSkipMark[5] = 0;
    }
    /*1 1 0 1 1 0 */
    else if(fps>=20)
    {
        gSkipMark[0] = 1;
        gSkipMark[1] = 1;
        gSkipMark[2] = 0;
        gSkipMark[3] = 1;
        gSkipMark[4] = 1;
        gSkipMark[5] = 0;
    }
    /*1 0 1 0 1 0 */
    else if(fps>=15)
    {
        gSkipMark[0] = 1;
        gSkipMark[1] = 0;
        gSkipMark[2] = 1;
        gSkipMark[3] = 0;
        gSkipMark[4] = 1;
        gSkipMark[5] = 0;
    }
    /*1 0 0 1 0 0 */
    else if(fps>=10)
    {
        gSkipMark[0] = 1;
        gSkipMark[1] = 0;
        gSkipMark[2] = 0;
        gSkipMark[3] = 1;
        gSkipMark[4] = 0;
        gSkipMark[5] = 0;
    }
    /*1 0 0 0 0 0 */
    else if(fps>=5)
    {
        gSkipMark[0] = 1;
        gSkipMark[1] = 0;
        gSkipMark[2] = 0;
        gSkipMark[3] = 0;
        gSkipMark[4] = 0;
        gSkipMark[5] = 0;
    }
    /*0 0 0 0 0 0 */
    else
    {
        gSkipMark[0] = 0;
        gSkipMark[1] = 0;
        gSkipMark[2] = 0;
        gSkipMark[3] = 0;
        gSkipMark[4] = 0;
        gSkipMark[5] = 0;
    }
}
 	 	 
 	 	 
/*
 * ****************************************************************************
 * ** Should run at a relatively high priority so that the event queue doesn't
 * ** fill up (if it did, we would drop events).
 * ****************************************************************************
 */
static uint32_t SendOutEventCnt ;
static void * eventLoop(void *vCtrl)
{
    g_ipipe_ctx_t *ctx;
    icCtrl *ctrl;
    icEvent ev;
    unsigned int evno;
    int i, flg_snd_success = 0;
	int ret = -1;

    ctx = vCtrl;
    ctrl = ctx->ctrl;
	
	//默认处理,发送30FPS
//     SetFrameSkipFlag(30);

	//    //丢帧测试,发送10FPS
	SetFrameSkipFlag(15);

    /*
     * This thread runs until it is cancelled (pthread_cond_wait() is a
     * cancellation point).
     */
    for (;;) {
        if (!icGetEvent(ctrl, &ev)) {
            evno = ev.ctrl & IC_EVENT_CTRL_TYPE_MASK;

            switch (evno) {
            case IC_EVENT_TYPE_LEON_RT_READY:
                /* TODO: PROFILE_ADD */
                assert(sem_post(&semWaitForLrtReady) != -1);
                break;
            case IC_EVENT_TYPE_SETUP_SOURCES_RESULT:
                /* TODO: PROFILE_ADD */
                assert(sem_post(&semWaitForSourceCommit) != -1);
                break;
            case IC_EVENT_TYPE_SOURCE_READY:
                /* TODO: PROFILE_ADD */
                assert(sem_post(&semWaitForSourceReady) != -1);
                break;
            case IC_EVENT_TYPE_READOUT_START:
                if (ev.u.lineEvent.userData != NULL) {
                    PROFILE_ADD(
                            PROFILE_ID_LRT_START_FRAME,
                            ev.u.lineEvent.userData,
                            ev.u.lineEvent.sourceInstance
                        );
                    inc_cam_frame_start(
                            NULL,
                            ev.u.lineEvent.sourceInstance,
                            ev.u.lineEvent.seqNo,
                            ev.u.lineEvent.ts,
                            ev.u.lineEvent.userData
                        );
                    inc_cam_ipipe_cfg_ready(
                            NULL,
                            ev.u.ispEvent.seqNo,
                            ev.u.ispEvent.userData
                        );
                }
                break;
            case IC_EVENT_TYPE_LINE_REACHED:
                if (ev.u.lineEvent.userData != NULL) {
                    PROFILE_ADD(
                            PROFILE_ID_LRT_LINE_REACHED,
                            ev.u.lineEvent.userData,
                            ev.u.lineEvent.sourceInstance
                        );
                    #if 0 /* TODO: IPIPE2 not implelmented */
                    inc_cam_frame_line_reached(
                            NULL,
                            ev.u.lineEvent.sourceInstance,
                            ev.u.lineEvent.seqNo,
                            ev.u.lineEvent.ts,
                            ev.u.lineEvent.userData
                        );
                    #endif
                }
                break;
            case IC_EVENT_TYPE_READOUT_END:
                if (ev.u.lineEvent.userData != NULL) {
                    PROFILE_ADD(
                            PROFILE_ID_LRT_END_FRAME,
                            ev.u.lineEvent.userData,
                            ev.u.lineEvent.sourceInstance
                        );
                    inc_cam_frame_end(
                            NULL,
                            ev.u.lineEvent.sourceInstance,
                            ev.u.lineEvent.seqNo,
                            ev.u.lineEvent.ts,
                            ev.u.lineEvent.userData
                        );
                }
                break;
            case IC_EVENT_TYPE_ISP_START:
                if (ev.u.ispEvent.userData != NULL) {
                    PROFILE_ADD(
                            PROFILE_ID_LRT_ISP_START,
                            ev.u.ispEvent.userData,
                            ev.u.ispEvent.ispInstance
                        );
                    #if 0 /* TODO: IPIPE2 not implelmented */
                    inc_cam_ipipe_cfg_ready(
                            NULL,
                            ev.u.ispEvent.seqNo,
                            ev.u.ispEvent.userData
                        );
                    #endif
                } else {
                    mmsdbg(
                            DL_ERROR,
                            "IC_EVENT_TYPE_ISP_START with userdata == NULL"
                        );
                }
                break;
            case IC_EVENT_TYPE_ISP_END:
                if (ev.u.ispEvent.userData != NULL) {
                    PROFILE_ADD(
                            PROFILE_ID_LRT_ISP_END,
                            ev.u.ispEvent.userData,
                            ev.u.ispEvent.ispInstance
                        );
                    inc_cam_stats_ready(
                            NULL,
                            ev.u.ispEvent.seqNo,
                            ev.u.ispEvent.userData
                        );
                    #if 0 /* TODO: IPIPE2 not implelmented */
                    inc_cam_capture_ready(
                            NULL,
                            ev.u.ispEvent.seqNo,
                            ev.u.ispEvent.userData
                        );
                    #endif
                } else {
                    mmsdbg(
                            DL_ERROR,
                            "IC_EVENT_TYPE_ISP_END with userdata == NULL"
                        );
                }
                break;
            case IC_EVENT_TYPE_ZSL_LOCKED:
                PROFILE_ADD(
                        PROFILE_ID_LRT_ZSL_LOCKED,
                        ev.u.buffLockedZSL.userData,
                        ev.u.buffLockedZSL.sourceInstance
                    );
                inc_cam_ipipe_buff_locked(
                        NULL,
                        ev.u.buffLockedZSL.userData,
                        ev.u.buffLockedZSL.sourceInstance,
                        ev.u.buffLockedZSL.buffZsl,
                        ev.u.buffLockedZSL.buffZsl->timestamp[0], // time stamp when zsl was arrived in memory
                        //ev.u.buffLockedZSL.buffZsl->timestamp[ev.u.buffLockedZSL.buffZsl->timestampNr-1] // time stamp when frame was processed
                        ev.u.buffLockedZSL.buffZsl->seqNo
                    );
                break;
            case IC_EVENT_TYPE_SOURCE_STOPPED:
                /* TODO: PROFILE_ADD */
                assert(sem_post(&semWaitForSourceStoped) != -1);
                break;
            case IC_EVENT_TYPE_SEND_OUTPUT_DATA: {
                SendOutEventCnt++;
                FrameT *frame;
                uint32_t outId;
                uint32_t frmType;
                uint32_t frmFmt;
				uint32_t sendFlg = 0;
				
				int usb_vsc_sendA(FrameT *frame);
				int usb_vsc_sendA_capture(FrameT *frame);
				int usb_vsc_sendB(FrameT *frame);
				int usb_vsc_sendB_capture(FrameT *frame);


                frame   = ev.u.sentOutput.dataBufStruct;
                // guzzi create a dependency between source id and output id, so here will be the source id
                // depend on
                outId  = ctrl->icPipelineDescription.icQueryOutput[ev.u.sentOutput.outputId]->dependentSources;
                frmType = ctrl->icPipelineDescription.icQueryOutput[ev.u.sentOutput.outputId]->attrs;; //type is still, video ...
                    frmFmt  = ev.u.sentOutput.dataBufStruct->type;

                PROFILE_ADD(
                        PROFILE_ID_LRT_SEND_DATA,
                        frame->appSpecificData,
                        outId
                    );

                assert(outId<INSTANCES_COUNT_MAX);
                assert(frmType<FRAME_DATA_TYPE_MAX);
                assert(frmFmt<FRAME_T_FORMAT_MAX);

                /*copy to usbstreambuf */
                switch(outId)
                {
					case CAMERA_A:
						frameCountA ++;
						
						//获取丢帧掩码
                        sendFlg = gSkipMark[frameCountA
                                % (sizeof(gSkipMark) / sizeof(gSkipMark[0]))];
						
						MetaData_CamA.frameCount =  frameCountA;
                        MetaData_CamA.version[0]=0x00050300;
                        MetaData_CamA.version[1]=0x0133C6A1;
                        MetaData_CamA.version[2]=0x00000000;
						MetaData_CamA.getFrameTime = frame->flgCapNo;
						MetaData_CamA.sendFrameTime =  frame->timestamp[0];
						memcpy(frame->fbPtr[0]+PAYLOAD_HEADER_OFFSET, &MetaData_CamA, sizeof(MetaData_CamA));
						pUsbStreamA = frame->fbPtr[0];//UsbStreambuf
						ret = -1; //Initialize
						//application is ready to send a frame
                        if((app_ready_flag==1) && (sendFlg==1) && (start_cap_flg!=1)
                            && (frame->flgCapNo != CAPSPECMAGICNUM) && (frame->flgCapNo != CAPTEXMAGICNUM)){
            				ret = usb_vsc_sendA(frame);            					
                        }

                        //
                        if((app_ready_flag==1) && ((frame->flgCapNo== CAPSPECMAGICNUM)||(frame->flgCapNo== CAPTEXMAGICNUM)))
                        {
                           ret = usb_vsc_sendA_capture(frame);
						}

						if(RTEMS_SUCCESSFUL == ret){
							frame_cnt++;
						}else{
							los_frameC++;
							los_dataWasSent(frame,outId,frmType);
						}
				break;

				case CAMERA_B:
						frameCountB ++;
						
						//获取丢帧掩码
                        sendFlg = gSkipMark[frameCountB % (sizeof(gSkipMark) / sizeof(gSkipMark[0]))];
						
						MetaData_CamB.frameCount =  frameCountB;
                        MetaData_CamB.version[0]=0x00050300;
                        MetaData_CamB.version[1]=0x0133C6A1;
                        MetaData_CamB.version[2]=0x00000000;
						MetaData_CamB.getFrameTime = frame->flgCapNo;
						MetaData_CamB.sendFrameTime =  frame->timestamp[0];
						memcpy(frame->fbPtr[0]+PAYLOAD_HEADER_OFFSET, &MetaData_CamB, sizeof(MetaData_CamB));
						pUsbStreamB = frame->fbPtr[0];//UsbStreambuf
						ret = -1; //Initialize
						//application is ready to send a frame
                        if((app_ready_flag==1) && (sendFlg==1) && (start_cap_flg!=1)
                                && (frame->flgCapNo != CAPSPECMAGICNUM) && (frame->flgCapNo != CAPTEXMAGICNUM))
                        {
                        	ret = usb_vsc_sendB(frame);   
                        }

                        if((app_ready_flag==1) && ((frame->flgCapNo== CAPSPECMAGICNUM)||(frame->flgCapNo== CAPTEXMAGICNUM)))
                        {
                           ret = usb_vsc_sendB_capture(frame);
						}

						if(RTEMS_SUCCESSFUL == ret){
							frame_cnt++;
						}else{
							los_frameC++;
							los_dataWasSent(frame,outId,frmType);
						}

				break;

				case CAMERB_A:
						frameCountC++;
						
						//获取丢帧掩码
                        sendFlg = gSkipMark[frameCountC % (sizeof(gSkipMark) / sizeof(gSkipMark[0]))];
						
						MetaData_CamC.frameCount =  frameCountC;
						memcpy(frame->fbPtr[0]+PAYLOAD_HEADER_OFFSET, &MetaData_CamC, sizeof(MetaData_CamC));
						pUsbStreamC = frame->fbPtr[0];//UsbStreambuf
				break;

				default:
                    return NULL;
                }

#if 1
                if(RTEMS_SUCCESSFUL == ret)
                {
                    if(frame->flgCapNo == CAPSPECMAGICNUM)
                        printf("Cap Speckle frame(%d)	seqNO:%d, TmStamp:%u sending...\n", outId,	frame->seqNo, frame->timestamp[0]);

                    if(frame->flgCapNo == CAPTEXMAGICNUM)
                        printf("Cap Texture frame(%d)	seqNO:%d, TmStamp:%u sending...\n", outId,	frame->seqNo, frame->timestamp[0]);
                }
                else
                {
                    if((frame->flgCapNo == CAPTEXMAGICNUM)||(frame->flgCapNo == CAPSPECMAGICNUM))
                    {
                        printf("--LOST Frame  ID(%d)(0x%x) seqNO:%d !!!!!\n", outId, frame->flgCapNo, frame->seqNo);
//                        printf(">>Resend cnt(%d) frame ID: (0x%x)(%d) seqNO-%d !!\n", i, frame->flgCapNo, outId, frame->seqNo);
                    }
                }
#endif
            }
                break;
            case IC_EVENT_TYPE_ERROR:
                PROFILE_ADD(
                        PROFILE_ID_LRT_ERROR,
                        ev.u.error.userData,
                        ev.u.error.sourceInstance
                    );
                if ((ev.u.error.userData) && (ev.u.error.errorNo == IC_STATS_OUT_OF_MEM)) {
                    inc_cam_terminate_fr(
                            NULL,
                            ev.u.error.sourceInstance,
                            ev.u.error.userData
                        );
                }
                break;
            case IC_EVENT_TYPE_TORN_DOWN:
                /* TODO: PROFILE_ADD */
                return NULL;
            default:
                mmsdbg(DL_ERROR, "Unknown evt: %d", evno);
            }
        } else {
            /*
             * no activity in last period, lrt crash
             * or lrt is started, but no camera connected
             * in this case cut down lrt
             */
            mmsdbg(DL_ERROR, "Error X");
        }
    }

    return NULL;
}

void los_start(void *arg)
{
    int policy;
    struct sched_param param;
    UNUSED(arg);

    PROFILE_ADD(PROFILE_ID_LOS_START, 0, 0);

    gctx.ref_cnt++;
    if (gctx.ref_cnt == 1) {
        gctx.lock = osal_mutex_create();

        assert(sem_init(&semWaitForLrtReady, 0, 0) != -1);
        assert(sem_init(&semWaitForSourceCommit, 0, 0) != -1);
        assert(sem_init(&semWaitForSourceReady, 0, 0) != -1);
        assert(sem_init(&semWaitForSourceStoped, 0, 0) != -1);
        gctx.ctrl = icSetup(
                1,
                (uint32_t)ddrStaticAlocatedMemory,
                sizeof (ddrStaticAlocatedMemory)
            );

        assert(pthread_create(&eventThread, NULL, eventLoop, &gctx) == 0);

        pthread_getschedparam(eventThread, &policy, &param);
        param.sched_priority = 210;
        pthread_setschedparam(eventThread, policy, &param);

        assert(sem_wait(&semWaitForLrtReady) != -1);

//        sendOutInit();
		
	//ͬ����������
	assert(pthread_create(&SynTriggerThread, NULL, SynTriggerThrd, NULL) == 0);
    assert(pthread_create(&TimerCntThread, NULL, TimerCntThrd, NULL) == 0);
		
        los_SetupSource(0, &srcSet);
        los_SetupSource(1, &srcSet);
//        los_SetupSource(2, &srcSet);

        los_SetupSourceCommit();
#if 0//USE_USB_TX
        sendOutInit();    //add by wzz
#endif
    }

}

void los_stop(void)
{
    PROFILE_ADD(PROFILE_ID_LOS_STOP, 0, 0);

    assert(gctx.ref_cnt);

    gctx.ref_cnt--;
    if (!gctx.ref_cnt) {
        osal_usleep(33*1000);  // Wait for all in process frames to finish
        icTeardown(gctx.ctrl);
        pthread_join(eventThread, NULL);

//        sendOutFini();
        osal_mutex_destroy(gctx.lock);
        sem_destroy(&semWaitForSourceStoped);
        sem_destroy(&semWaitForSourceReady);
        sem_destroy(&semWaitForSourceCommit);
        sem_destroy(&semWaitForLrtReady);
    }
}

void los_ConfigureGlobal(void *gconf)
{
    PROFILE_ADD(PROFILE_ID_LOS_GOB_CONFIG, gconf, 0);
}

void los_ConfigureSource(int srcIdx, icSourceConfig *sconf, int pipeId)
{
    UNUSED(pipeId);
//    osal_mutex_lock(gctx.lock);

    //Init source in RT
    PROFILE_ADD(PROFILE_ID_LOS_SRC_CONFIG, sconf, srcIdx);
    icConfigureSource(gctx.ctrl, (icSourceInstance)srcIdx, sconf);
//    osal_mutex_unlock(gctx.lock);
}
void los_SetupSource(int srcIdx, icSourceSetup *sconf)
{
//    osal_mutex_lock(gctx.lock);
    PROFILE_ADD(PROFILE_ID_LOS_SRC_CONFIG, sconf, srcIdx);
    icSetupSource (gctx.ctrl, (icSourceInstance)srcIdx, sconf);
//    osal_mutex_unlock(gctx.lock);
}
void los_SetupSourceCommit(void)
{
    osal_mutex_lock(gctx.lock);
    /* TODO: PROFILE_ADD */
    icSetupSourcesCommit(gctx.ctrl);
    assert(sem_wait(&semWaitForSourceCommit) != -1);
    osal_mutex_unlock(gctx.lock);
}

void los_ipipe_LockZSL(uint32_t srcIdx) {
    PROFILE_ADD(PROFILE_ID_LOS_LOCK_ZSL, 0, srcIdx);
    icLockZSL (gctx.ctrl, srcIdx, 0, IC_LOCKZSL_TS_RELATIVE);
}

void los_ipipe_TriggerCapture(void* buff, void *iconf, uint32_t srcIdx) {
    PROFILE_ADD(PROFILE_ID_LOS_TRIGGER_CAPTURE, buff, srcIdx);
    icTriggerCapture (gctx.ctrl, srcIdx, buff, iconf, 0);
}

void los_configIsp(void *iconf, int ispIdx)
{
//    osal_mutex_lock(gctx.lock);
    PROFILE_ADD(PROFILE_ID_LOS_CONFIG_ISP, ((icIspConfig *)iconf)->userData, ispIdx);
  //  printf("Cfg: %p, %d \n", ((icIspConfig *)iconf)->userData, ispIdx);
    // TODO: HACK- until resize capability will be added on los side
    icIspConfig *ispCfg = (icIspConfig*)iconf;
    ispCfg->updnCfg0.vN   = 1;
    ispCfg->updnCfg0.vD   = 1;
    ispCfg->updnCfg0.hN   = 1;
    ispCfg->updnCfg0.hD   = 1;
    icConfigureIsp(gctx.ctrl, ispIdx, iconf);
//    osal_mutex_unlock(gctx.lock);
}

int los_startSource(int srcIdx)
{

    osal_mutex_lock(gctx.lock);
    PROFILE_ADD(PROFILE_ID_LOS_SRC_SART, 0, srcIdx);
    icStartSource(gctx.ctrl, (icSourceInstance)srcIdx);
    assert(sem_wait(&semWaitForSourceReady) != -1);
    osal_mutex_unlock(gctx.lock);

    return 0;
}

int los_stopSource(int srcIdx)
{
    osal_mutex_lock(gctx.lock);
    PROFILE_ADD(PROFILE_ID_LOS_SRC_STOP, 0, srcIdx);
    icStopSource(gctx.ctrl, (icSourceInstance)srcIdx);
    assert(sem_wait(&semWaitForSourceStoped) != -1);
    osal_mutex_unlock(gctx.lock);

    return 0;
}

void los_dataWasSent(FrameT *dataBufStruct, uint32_t outputId, uint32_t frmType)
{
    UNUSED(outputId);
    UNUSED(frmType);

    PROFILE_ADD(
            PROFILE_ID_LRT_DATA_SENT,
            ((FrameT *)dataBufStruct)->appSpecificData,
            0
        );

    icDataReceived(gctx.ctrl, dataBufStruct);
}



/*
 *4688 I2C 读写封装测试，操作4688 0x7000(OTP_SRAM， RM)寄存器，写入延时1S后读出
 * J5,SID=1,I2C slave addr 0x6c
 * J6,SID=0,I2C slave addr 0x20
 * add by xhwang, 2016/07/25
 */
#include <rtems.h>
#include <pthread.h>

#define SENSORS_4688_ADDR_6C  (0x6c>>1)  //J5
#define SENSORS_4688_ADDR_20  (0x20>>1)  //J6

/* 4688 I2C 读接口 */
extern int ov4688_i2c_read(unsigned int slave_addr, unsigned int reg, unsigned char *val);
/* 4688 I2C 写接口 */
extern int ov4688_i2c_write(unsigned int slave_addr, unsigned int reg, unsigned char val);
extern int lrt_g_mipi_cnt;
extern int lrt_g_mipi_cnt1;
extern volatile int lrt_g_send_data_cnt;
extern volatile int lrt_g_fifo_cnt;

/* 1S延时测试 */
static void task_cnt(void)
{
	unsigned char val_1,val_2;
	unsigned int test_val=0;
	int old_cont = 0;

	printf("task_cnt\n");
	while(1)
	{
		test_val++;

//		/* 分别写入有效数据到0x7000 */
//		ov4688_i2c_write(SENSORS_4688_ADDR_6C, 0x7000, test_val);
//		ov4688_i2c_write(SENSORS_4688_ADDR_20, 0x7000, test_val);

		/* 延时1S */
		rtems_task_wake_after(1000);

//		/* 分别从0x7000读取有效数据 */
//		val_1=val_2=0;
//		ov4688_i2c_read(SENSORS_4688_ADDR_6C, 0x7000, &val_1);
//		ov4688_i2c_read(SENSORS_4688_ADDR_20, 0x7000, &val_2);

//		printf("cnt(%d %d)(%d %d)\n",frameCountA,frameCountB,val_1, val_2);
		printf("cnt(%d %d source,fifo,out(%d %d %d) los(%d) send(%d)\n",frameCountA, frameCountB, lrt_g_mipi_cnt, lrt_g_fifo_cnt, lrt_g_send_data_cnt, los_frameC, frame_cnt );
		los_frameC=0;
		frame_cnt = 0;
	}
}

/* I2C测试程序创建 */ 
static void create_cnt(void)
{
	pthread_t Thread;
    int policy;
    struct sched_param param;
	int rc;
	printf("create_cnt\n");

	if ((rc = pthread_create(&Thread, NULL, task_cnt, NULL)) != 0)
	{
		printf( "pthread_create: fail\n");
		exit(1);
	}
		 
    pthread_getschedparam(Thread, &policy, &param);
     param.sched_priority = 211;
     pthread_setschedparam(Thread, policy, &param);	
}

void spioutInit(void)
{
	DrvGpioSetMode(SPI0_MOSI, D_GPIO_MODE_7 | D_GPIO_DIR_OUT);
	DrvGpioSetMode(SPI0_SS_1, D_GPIO_MODE_7 | D_GPIO_DIR_OUT);
	DrvGpioSetMode(SPI0_SCLK_OUT, D_GPIO_MODE_7 | D_GPIO_DIR_OUT);

	DrvGpioSetPinLo(SPI0_MOSI);
	DrvGpioSetPinLo(SPI0_SS_1);
	DrvGpioSetPinLo(SPI0_SCLK_OUT);
	printf("spi light has been turned off!\n");
}

/*GPIO27/MODE5/PWM1*/
void setSyncSignal(int enable)
{
	if(enable == EN)
	{
		DrvGpioEnPwm( 1, 0 );
//		printf("setsetSyncSignal!\n ");
		SET_REG_WORD(GPIO_PRE_SCALER_ADR, (0xb2  << 16));
		DrvGpioSetMode(27, D_GPIO_MODE_5 | D_GPIO_DIR_OUT);
        DrvGpioSetPwm(1, 0, 0, 30, 44680-2);//44810-3
		DrvGpioEnPwm( 1, 1 );
	}
	else
	{
		SET_REG_WORD(GPIO_PRE_SCALER_ADR, (0xb2 << 16));
		DrvGpioSetMode(27, D_GPIO_MODE_5 | D_GPIO_DIR_OUT);
        DrvGpioSetPwm(1, 0, 0, 30, 44680-2);
		DrvGpioEnPwm( 1, 0 );
	}	

	//create_cnt();
}

static void setFlashLight(int enable)
{
	/*\C9\E8\D6\C3\C9\C1\B9\E2\B5ƹܽ\C5\CA\E4\B3\F6*/
	DrvGpioSetMode(IOFLASHLIGHT, D_GPIO_MODE_7 | D_GPIO_DIR_OUT);

#if 1
	/*\C9\C1\B9\E2\B5ƿ\D8\D6\C6*/
	if(enable == EN)
	{
		DrvGpioSetPinLo(IOFLASHLIGHT);
	}	
	else
	{
		DrvGpioSetPinHi(IOFLASHLIGHT);	
	}

#else
	if(enable == EN)
	{
		printf("setFlashLight enable!\n ");
		SET_REG_WORD(GPIO_PRE_SCALER_ADR, (0xb2 << 16));
		DrvGpioSetMode(IOFLASHLIGHT, D_GPIO_MODE_5 | D_GPIO_DIR_OUT);
		DrvGpioSetPwm(2, 0, 0, 3000, 44810-3000);
		DrvGpioEnPwm( 2, 1 );
	}
	else
	{
		printf("setFlashLight enable!\n ");
		SET_REG_WORD(GPIO_PRE_SCALER_ADR, (0xb2 << 16));
		DrvGpioSetMode(IOFLASHLIGHT, D_GPIO_MODE_5 | D_GPIO_DIR_OUT);
		DrvGpioSetPwm(2, 0, 0, 3000, 44810-3000);
		DrvGpioEnPwm( 2, 0 );
	}
#endif

	return ;
}

static  void setFloodLight_old(int enable)
{
	/*ÉèÖÃÉÁ¹âµÆ¹Ü½ÅÊä³ö*/
	DrvGpioSetMode(IOFLOODLIGHT, D_GPIO_MODE_7 | D_GPIO_DIR_OUT);

	if(enable == EN)
	{
		DrvGpioSetPinLo(IOFLOODLIGHT);
	}	
	else
	{
		DrvGpioSetPinHi(IOFLOODLIGHT);	
	}

	return ;
}


void setFloodLight(int flood_light_ctl)
{
    printf("////setFloodLight:0x%x\n",flood_light_ctl);
	/*\C9\E8\D6\C3\C9\C1\B9\E2\B5ƹܽ\C5\CA\E4\B3\F6*/
    DrvGpioSetMode(SPI0_SS_1, D_GPIO_MODE_7 | D_GPIO_DIR_OUT);

    DrvGpioSetPinHi(SPI0_SS_1);
    DrvTimerSleepMicro(10);

    DrvGpioSetPinLo(SPI0_SS_1);
    DrvTimerSleepMicro(10);

    write_1byte(flood_light_ctl&0xFF);
    //printf("flood_light_ctl is set to 0x%x\n",flood_light_ctl);
    Texture_Brightness=(flood_light_ctl&0x0C)>>2;//read 0-3
    //printf("after set texture_brightness=0x%x\n",Texture_Brightness);

    DrvGpioSetPinHi(SPI0_SS_1);
    DrvTimerSleepMicro(10);

	return ;
}

void SetStartCapFlg(int flg, int time)
{
    start_cap_flg = flg;
    timer_cnt = time/100; //delay 500ms
}

void CamTrigger0(int capCmd)
{	
	//printf("g_flgCaptureSpec=%d,g_flgCaptureTex=%d\n",lrt_g_flgCaptureSpec,lrt_g_flgCaptureTex);
	//\D5\FD\D4\DAץ\C5\C4
#ifdef USE_MULTISPECKLE_VERSION		
	if((lrt_g_flgCaptureSpec !=0) || (lrt_g_flgCaptureTex ==1))
		return ;
#else
	if((lrt_g_flgCaptureSpec == 1) || (lrt_g_flgCaptureTex == 1))
		return ;
#endif		

    printf("CAM triggerMod0:%d ====\n",capCmd);

#if 0	
	//ץ\C5\C4ɢ\B0\DFͼ\CF\F1
	setFlashLight(EN);
	DrvTimerSleepMs(5);  
	
	/*\C9\E8\D6\C3ͬ\B2\BDץ\C5\C4\D0ź\C5*/
	setSyncSignal(EN);
	lrt_g_flgCaptureSpec = 1;

	rtems_task_wake_after(50);
	setFlashLight(DIS); /*\B9ر\D5\C9\C1\B9\E2\B5\C6*/

#else	
    if((capCmd&0x03) == MODCAPSPEC)//0-3 mod
	{
        printf("CAP speckle trigger (%d:%d) ====\n", camCfg.camSyncSpeckleLightLTime, camCfg.camSyncSpeckleLightHTime);

        lrt_g_flgCaptureSpec = 1;
//        setSyncSignal(EN);
        rtems_task_wake_after(camCfg.camSyncSpeckleLightLTime);
        setSpeckleLight((capCmd&0xE0)|0x10|((Speckle_Brightness<<2)&0x0C)|CAPMOD0);//specklelight on 5-7 id,4 switch,0-3 brightness
        rtems_task_wake_after(camCfg.camSyncSpeckleLightHTime);
//        setSpeckleLight((capCmd&0xE0)|0x00|((Speckle_Brightness<<2)&0x0C)|CAPMOD0); //specklelight off/*\B9乇\D5\C9\C1\B9\E2\B5\C6*/
	}
    else if((capCmd&0x03) == MODCAPTEX)
	{		
        printf("CAP texture trigger (%d:%d) ====\n", camCfg.camSyncTextureLightLTime, camCfg.camSyncTextureLightHTime);

        lrt_g_flgCaptureTex = 1/*1*/;
//        setSyncSignal(EN);
        rtems_task_wake_after(camCfg.camSyncTextureLightLTime);
        setFloodLight((capCmd&0xE0)|0x10|((Texture_Brightness<<2)&0x0C)|CAPMOD0);
        rtems_task_wake_after(camCfg.camSyncTextureLightHTime);
//        setFloodLight((capCmd&0xE0)|0x00|((Texture_Brightness<<2)&0x0C)|CAPMOD0);
	}
#endif

    if((capCmd&0x03) == MODCAPTEX) // dynamic modify texture exp/gain
    {
        // if 2 exps and 2 gains are the same, then skip the following steps to get a fast capture
        if ((camCfg.tri1_exp != camCfg.tri0_exp) || (camCfg.tri1_gain != camCfg.tri0_gain))
        {
            camera_control_ae_manual_exposure(0, camCfg.tri1_exp);
            camera_control_ae_manual_exposure(1, camCfg.tri1_exp);
            camera_control_ae_manual_gain(0, camCfg.tri1_gain);
            camera_control_ae_manual_gain(1, camCfg.tri1_gain);

            //sleep
            rtems_task_wake_after(camCfg.speExpGainTakeEffectTime);
        }
    }
    else if((capCmd&0x03) == MODCAPSPEC)
    {
        if((((capCmd&0xE0)>>5) - 1) == camCfg.tri1_seqlen) // last speckle cmd
        {
            // if 2 exps and 2 gains are the same, then skip the following steps to get a fast capture
            if ((camCfg.tri1_exp != camCfg.tri0_exp) || (camCfg.tri1_gain != camCfg.tri0_gain))
            {
                camera_control_ae_manual_exposure(0, camCfg.tri0_exp);
                camera_control_ae_manual_exposure(1, camCfg.tri0_exp);
                camera_control_ae_manual_gain(0, camCfg.tri0_gain);
                camera_control_ae_manual_gain(1, camCfg.tri0_gain);

                //sleep
                rtems_task_wake_after(camCfg.texExpGainTakeEffectTime);
            }
        }
    }

//    assert(sem_post(&semWaitForCapReady) != -1);
	return ;
}


void CamTrigger123(int capCmd)
{
	printf("enter CamTrigger123() --\n");
    if((lrt_g_flgCaptureSpec > 0) || (lrt_g_flgCaptureTex > 0))
        return ;

    printf("CAM triggerMod1:%d ====\n",capCmd);
    SetStartCapFlg(1,500); //500ms time out

/*
    // if 2 exps and 2 gains are the same, then skip the following steps to get a fast capture
    if ((camCfg.tri1_exp != camCfg.tri0_exp) || (camCfg.tri1_gain != camCfg.tri0_gain))
    {
        camera_control_ae_manual_exposure(0, camCfg.tri1_exp);
        camera_control_ae_manual_exposure(1, camCfg.tri1_exp);
        camera_control_ae_manual_gain(0, camCfg.tri1_gain);
        camera_control_ae_manual_gain(1, camCfg.tri1_gain);

        lrt_g_flgCaptureTex = 1;

        //sleep
        rtems_task_wake_after(camCfg.speExpGainTakeEffectTime);
    }
*/
    printf("CAP MOD1 trigger ====\n");
    lrt_g_flgCaptureSpec = camCfg.tri1_seqlen;
    //setSyncSignal(EN);
    if(lrt_g_capMod == CAPMOD1)
    {
        setSpeckleLight((camCfg.tri1_seqlen<<5)|0x10|((Speckle_Brightness<<2)&0x0C)|(capCmd&0x03));//specklelight on 5-7 id,4 switch,0-3 brightness
    }
    else if((lrt_g_capMod == CAPMOD2)||(lrt_g_capMod == CAPMOD3))
    {
//     	setSpeckleLight(((camCfg.tri1_seqlen+1)<<5)|0x10|((Speckle_Brightness<<2)&0x0C)|(capCmd&0x03));//specklelight on 5-7 id,4 switch,0-3 brightness
    	setSpeckleLight(((35-camCfg.tri1_exp/RATE_EXPOSURE)<<3)|(CAPMOD3&0x03));
// 		setSpeckleLight(((31)<<3)|(CAPMOD3&0x03));
    }
/*
    rtems_task_wake_after(200);
    // if 2 exps and 2 gains are the same, then skip the following steps to get a fast capture
    if ((camCfg.tri1_exp != camCfg.tri0_exp) || (camCfg.tri1_gain != camCfg.tri0_gain))
    {
        camera_control_ae_manual_exposure(0, camCfg.tri0_exp);
        camera_control_ae_manual_exposure(1, camCfg.tri0_exp);
        camera_control_ae_manual_gain(0, camCfg.tri0_gain);
        camera_control_ae_manual_gain(1, camCfg.tri0_gain);

        //sleep
        rtems_task_wake_after(camCfg.texExpGainTakeEffectTime);
    }
*/
    return ;
}

int capseq_send(int capCmd)
{
    int err;
    err = rtems_message_queue_send(q_capseq_msg,&capCmd,sizeof(int));
    return err;
}

void CamTriggerTask(void)
{

    //抓拍模式设置，单次抓拍或连拍
    lrt_g_capMod = CAPMOD3;

    while(1)
    {
        int capCmd = 0;

        rtems_task_wake_after(1);

        uint32 size;
        int err;
        if(!lrt_g_flgCaptureSpec && !lrt_g_flgCaptureTex)//last speckle/texture cmd has been proceesed
        {
            err = rtems_message_queue_receive(q_capseq_msg,&capCmd,&size,RTEMS_NO_WAIT,0);
        }
        else
        {
            continue;
        }

        if((err != RTEMS_SUCCESSFUL) || (0 == capCmd))
        {
            continue;
        }

        printf("capCmd_CamMode:%d\n",lrt_g_capMod);
        printf("capCmd_CamTrigger:%d\n",capCmd);

        if(lrt_g_capMod == CAPMOD0)
        {
            CamTrigger0(capCmd);
        }
        else if((lrt_g_capMod == CAPMOD1) || (lrt_g_capMod == CAPMOD2) || (lrt_g_capMod == CAPMOD3))
        {
            CamTrigger123(capCmd);
        }
    }
}


void write_1byte(u64 spi_data)
{
    DrvGpioSetMode(SPI0_SCLK_OUT, D_GPIO_MODE_7 | D_GPIO_DIR_OUT);
    DrvGpioSetMode(SPI0_MOSI, D_GPIO_MODE_7 | D_GPIO_DIR_OUT);

    DrvGpioSetPinHi(SPI0_SCLK_OUT);
    DrvTimerSleepMicro(10);

    for(int j=0;j<8;j++)
    {
        //DrvGpioSetPinHi(SPI0_SCLK_OUT);
        DrvGpioSetPinLo(SPI0_SCLK_OUT);
        DrvTimerSleepMicro(10);
        if(((spi_data & 0x80)>>7)==1)
        {
            DrvGpioSetPinHi(SPI0_MOSI);
            DrvTimerSleepMicro(10);
        }
        else
        {
            DrvGpioSetPinLo(SPI0_MOSI);
            DrvTimerSleepMicro(10);
        }

        //DrvGpioSetPinLo(SPI0_SCLK_OUT);
        DrvGpioSetPinHi(SPI0_SCLK_OUT);
        DrvTimerSleepMicro(10);

        spi_data = spi_data<<1;
    }

}

void setSpeckleLight(int speckle_light_ctl)
{	
#if 0
	if(speckle_light_ctl == SPECKLE_LIGHT_OFF)
	{
		setFlashLight(DIS);
	}	
	else if(speckle_light_ctl == SPECKLE_LIGHT_ON)
	{
		setFlashLight(EN);
	}	
#endif
    printf("////setSpeckleLight:0x%x\n",speckle_light_ctl);
	DrvGpioSetMode(SPI0_SS_1, D_GPIO_MODE_7 | D_GPIO_DIR_OUT);

	DrvGpioSetPinHi(SPI0_SS_1);
	DrvTimerSleepMicro(10);

	DrvGpioSetPinLo(SPI0_SS_1);
	DrvTimerSleepMicro(10);

	write_1byte((speckle_light_ctl)&0xFF);//write 0-7
	Speckle_Brightness=(speckle_light_ctl&0x0C)>>2;//read 0-3
	printf("after set speckle_brightness=0x%x\n",Speckle_Brightness);

	DrvGpioSetPinHi(SPI0_SS_1);
	DrvTimerSleepMicro(10);
	return ;
}

static void TimerCntThrd(void)
{
    while(1)
    {
//        DrvTimerSleepMs(10);
        rtems_task_wake_after(100);

        if(timer_cnt>0)
        {
            timer_cnt--;
        }
        else
        {
            start_cap_flg = 0;
        }
    }
}


static void SynTriggerThrd(void)
{
	unsigned char val_1,val_2;
	unsigned char test_val=0;

    //DrvTimerSleepMs(2000);
    //setSyncSignal(EN);

#if 0
	
	//ץ\C5\C4\CE\C6\C0\EDͼ\CF\F1
	setFlashLight(DIS);
	setFloodLight(EN);
	DrvTimerSleepMs(20); 	
	setFloodLight(DIS); /*\B9ر\D5\C9\C1\B9\E2\B5\C6*/
#else
    // added by zp 2017/11/23, indicate that myriad2 is prepared ready
    setFloodLight(0x100|(6<<5)|(1<<4)|0x0C|CAPMOD0);//0x1df
    DrvTimerSleepMs(10);
//    setFloodLight(0x100|(6<<5)|(0<<4)|0x0C|CAPMOD0);//0x1cf
#endif
	create_cnt();
/*
	rtems_task_wake_after(2000);
	printf("Start watch dog..\n");
    WatchdogInit(WATCHDOG_0, EN);
    FeedWatchdog(WATCHDOG_0);
*/
	while(1)
	{
		/* ???1S */
		rtems_task_wake_after(10000);	

		if(flg_cap_loop == 1)
			CamTrigger123(0x1F);
#if 0
		if(lrt_capFrame.flgCapture == 1)
		{
			continue;
		}
		else
		{
			 CamTrigger(MODCAPSPEC);
		}
#endif

	}
	
	return ;
	
}


// added by zp for reset, 2017/11/21
void HeartCheckFunc(int val)
{
    printf("HeartCheckFunc: %d\n",val);

    switch (val) {
    case HEART_CHECK_ENABLE:
        g_bEnableHeartCheck = true;
        break;
    case HEART_CHECK_DISABLE:
        g_bEnableHeartCheck = false;
        break;
    case HEART_CHECK_SET_HEARTBEAT:
        g_heartVal++;
        break;
    default:
        break;
    }
}

static int heartbeatInterval = 20000; // the interval of heart beat
void HeartCheckTask(void)
{
    while(1)
    {
        /* 20S */
        rtems_task_wake_after(heartbeatInterval);

        if (!g_bEnableHeartCheck)
            continue;

        if (g_heartVal)
        {
            printf("g_heartVal = %d\n",g_heartVal);
            g_heartVal = 0;
        }
        else // receive no heart
        {
            SET_REG_WORD(CPR_MAS_RESET_ADR,0);//reset
        }
    }
}

// added by zp for cam light sync test, 2017/11/23
void SetCamLightSyncLHTimeFunc(short val)
{
    printf("SetCamLightSyncLHTimeFunc: %d\n",val);

    char lowLevelTime = val & 0x00ff;
    char highLevelTime = (val & 0xff00) >> 8;

    if ((lowLevelTime > 0) && (lowLevelTime < 22))
    {
        camCfg.camSyncSpeckleLightLTime = lowLevelTime;
    }

    if ((highLevelTime > 33) && (highLevelTime < 72))
    {
        camCfg.camSyncSpeckleLightHTime = highLevelTime;
    }
    printf("SetCamLightSyncLHTimeFunc_[lowLevelTime, highLevelTime] = [%d,%d]\n",
           camCfg.camSyncSpeckleLightLTime, camCfg.camSyncSpeckleLightHTime);
}

// added by zp, 2017/11/25
extern volatile CamConfig __attribute__((section(".ddr_direct.data"))) camCfg;
void SetCamLightSyncCaptureFunc(short val)
{
    printf("SetCamLightSyncCaptureFunc: %d\n",val);

    char lowLevelTime, highLevelTime;
    char texlen, seqlen;

    switch (g_camCfg_para_cnt) {
    case 0:
        camCfg.tri0_exp = val;
        break;
    case 1:
        camCfg.tri0_gain = val;
        break;
    case 2:
        camCfg.tri1_exp = val;
        break;
    case 3:
        camCfg.tri1_gain = val;
        break;
    case 4:
        lowLevelTime = (val & 0xff00) >> 8;
        highLevelTime = val & 0x00ff;
	
        if ((lowLevelTime > 0) && (lowLevelTime < 36))
        {
            camCfg.camSyncSpeckleLightLTime = lowLevelTime;
        }

        if ((highLevelTime > 5) && (highLevelTime < 72))
        {
            camCfg.camSyncSpeckleLightHTime = highLevelTime;
        }
        break;
    case 5:
        lowLevelTime = (val & 0xff00) >> 8;
        highLevelTime = val & 0x00ff;
        if ((lowLevelTime > 0) && (lowLevelTime < 36))
        {
            camCfg.camSyncTextureLightLTime = lowLevelTime;
        }

        if ((highLevelTime > 5) && (highLevelTime < 72))
        {
            camCfg.camSyncTextureLightHTime = highLevelTime;
        }
        break;
    case 6:
        texlen = (val & 0xff00) >> 8;
        seqlen = val & 0x00ff;
        if (texlen == 1)
        {
            camCfg.tri1_texture = texlen;
        }

        if ((seqlen > 0) && (seqlen < 4))
        {
            camCfg.tri1_seqlen = seqlen;//1~3
        }
        break;
    case 7:
        texlen = (val & 0xff00) >> 8;
        seqlen = val & 0x00ff;

        camCfg.texExpGainTakeEffectTime = texlen;
        camCfg.speExpGainTakeEffectTime = seqlen;
        break;
    default:
        break;
    }
    g_camCfg_para_cnt++;

    if(g_camCfg_para_cnt==sizeof(CamConfig)/sizeof(short))//8 packets
    {
        g_camCfg_para_cnt = 0;
        g_camCfgOK = true;
        printf("SetCamLightSyncCaptureFunc_CamConfig:[%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d]\n",
               camCfg.tri0_exp,
               camCfg.tri0_gain,
               camCfg.tri1_exp,
               camCfg.tri1_gain,
               camCfg.camSyncSpeckleLightLTime,
               camCfg.camSyncSpeckleLightHTime,
               camCfg.camSyncTextureLightLTime,
               camCfg.camSyncTextureLightHTime,
               camCfg.tri1_texture,
               camCfg.tri1_seqlen,
               camCfg.texExpGainTakeEffectTime,
               camCfg.speExpGainTakeEffectTime);
    }
}


void OneCmdCaptureFunc(int val)
{
    printf("OneCmdCaptureFunc: %d\n",val);

    switch(val)
    {
    case 0:// original capture mode, CAPMOD3 for 4 continues frame(texture + speckles)
        lrt_g_capMod = CAPMOD3;
        if (g_camCfgOK && !g_camCfg_para_cnt)
        {
            if(lrt_g_capMod == CAPMOD0)
            {
                capseq_send((6<<5) | MODCAPTEX);
                rtems_task_wake_after(35);
                switch (camCfg.tri1_seqlen) {
                case 1:
                    capseq_send((2<<5) | MODCAPSPEC); //65
                    break;
                case 2:
                    capseq_send((2<<5) | MODCAPSPEC); //65
                    rtems_task_wake_after(35);
                    capseq_send((3<<5) | MODCAPSPEC); //97
                    break;
                case 3:
                    capseq_send((2<<5) | MODCAPSPEC); //65
                    rtems_task_wake_after(35);
                    capseq_send((3<<5) | MODCAPSPEC); //97
                    rtems_task_wake_after(35);
                    capseq_send((4<<5) | MODCAPSPEC); //129
                    break;
                default:
                    break;
                }
            }
            else if(lrt_g_capMod == CAPMOD1)
            {
                capseq_send(CAPMOD1);
            }
            else if(lrt_g_capMod == CAPMOD2)
            {
                capseq_send(CAPMOD2);
            }
            else if(lrt_g_capMod == CAPMOD3)
            {
                capseq_send(CAPMOD3);
            }
            else
                printf("error capmod!!!\n");

        }
        break;

    case 1:
        lrt_g_capMod = CAPMOD1; //prepareCapData1

        // if 2 exps and 2 gains are the same, then skip the following steps to get a fast capture
        if ((camCfg.tri1_exp == camCfg.tri0_exp) && (camCfg.tri1_gain == camCfg.tri0_gain))
        {
            lrt_g_flgCaptureTex = 1;
//            setFloodLight((7<<5)|0x10|((Texture_Brightness<<2)&0x0C)|CAPMOD0);
        }

        capseq_send(CAPMOD1);
        break;

    default:
        break;
    }
}

// added by zp for framerate setting, 2017/12/04
void SetFrmRateFunc(int val)
{
    printf("SetFrmRateFunc: %d\n",val);
    if (val > 30 || val < 0)
    {
        printf("framerate value error!\n");
        return;
    }
    SetFrameSkipFlag(val);
}

// added by zp for heartbeat interval setting, 2017/12/04
void SetHeartbeatIntervalFunc(int val) // unit:s
{
    printf("SetHeartbeatIntervalFunc: %d(unit:s)\n",val);

    if (val < 0)
    {
        printf("heartbeat interval value error!\n");
        return;
    }

    heartbeatInterval = val * 1000;// s --> ms
}

// added by zp for reset, 2018/01/11
void ResetFunc(int val)
{
    printf("ResetFunc: %d\n",val);

    SET_REG_WORD(CPR_MAS_RESET_ADR,0);//reset
}

//Time out ticks for watchdog
int updTicks = 3*TICKS_1S;
void WatchdogInit(int dogID, int stat)
{
	//enable the watchdog
	if(dogID == WATCHDOG_0)
	{
		SET_REG_WORD(TIM0_WATCHDOG_ADR, (u32)2400000000);
		SET_REG_WORD(TIM0_WATCHDOG_INT_THRES_ADR, 0);
		SET_REG_WORD(TIM0_SAFE_ADR, TIM_SAFE_VALUE);
		SET_REG_WORD(TIM0_WDOG_EN_ADR, stat);
	}
	else if(dogID == WATCHDOG_1)
	{
		SET_REG_WORD(TIM0_WATCHDOG_ADR, (u32)2400000000);
		SET_REG_WORD(TIM0_WATCHDOG_INT_THRES_ADR, 0);
		SET_REG_WORD(TIM1_SAFE_ADR, TIM_SAFE_VALUE);
		SET_REG_WORD(TIM1_WDOG_EN_ADR, stat);
	}
	else
		printf("Watchdog ID ERROR!! \n");
}

void FeedWatchdog(int dogID)
{
	// Configure elapsed ms before system reset
	if(dogID == 0)
	{
		SET_REG_WORD(TIM0_WATCHDOG_ADR, (u32)updTicks);
	}
	else if(dogID == 1)
	{
		SET_REG_WORD(TIM1_WATCHDOG_ADR, (u32)updTicks);
	}
	else
		printf("Watchdog ID ERROR!! \n");
}

//add by zp for awb control, 2018/01/22
// val: 1 off
//      0 on
static char AWBOffFlag = false;
void AWBOffOrRestartOn(int val)
{
    printf("AWBOffOrRestartOn: %d\n",val);

    if (val) //off
    {
	    camera_control_awb_turn_off_set_default(0);
	    camera_control_awb_turn_off_set_default(1);
	    AWBOffFlag = true;
    }
    else
    {
	    camera_control_awb_restart(0);
	    camera_control_awb_restart(1);
    }
}

// added by zp, 2018/01/22
extern volatile AwbGainsConfig __attribute__((section(".ddr_direct.data"))) awbGainsCfg;
void SetAwbGainsConfigFunc(short val)
{
    printf("SetAwbGainsConfigFunc: %d\n",val);

    switch (g_awbgains_para_cnt) {
    case 0:
        awbGainsCfg.left.r_gain = (float)(val)/1000.0f;
        break;
    case 1:
        awbGainsCfg.left.g_gain = (float)(val)/1000.0f;
        break;
    case 2:
        awbGainsCfg.left.b_gain = (float)(val)/1000.0f;
        break;
    case 3:
        awbGainsCfg.right.r_gain = (float)(val)/1000.0f;
        break;
    case 4:
        awbGainsCfg.right.g_gain = (float)(val)/1000.0f;
        break;
    case 5:
        awbGainsCfg.right.b_gain = (float)(val)/1000.0f;
        break;
    default:
        break;
    }
    g_awbgains_para_cnt++;

    if(g_awbgains_para_cnt==sizeof(awbGainsCfg)/sizeof(short))//6 packets
    {
        g_awbgains_para_cnt = 0;
        printf("SetAwbGainsConfigFunc:left [%f,%f,%f] right[%f,%f,%f]\n",
               awbGainsCfg.left.r_gain,
               awbGainsCfg.left.g_gain,
               awbGainsCfg.left.b_gain,
               awbGainsCfg.right.r_gain,
               awbGainsCfg.right.g_gain,
               awbGainsCfg.right.b_gain);
       camera_control_bayor_manual_all_gain(0);
       camera_control_bayor_manual_all_gain(1);
    }
}
