
/*
RTEMS 系统封装，参考POSIX协议
todo 其他封装
ADDBY XHWANG 2016-5-31
*/


#ifndef OS_ADAPTA_H_
#define OS_ADAPTA_H_

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <rtems.h>
#include <bsp.h>

#include <semaphore.h>
#include <pthread.h>
 #include <mqueue.h>  
#include <sched.h>
#include <mv_types.h>
#include <DrvLeon.h>
#include <DrvDdr.h>
#include <DrvCpr.h>
#include <DrvShaveL2Cache.h>
#include <swcLeonUtils.h>
#include <DrvLeonL2C.h>
#include <DrvTimer.h>

#include <rtems/blkdev.h>
#include <rtems/ide_part_table.h>
#include <rtems/ramdisk.h>
#include <rtems/libcsupport.h>
#include <OsDrvCpr.h>
#include <rtems/io.h>
#include <rtems/libio.h>
#include <OsDrvTimer.h>

/*****************************************/
/*
   thread      :创建线程返回的PID
   attr_arg    :创建线程的属性(默认使用NULL :继承父进程的属性)
   task_entry  :创建的线程函数 
   task_arg    :创建的线程函数的入参
   priority    :创建线程的优先级

	返回值: 0  : 创建成功

	example    : 创建优先级为10的线程，rtos_task_creat(&pid, NULL, task_entry, task_arg, 10)
*/

int rtos_task_creat(pthread_t *thread,  pthread_attr_t *attr_arg, void *(*task_entry)(void*), void *task_arg,  int priority);


/*****************************************/
/*
   key         :创建消息队列的关键字
   msg_num     :创建消息队列的个数
   msg_size    :创建消息队列的传输长度 
  
	返回值: -1  : 创建失败
	其他值:       创建的消息队列值

	example    : 创建消息队列的，个数为100,每个消息队列最大长度为4, rtos_msgq_create("/temp", 100, 4);
*/
int rtos_msgq_create (void* key, int msg_num, int msg_size);

/* 消息队列打开 */
int rtos_msgq_open (void* key);

/* 返回实际接收长度 */
int rtos_msgq_recv (int q_id,void *msg,int len) ;

/* 消息发送 */
int rtos_msgq_send(int q_id,void *msg,int len, int pri) ;

void myprintf(char* p_fmt, ...);

#endif

