
/*

VSC2��д��װ
ADDBY XHWANG 2016-5-31
*/

#ifndef VSC2_APP_IF_H

#define VSC2_APP_IF_H

#define ENDPOINT1 0
#define ENDPOINT2 1
#define ENDPOINT3 2
#define ENDPOINT4 3


/* ͨ��1��ݳ��� */
#define ENDPOINT1_SIZE   1023

/* ͨ��2��ݳ��� */
#define ENDPOINT2_SIZE   (224*172*2)

/* ͨ��3��ݳ��� */
#define ENDPOINT3_SIZE   (1280*720*2)

/*�����ӿ�����*/
typedef struct _st_port
{
	int ep;                               
	int bypass;           /* ��͸�� */                            
	int max_send_size;                          
	int max_recv_size;                          
}IF_PORT;

/*������������*/
typedef struct _st_if
{
	int(*if_send_req)(void* h ,void *p, int len); /*��������ӿ�*/
	void(*if_vsc_read_sync)(void* h ,void *p, int len); /*ͬ����ȡ���*/
	void (*if_rcv_cb)(void *pbuff,int len);       /*���շ�����*/	
	IF_PORT port;							      /*���շ���˿�*/
	int     isused;								  /*�ӿ�ʹ�ñ�־*/
}IF_SER;

/*  vscͨ����ʼ��
	ep 	:�˿ں�
	max_send_size:���������󳤶�
	max_recv_size:���������󳤶�
	cb:������ݻص�����
	bypass:�����Ƿ�͸��
*/
void *vsc2_if_get(int ep, int max_send_size, int max_recv_size,int bypass, void (*cb)(void *,int ));
void Send_Data(void* handle ,void *p, int len);

/*
int usb_vsc_sendA(FrameT *frame);
int usb_vsc_sendA_capture(FrameT *frame);
int usb_vsc_sendB(FrameT *frame);
int usb_vsc_sendB_capture(FrameT *frame);
*/

#endif
