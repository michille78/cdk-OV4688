
#define PU_BRIGHTNESS_CONTROL_INFO       0x03
#define PU_BRIGHTNESS_CONTROL_MIN        0x0000
#define PU_BRIGHTNESS_CONTROL_MAX        0x4000              // 0xFF
#define PU_BRIGHTNESS_CONTROL_RES        0x0001
#define PU_BRIGHTNESS_CONTROL_DEF        0x0600              //0x007F          For 4188  camera A  exposure

#define PU_CONTRAST_CONTROL_INFO           0x03
#define PU_CONTRAST_CONTROL_MIN            0x0000
#define PU_CONTRAST_CONTROL_MAX            0x03FF               //0x00FF
#define PU_CONTRAST_CONTROL_RES            0x0001
#define PU_CONTRAST_CONTROL_DEF            0x0080                    //0x007F       For 4188  camera A  GAIN


#define PU_HUE_CONTROL_INFO           0x03
#define PU_HUE_CONTROL_MIN            0x0000
#define PU_HUE_CONTROL_MAX            0x03FF                    //0x0168
#define PU_HUE_CONTROL_RES            0x0001                                 //0x0064
#define PU_HUE_CONTROL_DEF            0x0080                      //0x0000               For 4188  camera B  GAIN

#define PU_SATURATION_CONTROL_INFO       0x03
#define PU_SATURATION_CONTROL_MIN        0x0000
#define PU_SATURATION_CONTROL_MAX        0x4000                 // 0x00FF
#define PU_SATURATION_CONTROL_RES        0x0001
#define PU_SATURATION_CONTROL_DEF        0x0600                 //0x007F       For 4188  cameraB  exposure

typedef struct tag_queryctrl
{
	uint32_t maximum;
	uint32_t minimum;
	uint32_t step;
	uint32_t default_value;
}queryctrl_t;

typedef struct tag_DeviceInfo
{
	uint32_t  DeviceVersion;
	uint32_t  DeviceType;
	uint16_t  DepthFrameWidth;
	uint16_t  DepthFrameHeight;
	uint16_t  BitsPerPoint;
	uint16_t  VisibleFrameWidth;
	uint16_t  VisibleFrameHeight;
	uint16_t  BitsPerPixel;
	uint16_t  BlockSizeIn;
	uint16_t BlockSizeOut;
	queryctrl_t querygainctrla;
	queryctrl_t queryexptimectrla;
	queryctrl_t querygainctrlb;
	queryctrl_t queryexptimectrlb;
}DeviceInfo_t;


int GetDeviceInfo(DeviceInfo_t *deviceinfo);
