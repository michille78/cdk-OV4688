/*
 ============================================================================
 Name        : CmdHandle.h
 Author      : jnzhao@sunnyoptical.com
 Version     : 2016骞�2鏈�4鏃� 涓婂崍11:27:46
 Copyright   : sunnyoptical.com
 Description : 
 History     : 
 ============================================================================
 */
#ifndef PMD_MV2_USB_FIRMWARE_UPDATE_VSC_TEST_APP_LEON_CMDHANDLE_H_
#define PMD_MV2_USB_FIRMWARE_UPDATE_VSC_TEST_APP_LEON_CMDHANDLE_H_

#define  ACK_CMD_PACKETS	4
#define PACKETS_LEN	(1024*1024)
#define ACK_LEN            1023
typedef struct
{
	char *pPacket;
	int PacketSize;
	int offset;
}SUsbPacket;

typedef struct
{
	SUsbPacket *pPacketList[ACK_CMD_PACKETS];
	int CurPacket;
	int VaildPacketNum;
	int Size;
}SCmdList;

//command
typedef  struct tag_FlashCMD
{
	uint32_t  Cmd;
	uint32_t  PageAddr;   //璇汇�佸啓銆佹摝闄よ捣濮嬪湴鍧�
	uint32_t  NumPages2;//鎿嶄綔Sectors, pages鏁伴噺
}FlashCMD_t;

typedef struct tag_Preamble
{
	uint8_t buffer[8];
	uint8_t length;
	uint8_t ctrlMask;
}Preamble_t;

typedef struct tag_I2CCMD
{
	uint32_t Cmd;   //command
	uint32_t Len;//Size of the transfer in bytes
	uint32_t Retry; //Number of times to retry request in case of a NAK response or error
	Preamble_t preamble;
}I2CCMD_t;


/**************** Control endpoint commands **************/
#define CMD_READ_FLASH					    (1)          /* Command: Read the SPI-Flash */
#define CMD_ERASE_FLASH					(2)          /* Command: Erase the SPI-Flash */
#define CMD_WRITE_FLASH					(3)          /* Command: Write Data to the SPI-Flash */
#define CMD_WRITE_I2C					        (7)          /* Command: Write data via I2C-Bus */
#define CMD_READ_I2C					        (8)          /* Command: Read data via I2C-Bus */
#define CMD_START_IMAGER				(10)         /* Command: Start the Mira measurement sequence */
#define CMD_STOP_IMAGER					(11)         /* Command: Stop the Mira measurement sequence */
#define CMD_INIT_IMAGER					(12)         /* Command: Initialize the Mira */
#ifdef HLDS_FUNCTIONS
#define CMD_ACTIVATE_LDD_ENABLE			(13)         /* Command: Set CTL4 (LDD Enable) to "high" */
#define CMD_DEACTIVATE_LDD_ENABLE		(14)         /* Command: Set CTL4 (LDD Enable) to "low" */
#define CMD_ACTIVATE_HFM				(15)         /* Command: Set CTL7 (HFM) to "high" */
#define CMD_DEACTIVATE_HFM				(16)         /* Command: Set CTL7 (HFM) to "low" */
#endif
#define CMD_SET_MODE					        (17)         /* Command: Set a GPIO pin */
#define CMD_SET_FPS				                (18)         /* Command: Trigger a temperature readout */
#define CMD_SET_EXP					            (19)         /* Command: Set exposure */
#define CMD_SET_GAIN						    (20)	 /* Command: Set gain */
#define CMD_GET_LENTHPARAM         (21)
#define CMD_GET_CONFIG                         (23)
#define CMD_CLOSE_CAMERA              (24)
#define CMD_SEND_CALIBRATION_FILE     (25)
#define CMD_READ_CALIBRATION_FILE     (26)
#define CMD_CAPTURE_IMAGES	      (27)
#define CMD_CAPTURE_LOOP_TEST	     (28)

#define CMD_GET_DEVICEINFO             (80)
#define CMD_UPDATE_CALIBRATION   (81)
#define CMD_UPDATE_APP                       (82)

#define CMD_CMD_ERR			                 (-1)
#define CMD_NOTNEED_ACK                  (1)
#define CMD_CMD_OK				                 (2)

int CmdHandleFn(char *	pInPacket,int PacketLen);

//USB CMD ACK Packets Lists
int InitCMDQe();

int PutIdleCMDQe(SUsbPacket *pUsbPacket);
SUsbPacket * GetIdleCMDQe(void);
int PutReadyCMDQe(SUsbPacket *pUsbPacket);
SUsbPacket * GetReadyCMDQe(void);
int  GetNextReadyCMDQe(void);
int GetReadyListSize(void);

//static int I2cTransmitBytes(Preamble_t *pI2C_Preamble, uint8_t *pBuf, uint32_t uiLen, uint32_t uiRetry);
//static int I2cReceiveBytes(Preamble_t *pI2C_Preamble, uint8_t *pBuf, uint32_t uiLen, uint32_t uiRetry);

//static int erase_spi_flash(int fd, struct tFlashParams *p);
//static int write_spi_flash(int fd, struct tFlashParams *p);
//static int read_spi_flash(int fd, struct tFlashParams *p);

#endif /* PMD_MV2_USB_FIRMWARE_UPDATE_VSC_TEST_APP_LEON_CMDHANDLE_H_ */
