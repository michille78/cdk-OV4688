
/*
VSC2��д��װ
ADDBY XHWANG 2016-5-31
*/

#include"vsc2app_if.h"
#include"os_adapta.h"

#include "usbpumpdebug.h"
#include "usbpump_application_rtems_api.h"

#include "vsc2app_outcall.h"
#include "usbpump_vsc2app.h"
#include"string.h"


#include <ipipe.h>

#include <utils/mms_debug.h>
#include <utils/profile/profile.h>
#include <osal/osal_stdlib.h>
#include <osal/osal_mutex.h>
#include <osal/osal_time.h>
#include <osal/pool.h>

#include <registersMyriad.h>
#include <DrvRegUtils.h>
#include <DrvMipi.h>
#include <DrvMss.h>

#include "camera_control.h"
#include <rtems.h>   	//add by xw
#include "metadata.h"       //add by XW
#include "user_config.h"    //add by XW


#define MAX_IF			50					/*���֧�ֵĽӿ�����*/
#define TID_PRO_USED	1					/*�ӿ��Ѿ�ʹ��*/
#define TID_PRO_UNUSED  0					/*�ӿ�δʹ��*/

/*�ӿھ������*/
typedef IF_SER* IF_HANDLER;

static void vsc_if_rcv(IF_HANDLER vsc_if);
static void vsc_read_sync(void* handle ,void *p, int len);
static int vsc_write_sync(void* handle ,void *p, int len);

extern USBPUMP_VSC2APP_CONTEXT *	pSelf;
extern void FeedWatchdog(int dogID);

void *g_ep1_handle = 0;     //channel A
void *g_ep2_handle = 0;     //channel B
static rtems_id  q_usb_msgA =-1;
static rtems_id  q_usb_capture_msgA =-1;
static rtems_id  q_usb_msgB =-1;
static rtems_id  q_usb_capture_msgB =-1;
static pthread_t VSCSendAThread;
static pthread_t VSCSendBThread;

static void usb_vsc_init(void);
static void VSCSendAThrd(void);
static void VSCSendBThrd(void);
void los_dataWasSent(void *dataBufStruct);

/*����ӿ�ʹ�õ���*/
IF_SER		g_st_if[MAX_IF];
static  int  g_vsc_if_init_flag = 0;

static int createSemaphores()
{
	int i, status;
	for(i = 0; i < USBPUMP_VSC2APP_NUM_EP_IN; i++)
	{
		status = rtems_semaphore_create(rtems_build_name('I', 'N', '_', '0' + i), 0,
			RTEMS_SIMPLE_BINARY_SEMAPHORE, 0, &pSelf->semWriteId[i]);
		if (status != RTEMS_SUCCESSFUL)
		{
			return 1;
		}
	}

	for(i = 0; i < USBPUMP_VSC2APP_NUM_EP_OUT; i++)
	{
		status = rtems_semaphore_create(rtems_build_name('O', 'U', 'T', '0' + i), 0,
			RTEMS_SIMPLE_BINARY_SEMAPHORE, 0, &pSelf->semReadId[i]);
		if (status != RTEMS_SUCCESSFUL)
		{
			return 1;
		}
	}

	return 0;
}

void if_init(void)
{
	// create semaphores for transfer synchronization
	if(createSemaphores() != 0)
	{
		printf("Error creating semaphores");
	}

	/*��ʼ���ӿ��ڴ�*/	
	memset(g_st_if,0,sizeof(IF_SER)*MAX_IF);	

	usb_vsc_init();
}

void *vsc2_if_get(int ep, int max_send_size, int max_recv_size,int bypass, void (*cb)(void *,int ))
{
	int i			     = 0;
	IF_HANDLER handler 	 = 0;

	if(g_vsc_if_init_flag == 0)
	{
		if_init();
		g_vsc_if_init_flag = 1;
	}
	
	/*����δʹ�õĽӿ�*/
	for(i = 0; i < MAX_IF;i++)
	{
		if(g_st_if[i].isused == TID_PRO_UNUSED)
		{
			/*�ҵ����еĽӿ�*/
			/*ռ�øýӿ�*/
			g_st_if[i].isused 	= TID_PRO_USED;
			handler 			= &g_st_if[i];
			
			break;
		}
	}
	/*�Ƿ���Ч�ľ��*/
	/*��Ч�ľ��ֱ�ӷ��ش���*/
	if(handler == 0)
	{
		myprintf("if_get:��Ч�ľ��\n");
		return (int*)handler;
	}

	/*����ӿڲ���*/
	g_st_if[i].port.ep            = ep;
	g_st_if[i].port.max_recv_size = max_recv_size;
	g_st_if[i].port.max_send_size = max_send_size;
	
	g_st_if[i].if_send_req   = vsc_write_sync;

	/* �첽��ȡ,������ȡ�߳� */
	if(bypass == TRUE )
	{
		/* ͨ����Ҫ��ȡ���� */
		if(cb != 0)
		{
			g_st_if[i].if_rcv_cb   = cb;
			
			rtos_task_creat(0, 0, vsc_if_rcv, handler, 0);
		}
	}
	else
	{
		g_st_if[i].if_vsc_read_sync = vsc_read_sync;
	}
	
	/*������Ч���*/
	return (int *)handler;	
}

/* ͬ����ȡ���� */
static void vsc_read_sync(void* handle ,void *p, int len)
{
	UsbVscAppRead(pSelf, len, p, ((IF_SER*)handle)->port.ep);
}

/* ͬ��д������ */
static int vsc_write_sync(void* handle ,void *p, int len)
{
	int ret = 0;
	
	if(((IF_SER*)handle)->port.max_send_size != len)
	{
		myprintf("EP(%d)_WRITE_FAIL,LEN ERR %d %d\n", ((IF_SER*)handle)->port.ep+1,((IF_SER*)handle)->port.max_send_size, len);
		return -1;
	}
	
	ret = UsbVscAppWrite(pSelf, len, p, ((IF_SER*)handle)->port.ep);	

	rtems_semaphore_obtain(pSelf->semWriteId[((IF_SER*)handle)->port.ep], RTEMS_WAIT, (len/(500*1024))*10 + 10);

	return ret;
}

void Send_Data(void* handle ,void *p, int len)
{
	((IF_SER*)handle)->if_send_req(handle, p, len);
}


static char recv_buf[1024*10];

/********************************************************************

��������:vsc_if_rcv

********************************************************************/
static void vsc_if_rcv(IF_HANDLER vsc_if)	
{
	int max_recv_size = vsc_if->port.max_recv_size;
	int ret = 0;
	void *p_recv = recv_buf;//(void *)malloc(vsc_if->port.max_recv_size);
	if(p_recv == 0)
	{
		return;
	}

	myprintf("vsc_if_rcvsssss:%d %d\n\n",max_recv_size, vsc_if->port.ep);
	
	while(1)
	{

		UsbVscAppRead(pSelf, max_recv_size, p_recv, vsc_if->port.ep);
		ret = rtems_semaphore_obtain(pSelf->semReadId[vsc_if->port.ep], RTEMS_WAIT, RTEMS_NO_TIMEOUT);
		if(RTEMS_SUCCESSFUL == ret)
		{
			/* ���ջص� */
			vsc_if->if_rcv_cb(p_recv, max_recv_size);
		}
		else if(RTEMS_TIMEOUT == ret)
		{
			myprintf("vsc_if_rcv:EP%d,RTEMS_TIMEOUT\n",vsc_if->port.ep);			
		}
	}
}
 

/////////////////////////// add by XW ////////////////////////////
static void usb_vsc_init(void)
{
	static int flag = 0;
	int err, err2;
	if(flag == 1)
		return;

    err = rtems_message_queue_create(
            rtems_build_name('U', 'B', 'A', 'S'),
            1,
            sizeof (void*),
            RTEMS_DEFAULT_ATTRIBUTES,
            &q_usb_msgA
        );

    err2 = rtems_message_queue_create(
            rtems_build_name('U', 'B', 'A', 'C'),
            2,
            sizeof (void*),
            RTEMS_DEFAULT_ATTRIBUTES,
            &q_usb_capture_msgA
        );
    
    err = rtems_message_queue_create(
            rtems_build_name('U', 'B', 'B', 'S'),
            1,
            sizeof (void*),
            RTEMS_DEFAULT_ATTRIBUTES,
            &q_usb_msgB
        );

    err2 = rtems_message_queue_create(
            rtems_build_name('U', 'B', 'B', 'C'),
            2,
            sizeof (void*),
            RTEMS_DEFAULT_ATTRIBUTES,
            &q_usb_capture_msgB
        );

	printf("usb_initA:%d %d %d\n", err,q_usb_msgA,q_usb_capture_msgA);
	printf("usb_initB:%d %d %d\n", err,q_usb_msgB,q_usb_capture_msgB);
	flag=1;
	
	assert(pthread_create(&VSCSendAThread, NULL, VSCSendAThrd, NULL) == 0);
    assert(pthread_create(&VSCSendBThread, NULL, VSCSendBThrd, NULL) == 0);
    
    /* add for vsc */
    g_ep1_handle = vsc2_if_get(ENDPOINT2, VSC_PKG_SIZE, VSC_PKG_SIZE, FALSE, 0);
    g_ep2_handle = vsc2_if_get(ENDPOINT3, VSC_PKG_SIZE, VSC_PKG_SIZE, FALSE, 0);

}

#if 1
int usb_vsc_sendA(FrameT *frame)
{
	int err;

	err = rtems_message_queue_send(q_usb_msgA, &frame, sizeof(FrameT*));

	return err;
}

int usb_vsc_sendA_capture(FrameT *frame)
{
	int err;

	err = rtems_message_queue_send(q_usb_capture_msgA, &frame, sizeof(FrameT*));

	return err;
}

int usb_vsc_sendB(FrameT *frame)
{
	int err;

	err = rtems_message_queue_send(q_usb_msgB, &frame, sizeof(FrameT*));

	return err;
}

int usb_vsc_sendB_capture(FrameT *frame)
{
	int err;

	err = rtems_message_queue_send(q_usb_capture_msgB, &frame, sizeof(FrameT*));

	return err;
}

static void VSCSendAThrd(void)
{
	u32 msg, ret,size,cnt;
	FrameT *frame=0;
	char  *pbuffer;
	int usb_resend_count = 0;	
	
    while(1)
    {
            if(0 != (ret = rtems_message_queue_receive(q_usb_capture_msgA,&frame,&size,RTEMS_NO_WAIT, 0)))
            {
            	ret= rtems_message_queue_receive(q_usb_msgA,&frame,&size,RTEMS_NO_WAIT, 0);
            }

            if(ret == 0)
            {
            	pbuffer = frame->fbPtr[0]+ PAYLOAD_HEADER_OFFSET - PAYLOAD_HEADER_SIZE;               
    			ret = ((IF_SER*)g_ep2_handle)->if_send_req(g_ep2_handle, frame->fbPtr[0]+PAYLOAD_HEADER_OFFSET, VSC_PKG_SIZE);
    			if(RTEMS_SUCCESSFUL != ret)
                    printf("<.>VSC LOST Frame  ID(A)(0x%x) seqNO:%d !!!!!\n", frame->flgCapNo, frame->seqNo);
    			else
    				FeedWatchdog(WATCHDOG_0);

    			los_dataWasSent(frame);
            }
            else
            {
				rtems_task_wake_after(1);
/*				
				usb_resend_count++;				
				if(usb_resend_count<10)
					goto usb_send;//at most resend cap frame for 10 times
				pbuffer = 0;
*/				
            }
    }
}

static void VSCSendBThrd(void)
{
	u32 msg, ret,size,cnt;
	FrameT *frame=0;
	char  *pbuffer;
	int usb_resend_count = 0;	
	
    while(1)
    {
            if(0 != (ret = rtems_message_queue_receive(q_usb_capture_msgB,&frame,&size,RTEMS_NO_WAIT, 0)))
            {
            	ret= rtems_message_queue_receive(q_usb_msgB,&frame,&size,RTEMS_NO_WAIT, 0);
            }

            if(ret == 0)
            {
            	pbuffer = frame->fbPtr[0]+ PAYLOAD_HEADER_OFFSET - PAYLOAD_HEADER_SIZE;               
    			ret = ((IF_SER*)g_ep1_handle)->if_send_req(g_ep1_handle, frame->fbPtr[0]+PAYLOAD_HEADER_OFFSET, VSC_PKG_SIZE);
    			if(RTEMS_SUCCESSFUL != ret)
                    printf("<.>VSC LOST Frame  ID(B)(0x%x) seqNO:%d !!!!!\n", frame->flgCapNo, frame->seqNo);
    			else
    				FeedWatchdog(WATCHDOG_0);

    			los_dataWasSent(frame);
            }
            else
            {
				rtems_task_wake_after(1);	
            }
    }
}

#endif





