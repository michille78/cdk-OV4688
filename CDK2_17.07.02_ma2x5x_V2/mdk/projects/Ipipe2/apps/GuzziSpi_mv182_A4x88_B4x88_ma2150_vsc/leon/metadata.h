/*
 * metadata.h
 *
 *  Created on: 2016�?�?1�? *      Author: sunny
 */

#ifndef MDK_PROJECTS_IPIPE2_APPS_GUZZISPI_MV182_A4188M_B4188S_MA2150_LEON_METADATA_H_
#define MDK_PROJECTS_IPIPE2_APPS_GUZZISPI_MV182_A4188M_B4188S_MA2150_LEON_METADATA_H_

enum{
		CAMERA_A,
		CAMERA_B,
		CAMERB_A,
		CAMERB_B,
};/*camera number*/

enum{
		RAW_10 =10,
		RAW_12 =12,
		RAW_16 =16,
		YUV_8    = 8 ,
};/*bpp  bits per pixel*/

enum
{
		BAYER_DEF = 0,
		BAYER_GBRG = BAYER_DEF,
		BAYER_RGGB = 1,
		BAYER_BGGR = 2,
		BAYER_GRBG = 3,
		YUV422 =4,
		YUV420_I420=5,     //add by wzz
} ;    /*data format*/

typedef struct{
		int  version[3];         /*reserve byte*/
		int cameraNum;      /*camera number*/
		int width;                   /*image width*/
		int height;                  /*image height*/
		int  bpp;                      /*bpp  bits per pixel*/
		int dataFormat;       /*data format*/
		int frameCount;
		int getFrameTime;
		int sendFrameTime;
		int headerLength;
        int lastCapAFramesError;
        int lastCapBFramesError;
}S_MetaData;

//#define METE_DATA_SIZE 2688
//#define PAYLOAD_HEADER_OFFSET 16

typedef struct caputure
{	
	volatile int flgCapture;		
    volatile int capASending;
    volatile int capBSending;
	volatile int camACount;	//从触发信号时刻的camA帧计数	
	volatile int camBCount;	//从触发信号时刻的camB帧计数
	volatile int camASended;	//camA 已经发送text标记
	volatile int camBSended;	//camB 已经发送text标记
#ifdef USE_MULTISPECKLE_VERSION	
    volatile int camASpeckSended;	//camA 已经发送speck标记
	volatile int camBSpeckSended;	//camB 已经发送speck标记
#endif	
	//	pthread_mutex_t lock;
}CaptureT;


#endif /* MDK_PROJECTS_IPIPE2_APPS_GUZZISPI_MV182_A4188M_B4188S_MA2150_LEON_METADATA_H_ */
