/*
 * spi.c
 *
 */

#ifndef  _SPI_H
#define _SPI_H

#include <rtems.h>
#include <inttypes.h>
#include <mv_types.h>

#define SPI_BUS_NAME   "/dev/spi"
#define SPI_FLASH_NAME "flash"

/* SPI FLASH TEST */
#define  SPI_FLASH_TEST  1


// Flash chip specific defines
#define FLASH_CHIP_SIZE     (8 * 1024 * 1024)
#define SUBSECTOR_SIZE          (4096)

// Macro to align sizes and addresses to the flash subsector size
#define ALIGN_TO_SUBSECTOR(S)   ((((S) + (SUBSECTOR_SIZE) - 1)) & ~((SUBSECTOR_SIZE) - 1))

struct tFlashParams
{
    const char * devName;
    char * inputfname;
    char * outputfname;
    void *inBuff;   // this buffer is saved to flash
    void *outBuff;  // data from flash
    u32 offset;     // flash start address for read/write operations
    u32 size;       // size for read/write operations
    u32 imgId;      // active image - 0/1
    u32 writeImg;   // should the flash be written
};

extern struct tFlashParams flash_params;

int spi_init(void);
int spi_open(struct tFlashParams *flash_params);
int erase_spi_flash(int fd, struct tFlashParams *p);
int write_spi_flash(int fd, struct tFlashParams *p);
int read_spi_flash(int fd, struct tFlashParams *p);
void spi_test(void);
void spi_write_calib(void);
void spi_read_calib(void);
void flash_spi_init(void);
void calib_init_write(void);
void spi_series_init();

#endif
