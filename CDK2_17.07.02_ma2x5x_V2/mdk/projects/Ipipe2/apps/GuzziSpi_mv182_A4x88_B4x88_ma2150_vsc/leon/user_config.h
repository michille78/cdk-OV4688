/*
 * user_config.h
 *
 *  Created on: 2016年3月24日
 *      Author: sunny
 */

#ifndef MDK_PROJECTS_IPIPE2_APPS_GUZZISPI_MV182_A4188M_B4188S_MA2150_LEON_APP_CONFIG_H_
#define MDK_PROJECTS_IPIPE2_APPS_GUZZISPI_MV182_A4188M_B4188S_MA2150_LEON_APP_CONFIG_H_

#define USE_MIPI_TX   0
#define USE_USB_TX    1

#define INSTANCES_COUNT_MAX     2

#define IC_IMAGE_WIDTH        2688
#define IC_IMAGE_HEIGHT        1520
#define YUV_DATA_BPP      3/2
/*metaData size*/
#define METADATA_IMAGE_WIDTH    IC_IMAGE_WIDTH
#define METADATA_IMAGE_HEIGHT   2 //2
#define META_DATA_SIZE                       (METADATA_IMAGE_WIDTH*METADATA_IMAGE_HEIGHT*YUV_DATA_BPP)

/*usb data size */
#define USB_IMAGE_WIDTH     IC_IMAGE_WIDTH
#define USB_IMAGE_HEIGHT    ( IC_IMAGE_HEIGHT+METADATA_IMAGE_HEIGHT)   /*add metadata  for usb transfer*/
#define  FRAME_SIZE_BYTES   (IC_IMAGE_WIDTH*IC_IMAGE_HEIGHT*YUV_DATA_BPP)

/*usb header size*/
#define PAYLOAD_HEADER_OFFSET 16
#define PAYLOAD_HEADER_SIZE        (12)

#define  USB_SEND_BUF_SIZE   (FRAME_SIZE_BYTES + META_DATA_SIZE + PAYLOAD_HEADER_SIZE)

#define RATE_EXPOSURE 437
#define RATE_GAIN 100

#define VSC_PKG_SIZE   FRAME_SIZE_BYTES + PAYLOAD_HEADER_SIZE + 1012

#define EN 1
#define DIS 0

//watchdog sets
#define WATCHDOG_0  0
#define WATCHDOG_1  1
#define TICKS_1S 480000*1000

#ifndef CAP_SEQ_CMD_COUNT
#define CAP_SEQ_CMD_COUNT (3+1)
#endif

#define HEART_CHECK_ENABLE                  (0x3000)
#define HEART_CHECK_DISABLE                 (0x3001)
#define HEART_CHECK_SET_HEARTBEAT           (0x3002)

#define CAPCOUNTEND 1  //抓拍延迟帧计数
#define CAPSPECMAGICNUM 0xABCD
#define CAPTEXMAGICNUM 0xCDCD
#define CALIBFILEFLAG 0xCCCC

#define MODPREVIEW 0
#define MODCAPSPEC 1
#define MODCAPTEX 2

#define SPECKLE_LIGHT_OFF 0x100
#define SPECKLE_LIGHT_ON 0x101

#define CAPMOD0 0	/*拍单帧*/
#define CAPMOD1 1	/*3散斑连拍*/
#define CAPMOD2 2	/*多帧连拍，先拍3散斑，后拍纹理*/
#define CAPMOD3 3	/*多帧连拍，先拍纹理，后拍3散斑*/


#define IOSPECLIGHT2          54
#define IOSPECLIGHT3          54
#define IOSPECLIGHT4          54
#define IOTEXTLIGHT          55

#define PWMSPECLIGHT        27
#define PWMTEXTLIGHT        33

typedef unsigned char uchar;
// added by zp 2017/11/25
typedef struct
{
    short tri0_exp;  // exposure for texture image
    short tri0_gain; // gain for texture image
    short tri1_exp; // exposure for speckle image
    short tri1_gain;// gain for speckle image
    uchar  camSyncSpeckleLightLTime;
    uchar  camSyncSpeckleLightHTime;
    uchar  camSyncTextureLightLTime;
    uchar  camSyncTextureLightHTime;
    uchar  tri1_texture;// 1
    uchar  tri1_seqlen;// seckle seqence length(1~3)
    uchar  texExpGainTakeEffectTime;
    uchar  speExpGainTakeEffectTime;
}CamConfig;

// added by zp 2018/01/22
typedef struct
{
   struct{
       float r_gain;
       float g_gain;
       float b_gain;
   }left;
   struct{
       float r_gain;
       float g_gain;
       float b_gain;
   }right;
}AwbGainsConfig;

//typedef unsigned char bool;
//enum {false,true};


#endif /* MDK_PROJECTS_IPIPE2_APPS_GUZZISPI_MV182_A4188M_B4188S_MA2150_LEON_APP_CONFIG_H_ */
