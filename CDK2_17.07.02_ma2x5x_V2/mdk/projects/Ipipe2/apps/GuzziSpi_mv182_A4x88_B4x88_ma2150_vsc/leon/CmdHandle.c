/*
 ============================================================================
 Name        : CmdHandle.c
 Author      : jnzhao@sunnyoptical.com
 Version     : 2016骞�2鏈�4鏃� 涓婂崍11:27:46
 Copyright   : sunnyoptical.com
 Description :
 History     :
 ============================================================================
 */
#include "usbpump_vsc2app.h"
#include "usbpump_proto_vsc2_api.h"
#include "usbpumpapi.h"
#include "usbpumpdebug.h"
#include "usbpumplib.h"
#include "uplatformapi.h"
#include "stdio.h"
#include "string.h"
#include "CmdHandle.h"
#include <VcsHooksApi.h>
//#include "../lib/TOFAlg.h"
//#include "Board182Api.h"
#include "spi.h"
#include "DeviceInfo.h"
//#include "i2c.h"
//#include "../share/Camera.h"
#include "camera_control.h"
#include "os_adapta.h"
#include "vsc2app_if.h"
#include "user_config.h"    //add by xw

extern void CamTrigger123(int capMod);
extern void setSpeckleLight(int speckle_light_ctl);
extern void WatchdogInit(int dogID, int stat);

#define LENTHPARAM    36
extern struct tFlashParams flash_params;
extern int app_ready_flag;
extern int flg_cap_loop;
extern void *g_ep0_handle;

#ifdef USE_TOF_ALG
	extern SwitchStatus switch_en; /*TURNOFF, TURNON*/
	extern FreqSwitch switch_mode;  /*MONOFREQUENCY, MULTIFREQUENCY*/
	extern SensorConig_t *pSensorConfig;
#endif
extern char *convertCalib;
SUsbPacket m_Packet[ACK_CMD_PACKETS];
char m_PacketData[ACK_CMD_PACKETS][PACKETS_LEN] ;

char CMDBuf[ACK_LEN+1];
char ACKBuf[ACK_LEN+1];

#define Calib_Lensparam    36
SCmdList  CmdIdleList;
SCmdList  CmdReadyList;

char calibrationHead[128] = {"test: read flash"};

uint32_t uiCalibDataLen;

int InitCMDQe()
{
	int i;
	memset(&CmdIdleList,0,sizeof(SCmdList));
	memset(&CmdReadyList,0,sizeof(SCmdList));

	for(i=0;i<ACK_CMD_PACKETS;i++)
	{
		m_Packet[i].pPacket = m_PacketData[i];
		m_Packet[i].PacketSize = PACKETS_LEN;
		PutIdleCMDQe(&m_Packet[i]);
	}
	return 0;
}

//灏嗙┖闂茬紦瀛樻斁鍏ョ┖闂查槦鍒�
int PutIdleCMDQe(SUsbPacket *pUsbPacket)
{
	int next;
	if(pUsbPacket)
	{
		next = CmdIdleList.VaildPacketNum;
		CmdIdleList.pPacketList[next] = pUsbPacket;
		next++;
		CmdIdleList.VaildPacketNum = next % ACK_CMD_PACKETS;
		CmdIdleList.Size++;
		return 0;
	}
	return -1;
}

SUsbPacket* GetIdleCMDQe(void)
{
	int cur;
//	SUsbPacket *pUsbPacket;
	if(CmdIdleList.Size)
	{
		cur = CmdIdleList.CurPacket;
		SDebug("CmdIdleList.CurPacket:%d\n",CmdIdleList.CurPacket);
		CmdIdleList.CurPacket ++;
		CmdIdleList.CurPacket = CmdIdleList.CurPacket % ACK_CMD_PACKETS;
		CmdIdleList.Size--;
		return  CmdIdleList.pPacketList[cur];
	}
	return 0;
}

int PutReadyCMDQe(SUsbPacket *pUsbPacket)
{
	int next;
	if(pUsbPacket && CmdReadyList.Size < ACK_CMD_PACKETS )
	{
		next = CmdReadyList.VaildPacketNum;
		CmdReadyList.pPacketList[next] = pUsbPacket;
		SDebug("next_Ready:%d\n",next);
		next++;
		CmdReadyList.VaildPacketNum = next % ACK_CMD_PACKETS;
		CmdReadyList.Size++;
		SDebug("CmdReadyList.Size:%d\n",CmdReadyList.Size);
		return 0;
	}
	return -1;
}

SUsbPacket* GetReadyCMDQe(void)
{
	int cur;
//	SUsbPacket *pUsbPacket;
	if(CmdReadyList.Size)
	{
		cur = CmdReadyList.CurPacket;
		return CmdReadyList.pPacketList[cur];
	}
	return 0;
}

int  GetNextReadyCMDQe(void)
{
	int cur;
	if(CmdReadyList.Size)
	{
		CmdReadyList.CurPacket++;
		CmdReadyList.CurPacket = CmdReadyList.CurPacket % ACK_CMD_PACKETS;
		CmdReadyList.Size--;
//		SDebug("CmdReadyList.Size:%d, \n",CmdReadyList.Size);
//		printf("CmdReadyList.Size:%d, \n",CmdReadyList.Size);
	}
	return 0;
}

int GetReadyListSize(void)
{
	return CmdReadyList.Size;
}

/*
 * SendAck鍙戦�侀暱鍖咃紝澶т簬EP packetsize闀垮害锛岄渶瑕佸娆″彂閫�
 */
void MakeAckPacket(unsigned int uiCmd,unsigned int apiRetStatus,unsigned int * pAckBuffer)
{
	pAckBuffer[0] = uiCmd;
	pAckBuffer[1] = apiRetStatus;

	return;
}

void MakeDataPacket(char * pAckBuffer, char *pbuff, unsigned int len)
{
	memcpy(pAckBuffer,pbuff,len);
}

int GetCurQeNum(SCmdList *pCmdList)
{
	if(pCmdList==NULL)
	{
		SDebug("Buffer Error\n");
		return CMD_CMD_ERR;
	}
	return pCmdList->CurPacket;
}

extern int spi_fd;
extern u8 UsbOutputType ;
int CmdHandleFn(char *	pInPacket,int PacketLen)
{
	typedef  unsigned int  uint32_t;
	uint32_t uiCmd;
	uint32_t uiCmdUsbOutType;
    uint32_t uiPageAddr;
    uint32_t uiPages2Read;
    uint32_t uiPages2Write;
	uint32_t uiPkgId;
    uint32_t chunks;
    uint32_t uiSectorAddr;
    uint32_t uiSectors2Erase;
    uint32_t uiOffset;
    uint32_t uiLen;
    uint32_t uiRetry;
//    uint32_t uiGpioPin;
//    uint32_t uiGpioState;
    uint32_t apiRetStatus = 0;
    uint16_t i2cRegAddr;
    uint16_t i2cRegVal;
//    uint16_t temperature;
    uint32_t sensorId;
    uint32_t expValue;
    uint32_t gainValue;
    uint32_t fpsValue;
    uint32_t Imager_Mode;
    uint32_t Current_Imager_Mode;
    uint32_t capMode;

    char *pLenthParameter=NULL;
    char *pBuf;
    uint32_t flag;
    int ret = 0;
    int CurBufNum=0;

    DeviceInfo_t *pDeviceInfo = NULL;
    struct tFlashParams *p= NULL;
//    Preamble_t *pI2C_Preamble = NULL;
    SUsbPacket*	pUsbPacket;
    char * pAckPacket=NULL;

	if(pInPacket == NULL)
	{
		SDebug("Buffer Error\n");
		return CMD_CMD_ERR;
	}

	memset(CMDBuf,0,ACK_LEN);
	memcpy(&uiCmd,pInPacket,sizeof(uiCmd));
	uiCmd &=0xff;
	SDebug("uiCmd:%d\n",uiCmd);


	memcpy(&uiCmdUsbOutType ,pInPacket+sizeof(uiCmd), sizeof(uiCmdUsbOutType));
	uiCmdUsbOutType &= 0xff;
//	printf("usbtype = %d\n", uiCmdUsbOutType);

	switch(uiCmd)
	{
		case CMD_READ_FLASH:
			memcpy(&uiPageAddr,pInPacket+ sizeof(uiCmd),sizeof(uiPageAddr));
			memcpy(&uiPages2Read,pInPacket + sizeof(uiCmd) + sizeof(uiPageAddr),sizeof(uiPages2Read));
			SDebug("uiPageAddr:%d,uiPages2Read:%d\n",uiPageAddr, uiPages2Read);
//			apiRetStatus = read_spi_flash(uiPageAddr, uiPages2Read);
			pUsbPacket = GetIdleCMDQe();
			if(uiPages2Read ==1)
			{
				MakeDataPacket(pUsbPacket->pPacket, calibrationHead,  128);
				pUsbPacket->PacketSize = ACK_LEN;
			}
//			else
//			{
//				uiLen=698368;
//				loadMemFromFileSimple("./Data/1.bin", uiLen, pUsbPacket->pPacket);
//				memset(pUsbPacket->pPacket, 0xff, 100);
//				MakeDataPacket(pUsbPacket->pPacket+100, calibration,  uiLen);
//				MakeDataPacket(pUsbPacket->pPacket+100+uiLen, calibrationHead+100,  28);
//				MakeDataPacket(pUsbPacket->pPacket, calibration,  uiLen);
//				MakeDataPacket(pUsbPacket->pPacket+uiLen, calibrationHead,  128);
//				pUsbPacket->PacketSize = uiLen+ 256;
//			}
			PutReadyCMDQe(pUsbPacket);
			apiRetStatus = 0;
			pUsbPacket = GetIdleCMDQe();
			MakeAckPacket(uiCmd,(uint32_t)apiRetStatus,(unsigned int*)pUsbPacket->pPacket);
			PutReadyCMDQe(pUsbPacket);
			pUsbPacket->PacketSize = ACK_LEN;
			break;

		case CMD_ERASE_FLASH:
//			memcpy(&uiSectorAddr,pInPacket + sizeof(uiCmd),sizeof(uiSectorAddr));
//			memcpy(&uiSectors2Erase,pInPacket + sizeof(uiCmd) + sizeof(uiSectorAddr),sizeof(uiSectors2Erase));
//			apiRetStatus = EraseFlash(uiSectorAddr, uiSectors2Erase);
			apiRetStatus = 0;

			pUsbPacket = GetIdleCMDQe();
			MakeAckPacket(uiCmd,(uint32_t)apiRetStatus,(unsigned int*)pUsbPacket->pPacket);
			PutReadyCMDQe(pUsbPacket);
			pUsbPacket->PacketSize = ACK_LEN;
			break;

		case CMD_WRITE_FLASH:
//			memcpy(&uiPageAddr,pInPacket + sizeof(uiCmd),sizeof(uiPageAddr));
//			memcpy(&uiPages2Write,pInPacket + sizeof(uiCmd) + sizeof(uiPageAddr),sizeof(uiPages2Write));
//			apiRetStatus = WriteFlash(uiPageAddr, uiPages2Write);
			apiRetStatus = 0;

			pUsbPacket = GetIdleCMDQe();
			MakeAckPacket(uiCmd,(uint32_t)apiRetStatus,(unsigned int*)pUsbPacket->pPacket);
			PutReadyCMDQe(pUsbPacket);
			pUsbPacket->PacketSize = ACK_LEN;
			break;

		case CMD_WRITE_I2C:

//			uiOffset = sizeof(uiCmd);
//			memcpy(&uiLen,pInPacket + uiOffset,sizeof(uiLen));
//			uiOffset += sizeof(uiLen);
//			memcpy(&uiRetry,pInPacket + uiOffset,sizeof(uiRetry));
//			uiOffset += sizeof(uiRetry);
////			pI2C_Preamble = (Preamble_t*)(pInPacket + uiOffset);
//			pBuf= pInPacket + sizeof(I2CCMD_t);
//			i2cRegVal=(*pBuf)+(uint16_t)(*(pBuf+1)<<8);
//			SDebug("uiCmd:%d,uiLen:%d,uiRetry:%d,i2cRegVal:%d\n",uiCmd,uiLen,uiRetry,i2cRegVal);
//
//			apiRetStatus = 0;
//			pUsbPacket = GetIdleCMDQe();
//			MakeAckPacket(uiCmd,(uint32_t)apiRetStatus,(unsigned int*)pUsbPacket->pPacket);
//
//			PutReadyCMDQe(pUsbPacket);
//			pUsbPacket->PacketSize = ACK_LEN;

			break;

		case CMD_READ_I2C:

			break;

		case CMD_START_IMAGER:
//			CyU3PDmaChannelDiscardBuffer(&glChHandleUtoCPU);
//			apiRetStatus = StartFrameCapturing();
//			CyU3PThreadSleep(500); // Allow time to reset USB

//			apiRetStatus = Imager_start();
//			pUsbPacket = GetIdleCMDQe();
			MakeAckPacket(uiCmd,(uint32_t)apiRetStatus,(unsigned int*)ACKBuf);
			//////Send_Data(g_ep0_handle, ACKBuf, ACK_LEN);
//			PutReadyCMDQe(pUsbPacket);
//			pUsbPacket->PacketSize = ACK_LEN;
			UsbOutputType = uiCmdUsbOutType;
			app_ready_flag =1;
			printf("Start watch dog..\n");
//			WatchdogInit(WATCHDOG_0, EN);
			break;

		case CMD_STOP_IMAGER:
			app_ready_flag =0;
			printf("Stop watch dog..\n");
			WatchdogInit(WATCHDOG_0, DIS);
//			apiRetStatus = Imager_stop();
//			pUsbPacket = GetIdleCMDQe();
			MakeAckPacket(uiCmd,(uint32_t)apiRetStatus,(unsigned int*)ACKBuf);
			Send_Data(g_ep0_handle, ACKBuf, ACK_LEN);
//			PutReadyCMDQe(pUsbPacket);
//			pUsbPacket->PacketSize = ACK_LEN;
			break;

		case CMD_INIT_IMAGER:
//			apiRetStatus=0;
//			apiRetStatus = Imager_reset();
//			pUsbPacket = GetIdleCMDQe();
			MakeAckPacket(uiCmd,(uint32_t)apiRetStatus,(unsigned int*)ACKBuf);
			Send_Data(g_ep0_handle, ACKBuf, ACK_LEN);
//			PutReadyCMDQe(pUsbPacket);
//			pUsbPacket->PacketSize = ACK_LEN;
			break;

#ifdef HLDS_FUNCTIONS
		case CMD_ACTIVATE_LDD_ENABLE:
			CyU3PDmaChannelDiscardBuffer(&glChHandleUtoCPU);
			apiRetStatus = CyU3PGpioSetValue (LDD_ENABLE_GPIO, CyTrue);
			SendAck(uiCmd,(uint32_t)apiRetStatus);
			break;

		case CMD_DEACTIVATE_LDD_ENABLE:
			CyU3PDmaChannelDiscardBuffer(&glChHandleUtoCPU);
			apiRetStatus = CyU3PGpioSetValue (LDD_ENABLE_GPIO, CyFalse);
			SendAck(uiCmd,(uint32_t)apiRetStatus);
			break;

		case CMD_ACTIVATE_HFM:
			CyU3PDmaChannelDiscardBuffer(&glChHandleUtoCPU);
			apiRetStatus = CyU3PGpioSetValue (LDD_HFM_CONTROL_GPIO, CyTrue);
			SendAck(uiCmd,(uint32_t)apiRetStatus);
			break;

		case CMD_DEACTIVATE_HFM:
			CyU3PDmaChannelDiscardBuffer(&glChHandleUtoCPU);
			apiRetStatus = CyU3PGpioSetValue (LDD_HFM_CONTROL_GPIO, CyFalse);
			SendAck(uiCmd,(uint32_t)apiRetStatus);
			break;
#endif

		case CMD_SET_MODE:
//			apiRetStatus=0;
//			uiOffset = sizeof(uiCmd);
//			memcpy(&Imager_Mode,pInPacket + uiOffset,sizeof(Imager_Mode));
//			Current_Imager_Mode = pSensorConfig->mode;
//			printf("\ncurrent mode:%d,set mode:%d\n",Current_Imager_Mode,Imager_Mode);
//			if(Imager_Mode != Current_Imager_Mode)
//			{
//				pSensorConfig->mode = Imager_Mode;
//				Imager_stop();
//				apiRetStatus=Set_Sensor_Mode(Imager_Mode);
//				Imager_start();
//#ifdef USE_TOF_ALG
//				switch_mode=Imager_Mode;
//				switch_en = TURNON;
//#endif
//			}
			break;

		case CMD_SET_FPS:
//			uiOffset = sizeof(uiCmd);
//			memcpy(&fpsValue,pInPacket + uiOffset,sizeof(expValue));
//			printf("\n\nFPS set:%d fps\n",fpsValue);
//			app_ready_flag = 0;
//			Imager_stop();
//			apiRetStatus=Set_Sensor_FPS(fpsValue);
//			Imager_start();
//			pSensorConfig->fps =  fpsValue;
//			app_ready_flag = 1;
//			MakeAckPacket(uiCmd,(uint32_t)apiRetStatus,(unsigned int*)ACKBuf);
//			Send_Data(g_ep0_handle, ACKBuf, ACK_LEN);
//			tempRetStatus = CyU3PI2cReceiveBytes (fpsValue, tempBuffer, uiLen, uiRetry);

			break;

		case CMD_SET_EXP:
//			uiOffset = sizeof(uiCmd);
			memcpy(&sensorId,pInPacket + 4,sizeof(sensorId));
			memcpy(&expValue,pInPacket + 8,sizeof(expValue));
			printf("\n\nsensor id:%d\n",sensorId);
			printf("\n\nexposure time set:%d us\n",expValue);
			app_ready_flag = 0;
			camera_control_ae_manual_exposure(sensorId, expValue);
//			apiRetStatus=Set_Sensor_Exposure(expValue);
//			pSensorConfig->exposure = expValue;
			app_ready_flag = 1;
			MakeAckPacket(uiCmd,(uint32_t)apiRetStatus,(unsigned int*)ACKBuf);
			///////Send_Data(g_ep0_handle, ACKBuf, ACK_LEN);
			break;

		case CMD_SET_GAIN:
//			uiOffset = sizeof(uiCmd);
			memcpy(&sensorId,pInPacket + 4,sizeof(sensorId));
			memcpy(&gainValue,pInPacket + 8,sizeof(gainValue));
			printf("\n\nsensor id:%d\n",sensorId);
			printf("\n\ngain set:%d \n",gainValue);
			app_ready_flag = 0;
			camera_control_ae_manual_gain(sensorId, gainValue);
//			apiRetStatus=Set_Sensor_Exposure(expValue);
//			pSensorConfig->exposure = expValue;
			app_ready_flag = 1;
			MakeAckPacket(uiCmd,(uint32_t)apiRetStatus,(unsigned int*)ACKBuf);
			////////Send_Data(g_ep0_handle, ACKBuf, ACK_LEN);
			break;

		case CMD_GET_LENTHPARAM:
//			uiLen= LENTHPARAM;
////			pUsbPacket = GetIdleCMDQe();
//			pLenthParameter=(char*)(convertCalib+462416);
//			MakeDataPacket(CMDBuf, pLenthParameter,  uiLen);
//			printf("EP%d OUT :%x %x %x %x\n",ENDPOINT1,CMDBuf[0],CMDBuf[1],CMDBuf[2],CMDBuf[3]);
////			((IF_SER*)g_ep0_handle)->if_send_req(g_ep0_handle, CMDBuf, ACK_LEN);
//			Send_Data(g_ep0_handle, CMDBuf, ACK_LEN);
////			PutReadyCMDQe(pUsbPacket);
////			pUsbPacket->PacketSize = ACK_LEN;
//
//			apiRetStatus = 0;
////			pUsbPacket = GetIdleCMDQe();
////			MakeAckPacket(uiCmd,(uint32_t)apiRetStatus,pUsbPacket->pPacket);
////			PutReadyCMDQe(pUsbPacket);
////			pUsbPacket->PacketSize = ACK_LEN;
//			MakeAckPacket(uiCmd,(uint32_t)apiRetStatus,(unsigned int*)ACKBuf);
//			printf("EP%d OUT :%x %x %x %x\n",ENDPOINT1,ACKBuf[0],ACKBuf[1],ACKBuf[2],ACKBuf[3]);
////			((IF_SER*)g_ep0_handle)->if_send_req(g_ep0_handle, ACKBuf, ACK_LEN);
//			Send_Data(g_ep0_handle, ACKBuf, ACK_LEN);
////			free(pLenthParameter);
//			app_ready_flag = 1;
//			ret=CMD_CMD_OK;
			break;

		case CMD_GET_CONFIG:

//			Get_Sensor_Config(pSensorConfig);
//			MakeDataPacket(CMDBuf, (char *)pSensorConfig,  sizeof(SensorConig_t));
//			Send_Data(g_ep0_handle, CMDBuf, ACK_LEN);
////			free(pSensorConfig);
//
//			apiRetStatus = 0;
//			MakeAckPacket(uiCmd,(uint32_t)apiRetStatus,(unsigned int*)ACKBuf);
//			Send_Data(g_ep0_handle, ACKBuf, ACK_LEN);

			break;

		case CMD_CLOSE_CAMERA:
			app_ready_flag = 0;
			printf("Stop watch dog..\n");
			WatchdogInit(WATCHDOG_0, DIS);
			MakeAckPacket(uiCmd,(uint32_t)apiRetStatus,(unsigned int*)ACKBuf);
			////////Send_Data(g_ep0_handle, ACKBuf, ACK_LEN);
			break;

		case CMD_SEND_CALIBRATION_FILE:
			printf("Myraid2 receives calibration file\n");
			memcpy(&uiCmd,pInPacket,sizeof(uiCmd));	uiCmd &=0xff;
			memcpy(&uiPkgId,pInPacket+ sizeof(uiCmd),sizeof(uiPkgId));
			if (uiPkgId == 0){
				memcpy(&uiCalibDataLen,pInPacket+ sizeof(uiCmd)+ sizeof(uiPkgId),sizeof(uiCalibDataLen));
				if (uiCalibDataLen > flash_params.size) //calibration data file size excesses the size
					printf("!!!!!!!calibration file size is too big\n");
				memcpy(flash_params.inBuff, &uiCalibDataLen, sizeof(uiCalibDataLen));
			}else{
				//write calibration file to flash
				//有额外数据，且数据需要多次传输
				if (uiCalibDataLen > ACK_LEN){
					/* 偏移命令部分到数据部分 */
					chunks         = (uiCalibDataLen / ACK_LEN) + 1;		
				}
				pBuf = pInPacket+ sizeof(uiCmd)+ sizeof(uiPkgId);
				printf("%02d %02d %02d %02d\n",pBuf[0],pBuf[1],pBuf[2],pBuf[3]);
				memcpy(flash_params.inBuff + (uiPkgId-1)*(ACK_LEN-8) + sizeof(uiCalibDataLen), pBuf, ACK_LEN-8);
				printf("copy [%d], len = %d\n", uiPkgId, ACK_LEN-8);
				if (uiPkgId == chunks){
					spi_write_calib();
					/*MakeAckPacket(uiCmd,(uint32_t)apiRetStatus,(unsigned int*)ACKBuf);
					Send_Data(g_ep0_handle, ACKBuf, ACK_LEN);	*/				
				}
			}

			break;

		case CMD_READ_CALIBRATION_FILE:
			printf("Myraid2 sends calibration file\n");
			// read calibration file from flash
			apiRetStatus = 0;//read_spi_flash(spi_fd, &flash_params);
			spi_read_calib();

			memcpy(&uiCalibDataLen, flash_params.outBuff,  sizeof(uiCalibDataLen));
			printf("uiCalibDataLen = %d\n",uiCalibDataLen);
			char  *p_read = &uiCalibDataLen;
			printf("%02x %02x %02x %02x\n",p_read[0],p_read[1],p_read[2],p_read[3]);

			//有额外数据，且数据需要多次传输
			chunks = 0;
			if (uiCalibDataLen > ACK_LEN){
			    /* 偏移命令部分到数据部分 */
			    chunks         = (uiCalibDataLen / ACK_LEN) + 1;		
			}

			for (int i = 0; i < chunks; ++i) //send calib data
			{
			    memcpy(ACKBuf, flash_params.outBuff /*+ sizeof(uiCalibDataLen)*/ + i*ACK_LEN, ACK_LEN);
			    Send_Data(g_ep0_handle, ACKBuf, ACK_LEN);
			}

			/*MakeAckPacket(uiCmd,(uint32_t)apiRetStatus,(unsigned int*)ACKBuf); //ACK
			//memcpy(ACKBuf + 8, flash_params.outBuff,  flash_params.size);
			Send_Data(g_ep0_handle, ACKBuf, ACK_LEN);*/
			break;

		case CMD_GET_DEVICEINFO:
			pUsbPacket = GetIdleCMDQe();
			pDeviceInfo =   (DeviceInfo_t *)malloc(sizeof(DeviceInfo_t));
			GetDeviceInfo(pDeviceInfo);
			MakeDataPacket(CMDBuf,(char *)pDeviceInfo,  sizeof(DeviceInfo_t));
			Send_Data(g_ep0_handle, CMDBuf, ACK_LEN);
			PutReadyCMDQe(pUsbPacket);
			pUsbPacket->PacketSize = ACK_LEN;
			free(pDeviceInfo);
			apiRetStatus = 0;
			pUsbPacket = GetIdleCMDQe();
			MakeAckPacket(uiCmd,(uint32_t)apiRetStatus,(unsigned int*)ACKBuf);
			Send_Data(g_ep0_handle, ACKBuf, ACK_LEN);
			PutReadyCMDQe(pUsbPacket);
			pUsbPacket->PacketSize = ACK_LEN;
		break;

		case CMD_CAPTURE_IMAGES:
			memcpy(&capMode ,pInPacket+sizeof(uiCmd), sizeof(capMode));
			printf("capMode:0x%x \n", capMode);
			capMode = 0x1F;
			if (capMode < 0x100){
                CamTrigger123(capMode);
			}
			else if((capMode>=0x100) && (capMode<0x200)){
				setSpeckleLight(capMode);
			}
			apiRetStatus = 0;
			MakeAckPacket(uiCmd,(uint32_t)apiRetStatus,(unsigned int*)ACKBuf);
			///////Send_Data(g_ep0_handle, ACKBuf, ACK_LEN);
		break;

		case CMD_CAPTURE_LOOP_TEST:
			flg_cap_loop = 1;
		break;


		default:
//			CyU3PDmaChannelDiscardBuffer(&glChHandleUtoCPU);
			ret = CMD_NOTNEED_ACK;
			break;
	}
	return ret;
}

///////////////////////////////////////////////////////////////////////////////












