///
/// @file
/// @copyright All code copyright Movidius Ltd 2012, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     main leon file
///

// 1: Includes
// ----------------------------------------------------------------------------
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <rtems.h>
#include <fcntl.h>
#include <mv_types.h>
#include <rtems/fsmount.h>
#include <rtems/bdpart.h>
#include <OsDrvCpr.h>
#include <OsDrvSdio.h>
#include <OsBrdMv0182.h>
#include <DrvGpio.h>
#include <assert.h>
//#include "rtems_config.h"
#include <rtems/dosfs.h>

#include <OsDrvTimer.h>
//#ifdef MMC_BUILD
#include <GpioConfig.h>
//#endif

// Perform an MMC / SDcard format
#define FORMAT_CARD
//#define MMC_BUILD
// 2:  Source Specific #defines and types  (typedef, enum, struct)
// ----------------------------------------------------------------------------
#define BUFFER_SIZE                                 (1024*1024*5)
#define DEFAULT_SDIO_INT_PRIORITY                   10
#define SDIO_SLOT_USED                              0
#define SDIO_DEVNAME_USED                           "/dev/sdc0"

// 3: Global Data (Only if absolutely necessary)
// ----------------------------------------------------------------------------

// 4: Static Local Data
// ----------------------------------------------------------------------------
/* Sections decoration is require here for downstream tools */
/* Mount Table */
static const rtems_fstab_entry fs_table [] = {
   {
     .source = "/dev/sdc0",
     .target = "/mnt/sdcard",
     .type = "dosfs",
     .options = RTEMS_FILESYSTEM_READ_WRITE,
     .report_reasons = RTEMS_FSTAB_NONE,
     .abort_reasons = RTEMS_FSTAB_OK
    }/*, 
	{
     .source = "/dev/sdc01",
     .target = "/mnt/sdcard",
     .type = "dosfs",
     .options = RTEMS_FILESYSTEM_READ_WRITE,
     .report_reasons = RTEMS_FSTAB_NONE,
     .abort_reasons = RTEMS_FSTAB_NONE
    } */
};
/* Read/Write buffers */
//static u8 write_buffer[BUFFER_SIZE];
//static u8 read_buffer[BUFFER_SIZE];


// 5: Static Function Prototypes
// ----------------------------------------------------------------------------
void *sdCardExample(void *arg);
void *sdCardExample_test(void *arg);

// 6: Functions Implementation
// ----------------------------------------------------------------------------

#ifdef FORMAT_CARD

#define NB_OF_PARTITIONS        1

static const msdos_format_request_param_t rqdata = {
/* Optimized for read/write speed */
    .OEMName             = NULL,
    .VolLabel            = NULL,
    .sectors_per_cluster = 64,
    .fat_num             = 0,
    .files_per_root_dir  = 0,
    .media               = 0,
    .quick_format        = true,
    .skip_alignment      = false,
    .sync_device         = false
};

static const rtems_bdpart_format requested_format = {
  .mbr = {
    .type = RTEMS_BDPART_FORMAT_MBR,
    .disk_id = 0xdeadbeef,
    .dos_compatibility = true
  }
};

static const unsigned distribution [NB_OF_PARTITIONS] = {1};

static rtems_bdpart_partition created_partitions [RTEMS_BDPART_PARTITION_NUMBER_HINT];

// Allows to create a partition on the card in case a bare-metal test has overwritten the partition table
static void create_partition(const char *disk,uint32_t partitions)
{
    uint32_t i;
    rtems_status_code status;

    // Initialize partitions types
    for (i = 0; i < partitions; i ++) {
        rtems_bdpart_to_partition_type( RTEMS_BDPART_MBR_FAT_32, created_partitions[i].type );
    }
    // Create 1 partition on sdc0
    status = rtems_bdpart_create(disk, &requested_format, &created_partitions[0], &distribution [0], partitions );
    assert(status == 0);
    //physically write the partition down to media
    status = rtems_bdpart_write(disk, &requested_format, &created_partitions[0], partitions );
    assert(status == 0);
}

#endif

void emmc_sdio_init(void)
{
    pthread_attr_t attr;	
    int status, rc1;
    pthread_t thread1;  
    rtems_status_code sc;

    swcLeonSetPIL(0);
	// Provide info to the sd card driver
    osDrvSdioEntries_t info = { 1,  // Number of slots
                                DEFAULT_SDIO_INT_PRIORITY, // Interrupt priority
                               {{SDIO_SLOT_USED, // Card slot to be used
                                SDIO_DEVNAME_USED}}}; // Device name  
    // Init Clocks 
    //initClocksAndMemory();
    OsDrvTimerInit();

//#ifndef MMC_BUILD // passed from Makefile
    // Init board driver         
//#else
    GpioConfig();
//#endif

    // Init the Sdio Driver
    printf("\nSdio driver initialising \n");
    status = OsDrvSdioInit(&info);
    printf("\nOsDrvSdioInit sc %s \n", rtems_status_text(status));
	
#ifdef FORMAT_CARD
    // Create partition before formatting it
    create_partition(SDIO_DEVNAME_USED, NB_OF_PARTITIONS);
#endif

    sc = rtems_bdpart_register_from_disk(SDIO_DEVNAME_USED);	
    printf("\nrtems_bdpart_register_from_disk sc %s \n", rtems_status_text(sc));
	
#ifdef FORMAT_CARD
    status = msdos_format( "/dev/sdc0", &rqdata );
    printf("\nFormatted the SD card (status = %d) \n", status);
    assert(status == 0);
#endif

#ifdef FORMAT_CARD
    // Mount the dosfs(write)
    sc = rtems_fsmount( fs_table, sizeof(fs_table)/sizeof(fs_table[0]), NULL); 
    printf("\nMounting File System %s \n", rtems_status_text(sc));

	sdCardExample(NULL);
#else
    // Mount the dosfs(read)
    sc = rtems_fsmount( fs_table, sizeof(fs_table)/sizeof(fs_table[0]), NULL); 
    printf("\nagain Mounting File System %s \n", rtems_status_text(sc));

	//sdCardExample_test(NULL);
#endif

    return;
}

static void fillInBuffers(u8 *write_buffer, u8 *read_buffer)
{
	int i = 0;

	for (i = 0; i < BUFFER_SIZE; i++)
	{
		// This buffer should contain data
		*(write_buffer + i) = i;
		// This buffer should be empty
		*(read_buffer + i) = 0;
	}	
}

void printSpeed(u64 tickCount, u32 sizeBytes)
{
    double time_ms = DrvTimerTicksToMs(tickCount);
    double time_s = time_ms / 1000.0;
    double sizeMB  = (double)sizeBytes / 1000.0 / 1000.0;
    double sizeMiB = (double)sizeBytes / 1024.0 / 1024.0;
    printf("\tSize: %7.3f\n\ttimeMS %9.3f\n\tspeed%8.3f\n\tspeed %8.3f,",
            sizeMiB, time_ms, sizeMB/time_s, sizeMiB/time_s);
}

void *sdCardExample(void *arg)
{
    UNUSED(arg);
int i = 0;
	int fd = 0;
	rtems_status_code sc;	
    const char file[] = "/mnt/sdcard/myfile";
	u8 *write_buffer = (u8 *)malloc(BUFFER_SIZE);
	u8 *read_buffer = (u8 *)malloc(BUFFER_SIZE);

    u64 ticks1,ticks2,ticks3,ticks4;
	// Init buffers
	fillInBuffers(write_buffer, read_buffer);		

	// Try to create the file if does not exist
	printf("\nCreating file %s\n", file);
    fd = creat(file, S_IRWXU | S_IRWXG | S_IRWXO ) ;
	assert(fd);	
	close(fd);

    fd = open(file, O_RDWR);
    assert(fd);

	printf("\nWriting %d bytes to file\n", BUFFER_SIZE);
	OsDrvTimerGetSystemTicks64(&ticks1);
	sc = write(fd, write_buffer, BUFFER_SIZE);
	OsDrvTimerGetSystemTicks64(&ticks2);
	printSpeed(ticks2 - ticks1, BUFFER_SIZE);
	assert(sc);
		

	printf("\nPerform fsync\n");
    OsDrvTimerGetSystemTicks64(&ticks3);
	sc = fsync(fd);
    OsDrvTimerGetSystemTicks64(&ticks4);
    printSpeed(ticks2 - ticks1 + ticks4 - ticks3, BUFFER_SIZE);
	assert((sc == 0));
	
	printf("\nClosing file\n");
	sc = close(fd);
	assert((sc == 0));

	// Validate written data
	printf("\nOpening file %s \n", file);
	fd = open("/mnt/sdcard/myfile", O_RDWR);
	printf("\n fd = %d\n",fd);
	assert(fd);
	
	printf("\nRead %d characters\n", BUFFER_SIZE);
    OsDrvTimerGetSystemTicks64(&ticks1);
	sc = read(fd, read_buffer, BUFFER_SIZE);
    OsDrvTimerGetSystemTicks64(&ticks2);
    printSpeed(ticks2 - ticks1, BUFFER_SIZE);
	assert(sc);
	
	sc = close(fd);
	// Check Values of read and written data
	printf("\nVerifying data...\n");
			
	assert(memcmp(read_buffer, write_buffer, BUFFER_SIZE) == 0);

	free(write_buffer);
	write_buffer = NULL;
	free(read_buffer);
	read_buffer = NULL;
	
	// Exit thread
	// pthread_exit(0);
    return NULL; // just so the compiler thinks we returned something
}

void *sdCardExample_test(void *arg)
{
    UNUSED(arg);
int i = 0;
	int fd = 0;
	rtems_status_code sc;	
    const char file[] = "/mnt/sdcard/myfile";

    u64 ticks1,ticks2,ticks3,ticks4;
	
	u8 *write_buffer = (u8 *)malloc(BUFFER_SIZE);
	u8 *read_buffer = (u8 *)malloc(BUFFER_SIZE);
	
	// Init buffers
	fillInBuffers(write_buffer, read_buffer);		

	// Validate written data
	printf("\nOpening file %s \n", file);
	fd = open("/mnt/sdcard/myfile", O_RDWR);
	printf("\n fd = %d\n",fd);
	assert(fd);
	
	printf("\nRead %d characters\n", BUFFER_SIZE);
    OsDrvTimerGetSystemTicks64(&ticks1);
	sc = read(fd, read_buffer, BUFFER_SIZE);
    OsDrvTimerGetSystemTicks64(&ticks2);
    printSpeed(ticks2 - ticks1, BUFFER_SIZE);
	assert(sc);
	
	sc = close(fd);
	// Check Values of read and written data
	printf("\nVerifying data...\n");
			
	for(i = 0; i < 100; i++)
	{
		printf("\n read buff[%d] = %d\n",i, read_buffer[i]);
	}

	assert(memcmp(read_buffer, write_buffer, BUFFER_SIZE) == 0);
	
	free(write_buffer);
	write_buffer = NULL;
	free(read_buffer);
	read_buffer = NULL;	
	// Exit thread
	// pthread_exit(0);
    return NULL; // just so the compiler thinks we returned something
}


u32 emmc_test_read_write(void)
{
	u32 err_flag;
	int fd = 0;
	rtems_status_code sc;	
    const char file[] = "/mnt/sdcard/testfile";

    u64 ticks1,ticks2,ticks3,ticks4;
	
	u8 *write_buffer = (u8 *)malloc(BUFFER_SIZE);
	if (write_buffer == NULL)
	{
		err_flag = -1;
		return err_flag;
	}
	u8 *read_buffer = (u8 *)malloc(BUFFER_SIZE);
	if (read_buffer == NULL)
	{
		err_flag = -1;
		return err_flag;
	}
	// Init buffers
	fillInBuffers(write_buffer, read_buffer);	

	// Try to create the file if does not exist
	printf("\nCreating file %s\n", file);
    fd = creat(file, S_IRWXU | S_IRWXG | S_IRWXO ) ;
	if(fd == 0)
	{
		err_flag = 1;
		return err_flag;
	}	
	
	close(fd);

    fd = open(file, O_RDWR);

	printf("\nWriting %d bytes to file\n", BUFFER_SIZE);
	OsDrvTimerGetSystemTicks64(&ticks1);
	sc = write(fd, write_buffer, BUFFER_SIZE);
	OsDrvTimerGetSystemTicks64(&ticks2);
	printSpeed(ticks2 - ticks1, BUFFER_SIZE);
	if(sc == 0)
	{
		err_flag = 2;
		return err_flag;
	}	
		

	printf("\nPerform fsync\n");
    OsDrvTimerGetSystemTicks64(&ticks3);
	sc = fsync(fd);
    OsDrvTimerGetSystemTicks64(&ticks4);
    printSpeed(ticks2 - ticks1 + ticks4 - ticks3, BUFFER_SIZE);
	if(sc != 0)
	{
		err_flag = 3;
		return err_flag;
	}	
	
	printf("\nClosing file\n");
	sc = close(fd);
	if(sc != 0)
	{
		err_flag = 4;
		return err_flag;
	}	

	// Validate written data
	printf("\nOpening file %s \n", file);
	fd = open("/mnt/sdcard/myfile", O_RDWR);
	printf("\n fd = %d\n",fd);
	
	printf("\nRead %d characters\n", BUFFER_SIZE);
    OsDrvTimerGetSystemTicks64(&ticks1);
	sc = read(fd, read_buffer, BUFFER_SIZE);
    OsDrvTimerGetSystemTicks64(&ticks2);
    printSpeed(ticks2 - ticks1, BUFFER_SIZE);
	if(sc == 0)
	{
		err_flag = 5;
		return err_flag;
	}	
	
	sc = close(fd);
	// Check Values of read and written data
	printf("\nVerifying data...\n");
			
	sc = memcmp(read_buffer, write_buffer, BUFFER_SIZE);
	if(sc != 0)
	{
		err_flag = 6;
		return err_flag;
	}	

	free(write_buffer);
	write_buffer = NULL;
	free(read_buffer);
	read_buffer = NULL;
	
    return 0; // no error
}


