#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <string.h>
#include <mv_types.h>

#include "fathomRun.h"
#include "utils.h"
#include <Fp16Convert.h>
#include <malloc.h>

#include "interpret_output.h"



#ifndef DDR_DATA
#define DDR_DATA  __attribute__((section(".ddr.data")))
#endif 
#ifndef DDR_BSS
#define DDR_BSS  __attribute__((section(".ddr.bss")))
#endif
#ifndef DMA_DESCRIPTORS_SECTION
#define DMA_DESCRIPTORS_SECTION __attribute__((section(".cmx.cdmaDescriptors")))
#endif

#define OUTPUTSIZE 2*1470
#define F_BSS_SIZE 40000000

const unsigned int cache_memory_size = 25 * 1024 * 1024;
const unsigned int scratch_memory_size = 110 * 1024;

char DDR_BSS cache_memory[cache_memory_size];
char __attribute__((section(".cmx.bss"))) scratch_memory[scratch_memory_size];

dmaTransactionList_t DMA_DESCRIPTORS_SECTION task[1];

cvector cvpeople=cvector_create(sizeof(tracking_msg));


u8 DDR_BSS fathomBSS[F_BSS_SIZE] __attribute__((aligned(64)));
u8 DDR_DATA fathomOutput[OUTPUTSIZE] __attribute__((aligned(64)));



static float *cmpdata;
int indexcmp(const void *a1, const void *b1)
{
	int *a = (int *)a1;
	int *b = (int *)b1;
	float diff = cmpdata[*b] - cmpdata[*a];
	if(diff < 0)
		return -1;
	else if(diff > 0)
		return 1;
	else return 0;
}


/*
extern "C"
void get_bbox_from_result(float *input_data,
			int data_lenth,
			int insize_w,
			int insize_h,
			int side,
			int num_class,
			int num_object,
			int *bbox_x,
			int *bbox_y,
			int *bbox_w,
			int *bbox_h,
			float *max_scale)
{
	int locations = side*side;
	float max_cale = 0;                  //每个格子的最大定位概率
	float max_box_x = 0;                 //每个格子的最大定位概率框坐标
	float max_box_y = 0;
	float max_box_w = 0;
	float max_box_h = 0;
	float max_cale_label=0;
	float max_cale_prob=0;
	for (int i = 0; i < locations; ++i)                        // =====================遍历7*7个小格子==========================
	{
		int pred_label = 0;                //每个格子的分类标签
		float max_prob = input_data[i];    //每个格子的最大概率

	
		for (int j = 1; j < num_class; ++j)                            // 1.找140类中最大的概率和类别
		{
			int class_index = j * locations + i;   
			if (input_data[class_index] > max_prob) 
			{
				pred_label = j;
				max_prob = input_data[class_index];
			}
		}

		int obj_index = num_class * locations + i;
		int coord_index = (num_class + num_object) * locations + i;
    
		float pred_box_x = 0;              //每个格子的最大定位概率框坐标
		float pred_box_y = 0;
		float pred_box_w = 0;
		float pred_box_h = 0;	
		for (int k = 0; k < num_object; ++k)                          // 2.两个框
		{
			float scale = input_data[obj_index + k * locations];

			int box_index = coord_index + k * 4 * locations;

			pred_box_x= (i % side + input_data[box_index + 0 * locations])*insize_w / side ;
			pred_box_y= (i / side + input_data[box_index + 1 * locations])*insize_h / side ;
			float w = input_data[box_index + 2 * locations];
			float h = input_data[box_index + 3 * locations];
			pred_box_w=w*w*insize_w;
			pred_box_h=h*h*insize_h;



			if(scale>max_cale)
			{
				max_cale=scale;
				max_box_x = pred_box_x;                       //每个格子的最大定位概率框坐标
				max_box_y = pred_box_y;
				max_box_w = pred_box_w;
				max_box_h = pred_box_h;
				max_cale_prob=max_prob;
				max_cale_label=pred_label;
			}
		}//for (int k = 0; k < num_object; ++k)

	}// for (int i = 0; i < locations; ++i)
       
	*max_scale=max_cale;
	*bbox_x=(int)max_box_x;
	*bbox_y=(int)max_box_y;
	*bbox_w=(int)max_box_w;
	*bbox_h=(int)max_box_h;

}

 void print_det_result(void* fathomOutput)
 {
 
 u16* probabilities = (u16*)fathomOutput;
        float max_cale = 0;                  //每个格子的最大定位概率
	int max_box_x = 0;                 //每个格子的最大定位概率框坐标
	int max_box_y = 0;
	int max_box_w = 0;
	int max_box_h = 0;
	int resultlen=7350;
	int reqsize=448;
	
	float *resultfp32;
	resultfp32=(float*)malloc(resultlen * sizeof(*resultfp32));
	
	  //float resultfp32[7350];
  
  for (u32 i = 0; i < resultlen; i++)
     resultfp32[i]= f16Tof32(probabilities[i]);
get_bbox_from_result(resultfp32,resultlen,reqsize,reqsize,7,140,2,&max_box_x,&max_box_y,&max_box_w,&max_box_h,&max_cale);
printf("-------------------------------------------max_cale:%f,max_box_x:%d,max_box_y:%d,max_box_w:%d,max_box_h:%d\n",max_cale,max_box_x,max_box_y,max_box_w,max_box_h);

free(resultfp32);
 printf("free succeed\n");
 return;
 }




extern "C"
void print_googleNet_classify_result(void *fathomOutput)
{
    u16* probabilities = (u16*)fathomOutput;
	

 //   printf("Debug messages from MvTensor:\n%s", debugMsg);
    printf("\nClassification probabilities:\n");
    
    
    int resultlen=140;
    float resultfp32[140];
    for (u32 i = 0; i < resultlen; i++)
     resultfp32[i]= f16Tof32(probabilities[i]);

	int *indexes;
	indexes = (int*)malloc(sizeof(*indexes) * resultlen);
		
	for(u32 i = 0; i < (int)resultlen; i++)
		indexes[i] = i;
	cmpdata = resultfp32;
	qsort(indexes, resultlen, sizeof(*indexes), indexcmp);
	for(u32 i = 0; i < 5 && i < resultlen; i++)
		printf("%d (%.2f%%) \n",indexes[i], resultfp32[indexes[i]] * 100.0);
    
	free(indexes);

}*/
//分析yolo模型的数据结果
extern "C"
void print_yolo_result(void *fathomOutput)
{
    u16* probabilities = (u16*)fathomOutput;
	

//    printf("Debug messages from MvTensor:\n%s", debugMsg);
    printf("\nClassification probabilities:\n");
    
    
  unsigned int resultlen=1470;
  YOLO_Result result[20];
  int result_num=0;
  float*resultfp32;
  resultfp32=(float*)malloc(resultlen * sizeof(*resultfp32));
  int img_width=648;
  int img_height=486;
   
  image im={img_width,img_height};
  
  for (u32 i = 0; i < resultlen; i++)
     resultfp32[i]= f16Tof32(probabilities[i]);
     
  interpret_output(resultfp32,result, &result_num, img_width, img_height,0.2);   
  for (int i = 0; i < result_num; ++i)
  { 
     printf("class:%s, x:%d, y:%d, width:%d, height:%d, probability:%.2f.\n",result[i].category,result[i].x,result[i].y,result[i].width,result[i].height,result[i].probability);
  }
  draw_detections(im, result_num, 0.2, result);
  printf("it's end!!\n"); 
}

extern "C"
void Classify_Test(unsigned char *blob, void *inTensor)
{
    u32 FathomBlobSizeBytes = *(u32*)&blob[BLOB_FILE_SIZE_OFFSET];
    u8* timings = NULL;
    u8* debugBuffer = NULL;
  
  
    FathomRunConfig config =
    {
        .fathomBSS = fathomBSS,
        .fathomBSS_size = 1000,
        .dmaLinkAgent = 0,
        .dataPartitionNo = 0,
        .instrPartitionNo = 1,
        .dmaTransactions = &task[0]
    };

    bzero(fathomBSS, sizeof(fathomBSS));


    FathomRun(blob, (u32)FathomBlobSizeBytes, inTensor, fathomOutput, &config, timings, debugBuffer,
		cache_memory_size, scratch_memory_size, cache_memory, scratch_memory, 1, 1);

    print_yolo_result(fathomOutput);
}

