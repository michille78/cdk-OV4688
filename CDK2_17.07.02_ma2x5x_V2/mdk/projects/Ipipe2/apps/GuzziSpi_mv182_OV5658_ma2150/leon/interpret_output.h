///
/// @file
/// @copyright All code copyright Movidius Ltd 2012, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Application configuration Leon header
///


#ifndef _INTERPRET_OUTPUT_H_
#define _INTERPRET_OUTPUT_H_

#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <string.h>
#include <mv_types.h>
#include <assert.h>

# define MIN_LEN 256
# define CVEFAILED  -1
# define CVESUCCESS  0
# define CVEPUSHBACK 1
# define CVEPOPBACK  2
# define CVEINSERT   3
# define CVERM       4
# define EXPANED_VAL 1
# define REDUSED_VAL 2

#define FALSE 0
#define TRUE 1

#define GOING_IN      1
#define GOING_OUT    2
#define UPLINEX 250 //  in this line ,we  add people point
#define UPLINEY 85

#define OUTTRIPWIREX 400  
#define OUTTRIPWIREY 260

#define TRIPWIREX 285
#define TRIPWIREY 360
#define SHAVES_USED 12



typedef struct _Point
{
        int x ;
        int y ;
} Point;

typedef struct{
        int left;
        int right;
        int top;
        int bottom;
} box_people;


typedef struct _TRACKS{
        int tracksize;
        Point point[500];
} tracks;


typedef struct _YOLO_Result
{
	char* category[20];
	int class_idx;
	int x;
	int y;
	int width;
	int height;
	float probability;
}YOLO_Result;

typedef struct _image
{
	int w;
	int h;
} image;

typedef struct tracking{
        int id ;
        int age;
        int x;
        int y;
        int dir;
        int state;
        int done;
        box_people now_box;
        tracks list;
        float prob;
} tracking_msg;


typedef struct _cvector
{
        tracking_msg *cv_pdata;   //记录每个person的状态数据
        int cv_len, cv_tot_len, cv_size;  //cv_len表示的是有多少个这样的单位长度，cv_size表示的是数据单位长度//
}cvector;
typedef void* citerator;



#ifdef __cplusplus
extern "C" {
#endif
void fp16tofloat(float *dst, unsigned char *src, unsigned nelem);
void interpret_output(float *original_out, YOLO_Result *result, int * result_num, int img_width, int img_height,float threshold);
void draw_detections(image im, int num, float thresh, YOLO_Result *result);

cvector cvector_create(int size);
int going_OUT(tracks person);
int going_IN(tracks person);
int  point_out(Point   p);
int point_in(Point   p);

int cvector_refresh(cvector cv, int index , tracking_msg memb);
int cvector_iter_val(cvector cv, tracking_msg iter, tracking_msg memb);
int cvector_iter_at(cvector cv, tracking_msg iter);

int cvector_rm_at(cvector cv, int index);
int cvector_rm(cvector cv, tracking_msg iter,int index);
int cvector_val_at(cvector cv, int index, tracking_msg memb);

tracking_msg cvector_end(cvector cv);
tracking_msg cvector_begin(cvector cv);
tracking_msg cvector_next(cvector cv, tracking_msg iter);

int cvector_pushback(cvector cv, tracking_msg memb);
void cvector_destroy( cvector cv);
int cvector_length(cvector cv);
#ifdef __cplusplus
}
#endif

#endif

