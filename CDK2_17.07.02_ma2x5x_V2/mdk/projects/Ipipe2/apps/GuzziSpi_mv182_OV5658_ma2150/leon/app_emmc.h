
/// @file
/// @copyright All code copyright Movidius Ltd 2012, all rights reserved
///            For License Warranty see: common/license.txt
///
/// @brief
///
///


#ifndef _APP_EMMC_H_
#define _APP_EMMC_H_

void emmc_sdio_init(void);
u32 emmc_test_read_write(void);


#endif
