///   
/// @file
/// @copyright All code copyright Movidius Ltd 2012, all rights reserved. 
///            For License Warranty see: common/license.txt   
///
/// @brief
/// 
/// 
/// 

// 1: Includes
// ----------------------------------------------------------------------------
#include <registersMyriad.h>
#include <stdio.h>
#include <DrvGpio.h>
#include <DrvRegUtils.h>
#include "GpioConfig.h"

int GpioConfig()
{
    #ifndef D_GPIO_PAD_DRIVE_12mA
    #define D_GPIO_PAD_DRIVE_12mA D_GPIO_PAD_DRIVE_8mA
    #endif

    // If running on MA2x5x and VDDIO_B (for GPIOs 16..21) is
    // powered from 1.8V, we need to set the AON_RETENTION0.23 bit
    #define VDDIO_B_POWERED_FROM_1v8   1  // 0 - 3.3V;  1 - 1.8V

    #if VDDIO_B_POWERED_FROM_1v8 && !defined(MA2100)
    SET_REG_BITS_MASK(AON_RETENTION0_ADR, 1 << 23);
    #endif

    const u32 GPIO_PAD_OPTS_OUT_CLK = 0
            | D_GPIO_PAD_DRIVE_12mA  // configurable: 2mA, 4mA, 8mA, 12mA
          //| D_GPIO_PAD_SLEW_FAST   // configurable
            ;
    const u32 GPIO_PAD_OPTS_OUT_CMD_DAT = 0
            | D_GPIO_PAD_DRIVE_12mA  // configurable: 2mA, 4mA, 8mA, 12mA
          //| D_GPIO_PAD_SLEW_FAST   // configurable
            ;
    const u32 GPIO_PAD_OPTS_IN_OUT = 0
            | GPIO_PAD_OPTS_OUT_CMD_DAT
            | D_GPIO_PAD_RECEIVER_ON
//          | D_GPIO_PAD_SCHMITT_ON  // configurable
            ;
    const u32 CLK_MODE_OPTS = 0
//          | D_GPIO_DATA_INV_ON     // configurable
            ;

    // Configure GPIOs accordingly
    // If operating in 4 bit-mode, DO NOT configure DAT[7:4] in SDIO mode!
    DrvGpioModeRange(63, 72, D_GPIO_MODE_2);
    DrvGpioSetMode(63, D_GPIO_MODE_2 | CLK_MODE_OPTS); // clk
    DrvGpioPadSet(63, GPIO_PAD_OPTS_OUT_CLK); // sd_clk

    DrvGpioPadSet(64, GPIO_PAD_OPTS_IN_OUT | D_GPIO_PAD_PULL_UP);  // sd_cmd    -in-out
    DrvGpioPadSet(65, GPIO_PAD_OPTS_IN_OUT | D_GPIO_PAD_PULL_UP);  // sd_dat_0  -in-out
    DrvGpioPadSet(66, GPIO_PAD_OPTS_IN_OUT | D_GPIO_PAD_PULL_UP);  // sd_dat_1 - in-out
    DrvGpioPadSet(67, GPIO_PAD_OPTS_IN_OUT | D_GPIO_PAD_PULL_UP);  // sd_dat_2  -in-out
    DrvGpioPadSet(68, GPIO_PAD_OPTS_IN_OUT | D_GPIO_PAD_PULL_UP);  // sd_dat_3  -in-out
    DrvGpioPadSet(69, GPIO_PAD_OPTS_IN_OUT | D_GPIO_PAD_PULL_UP);  // sd_dat_4  -in-out
    DrvGpioPadSet(70, GPIO_PAD_OPTS_IN_OUT | D_GPIO_PAD_PULL_UP);  // sd_dat_5  -in-out
    DrvGpioPadSet(71, GPIO_PAD_OPTS_IN_OUT | D_GPIO_PAD_PULL_UP);  // sd_dat_6  -in-out
    DrvGpioPadSet(72, GPIO_PAD_OPTS_IN_OUT | D_GPIO_PAD_PULL_UP);  // sd_dat_8  -in-out

	/*add by guanxing gpio79为reset管脚*/
	DrvGpioSetMode(79, D_GPIO_DIR_OUT | D_GPIO_MODE_7); 
	DrvGpioPadSet(79, D_GPIO_PAD_DEFAULTS | D_GPIO_PAD_DRIVE_12mA | D_GPIO_PAD_PULL_UP); 
	DrvGpioSetPinHi(79);
	
	/*add by guanxing gpio1为led灯管脚，长亮*/
	DrvGpioSetMode(1, D_GPIO_DIR_OUT | D_GPIO_MODE_7); 
	DrvGpioPadSet(1, D_GPIO_PAD_DEFAULTS | D_GPIO_PAD_DRIVE_12mA); 
	DrvGpioSetPinHi(79);


    return 0;
}
