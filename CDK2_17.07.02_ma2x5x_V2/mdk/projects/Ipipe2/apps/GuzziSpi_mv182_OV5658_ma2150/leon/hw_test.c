///
/// @file
/// @copyright Deepano Guanxing
///            
///
/// @brief     
///

// 1: Includes
// ----------------------------------------------------------------------------
#include <registersMyriad.h>
#include <stdio.h>
#include <rtems.h>
#include <DrvGpio.h>
#include <pthread.h>

#include "app_emmc.h"
#include "vsc2app_outcall.h"

// 2:  Source Specific #defines and types  (typedef,enum,struct)
// ----------------------------------------------------------------------------

// 3: Global Data (Only if absolutely necessary)

// ----------------------------------------------------------------------------
// Sections decoration is required here for downstream tools

// 4: Static Local Data
// ----------------------------------------------------------------------------
static pthread_t led_thread;


// 5: Static Function Prototypes
// ----------------------------------------------------------------------------

// Required for synchronisation between internal USB thread and our threads

// 6: Functions Implementation
// ----------------------------------------------------------------------------

static void system_led_on(void)
{
	DrvGpioSetPinHi(1);
}

static void system_led_off(void)
{
	DrvGpioSetPinLo(1);
}

void *system_led_blink_thread(void *arg)
{
    UNUSED(arg);
	u32 i = 0;
	for(i = 0; i < 50; i++)
	{
		//要建立一个线程，不知道能不能实现
		system_led_off();
		rtems_task_wake_after(100000);
		system_led_on();	
		rtems_task_wake_after(100000);
		//pthread_testcancel();
	}
	
	return NULL;
}

u32 system_led_blink_begin(void)
{
	s32 status;
	pthread_attr_t attr;

	// initialize pthread attr and create read and write threads
	if(pthread_attr_init(&attr) !=0)
	{
	   	printf("pthread_attr_init error");
	}
	if(pthread_attr_setinheritsched(&attr, PTHREAD_EXPLICIT_SCHED) != 0) {
	   	printf("pthread_attr_setinheritsched error");
	}
	if(pthread_attr_setschedpolicy(&attr, SCHED_RR) != 0) {
	   	printf("pthread_attr_setschedpolicy error");
	}
	if (pthread_create( &led_thread, &attr, system_led_blink_thread, NULL) != 0) {
	   	printf("led blink thread creation failed!\n");
	}
	else
	{
	   printf("led blink thread created\n");
	}
	
	status = pthread_join(led_thread,NULL);
	printf("led_thread pthread_join = %d!\n", status);
	if(status != 0) {
	   printf("led blink pthread_join error %d!\n", status);
	}
	else
	{
	   printf("led blink thread complete\n");
	}
	
}

u32 system_led_blink_stop(void)
{
	s32 status = 0;

	//status = pthread_cencel(&led_thread);

	return status;
	
}

void watchdog_feed_stop(void)
{
	return;
}


u32 hw_test(u32 arg)
{
	u8 ret[3] = {0};

	printf("\nBegin to hw_test\n");
	system_led_off();

	ret[1] = emmc_test_read_write();
	if(ret[1])
	{
		printf("\nemmc_test_read_write error, ret = %d\n", ret[1]);
	}


	UsbVscAppWrite(pSelf, 3, ret, 0);
	//rtems_semaphore_obtain(pSelf->semWriteId[0], RTEMS_WAIT, 0);
	system_led_on();
	
	printf("\nEnd to hw_test\n");
	
	return ret;
}


