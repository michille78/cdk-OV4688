#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <malloc.h>
#include <assert.h>

#ifdef __cplusplus
extern "C" {
#endif

static unsigned half2float(unsigned short h);
unsigned short float2half(unsigned f);
void floattofp16(unsigned char *dst, float *src, unsigned nelem);
#ifdef __cplusplus
}
#endif
