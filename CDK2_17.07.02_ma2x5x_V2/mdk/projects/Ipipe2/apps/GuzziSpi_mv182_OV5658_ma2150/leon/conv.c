#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <assert.h>
#include <mv_types.h>
#define STB_IMAGE_IMPLEMENTATION
#define STB_IMAGE_RESIZE_IMPLEMENTATION
#include "stb_image.h"
#include "stb_image_resize.h"
#include "fp16.h"
#include <Fp16Convert.h>


//调整图片大小，将之调整为448×448,再归一化，然后将float32转为float16半精度

u16* loadimage(unsigned char *img, int width,int height,int reqsize, float *mean, float *std)
{
	int i;
	unsigned char *imgresized=(unsigned char*)malloc(3*reqsize*reqsize);
	assert(imgresized!=NULL);
	
	
	
	
	stbir_resize_uint8(img, width, height, 0, imgresized, reqsize, reqsize, 0, 3);
          
          free(imgresized);

	float* imgfp32=(float*)malloc(sizeof(float)*3*reqsize*reqsize);
	assert(imgfp32!=NULL);
	
	for(i = 0; i < reqsize * reqsize * 3; i++)
		imgfp32[i] = imgresized[i];

	
	for(i = 0; i < reqsize*reqsize; i++)
	{
#if 0
		imgfp32[3*i+0] = (imgfp32[3*i+0]-mean[0])*std[0]; 
		imgfp32[3*i+1] = (imgfp32[3*i+1]-mean[1])*std[1];
		imgfp32[3*i+2] = (imgfp32[3*i+2]-mean[2])*std[2];
#else
		float blue, green, red;
                blue = imgfp32[3*i+2]/255.0;
                green = imgfp32[3*i+1]/255.0;
                red = imgfp32[3*i+0]/255.0;


                imgfp32[3*i+0] = blue;
                imgfp32[3*i+1] = green; 
                imgfp32[3*i+2] = red;
#endif
	}
	
	u16* imgfp16=(u16*) malloc(sizeof(u16) * reqsize * reqsize * 3);
	
	assert(imgfp16!=NULL);
	
	for(i=0;i<reqsize*reqsize;++i)
	{
	   imgfp16[3*i]=f32Tof16(imgfp32[3*i]);
	   imgfp16[3*i+1]=f32Tof16(imgfp32[3*i+1]);
	   imgfp16[3*i+2]=f32Tof16(imgfp32[3*i+2]);
	}
	
	
	//floattofp16((unsigned char*)imgfp16, imgfp32, 3*reqsize*reqsize);
   /*	
	 for (int i=0;i<448*448;++i)
   {
     printf("16dist+%d+0:%d",i,*(imgfp16+i));
     printf("  16dist+%d+1:%d",i,*(imgfp16+i+1));
     printf("  16dist+%d+2:%d",i,*(imgfp16+i+2));
     printf("\n");
   }*/
	
	
	
	free(imgfp32);

	
	return imgfp16;
}

