
#include "interpret_output.h" 
#include "vsc2app_outcall.h"
#include "usbpump_vsc2app.h"
int pid;
extern cvector cvpeople;

extern USBPUMP_VSC2APP_CONTEXT *	pSelf;
extern unsigned int Count_Total;
extern unsigned int DETECTION_RESOULT;

/*
static float *cmpdata;
int indexcmp(const void *a1, const void *b1)
{
	int *a = (int *)a1;
	int *b = (int *)b1;
	float diff = cmpdata[*b] - cmpdata[*a];
	if (diff < 0)
		return -1;
	else if (diff > 0)
		return 1;
	else return 0;
}
*/

void getbox(float *result, float *pro_obj, int *idx_class, int *bboxs, int *bboxs_num, float thresh, int img_width, int img_height)
{
	float pro_class[49];
	int idx;
	int max_idx;
	float max;

	int Fathom=0;

	if(Fathom)//to normalization the Fathom output
	{
	int Fathom_prob_max_idx_i=0;
	int Fathom_prob_max_idx_j=0;
	int Fathom_prob_max_idx_k=0;
	int Fathom_scale_max_idx_i=0;
	int Fathom_scale_max_idx_j=0;
	int Fathom_scale_max_idx_m=0;		
	float Fathom_prob_max=0.00001f;
	float Fathom_scale_max=0.00001f;
	
	for (int i = 0; i < 7; i++)
	{
		for (int j = 0; j < 7; ++j)
		{
			for (int k = 0; k < 20; k++)
			{
				if (result[(i * 7 + j) * 20  + k] > Fathom_prob_max)
				{
					Fathom_prob_max = result[(i * 7 + j) * 20 + k];
					Fathom_prob_max_idx_i=i;
					Fathom_prob_max_idx_j=j;
					Fathom_prob_max_idx_k=k;	
				}

			}
			for (int m = 0; m < 2; m++)
			{
				if (result[ 980 + (i * 7 + j) * 2  + m] > Fathom_scale_max)
				{
					Fathom_scale_max = result[ 980 + (i * 7 + j) * 2 + m];
					Fathom_scale_max_idx_i=i;
					Fathom_scale_max_idx_j=j;
					Fathom_scale_max_idx_m=m;	
				}

			}
		}
	}
	for (int i = 0; i < 7; i++)
	{
		for (int j = 0; j < 7; ++j)
		{
			for (int k = 0; k < 20; k++)
			{
				result[(i * 7 + j) * 20 + k]/=Fathom_prob_max;

			}
			for (int m = 0; m < 2; m++)
			{
				result[ 980 + (i * 7 + j) * 2 + m]/=Fathom_scale_max;
			}
		}
	}	
	}

	for (int i = 0; i < 7; i++)
	{
		for (int j = 0; j < 7; ++j)
		{
			max = 0;
			max_idx = 0;
			idx = (i * 7 + j) * 20;
			for (int k = 0; k < 20; k++)
			{
				if (result[idx + k] > max)
				{
					max = result[idx + k];
					max_idx = k + 1;
				}

			}
			idx_class[i * 7 + j] = max_idx;
			pro_class[i * 7 + j] = max;
			pro_obj[(i * 7 + j) * 2] = max*result[7 * 7 * 20 + (i * 7 + j) * 2];
			pro_obj[(i * 7 + j) * 2 + 1] = max*result[7 * 7 * 20 + (i * 7 + j) * 2 + 1];

			//printf("max_idx: %d, max: %.3f, scales1:%.3f, scales2:%.3f   ",max_idx,max,result[7 * 7 * 20 + (i * 7 + j) * 2],result[7 * 7 * 20 + (i * 7 + j) * 2 + 1]);
		}
	}
	//printf("\n");
	//bboxs = (int **)calloc(7 * 7 * 2, sizeof(int *));
	int x_min, x_max, y_min, y_max;
	float x, y, w, h;
	*bboxs_num = 0;
	for (int i = 0; i < 7; ++i)
	{
		for (int j = 0; j < 7; ++j)
		{
			for (int k = 0; k < 2; ++k)
			{
				if (pro_obj[(i * 7 + j) * 2 + k] > thresh)
				{

					idx = 49 * 20 + 49 * 2 + ((i * 7 + j) * 2 + k) * 4;
					x = img_width*(result[idx++] + j) / 7;
					y = img_height*(result[idx++] + i) / 7;
					w = img_width*result[idx] * result[idx++];
					h = img_height*result[idx] * result[idx];

					x_min = x - w / 2;
					y_min = y - h / 2;
					x_max = x + w / 2;
					y_max = y + h / 2;

					//int *bbox = (int *)calloc(6, sizeof(int));

					bboxs[*bboxs_num* 6 + 0] = idx_class[i * 7 + j];
					bboxs[*bboxs_num* 6 + 1] = x_min;
					bboxs[*bboxs_num* 6 + 2] = y_min;
					bboxs[*bboxs_num* 6 + 3] = x_max;
					bboxs[*bboxs_num* 6 + 4] = y_max;
					bboxs[*bboxs_num* 6 + 5] = (int)(pro_obj[(i * 7 + j) * 2 + k] * 100);

					//printf("filtered index: i: %d,j:%d,k:%d probility: %.3f\n",i,j,k,pro_obj[(i * 7 + j) * 2 + k]);
					*bboxs_num = *bboxs_num + 1;

				}
			}
		}
	}
}
int lap(int x1_min, int x1_max, int x2_min, int x2_max){
	if (x1_min < x2_min){
		if (x1_max < x2_min){
			return 0;
		}
		else{
			if (x1_max > x2_min){
				if (x1_max < x2_max){
					return x1_max - x2_min;
				}
				else{
					return x2_max - x2_min;
				}
			}
			else{
				return 0;
			}
		}
	}
	else{
		if (x1_min < x2_max){
			if (x1_max < x2_max)
				return x1_max - x1_min;
			else
				return x2_max - x1_min;
		}
		else{
			return 0;
		}
	}
}


void interpret_output(float *original_out, YOLO_Result *result, int * result_num, int img_width, int img_height,float threshold)
{
        char* categories[]={"aeroplane","bicycle","bird","boat","bottle","bus","car","cat","chair","cow","diningtable",
              "dog","horse","motorbike","person","pottedplant","sheep","sofa","train","tvmonitor"};
	int w_img = img_width;
	int h_img = img_height;
	printf("image width: %d, image height: %d \n", w_img, h_img);
	float overlap;
	float overlap_thresh = 0.5;
	int num_class = 20;

	float pro_obj[49][2];
	int idx_class[49];
	int bbox_num = 0;
	int bboxs[98][6];

	getbox(original_out, &pro_obj[0][0], idx_class, &bboxs[0][0], &bbox_num, threshold, w_img, h_img);

        int *mark = (int*)malloc(bbox_num*sizeof(int));
	for (int i = 0; i < bbox_num; i++)
	{
		mark[i] = 1;
	}

	for (int i = 0; i < bbox_num; ++i)
	{
		for (int j = i + 1; j < bbox_num; ++j)
		{
			int overlap_x = lap(bboxs[i][0], bboxs[i][2], bboxs[j][0], bboxs[j][2]);
			int overlap_y = lap(bboxs[i][1], bboxs[i][3], bboxs[j][1], bboxs[j][3]);
			overlap = (overlap_x*overlap_y)*1.0 / ((bboxs[i][0] - bboxs[i][2])*(bboxs[i][1] - bboxs[i][3]) + (bboxs[j][0] - bboxs[j][2])*(bboxs[j][1] - bboxs[j][3]) - (overlap_x*overlap_y));
			if (overlap > overlap_thresh)
			{
				if (bboxs[i][4] > bboxs[j][4])
				{

					mark[j] = 0;
				}
				else
				{
					mark[i] = 0;
				}
			}
		}
	}

	int filtered_bbox_num=0;
	for (int i = 0; i < bbox_num; ++i)
	{
		if (mark[i])
		{
			strcpy(result[filtered_bbox_num].category, categories[bboxs[i][0] - 1]);
			result[filtered_bbox_num].class_idx =bboxs[i][0] - 1;
			result[filtered_bbox_num].x =(bboxs[i][3]+bboxs[i][1])/2;
			result[filtered_bbox_num].y =(bboxs[i][4]+bboxs[i][2])/2;
			result[filtered_bbox_num].width =(bboxs[i][3]-bboxs[i][1]);
			result[filtered_bbox_num].height =(bboxs[i][4]-bboxs[i][2]);
			result[filtered_bbox_num].probability=bboxs[i][5]*1.0/100.0;
			filtered_bbox_num++;
			*result_num=filtered_bbox_num;
			printf("class: %s %d%% \n", categories[bboxs[i][0] - 1], bboxs[i][5] );
			//printf("rectangle: xmin:%d ymin:%d xmax:%d,ymax:%d \n", bboxs[i][1], bboxs[i][2], bboxs[i][3], bboxs[i][4]);
			printf("x:%d y:%d w:%d,h:%d \n", (bboxs[i][3]+bboxs[i][1])/2, (bboxs[i][4]+bboxs[i][2])/2, (bboxs[i][3]-bboxs[i][1]), (bboxs[i][4]-bboxs[i][2]));

		}
	}
	if(DETECTION_RESOULT)
  {
      unsigned char* send =(unsigned char*)malloc(sizeof(YOLO_Result));
      memcpy(send,result,sizeof(YOLO_Result)*20);
      UsbVscAppWrite(pSelf,sizeof(YOLO_Result)*20,send,0);
      DETECTION_RESOULT=0;
      free(send);
  }
	free(mark);
}


cvector cvector_create(int size)
{
        cvector cv;

        cv.cv_pdata =(tracking_msg*)malloc(sizeof(tracking_msg)*MIN_LEN);

        assert(cv.cv_pdata!=NULL);
        cv.cv_size = size;
        cv.cv_tot_len = MIN_LEN;
        cv.cv_len = 0;

        return cv;
}

void cvector_destroy(cvector cv)
{
        free(cv.cv_pdata);
        return;
}

int cvector_length(cvector cv)
{
        return cv.cv_len;
}

int cvector_pushback(cvector cv, tracking_msg memb)
{
        if (cv.cv_len >= cv.cv_tot_len)
        {
                tracking_msg *pd_sav = cv.cv_pdata;
                cv.cv_tot_len =(cv.cv_tot_len<<EXPANED_VAL);
                cv.cv_pdata = realloc(cv.cv_pdata, cv.cv_tot_len * cv.cv_size);

                if (!cv.cv_pdata)
                {
                        cv.cv_pdata = pd_sav;
                        cv.cv_tot_len =(cv.cv_tot_len>>EXPANED_VAL);
                        return CVEPUSHBACK;
                }
        }
        
        cv.cv_pdata[cv.cv_len * cv.cv_size]=memb;
        cv.cv_len++;

        return CVESUCCESS;
}



tracking_msg cvector_begin(cvector cv)
{
        return cv.cv_pdata[0];
}

tracking_msg cvector_end(cvector cv)
{
        return cv.cv_pdata[cv.cv_size * (cv.cv_len-1)];
}

int cvector_val(cvector cv, tracking_msg iter,tracking_msg memb)
{
       memb=iter;
        return 0;
}

int cvector_val_at(cvector cv, int index, tracking_msg memb)
{
        
        memb=cv.cv_pdata[index * cv.cv_size];
        return 0;
}

int cvector_rm(cvector cv, tracking_msg iter,int index)
{
        for(int i=index;i<(cv.cv_len-1);i++)
        {
           cv.cv_pdata[i * cv.cv_size]=cv.cv_pdata[(i+1)* cv.cv_size];
        }
        
        cv.cv_len--;

        if ((cv.cv_tot_len >= (MIN_LEN << REDUSED_VAL))
                        && (cv.cv_len <= (cv.cv_tot_len >> REDUSED_VAL)))
        {
                void *pd_sav = cv.cv_pdata;
                cv.cv_tot_len >>= EXPANED_VAL;
                cv.cv_pdata = realloc(cv.cv_pdata, cv.cv_tot_len * cv.cv_size);

                if (!cv.cv_pdata)
                {
                        cv.cv_tot_len <<= EXPANED_VAL;
                        cv.cv_pdata = pd_sav;
                        return CVERM;
                }
        }

        return CVESUCCESS;
}

int cvector_rm_at(cvector cv, int index)
{
        tracking_msg iter;
        iter = cv.cv_pdata[cv.cv_size * index];
        return cvector_rm(cv, iter,index);
}

tracking_msg cvector_next(cvector cv, tracking_msg iter)
{
        int i=0;
        for(i=0;i<cv.cv_len;i++)
        {
          if(cv.cv_pdata[i*cv.cv_size].id==iter.id)
            break;
        }
        return cv.cv_pdata[(i+1)*cv.cv_size];
}
int cvector_iter_at(cvector cv, tracking_msg iter)
{       
        int i=0;
        for(i=0;i<cv.cv_len;i++)
        {
          if(cv.cv_pdata[i*cv.cv_size].id==iter.id)
            break;       
        }
        return i;
}



int cvector_iter_val(cvector cv, tracking_msg iter, tracking_msg memb)
{
        memb=iter;
        return 0;
}

int cvector_refresh(cvector cv, int index , tracking_msg memb)
{
        if (index < cv.cv_tot_len)
        {
              
                cv.cv_pdata[ cv.cv_size * index]=memb;
        }
        return 0;
}
int  point_out(Point   p)
{
	if ( p.x > OUTTRIPWIREX && p.y < OUTTRIPWIREY ) 
		return TRUE;
	return FALSE;
}

int point_in(Point   p)
{
	if ( p.x < TRIPWIREX || p.y > TRIPWIREY ) 
		return TRUE;
	return FALSE;
}

int going_IN(tracks person){
	int len = person.tracksize;

	if (len >= 4){
		if (point_out(person.point[len-1]) &&
				(((person.point[len-1].x - OUTTRIPWIREX) > 20) ||  ((OUTTRIPWIREY - person.point[len-1].y) > 20)) &&
				point_in(person.point[1]))
			return TRUE;
	}
	return FALSE;
}

int going_OUT(tracks person){
	int len = person.tracksize;
	if (len >= 4){
		if (point_in(person.point[len-1])&&
				( ((TRIPWIREX - person.point[len-1].x) > 20) || ((person.point[len-1].y - TRIPWIREY) > 20)) &&
				(point_out(person.point[1] ) ||
				 (person.point[1].x == OUTTRIPWIREX && person.point[1].y <= OUTTRIPWIREY)||
				 (person.point[1].x >= OUTTRIPWIREX && person.point[1].y == OUTTRIPWIREY)))
			return TRUE;
	}
	return FALSE;
}


void draw_detections(image im, int num, float thresh, YOLO_Result *result)
{  
	int i_class = 0;
	float prob=0.0;
	int newperson = TRUE;

	tracking_msg person;
	int max_p_age = 5;
	int cnt_up=0;
	int cnt_down=0;
		        
	for (int i_p = 0 ; i_p < cvector_length(cvpeople); i_p++)
	{
		cvector_val_at(cvpeople,i_p,person);
		person.age += 1;
		if (person.age > max_p_age){
			person.done = TRUE;
		}
		cvector_refresh(cvpeople,i_p,person);
	}
        
		
	for(int i = 0; i < num; ++i){

		YOLO_Result b = result[i];
		printf("box[%d]:class_idx:%d \n",i,b.class_idx);
		i_class = b.class_idx;
		prob = b.probability;

		if (prob > thresh && i_class == 14){
			

			int left  = (b.x-b.width/2.)*im.w;
			int right = (b.x+b.width/2.)*im.w;
			int top   = (b.y-b.height/2.)*im.h;
			int bot   = (b.y+b.height/2.)*im.h;

			if(left < 0) 
			    left = 0;
			if(right > im.w-1) 
			    right = im.w-1;
			if(top < 0) 
			    top = 0;
			if(bot > im.h-1) 
			     bot = im.h-1;

			newperson = TRUE;
			int headlen = right-left;
			int midy = ((top + 40) > im.h) ? top : (top +40) ;
			int midx = left + (headlen)/2;
						
			if (headlen > 100 )  //200是个标准的值，需要仔细进行设定研究
				headlen = 100;
			if (midy >= UPLINEY || midx <= UPLINEX){
				for(int i = 0 ; i < cvector_length(cvpeople); i++){
					cvector_val_at(cvpeople,i,person);
					if (abs(midx-person.x) <= headlen && abs(midy-person.y) <= headlen)
					{
						int size = person.list.tracksize;
						newperson = FALSE;
						if ( size > 499 ){
						        
						           unsigned char* send =(unsigned char*)malloc(sizeof(tracking_msg));
                                                           memcpy(send,&person,sizeof(tracking_msg));
						           UsbVscAppWrite(pSelf,sizeof(tracking_msg),send,0);
						           free(send);
						       
						        
							person.list.tracksize = 0;
							size = 0;
							memset((void *)person.list.point,0,50 * sizeof(Point));
						}

						person.list.point[size].x = midx;
						person.list.point[size].y = midy;
						person.list.tracksize++;

						person.x = midx;
						person.y = midy;

						person.now_box.left = left;
						person.now_box.top = top;
						person.now_box.right = right;
						person.now_box.bottom = bot;
						
						person.prob = prob;
						person.age = 0;

						cvector_refresh(cvpeople,i,person);
						break;
					}
				}
				if (newperson){
					tracking_msg p;
					memset((void*)&p,0,sizeof(tracking_msg));

					p.list.tracksize = 0;
					p.id = pid;
					p.x = midx;
					p.y = midy;
					p.done = FALSE;
					p.state = FALSE;
					p.age = 0;
					p.dir = 0;
					p.now_box.left = left;
					p.now_box.top = top;
					p.now_box.right = right;
					p.now_box.bottom = bot;
					p.prob = prob;
					cvector_pushback(cvpeople, p);
					pid += 1;
				}
			}
		}
	}

	{//fresh person
		tracking_msg iter;
		tracking_msg delperson;
		int delindex;
		if (cvector_length(cvpeople) > 0)
		{
			for (iter = cvector_begin(cvpeople);
					iter.id != (cvector_end(cvpeople)).id && cvector_length(cvpeople) > 0;)
			{
				cvector_iter_val(cvpeople, iter, delperson);
				if (delperson.done){
					delindex = cvector_iter_at(cvpeople, iter);
					cvector_rm_at(cvpeople, delindex);
				}
				else
					iter = cvector_next(cvpeople, iter);
			}
			//判断该记录的方向。
			for (int i = 0 ; i < cvector_length(cvpeople); i++){
				cvector_val_at(cvpeople,i,person);
				if (!person.state && going_IN(person.list) ){
					cnt_up += 1;
					person.state = TRUE;
					person.dir = GOING_IN;
				}
				else if (!person.state &&
						going_OUT(person.list))
				{
					cnt_down += 1;
					person.state = TRUE;
					person.dir = GOING_OUT;
				}

				if (person.state){
					if ( (person.dir == GOING_OUT) && (person.y > TRIPWIREY || person.x < TRIPWIREX) )
						person.done = TRUE;
					else if ( (person.dir == GOING_IN) &&  (person.y < OUTTRIPWIREY && person.x > TRIPWIREX) )
						person.done = TRUE;
				}
				cvector_refresh(cvpeople,i,person);
			}

		}
	}
	if(Count_Total)
  {  
      int peo_count[2];
      peo_count[0]=cnt_up; 
      peo_count[1]=cnt_down;
      unsigned char* send =(unsigned char*)malloc(sizeof(int)*2);
      //unsigned char* send="0";
      memcpy(send,peo_count,sizeof(int)*2);
      UsbVscAppWrite(pSelf,1,send,0);
      Count_Total=0;
      free(send);
  }

	printf("OUT:%d,IN:%d\n",cnt_up,cnt_down);
}
