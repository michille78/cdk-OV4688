//
// Created by xutianyi on 18-1-30.
//

#ifndef DEEPANO_USB_HOST_YK_API_H
#define DEEPANO_USB_HOST_YK_API_H

#endif //DEEPANO_USB_HOST_YK_API_H


#ifndef YK_API_H__
#define YK_API_H__

#include <stdint.h>

typedef struct rs_rect {
    int left;
    int top;
    int width;
    int height;
}__attribute__((aligned(4))) RS_RECT ;

typedef struct rs_point {
    float x;
    float y;
}__attribute__((aligned(4))) RS_POINT;

typedef enum rs_pixel_fmt{
    PIX_FORMAT_GRAY,
    PIX_FORMAT_BGR888,
    PIX_FORMAT_NV21,
    PIX_FORMAT_BGRA8888,
    PIX_FORMAT_I422,
    PIX_FORMAT_I420,
    PIX_FORMAT_YUV444,
    PIX_FORMAT_H264,
}RS_PIXEL_FMT;

typedef struct rs_img {
    int width;
    int height;
    int stride;
    int imgSize;
    RS_PIXEL_FMT fmt;
    int placeholder;
    unsigned char* img;
}__attribute__((aligned(16))) RS_IMG;

typedef struct YK_OBJ_INFO {
    RS_RECT rect;
    uint64_t timestamp;
    RS_IMG img;
    uint32_t trackId;
}__attribute__((aligned(4))) YK_OBJ_INFO;

#define RS_TRACE_MAX_LENGTH	250
typedef struct YK_TRACE_INFO
{
    int trackID;
    int pointNum;

#if UINTPTR_MAX == 0xffffffffffffffff
/* 64-bit */
    RS_POINT* points;
    uint64_t* timestamps;
#else
    //UINTPTR_MAX == 0xffffffff
    RS_POINT* points;
    uint32_t placeholder1;
    uint64_t* timestamps;
    uint32_t placeholder2;
#endif
}__attribute__((aligned(4))) YK_TRACE_INFO;

typedef struct YK_CAMERA_CONFIG
{
    int width;
    int height;
    int fps;
    int idx;
}__attribute__((aligned(4))) YK_CAMERA_CONFIG;

typedef enum YK_RUNMODE
{
    YK_RUNMODE_NONE,
    YK_RUNMODE_FACECAP = (1 << 1),
    YK_RUNMODE_FLOWSTA = (1 << 2),
    YK_RUNMODE_LIVEVID = (1 << 3),
    YK_RUNMODE_INOUT = (1 << 4),
    YK_RUNMODE_CAMERAONLY = (1 << 5),
    YK_RUNMODE_UNKNOWN = 0x7F
}__attribute__((aligned(4))) YK_RUNMODE;

typedef struct YK_VER {
    int major;
    int minor;
    int revision;
}__attribute__((aligned(4))) YK_VER;
/********************************************
 general purpose functions
 *******************************************/
int yk_init();
int yk_uninit();

int yk_ping();
int yk_version(YK_VER* ver);
int yk_hostversion(YK_VER* ver);
int yk_blobversion(YK_VER* ver);
int yk_getSysStatus(uint64_t * status);

/* set camera configuration */
int yk_setCameraConfig(YK_CAMERA_CONFIG config);
/* get current camera configuration */
int yk_getCameraConfig(YK_CAMERA_CONFIG * config);

/* set/switch yueke device running mode */
int yk_setRunMode(YK_RUNMODE mode);
/* set/switch yueke device running mode */
YK_RUNMODE yk_getRunMode();

/* update model */
//int yk_updateModel(char* filepath);
int yk_enterBootloaderForModel();
/* update firmware */
//int yk_updateFirmware(char* filepath);
int yk_enterBootloader();

int yk_checkEmmc();

/* capture a frame and return frame data */
int yk_captureFrame(RS_IMG* img);


int yk_startRecord(int interval, int recordOnDetect);
int yk_stopRecord();
int yk_startRetrieveRecord();
int yk_stopRetrieveRecord();
int yk_retrieveRecord(RS_IMG* img);

/* set region of interest for FACECAP/FLOWSTA mode */
int yk_setROI(RS_RECT roi);
/* get region of interest for FACECAP/FLOWSTA mode */
int yk_getROI(RS_RECT * roi);

/* sync timestamp between device and host */
int yk_syncTime();
int yk_getTime(uint64_t *t);


int yk_getLog(char* logdst, int * length);

/********************************************
 FACECAP mode functions
 *******************************************/
/* return current detected face info
    pFaceArray and pBodyArray's memory is allocated dynamically within the lib */
int yk_getObjDetected(int *pObjCount, YK_OBJ_INFO **pFaceArray, YK_OBJ_INFO **pBodyArray);

/* enable/disable body detect */
int yk_enableBodyDetect(int enableOrNot);
/* check if body detect is enabled */
int yk_getBodyDetectEnabled();

typedef void (*YK_OBJ_DETECTED_CALLBACK) (int pObjCount, YK_OBJ_INFO *pFaceArray, YK_OBJ_INFO *pBodyArray);

/* set object detected callback. The callback will be called once a object is detected */
int yk_setOBjDetectedCallback(YK_OBJ_DETECTED_CALLBACK callback);

/********************************************
 FLOWSTA mode functions
 *******************************************/
/* get how many in/out count since the last yk_getStatisticIncremental call
    inTimestamps and outTimestamps' memory is allocated statically within the lib.
    Be noted that inTimestamps/outTimestamps' max size is 100 even when inCountInc/outCountInc is great than 100 */
int yk_getStatisticIncremental(int * inCountInc, int * outCountInc, uint64_t ** inTimestamps, uint64_t ** outTimestamps);
/* get how many in/out count since the device bootup */
int yk_getStatistic(int * inCountTotal, int * outCountTotal);

typedef void (*YK_TRACE_READY_CALLBACK) (YK_TRACE_INFO trace);

/* set trace ready callback. The callback will be called everytime a trace ends, or the trace length exceeds max trace length */
int yk_setTraceReadyCallback(YK_TRACE_READY_CALLBACK callback);

typedef void (*YK_INOUT_EVENT_CALLBACK) (int inInc, int outInc, int inTotal, int outTotal);

int yk_setInoutEventCallback(YK_INOUT_EVENT_CALLBACK callback);

/********************************************
 LIVEVID mode functions
 *******************************************/
typedef void (*YK_FRAME_READY_CALLBACK) (unsigned char * data, int length);

/* set H264 frame ready callback. The callback will be called everytime a H264 frame is received from device */
int yk_setH264FrameReadyCallback(YK_FRAME_READY_CALLBACK callback);

#endif
