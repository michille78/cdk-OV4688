
/// @file
/// @copyright All code copyright Movidius Ltd 2012, all rights reserved
///            For License Warranty see: common/license.txt
///
/// @brief
///
///


#ifndef _GPIO_CONFIG_H_
#define _GPIO_CONFIG_H_

int GpioConfig();

#endif
