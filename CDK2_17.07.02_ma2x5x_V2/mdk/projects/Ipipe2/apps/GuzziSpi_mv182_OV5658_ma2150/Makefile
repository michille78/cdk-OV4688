MDK_ROOT =  ../../../../..
GUZZI_REPO =  $(realpath ../../../../..)
GUZZI_INC_DIR =  $(realpath $(MDK_ROOT)/mdk/projects/Ipipe2/components/$(SOC_REV_DRV)/include)
GUZZI_LIB_DIR =  $(realpath $(MDK_ROOT)/mdk/projects/Ipipe2/components/$(SOC_REV_DRV)/lib)

MV_TOOLS_DIR = $(MDK_ROOT)/tools
MV_TOOLS_VERSION=
MV_COMMON_BASE  ?= $(MDK_ROOT)/mdk/common
MV_FATHOM_RUN_DIR ?= ../..
MV_TENSOR_BASE ?= ../../../MvTensor

$(info ########################## BUILD settings ##########################)
MV_SOC_PLATFORM  = myriad2
MV_SOC_REV ?= ma2150
SOC_REV_DRV = ma2x5x
AppConfig = mv182_OV5658_ma2150

LEON_HEADERS += $(wildcard $(MV_FATHOM_RUN_DIR)/leon/*.h)
LEON_COMPONENT_HEADERS_PATHS += $(MV_FATHOM_RUN_DIR)/leon
LEON_COMPONENT_CPP_SOURCES_LOS += $(wildcard $(MV_FATHOM_RUN_DIR)/leon/*.cpp)

#ifeq ($(BLOB),buildin)
RAWDATAOBJECTFILES += $(DirAppObjDir)/FathomBlob.o
#endif

IPIPE_BASE           = $(MDK_ROOT)/mdk/projects/Ipipe2
include $(IPIPE_BASE)/build/$(SOC_REV_DRV)/ipipe2.mk
#include $(IPIPE_BASE)/build/$(MV_SOC_REV)/osalInclude.mk

DirLDScrCommon = $(MV_COMMON_BASE)/scripts/ld/$(MV_SOC_PLATFORM)memorySections
$(info LD script: $(DirLDScrCommon))
DdrInitHeaderMvcmd   ?= $(MV_COMMON_BASE)/utils/ddrInit/ddrInitHeaderForMvcmd_$(MV_SOC_REV)
#MVCONVOPT ?= -cv:$(MV_SOC_REV)
MvCmdfile = ./output/$(APPNAME)_ddrinit.mvcmd
DefaultFlashScript = ./scripts/flash/default_flash_$(MV_SOC_PLATFORM).scr
$(info Flash script: $(DefaultFlashScript))

# Adding dependency from Guzzi camera library
output/GuzziSpi_$(AppConfig).elf: $(GUZZI_LIB_DIR)/libguzzi_rtems_$(AppConfig).a

################## Appilcation settings #################

LEON_RT_BUILD   ?= yes


#Determine board type from Myriad chip revision - not suitable for client side implementations.
$(warning Warning: Board determined based purely on Myriad revision)

ifeq ($(MV_SOC_REV), ma2150) #MV182 Board presumed

TARGET_BOARD 	?= MV0182

else ifeq ($(MV_SOC_REV), ma2450) #MV212 Board presumed

TARGET_BOARD 	?= MV0212

endif #mv212

 

OUTPUT_UNIT 	?= USB

BOARD_SUPPORT_MIPI = no
BOARD_SUPPORT_HDMI = no
BOARD_SUPPORT_USB  = no

#Initialize error helpers for send out
ifeq ($(TARGET_BOARD),MV0182)

BOARD_SUPPORT_MIPI = yes
BOARD_SUPPORT_HDMI = yes
BOARD_SUPPORT_USB  = yes

else ifeq ($(TARGET_BOARD),MV0212)

BOARD_SUPPORT_MIPI = yes
BOARD_SUPPORT_HDMI = yes
BOARD_SUPPORT_USB  = yes

endif



# select yes to use JTAG or select no to use UART for debug logs
JTAG_LOG        ?= yes

CCOPT += -D$(TARGET_BOARD)
$(info TARGET_BOARD=$(TARGET_BOARD))
$(info OUTPUT_UNIT=$(OUTPUT_UNIT))

#Enable critical sections for Tx
CCOPT += -D'CRITICAL_SECTION_ENABLE=1'

CCOPT += -DAPP_CONFIGURATION
CCOPT += -D'MAX_NR_OF_CAMS                   = 1'
CCOPT += -D'FRAME_T_HEADER_BUFFER_HEIGHT     = 2'

CCOPT_LRT += -DAPP_CONFIGURATION
CCOPT_LRT += -D'MAX_NR_OF_CAMS               = 1'
CCOPT_LRT += -D'FRAME_T_HEADER_BUFFER_HEIGHT = 2'

CCOPT_LRT += -D'NR_OF_BUFFERS_PER_SOURCE     = 2'
CCOPT_LRT += -D'NR_OF_BUFFERS_PER_ISP_OUT    = 2'
CCOPT_LRT += -D'NR_OF_BUFFERS_PER_STILL_OUT  = 1'


TEMPLPROC   ?= ../version_info/tmpl_proc.sh
VERVARS     ?= ../version_info/vervars.sh

################## Appilcation settings END #################
# Basic project modules included
ComponentList += PipePrint
ComponentList_LOS += UnitTestVcs \
					VcsHooks    \
					I2CSlave    \
					Opipe       \
					Fp16Convert \
					$(IPIPE_COMPONENTS)/bicubicWarp	\
					$(IPIPE_COMPONENTS)/IpipeMsgQueue     \
					$(IPIPE_COMPONENTS)/IpipeClient       \
					$(IPIPE_COMPONENTS)/../common         \
					$(IPIPE_COMPONENTS)/sendOut/common    \
					$(IPIPE_PLUGINS_BASE_ARCH)/common/IspCommon


#MIPI Output unit
ifeq ($(OUTPUT_UNIT),MIPI)

ifeq ($(BOARD_SUPPORT_MIPI), no)
$(error ERROR: MIPI is not supported for the $(TARGET_BOARD) board type)
endif #mv182 or mv212

ComponentList_LOS   += $(IPIPE_COMPONENTS)/sendOut/MipiSend
ComponentList_LOS   += LcdGeneric
CCOPT += -DOUTPUT_UNIT_IS_MIPI

#HDMI Output unit
else ifeq ($(OUTPUT_UNIT),HDMI)

ifeq ($(BOARD_SUPPORT_HDMI), no)
$(error ERROR: HDMI is not supported for the $(TARGET_BOARD) board type)
endif #mv182 or mv212

ComponentList_LOS   += $(IPIPE_COMPONENTS)/sendOut/LcdHdmiSend
ComponentList_LOS   += LcdGeneric

CCOPT += -DOUTPUT_UNIT_IS_HDMI

ifeq ($(TARGET_BOARD),MV0182)
ComponentList_LOS   += Board182
endif

ifeq ($(TARGET_BOARD),MV0212)
ComponentList_LOS   += MV0212
endif

#USB Output unit
else ifeq ($(OUTPUT_UNIT),USB)

ifeq ($(BOARD_SUPPORT_USB), no)
$(error ERROR: USB is not supported for the $(TARGET_BOARD) board type)
endif #mv182 or mv212

ComponentList_LOS   += $(IPIPE_COMPONENTS)/sendOut/UsbSend
CCOPT += -DOUTPUT_UNIT_IS_USB
CCOPT += -DINPUT_UNIT_IS_USB
CCOPT += -D'USB_REFCLK_MHZ=USB_REFCLK_20MHZ'
RTEMS_USB_LIB_BUILD_TYPE = release
#Error output unit
else 
$(error ERROR: Unknown output unit - $(OUTPUT_UNIT))
endif



MV_USB_PROTOS = protovideo protovsc2

MV_LOCAL_OBJ=yes

# Ensure that the we are using the correct rtems libs etc RTEMS ADDING
MV_SOC_OS = rtems
RTEMS_BUILD_NAME =b-prebuilt
CCOPT_EXTRA = -D'USE_SOFTWARE=1'
#CCOPT += -D'RTEMS_LOS'

# app can decide, how mutch memory can be available for isp, and section name where this are.
CCOPT_LRT += -D'OPIPE_MEM_SECT=".cmx.data"'
CCOPT_LRT += -D'OPIPE_CBUFF_SZ=720896'
CCOPT += -D'DEFAULT_APP_CLOCK_KHZ=480000'
#########################################################################################


#########################################################################################
# OS-Leon (client)
#########################################################################################



#########################################################################################
# OS-Leon (SPI slave api)
#########################################################################################
SPI_SLAVE_API_BASE  = $(MV_COMMON_BASE)/components/MessageProtocol/leon/rtems/
CCOPT              += -I$(SPI_SLAVE_API_BASE)/include
LEON_APP_C_SOURCES += $(SPI_SLAVE_API_BASE)/src/MessRingBuff.c
LEON_APP_C_SOURCES += $(SPI_SLAVE_API_BASE)/src/MessageProtocol.c
LEON_APP_C_SOURCES += $(SPI_SLAVE_API_BASE)/src/OsDrvSpiSlaveCP.c
LEON_APP_C_SOURCES += $(SPI_SLAVE_API_BASE)/src/OsMessageProtocol.c

#########################################################################################
#Critical to allow multiple symbols definitions
#LDOPT_LRT += --allow-multiple-definition
#LDOPT += --allow-multiple-definition

#########################################################################################
# Include the top makefile


include $(IPIPE_BASE)/pipelines/ispUpTo3Cams/ispUpTo3Cams.mk
include $(MV_TENSOR_BASE)/mvTensorSources.mk
include $(MV_COMMON_BASE)/generic.mk
include $(MV_TENSOR_BASE)/mvTensorRules.mk

######### Extra app related options ##########
CCOPT			+= -DDEBUG
#CCOPT += -DNO_PRINT
CCOPT_LRT += -DOPIPE_PREVIEW_ABLE

CCOPT +=-DDEFAULT_ALLOC_TIMEOUT_MS=4000

CCOPT += -D__MMS_DEBUG__
CCOPT += -D___RTEMS___
CCOPT += -DSPI_SLAVE
CCOPT += -DCAMERA_INTERFACE_ANDROID_CAMERA3
#CCOPT	+= -DNOT_USE_SPI_COMMADN     #define by wzz
CCOPT += -DUSE_SUNNY_BOARD     #define by wzz
CCOPT_LRT += -DUSE_RGB_MODULE     #define by wzz

CCOPT += -I$(GUZZI_INC_DIR)/osal/include
CCOPT += -I$(GUZZI_INC_DIR)/hif/rpc/include
CCOPT += -I$(GUZZI_INC_DIR)/hif/camera3/c/include
CCOPT += -I$(GUZZI_INC_DIR)/dtp/server/source/include
CCOPT += -I$(GUZZI_INC_DIR)/guzzi/modules
CCOPT += -I$(GUZZI_INC_DIR)/guzzi/modules/camera
CCOPT += -I$(GUZZI_INC_DIR)/guzzi/modules/libs
CCOPT += -I$(GUZZI_INC_DIR)/guzzi/modules/include
CCOPT += -I$(GUZZI_INC_DIR)/guzzi/modules/hal/include
CCOPT += -I$(GUZZI_INC_DIR)/guzzi/modules/version_info/include
CCOPT += -I$(GUZZI_INC_DIR)/algorithms/source/include/aca
CCOPT += -I$(GUZZI_INC_DIR)/algorithms/source/include/sg/algs
CCOPT += -I$(GUZZI_LIB_DIR)/dtp_db
CCOPT += -I$(GUZZI_REPO)/camera_sdk/modules

CCOPT += -D'DTP_DB_NAME="database_OV5658.bin"'
CCOPT += -D'FULL_30FPS=1'

DefaultSparcRTEMSLibs += $(GUZZI_LIB_DIR)/libguzzi_rtems_$(AppConfig).a
DefaultSparcRTEMSLibs += $(GUZZI_LIB_DIR)/libversion_info.a
#DefaultSparcRTEMSLibs += $(GUZZI_LIB_DIR)/liblib_ipipe_simulator.a

CCOPT_LRT += -U'DEFAULT_HEAP_SIZE'
CCOPT     += -U'DEFAULT_HEAP_SIZE'

CCOPT_LRT += -D'DEFAULT_HEAP_SIZE=8192'
CCOPT     += -D'DEFAULT_HEAP_SIZE=8192'

ifeq ($(BLOB),buildin)
CCOPT += -D'BUILDIN_BLOB'
endif

$(DirAppObjDir)/FathomBlob.o: Fathom.blob
	@mkdir -p $(dir $@)
	$(OBJCOPY) -I binary --rename-section .data=.ddr.data \
		--redefine-sym  _binary_$(subst /,_,$(subst .,_,$<))_start=FathomBlob \
		-O elf32-sparc -B sparc $< $@

#########################################################################################
# Boot over spi (with DDR init) rule

output/$(APPNAME)_ddrinit.mvcmd: spi-boot

spi-boot:  output/$(APPNAME).mvcmd
	make output/$(APPNAME).mvcmd -j CCOPT_EXTRA="-DNO_PRINT" CCOPT_EXTRA_LRT="-DNO_PRINT"

	dd if=/dev/zero of=output/nulllcmd.mvcmd bs=1K count=1 # at 10MHz boot we should have a 819us timeout which is almost double thant the required 450us for DDr init reported
	cat $(DdrInitHeaderMvcmd) \
		./output/nulllcmd.mvcmd \
		./output/$(APPNAME).mvcmd > ./output/$(APPNAME)_ddrinit.mvcmd

# adb wait-for-device
# adb remount
# adb push ./output/$(APPNAME)_ddrinit.mvcmd /system/vendor/firmware/movidius/myriad_default.mvcmd
# adb shell sync
# adb shell sync
# adb shell "echo -n movidius/myriad_default.mvcmd > /sys/class/misc/myriad/boot_fw"
# adb shell sync
# adb shell sync
# adb shell sync

TEST_TYPE := "MANUAL"
$(info ####################################################################)
