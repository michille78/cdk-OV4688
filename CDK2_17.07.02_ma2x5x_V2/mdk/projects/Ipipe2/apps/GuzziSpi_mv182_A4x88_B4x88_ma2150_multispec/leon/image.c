#include "image.h"

#define PI 3.1415926

u8 CONVERT_ADJUST(u8 tmp)
{
	return ((tmp >= 0 && tmp <= 255)?tmp:(tmp < 0 ? 0 : 255));
}

void CONVERT_YUV420PtoRGB24(void* yuv_src,unsigned char* rgb_dst,u32 nWidth,u32 nHeight)
{
 	unsigned char* tmp_src=(unsigned char*)yuv_src;
	u8 Y,U,V,R,G,B;
	unsigned char* y_planar;
	unsigned char* u_planar;
	unsigned char* v_planar;
	u32 rgb_width , u_width;
	rgb_width = nWidth * 3;
	u_width = (nWidth >> 1);
	u32 ypSize = nWidth * nHeight;
	u32 upSize = (ypSize>>2);
	u32 offSet = 0;

	y_planar = tmp_src;
	u_planar = tmp_src + ypSize;
	v_planar = u_planar + upSize;

	for(u32 i = 0; i < nHeight; i++)
	{
		for(u32 j = 0; j < nWidth; j ++)
		{
			// Get the Y value from the y planar
			Y = *(y_planar + nWidth * i + j);
			// Get the V value from the u planar
			offSet = (i>>1) * (u_width) + (j>>1);
			V = *(u_planar + offSet);
			// Get the U value from the v planar
			U = *(v_planar + offSet);

			R= CONVERT_ADJUST((Y + (1.4075 * (V - 128))));
			G = CONVERT_ADJUST((Y - 0.3455 * (U - 128) - 0.7169 * (V - 128)));
			B = CONVERT_ADJUST((Y + (1.7790 * (U - 128))));
			offSet = rgb_width * i + j * 3;
                        
                        rgb_dst[offSet]=R;
                        rgb_dst[offSet+1]=G;
                        rgb_dst[offSet+2]= B;
		}
	}
}

//鱼眼矫正

void fish_undistored(unsigned char* pRGB_src,unsigned char* pRGB_dist,u32 Width,u32 Height)
{
   u32 Tempradius=Width<=Height?Width:Height;
   float radius=(float) (Tempradius>>1);
   printf("radius:%f\n",radius);
   float lens=(float)radius*3/PI;
   float k=0.0;
   int src_x=0;
   int src_y=0;
   for(u32 h_y=0;h_y<Height;++h_y)
   {
      for(u32 w_x=0;w_x<Width;++w_x)
      {
         u32 x=w_x-(Width>>1);
         u32 y=h_y-(Height>>1);
         float r=(float)sqrt(x*x+y*y);
         float thelta=(float)atan(r/radius);
         if(thelta<0.00001)
         {
            k=1.0;
         }
         else
         {
            k = (float)lens*thelta/r;
         }
         src_x=(int)x*k;
         src_y=(int)y*k;
         src_x += (Width>>1);
         src_y += (Height>>1);      
         memcpy(pRGB_dist+h_y*Width*3+w_x*3,pRGB_src+src_y*Width*3+src_x*3,3);
      }
   }
   printf("ok!\n");
}

//



//封装数据将矫正之后的数据，封装成16位的bin，将这个bin，传入到fathomrun函数当中
u16* loadimage(unsigned char *img, int width,int height,int reqsize, float *mean, float *std);



u16* Image_processor(void* inputTensor,u32 width,u32 height) 
{   
//yuv格式转化为rgb
    
    unsigned char* pRgbBuf=(unsigned char*)malloc(width*height*3);
 
    assert(pRgbBuf != NULL);
                    
    CONVERT_YUV420PtoRGB24(inputTensor,pRgbBuf,width,height);
  
    printf("pRGbBuf address:%p\n",pRgbBuf);
                    
    printf("yuv to rgb succed!!\n");
    
//鱼眼矫正
    
    unsigned char* pRGB_dist=(unsigned char*)malloc(width*height*3);
  
    assert(pRGB_dist != NULL);
    
                  
    fish_undistored(pRgbBuf,pRGB_dist,width,height);
    
    printf("pRGbdist address:%p\n",pRGB_dist);
 
    printf("fisher_eye undistored succed!!\n");
    
//调整大小,归一化，fp32->fp16   
 
    float mean[3],std[3];
    
    u16* dist=loadimage(pRGB_dist, width,height,448, mean, std);
   
   printf("pdist address:%p\n",dist);
/*   
   for (int i=0;i<448*448;++i)
   {
     printf("dist+%d+0:%f",i,*(dist+i));
     printf("  dist+%d+1:%f",i,*(dist+i+1));
     printf("  dist+%d+2:%f",i,*(dist+i+2));
     printf("\n");
   }   
  */ 
    
   printf("resized Image succed!!\n");
   free(pRGB_dist);
   free(pRgbBuf);
    
   return dist;   
}






