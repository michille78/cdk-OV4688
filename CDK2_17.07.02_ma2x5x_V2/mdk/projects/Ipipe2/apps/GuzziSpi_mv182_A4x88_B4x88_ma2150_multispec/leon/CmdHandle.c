//#include "CmdHandle.h"
#include <stdio.h>
#include "spi.h"
#include "stdlib.h"
#include "CSysInfoa.h"
#define BUF_LENGTH 60
#define CALIB_LENGTH 30

#define VERSION_LENGTH 4
#define SERIES_LENGTH 16
#define TIMES_LENGTH 8
#define FLASH_LENGTH 4080
#define OFFSET_LENGTH 1168
#define DOUBLE_LENGTH 30
#define DEBUGCALIB
#define OFFSET 512
//#define POITER
CSysInfo* csys;
CSysInfo csysa;
CameraPara  leftcamera;
UnitCaliInfo leftunit;
uint32_t uiCalibDataLen=FLASH_LENGTH;           
short  calib_data_array[BUF_LENGTH];//data buffer  60short
short calibVersion[VERSION_LENGTH];//8bytes for version
char  series_array[SERIES_LENGTH];//16bytes for serialnumber
short  calib_times_array[TIMES_LENGTH];//
double num[30];
short calibversion[8];
extern struct tFlashParams flash_params;
extern u32  flag_read_calib;
char* calib_float_buf;//float pointer

//calib_float_array[CALIBLENGTH];//24 float parameter
int ip=0;
int kp=0;
int mp=0;
float short_to_float(short Msb,short Lsb)
{
	int c=0;
	float f=0;
        short highbits=Msb&0xFFFF;
        short lowbits=Lsb&0xFFFF;//0-15bit
    	c=(int)((lowbits&0xFFFF)|(highbits<<16));
#ifdef DEBUGCALIB
	printf("ca=0x%x\n",c);
#endif
	f=*((float*)&c);//transfer to float pointer 
	return f;

}
void times_to_struct()
{

      
      csysa.m_CalibTime.mYear=calib_times_array[0]-OFFSET;
      csysa.m_CalibTime.mMonth=calib_times_array[1]-OFFSET;
      csysa.m_CalibTime.mDayofWeek=calib_times_array[2]-OFFSET;
      csysa.m_CalibTime.mDay=calib_times_array[3]-OFFSET;
      csysa.m_CalibTime.mHour=calib_times_array[4]-OFFSET;
      csysa.m_CalibTime.mMinute=calib_times_array[5]-OFFSET;
      csysa.m_CalibTime.mSecond=calib_times_array[6]-OFFSET;
      csysa.m_CalibTime.mMilliseconds=calib_times_array[7]-OFFSET;
    
#ifdef DEBUGCALIB
      printf("csysa.m_CalibTime.mYear=:%d \n",csysa.m_CalibTime.mYear);
      printf("csysa.m_CalibTime.mMonth=:%d \n",csysa.m_CalibTime.mMonth);
      printf("csysa.m_CalibTime.mDayofWeek=:%d \n",csysa.m_CalibTime.mDayofWeek);
      printf("csysa.m_CalibTime.mDay=:%d \n",csysa.m_CalibTime.mDay);
      printf("csysa.m_CalibTime.mHour=:%d \n",csysa.m_CalibTime.mHour);
      printf("csysa.m_CalibTime.mMinute=:%d \n",csysa.m_CalibTime.mMinute);
      printf("csysa.m_CalibTime.mSecond=:%d \n",csysa.m_CalibTime.mSecond);
      printf("csysa.m_CalibTime.mMilliseconds=:%d \n",csysa.m_CalibTime.mMilliseconds);
   
     
#endif
}
void series_to_struct()
{
	  int i=0;
      memcpy(&(csysa.m_SerialNumber),series_array,SERIES_LENGTH*sizeof(char));//16 char
      for(i=0;i<SERIES_LENGTH;i++)
{
      printf("csysa.m_SerialNumber[%d]=:0x%x \n",i,csysa.m_SerialNumber[i]);
}

}
void dataarray_to_struct()
{
       
     int j;
      j=0;
      for(j=0;j<CALIB_LENGTH;j++)//60 short to 30 double  8-68 data
{ 
       num[j]=(double)short_to_float(calib_data_array[2*j+1],calib_data_array[2*j]);
#ifdef DEBUGCALIB
       printf("num[%d]=:%f \n",j,num[j]);
#endif
}
       csysa.m_UnitLeft.m_StereoLeft.m_Height=2688;
       csysa.m_UnitLeft.m_StereoLeft.m_Width=1520;
     //  printf("m_Width:%d \n",csysa.m_UnitLeft.m_StereoLeft.m_Width);
//LEFT
//0-3
       csysa.m_UnitLeft.m_StereoLeft.m_M[0]=num[0];       
       csysa.m_UnitLeft.m_StereoLeft.m_M[2]=num[1];       
       csysa.m_UnitLeft.m_StereoLeft.m_M[4]=num[2];      
       csysa.m_UnitLeft.m_StereoLeft.m_M[5]=num[3];       
       csysa.m_UnitLeft.m_StereoLeft.m_M[8]=1.000000;
//4-8
       csysa.m_UnitLeft.m_StereoLeft.m_D[0]=num[4];       
       csysa.m_UnitLeft.m_StereoLeft.m_D[1]=num[5];      
       csysa.m_UnitLeft.m_StereoLeft.m_D[2]=num[6];      
       csysa.m_UnitLeft.m_StereoLeft.m_D[3]=num[7];       
       csysa.m_UnitLeft.m_StereoLeft.m_D[4]=num[8];       
       csysa.m_UnitLeft.m_StereoLeft.m_R[0]=1.0;
       csysa.m_UnitLeft.m_StereoLeft.m_R[4]=1.0;
       csysa.m_UnitLeft.m_StereoLeft.m_R[8]=1.0;
//right
//9-12
       csysa.m_UnitLeft.m_StereoRight.m_Height=2688;
       csysa.m_UnitLeft.m_StereoRight.m_Width=1520;
       csysa.m_UnitLeft.m_StereoRight.m_M[0]=num[9];      
       csysa.m_UnitLeft.m_StereoRight.m_M[2]=num[10];       
       csysa.m_UnitLeft.m_StereoRight.m_M[4]=num[11];       
       csysa.m_UnitLeft.m_StereoRight.m_M[5]=num[12];       
       csysa.m_UnitLeft.m_StereoRight.m_M[8]=1.0;
//13-17
       csysa.m_UnitLeft.m_StereoRight.m_D[0]=num[13];       
       csysa.m_UnitLeft.m_StereoRight.m_D[1]=num[14];       
       csysa.m_UnitLeft.m_StereoRight.m_D[2]=num[15];      
       csysa.m_UnitLeft.m_StereoRight.m_D[3]=num[16];      
       csysa.m_UnitLeft.m_StereoRight.m_D[4]=num[17];       
//18-26
       csysa.m_UnitLeft.m_StereoRight.m_R[0]=num[18];      
       csysa.m_UnitLeft.m_StereoRight.m_R[1]=num[19];       
       csysa.m_UnitLeft.m_StereoRight.m_R[2]=num[20];
       csysa.m_UnitLeft.m_StereoRight.m_R[3]=num[21];       
       csysa.m_UnitLeft.m_StereoRight.m_R[4]=num[22];      
       csysa.m_UnitLeft.m_StereoRight.m_R[5]=num[23];      
       csysa.m_UnitLeft.m_StereoRight.m_R[6]=num[24];       
       csysa.m_UnitLeft.m_StereoRight.m_R[7]=num[25];       
       csysa.m_UnitLeft.m_StereoRight.m_R[8]=num[26];           
//27-29
       csysa.m_UnitLeft.m_StereoRight.m_T[0]=num[27];      
       csysa.m_UnitLeft.m_StereoRight.m_T[1]=num[28];      
       csysa.m_UnitLeft.m_StereoRight.m_T[2]=num[29];
      
#ifdef DEBUGCALIB
	printf("csysa.m_UnitLeft.m_StereoLeft.m_M[0]:%lf \n",csysa.m_UnitLeft.m_StereoLeft.m_M[0]);
	printf("csysa.m_UnitLeft.m_StereoLeft.m_M[2]:%lf \n",csysa.m_UnitLeft.m_StereoLeft.m_M[2]);
 	printf("csysa.m_UnitLeft.m_StereoLeft.m_M[4]:%lf \n",csysa.m_UnitLeft.m_StereoLeft.m_M[4]);
	printf("csysa.m_UnitLeft.m_StereoLeft.m_M[5]:%lf \n",csysa.m_UnitLeft.m_StereoLeft.m_M[5]);
	printf("csys->m_UnitLeft.m_StereoLeft.m_D[0]:%lf \n",csysa.m_UnitLeft.m_StereoLeft.m_D[0]);
	printf("csys->m_UnitLeft.m_StereoLeft.m_D[1]:%lf \n",csysa.m_UnitLeft.m_StereoLeft.m_D[1]);
 	printf("csys->m_UnitLeft.m_StereoLeft.m_D[2]:%lf \n",csysa.m_UnitLeft.m_StereoLeft.m_D[2]);
	printf("csys->m_UnitLeft.m_StereoLeft.m_D[3]:%lf \n",csysa.m_UnitLeft.m_StereoLeft.m_D[3]);
	printf("csys->m_UnitLeft.m_StereoLeft.m_D[4]:%lf \n",csysa.m_UnitLeft.m_StereoLeft.m_D[4]);
 	printf("csysa.m_UnitLeft.m_StereoRight.m_M[0]:%lf \n",csysa.m_UnitLeft.m_StereoRight.m_M[0]);
	printf("csysa.m_UnitLeft.m_StereoRight.m_M[2]:%lf \n",csysa.m_UnitLeft.m_StereoRight.m_M[2]);
	printf("csysa.m_UnitLeft.m_StereoRight.m_M[4]:%lf \n",csysa.m_UnitLeft.m_StereoRight.m_M[4]);
	printf("csysa.m_UnitLeft.m_StereoRight.m_M[5]:%lf \n",csysa.m_UnitLeft.m_StereoRight.m_M[5]);
	printf("csysa.m_UnitLeft.m_StereoRight.m_D[0]:%lf \n",csysa.m_UnitLeft.m_StereoRight.m_D[0]);
	printf("csysa.m_UnitLeft.m_StereoRight.m_D[1]:%lf \n",csysa.m_UnitLeft.m_StereoRight.m_D[1]);
 	printf("csysa.m_UnitLeft.m_StereoRight.m_D[2]:%lf \n",csysa.m_UnitLeft.m_StereoRight.m_D[2]);
 	printf("csysa.m_UnitLeft.m_StereoRight.m_D[3]:%lf \n",csysa.m_UnitLeft.m_StereoRight.m_D[3]);
	printf("csysa.m_UnitLeft.m_StereoRight.m_D[4]:%lf \n",csysa.m_UnitLeft.m_StereoRight.m_D[4]);
 	printf("csysa.m_UnitLeft.m_StereoRight.m_R[0]:%lf \n",csysa.m_UnitLeft.m_StereoRight.m_R[0]);
	printf("csysa.m_UnitLeft.m_StereoRight.m_R[1]:%lf \n",csysa.m_UnitLeft.m_StereoRight.m_R[1]);
	printf("csysa.m_UnitLeft.m_StereoRight.m_R[2]:%lf \n",csysa.m_UnitLeft.m_StereoRight.m_R[2]);
	printf("csysa.m_UnitLeft.m_StereoRight.m_R[3]:%lf \n",csysa.m_UnitLeft.m_StereoRight.m_R[3]);
 	printf("csysa.m_UnitLeft.m_StereoRight.m_R[4]:%lf \n",csysa.m_UnitLeft.m_StereoRight.m_R[4]);
 	printf("csysa.m_UnitLeft.m_StereoRight.m_R[5]:%lf \n",csysa.m_UnitLeft.m_StereoRight.m_R[5]);
	printf("csysa.m_UnitLeft.m_StereoRight.m_R[6]:%lf \n",csysa.m_UnitLeft.m_StereoRight.m_R[6]);
	printf("csysa.m_UnitLeft.m_StereoRight.m_R[7]:%lf \n",csysa.m_UnitLeft.m_StereoRight.m_R[7]);
	printf("csysa.m_UnitLeft.m_StereoRight.m_R[8]:%lf \n",csysa.m_UnitLeft.m_StereoRight.m_R[8]); 
 	printf("csys->m_UnitLeft.m_StereoRight.m_T[0]:%lf \n",csysa.m_UnitLeft.m_StereoRight.m_T[0]);
 	printf("csys->m_UnitLeft.m_StereoRight.m_T[1]:%lf \n",csysa.m_UnitLeft.m_StereoRight.m_T[1]);
 	printf("csys->m_UnitLeft.m_StereoRight.m_T[2]:%lf \n",csysa.m_UnitLeft.m_StereoRight.m_T[2]);
#endif 
        
        
}
void read_calibration(int cmd)
{
	
	spi_read_calib();
	memcpy(&uiCalibDataLen, flash_params.outBuff,  sizeof(uiCalibDataLen));
	printf("uiCalibDataLen = %d\n",uiCalibDataLen);
	char  *p_read = &uiCalibDataLen;
	printf("%02x %02x %02x %02x\n",p_read[0],p_read[1],p_read[2],p_read[3]);
	flag_read_calib = 1;
}
void write_seriesnumber(char seriesnumber)
{
           
        mp++;
        memcpy(series_array+(mp-1),&seriesnumber,sizeof(char));//original dataoffset address
        printf("uiCalibWriteDataLen = %d\n",uiCalibDataLen);
#ifdef DEBUGCALIB
        printf("calib_series_array[%d]=0x%x\n",mp-1,series_array[mp-1]);
#endif
        if(mp==SERIES_LENGTH)//16 packets
        {
	 if (uiCalibDataLen != FLASH_LENGTH)
		uiCalibDataLen = FLASH_LENGTH;
      
          series_to_struct();
          memcpy(flash_params.inBuff,&uiCalibDataLen,sizeof(uiCalibDataLen));//length
          memcpy(flash_params.inBuff+sizeof(uiCalibDataLen),&(csysa),sizeof(csysa));//4080bytes
          spi_write_calib();//write flash     
          mp=0;    //RESET k
         }
}

void write_calibtimes(short calibtimes)
{
        kp++;
        memcpy(calib_times_array+(kp-1),&calibtimes,sizeof(short));//original dataoffset address
        printf("uiCalibWriteDataLen = %d\n",uiCalibDataLen);
#ifdef DEBUGCALIB
        printf("calib_times_array[%d]=0x%x\n",kp-1,calib_times_array[kp-1]);
#endif
        if(kp==TIMES_LENGTH)//8 packets
        {
      
          times_to_struct();
          memcpy(flash_params.inBuff,&uiCalibDataLen,sizeof(uiCalibDataLen));//length
          memcpy(flash_params.inBuff+sizeof(uiCalibDataLen),&(csysa),sizeof(csysa));//4080bytes
          spi_write_calib();//write flash     
          kp=0;    //RESET k
         }
}

void write_calibration(short calibdata)
{
        
        
      //  i=(i++)%BUF_LENGTH;
        ip++;
        printf("ip=%d",ip);
        memcpy(calib_data_array+(ip-1),&calibdata,sizeof(calibdata));//original dataoffset address
        printf("uiCalibWriteDataLen = %d\n",uiCalibDataLen);
#ifdef DEBUGCALIB
        printf("calib_data_array[%d]=0x%x\n",ip-1,calib_data_array[ip-1]);
#endif
        if(ip==BUF_LENGTH)//60 packets
        {
          dataarray_to_struct();
          memcpy(flash_params.inBuff,&uiCalibDataLen,sizeof(uiCalibDataLen));//length
          memcpy(flash_params.inBuff+sizeof(uiCalibDataLen),&(csysa),sizeof(csysa));//4080 bytes
#ifdef DEBUGCALIB
          printf("calib successfully received %d packets!\n",ip);
          printf("sizeof(csysa) is %d !\n",sizeof(csysa));
          printf("sizeof(csysm_M[0]) is %d !\n",sizeof(csysa.m_UnitLeft.m_StereoLeft.m_M[0]));
          printf("sizeof(double) is %d !\n",sizeof(double));
          printf("sizeof(csysa.m_UnitLeft.m_pMapLX) is %d !\n",sizeof(csysa.m_UnitLeft.m_pMapLX[0]));
#endif
          spi_write_calib();//write flash     
          ip=0;    //RESET i

        }   
}
