///
/// @file
/// @copyright All code copyright Movidius Ltd 2012, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     utils  Leon header
///

#ifndef _UTILS_H_
#define _UTILS_H_

#define TOP_RESULT          (5)

#ifdef __cplusplus
extern "C" {
#endif

typedef struct
{
    fp32 topK;
    s32 topIdx;

}ClassifyResult;

void UptoDown(s32 i, ClassifyResult *topKArray);

void Classify_Test(unsigned char *blob, void *inTensor);

#ifdef __cplusplus
}
#endif

#endif
