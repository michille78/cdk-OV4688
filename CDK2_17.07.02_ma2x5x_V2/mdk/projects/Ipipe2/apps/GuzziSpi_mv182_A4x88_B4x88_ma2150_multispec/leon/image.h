#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <malloc.h>
#include <assert.h>
#include <mv_types.h>


//ypedef unsigned short half;
#ifdef __cplusplus
extern "C" {
#endif
void CONVERT_YUV420PtoRGB24(void* yuv_src,unsigned char* rgb_dst,u32 nWidth,u32 nHeight);
void fish_undistored(unsigned char* pRGB_src,unsigned char* pRGB_dist,u32 Width,u32 Height);
u16* Image_processor(void* inputTensor,u32 width,u32 height);
#ifdef __cplusplus
}
#endif

