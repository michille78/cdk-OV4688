/*
 * spi.c
 *
 */

#include "spi.h"
#include "stdio.h"
#include <stdlib.h>
#include <rtems.h>
#include <rtems/libio.h>
#include <fcntl.h>
#include "DrvGpio.h"
#include "OsDrvSpiBus.h"
#include "OsDrvUsbPhy.h"
#include "OsDrvCpr.h"
#include <SpiFlashN25QDevice.h>
//#include <VcsHooksApi.h>
extern void read_calibration(int cmd);
extern void write_seriesnumber(char seriesnumber);//add by dzj


#if SPI_FLASH_TEST!=0
#define SDebug printf
#else
#define SDebug(...)
#endif
#define CALIBINITLENTH 4080
struct tFlashParams flash_params;
int             spi_fd;

DECLARE_SPI_BUS_GPIO_SS(myr2_spi_0, 1, 0, 0, 1, 1, 10*1000*1000, 8);

static void myr2_spi_test_pin_config(void)
{
    DrvGpioModeRange(74, 77, 0 |  D_GPIO_DIR_OUT);
}

static int myr2_register_libi2c_spi_bus(void)
{
    int ret_code;
    int spi0_busno;
    u32 spi_flash_minor;

    /*
     * init I2C library (if not already done)
     */
    if ((ret_code = OsDrvLibI2CInitialize()) != 0) {
        SDebug("rtems_libi2c_initialize FAILED %d \n", ret_code);
        return ret_code;
    }

    /*
     * register first I2C bus
     */

    ret_code = OsDrvLibI2CRegisterBus(SPI_BUS_NAME,
                                      (rtems_libi2c_bus_t *)&myr2_spi_0);
    if (ret_code < 0) {
        SDebug("Could not register the bus\n");
        return ret_code;
    }
    spi0_busno = ret_code;

    // Returns minor
    ret_code = OsDrvLibI2CRegisterDevice(SPI_FLASH_NAME,
                                         spi_flash_N25Q_driver_descriptor,
                                         spi0_busno, 77);
    if (ret_code < 0) {
        SDebug("Could not register the spi device\n");
        return ret_code;
    }
    spi_flash_minor = ret_code;

    if((ret_code = OsDrvIOInitialize(rtems_libi2c_major, spi_flash_minor, NULL)) != RTEMS_SUCCESSFUL) {
        SDebug("rtems_io_initialize failed with sc %d\n", ret_code);
        return ret_code;
    }

    return RTEMS_SUCCESSFUL;
}

int spi_init(void)
{
	int status;

	myr2_spi_test_pin_config();

	status=myr2_register_libi2c_spi_bus();

	 if(status)
	{
		SDebug("Failed to initialize bus or devices . Fatal ERROR !!! \n");
		return -1;
	}
	return 0;
}
void calib_init_write(void)
{
   printf("calib_init_write!\n");
   uint32_t uiCalibDataLen=CALIBINITLENTH;
   printf("flash_params.offset=%d",flash_params.offset);
   memcpy(flash_params.inBuff,&uiCalibDataLen,sizeof(uiCalibDataLen));
   printf("uiCalibDataLen=%d",uiCalibDataLen);
   memset(flash_params.inBuff+sizeof(uiCalibDataLen),0,CALIBINITLENTH);
   spi_write_calib();
  

}
int spi_open(struct tFlashParams *flash_params)
{
	int fd;
	fd =  open(flash_params->devName, O_RDWR, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
	 if (fd < 0) {
	        SDebug("spi open failed\n");

	        return -1;
	 }
	 return fd;
}

int erase_spi_flash(int fd, struct tFlashParams *p)
{
    int status;
    SDebug("Erasing %d Bytes from %08X\n", p->size, p->offset);

    struct spiFlashN25QEraseArgs_t eraseArgs =
            {
                .offset = p->offset,
                .size   = p->size,
            };
    status = ioctl(fd, FLASH_CMD_ERASE, &eraseArgs);

    SDebug("status:%d\n",status);
    if (status) {
        SDebug("Unable to erase device, err = %d\n", status);
    }

    return status;
}

int write_spi_flash(int fd, struct tFlashParams *p)
{
    int status;

    SDebug("write %d Bytes from %08X\n", p->size, p->offset);
    if(status)
    {
    	SDebug("Unable to erase device, err = %d\n", status);
    	return -1;
    }

    status =  lseek(fd, p->offset, SEEK_SET);
    if (status != (int)p->offset) {
        perror("seek error: ");
        return -1;
    }

    status = write(fd, p->inBuff, p->size);
    if (status != (int)p->size) {
        SDebug("Unable to write all %d bytes to device. %d sent \n", p->size, status);
        perror("");
        return -1;
    }
    SDebug("\n");
    return 0;
}

int read_spi_flash(int fd, struct tFlashParams *p)
{
    int status = 0;
//    SDebug("%s %d\n",__FUNCTION__,__LINE__);
    status =  lseek(fd, p->offset, SEEK_SET);
    if (status != (int)p->offset) {
        perror("seek error: ");
        return -1;
    }
//    SDebug("%s %d\n",__FUNCTION__,__LINE__);
    status = read(fd, p->outBuff, p->size);
    if (status != (int)p->size) {
        SDebug("Unable to read all %d bytes from device. %d sent \n", p->size, status);
        perror("");
        return -1;
    }

    return 0;
}

#if SPI_FLASH_TEST!=0

static unsigned char   spi_write[4096];
static unsigned char   spi_read[4096];
/* SPI test
 * 1.偏移地址  0x00000000写入 4096个字节数据，0,1,2,3...........255
 * 2.偏移地址  0x00000000读取 4096个字节数据
 * 3.读取数据和写入数据内容比较
 * */
void flash_spi_init(void)
{
//	struct tFlashParams flashparams;
//	int             spi_fd;
	int             i;
#if 1
	spi_init();
        printf("spi_init finished!\n");
#endif

	memset(spi_write, 0, sizeof(spi_write));
	memset(spi_read,  0, sizeof(spi_read));

	flash_params.devName    =  SPI_BUS_NAME "." SPI_FLASH_NAME;
	flash_params.imgId      = 0;
	flash_params.writeImg   = 0;

//	flash_params.inputfname    = "CamPara.sys"; // the input file
//	flash_params.outputfname   = "CamParaOut.sys"; // the output file

	/* 4K对齐!!!!!!!!!!!!!!! */
	flash_params.offset     = 0x400000;//offset changed from 0x00000000 to 0x10000
	/* 4K对齐!!!!!!!!!!!!!!! */
	flash_params.size       = ALIGN_TO_SUBSECTOR(sizeof(spi_write));
	flash_params.inBuff     = spi_write;
	flash_params.outBuff    = spi_read;

//	for(i=0;i<16/*sizeof(spi_write)*/;i++)
//	{
//		spi_write[i] = i;
//	}

//	loadMemFromFileSimple(flash_params.inputfname, 4016 * sizeof(char), flash_params.inBuff);
#if 1
	spi_fd = spi_open(&flash_params);

	SDebug("spi_fd= %d (offset,size, 必须4K对齐！！！！！！！！！！！！)\nerase_spi_flash\n", spi_fd);
#endif
/*	if (RTEMS_SUCCESSFUL != erase_spi_flash(spi_fd, &flash_params))
	{
		SDebug("SPI flash erase failed!\n");
	}

	SDebug("write_spi_flash\n");

	if (RTEMS_SUCCESSFUL != write_spi_flash(spi_fd,&flash_params))
	{
		SDebug("SPI flash write failed!\n");
	}

	SDebug("read_spi_flash\n");
	if (RTEMS_SUCCESSFUL != read_spi_flash(spi_fd, &flash_params))
	{
		SDebug("SPI flash read failed!\n");
	}

	if(memcmp(flash_params.outBuff, flash_params.inBuff, flash_params.size) !=0)
	{
		SDebug("SPI flash write and read test failed!\n");
		return;
	}

	for(i=0;i<16;i++)
	{
		SDebug("w r:0x%x 0x%x\n", spi_write[i],spi_read[i]);
	}

	SDebug("spi flash test OK\n");

//	saveMemoryToFile((u32) flash_params.outBuff, 4016,  flash_params.outputfname);*/
}

void spi_write_calib(void)
{
	if (RTEMS_SUCCESSFUL != erase_spi_flash(spi_fd, &flash_params))//erase_spi_flash
	{
		SDebug("SPI flash erase failed!\n");
	}

	SDebug("write_spi_flash\n");

	if (RTEMS_SUCCESSFUL != write_spi_flash(spi_fd,&flash_params))//write_spi_flash
	{
		SDebug("SPI flash write failed!\n");
	}

}
void spi_read_calib(void)
{
	SDebug("read_spi_flash\n");
	if (RTEMS_SUCCESSFUL != read_spi_flash(spi_fd, &flash_params))//read flash
	{
		SDebug("SPI flash read failed!\n");
	}

	if(memcmp(flash_params.outBuff, flash_params.inBuff, flash_params.size) !=0)//write and read test
	{
		SDebug("SPI flash write and read test failed!\n");
		//return;
	}

	for(int i=0;i<16;i++)
	{
		SDebug("w r:0x%x 0x%x\n", spi_write[i],spi_read[i]);
	}
}

void spi_test(void)//4bytes length add data,totally 4096 bytes
{
//	struct tFlashParams flashparams;
//	int             spi_fd;
	int             i;
        printf("enter SPI test!\n");
	spi_init();

	memset(spi_write, 0, sizeof(spi_write));
	memset(spi_read,  0, sizeof(spi_read));

	flash_params.devName    =  SPI_BUS_NAME "." SPI_FLASH_NAME;
	flash_params.imgId      = 0;
	flash_params.writeImg   = 0;

//	flash_params.inputfname    = "CamPara.sys"; // the input file
//	flash_params.outputfname   = "CamParaOut.sys"; // the output file

	/* 4K对齐!!!!!!!!!!!!!!! */
	flash_params.offset     = 0;
	/* 4K对齐!!!!!!!!!!!!!!! */
	flash_params.size       = ALIGN_TO_SUBSECTOR(sizeof(spi_write));
	flash_params.inBuff     = spi_write;
	flash_params.outBuff    = spi_read;

	for(i=0;i<16/*sizeof(spi_write)*/;i++)
	{
		spi_write[i] = i;
	}

//	loadMemFromFileSimple(flash_params.inputfname, 4016 * sizeof(char), flash_params.inBuff);

	spi_fd = spi_open(&flash_params);

	SDebug("spi_fd= %d (offset,size, 必须4K对齐！！！！！！！！！！！！)\nerase_spi_flash\n", spi_fd);

	if (RTEMS_SUCCESSFUL != erase_spi_flash(spi_fd, &flash_params))
	{
		SDebug("SPI flash erase failed!\n");
	}

	SDebug("write_spi_flash\n");

	if (RTEMS_SUCCESSFUL != write_spi_flash(spi_fd,&flash_params))
	{
		SDebug("SPI flash write failed!\n");
	}
#if 0
	SDebug("read_spi_flash\n");
	if (RTEMS_SUCCESSFUL != read_spi_flash(spi_fd, &flash_params))
	{
		SDebug("SPI flash read failed!\n");//read back
	}

	if(memcmp(flash_params.outBuff, flash_params.inBuff, flash_params.size) !=0)
	{
		SDebug("SPI flash write and read test failed!\n");
		return;
	}

	for(i=0;i<16;i++)
	{
		SDebug("w r:0x%x 0x%x\n", spi_write[i],spi_read[i]);
	}

	SDebug("spi flash test OK\n");
#endif

//	saveMemoryToFile((u32) flash_params.outBuff, 4016,  flash_params.outputfname);
}

void spi_series_init()
{
	int series = 0;
	int uiCalibDataLen = 0;
	read_calibration(series);
        printf("read series cmd!\n");

	memcpy(&uiCalibDataLen, flash_params.outBuff, sizeof(uiCalibDataLen));
	if (uiCalibDataLen != 4080){//no series, then write default series
		printf("set to default series:2017090600000001!\n");
		char* series_def = "2017090600000001";
		for (int i=0; i<16; i++){
		printf("%d-",series_def[i]);
		write_seriesnumber(series_def[i]);
		}
	}
}

#endif
