#ifndef SysInfo_H
#define SysInfo_H


#include <math.h>

#ifndef uchar
typedef unsigned char uchar;
#endif
#ifndef WORD
typedef unsigned short WORD;
#endif
typedef struct
{
	WORD mYear;
	WORD mMonth;
	WORD mDayofWeek;
	WORD mDay;
	WORD mHour;
	WORD mMinute;
	WORD mSecond;
	WORD mMilliseconds;
}CalibFileTime;
typedef struct
{
	//图像大小
	int			m_Height;
	int			m_Width;
	//相机内参数
	double		m_M[9];//4 1\245
	//畸变参数
	double		m_D[9];//5
	int		m_DN;//畸变参数数目
	//平移 相对世界坐标
	double		m_T[3];//3
	//旋转 相对世界坐标
	double		m_R[9];//9

}CameraPara;

typedef struct 
{
//	立体相机参数
	CameraPara	m_StereoLeft;//立体左相机 256bytes
	CameraPara	m_StereoRight;//立体右相机 256bytes
//	纹理相机参数
	CameraPara	m_StereoTexture;//纹理相机 256bytes

//	测量单元在世界坐标系中的参数
	////8*(3+9+9)=168
	//平移 相对世界坐标
	double		m_T[3];
	//旋转 相对世界坐标
	double		m_R[9];
	double		m_InvR[9];//旋转矩阵逆矩阵
	//system parameters after rectification	,these paras
	//are necessary in process of 3d reconstruction
	double		m_dU;//主点坐标U  //double 6*8+4*2+4*4+18*8=216
	double		m_dV;//主点坐标V
	double		m_dB;//基线距离
	double		m_dKy;//比例因子
	double		m_dKz;//比例因子
	double		m_dCxlCxr;//主点间距离参数

	int			m_iHeight;	//点云图像尺寸
	int			m_iWidth;	//点云图像尺寸

	float*		m_pMapLX[2]; //left mapx 4byte  add 4byte
	float*		m_pMapLY[2]; //left mapy  4byte  add 4byte
	float*		m_pMapRX[2]; //right mapx 4byte  add  4byte
	float*		m_pMapRY[2]; //right mapy  4byte  add 4byte

	double		m_pRotFromOrig[9];//校正引起的左相机旋转
	double		m_pInvRotFromOrig[9];//校正引起的左相机旋转
       
	
}UnitCaliInfo;

//读取相机标定参数矩阵，进行立体匹配，给出图像校正部分的插值用坐标矩阵
//本类对单双系统都有效

typedef struct  
{
    //add file head contains totally 64bytes  64bytes
    CalibFileTime m_CalibTime;//16bytes for time of calib file
    WORD m_CalibVersion[4];//8bytes for version
    char  m_SerialNumber[16];//16bytes for serialnumber
    char  m_ReservedInfo[24];//reserved 24bytes
    //第二个双目测量单元	
	UnitCaliInfo m_UnitMid;//1168bytes
	//第一个双目测量单元
	UnitCaliInfo m_UnitLeft;//1168bytes
	//第三个双目测量单元	
	UnitCaliInfo m_UnitRight;//1168bytes
	char m_CaliSrcFileID[512];//not be used 512bytes

}CSysInfo;


#endif


