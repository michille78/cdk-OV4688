
#########################################################################################
# OSAL - Made because apparently osal is not possible to be build as a component
#########################################################################################
OSAL_BASE           = $(MV_COMMON_BASE)/components/osal
CCOPT              += -I$(OSAL_BASE)/include/
CCOPT              += -I$(OSAL_BASE)/include/osal
CCOPT              += -I$(OSAL_BASE)/include/osal/rtems
LEON_APP_C_SOURCES += $(OSAL_BASE)/leon/osal/rtems/osal_fcntl.c
LEON_APP_C_SOURCES += $(OSAL_BASE)/leon/osal/rtems/osal_main.c
LEON_APP_C_SOURCES += $(OSAL_BASE)/leon/osal/rtems/osal_malloc.c
LEON_APP_C_SOURCES += $(OSAL_BASE)/leon/osal/rtems/osal_mb_fifo.c
LEON_APP_C_SOURCES += $(OSAL_BASE)/leon/osal/rtems/osal_mb_mqueue.c
LEON_APP_C_SOURCES += $(OSAL_BASE)/leon/osal/rtems/osal_mutex.c
LEON_APP_C_SOURCES += $(OSAL_BASE)/leon/osal/rtems/osal_process.c
LEON_APP_C_SOURCES += $(OSAL_BASE)/leon/osal/rtems/osal_thread.c
LEON_APP_C_SOURCES += $(OSAL_BASE)/leon/osal/rtems/osal_time.c
LEON_APP_C_SOURCES += $(OSAL_BASE)/leon/osal/common/ex_pool.c
LEON_APP_C_SOURCES += $(OSAL_BASE)/leon/osal/common/list_pool.c
LEON_APP_C_SOURCES += $(OSAL_BASE)/leon/osal/common/pool.c
LEON_APP_C_SOURCES += $(OSAL_BASE)/leon/osal/common/pool_unsafe.c
                  