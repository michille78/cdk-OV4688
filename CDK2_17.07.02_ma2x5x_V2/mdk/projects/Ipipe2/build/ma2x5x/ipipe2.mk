
#########################################################################################
# General Ipipe2 path
#########################################################################################
ifeq ($(MV_SOC_REV),$(filter $(MV_SOC_REV),ma2150 ma2155 ma2450 ma2455))
SOC_REV_DRV = ma2x5x
endif

IPIPE_COMPONENTS ?= ../../projects/Ipipe2/components
IPIPE_PLUGINS_BASE ?= ../../projects/Ipipe2/plugins
IPIPE_PLUGINS_BASE_ARCH ?= ../../projects/Ipipe2/plugins/arch/$(SOC_REV_DRV)
IPIPE_TEST_COMPONENTS ?= ../../projects/Ipipe2/tests/arch/$(SOC_REV_DRV)/common
IPIPE_APPS_COMPONENTS ?= ../../projects/Ipipe2/apps/arch/$(SOC_REV_DRV)/common

DirAppOutput    = ./output
