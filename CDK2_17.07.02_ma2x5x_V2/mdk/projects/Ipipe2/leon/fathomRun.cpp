/*
 * fathom.c
 *
 *  Created on: Jun 24, 2016
 *      Author: ian-movidius
 */

#include <stdio.h>
#include <stdlib.h>
#include <OsDrvCpr.h>
#include <OsDrvSvu.h>
#include <OsDrvShaveL2Cache.h>
#include <swcShaveLoaderLocal.h>
#include <DrvLeonL2C.h>

#include <mvTensor.h>
#include <mvTensorDebug.h>
#include "fathomRun.h"

#include "image.h"

#ifndef DDR_DATA
#define DDR_DATA __attribute__((section(".ddr.data")))
#endif

#define BLOB_STRING_LENGTH 100

//DynamicSubModule_t DynamicContext_t
extern DynamicContext_t MODULE_DATA(mvTensor);

//For use with readBlob, as we can only read unaligned data if the data is at least aligned with the data type. Use the data type you want to read from the blob.
unsigned char x_char;
unsigned int x_uint;
unsigned short x_short;

double gCallDuration;

//#define DPRINTF(...) printf(__VA_ARGS__)
#define DPRINTF(...)
DynamicContext_t localModule[MV_TENSOR_MAX_SHAVES];
DynamicContextInstances_elm localModulePrivD[MV_TENSOR_MAX_SHAVES];

extern u32 mvTensor0_printAddress;
extern rtems_id fathomSemId;

u64 busyShavesBitMask;

void generateMvTensorCalls(unsigned char * blobNetworkStart, unsigned int total_stages,
    unsigned int offset_to_weights, t_MvTensorParam * fathomNet,
    t_MvTensorMyriadResources* myriadResourcePool,
    t_MvMatMulMyriadResources* matmulResourcesPool,
    t_mvTensorGenData* inputStruct, t_mvTensorGenData* weightsStruct,
    t_mvTensorGenData* biasStruct, t_mvTensorGenData* outputStruct,
    t_MvTensorOp* preOpStruct, t_MvTensorOp* opStruct , t_MvTensorOp* postOpStruct,
    t_MvTensorDebugInfo* dbgInfoArray, FathomRunConfig* fathomResources, short first_shave,
    short last_shave, void* inputTensor, void* outputTensor,
    unsigned int cache_memory_size, unsigned int scratch_memory_size,
    char *cache_memory_ptr, char *scratch_memory_ptr, unsigned int *offset_tracker);

unsigned char* readBlobString(unsigned char* location, int size, unsigned int* offset);

template <typename T> T readBlob(unsigned char* location, T* data_type, unsigned int* offset);

t_MvTensorMyriadResources* configureMyriad(int firstShave, int lastShave, int dmaAgent,
    int dataPartition, int instrPartition, t_MvTensorMyriadResources* stageResources, dmaTransactionList_t* dmaTransactions);

t_MvMatMulMyriadResources *configureMatMulResources(unsigned int cache_memory_size, unsigned int scratch_memory_size,
                                                    char *cache_memory_ptr, char *scratch_memory_ptr,
                                                    t_MvMatMulMyriadResources* matmulResources);

void grabShaves(u64* shavesEnableMask, swcShaveUnit_t svuList[], u32 noShaves)
{
    rtems_semaphore_obtain(fathomSemId, RTEMS_WAIT, RTEMS_NO_TIMEOUT);

    u64 shavesEnableMaskTmp = *shavesEnableMask;

    // Check if the requested Shaves are already available otherwise
    // try to grab as many as we can.
    if ((busyShavesBitMask | shavesEnableMaskTmp) ==
        (busyShavesBitMask + shavesEnableMaskTmp))
    {
        busyShavesBitMask |= shavesEnableMaskTmp;
    }
    else
    {
        shavesEnableMaskTmp = 0;
        u32 s = 0;
        // Look for free Shaves
        for (u32 i = 0; (i < MV_TENSOR_MAX_SHAVES) && (s < noShaves); i++)
        {
            svuList[i] = 0;
            if ((busyShavesBitMask & (1 << i)) == 0)
            {
                shavesEnableMaskTmp |= (1 << i);
                svuList[i] = i;
                s++;
            }
        }

        if (s == noShaves)
        {
            // Found enough free Shaves so we mark them as busy
            *shavesEnableMask = shavesEnableMaskTmp;
            busyShavesBitMask |= shavesEnableMaskTmp;
        }
        else
        {
            // Not enough free Shaves found
            *shavesEnableMask = 0;
        }

    }

    rtems_semaphore_release(fathomSemId);
}

void dropShaves(u64 shavesEnableMask)
{
    // No need to mutex here because the freeing order doesn't matter
    busyShavesBitMask ^= shavesEnableMask;
}

void openShaves(swcShaveUnit_t* shaves, u32 shaveNo)
{
    int status = OsDrvSvuOpenShaves(shaves, shaveNo, OS_MYR_PROTECTION_SEM);
    ErrorManager::Assert(status == OS_MYR_DRV_SUCCESS, "openShaves function failed");
}

void closeShaves(swcShaveUnit_t* shaves, u32 shaveNo)
{
    int status = OsDrvSvuCloseShaves(shaves, shaveNo);
    ErrorManager::Assert(status == OS_MYR_DRV_SUCCESS, "closeShaves function failed");
}

void enableShaves(t_MvTensorMyriadResources* res, u64 shavesEnableMask)
{
    //printf("enableShaves: 0x%llx\n", shavesEnableMask);
    int status = OsDrvCprTurnOnShaveMask(shavesEnableMask);
    ErrorManager::Assert(status == RTEMS_SUCCESSFUL, "enableShaves function failed");

    // Set back stuff that was lost when the power islands went off
    for (int i = res->firstShave; i <= res->lastShave; i++)
    {
        swcDynSetShaveWindows(&localModule[i], i);
        OsDrvShaveL2CSetNonWindowedPartition((shaveId_t)i, res->dataPartitionNo, NON_WINDOWED_DATA_PARTITION);
        OsDrvShaveL2CSetNonWindowedPartition((shaveId_t)i, res->instrPartitionNo, NON_WINDOWED_INSTRUCTIONS_PARTITION);
    }
}

void disableShaves(t_MvTensorMyriadResources* res, u64 shavesEnableMask)
{
    (void)res;
    int status = OsDrvCprTurnOffShaveMask(shavesEnableMask);
    ErrorManager::Assert(status == RTEMS_SUCCESSFUL, "disableShaves function failed");
}

void saveTensorData(u32 first_shave, u32 noShaves)
{
    for (u32 i = 0; i < noShaves; i++)
    {
        memcpy(&localModule[i + first_shave], &MODULE_DATA(mvTensor),sizeof(DynamicContext_t));
        memcpy(&localModulePrivD[i + first_shave], MODULE_DATA(mvTensor).instancesData, sizeof(localModulePrivD[i + first_shave]));
        localModule[i + first_shave].instancesData = &localModulePrivD[i + first_shave];
    }
}

char ** FathomRunInformation(){
  return OptimizationNames();
}

int FathomRun(unsigned char * blob, int blob_size, void * inputTensor, void * outputTensor,
    FathomRunConfig * fathomResources, u8 * timings,  u8 * debugBuffer,
    unsigned int cache_memory_size, unsigned int scratch_memory_size,
    char *cache_memory_ptr, char *scratch_memory_ptr, int print_times, int debug, FathomRunCallback cb){

  unsigned int offset_tracker = 0;

  double callDuration = 0;
  
  (void)print_times;
  offset_tracker = BLOB_FILE_SIZE_OFFSET + sizeof(blob_size);

  //Read Header items
  unsigned int version = readBlob(&blob[offset_tracker], &x_uint, &offset_tracker);
  unsigned char * network_name = readBlobString(&blob[offset_tracker], BLOB_STRING_LENGTH, &offset_tracker);
  unsigned char * report_dir = readBlobString(&blob[offset_tracker], BLOB_STRING_LENGTH, &offset_tracker);

  unsigned int stage_count = readBlob(&blob[offset_tracker], &x_uint, &offset_tracker);
  unsigned int offset_to_weights = readBlob(&blob[offset_tracker], &x_uint, &offset_tracker);

  if(debug) {
    DPRINTF("BLOB LOCATION %x\n",blob);
    DPRINTF("FATHOM VERSION HEX %x\n",version);
    DPRINTF("Fathom v%u: '%s' .\n",version, network_name);
    DPRINTF("Outputting to:\n %s\n", report_dir);
  }

  unsigned short first_shave = (unsigned short)readBlob(&blob[offset_tracker], &x_short, &offset_tracker); // Myriad Resources from Blob - Unused right now.
  unsigned short last_shave = (unsigned short)readBlob(&blob[offset_tracker], &x_short, &offset_tracker); // Myriad Resources from Blob - Unused right now.

  if (debug)
  {
    DPRINTF("Shaves: %d:%d\n", first_shave, last_shave);
  }

  readBlobString(&blob[offset_tracker], 12, &offset_tracker); // Myriad Resources from Blob - Unused right now.

  t_MvTensorDebugInfo * dbgInfo = (t_MvTensorDebugInfo *)malloc(stage_count * sizeof(t_MvTensorDebugInfo));
  assert(dbgInfo != nullptr);

  if(debugBuffer){
    for(u32 i = 0; i != stage_count; i++){
      dbgInfo[i].debugMsg = (char *)debugBuffer;
      dbgInfo[0].ms = 0;
    }
    ErrorManager::Init((char *)debugBuffer);
  }else{
    for(u32 i = 0; i != stage_count; i++){
      dbgInfo[i].debugMsg = NULL;
      dbgInfo[0].ms = 0;
    }
    ErrorManager::Init(NULL);
  }
  
  // Generate MvTensor Calls & Execute.
  t_MvTensorParam *fathomNet = (t_MvTensorParam *)malloc(stage_count * sizeof(t_MvTensorParam));
  ErrorManager::Assert(fathomNet != nullptr, "malloc failed in FathomRun");
  t_mvTensorGenData *inputStruct = (t_mvTensorGenData *)malloc(stage_count * sizeof(t_mvTensorGenData));
  ErrorManager::Assert(inputStruct != nullptr, "malloc failed in FathomRun");
  t_mvTensorGenData *outputStruct = (t_mvTensorGenData *)malloc(stage_count * sizeof(t_mvTensorGenData));
  ErrorManager::Assert(outputStruct != nullptr, "malloc failed in FathomRun");
  t_mvTensorGenData *weightsStruct = (t_mvTensorGenData *)malloc(stage_count * sizeof(t_mvTensorGenData));
  ErrorManager::Assert(weightsStruct != nullptr, "malloc failed in FathomRun");
  t_mvTensorGenData *biasStruct = (t_mvTensorGenData *)malloc(stage_count * sizeof(t_mvTensorGenData));
  ErrorManager::Assert(biasStruct != nullptr, "malloc failed in FathomRun");
  t_MvTensorOp *preOpStruct = (t_MvTensorOp *)malloc(stage_count * sizeof(t_MvTensorOp));
  ErrorManager::Assert(preOpStruct != nullptr, "malloc failed in FathomRun");
  t_MvTensorOp *opStruct = (t_MvTensorOp *)malloc(stage_count * sizeof(t_MvTensorOp));
  ErrorManager::Assert(opStruct != nullptr, "malloc failed in FathomRun");
  t_MvTensorOp *postOpStruct = (t_MvTensorOp *)malloc(stage_count * sizeof(t_MvTensorOp));
  ErrorManager::Assert(postOpStruct != nullptr, "malloc failed in FathomRun");
  t_MvTensorMyriadResources * myriadResourcePool = (t_MvTensorMyriadResources *)malloc(stage_count * sizeof(t_MvTensorMyriadResources));
  ErrorManager::Assert(myriadResourcePool != nullptr, "malloc failed in FathomRun");
  t_MvMatMulMyriadResources *matmulResourcesPool = (t_MvMatMulMyriadResources *)malloc(stage_count * sizeof(t_MvMatMulMyriadResources));
  ErrorManager::Assert(matmulResourcesPool != nullptr, "malloc failed in FathomRun");

  // Build mask based on the Shaves preferred by the blob
  const u32 noShaves = last_shave - first_shave + 1;
  swcShaveUnit_t svuList[noShaves];
  u64 shavesEnableMask = 0;
  for (u32 i = 0; i < noShaves; i++)
  {
      svuList[i] = first_shave + i;
      shavesEnableMask |= 1 << (first_shave + i);
  }

  // Ask for free Shaves
  grabShaves(&shavesEnableMask, svuList, noShaves);
  if (shavesEnableMask == 0)
      return -1;

  // Adjust the Shave range to the actual chosen ones
  first_shave = svuList[0];
  last_shave = svuList[noShaves - 1];

  saveTensorData(first_shave, noShaves);
  
  u16* dist=Image_processor(inputTensor,648,486);

  generateMvTensorCalls(blob, stage_count, offset_to_weights, fathomNet, myriadResourcePool, matmulResourcesPool,
                        inputStruct, weightsStruct, biasStruct, outputStruct,
                        preOpStruct, opStruct, postOpStruct, dbgInfo, fathomResources, first_shave, last_shave, dist, outputTensor,
                        cache_memory_size, scratch_memory_size, cache_memory_ptr, scratch_memory_ptr, &offset_tracker);
  
  printf("MvTensor running on S%d:S%d\n", first_shave, last_shave);
  DPRINTF("MvTensor Shaves mask: 0x%04llx\n", shavesEnableMask);
  DPRINTF("----------------------------------\n");

  openShaves(svuList, noShaves);
  enableShaves(myriadResourcePool, shavesEnableMask);

  for (u32 i = 0; i < noShaves; i++)
      swcSetupDynShaveAppsComplete(&localModule[first_shave + i], &svuList[i], 1);
  DPRINTF("================PARSED BLOB================\n");

  for(unsigned int stage = 0; stage < stage_count; stage++)
  {
     
      enableShaves(myriadResourcePool, shavesEnableMask);
      mvTensor(&fathomNet[stage]);
      disableShaves(myriadResourcePool, shavesEnableMask);

      DPRINTF("MvTensor Call#: %d/%d OpType: %x\n", stage + 1, stage_count, fathomNet[stage].op->type);
      DPRINTF("MvTensor done in: %f ms\n", fathomNet[stage].debugInfo->ms);

      if (cb){
          cb(stage);
      }

      // Will be false on Deploy Mode, only trigger on wet run.
      if (timings)
      {
          void* time = (void *)(((char *)timings) + stage*4);
          float time_ms = fathomNet[stage].debugInfo->ms;
          memcpy(time, (void*)&time_ms, sizeof(float));
      }
      callDuration += fathomNet[stage].debugInfo->ms;
 }




  OsDrvShaveL2CachePartitionFlush(myriadResourcePool->dataPartitionNo, PERFORM_INVALIDATION);
  OsDrvShaveL2CachePartitionInvalidate(myriadResourcePool->instrPartitionNo);

  // Free up resources
  for (u32 i = 0; i < noShaves; i++)
  {
      swcCleanupDynShaveApps(&localModule[svuList[i]]);
  }
  closeShaves(svuList, noShaves);
  dropShaves(shavesEnableMask);

  DrvLL2CFlushOpOnAddrRange(LL2C_OPERATION_INVALIDATE, 0,
          (u32)outputStruct[stage_count-1].data,
          (u32)outputStruct[stage_count-1].data +
          (outputStruct[stage_count-1].dimX *
           outputStruct[stage_count-1].dimY *
           outputStruct[stage_count-1].dimZ) * sizeof(fp16));
  DPRINTF("----------------------------------\n");
  printf("MvTensor total time: %f ms\n", callDuration);

  free(fathomNet);
  free(inputStruct);
  free(outputStruct);
  free(weightsStruct);
  free(biasStruct);
  free(preOpStruct);
  free(opStruct);
  free(postOpStruct);
  free(dbgInfo);
  free(myriadResourcePool);
  free(matmulResourcesPool);
  
  free(dist);
  
  gCallDuration = callDuration;
  
  return 0;

}

// TODO: Reduce the crazy amount of parameters here..

void generateMvTensorCalls(unsigned char * blobNetworkStart, unsigned int total_stages, unsigned int offset_to_weights, t_MvTensorParam * fathomNet,
    t_MvTensorMyriadResources * myriadResourcePool, t_MvMatMulMyriadResources* matmulResourcesPool,
    t_mvTensorGenData * inputStruct, t_mvTensorGenData * weightsStruct, t_mvTensorGenData * biasStruct, t_mvTensorGenData * outputStruct,
    t_MvTensorOp * preOpStruct, t_MvTensorOp * opStruct , t_MvTensorOp * postOpStruct, t_MvTensorDebugInfo * dbgInfoArray, FathomRunConfig * fathomResources,
    short first_shave, short last_shave, void * inputTensor, void * outputTensor,
    unsigned int cache_memory_size, unsigned int scratch_memory_size,
    char *cache_memory_ptr, char *scratch_memory_ptr, unsigned int *offset_tracker ){

  for(unsigned int stage = 0; stage < total_stages; stage++){
    //Name
    unsigned char * StageName = readBlobString(&blobNetworkStart[*offset_tracker], BLOB_STRING_LENGTH, offset_tracker);

    //StageType
    opStruct[stage].type  = (t_MvTensorOpType)readBlob(&blobNetworkStart[*offset_tracker], &x_char, offset_tracker);
    opStruct[stage].optMask  = (t_MvTensorOpType)readBlob(&blobNetworkStart[*offset_tracker], &x_uint, offset_tracker);
    opStruct[stage].radixX = (int)(readBlob(&blobNetworkStart[*offset_tracker], &x_char, offset_tracker));
    opStruct[stage].radixY = (int)readBlob(&blobNetworkStart[*offset_tracker], &x_char, offset_tracker);
    opStruct[stage].strideX = (int)readBlob(&blobNetworkStart[*offset_tracker], &x_char, offset_tracker);
    opStruct[stage].strideY = (int)readBlob(&blobNetworkStart[*offset_tracker], &x_char, offset_tracker);

    //padStyle
    opStruct[stage].padX = (t_MvTensorPaddStyle)readBlob(&blobNetworkStart[*offset_tracker], &x_char, offset_tracker);
    opStruct[stage].padY = (t_MvTensorPaddStyle)readBlob(&blobNetworkStart[*offset_tracker], &x_char, offset_tracker);
    opStruct[stage].paddStyle = (t_MvTensorPaddStyle)readBlob(&blobNetworkStart[*offset_tracker], &x_char, offset_tracker);

    opStruct[stage].opX = 0;

    // Dimensions for In, Taps, Out
    inputStruct[stage].dimX = readBlob(&blobNetworkStart[*offset_tracker], &x_uint, offset_tracker);
    inputStruct[stage].dimY = readBlob(&blobNetworkStart[*offset_tracker], &x_uint, offset_tracker);
    inputStruct[stage].dimZ = readBlob(&blobNetworkStart[*offset_tracker], &x_uint, offset_tracker);

    weightsStruct[stage].dimX = readBlob(&blobNetworkStart[*offset_tracker], &x_uint, offset_tracker);
    weightsStruct[stage].dimY = readBlob(&blobNetworkStart[*offset_tracker], &x_uint, offset_tracker);
    weightsStruct[stage].dimZ = readBlob(&blobNetworkStart[*offset_tracker], &x_uint, offset_tracker);

    outputStruct[stage].dimX = readBlob(&blobNetworkStart[*offset_tracker], &x_uint, offset_tracker);
    outputStruct[stage].dimY = readBlob(&blobNetworkStart[*offset_tracker], &x_uint, offset_tracker);
    outputStruct[stage].dimZ = readBlob(&blobNetworkStart[*offset_tracker], &x_uint, offset_tracker);

    // Strides for those same buffers
    inputStruct[stage].dimXStride = readBlob(&blobNetworkStart[*offset_tracker], &x_uint, offset_tracker);
    inputStruct[stage].dimYStride = readBlob(&blobNetworkStart[*offset_tracker], &x_uint, offset_tracker);
    inputStruct[stage].dimZStride = readBlob(&blobNetworkStart[*offset_tracker], &x_uint, offset_tracker);

    weightsStruct[stage].dimXStride = readBlob(&blobNetworkStart[*offset_tracker], &x_uint, offset_tracker);
    weightsStruct[stage].dimYStride = readBlob(&blobNetworkStart[*offset_tracker], &x_uint, offset_tracker);
    weightsStruct[stage].dimZStride = readBlob(&blobNetworkStart[*offset_tracker], &x_uint, offset_tracker);

    outputStruct[stage].dimXStride = readBlob(&blobNetworkStart[*offset_tracker], &x_uint, offset_tracker);
    outputStruct[stage].dimYStride = readBlob(&blobNetworkStart[*offset_tracker], &x_uint, offset_tracker);
    outputStruct[stage].dimZStride = readBlob(&blobNetworkStart[*offset_tracker], &x_uint, offset_tracker);

#if DEBUG
    DPRINTF("Input X Y Z DIMs:  %i %i %i\n",inputStruct[stage].dimX, inputStruct[stage].dimY, inputStruct[stage].dimZ);
    DPRINTF("Weight X Y Z DIMs:  %i %i %i\n",weightsStruct[stage].dimX, weightsStruct[stage].dimY, weightsStruct[stage].dimZ);
    DPRINTF("Output X Y Z DIMs:  %i %i %i\n",outputStruct[stage].dimX, outputStruct[stage].dimY, outputStruct[stage].dimZ);
    DPRINTF("Input X Y Z Strides:  %i %i %i\n",inputStruct[stage].dimXStride, inputStruct[stage].dimYStride, inputStruct[stage].dimZStride);
    DPRINTF("Weight X Y Z Strides:  %i %i %i\n",weightsStruct[stage].dimXStride, weightsStruct[stage].dimYStride, weightsStruct[stage].dimZStride);
    DPRINTF("Output X Y Z Strides:  %i %i %i\n\n",outputStruct[stage].dimXStride, outputStruct[stage].dimYStride, outputStruct[stage].dimZStride);
#endif

    //dataType
    readBlob(&blobNetworkStart[*offset_tracker], &x_char, offset_tracker); // Read input datatype (we don't assign it just yet)
    inputStruct[stage].dataType = t_fp16;
    weightsStruct[stage].dataType = t_fp16;
    outputStruct[stage].dataType = t_fp16;

    //InternalPrecision
    readBlob(&blobNetworkStart[*offset_tracker], &x_char, offset_tracker); // Read internal precision datatype (we don't assign it just yet)

    //storageOrder
    switch(readBlob(&blobNetworkStart[*offset_tracker], &x_char, offset_tracker)){
      default:  //orderYXZ
        inputStruct[stage].storageOrder = orderYXZ;
        weightsStruct[stage].storageOrder = orderXYZ;   //TODO: Are these valid?
        outputStruct[stage].storageOrder = orderYXZ;
        if(inputStruct[stage].dataType == 0){
          inputStruct[stage].storageOrder = orderYXZ;
          weightsStruct[stage].storageOrder = orderXYZ;   //TODO: Are these valid?
          outputStruct[stage].storageOrder = orderYXZ;
        }
        break;
    }

    //The meaning of index is now this:
    // 0 - No data, pointer will be 0
    // 1 - Pointer is an offset into inputTensor
    // 2 - Pointer is an offset into outputTensor
    // 3 - Pointer is an offset into blob data
    // 4 - Pointer is an offset into fathomBSS
    //input pointer
    void *ptrs[4] = {inputTensor, outputTensor, blobNetworkStart + offset_to_weights, fathomResources->fathomBSS};
    unsigned int iPointer = (unsigned int)readBlob(&blobNetworkStart[*offset_tracker], &x_uint, offset_tracker);
    short iIndex = (short)readBlob(&blobNetworkStart[*offset_tracker], &x_short, offset_tracker);
    if(iIndex > 4)  // Indexes > 4 have the same meaning of 4 (offset into fathomBSS)
      iIndex = 4;   // They exist for the new hardware, they are not useful for us
    inputStruct[stage].data = iIndex >= 1 && iIndex <= 4 ? (char *)ptrs[iIndex-1] + iPointer : 0;

    //tap pointer
    unsigned int wPointer = (unsigned int)readBlob(&blobNetworkStart[*offset_tracker], &x_uint, offset_tracker);
    short wIndex = (short)readBlob(&blobNetworkStart[*offset_tracker], &x_short, offset_tracker);
    if(wIndex > 4)
      wIndex = 4;
    weightsStruct[stage].data = wIndex >= 1 && wIndex <= 4 ?(char *)ptrs[wIndex-1] + wPointer : 0;

    //bias pointer
    unsigned int biasPointer = (unsigned int)readBlob(&blobNetworkStart[*offset_tracker], &x_uint, offset_tracker);
    short biasIndex = (short)readBlob(&blobNetworkStart[*offset_tracker], &x_short, offset_tracker);
    if(biasIndex > 4)
      biasIndex = 4;
    biasStruct[stage].data = biasIndex >= 1 && biasIndex <= 4 ? (char *)ptrs[biasIndex-1] + biasPointer : 0;

    // Parameters pointer
    unsigned int opParamsPointer = (unsigned int)readBlob(&blobNetworkStart[*offset_tracker], &x_uint, offset_tracker);
    short opParamsIndex = (short)readBlob(&blobNetworkStart[*offset_tracker], &x_short, offset_tracker);
    if(opParamsIndex > 4)
      opParamsIndex = 4;
    opStruct[stage].params = opParamsIndex >=1 && opParamsIndex <= 4 ? (void *)((char *)ptrs[opParamsIndex - 1] + opParamsPointer) : 0;

    //output pointer
    unsigned int oPointer = (unsigned int)readBlob(&blobNetworkStart[*offset_tracker], &x_uint, offset_tracker);
    short oIndex = (short)readBlob(&blobNetworkStart[*offset_tracker], &x_short, offset_tracker);
    if(oIndex > 4)
      oIndex = 4;
    outputStruct[stage].data = oIndex >= 1 && oIndex <= 4 ? (char *)ptrs[oIndex-1] + oPointer : 0;

#if DEBUG
    DPRINTF("INPUT OFFSET: %x (offset of %u)\n", inputStruct[stage].data, iPointer);
    DPRINTF("WEIGHT OFFSET: %x (offset of %u)\n", weightsStruct[stage].data, wPointer);
    DPRINTF("Output Location: %x (offset of %u) \n", outputStruct[stage].data, oPointer);
    DPRINTF("NETWORKS START: %x buff_offset %x = %p + %i\n",  blobNetworkStart , offset_to_weights, blobNetworkStart+offset_to_weights, oPointer);
#endif

    //StageType
    preOpStruct[stage].type  = (t_MvTensorOpType)readBlob(&blobNetworkStart[*offset_tracker], &x_char, offset_tracker);
    DPRINTF("PRE OP: %x\n",postOpStruct[stage].type );

    preOpStruct[stage].radixX = (int)1;
    preOpStruct[stage].radixY = (int)1;
    preOpStruct[stage].strideX = (int)1;
    preOpStruct[stage].strideY = (int)1;
    preOpStruct[stage].opX = (int)0;

    postOpStruct[stage].type  = (t_MvTensorOpType)readBlob(&blobNetworkStart[*offset_tracker], &x_char, offset_tracker);
    DPRINTF("POST OP: %i #%i\n",postOpStruct[stage].type, offset_tracker );
    unsigned int post_param1 = (unsigned int)readBlob(&blobNetworkStart[*offset_tracker], &x_uint, offset_tracker);
    // I am brutally deceiving the compiler here, but I am tired of warnings
    size_t ptr = (size_t)&post_param1;
    postOpStruct[stage].opX = *(float *)ptr;

    postOpStruct[stage].radixX = (int)1;
    postOpStruct[stage].radixY = (int)1;
    postOpStruct[stage].strideX = (int)readBlob(&blobNetworkStart[*offset_tracker], &x_short, offset_tracker);
    postOpStruct[stage].strideY = (int)readBlob(&blobNetworkStart[*offset_tracker], &x_short, offset_tracker);

    fathomNet[stage].input = &inputStruct[stage];
    fathomNet[stage].weights = &weightsStruct[stage];
    fathomNet[stage].output = &outputStruct[stage];
    fathomNet[stage].biases = &biasStruct[stage]; // todo connect this to fathom
    

    fathomNet[stage].myriadResources = configureMyriad(first_shave, last_shave, fathomResources->dmaLinkAgent,
                                                       fathomResources->dataPartitionNo,
                                                       fathomResources->instrPartitionNo,
                                                       &myriadResourcePool[stage],
                                                       fathomResources->dmaTransactions);

    fathomNet[stage].matmulResources = configureMatMulResources(cache_memory_size, scratch_memory_size,
                                                                cache_memory_ptr, scratch_memory_ptr,
                                                                &matmulResourcesPool[stage]);

    fathomNet[stage].preOp = &preOpStruct[stage];
    fathomNet[stage].op = &opStruct[stage];
    fathomNet[stage].postOp = &postOpStruct[stage];

    fathomNet[stage].debugInfo = &dbgInfoArray[stage];
  }
#if DEBUG
  DPRINTF("Finished Parsing\n");
#endif

}

t_MvTensorMyriadResources * configureMyriad(int firstShave, int lastShave, int dmaAgent, int dataPartition,
                                            int instrPartition, t_MvTensorMyriadResources* stageResources,
                                            dmaTransactionList_t* dmaTransactions){
  stageResources->firstShave = firstShave;
  stageResources->lastShave = lastShave;
  stageResources->dmaLinkAgent = dmaAgent;
  stageResources->dataPartitionNo = dataPartition;
  stageResources->instrPartitionNo = instrPartition;
  stageResources->dmaTransactions = dmaTransactions;
  return stageResources;
}

t_MvMatMulMyriadResources * configureMatMulResources(unsigned int cache_memory_size, unsigned int scratch_memory_size,
                                                     char *cache_memory_ptr, char *scratch_memory_ptr,
                                                     t_MvMatMulMyriadResources* matmulResources){
  matmulResources->cache_memory_ptr = cache_memory_ptr;
  matmulResources->scratch_memory_ptr = scratch_memory_ptr;
  matmulResources->cache_memory_size = cache_memory_size;
  matmulResources->scratch_memory_size = scratch_memory_size;
  return matmulResources;
}

/*
 * location: area to read from.
 * data_type: variable to represent T
 * size: optional variable to just read N number of characters.
 * TODO: Remove T parameter and specify in <T>. Also cast.
 */
template <typename T>
T readBlob(unsigned char * location, T * data_type, unsigned int *offset)
{
  int data_sz = sizeof(T);
  (void)data_type;

  //Special Cases:
  if (data_sz == 1){  // It's a char, so just return the first item.
      *offset = *offset+1;
    return location[0];
  }

  unsigned char buffer[data_sz];

  T parts[data_sz];
  T combined = 0;

  for(int byte = 0; byte != data_sz; byte++){
    buffer[byte] = location[byte];
    parts[byte] = ((T)buffer[byte]) << (8*byte);
    combined = combined | parts[byte];
  }
  *offset = *offset + data_sz;
  return combined;
}

unsigned char * readBlobString(unsigned char * location, int size, unsigned int *offset){
    *offset =*offset+ size;
  return location;
}
