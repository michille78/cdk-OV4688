/**************************************************************************************************

 @File         : dumpISPConfigToBinary.h
 @Author       : MT
 Date          : 01 - March - 2016
 E-mail        : xxx.xxx@movidius.com
 Copyright     : � Movidius Ltd 2016

 Description :
    Dumps Opipe configuration into binary format.
    Loads Opipe configuration from binary format.
 **************************************************************************************************/
#include <VcsHooksApi.h>
#include "DrvLeonL2C.h"
#include "dumpISPConfig.h"
#include "IspCommon.h"
#include "string.h"
#include "stdio.h"
#include "assert.h"
#include <stdlib.h>

#define IPIPE_LSC_PADDING 4
#define MAX_LSC_SIZE             ((64+IPIPE_LSC_PADDING)*64*4)
#define MAX_GAMMA_SIZE           (512 * 4)
#define MAX_LTM_SIZE             (4 * 16 * 16 * 16)
#define MAX_AE_AWB_STATS_SIZE    (sizeof(icAeAwbStats))
#define MAX_AF_STATS_SIZE        (sizeof(icAfStats))
#define FILE_NAME_MAX_SIZE       256

#define LOAD_PARAM(dest_, size_)  memmove (dest_ , fileDumpBuffer + *pu32Size, size_);  \
                                                                *pu32Size+= size_; \

#define DUMP_PARAM(source_, size_)  memmove(fileDumpBuffer + *pu32Size,  source_, size_);  \
                                                            *pu32Size += size_;\

#define CHECK_POINTER(p_, text_) if (p_ == NULL)  {\
                                                            printf(text_); \
                                                            return ED_ERROR_NULL_VALUE;\
                                                        }\

#define LOAD_SKIP(size_)               *pu32Size+= size_;

#define DUMP_SKIP(size_)               *pu32Size+= size_;

int dumpIspConfigToBinary  (char* fileDumpBuffer , uint32_t *pu32Size, Opipe *pOpipe);
int loadIspConfigFromBinary(char* fileDumpBuffer , uint32_t *pu32Size, Opipe *pOpipe);


void dumpConfiguration(Opipe *pOpipe, char * pFileName, char * pBuff)
{
    uint32_t  size;
    int ed = 0;
    //dump configuration to binary
    ed = dumpIspConfigToBinary(pBuff , &size, pOpipe);
    if (0 == ed) {
        saveMemoryToFile((uint32_t)pBuff, size, (const char *)pFileName);
        printf("Configuration dumped to file\n %s size %ld B\n", pFileName, size);
    } else {
        printf("Error dumping configuration\n");
    }
}

void loadConfiguration(Opipe *pOpipe, char * pFileName, char * pBuff)
{
    int ed = 0;
    uint32_t u32Size = 0;
    uint32_t u32BinSize = 0;

    // read first 4 bytes from bin file to get the file size
    loadMemFromFileSimple(pFileName, 4, &u32Size);
    DrvLL2CFlushOpOnAllLines(LL2C_OPERATION_INVALIDATE, 0);

    // read configuration from binary
    memset(pBuff, '\0', FILE_DUMP_BUFFER_MAX_SIZE);
    loadMemFromFileSimple(pFileName, u32Size, pBuff);
    DrvLL2CFlushOpOnAllLines(LL2C_OPERATION_INVALIDATE, 0);

    // parse buffer
    ed = loadIspConfigFromBinary(pBuff , &u32BinSize, pOpipe);

    if (0 == ed) {
       printf("Loaded configuration from [%s]\n", pFileName);
    } else {
       printf("Error loading configuration from [%s]\n", pFileName);
    }
}

int dumpIspConfigToBinary(char* fileDumpBuffer , uint32_t *pu32Size, Opipe *pOpipe)
{

    *pu32Size = 0;

    DUMP_SKIP(sizeof(uint32_t));
    DUMP_SKIP(sizeof(uint32_t));//file size - added later
    DUMP_PARAM(&pOpipe->width,    sizeof(uint32_t));
    DUMP_PARAM(&pOpipe->height,   sizeof(uint32_t));

    //[Generic Params]
    DUMP_SKIP(sizeof(uint32_t));
    DUMP_SKIP(sizeof(uint32_t));
    DUMP_SKIP(sizeof(uint32_t));//skip 32 bit pointer(Myriad is 32 bits machine)
    DUMP_SKIP(sizeof(uint32_t));
    DUMP_SKIP(sizeof(uint32_t));
    DUMP_SKIP(sizeof(uint32_t));
    DUMP_PARAM(&pOpipe->rawBits, sizeof(int32_t));
    DUMP_PARAM(&pOpipe->bayerPattern, sizeof(int32_t));

    //[Black Level Correction]
    DUMP_PARAM(&pOpipe->pBlcCfg->gr,sizeof(uint32_t));
    DUMP_PARAM(&pOpipe->pBlcCfg->r, sizeof(uint32_t));
    DUMP_PARAM(&pOpipe->pBlcCfg->b, sizeof(uint32_t));
    DUMP_PARAM(&pOpipe->pBlcCfg->gb,sizeof(uint32_t));

    //[Sigma denoise]
    DUMP_PARAM(&pOpipe->pSigmaCfg->noiseFloor,sizeof(uint32_t));
    DUMP_PARAM(&pOpipe->pSigmaCfg->thresh1P0, sizeof(uint32_t));
    DUMP_PARAM(&pOpipe->pSigmaCfg->thresh2P0, sizeof(uint32_t));
    DUMP_PARAM(&pOpipe->pSigmaCfg->thresh1P1, sizeof(uint32_t));
    DUMP_PARAM(&pOpipe->pSigmaCfg->thresh2P1, sizeof(uint32_t));
    DUMP_PARAM(&pOpipe->pSigmaCfg->thresh1P2, sizeof(uint32_t));
    DUMP_PARAM(&pOpipe->pSigmaCfg->thresh2P2, sizeof(uint32_t));
    DUMP_PARAM(&pOpipe->pSigmaCfg->thresh1P3, sizeof(uint32_t));
    DUMP_PARAM(&pOpipe->pSigmaCfg->thresh2P3, sizeof(uint32_t));

    //[Lsc]
    DUMP_PARAM(&pOpipe->pLscCfg->lscWidth,  sizeof(uint32_t));
    DUMP_PARAM(&pOpipe->pLscCfg->lscHeight, sizeof(uint32_t));
    DUMP_PARAM(&pOpipe->pLscCfg->lscStride, sizeof(uint32_t));
    DUMP_SKIP(sizeof(uint32_t));//skip 32 bit pointer(Myriad is 32 bits machine)

    //[Raw]
    DUMP_PARAM(&pOpipe->pRawCfg->gainGr,sizeof(uint32_t));
    DUMP_PARAM(&pOpipe->pRawCfg->gainR, sizeof(uint32_t));
    DUMP_PARAM(&pOpipe->pRawCfg->gainB, sizeof(uint32_t));
    DUMP_PARAM(&pOpipe->pRawCfg->gainGb,sizeof(uint32_t));

    DUMP_PARAM(&pOpipe->pRawCfg->clampGr,sizeof(uint32_t));
    DUMP_PARAM(&pOpipe->pRawCfg->clampR, sizeof(uint32_t));
    DUMP_PARAM(&pOpipe->pRawCfg->clampB, sizeof(uint32_t));
    DUMP_PARAM(&pOpipe->pRawCfg->clampGb,sizeof(uint32_t));

    DUMP_PARAM(&pOpipe->pRawCfg->grgbImbalPlatDark,   sizeof(uint32_t));
    DUMP_PARAM(&pOpipe->pRawCfg->grgbImbalDecayDark,  sizeof(uint32_t));
    DUMP_PARAM(&pOpipe->pRawCfg->grgbImbalPlatBright, sizeof(uint32_t));
    DUMP_PARAM(&pOpipe->pRawCfg->grgbImbalDecayBright,sizeof(uint32_t));
    DUMP_PARAM(&pOpipe->pRawCfg->grgbImbalThr,        sizeof(uint32_t));

    DUMP_PARAM(&pOpipe->pRawCfg->dpcAlphaHotG,  sizeof(uint32_t));
    DUMP_PARAM(&pOpipe->pRawCfg->dpcAlphaHotRb, sizeof(uint32_t));
    DUMP_PARAM(&pOpipe->pRawCfg->dpcAlphaColdG, sizeof(uint32_t));
    DUMP_PARAM(&pOpipe->pRawCfg->dpcAlphaColdRb,sizeof(uint32_t));
    DUMP_PARAM(&pOpipe->pRawCfg->dpcNoiseLevel, sizeof(uint32_t));
    DUMP_PARAM(&pOpipe->pRawCfg->outputBits,    sizeof(uint32_t));

    //[AeAwb Cfg]
    DUMP_SKIP(sizeof(uint32_t));
    DUMP_SKIP(sizeof(uint32_t));
    DUMP_SKIP(sizeof(uint32_t));
    DUMP_SKIP(sizeof(uint32_t));
    DUMP_SKIP(sizeof(uint32_t));
    DUMP_SKIP(sizeof(uint32_t));
    DUMP_SKIP(sizeof(uint32_t));
    DUMP_SKIP(sizeof(uint32_t));
    DUMP_SKIP(sizeof(uint16_t));
    DUMP_SKIP(sizeof(uint16_t));

    //[AF Cfg]
    DUMP_SKIP(sizeof(uint32_t));
    DUMP_SKIP(sizeof(uint32_t));
    DUMP_SKIP(sizeof(uint32_t));
    DUMP_SKIP(sizeof(uint32_t));
    DUMP_SKIP(sizeof(uint32_t));
    DUMP_SKIP(sizeof(uint32_t));
    DUMP_SKIP(sizeof(int32_t));
    DUMP_SKIP(sizeof(int32_t));
    DUMP_SKIP(sizeof(int32_t));
    DUMP_SKIP(sizeof(int32_t) * 11);
    DUMP_SKIP(sizeof(int32_t) * 11);

    DUMP_SKIP(sizeof(uint32_t));//skip 32 bit pointer(aeAwbStats)
    DUMP_SKIP(sizeof(uint32_t));//skip 32 bit pointer(afStats)
    DUMP_SKIP(sizeof(uint32_t));//skip 32 bit pointer(histLuma)
    DUMP_SKIP(sizeof(uint32_t));//skip 32 bit pointer(histRgb)

    //[Demosaic]
    DUMP_PARAM(&pOpipe->pDbyrCfg->dewormGradientMul,sizeof(int32_t));
    DUMP_PARAM(&pOpipe->pDbyrCfg->dewormSlope,      sizeof(uint32_t));
    DUMP_PARAM(&pOpipe->pDbyrCfg->dewormOffset,     sizeof(int32_t));
    DUMP_PARAM(&pOpipe->pDbyrCfg->lumaWeightR,      sizeof(int32_t));
    DUMP_PARAM(&pOpipe->pDbyrCfg->lumaWeightG,      sizeof(int32_t));
    DUMP_PARAM(&pOpipe->pDbyrCfg->lumaWeightB,      sizeof(int32_t));

    //[Local tone mapping]
    DUMP_PARAM(&pOpipe->pLtmCfg->thr,  sizeof(uint32_t));
    DUMP_PARAM(pOpipe->pLtmCfg->curves,sizeof(uint16_t) * 16 * 8 );

    //[Dog denoise]
    DUMP_PARAM(pOpipe->pDogCfg,        sizeof(DogCfg));
    DUMP_PARAM(pOpipe->pLumaDnsCfg,    sizeof(LumaDnsCfg));
    DUMP_SKIP(sizeof(uint32_t));
    DUMP_PARAM(pOpipe->pLumaDnsRefCfg, sizeof(LumaDnsRefCfg));

    //[Sharpen]
    DUMP_PARAM(&pOpipe->pSharpCfg->sigma,           sizeof(float));
    DUMP_PARAM(&pOpipe->pSharpCfg->sharpenCoeffs,   sizeof(uint16_t) * 4);
    DUMP_PARAM(&pOpipe->pSharpCfg->strengthDarken,  sizeof(uint16_t));
    DUMP_PARAM(&pOpipe->pSharpCfg->strengthLighten, sizeof(uint16_t));
    DUMP_PARAM(&pOpipe->pSharpCfg->alpha,           sizeof(uint16_t));
    DUMP_PARAM(&pOpipe->pSharpCfg->overshoot,       sizeof(uint16_t));
    DUMP_PARAM(&pOpipe->pSharpCfg->undershoot,      sizeof(uint16_t));
    DUMP_PARAM(&pOpipe->pSharpCfg->rangeStop0,      sizeof(uint16_t));
    DUMP_PARAM(&pOpipe->pSharpCfg->rangeStop1,      sizeof(uint16_t));
    DUMP_PARAM(&pOpipe->pSharpCfg->rangeStop2,      sizeof(uint16_t));
    DUMP_PARAM(&pOpipe->pSharpCfg->rangeStop3,      sizeof(uint16_t));
    DUMP_PARAM(&pOpipe->pSharpCfg->minThr,          sizeof(uint16_t));

    //[Chroma generation]
    DUMP_PARAM(&pOpipe->pChrGenCfg->epsilon,    sizeof(uint32_t));
    DUMP_PARAM(&pOpipe->pChrGenCfg->kr,         sizeof(uint32_t));
    DUMP_PARAM(&pOpipe->pChrGenCfg->kg,         sizeof(uint32_t));
    DUMP_PARAM(&pOpipe->pChrGenCfg->kb,         sizeof(uint32_t));
    DUMP_PARAM(&pOpipe->pChrGenCfg->lumaCoeffR, sizeof(uint32_t));
    DUMP_PARAM(&pOpipe->pChrGenCfg->lumaCoeffG, sizeof(uint32_t));
    DUMP_PARAM(&pOpipe->pChrGenCfg->lumaCoeffB, sizeof(uint32_t));
    DUMP_PARAM(&pOpipe->pChrGenCfg->pfrStrength,sizeof(uint32_t));
    DUMP_PARAM(&pOpipe->pChrGenCfg->desatOffset,sizeof(int32_t));
    DUMP_PARAM(&pOpipe->pChrGenCfg->desatSlope, sizeof(uint32_t));

    //[Median]
    DUMP_PARAM(&pOpipe->pMedCfg->kernelSize,sizeof(uint32_t));
    DUMP_PARAM(&pOpipe->pMedCfg->slope,     sizeof(uint32_t));
    DUMP_PARAM(&pOpipe->pMedCfg->offset,    sizeof(int32_t));

    //[Chroma denoise]
    DUMP_PARAM(&pOpipe->pChromaDnsCfg->th_r,           sizeof(uint32_t));
    DUMP_PARAM(&pOpipe->pChromaDnsCfg->th_g,           sizeof(uint32_t));
    DUMP_PARAM(&pOpipe->pChromaDnsCfg->th_b,           sizeof(uint32_t));
    DUMP_PARAM(&pOpipe->pChromaDnsCfg->limit,          sizeof(uint32_t));
    DUMP_PARAM(&pOpipe->pChromaDnsCfg->hEnab,          sizeof(uint32_t));
    DUMP_PARAM(&pOpipe->pChromaDnsCfg->greyDesatSlope, sizeof(uint32_t));
    DUMP_PARAM(&pOpipe->pChromaDnsCfg->greyDesatOffset,sizeof(int32_t));
    DUMP_PARAM(&pOpipe->pChromaDnsCfg->greyCr,         sizeof(uint32_t));
    DUMP_PARAM(&pOpipe->pChromaDnsCfg->greyCg,         sizeof(uint32_t));
    DUMP_PARAM(&pOpipe->pChromaDnsCfg->greyCb,         sizeof(uint32_t));
    DUMP_PARAM(&pOpipe->pChromaDnsCfg->convCoeffCenter,sizeof(uint32_t));
    DUMP_PARAM(&pOpipe->pChromaDnsCfg->convCoeffEdge,  sizeof(uint32_t));
    DUMP_PARAM(&pOpipe->pChromaDnsCfg->convCoeffCorner,sizeof(uint32_t));


    //[Color combine]
    DUMP_PARAM(&pOpipe->pColCombCfg->ccm,   sizeof(float) * 9);
    DUMP_PARAM(&pOpipe->pColCombCfg->ccmOff,sizeof(float) * 3);
    DUMP_PARAM(&pOpipe->pColCombCfg->kr,    sizeof(uint32_t));
    DUMP_PARAM(&pOpipe->pColCombCfg->kg,    sizeof(uint32_t));
    DUMP_PARAM(&pOpipe->pColCombCfg->kb,    sizeof(uint32_t));
    DUMP_SKIP(sizeof(uint32_t));

    //[Gamma]
    DUMP_PARAM(&pOpipe->pLutCfg->rangetop, sizeof(uint32_t));
    DUMP_PARAM(&pOpipe->pLutCfg->size,     sizeof(uint32_t));
    DUMP_SKIP(sizeof(uint32_t) * 2);
    DUMP_SKIP(sizeof(uint32_t));

    //[Color convert]
    DUMP_PARAM(&pOpipe->pColConvCfg->mat, 9 * sizeof(float));
    DUMP_PARAM(&pOpipe->pColConvCfg->offset, 3 * sizeof(float));

    //[Env]
    DUMP_SKIP(sizeof(uint32_t));
    DUMP_SKIP(sizeof(uint32_t));
    DUMP_SKIP(sizeof(uint32_t));

    DUMP_SKIP(sizeof(uint32_t));
    DUMP_SKIP(sizeof(uint32_t));
    DUMP_SKIP(sizeof(uint32_t));
    DUMP_SKIP(sizeof(uint32_t));
    DUMP_SKIP(sizeof(uint32_t));
    DUMP_SKIP(sizeof(uint32_t));
    DUMP_SKIP(sizeof(uint32_t));
    DUMP_SKIP(sizeof(uint16_t));
    DUMP_SKIP(sizeof(uint16_t));
    DUMP_SKIP(sizeof(uint16_t));
    DUMP_SKIP(sizeof(uint16_t));

    DUMP_SKIP(sizeof(uint32_t));
    DUMP_SKIP(sizeof(uint32_t));
    DUMP_SKIP(sizeof(uint32_t));
    DUMP_SKIP(sizeof(uint32_t));
    DUMP_SKIP(sizeof(uint32_t));
    DUMP_SKIP(sizeof(uint32_t));
    DUMP_SKIP(sizeof(uint32_t));
    DUMP_SKIP(sizeof(uint16_t));
    DUMP_SKIP(sizeof(uint16_t));
    DUMP_SKIP(sizeof(uint16_t));
    DUMP_SKIP(sizeof(uint16_t));

    DUMP_PARAM(pOpipe->pLscCfg->pLscTable,  MAX_LSC_SIZE * sizeof(uint16_t));
    DUMP_PARAM(pOpipe->pLutCfg->table,    MAX_GAMMA_SIZE * sizeof(uint16_t));
    DUMP_SKIP(MAX_AE_AWB_STATS_SIZE);
    DUMP_SKIP(MAX_AF_STATS_SIZE);

    if (pOpipe->pColCombCfg->lut3D)
    {
        DUMP_PARAM(pOpipe->pColCombCfg->lut3D,  MAX_LTM_SIZE * sizeof(uint16_t));
    }
    else
    {
        DUMP_SKIP(MAX_LTM_SIZE * sizeof(uint16_t));
    }

    uint32_t u32BinSize = *pu32Size - 16;
    // store size on the second position
    memmove(fileDumpBuffer + sizeof(uint32_t), &u32BinSize, sizeof(uint32_t));

    return 0;

}

int loadIspConfigFromBinary(char* fileDumpBuffer , uint32_t *pu32Size, Opipe *pOpipe)
{
    int result;
    uint32_t u32FrameCount = 0;
    uint32_t u32FrameId    = 0;
    uint32_t u32BinarySize = 0;

    LOAD_PARAM(&u32FrameCount,  sizeof(uint32_t));
    LOAD_PARAM(&u32BinarySize,  sizeof(uint32_t));
    DUMP_PARAM(&pOpipe->width,  sizeof(uint32_t));
    DUMP_PARAM(&pOpipe->height, sizeof(uint32_t));

    //[Generic Params]
    LOAD_PARAM(&u32FrameCount,   sizeof(uint32_t));
    LOAD_PARAM(&u32FrameId,      sizeof(uint32_t));
    LOAD_SKIP(sizeof(uint32_t));//skip 32 bit pointer(Myriad is 32 bits machine)
    LOAD_SKIP(sizeof(uint32_t));
    LOAD_SKIP(sizeof(uint32_t));
    LOAD_SKIP(sizeof(uint32_t));
    LOAD_PARAM(&pOpipe->rawBits,      sizeof(int32_t));
    LOAD_PARAM(&pOpipe->bayerPattern, sizeof(int32_t));

    //[Black level correction]
    LOAD_PARAM(&pOpipe->pBlcCfg->gr,sizeof(uint32_t));
    LOAD_PARAM(&pOpipe->pBlcCfg->r, sizeof(uint32_t));
    LOAD_PARAM(&pOpipe->pBlcCfg->b, sizeof(uint32_t));
    LOAD_PARAM(&pOpipe->pBlcCfg->gb,sizeof(uint32_t));

    //[Sigma denoise]
    LOAD_PARAM(&pOpipe->pSigmaCfg->noiseFloor,sizeof(uint32_t));
    LOAD_PARAM(&pOpipe->pSigmaCfg->thresh1P0, sizeof(uint32_t));
    LOAD_PARAM(&pOpipe->pSigmaCfg->thresh2P0, sizeof(uint32_t));
    LOAD_PARAM(&pOpipe->pSigmaCfg->thresh1P1, sizeof(uint32_t));
    LOAD_PARAM(&pOpipe->pSigmaCfg->thresh2P1, sizeof(uint32_t));
    LOAD_PARAM(&pOpipe->pSigmaCfg->thresh1P2, sizeof(uint32_t));
    LOAD_PARAM(&pOpipe->pSigmaCfg->thresh2P2, sizeof(uint32_t));
    LOAD_PARAM(&pOpipe->pSigmaCfg->thresh1P3, sizeof(uint32_t));
    LOAD_PARAM(&pOpipe->pSigmaCfg->thresh2P3, sizeof(uint32_t));

    //[Lsc]
    LOAD_PARAM(&pOpipe->pLscCfg->lscWidth,  sizeof(uint32_t));
    LOAD_PARAM(&pOpipe->pLscCfg->lscHeight, sizeof(uint32_t));
    LOAD_PARAM(&pOpipe->pLscCfg->lscStride, sizeof(uint32_t));
    LOAD_SKIP(sizeof(uint32_t));//skip 32 bit pointer(Myriad is 32 bits machine)

    //[Raw]
    LOAD_PARAM(&pOpipe->pRawCfg->gainGr,sizeof(uint32_t));
    LOAD_PARAM(&pOpipe->pRawCfg->gainR, sizeof(uint32_t));
    LOAD_PARAM(&pOpipe->pRawCfg->gainB, sizeof(uint32_t));
    LOAD_PARAM(&pOpipe->pRawCfg->gainGb,sizeof(uint32_t));

    LOAD_PARAM(&pOpipe->pRawCfg->clampGr,sizeof(uint32_t));
    LOAD_PARAM(&pOpipe->pRawCfg->clampR, sizeof(uint32_t));
    LOAD_PARAM(&pOpipe->pRawCfg->clampB, sizeof(uint32_t));
    LOAD_PARAM(&pOpipe->pRawCfg->clampGb,sizeof(uint32_t));

    LOAD_PARAM(&pOpipe->pRawCfg->grgbImbalPlatDark,   sizeof(uint32_t));
    LOAD_PARAM(&pOpipe->pRawCfg->grgbImbalDecayDark,  sizeof(uint32_t));
    LOAD_PARAM(&pOpipe->pRawCfg->grgbImbalPlatBright, sizeof(uint32_t));
    LOAD_PARAM(&pOpipe->pRawCfg->grgbImbalDecayBright,sizeof(uint32_t));
    LOAD_PARAM(&pOpipe->pRawCfg->grgbImbalThr,        sizeof(uint32_t));

    LOAD_PARAM(&pOpipe->pRawCfg->dpcAlphaHotG,  sizeof(uint32_t));
    LOAD_PARAM(&pOpipe->pRawCfg->dpcAlphaHotRb, sizeof(uint32_t));
    LOAD_PARAM(&pOpipe->pRawCfg->dpcAlphaColdG, sizeof(uint32_t));
    LOAD_PARAM(&pOpipe->pRawCfg->dpcAlphaColdRb,sizeof(uint32_t));
    LOAD_PARAM(&pOpipe->pRawCfg->dpcNoiseLevel, sizeof(uint32_t));
    LOAD_PARAM(&pOpipe->pRawCfg->outputBits,    sizeof(uint32_t));

    //[AeAwb Cfg]
    LOAD_SKIP(sizeof(uint32_t));
    LOAD_SKIP(sizeof(uint32_t));
    LOAD_SKIP(sizeof(uint32_t));
    LOAD_SKIP(sizeof(uint32_t));
    LOAD_SKIP(sizeof(uint32_t));
    LOAD_SKIP(sizeof(uint32_t));
    LOAD_SKIP(sizeof(uint32_t));
    LOAD_SKIP(sizeof(uint32_t));
    LOAD_SKIP(sizeof(uint16_t));
    LOAD_SKIP(sizeof(uint16_t));

    //[AF Cfg]
    LOAD_SKIP(sizeof(uint32_t));
    LOAD_SKIP(sizeof(uint32_t));
    LOAD_SKIP(sizeof(uint32_t));
    LOAD_SKIP(sizeof(uint32_t));
    LOAD_SKIP(sizeof(uint32_t));
    LOAD_SKIP(sizeof(uint32_t));
    LOAD_SKIP(sizeof(int32_t));
    LOAD_SKIP(sizeof(int32_t));
    LOAD_SKIP(sizeof(int32_t));
    LOAD_SKIP(sizeof(int32_t) * 11);
    LOAD_SKIP(sizeof(int32_t) * 11);

    LOAD_SKIP(sizeof(uint32_t));//skip 32 bit pointer(aeAwbStats)
    LOAD_SKIP(sizeof(uint32_t));//skip 32 bit pointer(afStats)
    LOAD_SKIP(sizeof(uint32_t));//skip 32 bit pointer(histLuma)
    LOAD_SKIP(sizeof(uint32_t));//skip 32 bit pointer(histRgb)

    //[Demosaic]
    LOAD_PARAM(&pOpipe->pDbyrCfg->dewormGradientMul,sizeof(int32_t));
    LOAD_PARAM(&pOpipe->pDbyrCfg->dewormSlope,      sizeof(uint32_t));
    LOAD_PARAM(&pOpipe->pDbyrCfg->dewormOffset,     sizeof(int32_t));
    LOAD_PARAM(&pOpipe->pDbyrCfg->lumaWeightR,      sizeof(int32_t));
    LOAD_PARAM(&pOpipe->pDbyrCfg->lumaWeightG,      sizeof(int32_t));
    LOAD_PARAM(&pOpipe->pDbyrCfg->lumaWeightB,      sizeof(int32_t));

    //[Local tone mapping]
    LOAD_PARAM(&pOpipe->pLtmCfg->thr,  sizeof(uint32_t));
    LOAD_PARAM(pOpipe->pLtmCfg->curves,sizeof(uint16_t) * 16 * 8 );

    //[Dog denoise]
    LOAD_PARAM(pOpipe->pDogCfg,        sizeof(DogCfg));
    LOAD_PARAM(pOpipe->pLumaDnsCfg,    sizeof(LumaDnsCfg));
    LOAD_SKIP(sizeof(uint32_t));
    LOAD_PARAM(pOpipe->pLumaDnsRefCfg, sizeof(LumaDnsRefCfg));

    //[Sharpen]
    LOAD_PARAM(&pOpipe->pSharpCfg->sigma,           sizeof(float));
    LOAD_PARAM(&pOpipe->pSharpCfg->sharpenCoeffs,   sizeof(uint16_t) * 4);
    LOAD_PARAM(&pOpipe->pSharpCfg->strengthDarken,  sizeof(uint16_t));
    LOAD_PARAM(&pOpipe->pSharpCfg->strengthLighten, sizeof(uint16_t));
    LOAD_PARAM(&pOpipe->pSharpCfg->alpha,           sizeof(uint16_t));
    LOAD_PARAM(&pOpipe->pSharpCfg->overshoot,       sizeof(uint16_t));
    LOAD_PARAM(&pOpipe->pSharpCfg->undershoot,      sizeof(uint16_t));
    LOAD_PARAM(&pOpipe->pSharpCfg->rangeStop0,      sizeof(uint16_t));
    LOAD_PARAM(&pOpipe->pSharpCfg->rangeStop1,      sizeof(uint16_t));
    LOAD_PARAM(&pOpipe->pSharpCfg->rangeStop2,      sizeof(uint16_t));
    LOAD_PARAM(&pOpipe->pSharpCfg->rangeStop3,      sizeof(uint16_t));
    LOAD_PARAM(&pOpipe->pSharpCfg->minThr,          sizeof(uint16_t));

    //[Chroma generation]
    LOAD_PARAM(&pOpipe->pChrGenCfg->epsilon,    sizeof(uint32_t));
    LOAD_PARAM(&pOpipe->pChrGenCfg->kr,         sizeof(uint32_t));
    LOAD_PARAM(&pOpipe->pChrGenCfg->kg,         sizeof(uint32_t));
    LOAD_PARAM(&pOpipe->pChrGenCfg->kb,         sizeof(uint32_t));
    LOAD_PARAM(&pOpipe->pChrGenCfg->lumaCoeffR, sizeof(uint32_t));
    LOAD_PARAM(&pOpipe->pChrGenCfg->lumaCoeffG, sizeof(uint32_t));
    LOAD_PARAM(&pOpipe->pChrGenCfg->lumaCoeffB, sizeof(uint32_t));
    LOAD_PARAM(&pOpipe->pChrGenCfg->pfrStrength,sizeof(uint32_t));
    LOAD_PARAM(&pOpipe->pChrGenCfg->desatOffset,sizeof(int32_t));
    LOAD_PARAM(&pOpipe->pChrGenCfg->desatSlope, sizeof(uint32_t));

    //[Median]
    LOAD_PARAM(&pOpipe->pMedCfg->kernelSize,sizeof(uint32_t));
    LOAD_PARAM(&pOpipe->pMedCfg->slope,     sizeof(uint32_t));
    LOAD_PARAM(&pOpipe->pMedCfg->offset,    sizeof(int32_t));

    //[Chroma denoise]
    LOAD_PARAM(&pOpipe->pChromaDnsCfg->th_r,           sizeof(uint32_t));
    LOAD_PARAM(&pOpipe->pChromaDnsCfg->th_g,           sizeof(uint32_t));
    LOAD_PARAM(&pOpipe->pChromaDnsCfg->th_b,           sizeof(uint32_t));
    LOAD_PARAM(&pOpipe->pChromaDnsCfg->limit,          sizeof(uint32_t));
    LOAD_PARAM(&pOpipe->pChromaDnsCfg->hEnab,          sizeof(uint32_t));
    LOAD_PARAM(&pOpipe->pChromaDnsCfg->greyDesatSlope, sizeof(uint32_t));
    LOAD_PARAM(&pOpipe->pChromaDnsCfg->greyDesatOffset,sizeof(int32_t));
    LOAD_PARAM(&pOpipe->pChromaDnsCfg->greyCr,         sizeof(uint32_t));
    LOAD_PARAM(&pOpipe->pChromaDnsCfg->greyCg,         sizeof(uint32_t));
    LOAD_PARAM(&pOpipe->pChromaDnsCfg->greyCb,         sizeof(uint32_t));
    LOAD_PARAM(&pOpipe->pChromaDnsCfg->convCoeffCenter,sizeof(uint32_t));
    LOAD_PARAM(&pOpipe->pChromaDnsCfg->convCoeffEdge,  sizeof(uint32_t));
    LOAD_PARAM(&pOpipe->pChromaDnsCfg->convCoeffCorner,sizeof(uint32_t));


    //[Color combine]
    LOAD_PARAM(&pOpipe->pColCombCfg->ccm,   sizeof(float) * 9);
    LOAD_PARAM(&pOpipe->pColCombCfg->ccmOff,sizeof(float) * 3);
    LOAD_PARAM(&pOpipe->pColCombCfg->kr,    sizeof(uint32_t));
    LOAD_PARAM(&pOpipe->pColCombCfg->kg,    sizeof(uint32_t));
    LOAD_PARAM(&pOpipe->pColCombCfg->kb,    sizeof(uint32_t));
    LOAD_SKIP(sizeof(uint32_t));

    //[Gamma]
    LOAD_PARAM(&pOpipe->pLutCfg->rangetop, sizeof(uint32_t));
    LOAD_PARAM(&pOpipe->pLutCfg->size,     sizeof(uint32_t));
    LOAD_SKIP(sizeof(uint32_t) * 2);
    LOAD_SKIP(sizeof(uint32_t));

    //[Color convert]
    LOAD_PARAM(&pOpipe->pColConvCfg->mat, 9 * sizeof(float));
    LOAD_PARAM(&pOpipe->pColConvCfg->offset, 3 * sizeof(float));

    //[Env]
    LOAD_SKIP(sizeof(uint32_t));
    LOAD_SKIP(sizeof(uint32_t));
    LOAD_SKIP(sizeof(uint32_t));

    LOAD_SKIP(sizeof(uint32_t));
    LOAD_SKIP(sizeof(uint32_t));
    LOAD_SKIP(sizeof(uint32_t));
    LOAD_SKIP(sizeof(uint32_t));
    LOAD_SKIP(sizeof(uint32_t));
    LOAD_SKIP(sizeof(uint32_t));
    LOAD_SKIP(sizeof(uint32_t));
    LOAD_SKIP(sizeof(uint16_t));
    LOAD_SKIP(sizeof(uint16_t));
    LOAD_SKIP(sizeof(uint16_t));
    LOAD_SKIP(sizeof(uint16_t));

    LOAD_SKIP(sizeof(uint32_t));
    LOAD_SKIP(sizeof(uint32_t));
    LOAD_SKIP(sizeof(uint32_t));
    LOAD_SKIP(sizeof(uint32_t));
    LOAD_SKIP(sizeof(uint32_t));
    LOAD_SKIP(sizeof(uint32_t));
    LOAD_SKIP(sizeof(uint32_t));
    LOAD_SKIP(sizeof(uint16_t));
    LOAD_SKIP(sizeof(uint16_t));
    LOAD_SKIP(sizeof(uint16_t));
    LOAD_SKIP(sizeof(uint16_t));

    LOAD_PARAM(pOpipe->pLscCfg->pLscTable,  MAX_LSC_SIZE * sizeof(uint16_t));
    LOAD_PARAM(pOpipe->pLutCfg->table, MAX_GAMMA_SIZE * sizeof(uint16_t));
    LOAD_SKIP(MAX_AE_AWB_STATS_SIZE);
    LOAD_SKIP(MAX_AF_STATS_SIZE);
    if (pOpipe->pColCombCfg->lut3D) {
        LOAD_PARAM(pOpipe->pColCombCfg->lut3D, MAX_LTM_SIZE * sizeof(uint16_t));
    }
    else
    {
        LOAD_SKIP(MAX_LTM_SIZE * sizeof(uint16_t));
    }

    if ((u32BinarySize + 2 * sizeof(uint32_t)) == *pu32Size) {
        printf("\nMatch size %ld\n", *pu32Size);
        result = 0;
    } else {
        result = 1;
    }

    return result;
}
