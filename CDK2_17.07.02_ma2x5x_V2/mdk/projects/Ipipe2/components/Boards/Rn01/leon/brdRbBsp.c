#ifndef brdRbBsp_H__
#define brdRbBsp_H__

#include "brdRbDefines.h"

const GpioConfigDescriptor pmdCamGpios[] =
{
//    {
//        .configType = POWER_PIN,
//        .gpioNumber = Rb_V3V3_PIN,
//        .activeLevel = 0,
//        .delayMs = 0,
//    },
//    {
//        .configType = POWER_PIN,
//        .gpioNumber = Rb_V1V5_PIN,
//        .activeLevel = 0,
//        .delayMs = 500,
//    },
//    {
//        .configType = POWER_PIN,
//        .gpioNumber = Rb_V3V3_PIN,
//        .activeLevel = 1,
//        .delayMs = 0,
//    },
//    {
//        .configType = POWER_PIN,
//        .gpioNumber = Rb_V1V5_PIN,
//        .activeLevel = 1,
//        .delayMs = 500,
//    },
    {
        .configType = RESET_PIN,
        .gpioNumber = RB_GPIO_PIN_IR_CAM_RESET_N,
        .activeLevel = 0,
        .delayMs = 1,
    },
    {
        .configType = MCLK_PIN,
        .gpioNumber = RB_GPIO_PIN_IR_MCLK,
        .activeLevel = 1,
        .delayMs = 0,
    },
    {
        .configType = END, 0, 0, 0
    },
};

const GpioConfigDescriptor ov7750CamGpios[] =
{
//    {
//        .configType = POWER_PIN,
//        .gpioNumber = Rb_V2V8_PIN,
//        .activeLevel = 1,
//        .delayMs = 1,  // 1 ms after powering up reset can toggle
//    },
    {
        .configType = RESET_PIN,
        .gpioNumber = RB_GPIO_PIN_OVT_RESET,
        .activeLevel = 1,
        .delayMs = 10,   // 3.41 at 24MHz = 8192 MCLK's
    },
    {
        .configType = RESET_PIN,
        .gpioNumber = RB_GPIO_PIN_OVT_RESET,
        .activeLevel = 0,
        .delayMs = 10,   // 3.41 at 24MHz = 8192 MCLK's
    },
    {
        .configType = MCLK_PIN,
        .gpioNumber = RB_GPIO_PIN_OVT_MCLK,
        .activeLevel = 1,
        .delayMs = 0,
    },
    {
        .configType = END, 0, 0, 0
    },
};


#endif
