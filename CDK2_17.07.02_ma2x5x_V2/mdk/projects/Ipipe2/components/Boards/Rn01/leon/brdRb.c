#include <stdio.h>
#include <assert.h>
#include <DrvGpio.h>
#include <DrvDdr.h>
#include <mv_types.h>
#include <DrvTimer.h>
#include <DrvI2cMaster.h>

#include "brdRb.h"
#include "brdRbGpioDefaults.h"

// 2:  Source Specific #defines and types  (typedef,enum,struct)
// ----------------------------------------------------------------------------

#define IRQ_SRC_0   0
#define IRQ_SRC_1   1
#define IRQ_SRC_2   2
#define IRQ_SRC_3   3

// 3: Global Data (Only if absolutely necessary)
// ----------------------------------------------------------------------------

// 4: Static Local Data
// ----------------------------------------------------------------------------
// This function declaration is needed by the data initialised below
static u32 commonI2CErrorHandler(I2CM_StatusType error, u32 param1, u32 param2);

I2CM_Device i2cDevHandle[NUM_I2C_DEVICES];

tyAppDeviceHandles gAppDevHndls;

static tyI2cConfig i2c0DefaultConfiguration =
{
    .device                = IIC1_BASE_ADR,
    .sclPin                = Rb_I2C0_SCL_PIN,
    .sdaPin                = Rb_I2C0_SDA_PIN,
    .speedKhz              = Rb_I2C0_SPEED_KHZ_DEFAULT,
    .addressSize           = Rb_I2C0_ADDR_SIZE_DEFAULT,
    .errorHandler          = &commonI2CErrorHandler,
};

static tyI2cConfig i2c1DefaultConfiguration =
{
    .device                = IIC2_BASE_ADR,
    .sclPin                = Rb_I2C1_SCL_PIN,
    .sdaPin                = Rb_I2C1_SDA_PIN,
    .speedKhz              = Rb_I2C1_SPEED_KHZ_DEFAULT,
    .addressSize           = Rb_I2C1_ADDR_SIZE_DEFAULT,
    .errorHandler          = &commonI2CErrorHandler,
};

static tyI2cConfig i2c2DefaultConfiguration =
{
    .device                = IIC3_BASE_ADR,
    .sclPin                = Rb_I2C2_SCL_PIN,
    .sdaPin                = Rb_I2C2_SDA_PIN,
    .speedKhz              = Rb_I2C2_SPEED_KHZ_DEFAULT,
    .addressSize           = Rb_I2C2_ADDR_SIZE_DEFAULT,
    .errorHandler          = &commonI2CErrorHandler,
};

/******************************************************************************
 5: Functions Implementation
******************************************************************************/
s32 brdRbInitialiseI2C(tyI2cConfig * i2c0Cfg, tyI2cConfig * i2c1Cfg,tyI2cConfig * i2c2Cfg,I2CM_Device ** i2c0Dev,I2CM_Device ** i2c1Dev,I2CM_Device ** i2c2Dev)
{
    s32 ret = 0;

    // Unless the user specifies otherwise we use the default configuration
    if (i2c0Cfg == NULL)
        i2c0Cfg = &i2c0DefaultConfiguration;

    if (i2c1Cfg == NULL)
        i2c1Cfg = &i2c1DefaultConfiguration;

    if (i2c2Cfg == NULL)
        i2c2Cfg = &i2c2DefaultConfiguration;

    // Initialise the I2C device to use the I2C0 Hardware block
    ret = DrvI2cMInitFromConfig(&i2cDevHandle[0], i2c0Cfg);
    if (ret != I2CM_STAT_OK)
    {
        printf("i2c0 not inited sc = %d\n", ret);
        ret = 1<<0;
    }
    else
    {
        *i2c0Dev = &i2cDevHandle[0]; // Return the handle
    }

    // Initialise the I2C device to use the I2C1 Hardware block
    ret = DrvI2cMInitFromConfig(&i2cDevHandle[1], i2c1Cfg);
    if (ret != I2CM_STAT_OK)
    {
        printf("i2c1 not inited sc = %d\n", ret);
        ret = 1<<1;
    }
    else
    {
        *i2c1Dev = &i2cDevHandle[1]; // Return the handle
    }

    // Initialise the I2C device to use the I2C1 Hardware block
    ret = DrvI2cMInitFromConfig(&i2cDevHandle[2], i2c2Cfg);
    if (ret != I2CM_STAT_OK)
    {
        printf("i2c2 not inited sc = %d\n", ret);
        ret = 1<<2;
    }
    else
    {
        *i2c2Dev = &i2cDevHandle[2]; // Return the handle
    }

    // Also setup a common error handler for I2C
    if (i2c0Cfg->errorHandler)
        DrvI2cMSetErrorHandler(&i2cDevHandle[0], i2c0Cfg->errorHandler);

    if (i2c1Cfg->errorHandler)
        DrvI2cMSetErrorHandler(&i2cDevHandle[1], i2c1Cfg->errorHandler);

    if (i2c2Cfg->errorHandler)
        DrvI2cMSetErrorHandler(&i2cDevHandle[2], i2c2Cfg->errorHandler);

    return ret;
}

// -----------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------
// Static Function Implementations
// -----------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------

static u32 commonI2CErrorHandler(I2CM_StatusType i2cCommsError, u32 slaveAddr, u32 regAddr)
{
    slaveAddr=slaveAddr;
    regAddr=regAddr;

    if(i2cCommsError != I2CM_STAT_OK)
    {
    }
    return i2cCommsError; // Because we haven't really handled the error, pass it back to the caller
}


s32 brdRbInitializeBoard(void)
{
    DrvGpioIrqSrcDisable(IRQ_SRC_0);
    DrvGpioIrqSrcDisable(IRQ_SRC_1);
    DrvGpioIrqSrcDisable(IRQ_SRC_2);
    DrvGpioIrqSrcDisable(IRQ_SRC_3);

    DrvGpioInitialiseRange(brdRbGpioCfgDefault);

    brdRbInitialiseI2C(NULL,NULL,NULL,		    // Use Default I2C configuration
                            &gAppDevHndls.i2c0Handle,
                            &gAppDevHndls.i2c1Handle,
                            &gAppDevHndls.i2c2Handle);

    DrvDdrInitialise(NULL);
}
