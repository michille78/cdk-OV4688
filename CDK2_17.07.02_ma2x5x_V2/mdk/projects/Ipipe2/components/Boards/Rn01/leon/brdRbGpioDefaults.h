///
/// @file
/// @copyright All code copyright Movidius Ltd 2014, all rights reserved
///            For License Warranty see: common/license.txt
///
/// @brief     Default GPIO configuration for the Rb Board
///
/// Using the structure defined by this board it is possible to initialize
/// some of the GPIOS on the Rb PCB to good safe initial defaults
///
#ifndef BRD_Rb_GPIO_DEFAULTS_H_
#define BRD_Rb_GPIO_DEFAULTS_H_

#include <DrvGpioDefines.h>

// 1: Includes
// ----------------------------------------------------------------------------
// 2: Defines
// ----------------------------------------------------------------------------

// 3: Typedefs (types, enums, structs)
// ----------------------------------------------------------------------------

// 4: Local const declarations     NB: ONLY const declarations go here
// ----------------------------------------------------------------------------

static const drvGpioInitArrayType brdRbGpioCfgDefault =
{
    {
        0, 0  ,
        ACTION_UPDATE_ALL           // MAG_INT
        ,
        PIN_LEVEL_LOW
        ,
        D_GPIO_MODE_7            |
        D_GPIO_DIR_IN            |
        D_GPIO_DATA_INV_OFF      |
        D_GPIO_WAKEUP_OFF
        ,
        D_GPIO_PAD_DEFAULTS
    },
    {
        1, 1  ,
        ACTION_UPDATE_ALL           // IMU_DRD
        ,
        PIN_LEVEL_LOW
        ,
        D_GPIO_MODE_7            |
        D_GPIO_DIR_IN            |
        D_GPIO_DATA_INV_OFF      |
        D_GPIO_WAKEUP_OFF
        ,
        D_GPIO_PAD_DEFAULTS
    },
    {
        2, 3  ,
        ACTION_UPDATE_ALL           // ACCEL_INT 1 & 2
        ,
        PIN_LEVEL_LOW
        ,
        D_GPIO_MODE_7            |
        D_GPIO_DIR_IN            |
        D_GPIO_DATA_INV_OFF      |
        D_GPIO_WAKEUP_OFF
        ,
        D_GPIO_PAD_DEFAULTS
    },
    {
        6, 6  ,
        ACTION_UPDATE_ALL           // SYNC to loadboard
        ,
        PIN_LEVEL_LOW
        ,
        D_GPIO_MODE_7            |
        D_GPIO_DIR_OUT           |
        D_GPIO_DATA_INV_OFF      |
        D_GPIO_WAKEUP_OFF
        ,
        D_GPIO_PAD_DEFAULTS
    },
    {
        7, 10  ,
        ACTION_UPDATE_ALL           // SPI2 Slave to loadboard
        ,
        PIN_LEVEL_LOW
        ,
        D_GPIO_MODE_3            |
        D_GPIO_DIR_IN            |
        D_GPIO_DATA_INV_OFF      |
        D_GPIO_WAKEUP_OFF
        ,
        D_GPIO_PAD_DEFAULTS
    },
    {
        12, 13  ,
        ACTION_UPDATE_ALL           // I2C1 to BMX055 BMP180
        ,
        PIN_LEVEL_LOW
        ,
        D_GPIO_MODE_2            |
        D_GPIO_DIR_IN            |
        D_GPIO_DATA_INV_OFF      |
        D_GPIO_WAKEUP_OFF
        ,
        D_GPIO_PAD_DEFAULTS
    },
    {
        17, 17  ,
        ACTION_UPDATE_ALL           // OVT ULPM
        ,
        PIN_LEVEL_LOW
        ,
        D_GPIO_MODE_7            |
        D_GPIO_DIR_OUT           |
        D_GPIO_DATA_INV_OFF      |
        D_GPIO_WAKEUP_OFF
        ,
        D_GPIO_PAD_DEFAULTS
    },
    {
        20, 20  ,
        ACTION_UPDATE_ALL           // V3V75 rail (needed for DDR, always on)
        ,
        PIN_LEVEL_HIGH
        ,
        D_GPIO_MODE_7            |
        D_GPIO_DIR_OUT           |
        D_GPIO_DATA_INV_OFF      |
        D_GPIO_WAKEUP_OFF
        ,
        D_GPIO_PAD_DEFAULTS
    },
    {
        24, 24  ,
        ACTION_UPDATE_ALL           // V1V2 rail (needed for DDR, always on)
        ,
        PIN_LEVEL_HIGH
        ,
        D_GPIO_MODE_7            |
        D_GPIO_DIR_OUT           |
        D_GPIO_DATA_INV_OFF      |
        D_GPIO_WAKEUP_OFF
        ,
        D_GPIO_PAD_DEFAULTS
    },
    {
        21, 23  ,
        ACTION_UPDATE_ALL           // Peripheral voltage enable
        ,
        PIN_LEVEL_HIGH
        ,
        D_GPIO_MODE_7            |
        D_GPIO_DIR_OUT           |
        D_GPIO_DATA_INV_OFF      |
        D_GPIO_WAKEUP_OFF
        ,
        D_GPIO_PAD_DEFAULTS
    },
    {
        25, 25  ,
        ACTION_UPDATE_ALL           // Peripheral voltage enable OV7750 and IMU
        ,
        PIN_LEVEL_HIGH
        ,
        D_GPIO_MODE_7            |
        D_GPIO_DIR_OUT           |
        D_GPIO_DATA_INV_OFF      |
        D_GPIO_WAKEUP_OFF
        ,
        D_GPIO_PAD_DEFAULTS
    },
    {
        27, 27  ,
        ACTION_UPDATE_ALL           // PMD RESET
        ,
        PIN_LEVEL_LOW
        ,
        D_GPIO_MODE_7            |
        D_GPIO_DIR_OUT           |
        D_GPIO_DATA_INV_OFF      |
        D_GPIO_WAKEUP_OFF
        ,
        D_GPIO_PAD_DEFAULTS
    },
    {
        28, 28  ,
        ACTION_UPDATE_ALL           // OVT Shutdown
        ,
        PIN_LEVEL_LOW
        ,
        D_GPIO_MODE_7            |
        D_GPIO_DIR_OUT           |
        D_GPIO_DATA_INV_OFF      |
        D_GPIO_WAKEUP_OFF
        ,
        D_GPIO_PAD_DEFAULTS
    },
    {
        67, 67  ,
        ACTION_UPDATE_ALL           // GYRO1_INT
        ,
        PIN_LEVEL_LOW
        ,
        D_GPIO_MODE_7            |
        D_GPIO_DIR_IN            |
        D_GPIO_DATA_INV_OFF      |
        D_GPIO_WAKEUP_OFF
        ,
        D_GPIO_PAD_DEFAULTS
    },
    {
        31, 31  ,
        ACTION_UPDATE_ALL           // GYRO2_INT
        ,
        PIN_LEVEL_LOW
        ,
        D_GPIO_MODE_7            |
        D_GPIO_DIR_IN            |
        D_GPIO_DATA_INV_OFF      |
        D_GPIO_WAKEUP_OFF
        ,
        D_GPIO_PAD_DEFAULTS
    },
    {
        33, 33  ,
        ACTION_UPDATE_ALL           // PMD enable illum
        ,
        PIN_LEVEL_LOW
        ,
        D_GPIO_MODE_7            |
        D_GPIO_DIR_IN            |
        D_GPIO_DATA_INV_OFF      |
        D_GPIO_WAKEUP_OFF
        ,
        D_GPIO_PAD_DEFAULTS
    },
    {
        40, 41  ,
        ACTION_UPDATE_ALL           // CAM Clocks PMD-40 OVT-41
        ,
        PIN_LEVEL_LOW
        ,
        D_GPIO_MODE_4            |
        D_GPIO_DIR_OUT           |
        D_GPIO_DATA_INV_OFF      |
        D_GPIO_WAKEUP_OFF
        ,
        D_GPIO_PAD_DEFAULTS
    },
    {
        45, 45  ,
        ACTION_UPDATE_ALL           // OVT Strobe
        ,
        PIN_LEVEL_LOW
        ,
        D_GPIO_MODE_7            |
        D_GPIO_DIR_OUT           |
        D_GPIO_DATA_INV_OFF      |
        D_GPIO_WAKEUP_OFF
        ,
        D_GPIO_PAD_DEFAULTS
    },
    {
        49, 49  ,
        ACTION_UPDATE_ALL           // PMD Start
        ,
        PIN_LEVEL_LOW
        ,
        D_GPIO_MODE_7            |
        D_GPIO_DIR_OUT           |
        D_GPIO_DATA_INV_OFF      |
        D_GPIO_WAKEUP_OFF
        ,
        D_GPIO_PAD_DEFAULTS
    },
    {
        50, 50  ,
        ACTION_UPDATE_ALL           // OVT FSIN
        ,
        PIN_LEVEL_LOW
        ,
        D_GPIO_MODE_7            |
        D_GPIO_DIR_OUT           |
        D_GPIO_DATA_INV_OFF      |
        D_GPIO_WAKEUP_OFF
        ,
        D_GPIO_PAD_DEFAULTS
    },
    {
        60, 61  ,
        ACTION_UPDATE_ALL           // I2C0 to OVT and PMD
        ,
        PIN_LEVEL_LOW
        ,
        D_GPIO_MODE_0            |
        D_GPIO_DIR_IN            |
        D_GPIO_DATA_INV_OFF      |
        D_GPIO_WAKEUP_OFF
        ,
        D_GPIO_PAD_DEFAULTS
    },
    {
        74, 75  ,
        ACTION_UPDATE_ALL           // SPI0 Master to BOOT EEPROM
        ,
        PIN_LEVEL_LOW
        ,
        D_GPIO_MODE_0            |
        D_GPIO_DIR_OUT           |
        D_GPIO_DATA_INV_OFF      |
        D_GPIO_WAKEUP_OFF        ,

        D_GPIO_PAD_DEFAULTS
    },
    {
        79, 80,
        ACTION_UPDATE_ALL           // I2C2 to host
        ,
        PIN_LEVEL_LOW
        ,
        D_GPIO_MODE_2            |
        D_GPIO_DIR_IN            |
        D_GPIO_DATA_INV_OFF      |
        D_GPIO_WAKEUP_OFF
        ,
        D_GPIO_PAD_DEFAULTS
    },

    {
        84, 84,                  // CX3 V1V8 enable
        ACTION_UPDATE_ALL
        ,
        PIN_LEVEL_HIGH
        ,
        D_GPIO_MODE_7            |
        D_GPIO_DIR_OUT           |
        D_GPIO_DATA_INV_OFF      |
        D_GPIO_WAKEUP_OFF
        ,
        D_GPIO_PAD_DEFAULTS
    },

    // -----------------------------------------------------------------------
    // Finally we terminate the Array
    // -----------------------------------------------------------------------
    { 0, 0, ACTION_TERMINATE_ARRAY, 0, 0, 0 },
};

#endif // BRD_Rb_GPIO_DEFAULTS_H_
