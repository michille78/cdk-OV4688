
/**************************************************************************************************

 @File         : UsbSend.c
 @Author       : MT
 @Brief        : Allow display frames over USB
 Date          : 01 - March - 2015
 E-mail        : xxx.xxx@movidius.com
 Copyright     : � Movidius Srl 2014, � Movidius Ltd 2015

 Description :


 **************************************************************************************************/

/**************************************************************************************************
 ~~~ Included types first then APIs from other modules
 **************************************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <assert.h>
#include "IcTypes.h"
#include "UsbSend.h"
#include <OsDrvTimer.h>


#include "usbpumpdebug.h"
#include "usbpump_application_rtems_api.h"

//#include "utils.h"

#include "uplatformapi.h";

#include "usbpump_mempool.h";
#include "videodemo.h"

#define CMPSTR(a, b)     (strcasecmp(a, b))

#ifndef DISABLE_LEON_DCACHE
# define USBPUMP_MDK_CACHE_ENABLE		1
#else
# define USBPUMP_MDK_CACHE_ENABLE		0
#endif

#ifndef TIMEOUT
# define TIMEOUT 0
#endif

#ifndef WAIT_TIME
# define WAIT_TIME 200
#endif

#define ENDPOINT1 0
#define ENDPOINT2 1
#define ENDPOINT3 2

#define PACKET_SIZE  512
#define MAX_TRANSFER 2*1024*1024 //2MB

#define REPEAT_COUNT  1000
#define TRANSFER_SIZE (512*1024)
extern u8 __attribute__((section(".ddr.data"))) FathomBlob[];

extern VIDEODEMO_CONTEXT *    g_pVidemoDemoContext;

// Transfer buffers
static char rBuff[MAX_TRANSFER]__attribute__((aligned(64), section(".ddr_direct.bss")));
static unsigned char FathomBlobHost[BLOBSIZE] __attribute__((aligned(64), section(".ddr_direct.bss")));



extern int VideoDemo_SendFrame(sUsbTest *sUsbData);
extern int usb_in_ctrl_thread_init(UsbSend_InputCtrlCallback_Type cb_func);

sUsbTest usbStruct;


#ifdef DBG_USBSEND
static void UsbSendCalcDeltaTime(uint32_t frame_ctr);
#endif

#if 0
// Required for synchronisation between internal USB thread and our threads
static int createSemaphores()
{
	int i, status;
	for(i = 0; i < USBPUMP_VSC2APP_NUM_EP_IN; i++)
	{
		status = rtems_semaphore_create(rtems_build_name('I', 'N', '_', '0' + i), 0,
			RTEMS_SIMPLE_BINARY_SEMAPHORE, 0, &pSelf->semWriteId[i]);
		if (status != RTEMS_SUCCESSFUL)
		{
			return 1;
		}
	}

	for(i = 0; i < USBPUMP_VSC2APP_NUM_EP_OUT; i++)
	{
		status = rtems_semaphore_create(rtems_build_name('O', 'U', 'T', '0' + i), 0,
			RTEMS_SIMPLE_BINARY_SEMAPHORE, 0, &pSelf->semReadId[i]);
		if (status != RTEMS_SUCCESSFUL)
		{
			return 1;
		}
	}

	return 0;
}
#endif

// Initialization function for USB physical layer and USB Rtems DataPump
void UsbSendCreate(UsbCfg_t *cfg)
{
//    printf("\n\n UsbSendCreate \n\n\n");
	if(cfg != (UsbCfg_t *) NULL)
	{
		OsDrvUsbPhyInit(cfg->phyParamInit);

		if (UsbPump_Rtems_DataPump_Startup(cfg->dataPumpCfgInit) != NULL)
		{
			printf("\n\nUsbPump_Rtems_DataPump_Startup() init OK!\n\n\n");
		}
		else
		{
			printf("\n\nUsbPump_Rtems_DataPump_Startup() failed!\n\n\n");
			exit(1);
		}
		{
			if (usb_in_ctrl_thread_init(cfg->cb_function))
			{
				printf("\n\n usb_in_ctrl_thread_init() failed!\n\n\n");
				exit(1);
			}
		}
#if 0
                  	// create semaphores for transfer synchronization
        if(createSemaphores() != 0)
        {
           printf("Error creating semaphores");
        }
#endif
	}
}

#ifdef DBG_USBSEND
/* measure and print time (in [ms]) from previous call
 * calculation valid for sys clock = DEFAULT_APP_CLOCK_KHZ */
static void UsbSendCalcDeltaTime(uint32_t frame_ctr)
{
    static tyTimeStamp testUsbCamFrmTs;
    u32 testUsbCamFrmMs;
    u64 testUsbCamFrmTicks;

    if(frame_ctr > 0u)
    {
        if(OsDrvTimerGetElapsedTicks(&testUsbCamFrmTs, &testUsbCamFrmTicks) == MYR_DRV_SUCCESS)
        {
            testUsbCamFrmMs = (u32) ((u64) testUsbCamFrmTicks / (u32) DEFAULT_APP_CLOCK_KHZ);
            DBG_PRINTF(" -> %lu ms\n", testUsbCamFrmMs);
        }
        else
        {
            DBG_PRINTF("OsDrvTimerGetElapsedTicks error!\n\n");
        }
    }

    if(OsDrvTimerStartTicksCount(&testUsbCamFrmTs) != MYR_DRV_SUCCESS)
    {
        DBG_PRINTF("OsDrvTimerStartTicksCount error!\n\n");
    }
}
#endif // DBG_USBSEND

void UsbSendFrame(SendOutElement_t *task)
{

#if 1
InternalCbSent sendOutCbSent = task->localCallback;

    assert(NULL != task);
    assert(NULL != task->buffer);
#ifdef DBG_USBSEND
    DBG_PRINTF("\nCAM %lu av frm %lu", task->outId, task->buffer->seqNo);

    UsbSendCalcDeltaTime(task->buffer->seqNo);
#endif

    /* in case busy or sendInProgress set, skip frame
     * busy is set during structure update
     * sendInProgress is set during USB transmission */
    if((usbStruct.busy == 0) &&
       (usbStruct.sendInProgress == 0))
    {
        usbStruct.busy = 1;
        usbStruct.filled = 1;
        usbStruct.task = task;
        usbStruct.busy = 0;

        if (VideoDemo_SendFrame(&usbStruct))
        {
            usbStruct.filled = 0;
            /* release frame buffers */
            //DBG_ERROR_PRINTF("skip1!  %d %p\n", (int)task->buffer->seqNo, task->buffer);
            sendOutCbSent(task);
        }
    }
    else
    {
        /* skip the frame */
        DBG_ERROR_PRINTF("skip2! Cam%d NO:%d buf:%p\n", (int)task->outId, (int)task->buffer->seqNo, task->buffer);
        sendOutCbSent(task);
    }
#endif
}

#if 0
void * read_thread(void* ptr)
{
    int status;
    int error_status = 0;
    int blobFlag = BLOB_BUILDIN; // blobFlag = 0, will get the blob file from the host, other wise use buildin blob
   // tTaskState threadState = *(tTaskState *)ptr;
    USB_SWITCH usb_switch = *(USB_SWITCH*)ptr;
    tTaskState threadState = usb_switch.t;
	//unsigned int mode_flag = usb_switch.mode_flag;
	
    unsigned int blobMagicNum = 0;
    int transferTimeout = 0;
    BlobTransHeader * pblobTransHeader;
    unsigned char *blobPtr;
    unsigned char *netName;
    int blobOffset = 0;
    unsigned int blobDataSize = 0;
    int blobCount = 0;
    int blobLast = 0;
    int imageTransferSize = 0;
    int i;
    rtems_task_wake_after(1000); // Needed so that the datapump has time to do interface up

#ifdef BUILDIN_BLOB
    blobPtr = FathomBlob;
#else
    blobPtr = FathomBlobHost;
#endif
    
    while(1) {
		if(usb_switch.mode_flag)
		{
       printf("read thread start*****************************************\n");
       switch(threadState)
        {
            case NET_BLOB_GET:
                {   

					printf("blobFlag = %d\n",blobFlag);
                    if(blobFlag == BLOB_READY_FLAG) {
                        threadState = NET_BLOB_PARSER;
                    }
                    else {
                        //Read MagicHeader from Host, 16 byte
                        //0x1A2B3C4D, 0x55667788 , 0xDEADBEEF, BLOB_SIZE
                        printf("Read endpoint1 start!\n");
                        UsbVscAppRead(pSelf, 16, rBuff, ENDPOINT1);
						printf("Read endpoint1 stop\n");
                        status = rtems_semaphore_obtain(pSelf->semReadId[ENDPOINT1], RTEMS_WAIT, 0);
                        pblobTransHeader = (BlobTransHeader *)rBuff;
                        if(pblobTransHeader->magicNum[0] == 0x1A2B3C4D &&
                            pblobTransHeader->magicNum[1] == 0x55667788 &&
                            pblobTransHeader->magicNum[2] == 0xDEADBEEF) {
                            blobDataSize = pblobTransHeader->blobSize;
                            blobCount = (int)blobDataSize/(int)TRANSFER_SIZE;  //transfer 512K every iteration
                            blobLast = blobDataSize - blobCount*TRANSFER_SIZE;
                            blobOffset = 0;
                            threadState= NET_BLOB_READ;
							
							//return NULL; //stop rThread
                        }
                        else {
                            transferTimeout++;
                            if(transferTimeout > 100) {
                                printf("no vaild blob transfer header\n");
                                error_status++;
                            }
                        }
                    }
                }
                break;
            case NET_BLOB_READ:
                {
                    if(blobCount != 0) {
                        UsbVscAppRead(pSelf, TRANSFER_SIZE, (blobPtr+blobOffset), ENDPOINT1);
                        status = rtems_semaphore_obtain(pSelf->semReadId[ENDPOINT1], RTEMS_WAIT, 0);
                        blobCount--;
                        blobOffset += TRANSFER_SIZE;
                        DPRINTF("BLOB Transfer blobCount = %d\n", blobCount);

                    }
                    else {
                        if(blobLast != 0) {
                            UsbVscAppRead(pSelf, blobLast, (blobPtr+blobOffset), ENDPOINT1);
                            status = rtems_semaphore_obtain(pSelf->semReadId[ENDPOINT1], RTEMS_WAIT, 0);
                            blobLast = 0;
                        }
						
						//if code reach here,it indicates that the whole blob model has been received completely 
						//Just replace NET_BLOB_PARSER case,and run "Classify_Test" in NET_IMAGE_PROCESS case directly
						//[xutianyi]
                        threadState = NET_BLOB_PARSER;
                        blobFlag = BLOB_READY_FLAG;
                    }
                }
                break;
            case NET_BLOB_PARSER:
                {
                    netName = (blobPtr + 40);
                    DPRINTF("NetName is %s\n", netName);
                    if(CMPSTR(netName, "GoogleNet") == 0)
                        imageTransferSize = 224*224*3*2;
                        threadState = NET_IMAGE_PROCESS;
                }
                break;
            case NET_IMAGE_PROCESS:
                {
                    DPRINTF("Starting image read transfer\n");
                    //UsbVscAppRead(pSelf, TRANSFER_SIZE, rBuff, ENDPOINT1);
                    UsbVscAppRead(pSelf, imageTransferSize, rBuff, ENDPOINT1);
                    if(TIMEOUT)
                        status = rtems_semaphore_obtain(pSelf->semReadId[ENDPOINT1], RTEMS_WAIT, WAIT_TIME);
                    else
                        status = rtems_semaphore_obtain(pSelf->semReadId[ENDPOINT1], RTEMS_WAIT, 0);

                    switch(status)
                    {
                        case RTEMS_TIMEOUT:
                            DPRINTF("Read semaphore obtain timeout on EP:%d!\n", ENDPOINT1);
                            break;
                        case RTEMS_SUCCESSFUL:
                            {
                                pblobTransHeader = (BlobTransHeader *)rBuff;
                                if(pblobTransHeader->magicNum[0] == 0x9F8E7D6C &&
                                pblobTransHeader->magicNum[1] == 0x5B4A9527 &&
                                pblobTransHeader->magicNum[2] == 0xBEEFDEAD &&
                                pblobTransHeader->blobSize == 0xAA55CC33) {
                                blobFlag = 0;
                                threadState= NET_BLOB_GET;
                                }
                                else {
                                    printf("Execute the Classify\n");
//                                    Classify_Test(blobPtr, &rBuff);
                                }
                            }
                            break;
                        default:
                            error_status++;
                            DPRINTF("Error %d on EP:%d\n", status, ENDPOINT1);
                            break;
                    }
                }
                break;
            default:
                break;
        }
			printf("status = %d\n",threadState);
		}
		
	     
        if(error_status)
            break;
    }
	return NULL;
}
#endif
