///
/// @file
/// @copyright All code copyright Movidius Ltd 2014, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// -----------------------------------------------------------------------------
///
/// Revision History
/// ===================================
/// 05-Nov-2014 : Author ( MM Solutions AD )
/// Created
/// =============================================================================

#include <pthread.h>

pthread_mutex_t global_lock;

void critical_section_pc_init(void)
{
    pthread_mutexattr_t attr;

    pthread_mutexattr_init(&attr);
    pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_RECURSIVE);
    pthread_mutex_init(&global_lock, &attr);
}

void critical_section_pc_deinit(void)
{
    pthread_mutex_destroy(&global_lock);
}

int critical_section_enter(void)
{
    pthread_mutex_lock(&global_lock);
    return 0;
}

void critical_section_exit(int key)
{
    pthread_mutex_unlock(&global_lock);
}

