///
/// @file
/// @copyright All code copyright Movidius Ltd 2014, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// -----------------------------------------------------------------------------
///
/// Revision History
/// ===================================
/// 05-Nov-2014 : Author ( MM Solutions AD )
/// Created
/// =============================================================================

#ifndef _POOL_BM_H
#define _POOL_BM_H

#ifdef __cplusplus
extern "C" {
#endif

typedef struct pool_bm pool_bm_t;

void pool_bm_free(pool_bm_t *pool, void *p);
void * pool_bm_try_alloc(pool_bm_t *pool);

void pool_bm_destroy(pool_bm_t *pool);
pool_bm_t * pool_bm_create(
        char *pool_name,
        unsigned int elem_size,
        unsigned int elems_count,
        void *mem
    );

void pool_bm_init(void);

#ifdef __cplusplus
}
#endif

#endif /* _POOL_BM_H */

