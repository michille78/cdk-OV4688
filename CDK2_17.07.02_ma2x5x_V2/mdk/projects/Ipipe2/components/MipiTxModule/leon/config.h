///
/// @file
/// @copyright All code copyright Movidius Ltd 2014, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// -----------------------------------------------------------------------------
///
/// Revision History
/// ===================================
/// 05-Nov-2014 : Author ( MM Solutions AD )
/// Created
/// =============================================================================

#ifndef CRITICAL_SECTION_ENABLE
#define CRITICAL_SECTION_ENABLE 0
#endif

#ifndef MIPI_TX_MODULE_CLENTS_NUM
#define MIPI_TX_MODULE_CLENTS_NUM 16
#endif

#ifndef MIPI_TX_MODULE_REQUEST_QUEUE_SIZE
#define MIPI_TX_MODULE_REQUEST_QUEUE_SIZE 32
#endif

#ifndef POOL_BM_NUM
#define POOL_BM_NUM 16
#endif

