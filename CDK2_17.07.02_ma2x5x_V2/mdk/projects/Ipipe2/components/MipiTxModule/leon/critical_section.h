///
/// @file
/// @copyright All code copyright Movidius Ltd 2014, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// -----------------------------------------------------------------------------
///
/// Revision History
/// ===================================
/// 05-Nov-2014 : Author ( MM Solutions AD )
/// Created
/// =============================================================================

#ifndef _CRITICAL_SECTION_H
#define _CRITICAL_SECTION_H

#ifdef __cplusplus
extern "C" {
#endif

int critical_section_enter(void);
void critical_section_exit(int key);

#ifdef __cplusplus
}
#endif

#endif /* _CRITICAL_SECTION_H */

