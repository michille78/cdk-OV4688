///
/// @file
/// @copyright All code copyright Movidius Ltd 2013, all rights reserved
///            For License Warranty see: common/license.txt
///
/// @brief
///
///Configuration file for OmniVision 7750
//# 640x480p @ 100 fps RAW10

#ifndef _IMGOV7750CFGVGA100_H_
#define _IMGOV7750CFGVGA100_H_

#include "CamDefines.h"

#define OV7750_COLORBAR_CRC_8BIT  (0x2C83B24E)
#define OV7750_COLORBAR_CRC_10BIT (0x95873BC3)

#define OV7750_WIDTH  640
#define OV7750_HEIGHT 480

extern const GenericCamSpec imgOv7750CfgVga100Raw10_camCfg;
extern const GenericCamSpec imgOv7750CfgVga100Raw8_camCfg;

#endif // _IMGOV7750CFGVGA100RAW10CB_H_
