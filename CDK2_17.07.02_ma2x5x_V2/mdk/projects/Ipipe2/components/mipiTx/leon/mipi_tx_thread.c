/* =============================================================================
* Copyright (c) 2013-2015 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file mipi_tx_thread.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 16-Jun-2015 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <assert.h>
#include <utils/mms_debug.h>
#include <osal/osal_stdlib.h>
#include <osal/osal_thread.h>
#include <rtems.h>
#include <mipi_tx_module.h>
#include "mipi_tx_thread.h"

#ifndef MIPI_TX_THREAD_MBOX_SIZE
#define MIPI_TX_THREAD_MBOX_SIZE 64
#endif

mmsdbg_define_variable(
        vdl_mipi_tx_thread,
        DL_DEFAULT,
        0,
        "vdl_mipi_tx_thread",
        "MIPI Tx thread moduel."
    );
#define MMSDEBUGLEVEL mmsdbg_use_variable(vdl_mipi_tx_thread)

typedef struct {
    struct osal_thread *thread;
    rtems_id mbox;
} mipi_tx_thread_t;

struct mipi_tx_thread_client {
    void *clinet_prv;
    mipi_tx_thread_clinet_sent_callback_t *sent_callback;
    mipi_tx_client_t *tx;
};

typedef enum {
    MSG_CMD_THREAD_EXIT,
    MSG_CMD_SENT,
} message_cmd_t;

typedef struct {
    message_cmd_t cmd;
    union {
        struct {
            mipi_tx_client_t *client;
            void *clinet_prv;
            mipi_tx_header_t *header;
            void *p1;
            void *p2;
            void *p3;
        } sent;
    };
} message_t;

static mipi_tx_thread_t mipi_tx_module_storage;

static void tx_sent_callback(
        mipi_tx_client_t *client,
        void *clinet_prv,
        mipi_tx_header_t *header,
        void *p1,
        void *p2,
        void *p3
    )
{
    mipi_tx_thread_t *mipi_tx_thread;
    message_t msg;
    int err;

    mipi_tx_thread = &mipi_tx_module_storage;

    msg.cmd = MSG_CMD_SENT;
    msg.sent.client = client;
    msg.sent.clinet_prv = clinet_prv;
    msg.sent.header = header;
    msg.sent.p1 = p1;
    msg.sent.p2 = p2;
    msg.sent.p3 = p3;
    err = rtems_message_queue_send(
            mipi_tx_thread->mbox,
            &msg,
            sizeof (msg)
        );
    assert(err == RTEMS_SUCCESSFUL);
}

static void tx_sent_callback_thread(
        mipi_tx_client_t *client_tx,
        void *clinet_prv,
        mipi_tx_header_t *header,
        void *p1,
        void *p2,
        void *p3
    )
{
    mipi_tx_thread_client_t *client;

    UNUSED(client_tx);

    client = clinet_prv;
    client->sent_callback(
            client,
            client->clinet_prv,
            header,
            p1,
            p2,
            p3
        );
}

static void * mipi_tx_thread_func(void *arg)
{
    mipi_tx_thread_t *mipi_tx_thread;
    message_t msg;
    size_t size;
    int err;

    mipi_tx_thread = arg;

    for (;;) {
        err = rtems_message_queue_receive(
                mipi_tx_thread->mbox,
                &msg,
                &size,
                RTEMS_DEFAULT_OPTIONS,
                RTEMS_NO_TIMEOUT
            );
        if (err != RTEMS_SUCCESSFUL) {
            mmsdbg(DL_ERROR, "Failed to receive message: err=%d", err);
        }

        switch (msg.cmd) {
            case MSG_CMD_THREAD_EXIT:
                return NULL;
            case MSG_CMD_SENT:
                tx_sent_callback_thread(
                        msg.sent.client,
                        msg.sent.clinet_prv,
                        msg.sent.header,
                        msg.sent.p1,
                        msg.sent.p2,
                        msg.sent.p3
                    );
                break;
            default:
                mmsdbg(DL_ERROR, "Unknown message: msg.cmd=%d", msg.cmd);
        }
    }
}

static int mipi_tx_thread_done_cb(void *arg)
{
    mipi_tx_thread_t *mipi_tx_thread;
    message_t msg;
    mipi_tx_thread = arg;
    int err;

    msg.cmd = MSG_CMD_THREAD_EXIT;
    err = rtems_message_queue_send(
            mipi_tx_thread->mbox,
            &msg,
            sizeof (msg)
        );
    if (err != RTEMS_SUCCESSFUL) {
        mmsdbg(
                DL_ERROR,
                "Failed to send message: err=%d\n",
                err
            );
    }

    return 0;
}


int mipi_tx_thread_send(
        mipi_tx_thread_client_t *client,
        mipi_tx_header_t *header,
        void *p1,
        void *p2,
        void *p3
    )
{

    return mipi_tx_send(
            client->tx,
            header,
            p1,
            p2, // p2,
            p3
        );
}

mipi_tx_thread_client_t * mipi_tx_thread_client_register(
        mipi_tx_characteristics_t *characteristics,
        void *clinet_prv,
        mipi_tx_thread_clinet_sent_callback_t *sent_callback
    )
{
//    mipi_tx_thread_t *mipi_tx_thread;
    mipi_tx_thread_client_t *client;

//    mipi_tx_thread = &mipi_tx_module_storage;

    client = osal_malloc(sizeof (*client));
    if (!client) {
        mmsdbg(DL_ERROR, "Failed to allocate new client!");
        goto exit1;
    }

    client->tx = mipi_tx_client_register(
            characteristics,
            client,
            tx_sent_callback
        );
    if (!client->tx) {
        mmsdbg(DL_ERROR, "Failed to register mipi tx client!");
        goto exit2;
    }

    client->clinet_prv = clinet_prv;
    client->sent_callback = sent_callback;

    return client;
exit2:
    osal_free(client);
exit1:
    return NULL;
}

void mipi_tx_thread_client_unregister(
        mipi_tx_thread_client_t **client
    )
{
    mipi_tx_thread_client_t *cl;
    cl = *client;
    if (cl) {
        mipi_tx_client_unregister(cl->tx);
        osal_free(cl);
        *client = NULL;
    }
}

void mipi_tx_thread_destroy(void)
{
    mipi_tx_thread_t *mipi_tx_thread;
    mipi_tx_thread = &mipi_tx_module_storage;
    osal_thread_destroy(mipi_tx_thread->thread);
    rtems_message_queue_delete(mipi_tx_thread->mbox);
    mipi_tx_destroy();
}

int mipi_tx_thread_create(mipi_tx_thread_create_t *params)
{
    mipi_tx_thread_t *mipi_tx_thread;
    mipi_tx_create_t params_tx;
    int err;

    mipi_tx_thread = &mipi_tx_module_storage;

    params_tx.num_lanes = params->num_lanes;
    params_tx.mipi_clock = params->mipi_clock;
    params_tx.use_irq = params->use_irq;

    err = mipi_tx_create(&params_tx);
    if (err) {
        mmsdbg(DL_ERROR, "Failed to create mipi tx module!");
        goto exit1;
    }

    err = rtems_message_queue_create(
            rtems_build_name('M', 'i', 'p', 'i'),
            MIPI_TX_THREAD_MBOX_SIZE,
            sizeof (message_t),
            RTEMS_DEFAULT_ATTRIBUTES,
            &mipi_tx_thread->mbox
        );
    if (err != RTEMS_SUCCESSFUL) {
        mmsdbg(
                DL_ERROR,
                "Failed to create message queue: err=%d\n",
                err
            );
        goto exit2;
    }

    mipi_tx_thread->thread = osal_thread_create(
            "MipiTx",
            mipi_tx_thread,
            mipi_tx_thread_func,
            mipi_tx_thread_done_cb,
            0,
            params->stack_size
        );
    if (!mipi_tx_thread->thread) {
        mmsdbg(
                DL_ERROR,
                "Failed to create thread: name=MipiTx, stack_size=%d!",
                params->stack_size
            );
        goto exit3;
    }
    osal_thread_priority(mipi_tx_thread->thread, params->prio);

    return 0;
exit3:
    rtems_message_queue_delete(mipi_tx_thread->mbox);
exit2:
    mipi_tx_destroy();
exit1:
    return -1;
}

