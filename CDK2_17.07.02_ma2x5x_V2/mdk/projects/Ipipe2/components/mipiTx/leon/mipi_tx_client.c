/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file ic_mipi_tx_client.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! Dec 9, 2014 : Author aiovtchev
*! Created
* =========================================================================== */

#include "mipi_tx_client.h"
#include <assert.h>
#include <stdlib.h>

struct {
    struct list_head client_list;
} gMipiTxCtx =  { {&gMipiTxCtx.client_list, &gMipiTxCtx.client_list} };


void* los_registerMipiClient(losRegisterMipiTxClient_t * reg_params)
{
    losMipiTxMipiClient_t *losHndl;

    losHndl = calloc(1, sizeof(losMipiTxMipiClient_t));
    if (NULL == losHndl)
    {
        return NULL;
    }
    losHndl->reg_params = *reg_params;
    losHndl->state = IC_MIPI_TX_STATE_CREATED;
    list_add(&losHndl->node, &gMipiTxCtx.client_list);
    return losHndl;
}

void los_unregisterMipiClient(void* hndl)
{
    losMipiTxMipiClient_t *losHndl = (losMipiTxMipiClient_t *)hndl;

    assert((losHndl)&&(losHndl->state == IC_MIPI_TX_STATE_CREATED));

    list_del(&losHndl->node);
    free(hndl);
}

void los_openMipiClient(void* hndl, uint32_t dataType, uint32_t dataSizeMax)
{
    losMipiTxMipiClient_t *losHndl = (losMipiTxMipiClient_t *)hndl;
    assert((losHndl)&&(losHndl->state == IC_MIPI_TX_STATE_CREATED));

    losHndl->state = IC_MIPI_TX_STATE_OPENING;

    openMetaMipiClient(hndl, dataType, dataSizeMax, losHndl->reg_params.instance);
}

void los_closeMipiClient(void* hndl)
{
    losMipiTxMipiClient_t *losHndl = (losMipiTxMipiClient_t *)hndl;

    assert((losHndl)&&(losHndl->state == IC_MIPI_TX_STATE_IN_USE));
    losHndl->state = IC_MIPI_TX_STATE_CLOSING;

    closeMetaMipiClient(hndl, losHndl->reg_params.instance);
}

void los_sendTxClientData(void* hndl, void * inBuffer, uint32_t buffSize)
{
    losMipiTxMipiClient_t *losHndl = (losMipiTxMipiClient_t *)hndl;

    assert((losHndl)&&(losHndl->state == IC_MIPI_TX_STATE_IN_USE));

    sendMetaTxClientData(hndl, inBuffer, buffSize, losHndl->reg_params.instance);
}

void icMipiTxClientRegistered(uint32_t instance, void* clientHandle, uint32_t buffSize, void* userData)
{
    losMipiTxMipiClient_t *losHndl = (losMipiTxMipiClient_t *)userData;

    UNUSED(instance);

    assert((losHndl)&&(losHndl->state == IC_MIPI_TX_STATE_OPENING));
    losHndl->lrt_clientHandle = clientHandle;
    losHndl->state = IC_MIPI_TX_STATE_IN_USE;
    if (losHndl->reg_params.cb_mipi_tx_open)
        losHndl->reg_params.cb_mipi_tx_open(losHndl->reg_params.client_private, buffSize);
}

void icMipiTxClientUnregistered(uint32_t instance, void* clientHandle, void* userData)
{
    losMipiTxMipiClient_t *losHndl = (losMipiTxMipiClient_t *)userData;

    UNUSED(instance);
    UNUSED(clientHandle);

    assert((losHndl)&&(losHndl->state == IC_MIPI_TX_STATE_CLOSING));

    losHndl->state = IC_MIPI_TX_STATE_CREATED;
    losHndl->lrt_clientHandle = 0;

    if (losHndl->reg_params.cb_mipi_tx_close)
        losHndl->reg_params.cb_mipi_tx_close(losHndl->reg_params.client_private);

}

void icMipiTxClientDataSent(uint32_t instance, void* clientHandle, uint32_t dataSize, void* buff, void* userData)
{
    losMipiTxMipiClient_t *losHndl = (losMipiTxMipiClient_t *)userData;

    UNUSED(instance);
    UNUSED(clientHandle);

    if (losHndl->reg_params.cb_mipi_tx_sent)
        losHndl->reg_params.cb_mipi_tx_sent(losHndl->reg_params.client_private, dataSize, buff);
}

