
/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
*
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file
*
* @author Radoslav Ibrishimov ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 30-Oct-2014 : Author Radoslav Ibrishimov ( MM Solutions AD )
*! Created
* =========================================================================== */

#ifndef MIPITXSTILLHEADER_H_
#define MIPITXSTILLHEADER_H_

#include <stdint.h>
#include "IcTypes.h"

enum {
    SLICE_TYPE_Y,
    SLICE_TYPE_UV,
    SLICE_TYPE_YUV,
};

typedef struct {
    uint32_t frame_type;                     /* content type */
    uint32_t frame_format;                   /* content format */
    uint32_t frame_width;                    /* number of pixels */
    uint32_t frame_height;                   /* number of lines */
    uint32_t frame_time_stamp_hi;            /* time stamp captured by MIPI RX */
    uint32_t frame_time_stamp_lo;
    uint32_t frame_proc_time_stamp_hi;       /* time stamp at processing start */
    uint32_t frame_proc_time_stamp_lo;
    uint32_t frame_idx_req_hal;              /* HAL frame request index */
    uint32_t frame_idx_req_app;              /* Application frame request index */
    uint32_t frame_idx_mipi_rx;              /* MIPI RX frame incremental counter */
    uint32_t frame_idx_process;              /* Still image processing counter */
    uint32_t header_height;                  /* number of line for the header */
    uint32_t slice_data_type;
    uint32_t slice_y_offset;                 /* number of bytes within payload */
    uint32_t slice_y_size;                   /* number of bytes */
    uint32_t slice_uv_offset;                /* number of bytes within payload */
    uint32_t slice_uv_size;                  /* number of bytes */
    uint32_t slice_total_number;             /* incremental counter in a frame */
    uint32_t slice_last_flag;                /* 0 - one more slice, 1 - last */
    uint32_t debug_data_enable;
    uint32_t camera_id;                      /* 0 - left chunk, 1 - right chunk */
    uint32_t buff_width;
    uint32_t buff_height;
    uint32_t buff_stride;
    uint32_t buff_pxl_size_nom;
    uint32_t buff_pxl_size_denom;
    uint32_t check_sum;                      /* magic number to check for valid header */
} client_tx_frame_header_t;

typedef struct {
    uint32_t chunk;
    char client_data[sizeof (client_tx_frame_header_t)];
} mipi_tx_header_still_image_t;

uint32_t mipi_tx_header_calc_sum(client_tx_frame_header_t *h);

#endif /* MIPITXSTILLHEADER_H_ */
