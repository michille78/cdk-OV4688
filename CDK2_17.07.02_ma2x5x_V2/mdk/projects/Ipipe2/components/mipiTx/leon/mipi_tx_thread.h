/* =============================================================================
* Copyright (c) 2013-2015 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file mipi_tx_thread.h
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 16-Jun-2015 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#ifndef _MIPI_TX_THREAD_H
#define _MIPI_TX_THREAD_H

#ifdef __cplusplus
extern "C" {
#endif

#include <mipi_tx_module.h>

typedef struct mipi_tx_thread_client mipi_tx_thread_client_t;

typedef struct {
    int prio;
    int stack_size;
    int num_lanes;
    int mipi_clock;
    int use_irq;
} mipi_tx_thread_create_t;

typedef void mipi_tx_thread_clinet_sent_callback_t(
        mipi_tx_thread_client_t *client,
        void *clinet_prv,
        mipi_tx_header_t *header,
        void *p1,
        void *p2,
        void *p3
    );

int mipi_tx_thread_create(mipi_tx_thread_create_t *params);
void mipi_tx_thread_destroy(void);

mipi_tx_thread_client_t * mipi_tx_thread_client_register(
        mipi_tx_characteristics_t *characteristics,
        void *clinet_prv,
        mipi_tx_thread_clinet_sent_callback_t *sent_callback
    );
void mipi_tx_thread_client_unregister(
        mipi_tx_thread_client_t **client
    );

int mipi_tx_thread_send(
        mipi_tx_thread_client_t *client,
        mipi_tx_header_t *header,
        void *p1,
        void *p2,
        void *p3
    );

#ifdef __cplusplus
}
#endif

#endif /* _MIPI_TX_THREAD_H */

