///
/// @file
/// @copyright All code copyright Movidius Ltd 2014, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Sensor Functions Interface.
///
/// This is the implementation of a Sensors configuration available for 182 board.
///
///

// 1: Includes
// ----------------------------------------------------------------------------
// Drivers includes

#include "assert.h"
#include "string.h"
#include "DrvI2cMaster.h"
#include "DrvTimer.h"
#include "DrvGpio.h"


#include "CamSensor.h"

//#define _LOCAL_DEBUG_
#ifdef _LOCAL_DEBUG_
#include "stdio.h"
#define DPRINTF(...) printf(__VA_ARGS__)
#else
#define DPRINTF(...)
#endif


// 2:  Source Specific #defines and types  (typedef,enum,struct)
// ----------------------------------------------------------------------------

// 3: Global Data (Only if absolutely necessary)
// ----------------------------------------------------------------------------


// 4: Static Local Data
// ----------------------------------------------------------------------------

// 5: Static Function Prototypes
// ----------------------------------------------------------------------------
int searchAndBurnI2cConfigsByType(SensorConfiguration *senConf, i2cConfigType_t configType, u32 *overrideValue);
static int burnI2cConfigurationToCamera(SensorConfiguration *senConf, const I2CConfigDescriptor *i2cConfig, u32 *overrideValue);
int setAllGpioByType(const GpioConfigDescriptor gpioDescList[], gpioConfigType_t cfgType, int onOff);
// 6: Functions Implementation
// ----------------------------------------------------------------------------
void startSensor(SensorConfiguration *sensorConfigurations) {

    setAllGpioByType(sensorConfigurations->gpioConfigDescriptor, POWER_PIN, 1); // assert reset
    setAllGpioByType(sensorConfigurations->gpioConfigDescriptor, RESET_PIN, 1); // assert reset
    setAllGpioByType(sensorConfigurations->gpioConfigDescriptor, RESET_PIN, 0); // deassert reset

    // send all possible configuration steps
    searchAndBurnI2cConfigsByType(sensorConfigurations, CAM_STOP_STREAMING, NULL);
    searchAndBurnI2cConfigsByType(sensorConfigurations, CAM_FULL_CONFIG, NULL);
    searchAndBurnI2cConfigsByType(sensorConfigurations, CAM_START_STREAMING, NULL);
}

int setAllGpioByType(const GpioConfigDescriptor gpioDescList[],
        gpioConfigType_t cfgType, int onOff)
{
    int value;
    u32 i;
    int rval = 0;

    if(gpioDescList == NULL)
        return -6;

    for(i = 0 ; ; i++)
    {
        if(gpioDescList[i].configType == END)
        {
            return rval;
        }
        if(gpioDescList[i].configType == cfgType)
        {
            value = ((!(gpioDescList[i].activeLevel)) == (!(onOff))); // logical ex-or
            DrvGpioSetMode(gpioDescList[i].gpioNumber, (D_GPIO_DIR_OUT | D_GPIO_MODE_7));
            DrvGpioSetPin(gpioDescList[i].gpioNumber, value);
            DrvTimerSleepMs(gpioDescList[i].delayMs);
        }
    }
}

static int burnI2cConfigurationToCamera(SensorConfiguration *senConf,
        const I2CConfigDescriptor *i2cConfig,
        u32 *overrideValue)
{
    u32 i;
    u16 registerToSend;
    u16 dataBytesToSend;
    const GenericCamSpec* camSpec = senConf->sensorSpec;
    static u8 camWriteProto8b[]  = {S_ADDR_WR, R_ADDR_H, R_ADDR_L, DATAW, LOOP_MINUS_1};         //register addr on 16bits, data on 8 bits
    static u8 camWriteProto16b[] = {S_ADDR_WR, R_ADDR_H, R_ADDR_L, DATAW, DATAW, LOOP_MINUS_1};  //register addr on 16bits, data on 16 bits
    u8 *camWriteProto;
    u32 slaveAddr;

    if (i2cConfig == NULL)
        return (-1);

    if ( senConf->i2cSensorSelect == 0 )
        slaveAddr = camSpec->sensorI2CAddress1;
    else
        slaveAddr = camSpec->sensorI2CAddress2;

    if ( camSpec->regSize == 1 )
        camWriteProto = camWriteProto8b;
    else if ( camSpec->regSize == 2 )
        camWriteProto = camWriteProto16b;
    else
        assert(0); // Unsupported register size

    for (i = 0; i < i2cConfig->numberOfRegs; i++)
    {
        if(0xFFFE == i2cConfig->regValues[i][0]) { // sleep included in configuration
            DrvTimerSleepMs(i2cConfig->regValues[i][1]);
        }
        else {
            registerToSend = i2cConfig->regValues[i][0];
            if ( camSpec->regSize == 1 )
                dataBytesToSend = i2cConfig->regValues[i][1];
            else if ( camSpec->regSize == 2 )
                dataBytesToSend = ((i2cConfig->regValues[i][1] >> 8) & 0xFF) | ((i2cConfig->regValues[i][1] << 8) & 0xFF00);  // first data[15:8] then data[7:0]
            else
                return (-1);

            if(overrideValue && i < sizeof(*overrideValue))
                dataBytesToSend = (*overrideValue >> (i*8)) & 0xFF;

            if (I2CM_STAT_OK != DrvI2cMTransaction(*senConf->pI2cHandle, slaveAddr, registerToSend ,camWriteProto, (u8*)(&dataBytesToSend) , camSpec->regSize) )
            {
                DPRINTF("\n I2C sensor configuration failed at address 0x%x! Abort\n", registerToSend);
                //return (-1);
            }
            else {
                DPRINTF("   I2C registers 0x%x set with values 0x%x\n",registerToSend, dataBytesToSend);
            }
        }
    }
    if  (i2cConfig->delayMs)
        DrvTimerSleepMs(i2cConfig->delayMs);

    return (0);
}

int searchAndBurnI2cConfigsByType(SensorConfiguration *senConf,
        i2cConfigType_t configType,
        u32 *overrideValue)
{
    s32 rval = 0;
    u32 i;
    const GenericCamSpec* camSpec = senConf->sensorSpec;
    for(i = 0 ; i < camSpec->nbOfI2CConfigSteps ; i++)
    {
        if(camSpec->i2cConfigSteps[i].configType == configType)
        {
            rval = burnI2cConfigurationToCamera(senConf,
                    &camSpec->i2cConfigSteps[i],
                    overrideValue);
            if(rval)
                return (rval);
        }
    }

    return (rval);
}


