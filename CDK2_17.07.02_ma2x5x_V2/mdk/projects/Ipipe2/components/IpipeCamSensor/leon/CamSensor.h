///
/// @file
/// @copyright All code copyright Movidius Ltd 2014, all rights reserved
///            For License Warranty see: common/license.txt
///
/// @defgroup CAM GenericApi CAM sensor API.
/// @{
/// @brief    GenericCam API.
///
/// This is the API for generic camera component
///


#ifndef _CAM_SENSOR_API_H_
#define _CAM_SENSOR_API_H_

// 1: Includes
// ----------------------------------------------------------------------------
#include "mv_types.h"
#include "CamDefines.h"
#include "DrvI2cDefines.h"

#ifdef __cplusplus
extern "C" {
#endif


typedef struct {
const GenericCamSpec          *sensorSpec;
I2CM_Device             **pI2cHandle;
u32                     i2cSensorSelect;
const GpioConfigDescriptor    *gpioConfigDescriptor;
} SensorConfiguration;

// 2:  Exported Global Data (generally better to avoid)
// ----------------------------------------------------------------------------
// 3:  Exported Functions (non-inline)
// ----------------------------------------------------------------------------

/// This will initialize the camera component (the sensor and all the myriad components connected in the frames data path) for a specific sensor
/// @param  hndl               Pointer to the empty camera handle which will be filled back with all camera configurations at return (pseudo return parameter)
/// @param  camSpec            Fixed camera configuration
/// @param  userSpec           Dynamic camera configuration
/// @param  cbList             List of pointers to all user callback functions which will be called by CamGeneric
/// @return camErrorType       CAM_SUCCESS if camera initialization was OK, other values if it failed
///
void startSensor(SensorConfiguration *sensorConfigurations);
/// @}

int searchAndBurnI2cConfigsByType(SensorConfiguration *senConf, i2cConfigType_t configType, u32 *overrideValue);


int setAllGpioByType(const GpioConfigDescriptor gpioDescList[], gpioConfigType_t cfgType, int onOff);

#ifdef __cplusplus
}
#endif


#endif //_CAM_SENSOR_API_H_
