///
/// @file
/// @copyright All code copyright Movidius Ltd 2012, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @defgroup bicubicWarp      Bicubic Warp
/// @defgroup bicubicWarpApi Bicubic Warp API
/// @ingroup  bicubicWarp
/// @{
/// @brief     Bicubic Warp API
///
/// This is the functions API for the Bicubic Warp component
///

#ifndef __BICUBIC_WARP_API_H__
#define __BICUBIC_WARP_API_H__

#ifdef __cplusplus
extern "C" {
#endif

/// @brief Minimum value used for clamp
#define CLAMP_MIN 0x0000 // 0.0 f16

/// @brief Maximum value used for clamp
#define CLAMP_MAX 0x3c00 // 1.0 f16

/// @brief Initialize bicubic block
/// @param[in] bicubicIrqHandler - IRQ callback
/// @param[in] irqNumber         - IRQ number
/// @return    void
///
void bicubicWarpInit(void * bicubicIrqHandler, u32 irqNumber);

/// @brief Generate mesh for rotation and translation (RT) relative to the center.
/// 
/// Transformation matrix (3x2) is obtained from a rotation degree and translation coefficients
/// @param[in] xyRectifiedBuffer - pointer to x,y rectified coordinates
/// @param[in] matrixRT          - pointer to rotation&translation matrix
/// @param[in] width             - image width
/// @param[in] height            - image height
/// @return    void
///
void bicubicWarpGenerateMeshRT(fp32* xyRectifiedBuffer, const fp32 *matrixRT, u32 width, u32 height);

/// @brief Generate mesh for rotation, translation and projection (RTP)
/// 
///Transformation matrix (3x3) represents the OpenCV style homography having R, T and P coefficients
/// @param[in] xyRectifiedBuffer - pointer to x,y rectified coordinates
/// @param[in] matrixRT          - pointer to rotation&translation matrix
/// @param[in] width             - image width
/// @param[in] height            - image height
/// @return    void
///
void bicubicWarpGenerateMeshHomographyRTP(fp32* xyRectifiedBuffer, const fp32 *matrixRTP, u32 width, u32 height);

/// @brief Apply bicubic filter on frame
/// @param[in] input             - pointer to input image
/// @param[out] output           - pointer to output image
/// @param[in] xyRectifiedBuffer - pointer to x,y rectified coordinates
/// @param[in] width             - image width
/// @param[in] height            - image height
/// @param[in] bpp               - bytes per pixel
/// @return    void
///
void bicubicWarpProcessFrame(u8* input, u8* output, fp32* xyRectifiedBuffer, u32 width, u32 height, u32 bpp, u32 pixelFormat);
/// @}

/// @brief Apply bicubic filter on frame
/// @param[in] input             - pointer to input image
/// @param[out] output           - pointer to output image
/// @param[in] xyRectifiedBuffer - pointer to x,y rectified coordinates
/// @param[in] width             - image width
/// @param[in] height            - image height
/// @param[in] bpp               - bytes per pixel
/// @return    void
///
void bicubicWarpStart(u8* input, u8* output, fp32* xyRectifiedBuffer, u32 width, u32 height, u32 bpp, u32 pixelFormat);

#ifdef __cplusplus
}
#endif


#endif // __BICUBIC_WARP_API_H__
