ifeq ($(LEON_RT_BUILD),yes)
	PLGDM_PREFIX=lrt_
else
	PLGDM_PREFIX=
endif
#ENTRYPOINTS = -e runTask  --gc-sections
$(PlgGs2xD).mvlib : $(SHAVE_PlgGs2xD_OBJS) $(PROJECT_SHAVE_LIBS)
	$(ECHO) $(LD)  $(MVLIBOPT) $(SHAVE_PlgGs2xD_OBJS) $(PROJECT_SHAVE_LIBS) $(CompilerANSILibs) -o $@
#This creates a binary file packing the shvdlib file
$(PlgGs2xD)_bin.o : $(PlgGs2xD).shvdlib
	$(ECHO) $(OBJCOPY)  -I binary --rename-section .data=.data.plgGs2xDShvImageAddr \
	--redefine-sym  _binary_$(subst /,_,$(subst .,_,$<))_start=$(PLGDM_PREFIX)plgGs2xDShvImageAddr \
	-O elf32-sparc -B sparc $< $@

$(PlgGs2xD)_sym.o : $(PlgGs2xD).shvdcomplete
	echo BBBB $(PlgGs2xD)_sym.o
	$(ECHO) $(OBJCOPY) --prefix-symbols=$(PLGDM_PREFIX)PlgGs2xD_ --extract-symbol $< $@

