ifeq ($(LEON_RT_BUILD),yes)
	PLGDM_OBJECTS=RAWDATAOBJECTFILES
else
	PLGDM_OBJECTS=RAWDATAOBJECTFILES
endif

PlgGs2xD = PlgGs2xD
SHAVE_C_SOURCES_PlgGs2xD = $(wildcard $(IPIPE_BASE)/plugins/PlgGs2xD/shave/*.c)
SHAVE_ASM_SOURCES_PlgGs2xD = $(wildcard $(IPIPE_BASE)/plugins/PlgGs2xD/shave/*.asm)
SHAVE_GENASMS_PlgGs2xD = $(patsubst %.c,$(DirAppObjBase)%.asmgen,$(SHAVE_C_SOURCES_PlgGs2xD))
SHAVE_PlgGs2xD_OBJS = $(patsubst %.asm,$(DirAppObjBase)%_shave.o,$(SHAVE_ASM_SOURCES_PlgGs2xD)) \
                   $(patsubst $(DirAppObjBase)%.asmgen,$(DirAppObjBase)%_shave.o,$(SHAVE_GENASMS_PlgGs2xD))
$(PLGDM_OBJECTS)  += $(PlgGs2xD)_sym.o $(PlgGs2xD)_bin.o     
PROJECTCLEAN += $(SHAVE_GENASMS_PlgGs2xD) $(SHAVE_PlgGs2xD_OBJS) $(PlgGs2xD).mvlib        
PROJECTINTERM += $(SHAVE_GENASMS_PlgGs2xD)    
PROJECTCLEAN += $(PlgGs2xD).shvdlib $(PlgGs2xD).map
PROJECTCLEAN += $(PlgGs2xD).shvdcomplete $(PlgGs2xD)_sym.o   
