/**************************************************************************************************

 @File         : PlgGs2xD.c
 @Author       : MT
 @Brief        : Contain  Gauss 5 taps, 2x down-scale plugin
 Date          : 01 - March - 2015
 E-mail        : xxx.xxx@movidius.com
 Copyright     : © Movidius Srl 2014, © Movidius Ltd 2015

 Description :
    This plug-in apply gauss 5x5 down-scale 2x filter on a luma image
    Resources used:
         Leon RT.
         1 Shave, configurable id, dynamic code loaded.
         CmxDma controlled by LeonRt.

    Interrupt base.

 **************************************************************************************************/


/**************************************************************************************************
 ~~~ Included types first then APIs from other modules
 ***************************************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <limits.h>
#include <stdint.h>
#include "DrvCmxDma.h"
#include "TimeSyncMgr.h"
#include "FrameMgrApi.h"
#include "PlgGs2xDApi.h"
#include "DrvSvu.h"
#include "VcsHooksApi.h"

/**************************************************************************************************
 ~~~  Specific #defines
 **************************************************************************************************/
#define PLGGS2XDSHV__IRQ_LEVEL  3
// TODO: make it common with shave's define
#define PLGGS2XDSHV_KERNEL_SIZE 5
#define PLGGS2XDSHV_PADING_SIZE 8
#define PLGGS2XDSHV_MAX_LINE_WIDTH 1936
#define PLGGS2XDSHV_MAX_LINE_SIZE (PLGGS2XDSHV_MAX_LINE_WIDTH+PLGGS2XDSHV_PADING_SIZE*2)
#define PLGGS2XDSHV_BUF_HEIGHT (PLGGS2XDSHV_KERNEL_SIZE+2)
enum {
    PLGGS2X_STATS_OFF = 0,
    PLGGS2X_STATS_ON  = 1
};

/**************************************************************************************************
 ~~~  Imported Variable symbol from shave variables
 **************************************************************************************************/
extern u32 plgGs2xDShvImageAddr[];
extern u32 PlgGs2xD_plgGs2xDRunTask;
extern u32 PlgGs2xD_plgGs2xDShvBufIn;
extern u32 PlgGs2xD_plgGs2xDShvBufO;


/**************************************************************************************************
 ~~~ Local File function declarations
 **************************************************************************************************/
static        int32_t   init(FramePool *outputPools, int nOutputPools, void *pluginObject);
static        int32_t   stop(void *pluginObject);
static        void      runDmaInTask(PlgGs2xD *plug);
static        void      runDmaInOutTask(PlgGs2xD *plug);
static        void      runDmaOutTask(PlgGs2xD *plug);
static inline void      eolCallback(PlgGs2xD *plug);
static        void      dmaHndlIrq(dmaTransactionList* ListPtr, void* userContext);
static        void      shvIrqHandler(uint32_t source);
static inline void      eofFrameHandler(PlgGs2xD *plug);
static        void      runShvLines(PlgGs2xD *plug);
static        void      runFrame(PlgGs2xD *plug);
static        void      producedFrame(FrameT *frame, void *pluginObject);
static inline void      updateCircularBuffers(PlgGs2xD *plug);



/**************************************************************************************************
 ~~~ Local Functions Implementation
 **************************************************************************************************/

//
static int32_t init(FramePool *outputPools, int nOutputPools, void *pluginObject) {
    PlgGs2xD *plug         = (PlgGs2xD*)pluginObject;
    plug->outputPools   = outputPools;
    plug->crtStatus     = PLGGS2X_STATS_ON;
    return 0;
}

//
static int32_t  stop(void *pluginObject) {
    PlgGs2xD *plug = (PlgGs2xD*)pluginObject;
    plug->crtStatus      = PLGGS2X_STATS_OFF;
    return 0;
}

//
static inline void eolCallback(PlgGs2xD *plug) {
    if((plug->shvRunMask)&&(plug->dmaRunMask)) {
        runFrame(plug);
    }
}

//
static void dmaHndlIrq(dmaTransactionList* ListPtr, void* userContext) {
    PlgGs2xD *plug = (PlgGs2xD*)userContext;
    plug->dmaRunMask = 1;
    eolCallback(plug);
}

//
static inline void eofFrameHandler(PlgGs2xD *plug) {
    FrameMgrReleaseFrame(plug->iFrame);
    plug->oFrame->stride[0] = plug->size.w>>1;
    plug->oFrame->height[0] = plug->size.h>>1;
    FrameMgrProduceFrame(plug->oFrame);
    plug->plg.status = PLG_STATS_IDLE;
}

//
static void runDmaInTaskFirst(PlgGs2xD *plug) {
    plug->dmaRef[0] = DrvCmxDmaCreateTransactionFullOptions(
            plug->dmaId,
            &plug->dmaTask[0],
            plug->shaveTaskDesc[0].inAdrFrm,
            plug->shaveTaskDesc[0].in[plug->dmaRunNr & 15],
            plug->size.w*8,
            plug->size.w,
            plug->size.w,
            plug->size.w,
            PLGGS2XDSHV_MAX_LINE_SIZE);
    plug->shaveTaskDesc[0].inAdrFrm += plug->size.w*8;
    plug->dmaRunNr += 8; // first time are taken 8 lines
    DrvCmxDmaStartTaskAsync(plug->dmaRef[0], dmaHndlIrq, (void*)plug);
}

//
static void runDmaInTask(PlgGs2xD *plug) {
    plug->dmaRef[0] = DrvCmxDmaCreateTransactionFullOptions(
            plug->dmaId,
            &plug->dmaTask[0],
            plug->shaveTaskDesc[0].inAdrFrm,
            plug->shaveTaskDesc[0].in[plug->dmaRunNr & 15],
            plug->size.w*4,
            plug->size.w,
            plug->size.w,
            plug->size.w,
            PLGGS2XDSHV_MAX_LINE_SIZE);
    plug->shaveTaskDesc[0].inAdrFrm += plug->size.w*4;
    plug->dmaRunNr += 4; // take 4 lines
    DrvCmxDmaStartTaskAsync(plug->dmaRef[0], dmaHndlIrq, (void*)plug);
}

//
static void runDmaInOutTask(PlgGs2xD *plug) {
    plug->dmaRef[0] = DrvCmxDmaCreateTransactionFullOptions(
            plug->dmaId,
            &plug->dmaTask[0],
            plug->shaveTaskDesc[0].inAdrFrm,
            plug->shaveTaskDesc[0].in[plug->dmaRunNr & 15],
            plug->size.w*4,
            plug->size.w,
            plug->size.w,
            plug->size.w,
            PLGGS2XDSHV_MAX_LINE_SIZE);
    plug->shaveTaskDesc[0].inAdrFrm += plug->size.w*4;
    plug->dmaRunNr += 4; // take 4 lines
    plug->dmaRef[1] = DrvCmxDmaCreateTransactionFullOptions(
            plug->dmaId,
            &plug->dmaTask[1],
            plug->shaveTaskDesc[0].out[plug->dmaRunOutNr&3],
            plug->shaveTaskDesc[0].outAdrFrm,
            (plug->size.w>>1)*2,
            plug->size.w>>1,
            plug->size.w>>1,
            PLGGS2XDSHV_MAX_LINE_SIZE,
            plug->size.w>>1);
    plug->shaveTaskDesc[0].outAdrFrm += (plug->size.w*2)>>1;
    plug->dmaRunOutNr += 2; // take 4 lines
    DrvCmxDmaLinkTasks(plug->dmaRef[0], 1, plug->dmaRef[1]);
    DrvCmxDmaStartTaskAsync(plug->dmaRef[0], dmaHndlIrq, (void*)plug);
}

//
static void runDmaOutTask(PlgGs2xD *plug) {
    plug->dmaRef[1] = DrvCmxDmaCreateTransactionFullOptions(
            plug->dmaId,
            &plug->dmaTask[1],
            plug->shaveTaskDesc[0].out[plug->dmaRunOutNr&3],
            plug->shaveTaskDesc[0].outAdrFrm,
            (plug->size.w>>1)*2,
            plug->size.w>>1,
            plug->size.w>>1,
            PLGGS2XDSHV_MAX_LINE_SIZE,
            plug->size.w>>1);
    plug->shaveTaskDesc[0].outAdrFrm += (plug->size.w*2)>>1;
    plug->dmaRunOutNr += 2; // take 4 lines
    DrvCmxDmaStartTaskAsync(plug->dmaRef[1], dmaHndlIrq, (void*)plug);
}

//
static void shvIrqHandler(uint32_t source) {
    PlgGs2xD *plug = (PlgGs2xD*)GET_REG_WORD_VAL(SVU_CTRL_ADDR[source - IRQ_SVE_0]+SLC_OFFSET_SVU+IRF_BASE+(18<<2));
    plug->shvRunMask = 1;
    DrvIcbIrqClear(source);
    plug->shvRunNr++;
    plug->shaveTaskDesc[0].inCnt += 4;
    plug->shaveTaskDesc[0].outCnt += 2;
    eolCallback(plug);
}

//
static void runShvLines(PlgGs2xD *plug) {
    uint32_t shv = plug->shvUsed;
    plug->shaveTaskDesc[0].crtRunOut[0]   = plug->shaveTaskDesc[0].out[(plug->shaveTaskDesc[0].outCnt) & 3];
    plug->shaveTaskDesc[0].crtRunOut[1]   = plug->shaveTaskDesc[0].out[(plug->shaveTaskDesc[0].outCnt+1) & 3];
    // Set STOP bit in control register
    SET_REG_WORD(SVU_CTRL_ADDR[shv] + SLC_OFFSET_SVU + SVU_OCR, OCR_STOP_GO);
    swcSetAbsoluteDefaultStack(shv);
    //
    SET_REG_WORD(SVU_CTRL_ADDR[shv]+SLC_OFFSET_SVU+IRF_BASE+(18<<2), ((u32)plug));
    SET_REG_WORD(SVU_CTRL_ADDR[shv]+SLC_OFFSET_SVU+IRF_BASE+(17<<2), ((u32)plug->shaveTaskDesc[0].crtRunIn));
    SET_REG_WORD(SVU_CTRL_ADDR[shv]+SLC_OFFSET_SVU+IRF_BASE+(16<<2), ((u32)plug->shaveTaskDesc[0].crtRunOut));
    SET_REG_WORD(SVU_CTRL_ADDR[shv]+SLC_OFFSET_SVU+IRF_BASE+(15<<2), ((u32)plug->size.w));
    // Enable SWI interrupt
    SET_REG_WORD(SVU_CTRL_ADDR[shv] + SLC_OFFSET_SVU + SVU_IRR, 0xFF);
    SET_REG_WORD(SVU_CTRL_ADDR[shv] + SLC_OFFSET_SVU + SVU_ICR, 0x20);
    // set IP
    SET_REG_WORD(SVU_CTRL_ADDR[shv]+SLC_OFFSET_SVU+SVU_PTR, (u32)&PlgGs2xD_plgGs2xDRunTask);
    //Start shave
    SET_REG_WORD(SVU_CTRL_ADDR[shv]+SLC_OFFSET_SVU+SVU_OCR, 0);
}

//
static inline void updateCircularBuffers(PlgGs2xD *plug) {
    int x;
    int32_t nr;
    nr = plug->shaveTaskDesc[0].inCnt;
    nr = nr&15;
    for(x = 0; x < 7; x++) {
        plug->shaveTaskDesc[0].crtRunIn[x] = plug->shaveTaskDesc[0].in[nr++];
        nr = nr&15;
    }
}
//
static void runFrame(PlgGs2xD *plug) {
    plug->crtRunNr++;
    plug->dmaRunMask = 0;
    plug->shvRunMask = 0;
    if(0 == plug->crtRunNr) {
        plug->shvRunMask = 1;
        runDmaInTaskFirst(plug);
        return;
    }
    if(1 == plug->crtRunNr) {
        runDmaInTask(plug);
        plug->shaveTaskDesc[0].crtRunIn[0] = plug->shaveTaskDesc[0].in[0];
        plug->shaveTaskDesc[0].crtRunIn[1] = plug->shaveTaskDesc[0].in[0];
        plug->shaveTaskDesc[0].crtRunIn[2] = plug->shaveTaskDesc[0].in[0];
        plug->shaveTaskDesc[0].crtRunIn[3] = plug->shaveTaskDesc[0].in[1];
        plug->shaveTaskDesc[0].crtRunIn[4] = plug->shaveTaskDesc[0].in[2];
        plug->shaveTaskDesc[0].crtRunIn[5] = plug->shaveTaskDesc[0].in[3];
        plug->shaveTaskDesc[0].crtRunIn[6] = plug->shaveTaskDesc[0].in[4];
        runShvLines(plug);
        return;
    }
    if((plug->nrRunsPerFrame-1) == plug->crtRunNr) {
        runDmaOutTask(plug);
        updateCircularBuffers(plug);
        runShvLines(plug);
        return;
    }
    if((plug->nrRunsPerFrame) == plug->crtRunNr) {
        runDmaOutTask(plug);
        plug->shaveTaskDesc[0].crtRunIn[0] = plug->shaveTaskDesc[0].in[(plug->size.h-6)&15];
        plug->shaveTaskDesc[0].crtRunIn[1] = plug->shaveTaskDesc[0].in[(plug->size.h-5)&15];
        plug->shaveTaskDesc[0].crtRunIn[2] = plug->shaveTaskDesc[0].in[(plug->size.h-4)&15];
        plug->shaveTaskDesc[0].crtRunIn[3] = plug->shaveTaskDesc[0].in[(plug->size.h-3)&15];
        plug->shaveTaskDesc[0].crtRunIn[4] = plug->shaveTaskDesc[0].in[(plug->size.h-2)&15];
        plug->shaveTaskDesc[0].crtRunIn[5] = plug->shaveTaskDesc[0].in[(plug->size.h-1)&15];
        plug->shaveTaskDesc[0].crtRunIn[6] = plug->shaveTaskDesc[0].in[(plug->size.h-1)&15];
        runShvLines(plug);
        return;
    }
    if((plug->nrRunsPerFrame + 1) == plug->crtRunNr) {
        plug->shvRunMask = 1;
        runDmaOutTask(plug);
        return;
    }
    if((plug->nrRunsPerFrame + 2) == plug->crtRunNr) {
        eofFrameHandler(plug);
        return;
    }
    runDmaInOutTask(plug);
    updateCircularBuffers(plug);
    runShvLines(plug);
    return;
}

//
static void producedFrame(FrameT *frame, void *pluginObject) {
    PlgGs2xD *plug = (PlgGs2xD*)pluginObject;
    assert(PLG_STATS_IDLE == plug->plg.status);
    plug->iFrame = frame;
    uint32_t shv;
    if(PLGGS2X_STATS_ON == plug->crtStatus) {
        plug->plg.status = PLG_STATS_RUNNING;
        plug->oFrame = FrameMgrAcquireFrame(plug->outputPools);
        // skip frame if no more output buffer available
        if (NULL == plug->oFrame) { // release input frame and exit, frame will be dropped
            FrameMgrReleaseFrame(plug->iFrame);
            assert(plug->oFrame != NULL); // TODO: delete debug code
            plug->plg.status = PLG_STATS_IDLE;
            return;
        }
        uint32_t heightCropOfset = ((plug->iFrame->height[0] - plug->size.h)>>1)*plug->size.w;
        plug->shaveTaskDesc[0].inAdrFrm  = (uint8_t*)plug->iFrame->fbPtr[0] + heightCropOfset;
        plug->shaveTaskDesc[0].outAdrFrm = (uint8_t*)plug->oFrame->fbPtr[0];
        //plug->size.w                     = plug->iFrame->stride[0];
        //plug->size.h                     = plug->iFrame->height[0];
        plug->nrRunsPerFrame             = plug->size.h>>2;
        plug->crtRunNr                   = -1;
        // run the shave code
        plug->shaveTaskDesc[0].inCnt  = -2;
        plug->shaveTaskDesc[0].outCnt =  0;
        plug->shvRunNr   = 0;
        // dma cnt
        plug->dmaRunNr    = 0;
        plug->dmaRunOutNr = 0;
        runFrame(plug);
    }
}


/**************************************************************************************************
 ~~~ Exported Functions Implementation
 **************************************************************************************************/
void PlgGs2xDCreate(void *pluginObject) {
    PlgGs2xD *plug              = (PlgGs2xD*)pluginObject;
    plug->plg.init              = init;
    plug->plg.fini              = stop;
    plug->cbList[0].callback    = producedFrame;
    plug->cbList[0].pluginObj   = pluginObject;
    plug->plg.callbacks         = plug->cbList;
    plug->plg.status            = PLG_STATS_IDLE;
    plug->crtStatus             = PLGGS2X_STATS_OFF;
    DrvCmxDmaInitDefault();
}

void PlgGs2xDSetParams(void *pluginObject, uint32_t shv, icSize size) {
    PlgGs2xD *plug = (PlgGs2xD*)pluginObject;
    uint32_t x;
    // HARDCODE SIZE !!!
    plug->size.w                     = size.w;
    plug->size.h                     = size.h;
    plug->shvUsed = shv;
    plug->irqLevel = PLGGS2XDSHV__IRQ_LEVEL;
    swcResetShave(shv);
    swcSetAbsoluteDefaultStack(shv);
    swcSetShaveWindowsToDefault(shv);
    assert(plgGs2xDShvImageAddr != 0);
    swcLoadshvdlib((u8*)plgGs2xDShvImageAddr, shv);
    SET_REG_WORD(DCU_ICR(shv), ICR_SWI_ENABLE);
    DrvIcbSetupIrq(IRQ_SVE_0 + shv, plug->irqLevel, POS_EDGE_INT, shvIrqHandler);
    uint8_t *bufAdrI = (uint8_t*)swcSolveShaveRelAddrAHB((uint32_t)&PlgGs2xD_plgGs2xDShvBufIn, shv);
    uint8_t *bufAdrO = (uint8_t*)swcSolveShaveRelAddrAHB((uint32_t)&PlgGs2xD_plgGs2xDShvBufO, shv);
    for (x = 0; x < 16; x++) {
        plug->shaveTaskDesc[0].in[x] = (uint8_t *)&bufAdrI[x* PLGGS2XDSHV_MAX_LINE_SIZE + PLGGS2XDSHV_PADING_SIZE];
    }
    plug->shaveTaskDesc[0].out[0] = &bufAdrO[0 * PLGGS2XDSHV_MAX_LINE_SIZE + PLGGS2XDSHV_PADING_SIZE];
    plug->shaveTaskDesc[0].out[1] = &bufAdrO[1 * PLGGS2XDSHV_MAX_LINE_SIZE + PLGGS2XDSHV_PADING_SIZE];
    plug->shaveTaskDesc[0].out[2] = &bufAdrO[2 * PLGGS2XDSHV_MAX_LINE_SIZE + PLGGS2XDSHV_PADING_SIZE];
    plug->shaveTaskDesc[0].out[3] = &bufAdrO[3 * PLGGS2XDSHV_MAX_LINE_SIZE + PLGGS2XDSHV_PADING_SIZE];

    plug->dmaId  = DrvCmxDmaInitRequester(3);
}

