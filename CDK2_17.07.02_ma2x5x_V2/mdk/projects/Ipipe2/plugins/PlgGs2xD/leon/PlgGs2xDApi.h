/**************************************************************************************************

 @File         : PlgGs2xDApi.c
 @Author       : MT
 @Brief        : Contain  Gauss 5 taps, 2x down-scale plugin
 Date          : 01 - March - 2015
 E-mail        : xxx.xxx@movidius.com
 Copyright     : © Movidius Srl 2014, © Movidius Ltd 2015

 Description :
    This plug-in apply gauss 5x5 down-scale 2x filter on a luma image
    Resources used:
         Leon RT.
         1 Shave, configurable id, dynamic code loaded.
         CmxDma controlled by LeonRt.

    Interrupt base.

 **************************************************************************************************/
#ifndef __PlgGs2xD_API__
#define __PlgGs2xD_API__

/**************************************************************************************************
 ~~~ Included types first then APIs from other modules
 **************************************************************************************************/
#include "PlgTypes.h"
#include "DrvCmxDma.h"

/**************************************************************************************************
 ~~~  Specific #defines
 **************************************************************************************************/


/**************************************************************************************************
 ~~~  Basic typedefs
 **************************************************************************************************/
typedef struct {
    uint8_t *inAdrFrm;
    uint8_t *outAdrFrm;
    uint8_t *in[16];
    uint8_t *out[4];
    uint8_t *crtRunIn[8];
    uint8_t *crtRunOut[2];
    int32_t inCnt;
    int32_t outCnt;
}PlgGs2xDShaveTaskDesc;

typedef struct PlgGs2xDStruct {
    PlgType plg;
    volatile int32_t        crtStatus; // internal usage
    FrameProducedCB         cbList[1];
    FramePool               *outputPools;
    PlgGs2xDShaveTaskDesc   shaveTaskDesc[1];
    icSize                  size;
    FrameT                  *iFrame;
    FrameT                  *oFrame;
    uint32_t                shvUsed;
    uint32_t                irqLevel;
    uint32_t                shvRunNr;
    uint32_t                shvRunMask;
    dmaTransactionList_t    dmaTask[2];
    dmaTransactionList_t    *dmaRef[2];
    dmaRequesterId          dmaId;
    uint32_t                dmaRunNr;
    uint32_t                dmaRunOutNr;
    uint32_t                dmaRunMask;
    int32_t                 nrRunsPerFrame;
    int32_t                 crtRunNr;
}PlgGs2xD;

/**************************************************************************************************
 ~~~  Exported Functions
 **************************************************************************************************/
void PlgGs2xDCreate   (void *pluginObject);
void PlgGs2xDSetParams(void *pluginObject, uint32_t shv, icSize size);

#endif //__PlgGs2xD_API__
