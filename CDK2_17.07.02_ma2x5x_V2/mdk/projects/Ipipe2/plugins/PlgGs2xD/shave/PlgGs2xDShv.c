/**************************************************************************************************

 @File         : PlgGs2xDShv.c
 @Author       : MT
 @Brief        : Contain  Gauss 5 taps, 2x down-scale kernel, shave side
 Date          : 28 - November - 2015
 E-mail        : xxx.xxx@movidius.com
 Copyright     : © Movidius Srl 2014, © Movidius Ltd 2015

 Description :
    This code apply gauss 5x5 down-scale 2x filter on a luma image

 **************************************************************************************************/


/**************************************************************************************************
 ~~~ Included types first then APIs from other modules
 **************************************************************************************************/
#include <stdint.h>
#include <string.h>

/**************************************************************************************************
 ~~~  Specific #defines
 **************************************************************************************************/
#define PLGGS2XDSHV_KERNEL_SIZE 5
#define PLGGS2XDSHV_PADING_SIZE 8
#define PLGGS2XDSHV_MAX_LINE_WIDTH 1936
#define PLGGS2XDSHV_MAX_LINE_SIZE (PLGGS2XDSHV_MAX_LINE_WIDTH+PLGGS2XDSHV_PADING_SIZE*2)
#define PLGGS2XDSHV_BUF_HEIGHT (PLGGS2XDSHV_KERNEL_SIZE+2)
//#define RUN_C_CODE
#ifdef RUN_C_CODE
#define FNCASMC(NAME) (NAME ## C)
#else
#define FNCASMC(NAME) (NAME)
#endif
/**************************************************************************************************
 ~~~  Local variables
 **************************************************************************************************/
// not touched by LEON
uint8_t plgGs2xDShvBufIn[16 * (PLGGS2XDSHV_MAX_LINE_SIZE)];
uint8_t plgGs2xDShvBufO[4 * (PLGGS2XDSHV_MAX_LINE_SIZE)];


/**************************************************************************************************
 ~~~ Function declarations
 **************************************************************************************************/
#ifdef RUN_C_CODE
void mvcvPyrdownC(uint8_t **inLine,uint8_t **out,int width);
#else
void mvcvPyrdown(uint8_t **inLine,uint8_t **out,int width);
#endif


/**************************************************************************************************
 ~~~ Exported Functions Implementation
 **************************************************************************************************/
//                    i18,             i17,   i16,      i15
void* plgGs2xDRunTask(void *plugCall, uint8_t *input[8], uint8_t *output[2], uint32_t width) {
    uint8_t** inLines;
    uint8_t** outLines;
    uint32_t i;
    // make padding
    for (i = 0; i < 7; i++) {
        input[i][-2] = input[i][0];
        input[i][-1] = input[i][0];
        input[i][width]   = input[i][width-1];
        input[i][width+1] = input[i][width-1];
    }
    inLines = (uint8_t**)input;
    outLines = (uint8_t**)output;
    //memcpy(outLines[0], inLines[0],  192);
    //memcpy(outLines[1], inLines[0],  192);
    FNCASMC(mvcvPyrdown)(inLines, outLines, width);

    inLines = (uint8_t**)&input[2];
    outLines = (uint8_t**)&output[1];
    //memcpy(outLines[0], inLines[4],  192);
    FNCASMC(mvcvPyrdown)(inLines, outLines, width);

    __asm("cmu.cpii i18, %0 \n bru.swih 0x1f \n nop 6 \n" : : "r"(plugCall) : "I18");
    return plugCall;
}


/**************************************************************************************************
 ~~~ Local Functions Implementation (C reference code)
 **************************************************************************************************/
#ifdef RUN_C_CODE
void mvcvPyrdownC(uint8_t **inLine,uint8_t **out,int width)
{
    unsigned int gaus_matrix[3] = {16, 64,96 };
    int i, j;
    uint8_t outLine11[1924];
    uint8_t *outLine1;
    uint8_t *outLine;

    outLine1 = outLine11;
    outLine1 = outLine1 + 2;

    outLine=*out;

    for (i = -2; i < width +2; i++)
    {
        outLine1[i] = (((inLine[0][i] + inLine[4][i]) * gaus_matrix[0]) + ((inLine[1][i] + inLine[3][i]) * gaus_matrix[1]) + (inLine[2][i]  * gaus_matrix[2]))>>8;
    }


    for (j = 0; j<width;j+=2)
    {
        outLine[j>>1] = (((outLine1[j-2] + outLine1[j+2]) * gaus_matrix[0]) + ((outLine1[j-1] + outLine1[j+1]) * gaus_matrix[1]) + (outLine1[j]  * gaus_matrix[2]))>>8;
    }

    return;
}

#endif
