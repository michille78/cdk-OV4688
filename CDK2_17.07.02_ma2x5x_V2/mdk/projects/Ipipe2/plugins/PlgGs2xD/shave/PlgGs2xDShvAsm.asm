; ///
; /// @file
; /// @copyright All code copyright Movidius Ltd 2013, all rights reserved.
; ///            For License Warranty see: common/license.txt
; ///
; /// @brief
; ///

.version 00.50.05
.data .rodata.pyrdown
.align 16

.code .text.pyrdown ;text
.align 16
;==================================================================================================================
;==================================== GAUSSIAN DOWNSCALE 2X VERTICAL ================= END ========================
;==================================================================================================================
mvcvPyrdown:
LSU0.LD.32 i17 i17
IAU.SUB i10 i16 0
IAU.ADD i11 i10 16 ;width +padding
IAU.sub i16 i11 0

IAU.SUB i19 i19 i11
IAU.SUB i12 i19 0
IAU.ADD i13 i12 8


LSU0.LDO.64 i0,  i18, 0x00       ||LSU1.LDIL i5, 16
LSU0.LDO.64 i2,  i18, 0x08       ||LSU1.LDIL i6, 64                     ||CMU.CPIVR.x16 v18, i5
LSU0.LDO.32 i4,  i18, 0x10       ||LSU1.LDIL i9, 96                     ||CMU.CPIVR.x16 v17, i6
lsu0.ldil i8, pyrdown__GaussDownV_Loop   ||lsu1.ldih i8, pyrdown__GaussDownV_Loop ||CMU.CPIVR.x16 v16, i9
iau.incs i11, -1
iau.shr.u32 i7, i11, 3
NOP ;myriad2
IAU.SUB i0 i0 8
IAU.SUB i1 i1 8
IAU.SUB i2 i2 8
IAU.SUB i3 i3 8
IAU.SUB i4 i4 8
LSU0.LDI.128.U8.U16 v0, i0
LSU0.LDI.128.U8.U16 v1, i1
LSU0.LDI.128.U8.U16 v2, i2
LSU0.LDI.128.U8.U16 v3, i3
LSU0.LDI.128.U8.U16 v4, i4
NOP ;myriad2
LSU0.LDI.128.U8.U16 v0, i0
VAU.MACPZ.u16 v0, v18           ||LSU0.LDI.128.U8.U16 v1, i1
VAU.MACP.u16  v1, v17           ||LSU0.LDI.128.U8.U16 v2, i2
VAU.MACP.u16  v2, v16           ||LSU0.LDI.128.U8.U16 v3, i3
VAU.MACP.u16  v3, v17           ||LSU0.LDI.128.U8.U16 v4, i4
VAU.MACPW.u16 v7, v18, v4
;//loop
nop 4
.lalign
bru.rpl i8, i7
 pyrdown__GaussDownV_Loop:
LSU0.LDI.128.U8.U16 v0, i0
VAU.MACPZ.u16 v0, v18           ||LSU0.LDI.128.U8.U16 v1, i1
VAU.MACP.u16  v1, v17           ||LSU0.LDI.128.U8.U16 v2, i2
VAU.MACP.u16  v2, v16           ||LSU0.LDI.128.U8.U16 v3, i3    ||CMU.VSZM.BYTE v7, v7, [Z3Z1]
VAU.MACP.u16  v3, v17           ||LSU0.LDI.128.U8.U16 v4, i4    ||LSU1.STI.128.U16.U8 v7, i12
VAU.MACPW.u16 v7, v18, v4
NOP ;myriad2
;// end loop
nop 2 ;myriad2
CMU.VSZM.BYTE v7, v7, [Z3Z1]
LSU1.STI.128.U16.U8 v7, i12
nop

;==================================================================================================================
;=================================== GAUSSIAN DOWNSCALE 2X HORIZONTAL ================START========================
;==================================================================================================================


LSU0.LDI.128.U8.U16 v0, i13      ||LSU1.LDO.16 i3, i13, -2    ||iau.sub i6, i6, i6
LSU0.LDI.128.U8.U16 v1, i13      ||LSU1.LDO.8 i6, i13, 16
LSU0.LDIL i0, 16                ||LSU1.LDIL i1, 64          ||iau.sub i3, i3, i3
LSU1.LDIL i2, 96                ||CMU.CPIVR.x16 v18, i0
CMU.CPIVR.x16 v17, i1           ||iau.sub i4, i4, i4        ||lsu0.ldil i8, pyrdown__GaussDownH_Loop ||lsu1.ldih i8, pyrdown__GaussDownH_Loop
CMU.CPIVR.x16 v16, i2
iau.sub i5, i5, i5
NOP ;myriad2
cmu.vdilv.x16 v5,v4, v0, v1     ||LSU0.LDI.128.U8.U16 v0, i13||LSU1.LDO.16 i3, i13, -2
VAU.MACPZ.u16 v4, v16           ||CMU.ALIGNVEC v6, v4, v4, 2        ||IAU.FEXTU i4, i3, 0, 8     ||LSU0.LDI.128.U8.U16 v1, i13     ||LSU1.LDO.8 i6, i13, 16
VAU.MACP.u16  v5, v17           ||CMU.SHLIV.x16 V4, v4, i4      ||IAU.FEXTU i5, i3, 8, 8
VAU.MACP.u16  v4, v18           ||CMU.SHLIV.x16 V5, v5, i5
VAU.MACP.u16  v5, v17           ||CMU.CPIV.x16 v6.7 i6.l
VAU.MACPW.u16 v7, v18, v6
iau.incs i10, -9
NOP ;myriad2
cmu.vdilv.x16 v5,v4, v0, v1     ||LSU0.LDI.128.U8.U16 v0, i13||LSU1.LDO.16 i3, i13, -2
VAU.MACPZ.u16 v4, v16           ||CMU.ALIGNVEC v6, v4, v4, 2        ||IAU.FEXTU i4, i3, 0, 8     ||LSU0.LDI.128.U8.U16 v1, i13     ||LSU1.LDO.8 i6, i13, 16
VAU.MACP.u16  v5, v17           ||CMU.SHLIV.x16 V4, v4, i4      ||IAU.FEXTU i5, i3, 8, 8
VAU.MACP.u16  v4, v18           ||CMU.SHLIV.x16 V5, v5, i5
VAU.MACP.u16  v5, v17           ||CMU.CPIV.x16 v6.7 i6.l
VAU.MACPW.u16 v7, v18, v6       ||CMU.VSZM.BYTE v8, v7, [Z3Z1]
iau.shr.u32 i7, i10, 3
;// loop
nop 3
.lalign
bru.rpl i8, i7                  ||cmu.vdilv.x16 v5,v4, v0, v1   || LSU0.LDI.128.U8.U16 v0, i13||LSU1.LDO.16 i3, i13, -2
 pyrdown__GaussDownH_Loop:
 .nowarn 9
VAU.MACPZ.u16 v4, v16           || CMU.ALIGNVEC v6, v4, v4, 2           ||IAU.FEXTU i4, i3, 0, 8 ||LSU0.LDI.128.U8.U16 v1, i13     ||LSU1.LDO.8 i6, i13, 16
.nowarnend
VAU.MACP.u16  v5, v17           || CMU.SHLIV.x16 V4, v4, i4         ||IAU.FEXTU i5, i3, 8, 8
VAU.MACP.u16  v4, v18           || CMU.SHLIV.x16 V5, v5, i5         ||LSU0.STI.128.U16.U8 v8, i17
VAU.MACP.u16  v5, v17           || CMU.CPIV.x16 v6.7 i6.l
VAU.MACPW.u16 v7, v18, v6       || CMU.VSZM.BYTE v8, v7, [Z3Z1]
nop
NOP ;myriad2
;//end loop
BRU.JMP i30                     ||LSU0.STI.128.U16.U8 v8, i17
nop
nop
CMU.VSZM.BYTE v8, v7, [Z3Z1]
LSU0.STI.128.U16.U8 v8, i17
IAU.ADD i19 i19 i16
NOP ;myriad2

.end
