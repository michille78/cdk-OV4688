ifeq ($(LEON_RT_BUILD),yes)
	PLGDM_PREFIX=lrt_
else
	PLGDM_PREFIX=
endif
#ENTRYPOINTS = -e runTask  --gc-sections
$(PlgDmShv).mvlib : $(SHAVE_PlgDmShv_OBJS) $(PROJECT_SHAVE_LIBS)
	$(ECHO) $(LD)  $(MVLIBOPT) $(SHAVE_PlgDmShv_OBJS) $(PROJECT_SHAVE_LIBS) $(CompilerANSILibs) -o $@
#This creates a binary file packing the shvdlib file
$(PlgDmShv)_bin.o : $(PlgDmShv).shvdlib
	$(ECHO) $(OBJCOPY)  -I binary --rename-section .data=.data.plgShvImageAddr \
	--redefine-sym  _binary_$(subst /,_,$(subst .,_,$<))_start=$(PLGDM_PREFIX)plgShvImageAddr \
	-O elf32-sparc -B sparc $< $@

$(PlgDmShv)_sym.o : $(PlgDmShv).shvdcomplete
	echo BBBB $(PlgDmShv)_sym.o
	$(ECHO) $(OBJCOPY) --prefix-symbols=$(PLGDM_PREFIX)PlgDmShv_ --extract-symbol $< $@

