/**************************************************************************************************

 @File         : dmShv.h
 @Author       : MT
 @Brief        : Depth map calculation base on SAD filter, horizontal direction
 Date          : 06 - August - 2015
 E-mail        : xxx.xxx@movidius.com
 Copyright     : © Movidius Srl 2014, © Movidius Ltd 2015

 Description :


 **************************************************************************************************/


/**************************************************************************************************
 ~~~ Included types first then APIs from other modules
 **************************************************************************************************/

#include <stdint.h>
#include <string.h>

/**************************************************************************************************
 ~~~  Specific #defines
 **************************************************************************************************/
//#define RUN_C_CODE

#ifndef SRCH_SIZE
#define SRCH_SIZE 16  //Search area from current pixel pos in X-dir
#endif
#ifndef SRCH_INCR
#define SRCH_INCR 1
#endif
#define NRM_FACTOR ((255)/SRCH_SIZE) // normalization factor
#define SAD_KSIZE 4
#ifndef MAXSHV_W
#define MAXSHV_W 4096
#endif
#ifndef MAXSHV_PADDING
#define MAXSHV_PADDING 64
#endif
#ifndef MAXSHV_NRLINES
#define MAXSHV_NRLINES 8
#endif
#define ABSI16(X) (X<0?((uint16_t)(-X)):((uint16_t)X))
#ifdef RUN_C_CODE
#define FNCASMC(NAME) (NAME ## C)
#else
#define FNCASMC(NAME) (NAME)
#endif
/**************************************************************************************************
 ~~~  Local variables
 **************************************************************************************************/
// not touched by LEON
uint8_t shvBufL[MAXSHV_NRLINES * (MAXSHV_W + MAXSHV_PADDING*2)];
uint8_t shvBufR[MAXSHV_NRLINES * (MAXSHV_W + MAXSHV_PADDING*2)];
uint8_t shvBufO[2 * (MAXSHV_W + MAXSHV_PADDING*2)];
// BestSad
uint16_t bestSad[MAXSHV_W];
uint8_t  bestIdxL[MAXSHV_W];


/**************************************************************************************************
 ~~~ Function declarations
 **************************************************************************************************/
#ifdef RUN_C_CODE
void calculateSadC(uint16_t *sad, uint8_t *bestIdx, uint8_t *left[4], uint8_t *right[4],
        uint32_t size, uint8_t crtShift);
void calculateSadChoseC(uint16_t *sad, uint8_t *bestIdx, uint8_t *left[4], uint8_t *right[4],
        uint32_t size, uint8_t crtShift);
void copyBestIdxWithRepetionC(uint8_t *bestIdx, uint8_t *bestIdxL, uint32_t w);
#else
void calculateSad(uint16_t *sad, uint8_t *bestIdx, uint8_t *left[4], uint8_t *right[4],
        uint32_t size, uint8_t crtShift);
void calculateSadChose(uint16_t *sad, uint8_t *bestIdx, uint8_t *left[4], uint8_t *right[4],
        uint32_t size, uint8_t crtShift);
void copyBestIdxWithRepetion(uint8_t *bestIdx, uint8_t *bestIdxL, uint32_t w);
#endif


/**************************************************************************************************
 ~~~ Exported Functions Implementation
 **************************************************************************************************/
int dd = 0;
//                             i18,             i17,             i16,               i15,            i14
void* runTask(void *plugCall, uint8_t *bestIdx, uint8_t *left[4], uint8_t *right[4], uint32_t w) {
    dd++;
    int32_t i = 0;
    shvBufL[0] = 0;
    shvBufR[0] = 0;
    shvBufO[0] = 0;
    uint8_t *rightLoc[4];
    uint8_t *leftLoc[4];

    leftLoc[0] = left[0];
    leftLoc[1] = left[1];
    leftLoc[2] = left[2];
    leftLoc[3] = left[3];
    i = -SRCH_SIZE;
    rightLoc[0] = right[0] + i;
    rightLoc[1] = right[1] + i;
    rightLoc[2] = right[2] + i;
    rightLoc[3] = right[3] + i;

    FNCASMC(calculateSad)(bestSad, bestIdxL, leftLoc, rightLoc, w, (SRCH_SIZE*NRM_FACTOR));

    for (i =-SRCH_SIZE+SRCH_INCR; i<=SRCH_SIZE; i+=SRCH_INCR) {
        rightLoc[0] = right[0] + i;
        rightLoc[1] = right[1] + i;
        rightLoc[2] = right[2] + i;
        rightLoc[3] = right[3] + i;
        FNCASMC(calculateSadChose)(bestSad, bestIdxL,  leftLoc, rightLoc, w, (uint8_t)((ABSI16(i)*NRM_FACTOR)));
        //i = SRCH_SIZE;
    }

    FNCASMC(copyBestIdxWithRepetion)(bestIdx, bestIdxL,  w);
    __asm("cmu.cpii i18, %0 \n bru.swih 0x1f \n nop 6 \n" : : "r"(plugCall) : "I18");
    return plugCall;
}


/**************************************************************************************************
 ~~~ Local Functions Implementation (C reference code)
 **************************************************************************************************/
#ifdef RUN_C_CODE
uint16_t absI16Dif (uint8_t a, uint8_t b) {
    int16_t rez = a-b;
    if (rez < 0) return (-rez);
    return (rez);
}

void calculateSadC(uint16_t *sad, uint8_t *bestIdx, uint8_t *left[4], uint8_t *right[4], uint32_t size, uint8_t crtShift) {
    uint32_t i;
    uint32_t  idx = 0;
    for(i = 0; i < size; i+=SAD_KSIZE) {
        sad[idx] = absI16Dif(left[0][i+0], right[0][i+0]) +
                 absI16Dif(left[0][i+1], right[0][i+1]) +
                 absI16Dif(left[0][i+2], right[0][i+2]) +
                 absI16Dif(left[0][i+3], right[0][i+3]) +
                 absI16Dif(left[1][i+0], right[1][i+0]) +
                 absI16Dif(left[1][i+1], right[1][i+1]) +
                 absI16Dif(left[1][i+2], right[1][i+2]) +
                 absI16Dif(left[1][i+3], right[1][i+3]) +
                 absI16Dif(left[2][i+0], right[2][i+0]) +
                 absI16Dif(left[2][i+1], right[2][i+1]) +
                 absI16Dif(left[2][i+2], right[2][i+2]) +
                 absI16Dif(left[2][i+3], right[2][i+3]) +
                 absI16Dif(left[3][i+0], right[3][i+0]) +
                 absI16Dif(left[3][i+1], right[3][i+1]) +
                 absI16Dif(left[3][i+2], right[3][i+2]) +
                 absI16Dif(left[3][i+3], right[3][i+3]) ;
        idx++;
        bestIdx[idx] = crtShift;
    }
}


void calculateSadChoseC(uint16_t *sad, uint8_t *bestIdx, uint8_t *left[4], uint8_t *right[4], uint32_t size, uint8_t crtShift) {
    uint32_t i;
    uint16_t lSad = 0;
    uint32_t  idx = 0;
    for(i = 0; i < size; i+=SAD_KSIZE) {
        lSad =   absI16Dif(left[0][i+0], right[0][i+0]) +
                 absI16Dif(left[0][i+1], right[0][i+1]) +
                 absI16Dif(left[0][i+2], right[0][i+2]) +
                 absI16Dif(left[0][i+3], right[0][i+3]) +
                 absI16Dif(left[1][i+0], right[1][i+0]) +
                 absI16Dif(left[1][i+1], right[1][i+1]) +
                 absI16Dif(left[1][i+2], right[1][i+2]) +
                 absI16Dif(left[1][i+3], right[1][i+3]) +
                 absI16Dif(left[2][i+0], right[2][i+0]) +
                 absI16Dif(left[2][i+1], right[2][i+1]) +
                 absI16Dif(left[2][i+2], right[2][i+2]) +
                 absI16Dif(left[2][i+3], right[2][i+3]) +
                 absI16Dif(left[3][i+0], right[3][i+0]) +
                 absI16Dif(left[3][i+1], right[3][i+1]) +
                 absI16Dif(left[3][i+2], right[3][i+2]) +
                 absI16Dif(left[3][i+3], right[3][i+3]) ;
        if(lSad<sad[idx]) {
            sad[idx] = lSad;
            bestIdx[idx] = crtShift;
        }
        idx++;
    }
}


void copyBestIdxWithRepetionC(uint8_t *bestIdx, uint8_t *bestIdxL, uint32_t w) {
    uint32_t i;
    uint32_t  size = w>>2;
    for(i = 0; i < size; i++) {
        bestIdx[i*4+0] = bestIdxL[i];
        bestIdx[i*4+1] = bestIdxL[i];
        bestIdx[i*4+2] = bestIdxL[i];
        bestIdx[i*4+3] = bestIdxL[i];
    }
}
#endif
