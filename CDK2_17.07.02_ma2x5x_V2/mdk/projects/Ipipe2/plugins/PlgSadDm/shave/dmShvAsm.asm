; ///
; /// @file
; /// @copyright All code copyright Movidius Ltd 2013, all rights reserved.
; ///            For License Warranty see: common/license.txt
; ///
; /// @brief calculate sad 4x4 in order to identify best distance
; ///

;--------------------------------------------------------------------------------------------------------------
.version 00.50.37.8
.data .rodata.calculateSadData


.code   .text.calculateSad
;                    i18             i17               i16                  i15                i14               i13
;void calculateSad(uint16_t *sad, uint8_t *bestIdx, uint8_t *left[4], uint8_t *right[4], uint32_t size, uint8_t crtShift)
.lalign
calculateSad:
LSU0.LDO.32 i0 i16 0 || LSU1.LDO.32 i4 i15 0
LSU0.LDO.32 i1 i16 4 || LSU1.LDO.32 i5 i15 4
LSU0.LDO.32 i2 i16 8 || LSU1.LDO.32 i6 i15 8
LSU0.LDO.32 i3 i16 12|| LSU1.LDO.32 i7 i15 12
lsu0.ldil i8, 0x08
cmu.cpivr.x8 v21, i13
iau.incs i14, 31
lsu0.ldi.128.u8.u16 v0, i0, i8 || lsu1.ldi.128.u8.u16 v4, i4, i8
lsu0.ldi.128.u8.u16 v1, i1, i8 || lsu1.ldi.128.u8.u16 v5, i5, i8
lsu0.ldi.128.u8.u16 v2, i2, i8 || lsu1.ldi.128.u8.u16 v6, i6, i8
lsu0.ldi.128.u8.u16 v3, i3, i8 || lsu1.ldi.128.u8.u16 v7, i7, i8
nop
nop
lsu0.ldi.128.u8.u16 v0, i0, i8 || lsu1.ldi.128.u8.u16 v4, i4, i8
lsu0.ldi.128.u8.u16 v1, i1, i8 || lsu1.ldi.128.u8.u16 v5, i5, i8 ||vau.adiff.u16 v8, v0, v4
lsu0.ldi.128.u8.u16 v2, i2, i8 || lsu1.ldi.128.u8.u16 v6, i6, i8 ||vau.adiff.u16 v9, v1, v5
lsu0.ldi.128.u8.u16 v3, i3, i8 || lsu1.ldi.128.u8.u16 v7, i7, i8 ||vau.adiff.u16 v10, v2, v6
                                                                   vau.adiff.u16 v11, v3, v7 ||LSU0.LDIL i10 calculateSadStartLoop   ||  LSU1.LDIH i10 calculateSadStartLoop  ||iau.shr.u32 i14, i14, 5
                                                                   vau.iadds.u16 v12, v8, v9 ||iau.incs i14, -1
lsu0.ldi.128.u8.u16 v0, i0, i8 || lsu1.ldi.128.u8.u16 v4, i4, i8 ||vau.iadds.u16 v13, v10, v11
lsu0.ldi.128.u8.u16 v1, i1, i8 || lsu1.ldi.128.u8.u16 v5, i5, i8 ||vau.adiff.u16 v8, v0, v4
lsu0.ldi.128.u8.u16 v2, i2, i8 || lsu1.ldi.128.u8.u16 v6, i6, i8 ||vau.adiff.u16 v9, v1, v5
lsu0.ldi.128.u8.u16 v3, i3, i8 || lsu1.ldi.128.u8.u16 v7, i7, i8 ||vau.adiff.u16 v10, v2, v6
                                                                   vau.adiff.u16 v11, v3, v7
                                                                   vau.iadds.u16 v14, v8, v9
lsu0.ldi.128.u8.u16 v0, i0, i8 || lsu1.ldi.128.u8.u16 v4, i4, i8 ||vau.iadds.u16 v15, v10, v11
lsu0.ldi.128.u8.u16 v1, i1, i8 || lsu1.ldi.128.u8.u16 v5, i5, i8 ||vau.iadds.u16 v16, v12, v13
lsu0.ldi.128.u8.u16 v2, i2, i8 || lsu1.ldi.128.u8.u16 v6, i6, i8 ||vau.iadds.u16 v17, v14, v15
lsu0.ldi.128.u8.u16 v3, i3, i8 || lsu1.ldi.128.u8.u16 v7, i7, i8 ||vau.adiff.u16 v8, v0, v4
                                                                   vau.adiff.u16 v9, v1, v5
                                                                   vau.adiff.u16 v10, v2, v6
                                                                   vau.adiff.u16 v11, v3, v7
                                                                   vau.iadds.u16 v12, v8, v9
                                                                   vau.iadds.u16 v13, v10, v11
                                                                   vau.adiff.u16 v8, v0, v4
                                                                   vau.adiff.u16 v9, v1, v5
                                                                   vau.adiff.u16 v10, v2, v6   ||lsu0.ldi.128.u8.u16 v0, i0, i8 || lsu1.ldi.128.u8.u16 v4, i4, i8
                                                                   vau.adiff.u16 v11, v3, v7   ||lsu0.ldi.128.u8.u16 v1, i1, i8 || lsu1.ldi.128.u8.u16 v5, i5, i8
                                                                   vau.iadds.u16 v14, v8, v9   ||lsu0.ldi.128.u8.u16 v2, i2, i8 || lsu1.ldi.128.u8.u16 v6, i6, i8
                                                                   vau.iadds.u16 v15, v10, v11 ||lsu0.ldi.128.u8.u16 v3, i3, i8 || lsu1.ldi.128.u8.u16 v7, i7, i8
                                                                   vau.iadds.u16 v18, v12, v13
                                                                   vau.iadds.u16 v19, v14, v15
.lalign
bru.rpl i10, i14, calculateSadStartLoopDelaySlot ||lsu0.ldi.128.u8.u16 v0, i0, i8 || lsu1.ldi.128.u8.u16 v4, i4, i8                                    ||cmu.vdilv.x16 v16, v17, v16, v17
                                                                                                    cmu.vdilv.x16 v18, v19, v18, v19 ||vau.iadds.u16 v16, v16, v17
                                                                                                    vau.iadds.u16 v18, v18, v19
lsu0.ldi.128.u8.u16 v1, i1, i8 || lsu1.ldi.128.u8.u16 v5, i5, i8 ||vau.adiff.u16 v8, v0, v4
lsu0.ldi.128.u8.u16 v2, i2, i8 || lsu1.ldi.128.u8.u16 v6, i6, i8 ||vau.adiff.u16 v9, v1, v5    ||cmu.vdilv.x16 v16, v18, v16, v18
lsu0.ldi.128.u8.u16 v3, i3, i8 || lsu1.ldi.128.u8.u16 v7, i7, i8 ||vau.adiff.u16 v10, v2, v6
                                                                   vau.adiff.u16 v11, v3, v7
                                                                                        vau.iadds.u16 v20, v16, v18
                                                                   vau.iadds.u16 v12, v8, v9
lsu0.ldi.128.u8.u16 v0, i0, i8 || lsu1.ldi.128.u8.u16 v4, i4, i8 ||vau.iadds.u16 v13, v10, v11
lsu0.ldi.128.u8.u16 v1, i1, i8 || lsu1.ldi.128.u8.u16 v5, i5, i8 ||vau.adiff.u16 v8, v0, v4
lsu0.ldi.128.u8.u16 v2, i2, i8 || lsu1.ldi.128.u8.u16 v6, i6, i8 ||vau.adiff.u16 v9, v1, v5
lsu0.ldi.128.u8.u16 v3, i3, i8 || lsu1.ldi.128.u8.u16 v7, i7, i8 ||vau.adiff.u16 v10, v2, v6
                                                                   vau.adiff.u16 v11, v3, v7
                                                                   vau.iadds.u16 v14, v8, v9
lsu0.ldi.128.u8.u16 v0, i0, i8 || lsu1.ldi.128.u8.u16 v4, i4, i8 ||vau.iadds.u16 v15, v10, v11
lsu0.ldi.128.u8.u16 v1, i1, i8 || lsu1.ldi.128.u8.u16 v5, i5, i8 ||vau.iadds.u16 v16, v12, v13
lsu0.ldi.128.u8.u16 v2, i2, i8 || lsu1.ldi.128.u8.u16 v6, i6, i8 ||vau.iadds.u16 v17, v14, v15
lsu0.ldi.128.u8.u16 v3, i3, i8 || lsu1.ldi.128.u8.u16 v7, i7, i8 ||vau.adiff.u16 v8, v0, v4
                                                                   vau.adiff.u16 v9, v1, v5  ||lsu0.sto.64.l v20, i18, 0x00 || lsu1.sto.64.h v20, i18, 0x08 || iau.incs i18, 0x10
                                                                   vau.adiff.u16 v10, v2, v6 ||lsu0.sto.64.l v21, i17, 0x00 || iau.incs i17, 0x08
                                                                   vau.adiff.u16 v11, v3, v7
                                                                   vau.iadds.u16 v12, v8, v9
                                                                   vau.iadds.u16 v13, v10, v11
                                                                   vau.adiff.u16 v8, v0, v4

                                                                   vau.adiff.u16 v9, v1, v5

                                                                   vau.adiff.u16 v10, v2, v6   ||lsu0.ldi.128.u8.u16 v0, i0, i8 || lsu1.ldi.128.u8.u16 v4, i4, i8
                                                                   vau.adiff.u16 v11, v3, v7   ||lsu0.ldi.128.u8.u16 v1, i1, i8 || lsu1.ldi.128.u8.u16 v5, i5, i8
                                                                   vau.iadds.u16 v14, v8, v9   ||lsu0.ldi.128.u8.u16 v2, i2, i8 || lsu1.ldi.128.u8.u16 v6, i6, i8
                                                                   vau.iadds.u16 v15, v10, v11 ||lsu0.ldi.128.u8.u16 v3, i3, i8 || lsu1.ldi.128.u8.u16 v7, i7, i8
                                                                   vau.iadds.u16 v18, v12, v13
                                                                   vau.iadds.u16 v19, v14, v15
calculateSadStartLoop:
                                                                   nop 7 ;TODO(Marius): If have code here, loop become slower, investigate
calculateSadStartLoopDelaySlot:


cmu.vdilv.x16 v16, v17, v16, v17
cmu.vdilv.x16 v18, v19, v18, v19 ||vau.iadds.u16 v16, v16, v17
vau.iadds.u16 v18, v18, v19 ||bru.jmp i30
nop
cmu.vdilv.x16 v16, v18, v16, v18
vau.iadds.u16 v20, v16, v18
nop
lsu0.sto.64.l v20, i18, 0x00 || lsu1.sto.64.h v20, i18, 0x08 || iau.incs i18, 0x10
lsu0.sto.64.l v21, i17, 0x00 || iau.incs i17, 0x08


.code   .text.calculateSadChose
;                            i18             i17               i16                  i15                i14               i13
;void calculateSadChose(uint16_t *sad, uint8_t *bestIdx, uint8_t *left[4], uint8_t *right[4], uint32_t size, uint8_t crtShift)
.lalign
calculateSadChose:


LSU0.LDO.32 i0 i16 0 || LSU1.LDO.32 i4 i15 0
LSU0.LDO.32 i1 i16 4 || LSU1.LDO.32 i5 i15 4
LSU0.LDO.32 i2 i16 8 || LSU1.LDO.32 i6 i15 8
LSU0.LDO.32 i3 i16 12|| LSU1.LDO.32 i7 i15 12
lsu0.ldil i8, 0x08
cmu.cpivr.x8 v21, i13
iau.incs i14, 31
lsu0.ldi.128.u8.u16 v0, i0, i8 || lsu1.ldi.128.u8.u16 v4, i4, i8
lsu0.ldi.128.u8.u16 v1, i1, i8 || lsu1.ldi.128.u8.u16 v5, i5, i8
lsu0.ldi.128.u8.u16 v2, i2, i8 || lsu1.ldi.128.u8.u16 v6, i6, i8
lsu0.ldi.128.u8.u16 v3, i3, i8 || lsu1.ldi.128.u8.u16 v7, i7, i8
nop
nop
lsu0.ldi.128.u8.u16 v0, i0, i8 || lsu1.ldi.128.u8.u16 v4, i4, i8
lsu0.ldi.128.u8.u16 v1, i1, i8 || lsu1.ldi.128.u8.u16 v5, i5, i8 ||vau.adiff.u16 v8, v0, v4
lsu0.ldi.128.u8.u16 v2, i2, i8 || lsu1.ldi.128.u8.u16 v6, i6, i8 ||vau.adiff.u16 v9, v1, v5
lsu0.ldi.128.u8.u16 v3, i3, i8 || lsu1.ldi.128.u8.u16 v7, i7, i8 ||vau.adiff.u16 v10, v2, v6
                                                                   vau.adiff.u16 v11, v3, v7 ||LSU0.LDIL i10 calculateSadChoseStartLoop   ||  LSU1.LDIH i10 calculateSadChoseStartLoop  ||iau.shr.u32 i14, i14, 5
                                                                   vau.iadds.u16 v12, v8, v9 ||iau.incs i14, -1
lsu0.ldi.128.u8.u16 v0, i0, i8 || lsu1.ldi.128.u8.u16 v4, i4, i8 ||vau.iadds.u16 v13, v10, v11
lsu0.ldi.128.u8.u16 v1, i1, i8 || lsu1.ldi.128.u8.u16 v5, i5, i8 ||vau.adiff.u16 v8, v0, v4
lsu0.ldi.128.u8.u16 v2, i2, i8 || lsu1.ldi.128.u8.u16 v6, i6, i8 ||vau.adiff.u16 v9, v1, v5
lsu0.ldi.128.u8.u16 v3, i3, i8 || lsu1.ldi.128.u8.u16 v7, i7, i8 ||vau.adiff.u16 v10, v2, v6
                                                                   vau.adiff.u16 v11, v3, v7
                                                                   vau.iadds.u16 v14, v8, v9
lsu0.ldi.128.u8.u16 v0, i0, i8 || lsu1.ldi.128.u8.u16 v4, i4, i8 ||vau.iadds.u16 v15, v10, v11
lsu0.ldi.128.u8.u16 v1, i1, i8 || lsu1.ldi.128.u8.u16 v5, i5, i8 ||vau.iadds.u16 v16, v12, v13
lsu0.ldi.128.u8.u16 v2, i2, i8 || lsu1.ldi.128.u8.u16 v6, i6, i8 ||vau.iadds.u16 v17, v14, v15
lsu0.ldi.128.u8.u16 v3, i3, i8 || lsu1.ldi.128.u8.u16 v7, i7, i8 ||vau.adiff.u16 v8, v0, v4
                                                                   vau.adiff.u16 v9, v1, v5
                                                                   vau.adiff.u16 v10, v2, v6
                                                                   vau.adiff.u16 v11, v3, v7
                                                                   vau.iadds.u16 v12, v8, v9
                                                                   vau.iadds.u16 v13, v10, v11
                                                                   vau.adiff.u16 v8, v0, v4
                                                                   vau.adiff.u16 v9, v1, v5
                                                                   vau.adiff.u16 v10, v2, v6    ||lsu0.ldi.128.u8.u16 v0, i0, i8 || lsu1.ldi.128.u8.u16 v4, i4, i8
                                                                   vau.adiff.u16 v11, v3, v7    ||lsu0.ldi.128.u8.u16 v1, i1, i8 || lsu1.ldi.128.u8.u16 v5, i5, i8
                                                                   vau.iadds.u16 v14, v8, v9    ||lsu0.ldi.128.u8.u16 v2, i2, i8 || lsu1.ldi.128.u8.u16 v6, i6, i8
                                                                   vau.iadds.u16 v15, v10, v11  ||lsu0.ldi.128.u8.u16 v3, i3, i8 || lsu1.ldi.128.u8.u16 v7, i7, i8
                                                                   vau.iadds.u16 v18, v12, v13
                                                                   vau.iadds.u16 v19, v14, v15

.lalign
bru.rpl i10, i14, calculateSadChoseStartLoopDelaySlot||lsu0.ldi.128.u8.u16 v0, i0, i8 || lsu1.ldi.128.u8.u16 v4, i4, i8                                    ||cmu.vdilv.x16 v16, v17, v16, v17
                                                                                                    cmu.vdilv.x16 v18, v19, v18, v19 ||vau.iadds.u16 v16, v16, v17
                                                                                                    vau.iadds.u16 v18, v18, v19
lsu0.ldi.128.u8.u16 v1, i1, i8 || lsu1.ldi.128.u8.u16 v5, i5, i8 ||vau.adiff.u16 v8, v0, v4
lsu0.ldi.128.u8.u16 v2, i2, i8 || lsu1.ldi.128.u8.u16 v6, i6, i8 ||vau.adiff.u16 v9, v1, v5    ||cmu.vdilv.x16 v16, v18, v16, v18
lsu0.ldi.128.u8.u16 v3, i3, i8 || lsu1.ldi.128.u8.u16 v7, i7, i8 ||vau.adiff.u16 v10, v2, v6
                                                                   vau.adiff.u16 v11, v3, v7 ||lsu0.ldo.64.l v22, i18, 0x00 || lsu1.ldo.64.h v22, i18, 0x08
                                                                                        vau.iadds.u16 v20, v16, v18
                                                                   vau.iadds.u16 v12, v8, v9
lsu0.ldi.128.u8.u16 v0, i0, i8 || lsu1.ldi.128.u8.u16 v4, i4, i8 ||vau.iadds.u16 v13, v10, v11
lsu0.ldi.128.u8.u16 v1, i1, i8 || lsu1.ldi.128.u8.u16 v5, i5, i8 ||vau.adiff.u16 v8, v0, v4
lsu0.ldi.128.u8.u16 v2, i2, i8 || lsu1.ldi.128.u8.u16 v6, i6, i8 ||vau.adiff.u16 v9, v1, v5
lsu0.ldi.128.u8.u16 v3, i3, i8 || lsu1.ldi.128.u8.u16 v7, i7, i8 ||vau.adiff.u16 v10, v2, v6
                                                                   vau.adiff.u16 v11, v3, v7 ||cmu.min.u16 v23, v22, v20
                                                                   vau.iadds.u16 v14, v8, v9
lsu0.ldi.128.u8.u16 v0, i0, i8 || lsu1.ldi.128.u8.u16 v4, i4, i8 ||vau.iadds.u16 v15, v10, v11
lsu0.ldi.128.u8.u16 v1, i1, i8 || lsu1.ldi.128.u8.u16 v5, i5, i8 ||vau.iadds.u16 v16, v12, v13
lsu0.ldi.128.u8.u16 v2, i2, i8 || lsu1.ldi.128.u8.u16 v6, i6, i8 ||vau.iadds.u16 v17, v14, v15
lsu0.ldi.128.u8.u16 v3, i3, i8 || lsu1.ldi.128.u8.u16 v7, i7, i8 ||vau.adiff.u16 v8, v0, v4
                                                                   vau.adiff.u16 v9, v1, v5
                                                                   vau.adiff.u16 v10, v2, v6
                                                                   vau.adiff.u16 v11, v3, v7
                                                                   vau.iadds.u16 v12, v8, v9    ||lsu0.sto.64.l v23, i18, 0x00 || lsu1.sto.64.h v23, i18, 0x08 || iau.incs i18, 0x10
                                                                   vau.iadds.u16 v13, v10, v11  ||cmu.cmvv.u16 v20, v22
                                                                   vau.adiff.u16 v8, v0, v4     ||peu.pvl08 LT || lsu0.sto.64.l v21 i17 0
calculateSadChoseStartLoop:
                                                                   vau.adiff.u16 v9, v1, v5     ||iau.incs i17, 0x08
                                                                   vau.adiff.u16 v10, v2, v6    ||lsu0.ldi.128.u8.u16 v0, i0, i8 || lsu1.ldi.128.u8.u16 v4, i4, i8
                                                                   vau.adiff.u16 v11, v3, v7    ||lsu0.ldi.128.u8.u16 v1, i1, i8 || lsu1.ldi.128.u8.u16 v5, i5, i8
                                                                   vau.iadds.u16 v14, v8, v9    ||lsu0.ldi.128.u8.u16 v2, i2, i8 || lsu1.ldi.128.u8.u16 v6, i6, i8
                                                                   vau.iadds.u16 v15, v10, v11  ||lsu0.ldi.128.u8.u16 v3, i3, i8 || lsu1.ldi.128.u8.u16 v7, i7, i8
                                                                   vau.iadds.u16 v18, v12, v13
                                                                   vau.iadds.u16 v19, v14, v15
calculateSadChoseStartLoopDelaySlot:
cmu.vdilv.x16 v16, v17, v16, v17 ||lsu0.ldo.64.l v8, i18, 0x00 || lsu1.ldo.64.h v8, i18, 0x08
cmu.vdilv.x16 v18, v19, v18, v19 ||vau.iadds.u16 v16, v16, v17
vau.iadds.u16 v18, v18, v19
nop
cmu.vdilv.x16 v16, v18, v16, v18 ||bru.jmp i30
vau.iadds.u16 v20, v16, v18
nop
cmu.min.u16 v23, v8, v20
nop
lsu0.sto.64.l v23, i18, 0x00 || lsu1.sto.64.h v23, i18, 0x08 || iau.incs i18, 0x10 ||cmu.cmvv.u16 v20, v8
peu.pvl08 LT || lsu0.sto.64.l v21 i17 0 ||iau.incs i17, 0x08



.code   .text.copyBestIdxWithRepetion
;                                  i18,                 i17, i16
;void copyBestIdxWithRepetionC(uint8_t *bestIdx, uint8_t *bestIdxL, uint32_t w)
.lalign
copyBestIdxWithRepetion:
lsu0.ldo.64.l v0, i17, 0x00 ||lsu1.ldo.64.h v0, i17, 0x08 || iau.incs i17, 0x10
iau.incs i16, 63
LSU0.LDIL i10 copyBestIdxWithRepetionStartLoop   ||  LSU1.LDIH i10 copyBestIdxWithRepetionStartLoop  ||iau.shr.u32 i16, i16, 6
nop 4
.lalign
copyBestIdxWithRepetionStartLoop:
bru.rpl i10, i16, copyBestIdxWithRepetionStartLoopDelaySlot
cmu.vilv.x8 v1, v2, v0, v0 ||lsu0.ldo.64.l v0, i17, 0x00 ||lsu1.ldo.64.h v0, i17, 0x08 || iau.incs i17, 0x10
cmu.vilv.x16 v3, v4, v1, v1
cmu.vilv.x16 v5, v6, v2, v2 ||lsu0.sto.64.l v3, i18, 0x00 || lsu1.sto.64.h v3, i18, 0x08
lsu0.sto.64.l v4, i18, 0x10 || lsu1.sto.64.h v4, i18, 0x18
lsu0.sto.64.l v5, i18, 0x20 || lsu1.sto.64.h v5, i18, 0x28
lsu0.sto.64.l v6, i18, 0x30 || lsu1.sto.64.h v6, i18, 0x38 || iau.incs i18, 0x40
copyBestIdxWithRepetionStartLoopDelaySlot:
bru.jmp i30
nop 6
.end
	
	 
		

	
	
