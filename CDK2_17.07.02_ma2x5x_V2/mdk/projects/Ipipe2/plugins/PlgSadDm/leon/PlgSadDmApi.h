/**************************************************************************************************

 @File         : PlgSadDmApi.h
 @Author       : MT
 @Brief        : Contain Demo SAD base dense depth map plugin interface
 Date          : 01 - March - 2015
 E-mail        : xxx.xxx@movidius.com
 Copyright     : © Movidius Srl 2014, © Movidius Ltd 2015

 Description :
    This plug-in calculate a dense depth map, base just on sum of absolute difference filter.
     Resources used:
         Leon RT.
         Shaves, configurable number of shaves used, dynamic code loaded.
         CmxDma controled by LeonRt.

    Interrupt base.

 **************************************************************************************************/
#ifndef __PLG_DM_API__
#define __PLG_DM_API__

/**************************************************************************************************
 ~~~ Included types first then APIs from other modules
 **************************************************************************************************/
#include "PlgTypes.h"
#include "DrvCmxDma.h"


/**************************************************************************************************
 ~~~  Specific #defines
 **************************************************************************************************/
#define PLGDM_IRQ_LEVEL  3
#define PLGDM_PROC_PATCH  4
// this have to be identical with shaves defines,
#define PLGDM_MAX_PADSIZE  64
#ifndef PLGDM_MAX_LINEWIDTH
#define PLGDM_MAX_LINEWIDTH 4096
#endif
#define PLGDM_CMX_LINE_STRIDE (PLGDM_MAX_LINEWIDTH + PLGDM_MAX_PADSIZE*2)
#define PROC_PATCH 4
#define PLGDM_FRAME_MASK 3


/**************************************************************************************************
 ~~~  Basic typedefs
 **************************************************************************************************/
typedef struct {
    uint8_t *inL;
    uint8_t *inR;
    uint8_t *dm;
    uint8_t *left[2][PLGDM_PROC_PATCH];
    uint8_t *right[2][PLGDM_PROC_PATCH];
    uint8_t *out[2];
}ShaveTaskDesc;

typedef struct PlgDmStruct
{
    PlgType plg;
    volatile int32_t        crtStatus; // internal usage
    FrameProducedCB         cbList[2];
    //Private members. All data structures have to be internal
    FramePool               *outputPools;

    ShaveTaskDesc shaveTaskDesc[12];
    //Main opipe objects

    icSize   size;

    FrameT           *frameInExpectation[2];
    FrameT           *frameInProcessing[2];
    FrameT           *oFrame;
    uint32_t         runEnMask;
    uint32_t         frameInProcMask;
    //Specific component interface
    void    (*procesStart)   (uint32_t seqNr);
    void    (*procesEnd  )   (uint32_t seqNr);

    uint32_t                shvFirst;
    uint32_t                shvLast;
    uint32_t                irqLevel;
    uint32_t                shvRunNr;
    uint32_t                shvRunMask;

    dmaTransactionList_t    dmaTask[12*3];
    dmaTransactionList_t    *dmaRef[12*3];
    dmaRequesterId          dmaId;
    uint32_t                dmaRunNr;
    uint32_t                dmaRunMask;


    uint32_t                shvTotal;
    uint32_t                sliceH;
    uint32_t                nrRunsPerFrame;

}PlgDm;


/**************************************************************************************************
 ~~~  Exported Functions
 **************************************************************************************************/
void PlgDmCreate (void *pluginObject);
void PlgDmSetParams(void *pluginObject, icSize frameSz, uint32_t firstShave, uint32_t lastShave);
void PlgSadDmStopSrc(void *pluginObject);
#endif
