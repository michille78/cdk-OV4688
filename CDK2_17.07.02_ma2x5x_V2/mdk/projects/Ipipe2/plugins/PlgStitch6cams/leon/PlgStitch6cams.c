/**************************************************************************************************

 @File         : PlgStitch6cams.c
 @Author       : AG
 @Brief        : Produce a 1080p frame by stitching 6 input frames
 Date          : 22 - Jan - 2016
 E-mail        : xxx.xxx@movidius.com
 Copyright     : © Movidius Srl 2014, © Movidius Ltd 2015

 Description : Plugin creates a frame list containing maximum 6 frames
              and triggers the "stitch6camAlgo" algorithm
              In case one of the inputs has a bigger resolution it means
              that only the respective frame will be used, so calling the
              "stitch6camAlgo" algorithm is not necessary anymore.

    Resources used:
         Leon RT.
         CmxDma controlled by LeonRt.

    Interrupt base.

 **************************************************************************************************/


/**************************************************************************************************
 ~~~ Included types first then APIs from other modules
 ***************************************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <limits.h>
#include <stdint.h>
#include <DrvCmxDma.h>
#include "TimeSyncMgr.h"
#include "FrameMgrApi.h"
#include "DrvSvu.h"
#include "PlgStitch6camsApi.h"
#include "VcsHooksApi.h"


/**************************************************************************************************
 ~~~  Specific #defines
 **************************************************************************************************/
enum {
    PLGSTITCH6_STATS_OFF = 0,
    PLGSTITCH6_STATS_ON  = 1
};

#define OUT_WIDTH (1920)
/**************************************************************************************************
 ~~~  Imported Variable symbol from shave variables
 **************************************************************************************************/
PlgStitch6cams*             currentObject;
uint8_t                     bFrY[OUT_WIDTH];
uint8_t                     bFrUV[OUT_WIDTH];
uint32_t                    camRes[MAX_NR_OF_FRAME_STICTCHED] = {540, 540, 540, 540, 540, 540};
uint32_t                    memReseting = 0;
static dmaTransactionList_t task;
static uint32_t             curTask;
FrameT*                     framesN;
/**************************************************************************************************
 ~~~ Local File function declarations
 **************************************************************************************************/
static        int32_t   init(FramePool *outputPools, int nOutputPools, void *pluginObject);
static        int32_t   stop(void *pluginObject);
static        void      clearOutFrame(void);
static        void      useNewFrame(FrameT *frame, void *pluginObject, int inFrameId);
static        void      produceFrame(void *pluginObject);
static        void      producedStitchingFrame0(FrameT *frame, void *pluginObject);
static        void      producedStitchingFrame1(FrameT *frame, void *pluginObject);
static        void      producedStitchingFrame2(FrameT *frame, void *pluginObject);
static        void      producedStitchingFrame3(FrameT *frame, void *pluginObject);
static        void      producedStitchingFrame4(FrameT *frame, void *pluginObject);
static        void      producedStitchingFrame5(FrameT *frame, void *pluginObject);
static        void      producedStitchingFrameX(FrameT *frame, void *pluginObject, int inFrameId);

/**************************************************************************************************
 ~~~ Local Functions Implementation
 **************************************************************************************************/

//
static int32_t init(FramePool *outputPools, int nOutputPools, void *pluginObject) {
    PlgStitch6cams *plug         = (PlgStitch6cams*)pluginObject;
    plug->outputPools   = outputPools;
    plug->crtStatus     = PLGSTITCH6_STATS_ON;
    return 0;
}

//
static int32_t  stop(void *pluginObject) {
    PlgStitch6cams *plug = (PlgStitch6cams*)pluginObject;
    plug->crtStatus      = PLGSTITCH6_STATS_OFF;
    return 0;
}

//
static void clearOutFrame(void) {
    PlgStitch6cams* plug = currentObject;

    if(curTask > (1080 + 540)) {
        curTask = 0;
        task.src              = &bFrY;
        task.dst              = framesN->fbPtr[0];
        task.no_planes        = 1 - 1;   //1 plane
        task.src_width        = OUT_WIDTH; //line width
        task.src_stride       = OUT_WIDTH;
        task.dst_width        = OUT_WIDTH;
        task.dst_stride       = OUT_WIDTH;
        task.length           = OUT_WIDTH; //total transfer size
        task.src_plane_stride = OUT_WIDTH;
        task.dst_plane_stride = OUT_WIDTH;
        DrvCmxDmaTransactionBriefInit(plug->dmaId, DMA_2D_TRANSACTION, &task);
        DrvCmxDmaStartTaskAsync((dmaTransactionList_t*)&task, clearOutFrame, NULL);
    }
    else if(curTask == (1080 + 540)) {
        plug->oFrame = FrameMgrAcquireFrame(plug->outputPools);
        eofFrameHandler();
        if(framesN->next != NULL) {
            framesN = framesN->next;
            curTask = 2000;
            clearOutFrame();
        }
        else {
            curTask = 0;
            memReseting=0;
        }
    }
    else {
        if(curTask == 1080) {
            task.src              = &bFrUV;
            task.no_planes        = 2 - 1;   //2 planes
            task.dst              =   framesN->fbPtr[1];
        }
        else
            task.dst              =   task.dst + OUT_WIDTH;
        DrvCmxDmaStartTaskAsync((dmaTransactionList_t*)&task, clearOutFrame, NULL);
        curTask++;
    }

}

//
static void useNewFrame(FrameT *frame, void *pluginObject, int inFrameId) {
    PlgStitch6cams* objLoc        = (PlgStitch6cams*)pluginObject;
    FrameT* frameLoc              = (FrameT *)frame;
    uint32_t i;

    if (objLoc->nextInputFrameList[inFrameId]) {
        FrameMgrReleaseFrame(objLoc->nextInputFrameList[inFrameId]); //Release the old frame, use the new one
    }
    else {
        objLoc->noOfAvailableInputFrames++;
    }

    objLoc->nextInputFrameList[inFrameId] = frame;

    if (objLoc->noOfAvailableInputFrames == objLoc->noInputs) {
        for(i=0; i<objLoc->noInputs; i++) {
            objLoc->inputFrameList[i] = objLoc->nextInputFrameList[i];
            objLoc->nextInputFrameList[i] = 0;
        }

        objLoc->noOfAvailableInputFrames = 0;
        produceFrame(objLoc);
    }
}

//
static void producedStitchingFrameX(FrameT *frame, void *pluginObject, int inFrameId) {
    PlgStitch6cams* objLoc        = (PlgStitch6cams*)pluginObject;
    FrameT* frameLoc              = (FrameT *)frame;
    uint32_t i = 0;

    if (frame) {
        if((frameLoc->stride[0] <= 960)) {
            if(camRes[inFrameId] != frameLoc->height[0]) {
                camRes[inFrameId] = frameLoc->height[0];
                if(frameLoc->height[0] < 540) {
                    memReseting=1;
                    uint32_t idx = 0;
                    for(i=0;i<MAX_NR_OF_FRAME_STICTCHED;i++) {
                        if(camRes[i] == camRes[inFrameId])
                            idx++;
                    }
                    FrameMgrReleaseFrame(frame);
                    if(idx == MAX_NR_OF_FRAME_STICTCHED) {
                        framesN = objLoc->outputPools->frames;
                        curTask = 2000;
                        for(i=0;i<MAX_NR_OF_FRAME_STICTCHED;i++) {
                            if (objLoc->nextInputFrameList[i]) {
                                FrameMgrReleaseFrame(objLoc->nextInputFrameList[i]); //Release the old frame, use the new one
                                objLoc->nextInputFrameList[i] = 0;
                                objLoc->noOfAvailableInputFrames--;
                            }
                        }
                        clearOutFrame();
                    }
                }
                else
                    useNewFrame(frameLoc, objLoc, inFrameId);
            }
            else if(memReseting==1)
                FrameMgrReleaseFrame(frame);
            else
                useNewFrame(frameLoc, objLoc, inFrameId);
        }
        else
            FrameMgrReleaseFrame(frame);
    }
}
static void producedStitchingFrame0(FrameT *frame, void *pluginObject) {
    producedStitchingFrameX(frame, pluginObject, 0);

}

//
static void producedStitchingFrame1(FrameT *frame, void *pluginObject) {
    producedStitchingFrameX(frame, pluginObject, 1);
}

//
static void producedStitchingFrame2(FrameT *frame, void *pluginObject) {
    producedStitchingFrameX(frame, pluginObject, 2);
}

//
static void producedStitchingFrame3(FrameT *frame, void *pluginObject) {
    producedStitchingFrameX(frame, pluginObject, 3);
}

//
static void producedStitchingFrame4(FrameT *frame, void *pluginObject) {
    producedStitchingFrameX(frame, pluginObject, 4);
}

//
static void producedStitchingFrame5(FrameT *frame, void *pluginObject) {
    producedStitchingFrameX(frame, pluginObject, 5);
}

//
static void produceFrame(void *pluginObject) {
    PlgStitch6cams *plug = (PlgStitch6cams*)pluginObject;
    assert(PLG_STATS_IDLE == plug->plg.status);

    if(PLGSTITCH6_STATS_ON == plug->crtStatus) {
        plug->plg.status = PLG_STATS_RUNNING;
        plug->oFrame = FrameMgrAcquireFrame(plug->outputPools);
        // skip frame if no more output buffer available
        if (NULL == plug->oFrame) { // release input frame and exit, frame will be dropped
            uint32_t inFrameIdx;
            for(inFrameIdx=0; inFrameIdx<(plug->noInputs); inFrameIdx++) {
                if(plug->inputFrameList[inFrameIdx])
                    FrameMgrReleaseFrame(plug->inputFrameList[inFrameIdx]);
            }
            assert(plug->oFrame != NULL); // TODO: delete debug code
            plug->plg.status = PLG_STATS_IDLE;
            return;
        }
        StitchStart(plug->inputFrameList, plug->oFrame, MAX_NR_OF_FRAME_STICTCHED, plug);
    }
}

/**************************************************************************************************
 ~~~ Exported Functions Implementation
 **************************************************************************************************/
//
void eofFrameHandler(void) {
    uint32_t inFrameIdx;
    for(inFrameIdx=0; inFrameIdx<(currentObject->noInputs); inFrameIdx++) {
        if((currentObject->inputFrameList[inFrameIdx] != NULL) && memReseting==0)
            FrameMgrReleaseFrame(currentObject->inputFrameList[inFrameIdx]);
    }

    currentObject->oFrame->stride[0] = currentObject->size.w;
    currentObject->oFrame->height[0] = currentObject->size.h;
    FrameMgrProduceFrame(currentObject->oFrame);

    currentObject->plg.status = PLG_STATS_IDLE;
}

//
void PlgStitch6camsCreate(void *pluginObject) {
    PlgStitch6cams *plug        = (PlgStitch6cams*)pluginObject;
    plug->plg.init              = init;
    plug->plg.fini              = stop;
    plug->cbList[0].callback   = producedStitchingFrame0;
    plug->cbList[0].pluginObj  = pluginObject;
    plug->cbList[1].callback   = producedStitchingFrame1;
    plug->cbList[1].pluginObj  = pluginObject;
    plug->cbList[2].callback   = producedStitchingFrame2;
    plug->cbList[2].pluginObj  = pluginObject;
    plug->cbList[3].callback   = producedStitchingFrame3;
    plug->cbList[3].pluginObj  = pluginObject;
    plug->cbList[4].callback   = producedStitchingFrame4;
    plug->cbList[4].pluginObj  = pluginObject;
    plug->cbList[5].callback   = producedStitchingFrame5;
    plug->cbList[5].pluginObj  = pluginObject;
    plug->plg.callbacks         = plug->cbList;
    plug->noOfAvailableInputFrames = 0;
    plug->plg.status            = PLG_STATS_IDLE;
    plug->crtStatus             = PLGSTITCH6_STATS_OFF;
    DrvCmxDmaInitDefault();
}

//
void PlgStitch6camsSetParams(void *pluginObject, uint32_t noInputs, icSize frameSize) {
    PlgStitch6cams *plug = (PlgStitch6cams*)pluginObject;
    uint32_t i;
    plug->size                       = frameSize;
    plug->noInputs                   = noInputs;
    for(i=0;i<noInputs;i++)
        plug->nextInputFrameList[i] = 0;
    for(i=0;i<OUT_WIDTH;i++) {
        bFrY[i]  = 0x00;
        bFrUV[i] = 0x80;
    }
    currentObject                    = plug;
    plug->dmaId                      = DrvCmxDmaInitRequester(3);
}

