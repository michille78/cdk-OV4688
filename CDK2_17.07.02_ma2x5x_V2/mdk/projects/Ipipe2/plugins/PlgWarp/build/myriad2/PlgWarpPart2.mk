ifeq ($(LEON_RT_BUILD),yes)
	PlgWarp_PREFIX=lrt_
else
	PlgWarp_PREFIX=
endif
# APP mvlib
#MVASMOPT := $(filter-out -noSPrefixing,$(MVASMOPT))

ENTRYPOINTS += -e imageWarp -u generateMesh --gc-sections

$(APPNAME_WARP).mvlib : $(SHAVE_PlgWarpShv_OBJS) $(PROJECT_SHAVE_LIBS)
	$(ECHO) $(LD)  $(MVLIBOPT) $(ENTRYPOINTS) $(SHAVE_PlgWarpShv_OBJS) $(PROJECT_SHAVE_LIBS) $(CompilerANSILibs) -o $@
