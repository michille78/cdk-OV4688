#include <svuCommonShave.h>
#include <math.h>
#include <stdio.h>

typedef struct
{
    unsigned int meshWidth;
    unsigned int meshHeight;
    float* meshX;
    float* meshY;
    unsigned int coord_min_x;
    unsigned int coord_max_x;
    unsigned int coord_min_y;
    unsigned int coord_max_y;
} meshStruct;

typedef struct meshGenCtrlStruct {
    meshStruct* meshL;
    meshStruct* meshC;

    uint32_t minL_x;
    uint32_t maxL_x;
    uint32_t minL_y;
    uint32_t maxL_y;

    uint32_t minC_x;
    uint32_t maxC_x;
    uint32_t minC_y;
    uint32_t maxC_y;

    uint32_t total_shaves;
} plgWarpMeshGenCtrl_t;

extern "C" void generateMesh(plgWarpMeshGenCtrl_t* ctx,
                             int32_t dispH, int32_t dispV,
                             uint32_t shave_idx)
{
    u32 i;

    ///
    /// LUMA
    ///
    float dX = (float) dispH;
    float dY = (float) dispV;

    uint32_t meshSize = (ctx->meshL->meshWidth * ctx->meshL->meshHeight);
    uint32_t meshSizeOffset = (meshSize / ctx->total_shaves);

    float* meshX = (ctx->meshL->meshX) + shave_idx * meshSizeOffset;
    float* meshY = (ctx->meshL->meshY) + shave_idx * meshSizeOffset;

    float minX = *(meshX);
    float minY = *(meshY);
    float maxX = *(meshX);
    float maxY = *(meshY);

    for (i =0; i < meshSizeOffset; i++, meshX++, meshY++)
    {
        *meshX += dX;
        *meshY += dY;

        if(*meshX < minX)
            minX = *meshX;
        else if(*meshY < minY)
            minY = *meshY;
        else if(*meshX > maxX)
            maxX = *meshX;
        else if(*meshY > maxY)
            maxY = *meshY;
    }

    ctx->minL_x = (uint32_t)minX;
    ctx->maxL_x = (uint32_t)maxX;
    ctx->minL_y = (uint32_t)minY;
    ctx->maxL_y = (uint32_t)maxY;


    ///
    /// CHROMA
    ///

    uint32_t meshCSize    = (ctx->meshC->meshWidth * ctx->meshC->meshHeight);
    uint32_t meshCOffset  = shave_idx * (meshCSize / ctx->total_shaves);

    float* meshCX = ctx->meshC->meshX + shave_idx * meshCOffset;
    float* meshCY = ctx->meshC->meshY + shave_idx * meshCOffset;

    float dcX = (float) dispH  / 2.;
    float dcY = (float) dispV  / 2.;

    minX = *(meshCX);
    minY = *(meshCY);
    maxX = *(meshCX);
    maxY = *(meshCY);

    for (i =0; i < meshCOffset; i++, meshCX++, meshCY++)
    {
        *meshCX += dcX;
        *meshCY += dcY;

        if(*meshX < minX)
            minX = *meshX;
        else if(*meshY < minY)
            minY = *meshY;
        else if(*meshX > maxX)
            maxX = *meshX;
        else if(*meshY > maxY)
            maxY = *meshY;
    }

    ctx->minC_x = (uint32_t)minX;
    ctx->maxC_x = (uint32_t)maxX;
    ctx->minC_y = (uint32_t)minY;
    ctx->maxC_y = (uint32_t)maxY;

    SHAVE_HALT;
}
