/**************************************************************************************************

 @File         : PlgWarpApi.h
 @Author       : AG
 @Brief        : Contain Warp plug-in interface
 Date          : 03 - Dec - 2015
 E-mail        : xxx.xxx@movidius.com
 Copyright     : © Movidius Srl 2014, © Movidius Ltd 2015

 Description :

     Resources used:
         Leon RT.
         Bicubic filter

    Interrupt base.

 **************************************************************************************************/
#ifndef __PLG_WARP_API__
#define __PLG_WARP_API__

/**************************************************************************************************
 ~~~ Included types first then APIs from other modules
 **************************************************************************************************/
#include "PlgTypes.h"
#include "mv_types.h"
#include "swcShaveLoader.h"
#include <imageWarpDefines.h>     /// warp component API

/**************************************************************************************************
 ~~~  Basic typedefs
 **************************************************************************************************/

 // After a stop, at the new start not allocate again the buffers, just change the size
typedef enum {
    PLG_WARP_NOTMADE = 0,
    PLG_WARP_CREATED = 1,
    PLG_WARP_INUSE   = 2
}PlgWarpStatus;

#define NO_FRM_EXP   3

#define MAX_NR_MESHES (2)

//meshStruct* chr_mesh;
typedef struct PlgWarpStruct {
    PlgType plg;

    //Bicubic params
    icSize        frmSz; //input frame size
    icSize        frmSzOut; // output frame size

    //Specific component interface; called when Bicubic processing is done
    void    (*procesEnd  )   (void *pluginObject);

    //use in IRQ the frame processed by bicubic filter
    FrameT           *oFrame;
    FrameT           *iFrame;
    FrameT           *frameInExpectation[NO_FRM_EXP];
    int32_t          nextFreeIdx;
    int32_t          nextRunIdx;


    //Private members. All data structures have to be internal
    FramePool               *outputPools;
    volatile int32_t        crtStatus; // internal usage
    FrameProducedCB         cbList[1];
    PlgWarpStatus        status; // used for avid double plug-in initialization start/stop.

    meshStruct* meshY0;
    meshStruct* meshY1;
    meshStruct* meshUV;
    unsigned short paddingvalue;

    // Specific plug-in future
    void (*triger)(void *pluginObject);
    void (*triggerStartWarp)(void *pluginObject);
    void (*triggerStartVPlane)(void *pluginObject);
    void (*triggerLuma)(void *pluginObject);
    void (*triggerChroma)(void *pluginObject);
    void (*dropFrame)(void *pluginObject);
    volatile uint32_t   runEnFlag;
    volatile uint32_t   lumaEOF;
    volatile uint32_t   chrEOF;
    volatile uint32_t   meshGenEnd;
    volatile uint32_t   lumaisrcount;
    volatile uint32_t   chromaisrcount;
    uint32_t camId;

} PlgWarp;

typedef struct meshGenCtrlStruct {
    meshStruct* meshL;
    meshStruct* meshC;

    uint32_t minL_x;
    uint32_t maxL_x;
    uint32_t minL_y;
    uint32_t maxL_y;

    uint32_t minC_x;
    uint32_t maxC_x;
    uint32_t minC_y;
    uint32_t maxC_y;

    uint32_t total_shaves;
} plgWarpMeshGenCtrl_t;

/**************************************************************************************************
 ~~~  Exported Functions
 **************************************************************************************************/
void PlgWarpCreate(void *pluginObject);
void PlgWarpConfig(void *pluginObject, icSize inframeSz, icSize outframeSz,
                    meshStruct* meshY0, meshStruct* meshY1, meshStruct* meshUV,
                    unsigned short paddingvalue, uint32_t camId);
void PlgWarpReConfig(void *pluginObject, icSize inframeSz, icSize outframeSz,
                    meshStruct* meshY0, meshStruct* meshY1, meshStruct* meshUV,
                    unsigned short paddingvalue);
#endif
