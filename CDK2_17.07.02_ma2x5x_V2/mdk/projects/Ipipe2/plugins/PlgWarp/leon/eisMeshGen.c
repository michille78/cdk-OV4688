/// =====================================================================================
///
///        @file:      eisMeshGen.c
///        @brief:     simple imageWArp mesh generation example code. 
///                    Integrates into plgWarp
///        @author:    csoka, attila.csok@movidius.com
///        @copyright: All code copyright Movidius Ltd 2013, all rights reserved.
///                  For License Warranty see: common/license.txt
/// =====================================================================================
///

/// System Includes
/// -------------------------------------------------------------------------------------

/// Application Includes
/// -------------------------------------------------------------------------------------
#include "eisMeshGen.h"

/// Source Specific #defines and types (typedef,enum,struct)
/// -------------------------------------------------------------------------------------

/// Global Data (Only if absolutely necessary)
/// -------------------------------------------------------------------------------------

/// Static Local Data
/// -------------------------------------------------------------------------------------

/// Static Function Prototypes
/// -------------------------------------------------------------------------------------

/// Functions Implementation
/// -------------------------------------------------------------------------------------
void eisMeshGenConfigureExtremas(
            plgWarpMeshGenCtrl_t* mg0, plgWarpMeshGenCtrl_t* mg1,
            meshStruct* mY0, meshStruct* mY1)
{
    mY0->coord_min_y = mg0->minL_y;
    mY1->coord_min_y = mg0->minL_y;

    mY0->coord_max_y = mg1->maxL_y;
    mY1->coord_max_y = mg1->maxL_y;

    mY0->coord_min_x = mg0->minL_x;
    mY1->coord_min_x = mg0->minL_x + ((mg0->maxL_x - mg0->minL_x) >> 1);

    mY0->coord_max_x = mY1->coord_min_x;
    mY1->coord_max_x = mg1->maxL_x;
}
