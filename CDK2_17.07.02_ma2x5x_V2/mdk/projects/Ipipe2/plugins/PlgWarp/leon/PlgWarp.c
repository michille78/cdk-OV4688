/**************************************************************************************************

 @File         : PlgWarp.c
 @Author       : AG
 @Brief        : plugin wrapper for imageWarp mdk component
 Date          : 03 - Dec - 2015
 E-mail        : xxx.xxx@movidius.com
 Copyright     : © Movidius Srl 2014, © Movidius Ltd 2015

    Interrupt base.
 **************************************************************************************************/

/**************************************************************************************************
 ~~~ Included types first then APIs from other modules
 **************************************************************************************************/
#include <string.h>
#include <stdio.h>
#include <assert.h>
#include <math.h>
#include "DrvTimer.h"
#include "swcLeonUtils.h"
#include "TimeSyncMgr.h"
#include "FrameMgrApi.h"
#include "ipipeDbg.h"
#include "ipipeUtils.h"
#include "DrvShaveL2Cache.h"    /// Shave L2 Cache driver
#include <DrvIcbDefines.h>
#include <DrvSvuDefines.h>
#include "PlgWarpApi.h"
#include <swcFrameTypes.h>
#include "DrvRegUtilsDefines.h"
#include "DrvSvu.h"
#include "VcsHooksApi.h"

#include "eisMeshGen.h"

/**************************************************************************************************
 ~~~  Specific #defines
 **************************************************************************************************/
//Plugin status
#define ALIGNED(x) __attribute__((aligned(x)))
#define SECTION(x) __attribute__((section(x)))

#define PLGWARP_SHAVE_L2_PARTITION  (0)

#define NR_SHAVES_Y   (2)
#define NR_SHAVES_UV  (1)

#define NR_MESH_GEN_SHAVES (NR_SHAVES_Y)

#define MAX_TILES_TOTAL_NR (69*121)

//#define PLGWARP_STATIC_MESH
#ifdef PLGWARP_STATIC_MESH
#define DISABLE_DYNAMIC_MESH_GEN
#endif

enum {
    PLG_OFF = 0,
    PLG_ON  = 1
};

#ifndef PLGWARP_SHV_L1
#define PLGWARP_SHV_L1 4
#endif
#ifndef PLGWARP_SHV_L2
#define PLGWARP_SHV_L2 5
#endif
#ifndef PLGWARP_SHV_UV1
#define PLGWARP_SHV_UV1 6
#endif

#define PLGWARP_CONCAT(A,X,B) (A ## X ## B)
#define SHV_WARP_NAME(X) PLGWARP_CONCAT(warpYuv,X,_imageWarp)
#define SHV_GENMESH_NAME(X) PLGWARP_CONCAT(warpYuv,X,_generateMesh)

#define IMAGE_WARP_FUNC_L1 (SHV_WARP_NAME(PLGWARP_SHV_L1))
#define IMAGE_WARP_FUNC_L2 (SHV_WARP_NAME(PLGWARP_SHV_L2))
#define IMAGE_WARP_FUNC_C1 (SHV_WARP_NAME(PLGWARP_SHV_UV1))

#define IMAGE_WARP_GENMESH1 (SHV_GENMESH_NAME(PLGWARP_SHV_L1))
#define IMAGE_WARP_GENMESH2 (SHV_GENMESH_NAME(PLGWARP_SHV_L2))
/**************************************************************************************************
 ~~~  Local variables
 **************************************************************************************************/
PlgWarp*      currentObject;

// Shave entrypoints for warp
extern uint32_t IMAGE_WARP_FUNC_L1;
extern uint32_t IMAGE_WARP_FUNC_L2;
extern uint32_t IMAGE_WARP_FUNC_C1;

static uint32_t plgWarpShavesY[NR_SHAVES_Y] =
{
        PLGWARP_SHV_L1, PLGWARP_SHV_L2,
};

static uint32_t plgWarpShavesUV[NR_SHAVES_UV] =
{
        PLGWARP_SHV_UV1,
};

static uint32_t plgWarpSvuEntryPtsY[NR_SHAVES_Y] = {
        (u32)&IMAGE_WARP_FUNC_L1,
        (u32)&IMAGE_WARP_FUNC_L2
};
static uint32_t plgWarpSvuEntryPtsUV[NR_SHAVES_UV] = {
        (u32)&IMAGE_WARP_FUNC_C1,
};

#ifndef DISABLE_DYNAMIC_MESH_GEN
// Shave entrypoints for meshGen
extern uint32_t IMAGE_WARP_GENMESH1;
extern uint32_t IMAGE_WARP_GENMESH2;

static uint32_t plgWarpSvuEntryPtsYMeshGen[NR_MESH_GEN_SHAVES] =
{
        (u32)&IMAGE_WARP_GENMESH1,
        (u32)&IMAGE_WARP_GENMESH2,
};

/// Params for simple mesh shifter algo
SECTION(".cmx_direct.data") volatile int32_t displaceMeshX = 0;
SECTION(".cmx_direct.data") volatile int32_t displaceMeshY = 0;
#endif


ALIGNED(16) SECTION(".ddr.data")
tileList tileNodesL[NR_SHAVES_Y][MAX_TILES_TOTAL_NR];

ALIGNED(16) SECTION(".ddr.data")
tileList tileNodesC[NR_SHAVES_UV][MAX_TILES_TOTAL_NR >> 1];

ALIGNED(16) SECTION(".cmx_direct.data") meshStruct lumaMesh[NR_SHAVES_Y];

SECTION(".cmx_direct.data") int32_t runWarpY = 0;
SECTION(".cmx_direct.data") int32_t runWarpUV = 0;

/// Luma framebuffers and framespecs
SECTION(".cmx_direct.data") frameBuffer inputFb[NR_SHAVES_Y], outputFb[NR_SHAVES_Y];
SECTION(".cmx_direct.data") frameSpec frSpec, frSpecOut;

/// Chroma framebuffers and framespecs
SECTION(".cmx_direct.data") frameBuffer inputFbU, outputFbU;
SECTION(".cmx_direct.data") frameBuffer inputFbV, outputFbV;
SECTION(".cmx_direct.data") frameSpec frSpecUV, frSpecOutUV;
uint32_t chroma_out_offset;

SECTION(".cmx_direct.data") volatile uint32_t warpFcnt = 0;
SECTION(".cmx_direct.data") volatile uint32_t warpDropCnt = 0;
SECTION(".cmx_direct.data") volatile uint32_t meshgenisrcount = 0;

SECTION(".cmx_direct.data") plgWarpMeshGenCtrl_t plgWarpMeshGenCtrl[NR_MESH_GEN_SHAVES];


#ifdef MEASURE_WARP_RUNTIME
SECTION(".cmx_direct.data") uint32_t warp_fcnt = 0;
SECTION(".cmx_direct.data") float    warp_fps  = 0.;
SECTION(".cmx_direct.data") float    total_fps = 0.;
SECTION(".cmx_direct.data") tyTimeStamp  warp_timer, total_timer;
SECTION(".cmx_direct.data") u64 warp_cycles, total_cycles;
#endif

/**************************************************************************************************
 ~~~ Local File function declarations
 **************************************************************************************************/
void warpEof(void *pluginObject);
static void warpLumaEof(void *pluginObject);
static void warpChromaEof(void *pluginObject);
static void applyWarp(int shaveNo, u32 Sched, meshStruct* mesh, frameBuffer *inputFb,
            frameBuffer *outputFb, tileList* tileNodes,
            unsigned short paddingvalue,
            uint32_t nr_shaves_vsplit,
            uint32_t proc_shave_idx,
            irq_handler shvIrqCb);

/**************************************************************************************************
 ~~~ Local Functions Implementation
 **************************************************************************************************/
//

static void plgWarpISRLuma(uint32_t source)
{
    UNUSED(source);

    currentObject->lumaisrcount++;
    if(currentObject->lumaisrcount == NR_SHAVES_Y)
    {
        currentObject->lumaEOF = 1;
        currentObject->lumaisrcount = 0;
    }
}


static void plgWarpISRChromas(uint32_t source)
{
    UNUSED(source);

    currentObject->chromaisrcount++;
    if(currentObject->chromaisrcount == (NR_SHAVES_UV + 2)) /// +1 fpr second plane, +1 for sched.
    {
        currentObject->chrEOF = 1;
        currentObject->chromaisrcount = 0;
    }
}

static void plgWarpStartVPlane(void *pluginObject)
{
    PlgWarp* obj = (PlgWarp*)pluginObject;

    currentObject->chromaisrcount++;

    applyWarp(plgWarpShavesUV[0], plgWarpSvuEntryPtsUV[0],
            obj->meshUV, &inputFbV, &outputFbV, tileNodesC[1],
            obj->paddingvalue,
            NR_SHAVES_UV,
            0,
            plgWarpISRChromas);
}

#ifndef DISABLE_DYNAMIC_MESH_GEN
static void plgWarpMeshGenISR(uint32_t source)
{
    UNUSED(source);

    meshgenisrcount++;
    if(meshgenisrcount == NR_MESH_GEN_SHAVES)
    {
        currentObject->meshGenEnd = 1;
        meshgenisrcount = 0;
    }
}
#endif

static void warpLumaEof(void *pluginObject)
{
    UNUSED(pluginObject);
    runWarpY++;
    if(runWarpUV == 1)
    {
        warpEof(currentObject);
    }
}

//
static void warpChromaEof(void *pluginObject)
{
    UNUSED(pluginObject);
    runWarpUV++;

    if(runWarpY == 1)
    {
        warpEof(currentObject);
    }
}

//
static void applyWarp(int shaveNo, u32 Sched, meshStruct* mesh, frameBuffer *inputFb,
            frameBuffer *outputFb, tileList* tileNodes,
            unsigned short paddingvalue,
            uint32_t nr_shaves_vsplit,
            uint32_t proc_shave_idx,
            irq_handler shvIrqCb)
{
    swcResetShave(shaveNo);
    swcSetAbsoluteDefaultStack(shaveNo);
    swcStartShaveAsyncCC(shaveNo, Sched, shvIrqCb,"iiiiiii", mesh,
            inputFb,
            outputFb,
            tileNodes,
            nr_shaves_vsplit,
            proc_shave_idx,
            paddingvalue);
}
#ifndef DISABLE_DYNAMIC_MESH_GEN
static inline void plgWarpStartMeshGenSVU(int shaveNo, uint32_t entryPt, irq_handler shvIsr,
                plgWarpMeshGenCtrl_t* meshGenControl,
                uint32_t shave_idx)
{
    swcResetShave(shaveNo);
    swcSetAbsoluteDefaultStack(shaveNo);
    swcStartShaveAsyncCC(shaveNo, entryPt, shvIsr, "iiii",
                            meshGenControl,
                            displaceMeshX, displaceMeshY,
                            shave_idx);
}
#endif

static inline void plgWarpConfigureMeshExtremas()
{
    plgWarpMeshGenCtrl_t* mg0 = &plgWarpMeshGenCtrl[0];
    plgWarpMeshGenCtrl_t* mg1 = &plgWarpMeshGenCtrl[1];
    meshStruct* mY0 = &lumaMesh[0];
    meshStruct* mY1 = &lumaMesh[1];

    /// only works for this case
    mY0->coord_min_y = mg0->minL_y;
    mY1->coord_min_y = mg0->minL_y;

    mY0->coord_max_y = mg1->maxL_y;
    mY1->coord_max_y = mg1->maxL_y;

    mY0->coord_min_x = mg0->minL_x;
    mY1->coord_min_x = mg0->minL_x + ((mg0->maxL_x - mg0->minL_x) >> 1);

    mY0->coord_max_x = mY1->coord_min_x;
    mY1->coord_max_x = mg1->maxL_x;
}

static void plgWarpStartWarping (void *pluginObject)
{
    PlgWarp* obj = (PlgWarp*)pluginObject;
    uint32_t i;

#ifdef MEASURE_WARP_RUNTIME
    DrvTimerStartTicksCount(&warp_timer);
#endif
    /// FLush Shave L2 partition
    DrvShaveL2CachePartitionFlush(PLGWARP_SHAVE_L2_PARTITION);

#ifndef DISABLE_DYNAMIC_MESH_GEN
    /// Configure mesh extremas based on mesgen results
    eisMeshGenConfigureExtremas(
             &plgWarpMeshGenCtrl[0],  &plgWarpMeshGenCtrl[1],
             &lumaMesh[0],  &lumaMesh[1]);
#endif

    /// Start luma shaves
    for(i=0; i < NR_SHAVES_Y; i++)
    {
        /// Adjust buffer pointers according to mesh calculations
        inputFb[i].p1 += lumaMesh[i].coord_min_x * inputFb[i].spec.bytesPP;

        /// Start shaves
        applyWarp(plgWarpShavesY[i], plgWarpSvuEntryPtsY[i],
                &lumaMesh[i], &inputFb[i], &outputFb[i],
                tileNodesL[i], obj->paddingvalue,
                NR_SHAVES_Y, i, plgWarpISRLuma);
    }

    /// Start U chroma shave
    applyWarp(plgWarpShavesUV[0], plgWarpSvuEntryPtsUV[0],
            obj->meshUV, &inputFbU, &outputFbU, tileNodesC[0],
            obj->paddingvalue,
            NR_SHAVES_UV,
            0,
            plgWarpISRChromas);
}


static void tryRun(void *pluginObject) {

    PlgWarp* obj = (PlgWarp*)pluginObject;
    uint32_t i;

    if(obj->frameInExpectation[obj->nextRunIdx] != 0)
    {
        obj->plg.status = PLG_STATS_RUNNING;

        //Get a frame from output mempool
        FrameT *oFrame = FrameMgrAcquireFrame(obj->outputPools);
        obj->oFrame = oFrame;
        //Skip input frame if no more output buffer available
        if (NULL == oFrame) {
            FrameMgrReleaseFrame(obj->frameInExpectation[obj->nextRunIdx]);
            obj->frameInExpectation[obj->nextRunIdx] = 0;
            obj->nextRunIdx++;
            if(obj->nextRunIdx >= NO_FRM_EXP)
                obj->nextRunIdx = 0;
            runWarpY = 0;
            runWarpUV = 0;
            obj->plg.status = PLG_STATS_IDLE;
            //assert(0);
            return;
        }
        //Else, we can process

        /// Save i/o frames for slow scheduler
        FrameT* currentIFrame = obj->frameInExpectation[obj->nextRunIdx];
        FrameT* currentOFrame = oFrame;
        // update output buffer size, and isp depend on informations
        currentOFrame->appSpecificData = currentIFrame->appSpecificData;
        currentOFrame->stride[0] = obj->frmSzOut.w;
        currentOFrame->stride[1] = obj->frmSzOut.w>>1;
        currentOFrame->height[0] = obj->frmSzOut.h;
        currentOFrame->height[1] = obj->frmSzOut.h;

        currentOFrame->tSize[0] = currentOFrame->stride[0] * currentOFrame->height[0];
        currentOFrame->tSize[1] = currentOFrame->stride[1] * currentOFrame->height[1];
        currentOFrame->tSize[3] = 0; //currentOFrame->stride[0] * FRAME_T_HEADER_BUFFER_HEIGHT;

//        currentOFrame->fbPtr[0] = currentOFrame->fbPtr[3] + currentOFrame->tSize[3];
        currentOFrame->fbPtr[1] = currentOFrame->fbPtr[0] + currentOFrame->tSize[0];

        /// Set input Framebuffers for Luma
        for(i = 0; i < NR_SHAVES_Y; i++)
        {
            inputFb[i].p1 = currentIFrame->fbPtr[0];
            inputFb[i].spec.height  = currentIFrame->height[0];
            inputFb[i].spec.width   = currentIFrame->stride[0];
          inputFb[i].spec.stride  = inputFb[i].spec.width * inputFb[i].spec.bytesPP;
        }

        /// Set output Framebuffers
        outputFb[0].p1 = currentOFrame->fbPtr[0];
        outputFb[1].p1 = currentOFrame->fbPtr[0];

        /// Mesh
        lumaMesh[0].meshWidth  = obj->meshY0->meshWidth;
        lumaMesh[0].meshHeight = obj->meshY0->meshHeight;
        lumaMesh[0].meshX = obj->meshY0->meshX;
        lumaMesh[0].meshY = obj->meshY0->meshY;
        lumaMesh[0].coord_min_x = obj->meshY0->coord_min_x;
        lumaMesh[0].coord_max_x = obj->meshY0->coord_max_x;
        lumaMesh[0].coord_min_y = obj->meshY0->coord_min_y;
        lumaMesh[0].coord_max_y = obj->meshY0->coord_max_y;

        lumaMesh[1].meshWidth  = obj->meshY1->meshWidth;
        lumaMesh[1].meshHeight = obj->meshY1->meshHeight;
        lumaMesh[1].meshX = obj->meshY1->meshX;
        lumaMesh[1].meshY = obj->meshY1->meshY;
        lumaMesh[1].coord_min_x = obj->meshY1->coord_min_x;
        lumaMesh[1].coord_max_x = obj->meshY1->coord_max_x;
        lumaMesh[1].coord_min_y = obj->meshY1->coord_min_y;
        lumaMesh[1].coord_max_y = obj->meshY1->coord_max_y;

        /// Framebuffers for Chromas
        inputFbU.spec.height = currentIFrame->height[0] >> 1;
        inputFbU.spec.width  = currentIFrame->stride[0] >> 1;
        inputFbU.spec.stride = inputFbU.spec.width * inputFbU.spec.bytesPP;
        inputFbU.p1 = currentIFrame->fbPtr[1];
        outputFbU.p1 = currentOFrame->fbPtr[1];

        inputFbV.spec.height = currentIFrame->height[0]>>1;
        inputFbV.spec.width  = currentIFrame->stride[0]>>1;
        inputFbV.spec.stride = inputFbV.spec.width * inputFbV.spec.bytesPP;
        inputFbV.p1 = currentIFrame->fbPtr[1] + ((currentIFrame->stride[0] *
                      currentIFrame->height[0]) >> 2);
        outputFbV.p1 = currentOFrame->fbPtr[1] + chroma_out_offset;

#ifdef MEASURE_WARP_RUNTIME
        DrvTimerStartTicksCount(&total_timer);
#endif

#ifndef DISABLE_DYNAMIC_MESH_GEN
            /// Start mesh gen shaves
            plgWarpMeshGenCtrl[0].meshL = &lumaMesh[0];
            plgWarpMeshGenCtrl[0].meshC = obj->meshUV;
            plgWarpMeshGenCtrl[0].total_shaves = NR_MESH_GEN_SHAVES;

            plgWarpMeshGenCtrl[1].meshL = &lumaMesh[1];
            plgWarpMeshGenCtrl[1].meshC = obj->meshUV;
            plgWarpMeshGenCtrl[1].total_shaves = NR_MESH_GEN_SHAVES;

            plgWarpStartMeshGenSVU(plgWarpShavesY[0],
                                   plgWarpSvuEntryPtsYMeshGen[0],
                                   plgWarpMeshGenISR,
                                   &plgWarpMeshGenCtrl[0],
                                   0);
            plgWarpStartMeshGenSVU(plgWarpShavesY[1],
                                   plgWarpSvuEntryPtsYMeshGen[1],
                                   plgWarpMeshGenISR,
                                   &plgWarpMeshGenCtrl[1],
                                   1);
            displaceMeshX = 0;
            displaceMeshY = 0;
#endif

#ifdef DISABLE_DYNAMIC_MESH_GEN
        /// debug
        plgWarpStartWarping(obj);
#endif
    }
}

//
static void cbNewInputFrame(FrameT *iFrame, void *pluginObject) {
    PlgWarp* obj = (PlgWarp*)pluginObject;

    assert(iFrame);
    assert(iFrame->stride[0] != 4000);
    if(PLG_ON == obj->crtStatus)
    {
        if(obj->frameInExpectation[obj->nextFreeIdx] != 0) {
            if(obj->nextFreeIdx != obj->nextRunIdx) {
                // release this frame (the oldest) if is not in processing
                FrameMgrReleaseFrame(obj->frameInExpectation[obj->nextFreeIdx]);
            }
            else {
                obj->nextFreeIdx++;
                if(obj->nextFreeIdx >= NO_FRM_EXP) {
                    obj->nextFreeIdx = 0;
                }
                if(obj->frameInExpectation[obj->nextFreeIdx] != 0) {
                    FrameMgrReleaseFrame(obj->frameInExpectation[obj->nextFreeIdx]);
                }
            }
        }

        obj->frameInExpectation[obj->nextFreeIdx] = iFrame;
        obj->nextFreeIdx++;
        if(obj->nextFreeIdx >= NO_FRM_EXP)
            obj->nextFreeIdx = 0;
        obj->runEnFlag++;
    }
}
//warp filter EOF callback: adjust associated Plugin Frame buffers
void warpEof(void *pluginObject){
    PlgWarp* obj = (PlgWarp*)pluginObject;
    FrameT     *oFrame = obj->oFrame;

    assert(oFrame);

    warpFcnt++;

    /// FLush Shave L2 partition
    DrvShaveL2CachePartitionFlush(PLGWARP_SHAVE_L2_PARTITION);

#ifdef MEASURE_WARP_RUNTIME
    DrvTimerGetElapsedTicks(&warp_timer, &warp_cycles);
    DrvTimerGetElapsedTicks(&total_timer, &total_cycles);
    warp_fcnt++;
    /// Print after each 30th frame
    if((warp_fcnt % 30) == 0)
    {
        warp_fps  = (float)(DEFAULT_APP_CLOCK_KHZ * 1000) / (float)warp_cycles;
        total_fps = (float)(DEFAULT_APP_CLOCK_KHZ * 1000) / (float)total_cycles;
        printf ( "plgWarp > Fps: %3.3f FpsWarp: %3.3f "
                "CC: %llu: %llu / %llu (warp / mesh)\n",
                total_fps, warp_fps,
                total_cycles, warp_cycles, (total_cycles-warp_cycles));
    }
#endif

    FrameMgrReleaseFrame(obj->frameInExpectation[obj->nextRunIdx]);
    obj->frameInExpectation[obj->nextRunIdx] = 0;
    FrameMgrProduceFrame(oFrame);
    obj->nextRunIdx++;
    if(obj->nextRunIdx >= NO_FRM_EXP)
        obj->nextRunIdx = 0;
    runWarpY = 0;
    runWarpUV = 0;

    obj->plg.status = PLG_STATS_IDLE;
}

void warpDropFrame(void *pluginObject)
{
    PlgWarp* obj = (PlgWarp*)pluginObject;

    warpDropCnt++;

    assert(obj->frameInExpectation[obj->nextRunIdx]);

    FrameMgrReleaseFrame(obj->frameInExpectation[obj->nextRunIdx]);
    obj->frameInExpectation[obj->nextRunIdx] = 0;
    obj->nextRunIdx++;
    if(obj->nextRunIdx >= NO_FRM_EXP)
        obj->nextRunIdx = 0;
    runWarpY = 0;
    runWarpUV = 0;

    obj->plg.status = PLG_STATS_IDLE;
}

//
static int32_t fini(void *pluginObject) {
    PlgWarp *object = (PlgWarp*)pluginObject;
    object->crtStatus  = PLG_OFF;
    return 0;
}

//
static int32_t init(FramePool *outputPools, int nOutputPools, void *pluginObject) {
    UNUSED(nOutputPools);
    PlgWarp* obj = (PlgWarp*)pluginObject;
    // this plugin produce just 1 output frame, so not take in consideration nOutputPools params,
    // as this have to be 1
    obj->outputPools = outputPools;
    obj->crtStatus  = PLG_ON;

    return 0;
}

/**************************************************************************************************
 ~~~ Exported Functions Implementation
 **************************************************************************************************/
//
void PlgWarpCreate(void *pluginObject) {
    PlgWarp *object          = (PlgWarp*)pluginObject;
    // init hw things, or all the init side that not need params,
    object->plg.init            = init; //to associate output pool
    object->plg.fini            = fini; //to mark STOP
    object->cbList[0].callback  = cbNewInputFrame;
    object->cbList[0].pluginObj = pluginObject;
    object->plg.callbacks       = object->cbList;
    object->plg.status          = PLG_STATS_IDLE;
    object->triger              = tryRun;
    object->crtStatus           = PLG_ON;
    object->status              = PLG_WARP_CREATED;
    object->procesEnd           = warpEof;
}

//
void PlgWarpConfig(void *pluginObject, icSize inframeSz, icSize outframeSz,
                    meshStruct* meshY0, meshStruct* meshY1, meshStruct* meshUV,
                    unsigned short paddingvalue, uint32_t camId)
{
    PlgWarp *obj          = (PlgWarp*)pluginObject;
    uint32_t i;

    obj->frmSz.w            = inframeSz.w;
    obj->frmSz.h            = inframeSz.h;

    obj->frmSzOut.w         = outframeSz.w;
    obj->frmSzOut.h         = outframeSz.h;

    obj->meshY0 = meshY0;
    obj->meshY1 = meshY1;
    obj->meshUV = meshUV;

    obj->paddingvalue = paddingvalue;

    obj->nextFreeIdx = 0;
    obj->nextRunIdx = 0;
    obj->lumaEOF = 0;
    obj->chrEOF = 0;
    obj->meshGenEnd = 0;
    obj->lumaisrcount = 0;
    obj->chromaisrcount = 0;

    obj->triger = tryRun;
    obj->triggerStartWarp = plgWarpStartWarping;
    obj->triggerStartVPlane = plgWarpStartVPlane;
    obj->triggerLuma = warpLumaEof;
    obj->triggerChroma = warpChromaEof;
    obj->dropFrame = warpDropFrame;

    frSpec.bytesPP = 1;
    frSpec.height  = obj->frmSz.h;
    frSpec.width   = obj->frmSz.w;
    frSpec.stride  = frSpec.width * frSpec.bytesPP;
    frSpec.type    = YUV400p;

    frSpecOut.bytesPP = 1;
    frSpecOut.height  = obj->frmSzOut.h;
    frSpecOut.width   = obj->frmSzOut.w;
    frSpecOut.stride  = frSpecOut.width * frSpecOut.bytesPP;
    frSpecOut.type    = YUV400p;

    for(i=0; i < NR_SHAVES_Y; i++)
    {
        inputFb[i].p1 = NULL;
        inputFb[i].p2 = NULL;
        inputFb[i].p3 = NULL;
        inputFb[i].spec = frSpec;

        outputFb[i].p1 = NULL;
        outputFb[i].p2 = NULL;
        outputFb[i].p3 = NULL;
        outputFb[i].spec = frSpecOut;
    }

    chroma_out_offset = obj->frmSzOut.w*obj->frmSzOut.h/4;

    frSpecUV.bytesPP = 1;
    frSpecUV.height = obj->frmSzOut.h/2;
    frSpecUV.width = obj->frmSzOut.w/2;
    frSpecUV.stride = frSpecUV.width * frSpecUV.bytesPP;
    frSpecUV.type = YUV400p;

    for(i=0;i<NO_FRM_EXP;i++)
    {
        obj->frameInExpectation[i] = 0;
    }

    inputFbU.p1 = NULL;
    inputFbU.p2 = NULL;
    inputFbU.p3 = NULL;
    inputFbU.spec = frSpecUV;
    outputFbU.p1 = NULL;
    outputFbU.p2 = NULL;
    outputFbU.p3 = NULL;
    outputFbU.spec = frSpecUV;

    inputFbV.p1 = NULL;
    inputFbV.p2 = NULL;
    inputFbV.p3 = NULL;
    inputFbV.spec = frSpecUV;
    outputFbV.p1 = NULL;
    outputFbV.p2 = NULL;
    outputFbV.p3 = NULL;
    outputFbV.spec = frSpecUV;
    obj->runEnFlag = 0;
    obj->camId = camId;
    currentObject = obj;

    //Set Shave L2 cache partitions
    DrvShaveL2CacheClearPartitions();
    DrvShaveL2CacheSetupPartition(SHAVEPART256KB);

    /// Set cache part for Y shaves
    for(i=0; i < NR_SHAVES_Y; i++)
    {
        DrvShaveL2CacheSetLSUPartId(plgWarpShavesY[i], PLGWARP_SHAVE_L2_PARTITION);
    }

    /// Set cache part for UV shaves
    for(i=0; i < NR_SHAVES_UV; i++)
    {
        DrvShaveL2CacheSetLSUPartId(plgWarpShavesUV[i], PLGWARP_SHAVE_L2_PARTITION);
    }

    /// Invalidate the cache
    DrvShaveL2CachePartitionInvalidate(PLGWARP_SHAVE_L2_PARTITION);

}


//
void PlgWarpReConfig(void *pluginObject, icSize inframeSz, icSize outframeSz,
                    meshStruct* meshY0, meshStruct* meshY1, meshStruct* meshUV,
                    unsigned short paddingvalue)
{
    PlgWarp *obj          = (PlgWarp*)pluginObject;
    uint32_t i;

    obj->frmSz.w            = inframeSz.w;
    obj->frmSz.h            = inframeSz.h;

    obj->frmSzOut.w         = outframeSz.w;
    obj->frmSzOut.h         = outframeSz.h;

    obj->meshY0 = meshY0;
    obj->meshY1 = meshY1;
    obj->meshUV = meshUV;

    obj->paddingvalue = paddingvalue;

    frSpec.bytesPP = 1;
    frSpec.height  = obj->frmSz.h;
    frSpec.width   = obj->frmSz.w;
    frSpec.stride  = frSpec.width * frSpec.bytesPP;
    frSpec.type    = YUV400p;

    frSpecOut.bytesPP = 1;
    frSpecOut.height  = obj->frmSzOut.h;
    frSpecOut.width   = obj->frmSzOut.w;
    frSpecOut.stride  = frSpecOut.width * frSpecOut.bytesPP;
    frSpecOut.type    = YUV400p;

    for(i=0; i < NR_SHAVES_Y; i++)
    {
        inputFb[i].p1 = NULL;
        inputFb[i].p2 = NULL;
        inputFb[i].p3 = NULL;
        inputFb[i].spec = frSpec;

        outputFb[i].p1 = NULL;
        outputFb[i].p2 = NULL;
        outputFb[i].p3 = NULL;
        outputFb[i].spec = frSpecOut;
    }

    chroma_out_offset = obj->frmSzOut.w*obj->frmSzOut.h/4;

    frSpecUV.bytesPP = 1;
    frSpecUV.height = obj->frmSzOut.h/2;
    frSpecUV.width = obj->frmSzOut.w/2;
    frSpecUV.stride = frSpecUV.width * frSpecUV.bytesPP;
    frSpecUV.type = YUV400p;

    inputFbU.p1 = NULL;
    inputFbU.p2 = NULL;
    inputFbU.p3 = NULL;
    inputFbU.spec = frSpecUV;
    outputFbU.p1 = NULL;
    outputFbU.p2 = NULL;
    outputFbU.p3 = NULL;
    outputFbU.spec = frSpecUV;

    inputFbV.p1 = NULL;
    inputFbV.p2 = NULL;
    inputFbV.p3 = NULL;
    inputFbV.spec = frSpecUV;
    outputFbV.p1 = NULL;
    outputFbV.p2 = NULL;
    outputFbV.p3 = NULL;
    outputFbV.spec = frSpecUV;
}
