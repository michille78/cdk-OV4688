/// =====================================================================================
///
///        @file:      eisMeshGen.h
///        @brief:     :
///        @created:   05/25/2016 03:16:29 PM
///        @author:    csoka, attila.csok@movidius.com
///        @copyright: All code copyright Movidius Ltd 2013, all rights reserved.
///                  For License Warranty see: common/license.txt
/// =====================================================================================
///

/// System Includes
/// -------------------------------------------------------------------------------------
#include "PlgWarpApi.h"

/// Application Includes
/// -------------------------------------------------------------------------------------

/// Source Specific #defines and types (typedef,enum,struct)
/// -------------------------------------------------------------------------------------

/// Global Data (Only if absolutely necessary)
/// -------------------------------------------------------------------------------------

/// Static Local Data
/// -------------------------------------------------------------------------------------

/// Static Function Prototypes
/// -------------------------------------------------------------------------------------

/// Functions Implementation
/// -------------------------------------------------------------------------------------

void eisMeshGenConfigureExtremas(
            plgWarpMeshGenCtrl_t* mg0, plgWarpMeshGenCtrl_t* mg1,
            meshStruct* mY0, meshStruct* mY1);
