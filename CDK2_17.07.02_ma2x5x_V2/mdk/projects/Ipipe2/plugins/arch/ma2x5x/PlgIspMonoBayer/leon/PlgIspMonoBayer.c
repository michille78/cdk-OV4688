/**************************************************************************************************

  @File         : PlgIspMonoBayer.c
  @Author       : Florin Cotoranu
  @Brief        : Contains Opipe mono luma Isp plug-in implementation
  Date          : 09 - March - 2016
  E-mail        : florin.cotoranu@movidius.com
  Copyright     : © Movidius Srl 2016, © Movidius Ltd 2016

  Description :

      Plugin does mono bayer Isp

 **************************************************************************************************/

/**************************************************************************************************
  ~~~ Included types first then APIs from other modules
 **************************************************************************************************/
#include <string.h>
#include <assert.h>
#include "DrvTimer.h"
#include "swcLeonUtils.h"
#include "Opipe.h"
#include "TimeSyncMgr.h"
#include "PlgIspMonoBayerApi.h"
#include "FrameMgrApi.h"
#include "ipipeDbg.h"
#include "ipipeUtils.h"


/**************************************************************************************************
 ~~~  Specific #defines
 **************************************************************************************************/
#ifndef PLG_ISP_MAX_GAMMA_SIZE
#define PLG_ISP_MAX_GAMMA_SIZE (512)
#endif

#ifndef PLG_ISP_GAMMA_SECT
#define PLG_ISP_GAMMA_SECT ".cmx.bss"
#endif

#ifndef PLG_ISP_MAX_LSC_SIZE
#define PLG_ISP_MAX_LSC_SIZE (4096)
#endif

#ifndef PLG_ISP_LSC_SECT
#define PLG_ISP_LSC_SECT ".cmx.bss"
#endif



//Plugin status
enum {
    PLG_OFF = 0,
    PLG_ON  = 1
};

/**************************************************************************************************
 ~~~  Local variables
 **************************************************************************************************/

uint16_t locGammaMono[PLG_ISP_MAX_GAMMA_SIZE] SECTION(PLG_ISP_GAMMA_SECT) ALIGNED(8);
LutCfg   locLutMonoCfg = {0, 0, {0, 0}, (uint16_t*)locGammaMono};

/**************************************************************************************************

  ~~~ Local Functions Implementation
 **************************************************************************************************/

static void fetchIcIspMonoConfig(Opipe *p, icIspConfig *ic)
{
    // Function transfers information from ic -> p.
    // The gamma lut requires only one channel.
    uint32_t i;

    p->format       = BAYER;
    p->rawBits      = ic->pipelineBits;
    p->bayerPattern = ic->bayerOrder;

    //Filter specific
    p->pBlcCfg        = &ic->blc;
    p->pSigmaCfg      = &ic->sigma;
    p->pLscCfg        = &ic->lsc;
    p->pRawCfg        = &ic->raw;
    p->pDbyrCfg       = &ic->demosaic;
    p->pLtmCfg        = &ic->ltm;
    p->pDogCfg        = &ic->dog;
    p->pLumaDnsCfg    = &ic->lumaDenoise;
    p->pLumaDnsRefCfg = &ic->lumaDenoiseRef;
    p->pSharpCfg      = &ic->sharpen;
    p->pChrGenCfg     = &ic->chromaGen;
    p->pMedCfg        = &ic->median;
    p->pChromaDnsCfg  = &ic->chromaDenoise;
    p->pColCombCfg    = &ic->colorCombine;
    p->pLutCfg        = &locLutMonoCfg; //Tell Opipe to use local LutCfg instead of &ic->gamma;
    p->pColConvCfg    = &ic->colorConvert;
    p->aeCfg          = &ic->aeAwbConfig;
    p->aeStats        = ic->aeAwbStats;
    p->afCfg          = &ic->afConfig;
    p->afStats        =  ic->afStats;
    p->pUpfirdn0Cfg   = &ic->updnCfg0;
    p->pUpfirdn12Cfg  = &ic->updnCfg12;

    // LUT. Only one channel is used
    locLutMonoCfg.size       = ic->gamma.size;
    locLutMonoCfg.rgnSize[0] = ic->gamma.rgnSize[0];
    locLutMonoCfg.rgnSize[1] = ic->gamma.rgnSize[1];
    for(i = 0; i < locLutMonoCfg.size; i++)
    {
        locGammaMono[i] = ic->gamma.table[i*4];
    }
}

static void plgIspMonoBayerSetParams(PlgIspMonoBayer *obj)
{
    Opipe       *p  = &obj->op.p;
    icIspConfig *ic =  obj->ispCfg;

    #define GRGB_IMB_EN          0 //Gr/Gb imbalance enable
    #define BAD_PIXEL_FIX_EN     1 //Hot/Cold pixel suppression enable
    #define LUMA_HIST_EN         0 //Luma histogram enable
    #define GAIN_MODE            1 //Bayer 2x2 mode
    #define AF_STATS_EN          0 //AF stats
    #define RGB_HIST_EN          0 //RGB histogram enable
    #define SDC_EN               0 //Static pixel correction
    #define RAW_CFG             (p->format                <<  0) |\
                                (p->bayerPattern          <<  1) |\
                                (GRGB_IMB_EN              <<  3) |\
                                (BAD_PIXEL_FIX_EN         <<  4) |\
                                (LUMA_HIST_EN             <<  7) |\
                                ((p->rawBits - 1)         <<  8) |\
                                (GAIN_MODE                << 12) |\
                                (AF_STATS_EN              << 13) |\
                                (p->pRawCfg->grgbImbalThr << 16) |\
                                (RGB_HIST_EN              << 24) |\
                                (SDC_EN                   << 27)

    #define FP16_MODE            1 // FP16 mode
    #define CHANNEL_MODE         0 // Channel mode
    #define LUT_CFG             (FP16_MODE                   << 0) |\
                                (CHANNEL_MODE                << 1) |\
                                ((p->oPlanes[SIPP_LUT_ID]-1) << 12)


    //icIspConfig -> Opipe translation
    fetchIcIspMonoConfig(p, ic);

    // Configure SIPP filters
    OpipeDefaultCfg(p, SIPP_SIGMA_ID  );
    OpipeDefaultCfg(p, SIPP_DBYR_ID   );
    OpipeDefaultCfg(p, SIPP_DOGL_ID   );
    OpipeDefaultCfg(p, SIPP_SHARPEN_ID);

    p->cfg[SIPP_RAW_ID]   = RAW_CFG;

    //Opipe internally will force progPlanes = 1;
    p->iPlanes[SIPP_LUT_ID] = 1; //just 1 plane
    p->oPlanes[SIPP_LUT_ID] = 1; //just 1 plane
    p->cfg[SIPP_LUT_ID] = LUT_CFG;
}

static int32_t cbTrigerCapture(FrameT *frame, void *params, int (*callback)(int status), void *pluginObj)
{
    UNUSED(callback);
    PlgIspMonoBayer* obj = (PlgIspMonoBayer*)pluginObj;
    assert(frame);
    assert(params);
    if(PLG_ON == obj->crtStatus)
    {
        obj->plg.status = PLG_STATS_RUNNING;
        obj->ispCfg = (icIspConfig  *)params;
        //Get a frame from output mempool
        FrameT *oFrame = FrameMgrAcquireFrame(obj->outputPools);

        //Skip input frame if no more output buffer available
        if (NULL == oFrame) {
            if (obj->procesIspError) {
            obj->procesIspError(pluginObj, IC_SEVERITY_NORMAL, IC_ERROR_RT_OUT_BUFFERS_NOT_AVAILABLE, ((icIspConfig  *)frame->appSpecificData)->userData);
            } else {
                assert (0);
            }
            FrameMgrReleaseFrame(frame);
            obj->plg.status = PLG_STATS_IDLE;
            return -1;
        }

        //Else, we can process
        if (obj->procesStart) obj->procesStart((void*)obj, oFrame->seqNo, obj->ispCfg->userData);

        //From currently set "icIspConfig" to Opipe regs
        plgIspMonoBayerSetParams(obj);

        // Set image size
        obj->op.p.width = obj->frmSz.w;
        obj->op.p.height = obj->frmSz.h;

        //Frame pointers:
        obj->op.pIn->ddr.base    = (uint32_t)frame->fbPtr[0];
        obj->op.pOut->ddr.base  = (uint32_t)oFrame->fbPtr[0];

        //References needed in Opipe EOF handler to update plugin info
        obj->op.p.params[0] = (void*)obj;
        obj->op.p.params[1] = (void*)frame;
        obj->op.p.params[2] = (void*)oFrame;
        //The kick
        OpipeStart(&obj->op.p);
    }
    return 0;
}
//
static void cbNewInputFrame(FrameT *iFrame, void *pluginObject)
{
    PlgIspMonoBayer* obj = (PlgIspMonoBayer*)pluginObject;
    assert(iFrame);
    if(PLG_ON == obj->crtStatus)
    {
        obj->plg.status = PLG_STATS_RUNNING;

        if(iFrame->appSpecificData) {
            obj->ispCfg = (icIspConfig  *)iFrame->appSpecificData;
        }
        else {
            FrameMgrReleaseFrame(iFrame);
            obj->plg.status = PLG_STATS_IDLE;
            return;
        }

        //Else, can try capture (will succeed if oFrame can be allocated)
        cbTrigerCapture(iFrame, obj->ispCfg, NULL, pluginObject);
    }
}
//Opipe EOF callback: adjust associated Plugin Frame buffers
static void opipeIspEof(Opipe *p)
{
    PlgIspMonoBayer *obj    = (PlgIspMonoBayer *)p->params[0];
    FrameT     *iFrame = (FrameT     *)p->params[1];
    FrameT     *oFrame = (FrameT     *)p->params[2];

    assert(iFrame);
    assert(oFrame);

    //Set correct resolution to out frame (now that frame's produced out res is also known)
    switch(iFrame->type)
    {
    case FRAME_T_FORMAT_RAW_8:
        oFrame->stride[0] = iFrame->stride[0];
        oFrame->height[0] = iFrame->height[0];
        break;
    case FRAME_T_FORMAT_RAW_10:
    case FRAME_T_FORMAT_RAW_10_PACK:
        oFrame->stride[0] = ((iFrame->stride[0] * 4) / 5);
        oFrame->height[0] = iFrame->height[0];
        break;
    case FRAME_T_FORMAT_RAW_16:
        oFrame->stride[0] = (iFrame->stride[0] / 2);
        oFrame->height[0] = iFrame->height[0];
        break;
    default:
        oFrame->stride[0] = iFrame->stride[0];
        oFrame->height[0] = iFrame->height[0];
    }

    if (obj->procesEnd)   obj->procesEnd((void*)obj, oFrame->seqNo, obj->ispCfg->userData);

    //new frame produced, updating times informations
    FrameMgrAddTimeStampHist(oFrame, iFrame);
    FrameMgrAndAddTimeStamp (oFrame, TimeSyncMsgGetTimeUs());

    FrameMgrReleaseFrame(iFrame);
    FrameMgrProduceFrame(oFrame);
    obj->plg.status = PLG_STATS_IDLE;
}

//
static int32_t fini(void *pluginObject)
{
    PlgIspMonoBayer *object = (PlgIspMonoBayer*)pluginObject;
    object->crtStatus  = PLG_OFF;
    return 0;
}

//
static int32_t init(FramePool *outputPools, int nOutputPools, void *pluginObject)
{
    UNUSED(nOutputPools);
    PlgIspMonoBayer* obj = (PlgIspMonoBayer*)pluginObject;
    // this plugin produce just 1 output frame, so not take in consideration nOutputPools params,
    // as this have to be 1
    obj->outputPools = outputPools;
    return 0;
}

//Opipe Mono Bayer Isp yuv420 output creation
static void CreateOpipe(PlgIspMonoBayer *o, uint32_t inFmt, uint32_t prevAble)
{
    UNUSED(prevAble);
    OpipeBayerMono *opM = &o->op;

    if(o->cSigma == NULL) o->cSigma   = ogcBuff[SIPP_MIPI_RX0_ID ];
    if(o->cDbyr  == NULL) o->cDbyr    = ogcBuff[SIPP_DBYR_LUMA_ID];
    if(o->cUsm   == NULL) o->cUsm     = ogcBuff[SIPP_SHARPEN_ID  ];
    if(o->cLut   == NULL) o->cLut     = ogcBuff[SIPP_LUT_ID      ];

    assert(o->cSigma != NULL);
    assert(o->cDbyr  != NULL);
    assert(o->cUsm   != NULL);
    assert(o->cLut   != NULL);

    //Must specify buffers first
    opM->cBufInSig  .base = (uint32_t)o->cSigma;   opM->cBufInSig.h   = I_CBUFF_H;
    opM->cBufOutDbyr.base = (uint32_t)o->cDbyr;    opM->cBufOutDbyr.h = DBYR_Y_H ;
    opM->cBufOutUsm .base = (uint32_t)o->cUsm;     opM->cBufOutUsm.h  = O_CBUFF_H;
    opM->cBufOutLut .base = (uint32_t)o->cLut;     opM->cBufOutLut.h  = O_CBUFF_H;

    OpipeCreateBayerMono(opM, inFmt);
    initLutCopyTask();
}

//Opipe related: returns Opipe circular buffer requirements in bytes.
//WARNING: using SIPP_MIPI_RX0_ID for Sigma Input !
// wSig  : width for RAW filters (Sigma, Lsc, Raw, Dbyr input)
// wMain : width for filters below debayer (can be wSig/2 if preview is enabled)
// req   : required circular-output-buffer size in bytes
static void QueryCircBuffSizes(PlgIspBase *me, uint32_t wSig, uint32_t wMain, uint32_t wOut, uint32_t* req)
{
    UNUSED(me);
    //Enlarge line widths a bit to accommodate internal padding:
    wSig += 8; wMain += 8;

    //                         height    * pl * width * bpp
    req[SIPP_MIPI_RX0_ID ] =  I_CBUFF_H      * wSig     ; // bpp:N/A
    req[SIPP_DBYR_LUMA_ID] =  DBYR_Y_H       * wMain * 2; // bpp:2(fp16)
    req[SIPP_SHARPEN_ID  ] =  O_CBUFF_H      * wMain * 2; // bpp:2(fp16)
    req[SIPP_LUT_ID      ] =  O_CBUFF_H      * wOut * 1;  // bpp:1(  u8)
}


/**************************************************************************************************
  ~~~ Exported Functions Implementation
 **************************************************************************************************/

//
void PlgIspMonoBayerConfig(void *plgObject, icSize frameSz, uint32_t inFmt, uint32_t prevAble)
{
    PlgIspMonoBayer *obj   = (PlgIspMonoBayer *)plgObject;
    obj->frmSz  = frameSz;
    //Must know resolution in order do create an Opipe object
    if (PLG_ISPMONOBAYER_CREATED == obj->status) {
        CreateOpipe(obj, inFmt, prevAble);
        obj->status = PLG_ISPMONOBAYER_INUSE;
    }
    else {
        if(PLG_ISPMONOBAYER_INUSE == obj->status) {
            //Assume that change resolution, so update it
            //Resolution updates (if needed)
            //OpipeSetRes(&obj->op.p, frameSz.w, frameSz.h);
        }
        else {
            // try to start a source but not create is first,
            // or a memory corruption overwrite plug-in structure
            assert(0);
        }
    }

    obj->crtStatus = PLG_ON;
    //In Opipe EOF callback, will update Ipipe Frame buffers status
    obj->op.p.cbEndOfFrame = opipeIspEof;
}


//
void PlgIspMonoBayerCreate(void *pluginObject)
{
    memset(pluginObject, 0, sizeof(PlgIspMonoBayer));
    PlgIspMonoBayer *object          = (PlgIspMonoBayer*)pluginObject;
    // init hw things, or all the init side that not need params,
    object->plg.init            = init; //to associate output pool
    object->plg.fini            = fini; //to mark STOP
    object->plg.trigger         = cbTrigerCapture;
    object->cbList[0].callback  = cbNewInputFrame;
    object->cbList[0].pluginObj = pluginObject;
    object->plg.callbacks       = object->cbList;
    object->plg.status          = PLG_STATS_IDLE;
    object->crtStatus           = PLG_OFF;
    object->status              = PLG_ISPMONOBAYER_CREATED;

    object->base.fnQueryMemReq  = QueryCircBuffSizes;
}
