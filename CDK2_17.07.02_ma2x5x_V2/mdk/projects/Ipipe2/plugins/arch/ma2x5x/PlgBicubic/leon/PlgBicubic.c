/**************************************************************************************************

 @File         : PlgBicubicApi.c
 @Author       : AG
 @Brief        : Contain Bicubic plug-in implementation
 Date          : 03 - Dec - 2015
 E-mail        : xxx.xxx@movidius.com
 Copyright     : © Movidius Srl 2014, © Movidius Ltd 2015

 Description :

    Applies Bicubic filter on specified input frame, with specified (or default) homography
    calibration values.
    Resources used:
         Leon RT.
         Bicubic filter

    Interrupt base.
 **************************************************************************************************/

/**************************************************************************************************
 ~~~ Included types first then APIs from other modules
 **************************************************************************************************/
#include <string.h>
#include <stdio.h>
#include <assert.h>
#include "DrvTimer.h"
#include "swcLeonUtils.h"
#include "TimeSyncMgr.h"
#include "FrameMgrApi.h"
#include "ipipeDbg.h"
#include "ipipeUtils.h"
#include "DrvShaveL2Cache.h"    /// Shave L2 Cache driver
#include "PlgBicubicApi.h"
#include <DrvIcbDefines.h>

/**************************************************************************************************
 ~~~  Specific #defines
 **************************************************************************************************/
//Plugin status
enum {
    PLG_OFF = 0,
    PLG_ON  = 1
};

// Fixed for the moment
// TODO: dynamically allocate memory
#ifndef PLG_BICUBIC_MAX_WIDTH
#define PLG_BICUBIC_MAX_WIDTH 640
#endif
#ifndef PLG_BICUBIC_MAX_HEIGHT
#define PLG_BICUBIC_MAX_HEIGHT 480
#endif

#define FRAME_SIZE_BYTES         (PLG_BICUBIC_MAX_WIDTH * PLG_BICUBIC_MAX_WIDTH) // Width * Height *AlgoBPP(algorithm processes YUV400, so it is 1)

#ifndef PLG_BICUBIC_XY_SECTION
#define PLG_BICUBIC_XY_SECTION  ".ddr.bss"
#endif

#define ALIGNED(x) __attribute__((aligned(x)))

#ifndef HOMOGRAPHY_SIZE
#define HOMOGRAPHY_SIZE         9
#endif
/**************************************************************************************************
 ~~~  Local variables
 **************************************************************************************************/
// Fixed for the moment
// TODO: dynamically allocate memory
fp32*   xyRectifiedBuffer[FRAME_SIZE_BYTES * 2]  __attribute__((section(PLG_BICUBIC_XY_SECTION))) ALIGNED(8); // xy coefficients for rectified image

PlgBicubic*      currentObject;
uint32_t         configuredWidth;
uint32_t         configuredHeight;

// Calibration info config - by default IMX208_200124
float calibrationH[HOMOGRAPHY_SIZE] ={ // SENSOR_IMX208_200315
                                0.9744101060179016, -0.01842923359075381, 36.02105011698299,
                                0.01303609916072842, 0.9824515102716611, -3.28977967967297,
                                -1.787124750237852e-005, -3.439739752742806e-006, 1};

static  float*  homography  = &calibrationH[0]; //precalibrated: OV7251_21012, OV7251_210013, OV7251_210014, OV7251_210015, IMX208_200124, IMX208_200315

/**************************************************************************************************
 ~~~ Local Functions Implementation
 **************************************************************************************************/
static void cbNewInputFrame(FrameT *iFrame, void *pluginObject) {
    PlgBicubic* obj = (PlgBicubic*)pluginObject;


    assert(iFrame);
    assert(configuredWidth==obj->frmSz.w);
    assert(configuredHeight==obj->frmSz.h);

    if(PLG_ON == obj->crtStatus)
    {
        if(PLG_STATS_RUNNING == obj->plg.status) {
            FrameMgrReleaseFrame(iFrame);
            return;
        }

        obj->plg.status = PLG_STATS_RUNNING;

        //Get a frame from output mempool
        FrameT *oFrame = FrameMgrAcquireFrame(obj->outputPools);
        obj->oFrame = oFrame;
        obj->iFrame = iFrame;
        //Skip input frame if no more output buffer available
        if (NULL == oFrame) {

            FrameMgrReleaseFrame(iFrame);
            //assert(oFrame != NULL); // TODO: delete debug code
            obj->plg.status = PLG_STATS_IDLE;
            //assert(0);
            return;
        }
        //Else, we can process

        // Hack, just horizontal vertical crop supported, need real crop capability from opipe
        //oFrame->stride[0] = iFrame->stride[0];
        //oFrame->height[0] = iFrame->height[0];
        oFrame->stride[0] = obj->frmSz.w;
        oFrame->height[0] = obj->frmSz.h;
        uint32_t heightCropOfset = ((iFrame->height[0] - obj->frmSz.h)>>1)*obj->frmSz.w;
        // Rectify image
        bicubicWarpStart(iFrame->fbPtr[0] + heightCropOfset,
                oFrame->fbPtr[0],
                (fp32*)&xyRectifiedBuffer,
                obj->frmSz.w,
                obj->frmSz.h, 1, 1
        );
        currentObject = (PlgBicubic*)pluginObject;
    }
}

//bicubic filter EOF callback: adjust associated Plugin Frame buffers
void bicubicEof(void){
    //PlgBicubic* obj = pluginObject;
    FrameT     *iFrame = currentObject->iFrame;
    FrameT     *oFrame = currentObject->oFrame;

    assert(iFrame);
    assert(oFrame);

    /// Flush L2C used by bicubic
    DrvShaveL2CachePartitionFlushAndInvalidate(0);

    FrameMgrReleaseFrame(iFrame);
    FrameMgrProduceFrame(oFrame);
    currentObject->plg.status = PLG_STATS_IDLE;

}

//
static int32_t fini(void *pluginObject) {
    PlgBicubic *object = (PlgBicubic*)pluginObject;
    object->crtStatus  = PLG_OFF;
    return 0;
}

//
static int32_t init(FramePool *outputPools, int nOutputPools, void *pluginObject) {
    PlgBicubic* obj = (PlgBicubic*)pluginObject;
    // this plugin produce just 1 output frame, so not take in consideration nOutputPools params,
    // as this have to be 1
    obj->outputPools = outputPools;
    obj->crtStatus  = PLG_ON;

    return 0;
}

/**************************************************************************************************
 ~~~ Exported Functions Implementation
 **************************************************************************************************/
//
void PlgBicubicCreate(void *pluginObject) {
    PlgBicubic *object          = (PlgBicubic*)pluginObject;
    // init hw things, or all the init side that not need params,
    object->plg.init            = init; //to associate output pool
    object->plg.fini            = fini; //to mark STOP
    object->cbList[0].callback  = cbNewInputFrame;
    object->cbList[0].pluginObj = pluginObject;
    object->plg.callbacks       = object->cbList;
    object->plg.status          = PLG_STATS_IDLE;
    object->crtStatus           = PLG_ON;
    object->status              = PLG_BICUBIC_CREATED;
    object->procesEnd           = bicubicEof;
}

//
void PlgBicubicConfig(void *pluginObject, icSize frameSz, float*  homographyValues) {
    PlgBicubic *obj          = (PlgBicubic*)pluginObject;

    if(homographyValues != NULL)
    {
        homography = homographyValues;
    }

    obj->frmSz.w            = frameSz.w;
    obj->frmSz.h            = frameSz.h;
    configuredWidth            = frameSz.w;
    configuredHeight           = frameSz.h;

    currentObject=obj;

    DrvShaveL2CacheClearPartitions();
    //TODO: allow user to define other partitions, in order to use shave l2c and for other scope
    //Set Shave L2 cache partitions
    DrvShaveL2CacheSetupPartition(SHAVEPART256KB);
    //Allocate Shave L2 cache set partitions
    DrvShaveL2CacheAllocateSetPartitions();

//    loadHomography(CALIB_FILENAME, 9*sizeof(float), (void*)homography);

    /// Build and configure the SIPP ISP
    bicubicWarpGenerateMeshHomographyRTP((fp32*)&xyRectifiedBuffer, homography, obj->frmSz.w, obj->frmSz.h);
    bicubicWarpInit(currentObject->procesEnd, IRQ_BICUBIC);
}
