/**************************************************************************************************

 @File         : PlgTemplateApi.h
 @Author       : MT
 @Brief        : Contain Dummy Example plug-in interface
 Date          : 29 - June - 2015
 E-mail        : xxx.xxx@movidius.com
 Copyright     : © Movidius Srl 2014, © Movidius Ltd 2015

 Description :
     This plug-in just copy a frame from input place to output place. His scope is to allow easy
     understand plug-in framework interface.

     Resources used:
         Leon

     No interrupt base, used Leon Rt for a full frame time processing time

 **************************************************************************************************/


#ifndef __PLG_SAMPLE_API__
#define __PLG_SAMPLE_API__


/**************************************************************************************************
 ~~~ Included types first then APIs from other modules
 ***************************************************************************************************/
#include "PlgTypes.h"

/**************************************************************************************************
 ~~~  Specific #defines
 **************************************************************************************************/


/**************************************************************************************************
 ~~~  Basic typdefs
 **************************************************************************************************/
typedef struct PlgTemplateStruct {
    PlgType plg;
    // specific component interface
    // specific plugin command function, init, configs ...
    void    (*init)          (icSize iframeSize, void *pluginObject);
    // callback possible to be linked to the plugin,
    void    (*procesStart)   (uint32_t seqNr);
    void    (*procesEnd)     (uint32_t seqNr);

    /// Private members. All data structures have to be internal
    FramePool               *outputPools;
    icSize                  privateCfg;
    volatile int32_t        crtStatus; // internal usage
    FrameProducedCB         cbList[1];
} PlgTemplate;


/**************************************************************************************************
 ~~~  Exported Functions
 **************************************************************************************************/
void PlgTemplateCreate(void *pluginObject);


#endif //__PLG_SAMPLE_API__
