/**************************************************************************************************

 @File         : PlgSgbm.c
 @Author       : MT
 @Brief        : Contain  Disparity map interface plug-in
 Date          : 01 - March - 2015
 E-mail        : xxx.xxx@movidius.com
 Copyright     : © Movidius Srl 2014, © Movidius Ltd 2015

 Description :
    This plug-in Compute hierarchical Dense Disparity Map from two stereo images.
    Resources used:
         Leon RT.
         2 Shave (0 and 2),  4 slices (0,1,2,3).
         CmxDma controlled by Shaves.
         Resolution is fixed, 640x480
    Interrupt base.

 **************************************************************************************************/


/**************************************************************************************************
 ~~~ Included types first then APIs from other modules
***************************************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <limits.h>
#include <stdint.h>
#include "DrvCmxDma.h"
#include "TimeSyncMgr.h"
#include "FrameMgrApi.h"
#include "PlgSgbmApi.h"
#include "DrvSvu.h"
#include "VcsHooksApi.h"

#include "DisparityMapApiDefines.h"
#include "Sections.h"
#include "Defines.h"

#include <swcTestUtils.h>
#include <swcShaveLoader.h>
#include <DrvShaveL2Cache.h>

/**************************************************************************************************
 ~~~  Specific #defines
 **************************************************************************************************/
#define PLGSGBMDSHV__IRQ_LEVEL  3
#define PLGSGBM_FRAME_MASK (0xF)
enum {
    PLGSGBM_STATS_OFF = 0,
    PLGSGBM_STATS_ON  = 1
};

/**************************************************************************************************
 ~~~  Imported Variable symbol from shave variables
 **************************************************************************************************/
// Shave entrypoints
extern u32 DisparityMap0_disparityMapRunFrame;
extern u32 DisparityMap2_disparityMapRunFrame;
static u32 FnDisparityMapRun[SHAVES_USED] = {(u32)&DisparityMap0_disparityMapRunFrame, (u32)&DisparityMap2_disparityMapRunFrame};


/**************************************************************************************************
 ~~~  Local Variable
 **************************************************************************************************/
static u32 listShaves[SHAVES_USED] = {0, 2};
static u32 listEntrypoints[SHAVES_USED] = {0, 1};


DisparityConfig dispCfg[MAX_PATCHES * PYR_LEVELS];
AlgorithmConfig algoCfg;
static Tile_t   tileInit[4];
#define LEVEL0_TILES_SIZE   ((DOWNSAMPLED_WIDTH + PATCH_W_OVERLAP_LEVEL0) * (DOWNSAMPLED_HEIGHT))
#define LEVEL1_TILES_SIZE   ((MAX_WIDTH + PATCH_W_OVERLAP_LEVEL1) * (MAX_HEIGHT))
u8 disparityTilesLevel1[LEVEL1_TILES_SIZE]      ALIGNED(8)  DDR_BSS;       // buffer for tiles of Level 1 VGA
u8 disparityTilesLevel0[LEVEL0_TILES_SIZE]      ALIGNED(8)  DDR_BSS;       // buffer for tiles of Level 0 QVGA

/**************************************************************************************************
 ~~~ Local File function declarations
 **************************************************************************************************/
static        int32_t   init(FramePool *outputPools, int nOutputPools, void *pluginObject);
static        int32_t   stop(void *pluginObject);
static        void      runDisparityMap(AlgorithmConfig* algoConfig, DisparityConfig* dispConfig,
        u32 width, void *appRef);
static        void      shvIrqHandler(uint32_t source);
static inline void      eofFrameHandler(PlgSgbm *plug);
static        void      tryRun(PlgSgbm *plug);
/**************************************************************************************************
 ~~~ Local Functions Implementation
 **************************************************************************************************/

//
static int32_t init(FramePool *outputPools, int nOutputPools, void *pluginObject) {
    UNUSED(nOutputPools);
    PlgSgbm *plug       = (PlgSgbm*)pluginObject;
    plug->outputPools   = outputPools;
    plug->crtStatus     = PLGSGBM_STATS_ON;
    return 0;
}

//
static int32_t  stop(void *pluginObject) {
    PlgSgbm *plug     = (PlgSgbm*)pluginObject;
    plug->crtStatus   = PLGSGBM_STATS_OFF;
    return 0;
}

//
static void shvIrqHandler(uint32_t source) {
    PlgSgbm *plug = (PlgSgbm*)GET_REG_WORD_VAL(SVU_CTRL_ADDR[source - IRQ_SVE_0]+SLC_OFFSET_SVU+IRF_BASE+(18<<2));
    plug->shvRunMask++;
    //printf("Irq %d \n", plug->shvRunMask);
    DrvIcbIrqClear(source);
    if(plug->shvRunMask == plug->shvTotal) {
        plug->shvRunNr++;
        eofFrameHandler(plug);
    }
}
//
#define SGBM_DEBUG_ON
volatile uint32_t sgbmDump = 0;
static inline void eofFrameHandler(PlgSgbm *plug) {
#ifdef SGBM_DEBUG_ON
    if (sgbmDump) {
        saveMemoryToFile((uint32_t)plug->frameInProcessing[0]->fbPtr[0], MAX_WIDTH*MAX_HEIGHT, "inputFrameLeft_smoothed_640x480_P400.bw");
        saveMemoryToFile((uint32_t)plug->frameInProcessing[1]->fbPtr[0], MAX_WIDTH*MAX_HEIGHT, "inputFrameRight_smoothed_640x480_P400.bw");
        saveMemoryToFile((uint32_t)plug->frameInProcessing[2]->fbPtr[0], MAX_WIDTH*MAX_HEIGHT>>2, "inputFrameLeft_smoothed_320x240_P400.bw");
        saveMemoryToFile((uint32_t)plug->frameInProcessing[3]->fbPtr[0], MAX_WIDTH*MAX_HEIGHT>>2, "inputFrameRight_smoothed_320x240_P400.bw");
        saveMemoryToFile((uint32_t)plug->oFrame->fbPtr[0], MAX_WIDTH*MAX_HEIGHT, "outputFrame_640x480_P400.bw");
        sgbmDump = 0;
    }

#endif
    FrameMgrReleaseFrame(plug->frameInProcessing[0]);
    FrameMgrReleaseFrame(plug->frameInProcessing[1]);
    FrameMgrReleaseFrame(plug->frameInProcessing[2]);
    FrameMgrReleaseFrame(plug->frameInProcessing[3]);
    plug->frameInProcessing[0] = NULL;
    plug->frameInProcessing[1] = NULL;
    plug->frameInProcessing[2] = NULL;
    plug->frameInProcessing[3] = NULL;
    FrameMgrProduceFrame(plug->oFrame);
    plug->frameInProcMask = 0;
    plug->plg.status = PLG_STATS_IDLE;
    tryRun(plug);
}


// SHAVE Disparity Map computation
static void runDisparityMap(AlgorithmConfig* algoConfig, DisparityConfig* dispConfig, u32 width, void *appRef) {
    u32 shaveNumber, entrypointIdx, patchIdx;

    //printf("start_shv \n");
    for (patchIdx = 0; patchIdx < MAX_PATCHES; patchIdx++)
    {
        shaveNumber = listShaves[patchIdx];
        entrypointIdx = listEntrypoints[patchIdx];

        swcResetShave(shaveNumber);

        swcSetAbsoluteDefaultStack(shaveNumber);

        swcStartShaveAsyncCC(shaveNumber, FnDisparityMapRun[entrypointIdx], shvIrqHandler, "iiii", algoConfig, &dispConfig[entrypointIdx], width, appRef);
        //swcStartShaveCC(shaveNumber, FnDisparityMapRun[entrypointIdx], "iiii", algoConfig, &dispConfig[entrypointIdx], width, appRef);
    }
    //swcWaitShaves(SHAVES_USED, listShaves);

    //DrvShaveL2CachePartitionFlushAndInvalidate(0);
}

// Initialize all output buffers to random value
void setBuffers(uint8_t* frameBufferDisparities, uint8_t* disparityMapL1, uint8_t* disparityMapL0) {
    memset(frameBufferDisparities, 0xAA, MAX_WIDTH * MAX_HEIGHT);
    memset(disparityMapL1, 0xAA, (MAX_WIDTH + PATCH_W_OVERLAP_LEVEL1) * (MAX_HEIGHT));
    memset(disparityMapL0, 0xAA, (DOWNSAMPLED_WIDTH + PATCH_W_OVERLAP_LEVEL0) * (DOWNSAMPLED_HEIGHT));
}

// Setup
void setupAlgoConfiguration(AlgorithmConfig* algoCfg, u32 censusKernelSize, u32 censusLinePadding, u32 medianKernelSize, u32 medianLinePadding, u32 maxDisparities) {
    algoCfg->cfgCensusKernelSize    = censusKernelSize;
    algoCfg->cfgMedianKernelSize    = medianKernelSize;
    algoCfg->cfgLinePadding         = censusLinePadding;
    algoCfg->cfgMedianPadding       = medianLinePadding;
    algoCfg->cfgMaxDisparities      = maxDisparities;
}

void initializeImageTiles(DisparityConfig* dispCfg, u32 patchW, u32 patchH, u32 overlapW, u32 overlapH, u32 overlapPatch) {
    u32 i;
    for (i = 0; i < PYR_LEVELS; i++) {
        dispCfg[i].tile->no = i%2;
        dispCfg[i].tile->width = patchW;
        dispCfg[i].tile->height = patchH;
        dispCfg[i].tile->overlapW = overlapW;
        dispCfg[i].tile->overlapH = overlapH;
        dispCfg[i].tile->overlapPatch = overlapPatch;
    }
}

// Configuration of algorithm parameters and buffers
void setupTileConfiguration(DisparityConfig* pyrCfg, AlgorithmConfig* algoCfg, u8* imgL, u8* imgR, u8* disparities, u32 width, u32 height, u32 stride, u32 bpp) {
    UNUSED(algoCfg);
    // left image
    pyrCfg->leftImage.p1 = imgL;
    pyrCfg->leftImage.spec.type = YUV400p;
    pyrCfg->leftImage.spec.width = width;
    pyrCfg->leftImage.spec.height = height;
    pyrCfg->leftImage.spec.bytesPP = bpp;
    pyrCfg->leftImage.spec.stride = stride;

    // right image
    pyrCfg->rightImage.p1 = imgR;
    pyrCfg->rightImage.spec.type = YUV400p;
    pyrCfg->rightImage.spec.width = width;
    pyrCfg->rightImage.spec.height = height;
    pyrCfg->rightImage.spec.bytesPP = bpp;
    pyrCfg->rightImage.spec.stride = stride;

    // disparity map
    pyrCfg->disparityMap = disparities;
}

// Split image into patches
void computeImageTiles(DisparityConfig* dispCfg, AlgorithmConfig* algoCfg, u8* imgTileLeft, u8* imgTileRight, u8* tileDisparity, u32 width, u32 height) {
    u32 ix, iy, patchNo = 0;
    u32 iyDisparities = 0;
    u32 ixDisparities = 0;
    u32 patchWidth = dispCfg->tile->width;
    u32 patchHeight = dispCfg->tile->height;
    u32 overlapCensusW = dispCfg->tile->overlapW;
    u32 overlapCensusH = dispCfg->tile->overlapH;

    u32 w;
  //u32 h;

    //printf("\nCompute image tiles...\n");
    for (iy = 0; iy < height; iy += (patchHeight - overlapCensusH))
    {
        ixDisparities = 0;
        for (ix = 0; ix < width; ix += (patchWidth - overlapCensusW))
        {
            w = dispCfg[patchNo].tile->width;
          //h = dispCfg[patchNo].tile->height; //unused?

            if (ix + patchWidth > width)
                dispCfg[patchNo].tile->x = width - patchWidth;
            else
                dispCfg[patchNo].tile->x = ix;

            if (iy + patchHeight > height)
                dispCfg[patchNo].tile->y = height - patchHeight;
            else
                dispCfg[patchNo].tile->y = iy;

            setupTileConfiguration(&dispCfg[patchNo], algoCfg, &imgTileLeft[w * dispCfg[patchNo].tile->y + dispCfg[patchNo].tile->x], &imgTileRight[w * dispCfg[patchNo].tile->y + dispCfg[patchNo].tile->x],
                    &tileDisparity[patchNo * patchWidth * patchHeight], dispCfg[patchNo].tile->width, dispCfg[patchNo].tile->height, width, 1);

            patchNo++;
            ixDisparities += patchWidth;
            if ( ix + patchWidth >= width )
                break;
        }
        if  ( iy + patchHeight >= height )
            break;
        iyDisparities += patchHeight;
    }
}

// Compute disparity map for image tiles
void computeDisparityMap(uint8_t* smoothedLeft, uint8_t* smoothedRight,
        uint8_t* downsampledLeft, uint8_t* downsampledRight,
        uint8_t* depthMapPyramidal,
        uint32_t width, uint32_t height,
        void *appRef )
{
    UNUSED(width);  //???
    UNUSED(height); //???

    // L0 = QVGA, L1 = VGA
    uint32_t widthL1 = MAX_WIDTH;
    uint32_t heightL1 = MAX_HEIGHT;
    uint32_t patchWidthL1 = PATCH_WIDTH_LEVEL1;
  //uint32_t patchHeightL1 = PATCH_HEIGHT_LEVEL1;

    uint32_t widthL0 = widthL1/2;
    uint32_t heightL0 = heightL1/2;
  //uint32_t patchWidthL0 = PATCH_WIDTH_LEVEL0;
  //uint32_t patchHeightL0 = PATCH_HEIGHT_LEVEL0;

    uint32_t i;



    // 1. Initialization: disparity offsets for dma-ing last 2 image tiles (L1 VGA); fill offsets for first 2 image tiles with dummy 0 0
    uint32_t finalDisparityOffset[PYR_LEVELS*MAX_PATCHES] = {0, 0, 0, PATCH_0_WIDTH_LEVEL1_TO_KEEP};
    for (i = 0; i < PYR_LEVELS * MAX_PATCHES; i++)
    {
        dispCfg[i].tile = &tileInit[i];
        dispCfg[i].disparityMapFinal = depthMapPyramidal + finalDisparityOffset[i];
    }

    //setBuffers(depthMapPyramidal, disparityTilesLevel1, disparityTilesLevel0);
    setupAlgoConfiguration(&algoCfg, CENSUS_KERNEL_SIZE, LINE_PADDING, MEDIAN_KERNEL_SIZE, MEDIAN_PADDING, MAX_DISPARITIES_LEVEL0);

    // 3. Compute tiles for level 0: QVGA
    initializeImageTiles(&dispCfg[0], PATCH_WIDTH_LEVEL0, PATCH_HEIGHT_LEVEL0, CENSUS_W_OVERLAP_LEVEL0, CENSUS_H_OVERLAP_LEVEL0, PATCH_W_OVERLAP_LEVEL0);
    computeImageTiles(&dispCfg[0], &algoCfg, downsampledLeft, downsampledRight, disparityTilesLevel0, widthL0, heightL0);

    // 4. Compute tiles for level 1: VGA
    initializeImageTiles(&dispCfg[2], PATCH_WIDTH_LEVEL1, PATCH_HEIGHT_LEVEL1, CENSUS_W_OVERLAP_LEVEL1, CENSUS_H_OVERLAP_LEVEL1, PATCH_W_OVERLAP_LEVEL1);
    computeImageTiles(&dispCfg[2], &algoCfg, smoothedLeft, smoothedRight, disparityTilesLevel1, widthL1, heightL1);

    // 5. Run disparity map computation
    runDisparityMap(&algoCfg, dispCfg, patchWidthL1, appRef);
}

//
static void produceFrame(PlgSgbm *plug) {
    assert(PLG_STATS_IDLE == plug->plg.status);
    if(PLGSGBM_STATS_ON == plug->crtStatus) {
        plug->plg.status = PLG_STATS_RUNNING;
        //InRunningPlugAddress = pluginObject;

        FrameT *oFrame = FrameMgrAcquireFrame(plug->outputPools);
        // skip frame if no more output buffer available
        if (NULL == oFrame) { // release input frame and exit, frame will be dropped
            FrameMgrReleaseFrame(plug->frameInProcessing[0]);
            FrameMgrReleaseFrame(plug->frameInProcessing[1]);
            FrameMgrReleaseFrame(plug->frameInProcessing[2]);
            FrameMgrReleaseFrame(plug->frameInProcessing[3]);
            plug->plg.status = PLG_STATS_IDLE;
            return;
        }
        plug->oFrame = oFrame;
        oFrame->stride[0] = MAX_WIDTH;
        oFrame->height[0] = MAX_HEIGHT;
        //oFrame->height[0] = plug->frameInProcessing[0]->height[0];

        //assert(plug->frameInProcessing[0]->stride[0] == MAX_WIDTH);
        //assert(plug->frameInProcessing[0]->height[0] == MAX_HEIGHT);
        uint32_t heightCropOfset = ((plug->frameInProcessing[0]->height[0] - MAX_HEIGHT)>>1)*MAX_WIDTH;
        assert(plug->frameInProcessing[1]->stride[0] == MAX_WIDTH);
        assert(plug->frameInProcessing[2]->stride[0] == MAX_WIDTH>>1);
        assert(plug->frameInProcessing[3]->stride[0] == MAX_WIDTH>>1);
        assert(plug->frameInProcessing[1]->height[0] == MAX_HEIGHT);
        assert(plug->frameInProcessing[2]->height[0] == MAX_HEIGHT>>1);
        assert(plug->frameInProcessing[3]->height[0] == MAX_HEIGHT>>1);
#ifdef PLGSGBM_NO_IRQ_TRIGER
        plug->plgSgbmOnToRun = 1;
#else
        computeDisparityMap(
                plug->frameInProcessing[0]->fbPtr[0] + heightCropOfset,
                plug->frameInProcessing[1]->fbPtr[0],
                plug->frameInProcessing[2]->fbPtr[0],
                plug->frameInProcessing[3]->fbPtr[0],
                oFrame->fbPtr[0],
                MAX_WIDTH,
                MAX_HEIGHT,
                (void*)plug);
        // run the shave code
        plug->shvRunMask = 0;
#endif
    }
}

void plgSgbmTryRun(void *pluginObject) {
    PlgSgbm *plug = (PlgSgbm*)pluginObject;
#ifdef PLGSGBM_NO_IRQ_TRIGER
    if(plug->plgSgbmOnToRun) {
        uint32_t heightCropOfset = ((plug->frameInProcessing[0]->height[0] - MAX_HEIGHT)>>1)*MAX_WIDTH;
        computeDisparityMap(
                plug->frameInProcessing[0]->fbPtr[0] + heightCropOfset,
                plug->frameInProcessing[1]->fbPtr[0],
                plug->frameInProcessing[2]->fbPtr[0],
                plug->frameInProcessing[3]->fbPtr[0],
                plug->oFrame->fbPtr[0],
                MAX_WIDTH,
                MAX_HEIGHT,
                (void*)plug);
        // run the shave code
        plug->shvRunMask = 0;
        plug->plgSgbmOnToRun = 0;
    }
#else
    // nothing to be made in this, as execution is controlled from interrupt context
#endif
}

//
static void tryRun(PlgSgbm *plug) {
    if(PLGSGBM_FRAME_MASK == plug->runEnMask) {
        if(0 == plug->frameInProcMask) {
            plug->frameInProcMask = PLGSGBM_FRAME_MASK;
            plug->frameInProcessing[0] = plug->frameInExpectation[0];
            plug->frameInProcessing[1] = plug->frameInExpectation[1];
            plug->frameInProcessing[2] = plug->frameInExpectation[2];
            plug->frameInProcessing[3] = plug->frameInExpectation[3];
            plug->frameInExpectation[0] = 0;
            plug->frameInExpectation[1] = 0;
            plug->frameInExpectation[2] = 0;
            plug->frameInExpectation[3] = 0;
            produceFrame(plug);
            plug->runEnMask = 0;
        }
    }
}

//
static void producedFrameX(FrameT *frame, void *pluginObject, int inFrameId) {
    PlgSgbm *plug = (PlgSgbm*)pluginObject;
    if(frame) {
        if(plug->frameInExpectation[inFrameId]) {
            FrameMgrReleaseFrame(plug->frameInExpectation[inFrameId]);
            plug->frameInExpectation[inFrameId] = frame;
            plug->runEnMask = plug->runEnMask | (1 << inFrameId);
        }
        else {
            plug->frameInExpectation[inFrameId] = frame;
            plug->runEnMask = plug->runEnMask | (1 << inFrameId);
        }
    }
    tryRun(plug);
}

//
static void producedFrameLUp(FrameT *frame, void *pluginObject) {
    producedFrameX(frame, pluginObject, 0);
}
//
static void producedFrameRUp(FrameT *frame, void *pluginObject) {
    producedFrameX(frame, pluginObject, 1);
}
//
static void producedFrameLDw(FrameT *frame, void *pluginObject) {
    producedFrameX(frame, pluginObject, 2);
}
//
static void producedFrameRDw(FrameT *frame, void *pluginObject) {
    producedFrameX(frame, pluginObject, 3);
}

/**************************************************************************************************
 ~~~ Exported Functions Implementation
 **************************************************************************************************/
void PlgSgbmCreate(void *pluginObject) {
    PlgSgbm *plug              = (PlgSgbm*)pluginObject;
    memset(pluginObject, 0, sizeof(PlgSgbm));
    plug->plg.init              = init;
    plug->plg.fini              = stop;
    plug->cbList[0].callback    = producedFrameLUp;
    plug->cbList[0].pluginObj   = pluginObject;
    plug->cbList[1].callback    = producedFrameRUp;
    plug->cbList[1].pluginObj   = pluginObject;
    plug->cbList[2].callback    = producedFrameLDw;
    plug->cbList[2].pluginObj   = pluginObject;
    plug->cbList[3].callback    = producedFrameRDw;
    plug->cbList[3].pluginObj   = pluginObject;
    plug->plg.callbacks         = plug->cbList;
    plug->plg.status            = PLG_STATS_IDLE;
    plug->crtStatus             = PLGSGBM_STATS_OFF;
    // Disparity map is implemented in order to run on 2 shaves
    plug->shvTotal              = 2;
}

// clear internal copy of frames, in order to avoid wrong restarting
// srcId 0 or 1
void PlgSgbmStopSrc(void *pluginObject) {
    PlgSgbm *plug = (PlgSgbm*)pluginObject;
    plug->frameInExpectation[0] = 0;
    plug->frameInExpectation[1] = 0;
    plug->frameInExpectation[2] = 0;
    plug->frameInExpectation[3] = 0;
    plug->runEnMask = 0;
    plug->plgSgbmOnToRun = 0;
}
