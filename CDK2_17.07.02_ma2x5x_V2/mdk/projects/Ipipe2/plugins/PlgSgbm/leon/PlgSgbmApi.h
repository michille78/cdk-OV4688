/**************************************************************************************************

 @File         : PlgSgbmApi.c
 @Author       : MT
 @Brief        : Contain  Sgbm algorithm plug-in interface
 Date          : 01 - March - 2015
 E-mail        : xxx.xxx@movidius.com
 Copyright     : © Movidius Srl 2014, © Movidius Ltd 2015

 Description :
    This plug-in apply gauss 5x5 down-scale 2x filter on a luma image
    Support just 1 plugin instance. Not posible to run on other shaves. Resolution is hardcoded.
    Resources used:
         Leon RT.
         2 shaves 0 and 2, 4 slices, from 0 to 4.
         CmxDma controlled by Shaves.
         Resolution is fixed, 640x480
    Interrupt base.

 **************************************************************************************************/
#ifndef __PlgSgbm_API__
#define __PlgSgbm_API__

/**************************************************************************************************
 ~~~ Included types first then APIs from other modules
 **************************************************************************************************/
#include "PlgTypes.h"
#include "DrvCmxDma.h"

/**************************************************************************************************
 ~~~  Specific #defines
 **************************************************************************************************/


/**************************************************************************************************
 ~~~  Basic typedefs
 **************************************************************************************************/
typedef struct PlgSgbmStruct {
    PlgType plg;
    volatile int32_t        crtStatus; // internal usage
    FrameProducedCB         cbList[4];
    FramePool               *outputPools;
    icSize                  size;
    FrameT                  *frameInExpectation[4];
    FrameT                  *frameInProcessing[4];
    FrameT                  *oFrame;
    uint32_t                runEnMask;
    uint32_t                frameInProcMask;
    uint32_t                shvTotal;
    uint32_t                irqLevel;
    uint32_t                shvRunNr;
    uint32_t                shvRunMask;
    int32_t                 crtRunNr;
    //
    uint32_t                plgSgbmOnToRun;
}PlgSgbm;

/**************************************************************************************************
 ~~~  Exported Functions
 **************************************************************************************************/
void PlgSgbmCreate   (void *pluginObject);
// When 1 of the input source get stopped, need to call this function in order to delete history
// saved pointers the the input frames
void PlgSgbmStopSrc(void *pluginObject);

// in order to allow outside of irq start plugin
void plgSgbmTryRun(void *pluginObject);
#endif //__PlgSgbm_API__
