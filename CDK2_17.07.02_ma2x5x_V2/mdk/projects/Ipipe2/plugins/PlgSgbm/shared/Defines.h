#ifndef __DEFINES_SPARSE_H__
#define __DEFINES_SPARSE_H__

#ifndef DISP
#define DISP                    128
#endif

// Application configuration
#define SHAVES_USED             2
#define SGBM_NUMBER_OF_PATHS    3
#define MAX_PATCHES             SHAVES_USED

#define PYR_LEVELS              2

// level 1: VGA
#define MAX_WIDTH               640
#define MAX_HEIGHT              480

#define MAX_DISPARITIES_LEVEL1          DISP
#define PATCH_WIDTH_LEVEL1              390
#define PATCH_HEIGHT_LEVEL1             480
#define CENSUS_W_OVERLAP_LEVEL1         (2*CENSUS_KERNEL_SIZE)
#define CENSUS_H_OVERLAP_LEVEL1         0
#define PATCH_W_OVERLAP_LEVEL1          (PATCH_WIDTH_LEVEL1 * 2 - MAX_WIDTH)
#define PATCH_0_WIDTH_LEVEL1_TO_KEEP    (PATCH_WIDTH_LEVEL1-CENSUS_KERNEL_SIZE)
#define PATCH_1_WIDTH_LEVEL1_TO_KEEP    (MAX_WIDTH-PATCH_0_WIDTH_LEVEL1_TO_KEEP)

// level 0: QVGA
#define SCALE_FACTOR                2
#define DOWNSAMPLED_WIDTH           (MAX_WIDTH/SCALE_FACTOR)
#define DOWNSAMPLED_HEIGHT          (MAX_HEIGHT/SCALE_FACTOR)

#define MAX_DISPARITIES_LEVEL0      (DISP/SCALE_FACTOR)
#define PATCH_WIDTH_LEVEL0          195
#define PATCH_HEIGHT_LEVEL0         240
#define CENSUS_W_OVERLAP_LEVEL0     (2*CENSUS_KERNEL_SIZE)
#define CENSUS_H_OVERLAP_LEVEL0     0
#define PATCH_W_OVERLAP_LEVEL0      (PATCH_WIDTH_LEVEL0 * 2 - DOWNSAMPLED_WIDTH)

#define PREVIOUS_AGG_BUF_SIZE       ((SGBM_NUMBER_OF_PATHS-1) * MAX_PATCH_WIDTH * (MAX_DISPARITIES_LEVEL0) + (SGBM_NUMBER_OF_PATHS-1) * (MAX_DISPARITIES_LEVEL0))
#define CURRENT_AGG_BUF_SIZE        (SGBM_NUMBER_OF_PATHS * MAX_PATCH_WIDTH * (MAX_DISPARITIES_LEVEL0) + SGBM_NUMBER_OF_PATHS * (MAX_DISPARITIES_LEVEL0))

// defines for shave buffers dimension
#define MAX_PATCH_WIDTH             PATCH_WIDTH_LEVEL1
#define MAX_PATCH_HEIGHT            PATCH_HEIGHT_LEVEL1

// Unit test CRC for VGA static images from Middleburry dataset (Input/inputFrameLeft_640x480.bin and Input/inputFrameRight_640x480.bin)
#define GOLDEN_CRC_PC_D_64          1316942046
#define GOLDEN_CRC_PC_D_128         3280220337
// due to fp16 operations in computeCombined32_asm, asm results differ with +/-1 then C; these +/-1 differences propagate to SGBM module and become larger differences
#define GOLDEN_CRC_M2_D_64          160539776
#define GOLDEN_CRC_M2_D_128         2872373864

// Padding
#define MAX_PADDING_8                   7   // maximum padding needed to align each line to 8 bytes
#define MAX_PADDING_16                  15  // maximum padding needed to align each line to 16 bytes
#define BASE_BUFFER_DISPARITIES_SIZE    ( (MAX_WIDTH+MAX_PADDING_8)*MEDIAN_KERNEL_SIZE )

// PC - Myriad2 defines
#ifdef __PC__
 #define ALIGNED(x)
 #define SHAVE_HALT
#else
 #define ALIGNED(x) __attribute__((aligned(x)))
#endif

#endif
