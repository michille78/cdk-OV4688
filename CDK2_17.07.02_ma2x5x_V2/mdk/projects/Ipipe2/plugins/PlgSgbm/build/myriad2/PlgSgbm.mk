ifeq ($(LEON_RT_BUILD),yes)
	PlgSgbm_OBJECTS=RAWDATAOBJECTFILES
else
	PlgSgbm_OBJECTS=RAWDATAOBJECTFILES
endif

#if build app using shave sources
#MVCV_KERNELS_USED = censusTransform5x5 censusMatching64 censusMatching32 censusMatchingPyr \
#					censusMatchingPyrOnePos censusMin64 censusMin32 censusMin7 censusMin3 \
#					computeCombinedCost64 computeCombinedCost32 computeCombinedCost3 \
#					computeADPyrOnePos computeAD64 computeAD32 minKernel64 minKernel32 \
#					aggregateCostSGBM64_clamp aggregateCostSGBM32_clamp aggregateThreePaths64 \
#					aggregateThreePaths32

#MVCV_KERNELS_PATH_UP =  $(MV_COMMON_BASE)/components/kernelLib/MvCV/kernels
#MVCV_KERNELS_PATH_01 += $(patsubst %, $(MVCV_KERNELS_PATH_UP)/%/shave/src, $(MVCV_KERNELS_USED))
#MVCV_KERNELS_PATH_02 += $(patsubst %, $(MVCV_KERNELS_PATH_UP)/%/arch/ma2x5x/shave/src, $(MVCV_KERNELS_USED))


#SHAVE_ASM_SOURCES_MVCV_PlgSgbmShv += $(foreach var, $(MVCV_KERNELS_PATH_01),$(wildcard $(var)/*.asm))
#SHAVE_ASM_SOURCES_MVCV_PlgSgbmShv += $(foreach var, $(MVCV_KERNELS_PATH_02),$(wildcard $(var)/*.asm))
#SHAVE_ASM_SOURCES_MVCV_PlgSgbmShv += $(wildcard $(MVCV_KERNELS_PATH_01)/*.asm)
#SHAVE_ASM_SOURCES_MVCV_PlgSgbmShv += $(wildcard $(MVCV_KERNELS_PATH_02)/*.asm)


#MVCV_KERNELS_PATH_INCLUDE = $(patsubst %, $(MVCV_KERNELS_PATH_UP)/%/shave/include, $(MVCV_KERNELS_USED))
#MVCV_KERNELS_PATH_INCLUDE +=  $(MVCV_KERNELS_PATH_UP)/../include
#MVCV_KERNELS_PATH_INCLUDE += $(IPIPE_BASE)/plugins/PlgSgbm/shared

#MVCV_KERNELS_INCLUDE += $(patsubst %,-I %, $(MVCV_KERNELS_PATH_INCLUDE))

#MVCCOPT += $(MVCV_KERNELS_INCLUDE)
#endif

APPNAME_SGBM = DisparityMap
SHAVE_APP_LIBS = $(APPNAME_SGBM).mvlib
SHAVE0_APPS = $(APPNAME_SGBM).shv0lib
SHAVE2_APPS = $(APPNAME_SGBM).shv2lib

#if build app using shave sources
#SHAVE_C_SOURCES_PlgSgbmShv = $(wildcard $(IPIPE_BASE)/plugins/PlgSgbm/shave/*.cpp)
#SHAVE_ASM_SOURCES_PlgSgbmShv = $(wildcard $(IPIPE_BASE)/plugins/PlgSgbm/shave/*.asm)
#SHAVE_GENASMS_PlgSgbmShv = $(patsubst %.cpp,$(DirAppObjBase)%.asmgen,$(SHAVE_C_SOURCES_PlgSgbmShv))
#endif

SHAVE_PlgSgbmShv_OBJS = $(patsubst %.asm,$(DirAppObjBase)%_shave.o,$(SHAVE_ASM_SOURCES_PlgSgbmShv)) \
						$(patsubst %.asm,$(DirAppObjBase)%_shave.o,$(SHAVE_ASM_SOURCES_MVCV_PlgSgbmShv)) \
                   $(patsubst $(DirAppObjBase)%.asmgen,$(DirAppObjBase)%_shave.o,$(SHAVE_GENASMS_PlgSgbmShv))
                   
                   
                   
PROJECTCLEAN += $(SHAVE_PlgSgbmShv_OBJS) $(SHAVE_GENASMS_PlgSgbmShv)
PROJECTINTERM += $(SHAVE_GENASMS_PlgSgbmShv)
                   
