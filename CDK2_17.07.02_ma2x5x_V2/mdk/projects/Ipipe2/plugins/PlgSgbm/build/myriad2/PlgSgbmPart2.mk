ifeq ($(LEON_RT_BUILD),yes)
	PlgSgbm_PREFIX=lrt_
else
	PlgSgbm_PREFIX=
endif
# APP mvlib
MVASMOPT := $(filter-out -noSPrefixing,$(MVASMOPT))

ENTRYPOINTS += -e disparityMapRunFrame --gc-sections

#if build app using shave sources
#$(APPNAME_SGBM).mvlib : $(SHAVE_PlgSgbmShv_OBJS) $(PROJECT_SHAVE_LIBS)
#	$(ECHO) $(LD)  $(MVLIBOPT) $(ENTRYPOINTS) $(SHAVE_PlgSgbmShv_OBJS) $(PROJECT_SHAVE_LIBS) $(CompilerANSILibs) -o $@
#else - build app using shave libs
$(APPNAME_SGBM).shv0lib : 
	$(shell cp $(IPIPE_BASE)/plugins/PlgSgbm/binary/shaveLibs/$(APPNAME_SGBM).shv0lib $(APPNAME_SGBM).shv0lib )

$(APPNAME_SGBM).shv2lib : 
	$(shell cp $(IPIPE_BASE)/plugins/PlgSgbm/binary/shaveLibs/$(APPNAME_SGBM).shv2lib $(APPNAME_SGBM).shv2lib )
#endif

ifeq ($(DISP), 64)
    MVCCOPT   += -D"DISP=64"
	CCOPT_LRT   += -D"DISP=64"
else ifeq ($(DISP), 128)
    MVCCOPT   += -D"DISP=128"
	CCOPT_LRT   += -D"DISP=128"
endif