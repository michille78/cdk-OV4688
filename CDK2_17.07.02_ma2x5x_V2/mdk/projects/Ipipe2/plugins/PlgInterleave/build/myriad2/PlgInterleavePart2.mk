ifeq ($(LEON_RT_BUILD),yes)
	PLGINTERLEAVE_PREFIX=lrt_
else
	PLGINTERLEAVE_PREFIX=
endif
#ENTRYPOINTS = -e runTask  --gc-sections
$(PlgInterleaveShv).mvlib : $(SHAVE_PlgInterleaveShv_OBJS) $(PROJECT_SHAVE_LIBS)
	$(ECHO) $(LD)  $(MVLIBOPT) $(SHAVE_PlgInterleaveShv_OBJS) $(PROJECT_SHAVE_LIBS) $(CompilerANSILibs) -o $@
#This creates a binary file packing the shvdlib file
$(PlgInterleaveShv)_bin.o : $(PlgInterleaveShv).shvdlib
	$(ECHO) $(OBJCOPY)  -I binary --rename-section .data=.data.plgShvImageAddr \
	--redefine-sym  _binary_$(subst /,_,$(subst .,_,$<))_start=$(PLGINTERLEAVE_PREFIX)plgShvImageAddr \
	-O elf32-sparc -B sparc $< $@

$(PlgInterleaveShv)_sym.o : $(PlgInterleaveShv).shvdcomplete
	echo BBBB $(PlgInterleaveShv)_sym.o
	$(ECHO) $(OBJCOPY) --prefix-symbols=$(PLGINTERLEAVE_PREFIX)PlgInterleaveShv_ --extract-symbol $< $@
