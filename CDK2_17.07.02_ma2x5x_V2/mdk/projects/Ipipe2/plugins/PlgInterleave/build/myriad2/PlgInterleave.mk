ifeq ($(LEON_RT_BUILD),yes)
	PLGINTERLEAVE_OBJECTS=RAWDATAOBJECTFILES
    CCOPT_LRT += -I $(IPIPE_BASE)/plugins/PlgInterleave/shared
else
	PLGINTERLEAVE_OBJECTS=RAWDATAOBJECTFILES
    CCOPT += -I $(IPIPE_BASE)/plugins/PlgInterleave/shared
endif

MVCCOPT += -I $(IPIPE_BASE)/plugins/PlgInterleave/shared
PLGINTERLEAVE_LEON_C_SOURCES += $(IPIPE_BASE)/plugins/PlgInterleave/leon/PlgInterleave.c

PlgInterleaveShv = PlgInterleaveShv
SHAVE_C_SOURCES_PlgInterleaveShv = $(wildcard $(IPIPE_BASE)/plugins/PlgInterleave/shave/*.c)
SHAVE_ASM_SOURCES_PlgInterleaveShv = $(wildcard $(IPIPE_BASE)/plugins/PlgInterleave/shave/*.asm)
SHAVE_GENASMS_PlgInterleaveShv = $(patsubst %.c,$(DirAppObjBase)%.asmgen,$(SHAVE_C_SOURCES_PlgInterleaveShv))
SHAVE_PlgInterleaveShv_OBJS = $(patsubst %.asm,$(DirAppObjBase)%_shave.o,$(SHAVE_ASM_SOURCES_PlgInterleaveShv)) \
                   $(patsubst $(DirAppObjBase)%.asmgen,$(DirAppObjBase)%_shave.o,$(SHAVE_GENASMS_PlgInterleaveShv))
$(PLGINTERLEAVE_OBJECTS)  += $(PlgInterleaveShv)_sym.o $(PlgInterleaveShv)_bin.o     
PROJECTCLEAN += $(SHAVE_GENASMS_PlgInterleaveShv) $(SHAVE_PlgInterleaveShv_OBJS) $(PlgInterleaveShv).mvlib        
PROJECTINTERM += $(SHAVE_GENASMS_PlgInterleaveShv)    
PROJECTCLEAN += $(PlgInterleaveShv).shvdlib $(PlgInterleaveShv).map
PROJECTCLEAN += $(PlgInterleaveShv).shvdcomplete $(PlgInterleaveShv)_sym.o   
