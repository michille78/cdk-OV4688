/**************************************************************************************************

 @File         : PlgInterleaveSharedDefines.h
 @Author       : csoka
 @Brief        : header for defines shared between shave and leon
 Copyright     : © Movidius Srl 2014, © Movidius Ltd 2015

  **************************************************************************************************/


#ifndef __PLG_INTERLEAVE_SHARED_DEFINES_H__
#define __PLG_INTERLEAVE_SHARED_DEFINES_H__

/**************************************************************************************************
 ~~~  Specific #defines
 **************************************************************************************************/
#define ALIGNED(x) __attribute__((aligned(x)))

#define SHV_MAX_LINE_W          (2016)
#define SHV_NR_OF_LINES_IN_BUF  (4)
#define SHV_LINEBUFFS_IN_Y_SZ   (SHV_MAX_LINE_W * SHV_NR_OF_LINES_IN_BUF)
#define SHV_LINEBUFFS_IN_UV_SZ  (SHV_LINEBUFFS_IN_Y_SZ >> 1)
#define SHV_LINEBUFFS_OUT_SZ    (SHV_LINEBUFFS_IN_Y_SZ << 1)
#define SHV_NR_BUFFS            (2)

#endif //__PLG_INTERLEAVE_SHARED_DEFINES_H__
