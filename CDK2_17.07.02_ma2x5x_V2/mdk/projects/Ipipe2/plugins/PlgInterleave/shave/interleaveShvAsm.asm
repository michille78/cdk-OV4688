; ///
; /// @file
; /// @copyright All code copyright Movidius Ltd 2013, all rights reserved.
; ///            For License Warranty see: common/license.txt
; ///
; /// @brief
; ///

.version 00.50.05
.data .rodata.interleave
.align 16

.code .text.interleave ;text
.align 16
;==================================================================================================================
;====================================INTERLEAVE ================= END ========================
;==================================================================================================================
;void yuv422Pto422Iconversion(uint8_t *out, uint8_t *inY, uint8_t *inU, uint8_t *inV, uint32_t width) {
;    uint32_t i;
;    uint32_t i_o   = 0;
;    uint32_t i_y   = 0;
;    uint32_t i_uv  = 0;
;    for(i=0; i < w; i++)
;    {
;        out[i_o++] = inY[i_y++];
;        out[i_o++] = inU[i_uv];
;        out[i_o++] = inY[i_y++];
;        out[i_o++] = inV[i_uv++];
;    }
;}
yuv422Pto422Iconversion_asm:
lsu0.ldil i8, yuv422Pto422Iconversion_Loop   ||lsu1.ldih i8, yuv422Pto422Iconversion_Loop
iau.incs i14, 31
iau.shr.u32 i7, i14, 5
LSU0.LDO.64.L v4,  i16, 0x00 || LSU1.LDO.64.h v4,  i16, 0x08
LSU0.LDO.64.L v5,  i16, 0x00 || LSU1.LDO.64.h v5,  i16, 0x08 || iau.incs i16, 0x20
LSU0.LDO.64.L v6,  i15, 0x00 || LSU1.LDO.64.h v6,  i15, 0x08
LSU0.LDO.64.L v7,  i15, 0x00 || LSU1.LDO.64.h v7,  i15, 0x08 || iau.incs i15, 0x20
LSU0.LDO.64.L v0,  i17, 0x00 || LSU1.LDO.64.h v0,  i17, 0x08
LSU0.LDO.64.L v1,  i17, 0x10 || LSU1.LDO.64.h v1,  i17, 0x18
LSU0.LDO.64.L v2,  i17, 0x20 || LSU1.LDO.64.h v2,  i17, 0x28
LSU0.LDO.64.L v3,  i17, 0x30 || LSU1.LDO.64.h v3,  i17, 0x38 || iau.incs i17, 0x40
.lalign
bru.rpl i8, i7, yuv422Pto422Iconversion_DelayLoop
LSU0.LDO.64.L v4,  i16, 0x00 || LSU1.LDO.64.h v4,  i16, 0x08
LSU0.LDO.64.L v5,  i16, 0x00 || LSU1.LDO.64.h v5,  i16, 0x08 || iau.incs i16, 0x20
LSU0.LDO.64.L v6,  i15, 0x00 || LSU1.LDO.64.h v6,  i15, 0x08                        ||cmu.vilv.x8 v9, v10,  v6  v4
LSU0.LDO.64.L v7,  i15, 0x00 || LSU1.LDO.64.h v7,  i15, 0x08 || iau.incs i15, 0x20  ||cmu.vilv.x8 v11, v12, v7  v5
LSU0.LDO.64.L v0,  i17, 0x00 || LSU1.LDO.64.h v0,  i17, 0x08                        ||cmu.vilv.x8 v13, v14, v9    v0
LSU0.LDO.64.L v1,  i17, 0x10 || LSU1.LDO.64.h v1,  i17, 0x18                        ||cmu.vilv.x8 v15, v16, v10   v1
LSU0.LDO.64.L v2,  i17, 0x20 || LSU1.LDO.64.h v2,  i17, 0x28                        ||cmu.vilv.x8 v17, v18, v11   v2
LSU0.LDO.64.L v3,  i17, 0x30 || LSU1.LDO.64.h v3,  i17, 0x38 || iau.incs i17, 0x40  ||cmu.vilv.x8 v19, v20, v12   v3
LSU0.STO.64.L v13,  i18, 0x00 || LSU1.STO.64.h v13,  i18, 0x08
yuv422Pto422Iconversion_Loop:
LSU0.STO.64.L v14,  i18, 0x10 || LSU1.STO.64.h v14,  i18, 0x18
LSU0.STO.64.L v15,  i18, 0x20 || LSU1.STO.64.h v15,  i18, 0x28
LSU0.STO.64.L v16,  i18, 0x30 || LSU1.STO.64.h v16,  i18, 0x38
LSU0.STO.64.L v17,  i18, 0x40 || LSU1.STO.64.h v17,  i18, 0x48
LSU0.STO.64.L v18,  i18, 0x50 || LSU1.STO.64.h v18,  i18, 0x58
LSU0.STO.64.L v19,  i18, 0x60 || LSU1.STO.64.h v19,  i18, 0x68
LSU0.STO.64.L v20,  i18, 0x70 || LSU1.STO.64.h v20,  i18, 0x78 || iau.incs i18, 0x80
yuv422Pto422Iconversion_DelayLoop:

BRU.JMP i30
nop 6
nop

.end
