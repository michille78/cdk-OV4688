/**************************************************************************************************

 @File         : PlgInterleave.c
 @Author       : csoka
 @Brief        : plugin to interleave YUV422 planar data
 Copyright     : © Movidius Srl 2014, © Movidius Ltd 2015
 **************************************************************************************************/

/**************************************************************************************************
 ~~~ Included types first then APIs from other modules
 ***************************************************************************************************/
#include "PlgInterleaveApi.h"
#include "PlgInterleaveSharedDefines.h"
#include "FrameMgrApi.h"
#include <DrvTimer.h>
#include "swcLeonUtils.h"
#include <string.h>
#include "assert.h"
#include "DrvCmxDma.h"
#include "DrvSvu.h"
#include "TimeSyncMgr.h"

/**************************************************************************************************
 ~~~  Specific #defines
 **************************************************************************************************/
enum {
    PLGSAMPLE_STATS_OFF = 0,
    PLGSAMPLE_STATS_ON  = 1
};

#define MAX_NR_OF_FRM_PENDING 3

extern u32 plgShvImageAddr[];
extern u32 PlgInterleaveShv_runTask;
extern u32 PlgInterleaveShv_shvBufY;
extern u32 PlgInterleaveShv_shvBufU;
extern u32 PlgInterleaveShv_shvBufV;
extern u32 PlgInterleaveShv_shvBufO;

/**************************************************************************************************
 ~~~  Basic project types definition
 **************************************************************************************************/
typedef struct {
    FrameT              *buffer;
    PlgInterleave       *plug;
}PlgInterQueElement;

typedef struct {
    PlgInterQueElement  queue[MAX_NR_OF_FRM_PENDING];
    int32_t queueIn;
    int32_t queueOut;
} PlgInterQue;

/**************************************************************************************************
 ~~~  Local variables
 **************************************************************************************************/
static volatile PlgStatus PlgInterleave_global_status;  /// To enable multiple instances use same svu
static volatile uint32_t  PlgInterleave_svu_loaded = 0; /// Shave load flag for multi instance
static volatile PlgInterleave *previousPlg[2] = {(PlgInterleave *)0x1, (PlgInterleave *)0x2};
uint64_t interProcTime;
uint64_t interProcTimeStart;
static PlgInterQue     plgInterQue;
/**************************************************************************************************
 ~~~ Local File function declarations
 **************************************************************************************************/

static void     producedInputFrame(FrameT *frame, void *pluginObject);
static int32_t  init(FramePool *outputPools, int nOutputPools, void *pluginObject);
static int32_t  fini(void *pluginObject);

static void runShvLines(PlgInterleave *plug);
static void shvIrqHandler(uint32_t source);

static void runDmaInTask(PlgInterleave *plug);
static void runDmaInOutTask(PlgInterleave *plug);
static void runDmaOutTask(PlgInterleave *plug);
static void dmaHndlIrq(dmaTransactionList* ListPtr, void* userContext);

static void        runFrame(PlgInterleave *plug);
static inline void eofFrameHandler(PlgInterleave *plug);


static int queuePut(FrameT *buffer, PlgInterleave *plug);
static int queueGet(PlgInterQueElement **old);
/**************************************************************************************************
 ~~~ Exported Functions Implementation
 **************************************************************************************************/
void PlgInterleaveCreate(void *pluginObject) {
    PlgInterleave *object = (PlgInterleave*)pluginObject;
// init hw things, or all the init side that not need params,
    object->plg.init       = init;
    object->plg.fini       = fini;
    object->init           = (void*)PlgInterleaveSetParams;
    object->cbList[0].callback     = producedInputFrame;
    object->cbList[0].pluginObj    = pluginObject;
    object->plg.callbacks  = object->cbList;
    PlgInterleave_global_status    = PLG_STATS_IDLE;
    object->crtStatus      = PLGSAMPLE_STATS_OFF;
    DrvCmxDmaInitDefault();
}

//
static int32_t  fini(void *pluginObject) {
    PlgInterleave *object = (PlgInterleave*)pluginObject;
    object->crtStatus      = PLGSAMPLE_STATS_OFF;
    return 0;
}

//
static int32_t init(FramePool *outputPools, int nOutputPools, void *pluginObject) {
    PlgInterleave* object = (PlgInterleave*)pluginObject;
    object->outputPools = outputPools;
    object->crtStatus      = PLGSAMPLE_STATS_ON;
    return 0;
}

//
void PlgInterleaveSetParams(void *pluginObject, uint32_t shvId)
{
    PlgInterleave *plug = (PlgInterleave*)pluginObject;
    plug->shvId = shvId;
    plug->irqLevel = PLGINTERLEAVE_IRQ_LEVEL;

    plug->dmaRunNr = 0;

    /// Load shave only once (even if multi-instance)
    if(PlgInterleave_svu_loaded == 0)
    {
        swcResetShave(shvId);
        swcSetAbsoluteDefaultStack(shvId);
        swcSetShaveWindowsToDefault(shvId);
        swcLoadshvdlib((u8*)plgShvImageAddr, shvId);
        SET_REG_WORD(DCU_ICR(shvId), ICR_SWI_ENABLE);
        DrvIcbSetupIrq(IRQ_SVE_0 + shvId, plug->irqLevel, POS_EDGE_INT, shvIrqHandler);
        PlgInterleave_svu_loaded++;
    }

    uint8_t *iBuffYAddr = (uint8_t*)swcSolveShaveRelAddrAHB((uint32_t)&PlgInterleaveShv_shvBufY, shvId);
    uint8_t *iBuffUAddr = (uint8_t*)swcSolveShaveRelAddrAHB((uint32_t)&PlgInterleaveShv_shvBufU, shvId);
    uint8_t *iBuffVAddr = (uint8_t*)swcSolveShaveRelAddrAHB((uint32_t)&PlgInterleaveShv_shvBufV, shvId);
    uint8_t *oBuffAddr  = (uint8_t*)swcSolveShaveRelAddrAHB((uint32_t)&PlgInterleaveShv_shvBufO, shvId);
    plug->shaveTaskDesc.inShvY[0] = iBuffYAddr;
    plug->shaveTaskDesc.inShvU[0] = iBuffUAddr;
    plug->shaveTaskDesc.inShvV[0] = iBuffVAddr;
    plug->shaveTaskDesc.outShv[0] = oBuffAddr;
    plug->shaveTaskDesc.inShvY[1] = iBuffYAddr + SHV_LINEBUFFS_IN_Y_SZ;
    plug->shaveTaskDesc.inShvU[1] = iBuffUAddr + SHV_LINEBUFFS_IN_UV_SZ;
    plug->shaveTaskDesc.inShvV[1] = iBuffVAddr + SHV_LINEBUFFS_IN_UV_SZ;
    plug->shaveTaskDesc.outShv[1] = oBuffAddr  + SHV_LINEBUFFS_OUT_SZ;

    plug->dmaId  = DrvCmxDmaInitRequester(3);
}


static void RunInputFrame(FrameT *frame, PlgInterleave *plug) {
    plug->size.w = frame->stride[0];
    plug->size.h = frame->height[0];
    plug->dmaTotalRunsNecessary = plug->size.h / SHV_NR_OF_LINES_IN_BUF;

    // fini suport, assuming that finish is not possible to stop immediately
    if(PLGSAMPLE_STATS_ON == plug->crtStatus)
    {
        PlgInterleave_global_status = PLG_STATS_RUNNING;

        FrameT *oFrame = FrameMgrAcquireFrame(plug->outputPools);
        // skip frame if no more output buffer available
        if (NULL == oFrame)
        {
            // release input frame and exit, frame will be dropped
            FrameMgrReleaseFrame(frame);
            PlgInterleave_global_status = PLG_STATS_IDLE;
            return;
        }

        if (plug->procesStart) {
            plug->procesStart(oFrame->seqNo);
        }

        plug->oFrame = oFrame;
        plug->frameInProcessing = frame;

        uint32_t chroma_plane_sz_bytes = (plug->size.w * plug->size.h) >> 1; /// for 422
        uint8_t *inputY = (uint8_t*)plug->frameInProcessing->fbPtr[0];
        uint8_t *inputU = (uint8_t*)plug->frameInProcessing->fbPtr[1];
        uint8_t *inputV = (uint8_t*)(plug->frameInProcessing->fbPtr[1] + chroma_plane_sz_bytes);
        uint8_t *output = (uint8_t*)plug->oFrame->fbPtr[0];

        plug->shaveTaskDesc.inY = inputY;
        plug->shaveTaskDesc.inU = inputU;
        plug->shaveTaskDesc.inV = inputV;
        plug->shaveTaskDesc.out = output;

        // run the shave code
        plug->dmaRunNr = 0;
        plug->shvRunNr = 0;
        interProcTimeStart = TimeSyncMsgGetTimeUs();
        runFrame(plug);
        //eofFrameHandler(plug);
    }
}

static void producedInputFrame(FrameT *frame, void *pluginObject)
{
    PlgInterleave* plug        = (PlgInterleave*)pluginObject;
    FrameT *frameX;
    assert(0 == queuePut(frame, plug)); // max que size is 3. If is not enough fast will crash
    /// Check if plugin finished previous frame
    if((PLG_STATS_IDLE != PlgInterleave_global_status))
    {
        //task is in gue, not run for now as plugin running another frame
        return;
    }
    else
    {
        PlgInterQueElement *taskDescriptor;
        if(0 == queueGet(&taskDescriptor)) { // there is a new element in queue
            plug = taskDescriptor->plug;
            frameX = taskDescriptor->buffer;
            RunInputFrame(frameX, plug);
        }
        else {
            // a new frame come and is not anymore in the que, this is not normal
            assert(0);
        }
    }
}

//
static void runFrame(PlgInterleave *plug) {
    plug->runMask = 0;
    if(0 == plug->dmaRunNr) {
        plug->runMask = 1;
        runDmaInTask(plug);
        return;
    }
    if(1 == plug->dmaRunNr) {
        runDmaInTask(plug);
        runShvLines(plug);
        return;
    }
    if((plug->dmaTotalRunsNecessary+0) == plug->dmaRunNr) {
        runDmaOutTask(plug);
        runShvLines(plug);
        return;
    }
    if((plug->dmaTotalRunsNecessary + 1) == plug->dmaRunNr) {
        plug->runMask = 1;
        runDmaOutTask(plug);
        return;
    }
    if((plug->dmaTotalRunsNecessary + 2) == plug->dmaRunNr) {
        eofFrameHandler(plug);
        return;
    }
    runDmaInOutTask(plug);
    runShvLines(plug);
    return;
}


//////////// Shaves and DMAs //////////////////////
static void runShvLines(PlgInterleave *plug)
{
    uint32_t shv =  plug->shvId;

    // Set STOP bit in control register
    SET_REG_WORD(SVU_CTRL_ADDR[shv] + SLC_OFFSET_SVU + SVU_OCR, OCR_STOP_GO);
    swcSetAbsoluteDefaultStack(shv);
    //
    SET_REG_WORD(SVU_CTRL_ADDR[shv]+SLC_OFFSET_SVU+IRF_BASE+(18<<2), ((u32)plug));
    SET_REG_WORD(SVU_CTRL_ADDR[shv]+SLC_OFFSET_SVU+IRF_BASE+(17<<2), ((u32)plug->shvRunNr&1));
    SET_REG_WORD(SVU_CTRL_ADDR[shv]+SLC_OFFSET_SVU+IRF_BASE+(16<<2), ((u32)plug->size.w));
    // Enable SWI interrupt
    SET_REG_WORD(SVU_CTRL_ADDR[shv] + SLC_OFFSET_SVU + SVU_IRR, 0xFF);
    SET_REG_WORD(SVU_CTRL_ADDR[shv] + SLC_OFFSET_SVU + SVU_ICR, 0x20);
    // set IP
    SET_REG_WORD(SVU_CTRL_ADDR[shv]+SLC_OFFSET_SVU+SVU_PTR, (u32)&PlgInterleaveShv_runTask);
    //Start shave
    SET_REG_WORD(SVU_CTRL_ADDR[shv]+SLC_OFFSET_SVU+SVU_OCR, 0);
}

static void dmaHndlIrq(dmaTransactionList* ListPtr, void* userContext)
{
    PlgInterleave *plug = (PlgInterleave*)userContext;
    plug->dmaRunNr++;
    plug->runMask++;
    if(2 == plug->runMask)
    {
        runFrame(plug);
    }
}

static void shvIrqHandler(uint32_t source)
{
    PlgInterleave *plug = (PlgInterleave*)GET_REG_WORD_VAL(SVU_CTRL_ADDR[source - IRQ_SVE_0]+SLC_OFFSET_SVU+IRF_BASE+(18<<2));
    DrvIcbIrqClear(source);
    plug->shvRunNr++;
    plug->runMask++;
    if(2 == plug->runMask)
    {
        runFrame(plug);
    }
}

//
static inline void eofFrameHandler(PlgInterleave *plug)
{
    PlgInterleave *plugN;
    FrameT        *frameN;
    FrameMgrReleaseFrame(plug->frameInProcessing);
    plug->frameInProcessing = NULL;
    plug->oFrame->type = FRAME_T_FORMAT_422I;
    FrameMgrProduceFrame(plug->oFrame);
    PlgInterleave_global_status = PLG_STATS_IDLE;
    uint64_t tt = TimeSyncMsgGetTimeUs();
    interProcTime = tt - interProcTimeStart;
    PlgInterQueElement *taskDescriptor;
    if(0 == queueGet(&taskDescriptor)) { // there is a new element in queue
        plugN = taskDescriptor->plug;
        frameN = taskDescriptor->buffer;
        RunInputFrame(frameN, plugN);
    }

}

// dma side
static void runDmaInTask(PlgInterleave *plug)
{
    uint32_t uv_line_sz = plug->size.w >> 1; /// 422 Chromas

    /// DMA-In Y
    plug->dmaRef[0] = DrvCmxDmaCreateTransactionFullOptions(
                plug->dmaId,
                &plug->dmaTask[0],
                plug->shaveTaskDesc.inY,
                plug->shaveTaskDesc.inShvY[plug->dmaRunNr&1],
                plug->size.w * SHV_NR_OF_LINES_IN_BUF,
                plug->size.w,
                plug->size.w,
                plug->size.w,
                plug->size.w );
    plug->shaveTaskDesc.inY += plug->size.w * SHV_NR_OF_LINES_IN_BUF;
    /// DMA-In U
    plug->dmaRef[1] = DrvCmxDmaCreateTransactionFullOptions(
                plug->dmaId,
                &plug->dmaTask[1],
                plug->shaveTaskDesc.inU,
                plug->shaveTaskDesc.inShvU[plug->dmaRunNr&1],
                uv_line_sz * SHV_NR_OF_LINES_IN_BUF,
                uv_line_sz,
                uv_line_sz,
                uv_line_sz,
                uv_line_sz);
    plug->shaveTaskDesc.inU += uv_line_sz * SHV_NR_OF_LINES_IN_BUF;
    /// DMA-In V
    plug->dmaRef[2] = DrvCmxDmaCreateTransactionFullOptions(
                plug->dmaId,
                &plug->dmaTask[2],
                plug->shaveTaskDesc.inV,
                plug->shaveTaskDesc.inShvV[plug->dmaRunNr&1],
                uv_line_sz * SHV_NR_OF_LINES_IN_BUF,
                uv_line_sz,
                uv_line_sz,
                uv_line_sz,
                uv_line_sz);
    plug->shaveTaskDesc.inV += uv_line_sz * SHV_NR_OF_LINES_IN_BUF;

    DrvCmxDmaLinkTasks(plug->dmaRef[0], 2, plug->dmaRef[1], plug->dmaRef[2]);

    DrvCmxDmaStartTaskAsync(plug->dmaRef[0], dmaHndlIrq, (void*)plug);
}

// dma side
static void runDmaOutTask(PlgInterleave *plug)
{
    uint32_t output_size = plug->size.w << 1; /// Interleaved 422
    /// DMA-out YUYV
    plug->dmaRef[3] = DrvCmxDmaCreateTransactionFullOptions(
                plug->dmaId,
                &plug->dmaTask[3],
                plug->shaveTaskDesc.outShv[plug->dmaRunNr&1],
                plug->shaveTaskDesc.out,
                output_size * SHV_NR_OF_LINES_IN_BUF,
                output_size,
                output_size,
                output_size,
                output_size);
    plug->shaveTaskDesc.out += output_size * SHV_NR_OF_LINES_IN_BUF;

    DrvCmxDmaStartTaskAsync(plug->dmaRef[3], dmaHndlIrq, (void*)plug);
}

// dma side
static void runDmaInOutTask(PlgInterleave *plug)
{
    uint32_t uv_line_sz = plug->size.w >> 1; /// 422 Chromas
    uint32_t output_size = plug->size.w << 1; /// Interleaved 422

    /// DMA-In Y
    plug->dmaRef[0] = DrvCmxDmaCreateTransactionFullOptions(
                plug->dmaId,
                &plug->dmaTask[0],
                plug->shaveTaskDesc.inY,
                plug->shaveTaskDesc.inShvY[plug->dmaRunNr&1],
                plug->size.w * SHV_NR_OF_LINES_IN_BUF,
                plug->size.w,
                plug->size.w,
                plug->size.w,
                plug->size.w );
    plug->shaveTaskDesc.inY += plug->size.w * SHV_NR_OF_LINES_IN_BUF;
    /// DMA-In U
    plug->dmaRef[1] = DrvCmxDmaCreateTransactionFullOptions(
                plug->dmaId,
                &plug->dmaTask[1],
                plug->shaveTaskDesc.inU,
                plug->shaveTaskDesc.inShvU[plug->dmaRunNr&1],
                uv_line_sz * SHV_NR_OF_LINES_IN_BUF,
                uv_line_sz,
                uv_line_sz,
                uv_line_sz,
                uv_line_sz);
    plug->shaveTaskDesc.inU += uv_line_sz * SHV_NR_OF_LINES_IN_BUF;
    /// DMA-In V
    plug->dmaRef[2] = DrvCmxDmaCreateTransactionFullOptions(
                plug->dmaId,
                &plug->dmaTask[2],
                plug->shaveTaskDesc.inV,
                plug->shaveTaskDesc.inShvV[plug->dmaRunNr&1],
                uv_line_sz * SHV_NR_OF_LINES_IN_BUF,
                uv_line_sz,
                uv_line_sz,
                uv_line_sz,
                uv_line_sz);
    plug->shaveTaskDesc.inV += uv_line_sz * SHV_NR_OF_LINES_IN_BUF;
    /// DMA-out YUYV
    plug->dmaRef[3] = DrvCmxDmaCreateTransactionFullOptions(
                plug->dmaId,
                &plug->dmaTask[3],
                plug->shaveTaskDesc.outShv[plug->dmaRunNr&1],
                plug->shaveTaskDesc.out,
                output_size * SHV_NR_OF_LINES_IN_BUF,
                output_size,
                output_size,
                output_size,
                output_size);
    plug->shaveTaskDesc.out += output_size * SHV_NR_OF_LINES_IN_BUF;

    DrvCmxDmaLinkTasks(plug->dmaRef[0], 3, plug->dmaRef[1], plug->dmaRef[2], plug->dmaRef[3]);

    DrvCmxDmaStartTaskAsync(plug->dmaRef[0], dmaHndlIrq, (void*)plug);
}


//
static int queuePut(FrameT *buffer, PlgInterleave *plug) {
    if(plgInterQue.queueIn == (( plgInterQue.queueOut - 1 +
            MAX_NR_OF_FRM_PENDING) % MAX_NR_OF_FRM_PENDING)) {
        return -1; /* Queue Full*/
    }
    plgInterQue.queue[plgInterQue.queueIn].buffer           = buffer;
    plgInterQue.queue[plgInterQue.queueIn].plug             = plug;
    plgInterQue.queueIn = (plgInterQue.queueIn + 1) % MAX_NR_OF_FRM_PENDING;
    return 0; // No errors
}

//
static int queueGet(PlgInterQueElement **old) {
    if(plgInterQue.queueIn == plgInterQue.queueOut) {
        return -1; /* Queue Empty - nothing to get*/
    }
    *old = (PlgInterQue *)&plgInterQue.queue[plgInterQue.queueOut];
    plgInterQue.queueOut = (plgInterQue.queueOut + 1) % MAX_NR_OF_FRM_PENDING;
    return 0; // No errors
}
