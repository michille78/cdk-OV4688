/**************************************************************************************************

 @File         : PlgInterleaveApi.h
 @Author       : csoka
 @Brief        : API for yuv interleave ipipe plugin
 Copyright     : © Movidius Srl 2014, © Movidius Ltd 2015

 Description :
    Interleaves planar YUV422 data

 **************************************************************************************************/


#ifndef __PLG_INTERLEAVE_API_H__
#define __PLG_INTERLEAVE_API_H__


/**************************************************************************************************
 ~~~ Included types first then APIs from other modules
 ***************************************************************************************************/
#include "PlgTypes.h"
#include "DrvCmxDma.h"

/**************************************************************************************************
 ~~~  Specific #defines
 **************************************************************************************************/
#define PLGINTERLEAVE_IRQ_LEVEL             (3)
#define PLGINTERLEAVE_MAX_DMA_TASKS         (10)

/**************************************************************************************************
 ~~~  Basic typdefs
 **************************************************************************************************/
typedef struct {
    uint8_t *inY;
    uint8_t *inU;
    uint8_t *inV;
    uint8_t *out;
    uint8_t *inShvY[2];
    uint8_t *inShvU[2];
    uint8_t *inShvV[2];
    uint8_t *outShv[2];
}ShaveTaskDesc;

typedef struct PlgInterleaveStruct {
    PlgType plg;
    // specific component interface
    // specific plugin command function, init, configs ...
    void    (*init)          (icSize iframeSize, void *pluginObject);
    // callback possible to be linked to the plugin,
    void    (*procesStart)   (uint32_t seqNr);
    void    (*procesEnd)     (uint32_t seqNr);

    FrameT           *frameInProcessing;
    FrameT           *oFrame;

    icSize size;

    ShaveTaskDesc shaveTaskDesc;

    uint32_t                shvId;
    uint32_t                irqLevel;
    uint32_t                shvRunNr;

    dmaTransactionList_t    dmaTask[PLGINTERLEAVE_MAX_DMA_TASKS];
    dmaTransactionList_t    *dmaRef[PLGINTERLEAVE_MAX_DMA_TASKS];
    dmaRequesterId          dmaId;
    uint32_t                dmaRunNr;

    uint32_t                runMask;
    uint32_t                dmaTotalRunsNecessary;

    /// Private members. All data structures have to be internal
    FramePool               *outputPools;
    icSize                  privateCfg;
    volatile int32_t        crtStatus; // internal usage
    FrameProducedCB         cbList[1];
} PlgInterleave;


/**************************************************************************************************
 ~~~  Exported Functions
 **************************************************************************************************/
void PlgInterleaveCreate(void *pluginObject);
void PlgInterleaveSetParams(void *pluginObject, uint32_t shvId);

#endif //__PLG_INTERLEAVE_API_H__
