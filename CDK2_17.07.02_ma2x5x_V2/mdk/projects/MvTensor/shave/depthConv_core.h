///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Simple effect header
///

#ifndef _MVDEPTHCONV_CORE_H_
#define _MVDEPTHCONV_CORE_H_

// 1: Includes
// ----------------------------------------------------------------------------
#include <mvDepthConvParam.h>

// 2:  Exported Global Data (generally better to avoid)
// ----------------------------------------------------------------------------

// 3:  Exported Functions (non-inline)
// ----------------------------------------------------------------------------
#ifdef __cplusplus
extern "C"
{
#endif

void mvDepthConv(t_MvDepthConvParam *p);

#ifdef __cplusplus
}
#endif

#endif //__MVDEPTHCONV_CORE_H__
