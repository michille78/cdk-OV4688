///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Simple effect header
///

#ifndef _MVMAXPOOL3X3_CORE_H_
#define _MVMAXPOOL3X3_CORE_H_

// 1: Includes
// ----------------------------------------------------------------------------
#include <mvMaxPool3x3Param.h>

// 2:  Exported Global Data (generally better to avoid)
// ----------------------------------------------------------------------------

// 3:  Exported Functions (non-inline)
// ----------------------------------------------------------------------------
#ifdef __cplusplus
extern "C"
{
#endif

void mvMaxPool3x3(t_MvMaxPool3x3Param *p);

#ifdef __cplusplus
}
#endif

#endif//__MVMAXPOOL3X3_CORE_H__
