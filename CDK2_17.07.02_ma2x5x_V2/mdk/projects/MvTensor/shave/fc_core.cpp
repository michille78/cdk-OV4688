///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     FC core implementation for SHAVE. This function
///

// 1: Includes
// ----------------------------------------------------------------------------
#include <mv_types.h>
#include <svuCommonShave.h>
#include <swcCdma.h>
#include <stdio.h>
#include <math.h>

#include <moviVectorUtils.h>
#include "fc_core.h"
#include <matrixVectorMultfp16x4.h>
#include <matmul.h>
#include <mvFCParam.h>

#include "matmul_kernel.h"

// 2:  Source Specific #defines and types  (typedef,enum,struct)
// ----------------------------------------------------------------------------
#define MIN(a, b) ((a)<(b)?(a):(b))
#define MAX(a, b) ((a)>(b)?(a):(b))
#define INPUT_BPP            2

// size of buffer B is 16 rows at MAX_SLICE_OUT_UNITS elements/row
#define BUFF_B_ELEMS         16 * MAX_SLICE_OUT_UNITS
#define MAX_SLICE_IN_UNITS   1024

// 3: Global Data (Only if absolutely necessary)
// ----------------------------------------------------------------------------
// 4: Static Local Data
// ----------------------------------------------------------------------------
// 5: Static Function Prototypes
// ----------------------------------------------------------------------------
// 6: Functions Implementation
// ----------------------------------------------------------------------------

void mvFC(t_MvFCParam *p)
{
    u32 sliceOutUnits = p->sliceOutUnits;
    u32 sliceInUnits, sliceInUnitsMax, sliceOutUnitsStride;
    u32 sliceInUnits_start = 0;
    u32 inUnits = p->inUnits;
    u32 outUnits = p->outUnits;
    u32 i;
    u8* inAddressA = (u8*) ((u32) p->inputA);
    u8* inAddressB = (u8*) ((u32) p->inputB);
    u8* outAddress = (u8*) ((u32) p->output);
    u8 *inputBufferA, *inputBufferB, *outBufferC;
    u8 *inputBufferA_db[2], *inputBufferB_db[2], buf_idx;
    dmaTransactionList_t task1, taskA, taskB;
    dmaTransactionList_t *ref1, *refA, *refB;
    u32 id1;

    // dmaLinkAgent
    id1 = dmaInitRequesterWithAgent(1, p->dmaLinkAgent);

    // ! The size of cmxslice must be at least
    // ! 127 + (2*MAX_SLICE_IN_UNITS + 2*BUFF_B_ELEMS  + MAX_SLICE_OUT_UNITS)*INPUT_BPP bytes = 71807 bytes

    // set buffers to point to locations relative to cmxslice
    // align the first buffer to 128 bytes boundary;
    // the following buffers will also be aligned, because the increment is a multiple of 128
    inputBufferA_db[0] = (u8*)(((u32)p->cmxslice + 0x7f) & ~0x7f);
    inputBufferA_db[1] = inputBufferA_db[0] + MAX_SLICE_IN_UNITS * INPUT_BPP;
    inputBufferB_db[0] = inputBufferA_db[1] + MAX_SLICE_IN_UNITS * INPUT_BPP;
    inputBufferB_db[1] = inputBufferB_db[0] + BUFF_B_ELEMS * INPUT_BPP;
    outBufferC         = inputBufferB_db[1] + BUFF_B_ELEMS * INPUT_BPP;

    // DMA inputC
    ref1 = dmaCreateTransaction(id1, &task1,
                                outAddress,
                                outBufferC,
                                1 * 1 * sliceOutUnits * INPUT_BPP);
    dmaStartListTask(ref1);
    dmaWaitTask(ref1);

    sliceOutUnits      = MIN(sliceOutUnits, MAX_SLICE_OUT_UNITS);
    sliceOutUnitsStride = sliceOutUnits;
    if(sliceOutUnits % 8)
        sliceOutUnitsStride = sliceOutUnits + 8 - (sliceOutUnits % 8);

    // sliceInUnits will always be >= BUFF_B_ELEMS * MAX_SLICE_OUT_UNITS (= 16)
    sliceInUnitsMax    = BUFF_B_ELEMS / sliceOutUnitsStride;
    // make sliceInUnitsMax a multiple of 8
    sliceInUnitsMax    = MIN(sliceInUnitsMax, inUnits);
    sliceInUnitsMax    = MIN(sliceInUnitsMax, MAX_SLICE_IN_UNITS);
    sliceInUnitsMax   &= ~0x7;

    sliceInUnits       = sliceInUnitsMax;
    sliceInUnits_start = 0;

    buf_idx = 0;
    inputBufferA = inputBufferA_db[buf_idx];
    inputBufferB = inputBufferB_db[buf_idx];
    refA = dmaCreateTransaction(id1, &taskA,
                                inAddressA + sliceInUnits_start * INPUT_BPP,
                                inputBufferA,
                                1 * 1 * sliceInUnits * INPUT_BPP);
    refB = dmaCreateTransactionFullOptions(id1, &taskB,
                                           inAddressB + sliceInUnits_start * outUnits * INPUT_BPP,
                                           inputBufferB,
                                           sliceInUnits * sliceOutUnits * INPUT_BPP, // byte length
                                           sliceOutUnits * INPUT_BPP,       // src width
                                           sliceOutUnits * INPUT_BPP,       // dst width
                                           outUnits * INPUT_BPP,      // src stride
                                           sliceOutUnitsStride * INPUT_BPP);    // dst stride
    dmaLinkTasks(refA, 1, refB);
    dmaStartListTask(refA);
    dmaWaitTask(refA);
    sliceInUnits_start += sliceInUnits;


    while (sliceInUnits_start < inUnits)
    {
        // compute sliceInUnits for the next iteration
        sliceInUnits = MIN(sliceInUnitsMax, inUnits - sliceInUnits_start);

        // fetch the inputA and inputB for the next iteration (over DMA)
        buf_idx = 1 - buf_idx;
        refA = dmaCreateTransaction(id1, &taskA,
                                    inAddressA + sliceInUnits_start * INPUT_BPP,
                                    inputBufferA_db[buf_idx],
                                    1 * 1 * sliceInUnits * INPUT_BPP);
        refB = dmaCreateTransactionFullOptions(id1, &taskB,
                                               inAddressB + sliceInUnits_start * outUnits * INPUT_BPP,
                                               inputBufferB_db[buf_idx],
                                               sliceInUnits * sliceOutUnits * INPUT_BPP, // byte length
                                               sliceOutUnits * INPUT_BPP,       // src width
                                               sliceOutUnits * INPUT_BPP,       // dst width
                                               outUnits * INPUT_BPP,      // src stride
                                               sliceOutUnitsStride * INPUT_BPP);    // dst stride
        dmaLinkTasks(refA, 1, refB);
        dmaStartListTask(refA);

        // pad bufferA with 0's up the sliceInUnitsMax elements;
        // this goes in parallel with the DMA transfer, but it writes
        // to an area not overlapping with the destination of taskA
        for (i=sliceInUnits; i<sliceInUnitsMax; i++)
        {
            ((half*)inputBufferA_db[buf_idx])[i] = 0.0;
        }

        gemm_hhhh_nnn((half*)inputBufferA, (half*)inputBufferB, (half*)outBufferC, 1, sliceInUnitsMax, sliceOutUnitsStride, sliceInUnitsMax, sliceOutUnitsStride, sliceOutUnitsStride);

        dmaWaitTask(refA);
        // set the processing buffers for the next iteration
        inputBufferA = inputBufferA_db[buf_idx];
        inputBufferB = inputBufferB_db[buf_idx];
        sliceInUnits_start += sliceInUnits;
    };

    // the previous while breaks at the end of the block, before gemm_hhhh
    // is applied for the last transfered slice, therefore we process the last slice out of the loop
    if (sliceInUnits_start == inUnits)
    {
        gemm_hhhh_nnn((half*)inputBufferA, (half*)inputBufferB, (half*)outBufferC, 1, sliceInUnitsMax, sliceOutUnitsStride, sliceInUnitsMax, sliceOutUnitsStride, sliceOutUnitsStride);
    }

    ref1 = dmaCreateTransaction(id1, &task1,
                                outBufferC,
                                outAddress,
                                sliceOutUnits * INPUT_BPP);
    dmaStartListTask(ref1);
    dmaWaitTask(ref1);

    SHAVE_HALT;
}
