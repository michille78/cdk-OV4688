.version 00.50.70

.set pdest  i18
.set psrc i17
.set size  i16

.set icond i0
.set itail i1
.set iter  i2
.set iTemp i3
.set iTemp1 i4

.code .text.memcpy_asm
;Calling convention
;void * memcpy_asm(void *destination, const void * source, size_t num);
;                              (i18)                (i17)       (i16)
.lalign
memcpy_asm:
LSU0.LDIL iTemp, 0x3
IAU.SHR.U32 iter, size, iTemp || LSU0.LDIL icond, 0x11111111 || LSU1.LDIH icond, 0x11111111
                    LSU1.LDI.64.L v0, psrc || LSU0.LDIL iTemp, 7
PEU.PCIX.NEQ 0x20 || LSU1.LDI.64.L v0, psrc || IAU.AND itail, size, iTemp || LSU0.LDIL iTemp, 8
                     LSU1.LDI.64.L v0, psrc || IAU.SUBSU itail, iTemp, itail || LSU0.LDIL iTemp, 2 || CMU.CMZ.I32 itail
                     LSU1.LDI.64.L v0, psrc || IAU.SHL itail, itail, iTemp || LSU0.LDIL iTemp, 1
                     LSU1.LDI.64.L v0, psrc || IAU.SHR.U32 icond, icond, itail
PEU.PCCX.EQ  0x01 || LSU1.LDI.64.L v0, psrc || IAU.XOR icond, icond, icond  || LSU0.LDIL iTemp, 0x3
                     LSU1.LDI.64.L v0, psrc || CMU.CPIT C_CMU0, icond || IAU.SHR.U32 iter, size, iTemp
PEU.PCIX.NEQ 0x30 || LSU1.LDI.64.L v0, psrc || LSU0.STI.64.L v0, pdest || BRU.RPI iter
PEU.PVL08 EQ      || LSU0.ST.64.L v0, pdest || BRU.JMP i30
nop 6

.end
