/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief    Transforms an input volume V in channel minor to V' such that:
///           a kxl filter operation applied on V can be performed through
///           a 1x1 filter operation on V'.
///

// 1: Includes
// ----------------------------------------------------------------------------
#include <mv_types.h>
#include <svuCommonShave.h>
#include <swcCdma.h>
#include <cstring>

#include <moviVectorTypes.h>
#include "mvRelayoutParam.h"
#include "relayout_core.h"

#include <mvTensor.h>

// 2:  Source Specific #defines and types  (typedef,enum,struct)
// ----------------------------------------------------------------------------
#define CMX_SIZE         84000

// 3: Global Data (Only if absolutely necessary)
// ----------------------------------------------------------------------------
// 4: Static Local Data
// ----------------------------------------------------------------------------
// 5: Static Function Prototypes
// ----------------------------------------------------------------------------
// 6: Functions Implementation
// ----------------------------------------------------------------------------
void relayout_core(t_RelayoutParams *params)
{
    if(params->version == CONVOLUTION_V1)
        relayout_core_v1(params);
    else if(params->version == CONVOLUTION_V0)
        relayout_core_v0(params);
    else if(params->version == DECONVOLUTION)
        relayout_deconvolution(params);
}

void relayout_core_v0(t_RelayoutParams *params)
{
    u8  *input      = (u8 *)params->input;
    u8  *output     = (u8 *)params->output;
    s32 width       = params->width;
    s32 height      = params->height;
    s32 no_channels = params->no_channels;
    s32 radixX      = params->radixX;
    s32 radixY      = params->radixY;
    s32 inOutBpp    = params->inOutBpp;
    s32 kernelStrideX = params->kernelStrideX;
    s32 kernelStrideY = params->kernelStrideY;
    s32 pad_style = params->pad_style;

    s32 id1 = dmaInitRequesterWithAgent(1, params->dmaLinkAgent);

    s32 no_pixels_to_process = params->no_pixels;
    s32 first_input_pixel = params->first_pixel;

    dmaTransactionList_t task_in_out;
    dmaTransactionList_t *ref_in_out;

    s32 cmx_buffer_size = CMX_SIZE / 3;
    s32 in_pixel_size = no_channels * inOutBpp;
    s32 pixels_per_buffer = cmx_buffer_size / in_pixel_size;

    u8 *cmxInput = params->cmxslice;
    u8 *cmxOutput0 = cmxInput + cmx_buffer_size;
    u8 *cmxOutput1 = cmxOutput0 + cmx_buffer_size;

    bool ping_pong = true;
    // This boolean is needed because waiting on already finished task will
    // hang in dmaWaitTask.
    bool is_transfering = false;

    s32 half_radixX = radixX >> 1;
    s32 half_radixY = radixY >> 1;

    s32 no_in_transfers = no_pixels_to_process / pixels_per_buffer;
    s32 no_pixels_remainder = no_pixels_to_process % pixels_per_buffer;

    if(no_pixels_remainder)
        ++no_in_transfers;

    s32 input_offset = first_input_pixel * in_pixel_size;
    // We don't need to skip any input pixels because first
    // pixel to process is in the padding.
    if(input_offset < 0)
        input_offset = 0;

    s32 pixels_per_transfer = pixels_per_buffer;

    for(s32 in_transfer_i = 0; in_transfer_i < no_in_transfers; ++in_transfer_i)
    {
        if(no_pixels_remainder && (in_transfer_i == (no_in_transfers - 1)))
            pixels_per_transfer = no_pixels_remainder;

        s32 padd_pixels_before = 0;
        // We have to process top padding pixels.
        if(first_input_pixel < 0)
            padd_pixels_before = -first_input_pixel;

        s32 last_pixel_index = first_input_pixel + pixels_per_transfer;
        s32 padd_pixels_after = 0;
        // Check how many pixels fall in bottom padding.
        if(last_pixel_index >= width * height)
            padd_pixels_after = (last_pixel_index - width * height);

        if(padd_pixels_after > pixels_per_transfer)
            padd_pixels_after = pixels_per_transfer;

        s32 pixels_to_transfer_in = pixels_per_transfer - padd_pixels_before - padd_pixels_after;

        if(pixels_to_transfer_in > 0)
        {
            if(is_transfering)
            {
                dmaWaitTask(ref_in_out);
                is_transfering = false;
            }

            // Transfer input pixels.
            ref_in_out = dmaCreateTransaction(id1,
                    &task_in_out,
                    (u8 *)(input + input_offset),
                    (u8 *)(cmxInput + padd_pixels_before * in_pixel_size),
                    pixels_to_transfer_in * in_pixel_size);

            dmaStartListTask(ref_in_out);
        }
        else
            // Needed for correct bottom padding.
            pixels_to_transfer_in = 0;

        // Fill top padding pixels if any.
        memset((void *)(cmxInput), 0, padd_pixels_before * in_pixel_size);
        // Fill bottom padding pixels if any.
        memset((void *)(cmxInput + (padd_pixels_before + pixels_to_transfer_in) * in_pixel_size), 0, padd_pixels_after * in_pixel_size);

        if(pixels_to_transfer_in > 0)
        {
            // Wait for input to be transfered.
            dmaWaitTask(ref_in_out);
            // Move offset to next input pixel to process.
            input_offset += (pixels_to_transfer_in * in_pixel_size);
        }

        // transfer_no out of radixX * radixY transfers for the current batch of
        // pixels (i.e. in_transfer_i). Needed for output offset computation.
        s32 transfer_no = 0;
        // The output volume in row major channel minor is obtained from
        // radixX * radixY copies of the input shifted by ry * width + rx
        // concatenated along the channel dimension - see example at the bottom.
        for(s32 ry = -half_radixY; ry <= half_radixY; ++ry)
        {
            for(s32 rx = -half_radixX; rx <= half_radixX; ++rx)
            {
                // We have to transfer out at most as many pixels as we transfered in.
                s32 out_pixels_to_transfer = pixels_per_transfer;

                s32 first_out_pixel_index = first_input_pixel + ((-ry * width) - rx);

                s32 in_offset = 0;
                // Simulate a shift up with a shift down in input.
                if(first_out_pixel_index < 0)
                {
                    in_offset = (-first_out_pixel_index) * in_pixel_size;
                    out_pixels_to_transfer += first_out_pixel_index;
                    first_out_pixel_index = 0;
                }

                // If there are pixels that fall outside the output image cut them out by
                // not transferring them.
                if(first_out_pixel_index + out_pixels_to_transfer >= width * height)
                    out_pixels_to_transfer = width * height - first_out_pixel_index;

                // It is possible that we have nothing to transfer out.
                if(out_pixels_to_transfer > 0)
                {
                    u8 *ping_pong_buffer = ping_pong ? cmxOutput0 : cmxOutput1;

                    // When need to know the current output pixel row and col to create the left |
                    // right padding.
                    s32 pixel_row = first_out_pixel_index / width;
                    s32 pixel_col = first_out_pixel_index % width;

                    // If the stride is 1 in both directions use the faster approach
                    // that avoids the downsampling.
                    if(pad_style == paddStyleTFValid)
                    {
                        const s32 valid_x = (radixX - 1) >> 1;
                        const s32 valid_y = (radixY - 1) >> 1;

                        if(kernelStrideY != 1 || kernelStrideX != 1)
                        {
                            s32 down_sampled_pixels_to_transfer = 0;

                            s32 pixel_i = 0;
                            // Find the first output pixel that is a kernel center and fits in valid padding.
                            for(pixel_i = 0; pixel_i < out_pixels_to_transfer; ++pixel_i)
                            {
                                if((pixel_col - valid_x) % kernelStrideX == 0 && (pixel_row - valid_y) % kernelStrideY == 0 &&
                                        pixel_col >= valid_x && pixel_col < width - valid_x &&
                                        pixel_row >= valid_y && pixel_row < height - valid_y)
                                {
                                    s32 out_width = (width - 2 * valid_x + kernelStrideX - 1) / kernelStrideX;
                                    first_out_pixel_index = (pixel_row - valid_y) / kernelStrideY * out_width + (pixel_col - valid_x) / kernelStrideX;
                                    break;
                                }

                                ++pixel_col;
                                if(pixel_col >= width)
                                {
                                    pixel_col = 0;
                                    ++pixel_row;

                                    if(pixel_row >= height - valid_y)
                                    {
                                        pixel_i = out_pixels_to_transfer;
                                        break;
                                    }
                                }
                            }

                            // Step by kernelWidth and copy pixels, that fit in the valid padding, in the output.
                            while(pixel_i < out_pixels_to_transfer )
                            {
                                if(pixel_col < width - valid_x)
                                {
                                    memcpy_asm((void *)(ping_pong_buffer),
                                            (void *)(cmxInput + in_offset + pixel_i * in_pixel_size),
                                            in_pixel_size);

                                    ping_pong_buffer += in_pixel_size;
                                    ++down_sampled_pixels_to_transfer;
                                }

                                // Step to the next kernel column.
                                pixel_i += kernelStrideX;

                                pixel_col += kernelStrideX;
                                if(pixel_col >= width)
                                {
                                    pixel_i = pixel_i - (pixel_col - width) + (kernelStrideY - 1) * width + valid_x;
                                    pixel_col = valid_x;
                                    // Step the correct number of rows.
                                    pixel_row += kernelStrideY;

                                    if(pixel_row >= height - valid_y)
                                        break;
                                }
                            }

                            out_pixels_to_transfer = down_sampled_pixels_to_transfer;
                            // Revert the ping_pong_buffer to start because we are going to
                            // use the same pointer for the DMA output transfer.
                            ping_pong_buffer -= in_pixel_size * out_pixels_to_transfer;
                        }
                        else // VALID PADDING stride = 1
                        {
                            // The "down sampled" in this case stands for the
                            // pixels trimmed from the edges.
                            s32 down_sampled_pixels_to_transfer = 0;

                            s32 pixel_i = 0;
                            // Find the first output pixel that fits in valid padding.
                            for(pixel_i = 0; pixel_i < out_pixels_to_transfer; ++pixel_i)
                            {
                                if(pixel_col >= valid_x && pixel_col < width - valid_x &&
                                        pixel_row >= valid_y && pixel_row < height - valid_y)
                                {
                                    s32 out_width = (width - 2 * valid_x + kernelStrideX - 1) / kernelStrideX;
                                    first_out_pixel_index = (pixel_row - valid_y) / kernelStrideY * out_width + (pixel_col - valid_x) / kernelStrideX;
                                    break;
                                }

                                ++pixel_col;
                                if(pixel_col >= width)
                                {
                                    pixel_col = 0;
                                    ++pixel_row;

                                    if(pixel_row >= height - valid_y)
                                    {
                                        pixel_i = out_pixels_to_transfer;
                                        break;
                                    }
                                }
                            }

                            // Copy pixels, that fit in the valid padding, starting with
                            // the first pixel and ending with pixel (width-validx).
                            while(pixel_i < out_pixels_to_transfer && pixel_row < height - valid_y)
                            {
                                // Copy pixels until the end of the valid row or until the end of the pixel buffer.
                                s32 no_pixels_to_copy = width - valid_x - pixel_col;
                                if(pixel_i + no_pixels_to_copy >= out_pixels_to_transfer)
                                    no_pixels_to_copy = out_pixels_to_transfer - pixel_i;

                                memcpy_asm((void *)(ping_pong_buffer),
                                        (void *)(cmxInput + in_offset + pixel_i * in_pixel_size),
                                        in_pixel_size * no_pixels_to_copy);

                                ping_pong_buffer += in_pixel_size * no_pixels_to_copy;
                                down_sampled_pixels_to_transfer += no_pixels_to_copy;

                                // Step to the next row.
                                pixel_i += no_pixels_to_copy + 2 * valid_x;
                                pixel_col = valid_x;
                                ++pixel_row;
                            }

                            out_pixels_to_transfer = down_sampled_pixels_to_transfer;
                            // Revert the ping_pong_buffer to start because we are going to
                            // use the same pointer for the DMA output transfer.
                            ping_pong_buffer -= in_pixel_size * out_pixels_to_transfer;
                        }

                    }
                    else if(pad_style == paddStyleTFSame)
                    {
                        if(kernelStrideY != 1 || kernelStrideX != 1)
                        {
                            s32 down_sampled_pixels_to_transfer = 0;

                            s32 pixel_i = 0;
                            // Find the first output pixel that is a kernel center.
                            for(pixel_i = 0; pixel_i < out_pixels_to_transfer; ++pixel_i)
                            {
                                if(pixel_col % kernelStrideX == 0 && pixel_row % kernelStrideY == 0)
                                {
                                    s32 out_width = (width + kernelStrideX - 1) / kernelStrideX;
                                    first_out_pixel_index = pixel_row / kernelStrideY * out_width + pixel_col / kernelStrideX;
                                    break;
                                }

                                ++pixel_col;
                                if(pixel_col >= width)
                                {
                                    pixel_col = 0;
                                    ++pixel_row;
                                }
                            }

                            // Starting with the first output pixel that is a kernel center
                            // and step n pixels to the next kernel center copy the pixel
                            // in the output buffer or copy 0 for left right padding.
                            while(pixel_i < out_pixels_to_transfer)
                            {
                                // Check if for the current (ry, rx) shift pair the pixel
                                // falls in the left or right padding.
                                if(pixel_col + rx >= 0 && pixel_col + rx < width)
                                {
                                    memcpy_asm((void *)(ping_pong_buffer),
                                            (void *)(cmxInput + in_offset + pixel_i * in_pixel_size),
                                            in_pixel_size);
                                }
                                else
                                {
                                    memset((void *)(ping_pong_buffer),
                                            0, in_pixel_size);
                                }

                                ping_pong_buffer += in_pixel_size;
                                ++down_sampled_pixels_to_transfer;

                                // Step to the next kernel column.
                                pixel_i += kernelStrideX;

                                pixel_col += kernelStrideX;
                                if(pixel_col >= width)
                                {
                                    pixel_i = pixel_i - (pixel_col - width) + (kernelStrideY - 1) * width;
                                    pixel_col = 0;
                                    // Step the correct number of rows.
                                    pixel_row += kernelStrideY;
                                }
                            }

                            out_pixels_to_transfer = down_sampled_pixels_to_transfer;
                            // Revert the ping_pong_buffer to start because we are going to
                            // use the same pointer for the DMA output transfer.
                            ping_pong_buffer -= in_pixel_size * out_pixels_to_transfer;
                        }
                        else
                        {
                            // Both kernel strides are 1. No need to downsample.
                            // Copy the input pixels in the output buffer.
                            memcpy_asm(ping_pong_buffer,
                                    (void *)(cmxInput + in_offset),
                                    out_pixels_to_transfer * in_pixel_size);

                            if(rx != 0)
                            {
                                // Find the first pixel that falls into the left|right padding.
                                s32 pixel_i = 0;
                                for(pixel_i = 0; pixel_i < out_pixels_to_transfer; ++pixel_i)
                                {
                                    if(pixel_col + rx < 0 || pixel_col + rx >= width)
                                        break;

                                    ++pixel_col;
                                    if(pixel_col >= width)
                                    {
                                        pixel_col = 0;
                                        ++pixel_row;
                                    }
                                }

                                // The next, at most, rx pixels are padding.
                                while(pixel_i < out_pixels_to_transfer)
                                {
                                    // If the pixel falls in the left right padding replace it with padding.
                                    if(pixel_col + rx < 0 || pixel_col + rx >= width)
                                    {
                                        memset((void *)(ping_pong_buffer + pixel_i * in_pixel_size), 0, in_pixel_size);
                                        ++pixel_col;
                                        if(pixel_col >= width)
                                        {
                                            pixel_col = 0;
                                            ++pixel_row;
                                        }
                                        ++pixel_i;
                                    }
                                    else
                                    {
                                        // After the padding pixels the next
                                        // width - |rx| pixels are valid so we can
                                        // skip testing them.
                                        if(rx < 0)
                                        {
                                            pixel_col += (width + rx);
                                            pixel_i += (width + rx);
                                        }
                                        else
                                        {
                                            pixel_i += (width - rx);
                                            pixel_col += (width - rx);
                                        }

                                        if(pixel_col >= width)
                                        {
                                            pixel_col = 0;
                                            ++pixel_row;
                                        }
                                    }
                                }
                            }
                        }
                    }


                    // When either stride is != 1, due to down-sampling, there might not be
                    // any pixels to transfer out.
                    if(out_pixels_to_transfer > 0)
                    {
                        if(is_transfering)
                        {
                            // Wait for the previous transfer to finish.
                            dmaWaitTask(ref_in_out);
                            is_transfering = false;
                        }

                        ref_in_out = dmaCreateTransactionDstStride(id1,
                                &task_in_out,
                                ping_pong_buffer,
                                (u8 *)(output + first_out_pixel_index * in_pixel_size * radixX * radixY + transfer_no * in_pixel_size),
                                out_pixels_to_transfer * in_pixel_size,
                                in_pixel_size,
                                radixX * radixY * in_pixel_size);

                        dmaStartListTask(ref_in_out);

                        // Switch buffer.
                        ping_pong = !ping_pong;
                        is_transfering = true;
                    }
                }

                // transfer_no out of radixX * radixY transfers for the current batch of
                // pixels (i.e. in_transfer_i).
                ++transfer_no;
            }
        }

        first_input_pixel += pixels_per_transfer;
    }

    if(is_transfering)
    {
        dmaWaitTask(ref_in_out);
    }

    SHAVE_HALT;
}

void relayout_core_v1(t_RelayoutParams *params)
{
    u8  *input          = (u8 *)params->input;
    u8  *output         = (u8 *)params->output;
    s32 width           = params->width;
    s32 height          = params->height;
    s32 input_channels  = params->no_channels;
    s32 radixX          = params->radixX;
    s32 radixY          = params->radixY;
    s32 bpp             = params->inOutBpp;
    s32 kernelStrideX   = params->kernelStrideX;
    s32 kernelStrideY   = params->kernelStrideY;
    s32 pad_style       = params->pad_style;
    s32 slice_width     = params->slice_width;
    s32 slice_height    = params->slice_height;

    s32 input_channels_size = input_channels * bpp;
    s32 output_channels     = input_channels * radixX * radixY;

    s32 output_width;
    if(paddStyleTFValid == pad_style)
        output_width = (width - radixX + 1 + kernelStrideX - 1) / kernelStrideX;
    else
        output_width = (width + kernelStrideX - 1) / kernelStrideX;

    dmaTransactionList_t task_in_out;
    dmaTransactionList_t *ref_in_out;
    s32 id1 = dmaInitRequesterWithAgent(1, params->dmaLinkAgent);

    bool solution_found = false;
    s32 max_pixels_per_row = slice_width;
    s32 max_pixels_per_col = slice_height;
    s32 no_width_slices  = 1;
    s32 no_height_slices = 1;

    // Step 0.
    // Split the input volume in horizontal slices of size max_pixels_per_col X slice_width.
    // Stop when the slice fits in the cmx or max_pixels_per_col < radixY => solution_found = true
    while(max_pixels_per_col >= radixY || max_pixels_per_col == slice_height)
    {
        s32 X = max_pixels_per_row + radixX - 1;
        s32 Y = max_pixels_per_col + radixY - 1;

        s32 temp_input_buffer_size = (X * Y * input_channels) * bpp;
        s32 temp_relayout_buffer_size = ((max_pixels_per_row + kernelStrideX - 1) / kernelStrideX) *
                ((max_pixels_per_col + kernelStrideY - 1) / kernelStrideY) * output_channels * bpp;

        if(temp_input_buffer_size + temp_relayout_buffer_size <= CMX_SIZE)
        {
            solution_found = true;
            break;
        }

        ++no_height_slices;
        max_pixels_per_col = height / no_height_slices;
    }

    if(!solution_found)
    {
        // Step 1.
        // We don't have a solution hence we have to slice vertically.
        // Split the input volume in vertical slices of size:
        // max_pixels_per_col (determined in the previous step) x max_pixels_per_row
        // Stop when the slice fits in the cmx or max_pixels_per_row < radixX => solution_found = true
        --no_height_slices;
        max_pixels_per_col = height / no_height_slices;
        while(max_pixels_per_row >= radixX || max_pixels_per_row == slice_width)
        {
            s32 X = max_pixels_per_row + radixX - 1;
            s32 Y = max_pixels_per_col + radixY - 1;

            s32 input_buffer_size = (X * Y * input_channels) * bpp;
            s32 relayout_buffer_size = ((max_pixels_per_row + kernelStrideX - 1) / kernelStrideX) *
                    ((max_pixels_per_col + kernelStrideY - 1) / kernelStrideY) * output_channels * bpp;

            if(input_buffer_size + relayout_buffer_size <= CMX_SIZE)
            {
                solution_found = true;
                break;
            }

            ++no_width_slices;
            max_pixels_per_row = width / no_width_slices;
        }

        if(solution_found)
        {
            // If we found a solution in Step 1 we might be able to increase the height of the slice.
            while(1)
            {
                if(no_height_slices == 1)
                    break;

                --no_height_slices;
                max_pixels_per_col = height / no_height_slices;

                s32 X = max_pixels_per_row + radixX - 1;
                s32 Y = max_pixels_per_col + radixY - 1;
                s32 input_buffer_size = (X * Y * input_channels) * bpp;
                s32 relayout_buffer_size = ((max_pixels_per_row + kernelStrideX - 1) / kernelStrideX) *
                        ((max_pixels_per_col + kernelStrideY - 1) / kernelStrideY) * output_channels * bpp;

                if(input_buffer_size + relayout_buffer_size > CMX_SIZE)
                {
                    ++no_height_slices;
                    max_pixels_per_col = height / no_height_slices;
                    break;
                }

            }
        }
        else
        {
            // We can't hold in cmx a slice of the imput image of size:
            // (radixX + left_right padding) * (radixY + top_bottom_padding).
            // Default to computing for one pixel at a time.
            // The assumption is that we can hold in cmx at least the data
            // needed to output one output pixel.
            max_pixels_per_row = 1;
            max_pixels_per_col = 1;
        }
    }

    // Parameters of the volumes to be held in cmx.
    s32 maxX = max_pixels_per_row + radixX - 1; // Width of the input slice + left & right padding.
    s32 maxY = max_pixels_per_col + radixY - 1; // Height of the input slice + top & bottom padding.

    s32 max_input_buffer_size = maxX * maxY * input_channels_size;

    u8 *cmx_input_buffer    = params->cmxslice;
    u8 *cmx_relayout_buffer = cmx_input_buffer + max_input_buffer_size;

    no_width_slices  = slice_width / max_pixels_per_row;
    s32 last_slice_width = slice_width % max_pixels_per_row;
    if(last_slice_width)
        ++no_width_slices;

    no_height_slices  = slice_height / max_pixels_per_col;
    s32 last_slice_height = slice_height % max_pixels_per_col;
    if(last_slice_height)
        ++no_height_slices;

    for(s32 h_slice_i = 0; h_slice_i < no_height_slices; ++h_slice_i)
    {
        s32 current_slice_height = max_pixels_per_col;
        if(h_slice_i == (no_height_slices - 1) && last_slice_height != 0)
            current_slice_height = last_slice_height;

        s32 first_pixel_row = params->first_pixel_row + h_slice_i * max_pixels_per_col;
        s32 pad_top = ((radixY - 1) >> 1) - first_pixel_row;
        if(pad_top < 0)
            pad_top = 0;

        s32 last_pixel_row = first_pixel_row + current_slice_height;
        s32 pad_bottom = last_pixel_row + ((radixY - 1) >> 1) - height;
        if(pad_bottom < 0)
            pad_bottom = 0;

        for(s32 w_slice_i = 0; w_slice_i < no_width_slices; ++w_slice_i)
        {
            s32 current_slice_width = max_pixels_per_row;
            if(w_slice_i == (no_width_slices - 1) && last_slice_width != 0)
                current_slice_width = last_slice_width;

            s32 first_pixel_col = params->first_pixel_col + w_slice_i * max_pixels_per_row;
            s32 pad_left = ((radixX - 1) >> 1) - first_pixel_col;
            if(pad_left < 0)
                pad_left = 0;

            s32 last_pixel_col = first_pixel_col + current_slice_width;
            s32 pad_right = last_pixel_col + ((radixX - 1) >> 1) - width;
            if(pad_right < 0)
                pad_right = 0;

            s32 transfer_pixel_start_row = first_pixel_row - ((radixY - 1) >> 1);
            if(transfer_pixel_start_row < 0)
                transfer_pixel_start_row = 0;

            s32 transfer_pixel_start_col = first_pixel_col - ((radixX - 1) >> 1);
            if(transfer_pixel_start_col < 0)
                transfer_pixel_start_col = 0;

            s32 ddr_input_offset = (transfer_pixel_start_row * width +
                    transfer_pixel_start_col) * input_channels_size;

            s32 cmx_input_offset          = (pad_top * maxX + pad_left) * input_channels_size;
            s32 input_transfer_size       = (current_slice_width + (radixX - 1) - pad_left - pad_right) *
                    (current_slice_height + (radixY - 1) - pad_top - pad_bottom) * input_channels_size;
            s32 input_transfer_src_width  = (current_slice_width + (radixX - 1) - pad_left - pad_right) * input_channels_size;
            s32 input_transfer_src_stride = width * input_channels_size;
            s32 input_transfer_dst_width  = input_transfer_src_width;
            s32 input_transfer_dst_stride = maxX * input_channels_size;

            // DMA Input slice;
            ref_in_out = dmaCreateTransactionFullOptions(
                    id1,
                    &task_in_out,
                    (u8 *)(input + ddr_input_offset),
                    (u8 *)(cmx_input_buffer + cmx_input_offset),
                    input_transfer_size,
                    input_transfer_src_width,
                    input_transfer_dst_width,
                    input_transfer_src_stride,
                    input_transfer_dst_stride);

            dmaStartListTask(ref_in_out);
            dmaWaitTask(ref_in_out);

            // Pad with zeros.
            if(pad_top != 0)
                memset((void *)cmx_input_buffer, 0, pad_top * maxX * input_channels_size);

            if(pad_bottom != 0)
                memset((void *)(cmx_input_buffer + maxX * (current_slice_height + (radixY - 1) - pad_bottom) * input_channels_size),
                        0, pad_bottom * maxX * input_channels_size);

            // Left right padding.
            for(s32 row_i = pad_top; row_i < maxY - pad_bottom; ++row_i)
            {
                if(pad_left != 0)
                    memset((void *)(cmx_input_buffer + maxX * row_i * input_channels_size),
                            0, pad_left * input_channels_size);

                if(pad_right != 0)
                    memset((void *)(cmx_input_buffer + (maxX * row_i + current_slice_width + (radixX - 1) - pad_right) * input_channels_size),
                            0, pad_right * input_channels_size);
            }

            // Column index of the first pixel that is a kernel center in the current slice.
            s32 start_pixel_col = 0;
            if(paddStyleTFValid == pad_style)
            {
                if((first_pixel_col - ((radixX - 1) >> 1)) % kernelStrideX)
                    start_pixel_col = kernelStrideX - ((first_pixel_col - ((radixX - 1) >> 1)) % kernelStrideX);
            }
            else
            {
                if(first_pixel_col % kernelStrideX)
                    start_pixel_col = kernelStrideX - (first_pixel_col % kernelStrideX);
            }

            // Row index of the first pixel that is a kernel center in the current slice.
            s32 start_pixel_row = 0;
            if(paddStyleTFValid == pad_style)
            {
                if((first_pixel_row - ((radixY - 1) >> 1)) % kernelStrideY)
                    start_pixel_row = kernelStrideY - ((first_pixel_row - ((radixY - 1) >> 1)) % kernelStrideY);
            }
            else
            {
                if(first_pixel_row % kernelStrideY)
                    start_pixel_row = kernelStrideY - (first_pixel_row % kernelStrideY);
            }

            // Relayout
            // For each pixel in the current slice that is a kernel center, create
            // the output pixel copying radixX pixels from each radixY lines.
            s32 pixels_to_output = 0;
            s32 output_slice_width = (current_slice_width - start_pixel_col + kernelStrideX - 1) / kernelStrideX;
            for(s32 row_i = start_pixel_row; row_i < current_slice_height; row_i += kernelStrideY)
            {
                for(s32 col_i = start_pixel_col; col_i < current_slice_width; col_i += kernelStrideX)
                {
                    for(s32 ry_i = 0; ry_i < radixY; ++ry_i)
                    {
                        s32 relayout_offset = (pixels_to_output * radixY + ry_i) * radixX * input_channels_size;
                        s32 input_offset    = ((row_i + ry_i) * maxX + col_i) * input_channels_size;

                        memcpy_asm((void *)(cmx_relayout_buffer + relayout_offset),
                                (void *)(cmx_input_buffer + input_offset),
                                (radixX * input_channels_size));
                    }

                    ++pixels_to_output;
                }
            }

            if(pixels_to_output > 0)
            {
                // Find the first output pixel that is a kernel center.
                s32 first_output_pixel_col;
                s32 first_output_pixel_row;

                if(paddStyleTFValid == pad_style)
                {
                    first_output_pixel_col = (first_pixel_col - ((radixX - 1) >> 1) + start_pixel_col) / kernelStrideX;
                    first_output_pixel_row = (first_pixel_row - ((radixY - 1) >> 1) + start_pixel_row) / kernelStrideY;
                }
                else
                {
                    first_output_pixel_col = (first_pixel_col + start_pixel_col) / kernelStrideX;
                    first_output_pixel_row = (first_pixel_row + start_pixel_row) / kernelStrideY;
                }

                s32 output_offset = (first_output_pixel_row * output_width +
                        first_output_pixel_col) * output_channels * bpp;

                ref_in_out = dmaCreateTransactionDstStride(
                        id1,
                        &task_in_out,
                        (u8 *)(cmx_relayout_buffer),
                        (u8 *)(output + output_offset),
                        pixels_to_output * output_channels * bpp,
                        output_channels * output_slice_width * bpp,
                        output_width * output_channels * bpp);

                dmaStartListTask(ref_in_out);
                dmaWaitTask(ref_in_out);
            }

        }
    }

    SHAVE_HALT;

}

void relayout_deconvolution(t_RelayoutParams *params)
{
    u8  *input          = (u8 *)params->input;
    u8  *output         = (u8 *)params->output;
    s32 width           = params->width;
    s32 height          = params->height;
    s32 input_channels  = params->no_channels;
    s32 radixX          = params->radixX;
    s32 radixY          = params->radixY;
    s32 bpp             = params->inOutBpp;
    s32 kernel_stride_X = params->kernelStrideX;
    s32 kernel_stride_Y = params->kernelStrideY;
    s32 input_stride    = params->input_stride;
    s32 output_stride   = params->output_stride;
    s32 pad_top         = params->pad_top;
    s32 pad_bottom      = params->pad_bottom;
    s32 pad_left        = params->pad_left;
    s32 pad_right       = params->pad_right;

    s32 input_channels_size = input_channels * bpp;
    s32 output_channels     = input_channels * radixX * radixY;

    s32 input_width = width + pad_left + pad_right;
    if(kernel_stride_X > 1)
        input_width += (width - 1) * (kernel_stride_X - 1);

    s32 input_height = height + pad_top + pad_bottom;
    if(kernel_stride_Y > 1)
        input_height += (height - 1) * (kernel_stride_Y - 1);

    dmaTransactionList_t task_in_out;
    dmaTransactionList_t *ref_in_out;
    s32 id1 = dmaInitRequesterWithAgent(1, params->dmaLinkAgent);

    s32 slice_width  = input_width;
    s32 slice_height = input_height;
    s32 out_slice_width       = slice_width  - (radixX - 1);
    s32 out_slice_height      = slice_height - (radixY - 1);

    s32 input_buffer_size = slice_width * slice_height * input_channels_size;
    s32 relayout_buffer_size = out_slice_width * out_slice_height * radixX * radixY * input_channels_size;

    while(input_buffer_size + relayout_buffer_size > CMX_SIZE && slice_height > radixY)
    {
        --slice_height;
        --out_slice_height;
        input_buffer_size = slice_width * slice_height * input_channels_size;
        relayout_buffer_size = out_slice_width * out_slice_height * radixX * radixY * input_channels_size;
    }

    while(input_buffer_size + relayout_buffer_size > CMX_SIZE && slice_width > radixX)
    {
        --slice_width;
        --out_slice_width;
        input_buffer_size = slice_width * slice_height * input_channels_size;
        relayout_buffer_size = out_slice_width * out_slice_height * radixX * radixY * input_channels_size;
    }

    s32 slice_X_stride = slice_width  - (radixX - 1);
    s32 slice_Y_stride = slice_height - (radixY - 1);

    u8 *cmx_input_buffer    = params->cmxslice;
    u8 *cmx_relayout_buffer = cmx_input_buffer + input_buffer_size * bpp;

    s32 last_out_slice_width  = out_slice_width;
    s32 last_out_slice_height = out_slice_height;

    s32 no_width_slices = (input_width - (radixX - 1)) / out_slice_width;
    if((input_width - (radixX- 1)) % out_slice_width)
    {
        last_out_slice_width = (input_width - (radixX - 1)) % out_slice_width;
        ++no_width_slices;
    }

    s32 no_height_slices = (input_height - (radixY - 1)) / out_slice_height;
    if((input_height - (radixY- 1)) % out_slice_height)
    {
        last_out_slice_height = (input_height - (radixY - 1)) % out_slice_height;
        ++no_height_slices;
    }

    for(s32 h_slice_i = 0; h_slice_i < no_height_slices; ++h_slice_i)
    {
        s32 current_slice_height = h_slice_i == (no_height_slices - 1) ? last_out_slice_height : out_slice_height;

        for(s32 w_slice_i = 0; w_slice_i < no_width_slices; ++w_slice_i)
        {
            s32 current_slice_width = w_slice_i == (no_width_slices - 1) ? last_out_slice_width : out_slice_width;

            memset((void *)cmx_input_buffer, 0, input_buffer_size);

            // Row and column index in the padded and "interpolated"
            // input volume of the first and last non zero value pixel
            s32 first_pixel_row = h_slice_i * slice_Y_stride;
            s32 first_pixel_col = w_slice_i * slice_X_stride;

            s32 last_pixel_col = first_pixel_col + slice_width - 1;
            s32 last_pixel_row = first_pixel_row + slice_height - 1;

            // Row and column index in the input volume.
            s32 input_row = 0;
            s32 input_col = 0;
            s32 t_pad_size = 0;
            s32 l_pad_size = 0;

            // Nothing to transfer if the slice is padding only.
            if((last_pixel_col >= pad_left) &&
                    (first_pixel_col < (input_width - pad_right)) &&
                    (last_pixel_row >= pad_top) &&
                    (first_pixel_row < (input_height - pad_bottom)))
            {
                first_pixel_row -= pad_top;
                if(first_pixel_row < 0)
                {
                    t_pad_size = -first_pixel_row;
                    first_pixel_row = 0;
                }

                if(first_pixel_row % kernel_stride_Y != 0)
                {
                    t_pad_size += (first_pixel_row % kernel_stride_Y);
                    first_pixel_row += (kernel_stride_Y - (first_pixel_row % kernel_stride_Y));
                }

                input_row = first_pixel_row / kernel_stride_Y;

                if(last_pixel_row >= (input_height - pad_bottom))
                    last_pixel_row = input_height - pad_bottom - 1;

                if((last_pixel_row - pad_top) % kernel_stride_Y != 0)
                    last_pixel_row -= ((last_pixel_row - pad_top) % kernel_stride_Y);

                s32 rows_to_transfer = (last_pixel_row - pad_top) / kernel_stride_Y - input_row + 1;

                first_pixel_col -= pad_left;
                if(first_pixel_col < 0)
                {
                    l_pad_size = -first_pixel_col;
                    first_pixel_col = 0;
                }

                if(first_pixel_col % kernel_stride_X != 0)
                {
                    l_pad_size += (first_pixel_col % kernel_stride_X);
                    first_pixel_col += (kernel_stride_X - (first_pixel_col % kernel_stride_X));
                }

                input_col = first_pixel_col / kernel_stride_X;

                if(last_pixel_col >= (input_width - pad_right))
                    last_pixel_col = input_width - pad_right - 1;

                if((last_pixel_col - pad_left) % kernel_stride_X != 0)
                    last_pixel_col -= ((last_pixel_col - pad_left) % kernel_stride_X);

                s32 cols_to_transfer = (last_pixel_col - pad_left) / kernel_stride_X - input_col + 1;

                s32 cmx_input_offset = (t_pad_size * slice_width + l_pad_size) * input_channels * bpp;

                s32 input_transfer_size       = cols_to_transfer * input_channels_size;
                s32 input_transfer_dst_width  = kernel_stride_X == 1 ? input_transfer_size : input_channels_size;
                s32 input_transfer_dst_stride = kernel_stride_X == 1 ? slice_width * input_channels_size : kernel_stride_X * input_channels_size;
                s32 ddr_input_offset = input_row * input_stride + input_col * input_channels_size;

                // Transfer input pixels one row at a time with corresponding
                // x and y stride. The resulting volume has zeros inserted
                // between transfered pixels according to the strides and padding.
                for(s32 transfer_i = 0; transfer_i < rows_to_transfer; ++transfer_i)
                {
                    // DMA Input slice;
                    ref_in_out = dmaCreateTransactionDstStride(
                            id1,
                            &task_in_out,
                            (u8 *)(input + ddr_input_offset),
                            (u8 *)(cmx_input_buffer + cmx_input_offset),
                            input_transfer_size,
                            input_transfer_dst_width,
                            input_transfer_dst_stride);

                    dmaStartListTask(ref_in_out);
                    dmaWaitTask(ref_in_out);

                    ddr_input_offset += input_stride;
                    cmx_input_offset += slice_width * kernel_stride_Y * input_channels_size;
                }
            }

            // Relayout
            s32 relayout_offset = 0;
            for(s32 row_i = 0; row_i < current_slice_height ; ++row_i)
            {
                for(s32 col_i = 0; col_i < current_slice_width; ++col_i)
                {
                    for(s32 ry_i = 0; ry_i < radixY; ++ry_i)
                    {
                        s32 input_offset = ((row_i + ry_i) * slice_width + col_i) * input_channels_size;

                        memcpy_asm((void *)(cmx_relayout_buffer + relayout_offset),
                                (void *)(cmx_input_buffer + input_offset),
                                (radixX * input_channels_size));

                        relayout_offset += radixX * input_channels_size;
                    }
                }
            }

            s32 output_offset = h_slice_i * slice_Y_stride * output_stride +
                    w_slice_i * slice_X_stride * output_channels * bpp;

            s32 pixels_to_output = current_slice_width * current_slice_height;

            ref_in_out = dmaCreateTransactionDstStride(
                    id1,
                    &task_in_out,
                    (u8 *)(cmx_relayout_buffer),
                    (u8 *)(output + output_offset),
                    pixels_to_output * output_channels * bpp,
                    output_channels * current_slice_width * bpp,
                    output_stride);

            dmaStartListTask(ref_in_out);
            dmaWaitTask(ref_in_out);

        }
    }

    SHAVE_HALT;

}
