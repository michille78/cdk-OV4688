///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Simple effect code
///

// 1: Includes
// ----------------------------------------------------------------------------
#include <mv_types.h>
#include <svuCommonShave.h>
#include <swcCdma.h>
#include <stdio.h>
#include <string.h>
#include "addV2Fp16.h"
#include "scaleFp16.h"
#include <moviVectorTypes.h>
#include "avgpoolMxN_core.h"
#include <mvAvgPoolMxNParam.h>
#include "arithmeticSubFp16ToFp16.h"
#include "mvTensor.h"

// 2:  Source Specific #defines and types  (typedef,enum,struct)
// ----------------------------------------------------------------------------
#define MIN(a, b) ((a)<(b)?(a):(b))
#define MAX(a, b) ((a)>(b)?(a):(b))

#define PADCONTENT_ZERO  0
#define PADCONTENT_AVG   1

// 3: Global Data (Only if absolutely necessary)
// ----------------------------------------------------------------------------
// 4: Static Local Data
// ----------------------------------------------------------------------------
// 5: Static Function Prototypes
// ----------------------------------------------------------------------------
// 6: Functions Implementation
// ----------------------------------------------------------------------------

static void addMx1_kernel(u32 dmaId, dmaTransactionList_t* dmaRef, dmaTransactionList_t* dmaTask,
    u8* linesBuffer, u8* outputBuffer, u32 radix, u32 elemDist, u32 numElem)
{
    u32 x, span, hasIntermediateOut;
    half* inputLines[2];
    half* outputLines[1];

    x = 1;
    span = radix;
    hasIntermediateOut = 0;

    do
    {
        if (span%2 == 1)
        {
            inputLines[0] = (half*)linesBuffer + x * (span-1) * elemDist;
            inputLines[1] = (half*)outputBuffer;
            outputLines[0] = (half*)outputBuffer;
            if (hasIntermediateOut == 0)
            {
                hasIntermediateOut = 1;
                // this is the first element written to the output buffer -> a simple copy is sufficient

                dmaRef = dmaCreateTransaction(dmaId, dmaTask, (u8*)inputLines[0], (u8*)outputLines[0], numElem * INPUT_BPP);
                dmaStartListTask(dmaRef);
                dmaWaitTask(dmaRef);
            }
            else
            {
                mvcvAddV2Fp16_asm((half**)outputLines, (half**)inputLines, numElem);
            }
        }

        inputLines[0] = (half*)linesBuffer + x * elemDist;
        inputLines[1] = (half*)linesBuffer;
        outputLines[0] = (half*)linesBuffer;

        x = x*2;
        span = span/2;

        // last iteration writes to output buffer
        if (span == 1 && !hasIntermediateOut)
        {
            outputLines[0] = (half*)outputBuffer;
        }

        mvcvAddV2Fp16_asm((half**)outputLines, (half**)inputLines, numElem);
    } while (span > 1);

    if (hasIntermediateOut)
    {
        inputLines[0] = (half*)linesBuffer;
        inputLines[1] = (half*)outputBuffer;
        outputLines[0] = (half*)outputBuffer;

        mvcvAddV2Fp16_asm((half**)outputLines, (half**)inputLines, numElem);
    }
}


void padAvgAfter(half* inArray, u32 dimIn, u32 dimOut, u32 padBefore, u32 padAfter, u32 padUserAfter,
                 u32 padContent, u32 radix, u32 stride, u32 sliceC)
{
    // padding after the array
    u32  sliceC_m8 = (sliceC/8) * 8;
    half* inputLines[2];
    half* outputLines[1];
    u32  i, j, k;
    half padScale;
    u32  padPos;

    // init all pad elements with 0
    for (j=0; j<padAfter; j++)
    {
        memset(inArray + (padBefore+dimIn+j) * sliceC, 0, sliceC * sizeof (half));
    }

    if (padContent == PADCONTENT_AVG && (padAfter - padUserAfter) > 0)
    {
        u32 sumFrom, sumTo, sumN;
        u32 iFirst, iLast;

        // compute the index from the output array which need to use avg padding
        iFirst = (padBefore + dimIn + padUserAfter - radix + 1 + stride - 1)/stride;
        iLast =  dimOut - 1;

        for (i = iFirst; i <= iLast; i++)
        {
            // sumFrom and sumTo are relative to the input array
            sumFrom  = i*stride;
            sumTo    = padBefore + dimIn + padUserAfter - 1;
            sumN     = sumTo - sumFrom + 1;

            padPos   = i*stride + radix - 1;
            padScale = (half)(radix - sumN)/sumN;

            for (k = sumFrom; k <= sumTo; k++)
            {
                inputLines[0]  = (half*)(inArray + (k) * sliceC);
                inputLines[1]  = (half*)(inArray + (padPos) * sliceC);
                outputLines[0] = (half*)(inArray + (padPos) * sliceC);

                // process the first k x 8 elements of slice C using the asm code
                if (sliceC_m8 > 0)
                    mvcvAddV2Fp16_asm((half**)outputLines, (half**)inputLines, sliceC_m8);

                // ... and the remainder with the C version
                inputLines[0]  += sliceC_m8;
                inputLines[1]  += sliceC_m8;
                outputLines[0] += sliceC_m8;
                mvcvAddV2Fp16((half**)outputLines, (half**)inputLines, sliceC - sliceC_m8);
            }

            // process the first k x 8 elements of slice C using the asm code
            inputLines[0]  = (half*)(inArray + (padPos) * sliceC);
            outputLines[0] = (half*)(inArray + (padPos) * sliceC);
            if (sliceC_m8 > 0)
                mvcvScaleFp16_asm(inputLines, outputLines, padScale, sliceC_m8);

            // ... and the remainder with the C version
            inputLines[0]  += sliceC_m8;
            outputLines[0] += sliceC_m8;
            mvcvScaleFp16(inputLines, outputLines, padScale, sliceC - sliceC_m8);
        }

        for (i = iLast; i >= iFirst; i--)
        {
            padPos   = i*stride + radix - 1;
            for (j = iFirst; j <= i-1; j++)
            {
                u32 padPosSub = j*stride + radix - 1;

                // process the first k x 8 elements of slice C using the asm code
                inputLines[0]  = (half*)(inArray + (padPos) * sliceC);
                inputLines[1]  = (half*)(inArray + (padPosSub) * sliceC);
                outputLines[0] = (half*)(inArray + (padPos) * sliceC);
                if (sliceC_m8 > 0)
                {
                    mvcvArithmeticSubFp16ToFp16_asm(&inputLines[0], &inputLines[1], &outputLines[0], sliceC_m8);
                }

                // ... and the remainder with the C version
                inputLines[0]  += sliceC_m8;;
                inputLines[1]  += sliceC_m8;;
                outputLines[0] += sliceC_m8;
                mvcvArithmeticSubFp16ToFp16(&inputLines[0], &inputLines[1], &outputLines[0], sliceC - sliceC_m8);
            }
        }
    }
}



void padAvgBefore(half* inArray, u32 padBefore, u32 padUserBefore, u32 padContent,
                 u32 radix, u32 stride, u32 sliceC)
{
    // padding after the array
    u32  sliceC_m8 = (sliceC/8) * 8;
    half* inputLines[2];
    half* outputLines[1];
    u32  i, j, k;
    half padScale;
    u32  padPos;

    // init all pad elements with 0
    for (j=0; j<padBefore; j++)
    {
        memset(inArray + j * sliceC, 0, sliceC * sizeof (half));
    }

    if (padContent == PADCONTENT_AVG && (padBefore - padUserBefore) > 0)
    {
        u32 sumFrom, sumTo, sumN;
        u32 iFirst, iLast;

        // compute the index from the output array which need to use avg padding
        iFirst = 0;
        iLast =  (padBefore - padUserBefore - 1)/stride;

        for (i = iFirst; i <= iLast; i++)
        {
            sumFrom  = padBefore;
            sumTo    = i*stride + radix - 1;
            sumN     = sumTo - sumFrom + 1;

            padPos   = i*stride;
            padScale = (half)(radix - sumN)/sumN;

            for (k = sumFrom; k <= sumTo; k++)
            {
                inputLines[0]  = (half*)(inArray + (k) * sliceC);
                inputLines[1]  = (half*)(inArray + (padPos) * sliceC);
                outputLines[0] = (half*)(inArray + (padPos) * sliceC);

                // process the first k x 8 elements of slice C using the asm code
                if (sliceC_m8 > 0)
                    mvcvAddV2Fp16_asm((half**)outputLines, (half**)inputLines, sliceC_m8);

                // ... and the remainder with the C version
                inputLines[0]  += sliceC_m8;
                inputLines[1]  += sliceC_m8;
                outputLines[0] += sliceC_m8;
                mvcvAddV2Fp16((half**)outputLines, (half**)inputLines, sliceC - sliceC_m8);
            }

            // process the first k x 8 elements of slice C using the asm code
            inputLines[0]  = (half*)(inArray + (padPos) * sliceC);
            outputLines[0] = (half*)(inArray + (padPos) * sliceC);
            if (sliceC_m8 > 0)
                mvcvScaleFp16_asm(inputLines, outputLines, padScale, sliceC_m8);

            // ... and the remainder with the C version
            inputLines[0]  += sliceC_m8;
            outputLines[0] += sliceC_m8;
            mvcvScaleFp16(inputLines, outputLines, padScale, sliceC - sliceC_m8);
        }

        for (i = iFirst; i <= iLast; i++)
        {
            padPos   = i*stride;
            for (j = i+1; j <= iLast; j++)
            {
                u32 padPosSub = j*stride;

                // process the first k x 8 elements of slice C using the asm code
                inputLines[0]  = (half*)(inArray + (padPos) * sliceC);
                inputLines[1]  = (half*)(inArray + (padPosSub) * sliceC);
                outputLines[0] = (half*)(inArray + (padPos) * sliceC);
                if (sliceC_m8 > 0)
                {
                    mvcvArithmeticSubFp16ToFp16_asm(&inputLines[0], &inputLines[1], &outputLines[0], sliceC_m8);
                }

                // ... and the remainder with the C version
                inputLines[0]  += sliceC_m8;
                inputLines[1]  += sliceC_m8;
                outputLines[0] += sliceC_m8;
                mvcvArithmeticSubFp16ToFp16(&inputLines[0], &inputLines[1], &outputLines[0], sliceC - sliceC_m8);
            }
        }
    }
}


void mvAvgPoolMxN(t_MvAvgPoolMxNParam *p)
{
    dmaTransactionList_t *ref;
    dmaRequesterId dmaId;
    dmaTransactionList_t task1;
    u32 radixX = p->radixX, radixY = p->radixY;
    u32 strideX = p->strideX, strideY = p->strideY;
    // convenience padding; used to simplify computations
    u32 padConvLeft = radixX/2, padConvRight = radixX/2, padConvTop = radixY/2, padConvBottom = radixY/2;
    u32 H = p->height, HP = p->height + padConvTop + padConvBottom;
    u32 Hout, Wout;
    u32 W = p->width, WP;
    // user specified padding (used only for CAFFE-style padding)
    u32 padUserX = p->padX, padUserY = p->padY;
    // total padding: user + convenience padding
    u32 padLeft, padRight, padTop, padBottom;
    u32 C = p->channels;
    u32 sliceC = p->sliceC;
    u32 ostrideX = p->ostrideX;
    u8* inAddr = (u8*)p->input;
    u8* outAddr = (u8*)p->output;
    u8* interimAddr = (u8*)p->output;
    u32 i;
    u8* linesBuffer;
    u8* outputBuffer;
    u32 writtenElems = 0 , readElems = 0;
    u32 procLines, procCols;
    s32 Hpad, Wpad;
    half scale;
    u32 padContent = PADCONTENT_ZERO;
    half* inputLines[2];
    half* outputLines[1];
    u32 numElem;

    // dmaLinkAgent
    dmaId = dmaInitRequesterWithAgent(1, p->dmaLinkAgent);

    // set buffers to point to locations relative to cmxslice
    linesBuffer      = p->cmxslice;
    outputBuffer     = p->cmxslice + CMX_DATA_SIZE/2;

    padContent = PADCONTENT_ZERO;
    if (p->paddStyle == paddStyleTFSame)
    {
        // rules for TensorFlow SAME padding
        Hout = (H + strideY - 1) / strideY;
        Wout = (W + strideX - 1) / strideX;
        Hpad = ((Hout - 1) * strideY + radixY - H);
        Wpad = ((Wout - 1) * strideX + radixX - W);

        padConvLeft   = Wpad/2;
        padConvRight  = Wpad - padConvLeft;
        padConvTop    = Hpad/2;
        padConvBottom = Hpad - padConvTop;
        padUserX = 0;
        padUserY = 0;

        padContent = PADCONTENT_AVG;
    }
    else if (p->paddStyle == paddStyleCaffe)
    {
        // rules for CAFFE padding
        Hout = ((H + 2*padUserY - radixY + strideY - 1) / strideY) + 1;
        Wout = ((W + 2*padUserX - radixX + strideX - 1) / strideX) + 1;
        Hout = MIN(Hout, (H + padUserY + strideY - 1) / strideY);
        Wout = MIN(Wout, (W + padUserX + strideX - 1) / strideX);

        Hpad = ((Hout - 1) * strideY + radixY - H - 2*padUserY);
        Wpad = ((Wout - 1) * strideX + radixX - W - 2*padUserX);
        Hpad = MAX(0, Hpad);
        Wpad = MAX(0, Wpad);

        padConvLeft   = 0;
        padConvRight  = Wpad - padConvLeft;
        padConvTop    = 0;
        padConvBottom = Hpad - padConvTop;
        padContent = PADCONTENT_AVG;
    }
    else
    {
        // rules for TensorFlow VALID padding
        // treat unknown padding schemes as TF-VALID
        Hout = (H - radixY + 1 + strideY - 1) / strideY;
        Wout = (W - radixX + 1 + strideX - 1) / strideX;
        Hpad = Wpad = 0;
        padConvTop  = padConvBottom = 0;
        padConvLeft = padConvRight  = 0;
        padUserX = 0;
        padUserY = 0;
    }

    padLeft   = padUserX + padConvLeft;
    padRight  = padUserX + padConvRight;
    padTop    = padUserY + padConvTop;
    padBottom = padUserY + padConvBottom;

    HP = p->height + padTop + padBottom;
    WP = p->width + padLeft + padRight;

    scale = (half)1.0/(radixX*radixY);

    // Compute how many rows/cols we can store in the available memory
    // memory needed to store one row (+padding): WP*sliceC
    procLines = (CMX_DATA_SIZE/2 - 7*INPUT_BPP) / (WP*sliceC*INPUT_BPP);
    // memory needed to store one column (+padding): HP*sliceC
    procCols  = (CMX_DATA_SIZE/2 - 7*INPUT_BPP) / (HP*sliceC*INPUT_BPP);

    interimAddr = inAddr;
    if (Wout > W)
    {
        interimAddr = outAddr;
    }
    // apply horizontal kernel
    readElems = 0;
    writtenElems = 0;
    do
    {
        procLines = MIN(procLines, H-readElems);
        for (i=0; i<procLines; i++)
        {
            ref = dmaCreateTransactionFullOptions(dmaId, &task1, inAddr + readElems * W * C * INPUT_BPP,
                    linesBuffer + (i * WP + padLeft ) * sliceC * INPUT_BPP,
                    W * sliceC * INPUT_BPP,   // byte length
                    sliceC * INPUT_BPP,       // src width
                    sliceC * INPUT_BPP,       // dst width
                    C * INPUT_BPP,            // src stride
                    sliceC * INPUT_BPP);      // dst stride
            dmaStartListTask(ref);
            dmaWaitTask(ref);
            readElems += 1;
        }

        for (u32 line = 0; line<procLines; line++)
        {
            half* inLine = (half*) linesBuffer + line * WP * sliceC;

            // left padding
            padAvgBefore(inLine, padLeft, padUserX, padContent, radixX, strideX, sliceC);

            // right padding
            padAvgAfter(inLine, W, Wout, padLeft, padRight, padUserX, padContent, radixX, strideX, sliceC);
        }

        // align the processing width to the next multiple of 8 (constraint of the asm implementation)
        numElem = (sliceC * WP * procLines + 7) & ~7;
        addMx1_kernel(dmaId, ref, &task1, linesBuffer, outputBuffer, radixX, sliceC, numElem);

        for (i=0; i<procLines; i++)
        {
            ref = dmaCreateTransactionFullOptions(
                    dmaId, &task1,
                    outputBuffer + (i * WP * sliceC) * INPUT_BPP,
                    interimAddr + writtenElems * Wout * C * INPUT_BPP,
                    Wout * sliceC * INPUT_BPP,          // byte length
                    sliceC * INPUT_BPP,                 // src width
                    sliceC * INPUT_BPP,                 // dst width
                    strideX * sliceC * INPUT_BPP,       // src stride
                    C * INPUT_BPP);                     // dst stride
            dmaStartListTask(ref);
            dmaWaitTask(ref);
            writtenElems += 1 ;
        }
    } while (readElems < H);

    // apply vertical kernel
    readElems = 0;
    writtenElems = 0;

    do
    {
        procCols = MIN(procCols, Wout - readElems);
        for (i=0; i<procCols; i++)
        {
            ref = dmaCreateTransactionFullOptions(
                    dmaId, &task1, interimAddr + readElems * C * INPUT_BPP,
                    linesBuffer + (i * HP + padTop ) * sliceC * INPUT_BPP,
                    H * sliceC * INPUT_BPP,       // byte length
                    sliceC     * INPUT_BPP,       // src width
                    sliceC     * INPUT_BPP,       // dst width
                    Wout * C   * INPUT_BPP,       // src stride
                    sliceC     * INPUT_BPP);      // dst stride
            dmaStartListTask(ref);
            dmaWaitTask(ref);
            readElems += 1;
        }

        for (u32 col = 0; col<procCols; col++)
        {
            half* inCol = (half*) linesBuffer + col * HP * sliceC;
            // top padding
            padAvgBefore(inCol, padTop, padUserY, padContent, radixY, strideY, sliceC);

            // bottom padding
            padAvgAfter(inCol, H, Hout, padTop, padBottom, padUserY, padContent, radixY, strideY, sliceC);
        }

        // align the processing width to the next multiple of 8 (constraint of the asm implementation)
        numElem = (sliceC * HP * procCols + 7) & ~7;
        addMx1_kernel(dmaId, ref, &task1, linesBuffer, outputBuffer, radixY, sliceC, numElem);

        inputLines[0]  = (half*)outputBuffer;
        outputLines[0] = (half*)outputBuffer;
        mvcvScaleFp16_asm(inputLines, outputLines, scale, numElem);

        for (i=0; i<procCols; i++)
        {
            ref = dmaCreateTransactionFullOptions(
                    dmaId, &task1,
                    outputBuffer + (i * HP * sliceC) * INPUT_BPP,
                    outAddr + writtenElems * ostrideX,
                    Hout * sliceC * INPUT_BPP,          // byte length
                    sliceC * INPUT_BPP,                 // src width
                    sliceC * INPUT_BPP,                 // dst width
                    strideY * sliceC * INPUT_BPP,       // src stride
                    Wout * ostrideX);                   // dst stride
            dmaStartListTask(ref);
            dmaWaitTask(ref);
            writtenElems += 1;
        }
    } while (readElems < Wout);

    SHAVE_HALT;
}
