///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Simple effect code
///

// 1: Includes
// ----------------------------------------------------------------------------
#include <stdio.h>
#include <string.h>

#include <moviVectorUtils.h>
#include <swcCdma.h>
#include <svuCommonShave.h>
#include <mv_types.h>

#include <convolution7x7s2hhhh.h>
#include <accumulateFp16.h>
#include <mvConv7x7s2Param.h>
#include "conv7x7s2_core.h"

// 2:  Source Specific #defines and types  (typedef,enum,struct)
// ----------------------------------------------------------------------------
#define INPUT_BPP   2
#define FILTER_SIZE 7
#define STRIDE 2
#define MAX_K 3
#define BUFF_SZ 9  // 9-element circular buffer
#define PAD 8

// 3: Global Data (Only if absolutely necessary)
// ----------------------------------------------------------------------------
// 4: Static Local Data
// ----------------------------------------------------------------------------
// 5: Static Function Prototypes
// ----------------------------------------------------------------------------
// 6: Functions Implementation
// ----------------------------------------------------------------------------

void mvConv7x7s2(t_MvConv7x7s2Param *p) {
        u32 H = p->in_height, W = p->in_width;
        u32 K = p->in_channels, N = p->out_channels;
        u32 outW = W/2, outH = H/2;
        u32 padW = W + PAD;
        u32 in_line_size = K * W * INPUT_BPP;
        half* output = p->output, *input = p->input;
        half* weights = p->weights;
        half* weightsCMX;
        half* ping_inputCMX;
        half* line_zerosCMX;
        half* transposed_inputCMX;
        half* ping_outputCMX;
        half* pong_outputCMX;
        u32 k, n, i, j;
        half* input_lines[MAX_K][FILTER_SIZE];
        half* output_lines[1];
        u32 start_line_to_load = 0;
        u32 idx_out_row = p->start_out_row;
        u32 idx_last_loaded_line;
        u32 idx_processed_line;
        u32 lines_to_load;
        dmaTransactionList_t task_input;
        dmaTransactionList_t task_output;
        dmaTransactionList_t *ref_input;
        dmaTransactionList_t *ref_output;
        u32 id1;
        u32 offset_buffersCMX = 0;
        u32 weightsCMX_size = (FILTER_SIZE * FILTER_SIZE *
                               K * N * INPUT_BPP + 0xF) & (u32) ~0xF; // 18.375 KB
        u32 ping_inputCMX_size = (BUFF_SZ * W * K * INPUT_BPP + 0xF) & (u32) ~0xF; // 11.8125 KB
        u32 line_zerosCMX_size = ((W + 2*PAD) * INPUT_BPP + 0xF) & (u32) ~0xF; // 0.46875 KB
        u32 transposed_inputCMX_size = (BUFF_SZ * padW * K * INPUT_BPP + 0xF) & (u32) ~0xF; // 12.234375 KB
        u32 ping_outputCMX_size = (outW * N * INPUT_BPP + 0xF) & (u32) ~0xF; // 14 KB
        u32 pong_outputCMX_size = (outW * N * INPUT_BPP + 0xF) & (u32) ~0xF; // 14 KB

        weightsCMX = (half*)(p->cmxslice + offset_buffersCMX);
        offset_buffersCMX += weightsCMX_size;
        ping_inputCMX = (half*)(p->cmxslice + offset_buffersCMX);
        offset_buffersCMX += ping_inputCMX_size;
        line_zerosCMX = (half*)(p->cmxslice + offset_buffersCMX);
        bzero(line_zerosCMX, line_zerosCMX_size);
        offset_buffersCMX += line_zerosCMX_size;
        transposed_inputCMX = (half*)(p->cmxslice + offset_buffersCMX);
        offset_buffersCMX += transposed_inputCMX_size;
        ping_outputCMX = (half*)(p->cmxslice + offset_buffersCMX);
        offset_buffersCMX += ping_outputCMX_size;
        pong_outputCMX = (half*)(p->cmxslice + offset_buffersCMX);
        offset_buffersCMX += pong_outputCMX_size;

        // copy weights to CMX
        for (n = 0; n < N; n++)
                for (k = 0; k < K; k++)
                        for (j = 0; j < FILTER_SIZE; j++) // H
                                for (i = 0; i < FILTER_SIZE; i++) // W
                                        weightsCMX[n*K*FILTER_SIZE*FILTER_SIZE + k*FILTER_SIZE*FILTER_SIZE + j*FILTER_SIZE + i] = weights[j*FILTER_SIZE*K*N + i*K*N + k*N + n];

        id1 = dmaInitRequesterWithAgent(1, p->dmaLinkAgent);
        if (idx_out_row == 0) {
                // get (BUFF_SZ - 3) input lines from beginning
                ref_input = dmaCreateTransaction(
                        id1, &task_input,
                        (u8*)input,             // Src
                        (u8*)ping_inputCMX,     // Dst
                        (BUFF_SZ - 3) * in_line_size); // Byte Length
                dmaStartListTask(ref_input);
                dmaWaitTask(ref_input);
                idx_last_loaded_line = 5;
                idx_processed_line = 0;
        } else {
                // get BUFF_SZ input lines from start_out_row * STRIDE - 3
                ref_input = dmaCreateTransaction(
                        id1, &task_input,
                        (u8*)input + (p->start_out_row * STRIDE - 3) * in_line_size, // Src
                        (u8*)ping_inputCMX,                                // Dst
                        BUFF_SZ * in_line_size);                         // Byte Length
                dmaStartListTask(ref_input);
                dmaWaitTask(ref_input);
                idx_last_loaded_line = 8;
                idx_processed_line = 3;
        }
        start_line_to_load = p->start_out_row * STRIDE + 6;

        half8x8 result, slice;
        half8 row0, row1, row2, row3;
        half8 row4, row5, row6, row7;
        half8 zero = {0, 0, 0, 0, 0, 0, 0, 0};
        for (i = 0; i <= idx_last_loaded_line; i++) {
                for (j = 0; j < W / 8; j++) {
                        row0 = *((half8*)(ping_inputCMX + 0 * K +  j * K * 8 + i * W * K));
                        row1 = *((half8*)(ping_inputCMX + 1 * K +  j * K * 8 + i * W * K));
                        row2 = *((half8*)(ping_inputCMX + 2 * K +  j * K * 8 + i * W * K));
                        row3 = *((half8*)(ping_inputCMX + 3 * K +  j * K * 8 + i * W * K));
                        row4 = *((half8*)(ping_inputCMX + 4 * K +  j * K * 8 + i * W * K));
                        row5 = *((half8*)(ping_inputCMX + 5 * K +  j * K * 8 + i * W * K));
                        row6 = *((half8*)(ping_inputCMX + 6 * K +  j * K * 8 + i * W * K));
                        row7 = *((half8*)(ping_inputCMX + 7 * K +  j * K * 8 + i * W * K));
                        slice.rows[0] = row0;
                        slice.rows[1] = row1;
                        slice.rows[2] = row2;
                        slice.rows[3] = row3;
                        slice.rows[4] = row4;
                        slice.rows[5] = row5;
                        slice.rows[6] = row6;
                        slice.rows[7] = row7;
                        result = mvuTranspose(slice);
                        *((half8*) (transposed_inputCMX + 0 * BUFF_SZ * padW + j * 8 + i * padW)) = result.rows[0];
                        *((half8*) (transposed_inputCMX + 1 * BUFF_SZ * padW + j * 8 + i * padW)) = result.rows[1];
                        *((half8*) (transposed_inputCMX + 2 * BUFF_SZ * padW + j * 8 + i * padW)) = result.rows[2];
                }
        }
        for (i = 0; i <= BUFF_SZ; i++) {
                // left-right padding
                *((half8*) (transposed_inputCMX + 0 * BUFF_SZ * padW + j * 8 + i * padW)) = zero;
                *((half8*) (transposed_inputCMX + 1 * BUFF_SZ * padW + j * 8 + i * padW)) = zero;
                *((half8*) (transposed_inputCMX + 2 * BUFF_SZ * padW + j * 8 + i * padW)) = zero;
        }

        do {
                for (i = 0; i < MAX_K; i++) {
                        if (idx_out_row == 0) {
                                input_lines[i][0] = line_zerosCMX + PAD;
                                input_lines[i][1] = line_zerosCMX + PAD;
                                input_lines[i][2] = line_zerosCMX + PAD;
                                input_lines[i][3] = &transposed_inputCMX[0 + i * BUFF_SZ * padW];
                                input_lines[i][4] = &transposed_inputCMX[1 * padW + i * BUFF_SZ * padW];
                                input_lines[i][5] = &transposed_inputCMX[2 * padW + i * BUFF_SZ * padW];
                                input_lines[i][6] = &transposed_inputCMX[3 * padW + i * BUFF_SZ * padW];
                        } else if (idx_out_row == 1) {
                                input_lines[i][0] = line_zerosCMX + PAD;
                                input_lines[i][1] = &transposed_inputCMX[0 + i * BUFF_SZ * padW];
                                input_lines[i][2] = &transposed_inputCMX[1 * padW + i * BUFF_SZ * padW];
                                input_lines[i][3] = &transposed_inputCMX[2 * padW + i * BUFF_SZ * padW];
                                input_lines[i][4] = &transposed_inputCMX[3 * padW + i * BUFF_SZ * padW];
                                input_lines[i][5] = &transposed_inputCMX[4 * padW + i * BUFF_SZ * padW];
                                input_lines[i][6] = &transposed_inputCMX[5 * padW + i * BUFF_SZ * padW];
                        } else if (idx_out_row == outH - 1) {
                                input_lines[i][0] = &transposed_inputCMX[(idx_processed_line + BUFF_SZ - 3) % BUFF_SZ * padW + i * BUFF_SZ * padW];
                                input_lines[i][1] = &transposed_inputCMX[(idx_processed_line + BUFF_SZ - 2) % BUFF_SZ * padW + i * BUFF_SZ * padW];
                                input_lines[i][2] = &transposed_inputCMX[(idx_processed_line + BUFF_SZ - 1) % BUFF_SZ * padW + i * BUFF_SZ * padW];
                                input_lines[i][3] = &transposed_inputCMX[idx_processed_line * padW + i * BUFF_SZ * padW];
                                input_lines[i][4] = &transposed_inputCMX[(idx_processed_line + 1) % BUFF_SZ * padW + i * BUFF_SZ * padW];
                                input_lines[i][5] = line_zerosCMX + PAD;
                                input_lines[i][6] = line_zerosCMX + PAD;
                        } else {
                                input_lines[i][0] = &transposed_inputCMX[(idx_processed_line + BUFF_SZ - 3) % BUFF_SZ * padW + i * BUFF_SZ * padW];
                                input_lines[i][1] = &transposed_inputCMX[(idx_processed_line + BUFF_SZ - 2) % BUFF_SZ * padW + i * BUFF_SZ * padW];
                                input_lines[i][2] = &transposed_inputCMX[(idx_processed_line + BUFF_SZ - 1) % BUFF_SZ * padW + i * BUFF_SZ * padW];
                                input_lines[i][3] = &transposed_inputCMX[idx_processed_line * padW + i * BUFF_SZ * padW];
                                input_lines[i][4] = &transposed_inputCMX[(idx_processed_line + 1) % BUFF_SZ * padW + i * BUFF_SZ * padW];
                                input_lines[i][5] = &transposed_inputCMX[(idx_processed_line + 2) % BUFF_SZ * padW + i * BUFF_SZ * padW];
                                input_lines[i][6] = &transposed_inputCMX[(idx_processed_line + 3) % BUFF_SZ * padW + i * BUFF_SZ * padW];
                        }
                }

                // 2D convolutions + accumulate
                for (j = 0; j < N; j++) {
                        // channel 1
                        output_lines[0] = ping_outputCMX + j * outW;
                        mvcvConvolution7x7s2hhhh_asm(input_lines[0], output_lines, &weightsCMX[j*K*FILTER_SIZE*FILTER_SIZE], W);
                        // channel 2
                        output_lines[0] = pong_outputCMX + j * outW;
                        mvcvConvolution7x7s2hhhh_asm(input_lines[1], output_lines, &weightsCMX[j*K*FILTER_SIZE*FILTER_SIZE + 1*FILTER_SIZE*FILTER_SIZE], W);
                }
                mvcvAccumulateFp16_asm(&ping_outputCMX, &pong_outputCMX, N * outW);
                for (j = 0; j < N; j++) {
                        // channel 3
                        output_lines[0] = pong_outputCMX + j * outW;
                        mvcvConvolution7x7s2hhhh_asm(input_lines[2], output_lines, &weightsCMX[j*K*FILTER_SIZE*FILTER_SIZE + 2*FILTER_SIZE*FILTER_SIZE], W);
                }
                mvcvAccumulateFp16_asm(&ping_outputCMX, &pong_outputCMX, N * outW);

                // transpose output
                for (i = 0; i < N / 8; i++)
                        for (j = 0; j < outW / 8; j++) {
                                row0 = *((half8*) (ping_outputCMX + j * 8 + i * 8 * outW));
                                row1 = *((half8*) (ping_outputCMX + j * 8 + i * 8 * outW + 1 * outW));
                                row2 = *((half8*) (ping_outputCMX + j * 8 + i * 8 * outW + 2 * outW));
                                row3 = *((half8*) (ping_outputCMX + j * 8 + i * 8 * outW + 3 * outW));
                                row4 = *((half8*) (ping_outputCMX + j * 8 + i * 8 * outW + 4 * outW));
                                row5 = *((half8*) (ping_outputCMX + j * 8 + i * 8 * outW + 5 * outW));
                                row6 = *((half8*) (ping_outputCMX + j * 8 + i * 8 * outW + 6 * outW));
                                row7 = *((half8*) (ping_outputCMX + j * 8 + i * 8 * outW + 7 * outW));
                                slice.rows[0] = row0;
                                slice.rows[1] = row1;
                                slice.rows[2] = row2;
                                slice.rows[3] = row3;
                                slice.rows[4] = row4;
                                slice.rows[5] = row5;
                                slice.rows[6] = row6;
                                slice.rows[7] = row7;
                                result = mvuTranspose(slice);
                                *((half8*) (pong_outputCMX + i * 8 + j * 8 * N)) = result.rows[0];
                                *((half8*) (pong_outputCMX + i * 8 + j * 8 * N + 1 * N )) = result.rows[1];
                                *((half8*) (pong_outputCMX + i * 8 + j * 8 * N + 2 * N )) = result.rows[2];
                                *((half8*) (pong_outputCMX + i * 8 + j * 8 * N + 3 * N )) = result.rows[3];
                                *((half8*) (pong_outputCMX + i * 8 + j * 8 * N + 4 * N )) = result.rows[4];
                                *((half8*) (pong_outputCMX + i * 8 + j * 8 * N + 5 * N )) = result.rows[5];
                                *((half8*) (pong_outputCMX + i * 8 + j * 8 * N + 6 * N )) = result.rows[6];
                                *((half8*) (pong_outputCMX + i * 8 + j * 8 * N + 7 * N )) = result.rows[7];
                        }

                ref_output = dmaCreateTransaction(
                        id1, &task_output,
                        (u8*)pong_outputCMX,                            // Src
                        (u8*)(output + idx_out_row * outW * N),         // Dst
                        outW * N * INPUT_BPP);                  // Byte Length
                dmaStartListTask(ref_output);
                dmaWaitTask(ref_output);

                lines_to_load = 2;
                if (idx_out_row == outH - 1)
                        lines_to_load = 0;

                if (lines_to_load) {
                        ref_input = dmaCreateTransaction(
                                id1, &task_input,
                                (u8*)input + start_line_to_load * in_line_size,   // Src
                                (u8*)ping_inputCMX,                               // Dst
                                lines_to_load * in_line_size);            // Byte Length
                        dmaStartListTask(ref_input);
                        dmaWaitTask(ref_input);
                        start_line_to_load +=2;
                }

                // transpose
                for (i = 0; i < lines_to_load; i++) {
                        for (j = 0; j < W / 8; j++) {
                                row0 = *((half8*)(ping_inputCMX + 0 * K +  j * K * 8 + i * W * K));
                                row1 = *((half8*)(ping_inputCMX + 1 * K +  j * K * 8 + i * W * K));
                                row2 = *((half8*)(ping_inputCMX + 2 * K +  j * K * 8 + i * W * K));
                                row3 = *((half8*)(ping_inputCMX + 3 * K +  j * K * 8 + i * W * K));
                                row4 = *((half8*)(ping_inputCMX + 4 * K +  j * K * 8 + i * W * K));
                                row5 = *((half8*)(ping_inputCMX + 5 * K +  j * K * 8 + i * W * K));
                                row6 = *((half8*)(ping_inputCMX + 6 * K +  j * K * 8 + i * W * K));
                                row7 = *((half8*)(ping_inputCMX + 7 * K +  j * K * 8 + i * W * K));
                                slice.rows[0] = row0;
                                slice.rows[1] = row1;
                                slice.rows[2] = row2;
                                slice.rows[3] = row3;
                                slice.rows[4] = row4;
                                slice.rows[5] = row5;
                                slice.rows[6] = row6;
                                slice.rows[7] = row7;
                                result = mvuTranspose(slice);
                                *((half8*) (transposed_inputCMX + 0 * BUFF_SZ * padW + j * 8 + (idx_last_loaded_line + i + 1) % BUFF_SZ * padW)) = result.rows[0];
                                *((half8*) (transposed_inputCMX + 1 * BUFF_SZ * padW + j * 8 + (idx_last_loaded_line + i + 1) % BUFF_SZ * padW)) = result.rows[1];
                                *((half8*) (transposed_inputCMX + 2 * BUFF_SZ * padW + j * 8 + (idx_last_loaded_line + i + 1) % BUFF_SZ * padW)) = result.rows[2];
                        }
                        // left-right padding
                        *((half8*) (transposed_inputCMX + 0 * BUFF_SZ * padW + j * 8 + (idx_last_loaded_line + i + 1) % BUFF_SZ * padW)) = zero;
                        *((half8*) (transposed_inputCMX + 1 * BUFF_SZ * padW + j * 8 + (idx_last_loaded_line + i + 1) % BUFF_SZ * padW)) = zero;
                        *((half8*) (transposed_inputCMX + 2 * BUFF_SZ * padW + j * 8 + (idx_last_loaded_line + i + 1) % BUFF_SZ * padW)) = zero;
                }
                idx_out_row++;
                idx_processed_line = (idx_processed_line + 2) % BUFF_SZ;
                idx_last_loaded_line = (idx_last_loaded_line + 2) % BUFF_SZ;

        } while (idx_out_row != (p->start_out_row + p->out_rows));

        SHAVE_HALT;
}
