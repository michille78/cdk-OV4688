///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Simple effect header
///

#ifndef _MVFC_CORE_H_
#define _MVFC_CORE_H_

// 1: Includes
// ----------------------------------------------------------------------------
#include <mvFCParam.h>

// 2:  Exported Global Data (generally better to avoid)
// ----------------------------------------------------------------------------

// 3:  Exported Functions (non-inline)
// ----------------------------------------------------------------------------
#ifdef __cplusplus
extern "C"
{
#endif

void mvFC(t_MvFCParam *p);

#ifdef __cplusplus
}
#endif

#endif//__MVFC_CORE_H__
