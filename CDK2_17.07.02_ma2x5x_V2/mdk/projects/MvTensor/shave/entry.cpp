#include <stdio.h>
#include <assert.h>
#include <mv_types.h>
#include <mvTensorInternal.h>

u8 SLICE_BSS sData[MVTENSOR_HEAP_DATA_SIZE];

extern "C" void Entry(u32 entryAddr, u32 paramAddr, u32 paramVal1)
{
	assert(entryAddr != 0);
	assert(paramAddr != 0);

	((void (*)(void*, u32))entryAddr)((void*)paramAddr, paramVal1);

	SHAVE_HALT;
}