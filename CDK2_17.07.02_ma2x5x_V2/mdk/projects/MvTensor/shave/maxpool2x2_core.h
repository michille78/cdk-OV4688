///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Simple effect header
///

#ifndef _MVMAXPOOL2x2_CORE_H_
#define _MVMAXPOOL2x2_CORE_H_

// 1: Includes
// ----------------------------------------------------------------------------
#include <mvMaxPool2x2Param.h>

// 2:  Exported Global Data (generally better to avoid)
// ----------------------------------------------------------------------------

// 3:  Exported Functions (non-inline)
// ----------------------------------------------------------------------------
#ifdef __cplusplus
extern "C"
{
#endif

void mvMaxPool2x2(t_MvMaxPool2x2Param *p);

#ifdef __cplusplus
}
#endif

#endif//__MVMAXPOOL2x2_CORE_H__
