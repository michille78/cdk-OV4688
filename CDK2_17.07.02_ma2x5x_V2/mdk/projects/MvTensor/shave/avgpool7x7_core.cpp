///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Simple effect code
///

// 1: Includes
// ----------------------------------------------------------------------------
#include <mv_types.h>
#include <svuCommonShave.h>
#include <swcCdma.h>
#include <stdio.h>

#include <moviVectorTypes.h>
#include "avgpool7x7xk_core.h"
#include <mvAvgPool7x7xkParam.h>

// 2:  Source Specific #defines and types  (typedef,enum,struct)
// ----------------------------------------------------------------------------
#define INPUT_BPP 2

// 3: Global Data (Only if absolutely necessary)
// ----------------------------------------------------------------------------
// 4: Static Local Data
// ----------------------------------------------------------------------------
// 5: Static Function Prototypes
// ----------------------------------------------------------------------------
static void avgPool7x7xk(half** in, half **out, u32 k)
{
    u32 i, j;
    float sum;
    const float f49 = (float)0.02040816326530612;

    for( j = 0; j < k; j++)
    {
        sum = 0;
        for( i = 0; i < 49; i++)
            sum += in[i][j];
        out[0][j] = (half) (sum * f49);
    }
}
// 6: Functions Implementation
// ----------------------------------------------------------------------------

void mvAvgPool7x7xk(t_MvAvgPool7x7xkParam *p)
{
    u32 C = p->channels;
    u32 sliceC = p->sliceC;
    u32 i;
    u8* inAddress = (u8*) ((u32) p->input);
    u8* inputBuffer;
    u8* outputBuffer;
    half* inputLines[49];
    half* outputLine[1];
    dmaTransactionList_t task1;
    dmaTransactionList_t *ref1;
    u32 id1;

    // fix dmaLinkAgent
    id1 = dmaInitRequesterWithAgent(1, p->dmaLinkAgent);
    // id1 = dmaInitRequester(1)
                    ;
    // set buffers to point to locations relative to cmxslice
    // the memory allocation is done in MatMul (88000 bytes/slice)
    inputBuffer = p->cmxslice;
    outputBuffer = p->cmxslice + 81928;
    outputLine[0] = (half*)outputBuffer;

    ref1 = dmaCreateTransactionFullOptions(id1, &task1, inAddress,
                                           inputBuffer,
                                           7 * 7 * sliceC * INPUT_BPP, // byte length
                                           sliceC * INPUT_BPP,       // src width
                                           sliceC * INPUT_BPP,       // dst width
                                           C * INPUT_BPP,      // src stride
                                           sliceC * INPUT_BPP);    // dst stride
    dmaStartListTask(ref1);
    dmaWaitTask(ref1);

    for(i = 0 ; i < 49; i++)
        inputLines[i] = (half*)inputBuffer + i * sliceC;

    avgPool7x7xk(inputLines, outputLine, sliceC);

    ref1 = dmaCreateTransactionFullOptions(
                   id1, &task1,
                   outputBuffer,
                   (u8*)p->output,
                   1 * 1 * sliceC * INPUT_BPP, // byte length
                   sliceC * INPUT_BPP,       // src width
                   sliceC * INPUT_BPP,       // dst width
                   sliceC * INPUT_BPP,      // src stride
                   C * INPUT_BPP);    // dst stride
    dmaStartListTask(ref1);
    dmaWaitTask(ref1);

    SHAVE_HALT;
}
