///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Simple effect code
///

// 1: Includes
// ----------------------------------------------------------------------------
#include <mv_types.h>
#include <svuCommonShave.h>
#include <swcCdma.h>
#include <stdio.h>
#include <math.h>

#include <moviVectorTypes.h>
#include "softMax_core.h"
#include <mvSoftMaxParam.h>

// 2:  Source Specific #defines and types  (typedef,enum,struct)
// ----------------------------------------------------------------------------
#define INPUT_BPP 2

// 3: Global Data (Only if absolutely necessary)
// ----------------------------------------------------------------------------
// 4: Static Local Data
// ----------------------------------------------------------------------------
// 5: Static Function Prototypes
// ----------------------------------------------------------------------------
static half getMax(half *x, u32 h){
    u32 i;
    half max = x[0];
    for (i = 0; i < h; i++)
    {
        if(max < x[i]) max = x[i];
    }
    return max;
}


static void expAll(half *x,u32 h){
    u32 i;
    for (i = 0 ; i < h; i++)
    {
        x[i] = exp(x[i]);
    }
}

static void plusAll(half *x, u32 h, half value){
    u32 i;
    for (i = 0; i < h; i++)
    {
        x[i] = x[i] + value;
    }
}

static half sumAll(half *x, u32 h){
    u32 i;
    float sum = 0;
    for (i = 0; i < h; i++)
    {
        sum += x[i];
    }
    return (half)sum;
}

static void divideAll(half *x, u32 h, half value){
    u32 i;
    for (i = 0; i < h; i++)
    {
        x[i] = x[i] / value;
    }
}

// 6: Functions Implementation
// ----------------------------------------------------------------------------

void mvSoftMax(t_MvSoftMaxParam *p)
{
    u32 sliceC = p->sliceC;
    u32 i;
    u8* inAddress = (u8*) ((u32) p->input);
    u8* outAddress = (u8*) ((u32) p->output);
    u8* inputBuffer;
    u32 offset = 83952;
    dmaTransactionList_t task1;
    dmaTransactionList_t *ref1;
    u32 id1;

    // fix dmaLinkAgent
    id1 = dmaInitRequesterWithAgent(1, p->dmaLinkAgent);
    // id1 = dmaInitRequester(1);
    // set buffers to point to locations relative to cmxslice
    // the memory allocation is done in MatMul (88000 bytes/slice)
    inputBuffer = p->cmxslice;

    if((p->bufferOverflow == 1 && (p->stage == 1 || p->stage == 3 || p->stage == 5)) ||
        (p->bufferOverflow == 0 && p->stage == 1))
    {

    ref1 = dmaCreateTransaction(id1, &task1,
                                inAddress,
                                inputBuffer,
                                1 * 1 * sliceC * INPUT_BPP);
    dmaStartListTask(ref1);
    dmaWaitTask(ref1);
    }

    if(p->stage == 1)
    {
    // local max
    if( *(half*)(p->cmxslice0 + p->offset) < getMax((half*)inputBuffer, sliceC))
        *(half*)(p->cmxslice0 + p->offset) = getMax((half*)inputBuffer, sliceC);
    }
    else
    if(p->stage == 2)
    {
        // run only on 1 SHAVE
        for(i = 1; i < 12; i++)
            {
            // global max
            if( *((half*)(p->cmxslice0 + offset) + i) > *((half*)(p->cmxslice0 + offset)) )
                *((half*)(p->cmxslice0 + offset)) = *((half*)(p->cmxslice0 + offset) + i);
            }
    }
    else
    if(p->stage == 3)
    {
     plusAll((half*)inputBuffer, sliceC, -*((half*)(p->cmxslice0 + offset)));
     expAll((half*)inputBuffer, sliceC);

     // local sum
     *(half*)(p->cmxslice0 + p->offset + 12 * 2) += sumAll((half*)inputBuffer, sliceC);
    }
    else
    if(p->stage == 4)
    {
         // run only on 1 SHAVE
         for(i = 1; i < 12; i++)
         {
         // global sum
         *(half*)(p->cmxslice0 + offset + 12 * 2) += *((half*)(p->cmxslice0 + offset + 12 * 2)+ i);
         }
    }
    else
    {
    // p->stage == 5
    if(p->bufferOverflow == 1)
    {
        plusAll((half*)inputBuffer, sliceC, -*((half*)(p->cmxslice0 + offset)));
        expAll((half*)inputBuffer, sliceC);
    }
    divideAll((half*)inputBuffer, sliceC, *((half*)(p->cmxslice0 + offset + 12 * 2)));




    ref1 = dmaCreateTransaction(id1, &task1,
                                inputBuffer,
                                outAddress,
                                1 * 1 * sliceC * INPUT_BPP);
    dmaStartListTask(ref1);
    dmaWaitTask(ref1);
    }

    SHAVE_HALT;
}
