///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Simple effect code
///

// 1: Includes
// ----------------------------------------------------------------------------
#include <mv_types.h>
#include <svuCommonShave.h>
#include <swcCdma.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <moviVectorTypes.h>
#include "eltwise_core.h"

#define INPUT_BPP 2

void mvEltwise(t_MvEltwiseParam *p)
{
    dmaTransactionList_t task1;
    dmaTransactionList_t *ref1;
    u32 id1;
    id1 = dmaInitRequesterWithAgent(1, p->dmaLinkAgent);
    u32 max_slice = 41976 / (p->channels + p->channels2);   // Number of "pixels" to process at each stage
    half *cmxslice2 = p->cmxslice + (max_slice * p->channels + 7) / 8 * 8;
    for(u32 i = 0; i < p->nelements; i += max_slice)
    {
        u32 nelem = p->nelements - i;
        if(nelem > max_slice)
            nelem = max_slice;
        ref1 = dmaCreateTransactionFullOptions(id1, &task1, (u8 *)p->input + i * p->istrideX,
                                           (u8 *)p->cmxslice,
                                           nelem * p->channels * INPUT_BPP, // byte length
                                           p->channels * INPUT_BPP,       // src width
                                           p->channels * INPUT_BPP,       // dst width
                                           p->istrideX,                   // src stride
                                           p->channels * INPUT_BPP);      // dst stride
        dmaStartListTask(ref1);
        dmaWaitTask(ref1);
        ref1 = dmaCreateTransactionFullOptions(id1, &task1, (u8 *)p->input2 + i * p->i2strideX,
                                           (u8 *)cmxslice2,
                                           nelem * p->channels2 * INPUT_BPP, // byte length
                                           p->channels2 * INPUT_BPP,       // src width
                                           p->channels2 * INPUT_BPP,       // dst width
                                           p->i2strideX,                   // src stride
                                           p->channels2 * INPUT_BPP);      // dst stride
        dmaStartListTask(ref1);
        dmaWaitTask(ref1);
        switch(p->op)
        {
        case Eltwise_sum:
            for(u32 j = 0; j < nelem; j++)
            {
                u32 k;
                for(k = 0; k+7 < p->channels2; k += 8)
                    *(half8 *)&p->cmxslice[j * p->channels + k] += *(half8 *)&cmxslice2[j * p->channels2 + k];
                for(; k < p->channels2; k++)
                    p->cmxslice[j * p->channels + k] += cmxslice2[j * p->channels2 + k];
            }
            break;
        case Eltwise_prod:
            for(u32 j = 0; j < nelem; j++)
            {
                u32 k;
                for(k = 0; k+7 < p->channels2; k += 8)
                    *(half8 *)&p->cmxslice[j * p->channels + k] *= *(half8 *)&cmxslice2[j * p->channels2 + k];
                for(; k < p->channels2; k++)
                    p->cmxslice[j * p->channels + k] *= cmxslice2[j * p->channels2 + k];
            }
            break;
        case Eltwise_max:
            for(u32 j = 0; j < nelem; j++)
            {
                u32 k;
                for(k = 0; k+7 < p->channels2; k += 8)
                    *(half8 *)&p->cmxslice[j * p->channels + k] =
                        __builtin_shave_cmu_max_f16_rr_half8 (*(half8 *)&p->cmxslice[j * p->channels + k], *(half8 *)&cmxslice2[j * p->channels2 + k]);
                for(; k < p->channels2; k++)
                    p->cmxslice[j * p->channels + k] =
                        __builtin_shave_cmu_max_f16_rr_half(p->cmxslice[j * p->channels + k], cmxslice2[j * p->channels2 + k]);
            }
            break;
        }
        ref1 = dmaCreateTransactionFullOptions(id1, &task1, (u8 *)p->cmxslice,
                                           (u8 *)p->output + i * p->ostrideX,
                                           nelem * p->channels * INPUT_BPP, // byte length
                                           p->channels * INPUT_BPP,       // src width
                                           p->channels * INPUT_BPP,       // dst width
                                           p->channels * INPUT_BPP,       // src stride
                                           p->ostrideX);                  // dst stride
        dmaStartListTask(ref1);
        dmaWaitTask(ref1);
    }
    SHAVE_HALT;
}
