///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Simple effect header
///

#ifndef _MV_POSTOPS_CORE_H_
#define _MV_POSTOPS_CORE_H_

// 1: Includes
// ----------------------------------------------------------------------------
#include <mvPostOpsParam.h>

// 2:  Exported Global Data (generally better to avoid)
// ----------------------------------------------------------------------------

// 3:  Exported Functions (non-inline)
// ----------------------------------------------------------------------------
#ifdef __cplusplus
extern "C"
{
#endif

void postOps_core(t_PostOpsParams *params);

void reluFp16          (half8 * __restrict__ data_in, half8 * __restrict__ data_out, half8 * __restrict__ weights, half8 * __restrict__ bias, s32 no_lines, s32 line_size, half x, void *parameters);
void reluFp16WithBias  (half8 * __restrict__ data_in, half8 * __restrict__ data_out, half8 * __restrict__ weights, half8 * __restrict__ bias, s32 no_lines, s32 line_size, half x, void *parameters);
void reluNegSlopeFp16  (half8 * __restrict__ data_in, half8 * __restrict__ data_out, half8 * __restrict__ weights, half8 * __restrict__ bias, s32 no_lines, s32 line_size, half x, void *parameters);
void preluFp16         (half8 * __restrict__ data_in, half8 * __restrict__ data_out, half8 * __restrict__ weights, half8 * __restrict__ bias, s32 no_lines, s32 line_size, half x, void *parameters);
void biasFp16          (half8 * __restrict__ data_in, half8 * __restrict__ data_out, half8 * __restrict__ weights, half8 * __restrict__ bias, s32 no_lines, s32 line_size, half x, void *parameters);
void scaleFp16         (half8 * __restrict__ data_in, half8 * __restrict__ data_out, half8 * __restrict__ weights, half8 * __restrict__ bias, s32 no_lines, s32 line_size, half x, void *parameters);
void scaleFp16WithBias (half8 * __restrict__ data_in, half8 * __restrict__ data_out, half8 * __restrict__ weights, half8 * __restrict__ bias, s32 no_lines, s32 line_size, half x, void *parameters);
void squareFp16        (half8 * __restrict__ data_in, half8 * __restrict__ data_out, half8 * __restrict__ weights, half8 * __restrict__ bias, s32 no_lines, s32 line_size, half x, void *parameters);
void innerLRNFp16      (half8 * __restrict__ data_in, half8 * __restrict__ data_out, s32 no_lines, s32 line_size, half alpha, half beta);
void tanh_accurate     (half8 * __restrict__ data_in, half8 * __restrict__ data_out, half8 * __restrict__ weights, half8 * __restrict__ bias, s32 no_lines, s32 line_size, half x, void *parameters);
void tanh_fast         (half8 * __restrict__ data_in, half8 * __restrict__ data_out, half8 * __restrict__ weights, half8 * __restrict__ bias, s32 no_lines, s32 line_size, half x, void *parameters);
void sigmoid_accurate  (half8 * __restrict__ data_in, half8 * __restrict__ data_out, half8 * __restrict__ weights, half8 * __restrict__ bias, s32 no_lines, s32 line_size, half x, void *parameters);
void sigmoid_fast      (half8 * __restrict__ data_in, half8 * __restrict__ data_out, half8 * __restrict__ weights, half8 * __restrict__ bias, s32 no_lines, s32 line_size, half x, void *parameters);
void eluFp16           (half8 * __restrict__ data_in, half8 * __restrict__ data_out, half8 * __restrict__ weights, half8 * __restrict__ bias, s32 no_lines, s32 line_size, half x, void *parameters);
void powerFp16_accurate(half8 * __restrict__ data_in, half8 * __restrict__ data_out, half8 * __restrict__ weights, half8 * __restrict__ bias, s32 no_lines, s32 line_size, half x, void *parameters);
void powerFp16_fast    (half8 * __restrict__ data_in, half8 * __restrict__ data_out, half8 * __restrict__ weights, half8 * __restrict__ bias, s32 no_lines, s32 line_size, half x, void *parameters);

#ifdef __cplusplus
}
#endif

#endif /* _MV_PRELUFP16_CORE_H_ */
