///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Simple effect header
///

#ifndef _MVSPATIALCONV_CORE_H_
#define _MVSPATIALCONV_CORE_H_

// 1: Includes
// ----------------------------------------------------------------------------
#include <mvSpatialConvParam.h>

// Include MvCV kerneles for Convolutions
#include <convolution3x3Fp16ToFp16.h>
#include <convolution5x5Fp16ToFp16.h>
#include <convolution7x7Fp16ToFp16.h>
#include <convolution9x9Fp16ToFp16.h>
#include <convolution3x3s2hhhh.h>
#include <convolution3x3s3hhhh.h>
#include <convolution3x3s4hhhh.h>
#include <convolution3x3s8hhhh.h>
#include <convolution5x5s2hhhh.h>
#include <convolution5x5s3hhhh.h>
#include <convolution5x5s4hhhh.h>
#include <convolution5x5s8hhhh.h>
#include <convolution7x7s2hhhh.h>
#include <convolution7x7s3hhhh.h>
#include <convolution7x7s4hhhh.h>
#include <convolution7x7s8hhhh.h>
#include <convolution9x9s2hhhh.h>
#include <convolution9x9s3hhhh.h>
#include <convolution9x9s4hhhh.h>
#include <convolution9x9s8hhhh.h>
#include <convolution11x11s1hhhh.h>
#include <convolution11x11s2hhhh.h>
#include <convolution11x11s3hhhh.h>
#include <convolution11x11s4hhhh.h>
#include <convolution11x11s8hhhh.h>

typedef void (*FUNCPTR_T)(void **a,
                          void **b,
                          void  *c,
                          u32 width);

#ifndef SIZE_NO
#define SIZE_NO 5
#endif
#ifndef STRIDE_NO
#define STRIDE_NO 5
#endif

static FUNCPTR_T convMatrix[SIZE_NO][STRIDE_NO] =
{
  {
      //size 3x3
      (FUNCPTR_T)mvcvConvolution3x3Fp16ToFp16_asm,
      (FUNCPTR_T)mvcvConvolution3x3s2hhhh_asm,
      (FUNCPTR_T)mvcvConvolution3x3s3hhhh_asm,
      (FUNCPTR_T)mvcvConvolution3x3s4hhhh_asm,
      (FUNCPTR_T)mvcvConvolution3x3s8hhhh_asm,
  },

  {   //size 5x5
      (FUNCPTR_T)mvcvConvolution5x5Fp16ToFp16_asm,
      (FUNCPTR_T)mvcvConvolution5x5s2hhhh_asm,
      (FUNCPTR_T)mvcvConvolution5x5s3hhhh_asm,
      (FUNCPTR_T)mvcvConvolution5x5s4hhhh_asm,
      (FUNCPTR_T)mvcvConvolution5x5s8hhhh_asm,
  },

  {   //size 7x7
      (FUNCPTR_T)mvcvConvolution7x7Fp16ToFp16_asm,
      (FUNCPTR_T)mvcvConvolution7x7s2hhhh_asm,
      (FUNCPTR_T)mvcvConvolution7x7s3hhhh_asm,
      (FUNCPTR_T)mvcvConvolution7x7s4hhhh_asm,
      (FUNCPTR_T)mvcvConvolution7x7s8hhhh_asm,
  },

  {   //size 9x9
      (FUNCPTR_T)mvcvConvolution9x9Fp16ToFp16_asm,
      (FUNCPTR_T)mvcvConvolution9x9s2hhhh_asm,
      (FUNCPTR_T)mvcvConvolution9x9s3hhhh_asm,
      (FUNCPTR_T)mvcvConvolution9x9s4hhhh_asm,
      (FUNCPTR_T)mvcvConvolution9x9s8hhhh_asm,
  },

  {   // size 11x11
      (FUNCPTR_T)mvcvConvolution11x11s1hhhh_asm,
      (FUNCPTR_T)mvcvConvolution11x11s2hhhh_asm,
      (FUNCPTR_T)mvcvConvolution11x11s3hhhh_asm,
      (FUNCPTR_T)mvcvConvolution11x11s4hhhh_asm,
      (FUNCPTR_T)mvcvConvolution11x11s8hhhh_asm,
  }
};

// 2:  Exported Global Data (generally better to avoid)
// ----------------------------------------------------------------------------

// 3:  Exported Functions (non-inline)
// ----------------------------------------------------------------------------
#ifdef __cplusplus
extern "C"
{
#endif

void mvSpatialConv(t_MvSpatialConvParam *p);

#ifdef __cplusplus
}
#endif

#endif //__MVSPATIALCONV_CORE_H__
