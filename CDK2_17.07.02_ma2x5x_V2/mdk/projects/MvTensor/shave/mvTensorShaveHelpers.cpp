#include <svuCommonShave.h>
#include <mv_types.h>
#include <moviVectorTypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <moviVectorFunctions.h>
#include <accumulateFp16.h>
extern "C"
{

void printAddress(half *input,u32 start, u32 stop)
{
    printf("TEST\n");
    for(u32 i = start; i < stop; i++)
        printf("Elem %i: %f\n",i,  input[i]);

    SHAVE_HALT;
}

}
