///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Simple effect header
///

#ifndef _MVAVGPOOL7X7XK_CORE_H_
#define _MVAVGPOOL7X7XK_CORE_H_

// 1: Includes
// ----------------------------------------------------------------------------
#include <mvAvgPool7x7xkParam.h>

// 2:  Exported Global Data (generally better to avoid)
// ----------------------------------------------------------------------------

// 3:  Exported Functions (non-inline)
// ----------------------------------------------------------------------------
#ifdef __cplusplus
extern "C"
{
#endif

void mvAvgPool7x7xk(t_MvAvgPool7x7xkParam *p);

#ifdef __cplusplus
}
#endif

#endif//__MVAVGPOOL7X7XK_CORE_H__
