///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Simple effect code
///

// 1: Includes
// ----------------------------------------------------------------------------
#include <mv_types.h>
#include <svuCommonShave.h>
#include <swcCdma.h>
#include <stdio.h>

#include <moviVectorTypes.h>
#include "depthConv_core.h"
#include <mvDepthConvParam.h>
#include <convolution3x3Fp16ToFp16.h>
#include <convolution3x3s2hhhh.h>
#include <convolution3x3s3hhhh.h>
#include <convolution3x3s4hhhh.h>
#include <convolution3x3s8hhhh.h>
#include <convolution5x5Fp16ToFp16.h>
#include <convolution5x5s2hhhh.h>
#include <convolution5x5s3hhhh.h>
#include <convolution5x5s4hhhh.h>
#include <convolution5x5s8hhhh.h>
#include <convolution7x7Fp16ToFp16.h>
#include <convolution7x7s2hhhh.h>
#include <convolution7x7s3hhhh.h>
#include <convolution7x7s4hhhh.h>
#include <convolution7x7s8hhhh.h>
#include <convolution9x9Fp16ToFp16.h>
#include <convolution9x9s2hhhh.h>
#include <convolution9x9s3hhhh.h>
#include <convolution9x9s4hhhh.h>
#include <convolution9x9s8hhhh.h>

// 2:  Source Specific #defines and types  (typedef,enum,struct)
// ----------------------------------------------------------------------------
#define MIN(a, b) ((a)<(b)?(a):(b))
#define MAX(a, b) ((a)>(b)?(a):(b))
#define INPUT_BPP            2
#define MAX_FILTER_SIZE     15
#define CMX_SIZE          2000
#define USE_DMA              1

//#define DEBUG 1


// 3: Global Data (Only if absolutely necessary)
// ----------------------------------------------------------------------------
// 4: Static Local Data
// ----------------------------------------------------------------------------
// 5: Static Function Prototypes
// ----------------------------------------------------------------------------
// 6: Functions Implementation
// ----------------------------------------------------------------------------

void mvDepthConv(t_MvDepthConvParam *p) {
    u32 out_h = p->out_height, out_h_idx, h;
    u32 in_start_h, in_slice_h, out_start_h, out_slice_h;
    u32 out_w = p->out_width, out_w_idx, w, in_w;
    u32 H = p->in_height;
    u32 W = p->in_width;
    u32 stride_h = p->stride_h, stride_w = p->stride_w;
    u32 K = p->in_channels, N = p->channel_multiplier;
    u32 sliceK = p->in_channels_slice;
    u32 k, n, i, j;
    u32 kH = p->filter_height, kW = p->filter_width;
    half* output = p->output, *input = p->input;
    half* weights = p->weights;
    half* inputCMX;
    half* weightsCMX;
    half* outputCMX;
    half* inputLines[MAX_FILTER_SIZE];
    half* outputLines[1];
    u32 valid = 0;
    u32 padW;
    u32 padH;
    dmaTransactionList_t task_input;
    dmaTransactionList_t task_input_aux;
    dmaTransactionList_t task_output;
    dmaTransactionList_t *ref_input;
    dmaTransactionList_t *ref_input_aux;
    dmaTransactionList_t *ref_output;
    u32 id1;
    const int g_useDMA = USE_DMA;

    u32 size_weightsCMX = (MAX_FILTER_SIZE * MAX_FILTER_SIZE * INPUT_BPP + 0xF) & (u32)~0xF;
    u32 max_w           = ((out_w-1)*stride_w + kW + 7) & (u32)~0x7;
    u32 max_h_cmx       = (CMX_SIZE - size_weightsCMX) / max_w;
    u32 max_out_slice_h = 0;
    u32 max_in_slice_h  = 0;

    weightsCMX = (half*)(p->cmxslice + 0);
    inputCMX   = (half*)(p->cmxslice + size_weightsCMX);

    id1 = dmaInitRequesterWithAgent(1, p->dmaLinkAgent);

    max_out_slice_h = (max_h_cmx - 2*kH + 1 + stride_h)/(stride_h + 1);
    max_in_slice_h = (max_out_slice_h-1) * stride_h + kH;

    outputCMX  = (half*)(p->cmxslice + size_weightsCMX + (max_in_slice_h + kH - 1) * max_w*INPUT_BPP);

    // VALID - no padding (by default consider to have VALID padding
    valid = 1;
    in_w = W;
    padW = 0;
    padH = 0;
    // RVA: review the condition for SAME padding
    if (out_h == (H + stride_h - 1)/stride_h &&
        out_w == (W + stride_w - 1)/stride_w)
    {
        // SAME padding
        padW = kW/2;
        padH = kH/2;
        in_w = W + 2*padW;
        valid = 0;
    }

#ifdef DEBUG
    printf("max_out_slice_h: %d, max_in_slice_h: %d\n", max_out_slice_h, max_in_slice_h);
#endif

    for (k = 0; k < sliceK; k++)
    {
        for (out_start_h=0; out_start_h < out_h; out_start_h += out_slice_h)
        {
            // compute proc_h (number of output lines to process in one iteration)
            // inputCMX should fit proc_h + 2*padH rows

            out_slice_h = MIN(max_out_slice_h, out_h - out_start_h);
            in_start_h = out_start_h * stride_h;
            in_slice_h = MIN((out_slice_h - 1) * stride_h + kH, H - in_start_h);
#ifdef DEBUG
            printf("k=%-2d; in: %d .. %d;\t\tout: %d .. %d; \n", k, in_start_h, in_start_h+in_slice_h-1, out_start_h, out_start_h+out_slice_h-1);
#endif

            if (valid)
            {
                // copy input to CMX
                // this DMA transfer has quite little influence on performance; probably it depends on the transfer size
                if (g_useDMA)
                {
                    ref_input = dmaCreateTransactionFullOptions(id1, &task_input, (u8*)(input + in_start_h * W * K + k),
                            (u8*)inputCMX,
                            (in_slice_h + (kH-1)) * W * INPUT_BPP,   // byte length
                            1 * INPUT_BPP,       // src width
                            1 * INPUT_BPP,       // dst width
                            K * INPUT_BPP,       // src stride
                            1 * INPUT_BPP);      // dst stride

                    dmaStartListTask(ref_input);
                    dmaWaitTask(ref_input);
                }
                else
                {
                    for (h = 0; h < in_slice_h + (kH-1); h++)
                        for (w = 0; w < W; w++)
                            inputCMX[h*in_w + w] = input[(h+in_start_h) * W * K + w * K + k];
                }
            }
            else
            {
                // copy input to CMX
                if (g_useDMA)
                {
                    // use the outputCMX buffer as scratch to copy input unpadded
                    ref_input_aux = dmaCreateTransactionFullOptions(id1, &task_input_aux, (u8*)(input + (in_start_h-padH) * W * K + k),
                            (u8*)outputCMX,
                            (in_slice_h + 2*padH) * W * INPUT_BPP,   // byte length
                            1 * INPUT_BPP,       // src width
                            1 * INPUT_BPP,       // dst width
                            K * INPUT_BPP,       // src stride
                            1 * INPUT_BPP);      // dst stride

                    // copy outputCMX to inputCMX reserving room for left-right padding
                    ref_input = dmaCreateTransactionFullOptions(id1, &task_input, (u8*)outputCMX,
                            (u8*)(inputCMX + padW),
                            (in_slice_h + 2*padH) * W * INPUT_BPP,   // byte length
                            W * INPUT_BPP,                       // src width
                            W * INPUT_BPP,                       // dst width
                            W * INPUT_BPP,                       // src stride
                            (W+2*padW) * INPUT_BPP);             // dst stride

                    dmaLinkTasks(ref_input_aux, 1, &task_input);
                    dmaStartListTask(ref_input_aux);
                    dmaWaitTask(ref_input_aux);
                }
                else
                {
                    for (h = 0; h < in_slice_h + 2*padH; h++)
                        for (w = 0; w < W; w++)
                            inputCMX[h*in_w + (w+padW)] = input[(h+in_start_h-padH) * W * K + w * K + k];
                }

                // top padding
                if (in_start_h == 0)
                {
                    for (w = 0; w < in_w; w++)
                    {
                        for (h = 0; h < padH; h++)
                        {
                            inputCMX[ h*in_w + w] = 0.0;
                        }
                    }
                }

                // bottom padding
                if (in_start_h + in_slice_h == H)
                {
                    for (w = 0; w < in_w; w++)
                    {
                        for (h = 0; h < padH; h++)
                        {
                            inputCMX[(in_slice_h+padH+h)*in_w + w] = 0.0;
                        }
                    }
                }
                // apply vertical padding (left-right)
                for (h = 0; h < in_slice_h + 2*padH; h++)
                {
                    for (w = 0; w < padW; w++)
                    {
                        inputCMX[h*in_w + w]          = 0.0;
                        inputCMX[h*in_w + (W+padW+w)] = 0.0;
                    }
                }
            }
            for (n = 0; n < N; n++)
            {
                // copy weights to CMX
                for (j = 0; j < kH; j++)
                    for (i = 0; i < kW; i++)
                        weightsCMX[j*kW + i] = weights[j * kW * K * N + i * K * N + k * N + n];

                for (out_h_idx = 0; out_h_idx < out_slice_h; out_h_idx ++)
                {
                    u32 kernel_w;

                    for (j = 0; j < kH && j < MAX_FILTER_SIZE; j++)
                    {
                        inputLines[j] = &inputCMX[(out_h_idx*stride_h+j)*in_w + kW/2];
                    }
                    outputLines[0] = &outputCMX[out_h_idx*out_w];

                    // for the asm convolution kernels the width needs to be a multiple of 8
                    // -> align out_w to the next multiple of 8 (kernel_w)
                    kernel_w = (out_w*stride_w + 7) & (u32)~0x7;

                    if (kH == 3 && kW == 3 && stride_w == 1)
                    {
                        mvcvConvolution3x3Fp16ToFp16_asm(inputLines, outputLines, weightsCMX, kernel_w);
                    }
                    else if  (kH == 3 && kW == 3 && stride_w == 2)
                    {
                        mvcvConvolution3x3s2hhhh_asm(inputLines, outputLines, weightsCMX, kernel_w);
                    }
                    else if  (kH == 3 && kW == 3 && stride_w == 3)
                    {
                        mvcvConvolution3x3s3hhhh_asm(inputLines, outputLines, weightsCMX, kernel_w);
                    }
                    else if  (kH == 3 && kW == 3 && stride_w == 4)
                    {
                        mvcvConvolution3x3s4hhhh_asm(inputLines, outputLines, weightsCMX, kernel_w);
                    }
                    else if  (kH == 3 && kW == 3 && stride_w == 8)
                    {
                        mvcvConvolution3x3s8hhhh_asm(inputLines, outputLines, weightsCMX, kernel_w);
                    }
                    else if (kH == 5 && kW == 5 && stride_w == 1)
                    {
                        mvcvConvolution5x5Fp16ToFp16_asm(inputLines, outputLines, weightsCMX, kernel_w);
                    }
                    else if (kH == 5 && kW == 5 && stride_w == 2)
                    {
                        mvcvConvolution5x5s2hhhh_asm(inputLines, outputLines, weightsCMX, kernel_w);
                    }
                    else if (kH == 5 && kW == 5 && stride_w == 3 && kernel_w >= 24)
                    {
                        mvcvConvolution5x5s3hhhh_asm(inputLines, outputLines, weightsCMX, kernel_w);
                    }
                    else if (kH == 5 && kW == 5 && stride_w == 4)
                    {
                        mvcvConvolution5x5s4hhhh_asm(inputLines, outputLines, weightsCMX, kernel_w);
                    }
                    else if (kH == 5 && kW == 5 && stride_w == 8)
                    {
                        mvcvConvolution5x5s8hhhh_asm(inputLines, outputLines, weightsCMX, kernel_w);
                    }
                    else if (kH == 7 && kW == 7 && stride_w == 1)
                    {
                        // mvcvConvolution7x7Fp16ToFp16_asm already adds 7 to the width parameter,
                        // therefore alignment to the next multiple of 8 is not needed
                        kernel_w = out_w;
                        mvcvConvolution7x7Fp16ToFp16_asm(inputLines, outputLines, weightsCMX, kernel_w);
                    }
                    else if (kH == 7 && kW == 7 && stride_w == 2)
                    {
                        mvcvConvolution7x7s2hhhh_asm(inputLines, outputLines, weightsCMX, kernel_w);
                    }
                    else if (kH == 7 && kW == 7 && stride_w == 3 && kernel_w >= 24)
                    {
                        mvcvConvolution7x7s3hhhh_asm(inputLines, outputLines, weightsCMX, kernel_w);
                    }
                    else if (kH == 7 && kW == 7 && stride_w == 4)
                    {
                        mvcvConvolution7x7s4hhhh_asm(inputLines, outputLines, weightsCMX, kernel_w);
                    }
                    else if (kH == 7 && kW == 7 && stride_w == 8)
                    {
                        mvcvConvolution7x7s8hhhh_asm(inputLines, outputLines, weightsCMX, kernel_w);
                    }
                    else if (kH == 9 && kW == 9 && stride_w == 1)
                    {
                        mvcvConvolution9x9Fp16ToFp16_asm(inputLines, outputLines, weightsCMX, kernel_w);
                    }
                    else if (kH == 9 && kW == 9 && stride_w == 2)
                    {
                        mvcvConvolution9x9s2hhhh_asm(inputLines, outputLines, weightsCMX, kernel_w);
                    }
                    else if (kH == 9 && kW == 9 && stride_w == 3 && kernel_w >= 24)
                    {
                        mvcvConvolution9x9s3hhhh_asm(inputLines, outputLines, weightsCMX, kernel_w);
                    }
                    else if (kH == 9 && kW == 9 && stride_w == 4)
                    {
                        mvcvConvolution9x9s4hhhh_asm(inputLines, outputLines, weightsCMX, kernel_w);
                    }
                    else if (kH == 9 && kW == 9 && stride_w == 8)
                    {
                        mvcvConvolution9x9s8hhhh_asm(inputLines, outputLines, weightsCMX, kernel_w);
                    }
                    else
                    {
                        for (out_w_idx = 0; out_w_idx < out_w; out_w_idx ++)
                        {
                            outputCMX[out_h_idx*out_w + out_w_idx] = 0.0;
                            for (j = 0; j < kH; j++)
                            {
                                for (i = 0; i < kW; i++)
                                {
                                    outputCMX[out_h_idx*out_w + out_w_idx] +=
                                            inputCMX[(out_h_idx*stride_h+j)*in_w + out_w_idx*stride_w + i] * weightsCMX[j*kW + i];
                                }
                            }
                        }
                    }
                }

                if (g_useDMA)
                {
                    ref_output = dmaCreateTransactionFullOptions(id1, &task_output, (u8*)(outputCMX),
                            (u8*)(output + out_start_h * out_w * K * N + k * N + n),
                            out_w * out_slice_h * INPUT_BPP, // byte length
                            1 * INPUT_BPP,       // src width
                            1 * INPUT_BPP,       // dst width
                            1 * INPUT_BPP,       // src stride
                            K * N * INPUT_BPP);  // dst stride

                    dmaStartListTask(ref_output);
                    dmaWaitTask(ref_output);
                }
                else
                {
                    // copy output to DDR
                    for (out_h_idx = 0; out_h_idx < out_slice_h; out_h_idx ++)
                    {
                        for (out_w_idx = 0; out_w_idx < out_w; out_w_idx ++)
                        {
                            output[(out_start_h + out_h_idx) * out_w * K * N + out_w_idx * K * N + k * N + n] = outputCMX[out_h_idx*out_w + out_w_idx];
                        }
                    }
                }
            }
        }
    }

    SHAVE_HALT;
}
