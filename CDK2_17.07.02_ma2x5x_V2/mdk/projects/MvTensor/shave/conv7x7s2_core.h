///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Simple effect header
///

#ifndef _MVCONV7X7S2_CORE_H_
#define _MVCONV7X7S2_CORE_H_

// 1: Includes
// ----------------------------------------------------------------------------
#include <mvConv7x7s2Param.h>

// 2:  Exported Global Data (generally better to avoid)
// ----------------------------------------------------------------------------

// 3:  Exported Functions (non-inline)
// ----------------------------------------------------------------------------
#ifdef __cplusplus
extern "C"
{
#endif

void mvConv7x7s2(t_MvConv7x7s2Param *p);

#ifdef __cplusplus
}
#endif

#endif //__MVCONV7X7S2_CORE_H__
