///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief  Generic Spatial Convolution
///

// 1: Includes
// ----------------------------------------------------------------------------
#include <stdio.h>
#include <string.h>

#include <moviVectorUtils.h>
#include <swcCdma.h>
#include <svuCommonShave.h>
#include <mv_types.h>

#include <accumulateFp16.h>
#include <mvSpatialConvParam.h>
#include "spatialconv_core.h"

// 2:  Source Specific #defines and types  (typedef,enum,struct)
// ----------------------------------------------------------------------------
#define INPUT_BPP   2

// 3: Global Data (Only if absolutely necessary)
// ----------------------------------------------------------------------------
// 4: Static Local Data
// ----------------------------------------------------------------------------
// 5: Static Function Prototypes
// ----------------------------------------------------------------------------
// 6: Functions Implementation
// ----------------------------------------------------------------------------
#define align8(x) (((x) + 0x7) & (u32) ~0x7)
#define align16(x) (((x) + 0xF) & (u32) ~0xF)
#define align32(x) (((x) + 0x1F) & (u32) ~0x1F)

#define CMX_BUDGET (21000 * 4)
// kH: radix_y, sH: stride_y
#define req_cmx(kH,kW,sH,sW,lines,w,h,k,pad_t,pad_l,pad_b,pad_r,n) (( \
    align8(((w) + (pad_l) + (pad_r))*(k)*(lines)) + /*input*/ \
    align8(align8((w)+(pad_l)+(pad_r))*(lines)*(k)) + /*transposed*/ \
    align8(2*align8(((w)+(pad_l)+(pad_r)-(kW))/(sW)+1)*(n)) + /*output*/ \
    align8((w)+(pad_l)+(pad_r)) + /*zeros*/ \
    align8((kH)*(kW)*(k)*(n)) /*weights*/ \
    )*INPUT_BPP + \
    2 * align32((kH)*(k)*sizeof(half*)) /*addresses input/output_lines*/ \
)


void mvSpatialConv(t_MvSpatialConvParam *p) {
        u32 H = p->in_height, W = p->in_width;
        u32 K = p->in_channels, N = p->out_channels;
        half* output = p->output, *input = p->input;
        half* weights = p->weights;
        half* weightsCMX;
        half* ping_inputCMX;
        half* line_zerosCMX;
        half* transposed_inputCMX;
        half* ping_outputCMX;
        half* pong_outputCMX;
        u32 k, n, i, j;
        half** input_lines;
        half** output_lines;
        u32 idx_out_row = p->start_out_row;
        u32 lines_to_load;
        dmaTransactionList_t task_input;
        dmaTransactionList_t task_output;
        dmaTransactionList_t *ref_input;
        dmaTransactionList_t *ref_output;
        u32 id1;
        u32 offset_buffersCMX = 0;
        u32 kH = p->radix_y,  kW = p->radix_x;
        u32 sH = p->stride_y, sW = p->stride_x, lines;
        u32 pad_t, pad_l, pad_b, pad_r;
        // SAME_CAFFE:
        pad_t = pad_l = pad_b = pad_r = p->padding_type * (kH/2);
        u32 w, h;
        w = W; h = H; k = K; n = N;
        u32 total_parts, part;  // part = {0, 1, ..., total_parts - 1}
        u32 start_part = 1, middle_part = 0, end_part = 0;

        // Choose the right convolution function based on the input parameters
        FUNCPTR_T generic_conv = convMatrix[kH/2-1][(sH-1 > 4 ? 4 : sH-1)];

        lines = kH;
        total_parts = 1;
        part = 0;
        while (req_cmx(kH, kW, sH, sW, lines, w, h, k, \
                pad_t, kW /*pad_l*/, pad_b, kW /*pad_r*/, n) > CMX_BUDGET) {
                total_parts++;
                w = align8((W + total_parts - 1) / total_parts);
        }

        do {
            if (req_cmx(kH, kW, sH, sW, lines, w, h, k, \
                pad_t, kW /*pad_l*/, pad_b, kW /*pad_r*/, n) <= CMX_BUDGET) {
                    lines++;
                    continue;
                }
            break;
        } while (1);
        lines--;

        lines = lines > H ? H : lines;

        u32 outW_initial = (W + pad_l + pad_r - kW)/sW + 1;

        u32 weightsCMX_size = align16(kH * kW * k * n * INPUT_BPP);
        weightsCMX = (half*)(p->cmxslice + offset_buffersCMX);

        half *pweights_DDR, *pweights_CMX;
        pweights_DDR = weights;
        pweights_CMX = weightsCMX;
        for (i = 0; i < kW; i++) {
            for (j = 0; j < kH; j++) {
                for (k = 0; k < K; k++) {
                    for (n = 0; n < N; n++){
                        *pweights_CMX = *pweights_DDR;
                        pweights_DDR++;
                        pweights_CMX += kW * kH * K;
                    }
            pweights_CMX -= N * kW * kH * K;
            pweights_CMX += kW * kH;
           }
           pweights_CMX -= K * kW * kH;
           pweights_CMX++;
          }
        }

        u32 outW_offset = 0;
        id1 = dmaInitRequesterWithAgent(1, p->dmaLinkAgent);

        do {

            offset_buffersCMX = weightsCMX_size;
            lines_to_load = lines;

            pad_r = w/sW * sW - w - pad_l + kW - 1;

        if (part == total_parts - 1) {
            w = W - w * part;  // last line's width is the remaining
            pad_r = p->padding_type * (kH/2);
        }

        u32 outW = (w + pad_l + pad_r - kW)/sW + 1;
        u32 in_line_size = K * W * INPUT_BPP;
        u32 padW = w + pad_l + pad_r;  // needed aligned

        u32 ping_inputCMX_size = align16(lines * (w + pad_l + pad_r) * k * INPUT_BPP);
        u32 line_zerosCMX_size = align16((w + pad_l + pad_r) * INPUT_BPP);
        u32 transposed_inputCMX_size = align16(lines * align8(padW) * k * INPUT_BPP);
        u32 ping_outputCMX_size = align16(align8((w + pad_l + pad_r - kW)/sW + 1) * n * INPUT_BPP);
        u32 pong_outputCMX_size = align16(align8((w + pad_l + pad_r - kW)/sW + 1) * n * INPUT_BPP);
        u32 input_lines_size = align32(kH * k * sizeof(half*));

        ping_inputCMX = (half*)(p->cmxslice + offset_buffersCMX);
        offset_buffersCMX += ping_inputCMX_size;
        line_zerosCMX = (half*)(p->cmxslice + offset_buffersCMX);
        bzero((u8*)line_zerosCMX, line_zerosCMX_size);
        offset_buffersCMX += line_zerosCMX_size;
        transposed_inputCMX = (half*)(p->cmxslice + offset_buffersCMX);
        offset_buffersCMX += transposed_inputCMX_size;
        ping_outputCMX = (half*)(p->cmxslice + offset_buffersCMX);
        offset_buffersCMX += ping_outputCMX_size;
        pong_outputCMX = (half*)(p->cmxslice + offset_buffersCMX);
        offset_buffersCMX += pong_outputCMX_size;
        input_lines = (half**)(p->cmxslice + offset_buffersCMX);
        output_lines = (half**)(p->cmxslice + offset_buffersCMX + input_lines_size);

        // inA - input lines, bufB - circular buffer
        u32 start_inA = (s32)(idx_out_row * sH - pad_t) > 0 ? \
                        (idx_out_row * sH - pad_t) : 0;
        u32 start_bufB = 0;

        if (K == 1) {
            if (total_parts == 1)
            {
                ref_input = dmaCreateTransactionFullOptions(
                    id1, &task_input,
                    (u8*)input + start_inA * in_line_size,  // Src
                    (u8*)transposed_inputCMX,               // Dst
                    lines_to_load * in_line_size,           // Byte Length
                    w * INPUT_BPP,                          // SrcLineWidth
                    w * INPUT_BPP,                          // DstLineWidth,
                    w * INPUT_BPP,                          // SrcStride
                    align8(padW) * INPUT_BPP);              // DstStride
            }
            else {
             if(start_part) {
                ref_input = dmaCreateTransactionFullOptions(
                            id1, &task_input,
                            (u8*)input + start_inA * W * INPUT_BPP,   // Src
                            (u8*)transposed_inputCMX,                 // Dst
                            lines_to_load * (w + pad_r) * INPUT_BPP,  // Byte Length
                            (w + pad_r) * INPUT_BPP,                  // SrcLineWidth
                            (w + pad_r) * INPUT_BPP,                  // DstLineWidth,
                            W * INPUT_BPP,                            // SrcStride
                            align8(padW) * INPUT_BPP);                // DstStride
            }

                if(middle_part) {
                    ref_input = dmaCreateTransactionFullOptions(
                    id1, &task_input,
                    (u8*)(input + start_inA * W \
                          + part * w - pad_l),                         // Src
                    (u8*)(transposed_inputCMX - pad_l),                // Dst
                    lines_to_load * (w + pad_r + pad_l) * INPUT_BPP,   // Byte Length
                    (w + pad_r + pad_l) * INPUT_BPP,                   // SrcLineWidth
                    (w + pad_r + pad_l) * INPUT_BPP,                   // DstLineWidth,
                    W * INPUT_BPP,                                     // SrcStride
                    align8(padW) * INPUT_BPP);                         // DstStride
                }
            if(end_part) {
                ref_input = dmaCreateTransactionFullOptions(
                id1, &task_input,
                (u8*)(input + start_inA * W \
                      + part * w - pad_l),                  // Src
                (u8*)(transposed_inputCMX - pad_l),         // Dst
                lines_to_load * (w + pad_l) * INPUT_BPP,    // Byte Length
                (w + pad_l) * INPUT_BPP,                    // SrcLineWidth
                (w + pad_l) * INPUT_BPP,                    // DstLineWidth,
                W * INPUT_BPP,                              // SrcStride
                align8(padW) * INPUT_BPP);                  // DstStride
            }
        }
        }
        else
        {
            if (total_parts == 1) {
            ref_input = dmaCreateTransactionFullOptions(
                 id1, &task_input,
                 (u8*)input + start_inA * W * K * INPUT_BPP,  // Src
                 (u8*)ping_inputCMX,                          // Dst
                 lines_to_load * W * K * INPUT_BPP,           // Byte Length
                 W * K * INPUT_BPP,                           // SrcLineWidth
                 W * K * INPUT_BPP,                           // DstLineWidth
                 W * K * INPUT_BPP,                           // SrcStride
                 padW * K * INPUT_BPP);                       // DstStride
            }
            else {
              if (start_part) {
                  ref_input = dmaCreateTransactionFullOptions(
                             id1, &task_input,
                             (u8*)input + start_inA * W * K * INPUT_BPP,    // Src
                             (u8*)ping_inputCMX,                            // Dst
                             lines_to_load * (w + pad_r) *  K * INPUT_BPP,  // Byte Length
                             (w + pad_r) * K * INPUT_BPP,                   // SrcLineWidth
                             (w + pad_r) * K * INPUT_BPP,                   // DstLineWidth
                             W * K * INPUT_BPP,                             // SrcStride
                             padW * K * INPUT_BPP);                         // DstStride
             }
              if (middle_part) {
                      ref_input = dmaCreateTransactionFullOptions(
                      id1, &task_input,
                      (u8*)(input + (start_inA * W \
                            + part * w  - pad_l) * K),                      // Src
                      (u8*)ping_inputCMX,                                   // Dst
                      lines_to_load * (w + pad_r + pad_l) * K * INPUT_BPP,  // Byte Length
                      (w + pad_r + pad_l) * K * INPUT_BPP,                  // SrcLineWidth
                      (w + pad_r + pad_l) * K * INPUT_BPP,                  // DstLineWidth
                      W * K * INPUT_BPP,                                    // SrcStride
                      padW * K * INPUT_BPP);                                // DstStride
                  }

              if (end_part) {
                  ref_input = dmaCreateTransactionFullOptions(
                  id1, &task_input,
                  (u8*)(input + (start_inA * W \
                        + part * w - pad_l) * K),               // Src
                  (u8*)ping_inputCMX,                           // Dst
                  lines_to_load * (w + pad_l) * K * INPUT_BPP,  // Byte Length
                  (w + pad_l) * K * INPUT_BPP,                  // SrcLineWidth
                  (w + pad_l) * K * INPUT_BPP,                  // DstLineWidth
                  W * K * INPUT_BPP,                            // SrcStride
                  padW * K * INPUT_BPP);                        // DstStride
              }
            }
        }
        dmaStartListTask(ref_input);
        dmaWaitTask(ref_input);

        u32 end_inA  = kH - pad_t - 1;
        u32 end_bufB = start_bufB + lines - 1;
        u32 start_offset_bufB, end_offset_bufB;
        start_offset_bufB = end_offset_bufB = start_inA;
        // start/end_bufB + start/end_offset_bufB = start/end_inA
        // start/end_bufB = {0, 1, 2, ..., lines - 1}
        // start/end_inA  = {0, 1, 2, ..., H - 1}

        half *ptrans;

        if (K == 1) {
            // fast path
        }
        else
        {
            for (i = 0; i < lines; i++) {
                    for (j = 0; j < align8(padW) / 8; j++) {
                            half8 row0, row1, row2, row3;
                            half8 row4, row5, row6, row7;
                            half8x8 result, slice;
                            row0 = *((half8*)(ping_inputCMX + 0 * K +  j * K * 8 + i * padW * K));
                            row1 = *((half8*)(ping_inputCMX + 1 * K +  j * K * 8 + i * padW * K));
                            row2 = *((half8*)(ping_inputCMX + 2 * K +  j * K * 8 + i * padW * K));
                            row3 = *((half8*)(ping_inputCMX + 3 * K +  j * K * 8 + i * padW * K));
                            row4 = *((half8*)(ping_inputCMX + 4 * K +  j * K * 8 + i * padW * K));
                            row5 = *((half8*)(ping_inputCMX + 5 * K +  j * K * 8 + i * padW * K));
                            row6 = *((half8*)(ping_inputCMX + 6 * K +  j * K * 8 + i * padW * K));
                            row7 = *((half8*)(ping_inputCMX + 7 * K +  j * K * 8 + i * padW * K));
                            slice.rows[0] = row0;
                            slice.rows[1] = row1;
                            slice.rows[2] = row2;
                            slice.rows[3] = row3;
                            slice.rows[4] = row4;
                            slice.rows[5] = row5;
                            slice.rows[6] = row6;
                            slice.rows[7] = row7;
                            result = mvuTranspose(slice);
                            if (total_parts == 1) {
                            *((half8*) (transposed_inputCMX + j * 8 + i * align8(padW))) = result.rows[0];
                            if(K > 1)
                                *((half8*) (transposed_inputCMX + 1 * lines * align8(padW) + j * 8 + i * align8(padW))) = result.rows[1];
                            if( K > 2)
                                *((half8*) (transposed_inputCMX + 2 * lines * align8(padW) + j * 8 + i * align8(padW))) = result.rows[2];
                            if( K > 3)
                                *((half8*) (transposed_inputCMX + 3 * lines * align8(padW) + j * 8 + i * align8(padW))) = result.rows[3];
                            }
                            else
                            {
                                if (start_part) {
                                    *((half8*) (transposed_inputCMX + 0 * lines * align8(padW) + j * 8 + i * align8(padW))) = result.rows[0];
                                    if(K > 1)
                                        *((half8*) (transposed_inputCMX + 1 * lines * align8(padW) + j * 8 + i * align8(padW))) = result.rows[1];
                                    if( K > 2)
                                        *((half8*) (transposed_inputCMX + 2 * lines * align8(padW) + j * 8 + i * align8(padW))) = result.rows[2];
                                    if( K > 3)
                                        *((half8*) (transposed_inputCMX + 3 * lines * align8(padW) + j * 8 + i * align8(padW))) = result.rows[3];
                                }
                                if (middle_part)
                                {
                                    *((half8*) (transposed_inputCMX  - pad_l + 0 * lines * align8(padW) + j * 8 + i * align8(padW))) = result.rows[0];
                                    if(K > 1)
                                        *((half8*) (transposed_inputCMX - pad_l + 1 * lines * align8(padW) + j * 8 + i * align8(padW))) = result.rows[1];
                                    if( K > 2)
                                        *((half8*) (transposed_inputCMX - pad_l + 2 * lines * align8(padW) + j * 8 + i * align8(padW))) = result.rows[2];
                                    if( K > 3)
                                        *((half8*) (transposed_inputCMX - pad_l + 3 * lines * align8(padW) + j * 8 + i * align8(padW))) = result.rows[3];
                                }
                                if (end_part)
                                {
                                    *((half8*) (transposed_inputCMX  - pad_l + 0 * lines * align8(padW) + j * 8 + i * align8(padW))) = result.rows[0];
                                    if(K > 1)
                                        *((half8*) (transposed_inputCMX - pad_l + 1 * lines * align8(padW) + j * 8 + i * align8(padW))) = result.rows[1];
                                    if( K > 2)
                                        *((half8*) (transposed_inputCMX - pad_l + 2 * lines * align8(padW) + j * 8 + i * align8(padW))) = result.rows[2];
                                    if( K > 3)
                                        *((half8*) (transposed_inputCMX - pad_l + 3 * lines * align8(padW) + j * 8 + i * align8(padW))) = result.rows[3];
                                }
                            }
                    }
            }
        }
        if (pad_l || pad_r) {
            if (total_parts > 1) {
                for (i = 0; i < lines; i++) {

                if(start_part) {
                for (j = 0; j < pad_l; j++) {
                    *(transposed_inputCMX + align8(padW) - pad_l + i * align8(padW) + j)= 0;
                    if (K > 1)
                        *(transposed_inputCMX + 1 * lines * align8(padW) + align8(padW) - pad_l + i * align8(padW) + j)= 0;
                    if (K > 2)
                        *(transposed_inputCMX + 2 * lines * align8(padW) + align8(padW) - pad_l + i * align8(padW) + j)= 0;
                    if (K > 3)
                        *(transposed_inputCMX + 3 * lines * align8(padW) + align8(padW) - pad_l + i * align8(padW) + j)= 0;
                 }
                }
                else if (middle_part) {
                    //
                }
                else if (end_part) {
                for (j = 0; j < pad_r; j++) {
                    *(transposed_inputCMX + w + i * align8(padW) + j) = 0;
                    if (K > 1)
                        *(transposed_inputCMX + 1 * lines * align8(padW) + w + i * align8(padW) + j) = 0;
                    if (K > 2)
                        *(transposed_inputCMX + 2 * lines * align8(padW) + w + i * align8(padW) + j) = 0;
                    if (K > 3)
                        *(transposed_inputCMX + 3 * lines * align8(padW) + w + i * align8(padW) + j) = 0;
                 }
                }
               }
            }
            else {
            for (i = 0; i < lines; i++) {
                for (j = 0; j < pad_r; j++) {
                    *(transposed_inputCMX + w + i * align8(padW) + j)= 0;
                if (K > 1)
                    *(transposed_inputCMX + 1 * lines * align8(padW) + w + i * align8(padW) + j)= 0;
                if (K > 2)
                    *(transposed_inputCMX + 2 * lines * align8(padW) + w + i * align8(padW) + j)= 0;
                if (K > 3)
                    *(transposed_inputCMX + 3 * lines * align8(padW) + w + i * align8(padW) + j)= 0;

                }
                for (j = 0; j < pad_l; j++) {
                    *(transposed_inputCMX + align8(padW) - pad_l + i * align8(padW) + j)= 0;
                    if (K > 1)
                        *(transposed_inputCMX + 1 * lines * align8(padW) + align8(padW) - pad_l + i * align8(padW) +  j)= 0;
                    if (K > 2)
                        *(transposed_inputCMX + 2 * lines * align8(padW) + align8(padW) - pad_l + i * align8(padW) +  j)= 0;
                    if (K > 3)
                        *(transposed_inputCMX + 3 * lines * align8(padW) + align8(padW) - pad_l + i * align8(padW) +  j)= 0;
                    }
            }
            }
        }

        u32 top_pad_k_lines = (s32)(pad_t - idx_out_row * sH) > 0 ? \
                              (pad_t - idx_out_row * sH) : 0;
        u32 bottom_pad_k_lines = (s32)((idx_out_row * sH + kH) - (H + pad_t)) > 0 ? \
                                 ((idx_out_row * sH + kH) - (H + pad_t)) : 0;
        u32 input_k_lines = kH - top_pad_k_lines - bottom_pad_k_lines;

        do {
                // kH lines in the following layout:
                // [(input|pad)|(input|pad)|(input|pad)]
                input_lines = (half**)(p->cmxslice + offset_buffersCMX);
                for (i = 0; i < K; i++) {
                        u32 idx_buf = start_bufB;
                        s32 is_top_padding = top_pad_k_lines;
                        s32 is_input = input_k_lines;
                        s32 is_bottom_padding = bottom_pad_k_lines;

                        for (j = 0; j < kH; j++) {
                            if (is_top_padding) {
                                *input_lines = line_zerosCMX + pad_l;
                                is_top_padding--;
                                }
                            else if (is_input) {
                                ptrans = transposed_inputCMX + i * align8(padW) * lines + idx_buf * align8(padW) + kW/2 - pad_l;
                                *input_lines = ptrans;
                                idx_buf = (idx_buf + 1) % lines;
                                is_input--;
                                }
                            else if(is_bottom_padding) {
                                *input_lines = line_zerosCMX + pad_l;
                                is_bottom_padding--;
                                }
                            input_lines++;
                        }
                    }
                input_lines -= kH * K;

                input_lines = (half**)(p->cmxslice + offset_buffersCMX);
                // 2D convolutions + accumulate
                for (j = 0; j < N; j++) {
                        // channel 1
                        *output_lines = ping_outputCMX + j * outW;
                        generic_conv((void **)input_lines, (void **)output_lines,\
                                     (void *)&weightsCMX[j*K*kW*kH],\
                                     align8(outW * sW));
                    }
                if (K > 1)
                {
                    input_lines += kH;
                    for (j = 0; j < N; j++) {
                            // channel 2
                            *output_lines = pong_outputCMX + j * outW;
                            generic_conv((void **)input_lines, (void **)output_lines, \
                                         (void *)&weightsCMX[j*K*kW*kH + 1*kW*kH],\
                                         align8(outW * sW));
                    }
                    mvcvAccumulateFp16_asm(&ping_outputCMX, &pong_outputCMX, N * outW);
                }
                if (K > 2)
                {
                    input_lines += kH;
                    for (j = 0; j < N; j++) {
                            // channel 3
                            *output_lines = pong_outputCMX + j * outW;
                            generic_conv((void **)input_lines, (void **)output_lines,\
                                         (void *)&weightsCMX[j*K*kW*kH + 2*kW*kH],\
                                         align8(outW * sW));
                    }
                    mvcvAccumulateFp16_asm(&ping_outputCMX, &pong_outputCMX, N * outW);
                }
                if (K > 3)
                {
                    input_lines += kH;
                    for (j = 0; j < N; j++) {
                            // channel 4
                            *output_lines = pong_outputCMX + j * outW;
                            generic_conv((void **)input_lines, (void **)output_lines,\
                                         (void *)&weightsCMX[j*K*kW*kH + 3*kW*kH],\
                                         align8(outW * sW));
                    }
                    mvcvAccumulateFp16_asm(&ping_outputCMX, &pong_outputCMX, N * outW);
                }
                // transpose output
                for (i = 0; i < N / 8; i++)
                        for (j = 0; j < (outW + 8 - 1) / 8; j++) {
                                half8 row0, row1, row2, row3;
                                half8 row4, row5, row6, row7;
                                half8x8 result, slice;
                                row0 = *((half8*) (ping_outputCMX + j * 8 + i * 8 * outW));
                                row1 = *((half8*) (ping_outputCMX + j * 8 + i * 8 * outW + 1 * outW));
                                row2 = *((half8*) (ping_outputCMX + j * 8 + i * 8 * outW + 2 * outW));
                                row3 = *((half8*) (ping_outputCMX + j * 8 + i * 8 * outW + 3 * outW));
                                row4 = *((half8*) (ping_outputCMX + j * 8 + i * 8 * outW + 4 * outW));
                                row5 = *((half8*) (ping_outputCMX + j * 8 + i * 8 * outW + 5 * outW));
                                row6 = *((half8*) (ping_outputCMX + j * 8 + i * 8 * outW + 6 * outW));
                                row7 = *((half8*) (ping_outputCMX + j * 8 + i * 8 * outW + 7 * outW));
                                slice.rows[0] = row0;
                                slice.rows[1] = row1;
                                slice.rows[2] = row2;
                                slice.rows[3] = row3;
                                slice.rows[4] = row4;
                                slice.rows[5] = row5;
                                slice.rows[6] = row6;
                                slice.rows[7] = row7;
                                result = mvuTranspose(slice);
                                *((half8*) (pong_outputCMX + i * 8 + j * 8 * N)) = result.rows[0];
                                *((half8*) (pong_outputCMX + i * 8 + j * 8 * N + 1 * N )) = result.rows[1];
                                *((half8*) (pong_outputCMX + i * 8 + j * 8 * N + 2 * N )) = result.rows[2];
                                *((half8*) (pong_outputCMX + i * 8 + j * 8 * N + 3 * N )) = result.rows[3];
                                *((half8*) (pong_outputCMX + i * 8 + j * 8 * N + 4 * N )) = result.rows[4];
                                *((half8*) (pong_outputCMX + i * 8 + j * 8 * N + 5 * N )) = result.rows[5];
                                *((half8*) (pong_outputCMX + i * 8 + j * 8 * N + 6 * N )) = result.rows[6];
                                *((half8*) (pong_outputCMX + i * 8 + j * 8 * N + 7 * N )) = result.rows[7];
                        }

                ref_output = dmaCreateTransactionFullOptions(
                            id1, &task_output,
                            (u8*)pong_outputCMX,                                                // Src
                            (u8*)(output + idx_out_row * outW_initial * N + outW_offset * N),   // Dst
                            outW * N * INPUT_BPP,                                               // Byte Length
                            outW * N * INPUT_BPP,                                               // SrcLineWidth
                            outW * N * INPUT_BPP,                                               // DstLineWidth,
                            outW * N * INPUT_BPP,                                               // SrcStride
                            outW_initial * N * INPUT_BPP);                                      // DstStride
                dmaStartListTask(ref_output);
                dmaWaitTask(ref_output);
                idx_out_row++;

                start_inA = (s32)(idx_out_row * sH - pad_t) > 0 ? \
                            (idx_out_row * sH - pad_t) : 0;
                top_pad_k_lines = (s32)(pad_t - idx_out_row * sH) > 0 ? \
                                  (pad_t - idx_out_row * sH) : 0;
                bottom_pad_k_lines = (s32)((idx_out_row * sH + kH) - (H + pad_t)) > 0 ? \
                                     ((idx_out_row * sH + kH) - (H + pad_t)) : 0;
                input_k_lines = kH - top_pad_k_lines - bottom_pad_k_lines;
                end_inA = start_inA + input_k_lines - 1;

                lines_to_load = 0;
                if (K == 1) {
                    //fast path
                    start_bufB = start_inA - start_offset_bufB;
                    if (end_inA > end_bufB + end_offset_bufB)
                    {
                        lines_to_load = lines > H - start_inA ? \
                                H - start_inA : lines;
                        start_bufB = 0;
                        start_offset_bufB = end_offset_bufB = start_inA;
                        end_bufB = lines_to_load - 1;
                    }
                }
                else {
                    if ((start_inA >= start_bufB + start_offset_bufB) && \
                        (start_inA <= end_bufB + end_offset_bufB)) {

                        while ((start_bufB + start_offset_bufB) != start_inA)
                            start_bufB++;

                        start_offset_bufB += start_bufB / lines * lines;
                        start_bufB = start_bufB % lines;
                    }
                    else {
                        start_bufB = 0;
                        start_offset_bufB = start_inA;
                    }
                    if (end_inA > (end_bufB + end_offset_bufB)) {
                        while ( ((end_bufB +lines_to_load) % lines) != \
                                ((start_bufB + lines - 1 ) % lines) ) {
                            if (end_bufB + lines_to_load + end_offset_bufB == H - 1)
                                break;
                            lines_to_load++;
                        }
                    }
                }

                if (lines_to_load) {
                    id1 = dmaInitRequesterWithAgent(1, p->dmaLinkAgent);
                    if (K == 1) {
                        if (total_parts == 1) {
                        // fast path
                        ref_input = dmaCreateTransactionFullOptions(
                            id1, &task_input,
                            (u8*)input + start_inA * in_line_size,  // Src
                            (u8*)transposed_inputCMX,               // Dst
                            lines_to_load * in_line_size,           // Byte Length
                            w * INPUT_BPP,                          // SrcLineWidth
                            w * INPUT_BPP,                          // DstLineWidth,
                            w * INPUT_BPP,                          // SrcStride
                            align8(padW) * INPUT_BPP);                      // DstStride
                        }
                        else {
                            if (start_part) {
                                ref_input = dmaCreateTransactionFullOptions(
                                    id1, &task_input,
                                    (u8*)input + start_inA * W * K * INPUT_BPP,  // Src
                                    (u8*)transposed_inputCMX,                    // Dst
                                    lines_to_load * (w + pad_r) * INPUT_BPP,     // Byte Length
                                    (w + pad_r) * INPUT_BPP,                     // SrcLineWidth
                                    (w + pad_r) * INPUT_BPP,                     // DstLineWidth,
                                    W * INPUT_BPP,                               // SrcStride
                                    align8(padW) * INPUT_BPP);                   // DstStride
                            }
                            if (middle_part) {
                                ref_input = dmaCreateTransactionFullOptions(
                                    id1, &task_input,
                                    (u8*)input \
                                    + start_inA * W * K * INPUT_BPP \
                                    + part * w * K * INPUT_BPP \
                                    - pad_r * K * INPUT_BPP,                          // Src
                                    (u8*)(transposed_inputCMX - pad_l) ,              // Dst
                                    lines_to_load * (w + pad_r + pad_l) * INPUT_BPP,  // Byte Length
                                    (w + pad_r + pad_l) * INPUT_BPP,                  // SrcLineWidth
                                    (w + pad_r + pad_l) * INPUT_BPP,                  // DstLineWidth,
                                    W * INPUT_BPP,                                    // SrcStride
                                    align8(padW) * INPUT_BPP);                        // DstStride
                            }
                            if (end_part){
                                ref_input = dmaCreateTransactionFullOptions(
                                    id1, &task_input,
                                    (u8*)input \
                                    + start_inA * W * K * INPUT_BPP \
                                    + part * w * K * INPUT_BPP \
                                    - pad_r * K * INPUT_BPP,                  // Src
                                    (u8*)(transposed_inputCMX - pad_l) ,      // Dst
                                    lines_to_load * (w + pad_l) * INPUT_BPP,  // Byte Length
                                    (w + pad_l) * INPUT_BPP,                  // SrcLineWidth
                                    (w + pad_l) * INPUT_BPP,                  // DstLineWidth,
                                    W * INPUT_BPP,                            // SrcStride
                                    align8(padW) * INPUT_BPP);                // DstStride
                            }
                        }
                    }
                    else
                    {
                        if (total_parts == 1) {
                           // fast path
                           ref_input = dmaCreateTransactionFullOptions(
                               id1, &task_input,
                               (u8*)input + (end_bufB + end_offset_bufB + 1) * in_line_size,  // Src
                               (u8*)ping_inputCMX,                     // Dst
                               lines_to_load * in_line_size,           // Byte Length
                               w * K * INPUT_BPP,                      // SrcLineWidth
                               w * K * INPUT_BPP,                      // DstLineWidth
                               w * K * INPUT_BPP,                      // SrcStride
                               padW * K * INPUT_BPP);                  // DstStride
                           }
                           else {
                               if (start_part) {
                                   ref_input = dmaCreateTransactionFullOptions(
                                              id1, &task_input,
                                              (u8*)input + (end_bufB + end_offset_bufB + 1) * W * K * INPUT_BPP,    // Src
                                              (u8*)ping_inputCMX,                            // Dst
                                              lines_to_load * (w + pad_r) *  K * INPUT_BPP,  // Byte Length
                                              (w + pad_r) * K * INPUT_BPP,                   // SrcLineWidth
                                              (w + pad_r) * K * INPUT_BPP,                   // DstLineWidth
                                              W * K * INPUT_BPP,                             // SrcStride
                                              padW * K * INPUT_BPP);                         // DstStride
                              }
                               if (middle_part) {
                                       ref_input = dmaCreateTransactionFullOptions(
                                       id1, &task_input,
                                       (u8*)(input + ((end_bufB + end_offset_bufB + 1) * W \
                                             + part * w  - pad_l) * K),                      // Src
                                       (u8*)ping_inputCMX,                                   // Dst
                                       lines_to_load * (w + pad_r + pad_l) * K * INPUT_BPP,  // Byte Length
                                       (w + pad_r + pad_l) * K * INPUT_BPP,                  // SrcLineWidth
                                       (w + pad_r + pad_l) * K * INPUT_BPP,                  // DstLineWidth
                                       W * K * INPUT_BPP,                                    // SrcStride
                                       padW * K * INPUT_BPP);                                // DstStride
                                   }

                               if (end_part) {
                                   ref_input = dmaCreateTransactionFullOptions(
                                   id1, &task_input,
                                   (u8*)(input + ((end_bufB + end_offset_bufB + 1) * W \
                                         + part * w - pad_l) * K),               // Src
                                   (u8*)ping_inputCMX,                           // Dst
                                   lines_to_load * (w + pad_l) * K * INPUT_BPP,  // Byte Length
                                   (w + pad_l) * K * INPUT_BPP,                  // SrcLineWidth
                                   (w + pad_l) * K * INPUT_BPP,                  // DstLineWidth
                                   W * K * INPUT_BPP,                            // SrcStride
                                   padW * K * INPUT_BPP);                        // DstStride
                           }
                       }
                    }
                    dmaStartListTask(ref_input);
                    dmaWaitTask(ref_input);
                }


                // TODO: refactor transpose
                // transpose
                for (i = 0; i < lines_to_load; i++) {
                        if (K == 1) {
                            // fast path
                        }
                        else
                            for (j = 0; j < align8(padW) / 8; j++) {
                                    half8 row0, row1, row2, row3;
                                    half8 row4, row5, row6, row7;
                                    half8x8 result, slice;
                                    row0 = *((half8*)(ping_inputCMX + 0 * K +  j * K * 8 + i * padW * K));
                                    row1 = *((half8*)(ping_inputCMX + 1 * K +  j * K * 8 + i * padW * K));
                                    row2 = *((half8*)(ping_inputCMX + 2 * K +  j * K * 8 + i * padW * K));
                                    row3 = *((half8*)(ping_inputCMX + 3 * K +  j * K * 8 + i * padW * K));
                                    row4 = *((half8*)(ping_inputCMX + 4 * K +  j * K * 8 + i * padW * K));
                                    row5 = *((half8*)(ping_inputCMX + 5 * K +  j * K * 8 + i * padW * K));
                                    row6 = *((half8*)(ping_inputCMX + 6 * K +  j * K * 8 + i * padW * K));
                                    row7 = *((half8*)(ping_inputCMX + 7 * K +  j * K * 8 + i * padW * K));
                                    slice.rows[0] = row0;
                                    slice.rows[1] = row1;
                                    slice.rows[2] = row2;
                                    slice.rows[3] = row3;
                                    slice.rows[4] = row4;
                                    slice.rows[5] = row5;
                                    slice.rows[6] = row6;
                                    slice.rows[7] = row7;
                                    result = mvuTranspose(slice);
                                    if (total_parts == 1) {
                                    *((half8*) (transposed_inputCMX + 0 * lines * align8(padW) + j * 8 + (end_bufB + i + 1) % lines * align8(padW))) = result.rows[0];
                                    if (K > 1)
                                        *((half8*) (transposed_inputCMX + 1 * lines * align8(padW) + j * 8 + (end_bufB + i + 1) % lines * align8(padW))) = result.rows[1];
                                    if (K > 2)
                                        *((half8*) (transposed_inputCMX + 2 * lines * align8(padW) + j * 8 + (end_bufB + i + 1) % lines * align8(padW))) = result.rows[2];
                                    if (K > 3)
                                        *((half8*) (transposed_inputCMX + 3 * lines * align8(padW) + j * 8 + (end_bufB + i + 1) % lines * align8(padW))) = result.rows[3];
                                    }
                                    else
                                    {
                                        if (start_part) {
                                            *((half8*) (transposed_inputCMX + 0 * lines * align8(padW) + j * 8 + (end_bufB + i + 1) % lines * align8(padW))) = result.rows[0];
                                            if(K > 1)
                                                *((half8*) (transposed_inputCMX + 1 * lines * align8(padW) + j * 8 + (end_bufB + i + 1) % lines * align8(padW))) = result.rows[1];
                                            if( K > 2)
                                                *((half8*) (transposed_inputCMX + 2 * lines * align8(padW) + j * 8 + (end_bufB + i + 1) % lines * align8(padW))) = result.rows[2];
                                            if( K > 3)
                                                *((half8*) (transposed_inputCMX + 3 * lines * align8(padW) + j * 8 + (end_bufB + i + 1) % lines * align8(padW))) = result.rows[3];
                                        }
                                        if (middle_part)
                                        {
                                            *((half8*) (transposed_inputCMX  - pad_l + 0 * lines * align8(padW) + j * 8 + (end_bufB + i + 1) % lines * align8(padW))) = result.rows[0];
                                            if(K > 1)
                                                *((half8*) (transposed_inputCMX - pad_l + 1 * lines * align8(padW) + j * 8 + (end_bufB + i + 1) % lines * align8(padW))) = result.rows[1];
                                            if( K > 2)
                                                *((half8*) (transposed_inputCMX - pad_l + 2 * lines * align8(padW) + j * 8 + (end_bufB + i + 1) % lines * align8(padW))) = result.rows[2];
                                            if( K > 3)
                                                *((half8*) (transposed_inputCMX - pad_l + 3 * lines * align8(padW) + j * 8 + (end_bufB + i + 1) % lines * align8(padW))) = result.rows[3];
                                        }
                                        if (end_part)
                                        {
                                            *((half8*) (transposed_inputCMX  - pad_l + 0 * lines * align8(padW) + j * 8 + (end_bufB + i + 1) % lines * align8(padW))) = result.rows[0];
                                            if(K > 1)
                                                *((half8*) (transposed_inputCMX - pad_l + 1 * lines * align8(padW) + j * 8 + (end_bufB + i + 1) % lines * align8(padW))) = result.rows[1];
                                            if( K > 2)
                                                *((half8*) (transposed_inputCMX - pad_l + 2 * lines * align8(padW) + j * 8 + (end_bufB + i + 1) % lines * align8(padW))) = result.rows[2];
                                            if( K > 3)
                                                *((half8*) (transposed_inputCMX - pad_l + 3 * lines * align8(padW) + j * 8 + (end_bufB + i + 1) % lines * align8(padW))) = result.rows[3];
                                        }
                                    }
                            }
                        // left-right padding
                        if (pad_l || pad_r)
                        {
                            if (total_parts > 1)
                            {
                                if (start_part)
                                {
                                    for (j = 0; j < pad_l; j++) {
                                        *(transposed_inputCMX + align8(padW) - pad_l + (end_bufB + i + 1) % lines * align8(padW) + j)= 0;
                                        if (K > 1)
                                            *(transposed_inputCMX + 1 * lines * align8(padW) + align8(padW) - pad_l + (end_bufB + i + 1) % lines * align8(padW) + j)= 0;
                                        if (K > 2)
                                            *(transposed_inputCMX + 2 * lines * align8(padW) + align8(padW) - pad_l + (end_bufB + i + 1) % lines * align8(padW) + j)= 0;
                                        if (K > 3)
                                            *(transposed_inputCMX + 3 * lines * align8(padW) + align8(padW) - pad_l + (end_bufB + i + 1) % lines * align8(padW) + j)= 0;
                                    }
                                }
                                if (middle_part)
                                {
                                    //
                                }
                                if (end_part) {
                                    for (j = 0; j < pad_r; j++) {
                                        *(transposed_inputCMX + w + (end_bufB + i + 1) % lines * align8(padW) + j) = 0;
                                        if (K > 1)
                                            *(transposed_inputCMX + 1 * lines * align8(padW) + w + (end_bufB + i + 1) % lines * align8(padW) + j) = 0;
                                        if (K > 2)
                                            *(transposed_inputCMX + 2 * lines * align8(padW) + w + (end_bufB + i + 1) % lines * align8(padW) + j) = 0;
                                        if (K > 3)
                                            *(transposed_inputCMX + 3 * lines * align8(padW) + w + (end_bufB + i + 1) % lines * align8(padW) + j) = 0;
                                    }
                                }
                            }
                            else {
                            for (j = 0; j < pad_r; j++) {
                                *(transposed_inputCMX + w + (end_bufB + i + 1) % lines * align8(padW) + j)= 0;
                                if (K > 1)
                                    *(transposed_inputCMX + 1 * lines * align8(padW) + w + (end_bufB + i + 1) % lines * align8(padW) + j)= 0;
                                if (K > 2)
                                    *(transposed_inputCMX + 2 * lines * align8(padW) + w + (end_bufB + i + 1) % lines * align8(padW) + j)= 0;
                                if (K > 3)
                                    *(transposed_inputCMX + 3 * lines * align8(padW) + w + (end_bufB + i + 1) % lines * align8(padW) + j)= 0;
                            }
                            for (j = 0; j < pad_l; j++) {
                                *(transposed_inputCMX + align8(padW) - pad_l + (end_bufB + i + 1) % lines * align8(padW) + j)= 0;
                                if (K > 1)
                                    *(transposed_inputCMX + 1 * lines * align8(padW) + align8(padW) - pad_l + (end_bufB + i + 1) % lines * align8(padW) + j)= 0;
                                if (K > 2)
                                    *(transposed_inputCMX + 2 * lines * align8(padW) + align8(padW) - pad_l + (end_bufB + i + 1) % lines * align8(padW) + j)= 0;
                                if (K > 3)
                                    *(transposed_inputCMX + 3 * lines * align8(padW) + align8(padW) - pad_l + (end_bufB + i + 1) % lines * align8(padW) + j)= 0;
                            }
                            }
                        }
                }

                if( K > 1)
                if (lines_to_load) {
                    end_offset_bufB += (end_bufB + lines_to_load) / lines * lines;
                    end_bufB = (end_bufB + lines_to_load) % lines;
                }

        } while (idx_out_row != (p->start_out_row + p->out_rows));
        idx_out_row = p->start_out_row;
        part++;
        if(part == 1) {
            start_part = 0;
            middle_part = 1;
        }
        if (part == total_parts -1) {
            start_part = 0;
            middle_part = 0;
            end_part = 1;
        }

        outW_offset += outW;
        } while(part != total_parts);
        SHAVE_HALT;
}
