///
/// \file
/// \copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// \brief
/// 
/// \details
///


#ifndef _MV_AVGPOOL7x7xKPARAM_H
#define _MV_AVGPOOL7x7xKPARAM_H

#include <mv_types.h>

#ifdef __MOVICOMPILE__
    #include <moviVectorTypes.h>
#else
    typedef fp16 half;
#endif

/// MvAvgPool7x7xk global parameters structure
typedef struct
{
    half* input;
    u32 channels;
    u32 sliceC;
    u8* cmxslice;
    u32 dmaLinkAgent;
    half* output;
} t_MvAvgPool7x7xkParam;

#endif // _MV_AVGPOOL7x7xKPARAM_H
