///
/// \file
/// \copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// \brief
///
/// \details
///

#ifndef _MV_CROPPARAM_H_
#define _MV_CROPPARAM_H_

#include <mv_types.h>

#ifdef __MOVICOMPILE__
#include <moviVectorTypes.h>
#else
typedef fp16 half;
#endif

typedef struct
{
    s32 offset_dimX;
    s32 offset_dimY;
    s32 offset_dimZ;
} t_CropLayerParams;

#endif /* _MV_CROPPARAM_H_ */
