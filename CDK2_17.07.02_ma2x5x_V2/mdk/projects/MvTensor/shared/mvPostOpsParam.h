///
/// \file
/// \copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// \brief
///
/// \details
///

#ifndef _MV_POSTOPSPARAM_H_
#define _MV_POSTOPSPARAM_H_

#include <mv_types.h>

#ifdef __MOVICOMPILE__
#include <moviVectorTypes.h>
#else
typedef fp16 half;
#endif

enum PostOpType {RELU, RELU_NEG_SLOPE, PRELU, BIAS, SCALE, SQUARE,
    INNERLRN, TANH_ACCURATE, TANH_FAST, SIGMOID_ACCURATE, SIGMOID_FAST,
    ELU, POWER_ACCURATE, POWER_FAST};

/// Bias parameters
typedef struct
{
    half *input;
    half *output;
    half *weights; // pReLU and affine
    half *bias;    // ReLU, bias and affine
    u32   width;
    u32   height;
    u32   stride;
    u8*   cmxslice;
    u32   dmaLinkAgent;
    float x; // Used only by ReLU and x = alpha for ELU.
    PostOpType postOpType;
    void *params;
} t_PostOpsParams;

typedef struct
{
    float shift;
    float scale;
    float power;
} t_PowerLayerParams;

#endif /* _MV_POSTOPSPARAM_H_ */
