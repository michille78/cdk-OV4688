///
/// \file
/// \copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// \brief
///
/// \details
///

#ifndef _MV_CONV7x7S2PARAM_H_
#define _MV_CONV7x7S2PARAM_H_
#include <mv_types.h>

#ifdef __MOVICOMPILE__
    #include <moviVectorTypes.h>
#else
    typedef fp16 half;
#endif

/// mvConv7x7s2 global parameters structure
typedef struct
{
    half* input;
    u32 in_height;
    u32 in_width;
    u32 in_channels;
    u32 out_channels;
    u32 start_out_row;
    u32 out_rows;
    half* weights;
    half* output;
    u8* cmxslice;
    u32 dmaLinkAgent;
} t_MvConv7x7s2Param;

#endif  //_MV_CONV7x7S2PARAM_H_
