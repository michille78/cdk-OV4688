///
/// \file
/// \copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// \brief     MvTensor API -- interface to MvTensor compute library
///
/// \details   Library API definition
///


#ifndef _MV_TENSOR_H_
#define _MV_TENSOR_H_

#ifndef __MOVICOMPILE__
#ifdef __RTEMS__
#include <OsDrvCmxDma.h>
#else
#include <DrvCmxDma.h>
#endif
#else
#include <swcCdma.h>
#endif

#include <mv_types.h>
#include <mvTensorConfig.h>

#define L2C_LINE_SZ 64
#define MV_TENSOR_DMA_PRIORITY 25
#define MV_TENSOR_MAX_SHAVES 12

/// Generic macro to embed a structure alignment into it's definition so the
/// optimal alignment is enforced for all instantiations and even for arrays.
#define ALIGNED_STRUCT_INNER(fields, align)                                                           \
    typedef struct                                                                      \
    {                                                                                   \
        union                                                                           \
        {                                                                               \
            struct                                                                      \
            {                                                                           \
                fields                                                                  \
            };                                                                          \
            u8 padding[align] __attribute__((aligned(align)));                          \
        };                                                                              \
    }

/// Enforce alignment to the Leon/Shave L2 cache line size which is the highest
/// alignment needed on MA2x5x for best performance and safe data sharing.
#define ALIGNED_STRUCT(fields) ALIGNED_STRUCT_INNER(fields, L2C_LINE_SZ)

/// Optimization mask value that will say MvTensor to use the default optimization paths
#define MV_TENSOR_DEFAULT_OPT 0x80000000

/// MvTensor data type structure
typedef enum
{
    t_fp16,                  ///< half precision floating point
    t_u8f,                   ///< Unsigned byte
    t_int                    ///< Integer
}t_MvTensorDataType;

/// Optimizations
/// Naming Convention: opt_stge_x_y_sX_sY_optName. Please add fields as needed (rather than reflecting this in the optname)

typedef enum
{
  opt_conv_3_3_1_1_specific,
  opt_conv_im2col,
  opt_conv_im2col_v2,
  opt_conv_3_3_2_2_specific,
  opt_conv_5_5_1_1_specific,
  opt_conv_5_5_2_2_specific,
  opt_conv_7_7_2_2_specific,
  opt_maxpool_2_2_2_2_specific,
  opt_maxpool_3_3_1_1_specific,
  opt_conv_7_7_2_2_spatial,
  opt_conv_generic_spatial,
  opt_deconv_3_3_1_1_same_specific,
  opt_deconv_5_5_1_1_same_specific,
  opt_deconv_M_N_1_1_same_spatial,
  opt_deconv_M_N_1_1_same_generic,
  opt_power_accurate,
  opt_MAXIMUM_NAME_SIZE = 50,      // Used for buffer sizes as we cannot get the length of an Enum.
  opt_MAXIMUM_OPTIMIZATIONS = 40,  //  `` `` `` `` `` `` `` `` `` `` `` `` `` `` `` `` `` `` `` ``
}t_MvTensorOptimization;


char ** OptimizationNames();


/// Padding style
typedef enum
{
    paddStyleNone,
    paddStyleTFValid,
    paddStyleCaffe,
    paddStyleTFSame
}t_MvTensorPaddStyle;

/// MvTensor data storage order options
typedef enum
{
    orderYXZ,                ///< Option1: YXZ channel minor
    orderZYX,                ///< Option2: ZYX column minor
    orderYZX,                ///< Option3: YZX
    orderXYZ,                ///< Option4: XYZ
    orderXZY                 ///< Option5: XZY
}t_MvTensorStorageOrder;

/// Filtering types
typedef enum
{
    kConv,          ///< Convolution
    kMaxPool,       ///< Max-Pooling
    kAvgPool,       ///< Average-Pooling
    kSoftMax,       ///< SoftMax
    kFC,            ///< Fully connected layer
    kNone0,         ///< No post operation
    kRelu,          ///< rectified linear unit (Relu) rectifier
    kReluX,         ///< rectified linear unit (Relu) rectifier - clamp(0,X)
    kDepthConv,     ///< Depthwise Convolution
    kBias,          ///< Bias
    kPRelu,         ///< PReLU
    kLRN,           ///< LRN
    kSum,           ///< Sum of input and weight
    kProd,          ///< Prod of input and weight
    kMax,           ///< Max between input and weight
    kScale,         ///< Multiply each plane with a multiplier and add a bias
    kRelayout,
    kSquare,       ///< Square the input
    kInnerLRN,     ///< Output = (1 + alpha * input) ^ -beta
    kCopy,         ///< Copy considering input and output strides
    kSigmoid,      ///< Sigmoid
    kTanh,         ///< Tanh
    kDeconvolution,///< Deconvolution a.k.a. Transposed convolution
    kElu,          ///< Exponential linear unit (ELU) rectifier
    kReshape,      ///< Reshape
    kToPlaneMajor, ///< Convert from plane major to plane minor
    kPower,        ///< Power layer
    kCrop          ///< Crop layer
}t_MvTensorOpType;

/// Strucutre use to keep filter type and additional info
ALIGNED_STRUCT
(
    t_MvTensorOpType type;
    u32 optMask;  ///< Mask for optimizations that will be applied for this operation
    u32 radixX;   ///< Operation radix on X dimension
    u32 radixY;   ///< Operation radix on Y dimension
    u32 strideX;  ///< Operation stride in the X-direction
    u32 strideY;  ///< Operation stride in the Y-direction
    u32 padX;     ///< Input padding in the X-direction
    u32 padY;     ///< Input padding in the Y-direction
    t_MvTensorPaddStyle paddStyle;       ///< Padding style
    float opX;    ///< When operation is reluX this is the X value
    void *params; ///< Operation parameters. E.g. for power layer:shift, scale and power
) t_MvTensorOp;

/// Basic data structure
ALIGNED_STRUCT
(
    void* data;                          ///< Data Pointer
    int dimX;                            ///< Elements in x-dimension
    int dimY;                            ///< Elements in y-dimension
    int dimZ;                            ///< Elements in z-dimension
    int dimXStride;                      ///< Stride (in bytes) in the x-direction
    int dimYStride;                      ///< Stride (in bytes) in the y-direction
    int dimZStride;                      ///< Stride (in bytes) in the z-direction
    t_MvTensorDataType dataType;         ///< Data type
    t_MvTensorStorageOrder storageOrder; ///< Data storage order
) t_mvTensorGenData;

/// Myriad resources structure
ALIGNED_STRUCT
(
    int firstShave;               ///< Index of the first SHAVE
    int lastShave;                ///< Index of the last SHAVE
    int dmaLinkAgent;             ///< Link to the Direct Memory Access Agent
    int dataPartitionNo;          ///< Number of shave L2 cache data partition use by MvTensor
    int instrPartitionNo;         ///< Number of shave L2 cache instruction partition use by MvTensor
    dmaTransactionList_t* dmaTransactions;
) t_MvTensorMyriadResources;

/// Debug messages table
typedef struct
{
    double ms;                               ///< Duration of the mvTensor call (in ms)
    char * debugMsg;   ///< Debug messages of size MV_TENSOR_DBG_MSG_SIZE
}t_MvTensorDebugInfo;

/// Matmul resources structure
typedef struct
{
    unsigned int cache_memory_size;
    unsigned int scratch_memory_size;
    char*  cache_memory_ptr;
    char* scratch_memory_ptr;
} t_MvMatMulMyriadResources;

/// MvTensor global parameters structure
ALIGNED_STRUCT
(
    t_mvTensorGenData *input;           ///< Input data structure: pointer to input data, width, height, horizontal and vertical stride, data type and storage order.
    t_mvTensorGenData *output;          ///< Output data structure: same type of information as for input
    t_mvTensorGenData *weights;         ///< Weights data structure: same type of information as for input
    t_mvTensorGenData *accumulation;    ///< Intermediate data structure: needed only when operation needs to be done on another data type then the input/output are
    t_mvTensorGenData *biases;          ///< Biases input structure; typically 1D
    t_MvTensorOp *preOp;                 ///< Pre-processing operation:   TODO create enum type for this field
    t_MvTensorOp *op;                    ///< Filtering operation: Convoltion, max-pooling..
    t_MvTensorOp *postOp;                ///< Post-operation: Relu..
    t_MvTensorMyriadResources *myriadResources;     ///< Myriad resources structure: first shave, last shave and DMA link agent
    t_MvTensorDebugInfo *debugInfo;      ///< Debug table: debug mesage and time of mvTensor function call (in ms)
    t_MvMatMulMyriadResources *matmulResources;

) t_MvTensorParam;


// Prototypes

/// @brief mvTensor main function
///
/// @param[in] mvTensorParam pointer to a structure that holds information about:
///     - Input
///     - Output
///     - Weights
///     - Accumulation
///     - Pre-operation type
///     - Operation
///     - Post-operation type
///     - Myriad used resources
///     - Debug informations
/// @return void
///
void* mvTensor(void *params);


#endif // _MV_TENSOR_H_
