///
/// \file
/// \copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// \brief
/// 
/// \details
///


#ifndef _MV_SOFTMAXPARAM_H
#define _MV_SOFTMAXPARAM_H

#include <mv_types.h>

#ifdef __MOVICOMPILE__
    #include <moviVectorTypes.h>
#else
    typedef fp16 half;
#endif

/// MvSoftMax global parameters structure
typedef struct
{
    half* input;
    u32 sliceC;
    u32 stage;
    u8* cmxslice0;
    u8* cmxslice;
    u32 offset;
    u32 bufferOverflow;
    u32 dmaLinkAgent;
    half* output;
} t_MvSoftMaxParam;

#endif // _MV_SOFTMAXPARAM_H
