///
/// \file
/// \copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// \brief
/// 
/// \details
///


#ifndef _MV_ELTWISEPARAM_H
#define _MV_ELTWISEPARAM_H

#include <mv_types.h>
#include <mvTensor.h>

#ifdef __MOVICOMPILE__
    #include <moviVectorTypes.h>
#else
    typedef fp16 half;
#endif

enum Eltwise_op {Eltwise_sum, Eltwise_prod, Eltwise_max};

/// MvELTWISE global parameters structure
typedef struct
{
    enum Eltwise_op op;
    half* input;
    half* input2;
    half* output;
    u32 nelements, channels, channels2;
    u32 istrideX, i2strideX, ostrideX;
    half *cmxslice;
    u32 dmaLinkAgent;
} t_MvEltwiseParam;

#endif // _MV_ELTWISEPARAM_H
