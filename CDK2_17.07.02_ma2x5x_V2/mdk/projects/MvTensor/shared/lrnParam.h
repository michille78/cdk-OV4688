///
/// \file
/// \copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// \brief
/// 
/// \details
///


#ifndef _MV_LRNPARAM_H
#define _MV_LRNPARAM_H

#include <mv_types.h>

#ifdef __MOVICOMPILE__
    #include <moviVectorTypes.h>
#else
    typedef fp16 half;
#endif

/// MvLRN global parameters structure
typedef struct
{
    half* input;
    half* output;
    u32 channels;
    u32 stage;
    u32 numlines;
    u32 size;
    half k;
    half alpha;
    half beta;
    half *cmxslice;
    u32 dmaLinkAgent;
} t_MvLRNParam;

#endif // _MV_LRNPARAM_H
