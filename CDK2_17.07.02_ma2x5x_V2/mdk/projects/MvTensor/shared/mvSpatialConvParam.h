///
/// \file
/// \copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// \brief
///
/// \details
///

#ifndef _MV_SPATIALCONVPARAM_H_
#define _MV_SPATIALCONVPARAM_H_
#include <mv_types.h>

#ifdef __MOVICOMPILE__
    #include <moviVectorTypes.h>
#else
    typedef fp16 half;
#endif

/// mvSpatialConv global parameters structure
typedef struct
{
    half* input;
    u32 in_height;
    u32 in_width;
    u32 in_channels;
    u32 out_channels;
    u32 radix_x;
    u32 radix_y;
    u32 stride_x;
    u32 stride_y;
    u32 padding_type;
    u32 start_out_row;
    u32 out_rows;
    half* weights;
    half* output;
    u8* cmxslice;
    u32 dmaLinkAgent;
} t_MvSpatialConvParam;

#endif  //_MV_SPATIALCONVPARAM_H_
