///
/// @file
/// @copyright All code copyright Movidius Ltd 2012, all rights reserved
///            For License Warranty see: common/license.txt
///
/// @defgroup ShaveLoader Shave Loader
/// @{
/// @brief  API for the Shave Loader module
///
/// Used for executing different functionalities on SHAVEs
///

#ifndef SWC_SHAVE_LOADER_LOCAL_H
#define SWC_SHAVE_LOADER_LOCAL_H

// 1: Includes
// ----------------------------------------------------------------------------
#include <stdarg.h>
#include "mv_types.h"
#include <DrvIcb.h>
#include <swcDmaTypes.h>
#include <swcShaveLoader.h>
#include "DrvCommon.h"
#include "theDynContext.h"
#include "swcLeonUtils.h"

// 2:  Exported Global Data (generally better to avoid)
// ----------------------------------------------------------------------------

// 2.5: Global definitions
// ----------------------------------------------------------------------------

#ifdef __cplusplus
extern "C" {
#endif

// 3:  Exported Functions (non-inline)
// ----------------------------------------------------------------------------

void swcDynSetShaveWindows(DynamicContext_t *moduleStData, u32 shaveNumber);

/// @brief Sets up and launches one dynamic application instance on a specifically requested SHAVE
///        Uses the shaves preliminary assigned by user via function swcSetupDynShaveApps().
///        Allocates all necessary memory, loads the dynamic library, then starts the shave.
///        Checks if the requested shave has bee configured in advance and if it is not running.
/// @param[in] moduleStData - DynamicContext_t pointer to ModuleData structure
/// @param[in] shaveNumber  - specific shave requested by the user to run the algorithm on
/// @param[in] *fmt - string containing i, s, or v according to irf, srf or vrf ex. "iisv"
/// @param[in] ... - variable number of params according to fmt
/// @return operation status
///
s32 swcRunShaveAlgoOnAssignedShaveCCNoSetup(DynamicContext_t *moduleStData, u32 shaveNumber, const char *fmt, ...);

/// @brief This function allocates heap and group data memory for all configured instances of one application.
///        It must be called prior to using swcRunShaveAlgo().
///        Can be used from both Leons.
///        svuList below is not copied internally, instead just the pointer is
///        assigned to an internal structure. Please ensure the svuList memory
///        is alive until the call of swcCleanupDynShaveApps. Note: be careful
///        about stack declared svuList.
/// @param[in] moduleStData - DynamicContext_t pointer to application's module data structure
/// @param[in] svuList - pointer to a shave list to be used for all application instances
/// @param[in] instances - number of application instances; must correspond to number of shaves configured in svuList
/// @return operation status
///
s32 swcSetupDynShaveAppsLocal(DynamicContext_t *moduleStData, const swcShaveUnit_t *svuList, const uint32_t instances);

/// @brief This function allocates heap and group data memory for all configured instances of one application.
///        The complete suffix means it also sets Shave windows, stack and loads data sections before
///        running any Shaves. This version of the function is useful in order to avoid the
///        run-time penalty of re-loading the Shaves.
///        It must be called prior to using swcRunShaveAlgo().
///        Can be used from both Leons.
///        svuList below is not copied internally, instead just the pointer is
///        assigned to an internal structure. Please ensure the svuList memory
///        is alive until the call of swcCleanupDynShaveApps. Note: be careful
///        about stack declared svuList.
/// @param[in] moduleStData - DynamicContext_t pointer to application's module data structure
/// @param[in] svuList - pointer to a shave list to be used for all application instances
/// @param[in] instances - number of application instances; must correspond to number of shaves configured in svuList
/// @return operation status (to be enhanced in the future)
///
s32 swcSetupDynShaveAppsComplete(DynamicContext_t *mData, const swcShaveUnit_t *svuList, const uint32_t instances);

/// @brief Launches one dynamic application instance.
///        Targets a specific shave designated.
///        Writes the variable parameters to IRF/VRF shave's registers.
///        Target shave needs to be preliminary assigned via function OsDrvSvuSetupDynShaveApps().
///        Allocates all necessary memory, then starts the shave.
/// @param[in] moduleStData - DynamicSubModule_t pointer to ModuleData structure
/// @param[in] shaveNumber - specific shave to be used
/// @param[in] *fmt - string containing i or v according to irf or vrf ex. "iiv"
/// @param[in] ... - variable number of parameters according to fmt
/// @return operation status
int OsDrvSvuRunShaveAlgoOnAssignedShaveCCNoSetup(DynamicContext_t *moduleStData, u32 shvNumber, const char *fmt, ...);
/// @}
#ifdef __cplusplus
}
#endif

#endif // SWC_SHAVE_LOADER_LOCAL_H
