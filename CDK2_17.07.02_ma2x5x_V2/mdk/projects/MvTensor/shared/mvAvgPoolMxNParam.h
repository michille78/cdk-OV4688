///
/// \file
/// \copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// \brief
/// 
/// \details
///


#ifndef _MV_AVGPOOLMxNPARAM_H
#define _MV_AVGPOOLMxNPARAM_H

#define CMX_DATA_SIZE   84000
#define INPUT_BPP       2

#include <mv_types.h>

#ifdef __MOVICOMPILE__
    #include <moviVectorTypes.h>
#else
    typedef fp16 half;
#endif

/// MvAvgPoolMxN global parameters structure
typedef struct
{
    half* input;
    u32 channels;
    u32 sliceC;
    u32 height;
    u32 width;
    u32 radixX;
    u32 radixY;
    u32 strideX;
    u32 strideY;
    u32 padX;
    u32 padY;
    u32 ostrideX;
    u8* cmxslice;
    u32 dmaLinkAgent;
    u32 paddStyle;
    half* output;
} t_MvAvgPoolMxNParam;

#endif // _MV_AVGPOOLMxNPARAM_H
