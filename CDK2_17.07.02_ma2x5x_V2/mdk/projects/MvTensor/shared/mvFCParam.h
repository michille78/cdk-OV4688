///
/// \file
/// \copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// \brief
/// 
/// \details
///


#ifndef _MV_FCPARAM_H
#define _MV_FCPARAM_H

#include <mv_types.h>

#ifdef __MOVICOMPILE__
    #include <moviVectorTypes.h>
#else
    typedef fp16 half;
#endif

// MAX_SLICE_OUT_UNITS must be a power of 2
#define MAX_SLICE_OUT_UNITS 1024

/// MvFC global parameters structure
typedef struct
{
    half* inputA;
    half* inputB;
    u32   inUnits;
    u32   outUnits;
    u32   sliceOutUnits;
    u8*   cmxslice;
    u32   dmaLinkAgent;
    half* output;
} t_MvFCParam;

#endif // _MV_FCPARAM_H
