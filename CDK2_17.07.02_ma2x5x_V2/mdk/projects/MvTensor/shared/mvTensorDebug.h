/*
 * MvAssert.h
 *
 *  Created on: Oct 20, 2016
 *      Author: ian-movidius
 */

#ifndef SHARED_MVASSERT_H_
#define SHARED_MVASSERT_H_

#include <stdlib.h>
#include <string.h>

#include <mv_types.h>
#include <swcWhoAmI.h>
#include "mprintf.h"
#include "mvTensorConfig.h"

class ErrorManager
{
/* This Error Manager was created to get around the problem of normal C++ assert calling abort() and crashing the myriad.
 * meaning that the USB for Fathom was not able to know if the program had actually crashed or if there was a communication
 * issue.
 *
 * Instead on ErrorManagerVar.MvAssert, we populate a given buffer - Generally the MvTensor Debug Buffer, but it can be
 * used in other projects.
 *
 */
private:
    static char* msg;
    
    ErrorManager() 
    {}

public:
    static void Init(char* mb)
    {
        msg = mb;
    }

    static void Assert(int expression, char const* newMsg = "MvTensor assertion failed")
    {
        if (expression) 
            return; //Expression evaluated as truthy. No need to exit

        MPRINTF("%s: %s\n", __FUNCTION__, newMsg);

        if (msg && newMsg)
        {
            u32 existingLen = strlen(msg);
            u32 lenLeft = MV_TENSOR_DBG_MSG_SIZE - existingLen - 1;
            strncat(msg, newMsg, lenLeft);
        }

        exit(-1);
    }
};


static inline void mvTensorAssertHelper(const u32 cond, const char* condText,
      const char* file, const u32 line, const char* func, const char* msg = "")
{
    (void)cond;

    MPRINTF("%s: assertion failed in\n"
        "\tfile: %s, line %u, \n"
        "\tfunction: %s\n"
        "\tcondition: %s\n" 
        "\tmessage: %s\n", 
        swcGetProcessorName(swcWhoAmI()), file, (unsigned)line, func, condText, msg);
}

#ifdef DEPLOY
#define mvTensorAssert(cond, ...)                                                 \
    if (!(cond))                                                                  \
        mvTensorAssertHelper(cond, #cond, __FILE__, __LINE__, __FUNCTION__, ##__VA_ARGS__)
#else
#define mvTensorAssert(cond, ...) ErrorManager::Assert(cond, ##__VA_ARGS__)
#endif


static inline u32 isValidCmxAddress(u32 addr)
{
    const u32 cmxSize = 2 * 1024 * 1024;
    const u32 cmxStartCached = 0x70000000;
    const u32 cmxStartUnCached = 0x78000000;

    return (((addr >= cmxStartCached) && (addr < cmxStartCached + cmxSize)) ||
            ((addr >= cmxStartUnCached) && (addr < cmxStartUnCached + cmxSize)));
}

static inline u32 isValidDdrAddress(u32 addr)
{
#ifdef MA2150
    const u32 ddrSize = 128 * 1024 * 1024;
#else
    const u32 ddrSize = 512 * 1024 * 1024;
#endif
    const u32 ddrStartCached = 0x80000000;
    const u32 ddrStartUnCached = 0xC0000000;

    return (((addr >= ddrStartCached) && (addr < ddrStartCached + ddrSize)) ||
            ((addr >= ddrStartUnCached) && (addr < ddrStartUnCached + ddrSize)));
}

static inline u32 isValidVirtualAddress(u32 addr)
{
  u32 win = (addr >> 24) - 0x1C;

  // win can be [0-3]
  return (win <= 3);
}

static inline u32 isValidAddress(u32 addr)
{
  u32 valid = isValidCmxAddress(addr) || isValidDdrAddress(addr);
  swcProcessorType type = swcWhoAmI();
  
  if ((type != PROCESS_LEON_OS) && (type != PROCESS_LEON_RT))
    valid = valid || isValidVirtualAddress(addr);

  return valid;
}

template <typename T>
static u32 isWithinRangeInclusive(T val, T start, T end)
{
  u32 valid = ((val >= start) && (val <= end));

  if (!valid)
  {
    MPRINTF("Value %f (0x%x) out of range (%f (0x%x), %f (0x%x))\n", (fp32)val, val, 
      (fp32)start, start, (fp32)end, end);
    exit(-1);
  }

    return valid;
}

template <typename T>
static u32 isWithinRangeExclusive(T val, T start, T end)
{
  u32 valid = ((val > start) && (val < end));

  if (!valid)
  {
    MPRINTF("Value %f (0x%x) out of range [%f (0x%x), %f (0x%x)]\n", (fp32)val, val, 
      (fp32)start, start, (fp32)end, end);
    exit(-1);
  }

    return valid;
}

#endif /* SHARED_MVASSERT_H_ */
