///
/// \file
/// \copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// \brief
/// 
/// \details
///


#ifndef _MV_AVGPOOL3x3PARAM_H
#define _MV_AVGPOOL3x3PARAM_H

#include <mv_types.h>

#define CMX_DATA_SIZE   84000
#define INPUT_BPP       2

#ifdef __MOVICOMPILE__
    #include <moviVectorTypes.h>
#else
    typedef fp16 half;
#endif

/// MvAvgPool3x3 global parameters structure
typedef struct
{
    half* input;
    u32 channels;
    u32 sliceC;
    u32 ostrideX;
    u32 height;
    u32 width;
    u32 stride;
    u32 pad;
    u8* cmxslice;
    u32 dmaLinkAgent;
    u32 paddStyle;
    half* output;
} t_MvAvgPool3x3Param;

#endif // _MV_AVGPOOL3x3PARAM_H
