#ifndef __MVTENSOR_INTERNAL_H__
#define __MVTENSOR_INTERNAL_H__

#include <stdio.h>
#include <stdlib.h>
#include <mv_types.h>
#include <swcWhoAmI.h>

#ifndef __MOVICOMPILE__
#include <assert.h>
#include <DrvSvu.h>
#include <swcShaveLoaderLocal.h>
#endif // ifndef __MOVICOMPILE__

#if defined(__RTEMS__)
#include <OsDrvSvu.h>
#endif

#include "mprintf.h"
#include "mvTensorConfig.h"
#include "mvTensorDebug.h"


// Dynamic loading macros -----------------------------------------------
#define PRIMITIVE_CAT(a, ...) a ## __VA_ARGS__
#define CAT(a, ...) PRIMITIVE_CAT(a, __VA_ARGS__)

#define MODULE_ENTRY_DECL(entry) extern u32 CAT(MVTENSOR, X ## _ ## entry[])
#define MODULE_ENTRY(entry) CAT(MVTENSOR, X ## _ ## entry)

MODULE_ENTRY_DECL(mvAvgPool3x3);
MODULE_ENTRY_DECL(mvAvgPool7x7xk);
MODULE_ENTRY_DECL(mvAvgPoolMxN);
MODULE_ENTRY_DECL(mvDepthConv);
MODULE_ENTRY_DECL(mvFC);
MODULE_ENTRY_DECL(mvMaxPool2x2);
MODULE_ENTRY_DECL(mvMaxPool3x3);
MODULE_ENTRY_DECL(mvMaxPoolMxN);
MODULE_ENTRY_DECL(mvSoftMax);
MODULE_ENTRY_DECL(mvLRN);
MODULE_ENTRY_DECL(mvEltwise);
MODULE_ENTRY_DECL(postOps_core);
MODULE_ENTRY_DECL(relayout_core);
MODULE_ENTRY_DECL(mvConv7x7s2);
MODULE_ENTRY_DECL(mvSpatialConv);
MODULE_ENTRY_DECL(relayout_core_v1);
// ----------------------------------------------------------------------


#ifndef __MOVICOMPILE__

static inline u8* getCMXSliceDataSection(int shvno)
{
	mvTensorAssert(shvno >= 0 && shvno < MVTENSOR_MAX_SHAVES);

	// This is actually a data buffer
	MODULE_ENTRY_DECL(sData);

	// Some Mv* modules rely on absolute addresses for initializations or precalculations.
	return (u8*)swcSolveShaveRelAddr((u32)&MODULE_ENTRY(sData), shvno);
}

static inline void busyWaitShaveL1(int shvno)
{
	const static u32 shv_iL1_status_addr[MVTENSOR_MAX_SHAVES] =
	{
	    SHAVE_0_BASE_ADR + SLC_OFFSET_IL1 + SHV_IL1_STATUS,
	    SHAVE_1_BASE_ADR + SLC_OFFSET_IL1 + SHV_IL1_STATUS,
	    SHAVE_2_BASE_ADR + SLC_OFFSET_IL1 + SHV_IL1_STATUS,
	    SHAVE_3_BASE_ADR + SLC_OFFSET_IL1 + SHV_IL1_STATUS,
	    SHAVE_4_BASE_ADR + SLC_OFFSET_IL1 + SHV_IL1_STATUS,
	    SHAVE_5_BASE_ADR + SLC_OFFSET_IL1 + SHV_IL1_STATUS,
	    SHAVE_6_BASE_ADR + SLC_OFFSET_IL1 + SHV_IL1_STATUS,
	    SHAVE_7_BASE_ADR + SLC_OFFSET_IL1 + SHV_IL1_STATUS,
	    SHAVE_8_BASE_ADR + SLC_OFFSET_IL1 + SHV_IL1_STATUS,
	    SHAVE_9_BASE_ADR + SLC_OFFSET_IL1 + SHV_IL1_STATUS,
	    SHAVE_10_BASE_ADR + SLC_OFFSET_IL1 + SHV_IL1_STATUS,
	    SHAVE_11_BASE_ADR + SLC_OFFSET_IL1 + SHV_IL1_STATUS,
	};

	mvTensorAssert(shvno >= 0 && shvno < MVTENSOR_MAX_SHAVES);

	while (GET_REG_WORD_VAL(shv_iL1_status_addr[shvno]))
		;
}
static inline void startShave( u32 shvno, u32 entry, u32 param)
{
	mvTensorAssert(shvno < MVTENSOR_MAX_SHAVES);
	mvTensorAssert(isValidDdrAddress(entry));
	mvTensorAssert(isValidDdrAddress(param) || isValidCmxAddress(param));

    busyWaitShaveL1(shvno);
    extern DynamicContext_t localModule[12];

    s32 status;
#if defined(__RTEMS__)
    status = OsDrvSvuRunShaveAlgoOnAssignedShaveCCNoSetup(&localModule[shvno], shvno, "ii", entry, param);
    assert((status == OS_MYR_DRV_SUCCESS) && "Attempting to start an unallocated Shave. Make sure the Shave \
        belongs to your application, is not busy and it has been initialized.");
#else
    status = swcRunShaveAlgoOnAssignedShaveCCNoSetup(&localModule[shvno], shvno, "ii", entry, param);
    assert((status == (s32)shvno) && "Attempting to start an unallocated Shave. Make sure the Shave \
        belongs to your application, is not busy and it has been initialized.");
#endif


}

static inline void waitShaves(u32 firstShave, u32 lastShave)
{
    mvTensorAssert(firstShave <= lastShave);
    mvTensorAssert(lastShave < MVTENSOR_MAX_SHAVES);
#if defined(__RTEMS__)
    swcShaveUnit_t svuList[lastShave  - firstShave + 1];
    u32 runningShaves;
    u32 shv_start_count = 0;
    for(u32 i = firstShave; i <= lastShave; i++)
    {
        svuList[shv_start_count] = i;
        shv_start_count++;
    }
    OsDrvSvuDynWaitShaves(svuList, shv_start_count, OS_DRV_SVU_WAIT_FOREVER, &runningShaves);
#else
    while (swcShavesRunning(firstShave, lastShave));
#endif
}

#endif

#endif
