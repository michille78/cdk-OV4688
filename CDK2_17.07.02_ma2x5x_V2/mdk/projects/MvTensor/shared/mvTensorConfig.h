#ifndef MVTENSOR_CONFIG_H
#define MVTENSOR_CONFIG_H

// System resources configuration

static const int MVTENSOR_HEAP_DATA_SIZE = 21000 * sizeof(float);
static const int MVTENSOR_STACK_SIZE = 1024;

#define MVTENSOR_MAX_SHAVES 12
#define MVTENSOR_MUTEX 5
#define MV_TENSOR_DBG_MSG_SIZE 120


#ifndef __MOVICOMPILE__
#	ifndef DDR_DATA
#		define DDR_DATA  __attribute__((section(".ddr.data"), aligned(64)))
#	endif
#       ifndef DDR_BSS
#       	define DDR_BSS __attribute__((section(".ddr.bss"), aligned(64)))
#       endif
#	define NOCACHE __attribute__((section(".cmx.data"), aligned (64)))
#else
#	ifndef SLICE_BSS
#		define SLICE_BSS __attribute__((section(".bss"), aligned(16)))
#	endif
#endif

#endif
