///
/// \file
/// \copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// \brief
/// 
/// \details
///


#ifndef _MV_MAXPOOLMxNPARAM_H
#define _MV_MAXPOOLMxNPARAM_H

#include <mv_types.h>

#define CMX_DATA_SIZE   84000
#define INPUT_BPP       2

#ifdef __MOVICOMPILE__
    #include <moviVectorTypes.h>
#else
    typedef fp16 half;
#endif

/// MvMaxPoolMxN global parameters structure
typedef struct
{
    half* input;
    u32 channels;
    u32 sliceC;
    u32 height;
    u32 width;
    u32 radixX;
    u32 radixY;
    u32 strideX;
    u32 strideY;
    u32 padX;
    u32 padY;
    u32 ostrideX;
    u8* cmxslice;
    u32 dmaLinkAgent;
    u32 paddStyle;
    half* output;
} t_MvMaxPoolMxNParam;

#endif // _MV_MAXPOOLMxNPARAM_H
