///
/// \file
/// \copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// \brief
///
/// \details
///

#ifndef _MV_RELAYOUTPARAM_H_
#define _MV_RELAYOUTPARAM_H_

#include <mv_types.h>
#include "mvTensor.h"

#ifdef __MOVICOMPILE__
#include <moviVectorTypes.h>
#else
typedef fp16 half;
#endif

enum t_MvTensorPadStyle {VALID, SAME};
enum t_MvRelayoutVersion {CONVOLUTION_V0, CONVOLUTION_V1, DECONVOLUTION};

/// Relayout parameters
typedef struct
{
    u8  *input;
    u8  *output;
    s32 width;
    s32 height;
    s32 no_channels;
    s32 radixX;
    s32 radixY;
    s32 kernelStrideX;
    s32 kernelStrideY;
    s32 inOutBpp;
    u8 *cmxslice;
    u32 dmaLinkAgent;
    s32 pad_style;
    s32 version;
    // Used only by relayout version 0 >>>
    s32 first_pixel;
    s32 no_pixels;
    // Used only by relayout version 0 <<<
    // Used only by relayout version 1 >>>
    s32 slice_width;
    s32 slice_height;
    s32 first_pixel_col;
    s32 first_pixel_row;
    // Used only by relayout version 1 <<<<
    // Used only by transposed convolution <<<<
    s32 input_stride;
    s32 output_stride;
    s32 pad_top;
    s32 pad_bottom;
    s32 pad_left;
    s32 pad_right;
    // Used only by transposed convolution >>>>
} t_RelayoutParams;

#endif /* _MV_RELAYOUTPARAM_H_ */
