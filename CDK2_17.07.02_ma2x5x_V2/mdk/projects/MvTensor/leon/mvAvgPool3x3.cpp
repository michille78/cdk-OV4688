///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     main leon file
///

// 1: Includes
// ----------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <DrvSvu.h>
#include <DrvShaveL2Cache.h>
#include <DrvTimer.h>
#include <mv_types.h>
#include "mvAvgPool3x3.h"
#include <mvAvgPool3x3Param.h>
#include <mvTensorInternal.h>

// 2:  Source Specific #defines and types  (typedef, enum, struct)
// ----------------------------------------------------------------------------
// 3: Global Data (Only if absolutely necessary)
// ----------------------------------------------------------------------------
t_MvAvgPool3x3Param avgPool3x3Param[MVTENSOR_MAX_SHAVES] NOCACHE;
// 4: Static Local Data
// ----------------------------------------------------------------------------
// 5: Static Function Prototypes
// ----------------------------------------------------------------------------
// 6: Functions Implementation
// ----------------------------------------------------------------------------
void avgPool3x3(fp16* input, u32 channels, u32 height,
        u32 width, u32 paddStyle, u32 stride, u32 pad, u32 ostrideX, fp16* output,
        u32 dmaLinkAgent,
        u32 firstShave, u32 lastShave)
{
    u32 i;
    u32 sliceC; // channels to process per SHAVE
    u32 remainingC;
    u32 offsetIO = 0;
    u32 channelsMaxToProcess;
    u32 group = channels % 8 == 0 ? 8 : 1;
    // allocate 7 words extra, in order to allow the Shave function to enlarge the
    // input/output buffers to the next multiple of 8
    u32 max_sliceC = u32((CMX_DATA_SIZE/INPUT_BPP/2 - 7)/(3*(width + 2*pad + 2))) / group;
    u32 shaves_no =(lastShave - firstShave + 1);

    channelsMaxToProcess = channels / group;

    do{
        sliceC = channelsMaxToProcess / shaves_no;
        if(sliceC > max_sliceC)
        {
            sliceC = max_sliceC;
            remainingC = 0;
            channelsMaxToProcess -= sliceC * shaves_no;
        }
        else
        {
            remainingC = channelsMaxToProcess % shaves_no;
            channelsMaxToProcess = 0;
        }
        u32 at_leastC = sliceC;
        for (i = firstShave; i <= lastShave; i++)
        {
            sliceC = at_leastC;
            if(remainingC)
            {
                sliceC++;
                remainingC--;
            }

            if (sliceC == 0)
                break;

            //initialize AvgPool3x3 SHAVE param
            avgPool3x3Param[i].input    = input + offsetIO;
            avgPool3x3Param[i].channels = channels;
            avgPool3x3Param[i].sliceC   = sliceC * group;
            avgPool3x3Param[i].ostrideX = ostrideX;
            avgPool3x3Param[i].height   = height;
            avgPool3x3Param[i].width    = width;
            avgPool3x3Param[i].stride   = stride;
            avgPool3x3Param[i].pad      = pad;
            avgPool3x3Param[i].cmxslice = getCMXSliceDataSection(i);
            avgPool3x3Param[i].dmaLinkAgent = dmaLinkAgent;
            avgPool3x3Param[i].paddStyle    = paddStyle;
            avgPool3x3Param[i].output       = output + offsetIO;

            u32 readBackAddr = (u32)(&(avgPool3x3Param[i].output));
            GET_REG_WORD_VAL(readBackAddr);

            startShave(i, (u32)&MODULE_ENTRY(mvAvgPool3x3), (u32)&avgPool3x3Param[i]);

            offsetIO += sliceC * group;
        }
    
        waitShaves(firstShave, lastShave);

    }while(channelsMaxToProcess > 0);
}
