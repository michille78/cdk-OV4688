///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief Leon wrappers for CNN post operations.
///

// 1: Includes
// ----------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <DrvSvu.h>
#include <DrvTimer.h>
#include <DrvCmxDma.h>
#include <DrvLeonL2C.h>
#include <DrvShaveL2Cache.h>
#include <mv_types.h>

#include "mvReshape.h"
#include <mvTensorInternal.h>

// 2:  Source Specific #defines and types  (typedef, enum, struct)
// ----------------------------------------------------------------------------
#define DMA_DESCRIPTORS_SECTION __attribute__((section(".cmx.cdmaDescriptors")))
#define MAX(a, b) ((a)>(b)?(a):(b))
#define MIN(a, b) ((a)<(b)?(a):(b))

// 3: Global Data (Only if absolutely necessary)
// ----------------------------------------------------------------------------
// 4: Static Local Data
// ----------------------------------------------------------------------------
// 5: Static Function Prototypes
// ----------------------------------------------------------------------------
// 6: Functions Implementation
// ----------------------------------------------------------------------------
void reshape(fp16 *input, fp16 *output,
            s32 in_width, s32 in_height,
            s32 out_width, s32 out_height, s32 out_channels,
            u32 inputStride, u32 outputStride,
            t_MvTensorMyriadResources *myriadResources)
{
    static dmaTransactionList_t DMA_DESCRIPTORS_SECTION task1;
    static dmaTransactionList_t *ref1;
    static dmaRequesterId id1;
    s32 elems_in_ch = in_width * in_height;
    s32 elems_out_ch = out_width * out_height;
    s32 rem_in_ch, rem_out_ch;
    s32 elems_dma = 0;
    s32 c_in, c_out;
    u8  *p_input, *p_output;

    DrvCmxDmaInitDefault();
    id1 = DrvCmxDmaInitRequesterOnAgent(1, myriadResources->dmaLinkAgent);

    // this function needs to perform the reshaping as if data was in
    // ZYX order (Caffe), therefore, we need to transfer every channel separately
    c_in = 0;
    c_out = 0;
    rem_in_ch = elems_in_ch;
    rem_out_ch = elems_out_ch;
    p_input  = (u8*)input  + c_in*sizeof(fp16);
    p_output = (u8*)output + c_out*sizeof(fp16);

    do
    {
        elems_dma = MIN(rem_in_ch, rem_out_ch);
        ref1 = DrvCmxDmaCreateTransactionFullOptions(id1, &task1,
                 p_input,                                         // src
                 p_output,                                        // dst
                 sizeof(fp16) * elems_dma,                        // byte length
                 sizeof(fp16) * 1,                                // src width
                 sizeof(fp16) * 1,                                // dst width
                 inputStride,                                     // src stride
                 outputStride);                                   // dst stride
        DrvCmxDmaStartListTask(ref1);
        DrvCmxDmaWaitTask(ref1);

        p_input  += elems_dma * inputStride;
        p_output += elems_dma * outputStride;
        rem_in_ch  -= elems_dma;
        rem_out_ch -= elems_dma;

        if(rem_in_ch == 0)
        {
            // wrap input
            c_in++;
            rem_in_ch = elems_in_ch;
            p_input  = (u8*)input  + c_in*sizeof(fp16);
        }

        if(rem_out_ch == 0)
        {
            // wrap output
            c_out++;
            rem_out_ch = elems_out_ch;
            p_output = (u8*)output + c_out*sizeof(fp16);
        }


    } while (c_out < out_channels);
}
