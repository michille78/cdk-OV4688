///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Simple effect header
///

#ifndef _DEPTHCONV_H_
#define _DEPTHCONV_H_

// 1: Includes
// ----------------------------------------------------------------------------
#include <mv_types.h>
#include <mvTensor.h>

#ifdef __cplusplus
extern "C"
{
#endif

// 2:  Exported Global Data (generally better to avoid)
// ----------------------------------------------------------------------------
// 3:  Exported Functions (non-inline)
// ----------------------------------------------------------------------------

void depthConv(fp16* input, u32 in_height, u32 in_width,
               u32 in_channels, fp16* weights, u32 filter_height, u32 filter_width,
               u32 stride_h, u32 stride_w, u32 channel_multiplier, fp16* output,
               u32 out_height, u32 out_width,
               t_MvTensorMyriadResources *myriadResources);

#ifdef __cplusplus
}
#endif

#endif//_DEPTHCONV_H_
