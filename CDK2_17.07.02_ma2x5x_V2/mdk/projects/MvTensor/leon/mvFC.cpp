///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     main leon file
///

// 1: Includes
// ----------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <DrvSvu.h>
#include <DrvTimer.h>
#include <mv_types.h>
#include "mvFC.h"
#include <mvFCParam.h>
#include <mvTensorInternal.h>

// 2:  Source Specific #defines and types  (typedef, enum, struct)
// ----------------------------------------------------------------------------
#define MAX(a, b) ((a)>(b)?(a):(b))
#define MIN(a, b) ((a)<(b)?(a):(b))

// 3: Global Data (Only if absolutely necessary)
// ----------------------------------------------------------------------------
t_MvFCParam fcParam[MVTENSOR_MAX_SHAVES] NOCACHE;

// 4: Static Local Data
// ----------------------------------------------------------------------------
// 5: Static Function Prototypes
// ----------------------------------------------------------------------------
// 6: Functions Implementation
// ----------------------------------------------------------------------------

void fc(fp16* inVecA, fp16* inArrayB,
        u32 inUnits, u32 outUnits,
        fp16* outVec,
        u32 dmaLinkAgent,
        u32 firstShave, u32 lastShave)
{
    u32 i;
    u32 sliceOutUnits, sliceOutUnitsMax;
    u32 sliceOutUnits_start = 0;
    u32 outUnits_rem;
    (void)lastShave;

    (void)lastShave;

    // outVec must be initialized with 0
    bzero((u8*)outVec, outUnits*sizeof(fp16));

    // split outUnits in slices of MAX_SLICE_OUT_UNITS
    sliceOutUnitsMax = MIN(outUnits, MAX_SLICE_OUT_UNITS);
    outUnits_rem = outUnits;
    sliceOutUnits_start = 0;

    // it was observed that it doesn't make any difference in run time
    // when more than one SHAVE are used.
    // Therefore, we limit the number of SHAVEs to 1, in order to reduce power usage
    i = firstShave;
    do {
        // compute remaining units
        sliceOutUnits = MIN(sliceOutUnitsMax, outUnits_rem);
        outUnits_rem -= sliceOutUnits;

        //initialize FC SHAVE param
        fcParam[i].inputA       = inVecA;
        fcParam[i].inputB       = inArrayB  + sliceOutUnits_start;
        fcParam[i].inUnits      = inUnits;
        fcParam[i].outUnits     = outUnits;
        fcParam[i].sliceOutUnits = sliceOutUnits;
        fcParam[i].cmxslice     = getCMXSliceDataSection(i);
        fcParam[i].dmaLinkAgent = dmaLinkAgent;
        fcParam[i].output       = outVec + sliceOutUnits_start;

        u32 readBackAddr = (u32)(&(fcParam[i].output));
        GET_REG_WORD_VAL(readBackAddr);

        startShave(i, (u32)&MODULE_ENTRY(mvFC), (u32)&fcParam[i]);

        sliceOutUnits_start += sliceOutUnits;

        waitShaves(i, i);
    } while (outUnits_rem > 0);
}
