///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief Leon wrappers for CNN post operations.
///

// 1: Includes
// ----------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <DrvSvu.h>
#include <DrvTimer.h>
#include <DrvLeonL2C.h>
#include <DrvShaveL2Cache.h>
#include <mv_types.h>

#include "mvRelayout.h"
#include "mvRelayoutParam.h"
#include <mvTensorInternal.h>

#if defined(__RTEMS__)
#include <OsDrvShaveL2Cache.h>
#endif

// 2:  Source Specific #defines and types  (typedef, enum, struct)
// ----------------------------------------------------------------------------
// 3: Global Data (Only if absolutely necessary)
// ----------------------------------------------------------------------------
// 4: Static Local Data
// ----------------------------------------------------------------------------
t_RelayoutParams    relayoutParams[MVTENSOR_MAX_SHAVES] NOCACHE;

// 5: Static Function Prototypes
// ----------------------------------------------------------------------------
// 6: Functions Implementation
// ----------------------------------------------------------------------------
void relayout(fp16 *input, fp16 *output, s32 width, s32 height,
        s32 channels, s32 radixX, s32 radixY,
        s32 kernelStrideX, s32 kernelStrideY, t_MvTensorPaddStyle pad_style,
        s32 padX, s32 padY, t_MvRelayoutVersion version,
        t_MvTensorMyriadResources *myriadResources)
{
    switch(version)
    {
    case CONVOLUTION_V0:
        relayout_conv_v0(input, output, width, height, channels, radixX, radixY,
                kernelStrideX, kernelStrideY, pad_style, myriadResources);
        break;
    case CONVOLUTION_V1:
        relayout_conv_v1(input, output, width, height, channels, radixX, radixY,
                kernelStrideX, kernelStrideY, pad_style, myriadResources);
        break;
    case DECONVOLUTION:
        relayout_deconv(input, output, width, height, channels, radixX, radixY,
                kernelStrideX, kernelStrideY, pad_style,
                padX, padY, myriadResources);
        break;
    }
}

void relayout_conv_v0(fp16 *input, fp16 *output, s32 width, s32 height,
        s32 channels, s32 radixX, s32 radixY,
        s32 kernelStrideX, s32 kernelStrideY, t_MvTensorPaddStyle pad_style,
        t_MvTensorMyriadResources *myriadResources)
{
    if(!(pad_style == paddStyleTFSame || pad_style == paddStyleTFValid))
        // Does not support other type of padding.
        return;

    s32 shave_i;
    s32 no_shaves = myriadResources->lastShave - myriadResources->firstShave + 1;
    s32 half_radixY = radixY >> 1;
    s32 half_radixX = radixX >> 1;
    // We have to process the whole image + top and bottom padding lines + (kernel_widht - 1
    // left right padding) pixels.
    s32 pixels_to_process = 0;
    if(pad_style == paddStyleTFSame)
        pixels_to_process = width * (height + 2 * half_radixY) + 2 * half_radixX;
    else if(pad_style == paddStyleTFValid)
        pixels_to_process = width * height;

    s32 pixels_per_shave = pixels_to_process / no_shaves;
    s32 pixels_remainder = pixels_to_process % no_shaves;
    // We start with the padding.
    s32 first_pixel = 0;

    if(pad_style == paddStyleTFSame)
        first_pixel = -half_radixY * width - half_radixX;
    else if(pad_style == paddStyleTFValid)
        first_pixel = 0;

    for(shave_i = myriadResources->firstShave; shave_i <= myriadResources->lastShave; ++shave_i)
    {
        s32 no_pixels = pixels_per_shave;
        if(pixels_remainder)
        {
            ++no_pixels;
            --pixels_remainder;
        }
        else
        {
            // Less pixels than shaves.
            if(pixels_per_shave == 0)
                break;
        }

        relayoutParams[shave_i].input          = (u8 *)input;
        relayoutParams[shave_i].output         = (u8 *)output;
        relayoutParams[shave_i].width          = width;
        relayoutParams[shave_i].height         = height;
        relayoutParams[shave_i].no_channels    = channels;
        relayoutParams[shave_i].radixX         = radixX;
        relayoutParams[shave_i].radixY         = radixY;
        relayoutParams[shave_i].kernelStrideX  = kernelStrideX;
        relayoutParams[shave_i].kernelStrideY  = kernelStrideY;
        relayoutParams[shave_i].inOutBpp       = 2;
        relayoutParams[shave_i].cmxslice       = getCMXSliceDataSection(shave_i);
        relayoutParams[shave_i].dmaLinkAgent   = myriadResources->dmaLinkAgent;
        relayoutParams[shave_i].pad_style      = pad_style;

        relayoutParams[shave_i].first_pixel = first_pixel;
        relayoutParams[shave_i].no_pixels   = no_pixels;
        relayoutParams[shave_i].version     = CONVOLUTION_V0;

        u32 readBackAddr = (u32)(&(relayoutParams[shave_i].version));
        GET_REG_WORD_VAL(readBackAddr);

        first_pixel += no_pixels;
#if defined(__RTEMS__)
        rtems_cache_writeback_l2();
#else
        DrvLL2CFlushOpOnAllLines(LL2C_OPERATION_WRITE_BACK, 0);
#endif
        startShave(shave_i, (u32)&MODULE_ENTRY(relayout_core), (u32)&relayoutParams[shave_i]);

    }

    waitShaves(myriadResources->firstShave, myriadResources->lastShave);
#if defined(__RTEMS__)
    OsDrvShaveL2CachePartitionFlush(myriadResources->dataPartitionNo, PERFORM_INVALIDATION);
    rtems_cache_invalidate_l2();
#else
    DrvShaveL2CachePartitionFlushAndInvalidate(myriadResources->dataPartitionNo);
    DrvLL2CFlushOpOnAllLines(LL2C_OPERATION_INVALIDATE, 0);
#endif
}

void relayout_conv_v1(fp16 *input, fp16 *output, s32 width, s32 height,
        s32 channels, s32 radixX, s32 radixY,
        s32 kernelStrideX, s32 kernelStrideY, t_MvTensorPaddStyle  pad_style,
        t_MvTensorMyriadResources *myriadResources)
{
    if(!(pad_style == paddStyleTFSame || pad_style == paddStyleTFValid))
        // Does not support other type of padding.
        return;

    s32 shave_i;
    s32 no_shaves = myriadResources->lastShave - myriadResources->firstShave + 1;

    s32 no_width_slices;
    s32 no_height_slices;

    switch(no_shaves)
    {
    case 2:
        no_width_slices  = 1;
        no_height_slices = 2;
        break;
    case 4:
        no_width_slices  = 2;
        no_height_slices = 2;
        break;
    case 6:
        no_width_slices  = 2;
        no_height_slices = 3;
        break;
    case 8:
        no_width_slices  = 2;
        no_height_slices = 4;
        break;
    case 10:
        no_width_slices  = 2;
        no_height_slices = 5;
        break;
    case 12:
        no_width_slices  = 3;
        no_height_slices = 4;
        break;
    default:
        no_width_slices  = 1;
        no_height_slices = no_shaves;
    }

    s32 cols_per_slice = 0;
    s32 remainder_cols = 0;
    s32 rows_per_slice = 0;
    s32 remainder_rows = 0;
    s32 first_pixel_col_offset = 0;
    s32 first_pixel_row_offset = 0;

    switch(pad_style)
    {
    case paddStyleTFSame:
        cols_per_slice = width  / no_width_slices;
        rows_per_slice = height / no_height_slices;

        remainder_cols = width  % no_width_slices;
        remainder_rows = height % no_height_slices;
        break;
    case paddStyleTFValid:
        cols_per_slice = (width  - (radixX - 1)) / no_width_slices;
        rows_per_slice = (height - (radixY - 1)) / no_height_slices;

        remainder_cols = (width  - (radixX - 1)) % no_width_slices;
        remainder_rows = (height - (radixY - 1)) % no_height_slices;

        first_pixel_col_offset = (radixX - 1) >> 1;
        first_pixel_row_offset = (radixY - 1) >> 1;
        break;
    default:
        // use as default paddStyleTFValid
        cols_per_slice = (width  - (radixX - 1)) / no_width_slices;
        rows_per_slice = (height - (radixY - 1)) / no_height_slices;

        remainder_cols = (width  - (radixX - 1)) % no_width_slices;
        remainder_rows = (height - (radixY - 1)) % no_height_slices;

        first_pixel_col_offset = (radixX - 1) >> 1;
        first_pixel_row_offset = (radixY - 1) >> 1;
        break;

    }

    for(s32 w_slice_i = 0; w_slice_i < no_width_slices; ++w_slice_i)
    {
        for(s32 h_slice_i = 0; h_slice_i < no_height_slices; ++h_slice_i)
        {
            shave_i = h_slice_i + w_slice_i * no_height_slices;

            s32 slice_width = cols_per_slice;
            s32 slice_height = rows_per_slice;

            s32 first_pixel_col = first_pixel_col_offset + cols_per_slice * w_slice_i;
            s32 first_pixel_row = first_pixel_row_offset + rows_per_slice * h_slice_i;

            if(remainder_cols)
            {
                if(remainder_cols > w_slice_i)
                {
                    first_pixel_col += w_slice_i;
                    ++slice_width;
                }
                else
                first_pixel_col += remainder_cols;
            }

            if(remainder_rows)
            {
                if(remainder_rows > h_slice_i)
                {
                    first_pixel_row += h_slice_i;
                    ++slice_height;
                }
                else
                first_pixel_row += remainder_rows;
            }

            relayoutParams[shave_i + myriadResources->firstShave].input          = (u8 *)(input);
            relayoutParams[shave_i + myriadResources->firstShave].output         = (u8 *)output;
            relayoutParams[shave_i + myriadResources->firstShave].width          = width;
            relayoutParams[shave_i + myriadResources->firstShave].height         = height;
            relayoutParams[shave_i + myriadResources->firstShave].no_channels    = channels;
            relayoutParams[shave_i + myriadResources->firstShave].radixX         = radixX;
            relayoutParams[shave_i + myriadResources->firstShave].radixY         = radixY;
            relayoutParams[shave_i + myriadResources->firstShave].kernelStrideX  = kernelStrideX;
            relayoutParams[shave_i + myriadResources->firstShave].kernelStrideY  = kernelStrideY;
            relayoutParams[shave_i + myriadResources->firstShave].inOutBpp       = 2;
            relayoutParams[shave_i + myriadResources->firstShave].cmxslice       = getCMXSliceDataSection(shave_i + myriadResources->firstShave);
            relayoutParams[shave_i + myriadResources->firstShave].dmaLinkAgent   = myriadResources->dmaLinkAgent;

            relayoutParams[shave_i + myriadResources->firstShave].first_pixel_col = first_pixel_col;
            relayoutParams[shave_i + myriadResources->firstShave].first_pixel_row = first_pixel_row;
            relayoutParams[shave_i + myriadResources->firstShave].slice_width     = slice_width;
            relayoutParams[shave_i + myriadResources->firstShave].slice_height    = slice_height;

            relayoutParams[shave_i + myriadResources->firstShave].pad_style = pad_style;
            relayoutParams[shave_i + myriadResources->firstShave].version   = CONVOLUTION_V1;

            u32 readBackAddr = (u32)(&(relayoutParams[shave_i + myriadResources->firstShave].version));
            GET_REG_WORD_VAL(readBackAddr);


#if !defined(__RTEMS__)
            DrvLL2CFlushOpOnAllLines(LL2C_OPERATION_WRITE_BACK, 0);
#endif
            startShave(shave_i + myriadResources->firstShave, (u32)&MODULE_ENTRY(relayout_core), (u32)&relayoutParams[shave_i + myriadResources->firstShave]);
        }
    }

    waitShaves(myriadResources->firstShave, myriadResources->lastShave);

#if defined(__RTEMS__)
    OsDrvShaveL2CachePartitionFlush(myriadResources->dataPartitionNo, PERFORM_INVALIDATION);
    rtems_cache_invalidate_l2();
#else
    DrvShaveL2CachePartitionFlushAndInvalidate(myriadResources->dataPartitionNo);
    DrvLL2CFlushOpOnAllLines(LL2C_OPERATION_INVALIDATE, 0);
#endif
}

void relayout_deconv(fp16 *input, fp16 *output, s32 width, s32 height,
        s32 channels, s32 radixX, s32 radixY,
        s32 kernelStrideX, s32 kernelStrideY, t_MvTensorPaddStyle pad_style,
        s32 padX, s32 padY,
        t_MvTensorMyriadResources *myriadResources)
{
    s32 shave_i = 0;

    s32 pad_left_right = radixX - padX - 1;
    s32 pad_top_bottom = radixY - padY - 1;

    if(pad_style == paddStyleTFSame)
    {
        // SAME padding is only valid for odd kernel sizes.
        pad_left_right = radixX >> 1;
        pad_top_bottom = radixY >> 1;
    }
    else if(pad_style == paddStyleTFValid)
    {
        pad_left_right = radixY - 1;
        pad_top_bottom = radixY - 1;
    }
    else
    {
        pad_left_right = radixX - padX - 1;
        pad_top_bottom = radixY - padY - 1;
    }

    s32 no_shaves = myriadResources->lastShave - myriadResources->firstShave + 1;

    s32 no_width_slices;
    s32 no_height_slices;

    s32 volume_width = 2 * pad_left_right + width + (width - 1) * (kernelStrideX - 1);
    s32 volume_height = 2 * pad_top_bottom + height + (height - 1) * (kernelStrideY - 1);

    s32 output_width = volume_width - (radixX - 1);

    s32 cols_per_slice = 0;
    s32 remainder_cols = 0;
    s32 rows_per_slice = 0;
    s32 remainder_rows = 0;

    do
    {
        switch(no_shaves)
        {
        case 2:
            no_width_slices  = 1;
            no_height_slices = 2;
            break;
        case 4:
            no_width_slices  = 2;
            no_height_slices = 2;
            break;
        case 6:
            no_width_slices  = 2;
            no_height_slices = 3;
            break;
        case 8:
            no_width_slices  = 2;
            no_height_slices = 4;
            break;
        case 10:
            no_width_slices  = 2;
            no_height_slices = 5;
            break;
        case 12:
            no_width_slices  = 3;
            no_height_slices = 4;
            break;
        default:
            no_width_slices  = 1;
            no_height_slices = no_shaves;
        }

        cols_per_slice = volume_width / no_width_slices;
        remainder_cols = volume_width % no_width_slices;
        rows_per_slice = volume_height / no_height_slices;
        remainder_rows = volume_height % no_height_slices;

        // Make slice size at least radixX x radixY, in order to output at least one pixel
        // per shave otherwise decrease the number of shaves used.
        --no_shaves;

    } while(cols_per_slice < radixX || rows_per_slice < radixY);

    for(s32 w_slice_i = 0; w_slice_i < no_width_slices; ++w_slice_i)
    {

        for(s32 h_slice_i = 0; h_slice_i < no_height_slices; ++h_slice_i)
        {
            shave_i = h_slice_i + w_slice_i * no_height_slices;

            s32 in_w_offset  = 0;
            s32 in_h_offset  = 0;
            s32 slice_pad_left   = 0;
            s32 slice_pad_right  = 0;
            s32 slice_pad_top    = 0;
            s32 slice_pad_bottom = 0;
            s32 out_w_offset = 0;
            s32 out_h_offset = 0;

            s32 first_col = w_slice_i * cols_per_slice;
            s32 last_col  = first_col + cols_per_slice - 1;

            if(remainder_cols > w_slice_i)
            {
                first_col += w_slice_i;
                last_col += w_slice_i + 1;
            }
            else
            {
                first_col += remainder_cols;
                last_col += remainder_cols;
            }

            if(no_width_slices != 1)
            {
                if(w_slice_i > 0)
                    first_col -= (radixX - 1) >> 1;

                if(w_slice_i < (no_width_slices - 1))
                    last_col += (radixX - 1) >> 1;
            }

            out_w_offset = first_col;

            if(first_col > pad_left_right)
            {
                in_w_offset = (first_col - pad_left_right) / kernelStrideX;
                if((first_col - pad_left_right) % kernelStrideX != 0)
                {
                    slice_pad_left = kernelStrideX - ((first_col - pad_left_right) % kernelStrideX);
                    ++in_w_offset;
                }
            }
            else
            {
                slice_pad_left = pad_left_right - first_col;
                in_w_offset = 0;
            }

            if(last_col < volume_width - pad_left_right - 1)
            {
                slice_pad_right = (last_col - pad_left_right) % kernelStrideX;
                last_col = (last_col - pad_left_right) / kernelStrideX;
            }
            else
            {
                slice_pad_right = last_col - (volume_width - pad_left_right - 1);
                last_col = width - 1;
            }

            s32 first_row = h_slice_i * rows_per_slice;
            s32 last_row  = first_row + rows_per_slice - 1;

            if(remainder_rows > h_slice_i)
            {
                first_row += h_slice_i;
                last_row += h_slice_i + 1;
            }
            else
            {
                first_row += remainder_rows;
                last_row += remainder_rows;
            }

            if(no_height_slices != 1)
            {
                if(h_slice_i > 0)
                    first_row -= (radixY - 1) >> 1;

                if(h_slice_i < (no_height_slices - 1))
                    last_row += (radixY - 1) >> 1;
            }

            out_h_offset = first_row;

            if(first_row > pad_top_bottom)
            {
                in_h_offset = (first_row - pad_top_bottom) / kernelStrideY;
                if((first_row - pad_top_bottom) % kernelStrideY != 0)
                {
                    slice_pad_top = kernelStrideY - ((first_row - pad_top_bottom) % kernelStrideY);
                    ++in_h_offset;
                }
            }
            else
            {
                slice_pad_top = pad_top_bottom - first_row;
                in_h_offset = 0;
            }

            if(last_row < volume_height - pad_top_bottom - 1)
            {
                slice_pad_bottom = (last_row - pad_top_bottom) % kernelStrideY;
                last_row = (last_row - pad_top_bottom) / kernelStrideY;
            }
            else
            {
                slice_pad_bottom = last_row - (volume_height - pad_top_bottom - 1);
                last_row = height - 1;
            }

            relayoutParams[shave_i + myriadResources->firstShave].input          = (u8 *)(input + (in_h_offset * width + in_w_offset) * channels);
            relayoutParams[shave_i + myriadResources->firstShave].output         = (u8 *)(output + (out_h_offset * output_width + out_w_offset) * channels * radixX * radixY);
            relayoutParams[shave_i + myriadResources->firstShave].width          = last_col - in_w_offset + 1;
            relayoutParams[shave_i + myriadResources->firstShave].height         = last_row - in_h_offset + 1;
            relayoutParams[shave_i + myriadResources->firstShave].no_channels    = channels;
            relayoutParams[shave_i + myriadResources->firstShave].radixX         = radixX;
            relayoutParams[shave_i + myriadResources->firstShave].radixY         = radixY;
            relayoutParams[shave_i + myriadResources->firstShave].kernelStrideX  = kernelStrideX;
            relayoutParams[shave_i + myriadResources->firstShave].kernelStrideY  = kernelStrideY;
            relayoutParams[shave_i + myriadResources->firstShave].inOutBpp       = 2;
            relayoutParams[shave_i + myriadResources->firstShave].cmxslice       = getCMXSliceDataSection(shave_i + myriadResources->firstShave);
            relayoutParams[shave_i + myriadResources->firstShave].dmaLinkAgent   = myriadResources->dmaLinkAgent;
            relayoutParams[shave_i + myriadResources->firstShave].pad_left       = slice_pad_left;
            relayoutParams[shave_i + myriadResources->firstShave].pad_right      = slice_pad_right;
            relayoutParams[shave_i + myriadResources->firstShave].pad_top        = slice_pad_top;
            relayoutParams[shave_i + myriadResources->firstShave].pad_bottom     = slice_pad_bottom;

            relayoutParams[shave_i + myriadResources->firstShave].input_stride   = width * channels * 2;
            relayoutParams[shave_i + myriadResources->firstShave].output_stride  = output_width * channels * radixX * radixY * 2;

            relayoutParams[shave_i].version   = DECONVOLUTION;

            u32 readBackAddr = (u32)(&(relayoutParams[shave_i + myriadResources->firstShave].version));
            GET_REG_WORD_VAL(readBackAddr);

#if !defined(__RTEMS__)
            DrvLL2CFlushOpOnAllLines(LL2C_OPERATION_WRITE_BACK, 0);
#endif

            startShave(shave_i, (u32)&MODULE_ENTRY(relayout_core), (u32)&relayoutParams[shave_i]);
        }
    }

    waitShaves(myriadResources->firstShave, myriadResources->lastShave);

#if defined(__RTEMS__)
    OsDrvShaveL2CachePartitionFlush(myriadResources->dataPartitionNo, PERFORM_INVALIDATION);
    rtems_cache_invalidate_l2();
#else
    DrvShaveL2CachePartitionFlushAndInvalidate(myriadResources->dataPartitionNo);
    DrvLL2CFlushOpOnAllLines(LL2C_OPERATION_INVALIDATE, 0);
#endif
}
