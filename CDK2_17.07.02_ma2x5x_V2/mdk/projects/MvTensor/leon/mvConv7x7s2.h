///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Simple effect header
///

#ifndef _CONV7x7S2_H_
#define _CONV7x7S2_H_

// 1: Includes
// ----------------------------------------------------------------------------
#include <mv_types.h>
#include <mvTensor.h>

#ifdef __cplusplus
extern "C"
{
#endif

// 2:  Exported Global Data (generally better to avoid)
// ----------------------------------------------------------------------------
// 3:  Exported Functions (non-inline)
// ----------------------------------------------------------------------------

void conv7x7s2(fp16* input, u32 in_height, u32 in_width,
               u32 in_channels, u32 out_channels,
               fp16* weights, fp16* output,
               t_MvTensorMyriadResources *myriadResources);

#ifdef __cplusplus
}
#endif

#endif  //_CONV7x7S2_H_
