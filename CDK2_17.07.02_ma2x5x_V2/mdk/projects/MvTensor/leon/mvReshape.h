///
/// \file
/// \copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///

#ifndef _MV_RESHAPE_H_
#define _MV_RESHAPE_H_

#include <mvTensor.h>

void reshape(fp16 *input, fp16 *output,
            s32 in_width, s32 in_height,
            s32 out_width, s32 out_height, s32 out_channels,
            u32 inputStride, u32 outputStride,
            t_MvTensorMyriadResources *myriadResources);

#endif /* _MV_RESHAPE_H_ */
