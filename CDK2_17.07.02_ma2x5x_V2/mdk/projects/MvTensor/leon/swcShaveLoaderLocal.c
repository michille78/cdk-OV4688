/**
 * This is simple module to manage shave loading and windowed addresses
 *
 * @File
 * @Author    Cormac Brick
 * @Brief     Simple Shave loader and window manager
 * @copyright All code copyright Movidius Ltd 2012, all rights reserved
 *            For License Warranty see: common/license.txt
 *
 * @TODO(Cormac):  Some svu support stuff is duplicated here which should find a new home
 */

#include "swcShaveLoader.h"
#include "swcShaveLoaderPrivate.h"
#include <stdlib.h>
#include <stdio.h>
#include "swcMemoryTransfer.h"
#include <swcLeonUtils.h>
#include "registersMyriad.h"
#include "DrvSvu.h"
#include "assert.h"
#include "mv_types.h"
#include <DrvIcb.h>
#include <stdarg.h>
#include <dbgTracerApi.h>
#include <DrvCpr.h>
#include "DrvMutex.h"
#include "mvMacros.h"
#include <swcShaveLoader.h>

#ifdef __RTEMS__
#include <OsDrvSvu.h>
extern osDrvSvuHandler_t* osDrvSvuHandler[TOTAL_NUM_SHAVES];
extern osCommonIrqShareInfo_t osCommonIrqSharedInfo;
extern osDrvSvuHandler_t osDynHandler[TOTAL_NUM_SHAVES];

#define OS_DRV_SVU_LOCK(x)                osDrvSvuHandler[x]?protectionLock(x, osDrvSvuHandler[x]->protection):OS_MYR_DRV_NOTOPENED
#define OS_DRV_SVU_UNLOCK(x)              osDrvSvuHandler[x]?protectionUnlock(x, osDrvSvuHandler[x]->protection):OS_MYR_DRV_NOTOPENED

#endif

void swcDynSetShaveWindows(DynamicContext_t *moduleStData, u32 shaveNumber)
{
    //And set data and code windows according to code size needs
    swcSetShaveWindow(shaveNumber,WINDOW_A_NO,CMX_BASE_ADR+shaveNumber*CMX_SLICE_SIZE + moduleStData->cmxCriticalCodeSize);
    swcSetShaveWindow(shaveNumber,WINDOW_B_NO,CMX_BASE_ADR+shaveNumber*CMX_SLICE_SIZE);
    /*0x1Exxxxxx window needs to be set to the module group address before we load data*/
    swcSetShaveWindow(shaveNumber,WINDOW_C_NO,(u32)moduleStData->instancesData->GrpDataPools[shaveNumber]);
}

static void swcDynStartShaveDirect(u32 shave_nr, u32 SubModule)
{
    // Run the program. Note the first 2 steps are optional
    SET_REG_WORD(SVU_CTRL_ADDR[shave_nr] + SLC_OFFSET_SVU + SVU_OCR, OCR_STOP_GO | OCR_TRACE_ENABLE);// Set STOP bit in control register
    SET_REG_WORD(SVU_CTRL_ADDR[shave_nr] + SLC_OFFSET_SVU + SVU_IRR, 0xFFFFFFFF); // Clear any interrupts from previous test
    SET_REG_WORD(SVU_CTRL_ADDR[shave_nr] + SLC_OFFSET_SVU + SVU_ICR, 0x20);       // Enable SWI interrupt

    //Start Shave
    DrvSvuStart(shave_nr, (u32)((DynamicContext_t*)SubModule)->entryPoint);
}

static void swcSetupDynApp(DynamicContext_t *moduleStData, u32 shaveNumber)
{
    swcSetShaveWindowsToDefault(shaveNumber);
    //And set data and code windows according to code size needs
    swcSetShaveWindow(shaveNumber,WINDOW_A_NO,CMX_BASE_ADR+shaveNumber*CMX_SLICE_SIZE + moduleStData->cmxCriticalCodeSize);
    swcSetShaveWindow(shaveNumber,WINDOW_B_NO,CMX_BASE_ADR+shaveNumber*CMX_SLICE_SIZE);
    swcLoadshvdlib((u8*)moduleStData->appdyndata,shaveNumber);
    /*0x1Exxxxxx window needs to be set to the module group address before we load data*/
    swcSetShaveWindow(shaveNumber,WINDOW_C_NO,(u32)moduleStData->instancesData->GrpDataPools[shaveNumber]);
    swcLoadshvdlib((u8*)moduleStData->groupappdyndata,shaveNumber);
    swcSetWindowedDefaultStack(shaveNumber);
}

static s32 swcIsShaveAllocationValidLocal(DynamicContext_t *moduleData, swcShaveUnit_t shaveNumber)
{
    uint32_t i, j, perAppInstances;

    if (shaveNumber >= TOTAL_NUM_SHAVES)
        return MYR_DYN_INFR_INVALID_PARAMETERS;
    for (i=0;i<(unsigned int)GlobalContextData.DynamicContextAppsNumber;i++){

        perAppInstances = GlobalContextData.DynamicContextGlobalArray[i].module->instancesData->appInstances;
        /* we have to exclude current caller application */
        if (moduleData->entryPoint != GlobalContextData.DynamicContextGlobalArray[i].module->entryPoint)
            for(j=0;j < perAppInstances;j++)
            {
                if (GlobalContextData.DynamicContextGlobalArray[i].module->instancesData->shaveList[j] == shaveNumber)
                    /* error: shave already allocated to another app */
                    return MYR_DYN_INFR_MULTIPLE_SHAVE_ALLOCATION;
            }
    }
    return MYR_DYN_INFR_SUCCESS;
}

static s32 swcSetupAppDataPools(DynamicContext_t *mData, u32 shaveNumber)
{
    // allocate group data and heap space for this instance
    // add space for 64 bytes for possible alignment to cache line
    // round up the size to the cache line size (64)
    mData->instancesData->HeapPoolsStart[shaveNumber] = malloc((size_t)ROUND_UP(mData->heap_size + 64, 64));
    if (mData->instancesData->HeapPoolsStart[shaveNumber] == NULL)
        return MYR_DYN_INFR_CANNOT_ALLOC_HEAP;

    // Group data is used in its separate SHAVE window and each window needs 1024 (1K alignments) so using 1024 below
    // Round up the size to the cache line size (64)
    mData->instancesData->GrpDataPoolsStart[shaveNumber] = malloc((size_t)ROUND_UP(mData->groupappdyndatasize + 1024, 64));
    if (mData->instancesData->GrpDataPoolsStart[shaveNumber] == NULL)
        return MYR_DYN_INFR_CANNOT_ALLOC_GROUPDATA;

    // And update the module fields so that the next instantiation starts from the proper addresses
    // Round up to sensible 64 bytes alignments
    mData->instancesData->HeapPools[shaveNumber] = ALIGN_UP(mData->instancesData->HeapPoolsStart[shaveNumber], 64);;
    mData->instancesData->GrpDataPools[shaveNumber] = ALIGN_UP(mData->instancesData->GrpDataPoolsStart[shaveNumber], 1024);
    return MYR_DYN_INFR_SUCCESS;
}

static s32 swcSetupDynShaveAppsCore(DynamicContext_t *mData, const swcShaveUnit_t *svuList,
    const uint32_t instances, const uint32_t loadInstances)
{
    uint32_t i=0;
    MYRIAD_DYNAMIC_INFRASTR_STATUS_CODE status = MYR_DYN_INFR_ERROR;

    if ( (mData != NULL) && (svuList != NULL) && \
         (instances > 0) && (instances <= TOTAL_NUM_SHAVES) )
    {
        mData->instancesData->appInstances = instances;
        mData->instancesData->shaveList = (swcShaveUnit_t*) svuList;
        for (i=0; i < instances; i++){
            /* check if shave allocation is valid */
            status = swcIsShaveAllocationValidLocal(mData, mData->instancesData->shaveList[i]);
            if (status == MYR_DYN_INFR_SUCCESS){
                /* allocate needed memory for this instance */
                status = swcSetupAppDataPools(mData, svuList[i]);
                if (loadInstances)
                    swcSetupDynApp(mData, svuList[i]);
                if (status != MYR_DYN_INFR_SUCCESS){
                    //Initialize all windows to the historical default Myriad code size -> 32K
                    mData->cmxCriticalCodeSize=(u32)DEFAULT_CMX_CODE_SIZE;
                    return status;
                }
            } else
                return status;
        }
    }
    else
        status = MYR_DYN_INFR_INVALID_PARAMETERS;
    return status;
}

/*TODO add error handling */
s32 swcSetupDynShaveAppsLocal(DynamicContext_t *mData, const swcShaveUnit_t *svuList, const uint32_t instances)
{
    return swcSetupDynShaveAppsCore(mData, svuList, instances, FALSE);
}

/*TODO add error handling */
s32 swcSetupDynShaveAppsComplete(DynamicContext_t *mData, const swcShaveUnit_t *svuList, const uint32_t instances)
{
#ifdef __RTEMS__
    for(u32 i = 0; i < instances; i++)
    {
        //Clear all interrupts so we can have the next interrupt happening
        SET_REG_WORD(DCU_IRR((u32)svuList[i]), 0xFFFFFFFF);

        //Disable ICB (Interrupt Control Block) while setting new interrupt
        DrvIcbDisableIrq(IRQ_SVE_0 + (u32)svuList[i]);
        DrvIcbIrqClear(IRQ_SVE_0 + (u32)svuList[i]);
        DrvIcbIrqUnsetup(IRQ_SVE_0 + (u32)svuList[i]);
        //Configure interrupt handlers
        DrvIcbSetIrqHandler(IRQ_SVE_0 + (u32)svuList[i], osDrvSvuIrqHandler);
        //Enable interrupts on SHAVE done
        DrvIcbConfigureIrq(IRQ_SVE_0 + (u32)svuList[i], SHAVE_INTERRUPT_LEVEL, POS_EDGE_INT);
        DrvIcbEnableIrq(IRQ_SVE_0 + (u32)svuList[i]);

        //Enable SWIH IRQ sources
        SET_REG_WORD(DCU_ICR((u32)svuList[i]), ICR_SWI_ENABLE);
    }
#endif
    return swcSetupDynShaveAppsCore(mData, svuList, instances, TRUE);
}

s32 swcRunShaveAlgoOnAssignedShaveCCNoSetup(DynamicContext_t *moduleStData, u32 shaveNumber, const char *fmt, ...)
{
    va_list a_list;
    va_start(a_list, fmt);

    swcSetRegsCC(shaveNumber, fmt, a_list);
    
    //And set data and code windows according to code size needs
    swcSetShaveWindow(shaveNumber,WINDOW_A_NO,CMX_BASE_ADR+shaveNumber*CMX_SLICE_SIZE + moduleStData->cmxCriticalCodeSize);
    swcSetShaveWindow(shaveNumber,WINDOW_B_NO,CMX_BASE_ADR+shaveNumber*CMX_SLICE_SIZE);
    /*0x1Exxxxxx window needs to be set to the module group address before we load data*/
    swcSetShaveWindow(shaveNumber,WINDOW_C_NO,(u32)moduleStData->instancesData->GrpDataPools[shaveNumber]);
    swcSetWindowedDefaultStack(shaveNumber);

    swcDynStartShaveDirect(shaveNumber, (u32)moduleStData);

    va_end(a_list);
    return ((s32) shaveNumber);
}

#ifdef __RTEMS__
static int OsDrvSvuDynStartShave(osDrvSvuHandler_t* handler, u32 SubModule)
{
    OS_MYRIAD_DRIVER_STATUS_CODE res;

    res = OS_DRV_SVU_LOCK(handler->shaveNumber);
    if (res == OS_MYR_DRV_SUCCESS)
    {
        if (handler->shaveState == OS_DRV_SVU_TAKEN)
        {
            updateTaskId(handler);
            handler->shaveState = OS_DRV_SVU_RUNNING;
            swcDynStartShaveDirect(handler->shaveNumber, (u32)SubModule);
        }
        else if (handler->shaveState == OS_DRV_SVU_RUNNING)
        {
            res = OS_MYR_DRV_RESOURCE_BUSY;
        }
        else
        {
            res = OS_MYR_DRV_NOTOPENED;
        }
        OS_DRV_SVU_UNLOCK(handler->shaveNumber);
    }
    return res;
}

#include<DrvTimer.h>
int OsDrvSvuRunShaveAlgoOnAssignedShaveCCNoSetup(DynamicContext_t *moduleStData, u32 shvNumber, const char *fmt, ...)
{
    int osRetCod = OS_MYR_DRV_SUCCESS;
    va_list a_list;
    if (swcCheckFreeAndValidShave(moduleStData, shvNumber) == 0)
        osRetCod = OS_MYR_DYN_INFR_SHAVE_BUSY_OR_INVALID;
    else
    {
        if (osRetCod == OS_MYR_DRV_SUCCESS)
        {
            va_start(a_list, fmt);
            swcSetRegsCC(osDynHandler[shvNumber].shaveNumber, fmt, a_list);
            va_end(a_list);

               osRetCod = OsDrvSvuSetWindowedDefaultStack(&osDynHandler[shvNumber]);
               if (osRetCod != OS_MYR_DRV_SUCCESS)
                   return osRetCod;
               osRetCod = OsDrvSvuDynStartShave(&osDynHandler[shvNumber], (u32)moduleStData);
               return osRetCod;
        }
    }
    return osRetCod;
}

#endif
