///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Simple effect header
///

#ifndef _SPATIALCONV_H_
#define _SPATIALCONV_H_

// 1: Includes
// ----------------------------------------------------------------------------
#include <mv_types.h>
#include <mvTensor.h>

#ifdef __cplusplus
extern "C"
{
#endif

// 2:  Exported Global Data (generally better to avoid)
// ----------------------------------------------------------------------------
// 3:  Exported Functions (non-inline)
// ----------------------------------------------------------------------------

void spatialconv(fp16* input, u32 in_height, u32 in_width,
               u32 in_channels, u32 out_channels,
               u32 radix_x, u32 radix_y,
               u32 stride_x, u32 stride_y,
               u32 padding_type,
               fp16* weights, fp16* output,
               t_MvTensorMyriadResources *myriadResources);

#ifdef __cplusplus
}
#endif

#endif  //_SPATIALCONV_H_
