///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     MvTensor API
///


#include "mvTensor.h"

#include <stdio.h>
#include <malloc.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <assert.h>

#if defined(__RTEMS__)
#include <OsDrvTimer.h>
#include <OsDrvCpr.h>
#include <OsDrvCmxDma.h>
#include <OsDrvShaveL2Cache.h>
#else
#include <DrvTimer.h>
#endif

#include <mvMacros.h>
#include <DrvSvu.h>
#include <DrvCmxDma.h>
#include <DrvLeonL2C.h>
#include <swcShaveLoader.h>

// project includes
#include <matmul_iface.h>
#include <matmul_profile.h>
#include <matmul_leon.h>
#include <matmul.h>

#include <mvTensorDebug.h>
#include "mvMaxPool3x3.h"
#include "mvMaxPool2x2.h"
#include "mvMaxPoolMxN.h"
#include "mvAvgPool3x3.h"
#include "mvAvgPool7x7xk.h"
#include "mvAvgPoolMxN.h"
#include "mvSoftMax.h"
#include "mvFC.h"
#include "mvDepthConv.h"
#include "lrn.h"
#include "postOps.h"
#include "mvEltwise.h"
#include "mvRelayout.h"
#include "mvReshape.h"
#include <mvConv7x7s2.h>
#include <mvSpatialConv.h>
#include "mvCrop.h"

#define K_MAX 2555
#define ALIGN_VALUE 64
#define DEBUG 0

#if DEBUG
#define MVT_DPRINTF(...) printf(__VA_ARGS__)
#else
#define MVT_DPRINTF(...)
#endif

// Select which specific optimised conv and maxpool to use (if any)
u32 OPT_CONV3X3_S1         = (1<<opt_conv_3_3_1_1_specific);   // NOTE: when enabled, the output buffer has to be
                                // overallocated by 2 * pad bytes, where pad is
                                // (mvTensorParam->input->dimX + 1) * outputStride
                                // see the convoltuion 3x3s1 SAME implementation
u32  OPT_IM2COL            = (1<<opt_conv_im2col);
u32  OPT_IM2COL_V2         = (1<<opt_conv_im2col_v2);
u32  OPT_CONV3X3_S2        = (1<<opt_conv_3_3_2_2_specific);
u32  OPT_CONV5X5_S1        = (1<<opt_conv_5_5_1_1_specific);   // NOTE: when enabled, the output buffer has to be
                                // overallocated by 2 * pad bytes, where pad is
                                // (2 * mvTensorParam->input->dimX + 2) * outputStride
u32  OPT_CONV5X5_S2        = (1<<opt_conv_5_5_2_2_specific);
u32  OPT_CONV7X7_S2        = (1<<opt_conv_7_7_2_2_specific);
u32  OPT_MAXPOOL2X2        = (1<<opt_maxpool_2_2_2_2_specific);
u32  OPT_MAXPOOL3X3        = (1<<opt_maxpool_3_3_1_1_specific);
u32  OPT_SPATIALCONV7X7_S2 = (1<<opt_conv_7_7_2_2_spatial);
u32  OPT_SPATIALCONV       = (1<<opt_conv_generic_spatial);

u32 OPT_DECONV_SAME_3X3_S1         = (1<<opt_deconv_3_3_1_1_same_specific);   // NOTE: when enabled, the output buffer has to be
                                // overallocated by 2 * pad bytes, where pad is
                                // (mvTensorParam->input->dimX + 1) * outputStride
                                // see the convoltuion 3x3s1 SAME implementation
u32 OPT_DECONV_SAME_5X5_S1        = (1<<opt_deconv_5_5_1_1_same_specific);   // NOTE: when enabled, the output buffer has to be
                                // overallocated by 2 * pad bytes, where pad is
                                // (2 * mvTensorParam->input->dimX + 2) * outputStride
u32 OPT_SPATIAL_DECONV_SAME_S1    = (1<<opt_deconv_M_N_1_1_same_spatial);
u32 OPT_DECONV_GENERIC_SAME_S1    = (1<<opt_deconv_M_N_1_1_same_generic);
u32 OPT_POWER                     = (1 << opt_power_accurate);

#ifdef FATHOMRUN
const u32 g_optimisationsDefault =
    OPT_IM2COL_V2 |
    OPT_SPATIALCONV7X7_S2 |
    OPT_CONV3X3_S1 |
    OPT_CONV5X5_S1 |
    OPT_SPATIALCONV |
    OPT_MAXPOOL3X3 |
    OPT_MAXPOOL2X2;
#else
const u32 g_optimisationsDefault =
    OPT_CONV3X3_S1 |
    OPT_CONV5X5_S1 |
    OPT_CONV7X7_S2 |
    OPT_MAXPOOL3X3 |
    OPT_SPATIALCONV7X7_S2 |
    OPT_MAXPOOL2X2;
#endif

// Select if you want to use matmul debug trace
const u8 g_enableMatmulDebugTrace = 0;

//ErrorManager mvt_err_mngr;

// Function that check for errors on input data
static u32 checkForErrors(t_MvTensorParam *mvTensorParam);


unsigned short** GetBuffersForStride2(unsigned short* input, u32 H, u32 W, u32 K) {
    const u32 kStride = 2;
    unsigned short **buffers;
    u32 num_buffers;
    u32 idx_buffer;
    u32 i, j, k;

    num_buffers = 4;
    buffers = (unsigned short**)malloc(num_buffers*sizeof(unsigned short*));
    mvTensorAssert(buffers != NULL, "Unable to allocate space for stride2 buffer container. ");
    for (i = 0; i < num_buffers; i++) {
        buffers[i] = (unsigned short*)malloc(W/kStride*H/kStride*K * sizeof(unsigned short));
        mvTensorAssert(buffers[i] != NULL, "Unable to allocate enough space for stride2 buffers. ");
    }

    idx_buffer = 0;
    if (K==3) {
      for (i = 0; i < H; i+=kStride)
          for (j = 0; j < W; j+=kStride)
          {
                // A_ee  (even-even)
                buffers[0][idx_buffer+0] = input[((i+0)*W+(j+0))*K+0];
                buffers[0][idx_buffer+1] = input[((i+0)*W+(j+0))*K+1];
                buffers[0][idx_buffer+2] = input[((i+0)*W+(j+0))*K+2];

                // A_eo  (even-odd)
                buffers[1][idx_buffer+0] = input[((i+0)*W+(j+1))*K+0];
                buffers[1][idx_buffer+1] = input[((i+0)*W+(j+1))*K+1];
                buffers[1][idx_buffer+2] = input[((i+0)*W+(j+1))*K+2];

                // A_oe  (odd-even)
                buffers[2][idx_buffer+0] = input[((i+1)*W+(j+0))*K+0];
                buffers[2][idx_buffer+1] = input[((i+1)*W+(j+0))*K+1];
                buffers[2][idx_buffer+2] = input[((i+1)*W+(j+0))*K+2];

                // A_oo  (odd-odd)
                buffers[3][idx_buffer+0] = input[((i+1)*W+(j+1))*K+0];
                buffers[3][idx_buffer+1] = input[((i+1)*W+(j+1))*K+1];
                buffers[3][idx_buffer+2] = input[((i+1)*W+(j+1))*K+2];

                idx_buffer+=3;
          }
    }
    else
    {
      idx_buffer = 0;  // buffers[0] -> A_ee
      for (i = 0; i < H; i+=kStride)
          for (j = 0; j < W; j+=kStride)
              for (k = 0; k < K; k++)
                  buffers[0][idx_buffer++] = input[(i*W+j)*K+k];
      idx_buffer = 0;  // buffers[1] -> A_eo
      for (i = 0; i < H; i+=kStride)
          for (j = 1; j < W; j+=kStride)
              for (k = 0; k < K; k++)
                  buffers[1][idx_buffer++] = input[(i*W+j)*K+k];
      idx_buffer = 0;  // buffers[2] -> A_oe
      for (i = 1; i < H; i+=kStride)
          for (j = 0; j < W; j+=kStride)
              for (k = 0; k < K; k++)
                  buffers[2][idx_buffer++] = input[(i*W+j)*K+k];
      idx_buffer = 0;  // buffers[3] -> A_oo
      for (i = 1; i < H; i+=kStride)
          for (j = 1; j < W; j+=kStride)
              for (k = 0; k < K; k++)
                  buffers[3][idx_buffer++] = input[(i*W+j)*K+k];

    }

    return buffers;
}



void* mvTensor(void *params)
{
    // Start counting ticks
    tyTimeStamp timer_data;
    u64 cyclesElapsed;
    t_MvTensorParam *mvTensorParam = (t_MvTensorParam*) params;
    char * dbgBuf = mvTensorParam->debugInfo->debugMsg;
    ErrorManager::Init(dbgBuf);

    dmaTransactionList_t *task1 = mvTensorParam->myriadResources->dmaTransactions;
    mvTensorAssert((u32)task1 & 0x70000000, "DMA transaction param should be in CMX!");

#if defined(__RTEMS__)
    int sc;
    dmaRequesterId dmaReqId;

    OsDrvTimerInit();
    OsDrvTimerStartTicksCount(&timer_data);
#else
    DrvTimerStartTicksCount(&timer_data);
#endif


    u32 g_optimisations;
    int lastShave = mvTensorParam->myriadResources->lastShave;
    // Select optimizations that we want to use
    if(mvTensorParam->op->optMask != MV_TENSOR_DEFAULT_OPT)
    {
        g_optimisations = mvTensorParam->op->optMask;
        if(g_optimisations & 0x78000000)
        {
            int lastShaveOpt = mvTensorParam->myriadResources->firstShave + (g_optimisations >> 27) - 1;
            if(lastShaveOpt <= lastShave)
                lastShave = lastShaveOpt;
            else
                mvTensorAssert(0,"Number of shaves required in optMask for this layer is bigger then number of allocated shaves!");
        }

    } else {
        g_optimisations = g_optimisationsDefault;
    }

    int outDimX, outDimY, outDimZ;
    outDimZ = mvTensorParam->output->dimZ;
    if(mvTensorParam->op->type == kDeconvolution)
    {
        u32 padX, padY;
        switch(mvTensorParam->op->paddStyle)
        {
        case paddStyleTFSame:
            MVT_DPRINTF("Padding: TF-SAME\n");
            padX = (mvTensorParam->op->radixX - 1) >> 1;
            padY = (mvTensorParam->op->radixY - 1) >> 1;
            break;
        case paddStyleCaffe:
            MVT_DPRINTF("Padding: CAFFE\n");
            padX = mvTensorParam->op->padX;
            padY = mvTensorParam->op->padY;
            break;
        default:
            MVT_DPRINTF("Padding: TF-VALID/NONE\n");
            padX =  mvTensorParam->op->radixX - 1;
            padY =  mvTensorParam->op->radixY - 1;
        }

        outDimX = mvTensorParam->op->strideX * (mvTensorParam->input->dimX - 1) + mvTensorParam->op->radixX - 2 * padX;
        outDimY = mvTensorParam->op->strideY * (mvTensorParam->input->dimY - 1) + mvTensorParam->op->radixY - 2 * padY;
    }
    else if (mvTensorParam->op->type == kReshape)
    {
        MVT_DPRINTF("dimX=%d\n", mvTensorParam->output->dimX);
        MVT_DPRINTF("dimY=%d\n", mvTensorParam->output->dimY);
        MVT_DPRINTF("dimZ=%d\n", mvTensorParam->output->dimZ);

        outDimX = mvTensorParam->output->dimX;
        outDimY = mvTensorParam->output->dimY;
        outDimZ = mvTensorParam->output->dimZ;
        u32 totalDim = mvTensorParam->input->dimX * mvTensorParam->input->dimY * mvTensorParam->input->dimZ;

        if (outDimX == 0)
        {
            outDimX = mvTensorParam->input->dimX;
        }
        if (outDimY == 0)
        {
            outDimY = mvTensorParam->input->dimY;
        }
        if (outDimZ == 0)
        {
            outDimZ = mvTensorParam->input->dimZ;
        }

        if (outDimX == -1)
        {
            outDimX = totalDim / (outDimY * outDimZ);
        }
        if (outDimY == -1)
        {
            outDimY = totalDim / (outDimX * outDimZ);
        }
        if (outDimZ == -1)
        {
            outDimZ = totalDim / (outDimX * outDimY);
        }
    }
    else if (mvTensorParam->op->type == kAvgPool ||
            mvTensorParam->op->type == kMaxPool)
    {
        switch(mvTensorParam->op->paddStyle)
        {
        case paddStyleTFSame:
            MVT_DPRINTF("Padding: TF-SAME\n");
            outDimX = (mvTensorParam->input->dimX + mvTensorParam->op->strideX - 1)/mvTensorParam->op->strideX;
            outDimY = (mvTensorParam->input->dimY + mvTensorParam->op->strideY - 1)/mvTensorParam->op->strideY;
            break;
        case paddStyleCaffe:
            MVT_DPRINTF("Padding: CAFFE\n");
            if(mvTensorParam->op->type == kAvgPool || mvTensorParam->op->type == kMaxPool)
            {
                outDimX = (mvTensorParam->input->dimX + 2*mvTensorParam->op->padX - mvTensorParam->op->radixX + mvTensorParam->op->strideX - 1)/mvTensorParam->op->strideX + 1;
                outDimY = (mvTensorParam->input->dimY + 2*mvTensorParam->op->padY - mvTensorParam->op->radixY + mvTensorParam->op->strideY - 1)/mvTensorParam->op->strideY + 1;
                outDimX = MIN(outDimX, (int)((mvTensorParam->input->dimX + mvTensorParam->op->padX + mvTensorParam->op->strideX - 1)/ mvTensorParam->op->strideX));
                outDimY = MIN(outDimY, (int)((mvTensorParam->input->dimY + mvTensorParam->op->padY + mvTensorParam->op->strideY - 1)/ mvTensorParam->op->strideY));
            }
            else
            {
                outDimX = (mvTensorParam->input->dimX + 2*mvTensorParam->op->padX - mvTensorParam->op->radixX )/mvTensorParam->op->strideX + 1;
                outDimY = (mvTensorParam->input->dimY + 2*mvTensorParam->op->padY - mvTensorParam->op->radixY )/mvTensorParam->op->strideY + 1;
            }
            break;
        default:
            MVT_DPRINTF("Padding: TF-VALID/NONE\n");
            outDimX = (mvTensorParam->input->dimX - mvTensorParam->op->radixX + mvTensorParam->op->strideX)/mvTensorParam->op->strideX;
            outDimY = (mvTensorParam->input->dimY - mvTensorParam->op->radixY + mvTensorParam->op->strideY)/mvTensorParam->op->strideY;
        }
    }
    else
    {
        // The code is temporary, to maintain backwards compatibility. 
        // Normally, we should switch to the new paradigm, that is:
        // mvTensorParam->output->dim* are correctly computed based on the input dimensions, stride and radix,
        // as oposed to the current situation, where output dimensions  sent as input parameteres to mvTensor
        outDimX = mvTensorParam->output->dimX;
        outDimY = mvTensorParam->output->dimY;
        outDimZ = mvTensorParam->output->dimZ;
    }

#define RVA_TEMP
#ifdef RVA_TEMP
    // The code under RVA_TEMP is temporary, until the convolution kernels are switched to the new paradigm, that is:
    // mvTensorParam->output->dim* are computed based on the input dimensions, stride and radix,
    // as oposed to the current situation, where output dimensions  sent as input parameteres to mvTensor

    MVT_DPRINTF("mvTensorParam->input->dimX=%d, mvTensorParam->input->dimY=%d, mvTensorParam->input->dimZ=%d \n", mvTensorParam->input->dimX, mvTensorParam->input->dimY, mvTensorParam->input->dimZ);
    MVT_DPRINTF("mvTensorParam->output->dimX=%d, mvTensorParam->output->dimY=%d, mvTensorParam->output->dimZ=%d \n", mvTensorParam->output->dimX, mvTensorParam->output->dimY, mvTensorParam->output->dimZ);
    MVT_DPRINTF("outDimX=%d, outDimY=%d outDimZ=%d \n", outDimX, outDimY, outDimZ);

    int useSamePadding;

    // This condition will become obsolete, but for now, useSamePadding is still used by the convolution layre
    if ( ceil(float(mvTensorParam->input->dimX) / float(mvTensorParam->op->strideX)) - mvTensorParam->output->dimX == 0 &&
            ceil(float(mvTensorParam->input->dimY) / float(mvTensorParam->op->strideY)) - mvTensorParam->output->dimY == 0)
    {
        MVT_DPRINTF("SAME\n");
        useSamePadding = 1;
    }
    else
    {
        // output has the same width and height as the input
        MVT_DPRINTF("VALID\n");
        useSamePadding = 0;
    }
#endif

    mvTensorParam->output->dimX = outDimX;
    mvTensorParam->output->dimY = outDimY;
    mvTensorParam->output->dimZ = outDimZ;

    MVT_DPRINTF("mvTensorParam->output->dimX=%d, mvTensorParam->output->dimY=%d, mvTensorParam->output->dimZ=%d \n", mvTensorParam->output->dimX, mvTensorParam->output->dimY, mvTensorParam->output->dimZ);

    // Check for errors on input data
    u32 errorFound = checkForErrors(mvTensorParam);
    if(errorFound)
    {
#if defined(__RTEMS__)
        OsDrvTimerGetElapsedTicks(&timer_data,&cyclesElapsed);
        uint32_t clocksPerUs;
        OsDrvCprGetSysClockPerUs(&clocksPerUs);

        mvTensorParam->debugInfo->ms = cyclesElapsed / 1000.0 / clocksPerUs;
#else
        DrvTimerGetElapsedTicks(&timer_data,&cyclesElapsed);
        mvTensorParam->debugInfo->ms = DrvTimerTicksToMs(cyclesElapsed);
#endif
        return NULL;
    }

    u32 shaveNo = lastShave - mvTensorParam->myriadResources->firstShave + 1;
    // input, weights, and output are treated as matrices which have the following strides
    u32 inputStride , weightsStride, outputStride;
    switch(mvTensorParam->input->storageOrder){
        case orderZYX:
            inputStride = mvTensorParam->input->dimYStride;
            break;
        case orderYZX:
            inputStride = mvTensorParam->input->dimZStride;
            break;
        case orderYXZ:
            inputStride = mvTensorParam->input->dimXStride;
            break;
        default:
            inputStride = mvTensorParam->input->dimXStride;
            break;
    }

    if(mvTensorParam->op->type == kConv || mvTensorParam->op->type == kDepthConv)
    {
        switch(mvTensorParam->weights->storageOrder){
            case orderZYX:
                weightsStride = mvTensorParam->weights->dimYStride;
                break;
            case orderYZX:
                weightsStride = mvTensorParam->weights->dimZStride;
                break;
            case orderYXZ:
                weightsStride = mvTensorParam->weights->dimXStride;
                break;
            case orderXYZ:
                weightsStride = mvTensorParam->weights->dimYStride;
                if (mvTensorParam->output->storageOrder == orderYXZ)
                    mvTensorAssert(mvTensorParam->weights->dimZ == mvTensorParam->output->dimZ,
                                   "weights_width != output_width");
                break;
            case orderXZY:
                weightsStride = mvTensorParam->weights->dimZStride;
                break;
            default:
                weightsStride = mvTensorParam->weights->dimYStride;
                break;
        }
    }
    else
      weightsStride = 0;

    switch(mvTensorParam->output->storageOrder){
        case orderZYX:
            outputStride = mvTensorParam->output->dimYStride;
            break;
        case orderYZX:
            outputStride = mvTensorParam->output->dimZStride;
            break;
        case orderYXZ:
            outputStride = mvTensorParam->output->dimXStride;
            break;
        default:
            outputStride = mvTensorParam->output->dimXStride;
            break;
    }

    u32 outputBpp;
    switch(mvTensorParam->output->dataType)
    {
        case t_fp16:
            outputBpp = 2;
            break;
        case t_u8f:
            outputBpp = 1;
            break;
        case t_int:
            outputBpp = 4;
            break;
        default:
            outputBpp = 2;
    }

    u8 conditionAccomplishedToUseIm2Col = 0;
    if (mvTensorParam->op->type == kConv &&
        ((g_optimisations & OPT_IM2COL) || (g_optimisations & OPT_IM2COL_V2)))
    {
        // The 1x1 convolution works for number of filters >= 8.
        if(mvTensorParam->weights->dimZ >= 8
#ifdef FATHOMRUN
        && (mvTensorParam->op->optMask != MV_TENSOR_DEFAULT_OPT || mvTensorParam->input->dimZ < 200)
#endif
        )
        {
            conditionAccomplishedToUseIm2Col = 1;
        }
        MVT_DPRINTF("conditionAccomplishedToUseIm2Col? %d\n", conditionAccomplishedToUseIm2Col);
    }


    if (mvTensorParam->op->type == kConv || mvTensorParam->op->type == kDeconvolution) {

        mvTensorAssert(mvTensorParam->matmulResources->cache_memory_ptr != NULL,"Cache memory pointer is null");

        matmul::MatMulCache& cache = matmul::MatMulCache::instance();
        cache.config(mvTensorParam->matmulResources->cache_memory_ptr, mvTensorParam->matmulResources->cache_memory_size);

        matmul::MatMulConfig cfg;
        cfg.matrix_type = static_cast<matmul::MatMulType> (matmul::MMT_HALF);
        cfg.kernel_type = static_cast<matmul::kernel_t> (matmul::GEMM_HHHH_NNN);
        cfg.kernel_width = 8;
        cfg.dma_link_agent = mvTensorParam->myriadResources->dmaLinkAgent;
        cfg.first_shave = mvTensorParam->myriadResources->firstShave;
        cfg.scratch_memory_size = mvTensorParam->matmulResources->scratch_memory_size;
        cfg.scratch_memory = mvTensorParam->matmulResources->scratch_memory_ptr;
        cfg.error_buffer = dbgBuf;
        matmul::MvMatMul<half> matmul(cfg, cache);

        if(g_enableMatmulDebugTrace)
            matmul.enable_trace();

     const s32 SLICE_MAX = 512;  // split over input and output channels


      if (((mvTensorParam->op->type == kConv) &&
             ((mvTensorParam->op->radixX == 3 && mvTensorParam->op->radixY == 3) ||
            (mvTensorParam->op->radixX == 5 && mvTensorParam->op->radixY == 5) ||
            (mvTensorParam->op->radixX == 7 && mvTensorParam->op->radixY == 7) ||
            (mvTensorParam->op->radixX == 9 && mvTensorParam->op->radixY == 9)) &&
            ((mvTensorParam->op->strideX == 1 && mvTensorParam->op->strideY == 1) ||
            (mvTensorParam->op->strideX == 2 && mvTensorParam->op->strideY == 2) ||
            (mvTensorParam->op->strideX == 3 && mvTensorParam->op->strideY == 3) ||
            (mvTensorParam->op->strideX == 4 && mvTensorParam->op->strideY == 4)) &&
            (mvTensorParam->input->dimZ <= 4) &&
            (useSamePadding) &&
            (g_optimisations & OPT_SPATIALCONV)) ||
            ((mvTensorParam->op->type == kDeconvolution) &&
            ((mvTensorParam->op->radixX == 3 && mvTensorParam->op->radixY == 3) ||
            (mvTensorParam->op->radixX == 5 && mvTensorParam->op->radixY == 5) ||
            (mvTensorParam->op->radixX == 7 && mvTensorParam->op->radixY == 7) ||
            (mvTensorParam->op->radixX == 9 && mvTensorParam->op->radixY == 9)) &&
            (mvTensorParam->op->strideX == 1 && mvTensorParam->op->strideY == 1) &&
            (mvTensorParam->input->dimZ < 4) &&
            (useSamePadding) &&
            (g_optimisations & OPT_SPATIAL_DECONV_SAME_S1)))
     {
         spatialconv((fp16*)mvTensorParam->input->data,
                     mvTensorParam->input->dimY,
                     mvTensorParam->input->dimX,
                     mvTensorParam->input->dimZ,
                     mvTensorParam->output->dimZ,
                     mvTensorParam->op->radixX,
                     mvTensorParam->op->radixY,
                     mvTensorParam->op->strideX,
                     mvTensorParam->op->strideY,
                     useSamePadding,
                     (fp16*)mvTensorParam->weights->data,
                     (fp16*)mvTensorParam->output->data,
                     mvTensorParam->myriadResources);
     }
     else if (((mvTensorParam->op->type == kDeconvolution) &&
             !(useSamePadding && ((g_optimisations & OPT_DECONV_SAME_3X3_S1) ||
               (g_optimisations & OPT_DECONV_SAME_5X5_S1) ||
               (g_optimisations & OPT_DECONV_GENERIC_SAME_S1)))) ||
             (mvTensorParam->op->radixX == 1 && mvTensorParam->op->radixY == 1 &&
         mvTensorParam->op->strideX == 1 && mvTensorParam->op->strideY == 1) ||
         conditionAccomplishedToUseIm2Col)
    {
        fp16 *inputPtr = (fp16*)mvTensorParam->input->data;
        u32 M, K, N;
        fp16 *transformedInput = NULL;

        if(mvTensorParam->op->type == kDeconvolution)
        {
            mvTensorAssert(mvTensorParam->weights->dimZ >= 8);
            s32 transformed_width    = mvTensorParam->output->dimX;
            s32 transformed_height   = mvTensorParam->output->dimY;
            s32 transformed_channels = mvTensorParam->op->radixX * mvTensorParam->op->radixY *
                    mvTensorParam->input->dimZ;

            s32 transformed_size = transformed_width * transformed_height * transformed_channels;

            transformedInput = (fp16*)malloc(sizeof(half) * transformed_size + ALIGN_VALUE);

            fp16* transformedInputAligned = ALIGN_UP(transformedInput, ALIGN_VALUE);
            mvTensorAssert(transformedInputAligned != NULL, "Cannot Allocate space for 3x3s1 im2col output buffer.");

            relayout((fp16*)mvTensorParam->input->data, transformedInputAligned,
                     mvTensorParam->input->dimX, mvTensorParam->input->dimY, mvTensorParam->input->dimZ,
                     mvTensorParam->op->radixX, mvTensorParam->op->radixY,
                     mvTensorParam->op->strideX, mvTensorParam->op->strideY,
                     mvTensorParam->op->paddStyle,
                     mvTensorParam->op->padX,
                     mvTensorParam->op->padY,
                     DECONVOLUTION,
                     mvTensorParam->myriadResources);

            M = mvTensorParam->output->dimX * mvTensorParam->output->dimY;
            K = mvTensorParam->input->dimZ * mvTensorParam->op->radixX * mvTensorParam->op->radixY;
            N = mvTensorParam->weights->dimZ;
            inputStride = K * sizeof(half);

            inputPtr = transformedInputAligned;
        }
        else if(conditionAccomplishedToUseIm2Col)
        {
            u32 inputElements = mvTensorParam->output->dimX * mvTensorParam->output->dimY * mvTensorParam->input->dimZ;
            transformedInput = (fp16*)malloc(sizeof(half) * mvTensorParam->op->radixX * mvTensorParam->op->radixY * inputElements + ALIGN_VALUE);
            fp16* transformedInputAligned = ALIGN_UP(transformedInput, ALIGN_VALUE);
            mvTensorAssert(transformedInputAligned != NULL, "Cannot Allocate space for 3x3s1 im2col output buffer.");

            t_MvRelayoutVersion relVers = CONVOLUTION_V0;
            if (g_optimisations & OPT_IM2COL_V2)
                relVers = CONVOLUTION_V1;
            relayout((fp16*)mvTensorParam->input->data, transformedInputAligned,
                     mvTensorParam->input->dimX, mvTensorParam->input->dimY, mvTensorParam->input->dimZ,
                     mvTensorParam->op->radixX, mvTensorParam->op->radixY,
                     mvTensorParam->op->strideX, mvTensorParam->op->strideY,
                     useSamePadding == 1 ? paddStyleTFSame : paddStyleTFValid,
                     mvTensorParam->op->padX,
                     mvTensorParam->op->padY,
                     relVers,
                     mvTensorParam->myriadResources);

            M = mvTensorParam->output->dimX * mvTensorParam->output->dimY;
            K = mvTensorParam->input->dimZ * mvTensorParam->op->radixX * mvTensorParam->op->radixY;
            N = mvTensorParam->weights->dimZ;
            inputStride = K * sizeof(half);

            inputPtr = transformedInputAligned;
        }
        else
        {
            M = mvTensorParam->input->dimY * mvTensorParam->input->dimX;        // M - inChannelW * inChannelH
            K = mvTensorParam->input->dimZ;                                     // K - input channelsNo
            N = mvTensorParam->weights->dimZ;                                   // N - output MapsNo
        }

//        mvTensorAssert(M >= 8, "1x1 Convolution W*H Dimension under minimum size.");
//        mvTensorAssert(N >= 8, "1x1 Convolution outputChannels Dimension under minimum size.");

        MVT_DPRINTF("MATMUL: M K N: %li %li %li Shv %li iStride %li wStride %li oStride %li\n", (long int)M, (long int)K, (long int)N, (long int)shaveNo, (long int)inputStride, (long int)weightsStride, (long int)outputStride);

        matmul::MatMulBuffer<half> bufA((half*)inputPtr, M * inputStride);
        matmul::MatMulBuffer<half> bufB((half*)mvTensorParam->weights->data, K * weightsStride);
        matmul::Tensor A(&bufA, matmul::MMT_HALF, M, K, inputStride);
        matmul::Tensor B(&bufB, matmul::MMT_HALF, K, N, weightsStride);
        if (K < SLICE_MAX && N < SLICE_MAX)
        {
            matmul::MatMulBuffer<half> bufC((half*)mvTensorParam->output->data, M * outputStride);
            matmul::Tensor C(&bufC, matmul::MMT_HALF, M, N, outputStride);
            matmul.multiply(A, B, C, shaveNo, matmul::GEMM_HHHH_NNN_NAC);
        }
        else
        {
            u32 chunks_k, chunks_n;
            s32 remainder_k, remainder_n;
            u32 i, j, n_i, k_j, offset_k, offset_n = 0;
            chunks_k = (K + SLICE_MAX - 1) / SLICE_MAX;
            chunks_n = (N + SLICE_MAX - 1) / SLICE_MAX;
            remainder_n = N - N/chunks_n * chunks_n;
            for (i = 0; i < chunks_n; i++, remainder_n--) {
                n_i = remainder_n > 0? (N/chunks_n + 1): N/chunks_n;
                remainder_k = K - K/chunks_k * chunks_k;
                offset_k = 0;
                for (j = 0; j < chunks_k; j++, remainder_k--) {
                    k_j = remainder_k > 0? (K/chunks_k + 1): K/chunks_k;
                    // DrvLL2CFlushOpOnAllLines(LL2C_OPERATION_INVALIDATE, 0);
                    mvTensorAssert(k_j < K_MAX, "Common Dimension is too large.");
                    matmul.multiply(
                                    M, k_j, n_i,
                                    shaveNo,
                                    (half*) inputPtr + offset_k,
                                    (half*) mvTensorParam->weights->data + offset_k * N + offset_n,
                                    (half*) mvTensorParam->output->data + offset_n,
                                    inputStride,
                                    weightsStride,
                                    outputStride,
                                    j == 0 ? matmul::GEMM_HHHH_NNN_NAC : matmul::GEMM_HHHH_NNN);
                    offset_k += k_j;
                }
                offset_n += n_i;
            }
        }

        if(conditionAccomplishedToUseIm2Col || mvTensorParam->op->type == kDeconvolution)
            free(transformedInput);
    }

    else if(mvTensorParam->op->radixX == 3 && mvTensorParam->op->radixY == 3 &&
            mvTensorParam->op->strideX == 1 && mvTensorParam->op->strideY == 1 &&
            ((g_optimisations & OPT_CONV3X3_S1) || (g_optimisations & OPT_DECONV_SAME_3X3_S1)))
    {
        if(mvTensorParam->op->type == kDeconvolution)
        {
            MVT_DPRINTF("3x3s1 Deconv\n");
        }
        else
        {
            MVT_DPRINTF("3x3s1 Conv\n");
        }
        // M - inChannelW * inChannelH
        // K - input channelsNo
        // N - output MapsNo
        u32 W_in = mvTensorParam->input->dimX;
        u32 H_in = mvTensorParam->input->dimY;
        u32 W_out = mvTensorParam->output->dimX;
        u32 H_out = mvTensorParam->output->dimY;
        u32 M = W_in * H_in;
        u32 K = mvTensorParam->input->dimZ;
        u32 N = mvTensorParam->weights->dimZ;

        mvTensorAssert(K < K_MAX, "3x3 Convolution inputChannels Dimension too large.");
        mvTensorAssert(M >= 8, "3x3 Convolution Width*Height Dimension too small.");
        mvTensorAssert(N >= 8, "3x3 Convolution outputChannels Dimension too small.");


        unsigned int i = 0;
        unsigned int j = 0;
        unsigned int relative_i, relative_j;
        unsigned int convSize = 3;

        if (!useSamePadding) {
            // VALID padding

            dmaTransactionList_t *ref1;
            dmaRequesterId id1;

            for (i = 0; i < convSize; i++) {
                relative_i = i - (convSize/2);
                for (j = 0; j < convSize; j++) {
                    relative_j = j - (convSize/2);
                    matmul.multiply(
                                    H_out*W_out + (W_in-W_out)*(H_out-1), K, N,
                                    shaveNo,
                                    (half*) mvTensorParam->input->data + ((i*W_in) + j) * K,
                                    (half*) mvTensorParam->weights->data + ((i*convSize)+j)* K * N,
                                    (half*) mvTensorParam->output->data,
                                    inputStride,
                                    weightsStride,
                                    outputStride,
                                    i == 0 && j == 0 ? matmul::GEMM_HHHH_NNN_NAC : matmul::GEMM_HHHH_NNN);
                }
            }
#if defined(__RTEMS__)
            sc = OsDrvCmxDmaInitRequester(MV_TENSOR_DMA_PRIORITY, &dmaReqId);
            mvTensorAssert(sc == RTEMS_SUCCESSFUL, "DMA init requester failed");

            sc = OsDrvCmxDmaCreateTransactionFullOptions(dmaReqId, task1,
                                                         (u8*)mvTensorParam->output->data + W_in * N * sizeof(fp16),  //src
                                                         (u8*)mvTensorParam->output->data + W_out * N * sizeof(fp16), //dst
                                                         W_out * (H_out - 1) * N * sizeof(fp16),                      // byte length
                                                         W_out * N * sizeof(fp16),                                    // src width
                                                         W_out * N * sizeof(fp16),                                    // dst width
                                                         W_in * N * sizeof(fp16),                                     // src stride
                                                         W_out * N * sizeof(fp16));                                   // dst stride
            mvTensorAssert(sc == RTEMS_SUCCESSFUL, "DMA create transaction failed");
            sc = OsDrvCmxDmaStartListTask(task1, &sc);
            mvTensorAssert(sc == RTEMS_SUCCESSFUL, "DMA start link task failed");
            sc = OsDrvCmxDmaWaitTask(task1);
            mvTensorAssert(sc == RTEMS_SUCCESSFUL, "DMA wait failed");
#else
            id1 = DrvCmxDmaInitRequesterOnAgent(1, mvTensorParam->myriadResources->dmaLinkAgent);
            ref1 = DrvCmxDmaCreateTransactionFullOptions(id1, task1,
                 (u8*)mvTensorParam->output->data + W_in * N * sizeof(fp16), //src
                 (u8*)mvTensorParam->output->data + W_out * N * sizeof(fp16), //dst
                 W_out * (H_out - 1) * N * sizeof(fp16), // byte length
                 W_out * N * sizeof(fp16),       // src width
                 W_out * N * sizeof(fp16),       // dst width
                 W_in * N * sizeof(fp16),      // src stride
                 W_out * N * sizeof(fp16));    // dst stride
            DrvCmxDmaStartListTask(ref1);
            DrvCmxDmaWaitTask(ref1);
#endif
        }

        else{
            // SAME padding
            // NOTE: when output offsets are negative, it writes before the output->data pointer
            // and for positive offsets, it writes after the output->data pointer
            //                    +----------+
            //                    |          |
            //                    |   pad    |
            // output->data +---> +----------+
            //                    |          |
            //                    |  buffer  |
            //                    |          |
            //                    +----------+
            //                    |          |
            //                    |   pad    |
            //                    +----------+

            matmul::MatMulBuffer<half> bufA((half*)mvTensorParam->input->data, M * inputStride);
            matmul::MatMulBuffer<half> bufB((half*)mvTensorParam->weights->data, K * weightsStride);
            matmul::MatMulBuffer<half> bufC((half*)mvTensorParam->output->data, M * outputStride);
            matmul::Tensor A(&bufA, matmul::MMT_HALF, M, K, inputStride);
            matmul::Tensor B(&bufB, matmul::MMT_HALF, K, N, weightsStride);
            matmul::Tensor C(&bufC, matmul::MMT_HALF, M, N, outputStride);

            // left
            for (i = 0; i < W_in; i++)
                for (j = 0; j < N; j++)
                    *((half*) mvTensorParam->output->data + i*outputStride/outputBpp + j) = 0;
            matmul::TensorList leftOptions;
            for (i = 0; i != convSize; i++) {
                relative_i = i - (convSize/2);
                for (j = 0; j < convSize/2; j++) {
                    relative_j = j - (convSize/2);
                    matmul::MatMulOptions opt(0,
                        ((i * convSize) + j) * K * weightsStride,
                        ((-relative_i * W_in) - relative_j) * outputStride,
                        i == 0 && j == 0 ? matmul::GEMM_HHHH_NNN_NAC : matmul::GEMM_HHHH_NNN);
                    leftOptions.add(opt);
                }
            }
            matmul.multiply(A, B, C, shaveNo, leftOptions);

            if(g_enableMatmulDebugTrace)
            {
                matmul.tracer().log_short();
                matmul.tracer().log_extended();
            }

            for (i = 1; i <= H_in; i++)
                for (j = 0; j < N; j++)
                    *((half*) mvTensorParam->output->data + (i-1)*W_in*outputStride/outputBpp + j) = 0;

            // center
            matmul::TensorList centerOffsets;
            for (i = 0; i != convSize; i++) {
                relative_i = i - (convSize/2);
                j = convSize/2;
                relative_j = j - (convSize/2);
                matmul::MatMulOptions opt(0,
                    ((i*convSize)+j)* K * weightsStride,
                    ((-relative_i * W_in) - relative_j) * outputStride);
                centerOffsets.add(opt);
            }
            matmul.multiply(A, B, C, shaveNo, centerOffsets);

            for (i = 1; i <= H_in; i++)
                for (j = 0; j < K; j++)
                    *((half*) mvTensorParam->input->data + (i-1)*W_in*K + j) = 0;

            // right
            matmul::TensorList rightOffsets;
            for (i = 0; i != convSize; i++) {
                relative_i = i - (convSize/2);
                for (j = convSize/2 + 1; j < convSize; j++) {
                    relative_j = j - (convSize/2);
                    matmul::MatMulOptions opt(0,
                         ((i*convSize)+j)* K * weightsStride,
                         ((-relative_i * W_in) - relative_j) * outputStride);
                    rightOffsets.add(opt);
                }
            }
            matmul.multiply(A, B, C, shaveNo, rightOffsets);

        }

        MVT_DPRINTF("out_H %d\n", mvTensorParam->output->dimX);
        MVT_DPRINTF("out_W %d\n", mvTensorParam->output->dimY);
        }

    else if(mvTensorParam->op->radixX == 3 && mvTensorParam->op->radixY == 3 &&
            mvTensorParam->op->strideX == 2 && mvTensorParam->op->strideY == 2 && (g_optimisations & OPT_CONV3X3_S2))
    {
        MVT_DPRINTF("3x3s2 Conv\n");

        // M - inChannelW * inChannelH
        // K - input channelsNo
        // N - output MapsNo
        u32 M = mvTensorParam->input->dimY * mvTensorParam->input->dimX;
        u32 K = mvTensorParam->input->dimZ;
        u32 N = mvTensorParam->output->dimZ;

        mvTensorAssert(K < K_MAX, "3x3s2 Convolution inputChannels Dimension too large.");
        mvTensorAssert(M >= 8, "3x3s2 Convolution Width*Height Dimension too large.");
        mvTensorAssert(N >= 8, "3x3s2 Convolution outputChannels Dimension too small.");

        u32 W = mvTensorParam->input->dimX;
        u32 H = mvTensorParam->input->dimY;
        const u32 kMatmulCalls = 9;
        const u32 kMatmulOrderOfCalls[kMatmulCalls] =   {1, 2, 4, 5, 7, 8, 6, 0, 3};
        const u32 kBuffersOrder[kMatmulCalls] =         {2, 3, 0, 1, 2, 3, 3, 3, 1};
        u32 output_offsets[kMatmulCalls];
        unsigned short** buffers;
        u32 i, j, k;
        buffers = GetBuffersForStride2((unsigned short*)mvTensorParam->input->data, H, W, K);
        output_offsets[0] = (W/2 + 1) * N;
        output_offsets[1] = (W/2) * N;
        output_offsets[2] = (W/2) * N;
        output_offsets[3] = (1) * N;
        output_offsets[4] = (0) * N;
        output_offsets[5] = (0) * N;
        output_offsets[6] = (1) * N;
        output_offsets[7] = (0) * N;
        output_offsets[8] = (0) * N;

        matmul::MatMulBuffer<half> bufA(0, M/4 * K * sizeof(half));
        matmul::MatMulBuffer<half> bufB(0, K * N * sizeof(half));
        matmul::MatMulBuffer<half> bufC(0, M * N * sizeof(half));
        matmul::Tensor A(&bufA, matmul::MMT_HALF, M/4, K, inputStride);
        matmul::Tensor B(&bufB, matmul::MMT_HALF, K, N, weightsStride);
        matmul::Tensor C(&bufC, matmul::MMT_HALF, M, N, outputStride);

        for (i = 0; i < kMatmulCalls; i++)
        {
            if (kMatmulOrderOfCalls[i] == 6 || kMatmulOrderOfCalls[i] == 3)
            {
                // A_oo* and A_eo*
                for (j = 1; j <= H/2; j++)
                    for (k = 0; k < K; k++)
                        buffers[kBuffersOrder[i]][(W/2-1)*K+(j-1)*(W/2)*K+k] = 0;
            }

            //TODO: can be implemented with new matmul single call interface
            //(with offsets)
            A.set_data((half*)buffers[kBuffersOrder[i]]);
            B.set_data((half*)mvTensorParam->weights->data + kMatmulOrderOfCalls[i] * K * N);
            C.set_data((half*)mvTensorParam->output->data + output_offsets[kMatmulOrderOfCalls[i]]);
            matmul.multiply(A, B, C, shaveNo);

       }

        for(i = 0; i < 4; i++)
            free(buffers[i]);
        free(buffers);

        MVT_DPRINTF("out_H %d\n", mvTensorParam->output->dimX);
        MVT_DPRINTF("out_W %d\n", mvTensorParam->output->dimY);
    }


    else if(mvTensorParam->op->radixX == 5 && mvTensorParam->op->radixY == 5 &&
            mvTensorParam->op->strideX == 1 && mvTensorParam->op->strideY == 1 &&
            ((g_optimisations & OPT_CONV5X5_S1) || (g_optimisations & OPT_DECONV_SAME_5X5_S1)))
    {
        if(mvTensorParam->op->type == kDeconvolution)
        {
            MVT_DPRINTF("5x5s1 Deconv\n");
        }
        else
        {
            MVT_DPRINTF("5x5s1 Conv\n");
        }

        // M - inChannelW * inChannelH
        // K - input channelsNo
        // N - output MapsNo
        u32 W_in = mvTensorParam->input->dimX;
        u32 H_in = mvTensorParam->input->dimY;
        u32 W_out = mvTensorParam->output->dimX;
        u32 H_out = mvTensorParam->output->dimY;
        u32 M = W_in * H_in;
        u32 K = mvTensorParam->input->dimZ;
        u32 N = mvTensorParam->weights->dimZ;

        mvTensorAssert(K < K_MAX, "5x5 Convolution inputChannels Dimension too large.");
        mvTensorAssert(M >= 8, "5x5 Convolution Width*Height Dimension too small.");
        mvTensorAssert(N >= 8, "5x5 Convolution outputChannels Dimension too small.");

        unsigned int i = 0;
        unsigned int j = 0;
        unsigned int relative_i, relative_j;
        unsigned int convSize = 5;

        if (!useSamePadding) {
            // VALID padding

            dmaTransactionList_t *ref1;
            dmaRequesterId id1;

            for (i = 0; i < convSize; i++) {
                relative_i = i - (convSize/2);
                for (j = 0; j < convSize; j++) {
                    relative_j = j - (convSize/2);
                    matmul.multiply(
                            H_out*W_out + (W_in-W_out)*(H_out-1), K, N,
                            shaveNo,
                            (half*) mvTensorParam->input->data + ((i*W_in) + j) * K,
                            (half*) mvTensorParam->weights->data + ((i*convSize)+j)* K * N,
                            (half*) mvTensorParam->output->data,
                            inputStride,
                            weightsStride,
                            outputStride,
                            i == 0 && j == 0 ? matmul::GEMM_HHHH_NNN_NAC : matmul::GEMM_HHHH_NNN);
                }
            }
#if defined(__RTEMS__)
            sc = OsDrvCmxDmaInitRequester(MV_TENSOR_DMA_PRIORITY, &dmaReqId);
            mvTensorAssert(sc == RTEMS_SUCCESSFUL, "DMA init requester failed");

            sc = OsDrvCmxDmaCreateTransactionFullOptions(dmaReqId, task1,
                                                         (u8*)mvTensorParam->output->data + W_in * N * sizeof(fp16),  //src
                                                         (u8*)mvTensorParam->output->data + W_out * N * sizeof(fp16), //dst
                                                         W_out * (H_out - 1) * N * sizeof(fp16),                      // byte length
                                                         W_out * N * sizeof(fp16),                                    // src width
                                                         W_out * N * sizeof(fp16),                                    // dst width
                                                         W_in * N * sizeof(fp16),                                     // src stride
                                                         W_out * N * sizeof(fp16));                                   // dst stride
            mvTensorAssert(sc == RTEMS_SUCCESSFUL, "DMA create transaction failed");

            sc = OsDrvCmxDmaStartListTask(task1, &sc);
            mvTensorAssert(sc == RTEMS_SUCCESSFUL, "DMA start link task failed");

            sc = OsDrvCmxDmaWaitTask(task1);
            mvTensorAssert(sc == RTEMS_SUCCESSFUL, "DMA wait failed");
#else
            id1 = DrvCmxDmaInitRequesterOnAgent(1, mvTensorParam->myriadResources->dmaLinkAgent);
            ref1 = DrvCmxDmaCreateTransactionFullOptions(id1, task1,
                (u8*)mvTensorParam->output->data + W_in * N * sizeof(fp16), //src
                (u8*)mvTensorParam->output->data + W_out * N * sizeof(fp16), //dst
                W_out * (H_out - 1) * N * sizeof(fp16), // byte length
                W_out * N * sizeof(fp16),       // src width
                W_out * N * sizeof(fp16),       // dst width
                W_in * N * sizeof(fp16),      // src stride
                W_out * N * sizeof(fp16));    // dst stride
            DrvCmxDmaStartListTask(ref1);
            DrvCmxDmaWaitTask(ref1);
#endif
        }
        else{
            // SAME padding
            matmul::MatMulBuffer<half> bufA((half*)mvTensorParam->input->data, M * inputStride);
            matmul::MatMulBuffer<half> bufB((half*)mvTensorParam->weights->data, K * weightsStride);
            matmul::MatMulBuffer<half> bufC((half*)mvTensorParam->output->data, M * outputStride);
            matmul::Tensor A(&bufA, matmul::MMT_HALF, M, K, inputStride);
            matmul::Tensor B(&bufB, matmul::MMT_HALF, K, N, weightsStride);
            matmul::Tensor C(&bufC, matmul::MMT_HALF, M, N, outputStride);

            // 2
            matmul::TensorList leftmostOffsets_2, leftOffsets_2;
            // 3
            matmul::TensorList leftmostOffsets_3, leftOffsets_3;

            for (i = 0; i < W_in; i++)
                for (j = 0; j < N; j++)
                {
                    *((half*) mvTensorParam->output->data + i*outputStride/outputBpp + j) = 0;
                    *((half*) mvTensorParam->output->data + (W_in+i)*outputStride/outputBpp + j) = 0;
                }
            // leftmost 2
            for (i = 0; i < convSize/2; i++) {
                relative_i = i - (convSize/2);
                j = 0;
                relative_j = j - (convSize/2);
                matmul::MatMulOptions opt(0,
                    ((i*convSize)+j)* K * weightsStride,
                    ((-relative_i * W_in) - relative_j) * outputStride,
                    i == 0 ? matmul::GEMM_HHHH_NNN_NAC : matmul::GEMM_HHHH_NNN);
                leftmostOffsets_2.add(opt);
                }
                matmul.multiply(A, B, C, shaveNo, leftmostOffsets_2);

            // leftmost 3
            for (i = convSize/2; i < convSize ; i++) {
                relative_i = i - (convSize/2);
                j = 0;
                relative_j = j - (convSize/2);
                matmul::MatMulOptions opt(0,
                    ((i*convSize)+j)* K * weightsStride,
                    ((-relative_i * W_in) - relative_j) * outputStride);
                leftmostOffsets_3.add(opt);
                }
                matmul.multiply(A, B, C, shaveNo, leftmostOffsets_3);

                for (i = 1; i <= H_in; i++)
                    for (j = 0; j < N; j++)
                      *((half*) mvTensorParam->output->data + ((i-1)*W_in + 1)*outputStride/outputBpp + j) = 0;

            // left 2
            for (i = 0; i < convSize/2; i++) {
                relative_i = i - (convSize/2);
                j = 1;
                relative_j = j - (convSize/2);
                matmul::MatMulOptions opt(0,
                    ((i*convSize)+j)* K * weightsStride,
                    ((-relative_i * W_in) - relative_j) * outputStride);
                leftOffsets_2.add(opt);
                }
                matmul.multiply(A, B, C, shaveNo, leftOffsets_2);

            // left 3
            for (i = convSize/2; i < convSize; i++) {
                relative_i = i - (convSize/2);
                j = 1;
                relative_j = j - (convSize/2);
                matmul::MatMulOptions opt(0,
                    ((i*convSize)+j)* K * weightsStride,
                    ((-relative_i * W_in) - relative_j) * outputStride);
                leftOffsets_3.add(opt);
                }
                matmul.multiply(A, B, C, shaveNo, leftOffsets_3);

                for (i = 1; i <= H_in; i++)
                    for (j = 0; j < N; j++)
                      *((half*) mvTensorParam->output->data + (i-1)*W_in*outputStride/outputBpp + j) = 0;

            // center 2
            matmul::TensorList centerOffsets_2;
            matmul::TensorList centerOffsets_3;

            for (i = 0; i < convSize/2; i++) {
                relative_i = i - (convSize/2);
                j = convSize/2;
                relative_j = j - (convSize/2);
                    matmul::MatMulOptions opt(0,
                        ((i*convSize)+j)* K * weightsStride,
                        ((-relative_i * W_in) - relative_j) * outputStride);
                    centerOffsets_2.add(opt);
            }
            matmul.multiply(A, B, C, shaveNo, centerOffsets_2);

            // center 3
            for (i = convSize/2; i < convSize; i++) {
                relative_i = i - (convSize/2);
                j = convSize/2;
                relative_j = j - (convSize/2);
                    matmul::MatMulOptions opt(0,
                        ((i*convSize)+j)* K * weightsStride,
                        ((-relative_i * W_in) - relative_j) * outputStride);
                    centerOffsets_3.add(opt);
            }
            matmul.multiply(A, B, C, shaveNo, centerOffsets_3);

            matmul::TensorList rightOffsets_2, rightmostOffsets_2;
            matmul::TensorList rightOffsets_3, rightmostOffsets_3;
            // right
            for (i = 1; i <= H_in; i++)
                for (j = 0; j < K; j++)
                    *((half*) mvTensorParam->input->data + (i-1)*W_in*K + j) = 0;

            // right 2
            for (i = 0; i < convSize/2; i++) {
                relative_i = i - (convSize/2);
                j = convSize/2 + 1;
                relative_j = j - (convSize/2);
                matmul::MatMulOptions opt(0,
                    ((i*convSize)+j)* K * weightsStride,
                    ((-relative_i * W_in) - relative_j) * outputStride);
                rightOffsets_2.add(opt);
            }
            matmul.multiply(A, B, C, shaveNo, rightOffsets_2);

            // right 3
            for (i = convSize/2; i < convSize; i++) {
                relative_i = i - (convSize/2);
                j = convSize/2 + 1;
                relative_j = j - (convSize/2);
                matmul::MatMulOptions opt(0,
                    ((i*convSize)+j)* K * weightsStride,
                    ((-relative_i * W_in) - relative_j) * outputStride);
                rightOffsets_3.add(opt);
            }
            matmul.multiply(A, B, C, shaveNo, rightOffsets_3);

            // rightmost
            for (i = 1; i <= H_in; i++)
                for (j = 0; j < K; j++)
                    *((half*) mvTensorParam->input->data + ((i-1)*W_in + 1)*K + j) = 0;

            // rightmost 2
            for (i = 0; i < convSize/2; i++) {
                relative_i = i - (convSize/2);
                j = convSize/2 + 2;
                relative_j = j - (convSize/2);
                matmul::MatMulOptions opt(0,
                    ((i*convSize)+j)* K * weightsStride,
                    ((-relative_i * W_in) - relative_j) * outputStride);
                rightmostOffsets_2.add(opt);
            }
            matmul.multiply(A, B, C, shaveNo, rightmostOffsets_2);

            // rightmost 3
            for (i = convSize/2; i < convSize; i++) {
                relative_i = i - (convSize/2);
                j = convSize/2 + 2;
                relative_j = j - (convSize/2);
                matmul::MatMulOptions opt(0,
                    ((i*convSize)+j)* K * weightsStride,
                    ((-relative_i * W_in) - relative_j) * outputStride);
                rightmostOffsets_3.add(opt);
            }
            matmul.multiply(A, B, C, shaveNo, rightmostOffsets_3);
        }
    }

    else if (mvTensorParam->op->radixX == 5 && mvTensorParam->op->radixY == 5 &&
             mvTensorParam->op->strideX == 2 && mvTensorParam->op->strideY == 2 &&
             useSamePadding && (g_optimisations & OPT_CONV5X5_S2))
    {
        // SAME Padding
        MVT_DPRINTF("5x5s2 Conv\n");

        u32 W_in = mvTensorParam->input->dimX;
        u32 H_in = mvTensorParam->input->dimY;
        u32 M = W_in * H_in;
        u32 K = mvTensorParam->input->dimZ;
        u32 N = mvTensorParam->weights->dimZ;


        mvTensorAssert(K < K_MAX, "5x5s2 Convolution inputChannels Dimension too large.");
        mvTensorAssert(M >= 8, "5x5s2 Convolution Width*Height Dimension too small.");
        mvTensorAssert(N >= 8, "5x5s2 Convolution outputChannels Dimension too small.");

        unsigned int i = 0;
        unsigned int j = 0;
        int relative_i, relative_j;
        unsigned int convSize = 5;
        unsigned short** buffers;

        buffers = GetBuffersForStride2((unsigned short*)mvTensorParam->input->data, H_in, W_in, K);

        for (i = 0; i <= W_in/2; i++)
            for (j = 0; j < N; j++)
                *((half*) mvTensorParam->output->data + i*outputStride/outputBpp + j) = 0;
        // SAME padding
        // left
        u32 k;
        s32 output_offset;
        for (j = 0; j < convSize/2; j++) {
            relative_j = j - (convSize/2);
            for (i = 0; i != convSize; i++) {
                relative_i = i - (convSize/2);
                output_offset = 0;
                if (relative_j < 0)
                    output_offset += 1;
                if (relative_j > 1)
                    output_offset -= 1;
                if (relative_i < 0)
                    output_offset += W_in/2;
                if (relative_i > 1)
                    output_offset -= W_in/2;
                matmul.multiply(
                            M/4, K, N,
                            shaveNo,
                            (half*) buffers[(i*2+j)%4],
                            (half*) mvTensorParam->weights->data + ((i*convSize)+j)* K * N ,
                            (half*) ((unsigned char*)mvTensorParam->output->data + outputStride * output_offset),
                            inputStride,
                            weightsStride,
                            outputStride,
                            i == 0 && j == 0 ? matmul::GEMM_HHHH_NNN_NAC : matmul::GEMM_HHHH_NNN);
            }
        }

        for (i = 0; i <= (H_in)/2; i++)
            for (j = 0; j < N; j++)
                *((half*) mvTensorParam->output->data + i*(W_in/2)*outputStride/outputBpp + j) = 0;

        // center
        for (j = convSize/2; j < convSize/2 + 2; j++) {
            relative_j = j - (convSize/2);
            for (i = 0; i != convSize; i++) {
                relative_i = i - (convSize/2);
                output_offset = 0;
                if (relative_j < 0)
                    output_offset += 1;
                if (relative_j > 1)
                    output_offset -= 1;
                if (relative_i < 0)
                    output_offset += W_in/2;
                if (relative_i > 1)
                    output_offset -= W_in/2;
                matmul.multiply(
                                M/4, K, N,
                                shaveNo,
                                (half*) buffers[(i*2+ j - convSize/2)%4],
                                (half*) mvTensorParam->weights->data + ((i*convSize)+j)* K * N ,
                                (half*) ((unsigned char*)mvTensorParam->output->data + outputStride * output_offset),
                                inputStride,
                                weightsStride,
                                outputStride
                                );

            }
        }

        for (k = 0; k < 4; k++)
            for (i = 0; i < H_in/2; i++)
                for (j = 0; j < K; j++)
                    buffers[k][i*(W_in/2)*K +j] = 0;

        // right
        for (j = convSize/2 + 2; j < convSize; j++) {
            relative_j = j - (convSize/2);
            for (i = 0; i != convSize; i++) {
                relative_i = i - (convSize/2);
                output_offset = 0;
                if (relative_j < 0)
                    output_offset += 1;
                if (relative_j > 1)
                    output_offset -= 1;
                if (relative_i < 0)
                    output_offset += W_in/2;
                if (relative_i > 1)
                    output_offset -= W_in/2;
                matmul.multiply(
                                M/4, K, N,
                                shaveNo,
                                (half*) buffers[(i*2+j - convSize/2 - 2)%4],
                                (half*) mvTensorParam->weights->data + ((i*convSize)+j)* K * N ,
                                (half*) ((unsigned char*)mvTensorParam->output->data + outputStride * output_offset),
                                inputStride,
                                weightsStride,
                                outputStride
                                );

             }
        }
        for(i = 0; i < 4; i++)
            free(buffers[i]);
        free(buffers);
     }

    else if (mvTensorParam->op->radixX == 7 && mvTensorParam->op->radixY == 7 &&
             mvTensorParam->op->strideX == 2 && mvTensorParam->op->strideY == 2 &&
             mvTensorParam->input->dimX == 224 && mvTensorParam->input->dimY == 224 &&
             mvTensorParam->input->dimZ == 3 && mvTensorParam->output->dimZ == 64 &&
             (g_optimisations & OPT_SPATIALCONV7X7_S2))
    {
        conv7x7s2((fp16*)mvTensorParam->input->data,
                  mvTensorParam->input->dimY,
                  mvTensorParam->input->dimX,
                  mvTensorParam->input->dimZ,
                  mvTensorParam->output->dimZ,
                  (fp16*)mvTensorParam->weights->data,
                  (fp16*)mvTensorParam->output->data,
                  mvTensorParam->myriadResources);
    }

    else if (mvTensorParam->op->radixX == 7 && mvTensorParam->op->radixY == 7 &&
            mvTensorParam->op->strideX == 2 && mvTensorParam->op->strideY == 2 &&
            mvTensorParam->input->dimX != 223 && // Temporary hack
            useSamePadding && (g_optimisations & OPT_CONV7X7_S2))

    {
        MVT_DPRINTF("7x7s2 Conv\n");

        // SAME Padding
        // M - inChannelW * inChannelH
        // K - input channelsNo
        // N - output MapsNo
        u32 M = mvTensorParam->input->dimY * mvTensorParam->input->dimX;
        u32 K = mvTensorParam->input->dimZ;
        u32 N = mvTensorParam->output->dimZ;

        u32 W = mvTensorParam->input->dimX;
        u32 H = mvTensorParam->input->dimY;
        u32 hW = W/2;

        mvTensorAssert(K < K_MAX, "7x7s2 Convolution inputChannels Dimension too large.");
        mvTensorAssert(M >= 8, "7x7s2 Convolution Width*Height Dimension too small.");
        mvTensorAssert(N >= 8, "7x7s2 Convolution outputChannels Dimension too small.");

        const u32 kMatmulCalls = 49;
        s32 offsets[kMatmulCalls];  // output offsets
        u32 initial_offset = 0;  // relative to the 1st ouput position
        offsets[0] = initial_offset + (2*hW + 2);
        offsets[1] = offsets[2] = initial_offset + (2*hW + 1);
        offsets[3] = offsets[4] = initial_offset + (2*hW);
        offsets[5] = offsets[6] = initial_offset + (2*hW - 1);
        offsets[7] = initial_offset + (hW + 2);
        offsets[8] = offsets[9] = initial_offset + (hW + 1);
        offsets[10] = offsets[11] = initial_offset + (hW);
        offsets[12] = offsets[13] = initial_offset + (hW - 1);
        offsets[14] = initial_offset + (hW + 2);
        offsets[15] = offsets[16] = initial_offset + (hW + 1);
        offsets[17] = offsets[18] = initial_offset + (hW);
        offsets[19] = offsets[20] = initial_offset + (hW - 1);
        offsets[21] = initial_offset + (2);
        offsets[22] = offsets[23] = initial_offset + (1);
        offsets[24] = offsets[25] = initial_offset + (0);
        offsets[26] = offsets[27] = initial_offset + (- 1);
        offsets[28] = initial_offset + (2);
        offsets[29] = offsets[30] = initial_offset + (1);
        offsets[31] = offsets[32] = initial_offset + (0);
        offsets[33] = offsets[34] = initial_offset + (- 1);
        offsets[35] = initial_offset + (2 - hW);
        offsets[36] = offsets[37] = initial_offset + (1 - hW);
        offsets[38] = offsets[39] = initial_offset + (-hW);
        offsets[40] = offsets[41] = initial_offset + (-1 - hW);
        offsets[42] = initial_offset + (2 - hW);
        offsets[43] = offsets[44] = initial_offset + (1 - hW);
        offsets[45] = offsets[46] = initial_offset + (-hW);
        offsets[47] = offsets[48] = initial_offset + (-1 - hW);

        u32 i, j, k;
        unsigned short** buffers;

        buffers = GetBuffersForStride2((unsigned short*)mvTensorParam->input->data, H, W, K);

        if (K < 8) {
            u32 input_offsets[kMatmulCalls];
            u32 output_offsets[kMatmulCalls];
            unsigned short* hepta_buf, *tetrakaideca_buf, *weights_hepta_buf, *weights_tetrakaideca_buf;;

            dmaTransactionList_t *ref1;
            dmaRequesterId id1;

            for (i = 0; i < kMatmulCalls; i++)
                if (offsets[i] > 0) {
                    input_offsets[i] = 0;
                    output_offsets[i] = offsets[i] * K;
                } else {
                    input_offsets[i] = abs(offsets[i]) * K;
                    output_offsets[i] = 0;
                }

            hepta_buf = (unsigned short*)malloc((M/4 + offsets[0]) * 8 * K * sizeof(unsigned short));
            mvTensorAssert(hepta_buf != NULL, "Cannot Allocate space for 7x7s2 hBuffers.");
            bzero(hepta_buf, (M/4 + offsets[0]) * 8 * K * sizeof(unsigned short));
            tetrakaideca_buf = (unsigned short*)malloc((M/4 + 2) * 8 * 2 * K * sizeof(unsigned short));
            mvTensorAssert(tetrakaideca_buf != NULL, "Cannot Allocate space for 7x7s2 tBuffers.");
            // do not need to initialize this buffer, same results with either of:
            //memset(tetrakaideca_buf, 0xFFFFFFFF,  (M/4 + 2) * 8 * 2 * K * sizeof(unsigned short));
            //memset(tetrakaideca_buf, 0x00000000,  (M/4 + 2) * 8 * 2 * K * sizeof(unsigned short));
            weights_hepta_buf = (unsigned short*)malloc(8 * K * N * sizeof(unsigned short));
            mvTensorAssert(weights_hepta_buf != NULL, "Cannot Allocate space for 7x7s2 whBuffers.");
            bzero(weights_hepta_buf, 8 * K * N * sizeof(unsigned short));
            weights_tetrakaideca_buf = (unsigned short*)malloc(16 * K * N * sizeof(unsigned short));
            mvTensorAssert(weights_tetrakaideca_buf != NULL, "Cannot Allocate space for 7x7s2 wtBuffers.");
            bzero(weights_tetrakaideca_buf, 16 * K * N * sizeof(unsigned short));

            for (i = 0; i < 7;) {
#if defined(__RTEMS__)
                sc = OsDrvCmxDmaInitRequester(MV_TENSOR_DMA_PRIORITY, &dmaReqId);
                mvTensorAssert(sc == RTEMS_SUCCESSFUL, "DMA init requester failed");

                sc = OsDrvCmxDmaCreateTransactionFullOptions(dmaReqId, task1,
                                                             (u8*)buffers[2] + input_offsets[7*i] * sizeof(fp16),               //src
                                                             (u8*)hepta_buf + (i * K + output_offsets[7*i] * 8) * sizeof(fp16), //dst
                                                             (M/4 * K - input_offsets[7*i]) * sizeof(fp16),                     // byte length
                                                             K * sizeof(fp16),                                                  // src width
                                                             K * sizeof(fp16),                                                  // dst width
                                                             K * sizeof(fp16),                                                  // src stride
                                                             8 * K * sizeof(fp16));                                             // dst stride
                mvTensorAssert(sc == RTEMS_SUCCESSFUL, "DMA create transaction failed");

                sc = OsDrvCmxDmaStartListTask(task1, &sc);
                mvTensorAssert(sc == RTEMS_SUCCESSFUL, "DMA start link task failed");

                sc = OsDrvCmxDmaWaitTask(task1);
                mvTensorAssert(sc == RTEMS_SUCCESSFUL, "DMA wait failed");
#else
                id1 = DrvCmxDmaInitRequesterOnAgent(1, mvTensorParam->myriadResources->dmaLinkAgent);
                ref1 = DrvCmxDmaCreateTransactionFullOptions(id1, task1,
                    (u8*)buffers[2] + input_offsets[7*i] * sizeof(fp16), //src
                    (u8*)hepta_buf + (i * K + output_offsets[7*i] * 8) * sizeof(fp16), //dst
                    (M/4 * K - input_offsets[7*i]) * sizeof(fp16), // byte length
                    K * sizeof(fp16),       // src width
                    K * sizeof(fp16),       // dst width
                    K * sizeof(fp16),      // src stride
                    8 * K * sizeof(fp16));    // dst stride
                DrvCmxDmaStartListTask(ref1);
                DrvCmxDmaWaitTask(ref1);
#endif
                i += 1;
                if (i < 7) {
#if defined(__RTEMS__)
                    sc = OsDrvCmxDmaInitRequester(MV_TENSOR_DMA_PRIORITY, &dmaReqId);
                    mvTensorAssert(sc == RTEMS_SUCCESSFUL, "DMA init requester failed");

                    sc = OsDrvCmxDmaCreateTransactionFullOptions(dmaReqId, task1,
                                                                 (u8*)buffers[0] + input_offsets[7*i] * sizeof(fp16),               //src
                                                                 (u8*)hepta_buf + (i * K + output_offsets[7*i] * 8) * sizeof(fp16), //dst
                                                                 (M/4 * K - input_offsets[7*i]) * sizeof(fp16),                     // byte length
                                                                 K * sizeof(fp16),                                                  // src width
                                                                 K * sizeof(fp16),                                                  // dst width
                                                                 K * sizeof(fp16),                                                  // src stride
                                                                 8 * K * sizeof(fp16));                                             // dst stride
                    mvTensorAssert(sc == RTEMS_SUCCESSFUL, "DMA create transaction failed");

                    sc = OsDrvCmxDmaStartListTask(task1, &sc);
                    mvTensorAssert(sc == RTEMS_SUCCESSFUL, "DMA start link task failed");

                    sc = OsDrvCmxDmaWaitTask(task1);
                    mvTensorAssert(sc == RTEMS_SUCCESSFUL, "DMA wait failed");
#else
                    id1 = DrvCmxDmaInitRequesterOnAgent(1, mvTensorParam->myriadResources->dmaLinkAgent);
                    ref1 = DrvCmxDmaCreateTransactionFullOptions(id1, task1,
                        (u8*)buffers[0] + input_offsets[7*i] * sizeof(fp16), //src
                        (u8*)hepta_buf + (i * K + output_offsets[7*i] * 8) * sizeof(fp16), //dst
                        (M/4 * K - input_offsets[7*i]) * sizeof(fp16), // byte length
                        K * sizeof(fp16),       // src width
                        K * sizeof(fp16),       // dst width
                        K * sizeof(fp16),      // src stride
                        8 * K * sizeof(fp16));    // dst stride
                    DrvCmxDmaStartListTask(ref1);
                    DrvCmxDmaWaitTask(ref1);
#endif
                }
                i += 1;
            }

            // copy {1, 8, 15, 22, 29, 36, 43}
#if defined(__RTEMS__)
            sc = OsDrvCmxDmaInitRequester(MV_TENSOR_DMA_PRIORITY, &dmaReqId);
            mvTensorAssert(sc == RTEMS_SUCCESSFUL, "DMA init requester failed");

            sc = OsDrvCmxDmaCreateTransactionFullOptions(dmaReqId, task1,
                                                         (u8*)hepta_buf + 8 * K * sizeof(fp16), //src
                                                         (u8*)tetrakaideca_buf,                 //dst
                                                         (M/4 + 2) * 8 * K * sizeof(fp16),      // byte length
                                                         8 * K * sizeof(fp16),                  // src width
                                                         8 * K * sizeof(fp16),                  // dst width
                                                         8 * K * sizeof(fp16),                  // src stride
                                                         16 * K * sizeof(fp16));                // dst stride
            mvTensorAssert(sc == RTEMS_SUCCESSFUL, "DMA create transaction failed");

            sc = OsDrvCmxDmaStartListTask(task1, &sc);
            mvTensorAssert(sc == RTEMS_SUCCESSFUL, "DMA start link task failed");

            sc = OsDrvCmxDmaWaitTask(task1);
            mvTensorAssert(sc == RTEMS_SUCCESSFUL, "DMA wait failed");
#else
            id1 = DrvCmxDmaInitRequesterOnAgent(1, mvTensorParam->myriadResources->dmaLinkAgent);
            ref1 = DrvCmxDmaCreateTransactionFullOptions(id1, task1,
                (u8*)hepta_buf + 8 * K * sizeof(fp16), //src
                (u8*)tetrakaideca_buf, //dst
                (M/4 + 2) * 8 * K * sizeof(fp16), // byte length
                8 * K * sizeof(fp16),       // src width
                8 * K * sizeof(fp16),       // dst width
                8 * K * sizeof(fp16),      // src stride
                16 * K * sizeof(fp16));    // dst stride
            DrvCmxDmaStartListTask(ref1);
            DrvCmxDmaWaitTask(ref1);
#endif
            // create {0, 7, 14, 21, 28, 35, 42}
            for (i = 0; i < 7;) {
#if defined(__RTEMS__)
                sc = OsDrvCmxDmaInitRequester(MV_TENSOR_DMA_PRIORITY, &dmaReqId);
                mvTensorAssert(sc == RTEMS_SUCCESSFUL, "DMA init requester failed");

                sc = OsDrvCmxDmaCreateTransactionFullOptions(dmaReqId, task1,
                                                             (u8*)buffers[3] + input_offsets[7*i] * sizeof(fp16),               //src
                                                             (u8*)hepta_buf + (i * K + output_offsets[7*i] * 8) * sizeof(fp16), //dst
                                                             (M/4 * K - input_offsets[7*i]) * sizeof(fp16),                     // byte length
                                                             K * sizeof(fp16),                                                  // src width
                                                             K * sizeof(fp16),                                                  // dst width
                                                             K * sizeof(fp16),                                                  // src stride
                                                             8 * K * sizeof(fp16));                                             // dst stride
                mvTensorAssert(sc == RTEMS_SUCCESSFUL, "DMA create transaction failed");

                sc = OsDrvCmxDmaStartListTask(task1, &sc);
                mvTensorAssert(sc == RTEMS_SUCCESSFUL, "DMA start link task failed");

                sc = OsDrvCmxDmaWaitTask(task1);
                mvTensorAssert(sc == RTEMS_SUCCESSFUL, "DMA wait failed");
#else
                id1 = DrvCmxDmaInitRequesterOnAgent(1, mvTensorParam->myriadResources->dmaLinkAgent);
                ref1 = DrvCmxDmaCreateTransactionFullOptions(id1, task1,
                    (u8*)buffers[3] + input_offsets[7*i] * sizeof(fp16), //src
                    (u8*)hepta_buf + (i * K + output_offsets[7*i] * 8) * sizeof(fp16), //dst
                    (M/4 * K - input_offsets[7*i]) * sizeof(fp16), // byte length
                    K * sizeof(fp16),       // src width
                    K * sizeof(fp16),       // dst width
                    K * sizeof(fp16),      // src stride
                    8 * K * sizeof(fp16));    // dst stride
                DrvCmxDmaStartListTask(ref1);
                DrvCmxDmaWaitTask(ref1);
#endif
                i += 1;
                if (i < 7) {
#if defined(__RTEMS__)
                    sc = OsDrvCmxDmaInitRequester(MV_TENSOR_DMA_PRIORITY, &dmaReqId);
                    mvTensorAssert(sc == RTEMS_SUCCESSFUL, "DMA init requester failed");

                    sc = OsDrvCmxDmaCreateTransactionFullOptions(dmaReqId, task1,
                                                                 (u8*)buffers[1] + input_offsets[7*i] * sizeof(fp16),               //src
                                                                 (u8*)hepta_buf + (i * K + output_offsets[7*i] * 8) * sizeof(fp16), //dst
                                                                 (M/4 * K - input_offsets[7*i]) * sizeof(fp16),                     // byte length
                                                                 K * sizeof(fp16),                                                  // src width
                                                                 K * sizeof(fp16),                                                  // dst width
                                                                 K * sizeof(fp16),                                                  // src stride
                                                                 8 * K * sizeof(fp16));                                             // dst stride
                    mvTensorAssert(sc == RTEMS_SUCCESSFUL, "DMA create transaction failed");

                    sc = OsDrvCmxDmaStartListTask(task1, &sc);
                    mvTensorAssert(sc == RTEMS_SUCCESSFUL, "DMA start link task failed");

                    sc = OsDrvCmxDmaWaitTask(task1);
                    mvTensorAssert(sc == RTEMS_SUCCESSFUL, "DMA wait failed");
#else
                    id1 = DrvCmxDmaInitRequesterOnAgent(1, mvTensorParam->myriadResources->dmaLinkAgent);
                    ref1 = DrvCmxDmaCreateTransactionFullOptions(id1, task1,
                        (u8*)buffers[1] + input_offsets[7*i] * sizeof(fp16), //src
                        (u8*)hepta_buf + (i * K + output_offsets[7*i] * 8) * sizeof(fp16), //dst
                        (M/4 * K - input_offsets[7*i]) * sizeof(fp16), // byte length
                        K * sizeof(fp16),       // src width
                        K * sizeof(fp16),       // dst width
                        K * sizeof(fp16),      // src stride
                        8 * K * sizeof(fp16));    // dst stride
                    DrvCmxDmaStartListTask(ref1);
                    DrvCmxDmaWaitTask(ref1);
#endif
                }
                i += 1;
            }

            // create weights {T0, T7, T14, T21, T28, T35, T42}
#if defined(__RTEMS__)
            sc = OsDrvCmxDmaInitRequester(MV_TENSOR_DMA_PRIORITY, &dmaReqId);
            mvTensorAssert(sc == RTEMS_SUCCESSFUL, "DMA init requester failed");

            sc = OsDrvCmxDmaCreateTransactionFullOptions(dmaReqId, task1,
                                                         (u8*)mvTensorParam->weights->data, //src
                                                         (u8*)weights_hepta_buf,            //dst
                                                         7 * K * N * sizeof(fp16),          // byte length
                                                         K * N * sizeof(fp16),              // src width
                                                         K * N * sizeof(fp16),              // dst width
                                                         7 * K * N * sizeof(fp16),          // src stride
                                                         K * N * sizeof(fp16));             // dst stride
            mvTensorAssert(sc == RTEMS_SUCCESSFUL, "DMA create transaction failed");

            sc = OsDrvCmxDmaStartListTask(task1, &sc);
            mvTensorAssert(sc == RTEMS_SUCCESSFUL, "DMA start link task failed");

            sc = OsDrvCmxDmaWaitTask(task1);
            mvTensorAssert(sc == RTEMS_SUCCESSFUL, "DMA wait failed");
#else
            id1 = DrvCmxDmaInitRequesterOnAgent(1, mvTensorParam->myriadResources->dmaLinkAgent);
            ref1 = DrvCmxDmaCreateTransactionFullOptions(id1, task1,
                (u8*)mvTensorParam->weights->data, //src
                (u8*)weights_hepta_buf, //dst
                7 * K * N * sizeof(fp16), // byte length
                K * N * sizeof(fp16),       // src width
                K * N * sizeof(fp16),       // dst width
                7 * K * N * sizeof(fp16),      // src stride
                K * N * sizeof(fp16));    // dst stride
            DrvCmxDmaStartListTask(ref1);
            DrvCmxDmaWaitTask(ref1);
#endif

            // part1
            matmul.multiply(
                   M/4, 8 * K, N,
                   shaveNo,
                   (half*)hepta_buf,
                   (half*)weights_hepta_buf,
                   (half*)mvTensorParam->output->data,
                   8 * K * sizeof(fp16),
                   N * sizeof(fp16),
                   outputStride,
                   matmul::GEMM_HHHH_NNN_NAC);

            free(weights_hepta_buf);

            // zero the output
            for (i = 1; i < M/4; i = i + W/2)
                for (j = 0; j < N; j++)
                    *((half*)mvTensorParam->output->data + i * N + j) = 0;

#if defined(__RTEMS__)
            sc = OsDrvCmxDmaInitRequester(MV_TENSOR_DMA_PRIORITY, &dmaReqId);
            mvTensorAssert(sc == RTEMS_SUCCESSFUL, "DMA init requester failed");

            sc = OsDrvCmxDmaCreateTransactionFullOptions(dmaReqId, task1,
                                                         (u8*)hepta_buf + 8 * K * sizeof(fp16),        //src
                                                         (u8*)tetrakaideca_buf + 8 * K * sizeof(fp16), //dst
                                                         (M/4 + 2) * 8 * K * sizeof(fp16),             // byte length
                                                         8 * K * sizeof(fp16),                         // src width
                                                         8 * K * sizeof(fp16),                         // dst width
                                                         8 * K * sizeof(fp16),                         // src stride
                                                         16 * K * sizeof(fp16));                       // dst stride
            mvTensorAssert(sc == RTEMS_SUCCESSFUL, "DMA create transaction failed");

            sc = OsDrvCmxDmaStartListTask(task1, &sc);
            mvTensorAssert(sc == RTEMS_SUCCESSFUL, "DMA start link task failed");

            sc = OsDrvCmxDmaWaitTask(task1);
            mvTensorAssert(sc == RTEMS_SUCCESSFUL, "DMA wait failed");
#else
            id1 = DrvCmxDmaInitRequesterOnAgent(1, mvTensorParam->myriadResources->dmaLinkAgent);
            ref1 = DrvCmxDmaCreateTransactionFullOptions(id1, task1,
                (u8*)hepta_buf + 8 * K * sizeof(fp16), //src
                (u8*)tetrakaideca_buf + 8 * K * sizeof(fp16), //dst
                (M/4 + 2) * 8 * K * sizeof(fp16), // byte length
                8 * K * sizeof(fp16),       // src width
                8 * K * sizeof(fp16),       // dst width
                8 * K * sizeof(fp16),      // src stride
                16 * K * sizeof(fp16));    // dst stride
            DrvCmxDmaStartListTask(ref1);
            DrvCmxDmaWaitTask(ref1);
#endif
            free(hepta_buf);

            // create weights {T1, T8, T15, T22, T29, T36, T43, T2, T9, T16, T23, T30, T37, T44}
            for (i = 1; i <= 2; i++) {
#if defined(__RTEMS__)
                sc = OsDrvCmxDmaInitRequester(MV_TENSOR_DMA_PRIORITY, &dmaReqId);
                mvTensorAssert(sc == RTEMS_SUCCESSFUL, "DMA init requester failed");

                sc = OsDrvCmxDmaCreateTransactionFullOptions(dmaReqId, task1,
                                                             (u8*)mvTensorParam->weights->data + i * K * N * sizeof(fp16),     //src
                                                             (u8*)weights_tetrakaideca_buf + (i-1) * 8 * K * N * sizeof(fp16), //dst
                                                             7 * K * N * sizeof(fp16),                                         // byte length
                                                             K * N * sizeof(fp16),                                             // src width
                                                             K * N * sizeof(fp16),                                             // dst width
                                                             7 * K * N * sizeof(fp16),                                         // src stride
                                                             K * N * sizeof(fp16));                                            // dst stride
                mvTensorAssert(sc == RTEMS_SUCCESSFUL, "DMA create transaction failed");

                sc = OsDrvCmxDmaStartListTask(task1, &sc);
                mvTensorAssert(sc == RTEMS_SUCCESSFUL, "DMA start link task failed");

                sc = OsDrvCmxDmaWaitTask(task1);
                mvTensorAssert(sc == RTEMS_SUCCESSFUL, "DMA wait failed");
#else
                id1 = DrvCmxDmaInitRequesterOnAgent(1, mvTensorParam->myriadResources->dmaLinkAgent);
                ref1 = DrvCmxDmaCreateTransactionFullOptions(id1, task1,
                    (u8*)mvTensorParam->weights->data + i * K * N * sizeof(fp16), //src
                    (u8*)weights_tetrakaideca_buf + (i-1) * 8 * K * N * sizeof(fp16), //dst
                    7 * K * N * sizeof(fp16), // byte length
                    K * N * sizeof(fp16),       // src width
                    K * N * sizeof(fp16),       // dst width
                    7 * K * N * sizeof(fp16),      // src stride
                    K * N * sizeof(fp16));    // dst stride
                DrvCmxDmaStartListTask(ref1);
                DrvCmxDmaWaitTask(ref1);
#endif
            }
#if defined(__RTEMS__)
            rtems_cache_flush_l2();
            rtems_cache_invalidate_l2();

#else
            DrvLL2CFlushOpOnAllLines(LL2C_OPERATION_INVALIDATE_AND_WRITE_BACK, 0);
#endif
            // part2
            matmul.multiply(
                           M/4, 16 * K, N,
                           shaveNo,
                           (half*)tetrakaideca_buf,
                           (half*)weights_tetrakaideca_buf,
                           (half*)mvTensorParam->output->data,
                           16 * K * sizeof(fp16),
                           N * sizeof(fp16),
                           outputStride);


           // zero the output
           for (i = 0; i < M/4; i = i + W/2)
               for (j = 0; j < N; j++)
                   *((half*)mvTensorParam->output->data + i * N + j) = 0;
#if defined(__RTEMS__)
            sc = OsDrvCmxDmaInitRequester(MV_TENSOR_DMA_PRIORITY, &dmaReqId);
            mvTensorAssert(sc == RTEMS_SUCCESSFUL, "DMA init requester failed");

            sc = OsDrvCmxDmaCreateTransactionFullOptions(dmaReqId, task1,
                                                         (u8*)tetrakaideca_buf + 16 * K * sizeof(fp16), //src
                                                         (u8*)tetrakaideca_buf,                         //dst
                                                         (M/4 + 1) * 16 * K * sizeof(fp16),             // byte length
                                                         16 * K * sizeof(fp16),                         // src width
                                                         16 * K * sizeof(fp16),                         // dst width
                                                         16 * K * sizeof(fp16),                         // src stride
                                                         16 * K * sizeof(fp16));                        // dst stride
            mvTensorAssert(sc == RTEMS_SUCCESSFUL, "DMA create transaction failed");

            sc = OsDrvCmxDmaStartListTask(task1, &sc);
            mvTensorAssert(sc == RTEMS_SUCCESSFUL, "DMA start link task failed");

            sc = OsDrvCmxDmaWaitTask(task1);
            mvTensorAssert(sc == RTEMS_SUCCESSFUL, "DMA wait failed");
#else
            id1 = DrvCmxDmaInitRequesterOnAgent(1, mvTensorParam->myriadResources->dmaLinkAgent);
            ref1 = DrvCmxDmaCreateTransactionFullOptions(id1, task1,
                (u8*)tetrakaideca_buf + 16 * K * sizeof(fp16), //src
                (u8*)tetrakaideca_buf, //dst
                (M/4 + 1) * 16 * K * sizeof(fp16), // byte length
                16 * K * sizeof(fp16),       // src width
                16 * K * sizeof(fp16),       // dst width
                16 * K * sizeof(fp16),      // src stride
                16 * K * sizeof(fp16));    // dst stride
            DrvCmxDmaStartListTask(ref1);
            DrvCmxDmaWaitTask(ref1);
#endif

            // create weights {T3, T10, T17, T24, T31, T38, T45, T4, T11, T18, T25, T32, T39, T46}
            for (i = 3; i <= 4; i++) {
#if defined(__RTEMS__)
                sc = OsDrvCmxDmaInitRequester(MV_TENSOR_DMA_PRIORITY, &dmaReqId);
                mvTensorAssert(sc == RTEMS_SUCCESSFUL, "DMA init requester failed");

                sc = OsDrvCmxDmaCreateTransactionFullOptions(dmaReqId, task1,
                                                             (u8*)mvTensorParam->weights->data + i * K * N * sizeof(fp16),     //src
                                                             (u8*)weights_tetrakaideca_buf + (i-3) * 8 * K * N * sizeof(fp16), //dst
                                                             7 * K * N * sizeof(fp16),                                         // byte length
                                                             K * N * sizeof(fp16),                                             // src width
                                                             K * N * sizeof(fp16),                                             // dst width
                                                             7 * K * N * sizeof(fp16),                                         // src stride
                                                             K * N * sizeof(fp16));                                            // dst stride
                mvTensorAssert(sc == RTEMS_SUCCESSFUL, "DMA create transaction failed");

                sc = OsDrvCmxDmaStartListTask(task1, &sc);
                mvTensorAssert(sc == RTEMS_SUCCESSFUL, "DMA start link task failed");

                sc = OsDrvCmxDmaWaitTask(task1);
                mvTensorAssert(sc == RTEMS_SUCCESSFUL, "DMA wait failed");
#else
                id1 = DrvCmxDmaInitRequesterOnAgent(1, mvTensorParam->myriadResources->dmaLinkAgent);
                ref1 = DrvCmxDmaCreateTransactionFullOptions(id1, task1,
                    (u8*)mvTensorParam->weights->data + i * K * N * sizeof(fp16), //src
                    (u8*)weights_tetrakaideca_buf + (i-3) * 8 * K * N * sizeof(fp16), //dst
                    7 * K * N * sizeof(fp16), // byte length
                    K * N * sizeof(fp16),       // src width
                    K * N * sizeof(fp16),       // dst width
                    7 * K * N * sizeof(fp16),      // src stride
                    K * N * sizeof(fp16));    // dst stride
                DrvCmxDmaStartListTask(ref1);
                DrvCmxDmaWaitTask(ref1);
#endif
            }
#if defined(__RTEMS__)

            rtems_cache_flush_l2();
            rtems_cache_invalidate_l2();
#else
            DrvLL2CFlushOpOnAllLines(LL2C_OPERATION_INVALIDATE_AND_WRITE_BACK, 0);
#endif

            // part3
            matmul.multiply(
                           M/4, 16 * K, N,
                           shaveNo,
                           (half*)tetrakaideca_buf,
                           (half*)weights_tetrakaideca_buf,
                           (half*)mvTensorParam->output->data,
                           16 * K * sizeof(fp16),
                           N * sizeof(fp16),
                           outputStride);
#if defined(__RTEMS__)
            sc = OsDrvCmxDmaInitRequester(MV_TENSOR_DMA_PRIORITY, &dmaReqId);
            mvTensorAssert(sc == RTEMS_SUCCESSFUL, "DMA init requester failed");

            sc = OsDrvCmxDmaCreateTransactionFullOptions(dmaReqId, task1,
                                                         (u8*)tetrakaideca_buf + 16 * K * sizeof(fp16), //src
                                                         (u8*)tetrakaideca_buf,                         //dst
                                                         M/4 * 16 * K * sizeof(fp16),                   // byte length
                                                         16 * K * sizeof(fp16),                         // src width
                                                         16 * K * sizeof(fp16),                         // dst width
                                                         16 * K * sizeof(fp16),                         // src stride
                                                         16 * K * sizeof(fp16));                        // dst stride
            mvTensorAssert(sc == RTEMS_SUCCESSFUL, "DMA create transaction failed");

            sc = OsDrvCmxDmaStartListTask(task1, &sc);
            mvTensorAssert(sc == RTEMS_SUCCESSFUL, "DMA start link task failed");

            sc = OsDrvCmxDmaWaitTask(task1);
            mvTensorAssert(sc == RTEMS_SUCCESSFUL, "DMA wait failed");
#else
            id1 = DrvCmxDmaInitRequesterOnAgent(1, mvTensorParam->myriadResources->dmaLinkAgent);
            ref1 = DrvCmxDmaCreateTransactionFullOptions(id1, task1,
                (u8*)tetrakaideca_buf + 16 * K * sizeof(fp16), //src
                (u8*)tetrakaideca_buf, //dst
                M/4 * 16 * K * sizeof(fp16), // byte length
                16 * K * sizeof(fp16),       // src width
                16 * K * sizeof(fp16),       // dst width
                16 * K * sizeof(fp16),      // src stride
                16 * K * sizeof(fp16));    // dst stride
            DrvCmxDmaStartListTask(ref1);
            DrvCmxDmaWaitTask(ref1);
#endif
            // zero the input
            for (i = W/2 - 1; i < M/4; i = i + W/2)
                for (j = 0; j < 16 * K; j++)
                    *((half*)tetrakaideca_buf + i * 16 * K + j) = 0;

            // create weights {T5, T12, T19, T26, T33, T40, T47, T6, T13, T20, T27, T34, T41, T48}
            for (i = 5; i <= 6; i++) {
#if defined(__RTEMS__)
                sc = OsDrvCmxDmaInitRequester(MV_TENSOR_DMA_PRIORITY, &dmaReqId);
                mvTensorAssert(sc == RTEMS_SUCCESSFUL, "DMA init requester failed");

                sc = OsDrvCmxDmaCreateTransactionFullOptions(dmaReqId, task1,
                                                             (u8*)mvTensorParam->weights->data + i * K * N * sizeof(fp16),     //src
                                                             (u8*)weights_tetrakaideca_buf + (i-5) * 8 * K * N * sizeof(fp16), //dst
                                                             7 * K * N * sizeof(fp16),                                         // byte length
                                                             K * N * sizeof(fp16),                                             // src width
                                                             K * N * sizeof(fp16),                                             // dst width
                                                             7 * K * N * sizeof(fp16),                                         // src stride
                                                             K * N * sizeof(fp16));                                            // dst stride
                mvTensorAssert(sc == RTEMS_SUCCESSFUL, "DMA create transaction failed");

                sc = OsDrvCmxDmaStartListTask(task1, &sc);
                mvTensorAssert(sc == RTEMS_SUCCESSFUL, "DMA start link task failed");

                sc = OsDrvCmxDmaWaitTask(task1);
                mvTensorAssert(sc == RTEMS_SUCCESSFUL, "DMA wait failed");
#else
                id1 = DrvCmxDmaInitRequesterOnAgent(1, mvTensorParam->myriadResources->dmaLinkAgent);
                ref1 = DrvCmxDmaCreateTransactionFullOptions(id1, task1,
                    (u8*)mvTensorParam->weights->data + i * K * N * sizeof(fp16), //src
                    (u8*)weights_tetrakaideca_buf + (i-5) * 8 * K * N * sizeof(fp16), //dst
                    7 * K * N * sizeof(fp16), // byte length
                    K * N * sizeof(fp16),       // src width
                    K * N * sizeof(fp16),       // dst width
                    7 * K * N * sizeof(fp16),      // src stride
                    K * N * sizeof(fp16));    // dst stride
                DrvCmxDmaStartListTask(ref1);
                DrvCmxDmaWaitTask(ref1);
#endif
            }

#if defined(__RTEMS__)
            rtems_cache_flush_l2();
            rtems_cache_invalidate_l2();
#else
            DrvLL2CFlushOpOnAllLines(LL2C_OPERATION_INVALIDATE_AND_WRITE_BACK, 0);
#endif
            // part4
            matmul.multiply(
                           M/4, 16 * K, N,
                           shaveNo,
                           (half*)tetrakaideca_buf,
                           (half*)weights_tetrakaideca_buf,
                           (half*)mvTensorParam->output->data,
                           16 * K * sizeof(fp16),
                           N * sizeof(fp16),
                           outputStride);

            free(tetrakaideca_buf);
            free(weights_tetrakaideca_buf);
        }

        else
        {
            u32 hH = H/2;
            mvTensorAssert(K >= 8, "inputChannels too small.");
            mvTensorAssert(K % 8 == 0, "inputChannels must be %8.");

            const u32 kMatmulOrderOfCalls[kMatmulCalls] = {
                    10, 24, 38,        // A_ee
                    11, 25, 39,        // A_eo
                    3, 17, 31, 45,     // A_oe
                    4, 18, 32, 46,     // A_oo
                    12, 26, 40,        // A_ee*
                    13, 27, 41,        // A_eo*
                    5, 19, 33, 47,     // A_oe*
                    6, 20, 34, 48,     // A_oo*
                    8, 22, 36,         // A_ee+*
                    9, 23, 37,         // A_eo+*
                    1, 15, 29, 43,     // A_oe+*
                    2, 16, 30, 44,     // A_oo+*
                    7, 21, 35,         // A_eo**
                    0, 14, 28, 42      // A_oo**
                };
            const u32 kBuffersOrder[kMatmulCalls] = {
                    0, 0, 0,           // A_ee
                    1, 1, 1,           // A_eo
                    2, 2, 2, 2,        // A_oe
                    3, 3, 3, 3,        // A_oo
                    0, 0, 0,           // A_ee*
                    1, 1, 1,           // A_eo*
                    2, 2, 2, 2,        // A_oe*
                    3, 3, 3, 3,        // A_oo*
                    0, 0, 0,           // A_ee+*
                    1, 1, 1,           // A_eo+*
                    2, 2, 2, 2,        // A_oe+*
                    3, 3, 3, 3,        // A_oo+*
                    1, 1, 1,           // A_eo**
                    3, 3, 3, 3         // A_oo**
                };
             u32 current_tap, buf_idx;
             u32 num_buffers = 4;
             unsigned short** buf_values;
             buf_values = (unsigned short**)malloc(num_buffers*sizeof(unsigned short*));
             mvTensorAssert(buf_values != NULL, "7x7s2 Unable to assign buffer container.");
             for (i = 0; i < num_buffers; i++) {
                 buf_values[i] = (unsigned short*)malloc(hH * K * sizeof(unsigned short));
                 mvTensorAssert(buf_values[i] != NULL, "7x7s2 Unable to assign buffers.");
                 }

             u32 buf_values_idx = 0;

             for (i = 0; i < kMatmulCalls; i++) {
                 current_tap = kMatmulOrderOfCalls[i];

                if (current_tap == 12) {
                     // need all A*
                     for (buf_idx = 0; buf_idx < 4; buf_idx++) {
                         buf_values_idx = 0;
                         for (j = 1; j <=hH; j++)
                             for (k = 0; k < K; k++) {
                                 buf_values[buf_idx][buf_values_idx++] = buffers[buf_idx][(j-1)*hW*K + k];
                                 buffers[buf_idx][(j-1)*hW*K + k] = 0;
                             }
                     }
                 }
                if (current_tap == 8) {
                    // need all A+*
                    for (buf_idx = 0; buf_idx < 4; buf_idx++) {
                        buf_values_idx = 0;
                        for (j = 1; j <=hH; j++)
                            for (k = 0; k < K; k++) {
                                buffers[buf_idx][(j-1)*hW*K + k] = buf_values[buf_idx][buf_values_idx++];
                                buffers[buf_idx][(hW -1)*K + (j-1)*hW*K + k] = 0;
                            }
                    }
                }
                if (current_tap == 7) {
                    // need A_eo** and A_oo**
                    for (j = 1; j <=hH; j++)
                        for (k = 0; k < K; k++) {
                            buffers[1][(hW-2)*K + (j-1)*hW*K + k] = 0;
                            buffers[3][(hW-2)*K + (j-1)*hW*K + k] = 0;
                            }
                }

                matmul.multiply(
                       M/4, K, N,
                       shaveNo,
                       (half*)buffers[kBuffersOrder[i]],
                       (half*)mvTensorParam->weights->data + kMatmulOrderOfCalls[i] * K * N,
                       (half*)mvTensorParam->output->data + offsets[kMatmulOrderOfCalls[i]] * N,
                       inputStride,
                       weightsStride,
                       outputStride,
                       i == 0 ? matmul::GEMM_HHHH_NNN_NAC : matmul::GEMM_HHHH_NNN);

            }

            for (i = 0; i < 4; i++)
            {
                free(buf_values[i]);
            }
            free(buf_values);

        }

        for (i = 0; i < 4; i++)
        {
            free(buffers[i]);
        }

        free(buffers);
    }

    else {

        if(mvTensorParam->op->type == kDeconvolution)
        {
            MVT_DPRINTF("Deconv MxN with stride S1xS2 0x -> %p\n", mvTensorParam->output->data);
        }
        else
        {
            MVT_DPRINTF("Conv MxN with stride S1xS2 0x -> %p\n", mvTensorParam->output->data);
        }

        // silence the warnings
        (void)outputBpp;
        // u32 M = mvTensorParam->input->dimY * mvTensorParam->input->dimX;
        u32 K = mvTensorParam->input->dimZ;
        u32 N = mvTensorParam->output->dimZ;
        u32 W = mvTensorParam->input->dimX;
        u32 H = mvTensorParam->input->dimY;
        u32 radix_x = mvTensorParam->op->radixX;
        u32 radix_y = mvTensorParam->op->radixY;
        u32 stride_x = mvTensorParam->op->strideX;
        u32 stride_y = mvTensorParam->op->strideY;
        u32 out_W, out_H;
        // compute input buffers size
        u32 subblocks = ceil((float)W / stride_x);
        u32 tile_size = stride_x * stride_y;
        u32 group = subblocks * stride_x;
        u32 elems = group * H;
        u32 blocks = ceil((float)elems / (group * stride_y));
        u32 i = 0, j, k, n, buf_idx;
        u32 free_buffers;
        unsigned short** buffers = (unsigned short**)malloc(tile_size*sizeof(unsigned short*));
        mvTensorAssert(buffers != NULL, "MxN Unable to assign buffer container");
        if (stride_x > 1 || stride_y > 1) {
            free_buffers = 1;
            for (i = 0; i < tile_size; i++) {
                buffers[i] = (unsigned short*)malloc(blocks * subblocks * K * sizeof(unsigned short));
                mvTensorAssert(buffers[i] != NULL, "MxN Unable to assign buffers");
                bzero(buffers[i], blocks * subblocks * K * sizeof(unsigned short));
            }
            for (i = 0; i < H; i++)
                for (j = 0; j < W; j++)
                    for (k = 0; k < K; k++)
                        buffers[(i%stride_y)*stride_x + j%stride_x][i/stride_y * (u32)ceil((float)W/stride_x) * K + j/stride_x * K + k] = *((unsigned short*)mvTensorParam->input->data + k + j * K + i * K * W);
        }
        else {
            free_buffers = 0;
            buffers[0] = (unsigned short*)mvTensorParam->input->data;
        }

        if(useSamePadding) {
        // SAME padding
        out_W = ceil((float)W / stride_x);
        out_H = ceil((float)H / stride_y);
        u32 center_y = radix_y / 2;
        u32 center_x = radix_x / 2;
        s32 zero_out = (s32)ceil((float)center_x / stride_x);
        s32 relative_i, relative_j;
        s32 offset, x, y, zero_in_en;
        u32 start_buf_idx = ((u32)ceil((float)center_x/stride_x) * stride_x - center_x) % stride_x +
        (((u32)ceil((float)center_y/stride_y) * stride_y - center_y) % stride_y) * stride_x;
        u32 last_elem = (out_W * stride_x) - (stride_x / 2);
        for (j = 0; j < radix_x; j++) {
            relative_j = j - center_x;
            x = (s32)ceil((-relative_j)/(float)stride_x);
            zero_in_en = (s32)floor(relative_j/stride_x);
            if (relative_j <= 0 && x < zero_out) {
                for (k = zero_out - 1; k < out_W * out_H; k = k + out_W)
                    for (n = 0; n < N; n++)
                        if(W < (last_elem + ceil((float)radix_x / 2)))
                            *((unsigned short*)mvTensorParam->output->data + n + k*(outputStride/outputBpp)) = 0;
                zero_out = x;
            }

#if defined(__RTEMS__)
            rtems_cache_flush_l2();
            rtems_cache_invalidate_l2();
#else
            DrvLL2CFlushOpOnAllLines(LL2C_OPERATION_INVALIDATE, 0);
#endif
            for (i = 0; i < radix_y; i++) {
                buf_idx = ((start_buf_idx /stride_x + i) % stride_y) * stride_x + (start_buf_idx + j) % stride_x;
                if (relative_j > 0)
                    if (zero_in_en > 0)
                        for (k = 0; k < K; k++)
                            for (n = 0; n < blocks * subblocks * K; n = n + out_W*K)
                                buffers[buf_idx][(zero_in_en - 1)*K + k + n] = 0;
                relative_i = i - center_y;
                y = (s32)ceil((-relative_i)/(float)stride_y);
                u32 input_offset, output_offset;
                offset = (s32)y * out_W + x;
                if (offset > 0) {
                    input_offset = 0;
                    output_offset = offset;
                }
                else {
                    input_offset = abs(offset);
                    output_offset = 0;
                }

                u32 weights_offset =  (i * radix_x + j) * K * N;
                if(W >= (last_elem + ceil((float)radix_x / 2)))
                {
                    output_offset = 0;
                    weights_offset = buf_idx * K * N;
                }

                const u32 corner_case = 8065;
                if ((out_H*out_W - input_offset - output_offset) == corner_case)
                {
                    u32 m1 = corner_case/2;
                    u32 m2 = corner_case/2 + 1;
                    matmul.multiply(
                                    m1, K, N,
                                    shaveNo,
                                    (half*) buffers[buf_idx] + input_offset * K,
                                    (half*) mvTensorParam->weights->data + weights_offset,
                                    (half*) mvTensorParam->output->data + output_offset * (outputStride/outputBpp),
                                    inputStride,
                                    weightsStride,
                                    outputStride,
                                    matmul::GEMM_HHHH_NNN_NAC);


                    matmul.multiply(
                                    m2, K, N,
                                    shaveNo,
                                    (half*) buffers[buf_idx] + m1 * K + input_offset * K,
                                    (half*) mvTensorParam->weights->data + weights_offset,
                                    (half*) mvTensorParam->output->data + m1 * (outputStride/outputBpp) + output_offset * (outputStride/outputBpp),
                                    inputStride,
                                    weightsStride,
                                    outputStride
                                    );

                }
                else
                  matmul.multiply(
                                out_H*out_W - input_offset - output_offset, K, N,
                                shaveNo,
                                (half*) buffers[buf_idx] + input_offset * K,
                                (half*) mvTensorParam->weights->data + weights_offset,
                                (half*) mvTensorParam->output->data + output_offset * (outputStride/outputBpp),
                                inputStride,
                                weightsStride,
                                outputStride,
                                j == 0 && i == 0 ? matmul::GEMM_HHHH_NNN_NAC : matmul::GEMM_HHHH_NNN);

             }
          }
        }
        else {
            // VALID padding
            dmaTransactionList_t *ref1;
            dmaRequesterId id1;
            u32 buf_pos;
            out_W = ceil((W-radix_x + 1)/(float)stride_x);
            out_H = ceil((H-radix_y + 1)/(float)stride_y);
            unsigned short* buf_tmp = (unsigned short*)malloc(out_W * out_H * K * sizeof(unsigned short));
            for (j = 0; j < radix_x; j++) {
                for (i = 0; i < radix_y; i++) {
                    buf_idx = (i % stride_y) * stride_x + (j % stride_x);
                    buf_pos = (i / stride_y) * (u32)subblocks + (j / stride_x);
                    // TODO: reuse buf_tmp for the same buf_idx
#if defined(__RTEMS__)
                    sc = OsDrvCmxDmaInitRequester(MV_TENSOR_DMA_PRIORITY, &dmaReqId);
                    mvTensorAssert(sc == RTEMS_SUCCESSFUL, "DMA init requester failed");

                    sc = OsDrvCmxDmaCreateTransactionFullOptions(dmaReqId, task1,
                                                                 (u8*)buffers[buf_idx] + buf_pos * K * sizeof(fp16), //src
                                                                 (u8*)buf_tmp,                                       //dst
                                                                 out_W * out_H * K * sizeof(fp16),                   // byte length
                                                                 out_W * K * sizeof(fp16),                           // src width
                                                                 out_W * K * sizeof(fp16),                           // dst width
                                                                 (u32)subblocks * K * sizeof(fp16),                  // src stride
                                                                 out_W * K * sizeof(fp16));                          // dst stride
                    mvTensorAssert(sc == RTEMS_SUCCESSFUL, "DMA create transaction failed");

                    sc = OsDrvCmxDmaStartListTask(task1, &sc);
                    mvTensorAssert(sc == RTEMS_SUCCESSFUL, "DMA start link task failed");

                    sc = OsDrvCmxDmaWaitTask(task1);
                    mvTensorAssert(sc == RTEMS_SUCCESSFUL, "DMA wait failed");
#else
                    id1 = DrvCmxDmaInitRequesterOnAgent(1, mvTensorParam->myriadResources->dmaLinkAgent);
                    ref1 = DrvCmxDmaCreateTransactionFullOptions(id1, task1,
                         (u8*)buffers[buf_idx] + buf_pos * K * sizeof(fp16), //src
                         (u8*)buf_tmp, //dst
                         out_W * out_H * K * sizeof(fp16), // byte length
                         out_W * K * sizeof(fp16),       // src width
                         out_W * K * sizeof(fp16),       // dst width
                         (u32)subblocks * K * sizeof(fp16),      // src stride
                         out_W * K * sizeof(fp16));    // dst stride
                    DrvCmxDmaStartListTask(ref1);
                    DrvCmxDmaWaitTask(ref1);
#endif
                    matmul.multiply(
                                    out_H * out_W, K, N,
                                    shaveNo,
                                    (half*) buf_tmp,
                                    (half*) mvTensorParam->weights->data + (i * radix_x + j) * K * N,
                                    (half*) mvTensorParam->output->data,
                                    inputStride,
                                    weightsStride,
                                    outputStride,
                                    j == 0 && i == 0 ? matmul::GEMM_HHHH_NNN_NAC : matmul::GEMM_HHHH_NNN);

                }
            }
            free(buf_tmp);
          }
          if(free_buffers == 1)
            for (i = 0; i < tile_size; i++)
              free(buffers[i]);
          free(buffers);
        }
    }

    if(mvTensorParam->op->type == kDepthConv) {
        u32 channel_multiplier;
        channel_multiplier = mvTensorParam->output->dimZ / mvTensorParam->input->dimZ;
        depthConv((fp16*)mvTensorParam->input->data,
                  mvTensorParam->input->dimY,
                  mvTensorParam->input->dimX,
                  mvTensorParam->input->dimZ,
                  (fp16*)mvTensorParam->weights->data,
                  mvTensorParam->op->radixY,
                  mvTensorParam->op->radixX,
                  mvTensorParam->op->strideY,
                  mvTensorParam->op->strideX,
                  channel_multiplier,
                  (fp16*)mvTensorParam->output->data,
                  mvTensorParam->output->dimY,
                  mvTensorParam->output->dimX,
                  mvTensorParam->myriadResources);
    }

    if(mvTensorParam->op->type == kRelayout)
    {
        relayout((fp16 *)mvTensorParam->input->data,
                (fp16 *)mvTensorParam->output->data,
                mvTensorParam->input->dimX,
                mvTensorParam->input->dimY,
                mvTensorParam->input->dimZ,
                mvTensorParam->op->radixX,
                mvTensorParam->op->radixY,
                mvTensorParam->op->strideX,
                mvTensorParam->op->strideY,
                mvTensorParam->op->paddStyle,
                mvTensorParam->op->padX,
                mvTensorParam->op->padY,
                CONVOLUTION_V0,
                mvTensorParam->myriadResources);
    }

    if(mvTensorParam->op->type == kMaxPool)
    {
        if(mvTensorParam->op->radixX == 2 && mvTensorParam->op->radixY == 2 &&
           (g_optimisations & OPT_MAXPOOL2X2))
           // 2x2s2 SAME EVEN
        {
            MVT_DPRINTF("MaxPool 2x2\n");

            maxPool2x2((fp16*)mvTensorParam->input->data,
                         mvTensorParam->input->dimZ,
                         mvTensorParam->input->dimY,
                         mvTensorParam->input->dimX,
                         mvTensorParam->op->paddStyle,
                         mvTensorParam->op->strideX,
                         mvTensorParam->op->padX,
                         outputStride,
                         (fp16*)mvTensorParam->output->data,
                         mvTensorParam->myriadResources->dmaLinkAgent,
                         mvTensorParam->myriadResources->firstShave,
                         lastShave);
        }
        else if(mvTensorParam->op->radixX == 3 && mvTensorParam->op->radixY == 3
                 && (g_optimisations & OPT_MAXPOOL3X3))
        {
            MVT_DPRINTF("MaxPool 3x3\n");

            maxPool3x3((fp16*)mvTensorParam->input->data,
                         mvTensorParam->input->dimZ,
                         mvTensorParam->input->dimY,
                         mvTensorParam->input->dimX,
                         mvTensorParam->op->paddStyle,
                         mvTensorParam->op->strideX,
                         mvTensorParam->op->padX,
                         outputStride,
                         (fp16*)mvTensorParam->output->data,
                         mvTensorParam->myriadResources->dmaLinkAgent,
                         mvTensorParam->myriadResources->firstShave,
                         lastShave);

            MVT_DPRINTF("out_H %d\n", (int)mvTensorParam->output->dimY);
            MVT_DPRINTF("out_W %d\n", (int)mvTensorParam->output->dimX);
        }
        else
        {
            MVT_DPRINTF("MaxPool MxN\n");
            MVT_DPRINTF("dimZ: %d\n",   (int)mvTensorParam->input->dimZ);
            MVT_DPRINTF("dimY: %d\n",   (int)mvTensorParam->input->dimY);
            MVT_DPRINTF("dimX: %d\n",   (int)mvTensorParam->input->dimX);
            MVT_DPRINTF("pad: %d\n",    (int)(mvTensorParam->op->paddStyle));
            MVT_DPRINTF("radixX: %d\n", (int)mvTensorParam->op->radixX);
            MVT_DPRINTF("radixY: %d\n", (int)mvTensorParam->op->radixY);
            MVT_DPRINTF("strideX: %d\n", (int)mvTensorParam->op->strideX);
            MVT_DPRINTF("strideY: %d\n", (int)mvTensorParam->op->strideY);
            MVT_DPRINTF("padX: %d\n",   (int)mvTensorParam->op->padX);
            MVT_DPRINTF("padY: %d\n",   (int)mvTensorParam->op->padY);

            maxPoolMxN((fp16*)mvTensorParam->input->data,
                    mvTensorParam->input->dimZ,
                    mvTensorParam->input->dimY,
                    mvTensorParam->input->dimX,
                    mvTensorParam->op->paddStyle,
                    mvTensorParam->op->radixX,
                    mvTensorParam->op->radixY,
                    mvTensorParam->op->strideX,
                    mvTensorParam->op->strideY,
                    mvTensorParam->op->padX,
                    mvTensorParam->op->padY,
                    outputStride,
                    (fp16*)mvTensorParam->output->data,
                    mvTensorParam->myriadResources->dmaLinkAgent,
                    mvTensorParam->myriadResources->firstShave,
                    lastShave);

            MVT_DPRINTF("out_H %d\n", (int)mvTensorParam->output->dimY);
            MVT_DPRINTF("out_W %d\n", (int)mvTensorParam->output->dimX);

        }
#if defined(__RTEMS__)
        rtems_cache_flush_entire_l1_data();
#else
        swcLeonDataCacheFlush();
        swcLeonFlushCaches();
#endif
    }

    if(mvTensorParam->op->type == kAvgPool)
    {
        if(mvTensorParam->op->radixX == 3 && mvTensorParam->op->radixY == 3)
        {
            MVT_DPRINTF("AvgPool 3x3\n");

            avgPool3x3((fp16*)mvTensorParam->input->data,
                         mvTensorParam->input->dimZ,
                         mvTensorParam->input->dimY,
                         mvTensorParam->input->dimX,
                         mvTensorParam->op->paddStyle,
                         mvTensorParam->op->strideX,
                         mvTensorParam->op->padX,
                         outputStride,
                         (fp16*)mvTensorParam->output->data,
                         mvTensorParam->myriadResources->dmaLinkAgent,
                         mvTensorParam->myriadResources->firstShave,
                         lastShave);
#if defined(__RTEMS__)
        rtems_cache_flush_entire_l1_data();
#else
            swcLeonDataCacheFlush();
            swcLeonFlushCaches();
#endif

        }
        else if(mvTensorParam->op->radixX == 7 && mvTensorParam->op->radixY == 7 &&
           mvTensorParam->op->strideX == 1 && mvTensorParam->op->strideY == 1 &&
           mvTensorParam->input->dimX == 7 && mvTensorParam->input->dimY == 7)
        // global pooling
        {
            MVT_DPRINTF("AvgPool 7x7 \n");

            avgPool7x7xk((fp16*)mvTensorParam->input->data,
                         mvTensorParam->input->dimZ,
                         (fp16*)mvTensorParam->output->data,
                         mvTensorParam->myriadResources->dmaLinkAgent,
                         mvTensorParam->myriadResources->firstShave,
                         lastShave);
        }
        else
        {
            MVT_DPRINTF("AvgPool MxN \n");

            avgPoolMxN((fp16*)mvTensorParam->input->data,
                    mvTensorParam->input->dimZ,
                    mvTensorParam->input->dimY,
                    mvTensorParam->input->dimX,
                    mvTensorParam->op->paddStyle,
                    mvTensorParam->op->radixX,
                    mvTensorParam->op->radixY,
                    mvTensorParam->op->strideX,
                    mvTensorParam->op->strideY,
                    mvTensorParam->op->padX,
                    mvTensorParam->op->padY,
                    outputStride,
                    (fp16*)mvTensorParam->output->data,
                    mvTensorParam->myriadResources->dmaLinkAgent,
                    mvTensorParam->myriadResources->firstShave,
                    lastShave);
        }
#if defined(__RTEMS__)
        rtems_cache_flush_entire_l1_data();
#else
        swcLeonDataCacheFlush();
        swcLeonFlushCaches();
#endif
    }

    if(mvTensorParam->op->type == kSoftMax)
    {
        MVT_DPRINTF("Softmax\n");

        softMax((fp16*)mvTensorParam->input->data,
                mvTensorParam->input->dimZ,
                (fp16*)mvTensorParam->output->data,
                mvTensorParam->myriadResources->dmaLinkAgent,
                mvTensorParam->myriadResources->firstShave,
                lastShave);
    }

    if(mvTensorParam->op->type == kFC)
    {
        MVT_DPRINTF("FCL\n");

        fc((fp16*)mvTensorParam->input->data,
           (fp16*)mvTensorParam->weights->data,
           mvTensorParam->input->dimZ,
           mvTensorParam->output->dimZ,
           (fp16*)mvTensorParam->output->data,
           mvTensorParam->myriadResources->dmaLinkAgent,
           mvTensorParam->myriadResources->firstShave,
           lastShave);
    }

    if(mvTensorParam->op->type== kPRelu)
    {
        MVT_DPRINTF("PRELU0\n");
        // We put them in bias, because they are 1D, so they fit there better
        unsigned int weights_addr = (u32)mvTensorParam->biases->data;
        MVT_DPRINTF("PReLU DIMS: %i %i %i\n",mvTensorParam->output->dimZ,
               mvTensorParam->output->dimX * mvTensorParam->output->dimY,
               mvTensorParam->output->dimXStride/sizeof(half));

        preluFp16((fp16 *)mvTensorParam->input->data,
            (fp16 *)mvTensorParam->output->data,
            (fp16 *)weights_addr,
            mvTensorParam->output->dimZ,
            mvTensorParam->output->dimX * mvTensorParam->output->dimY,
            mvTensorParam->output->dimXStride/sizeof(half),
            mvTensorParam->myriadResources);
    }

    if(mvTensorParam->op->type== kSigmoid)
    {
        //true  = Use the accurate method (slower).
        //false = Use the fast method (less accurate).
        const bool be_accurate = false;
        sigmoidFp16((fp16 *)mvTensorParam->input->data,
                (fp16 *)mvTensorParam->output->data,
                mvTensorParam->input->dimZ,
                mvTensorParam->input->dimX * mvTensorParam->input->dimY,
                mvTensorParam->input->dimXStride/sizeof(half),
                mvTensorParam->myriadResources,
                be_accurate);
    }

    if(mvTensorParam->op->type == kTanh)
    {
        //true  = Use the accurate method (slower).
        //false = Use the fast method (less accurate).
        const bool be_accurate = false;
        tanhFp16((fp16 *)mvTensorParam->input->data,
                (fp16 *)mvTensorParam->output->data,
                mvTensorParam->input->dimZ,
                mvTensorParam->input->dimX * mvTensorParam->input->dimY,
                mvTensorParam->input->dimXStride/sizeof(half),
                mvTensorParam->myriadResources,
                be_accurate);
    }

    if(mvTensorParam->op->type== kLRN)
    {
        mvTensorAssert(mvTensorParam->matmulResources->cache_memory_ptr != NULL,"Cache memory pointer is null");

        matmul::MatMulCache& cache = matmul::MatMulCache::instance();
        cache.config(mvTensorParam->matmulResources->cache_memory_ptr, mvTensorParam->matmulResources->cache_memory_size);

        matmul::MatMulConfig cfg;
        cfg.matrix_type = static_cast<matmul::MatMulType> (matmul::MMT_HALF);
        cfg.kernel_type = static_cast<matmul::kernel_t> (matmul::GEMM_HHHH_NNN);
        cfg.kernel_width = 8;
        cfg.dma_link_agent = mvTensorParam->myriadResources->dmaLinkAgent;
        cfg.first_shave = mvTensorParam->myriadResources->firstShave;
        cfg.scratch_memory_size = mvTensorParam->matmulResources->scratch_memory_size;
        cfg.scratch_memory = mvTensorParam->matmulResources->scratch_memory_ptr;
        cfg.error_buffer = dbgBuf;
        matmul::MvMatMul<half> matmul(cfg, cache);

        if(g_enableMatmulDebugTrace)
            matmul.enable_trace();

        lrn((half *)mvTensorParam->input->data,
            mvTensorParam->input->dimZ,
            mvTensorParam->input->dimY,
            mvTensorParam->input->dimX,
            (half *)mvTensorParam->output->data,
            mvTensorParam->op->radixX,
            ((half *)mvTensorParam->biases->data)[0],
            ((half *)mvTensorParam->biases->data)[1],
            ((half *)mvTensorParam->biases->data)[2],
            mvTensorParam->myriadResources->firstShave,
            lastShave,
            mvTensorParam->myriadResources->dmaLinkAgent);
    }
    if(mvTensorParam->op->type == kSum ||
             mvTensorParam->op->type == kProd ||
             mvTensorParam->op->type == kMax)
    {
        eltwise(mvTensorParam->op->type,
             (half *)mvTensorParam->input->data,
             mvTensorParam->input->dimZ,
             mvTensorParam->input->dimY,
             mvTensorParam->input->dimX,
             (half *)mvTensorParam->weights->data,
             mvTensorParam->weights->dimZ,
             (half *)mvTensorParam->output->data,
             mvTensorParam->input->dimXStride,
             mvTensorParam->weights->dimXStride,
             mvTensorParam->output->dimXStride,
             mvTensorParam->myriadResources->firstShave,
             lastShave,
             mvTensorParam->myriadResources->dmaLinkAgent);
     }

    if(mvTensorParam->op->type == kScale)
    {
        MVT_DPRINTF("BATCHNORM\n");

        scaleFp16((fp16 *)mvTensorParam->input->data,
            (fp16 *)mvTensorParam->output->data,
            (fp16 *)mvTensorParam->weights->data,
            mvTensorParam->postOp->type == kBias ? (fp16 *)mvTensorParam->biases->data : 0,
            mvTensorParam->output->dimZ,
            mvTensorParam->output->dimX * mvTensorParam->output->dimY,
            mvTensorParam->output->dimXStride/sizeof(half),
            mvTensorParam->myriadResources);
    }

    if(mvTensorParam->op->type == kSquare)
    {
        MVT_DPRINTF("BATCHNORM\n");

        squareFp16((fp16 *)mvTensorParam->input->data,
            (fp16 *)mvTensorParam->output->data,
            mvTensorParam->output->dimZ,
            mvTensorParam->output->dimX * mvTensorParam->output->dimY,
            mvTensorParam->output->dimXStride/sizeof(half),
            mvTensorParam->myriadResources);
    }

    if(mvTensorParam->op->type == kInnerLRN)
    {
        MVT_DPRINTF("BATCHNORM\n");

        innerLRNFp16((fp16 *)mvTensorParam->input->data,
            (fp16 *)mvTensorParam->output->data,
            ((fp16 *)mvTensorParam->biases->data)[1],
            ((fp16 *)mvTensorParam->biases->data)[2],
            mvTensorParam->output->dimZ,
            mvTensorParam->output->dimX * mvTensorParam->output->dimY,
            mvTensorParam->output->dimXStride/sizeof(half),
            mvTensorParam->myriadResources);
    }

    if(mvTensorParam->op->type== kCopy)
    {
        dmaTransactionList_t *ref1;
        dmaRequesterId id1;

#if defined(__RTEMS__)
        sc = OsDrvCmxDmaInitRequester(MV_TENSOR_DMA_PRIORITY, &dmaReqId);
        mvTensorAssert(sc == RTEMS_SUCCESSFUL, "DMA init requester failed");

        sc = OsDrvCmxDmaCreateTransactionFullOptions(dmaReqId, task1,
                                                     (u8 *)mvTensorParam->input->data,
                                                     (u8 *)mvTensorParam->output->data,
                                                     sizeof(fp16) * mvTensorParam->output->dimX * mvTensorParam->output->dimY * mvTensorParam->output->dimZ,
                                                     sizeof(fp16) * mvTensorParam->output->dimZ,
                                                     sizeof(fp16) * mvTensorParam->output->dimZ,
                                                     inputStride,
                                                     outputStride);
        mvTensorAssert(sc == RTEMS_SUCCESSFUL, "DMA create transaction failed");

        sc = OsDrvCmxDmaStartListTask(task1, &sc);
        mvTensorAssert(sc == RTEMS_SUCCESSFUL, "DMA start link task failed");

        sc = OsDrvCmxDmaWaitTask(task1);
        mvTensorAssert(sc == RTEMS_SUCCESSFUL, "DMA wait failed");
#else
        DrvCmxDmaInitDefault();
        id1 = DrvCmxDmaInitRequesterOnAgent(1, mvTensorParam->myriadResources->dmaLinkAgent);
        ref1 = DrvCmxDmaCreateTransactionFullOptions(id1, task1,
             (u8 *)mvTensorParam->input->data,
             (u8 *)mvTensorParam->output->data,
             sizeof(fp16) * mvTensorParam->output->dimX * mvTensorParam->output->dimY * mvTensorParam->output->dimZ,
             sizeof(fp16) * mvTensorParam->output->dimZ,
             sizeof(fp16) * mvTensorParam->output->dimZ,
             inputStride,
             outputStride);

        DrvCmxDmaStartListTask(ref1);
        DrvCmxDmaWaitTask(ref1);
#endif
    }
    if(mvTensorParam->op->type == kToPlaneMajor)
    {
        dmaTransactionList_t *ref1;
        dmaRequesterId id1;
        int i;

#if defined(__RTEMS__)
        sc = OsDrvCmxDmaInitRequester(MV_TENSOR_DMA_PRIORITY, &dmaReqId);
        mvTensorAssert(sc == RTEMS_SUCCESSFUL, "DMA init requester failed");

        for(i = 0; i < mvTensorParam->input->dimZ; i++)
        {
            sc = OsDrvCmxDmaCreateTransactionFullOptions(dmaReqId, task1,
                        (u8*)mvTensorParam->input->data + sizeof(fp16)*i,
                        (u8*)mvTensorParam->output->data + i * sizeof(fp16) * mvTensorParam->input->dimX * mvTensorParam->input->dimY,
                        sizeof(fp16) * mvTensorParam->input->dimX * mvTensorParam->input->dimY,
                        sizeof(fp16),
                        sizeof(fp16),
                        inputStride,
                        sizeof(fp16));
            mvTensorAssert(sc == RTEMS_SUCCESSFUL, "DMA create transaction failed");

            sc = OsDrvCmxDmaStartListTask(task1, &sc);
            mvTensorAssert(sc == RTEMS_SUCCESSFUL, "DMA start link task failed");

            sc = OsDrvCmxDmaWaitTask(task1);
            mvTensorAssert(sc == RTEMS_SUCCESSFUL, "DMA wait failed");
        }
#else

        DrvCmxDmaInitDefault();
        id1 = DrvCmxDmaInitRequesterOnAgent(1, mvTensorParam->myriadResources->dmaLinkAgent);
        for(i = 0; i < mvTensorParam->input->dimZ; i++)
        {
            ref1 = DrvCmxDmaCreateTransactionFullOptions(id1, task1,
                 (u8 *)mvTensorParam->input->data + sizeof(fp16)*i,
                 (u8 *)mvTensorParam->output->data + i * sizeof(fp16) * mvTensorParam->input->dimX * mvTensorParam->input->dimY,
                 sizeof(fp16) * mvTensorParam->input->dimX * mvTensorParam->input->dimY,
                 sizeof(fp16),
                 sizeof(fp16),
                 inputStride,
                 sizeof(fp16));
            DrvCmxDmaStartListTask(ref1);
            DrvCmxDmaWaitTask(ref1);
        }
#endif
    }


    if(mvTensorParam->op->type== kReshape)
    {
        MVT_DPRINTF("Reshape \n");

        reshape((fp16*)mvTensorParam->input->data, (fp16*)mvTensorParam->output->data,
                 mvTensorParam->input->dimX, mvTensorParam->input->dimY,
                 mvTensorParam->output->dimX, mvTensorParam->output->dimY, mvTensorParam->output->dimZ,
                 inputStride, outputStride,
                 mvTensorParam->myriadResources);
    }

    // This is here for testing purposes using t33_elu
    // Fathom does not allow for operations that have parameters,
    // hence ELU is a post-operation.
    if(mvTensorParam->op->type== kElu)
    {
        MVT_DPRINTF("ELU\n");
        MVT_DPRINTF("ELU DIMS: %i %i %i\n",mvTensorParam->output->dimZ,
               mvTensorParam->output->dimX * mvTensorParam->output->dimY,
               mvTensorParam->output->dimXStride/sizeof(half));

        eluFp16((fp16 *)mvTensorParam->input->data,
            (fp16 *)mvTensorParam->output->data,
            mvTensorParam->output->dimZ,
            mvTensorParam->output->dimX * mvTensorParam->output->dimY,
            mvTensorParam->output->dimXStride/sizeof(half),
            mvTensorParam->op->opX, // opX = alpha
            mvTensorParam->myriadResources);
    }

    if(mvTensorParam->op->type == kPower)
    {
        powerFp16((fp16 *)mvTensorParam->input->data,
                (fp16 *)mvTensorParam->output->data,
                mvTensorParam->input->dimZ,
                mvTensorParam->input->dimX * mvTensorParam->input->dimY,
                mvTensorParam->input->dimXStride/sizeof(half),
                mvTensorParam->op->params,
                mvTensorParam->myriadResources,
                g_optimisations & OPT_POWER);
    }

    if(mvTensorParam->op->type== kCrop)
    {
        MVT_DPRINTF("CROP\n");

        crop(mvTensorParam);
    }

    if(mvTensorParam->postOp != NULL)
    {
        if(mvTensorParam->postOp->type == kBias && mvTensorParam->op->type != kScale)
        {
            MVT_DPRINTF("BIAS\n");

            biasFp16((fp16 *)mvTensorParam->output->data,
                (fp16 *)mvTensorParam->biases->data,
                mvTensorParam->output->dimZ,
                mvTensorParam->output->dimX * mvTensorParam->output->dimY,
                mvTensorParam->output->dimXStride/sizeof(half),
                mvTensorParam->myriadResources);
        }

        if(mvTensorParam->postOp->type== kRelu || mvTensorParam->postOp->type == kReluX)
        {
            if(mvTensorParam->postOp->type == kRelu)
            {
                MVT_DPRINTF("RELU0\n");
            }else{
                MVT_DPRINTF("RELU-X0\n");
            }

            float x_val = mvTensorParam->postOp->opX; //When ReLUX, ceiling - when ReLU, floor.
            //(mvTensorParam->postOp->type == kRelu) ? 0 : mvTensorParam->postOp->opX;

            // todo: really should have some mvTensorAsserts here to check the dimensions of the bias vector
            unsigned int bias_addr = (mvTensorParam->biases == NULL)  ? 0 : (u32)mvTensorParam->biases->data;

            MVT_DPRINTF("ReLU DIMS: %x %x %i %i %i %f\n",
                   (unsigned int)mvTensorParam->output->data,
                   bias_addr,
                   mvTensorParam->output->dimZ,
                   mvTensorParam->output->dimX * mvTensorParam->output->dimY,
                   mvTensorParam->output->dimXStride/sizeof(half),
                   x_val);

            if(x_val == 0 || mvTensorParam->postOp->type == kReluX){
                reluFp16((fp16 *)mvTensorParam->output->data,
                (fp16 *)bias_addr,
                mvTensorParam->output->dimZ,
                mvTensorParam->output->dimX * mvTensorParam->output->dimY,
                mvTensorParam->output->dimXStride/sizeof(half),
                x_val,
                mvTensorParam->myriadResources);
            }else{
//               printf("reluNegSlope x: %f\n", x_val);
                 reluNegSlopeFp16((fp16 *)mvTensorParam->output->data,
                        (fp16 *)bias_addr,
                        mvTensorParam->output->dimZ,
                        mvTensorParam->output->dimX * mvTensorParam->output->dimY,
                        mvTensorParam->output->dimXStride/sizeof(half),
                        x_val,
                        mvTensorParam->myriadResources);
            }
        }

        if(mvTensorParam->postOp->type == kElu)
        {
            MVT_DPRINTF("ELU\n");
            MVT_DPRINTF("ELU alpha = %f\n", mvTensorParam->postOp->opX);
            eluFp16((fp16 *)mvTensorParam->output->data,
                    (fp16 *)mvTensorParam->output->data,
                    mvTensorParam->output->dimZ,
                    mvTensorParam->output->dimX * mvTensorParam->output->dimY,
                    mvTensorParam->output->dimXStride/sizeof(half),
                    mvTensorParam->postOp->opX, // opX = alpha
                    mvTensorParam->myriadResources);
        }
    }

    // Count how many ms were spent in mvTensor function
#if defined(__RTEMS__)
    OsDrvTimerGetElapsedTicks(&timer_data,&cyclesElapsed);
    u32 clocksPerUs;
    OsDrvCprGetSysClockPerUs(&clocksPerUs);
    mvTensorParam->debugInfo->ms = cyclesElapsed /1000.0 / clocksPerUs;
#else
    DrvTimerGetElapsedTicks(&timer_data,&cyclesElapsed);
    mvTensorParam->debugInfo->ms = DrvTimerTicksToMs(cyclesElapsed);
#endif
    return NULL;
}

static u32 checkForErrors(t_MvTensorParam *mvTensorParam){
    u32 errors = 0;


    // Check if input pointer is null
    if(mvTensorParam->input->data == NULL)
    {
        if(mvTensorParam->debugInfo->debugMsg)
          strcat(mvTensorParam->debugInfo->debugMsg, "Err: in pointer NULL\n");
        errors++;
    }

    // Check if output pointer is null
    if(mvTensorParam->output->data == NULL)
    {
        if(mvTensorParam->debugInfo->debugMsg)
          strcat(mvTensorParam->debugInfo->debugMsg, "Err: out pointer NULL\n");
        errors++;
    }

    // Check weights pointer is needed to
    if(mvTensorParam->op->type == kConv ||
       mvTensorParam->op->type == kFC ||
       mvTensorParam->op->type == kDepthConv)
        if(mvTensorParam->weights->data == NULL)
        {
            if(mvTensorParam->debugInfo->debugMsg)
              strcat(mvTensorParam->debugInfo->debugMsg, "Err: Weights pointer NULL\n");
            errors++;
        }

    // Check shave first and last shave order
    if(mvTensorParam->myriadResources->firstShave > mvTensorParam->myriadResources->lastShave)
    {
        if(mvTensorParam->debugInfo->debugMsg)
          strcat(mvTensorParam->debugInfo->debugMsg, "Err: firstShave > lastShave\n");
        errors++;
    }

    if(mvTensorParam->postOp != NULL)
        if((mvTensorParam->postOp->type != kRelu) &&
           (mvTensorParam->postOp->type != kReluX) &&
           (mvTensorParam->postOp->type != kBias) &&
           (mvTensorParam->postOp->type != kNone0) &&
           (mvTensorParam->postOp->type != kElu)
           )
        {
           if(mvTensorParam->debugInfo->debugMsg)
             strcat(mvTensorParam->debugInfo->debugMsg, "Err: PostOp not supported!\n");
        }

    if(mvTensorParam->op->type == kConv)
        return errors;
    if(mvTensorParam->op->type == kDepthConv)
        return errors;
    if(mvTensorParam->op->type == kMaxPool)
        return errors;
    if(mvTensorParam->op->type == kAvgPool)
        return errors;
    if(mvTensorParam->op->type == kReshape)
    {
        if(mvTensorParam->input->dimX * mvTensorParam->input->dimY * mvTensorParam->input->dimZ !=
           mvTensorParam->output->dimX * mvTensorParam->output->dimY * mvTensorParam->output->dimZ)
        {
            if(mvTensorParam->debugInfo->debugMsg)
                strcat(mvTensorParam->debugInfo->debugMsg,
                        "Err: invalid reshape parameters (input size != output size)\n");
            errors++;
        }
        return errors;
    }
    if(mvTensorParam->op->type == kSoftMax)
        return errors;
    if(mvTensorParam->op->type == kFC)
        return errors;
    if(mvTensorParam->op->type == kPRelu)
        return errors;
    if(mvTensorParam->op->type == kLRN)
        return errors;
    if(mvTensorParam->op->type >= kSum && mvTensorParam->op->type <= kMax)
        return errors;
    if(mvTensorParam->op->type == kScale)
        return errors;
    if(mvTensorParam->op->type == kRelayout)
        return errors;
    if(mvTensorParam->op->type == kSquare)
        return errors;
    if(mvTensorParam->op->type == kInnerLRN)
        return errors;
    if(mvTensorParam->op->type == kCopy)
        return errors;
    if(mvTensorParam->op->type == kSigmoid)
        return errors;
    if(mvTensorParam->op->type == kTanh)
        return errors;
    if(mvTensorParam->op->type == kDeconvolution)
        return errors;
    if(mvTensorParam->op->type == kElu)
        return errors;
    if(mvTensorParam->op->type == kToPlaneMajor)
        return errors;
    if(mvTensorParam->op->type == kPower)
        return errors;
    if(mvTensorParam->op->type== kCrop)
        return errors;

    if(mvTensorParam->debugInfo->debugMsg)
      strcat(mvTensorParam->debugInfo->debugMsg, "Err: Operation not supported!\n");
    errors++;

    return errors;
}

char ** OptimizationNames()
{
    char ** names = (char **)calloc(sizeof(char*), opt_MAXIMUM_OPTIMIZATIONS);
    for (int i = 0; i != opt_MAXIMUM_OPTIMIZATIONS; i++){
        names[i] = (char *)calloc(sizeof(char), opt_MAXIMUM_NAME_SIZE);
    }
    //ensure these are inserted in the same order as above.

    strcpy(names[opt_conv_3_3_1_1_specific], "opt_conv_3_3_1_1_specific\x7E");
    strcpy(names[opt_conv_im2col], "opt_conv_im2col\x7E");
    strcpy(names[opt_conv_im2col_v2], "opt_conv_im2col_v2\x7E");
    strcpy(names[opt_conv_3_3_2_2_specific], "opt_conv_3_3_2_2_specific\x7E");
    strcpy(names[opt_conv_5_5_1_1_specific], "opt_conv_5_5_1_1_specific\x7E");
    strcpy(names[opt_conv_5_5_2_2_specific], "opt_conv_5_5_2_2_specific\x7E");
    strcpy(names[opt_conv_7_7_2_2_specific], "opt_conv_7_7_2_2_specific\x7E");
    strcpy(names[opt_maxpool_2_2_2_2_specific], "opt_maxpool_2_2_2_2_specific\x7E");
    strcpy(names[opt_maxpool_3_3_1_1_specific], "opt_maxpool_3_3_1_1_specific\x7E");
    strcpy(names[opt_conv_7_7_2_2_spatial], "opt_conv_7_7_2_2_spatial\x7E");
    strcpy(names[opt_conv_generic_spatial], "opt_conv_generic_spatial\x7E");

    strcpy(names[opt_deconv_3_3_1_1_same_specific], "opt_deconv_3_3_1_1_same_specific\x7E");
    strcpy(names[opt_deconv_5_5_1_1_same_specific], "opt_deconv_5_5_1_1_same_specific\x7E");
    strcpy(names[opt_deconv_M_N_1_1_same_spatial], "opt_deconv_M_N_1_1_same_spatial\x7E");
    strcpy(names[opt_deconv_M_N_1_1_same_generic], "opt_deconv_M_N_1_1_same_generic\x7E");
    strcpy(names[opt_power_accurate], "opt_power_accurate\x7E");

    return names;
}
