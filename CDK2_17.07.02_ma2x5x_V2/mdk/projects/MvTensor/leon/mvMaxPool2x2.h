///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Simple effect header
///

#ifndef _MAXPOOL2x2_H_
#define _MAXPOOL2x2_H_

// 1: Includes
// ----------------------------------------------------------------------------
#include <mv_types.h>
#include <mvTensor.h>

#ifdef __cplusplus
extern "C"
{
#endif

// 2:  Exported Global Data (generally better to avoid)
// ----------------------------------------------------------------------------
// 3:  Exported Functions (non-inline)
// ----------------------------------------------------------------------------

void maxPool2x2(fp16* input, u32 channels, u32 height,
        u32 width, u32 paddStyle, u32 stride, u32 pad, u32 ostrideX, fp16* output,
        u32 dmaLinkAgent,
        u32 firstShave, u32 lastShave);

#ifdef __cplusplus
}
#endif

#endif//_MAXPOOL2x2_H_
