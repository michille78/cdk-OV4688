///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Simple effect header
///

#ifndef _ELTWISE_H_
#define _ELTWISE_H_

// 1: Includes
// ----------------------------------------------------------------------------
#include <mv_types.h>
#include <assert.h>

#ifdef __cplusplus
extern "C"
{
#endif

// 2:  Exported Global Data (generally better to avoid)
// ----------------------------------------------------------------------------
// 3:  Exported Functions (non-inline)
// ----------------------------------------------------------------------------

void eltwise(u32 op, fp16* input, u32 channels, u32 height, u32 width,
      fp16* input2, u32 channels2, fp16* output,
      u32 istrideX, u32 i2strideX, u32 ostrideX,
      u32 firstShave, u32 lastShave, u32 dmaLinkAgent);

#ifdef __cplusplus
}
#endif

#endif//_ELTWISE_H_
