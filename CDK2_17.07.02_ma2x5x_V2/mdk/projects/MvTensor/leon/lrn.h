///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Simple effect header
///

#ifndef _LRN_H_
#define _LRN_H_

// 1: Includes
// ----------------------------------------------------------------------------
#include <mv_types.h>
#include <assert.h>
#include <matmul_iface.h>
#include <matmul_profile.h>
#include <matmul_leon.h>
#include <matmul.h>


#ifdef __cplusplus
extern "C"
{
#endif

// 2:  Exported Global Data (generally better to avoid)
// ----------------------------------------------------------------------------
// 3:  Exported Functions (non-inline)
// ----------------------------------------------------------------------------

void lrn(fp16* input, u32 channels, u32 height,
                  u32 width, fp16* output,
                  u32 size, fp16 k, fp16 alpha, fp16 beta,
                  u32 firstShave, u32 lastShave,
				  u32 dmaLinkAgent);

#ifdef __cplusplus
}
#endif

#endif//_LRN_H_
