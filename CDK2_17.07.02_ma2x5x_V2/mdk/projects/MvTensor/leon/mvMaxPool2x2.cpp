///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     main leon file
///

// 1: Includes
// ----------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <DrvSvu.h>
#include <DrvTimer.h>
#include <mv_types.h>
#include "mvMaxPool2x2.h"
#include <mvMaxPool2x2Param.h>
#include <mvTensorInternal.h>

// 2:  Source Specific #defines and types  (typedef, enum, struct)
// ----------------------------------------------------------------------------
// 3: Global Data (Only if absolutely necessary)
// ----------------------------------------------------------------------------
t_MvMaxPool2x2Param maxPool2x2Param[MVTENSOR_MAX_SHAVES] NOCACHE;
// 4: Static Local Data
// ----------------------------------------------------------------------------
// 5: Static Function Prototypes
// ----------------------------------------------------------------------------
// 6: Functions Implementation
// ----------------------------------------------------------------------------
void maxPool2x2(fp16* input, u32 channels, u32 height,
        u32 width, u32 paddStyle, u32 stride, u32 pad, u32 ostrideX, fp16* output,
        u32 dmaLinkAgent,
        u32 firstShave, u32 lastShave)
{
    u32 i;
    u32 sliceC; // channels to process per SHAVE
    u32 remainingC;
    u32 offsetIO = 0;
    u32 channelsMaxToProcess;
    // If the channels number is a multiple of 8, group channels in groups of 8
    u32 group = channels % 8 == 0 ? 8 : 1;
    // allocate 7 words extra, in order to allow the Shave function to enlarge the
    // input/output buffers to the next multiple of 8
    u32 max_sliceC = u32((CMX_DATA_SIZE/INPUT_BPP/2 - 7)/(3*(width + 2*pad + 2))) / group;
    u32 shaves_no =(lastShave - firstShave + 1);

    channelsMaxToProcess = channels / group;

    do{
        sliceC = channelsMaxToProcess / shaves_no;
        if(sliceC > max_sliceC)
        {
            sliceC = max_sliceC;
            remainingC = 0;
            channelsMaxToProcess -= sliceC * shaves_no;
        }
        else
        {
            remainingC = channelsMaxToProcess % shaves_no;
            channelsMaxToProcess = 0;
        }
        u32 at_leastC = sliceC;
        for (i = firstShave; i <= lastShave; i++)
        {
            sliceC = at_leastC;
            if(remainingC)
            {
                sliceC++;
                remainingC--;
            }

            if (sliceC == 0)
                break;

            //initialize MaxPool2x2 SHAVE param
            maxPool2x2Param[i].input    = input + offsetIO;
            maxPool2x2Param[i].channels = channels;
            maxPool2x2Param[i].sliceC   = sliceC * group;
            maxPool2x2Param[i].ostrideX = ostrideX;
            maxPool2x2Param[i].height   = height;
            maxPool2x2Param[i].width    = width;
            maxPool2x2Param[i].stride   = stride;
            maxPool2x2Param[i].pad      = pad;
            maxPool2x2Param[i].cmxslice = getCMXSliceDataSection(i);
            maxPool2x2Param[i].dmaLinkAgent = dmaLinkAgent;
            maxPool2x2Param[i].paddStyle    = paddStyle;
            maxPool2x2Param[i].output       = output + offsetIO;

            u32 readBackAddr = (u32)(&(maxPool2x2Param[i].output));
            GET_REG_WORD_VAL(readBackAddr);

            startShave(i, (u32)&MODULE_ENTRY(mvMaxPool2x2), (u32)&maxPool2x2Param[i]);

            offsetIO += sliceC * group;
        }
    
        waitShaves(firstShave, lastShave);

    }while(channelsMaxToProcess > 0);
}
