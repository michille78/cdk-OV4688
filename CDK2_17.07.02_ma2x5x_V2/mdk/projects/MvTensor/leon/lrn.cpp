///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     main leon file
///

// 1: Includes
// ----------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <DrvSvu.h>
#include <DrvTimer.h>
#include <DrvLeonL2C.h>
#include <mv_types.h>
#include "lrn.h"
#include "lrnParam.h"
#include <mvTensorInternal.h>

// 2:  Source Specific #defines and types  (typedef, enum, struct)
// ----------------------------------------------------------------------------
//#define BENCHMARK

// 3: Global Data (Only if absolutely necessary)
// ----------------------------------------------------------------------------
// 4: Static Local Data
// ----------------------------------------------------------------------------
// 5: Static Function Prototypes
// ----------------------------------------------------------------------------
// 5: Static Function Prototypes
// ----------------------------------------------------------------------------
t_MvLRNParam lrnParam[MVTENSOR_MAX_SHAVES] NOCACHE;

// 6: Functions Implementation
// ----------------------------------------------------------------------------

void lrn(fp16* input, u32 channels, u32 height,
                  u32 width, fp16* output,
                  u32 size, fp16 k, fp16 alpha, fp16 beta,
                  u32 firstShave, u32 lastShave,
				  u32 dmaLinkAgent)

{
#ifdef BENCHMARK
    tyTimeStamp t1, t2, t3, t4;
#endif
    u32 nShaves = lastShave - firstShave + 1;

    // Divide between shaves
#ifdef BENCHMARK
    DrvTimerStartTicksCount(&t1);
#endif
    u32 nlines_per_shave = (width * height + nShaves - 1) / nShaves;
    for(u32 i = 0; i < nShaves; i++)
    {
        // consider data as a 2D matrix, with num_cols=channels and num_lines=W*H
        lrnParam[i + firstShave].input = input + i * nlines_per_shave * channels;
        lrnParam[i + firstShave].output = output + i * nlines_per_shave * channels;
        lrnParam[i + firstShave].numlines = i == nShaves - 1 ?
            width * height - i * nlines_per_shave  : nlines_per_shave;
        lrnParam[i + firstShave].channels = channels;
        lrnParam[i + firstShave].size = size;
        lrnParam[i + firstShave].k = k;
        lrnParam[i + firstShave].alpha = alpha;
        lrnParam[i + firstShave].beta = beta;
        lrnParam[i + firstShave].stage = 0;  // Square input to output
	lrnParam[i + firstShave].dmaLinkAgent = dmaLinkAgent;
	lrnParam[i + firstShave].cmxslice = (half *)getCMXSliceDataSection(firstShave + i);

        u32 readBackAddr = (u32)(&(lrnParam[i + firstShave].cmxslice));
        GET_REG_WORD_VAL(readBackAddr);

        startShave(i + firstShave, (u32)&MODULE_ENTRY(mvLRN), (u32)&lrnParam[i + firstShave]);
    }
    waitShaves(firstShave, lastShave);

#ifdef BENCHMARK
    DrvTimerStartTicksCount(&t2);
    printf("Times: %f\n", (t2-t1)/600000.0);
#endif
    return;

}
