///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     main leon file
///

// 1: Includes
// ----------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <DrvSvu.h>
#include <DrvTimer.h>
#include <mv_types.h>
#include "mvAvgPoolMxN.h"
#include <mvAvgPoolMxNParam.h>
#include <mvTensorInternal.h>

// 2:  Source Specific #defines and types  (typedef, enum, struct)
// ----------------------------------------------------------------------------

// 3: Global Data (Only if absolutely necessary)
// ----------------------------------------------------------------------------
t_MvAvgPoolMxNParam avgPoolMxNParam[MVTENSOR_MAX_SHAVES] NOCACHE;
// 4: Static Local Data
// ----------------------------------------------------------------------------
// 5: Static Function Prototypes
// ----------------------------------------------------------------------------
// 6: Functions Implementation
// ----------------------------------------------------------------------------
void avgPoolMxN(fp16* input, u32 channels, u32 height,
        u32 width, u32 paddStyle, u32 radixX, u32 radixY,
        u32 strideX, u32 strideY, u32 padX, u32 padY,
        u32 ostrideX, fp16* output, u32 dmaLinkAgent,
        u32 firstShave, u32 lastShave)
{
    u32 i;
    u32 sliceC; // channels to process per SHAVE
    u32 remainingC;
    u32 offsetIO = 0;
    u32 channelsMaxToProcess;
    u32 group = channels % 8 == 0 ? 8 : 1;
    // allocate 7 words extra, in order to allow the Shave function to enlarge the
    // input/output buffers to the next multiple of 8
    u32 max_sliceC = u32((CMX_DATA_SIZE/INPUT_BPP/2 - 7) / (width + padX + padY + radixX)) / group;
    u32 shaves_no =(lastShave - firstShave + 1);

    channelsMaxToProcess = channels / group;

    do{
        sliceC = channelsMaxToProcess / shaves_no;
        if(sliceC > max_sliceC)
        {
            sliceC = max_sliceC;
            remainingC = 0;
            channelsMaxToProcess -= sliceC * shaves_no;
        }
        else
        {
            remainingC = channelsMaxToProcess % shaves_no;
            channelsMaxToProcess = 0;
        }
        u32 at_leastC = sliceC;
        for (i = firstShave; i <= lastShave; i++)
        {
            sliceC = at_leastC;
            if(remainingC)
            {
                sliceC++;
                remainingC--;
            }

            if (sliceC == 0)
                break;

            //initialize AvgPoolMxN SHAVE param
            avgPoolMxNParam[i].input    = input + offsetIO;
            avgPoolMxNParam[i].channels = channels;
            avgPoolMxNParam[i].sliceC   = sliceC * group;
            avgPoolMxNParam[i].height   = height;
            avgPoolMxNParam[i].width    = width;
            avgPoolMxNParam[i].strideX  = strideX;
            avgPoolMxNParam[i].strideY  = strideY;
            avgPoolMxNParam[i].ostrideX = ostrideX;
            avgPoolMxNParam[i].radixX   = radixX;
            avgPoolMxNParam[i].radixY   = radixY;
            avgPoolMxNParam[i].padX     = padX;
            avgPoolMxNParam[i].padY     = padY;
            avgPoolMxNParam[i].cmxslice = getCMXSliceDataSection(i);
            avgPoolMxNParam[i].dmaLinkAgent = dmaLinkAgent;
            avgPoolMxNParam[i].paddStyle = paddStyle;
            avgPoolMxNParam[i].output   = output + offsetIO;

            u32 readBackAddr = (u32)(&(avgPoolMxNParam[i].output));
            GET_REG_WORD_VAL(readBackAddr);

            startShave(i, (u32)&MODULE_ENTRY(mvAvgPoolMxN), (u32)&avgPoolMxNParam[i]);

            offsetIO += sliceC * group;
        }
        waitShaves(firstShave, lastShave);
        
    }while(channelsMaxToProcess > 0);
}
