///
/// \file
/// \copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///

#ifndef _MV_CROP_H_
#define _MV_CROP_H_

#include <mvTensor.h>
#include "mvCropParam.h"

// The crop layer is LEON side only because
// it requires only ddr-to-ddr DMA transfers.
void crop(t_MvTensorParam *tensor_param);

#endif /* _MV_CROP_H_ */
