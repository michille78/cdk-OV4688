///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     main leon file
///

// 1: Includes
// ----------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>

#include <DrvSvu.h>
#include <DrvTimer.h>
#include <DrvShaveL2Cache.h>
#include <mv_types.h>

#include <mvConv7x7s2Param.h>
#include <mvTensorInternal.h>
#include "mvConv7x7s2.h"

#if defined(__RTEMS__)
#include <OsDrvShaveL2Cache.h>
#endif

// 2:  Source Specific #defines and types  (typedef, enum, struct)
// ----------------------------------------------------------------------------
// 3: Global Data (Only if absolutely necessary)
// ----------------------------------------------------------------------------
t_MvConv7x7s2Param conv7x7s2Param[MVTENSOR_MAX_SHAVES] NOCACHE;
// 4: Static Local Data
// ----------------------------------------------------------------------------
// 5: Static Function Prototypes
// ----------------------------------------------------------------------------
// 6: Functions Implementation
// ----------------------------------------------------------------------------
// 6: Functions Implementation
// ----------------------------------------------------------------------------

void conv7x7s2(fp16* input, u32 in_height, u32 in_width,
               u32 in_channels, u32 out_channels,
               fp16* weights, fp16* output,
               t_MvTensorMyriadResources *myriadResources) {
    u32 shaves_no = (myriadResources->lastShave - myriadResources->firstShave + 1);
    u32 out_height = in_height / 2;
    // u32 out_width = in_width / 2;
    // How to Parallelize? Ans: each SHAVE is assigned
    // a range of output rows (start_out_row + out_rows)
    u32 start_out_row = 0;
    u32 out_rows = out_height / shaves_no;
    u32 remaining_rows = out_height - out_rows * shaves_no;
    s32 i;
    for (i = myriadResources->firstShave; i <= myriadResources->lastShave; i++) {
        out_rows =  out_height / shaves_no;
        if (remaining_rows) {
          remaining_rows--;
          out_rows++;
        }
        conv7x7s2Param[i].input = input;
        conv7x7s2Param[i].in_height = in_height;
        conv7x7s2Param[i].in_width = in_width;
        conv7x7s2Param[i].in_channels = in_channels;
        conv7x7s2Param[i].out_channels = out_channels;
        conv7x7s2Param[i].start_out_row = start_out_row;
        conv7x7s2Param[i].out_rows = out_rows;
        conv7x7s2Param[i].weights = weights;
        conv7x7s2Param[i].output = output;
        conv7x7s2Param[i].dmaLinkAgent = myriadResources->dmaLinkAgent;
        conv7x7s2Param[i].cmxslice = getCMXSliceDataSection(i);

        u32 readBackAddr = (u32)(&(conv7x7s2Param[i].cmxslice));
        GET_REG_WORD_VAL(readBackAddr);

        startShave(i, (u32)&MODULE_ENTRY(mvConv7x7s2), (u32)&conv7x7s2Param[i]);

        start_out_row += out_rows;
    }

    waitShaves(myriadResources->firstShave, myriadResources->lastShave);

#if defined(__RTEMS__)
    OsDrvShaveL2CachePartitionFlush(myriadResources->dataPartitionNo, PERFORM_INVALIDATION);
#else
    DrvShaveL2CachePartitionFlushAndInvalidate(myriadResources->dataPartitionNo);
#endif
}
