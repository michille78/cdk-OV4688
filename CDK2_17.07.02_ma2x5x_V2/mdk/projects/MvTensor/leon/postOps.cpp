///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief Leon wrappers for CNN post operations.
///

// 1: Includes
// ----------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <DrvSvu.h>
#include <DrvTimer.h>
#include <DrvShaveL2Cache.h>
#include <mv_types.h>

#include "postOps.h"
#include "mvPostOpsParam.h"
#include <mvTensorInternal.h>

#if defined(__RTEMS__)
#include <OsDrvShaveL2Cache.h>
#endif

// 2:  Source Specific #defines and types  (typedef, enum, struct)
// ----------------------------------------------------------------------------
// 3: Global Data (Only if absolutely necessary)
// ----------------------------------------------------------------------------
// 4: Static Local Data
// ----------------------------------------------------------------------------
t_PostOpsParams postOpsParams[MVTENSOR_MAX_SHAVES] NOCACHE;

// 5: Static Function Prototypes
// ----------------------------------------------------------------------------
// 6: Functions Implementation
// ----------------------------------------------------------------------------

void postOps(fp16 *input, fp16 *output, fp16 *weights, fp16 *bias, u32 width,
        u32 height, u32 stride, float x, void *params,
        t_MvTensorMyriadResources *myriadResources, PostOpType postOpType)
{
    int shave_i;
    u32 no_shaves = myriadResources->lastShave - myriadResources->firstShave + 1;
    u32 h_per_shave = height / no_shaves;
    u32 h_per_shave_remainder = height % no_shaves;
    s32 in_out_offset = 0;

    // Nothing to process.
    if(width == 0)
        return;
    for(shave_i = myriadResources->firstShave; shave_i <= myriadResources->lastShave; ++shave_i)
    {
        postOpsParams[shave_i].input        = (input  + in_out_offset);
        postOpsParams[shave_i].output       = (output + in_out_offset);
        postOpsParams[shave_i].weights      = weights;
        postOpsParams[shave_i].bias         = bias;
        postOpsParams[shave_i].width        = width;
        postOpsParams[shave_i].height       = h_per_shave;
        postOpsParams[shave_i].stride       = stride;
        postOpsParams[shave_i].cmxslice     = getCMXSliceDataSection(shave_i);
        postOpsParams[shave_i].dmaLinkAgent = myriadResources->dmaLinkAgent;
        postOpsParams[shave_i].postOpType   = postOpType;
        postOpsParams[shave_i].x            = x;
        postOpsParams[shave_i].params       = params;

        // Distribute one line of width to the first h_per_shave_remainder shave.
        in_out_offset += h_per_shave * stride;

        if(h_per_shave_remainder != 0)
        {
            postOpsParams[shave_i].height += 1;
            in_out_offset += stride;
            --h_per_shave_remainder;
        }

        u32 readBackAddr = (u32)(&(postOpsParams[shave_i].x));
        GET_REG_WORD_VAL(readBackAddr);

        startShave(shave_i, (u32)&MODULE_ENTRY(postOps_core), (u32)&postOpsParams[shave_i]);
    }

    waitShaves(myriadResources->firstShave, myriadResources->lastShave);

#if defined(__RTEMS__)
    OsDrvShaveL2CachePartitionFlush(myriadResources->dataPartitionNo, PERFORM_INVALIDATION);
#else
    DrvShaveL2CachePartitionFlushAndInvalidate(myriadResources->dataPartitionNo);
#endif
}

void biasFp16(fp16 *input, fp16 *bias, u32 width,
        u32 height, u32 stride,
        t_MvTensorMyriadResources *myriadResources)
{
    postOps(input, input, 0, bias, width, height, stride, 0.0f,
            NULL, myriadResources, BIAS);
}

void reluFp16(fp16 *input, fp16 *bias, u32 width,
        u32 height, u32 stride, float x,
        t_MvTensorMyriadResources *myriadResources)
{
    postOps(input, input, 0, bias, width,height, stride, x,
            NULL, myriadResources, RELU);
}

void reluNegSlopeFp16(fp16 *input, fp16 *bias, u32 width,
        u32 height, u32 stride, float x,
        t_MvTensorMyriadResources *myriadResources)
{
    postOps(input, input, 0, bias, width,height, stride, x,
            NULL, myriadResources, RELU_NEG_SLOPE);
}

void preluFp16(fp16 *input, fp16 *output, fp16 *weights, u32 width,
        u32 height, u32 stride,  t_MvTensorMyriadResources *myriadResources)
{
    postOps(input, output, weights, 0, width, height, stride, 0.0f,
            NULL, myriadResources, PRELU);
}

void scaleFp16(fp16 *input, fp16 *output, fp16 *weights, fp16 *bias, u32 width,
        u32 height, u32 stride,
        t_MvTensorMyriadResources *myriadResources)
{
    postOps(input, output, weights, bias, width, height, stride, 0.0f,
            NULL, myriadResources, SCALE);
}

void squareFp16(fp16 *input, fp16 *output, u32 width,
        u32 height, u32 stride,
        t_MvTensorMyriadResources *myriadResources)
{
    postOps(input, output, 0, 0, width, height, stride, 0.0f,
            NULL, myriadResources, SQUARE);
}

void innerLRNFp16(fp16 *input, fp16 *output, fp16 alpha, fp16 beta, u32 width,
        u32 height, u32 stride,
        t_MvTensorMyriadResources *myriadResources)
{
    postOps(input, output, &alpha, &beta, width, height, stride, 0.0f,
            NULL, myriadResources, INNERLRN);
}

void sigmoidFp16(fp16 *input, fp16 *output, u32 width, u32 height,
        u32 stride, t_MvTensorMyriadResources *myriadResources, bool be_accurate)
{
    if(be_accurate)
        postOps(input, output, 0, 0, width, height, stride, 0.0f,
                NULL, myriadResources, SIGMOID_ACCURATE);
    else
        postOps(input, output, 0, 0, width, height, stride, 0.0f,
                NULL, myriadResources, SIGMOID_FAST);
}

void tanhFp16(fp16 *input, fp16 *output, u32 width, u32 height,
        u32 stride, t_MvTensorMyriadResources *myriadResources, bool be_accurate)
{
    if(be_accurate)
        postOps(input, output, 0, 0, width, height, stride, 0.0f,
                NULL, myriadResources, TANH_ACCURATE);
    else
        postOps(input, output, 0, 0, width, height, stride, 0.0f,
                NULL, myriadResources, TANH_FAST);
}

void powerFp16(fp16 *input, fp16 *output, u32 width, u32 height,
        u32 stride, void *params, t_MvTensorMyriadResources *myriadResources,
        bool be_accurate)
{
    if(be_accurate)
        postOps(input, output, 0, 0, width, height, stride, 0.0f,
                params, myriadResources, POWER_ACCURATE);
    else
        postOps(input, output, 0, 0, width, height, stride, 0.0f,
                params, myriadResources, POWER_FAST);
}

void eluFp16(fp16 *input, fp16 *output, u32 width, u32 height, u32 stride, float alpha,
        t_MvTensorMyriadResources *myriadResources)
{
    postOps(input, output, 0, 0, width, height, stride, alpha,
            NULL, myriadResources, ELU);
}
