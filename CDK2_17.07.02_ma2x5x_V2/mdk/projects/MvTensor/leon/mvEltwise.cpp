///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     main leon file
///

// 1: Includes
// ----------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <OsDrvTimer.h>
#include <DrvSvu.h>
#include <DrvTimer.h>
#include <DrvShaveL2Cache.h>
#include <mv_types.h>
#include <OsDrvTimer.h>
#include "mvEltwise.h"
#include "mvEltwiseParam.h"
#include <mvTensorInternal.h>

// 2:  Source Specific #defines and types  (typedef, enum, struct)
// ----------------------------------------------------------------------------
// 3: Global Data (Only if absolutely necessary)
// ----------------------------------------------------------------------------
// 4: Static Local Data
// ----------------------------------------------------------------------------
// 5: Static Function Prototypes
// ----------------------------------------------------------------------------
// 5: Static Function Prototypes
// ----------------------------------------------------------------------------
// 6: Functions Implementation
// ----------------------------------------------------------------------------
t_MvEltwiseParam param[MVTENSOR_MAX_SHAVES] NOCACHE;

void eltwise(u32 op, fp16* input, u32 channels, u32 height, u32 width,
      fp16* input2, u32 channels2, fp16* output,
      u32 istrideX, u32 i2strideX, u32 ostrideX,
      u32 firstShave, u32 lastShave, u32 dmaLinkAgent)
{
    u32 nShaves = lastShave - firstShave + 1;
    // Divide between shaves
    u32 nelem = (width * height + nShaves - 1) / nShaves;
    for(u32 i = 0; i < nShaves; i++)
    {
        // Square input and multiply by band matrix
        param[i + firstShave].op = op == kProd ? Eltwise_prod : op == kMax ? Eltwise_max : Eltwise_sum;
        param[i + firstShave].input = (half *)((u8 *)input + i * nelem * istrideX);
        param[i + firstShave].channels = channels;
        param[i + firstShave].channels2 = channels2;
        param[i + firstShave].input2 = (half *)((u8 *)input2 + i * nelem * i2strideX);
        param[i + firstShave].output = (half *)((u8 *)output + i * nelem * ostrideX);
        param[i + firstShave].nelements = (i+1) * nelem > width * height ? width * height - i*nelem : nelem;
        param[i + firstShave].istrideX = istrideX;
        param[i + firstShave].i2strideX = i2strideX;
        param[i + firstShave].ostrideX = ostrideX;
        param[i + firstShave].dmaLinkAgent = dmaLinkAgent;
        param[i + firstShave].cmxslice = (half *)getCMXSliceDataSection(firstShave + i);

        u32 readBackAddr = (u32)(&(param[i + firstShave].cmxslice));
        GET_REG_WORD_VAL(readBackAddr);

        startShave(firstShave + i, (u32)&MODULE_ENTRY(mvEltwise), (u32)&param[i + firstShave]);
        if(param[i + firstShave].nelements + i*nelem == width*height)
        {
            lastShave = firstShave + i;
            break;
        }
    }

    waitShaves(firstShave, lastShave);

    return;

}
