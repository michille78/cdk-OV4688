///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Simple effect header
///

#ifndef _AVGPOOL7X7XK_H_
#define _AVGPOOL7X7XK_H_

// 1: Includes
// ----------------------------------------------------------------------------
#include <mv_types.h>
#include <mvTensor.h>

#ifdef __cplusplus
extern "C"
{
#endif

// 2:  Exported Global Data (generally better to avoid)
// ----------------------------------------------------------------------------
// 3:  Exported Functions (non-inline)
// ----------------------------------------------------------------------------

void avgPool7x7xk(fp16* input, u32 channels, fp16* output,
        u32 dmaLinkAgent,
        u32 firstShave, u32 lastShave);

#ifdef __cplusplus
}
#endif

#endif//_AVGPOOL7X7XK_H_
