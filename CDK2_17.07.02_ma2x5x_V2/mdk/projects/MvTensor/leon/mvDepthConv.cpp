///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     main leon file
///

// 1: Includes
// ----------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <DrvSvu.h>
#include <DrvTimer.h>
#include <DrvShaveL2Cache.h>
#include <mv_types.h>

#include "mvDepthConv.h"
#include <mvDepthConvParam.h>
#include <mvTensorInternal.h>

#if defined(__RTEMS__)
#include <OsDrvShaveL2Cache.h>
#endif

#define MIN(a, b) ((a)<(b)?(a):(b))


// Start counting ticks
tyTimeStamp timer_data;
u64 cyclesElapsed;

// 2:  Source Specific #defines and types  (typedef, enum, struct)
// ----------------------------------------------------------------------------
// 3: Global Data (Only if absolutely necessary)
// ----------------------------------------------------------------------------
t_MvDepthConvParam depthConvParam[MVTENSOR_MAX_SHAVES] NOCACHE;
// 4: Static Local Data
// ----------------------------------------------------------------------------
// 5: Static Function Prototypes
// ----------------------------------------------------------------------------
// 6: Functions Implementation
// ----------------------------------------------------------------------------
// 6: Functions Implementation
// ----------------------------------------------------------------------------

void depthConv(fp16* input, u32 in_height, u32 in_width,
        u32 in_channels, fp16* weights, u32 filter_height, u32 filter_width,
        u32 stride_h, u32 stride_w, u32 channel_multiplier, fp16* output,
        u32 out_height, u32 out_width,
        t_MvTensorMyriadResources *myriadResources)
{
    int i;
    u32 sliceK; // channels to process per SHAVE
    u32 shaves_no =(myriadResources->lastShave - myriadResources->firstShave + 1);
    u32 maxKToProcess = (in_channels + shaves_no - 1) / shaves_no;
    u32 offsetK = 0;

    for (i = myriadResources->firstShave; i <= myriadResources->lastShave; i++) {
        sliceK = MIN(maxKToProcess, in_channels-offsetK);
        if (sliceK == 0)
            break;

        depthConvParam[i].input = input + offsetK;
        depthConvParam[i].in_height = in_height;
        depthConvParam[i].in_width = in_width;
        depthConvParam[i].in_channels = in_channels;
        depthConvParam[i].in_channels_slice = sliceK;
        depthConvParam[i].weights = weights + offsetK * channel_multiplier;
        depthConvParam[i].filter_height = filter_height;
        depthConvParam[i].filter_width = filter_width;
        depthConvParam[i].stride_h = stride_h;
        depthConvParam[i].stride_w = stride_w;
        depthConvParam[i].channel_multiplier = channel_multiplier;
        depthConvParam[i].output = output + offsetK * channel_multiplier;
        depthConvParam[i].out_height = out_height;
        depthConvParam[i].out_width = out_width;
        depthConvParam[i].dmaLinkAgent = myriadResources->dmaLinkAgent;
        depthConvParam[i].cmxslice = getCMXSliceDataSection(i);

        u32 readBackAddr = (u32)(&(depthConvParam[i].cmxslice));
        GET_REG_WORD_VAL(readBackAddr);

        startShave(i, (u32)&MODULE_ENTRY(mvDepthConv), (u32)&depthConvParam[i]);

        offsetK += sliceK;
    }

    waitShaves(myriadResources->firstShave, myriadResources->lastShave);

#if defined(__RTEMS__)
    OsDrvShaveL2CachePartitionFlush(myriadResources->dataPartitionNo, PERFORM_INVALIDATION);
#else
    DrvShaveL2CachePartitionFlushAndInvalidate(myriadResources->dataPartitionNo);
#endif
}
