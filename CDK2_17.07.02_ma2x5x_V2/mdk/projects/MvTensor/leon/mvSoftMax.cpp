///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     main leon file
///

// 1: Includes
// ----------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <DrvSvu.h>
#include <DrvTimer.h>
#include <mv_types.h>
#include "mvSoftMax.h"
#include <mvSoftMaxParam.h>
#include <mvTensorInternal.h>
#include <DrvShaveL2Cache.h>

// 2:  Source Specific #defines and types  (typedef, enum, struct)
// ----------------------------------------------------------------------------
// 3: Global Data (Only if absolutely necessary)
// ----------------------------------------------------------------------------
// 4: Static Local Data
// ----------------------------------------------------------------------------
t_MvSoftMaxParam softMaxParam[MVTENSOR_MAX_SHAVES] NOCACHE;

// 5: Static Function Prototypes
// ----------------------------------------------------------------------------
// 6: Functions Implementation
// ----------------------------------------------------------------------------
void softMax(fp16* input, u32 classes, fp16* output,
             u32 dmaLinkAgent,
             u32 firstShave, u32 lastShave)
{
    u32 i;
    u32 sliceC; // classes to process per SHAVE
    u32 remainingC;
    u32 offsetIO = 0;
    u32 classesMaxToProcess;
    u32 max_sliceC = 41976;
    u32 bufferOverflow = 0;
    u32 shaves_no =(lastShave - firstShave + 1);

    // initialize local max;
    for(i = 0; i < MVTENSOR_MAX_SHAVES; i++)
        {
        // minimum value for half (-65504)
        *((u16*)(getCMXSliceDataSection(firstShave) + max_sliceC * 2)+ i) = 0xfbff;
        }
    // initialize local sum;
     for(i = 0; i < MVTENSOR_MAX_SHAVES; i++)
        {
         // minimum value for half (-65504)
         *((u16*)(getCMXSliceDataSection(firstShave) + max_sliceC * 2 + MVTENSOR_MAX_SHAVES * 2)+ i) = 0;
         }

    classesMaxToProcess = classes;

    do{
        sliceC = classesMaxToProcess / shaves_no;
        if(sliceC > max_sliceC)
            {
                sliceC = max_sliceC;
                remainingC = 0;
                classesMaxToProcess -= sliceC * shaves_no;
                if(classesMaxToProcess > 0)
                    bufferOverflow = 1;
            }
        else
            {
            remainingC = classesMaxToProcess % shaves_no;
            classesMaxToProcess = 0;
            }

    // stage 1 ; get local max
    u32 at_leastC = sliceC;
    for (i = firstShave; i <= lastShave; i++)
    {
        sliceC = at_leastC;
        if(remainingC)
        {
            sliceC++;
            remainingC--;
        }
        if(sliceC == 0)
            break;
        //initialize SoftMax SHAVE param
        softMaxParam[i].input = input + offsetIO;
        softMaxParam[i].sliceC = sliceC;
        softMaxParam[i].cmxslice = getCMXSliceDataSection(i);
        softMaxParam[i].dmaLinkAgent = dmaLinkAgent;
        softMaxParam[i].stage = 1;
        softMaxParam[i].cmxslice0 =  getCMXSliceDataSection(firstShave);
        softMaxParam[i].offset = 83952 + 2 * i;
        softMaxParam[i].bufferOverflow = bufferOverflow;
        softMaxParam[i].output = output + offsetIO;

        startShave(i, (u32)&MODULE_ENTRY(mvSoftMax), (u32)&softMaxParam[i]);
        
        offsetIO += sliceC;
    }
    
    waitShaves(firstShave, lastShave);
    
    }while(classesMaxToProcess > 0);

//    printf("Softmax: A: %x\n",((fp16*)softMaxParam[0].input)[0]);
//    printf("Softmax: B: %x\n",((fp16*)softMaxParam[0].output)[0]);

    // stage 2 ; get global max
    for (i = firstShave; i <= firstShave; i++)
    {
        softMaxParam[i].stage = 2;

        startShave(i, (u32)&MODULE_ENTRY(mvSoftMax), (u32)&softMaxParam[i]);
    }
    
    waitShaves(firstShave, firstShave);

//    printf("Softmax: A: %x\n",((fp16*)softMaxParam[0].input)[0]);
//    printf("Softmax: B: %x\n",((fp16*)softMaxParam[0].output)[0]);


    // stage 3 ; get plusall, expall, local sum
    classesMaxToProcess = classes;
    offsetIO = 0;
    do{
        sliceC = classesMaxToProcess / shaves_no;
        if(sliceC > max_sliceC)
            {
                sliceC = max_sliceC;
                remainingC = 0;
                classesMaxToProcess -= sliceC * shaves_no;
                if(classesMaxToProcess > 0)
                    bufferOverflow = 1;
            }
        else
            {
            remainingC = classesMaxToProcess % shaves_no;
            classesMaxToProcess = 0;
            }

    u32 at_leastC = sliceC;
    for (i = firstShave; i <= lastShave; i++)
    {
        sliceC = at_leastC;
        if(remainingC)
        {
            sliceC++;
            remainingC--;
        }
        if(sliceC == 0)
            break;
        //initialize SoftMax SHAVE param
        softMaxParam[i].input = input + offsetIO;
        softMaxParam[i].sliceC = sliceC;
        softMaxParam[i].cmxslice = getCMXSliceDataSection(i);
        softMaxParam[i].dmaLinkAgent = dmaLinkAgent;
        softMaxParam[i].stage = 3;
        softMaxParam[i].cmxslice0 = getCMXSliceDataSection(firstShave);
        softMaxParam[i].offset = 83952 + 2 * i;
        softMaxParam[i].bufferOverflow = bufferOverflow;
        softMaxParam[i].output = output + offsetIO;

        startShave(i, (u32)&MODULE_ENTRY(mvSoftMax), (u32)&softMaxParam[i]);

        offsetIO += sliceC;
    }
    
    waitShaves(firstShave, lastShave);
    
    }while(classesMaxToProcess > 0);

//    printf("Softmax: A: %x\n",((fp16*)softMaxParam[0].input)[0]);
//    printf("Softmax: B: %x\n",((fp16*)softMaxParam[0].output)[0]);


    // stage 4 ; get global sum
    for (i = firstShave; i <= firstShave; i++)
        {
            softMaxParam[i].stage = 4;

            startShave(i, (u32)&MODULE_ENTRY(mvSoftMax), (u32)&softMaxParam[i]);
        }
     waitShaves(firstShave, firstShave);

     // stage 5 ; get softmax
     classesMaxToProcess = classes;
     offsetIO = 0;
         do{
             sliceC = classesMaxToProcess / shaves_no;
             if(sliceC > max_sliceC)
                 {
                     sliceC = max_sliceC;
                     remainingC = 0;
                     classesMaxToProcess -= sliceC * shaves_no;
                     if(classesMaxToProcess > 0)
                         bufferOverflow = 1;
                 }
             else
                 {
                 remainingC = classesMaxToProcess % shaves_no;
                 classesMaxToProcess = 0;
                 }

         u32 at_leastC = sliceC;
         for (i = firstShave; i <= lastShave; i++)
         {
             sliceC = at_leastC;
             if(remainingC)
             {
                 sliceC++;
                 remainingC--;
             }
             if(sliceC == 0)
                 break;
             //initialize SoftMax SHAVE param
             softMaxParam[i].input = input + offsetIO;
             softMaxParam[i].sliceC = sliceC;
             softMaxParam[i].cmxslice = getCMXSliceDataSection(i);
             softMaxParam[i].dmaLinkAgent = dmaLinkAgent;
             softMaxParam[i].stage = 5;
             softMaxParam[i].cmxslice0 = getCMXSliceDataSection(firstShave);
             softMaxParam[i].offset = 83952 + 2 * i;
             softMaxParam[i].bufferOverflow = bufferOverflow;
             softMaxParam[i].output = output + offsetIO;

             u32 readBackAddr = (u32)(&( softMaxParam[i].output));
             GET_REG_WORD_VAL(readBackAddr);

             startShave(i, (u32)&MODULE_ENTRY(mvSoftMax), (u32)&softMaxParam[i]);

             offsetIO += sliceC;
         }
         
         waitShaves(firstShave, lastShave);
         
         }while(classesMaxToProcess > 0);
//
//         printf("Softmax: A: %x\n",((fp16*)softMaxParam[0].input)[0]);
//         printf("Softmax: B: %x\n",((fp16*)softMaxParam[0].output)[0]);
}
