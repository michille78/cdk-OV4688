///
/// \file
/// \copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///

#ifndef _MV_POSTOPS_H_
#define _MV_POSTOPS_H_

#include <mvTensor.h>
#include "mvPostOpsParam.h"

void postOps(fp16 *input, fp16 *output, fp16 *weights, fp16 *bias, u32 width,
        u32 height, u32 stride, void *params,
        t_MvTensorMyriadResources *myriadResources, PostOpType postOpType);

void reluFp16(fp16 *input, fp16 *bias, u32 width,
        u32 height, u32 stride, float x,
        t_MvTensorMyriadResources *myriadResources);

void reluNegSlopeFp16(fp16 *input, fp16 *bias, u32 width,
        u32 height, u32 stride, float x,
        t_MvTensorMyriadResources *myriadResources);

void preluFp16(fp16 *input, fp16 *output, fp16 *weights, u32 width,
        u32 height, u32 stride, t_MvTensorMyriadResources *myriadResources);

void biasFp16(fp16 *input, fp16 *bias, u32 width,
        u32 height, u32 stride, t_MvTensorMyriadResources *myriadResources);

void scaleFp16(fp16 *input, fp16 *output, fp16 *weights, fp16 *bias, u32 width,
        u32 height, u32 stride, t_MvTensorMyriadResources *myriadResources);

void squareFp16(fp16 *input, fp16 *output, u32 width,
        u32 height, u32 stride, t_MvTensorMyriadResources *myriadResources);

void innerLRNFp16(fp16 *input, fp16 *output, fp16 alpha, fp16 beta, u32 width,
        u32 height, u32 stride, t_MvTensorMyriadResources *myriadResources);

void sigmoidFp16(fp16 *input, fp16 *output, u32 width, u32 height,
        u32 stride, t_MvTensorMyriadResources *myriadResources, bool be_accurate = true);

void tanhFp16(fp16 *input, fp16 *output, u32 width, u32 height,
        u32 stride, t_MvTensorMyriadResources *myriadResources, bool be_accurate = true);

void eluFp16(fp16 *input, fp16 *output, u32 width, u32 height, u32 stride, float alpha,
        t_MvTensorMyriadResources *myriadResources);

void powerFp16(fp16 *input, fp16 *output, u32 width, u32 height,
        u32 stride, void *params, t_MvTensorMyriadResources *myriadResources,
        bool be_accurate = false);

#endif /* _MV_POSTOPS_H_ */
