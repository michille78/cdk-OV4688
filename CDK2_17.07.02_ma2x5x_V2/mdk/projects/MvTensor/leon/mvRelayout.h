///
/// \file
/// \copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///

#ifndef _MV_RELAYOUT_H_
#define _MV_RELAYOUT_H_

#include <mvTensor.h>
#include "mvRelayoutParam.h"

// Performs better than v1 for for nonstrided operations (stride == 1 in both dimension)
// and large number of input channels.
void relayout(fp16 *input, fp16 *output, s32 width, s32 height,
        s32 channels, s32 radixX, s32 radixY,
        s32 kernelStrideX, s32 kernelStrideY, t_MvTensorPaddStyle pad_style,
        s32 padX, s32 padY, t_MvRelayoutVersion version,
        t_MvTensorMyriadResources *myriadResources);

void relayout_conv_v0(fp16 *input, fp16 *output, s32 width, s32 height,
        s32 channels, s32 radixX, s32 radixY,
        s32 kernelStrideX, s32 kernelStrideY, t_MvTensorPaddStyle pad_style,
        t_MvTensorMyriadResources *myriadResources);

// Performs better than v0 for for strided operations (stride != 1 in either dimension)
// as well as small number of input channels
void relayout_conv_v1(fp16 *input, fp16 *output, s32 width, s32 height,
        s32 channels, s32 radixX, s32 radixY,
        s32 kernelStrideX, s32 kernelStrideY, t_MvTensorPaddStyle pad_style,
        t_MvTensorMyriadResources *myriadResources);

void relayout_deconv(fp16 *input, fp16 *output, s32 width, s32 height,
        s32 channels, s32 radixX, s32 radixY,
        s32 kernelStrideX, s32 kernelStrideY, t_MvTensorPaddStyle pad_style,
        s32 padX, s32 padY,
        t_MvTensorMyriadResources *myriadResources);

#endif /* _MV_RELAYOUT_H_ */
