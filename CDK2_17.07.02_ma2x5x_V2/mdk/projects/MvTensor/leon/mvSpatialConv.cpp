///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     main leon file
///

// 1: Includes
// ----------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>

#include <DrvSvu.h>
#include <DrvTimer.h>
#include <DrvShaveL2Cache.h>
#include <mv_types.h>

#include <mvSpatialConvParam.h>
#include <mvTensorInternal.h>
#include "mvSpatialConv.h"

#if defined(__RTEMS__)
#include <OsDrvShaveL2Cache.h>
#endif

// 2:  Source Specific #defines and types  (typedef, enum, struct)
// ----------------------------------------------------------------------------
// 3: Global Data (Only if absolutely necessary)
// ----------------------------------------------------------------------------
t_MvSpatialConvParam spatialconvParam[MVTENSOR_MAX_SHAVES] NOCACHE;
// 4: Static Local Data
// ----------------------------------------------------------------------------
// 5: Static Function Prototypes
// ----------------------------------------------------------------------------
// 6: Functions Implementation
// ----------------------------------------------------------------------------
// 6: Functions Implementation
// ----------------------------------------------------------------------------

void spatialconv(fp16* input, u32 in_height, u32 in_width,
               u32 in_channels, u32 out_channels,
               u32 radix_x, u32 radix_y,
               u32 stride_x, u32 stride_y,
               u32 padding_type,
               fp16* weights, fp16* output,
               t_MvTensorMyriadResources *myriadResources) {
    u32 shaves_no = (myriadResources->lastShave - myriadResources->firstShave + 1);
    u32 pad_l, pad_r;
    pad_l = pad_r = padding_type * (radix_y/2);
    u32 out_height = (in_height + pad_l + pad_r - radix_y)/stride_y + 1;
    // How to Parallelize? Ans: each SHAVE is assigned
    // a range of output rows (start_out_row + out_rows)
    u32 start_out_row = 0;
    u32 out_rows = out_height / shaves_no;
    u32 remaining_rows = out_height - out_rows * shaves_no;
    s32 i;
    for (i = myriadResources->firstShave; i <= myriadResources->lastShave; i++) {
        out_rows =  out_height / shaves_no;
        if (remaining_rows) {
          remaining_rows--;
          out_rows++;
        }
        spatialconvParam[i].input = input;
        spatialconvParam[i].in_height = in_height;
        spatialconvParam[i].in_width = in_width;
        spatialconvParam[i].in_channels = in_channels;
        spatialconvParam[i].out_channels = out_channels;
        spatialconvParam[i].radix_x = radix_x;
        spatialconvParam[i].radix_y = radix_y;
        spatialconvParam[i].stride_x = stride_x;
        spatialconvParam[i].stride_y = stride_y;
        spatialconvParam[i].padding_type = padding_type;
        spatialconvParam[i].start_out_row = start_out_row;
        spatialconvParam[i].out_rows = out_rows;
        spatialconvParam[i].weights = weights;
        spatialconvParam[i].output = output;
        spatialconvParam[i].dmaLinkAgent = myriadResources->dmaLinkAgent;
        spatialconvParam[i].cmxslice = getCMXSliceDataSection(i);

        u32 readBackAddr = (u32)(&(spatialconvParam[i].cmxslice));
        GET_REG_WORD_VAL(readBackAddr);

        startShave(i, (u32)&MODULE_ENTRY(mvSpatialConv), (u32)&spatialconvParam[i]);

        start_out_row += out_rows;
    }

    waitShaves(myriadResources->firstShave, myriadResources->lastShave);

#if defined(__RTEMS__)
    OsDrvShaveL2CachePartitionFlush(myriadResources->dataPartitionNo, PERFORM_INVALIDATION);
#else
    DrvShaveL2CachePartitionFlushAndInvalidate(myriadResources->dataPartitionNo);
#endif
}
