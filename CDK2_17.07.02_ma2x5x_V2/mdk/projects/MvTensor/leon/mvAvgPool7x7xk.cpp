///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     main leon file
///

// 1: Includes
// ----------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <DrvSvu.h>
#include <DrvTimer.h>
#include <mv_types.h>
#include "mvAvgPool7x7xk.h"
#include <mvAvgPool7x7xkParam.h>
#include <mvTensorInternal.h>

// 2:  Source Specific #defines and types  (typedef, enum, struct)
// ----------------------------------------------------------------------------
// 3: Global Data (Only if absolutely necessary)
// ----------------------------------------------------------------------------
t_MvAvgPool7x7xkParam avgPool7x7xkParam[MVTENSOR_MAX_SHAVES] NOCACHE;
// 4: Static Local Data
// ----------------------------------------------------------------------------
// 5: Static Function Prototypes
// ----------------------------------------------------------------------------
// 6: Functions Implementation
// ----------------------------------------------------------------------------
void avgPool7x7xk(fp16* input, u32 channels, fp16* output,
             u32 dmaLinkAgent,
             u32 firstShave, u32 lastShave)
{
    u32 i;
    u32 sliceC; // channels to process per SHAVE
    u32 remainingC;
    u32 offsetIO = 0;
    u32 channelsMaxToProcess;
    u32 max_sliceC = 209;
    u32 shaves_no =(lastShave - firstShave + 1);

    channelsMaxToProcess = channels / 4;

    do{
        sliceC = channelsMaxToProcess / shaves_no;
        if(sliceC > max_sliceC)
        {
            sliceC = max_sliceC;
            remainingC = 0;
            channelsMaxToProcess -= sliceC * shaves_no;
        }
        else
        {
            remainingC = channelsMaxToProcess % shaves_no;
            channelsMaxToProcess = 0;
        }
        u32 at_leastC = sliceC;
        for (i = firstShave; i <= lastShave; i++)
        {
            sliceC = at_leastC;
            if(remainingC)
            {
                sliceC++;
                remainingC--;
            }

            //initialize AvgPool7x7xk SHAVE param
            avgPool7x7xkParam[i].input = input + offsetIO;
            avgPool7x7xkParam[i].channels = channels;
            avgPool7x7xkParam[i].sliceC = sliceC * 4;
            avgPool7x7xkParam[i].cmxslice = getCMXSliceDataSection(i);
            avgPool7x7xkParam[i].dmaLinkAgent = dmaLinkAgent;
            avgPool7x7xkParam[i].output = output + offsetIO;

            u32 readBackAddr = (u32)(&(avgPool7x7xkParam[i].output));
            GET_REG_WORD_VAL(readBackAddr);

            startShave(i, (u32)&MODULE_ENTRY(mvAvgPool7x7xk), (u32)&avgPool7x7xkParam[i]);

            offsetIO += sliceC * 4;
        }
        waitShaves(firstShave, lastShave);

    }while(channelsMaxToProcess > 0);
}
