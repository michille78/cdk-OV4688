///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief Crop layer implementation.
///

// 1: Includes
// ----------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <DrvSvu.h>
#include <DrvTimer.h>
#include <DrvShaveL2Cache.h>
#include <DrvCmxDma.h>
#include <mv_types.h>

#include "mvCrop.h"
#include "mvCropParam.h"

// 2:  Source Specific #defines and types  (typedef, enum, struct)
// ----------------------------------------------------------------------------
#define DMA_DESCRIPTORS_SECTION __attribute__((section(".cmx.cdmaDescriptors")))

// 3: Global Data (Only if absolutely necessary)
// ----------------------------------------------------------------------------
// 4: Static Local Data
// ----------------------------------------------------------------------------
// 5: Static Function Prototypes
// ----------------------------------------------------------------------------
// 6: Functions Implementation
// ----------------------------------------------------------------------------

void crop(t_MvTensorParam *mvTensorParam)
{
    static dmaTransactionList_t DMA_DESCRIPTORS_SECTION task1;
    static dmaTransactionList_t *ref1;
    static dmaRequesterId id1;

    t_CropLayerParams * crop_params = reinterpret_cast<t_CropLayerParams *>(mvTensorParam->op->params);

    s32 in_axis0, in_axis1;
    s32 offset_axis0, offset_axis1, offset_axis2;
    s32 out_axis0, out_axis1, out_axis2;

    // Make sure that the crop can be done.
    if((mvTensorParam->output->dimX + crop_params->offset_dimX > mvTensorParam->input->dimX) ||
       (mvTensorParam->output->dimY + crop_params->offset_dimY > mvTensorParam->input->dimY) ||
       (mvTensorParam->output->dimZ + crop_params->offset_dimZ > mvTensorParam->input->dimZ))
    {
        return;
    }

    bool crop_axis0, crop_axis1;

    // input->storageOrder == output->storageOrder is assumed.
    switch(mvTensorParam->input->storageOrder)
    {
    case orderYXZ:
        in_axis0 = mvTensorParam->input->dimZ;
        in_axis1 = mvTensorParam->input->dimX;

        out_axis0 = mvTensorParam->output->dimZ;
        out_axis1 = mvTensorParam->output->dimX;
        out_axis2 = mvTensorParam->output->dimY;

        offset_axis0 = crop_params->offset_dimZ;
        offset_axis1 = crop_params->offset_dimX;
        offset_axis2 = crop_params->offset_dimY;

        break;
    case orderZYX:
        in_axis0 = mvTensorParam->input->dimX;
        in_axis1 = mvTensorParam->input->dimY;

        out_axis0 = mvTensorParam->output->dimX;
        out_axis1 = mvTensorParam->output->dimY;
        out_axis2 = mvTensorParam->output->dimZ;

        offset_axis0 = crop_params->offset_dimX;
        offset_axis1 = crop_params->offset_dimY;
        offset_axis2 = crop_params->offset_dimZ;
        break;
    case orderYZX:
        in_axis0 = mvTensorParam->input->dimX;
        in_axis1 = mvTensorParam->input->dimZ;

        out_axis0 = mvTensorParam->output->dimX;
        out_axis1 = mvTensorParam->output->dimZ;
        out_axis2 = mvTensorParam->output->dimY;

        offset_axis0 = crop_params->offset_dimX;
        offset_axis1 = crop_params->offset_dimZ;
        offset_axis2 = crop_params->offset_dimY;
        break;
    case orderXYZ:
        in_axis0 = mvTensorParam->input->dimZ;
        in_axis1 = mvTensorParam->input->dimY;

        out_axis0 = mvTensorParam->output->dimZ;
        out_axis1 = mvTensorParam->output->dimY;
        out_axis2 = mvTensorParam->output->dimX;

        offset_axis0 = crop_params->offset_dimZ;
        offset_axis1 = crop_params->offset_dimY;
        offset_axis2 = crop_params->offset_dimX;
        break;
    case orderXZY:
        in_axis0 = mvTensorParam->input->dimY;
        in_axis1 = mvTensorParam->input->dimZ;

        out_axis0 = mvTensorParam->output->dimY;
        out_axis1 = mvTensorParam->output->dimZ;
        out_axis2 = mvTensorParam->output->dimX;

        offset_axis0 = crop_params->offset_dimY;
        offset_axis1 = crop_params->offset_dimZ;
        offset_axis2 = crop_params->offset_dimX;
        break;
    default:
        return;
    }

    crop_axis0 = !(in_axis0 == out_axis0);
    crop_axis1 = !(in_axis1 == out_axis1);

    s32 transfer_size;
    s32 transfer_width;
    s32 src_stride;
    s32 no_transfers = 0;

    // Cropping on the slowest growing dimension(axis2) is handled
    // via DMA src offset and transfer size/no transfers.
    if(crop_axis0 && crop_axis1)
    {
        // If we have cropping on the fastest and second fastest growing
        // dimensions we have to make out_axis2 dma transfers.
        transfer_width = out_axis0;
        src_stride = in_axis0;
        transfer_size = out_axis0 * out_axis1;
        no_transfers = out_axis2;
    }
    else
    {
        // Otherwise we need one transfer, only.
        no_transfers = 1;

        if(crop_axis0)
        {
            // We have crop on the fastest growing dimension(axis0).
            transfer_width = out_axis0;
            src_stride = in_axis0;
            transfer_size = out_axis0 * out_axis1 * out_axis2;
        }
        else if(crop_axis1)
        {
            // We have crop on the second fastest growing dimension(axis1).
            transfer_width = out_axis0 * out_axis1;
            src_stride = in_axis0 * in_axis1;
            transfer_size = out_axis0 * out_axis1 * out_axis2;
        }
        else
        {
            // Crop only the slowest growing dimension.
            transfer_size = out_axis0 * out_axis1 * out_axis2;
            transfer_width = transfer_size;
            src_stride = 0;
        }
    }

    for(s32 transfer_i = 0; transfer_i < no_transfers; ++transfer_i)
    {
        s32 offset = (offset_axis2 + transfer_i) * in_axis1 * in_axis0 +
                offset_axis1 * in_axis0 + offset_axis0;

        u8 *src = (u8 *)mvTensorParam->input->data + offset * sizeof(fp16);
        u8 *dst = (u8 *)mvTensorParam->output->data + transfer_i * out_axis0 * out_axis1 * sizeof(fp16);

        swcLeonSetPIL(0);
        DrvCmxDmaInitDefault();
        id1 = DrvCmxDmaInitRequesterOnAgent(1, mvTensorParam->myriadResources->dmaLinkAgent);

        ref1 = DrvCmxDmaCreateTransactionSrcStride(id1, &task1,
                src,
                dst,
                sizeof(fp16) * transfer_size,
                sizeof(fp16) * transfer_width,
                sizeof(fp16) * src_stride);

        DrvCmxDmaStartListTask(ref1);
        DrvCmxDmaWaitTask(ref1);
    }
}
