#-------------------------------[ Local shave applications build rules ]------------------#
#Describe the rule for building the MvTensor application. Simple rule specifying
#which objects build up the said application. The application will be built into a library
ENTRYPOINTS_MVTENSOR = -e Entry -u start -u printAddress -u mvAvgPoolMxN -u mvMaxPoolMxN \
				-u reluNegSlopeFp16 -u postOps_core -u mvLRN -u mvMaxPool3x3 -u mvMaxPool2x2 \
				-u mvAvgPool3x3 -u mvAvgPool7x7xk -u mvSoftMax -u mvFC -u mvDepthConv \
			  	-u mvEltwise -u mvConv7x7s2 -u mvSpatialConv -u relayout_core -u SHVMatGEMM -u SHVMatGEMV \
			  	-u sData \
			  	--gc-section

$(DirTestOutput).mvlib : $(PROJECT_DEP_TARGETS) $(SHAVE_MvTensor_OBJS) $(PROJECT_SHAVE_LIBS)
	$(ECHO) $(LD) $(MVLIBOPT) $(ENTRYPOINTS_MVTENSOR) $(SHAVE_MvTensor_OBJS) \
				  $(PROJECT_DEP_LIBS) -o $@

fathomLib.a : $(LEON_ALL_OBJECTS)
	$(ECHO) $(AR) rs fathomLib.a $(LEON_ALL_OBJECTS)

dbg:
	@echo $(LEON_SHARED_OBJECTS_REQUIRED_INIT)

#ENTRYPOINTS_MVMATMUL = -e start -u SHVMatGEMM -u SHVMatGEMV --gc-section

#$(MvMatMul).mvlib : $(PROJECT_DEP_TARGETS) $(SHAVE_MvMatMul_OBJS)
	#$(ECHO) $(LD) $(MVLIBOPT) $(ENTRYPOINTS_MVMATMUL) $(SHAVE_MvMatMul_OBJS) $(PROJECT_DEP_LIBS) -o $@

# -------------------------------- [ Build Options ] ------------------------------ #
# Extra app related options
#CCOPT	 += -DDEBUG
#CPPOPT  += -Wno-unused-variable -Wno-error
#CCOPT   += -Wno-unused-variable -Wno-error
#MVCCOPT += -Wno-unused-variable -Wno-error  -gdwarf-2
