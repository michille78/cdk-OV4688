///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     MvTensor Test application
///

// Includes
// ----------------------------------------------------------------------------
#include <string.h>
#include <stdio.h>

#include "mv_types.h"
#include "app_config.h"
#include "mvHelpersApi.h"
#include "swcCrc.h"
#include <DrvLeonL2C.h>
#include <swcShaveLoaderLocal.h>
#include <DrvShaveL2Cache.h>

#include <UnitTestApi.h>
#include <VcsHooksApi.h>

// MvTensor specific
#include "mvTensor.h"
#include "inceptionDefines.h"


// Source Specific #defines and types  (typedef,enum,struct)
// ----------------------------------------------------------------------------
#define MV_TENSOR_CALLS 7
#define LEON_HEAP_SIZE 80000000
//#define PYTHON_TEST 1
#define EXPECTED_CRC 0x9d4b4baf
#define EXPECTED_CRC_CONV5x5_IM2COL 0x324f072f

#define MAX_SHAVES 12

// Global data
// ----------------------------------------------------------------------------
DynamicContext_t localModule[MAX_SHAVES];
DynamicContextInstances_elm localModulePrivD[MAX_SHAVES];

double DDR_DATA callDurationArray[MV_TENSOR_CALLS];
u8 DDR_BSS leonHeap[LEON_HEAP_SIZE];

// Functions Definitions
// ----------------------------------------------------------------------------
static void updateConfig();

#ifndef PYTHON_TEST
static void generateDummyTestData();
#endif
// Functions Implementation
// ----------------------------------------------------------------------------

int main(void)
{
    initClocksAndMemory();
    mvSetHeap((unsigned int)leonHeap, LEON_HEAP_SIZE);

    int shvNo;
    for(shvNo = 0; shvNo < MAX_SHAVES; shvNo++)
    {
        memcpy(&localModule[shvNo], &MODULE_DATA(mvTensor),sizeof(DynamicContext_t));
        memcpy(&localModulePrivD[shvNo], MODULE_DATA(mvTensor).instancesData, sizeof(localModulePrivD[shvNo]));
        localModule[shvNo].instancesData = &localModulePrivD[shvNo];
    }

    updateConfig();

#ifndef PYTHON_TEST
    unitTestInit();
    for(int lastShv = 0; lastShv < MAX_SHAVES; lastShv++)
    {
        gLastShave = lastShv;
        generateDummyTestData();
#endif

        bzero(output, sizeof(output));
        bzero(aux_path2, sizeof(aux_path2));
        bzero(aux_path3, sizeof(aux_path3));
        bzero(aux_path4, sizeof(aux_path4));

        swcSetupDynShaveAppsComplete(&localModule[lastShv], getShaveList(), getShaveNo());

        for(u32 i = 0; i < MV_TENSOR_CALLS; i++)
        {
            mvTensor(&mvTensorParamArray[i]);

#ifndef PYTHON_TEST
            printf("%s\n", mvTensorParamArray[i].debugInfo->debugMsg);
            printf("MvTensor done in: %f ms on %d SHAVES\n", mvTensorParamArray[i].debugInfo->ms, (int)gLastShave+1);
#endif
            callDurationArray[i] = mvTensorParamArray[i].debugInfo->ms;

            DrvShaveL2CachePartitionFlushAndInvalidate(0);
        }

        swcCleanupDynShaveApps(&localModule[lastShv]);

        DrvLL2CFlushOpOnAddrRange(LL2C_OPERATION_INVALIDATE, 0,
                                  (u32)output, (u32)output + sizeof(output));

#ifndef PYTHON_TEST
        u32 crc = swcCalcCrc32((u8*)(output + PADDING), IN_WIDTH * IN_HEIGHT * 128, le_pointer);
        saveMemoryToFile((u32)(output + PADDING), IN_WIDTH * IN_HEIGHT * 256 * 2, "outMyriad.bin");
        unitTestAssert(crc == EXPECTED_CRC);
    }
    unitTestFinalReport();
#endif

    return 0;
}


static void updateConfig()
{
    weightsPath1.data        = (void*)kOffsetList[0];
    weightsPath2Conv1x1.data = (void*)kOffsetList[1];
    weightsPath2Conv3x3.data = (void*)kOffsetList[2];
    weightsPath3Conv1x1.data = (void*)kOffsetList[3];
    weightsPath3Conv5x5.data = (void*)kOffsetList[4];
    weightsPath4Conv1x1.data = (void*)kOffsetList[5];
}

#ifndef PYTHON_TEST
static void generateDummyTestData()
{
    for(u32 i = 0; i < IN_SIZE; i++)
        input[i] = 0x2e66; //0.1

    //conv1x1
    for(u32 i = 0; i < WEIGHTS_PATH1_SIZE; i++)
        weights[i] = 0x3266; //0.2

    //conv3x3_r
    for(u32 i = 0; i < IN_CHANNELS * CONV1x1_PATH2_OUT_CHANNELS; i++)
        weights[WEIGHTS_PATH1_SIZE + i] = 0x34cd; //0.3
    //conv3x3
    for(u32 i = IN_CHANNELS * CONV1x1_PATH2_OUT_CHANNELS; i < WEIGHTS_PATH2_SIZE; i++)
        weights[WEIGHTS_PATH1_SIZE + i] = 0x3666; //0.4

    //conv5x5_r
    for(u32 i = 0; i < IN_CHANNELS * CONV1x1_PATH3_OUT_CHANNELS; i++)
        weights[WEIGHTS_PATH1_SIZE + WEIGHTS_PATH2_SIZE + i] = 0x3800; //0.5
    //conv5x5
    for(u32 i = IN_CHANNELS * CONV1x1_PATH3_OUT_CHANNELS; i < WEIGHTS_PATH3_SIZE; i++)
        weights[WEIGHTS_PATH1_SIZE + WEIGHTS_PATH2_SIZE + i] = 0x38cd; //0.6

    //conv1x1
    for(u32 i = 0; i < WEIGHTS_PATH4_SIZE; i++)
        weights[WEIGHTS_PATH1_SIZE + WEIGHTS_PATH2_SIZE + WEIGHTS_PATH3_SIZE + i] = 0x399a; //0.7

    mvTensorParamArray[0].weights->data = weights;
    mvTensorParamArray[1].weights->data = &weights[WEIGHTS_PATH1_SIZE];
    mvTensorParamArray[2].weights->data = &weights[WEIGHTS_PATH1_SIZE + IN_CHANNELS * CONV1x1_PATH2_OUT_CHANNELS];
    mvTensorParamArray[3].weights->data = &weights[WEIGHTS_PATH1_SIZE + WEIGHTS_PATH2_SIZE];
    mvTensorParamArray[4].weights->data = &weights[WEIGHTS_PATH1_SIZE + WEIGHTS_PATH2_SIZE + IN_CHANNELS * CONV1x1_PATH3_OUT_CHANNELS];
    mvTensorParamArray[6].weights->data = &weights[WEIGHTS_PATH1_SIZE + WEIGHTS_PATH2_SIZE + WEIGHTS_PATH3_SIZE];

    myriadResourcesInception.lastShave = gLastShave;
}
#endif

