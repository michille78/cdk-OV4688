#ifndef __INCEPTION_DEFINES__
#define __INCEPTION_DEFINES__

#include "mvTensor.h"

#define IN_WIDTH 28
#define IN_HEIGHT 28
#define IN_CHANNELS 192
#define CONV1x1_PATH1_OUT_CHANNELS 64
#define CONV1x1_PATH2_OUT_CHANNELS 96
#define CONV1x1_PATH3_OUT_CHANNELS 16
#define CONV1x1_PATH4_OUT_CHANNELS 32
#define CONV3x3_OUT_CHANNELS 128
#define CONV5x5_OUT_CHANNELS 32

#define OUT_STRIDE 256
#define PADDING (2 * OUT_STRIDE * (IN_WIDTH + 2))

#define AUX_PATH2_SIZE (IN_WIDTH * IN_HEIGHT * CONV1x1_PATH2_OUT_CHANNELS)
#define AUX_PATH3_SIZE (IN_WIDTH * IN_HEIGHT * CONV1x1_PATH3_OUT_CHANNELS)
#define AUX_PATH4_SIZE (IN_WIDTH * IN_HEIGHT * IN_CHANNELS)

#define IN_SIZE (IN_WIDTH * IN_HEIGHT * IN_CHANNELS)

#define WEIGHTS_PATH1_SIZE (IN_CHANNELS * CONV1x1_PATH1_OUT_CHANNELS)
#define WEIGHTS_PATH2_SIZE (IN_CHANNELS * CONV1x1_PATH2_OUT_CHANNELS + 9 * CONV1x1_PATH2_OUT_CHANNELS * CONV3x3_OUT_CHANNELS)
#define WEIGHTS_PATH3_SIZE (IN_CHANNELS * CONV1x1_PATH3_OUT_CHANNELS + 25 * CONV1x1_PATH3_OUT_CHANNELS * CONV5x5_OUT_CHANNELS)
#define WEIGHTS_PATH4_SIZE (IN_CHANNELS * CONV1x1_PATH4_OUT_CHANNELS)

#define WEIGHTS_SIZE (WEIGHTS_PATH1_SIZE + WEIGHTS_PATH2_SIZE + WEIGHTS_PATH3_SIZE + WEIGHTS_PATH4_SIZE)

#define FIRST_SHAVE 0
#define DMA_LINK_AGENT 1
#define DATA_SHV_L2_PARTITION 0
#define INSTRUCTION_SHV_L2_PARTITION 1


// global data
u32 DDR_DATA kOffsetList[10];

u32 DDR_DATA gStartShave = FIRST_SHAVE;
u32 DDR_DATA gLastShave = LAST_SHAVE;
u32 DDR_DATA gDmaLinkAgent = DMA_LINK_AGENT;

fp16 DDR_DATA input[IN_SIZE];

fp16 DDR_DATA aux_path2[AUX_PATH2_SIZE];
fp16 DDR_DATA aux_path3[AUX_PATH3_SIZE];
fp16 DDR_DATA aux_path4[AUX_PATH4_SIZE];

fp16 DDR_DATA output [IN_WIDTH * IN_HEIGHT * OUT_STRIDE + 2 * PADDING];

fp16 DDR_DATA weights[WEIGHTS_SIZE];

char debugMsg[MV_TENSOR_DBG_MSG_SIZE];

char __attribute__((section(".ddr.bss"))) cache_memory[25 * 1024 * 1024];
char __attribute__((section(".cmx.bss"))) scratch_memory[110 * 1024];

dmaTransactionList_t __attribute__((section(".cmx.cdmaDescriptors"))) task[1];

//########################################## shared structures MvTensor ########################################

static t_MvTensorMyriadResources myriadResourcesInception =
{{{
    .firstShave = FIRST_SHAVE,
    .lastShave = LAST_SHAVE,
    .dmaLinkAgent = DMA_LINK_AGENT,
    .dataPartitionNo = DATA_SHV_L2_PARTITION,
    .instrPartitionNo = INSTRUCTION_SHV_L2_PARTITION,
    .dmaTransactions = &task[0]
}}};

static t_MvTensorDebugInfo tensorDbgInfo = {
    .ms = 0,
    .debugMsg = debugMsg
};

static t_MvMatMulMyriadResources matmulResources =
{
    .cache_memory_size = sizeof(cache_memory),
    .scratch_memory_size = sizeof(scratch_memory),
    .cache_memory_ptr = cache_memory,
    .scratch_memory_ptr = scratch_memory
};


//################################################ PATH I conv1x1 ######################################################

 static t_mvTensorGenData inputPath1 =
 {{{
    .data = (void*)input,
    .dimX = IN_WIDTH,
    .dimY = IN_HEIGHT,
    .dimZ = IN_CHANNELS,
    .dimXStride = sizeof(fp16) * IN_CHANNELS,
    .dimYStride = sizeof(fp16) * IN_CHANNELS * IN_WIDTH,
    .dimZStride = sizeof(fp16),
    .dataType = t_fp16,
    .storageOrder = orderYXZ
}}};

static t_mvTensorGenData outputPath1 =
{{{
    .data = (void*)(output + PADDING),
    .dimX = IN_WIDTH,
    .dimY = IN_HEIGHT,
    .dimZ = CONV1x1_PATH1_OUT_CHANNELS,
    .dimXStride = sizeof(fp16) * OUT_STRIDE,
    .dimYStride = sizeof(fp16) * OUT_STRIDE * IN_WIDTH,
    .dimZStride = sizeof(fp16),
    .dataType = t_fp16,
    .storageOrder = orderYXZ
}}};

static t_mvTensorGenData weightsPath1 =
{{{
    .data = NULL,
    .dimX = 1,
    .dimY = IN_CHANNELS,
    .dimZ = CONV1x1_PATH1_OUT_CHANNELS,
    .dimXStride = sizeof(fp16) * CONV1x1_PATH1_OUT_CHANNELS * IN_CHANNELS,
    .dimYStride = sizeof(fp16) * CONV1x1_PATH1_OUT_CHANNELS,
    .dimZStride = sizeof(fp16),
    .dataType = t_fp16,
    .storageOrder = orderXYZ
}}};

static t_MvTensorOp opConv1x1 =
{{{
    .type = kConv,
    .optMask = MV_TENSOR_DEFAULT_OPT,
    .radixX = 1,
    .radixY = 1,
    .strideX = 1,
    .strideY = 1,
    .padX = 0,
    .padY = 0,
    .paddStyle = paddStyleCaffe,
    .opX = 0,
    .params = NULL
}}};

static t_MvTensorOp opRelu =
{{{
    .type = kRelu,
    .optMask = MV_TENSOR_DEFAULT_OPT,
    .radixX = 1,
    .radixY = 1,
    .strideX = 1,
    .strideY = 1,
    .padX = 0,
    .padY = 0,
    .paddStyle = paddStyleNone,
    .opX = 0,
    .params = NULL
}}};

//################################################ PATH II conv1x1 ######################################################

static t_mvTensorGenData inputPath2Conv1x1 =
{{{
    .data = (void*)input,
    .dimX = IN_WIDTH,
    .dimY = IN_HEIGHT,
    .dimZ = IN_CHANNELS,
    .dimXStride = sizeof(fp16) * IN_CHANNELS,
    .dimYStride = sizeof(fp16) * IN_CHANNELS * IN_WIDTH,
    .dimZStride = sizeof(fp16),
    .dataType = t_fp16,
    .storageOrder = orderYXZ
}}};

static t_mvTensorGenData outputPath2Conv1x1 =
{{{
    .data = (void*)aux_path2,
    .dimX = IN_WIDTH,
    .dimY = IN_HEIGHT,
    .dimZ = CONV1x1_PATH2_OUT_CHANNELS,
    .dimXStride = sizeof(fp16) * CONV1x1_PATH2_OUT_CHANNELS ,
    .dimYStride = sizeof(fp16) * CONV1x1_PATH2_OUT_CHANNELS * IN_WIDTH,
    .dimZStride = sizeof(fp16),
    .dataType = t_fp16,
    .storageOrder = orderYXZ,
}}};

static t_mvTensorGenData weightsPath2Conv1x1 =
{{{
    .data = NULL,
    .dimX = 1,
    .dimY = IN_CHANNELS,
    .dimZ = CONV1x1_PATH2_OUT_CHANNELS,
    .dimXStride = sizeof(fp16) * CONV1x1_PATH2_OUT_CHANNELS * IN_CHANNELS,
    .dimYStride = sizeof(fp16) * CONV1x1_PATH2_OUT_CHANNELS,
    .dimZStride = sizeof(fp16),
    .dataType = t_fp16,
    .storageOrder = orderXYZ,
}}};


//################################################ PATH II conv3x3 ######################################################

static t_mvTensorGenData outputPath2Conv3x3 =
{{{
    .data = (void*)(output + CONV1x1_PATH1_OUT_CHANNELS + PADDING),
    .dimX = IN_WIDTH,
    .dimY = IN_HEIGHT,
    .dimZ = CONV3x3_OUT_CHANNELS,
    .dimXStride = sizeof(fp16) * OUT_STRIDE ,
    .dimYStride = sizeof(fp16) * OUT_STRIDE * IN_WIDTH,
    .dimZStride = sizeof(fp16),
    .dataType = t_fp16,
    .storageOrder = orderYXZ
}}};

static t_mvTensorGenData weightsPath2Conv3x3 =
{{{
    .data = NULL,
    .dimX = 3 * 3,
    .dimY = CONV1x1_PATH2_OUT_CHANNELS,
    .dimZ = CONV3x3_OUT_CHANNELS,
    .dimXStride = sizeof(fp16) * CONV3x3_OUT_CHANNELS * CONV1x1_PATH2_OUT_CHANNELS,
    .dimYStride = sizeof(fp16) * CONV3x3_OUT_CHANNELS,
    .dimZStride = sizeof(fp16),
    .dataType = t_fp16,
    .storageOrder = orderXYZ
}}};

static t_MvTensorOp opConv3x3 =
{{{
    .type = kConv,
    .optMask = MV_TENSOR_DEFAULT_OPT,
    .radixX = 3,
    .radixY = 3,
    .strideX = 1,
    .strideY = 1,
    .padX = 1,
    .padY = 1,
    .paddStyle = paddStyleCaffe,
    .opX = 0,
    .params = NULL
}}};

//################################################ PATH III conv1x1 ######################################################

static t_mvTensorGenData inputPath3Conv1x1 =
{{{
    .data = (void*)input,
    .dimX = IN_WIDTH,
    .dimY = IN_HEIGHT,
    .dimZ = IN_CHANNELS,
    .dimXStride = sizeof(fp16) * IN_CHANNELS,
    .dimYStride = sizeof(fp16) * IN_CHANNELS * IN_WIDTH,
    .dimZStride = sizeof(fp16),
    .dataType = t_fp16,
    .storageOrder = orderYXZ
}}};

static t_mvTensorGenData outputPath3Conv1x1 =
{{{
   .data = (void*)aux_path3,
   .dimX = IN_WIDTH,
   .dimY = IN_HEIGHT,
   .dimZ = CONV1x1_PATH3_OUT_CHANNELS,
   .dimXStride = sizeof(fp16) * CONV1x1_PATH3_OUT_CHANNELS,
   .dimYStride = sizeof(fp16) * CONV1x1_PATH3_OUT_CHANNELS * IN_WIDTH,
   .dimZStride = sizeof(fp16),
   .dataType = t_fp16,
   .storageOrder = orderYXZ
}}};

static t_mvTensorGenData weightsPath3Conv1x1 =
{{{
  .data = NULL,
  .dimX = 1,
  .dimY = IN_CHANNELS,
  .dimZ = CONV1x1_PATH3_OUT_CHANNELS,
  .dimXStride = sizeof(fp16) * CONV1x1_PATH3_OUT_CHANNELS * IN_CHANNELS,
  .dimYStride = sizeof(fp16) * CONV1x1_PATH3_OUT_CHANNELS,
  .dimZStride = sizeof(fp16),
  .dataType = t_fp16,
  .storageOrder = orderXYZ
}}};

//################################################ PATH III conv5x5 ######################################################
static t_mvTensorGenData outputPath3Conv5x5 =
{{{
    .data = (void*)(output + CONV1x1_PATH1_OUT_CHANNELS + CONV3x3_OUT_CHANNELS + PADDING),
    .dimX = IN_WIDTH,
    .dimY = IN_HEIGHT,
    .dimZ = CONV5x5_OUT_CHANNELS,
    .dimXStride = sizeof(fp16) * OUT_STRIDE,
    .dimYStride = sizeof(fp16) * OUT_STRIDE * IN_WIDTH,
    .dimZStride = sizeof(fp16),
    .dataType = t_fp16,
    .storageOrder = orderYXZ
}}};

static t_mvTensorGenData weightsPath3Conv5x5 =
{{{
    .data = NULL,
    .dimX = 5 * 5,
    .dimY = CONV1x1_PATH3_OUT_CHANNELS,
    .dimZ = CONV5x5_OUT_CHANNELS,
    .dimXStride = sizeof(fp16) * CONV5x5_OUT_CHANNELS * CONV1x1_PATH3_OUT_CHANNELS,
    .dimYStride = sizeof(fp16) * CONV5x5_OUT_CHANNELS,
    .dimZStride = sizeof(fp16),
    .dataType = t_fp16,
    .storageOrder = orderXYZ
}}};

static t_MvTensorOp opConv5x5 =
{{{
    .type = kConv,
    .optMask = MV_TENSOR_DEFAULT_OPT,
    .radixX = 5,
    .radixY = 5,
    .strideX = 1,
    .strideY = 1,
    .padX = 2,
    .padY = 2,
    .paddStyle = paddStyleCaffe,
    .opX = 0,
    .params = NULL
}}};

//################################################ PATH IV max pool ######################################################

static t_mvTensorGenData inputPath4MaxPool =
{{{
    .data = (void*)input,
    .dimX = IN_WIDTH,
    .dimY = IN_HEIGHT,
    .dimZ = IN_CHANNELS,
    .dimXStride = sizeof(fp16) * IN_CHANNELS,
    .dimYStride = sizeof(fp16) * IN_CHANNELS * IN_WIDTH,
    .dimZStride = sizeof(fp16),
    .dataType = t_fp16,
    .storageOrder = orderYXZ
}}};

static t_mvTensorGenData outputPath4MaxPool =
{{{
    .data = (void*)aux_path4,
    .dimX = IN_WIDTH,
    .dimY = IN_HEIGHT,
    .dimZ = IN_CHANNELS,
    .dimXStride = sizeof(fp16) * IN_CHANNELS,
    .dimYStride = sizeof(fp16) * IN_CHANNELS * IN_WIDTH,
    .dimZStride = sizeof(fp16),
    .dataType = t_fp16,
    .storageOrder = orderYXZ
}}};

static t_MvTensorOp opMaxPool3x3 =
{{{
    .type = kMaxPool,
    .optMask = MV_TENSOR_DEFAULT_OPT,
    .radixX = 3,
    .radixY = 3,
    .strideX = 1,
    .strideY = 1,
    .padX = 1,
    .padY = 1,
    .paddStyle = paddStyleCaffe,
    .opX = 0,
    .params = NULL
}}};

//################################################ PATH IV conv1x1 ######################################################

static t_mvTensorGenData outputPath4Conv1x1 =
{{{
    .data = (void*)(output + OUT_STRIDE - CONV1x1_PATH4_OUT_CHANNELS + PADDING),
    .dimX = IN_WIDTH,
    .dimY = IN_HEIGHT,
    .dimZ = CONV1x1_PATH4_OUT_CHANNELS,
    .dimXStride = sizeof(fp16) * OUT_STRIDE,
    .dimYStride = sizeof(fp16) * OUT_STRIDE * IN_WIDTH,
    .dimZStride = sizeof(fp16),
    .dataType = t_fp16,
    .storageOrder = orderYXZ
}}};

static t_mvTensorGenData weightsPath4Conv1x1 =
{{{
    .data = NULL,
    .dimX = 1,
    .dimY = IN_CHANNELS,
    .dimZ = CONV1x1_PATH4_OUT_CHANNELS,
    .dimXStride = sizeof(fp16) * CONV1x1_PATH4_OUT_CHANNELS * IN_CHANNELS,
    .dimYStride = sizeof(fp16) * CONV1x1_PATH4_OUT_CHANNELS,
    .dimZStride = sizeof(fp16),
    .dataType = t_fp16,
    .storageOrder = orderXYZ
}}};


//####################################### Array with the 7 structs needed for inception ###############################################

static t_MvTensorParam mvTensorParamArray[7]
{
    //conv1x1
    {{{
        .input = &inputPath1,
        .output = &outputPath1,
        .weights = &weightsPath1,
        .accumulation = NULL,
        .biases = NULL,
        .preOp = NULL,
        .op = &opConv1x1,
        .postOp = &opRelu,
        .myriadResources = &myriadResourcesInception,
        .debugInfo = &tensorDbgInfo,
        .matmulResources = &matmulResources
    }}},

    //conv3x3r
    {{{
        .input = &inputPath2Conv1x1,
        .output = &outputPath2Conv1x1,
        .weights = &weightsPath2Conv1x1,
        .accumulation = NULL,
        .biases = NULL,
        .preOp = NULL,
        .op = &opConv1x1,
        .postOp = &opRelu,
        .myriadResources = &myriadResourcesInception,
        .debugInfo = &tensorDbgInfo,
        .matmulResources = &matmulResources
    }}},

    //conv3x3
    {{{
        .input = &outputPath2Conv1x1,
        .output = &outputPath2Conv3x3,
        .weights = &weightsPath2Conv3x3,
        .accumulation = NULL,
        .biases = NULL,
        .preOp = NULL,
        .op = &opConv3x3,
        .postOp = &opRelu,
        .myriadResources = &myriadResourcesInception,
        .debugInfo = &tensorDbgInfo,
        .matmulResources = &matmulResources
    }}},

    //conv5x5r
    {{{
        .input = &inputPath3Conv1x1,
        .output = &outputPath3Conv1x1,
        .weights = &weightsPath3Conv1x1,
        .accumulation = NULL,
        .biases = NULL,
        .preOp = NULL,
        .op = &opConv1x1,
        .postOp = &opRelu,
        .myriadResources = &myriadResourcesInception,
        .debugInfo = &tensorDbgInfo,
        .matmulResources = &matmulResources
    }}},

    //conv5x5
    {{{
        .input = &outputPath3Conv1x1,
        .output = &outputPath3Conv5x5,
        .weights = &weightsPath3Conv5x5,
        .accumulation = NULL,
        .biases = NULL,
        .preOp = NULL,
        .op = &opConv5x5,
        .postOp = &opRelu,
        .myriadResources = &myriadResourcesInception,
        .debugInfo = &tensorDbgInfo,
        .matmulResources = &matmulResources
    }}},

    //maxPool
    {{{
        .input = &inputPath4MaxPool,
        .output = &outputPath4MaxPool,
        .weights = NULL,
        .accumulation = NULL,
        .biases = NULL,
        .preOp = NULL,
        .op = &opMaxPool3x3,
        .postOp = &opRelu,
        .myriadResources = &myriadResourcesInception,
        .debugInfo = &tensorDbgInfo,
        .matmulResources = &matmulResources
    }}},

    //conv1x1
    {{{
        .input = &outputPath4MaxPool,
        .output = &outputPath4Conv1x1,
        .weights = &weightsPath4Conv1x1,
        .accumulation = NULL,
        .biases = NULL,
        .preOp = NULL,
        .op = &opConv1x1,
        .postOp = &opRelu,
        .myriadResources = &myriadResourcesInception,
        .debugInfo = &tensorDbgInfo,
        .matmulResources = &matmulResources
    }}}

};

#endif // __INCEPTION_DEFINES__
