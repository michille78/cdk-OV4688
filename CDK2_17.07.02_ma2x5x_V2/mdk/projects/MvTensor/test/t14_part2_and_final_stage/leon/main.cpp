///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     MvTensor Test application
///

// Includes
// ----------------------------------------------------------------------------
#include <string.h>
#include <stdio.h>

#include "mv_types.h"
#include "app_config.h"
#include "mvHelpersApi.h"
#include "swcCrc.h"
#include <swcShaveLoaderLocal.h>
#include <DrvLeonL2C.h>

#include <UnitTestApi.h>
#include <VcsHooksApi.h>

#include "defines.h"

// MvTensor specific
#include "mvTensor.h"

// Source Specific #defines and types  (typedef,enum,struct)
// ----------------------------------------------------------------------------
#define MV_TENSOR_CALLS 7
#define LEON_HEAP_SIZE 80000000
//#define PYTHON_TEST 1
#define EXPECTED_CRC 0x8d27d7e4

#define MAX_SHAVES 12

// Global data
// ----------------------------------------------------------------------------
DynamicContext_t localModule[MAX_SHAVES];
DynamicContextInstances_elm localModulePrivD[MAX_SHAVES];

double DDR_DATA gCallDuration = 0;
u8 DDR_BSS leonHeap[LEON_HEAP_SIZE];

// Functions Definitions
// ----------------------------------------------------------------------------
static void generateDummyTestData();
static void inceptionStage(t_inception *inceptionStruct);

// Functions Implementation
// ----------------------------------------------------------------------------

int main(void)
{
    initClocksAndMemory();
    mvSetHeap((unsigned int)leonHeap, LEON_HEAP_SIZE);

    int shvNo;
    for(shvNo = 0; shvNo < MAX_SHAVES; shvNo++)
    {
        memcpy(&localModule[shvNo], &MODULE_DATA(mvTensor),sizeof(DynamicContext_t));
        memcpy(&localModulePrivD[shvNo], MODULE_DATA(mvTensor).instancesData, sizeof(localModulePrivD[shvNo]));
        localModule[shvNo].instancesData = &localModulePrivD[shvNo];
    }


    bzero(auxInOut1, sizeof(auxInOut1));
    bzero(auxInOut2, sizeof(auxInOut2));


#ifndef PYTHON_TEST
        unitTestInit();
        generateDummyTestData();
#endif


    // Inception 3A
    inPtr[0] = input;
    u32 outChannelsInception1 = outChannels1x1[0] + outChannels3x3[0] + outChannels5x5[0] + outChannelsPoolPath[0];
    outPtr[0] = &auxInOut1[2 * outChannelsInception1 * (inWidth[0] + 2)];

    // Inception2
    inPtr[1] = &auxInOut1[2 * outChannelsInception1 * (inWidth[0] + 2)];
    u32 outChannelsInception2 = outChannels1x1[1] + outChannels3x3[1] + outChannels5x5[1] + outChannelsPoolPath[1];
    outPtr[1] = &auxInOut2[2 * outChannelsInception2 * (inWidth[1] + 2)];

    // Inception 4A
    inPtr[2] = &auxInOut1[2 * outChannelsInception2 * (inWidth[1] + 2)];
    u32 outChannelsInception3 = outChannels1x1[2] + outChannels3x3[2] + outChannels5x5[2] + outChannelsPoolPath[2];
    outPtr[2] = &auxInOut2[2 * outChannelsInception3 * (inWidth[2] + 2)];

    // Inception 4B
    inPtr[3] = &auxInOut2[2 * outChannelsInception3 * (inWidth[2] + 2)];
    u32 outChannelsInception4 = outChannels1x1[3] + outChannels3x3[3] + outChannels5x5[3] + outChannelsPoolPath[3];
    outPtr[3] = &auxInOut1[2 * outChannelsInception4 * (inWidth[3] + 2)];

    // Inception 4C
    inPtr[4] = &auxInOut1[2 * outChannelsInception4 * (inWidth[3] + 2)];
    u32 outChannelsInception5 = outChannels1x1[4] + outChannels3x3[4] + outChannels5x5[4] + outChannelsPoolPath[4];
    outPtr[4] = &auxInOut2[2 * outChannelsInception5 * (inWidth[4] + 2)];

    // Inception 4D
    inPtr[5] = &auxInOut2[2 * outChannelsInception5 * (inWidth[4] + 2)];
    u32 outChannelsInception6 = outChannels1x1[5] + outChannels3x3[5] + outChannels5x5[5] + outChannelsPoolPath[5];
    outPtr[5] = &auxInOut1[2 * outChannelsInception6 * (inWidth[5] + 2)];

    // Inception 4E
    inPtr[6] = &auxInOut1[2 * outChannelsInception6 * (inWidth[5] + 2)];
    u32 outChannelsInception7 = outChannels1x1[6] + outChannels3x3[6] + outChannels5x5[6] + outChannelsPoolPath[6];
    outPtr[6] = &auxInOut2[2 * outChannelsInception7 * (inWidth[6] + 2)];

    // Inception 5A
    inPtr[7] = &auxInOut1[2 * outChannelsInception7 * (inWidth[6] + 2)];
    u32 outChannelsInception8 = outChannels1x1[7] + outChannels3x3[7] + outChannels5x5[7] + outChannelsPoolPath[7];
    outPtr[7] = &auxInOut2[2 * outChannelsInception8 * (inWidth[7] + 2)];

    // Inception 5B
    inPtr[8] = &auxInOut2[2 * outChannelsInception8 * (inWidth[7] + 2)];
    u32 outChannelsInception9 = outChannels1x1[8] + outChannels3x3[8] + outChannels5x5[8] + outChannelsPoolPath[8];
    outPtr[8] = &auxInOut1[2 * outChannelsInception9 * (inWidth[8] + 2)];

    // Average Pool
    inputAvgPool.data = (void*)outPtr[8];
    inputAvgPool.dimZ = outChannelsInception9;
    inputAvgPool.dimX = 7;
    inputAvgPool.dimY = 7;
    outputAvgPool.data = (void*)auxInOut2;
    outputAvgPool.dimZ = outChannelsInception9;

    // Fully Connected Layer
    weightsFC.data = (void*)weightsValuesForFC;
    outputFC.data = (void*)auxInOut1;
    outputFC.dimZ = outClasses;

    // SoftMax
    outputSoftMax.data = (void*)auxInOut2;

    for(shvNo = 0; shvNo <= LAST_SHAVE; shvNo++)
            swcSetupDynShaveAppsComplete(&localModule[shvNo], getShaveList() + shvNo, 1);

    u32 maxPoolRun = 0;
    t_inception inceptionParam;
    u32 i;
    for(i = 0; i < INCEPTIONS_NO; i++)
    {
        u32 outChannels = outChannels1x1[i] + outChannels3x3[i]
                        + outChannels5x5[i] + outChannelsPoolPath[i];
        u32 outSize = outChannels * inWidth[i] * inHeight[i];

        bzero(auxPath2, sizeof(auxPath2));
        bzero(auxPath3, sizeof(auxPath3));
        bzero(auxPath4, sizeof(auxPath4));
        bzero(outPtr[i], outSize * sizeof(fp16));

        inceptionParam.input = inPtr[i];
        inceptionParam.output = outPtr[i];
        inceptionParam.weights[0] = (void*)kOffsetList[i*6 + 0];
        inceptionParam.weights[1] = (void*)kOffsetList[i*6 + 1];
        inceptionParam.weights[2] = (void*)kOffsetList[i*6 + 2];
        inceptionParam.weights[3] = (void*)kOffsetList[i*6 + 3];
        inceptionParam.weights[4] = (void*)kOffsetList[i*6 + 4];
        inceptionParam.weights[5] = (void*)kOffsetList[i*6 + 5];
        inceptionParam.path2Aux = auxPath2;
        inceptionParam.path3Aux = auxPath3;
        inceptionParam.path4Aux = auxPath4;
        inceptionParam.dbgInfoPtr0 = &debugStructInception;
        inceptionParam.dbgInfoPtr1 = &debugStructInception;
        inceptionParam.dbgInfoPtr2 = &debugStructInception;
        inceptionParam.dbgInfoPtr3 = &debugStructInception;
        inceptionParam.dbgInfoPtr4 = &debugStructInception;
        inceptionParam.dbgInfoPtr5 = &debugStructInception;
        inceptionParam.dbgInfoPtr6 = &debugStructInception;
        inceptionParam.inChannels = inChannels[i];
        inceptionParam.inWidth = inWidth[i];
        inceptionParam.inHeight = inHeight[i];
        inceptionParam.conv1x1OutChannels = outChannels1x1[i];
        inceptionParam.conv3x3ROutChannels = outChannels3x3_r[i];
        inceptionParam.conv3x3OutChannels = outChannels3x3[i];
        inceptionParam.conv5x5ROutChannels = outChannels5x5_r[i];
        inceptionParam.conv5x5OutChannels = outChannels5x5[i];
        inceptionParam.poolPathOutChannels = outChannelsPoolPath[i];
        inceptionParam.bpp = sizeof(fp16);
        inceptionParam.dataType = t_fp16;

        inceptionStage(&inceptionParam);

        if((i == 1) || (i == 6))
        {
            mvTensorMaxPoolParamArray[maxPoolRun].input->data = outPtr[i];
            mvTensorMaxPoolParamArray[maxPoolRun].input->dimX = inWidth[i];
            mvTensorMaxPoolParamArray[maxPoolRun].input->dimY = inHeight[i];
            mvTensorMaxPoolParamArray[maxPoolRun].input->dimZ = outChannels;
            mvTensorMaxPoolParamArray[maxPoolRun].input->dimXStride = sizeof(fp16) * outChannels;
            mvTensorMaxPoolParamArray[maxPoolRun].input->dimYStride = sizeof(fp16) * outChannels * inWidth[i];
            mvTensorMaxPoolParamArray[maxPoolRun].input->dimZStride = sizeof(fp16);
            mvTensorMaxPoolParamArray[maxPoolRun].input->dataType = t_fp16;
            mvTensorMaxPoolParamArray[maxPoolRun].input->storageOrder = orderYXZ;

            mvTensorMaxPoolParamArray[maxPoolRun].output->data = inPtr[i+1];
            mvTensorMaxPoolParamArray[maxPoolRun].output->dimX = inWidth[i] / 2;
            mvTensorMaxPoolParamArray[maxPoolRun].output->dimY = inHeight[i] / 2;
            mvTensorMaxPoolParamArray[maxPoolRun].output->dimZ = outChannels;
            mvTensorMaxPoolParamArray[maxPoolRun].output->dimXStride = sizeof(fp16) * outChannels;
            mvTensorMaxPoolParamArray[maxPoolRun].output->dimYStride = sizeof(fp16) * outChannels * inWidth[i] / 2;
            mvTensorMaxPoolParamArray[maxPoolRun].output->dimZStride = sizeof(fp16);
            mvTensorMaxPoolParamArray[maxPoolRun].output->dataType = t_fp16;
            mvTensorMaxPoolParamArray[maxPoolRun].output->storageOrder = orderYXZ;

            mvTensor(&mvTensorMaxPoolParamArray[maxPoolRun]);
            printf("%s\n", mvTensorMaxPoolParamArray[maxPoolRun].debugInfo->debugMsg);
            printf("MvTensor done in: %f ms\n", mvTensorMaxPoolParamArray[maxPoolRun].debugInfo->ms);
            printf("______________________________________________\n");

            gCallDuration += mvTensorMaxPoolParamArray[maxPoolRun].debugInfo->ms;

            maxPoolRun++;
        }
    }


    for(i = 0; i < NO_OF_MV_TENSOR_CALLS_FOR_FINAL_STAGE; i++)
    {
        mvTensor(&mvTensorFinalStageParamArray[i]);
        printf("%s\n", mvTensorFinalStageParamArray[i].debugInfo->debugMsg);
        printf("MvTensor done in: %f ms\n", mvTensorFinalStageParamArray[i].debugInfo->ms);
        gCallDuration += mvTensorFinalStageParamArray[i].debugInfo->ms;
    }

    for(shvNo = 0; shvNo <= LAST_SHAVE; shvNo++)
        swcCleanupDynShaveApps(&localModule[shvNo]);

    printf("______________________________________________\n");
    printf("All mvTensor calls took %f ms\n", gCallDuration);

    DrvLL2CFlushOpOnAddrRange(LL2C_OPERATION_INVALIDATE, 0,
        (u32)auxInOut2, (u32)auxInOut2 + sizeof(auxInOut2));

#ifndef PYTHON_TEST
    u32 crc = swcCalcCrc32((u8*)auxInOut2, outClasses * sizeof(fp16) / sizeof(u32), le_pointer);
    unitTestAssert(crc == EXPECTED_CRC);
    unitTestFinalReport();
#endif

    saveMemoryToFile((u32)auxInOut2, outClasses * sizeof(fp16), "outMyriad.bin");
    return 0;
}


static void inceptionStage(t_inception *inceptionStruct)
{
    u32 outStride = inceptionStruct->conv1x1OutChannels +
                    inceptionStruct->conv3x3OutChannels +
                    inceptionStruct->conv5x5OutChannels +
                    inceptionStruct->poolPathOutChannels;

    // set values for input struct (this is the input struct for all paths)
    mvTensorParamArray[0].input->data = inceptionStruct->input;
    mvTensorParamArray[0].input->dimX = inceptionStruct->inWidth;
    mvTensorParamArray[0].input->dimY = inceptionStruct->inHeight;
    mvTensorParamArray[0].input->dimZ = inceptionStruct->inChannels;
    mvTensorParamArray[0].input->dimZStride = inceptionStruct->bpp;
    mvTensorParamArray[0].input->dimXStride = mvTensorParamArray[0].input->dimZStride * mvTensorParamArray[0].input->dimZ;
    mvTensorParamArray[0].input->dimYStride = mvTensorParamArray[0].input->dimXStride * mvTensorParamArray[0].input->dimX;
    mvTensorParamArray[0].input->dataType = inceptionStruct->dataType;
    mvTensorParamArray[0].input->storageOrder = orderYXZ;

//###################################################### PATH I ###########################################################
    // set values for output struct - path1 (conv1x1)
    mvTensorParamArray[0].output->data = inceptionStruct->output;
    mvTensorParamArray[0].output->dimX = inceptionStruct->inWidth;
    mvTensorParamArray[0].output->dimY = inceptionStruct->inHeight;
    mvTensorParamArray[0].output->dimZ = inceptionStruct->conv1x1OutChannels;
    mvTensorParamArray[0].output->dimZStride = inceptionStruct->bpp;
    mvTensorParamArray[0].output->dimXStride = mvTensorParamArray[0].output->dimZStride * outStride;
    mvTensorParamArray[0].output->dimYStride = mvTensorParamArray[0].output->dimXStride * mvTensorParamArray[0].output->dimX;
    mvTensorParamArray[0].output->dataType = inceptionStruct->dataType;
    mvTensorParamArray[0].output->storageOrder = orderYXZ;

    // set values for weights struct - path1 (conv1x1)
    mvTensorParamArray[0].weights->data = inceptionStruct->weights[0];
    mvTensorParamArray[0].weights->dimX = 1;
    mvTensorParamArray[0].weights->dimY = inceptionStruct->inChannels;
    mvTensorParamArray[0].weights->dimZ = inceptionStruct->conv1x1OutChannels;
    mvTensorParamArray[0].weights->dimXStride = inceptionStruct->bpp * inceptionStruct->conv1x1OutChannels * inceptionStruct->inChannels;
    mvTensorParamArray[0].weights->dimYStride = inceptionStruct->bpp * inceptionStruct->conv1x1OutChannels;
    mvTensorParamArray[0].weights->dimZStride = inceptionStruct->bpp;
    mvTensorParamArray[0].weights->dataType = inceptionStruct->dataType;
    mvTensorParamArray[0].weights->storageOrder = orderXYZ;

//###################################################### PATH II ###########################################################
    // set values for output struct - path2 (conv1x1)
    mvTensorParamArray[1].output->data = inceptionStruct->path2Aux;
    mvTensorParamArray[1].output->dimX = inceptionStruct->inWidth;
    mvTensorParamArray[1].output->dimY = inceptionStruct->inHeight;
    mvTensorParamArray[1].output->dimZ = inceptionStruct->conv3x3ROutChannels;
    mvTensorParamArray[1].output->dimZStride = inceptionStruct->bpp;
    mvTensorParamArray[1].output->dimXStride = mvTensorParamArray[1].output->dimZStride * mvTensorParamArray[1].output->dimZ;
    mvTensorParamArray[1].output->dimYStride = mvTensorParamArray[1].output->dimXStride * mvTensorParamArray[1].output->dimX;
    mvTensorParamArray[1].output->dataType = inceptionStruct->dataType;
    mvTensorParamArray[1].output->storageOrder = orderYXZ;

    // set values for weights struct - path2 (conv1x1)
    mvTensorParamArray[1].weights->data = inceptionStruct->weights[1];
    mvTensorParamArray[1].weights->dimX = 1;
    mvTensorParamArray[1].weights->dimY = inceptionStruct->inChannels;
    mvTensorParamArray[1].weights->dimZ = inceptionStruct->conv3x3ROutChannels;
    mvTensorParamArray[1].weights->dimXStride = inceptionStruct->bpp * inceptionStruct->conv3x3ROutChannels * inceptionStruct->inChannels;
    mvTensorParamArray[1].weights->dimYStride = inceptionStruct->bpp * inceptionStruct->conv3x3ROutChannels;
    mvTensorParamArray[1].weights->dimZStride = inceptionStruct->bpp;
    mvTensorParamArray[1].weights->dataType = inceptionStruct->dataType;
    mvTensorParamArray[1].weights->storageOrder = orderXYZ;

    // set input - path2 (conv3x3)
    mvTensorParamArray[2].input = mvTensorParamArray[1].output;

    // set values for output struct - path2 (conv1x1)
    mvTensorParamArray[2].output->data = (u8*)inceptionStruct->output + inceptionStruct->conv1x1OutChannels * inceptionStruct->bpp;
    mvTensorParamArray[2].output->dimX = inceptionStruct->inWidth;
    mvTensorParamArray[2].output->dimY = inceptionStruct->inHeight;
    mvTensorParamArray[2].output->dimZ = inceptionStruct->conv3x3OutChannels;
    mvTensorParamArray[2].output->dimZStride = inceptionStruct->bpp;
    mvTensorParamArray[2].output->dimXStride = mvTensorParamArray[2].output->dimZStride * outStride;
    mvTensorParamArray[2].output->dimYStride = mvTensorParamArray[2].output->dimXStride * mvTensorParamArray[2].output->dimX;
    mvTensorParamArray[2].output->dataType = inceptionStruct->dataType;
    mvTensorParamArray[2].output->storageOrder = orderYXZ;

    // set values for weights struct - path2 (conv3x3)
    mvTensorParamArray[2].weights->data = inceptionStruct->weights[2];
    mvTensorParamArray[2].weights->dimX = 3 * 3;
    mvTensorParamArray[2].weights->dimY = inceptionStruct->conv3x3ROutChannels;
    mvTensorParamArray[2].weights->dimZ = inceptionStruct->conv3x3OutChannels;
    mvTensorParamArray[2].weights->dimXStride = inceptionStruct->bpp * inceptionStruct->conv3x3OutChannels * inceptionStruct->conv3x3ROutChannels;
    mvTensorParamArray[2].weights->dimYStride = inceptionStruct->bpp * inceptionStruct->conv3x3OutChannels;
    mvTensorParamArray[2].weights->dimZStride = inceptionStruct->bpp;
    mvTensorParamArray[2].weights->dataType = inceptionStruct->dataType;
    mvTensorParamArray[2].weights->storageOrder = orderXYZ;


//###################################################### PATH III ###########################################################
    // set values for output struct - path3 (conv1x1)
    mvTensorParamArray[3].output->data = inceptionStruct->path3Aux;
    mvTensorParamArray[3].output->dimX = inceptionStruct->inWidth;
    mvTensorParamArray[3].output->dimY = inceptionStruct->inHeight;
    mvTensorParamArray[3].output->dimZ = inceptionStruct->conv5x5ROutChannels;
    mvTensorParamArray[3].output->dimZStride = inceptionStruct->bpp;
    mvTensorParamArray[3].output->dimXStride = mvTensorParamArray[3].output->dimZStride * mvTensorParamArray[3].output->dimZ;
    mvTensorParamArray[3].output->dimYStride = mvTensorParamArray[3].output->dimXStride * mvTensorParamArray[3].output->dimX;
    mvTensorParamArray[3].output->dataType = inceptionStruct->dataType;
    mvTensorParamArray[3].output->storageOrder = orderYXZ;

    // set values for weights struct - path3 (conv1x1)
    mvTensorParamArray[3].weights->data = inceptionStruct->weights[3];
    mvTensorParamArray[3].weights->dimX = 1;
    mvTensorParamArray[3].weights->dimY = inceptionStruct->inChannels;
    mvTensorParamArray[3].weights->dimZ = inceptionStruct->conv5x5ROutChannels;
    mvTensorParamArray[3].weights->dimXStride = inceptionStruct->bpp * inceptionStruct->conv5x5ROutChannels * inceptionStruct->inChannels;
    mvTensorParamArray[3].weights->dimYStride = inceptionStruct->bpp * inceptionStruct->conv5x5ROutChannels;
    mvTensorParamArray[3].weights->dimZStride = inceptionStruct->bpp;
    mvTensorParamArray[3].weights->dataType = inceptionStruct->dataType;
    mvTensorParamArray[3].weights->storageOrder = orderXYZ;

    // set input - path3 (conv5x5)
    mvTensorParamArray[4].input = mvTensorParamArray[3].output;

    // set values for output struct - path3 (conv5x5)
    mvTensorParamArray[4].output->data = (u8*)inceptionStruct->output + (inceptionStruct->conv1x1OutChannels + inceptionStruct->conv3x3OutChannels) * inceptionStruct->bpp;
    mvTensorParamArray[4].output->dimX = inceptionStruct->inWidth;
    mvTensorParamArray[4].output->dimY = inceptionStruct->inHeight;
    mvTensorParamArray[4].output->dimZ = inceptionStruct->conv5x5OutChannels;
    mvTensorParamArray[4].output->dimZStride = inceptionStruct->bpp;
    mvTensorParamArray[4].output->dimXStride = mvTensorParamArray[4].output->dimZStride * outStride;
    mvTensorParamArray[4].output->dimYStride = mvTensorParamArray[4].output->dimXStride * mvTensorParamArray[4].output->dimX;
    mvTensorParamArray[4].output->dataType = inceptionStruct->dataType;
    mvTensorParamArray[4].output->storageOrder = orderYXZ;

    // set values for weights struct - path3 (conv5x5)
    mvTensorParamArray[4].weights->data = inceptionStruct->weights[4];
    mvTensorParamArray[4].weights->dimX = 5 * 5;
    mvTensorParamArray[4].weights->dimY = inceptionStruct->conv5x5ROutChannels;
    mvTensorParamArray[4].weights->dimZ = inceptionStruct->conv5x5OutChannels;
    mvTensorParamArray[4].weights->dimXStride = inceptionStruct->bpp * inceptionStruct->conv5x5OutChannels * inceptionStruct->conv5x5ROutChannels;
    mvTensorParamArray[4].weights->dimYStride = inceptionStruct->bpp * inceptionStruct->conv5x5OutChannels;
    mvTensorParamArray[4].weights->dimZStride = inceptionStruct->bpp;
    mvTensorParamArray[4].weights->dataType = inceptionStruct->dataType;
    mvTensorParamArray[4].weights->storageOrder = orderXYZ;


//###################################################### PATH IV ###########################################################
    // set values for output struct - path3 (conv1x1)
    mvTensorParamArray[5].output->data = inceptionStruct->path4Aux;
    mvTensorParamArray[5].output->dimX = inceptionStruct->inWidth;
    mvTensorParamArray[5].output->dimY = inceptionStruct->inHeight;
    mvTensorParamArray[5].output->dimZ = inceptionStruct->inChannels;
    mvTensorParamArray[5].output->dimZStride = inceptionStruct->bpp;
    mvTensorParamArray[5].output->dimXStride = mvTensorParamArray[5].output->dimZStride * mvTensorParamArray[5].output->dimZ;
    mvTensorParamArray[5].output->dimYStride = mvTensorParamArray[5].output->dimXStride * mvTensorParamArray[5].output->dimX;
    mvTensorParamArray[5].output->dataType = inceptionStruct->dataType;
    mvTensorParamArray[5].output->storageOrder = orderYXZ;

    // set values for weights struct - path3 (conv1x1)
    mvTensorParamArray[5].weights = NULL;

    // set input - path3 (conv5x5)
    mvTensorParamArray[6].input = mvTensorParamArray[5].output;

    // set values for output struct - path3 (conv5x5)
    mvTensorParamArray[6].output->data = (u8*)inceptionStruct->output +
                    (inceptionStruct->conv1x1OutChannels + inceptionStruct->conv3x3OutChannels +inceptionStruct->conv5x5OutChannels)
                    * inceptionStruct->bpp;
    mvTensorParamArray[6].output->dimX = inceptionStruct->inWidth;
    mvTensorParamArray[6].output->dimY = inceptionStruct->inHeight;
    mvTensorParamArray[6].output->dimZ = inceptionStruct->poolPathOutChannels;
    mvTensorParamArray[6].output->dimZStride = inceptionStruct->bpp;
    mvTensorParamArray[6].output->dimXStride = mvTensorParamArray[6].output->dimZStride * outStride;
    mvTensorParamArray[6].output->dimYStride = mvTensorParamArray[6].output->dimXStride * mvTensorParamArray[6].output->dimX;
    mvTensorParamArray[6].output->dataType = inceptionStruct->dataType;
    mvTensorParamArray[6].output->storageOrder = orderYXZ;

    // set values for weights struct - path3 (conv5x5)
    mvTensorParamArray[6].weights->data = inceptionStruct->weights[5];
    mvTensorParamArray[6].weights->dimX = 1;
    mvTensorParamArray[6].weights->dimY = inceptionStruct->inChannels;
    mvTensorParamArray[6].weights->dimZ = inceptionStruct->poolPathOutChannels;
    mvTensorParamArray[6].weights->dimXStride = inceptionStruct->bpp * inceptionStruct->poolPathOutChannels * inceptionStruct->inChannels;
    mvTensorParamArray[6].weights->dimYStride = inceptionStruct->bpp * inceptionStruct->poolPathOutChannels;
    mvTensorParamArray[6].weights->dimZStride = inceptionStruct->bpp;
    mvTensorParamArray[6].weights->dataType = inceptionStruct->dataType;
    mvTensorParamArray[6].weights->storageOrder = orderZYX;

    //set deebugInfo pointers
    mvTensorParamArray[0].debugInfo = inceptionStruct->dbgInfoPtr0;
    mvTensorParamArray[1].debugInfo = inceptionStruct->dbgInfoPtr1;
    mvTensorParamArray[2].debugInfo = inceptionStruct->dbgInfoPtr2;
    mvTensorParamArray[3].debugInfo = inceptionStruct->dbgInfoPtr3;
    mvTensorParamArray[4].debugInfo = inceptionStruct->dbgInfoPtr4;
    mvTensorParamArray[5].debugInfo = inceptionStruct->dbgInfoPtr5;
    mvTensorParamArray[6].debugInfo = inceptionStruct->dbgInfoPtr6;

    for(u32 i = 0; i < MV_TENSOR_CALLS; i++)
    {
        mvTensor(&mvTensorParamArray[i]);
        printf("%s\n", mvTensorParamArray[i].debugInfo->debugMsg);
        printf("MvTensor done in: %f ms\n", mvTensorParamArray[i].debugInfo->ms);
        gCallDuration += mvTensorParamArray[i].debugInfo->ms;
    }
    printf("______________________________________________\n");
}

//
static void generateDummyTestData()
{
    u32 offset = 0;
    u32 i, j;
    u16 valuesForWeights[INCEPTIONS_NO * (NO_OF_MV_TENSOR_CALLS_INSIDE_INCEPTION - 1)] =
        {0x251f, 0x27ae, 0x291f, 0x2a66, 0x2bae, 0x2c7b,
         0x1819, 0x1a25, 0x1c19, 0x1d1f, 0x1e25, 0x1f2b,
         0x2400, 0x1a00, 0x1970, 0x1992, 0x1800, 0x1fff,
         0x1e25, 0x17e8, 0x1801, 0x1992, 0x13cf, 0x1019,
         0x0cea, 0x0e8e, 0x0496, 0x068e, 0x1419, 0x0a8e,
         0x068e, 0x1019, 0x05c4, 0x05e6, 0x0a8e, 0x068e,
         0x2d1f, 0x1f2b, 0x209c, 0x1c19, 0x1a25, 0x27ae,
         0x1d39, 0x11bc, 0x1419, 0x1019, 0x1419, 0x2019,
         0x128e, 0x11bc, 0x10ea, 0x1019, 0x11bc, 0x2019};

    // Set weights values
    for(i = 0; i < INCEPTIONS_NO; i++)
    {
        kOffsetList[i*6 + 0] = (u32)&weightsForInception[offset];
        for(j = 0; j < inChannels[i] * outChannels1x1[i]; j++)
            weightsForInception[offset + j] = valuesForWeights[i*6 + 0];
        offset += inChannels[i] * outChannels1x1[i];

        kOffsetList[i*6 + 1] = (u32)&weightsForInception[offset];
        for(j = 0; j < inChannels[i] * outChannels3x3_r[i]; j++)
            weightsForInception[offset + j] = valuesForWeights[i*6 + 1];
        offset += inChannels[i] * outChannels3x3_r[i];

        kOffsetList[i*6 + 2] = (u32)&weightsForInception[offset];
        for(j = 0; j < 9 * outChannels3x3_r[i] * outChannels3x3[i]; j++)
            weightsForInception[offset + j] = valuesForWeights[i*6 + 2];
        offset += 9 * outChannels3x3_r[i] * outChannels3x3[i];

        kOffsetList[i*6 + 3] = (u32)&weightsForInception[offset];
        for(j = 0; j < inChannels[i] * outChannels5x5_r[i]; j++)
            weightsForInception[offset + j] = valuesForWeights[i*6 + 3];
        offset += inChannels[i] * outChannels5x5_r[i];

        kOffsetList[i*6 + 4] = (u32)&weightsForInception[offset];
        for(j = 0; j < 25 * outChannels5x5_r[i] * outChannels5x5[i]; j++)
            weightsForInception[offset + j] = valuesForWeights[i*6 + 4];
        offset += 25 * outChannels5x5_r[i] * outChannels5x5[i];

        kOffsetList[i*6 + 5] = (u32)&weightsForInception[offset];
        for(j = 0; j < inChannels[i] * outChannelsPoolPath[i]; j++)
            weightsForInception[offset + j] = valuesForWeights[i*6 + 5];
        offset += inChannels[i] * outChannelsPoolPath[i];
    }

    // Set input values
    for(i = 0; i < inChannels[0] * inWidth[0] * inHeight[0]; i++)
        input[i] = 0x2d1f;

    for(i = 0; i < CH_MAX * CLS_MAX; i++)
        weightsValuesForFC[i] = 0x1d1f;

}
