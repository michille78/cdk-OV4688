///
/// @file
/// @copyright All code copyright Movidius Ltd 2017, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     MvTensor Test application
///

// Includes
// ----------------------------------------------------------------------------
#include <string.h>
#include <stdio.h>
#include "mv_types.h"
#include "app_config.h"
#include "mvHelpersApi.h"
#include "swcCrc.h"
#include <swcShaveLoaderLocal.h>
#include <DrvLeonL2C.h>
#include <DrvShaveL2Cache.h>
#include <Fp16Convert.h>

// MvTensor specific
#include <UnitTestApi.h>
#include <VcsHooksApi.h>
#include "mvTensor.h"

// Source Specific #defines and types  (typedef,enum,struct)
// ----------------------------------------------------------------------------
#define M_MAX (1280*224)
#define K_MAX 4
#define N_MAX 16
#define KERNEL_SZ 3

#define MAX_SHAVES 12

#define IN_MAX_SIZE (M_MAX * K_MAX)
#define OUT_MAX_SIZE (640 * 112 * N_MAX)
#define WEIGHTS_SIZE (KERNEL_SZ * KERNEL_SZ * K_MAX * N_MAX)

#define LEON_HEAP_SIZE 80000000

#define EXPECTED_CRC 0x6cb053fe

// #define PYTHON_TEST 1

// Global data
// ----------------------------------------------------------------------------
DynamicContext_t localModule[MAX_SHAVES];
DynamicContextInstances_elm localModulePrivD[MAX_SHAVES];

// These dimensions are the same as 3x3 path in Googlenet first 3x3s1 in network STEM
u32 DDR_DATA gInWidth     = 1280;
u32 DDR_DATA gInHeight    = 224;
u32 DDR_DATA gInChannels  = 4;
u32 DDR_DATA gOutChannels = 16;
u32 DDR_DATA gOutWidth    = 640;
u32 DDR_DATA gOutHeight   = 112;

u32 DDR_DATA gConvWidth = 3;
u32 DDR_DATA gConvHeight = 3;

u32 DDR_DATA gOpStrideX = 2;
u32 DDR_DATA gOpStrideY = 2;

u32 DDR_DATA gPostOpStrideX = 1;
u32 DDR_DATA gPostOpStrideY = 1;

u32 DDR_DATA gStartShave = 0;
u32 DDR_DATA gLastShave = 0;

u32 gDataPartitionNo = 0;
u32 gInstrPartitionNo = 1;

float DDR_DATA gCallDuration = 0;

t_mvTensorGenData inputStruct;
t_mvTensorGenData outputStruct;
t_mvTensorGenData weightsStruct;
t_MvTensorMyriadResources myriadResources;
t_MvMatMulMyriadResources matmulParam;
t_MvTensorDebugInfo dbgInfo;
t_MvTensorOp opConv3x3;

char debugMsg[MV_TENSOR_DBG_MSG_SIZE];

fp16 DDR_BSS input[IN_MAX_SIZE]  __attribute__((aligned(64)));
fp16 DDR_DATA output[OUT_MAX_SIZE]  __attribute__((aligned(64)));
fp16 DDR_BSS weights[WEIGHTS_SIZE]  __attribute__((aligned(64)));

char __attribute__((section(".ddr.bss"))) cache_memory[12 * 1024 * 1024];
char __attribute__((section(".cmx.bss"))) scratch_memory[55 * 1024];

u8 DDR_BSS leonHeap[LEON_HEAP_SIZE];
dmaTransactionList_t __attribute__((section(".cmx.cdmaDescriptors"))) task[1];

#define MVTENSOR_APP_INSTANCES_NO MAX_SHAVES

// Functions Definitions
// ----------------------------------------------------------------------------
static void initMvTensorStruct(t_MvTensorParam *mvTensorParam);

#ifndef PYTHON_TEST
static void generateDummyTestData();
#endif

// Functions Implementation
// ----------------------------------------------------------------------------
int main(void)
{
    initClocksAndMemory();
    mvSetHeap((unsigned int)leonHeap, LEON_HEAP_SIZE);

    int shvNo;
    for(shvNo = 0; shvNo < MAX_SHAVES; shvNo++)
    {
        memcpy(&localModule[shvNo], &MODULE_DATA(mvTensor),sizeof(DynamicContext_t));
        memcpy(&localModulePrivD[shvNo], MODULE_DATA(mvTensor).instancesData, sizeof(localModulePrivD[shvNo]));
        localModule[shvNo].instancesData = &localModulePrivD[shvNo];
    }

    t_MvTensorParam mvTensorParam;

#ifndef PYTHON_TEST
    unitTestInit();

    for(int lastShv = 0; lastShv < MAX_SHAVES; lastShv++)
    {
        gLastShave = lastShv;
        generateDummyTestData();
#endif

        initMvTensorStruct(&mvTensorParam);

        swcShaveUnit_t svuList[MAX_SHAVES] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 };

        swcSetupDynShaveAppsComplete(&localModule[lastShv], svuList, MVTENSOR_APP_INSTANCES_NO);

        mvTensor(&mvTensorParam);

        swcCleanupDynShaveApps(&localModule[lastShv]);

        printf("%s\n", mvTensorParam.debugInfo->debugMsg);
        printf("MvTensor done in: %f ms on %i SHAVES\n", mvTensorParam.debugInfo->ms, ((int)gLastShave +1));
        gCallDuration += mvTensorParam.debugInfo->ms;

        DrvLL2CFlushOpOnAddrRange(LL2C_OPERATION_INVALIDATE, 0,
                                  (u32)output, (u32)output + sizeof(output));

#ifndef PYTHON_TEST
        u32 crc = swcCalcCrc32((u8*)(output), (gOutWidth * gOutHeight * gOutChannels) / 2, le_pointer);
        unitTestAssert(crc == EXPECTED_CRC);
    }

    saveMemoryToFile((u32)(output), gOutChannels * gOutWidth * gOutHeight * sizeof(fp16), "outMyriad.bin");
    unitTestFinalReport();
#endif

    return 0;
}

static void initMvTensorStruct(t_MvTensorParam *mvTensorParam)
{
    opConv3x3.type = kConv;
    opConv3x3.radixX = gConvWidth;
    opConv3x3.radixY = gConvHeight;
    opConv3x3.strideX = gOpStrideX;
    opConv3x3.strideY = gOpStrideY;
    opConv3x3.opX = 0;
    opConv3x3.optMask = MV_TENSOR_DEFAULT_OPT | 1 << opt_conv_generic_spatial;
    opConv3x3.paddStyle = paddStyleTFSame;

    // input init
    inputStruct.data = (void*)input;
    inputStruct.dataType = t_fp16;
    inputStruct.dimX = gInWidth;
    inputStruct.dimY = gInHeight;
    inputStruct.dimZ = gInChannels;
    inputStruct.dimXStride = sizeof(fp16) * gInChannels;
    inputStruct.dimYStride = sizeof(fp16) * gInChannels * gInWidth;
    inputStruct.dimZStride = sizeof(fp16);
    inputStruct.storageOrder = orderYXZ;
    mvTensorParam->input = &inputStruct;

    // output init
    outputStruct.data = (void*)(output);
    outputStruct.dataType = t_fp16;
    outputStruct.dimX = gInWidth / gOpStrideX;
    outputStruct.dimY = gInHeight /gOpStrideY;
    outputStruct.dimZ = gOutChannels;
    outputStruct.dimZStride = sizeof(fp16);
    outputStruct.dimXStride = outputStruct.dimZStride * outputStruct.dimZ;
    outputStruct.dimYStride = outputStruct.dimXStride * outputStruct.dimX;
    outputStruct.storageOrder = orderYXZ;
    mvTensorParam->output = &outputStruct;

    // weights init
    weightsStruct.data = (void*)weights;
    weightsStruct.dataType = t_fp16;
    weightsStruct.dimX = gConvWidth * gConvHeight;
    weightsStruct.dimY = gInChannels;
    weightsStruct.dimZ = gOutChannels;
    weightsStruct.dimXStride = sizeof(fp16) * gOutChannels * gInChannels;
    weightsStruct.dimYStride = sizeof(fp16) * gOutChannels;
    weightsStruct.dimZStride = sizeof(fp16);
    weightsStruct.storageOrder = orderXYZ;
    mvTensorParam->weights = &weightsStruct;

    // set operation params
    mvTensorParam->op = &opConv3x3;

    // set post-operation params
    mvTensorParam->postOp = NULL;


    // set myriad resources
    myriadResources.firstShave = gStartShave;
    myriadResources.lastShave = gLastShave;
    myriadResources.dmaLinkAgent = 1;
    myriadResources.dataPartitionNo = gDataPartitionNo;
    myriadResources.instrPartitionNo = gInstrPartitionNo;
    myriadResources.dmaTransactions = &task[0];
    mvTensorParam->myriadResources = &myriadResources;

    mvTensorParam->debugInfo = &dbgInfo;
    mvTensorParam->debugInfo->debugMsg = debugMsg;

    // set matmul resources
    matmulParam.cache_memory_size = sizeof(cache_memory);
    matmulParam.scratch_memory_size = sizeof(scratch_memory);
    matmulParam.cache_memory_ptr = cache_memory;
    matmulParam.scratch_memory_ptr = scratch_memory;
    mvTensorParam->matmulResources = &matmulParam;

}

#ifndef PYTHON_TEST
static void generateDummyTestData()
{
    fp32 ch1_val[2] = {2, 3};
    fp32 ch4_val[2] = {0.5, 0.25};
    fp32 weights_val[3] = {1, 0.5, 0.25};
    u32 j = 0;
    bzero(input, sizeof(input));
    for (u32 i = 0; i < gInChannels * gInWidth * gInHeight; i += gInChannels) {
        input[i] = f32Tof16(ch1_val[j]);
        input[i + 1] = f32Tof16(1);
        input[i + 2] = f32Tof16(0);
        input[i + 3] = f32Tof16(ch4_val[j]);
        j = (j + 1) % 2;
    }
    j = 0;
    bzero(weights, sizeof(weights));
    for (u32 i = 0; i < gInChannels * gOutChannels * gConvWidth * gConvHeight ; i++) {
        weights[i] = f32Tof16(weights_val[j]);
        j = (j + 1) % 3;
    }
    bzero(output, sizeof(output));
}
#endif
