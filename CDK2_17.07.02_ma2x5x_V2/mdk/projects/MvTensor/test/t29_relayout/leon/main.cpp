///


/// @file
/// @copyright All code copyright Movidius Ltd 2012, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     MvTensor input re-layout test application
///

// Includes
// ----------------------------------------------------------------------------
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <DrvLeonL2C.h>
#include <Fp16Convert.h>
#include "mv_types.h"
#include "app_config.h"
#include "mvHelpersApi.h"
#include "swcCrc.h"
#include <swcShaveLoaderLocal.h>

// MvTensor specific
#include <UnitTestApi.h>
#include <VcsHooksApi.h>
#include "mvTensor.h"
#include <mvRelayoutParam.h>

// Source Specific #defines and types  (typedef,enum,struct)
// ----------------------------------------------------------------------------
//#define DISPLAY_INPUT
//#define DISPLAY_OUTPUT

// Makes it easier to read the code
#define NO_TESTS 6

// MAX_CHANNELS corresponding to MAX_WIDHT and MAX_HEIGHT
#define MAX_CHANNELS 3
#define MAX_WIDHT  224
#define MAX_HEIGHT 224
#define MAX_RADIX  7

#define IN_MAX_SIZE  (MAX_WIDHT * MAX_HEIGHT * MAX_CHANNELS)
#define OUT_MAX_SIZE (IN_MAX_SIZE * MAX_RADIX * MAX_RADIX)

#define LEON_HEAP_SIZE 80000000

// #define PYTHON_TEST 1

// Global data
// ----------------------------------------------------------------------------
u32 DDR_DATA gConvWidth[NO_TESTS]  = {3, 3, 5, 5, 7, 7};
u32 DDR_DATA gConvHeight[NO_TESTS] = {3, 3, 5, 5, 7, 7};
u32 DDR_DATA gOpStrideX[NO_TESTS]  = {1, 2, 1, 2, 1, 2};
u32 DDR_DATA gOpStrideY[NO_TESTS]  = {1, 2, 1, 2, 1, 2};

u32 DDR_DATA gInWidth[NO_TESTS]    = {14, 14, 28, 28, 64, 64};
u32 DDR_DATA gInHeight[NO_TESTS]   = {14, 14, 28, 28, 64, 64};
u32 DDR_DATA gInChannels[NO_TESTS] = {16, 32, 16, 32, 1,  3 };

// To test transpoed convolution relayout set
// transposed_convolution = true; and change in mvTensor.cpp
// the operation for kRelayout to TRANSPOSED_CONVOLUTION
bool transposed_convolution = false;
s32 gPadStyle = VALID;
//s32 gPadStyle = SAME;

// Output padding != 0 not supported.
u32 outputPadding = 0;

u32 DDR_DATA gStartShave = 0;
u32 DDR_DATA gLastShave = LAST_SHAVE;

u32 gDataPartitionNo = 0;
u32 gInstrPartitionNo = 1;

float DDR_DATA gCallDuration = 0;

t_mvTensorGenData inputStruct;
t_mvTensorGenData outputStruct;
t_MvTensorMyriadResources myriadResources;
t_MvTensorDebugInfo dbgInfo;
t_MvTensorOp op;

fp16 DDR_BSS input[IN_MAX_SIZE]  __attribute__((aligned(32)));
fp16 DDR_BSS output[OUT_MAX_SIZE]  __attribute__((aligned(32)));

u8 DDR_BSS leonHeap[LEON_HEAP_SIZE];

u32 *expected_crc;
u32 expected_crc_conv_same[NO_TESTS] = {0xd6e02b87, 0x1658ef69, 0x65ef8571, 0x59e25a4d, 0xa2819459, 0x6bca934f};
u32 expected_crc_conv_valid[NO_TESTS] = {0xcd071e06, 0xdeb2a1f7, 0xdca48662, 0x3c6821ff, 0xd82919a1, 0x8ee0809f};

u32 expected_crc_trans_conv_same[NO_TESTS] = {0xd6e02b87, 0x947e34fd, 0x65ef8571, 0x49ccbca7, 0xa2819459, 0xb3f6da6a};
u32 expected_crc_trans_conv_valid[NO_TESTS] = {0xde621dd7, 0x432a2b9d, 0xbd7a897b, 0xbdef0aa3, 0x8d7e888, 0x43ddd4a9};

char debugMsg[MV_TENSOR_DBG_MSG_SIZE];
dmaTransactionList_t __attribute__((section(".cmx.cdmaDescriptors"))) task[1];

DynamicContext_t localModule[MAX_SHAVES];
DynamicContextInstances_elm localModulePrivD[MAX_SHAVES];

// Functions Definitions
// ----------------------------------------------------------------------------
static void initMvTensorStruct(t_MvTensorParam *mvTensorParam, s32 test_no);

#ifndef PYTHON_TEST
static void generateDummyTestData(s32 test_no);
static void checkResult(s32 test_no, bool saveOutput);
#endif
// Functions Implementation
// ----------------------------------------------------------------------------

int main(void)
{
    initClocksAndMemory();
    mvSetHeap((unsigned int)leonHeap, LEON_HEAP_SIZE);

    int shvNo;
    for(shvNo = 0; shvNo < MAX_SHAVES; shvNo++)
    {
        memcpy(&localModule[shvNo], &MODULE_DATA(mvTensor),sizeof(DynamicContext_t));
        memcpy(&localModulePrivD[shvNo], MODULE_DATA(mvTensor).instancesData, sizeof(localModulePrivD[shvNo]));
        localModule[shvNo].instancesData = &localModulePrivD[shvNo];
        swcSetupDynShaveAppsComplete(&localModule[shvNo], &(getShaveList()[shvNo]), 1);
    }

    t_MvTensorParam mvTensorParam;
    if(transposed_convolution)
        expected_crc = gPadStyle == VALID ? expected_crc_trans_conv_valid : expected_crc_trans_conv_same;
    else
        expected_crc = gPadStyle == VALID ? expected_crc_conv_valid : expected_crc_conv_same;

#ifndef PYTHON_TEST
    unitTestInit();
#endif


    for(s32 test_i = 0; test_i < NO_TESTS; ++test_i)
    {
#ifndef PYTHON_TEST
        for(int lastShv = 0; lastShv < MAX_SHAVES; lastShv++)
        {
            generateDummyTestData(test_i);
            gLastShave = lastShv;
#endif
            initMvTensorStruct(&mvTensorParam, test_i);

            s32 outWidth = 0, outHeight = 0, outChannels = 0;
            if(gPadStyle == SAME)
            {
                outWidth    = (gInWidth[test_i] + gOpStrideX[test_i] - 1) / gOpStrideX[test_i];
                outHeight   = (gInHeight[test_i] + gOpStrideY[test_i] - 1) / gOpStrideY[test_i];
                outChannels = gInChannels[test_i] * gConvWidth[test_i] * gConvHeight[test_i];
            }
            else if(gPadStyle == VALID)
            {
                outWidth    = (gInWidth[test_i] - gConvWidth[test_i] + 1 + gOpStrideX[test_i] - 1) / gOpStrideX[test_i];
                outHeight   = (gInHeight[test_i] - gConvHeight[test_i] + 1 + gOpStrideY[test_i] - 1) / gOpStrideY[test_i];
                outChannels = gInChannels[test_i] * gConvWidth[test_i] * gConvHeight[test_i];
            }

            memset((void *)output, 0x55, OUT_MAX_SIZE * sizeof(fp16));

            printf("Relayout %ldx%ldx%ld->%ldx%ldx%ld on %lu shaves (%lu:%lu)\n",
                    gInWidth[test_i], gInHeight[test_i], gInChannels[test_i],
                    outWidth, outHeight, outChannels,
                    gLastShave - gStartShave + 1, gStartShave, gLastShave);

            mvTensor(&mvTensorParam);
            printf("%s\n", mvTensorParam.debugInfo->debugMsg);
            printf("Execution time: %f ms\n", mvTensorParam.debugInfo->ms);
            gCallDuration += mvTensorParam.debugInfo->ms;

#ifndef PYTHON_TEST
            checkResult(test_i, false);
        }
#endif
    }

    for(shvNo = 0; shvNo < MAX_SHAVES; shvNo++)
        swcCleanupDynShaveApps(&localModule[shvNo]);

#ifndef PYTHON_TEST
    unitTestFinalReport();
#endif

    return 0;
}

static void initMvTensorStruct(t_MvTensorParam *mvTensorParam, s32 test_no)
{
    op.type = kRelayout;
    op.optMask = MV_TENSOR_DEFAULT_OPT;
    op.radixX = gConvWidth[test_no];
    op.radixY = gConvHeight[test_no];
    op.strideX = gOpStrideX[test_no];
    op.strideY = gOpStrideY[test_no];
    op.padX = 0;
    op.padY = 0;
    op.paddStyle = paddStyleTFValid;
    op.opX = 0;

    // input init
    inputStruct.data = (void*)input;
    inputStruct.dataType = t_fp16;
    inputStruct.dimX = gInWidth[test_no];
    inputStruct.dimY = gInHeight[test_no];
    inputStruct.dimZ = gInChannels[test_no];
    inputStruct.dimXStride = sizeof(fp16) * inputStruct.dimZ;
    inputStruct.dimYStride = sizeof(fp16) * inputStruct.dimZ * inputStruct.dimX;
    inputStruct.dimZStride = sizeof(fp16);
    inputStruct.storageOrder = orderYXZ;
    mvTensorParam->input = &inputStruct;

    // output init
    outputStruct.data = (void*)(output + outputPadding);
    outputStruct.dataType = t_fp16;

    if(gPadStyle == SAME)
    {
        outputStruct.dimX = (gInWidth[test_no] + gOpStrideX[test_no] - 1) / gOpStrideX[test_no];
        outputStruct.dimY = (gInHeight[test_no] + gOpStrideY[test_no] - 1) / gOpStrideY[test_no];
        outputStruct.dimZ = gInChannels[test_no] * gConvWidth[test_no] * gConvHeight[test_no];
    }
    else if(gPadStyle == VALID)
    {
        outputStruct.dimX = (gInWidth[test_no] - gConvWidth[test_no] + 1 + gOpStrideX[test_no] - 1) / gOpStrideX[test_no];
        outputStruct.dimY = (gInHeight[test_no] - gConvHeight[test_no] + 1 + gOpStrideY[test_no] - 1) / gOpStrideY[test_no];
        outputStruct.dimZ = gInChannels[test_no] * gConvWidth[test_no] * gConvHeight[test_no];
    }

    outputStruct.dimXStride = sizeof(fp16) * outputStruct.dimZ;
    outputStruct.dimYStride = sizeof(fp16) * outputStruct.dimZ * outputStruct.dimX;
    outputStruct.dimZStride = sizeof(fp16);
    outputStruct.storageOrder = orderYXZ;
    mvTensorParam->output = &outputStruct;

    mvTensorParam->weights = NULL;

    mvTensorParam->biases = NULL;

    mvTensorParam->preOp = NULL;

    // set operation params
    mvTensorParam->op = &op;

    // set post-operation params
    mvTensorParam->postOp = NULL;

    // set myriad resources
    myriadResources.firstShave = gStartShave;
    myriadResources.lastShave = gLastShave;
    myriadResources.dmaLinkAgent = 1;
    myriadResources.dataPartitionNo = gDataPartitionNo;
    myriadResources.instrPartitionNo = gInstrPartitionNo;
    myriadResources.dmaTransactions = &task[0];
    mvTensorParam->myriadResources = &myriadResources;

    mvTensorParam->debugInfo = &dbgInfo;
    mvTensorParam->debugInfo->debugMsg = debugMsg;
}

#ifndef PYTHON_TEST

static void generateDummyTestData(s32 test_no)
{
    bzero(input, sizeof(input));

    // INPUT
    //     c0 c1 c2 c3 ... c_Ninput
    // w0   1  1  1  1 ...  1
    // w1   2  2  2  2 ...  2
    // w2   3  3  3  3 ...  3
    // w3   4  4  4  4 ...  4
    // ...
    // wWH WH WH WH WH ... WH

#if defined(DISPLAY_INPUT)
    printf("Input volume - channel minor format.\n");
#endif
    for(u32 h_i = 0; h_i < gInHeight[test_no]; ++h_i)
    {
        for(u32 w_i = 0; w_i < gInWidth[test_no]; ++w_i)
        {
            for(u32 c_i = 0; c_i < gInChannels[test_no]; ++c_i)
            {
                u32 index = c_i + (w_i * gInChannels[test_no]) + (h_i * gInWidth[test_no] * gInChannels[test_no]);
                u32 value = w_i + (h_i * gInWidth[test_no]) + 1;
                input[index] = f32Tof16((float)(value));

#if defined(DISPLAY_INPUT)
                printf("%2.0f ", f16Tof32(input[index]));
#endif
            }
#if defined(DISPLAY_INPUT)
            printf("\n");
#endif
        }
#if defined(DISPLAY_INPUT)
        printf("\n");
#endif
    }

    swcLeonDataCacheFlush();
    DrvLL2CFlushOpOnAllLines(LL2C_OPERATION_WRITE_BACK, 0);
}

static void checkResult(s32 test_no, bool saveOutput)
{
    DrvLL2CFlushOpOnAddrRange(LL2C_OPERATION_INVALIDATE, 0,
            (u32)output, (u32)output + sizeof(output));

    // save output data
    s32 outHeight = 0;
    s32 outWidth  = 0;
    s32 outputSize = 0;

//    s32 outHeight = (gInHeight[test_no] + gOpStrideY[test_no] - 1) / gOpStrideY[test_no];
//    s32 outWidth  = (gInWidth[test_no] + gOpStrideX[test_no] - 1) / gOpStrideX[test_no];
//    s32 outputSize = gInChannels[test_no] * outWidth * outHeight *
//            gConvWidth[test_no] * gConvHeight[test_no];

    if(transposed_convolution)
    {
        outHeight = gInHeight[test_no] + (gInHeight[test_no] - 1)*(gOpStrideY[test_no] - 1);
        outWidth = gInWidth[test_no] + (gInWidth[test_no] - 1)*(gOpStrideX[test_no] - 1);
        if(gPadStyle == VALID)
        {
            outWidth += ((gConvWidth[test_no] - 1) - (gConvWidth[test_no] / 2)) * 2;
            outHeight += ((gConvHeight[test_no] - 1) - (gConvHeight[test_no] / 2)) * 2;
        }
        outputSize = gInChannels[test_no] * outWidth * outHeight *
                gConvWidth[test_no] * gConvHeight[test_no];
    }
    else // convolution
    {
        outHeight = (gInHeight[test_no] + gOpStrideY[test_no] - 1) / gOpStrideY[test_no];
        outWidth  = (gInWidth[test_no] + gOpStrideX[test_no] - 1) / gOpStrideX[test_no];
        outputSize = gInChannels[test_no] * outWidth * outHeight *
                gConvWidth[test_no] * gConvHeight[test_no];
    }

#if defined(DISPLAY_OUTPUT)
    s32 gOutChannels = gInChannels[test_no] * gConvWidth[test_no] * gConvHeight[test_no];
#endif

    u32 crc = swcCalcCrc32((u8*)(output), OUT_MAX_SIZE * sizeof(fp16), le_pointer);
    unitTestAssert(crc == expected_crc[test_no]);
#if defined(DISPLAY_OUTPUT)
    printf("crc = %x\n", (unsigned int)crc);
#endif

    if(saveOutput)
        saveMemoryToFile((u32)output, outputSize * sizeof(fp16) , "outMyriad.bin");

#if defined(DISPLAY_OUTPUT)
    printf("Output volume - channel minor format.\n");
    for(s32 h_i = 0; h_i < outHeight; ++h_i)
    {
        for(s32 w_i = 0; w_i < outWidth; ++w_i)
        {
            for(s32 c_i = 0; c_i < (s32)gOutChannels; ++c_i)
            {
                s32 index = c_i + (w_i * gOutChannels) + (h_i * outWidth * gOutChannels);
                printf("%2.0f ", f16Tof32(output[index]));
            }
            printf("\n");
        }
        printf("\n");
    }
#endif
}

#endif
