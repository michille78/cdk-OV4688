///


/// @file
/// @copyright All code copyright Movidius Ltd 2012, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     MvTensor input re-layout test application
///

// Includes
// ----------------------------------------------------------------------------
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <DrvLeonL2C.h>
#include <Fp16Convert.h>
#include "mv_types.h"
#include "app_config.h"
#include "mvHelpersApi.h"
#include "swcCrc.h"
#include <swcShaveLoaderLocal.h>
#include <DrvShaveL2Cache.h>
#include <cmath>
#include <mvMacros.h>

// MvTensor specific
#include <UnitTestApi.h>
#include <VcsHooksApi.h>
#include "mvTensor.h"

// Source Specific #defines and types  (typedef,enum,struct)
// ----------------------------------------------------------------------------
//#define DISPLAY_INPUT
//#define DISPLAY_OUTPUT
#define GET_STATS

#define NO_TESTS        3
#define TESTS_TO_RUN    3
#define MAX_SHAVES      12

// MAX_CHANNELS corresponding to MAX_WIDHT and MAX_HEIGHT
#define MAX_CHANNELS 64
#define MAX_WIDHT   112
#define MAX_HEIGHT  112

#define IN_MAX_SIZE  (MAX_WIDHT * MAX_HEIGHT * MAX_CHANNELS)
#define OUT_MAX_SIZE (IN_MAX_SIZE)

#define LEON_HEAP_SIZE 80000000

// #define PYTHON_TEST 1

// Global data
// ----------------------------------------------------------------------------
DynamicContext_t localModule[MAX_SHAVES];
DynamicContextInstances_elm localModulePrivD[MAX_SHAVES];

s32 DDR_DATA gInWidth[NO_TESTS]         = {28, 56, 56};
s32 DDR_DATA gInHeight[NO_TESTS]        = {28, 56, 56};
s32 DDR_DATA gInChannels[NO_TESTS]      = {192, 64, 64};
s32 DDR_DATA gSize[NO_TESTS]            = {5, 5, 5};
half DDR_DATA gBiases[NO_TESTS][3]      = {{0x3c00, 0x3c00, 0x3a00},   // k, alpha, beta
                                           {0x3c00, 0x3c00, 0x4200},
                                           {0x3c00, 0x3c00, 0x4400}};
const float test_threshold = 0.001f;
const float gSampleStart[NO_TESTS] = {10.0f, 10.0f, 10.0f};
const float gSampleStep[NO_TESTS] =  {0.001f, 0.0001f, 0.0001};

u32 DDR_DATA gStartShave = 0;
u32 DDR_DATA gLastShave = LAST_SHAVE;

u32 gDataPartitionNo = 0;
u32 gInstrPartitionNo = 1;

float DDR_DATA gCallDuration = 0;

t_mvTensorGenData inputStruct;
t_mvTensorGenData biasesStruct;
t_mvTensorGenData outputStruct;
t_MvTensorMyriadResources myriadResources;
t_MvMatMulMyriadResources matmulParam;
t_MvTensorDebugInfo dbgInfo;
t_MvTensorOp op;

fp16 DDR_BSS input[IN_MAX_SIZE]  __attribute__((aligned(32)));
fp16 DDR_BSS output[OUT_MAX_SIZE]  __attribute__((aligned(32)));

char __attribute__((section(".ddr.bss"))) cache_memory[12 * 1024 * 1024];
char __attribute__((section(".cmx.bss"))) scratch_memory[55 * 1024];

u8 DDR_BSS leonHeap[LEON_HEAP_SIZE];

u32 expected_crc[NO_TESTS] = {0x3497cf68, 0xbc04dae6, 0xa401ca6b};

char debugMsg[MV_TENSOR_DBG_MSG_SIZE];

dmaTransactionList_t __attribute__((section(".cmx.cdmaDescriptors"))) task[1];

// Functions Definitions
// ----------------------------------------------------------------------------
static void initMvTensorStruct(t_MvTensorParam *mvTensorParam, s32 test_no);

#ifndef PYTHON_TEST
static void generateDummyTestData(s32 test_no);
static void checkResult(s32 test_no, t_MvTensorParam *mvTensorParam, bool saveOutput);
#endif
// Functions Implementation
// ----------------------------------------------------------------------------

int main(void)
{
    initClocksAndMemory();
    mvSetHeap((unsigned int)leonHeap, LEON_HEAP_SIZE);

    int shvNo;
    for(shvNo = 0; shvNo < MAX_SHAVES; shvNo++)
    {
        memcpy(&localModule[shvNo], &MODULE_DATA(mvTensor),sizeof(DynamicContext_t));
        memcpy(&localModulePrivD[shvNo], MODULE_DATA(mvTensor).instancesData, sizeof(localModulePrivD[shvNo]));
        localModule[shvNo].instancesData = &localModulePrivD[shvNo];
    }

    t_MvTensorParam mvTensorParam;

#ifndef PYTHON_TEST
    unitTestInit();
#endif

    for(s32 test_i = 0; test_i < TESTS_TO_RUN; ++test_i)
    {
#ifndef PYTHON_TEST
        for(s32 lastShv = 0; lastShv < MAX_SHAVES; lastShv++)
        {
            printf("\n-------- TEST %ld, %ld SHAVES -----------\n", test_i, lastShv + 1);
            generateDummyTestData(test_i);
            gLastShave = lastShv;
#endif
            initMvTensorStruct(&mvTensorParam, test_i);

            memset((void *)output, 0x55, OUT_MAX_SIZE * sizeof(fp16));

            printf("LRN %ldx%ldx%ld, [k, alpha, beta] = [%f, %f, %f]\n",
                    gInWidth[test_i], gInHeight[test_i], gInChannels[test_i],
                    f16Tof32((unsigned int)gBiases[test_i][0]), f16Tof32((unsigned int)gBiases[test_i][1]),
                    f16Tof32((unsigned int)gBiases[test_i][2]));

            for(shvNo = 0; shvNo <= lastShv; shvNo++)
                swcSetupDynShaveAppsComplete(&localModule[shvNo], getShaveList() + shvNo, 1);

            mvTensor(&mvTensorParam);
            DrvShaveL2CachePartitionFlushAndInvalidate(0);
            DrvShaveL2CachePartitionInvalidate(1);

            for(shvNo = 0; shvNo <= lastShv; shvNo++)
                swcCleanupDynShaveApps(&localModule[shvNo]);

            printf("%s\n", mvTensorParam.debugInfo->debugMsg);
            printf("Execution time: %f ms\n", mvTensorParam.debugInfo->ms);
            gCallDuration += mvTensorParam.debugInfo->ms;

#ifndef PYTHON_TEST
            checkResult(test_i, &mvTensorParam, false);
        }
#endif
    }

#ifndef PYTHON_TEST
    unitTestFinalReport();
#endif

    return 0;
}

static void initMvTensorStruct(t_MvTensorParam *mvTensorParam, s32 test_no)
{
    op.type = kLRN;
    op.optMask = MV_TENSOR_DEFAULT_OPT;
    op.radixX  = gSize[test_no];
    op.radixY  = gSize[test_no];
    op.strideX = 1;
    op.strideY = 1;
    op.padX    = 0;
    op.padY    = 0;
    op.paddStyle = paddStyleTFValid;
    op.opX     = 0;

    // input init
    inputStruct.data = (void*)input;
    inputStruct.dataType = t_fp16;
    inputStruct.dimX = gInWidth[test_no];
    inputStruct.dimY = gInHeight[test_no];
    inputStruct.dimZ = gInChannels[test_no];
    inputStruct.dimXStride = sizeof(fp16) * inputStruct.dimZ;
    inputStruct.dimYStride = sizeof(fp16) * inputStruct.dimZ * inputStruct.dimX;
    inputStruct.dimZStride = sizeof(fp16);
    inputStruct.storageOrder = orderYXZ;
    mvTensorParam->input = &inputStruct;

    // output init
    outputStruct.data = (void*)(output);
    outputStruct.dataType = t_fp16;
    outputStruct.dimX = gInWidth[test_no];
    outputStruct.dimY = gInHeight[test_no];
    outputStruct.dimZ = gInChannels[test_no];

    outputStruct.dimXStride = sizeof(fp16) * outputStruct.dimZ;
    outputStruct.dimYStride = sizeof(fp16) * outputStruct.dimZ * outputStruct.dimX;
    outputStruct.dimZStride = sizeof(fp16);

    outputStruct.storageOrder = orderYXZ;
    mvTensorParam->output = &outputStruct;

    mvTensorParam->weights = NULL;

    biasesStruct.data = (void*)gBiases[test_no];
    mvTensorParam->biases = &biasesStruct;

    mvTensorParam->preOp = NULL;

    // set operation params
    mvTensorParam->op = &op;

    // set post-operation params
    mvTensorParam->postOp = NULL;

    // set myriad resources
    myriadResources.firstShave = gStartShave;
    myriadResources.lastShave = gLastShave;
    myriadResources.dmaLinkAgent = 1;
    myriadResources.dataPartitionNo = gDataPartitionNo;
    myriadResources.instrPartitionNo = gInstrPartitionNo;
    myriadResources.dmaTransactions = &task[0];
    mvTensorParam->myriadResources = &myriadResources;

    // set matmul resources
    matmulParam.cache_memory_size = sizeof(cache_memory);
    matmulParam.scratch_memory_size = sizeof(scratch_memory);
    matmulParam.cache_memory_ptr = cache_memory;
    matmulParam.scratch_memory_ptr = scratch_memory;
    mvTensorParam->matmulResources = &matmulParam;

    mvTensorParam->debugInfo = &dbgInfo;
    mvTensorParam->debugInfo->debugMsg = debugMsg;
}

#ifndef PYTHON_TEST

static void generateDummyTestData(s32 test_no)
{
    bzero(input, sizeof(input));

    float current = gSampleStart[test_no];
    float input_error_mean = 0.0f;
    float input_error_std  = 0.0f;

#if defined(DISPLAY_INPUT)

    printf("Input volume - row major format.\n");
#endif
    for(s32 c_i = 0; c_i < gInChannels[test_no]; ++c_i)
    {
        for(s32 h_i = 0; h_i < gInHeight[test_no]; ++h_i)
        {
            for(s32 w_i = 0; w_i < gInWidth[test_no]; ++w_i)
            {
                u32 index = c_i + (w_i * gInChannels[test_no]) + (h_i * gInWidth[test_no] * gInChannels[test_no]);
                input[index] = f32Tof16(current);

                input_error_mean += fabs(current - f16Tof32(input[index]));
                input_error_std  += (current - f16Tof32(input[index])) * (current - f16Tof32(input[index]));
                current -= gSampleStep[test_no];

#if defined(DISPLAY_INPUT)
                printf("%2.2f ", f16Tof32(input[index]));
#endif
            }
#if defined(DISPLAY_INPUT)
            printf("\n");
#endif
        }
#if defined(DISPLAY_INPUT)
        printf("\n");
#endif
    }

#if defined(GET_STATS)
    printf("Sampling interval = [%2.5f, %2.5f]\n", current + gSampleStep[test_no], gSampleStart[test_no]);
    float no_samples = (float)(gInChannels[test_no] * gInWidth[test_no] * gInHeight[test_no]);
    printf("no_samples = %.0f\n", no_samples);
    printf("Input error mean = %.6f\n", input_error_mean / no_samples);
    printf("Input error std  = %.6f\n", sqrtf((input_error_std / no_samples) -
            (input_error_mean / no_samples) * (input_error_mean / no_samples)));
#endif

    swcLeonDataCacheFlush();
    DrvLL2CFlushOpOnAllLines(LL2C_OPERATION_WRITE_BACK, 0);
}

static void checkResult(s32 test_no, t_MvTensorParam *mvTensorParam, bool saveOutput)
{
    s32 no_samples  = mvTensorParam->output->dimX * mvTensorParam->output->dimY * mvTensorParam->output->dimZ;
    s32 outChannels = mvTensorParam->output->dimZ;
    s32 outHeight   = mvTensorParam->output->dimY;
    s32 outWidth    = mvTensorParam->output->dimX;
    float output_error_mean = 0.0f;
    float output_error_std  = 0.0f;
    bool threshold_test_failed = true;

    DrvLL2CFlushOpOnAddrRange(LL2C_OPERATION_INVALIDATE, 0,
            (u32)output, (u32)output + sizeof(output));

    u32 crc = swcCalcCrc32((u8*)(output), OUT_MAX_SIZE * sizeof(fp16), le_pointer);
    unitTestAssert(crc == expected_crc[test_no]);

    printf("Output size: %dx%dx%d \n", mvTensorParam->output->dimX, mvTensorParam->output->dimY, mvTensorParam->output->dimZ);
    printf("crc = %x\n", (unsigned int)crc);

    if(saveOutput)
        saveMemoryToFile((u32)output, no_samples * sizeof(fp16) , "outMyriad.bin");

#if defined(DISPLAY_OUTPUT)
    printf("Output volume - row major format.\n");
#endif
    for(s32 c_i = 0; c_i < (s32)outChannels; ++c_i)
    {
        for(s32 h_i = 0; h_i < outHeight; ++h_i)
        {
            for(s32 w_i = 0; w_i < outWidth; ++w_i)
            {
                s32 index       = c_i + (w_i * outChannels) + (h_i * outWidth * outChannels);
                float sum       = 0;
                float k     = f16Tof32((unsigned int)gBiases[test_no][0]);
                float alpha = f16Tof32((unsigned int)gBiases[test_no][1]);
                float beta  = f16Tof32((unsigned int)gBiases[test_no][2]);
                s32 size    = gSize[test_no];

                for (s32 c_r = MAX(c_i-size/2, 0); c_r <= MIN(c_i+size/2, outChannels-1); c_r++ )
                {
                    s32 index_r = c_r + (w_i * outChannels) + (h_i * outWidth * outChannels);
                    float t = f16Tof32((unsigned int)input[index_r]);
                    sum += t*t;
                }

                float gt_value = f16Tof32((unsigned int)input[index]) * pow(k + sum*alpha/size, -beta);
                float output_value = f16Tof32((unsigned int)output[index]);
                output_error_mean += fabs(gt_value - output_value);
                output_error_std  += (gt_value - output_value) * (gt_value - output_value);

#if defined(DISPLAY_OUTPUT)
                printf("%2.2f ", output_value);
//                printf("%2.2f|%2.2f ", output_value, gt_value);
#endif
                if (fabs(output_value - gt_value) > test_threshold)
                    threshold_test_failed = true;
            }
#if defined(DISPLAY_OUTPUT)
            printf("\n");
#endif
        }
#if defined(DISPLAY_OUTPUT)
        printf("----------\n");
#endif
    }

#if defined(GET_STATS)
    printf("Output error mean = %.6f\n", output_error_mean / no_samples);
    printf("Output error std  = %.6f\n", sqrtf((output_error_std / no_samples) -
            (output_error_mean / no_samples) * (output_error_mean / no_samples)));
#endif

    if(!threshold_test_failed)
        unitTestLogFail();
}

#endif
