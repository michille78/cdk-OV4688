#ifndef __INCEPTION_DEFINES__
#define __INCEPTION_DEFINES__

#include "mvTensor.h"

#define FIRST_SHAVE 0
#define DMA_LINK_AGENT 1
#define DATA_SHV_L2_PARTITION 0
#define INSTRUCTION_SHV_L2_PARTITION 1

#define NO_OF_MV_TENSOR_CALLS_INSIDE_INCEPTION 7
#define INCEPTIONS_NO 2

#define WIDTH 28
#define HEIGHT 28

#define I1_IN_CH 192
#define I2_IN_CH 256
#define I1_CONV1x1_CH 64
#define I2_CONV1x1_CH 128
#define I1_CONV3x3_R_CH 96
#define I2_CONV3x3_R_CH 128
#define I1_CONV3x3_CH 128
#define I2_CONV3x3_CH 192
#define I1_CONV5x5_R_CH 16
#define I2_CONV5x5_R_CH 32
#define I1_CONV5x5_CH 32
#define I2_CONV5x5_CH 96
#define I1_PP_CH 32
#define I2_PP_CH 64

#define OUT_CHANNELS (I2_CONV1x1_CH + I2_CONV3x3_CH + I2_CONV5x5_CH + I2_PP_CH)

#define I1_WEIGHTS_SIZE (I1_IN_CH*I1_CONV1x1_CH  + I1_IN_CH* I1_CONV3x3_R_CH+ I1_CONV3x3_R_CH*9*I1_CONV3x3_CH \
                         + I1_IN_CH*I1_CONV5x5_R_CH+I1_CONV5x5_R_CH*25*I1_CONV5x5_CH  + I1_IN_CH*I1_PP_CH)
#define I2_WEIGHTS_SIZE (I2_IN_CH*I2_CONV1x1_CH + I2_IN_CH*I2_CONV3x3_R_CH+I2_CONV3x3_R_CH*9*I2_CONV3x3_CH \
                        + I2_IN_CH*I2_CONV5x5_R_CH+I2_CONV5x5_R_CH*25*I2_CONV5x5_CH  + I2_IN_CH*I2_PP_CH)

#define WEIGHTS_SIZE (I1_WEIGHTS_SIZE + I2_WEIGHTS_SIZE)
#define IN_SIZE (I1_IN_CH * WIDTH * HEIGHT)
#define OUT_SIZE (OUT_CHANNELS * WIDTH*HEIGHT + 4 * OUT_CHANNELS * (WIDTH + 2))

#define AUX2_MAX_SZ (I2_CONV3x3_R_CH * WIDTH * HEIGHT)
#define AUX3_MAX_SZ (I2_CONV5x5_R_CH * WIDTH * HEIGHT)
#define AUX4_MAX_SZ (I2_IN_CH * WIDTH * HEIGHT)

u32 DDR_DATA inChannels[INCEPTIONS_NO] = {I1_IN_CH, I2_IN_CH};
u32 DDR_DATA inWidth[INCEPTIONS_NO] = {WIDTH, WIDTH};
u32 DDR_DATA inHeight[INCEPTIONS_NO] = {HEIGHT, HEIGHT};
u32 DDR_DATA outChannels1x1[INCEPTIONS_NO] = {I1_CONV1x1_CH, I2_CONV1x1_CH};
u32 DDR_DATA outChannels3x3_r[INCEPTIONS_NO] = {I1_CONV3x3_R_CH, I2_CONV3x3_R_CH};
u32 DDR_DATA outChannels3x3[INCEPTIONS_NO] = {I1_CONV3x3_CH, I2_CONV3x3_CH};
u32 DDR_DATA outChannels5x5_r[INCEPTIONS_NO] = {I1_CONV5x5_R_CH, I2_CONV5x5_R_CH};
u32 DDR_DATA outChannels5x5[INCEPTIONS_NO] = {I1_CONV5x5_CH, I2_CONV5x5_CH};
u32 DDR_DATA outChannelsPoolPath[INCEPTIONS_NO] = {I1_PP_CH, I2_PP_CH};

u32 DDR_DATA kOffsetList[12];

fp16 DDR_DATA input[IN_SIZE];
fp16 DDR_DATA output[OUT_SIZE];
fp16 DDR_DATA weights[WEIGHTS_SIZE];
fp16 DDR_BSS auxPath2[AUX2_MAX_SZ];
fp16 DDR_BSS auxPath3[AUX3_MAX_SZ];
fp16 DDR_BSS auxPath4[AUX4_MAX_SZ];
fp16 DDR_BSS auxInOut[OUT_SIZE];

fp16 *inPtr[INCEPTIONS_NO];
fp16 *outPtr[INCEPTIONS_NO];

char debugMsg[MV_TENSOR_DBG_MSG_SIZE];

char __attribute__((section(".ddr.bss"))) cache_memory[25 * 1024 * 1024];
char __attribute__((section(".cmx.bss"))) scratch_memory[110 * 1024];

dmaTransactionList_t __attribute__((section(".cmx.cdmaDescriptors"))) task[1];

//###################################### inception struct ################################
typedef struct
{
    void *input;
    void *output;
    void *weights[6];
    void *path2Aux;
    void *path3Aux;
    void *path4Aux;
    t_MvTensorDebugInfo *dbgInfoPtr0;
    t_MvTensorDebugInfo *dbgInfoPtr1;
    t_MvTensorDebugInfo *dbgInfoPtr2;
    t_MvTensorDebugInfo *dbgInfoPtr3;
    t_MvTensorDebugInfo *dbgInfoPtr4;
    t_MvTensorDebugInfo *dbgInfoPtr5;
    t_MvTensorDebugInfo *dbgInfoPtr6;
    u32 inChannels;
    u32 inWidth;
    u32 inHeight;
    u32 conv1x1OutChannels;
    u32 conv3x3ROutChannels;
    u32 conv3x3OutChannels;
    u32 conv5x5ROutChannels;
    u32 conv5x5OutChannels;
    u32 poolPathOutChannels;
    u32 bpp;
    t_MvTensorDataType dataType;
}t_inception;

//########################################## shared structures MvTensor ########################################

static t_MvTensorMyriadResources myriadResourcesInception =
{{{
    .firstShave = FIRST_SHAVE,
    .lastShave = LAST_SHAVE,
    .dmaLinkAgent = DMA_LINK_AGENT,
    .dataPartitionNo = DATA_SHV_L2_PARTITION,
    .instrPartitionNo = INSTRUCTION_SHV_L2_PARTITION,
    .dmaTransactions = &task[0]
}}};

static t_MvTensorDebugInfo debugStruct = {
    .ms = 0,
    .debugMsg = debugMsg
};

static t_MvMatMulMyriadResources matmulResources =
{
    .cache_memory_size = sizeof(cache_memory),
    .scratch_memory_size = sizeof(scratch_memory),
    .cache_memory_ptr = cache_memory,
    .scratch_memory_ptr = scratch_memory
};

//################################################ PATH I conv1x1 ######################################################
static t_mvTensorGenData inputForAllPaths;
static t_mvTensorGenData outputPath1;
static t_mvTensorGenData weightsPath1;

static t_MvTensorOp opConv1x1 =
{{{
    .type = kConv,
    .optMask = MV_TENSOR_DEFAULT_OPT,
    .radixX = 1,
    .radixY = 1,
    .strideX = 1,
    .strideY = 1,
    .padX = 0,
    .padY = 0,
    .paddStyle = paddStyleCaffe,
    .opX = 0,
    .params = NULL
}}};

static t_MvTensorOp opRelu =
{{{
    .type = kRelu,
    .optMask = MV_TENSOR_DEFAULT_OPT,
    .radixX = 1,
    .radixY = 1,
    .strideX = 1,
    .strideY = 1,
    .padX = 0,
    .padY = 0,
    .paddStyle = paddStyleNone,
    .opX = 0,
    .params = NULL
}}};

//################################################ PATH II conv1x1 ######################################################
static t_mvTensorGenData outputPath2Conv1x1;
static t_mvTensorGenData weightsPath2Conv1x1;

//################################################ PATH II conv3x3 ######################################################
static t_mvTensorGenData outputPath2Conv3x3;
static t_mvTensorGenData weightsPath2Conv3x3;

static t_MvTensorOp opConv3x3 =
{{{
    .type = kConv,
    .optMask = MV_TENSOR_DEFAULT_OPT,
    .radixX = 3,
    .radixY = 3,
    .strideX = 1,
    .strideY = 1,
    .padX = 1,
    .padY = 1,
    .paddStyle = paddStyleCaffe,
    .opX = 0,
    .params = NULL
}}};

//################################################ PATH III conv1x1 ######################################################
static t_mvTensorGenData outputPath3Conv1x1;
static t_mvTensorGenData weightsPath3Conv1x1;

//################################################ PATH III conv5x5 ######################################################
static t_mvTensorGenData outputPath3Conv5x5;
static t_mvTensorGenData weightsPath3Conv5x5;

static t_MvTensorOp opConv5x5 =
{{{
    .type = kConv,
    .optMask = MV_TENSOR_DEFAULT_OPT,
    .radixX = 5,
    .radixY = 5,
    .strideX = 1,
    .strideY = 1,
    .padX = 2,
    .padY = 2,
    .paddStyle = paddStyleCaffe,
    .opX = 0,
    .params = NULL
}}};

//################################################ PATH IV max pool ######################################################
static t_mvTensorGenData outputPath4MaxPool;

static t_MvTensorOp opMaxPool3x3 =
{{{
    .type = kMaxPool,
    .optMask = MV_TENSOR_DEFAULT_OPT,
    .radixX = 3,
    .radixY = 3,
    .strideX = 1,
    .strideY = 1,
    .padX = 1,
    .padY = 1,
    .paddStyle = paddStyleCaffe,
    .opX = 0,
    .params = NULL
}}};

//################################################ PATH IV conv1x1 #######################################################
static t_mvTensorGenData outputPath4Conv1x1;
static t_mvTensorGenData weightsPath4Conv1x1;

//######################### Array with the 7*2 structs needed for inception 3A and 3B ####################################

static t_MvTensorParam mvTensorParamArray[INCEPTIONS_NO*NO_OF_MV_TENSOR_CALLS_INSIDE_INCEPTION]
{
    //conv1x1
    {{{
        .input = &inputForAllPaths,
        .output = &outputPath1,
        .weights = &weightsPath1,
        .accumulation = NULL,
        .biases = NULL,
        .preOp = NULL,
        .op = &opConv1x1,
        .postOp = &opRelu,
        .myriadResources = &myriadResourcesInception,
        .debugInfo = NULL,
        .matmulResources = &matmulResources
    }}},

    //conv3x3r
    {{{
        .input = &inputForAllPaths,
        .output = &outputPath2Conv1x1,
        .weights = &weightsPath2Conv1x1,
        .accumulation = NULL,
        .biases = NULL,
        .preOp = NULL,
        .op = &opConv1x1,
        .postOp = &opRelu,
        .myriadResources = &myriadResourcesInception,
        .debugInfo = NULL,
        .matmulResources = &matmulResources
    }}},

    //conv3x3
    {{{
        .input = &outputPath2Conv1x1,
        .output = &outputPath2Conv3x3,
        .weights = &weightsPath2Conv3x3,
        .accumulation = NULL,
        .biases = NULL,
        .preOp = NULL,
        .op = &opConv3x3,
        .postOp = &opRelu,
        .myriadResources = &myriadResourcesInception,
        .debugInfo = NULL,
        .matmulResources = &matmulResources
    }}},

    //conv5x5r
    {{{
        .input = &inputForAllPaths,
        .output = &outputPath3Conv1x1,
        .weights = &weightsPath3Conv1x1,
        .accumulation = NULL,
        .biases = NULL,
        .preOp = NULL,
        .op = &opConv1x1,
        .postOp = &opRelu,
        .myriadResources = &myriadResourcesInception,
        .debugInfo = NULL,
        .matmulResources = &matmulResources
    }}},

    //conv5x5
    {{{
        .input = &outputPath3Conv1x1,
        .output = &outputPath3Conv5x5,
        .weights = &weightsPath3Conv5x5,
        .accumulation = NULL,
        .biases = NULL,
        .preOp = NULL,
        .op = &opConv5x5,
        .postOp = &opRelu,
        .myriadResources = &myriadResourcesInception,
        .debugInfo = NULL,
        .matmulResources = &matmulResources
    }}},

    //maxPool
    {{{
        .input = &inputForAllPaths,
        .output = &outputPath4MaxPool,
        .weights = NULL,
        .accumulation = NULL,
        .biases = NULL,
        .preOp = NULL,
        .op = &opMaxPool3x3,
        .postOp = &opRelu,
        .myriadResources = &myriadResourcesInception,
        .debugInfo = NULL,
        .matmulResources = &matmulResources
    }}},

    //conv1x1
    {{{
        .input = &outputPath4MaxPool,
        .output = &outputPath4Conv1x1,
        .weights = &weightsPath4Conv1x1,
        .accumulation = NULL,
        .biases = NULL,
        .preOp= NULL,
        .op = &opConv1x1,
        .postOp = &opRelu,
        .myriadResources = &myriadResourcesInception,
        .debugInfo = NULL,
        .matmulResources = &matmulResources
    }}}

};

#endif // __INCEPTION_DEFINES__
