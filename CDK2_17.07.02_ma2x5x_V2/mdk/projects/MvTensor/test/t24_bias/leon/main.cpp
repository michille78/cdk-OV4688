///
/// @file
/// @copyright All code copyright Movidius Ltd 2012, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     MvTensor Test application
///

// Includes
// ----------------------------------------------------------------------------
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <DrvLeonL2C.h>
#include <Fp16Convert.h>
#include "mv_types.h"
#include "app_config.h"
#include "mvHelpersApi.h"
#include <swcShaveLoaderLocal.h>

// MvTensor specific
#include <UnitTestApi.h>
#include <VcsHooksApi.h>
#include "mvTensor.h"

// Source Specific #defines and types  (typedef,enum,struct)
// ----------------------------------------------------------------------------
//#define DISPLAY_DATA

// Makes it easier to read the code
#define FP16_1  0x3C00
#define FP16_0  0x0000

#define IN_ODD_COL_VAL              FP16_1
#define IN_EVEN_COL_VAL             FP16_0

#define M_MAX 3136
#define K_MAX 1024
#define N_MAX 1024

#define IN_MAX_SIZE (M_MAX * K_MAX)
#define OUT_MAX_SIZE (M_MAX * N_MAX)
#define WEIGHTS_SIZE (K_MAX * N_MAX)

#define LEON_HEAP_SIZE 80000000

// #define PYTHON_TEST 1
#define MAX_SHAVES 12

// Global data
// ----------------------------------------------------------------------------
DynamicContext_t localModule[MAX_SHAVES];
DynamicContextInstances_elm localModulePrivD[MAX_SHAVES];

u32 DDR_DATA gInWidth = 77;
u32 DDR_DATA gInHeight = 55;
u32 DDR_DATA gInChannels = 33;
u32 DDR_DATA gOutChannels = 99;

const u32 used_channels = gOutChannels >> 1;
// On the last gUnusedChannels do not apply the post operation (i.e. simulate stride).
u32 DDR_DATA gUsedChannels = used_channels;

// Convolution layer parameters must be 1 for the test to pass.
// Also it is easier to understand the output of the bias operation.
u32 DDR_DATA gConvWidth = 1;
u32 DDR_DATA gConvHeight = 1;
u32 DDR_DATA gOpStrideX = 1;
u32 DDR_DATA gOpStrideY = 1;

// Output padding != 0 not supported.
u32 outputPadding = 0;

u32 DDR_DATA gPostOpStrideX = 1;
u32 DDR_DATA gPostOpStrideY = 1;

u32 DDR_DATA gStartShave = 0;
u32 DDR_DATA gLastShave = LAST_SHAVE;

u32 gDataPartitionNo = 0;
u32 gInstrPartitionNo = 1;

float DDR_DATA gCallDuration = 0;

t_mvTensorGenData inputStruct;
t_mvTensorGenData outputStruct;
t_mvTensorGenData weightsStruct;
t_mvTensorGenData biasesStruct;
t_MvTensorMyriadResources myriadResources;
t_MvTensorDebugInfo dbgInfo;
t_MvMatMulMyriadResources matmulParam;
t_MvTensorOp op;
t_MvTensorOp postOp;

fp16 DDR_BSS input[IN_MAX_SIZE]  __attribute__((aligned(32)));
fp16 DDR_BSS output[OUT_MAX_SIZE]  __attribute__((aligned(32)));
fp16 DDR_BSS weights[WEIGHTS_SIZE]  __attribute__((aligned(32)));
fp16 DDR_BSS biases[IN_MAX_SIZE] __attribute__((aligned(32)));

u8 DDR_BSS leonHeap[LEON_HEAP_SIZE];

char debugMsg[MV_TENSOR_DBG_MSG_SIZE];

char __attribute__((section(".ddr.bss"))) cache_memory[12 * 1024 * 1024];
char __attribute__((section(".cmx.bss"))) scratch_memory[55 * 1024];
dmaTransactionList_t __attribute__((section(".cmx.cdmaDescriptors"))) task[1];

u32 outputSize;

// Functions Definitions
// ----------------------------------------------------------------------------
static void initMvTensorStruct(t_MvTensorParam *mvTensorParam);

#ifndef PYTHON_TEST
static void generateDummyTestData();
static void checkResult();
#endif
// Functions Implementation
// ----------------------------------------------------------------------------

int main(void)
{
    initClocksAndMemory();
    mvSetHeap((unsigned int)leonHeap, LEON_HEAP_SIZE);

    int shvNo;
    for(shvNo = 0; shvNo < MAX_SHAVES; shvNo++)
    {
        memcpy(&localModule[shvNo], &MODULE_DATA(mvTensor),sizeof(DynamicContext_t));
        memcpy(&localModulePrivD[shvNo], MODULE_DATA(mvTensor).instancesData, sizeof(localModulePrivD[shvNo]));
        localModule[shvNo].instancesData = &localModulePrivD[shvNo];
    }

    t_MvTensorParam mvTensorParam;
    initMvTensorStruct(&mvTensorParam);

#ifndef PYTHON_TEST
    unitTestInit();
#endif



#ifndef PYTHON_TEST
    for(int lastShv = 0; lastShv < MAX_SHAVES; lastShv++)
    {
        generateDummyTestData();
        mvTensorParam.myriadResources->lastShave = lastShv;
#endif
        swcSetupDynShaveAppsComplete(&localModule[lastShv], getShaveList(), lastShv+1);
        bzero(output + outputPadding, gOutChannels * gInWidth * gInHeight * sizeof(fp16));
        printf("Bias. stride = width (%lu:%lu)\n", gStartShave, gLastShave);
        gUsedChannels = gOutChannels;
        mvTensorParam.output->dimZ = gOutChannels;
        mvTensor(&mvTensorParam);
        printf("%s\n", mvTensorParam.debugInfo->debugMsg);
        printf("MvTensor done in: %f ms on %d SHAVES\n", mvTensorParam.debugInfo->ms,
                                                         mvTensorParam.myriadResources->lastShave + 1);
        gCallDuration += mvTensorParam.debugInfo->ms;
        swcCleanupDynShaveApps(&localModule[lastShv]);
#ifndef PYTHON_TEST
        checkResult();
    }
#endif

#ifndef PYTHON_TEST
    for(int lastShv = 0; lastShv < MAX_SHAVES; lastShv++)
    {
        generateDummyTestData();
        mvTensorParam.myriadResources->lastShave = lastShv;
#endif
        swcSetupDynShaveAppsComplete(&localModule[lastShv], getShaveList(), lastShv+1);
        bzero(output + outputPadding, gOutChannels * gInWidth * gInHeight * sizeof(fp16));
        printf("Bias. stride > width (%lu:%lu)\n", gStartShave, gLastShave);
        gUsedChannels = used_channels;
        mvTensorParam.output->dimZ = gUsedChannels;
        mvTensorParam.weights->dimZ = gUsedChannels;
        mvTensor(&mvTensorParam);
        printf("%s\n", mvTensorParam.debugInfo->debugMsg);
        printf("MvTensor done in: %f ms on %d SHAVES\n", mvTensorParam.debugInfo->ms,
                                                         mvTensorParam.myriadResources->lastShave + 1);
        gCallDuration += mvTensorParam.debugInfo->ms;
        swcCleanupDynShaveApps(&localModule[lastShv]);
#ifndef PYTHON_TEST
        checkResult();
    }
#endif



#ifndef PYTHON_TEST
    saveMemoryToFile((u32)output, outputSize * sizeof(fp16) , "outMyriad.bin");
    unitTestFinalReport();
#endif

    return 0;
}

static void initMvTensorStruct(t_MvTensorParam *mvTensorParam)
{
    op.type = kConv;
    op.optMask = MV_TENSOR_DEFAULT_OPT;
    op.radixX = gConvWidth;
    op.radixY = gConvHeight;
    op.strideX = gOpStrideX;
    op.strideY = gOpStrideY;
    op.padX = 0;
    op.padY = 0;
    op.paddStyle = paddStyleCaffe;
    op.opX = 0;

    postOp.type = kBias;
    postOp.radixX = 1;
    postOp.radixY = 1;
    postOp.strideX = 1;
    postOp.strideY = 1;
    op.padX = 0;
    op.padY = 0;
    op.paddStyle = paddStyleNone;
    postOp.opX = 0.0f;

    // input init
    inputStruct.data = (void*)input;
    inputStruct.dataType = t_fp16;
    inputStruct.dimX = gInWidth;
    inputStruct.dimY = gInHeight;
    inputStruct.dimZ = gInChannels;
    inputStruct.dimXStride = sizeof(fp16) * gInChannels;
    inputStruct.dimYStride = sizeof(fp16) * gInChannels * gInWidth;
    inputStruct.dimZStride = sizeof(fp16);
    inputStruct.storageOrder = orderYXZ;
    mvTensorParam->input = &inputStruct;

    // output init
    outputStruct.data = (void*)(output + outputPadding);
    outputStruct.dataType = t_fp16;
    outputStruct.dimX = gInWidth / gOpStrideX;
    outputStruct.dimY = gInHeight /gOpStrideY;
    outputStruct.dimZ = gOutChannels;
    outputStruct.dimZStride = sizeof(fp16);
    outputStruct.dimXStride = outputStruct.dimZStride * outputStruct.dimZ;
    outputStruct.dimYStride = outputStruct.dimXStride * outputStruct.dimX;
    outputStruct.storageOrder = orderYXZ;
    mvTensorParam->output = &outputStruct;

    // weights init
    weightsStruct.data = (void*)weights;
    weightsStruct.dataType = t_fp16;
    weightsStruct.dimX = gConvWidth * gConvHeight;
    weightsStruct.dimY = gInChannels;
    weightsStruct.dimZ = gOutChannels;
    weightsStruct.dimXStride = sizeof(fp16) * gOutChannels * gInChannels;
    weightsStruct.dimYStride = sizeof(fp16) * gOutChannels;
    weightsStruct.dimZStride = sizeof(fp16);
    weightsStruct.storageOrder = orderXYZ;
    mvTensorParam->weights = &weightsStruct;

    biasesStruct.data = (void*)biases;
    biasesStruct.dataType = t_fp16;
    // todo: Add proper sizes for the biases struct.
    // Though it seems they are not used.
    biasesStruct.dimX = gConvWidth * gConvHeight;
    biasesStruct.dimY = gInChannels;
    biasesStruct.dimZ = gOutChannels;
    biasesStruct.dimXStride = sizeof(fp16) * gInChannels;
    biasesStruct.dimYStride = sizeof(fp16) * gInChannels * gInWidth;
    biasesStruct.dimZStride = sizeof(fp16);
    biasesStruct.storageOrder = orderYXZ;
    mvTensorParam->biases = &biasesStruct;

    // set pre-operation params
    mvTensorParam->preOp = NULL;

    // set operation params
    mvTensorParam->op = &op;

    // set post-operation params
    mvTensorParam->postOp = &postOp;

    // set myriad resources
    myriadResources.firstShave = gStartShave;
    myriadResources.lastShave = gLastShave;
    myriadResources.dmaLinkAgent = 1;
    myriadResources.dataPartitionNo = gDataPartitionNo;
    myriadResources.instrPartitionNo = gInstrPartitionNo;
    myriadResources.dmaTransactions = &task[0];
    mvTensorParam->myriadResources = &myriadResources;

    mvTensorParam->debugInfo = &dbgInfo;
    mvTensorParam->debugInfo->debugMsg = debugMsg;

    // set MvMatMul resources
    matmulParam.cache_memory_size = sizeof(cache_memory);
    matmulParam.scratch_memory_size = sizeof(scratch_memory);
    matmulParam.cache_memory_ptr = cache_memory;
    matmulParam.scratch_memory_ptr = scratch_memory;
    mvTensorParam->matmulResources = &matmulParam;
}

#ifndef PYTHON_TEST

static void generateDummyTestData()
{
    bzero(output, sizeof(output));
    bzero(weights, sizeof(weights));
    bzero(biases, sizeof(biases));

    // INPUT
    // O = IN_ODD_COL_VAL
    // E = IN_EVEN_COL_VAL
    //    c0 c1 c2 c3 ... c_Ninput
    // w0  E  E  E  E ... c_Ninput
    // w1  O  O  O  O ... c_Ninput
    // w2  E  E  E  E ... c_Ninput
    // w3  O  O  O  O ... c_Ninput
    // ...
    // Output will be
    // N = c_Ninput
    // Bi = bias for output channel i
    //    c0    c1    c2    c3    ... c_NOutput
    // w0 NE+Bi NE+Bi NE+Bi NE+Bi ... c_NOutput
    // w1 NO+Bi NO+Bi NO+Bi NO+Bi ... c_NOutput
    // w2 NE+Bi NE+Bi NE+Bi NE+Bi ... c_NOutput
    // w3 NO+Bi NO+Bi NO+Bi NO+Bi ... c_NOutput
    // ...

#if defined(DISPLAY_DATA)
            printf("Input volume\n");
#endif
    for(u32 h_i = 0; h_i < gInHeight; ++h_i)
    {
        for(u32 w_i = 0; w_i < gInWidth; ++w_i)
        {
            for(u32 c_i = 0; c_i < gInChannels; ++c_i)
            {
                u32 index = c_i + (w_i * gInChannels) + (h_i * gInWidth * gInChannels);
                if(w_i % 2)
                {
                    input[index] = IN_ODD_COL_VAL;
                }
                else
                {
                    input[index] = IN_EVEN_COL_VAL;
                }
#if defined(DISPLAY_DATA)
                printf("%2.2f ", f16Tof32(input[index]));
#endif
            }
#if defined(DISPLAY_DATA)
            printf("\n");
#endif
        }
#if defined(DISPLAY_DATA)
        printf("\n");
#endif
    }

    // Channel number.
    for (u32 c_i = 0; c_i < gOutChannels; c_i++)
        biases[c_i] = f32Tof16((float)c_i);

    // All ones so it will be neutral
    for (u32 i = 0; i < gOutChannels * gInChannels * gConvWidth * gConvHeight; i++)
        weights[i] = FP16_1;

    swcLeonDataCacheFlush();
    DrvLL2CFlushOpOnAllLines(LL2C_OPERATION_WRITE_BACK, 0);
}

static void checkResult()
{
    DrvLL2CFlushOpOnAddrRange(LL2C_OPERATION_INVALIDATE, 0,
            (u32)output, (u32)output + sizeof(output));

    // save output data
    outputSize = gOutChannels * gInWidth/gOpStrideX * gInHeight/gOpStrideY;

#if defined(DISPLAY_DATA)
            printf("Output volume\n");
#endif
    for(u32 h_i = 0; h_i < gInHeight; ++h_i)
    {
        for(u32 w_i = 0; w_i < gInWidth; ++w_i)
        {
            for(u32 c_i = 0; c_i < gOutChannels; ++c_i)
            {
                u32 index = c_i + (w_i * gOutChannels) + (h_i * gInWidth * gOutChannels);
                float value = f16Tof32((unsigned int)output[index]);
                float gt_value; // ground truth value.

                if(c_i >= gUsedChannels)
                    gt_value = 0.0;
                else
                {
                    if(w_i % 2)
                        gt_value = f16Tof32(IN_ODD_COL_VAL) * gInChannels;
                    else
                        gt_value = f16Tof32(IN_EVEN_COL_VAL) * gInChannels;

                    gt_value += (float)c_i;
                }

#if defined(DISPLAY_DATA)
                printf("%2.2f ", f16Tof32(output[index]));
#endif
                if (value != gt_value)
                {
                    unitTestLogFail();
                    return;
                }
            }
#if defined(DISPLAY_DATA)
        printf("\n");
#endif
        }
#if defined(DISPLAY_DATA)
        printf("\n");
#endif
    }
}

#endif
