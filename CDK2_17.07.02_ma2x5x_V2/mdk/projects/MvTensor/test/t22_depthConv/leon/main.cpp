///
/// @file
/// @copyright All code copyright Movidius Ltd 2012, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     MvTensor Test application
///

// Includes
// ----------------------------------------------------------------------------
#include <string.h>
#include <stdio.h>
#include <math.h>

#include "mv_types.h"
#include "app_config.h"
#include "mvHelpersApi.h"
#include "swcCrc.h"
#include <swcShaveLoaderLocal.h>
#include <DrvLeonL2C.h>

#include <UnitTestApi.h>
#include <VcsHooksApi.h>

// MvTensor specific
#include "mvTensor.h"

// Source Specific #defines and types  (typedef,enum,struct)
// ----------------------------------------------------------------------------
#define MAX_SHAVES 12

//#define PYTHON_TEST 1

#define MAX_CHANNELS            28
#define MAX_INPUT_W_H           32
#define MAX_CHANNEL_MULTIPLIER	8
#define MAX_FILTER_SIZE         15
#define MAX_IN_BUFF_SIZE           (MAX_INPUT_W_H * MAX_INPUT_W_H * MAX_CHANNELS)
#define MAX_WEIGHTS_BUFF_SIZE      (MAX_FILTER_SIZE * MAX_FILTER_SIZE * MAX_CHANNELS * MAX_CHANNEL_MULTIPLIER)
#define MAX_OUT_BUFF_SIZE           (MAX_INPUT_W_H * MAX_INPUT_W_H * MAX_CHANNELS * MAX_CHANNEL_MULTIPLIER)


#define LEON_HEAP_SIZE 80000000

#define EXPECTED_CRC 0xa80f7668

// Global data
// ----------------------------------------------------------------------------
DynamicContext_t localModule[MAX_SHAVES];
DynamicContextInstances_elm localModulePrivD[MAX_SHAVES];

u32 DDR_DATA gInWidth = 28;
u32 DDR_DATA gInHeight = 28;

u32 DDR_DATA gOutWidth = 24;
u32 DDR_DATA gOutHeight = 24;

u32 DDR_DATA gInChannels = 16;
u32 DDR_DATA gOutChannels = 32;

u32 DDR_DATA gConvWidth  = 5;
u32 DDR_DATA gConvHeight = 5;

u32 DDR_DATA gOpStrideX  = 1;
u32 DDR_DATA gOpStrideY  = 1;

u32 DDR_DATA gStartShave = 0;
u32 DDR_DATA gLastShave  = LAST_SHAVE;

u32 gDataPartitionNo = 0;
u32 gInstrPartitionNo = 1;

double DDR_DATA gCallDuration = 0;

t_mvTensorGenData inputStruct;
t_mvTensorGenData outputStruct;
t_mvTensorGenData weightsStruct;
t_MvTensorMyriadResources myriadResources;
t_MvTensorDebugInfo dbgInfo;
t_MvMatMulMyriadResources matmulParam;
t_MvTensorOp opDepthConv;

fp16 DDR_DATA input[MAX_IN_BUFF_SIZE];
fp16 DDR_DATA weights[MAX_WEIGHTS_BUFF_SIZE];
fp16 DDR_DATA output[MAX_OUT_BUFF_SIZE];

char debugMsg[MV_TENSOR_DBG_MSG_SIZE];
u8 DDR_BSS leonHeap[LEON_HEAP_SIZE];

char __attribute__((section(".ddr.bss"))) cache_memory[12 * 1024 * 1024];
char __attribute__((section(".cmx.bss"))) scratch_memory[55 * 1024];
dmaTransactionList_t __attribute__((section(".cmx.cdmaDescriptors"))) task[1];

// Functions Definitions
// ----------------------------------------------------------------------------

static void initMvTensorStruct(t_MvTensorParam *mvTensorParam);

#ifndef PYTHON_TEST
static void generateDummyTestData();
#endif
// Functions Implementation
// ----------------------------------------------------------------------------

int main(void)
{
    initClocksAndMemory();
    mvSetHeap((unsigned int)leonHeap, LEON_HEAP_SIZE);

    int shvNo;
    for(shvNo = 0; shvNo < MAX_SHAVES; shvNo++)
    {
        memcpy(&localModule[shvNo], &MODULE_DATA(mvTensor),sizeof(DynamicContext_t));
        memcpy(&localModulePrivD[shvNo], MODULE_DATA(mvTensor).instancesData, sizeof(localModulePrivD[shvNo]));
        localModule[shvNo].instancesData = &localModulePrivD[shvNo];
    }

#ifndef PYTHON_TEST
    unitTestInit();
    for(int lastShv = 0; lastShv < MAX_SHAVES; lastShv++)
    {
        generateDummyTestData();
        gLastShave = lastShv;
#endif

        t_MvTensorParam mvTensorParam;
        initMvTensorStruct(&mvTensorParam);

        swcSetupDynShaveAppsComplete(&localModule[lastShv], getShaveList(), getShaveNo());

        mvTensor(&mvTensorParam);

        swcCleanupDynShaveApps(&localModule[lastShv]);

        printf("%s\n", mvTensorParam.debugInfo->debugMsg);
        printf("MvTensor done in: %f ms on %d SHAVES\n", mvTensorParam.debugInfo->ms,
                                                         mvTensorParam.myriadResources->lastShave + 1);
        gCallDuration += mvTensorParam.debugInfo->ms;

        DrvLL2CFlushOpOnAddrRange(LL2C_OPERATION_INVALIDATE, 0,
            (u32)output, (u32)output + sizeof(output));

#ifndef PYTHON_TEST
        saveMemoryToFile((u32)output, (gOutWidth * gOutHeight * gOutChannels) * sizeof(fp16), "outMyriad.bin");
        u32 crc = swcCalcCrc32((u8*)output, (gOutWidth * gOutHeight * gOutChannels) / 2, le_pointer);
        unitTestAssert(crc == EXPECTED_CRC);
    }
    unitTestFinalReport();
#endif
    return 0;
}

static void initMvTensorStruct(t_MvTensorParam *mvTensorParam)
{
    opDepthConv.type = kDepthConv;
    opDepthConv.optMask = MV_TENSOR_DEFAULT_OPT;
    opDepthConv.radixX = gConvWidth;
    opDepthConv.radixY = gConvHeight;
    opDepthConv.strideX = gOpStrideX;
    opDepthConv.strideY = gOpStrideY;
    opDepthConv.padX = 0;
    opDepthConv.padY = 0;
    opDepthConv.paddStyle = paddStyleCaffe;
    opDepthConv.opX = 0;

    // input init
    inputStruct.data = (void*)input;
    inputStruct.dataType = t_fp16;
    inputStruct.dimX = gInWidth;
    inputStruct.dimY = gInHeight;
    inputStruct.dimZ = gInChannels;
    inputStruct.dimZStride = sizeof(fp16);
    inputStruct.dimXStride = inputStruct.dimZ * inputStruct.dimZStride;
    inputStruct.dimYStride = inputStruct.dimX * inputStruct.dimXStride;
    inputStruct.storageOrder = orderYXZ;
    mvTensorParam->input = &inputStruct;

    // output init
    outputStruct.data = (void*)(output);
    outputStruct.dataType = t_fp16;
    outputStruct.dimX = gOutWidth; // ceil(float(gInWidth - gConvWidth + 1) / float(gOpStrideX));
    outputStruct.dimY = gOutHeight; //ceil(float(gInHeight - gConvHeight + 1) / float(gOpStrideY));
    outputStruct.dimZ = gOutChannels;
    outputStruct.dimZStride = sizeof(fp16);
    outputStruct.dimXStride = outputStruct.dimZ * outputStruct.dimZStride;
    outputStruct.dimYStride = (gInWidth / gOpStrideX) * outputStruct.dimXStride;
    outputStruct.storageOrder = orderYXZ;
    mvTensorParam->output = &outputStruct;

    // weights init
    weightsStruct.data = (void*)weights;
    weightsStruct.dataType = t_fp16;
    weightsStruct.dimX = 5*5;
    weightsStruct.dimY = gInChannels;
    weightsStruct.dimZ = gOutChannels;
    weightsStruct.dimZStride = sizeof(fp16);
    weightsStruct.dimYStride = gOutChannels * weightsStruct.dimZStride;
    weightsStruct.dimXStride = gInChannels * weightsStruct.dimYStride;
    weightsStruct.storageOrder = orderXYZ;
    mvTensorParam->weights = &weightsStruct;


    // set operation params
    mvTensorParam->op = &opDepthConv;

    // set post-operation params
    mvTensorParam->postOp = NULL;

    // set myriad resources
    myriadResources.firstShave = gStartShave;
    myriadResources.lastShave = gLastShave;
    myriadResources.dmaLinkAgent = 1;
    myriadResources.dataPartitionNo = gDataPartitionNo;
    myriadResources.instrPartitionNo = gInstrPartitionNo;
    myriadResources.dmaTransactions = &task[0];
    mvTensorParam->myriadResources = &myriadResources;

    mvTensorParam->debugInfo = &dbgInfo;
    mvTensorParam->debugInfo->debugMsg = debugMsg;

    // set MvMatMul resources
    matmulParam.cache_memory_size = sizeof(cache_memory);
    matmulParam.scratch_memory_size = sizeof(scratch_memory);
    matmulParam.cache_memory_ptr = cache_memory;
    matmulParam.scratch_memory_ptr = scratch_memory;
    mvTensorParam->matmulResources = &matmulParam;
}

#ifndef PYTHON_TEST
static void generateDummyTestData()
{
    fp16 input_values[28] = {
    0x0000,  // 0.000000
    0x3c00,  // 1.000000
    0x4000,  // 2.000000
    0x4200,  // 3.000000
    0x4400,  // 4.000000
    0x4500,  // 5.000000
    0x4600,  // 6.000000
    0x4700,  // 7.000000
    0x4800,  // 8.000000
    0x4880,  // 9.000000
    0x4900,  // 10.000000
    0x4980,  // 11.000000
    0x4a00,  // 12.000000
    0x4a80,  // 13.000000
    0x4b00,  // 14.000000
    0x4b80,  // 15.000000
    0x4c00,  // 16.000000
    0x4c40,  // 17.000000
    0x4c80,  // 18.000000
    0x4cc0,  // 19.000000
    0x4d00,  // 20.000000
    0x4d40,  // 21.000000
    0x4d80,  // 22.000000
    0x4dc0,  // 23.000000
    0x4e00,  // 24.000000
    0x4e40,  // 25.000000
    0x4e80,  // 26.000000
    0x4ec0   // 27.000000
    };

    for(u32 i = 0; i < gInHeight; i++)
      for(u32 j = 0; j < gInWidth; j++)
        for(u32 k = 0; k < gInChannels; k++)
          input[i*gInWidth*gInChannels +j*gInChannels + k] = input_values[i];

    bzero(output, sizeof(output));
    bzero(weights, sizeof(weights));
    u32 tapsOffset = 12 * gOutChannels;
    for(u32 i = 0; i < gOutChannels; i++)
            weights[tapsOffset + i] = 0x3c00;

    swcLeonDataCacheFlush();
    DrvLL2CFlushOpOnAllLines(LL2C_OPERATION_INVALIDATE_AND_WRITE_BACK, 0);
}
#endif
