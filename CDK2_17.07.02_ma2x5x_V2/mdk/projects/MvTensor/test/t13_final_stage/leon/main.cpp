///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     MvTensor Test application
///

// Includes
// ----------------------------------------------------------------------------
#include <string.h>
#include <stdio.h>

#include "mv_types.h"
#include "app_config.h"
#include "mvHelpersApi.h"
#include "swcCrc.h"
#include <swcShaveLoaderLocal.h>
#include <DrvLeonL2C.h>

#include <UnitTestApi.h>
#include <VcsHooksApi.h>

// MvTensor specific
#include "mvTensor.h"
#include "finalstageDefines.h"

// Source Specific #defines and types  (typedef,enum,struct)
// ----------------------------------------------------------------------------
#define MV_TENSOR_CALLS 3
#define LEON_HEAP_SIZE 80000000
#define MAX_SHAVES 12

//#define PYTHON_TEST 1
//#define USE_REAL_DATA

#ifdef USE_REAL_DATA
#define EXPECTED_CRC 0x0283b97a
#else
#define EXPECTED_CRC 0x8d27d7e4
#endif

// Global data
// ----------------------------------------------------------------------------
DynamicContext_t localModule[MAX_SHAVES];
DynamicContextInstances_elm localModulePrivD[MAX_SHAVES];

float DDR_DATA gCallDurationArray[MV_TENSOR_CALLS] = {0.0, 0.0, 0.0};
u8 DDR_BSS leonHeap[LEON_HEAP_SIZE];

u32 gDataPartitionNo = 0;
u32 gInstrPartitionNo = 1;

dmaTransactionList_t __attribute__((section(".cmx.cdmaDescriptors"))) task[1];

// Functions Definitions
// ----------------------------------------------------------------------------
#ifndef PYTHON_TEST
static void generateDummyTestData();
#endif
static void finalStage();

// Functions Implementation
// ----------------------------------------------------------------------------

int main(void)
{
    u32 i;
    initClocksAndMemory();
    mvSetHeap((unsigned int)leonHeap, LEON_HEAP_SIZE);

    int shvNo;
    for(shvNo = 0; shvNo < MAX_SHAVES; shvNo++)
    {
        memcpy(&localModule[shvNo], &MODULE_DATA(mvTensor),sizeof(DynamicContext_t));
        memcpy(&localModulePrivD[shvNo], MODULE_DATA(mvTensor).instancesData, sizeof(localModulePrivD[shvNo]));
        localModule[shvNo].instancesData = &localModulePrivD[shvNo];
    }

#ifndef PYTHON_TEST
    unitTestInit();
    for(u32 lastShv = 0; lastShv < MAX_SHAVES; lastShv++)
    {
        generateDummyTestData();
        gLastShave = lastShv;
#endif

        finalStage();

        swcSetupDynShaveAppsComplete(&localModule[lastShv], getShaveList(), getShaveNo());

        for(i = 0; i < MV_TENSOR_CALLS; i++)
        {
            mvTensor(&mvTensorParamArray[i]);
            printf("%s\n", mvTensorParamArray[i].debugInfo->debugMsg);
            printf("MvTensor done in: %f ms on %d SHAVES\n", mvTensorParamArray[i].debugInfo->ms, mvTensorParamArray[i].myriadResources->lastShave + 1);
            gCallDurationArray[i] = mvTensorParamArray[i].debugInfo->ms;
        }

        swcCleanupDynShaveApps(&localModule[lastShv]);

        printf("___________________________________________\n");

        DrvLL2CFlushOpOnAddrRange(LL2C_OPERATION_INVALIDATE, 0,
            (u32)output, (u32)output + sizeof(output));

#ifndef PYTHON_TEST
        saveMemoryToFile((u32)((u8*)output), (1 * 1 * outClasses) * sizeof(fp16), "outMyriad.bin");
        u32 crc = swcCalcCrc32((u8*)output, outClasses / 2, le_pointer);
        unitTestAssert(crc == EXPECTED_CRC);
    }

    unitTestFinalReport();
#endif

    return 0;
}

static void finalStage()
{
    u32 i;
    //########################## average pool ##################################

    inputAvgPool.data = (void*)input;
    inputAvgPool.dimZ = inChannels;
    inputAvgPool.dimX = 7;
    inputAvgPool.dimY = 7;
    outputAvgPool.data = (void*)aux1Out;
    outputAvgPool.dimZ = inChannels;

    //############################ fully connected layer #######################
    weightsFC.data = (void*)weights;
    outputFC.data = (void*)aux2Out;
    outputFC.dimZ = outClasses;

    //############################ softmax #####################################
    outputSoftMax.data = (void*)output;

    // set myriad resources
    myriadResources.firstShave = gStartShave;
    myriadResources.lastShave = gLastShave;
    myriadResources.dmaLinkAgent = 1;
    myriadResources.dataPartitionNo = gDataPartitionNo;
    myriadResources.instrPartitionNo = gInstrPartitionNo;
    myriadResources.dmaTransactions = &task[0];

    for(i = 0; i < MV_TENSOR_CALLS; i++)
        mvTensorParamArray[i].myriadResources = &myriadResources;
}

#ifndef PYTHON_TEST
static void generateDummyTestData()
{
    gStartShave = 0;
    gLastShave = 0;
    gDmaLinkAgent = 1;
    inChannels = 1024;
    outClasses = 1000;

#ifdef USE_REAL_DATA

    loadMemFromFileSimple((char*)"leon/P6_079.raw", 2*(7*7*1024), input);
    loadMemFromFileSimple((char*)"leon/loss3_classifier_weights.raw", 2*(1000*1024), weights);

#else

    u32 i;
    for(i = 0; i < inChannels; i++)
        input[i] = 0x2e66; //0.1

    for(i = 0; i < inChannels * outClasses; i++)
        weights[i] = 0x2bae; //0.06
#endif

}
#endif
