#ifndef _FINALSTAGE_DEFINES_H_
#define _FINALSTAGE_DEFINES_H_

#include "mvTensor.h"

#define FIRST_SHAVE 0
#define DMA_LINK_AGENT 1

#define MV_TENSOR_CALLS 3

#define CH_MAX 1024
#define CLS_MAX 1000

u32 DDR_DATA gStartShave = FIRST_SHAVE;
u32 DDR_DATA gLastShave = LAST_SHAVE;
u32 DDR_DATA gDmaLinkAgent = DMA_LINK_AGENT;
u32 DDR_DATA inChannels = 1024;
u32 DDR_DATA outClasses = 1000;

fp16 DDR_DATA input[7 * 7 * CH_MAX];
fp16 DDR_DATA output[CLS_MAX];
fp16 DDR_DATA weights[CH_MAX * CLS_MAX];
fp16 DDR_DATA aux1Out[CH_MAX];
fp16 DDR_DATA aux2Out[CLS_MAX];

char debugMsg[MV_TENSOR_DBG_MSG_SIZE];

char __attribute__((section(".ddr.bss"))) cache_memory[25 * 1024 * 1024];
char __attribute__((section(".cmx.bss"))) scratch_memory[110 * 1024];

//########################## shared structures MvTensor ########################
static t_MvTensorMyriadResources myriadResources;

static t_MvTensorDebugInfo debugStruct = {
    .ms = 0,
    .debugMsg = debugMsg
};

static t_MvMatMulMyriadResources matmulResources =
{
    .cache_memory_size = sizeof(cache_memory),
    .scratch_memory_size = sizeof(scratch_memory),
    .cache_memory_ptr = cache_memory,
    .scratch_memory_ptr = scratch_memory
};

static t_MvTensorOp opRelu =
{{{
    .type = kRelu,
    .optMask = MV_TENSOR_DEFAULT_OPT,
    .radixX = 1,
    .radixY = 1,
    .strideX = 1,
    .strideY = 1,
    .padX = 0,
    .padY = 0,
    .paddStyle = paddStyleNone,
    .opX = 9999.9f,
    .params = NULL
}}};

//########################## average pool 7x7x  #############################
static t_mvTensorGenData inputAvgPool;
static t_mvTensorGenData outputAvgPool;

static t_MvTensorOp opAvgPool7x7 =
{{{
    .type = kAvgPool,
    .optMask = MV_TENSOR_DEFAULT_OPT,
    .radixX = 7,
    .radixY = 7,
    .strideX = 1,
    .strideY = 1,
    .padX = 3,
    .padY = 3,
    .paddStyle = paddStyleCaffe,
    .opX = 0,
    .params = NULL
}}};

//############################ fully connected layer ###########################
static t_mvTensorGenData outputFC;
static t_mvTensorGenData weightsFC;

static t_MvTensorOp opFC =
{{{
    .type = kFC,
    .optMask = MV_TENSOR_DEFAULT_OPT,
    .radixX = 0,
    .radixY = 0,
    .strideX = 1,
    .strideY = 1,
    .padX = 0,
    .padY = 0,
    .paddStyle = paddStyleNone,
    .opX = 0,
    .params = NULL
}}};

//############################ softmax #########################################
static t_mvTensorGenData outputSoftMax;

static t_MvTensorOp opSoftMax =
{{{
    .type = kSoftMax,
    .optMask = MV_TENSOR_DEFAULT_OPT,
    .radixX = 0,
    .radixY = 0,
    .strideX = 1,
    .strideY = 1,
    .padX = 0,
    .padY = 0,
    .paddStyle = paddStyleNone,
    .opX = 0,
    .params = NULL
}}};


//############### Array with the 3 structs needed for finalstage ###############

static t_MvTensorParam mvTensorParamArray[MV_TENSOR_CALLS]
{
    // avgPool
    {{{
        .input = &inputAvgPool,
        .output = &outputAvgPool,
        .weights = NULL,
        .accumulation = NULL,
        .biases = NULL,
        .preOp = NULL,
        .op = &opAvgPool7x7,
        .postOp = &opRelu,
        .myriadResources = NULL,
        .debugInfo = &debugStruct,
        .matmulResources = &matmulResources
    }}},

    // fully connected
    {{{
        .input = &outputAvgPool,
        .output = &outputFC,
        .weights = &weightsFC,
        .accumulation = NULL,
        .biases = NULL,
        .preOp = NULL,
        .op = &opFC,
        .postOp = &opRelu,
        .myriadResources = NULL,
        .debugInfo = &debugStruct,
        .matmulResources = &matmulResources
    }}},

    // softmax
    {{{
        .input = &outputFC,
        .output = &outputSoftMax,
        .weights = NULL,
        .accumulation = NULL,
        .biases = NULL,
        .preOp = NULL,
        .op = &opSoftMax,
        .postOp = &opRelu,
        .myriadResources = NULL,
        .debugInfo = &debugStruct,
        .matmulResources = &matmulResources
    }}}
};

#endif // _FINALSTAGE_DEFINES_H_
