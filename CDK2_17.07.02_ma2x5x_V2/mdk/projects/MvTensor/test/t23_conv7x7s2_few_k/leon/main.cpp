///
/// @file
/// @copyright All code copyright Movidius Ltd 2012, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     MvTensor Test application
///

// Includes
// ----------------------------------------------------------------------------
#include <string.h>
#include <stdio.h>
#include "mv_types.h"
#include "app_config.h"
#include "mvHelpersApi.h"
#include "swcCrc.h"
#include "swcShaveLoaderLocal.h"
#include <strings.h>
#include <DrvLeonL2C.h>

// MvTensor specific
#include <UnitTestApi.h>
#include <VcsHooksApi.h>
#include "mvTensor.h"

// Source Specific #defines and types  (typedef,enum,struct)
// ----------------------------------------------------------------------------
#define M_MAX (224*224+1)
#define K_MAX 7
#define N_MAX 64

#define IN_MAX_SIZE (M_MAX*K_MAX)
#define OUT_MAX_SIZE (M_MAX/4*N_MAX)
#define WEIGHTS_SIZE (K_MAX*N_MAX)

#define LEON_HEAP_SIZE 80000000

// #define PYTHON_TEST 1
// #define EXPECTED_CRC 0x6412ccac  // using matmul
#define EXPECTED_CRC 0x54567a1e  // using spatial conv

#define MAX_SHAVES 12

// Global data
// ----------------------------------------------------------------------------
DynamicContext_t localModule[MAX_SHAVES];
DynamicContextInstances_elm localModulePrivD[MAX_SHAVES];

u32 DDR_DATA gInWidth;
u32 DDR_DATA gInHeight;
u32 DDR_DATA gInChannels;
u32 DDR_DATA gOutChannels;

u32 DDR_DATA gConvWidth = 7;
u32 DDR_DATA gConvHeight = 7;

u32 DDR_DATA gOpStrideX = 2;
u32 DDR_DATA gOpStrideY = 2;

t_MvTensorPaddStyle DDR_DATA gPaddStyle = paddStyleTFSame;
u32 DDR_DATA gPadX = 0;
u32 DDR_DATA gPadY = 0;

u32 DDR_DATA gStartShave;
u32 DDR_DATA gLastShave;

u32 gDataPartitionNo = 0;
u32 gInstrPartitionNo = 1;


fp32 DDR_DATA gCallDuration = 0;

t_mvTensorGenData inputStruct;
t_mvTensorGenData outputStruct;
t_mvTensorGenData weightsStruct;
t_MvTensorMyriadResources myriadResources;
t_MvTensorDebugInfo dbgInfo;
t_MvMatMulMyriadResources matmulParam;
t_MvTensorOp opConv7x7Stride2;

fp16 DDR_BSS input[IN_MAX_SIZE]  __attribute__((aligned(8)));
fp16 DDR_DATA output[OUT_MAX_SIZE]  __attribute__((aligned(8)));
fp16 DDR_BSS weights[49*WEIGHTS_SIZE]  __attribute__((aligned(8)));

u8 DDR_BSS leonHeap[LEON_HEAP_SIZE];

char debugMsg[MV_TENSOR_DBG_MSG_SIZE];

char __attribute__((section(".ddr.bss"))) cache_memory[12 * 1024 * 1024];
char __attribute__((section(".cmx.bss"))) scratch_memory[55 * 1024];
dmaTransactionList_t __attribute__((section(".cmx.cdmaDescriptors"))) task[1];

// Functions Definitions
// ----------------------------------------------------------------------------
static void initMvTensorStruct(t_MvTensorParam *mvTensorParam);
#ifndef PYTHON_TEST
static void generateDummyTestData();
#endif
// Functions Implementation
// ----------------------------------------------------------------------------

int main(void)
{
    initClocksAndMemory();
    mvSetHeap((unsigned int)leonHeap, LEON_HEAP_SIZE);

    int shvNo;
    for(shvNo = 0; shvNo < MAX_SHAVES; shvNo++)
    {
        memcpy(&localModule[shvNo], &MODULE_DATA(mvTensor),sizeof(DynamicContext_t));
        memcpy(&localModulePrivD[shvNo], MODULE_DATA(mvTensor).instancesData, sizeof(localModulePrivD[shvNo]));
        localModule[shvNo].instancesData = &localModulePrivD[shvNo];
    }

#ifndef PYTHON_TEST
    unitTestInit();
    for(int lastShv = 0; lastShv < MAX_SHAVES; lastShv++)
    {
        generateDummyTestData();
        gLastShave = lastShv;
#endif

        t_MvTensorParam mvTensorParam;
        initMvTensorStruct(&mvTensorParam);

        swcSetupDynShaveAppsComplete(&localModule[lastShv], getShaveList(), getShaveNo());

        mvTensor(&mvTensorParam);

        swcCleanupDynShaveApps(&localModule[lastShv]);

        printf("%s\n", mvTensorParam.debugInfo->debugMsg);
        printf("MvTensor done in: %f ms on %d SHAVES\n", mvTensorParam.debugInfo->ms,
                                            mvTensorParam.myriadResources->lastShave + 1);
        gCallDuration += mvTensorParam.debugInfo->ms;

        DrvLL2CFlushOpOnAddrRange(LL2C_OPERATION_INVALIDATE, 0,
            (u32)output, (u32)output + sizeof(output));

#ifndef PYTHON_TEST
        u32 crc = swcCalcCrc32((u8*)output, (gOutChannels * (gInHeight/2) * (gInWidth/2))/2, le_pointer);
        unitTestAssert(crc == EXPECTED_CRC);
    }
    saveMemoryToFile((u32)((u8*)output), (gOutChannels * (gInHeight/2) * (gInWidth/2)) * sizeof(fp16), "outMyriad.bin");
    unitTestFinalReport();
#endif

    return 0;
}

static void initMvTensorStruct(t_MvTensorParam *mvTensorParam)
{
    opConv7x7Stride2.type = kConv;
    opConv7x7Stride2.optMask = MV_TENSOR_DEFAULT_OPT;
    opConv7x7Stride2.radixX = gConvWidth;
    opConv7x7Stride2.radixY = gConvHeight;
    opConv7x7Stride2.strideX = gOpStrideX;
    opConv7x7Stride2.strideY = gOpStrideY;
    opConv7x7Stride2.padX = gPadX;
    opConv7x7Stride2.padY = gPadY;
    opConv7x7Stride2.paddStyle = gPaddStyle;


    // input init
    inputStruct.data = (void*)input;
    inputStruct.dataType = t_fp16;
    inputStruct.dimX = gInWidth;
    inputStruct.dimY = gInHeight;
    inputStruct.dimZ = gInChannels;
    inputStruct.dimZStride = sizeof(fp16);
    inputStruct.dimXStride = inputStruct.dimZStride * inputStruct.dimZ;
    inputStruct.dimYStride = inputStruct.dimXStride * inputStruct.dimX;
    inputStruct.storageOrder = orderYXZ;
    mvTensorParam->input = &inputStruct;

    // output init
    outputStruct.data = (half*)output;
    outputStruct.dataType = t_fp16;
    outputStruct.dimX = gInWidth / gOpStrideX;
    outputStruct.dimY = gInHeight /gOpStrideY;
    outputStruct.dimZ = gOutChannels;
    outputStruct.dimZStride = sizeof(fp16);
    outputStruct.dimXStride = outputStruct.dimZ * outputStruct.dimZStride;
    outputStruct.dimYStride = outputStruct.dimX * outputStruct.dimXStride;
    outputStruct.storageOrder = orderYXZ;
    mvTensorParam->output = &outputStruct;

    // weights init
    weightsStruct.data = (void*)weights;
    weightsStruct.dataType = t_fp16;
    weightsStruct.dimX = 7*7;
    weightsStruct.dimY = gInChannels;
    weightsStruct.dimZ = gOutChannels;
    weightsStruct.dimXStride = sizeof(fp16) * gOutChannels * gInChannels;
    weightsStruct.dimYStride = sizeof(fp16) * gOutChannels;;
    weightsStruct.dimZStride = sizeof(fp16);
    weightsStruct.storageOrder = orderXYZ;
    mvTensorParam->weights = &weightsStruct;

    // set operation params
    mvTensorParam->op = &opConv7x7Stride2;

    // set post-operation params
    mvTensorParam->postOp = NULL;

    // set myriad resources
    myriadResources.firstShave = gStartShave;
    myriadResources.lastShave = gLastShave;
    myriadResources.dmaLinkAgent = 1;
    myriadResources.dataPartitionNo = gDataPartitionNo;
    myriadResources.instrPartitionNo = gInstrPartitionNo;
    myriadResources.dmaTransactions = &task[0];
    mvTensorParam->myriadResources = &myriadResources;

    mvTensorParam->debugInfo = &dbgInfo;
    mvTensorParam->debugInfo->debugMsg = debugMsg;

    // set MvMatMul resources
    matmulParam.cache_memory_size = sizeof(cache_memory);
    matmulParam.scratch_memory_size = sizeof(scratch_memory);
    matmulParam.cache_memory_ptr = cache_memory;
    matmulParam.scratch_memory_ptr = scratch_memory;
    mvTensorParam->matmulResources = &matmulParam;
}

#ifndef PYTHON_TEST
static void generateDummyTestData()
{
    gInWidth = 224;
    gInHeight = 224;
    gInChannels = 3;
    gOutChannels = 64;
    gStartShave = 0;
    gLastShave = LAST_SHAVE;

    u32 i;
    u32 input_size = gInWidth * gInHeight * gInChannels;
    u32 weights_size = gConvWidth * gConvWidth * gInChannels * gOutChannels;
    for (i = 0; i < input_size; i++)
        input[i] = 0x2e66;    // 0.1

    for (i = 0; i < weights_size; i++)
        weights[i] = 0x2bae;  // 0.06

    bzero(output, sizeof(output));
}
#endif
