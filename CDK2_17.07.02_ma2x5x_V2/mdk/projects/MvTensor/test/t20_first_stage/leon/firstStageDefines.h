#ifndef _FINALSTAGE_DEFINES_H_
#define _FINALSTAGE_DEFINES_H_

#include "mvTensor.h"

#define FIRST_SHAVE 0
#define DMA_LINK_AGENT 1
#define DATA_SHV_L2_PARTITION 0
#define INSTRUCTION_SHV_L2_PARTITION 1

#define MV_TENSOR_CALLS 4

#define IN_CHANNELS 8
#define IN_WIDTH 224
#define IN_HEIGHT 224

#define CONV7x7_OUT_CHANNELS 64
#define CONV7x7_OUT_WIDTH 112
#define CONV7x7_OUT_HEIGHT 112
#define PADDING ((2 * CONV7x7_OUT_WIDTH + 2) * CONV7x7_OUT_CHANNELS)

#define MAXPOOL1_OUT_CHANNELS 64
#define MAXPOOL1_OUT_WIDTH 56
#define MAXPOOL1_OUT_HEIGHT 56

#define CONV3x3_OUT_CHANNELS 192
#define CONV3x3_OUT_WIDTH 56
#define CONV3x3_OUT_HEIGHT 56

#define MAXPOOL2_OUT_CHANNELS 192
#define MAXPOOL2_OUT_WIDTH 28
#define MAXPOOL2_OUT_HEIGHT 28

u32 gStartShave = FIRST_SHAVE;
u32 gLastShave = LAST_SHAVE;
u32 gDmaLinkAgent = DMA_LINK_AGENT;

fp16 DDR_BSS input[IN_WIDTH * IN_HEIGHT * IN_CHANNELS];
fp16 DDR_BSS weightsConv7x7s2[7 * 7 * IN_CHANNELS * CONV7x7_OUT_CHANNELS];
fp16 DDR_BSS outputConv7x7s2[CONV7x7_OUT_WIDTH * CONV7x7_OUT_HEIGHT * CONV7x7_OUT_CHANNELS + 2 * PADDING];
fp16 DDR_BSS outputMaxPool1s2[CONV7x7_OUT_WIDTH * CONV7x7_OUT_HEIGHT * CONV7x7_OUT_CHANNELS];
fp16 DDR_BSS weightsConv3x3s1[3 * 3 * MAXPOOL1_OUT_CHANNELS * CONV3x3_OUT_CHANNELS];
fp16 DDR_BSS outputConv3x3s1[CONV3x3_OUT_WIDTH * CONV3x3_OUT_HEIGHT * CONV3x3_OUT_CHANNELS];
fp16 DDR_BSS output[MAXPOOL2_OUT_WIDTH * MAXPOOL2_OUT_HEIGHT * MAXPOOL2_OUT_CHANNELS];

char debugMsg[MV_TENSOR_DBG_MSG_SIZE];

char __attribute__((section(".ddr.bss"))) cache_memory[12 * 1024 * 1024];
char __attribute__((section(".cmx.bss"))) scratch_memory[55 * 1024];
dmaTransactionList_t __attribute__((section(".cmx.cdmaDescriptors"))) task[1];

//########################## shared structures MvTensor ########################
static t_MvTensorMyriadResources myriadResources
{{{
    .firstShave = FIRST_SHAVE,
    .lastShave = LAST_SHAVE,
    .dmaLinkAgent = DMA_LINK_AGENT,
    .dataPartitionNo = DATA_SHV_L2_PARTITION,
    .instrPartitionNo = INSTRUCTION_SHV_L2_PARTITION,
    .dmaTransactions = &task[0]
}}};

static t_MvTensorDebugInfo debugStruct = {
    .ms = 0,
    .debugMsg = debugMsg
};

static t_MvMatMulMyriadResources matmulResources =
{
    .cache_memory_size = sizeof(cache_memory),
    .scratch_memory_size = sizeof(scratch_memory),
    .cache_memory_ptr = cache_memory,
    .scratch_memory_ptr = scratch_memory
};

//########################## conv 7x7x s2 #############################
static t_mvTensorGenData inputConv7x7
{{{
    .data = input,
    .dimX = IN_WIDTH,
    .dimY = IN_HEIGHT,
    .dimZ = IN_CHANNELS,
    .dimXStride = sizeof(fp16) * IN_CHANNELS,
    .dimYStride = sizeof(fp16) * IN_CHANNELS * IN_WIDTH,
    .dimZStride = sizeof(fp16),
    .dataType = t_fp16,
    .storageOrder = orderYXZ
}}};

static t_mvTensorGenData outputConv7x7
{{{
    .data = (void*)(outputConv7x7s2 + PADDING),
    .dimX = CONV7x7_OUT_WIDTH,
    .dimY = CONV7x7_OUT_HEIGHT,
    .dimZ = CONV7x7_OUT_CHANNELS,
    .dimXStride = sizeof(fp16) * CONV7x7_OUT_CHANNELS,
    .dimYStride = sizeof(fp16) * CONV7x7_OUT_CHANNELS * CONV7x7_OUT_WIDTH,
    .dimZStride = sizeof(fp16),
    .dataType = t_fp16,
    .storageOrder = orderYXZ
}}};

static t_mvTensorGenData weightsConv7x7
{{{
    .data = weightsConv7x7s2,
    .dimX = 7*7,
    .dimY = IN_CHANNELS,
    .dimZ = CONV7x7_OUT_CHANNELS,
    .dimXStride = sizeof(fp16) * CONV7x7_OUT_CHANNELS * IN_CHANNELS,
    .dimYStride = sizeof(fp16) * CONV7x7_OUT_CHANNELS,
    .dimZStride = sizeof(fp16),
    .dataType = t_fp16,
    .storageOrder = orderXYZ
}}};

static t_MvTensorOp opConv7x7s2 =
{{{
    .type = kConv,
    .optMask = MV_TENSOR_DEFAULT_OPT,
    .radixX = 7,
    .radixY = 7,
    .strideX = 2,
    .strideY = 2,
    .padX = 0,
    .padY = 0,
    .paddStyle = paddStyleTFSame,
    .opX = 0,
    .params = NULL
}}};

//############################ maxpool 3x3 s2 ###########################
static t_mvTensorGenData outputMaxPool1
{{{
    .data = outputMaxPool1s2,
    .dimX = MAXPOOL1_OUT_WIDTH,
    .dimY = MAXPOOL1_OUT_HEIGHT,
    .dimZ = MAXPOOL1_OUT_CHANNELS,
    .dimXStride = sizeof(fp16) * MAXPOOL1_OUT_CHANNELS,
    .dimYStride = sizeof(fp16) * MAXPOOL1_OUT_CHANNELS * MAXPOOL1_OUT_WIDTH,
    .dimZStride = sizeof(fp16),
    .dataType = t_fp16,
    .storageOrder = orderYXZ
}}};

static t_MvTensorOp opMaxPool3x3s2 =
{{{
    .type = kMaxPool,
    .optMask = MV_TENSOR_DEFAULT_OPT,
    .radixX = 3,
    .radixY = 3,
    .strideX = 2,
    .strideY = 2,
    .padX = 0,
    .padY = 0,
    .paddStyle = paddStyleTFSame,
    .opX = 0,
    .params = NULL
}}};

//############################ conv3x3 s1 #########################################
static t_mvTensorGenData outputConv3x3
{{{
    .data = outputConv3x3s1,
    .dimX = CONV3x3_OUT_WIDTH,
    .dimY = CONV3x3_OUT_HEIGHT,
    .dimZ = CONV3x3_OUT_CHANNELS,
    .dimXStride = sizeof(fp16) * CONV3x3_OUT_CHANNELS,
    .dimYStride = sizeof(fp16) * CONV3x3_OUT_CHANNELS * CONV3x3_OUT_WIDTH,
    .dimZStride = sizeof(fp16),
    .dataType = t_fp16,
    .storageOrder = orderYXZ
}}};

static t_mvTensorGenData weightsConv3x3
{{{
    .data = weightsConv3x3s1,
    .dimX = 3*3,
    .dimY = MAXPOOL1_OUT_CHANNELS,
    .dimZ = CONV3x3_OUT_CHANNELS,
    .dimXStride = sizeof(fp16) * CONV3x3_OUT_CHANNELS * MAXPOOL1_OUT_CHANNELS,
    .dimYStride = sizeof(fp16) * CONV3x3_OUT_CHANNELS,
    .dimZStride = sizeof(fp16),
    .dataType = t_fp16,
    .storageOrder = orderXYZ
}}};

static t_MvTensorOp opConv3x3s1 =
{{{
    .type = kConv,
    .optMask = MV_TENSOR_DEFAULT_OPT,
    .radixX = 3,
    .radixY = 3,
    .strideX = 1,
    .strideY = 1,
    .padX = 0,
    .padY = 0,
    .paddStyle = paddStyleTFSame,
    .opX = 0,
    .params = NULL
}}};

//############################ maxpool 3x3 s2 ###########################
static t_mvTensorGenData outputMaxPool2
{{{
    .data = output,
    .dimX = MAXPOOL2_OUT_WIDTH,
    .dimY = MAXPOOL2_OUT_HEIGHT,
    .dimZ = MAXPOOL2_OUT_CHANNELS,
    .dimXStride = sizeof(fp16) * MAXPOOL2_OUT_CHANNELS,
    .dimYStride = sizeof(fp16) * MAXPOOL2_OUT_CHANNELS * MAXPOOL2_OUT_WIDTH,
    .dimZStride = sizeof(fp16),
    .dataType = t_fp16,
    .storageOrder = orderYXZ
}}};

//############### Array with the 4 structs needed for firststage ###############

static t_MvTensorParam mvTensorParamArray[MV_TENSOR_CALLS]
{
    // conv7x7s2
    {{{
        .input = &inputConv7x7,
        .output = &outputConv7x7,
        .weights = &weightsConv7x7,
        .accumulation = NULL,
        .biases = NULL,
        .preOp = NULL,
        .op = &opConv7x7s2,
        .postOp = NULL,
        .myriadResources = &myriadResources,
        .debugInfo = &debugStruct,
        .matmulResources = &matmulResources
    }}},

    // maxpool 3x3 s2
    {{{
        .input = &outputConv7x7,
        .output = &outputMaxPool1,
        .weights = NULL,
        .accumulation = NULL,
        .biases = NULL,
        .preOp = NULL,
        .op = &opMaxPool3x3s2,
        .postOp = NULL,
        .myriadResources = &myriadResources,
        .debugInfo = &debugStruct,
        .matmulResources = &matmulResources
    }}},

    // conv3x3 s1
    {{{
        .input = &outputMaxPool1,
        .output = &outputConv3x3,
        .weights = &weightsConv3x3,
        .accumulation = NULL,
        .biases = NULL,
        .preOp = NULL,
        .op = &opConv3x3s1,
        .postOp = NULL,
        .myriadResources = &myriadResources,
        .debugInfo = &debugStruct,
        .matmulResources = &matmulResources
    }}},

    // maxpool3x3 s2
    {{{
        .input = &outputConv3x3,
        .output = &outputMaxPool2,
        .weights = NULL,
        .accumulation = NULL,
        .biases = NULL,
        .preOp = NULL,
        .op = &opMaxPool3x3s2,
        .postOp = NULL,
        .myriadResources = &myriadResources,
        .debugInfo = &debugStruct,
        .matmulResources = &matmulResources
    }}}
};

#endif // _FINALSTAGE_DEFINES_H_
