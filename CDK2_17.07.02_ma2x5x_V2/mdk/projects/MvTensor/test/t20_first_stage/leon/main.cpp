///
/// @file
/// @copyright All code copyright Movidius Ltd 2012, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     MvTensor Test application
///

// Includes
// ----------------------------------------------------------------------------
#include <string.h>
#include <stdio.h>
#include "mv_types.h"
#include "app_config.h"
#include "mvHelpersApi.h"
#include "swcCrc.h"
#include "swcShaveLoaderLocal.h"
#include <strings.h>
#include <DrvLeonL2C.h>

// MvTensor specific
#include <UnitTestApi.h>
#include <VcsHooksApi.h>

#include "firstStageDefines.h"
#include "mvTensor.h"

// Source Specific #defines and types  (typedef,enum,struct)
// ----------------------------------------------------------------------------
#define LEON_HEAP_SIZE 80000000
//#define PYTHON_TEST 1
#define EXPECTED_CRC 0xaf5c14c4

#define MAX_SHAVES 12

// Global data
// ----------------------------------------------------------------------------
DynamicContext_t localModule[MAX_SHAVES];
DynamicContextInstances_elm localModulePrivD[MAX_SHAVES];

u8 DDR_BSS leonHeap[LEON_HEAP_SIZE];
double gCallDuration = 0;
// Functions Definitions
// ----------------------------------------------------------------------------
#ifndef PYTHON_TEST
static void generateDummyTestData();
#endif

// Functions Implementation
// ----------------------------------------------------------------------------

int main(void)
{
    initClocksAndMemory();
    mvSetHeap((unsigned int)leonHeap, LEON_HEAP_SIZE);

    int shvNo;
    for(shvNo = 0; shvNo < MAX_SHAVES; shvNo++)
    {
        memcpy(&localModule[shvNo], &MODULE_DATA(mvTensor),sizeof(DynamicContext_t));
        memcpy(&localModulePrivD[shvNo], MODULE_DATA(mvTensor).instancesData, sizeof(localModulePrivD[shvNo]));
        localModule[shvNo].instancesData = &localModulePrivD[shvNo];
    }


#ifndef PYTHON_TEST
    unitTestInit();
    for(int lastShv = 0; lastShv < MAX_SHAVES; lastShv++)
    {
        generateDummyTestData();
        for(u32 i = 0; i < MV_TENSOR_CALLS; i++)
            mvTensorParamArray[i].myriadResources->lastShave = lastShv;
#endif
        bzero(output, sizeof(output));
        bzero(outputConv7x7s2, sizeof(outputConv7x7s2));
        bzero(outputConv3x3s1, sizeof(outputConv3x3s1));


        swcSetupDynShaveAppsComplete(&localModule[lastShv], getShaveList(), getShaveNo());

        for(u32 i = 0; i < MV_TENSOR_CALLS; i++)
        {
            mvTensor(&mvTensorParamArray[i]);

            printf("%s\n", mvTensorParamArray[i].debugInfo->debugMsg);
            printf("MvTensor done in: %f ms on %d SHAVES\n", mvTensorParamArray[i].debugInfo->ms,
                                                             mvTensorParamArray[i].myriadResources->lastShave + 1);
            gCallDuration += mvTensorParamArray[i].debugInfo->ms;
        }

        swcCleanupDynShaveApps(&localModule[lastShv]);

        DrvLL2CFlushOpOnAddrRange(LL2C_OPERATION_INVALIDATE, 0,
            (u32)output, (u32)output + sizeof(output));

#ifndef PYTHON_TEST
        u32 outputSize = MAXPOOL2_OUT_CHANNELS * MAXPOOL2_OUT_WIDTH * MAXPOOL2_OUT_HEIGHT * sizeof(fp16);
        saveMemoryToFile((u32)mvTensorParamArray[MV_TENSOR_CALLS-1].output->data, outputSize, "outMyriad.bin");

        u32 crc = swcCalcCrc32((u8*)mvTensorParamArray[MV_TENSOR_CALLS-1].output->data, outputSize / sizeof(u32), le_pointer);
        unitTestAssert(crc == EXPECTED_CRC);
    }
    unitTestFinalReport();
#endif

    return 0;
}

#ifndef PYTHON_TEST
static void generateDummyTestData()
{

    u32 i;
    u32 inputSize = IN_WIDTH * IN_HEIGHT * IN_CHANNELS;
    u32 weight7x7Size = 7 * 7 * IN_CHANNELS * CONV7x7_OUT_CHANNELS;
    u32 weight3x3Size = 3 * 3 * MAXPOOL1_OUT_CHANNELS * CONV3x3_OUT_CHANNELS;

    for (i = 0; i < inputSize; i++)
        input[i] = 0x2e66;  // 0.1

    for (i = 0; i < weight7x7Size; i++)
        weightsConv7x7s2[i] = 0x2bae;  // 0.06

    for (i = 0; i < weight3x3Size; i++)
        weightsConv3x3s1[i] = 0x251f;  // 0.02
}
#endif
