///
/// @file
/// @copyright All code copyright Movidius Ltd 2012, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     MvTensor Test application
///

// Includes
// ----------------------------------------------------------------------------
#include <string.h>
#include <stdio.h>
#include "mv_types.h"
#include "app_config.h"
#include "mvHelpersApi.h"
#include "swcCrc.h"
#include <swcShaveLoaderLocal.h>
#include <DrvLeonL2C.h>

// MvTensor specific
#include <UnitTestApi.h>
#include <VcsHooksApi.h>
#include "mvTensor.h"

// Source Specific #defines and types  (typedef,enum,struct)
// ----------------------------------------------------------------------------
#define M_MAX 1049
#define K_MAX 896
#define N_MAX 1049

#define IN_MAX_SIZE (M_MAX*K_MAX)
#define OUT_MAX_SIZE (M_MAX*N_MAX)
#define WEIGHTS_SIZE (1)

#define LEON_HEAP_SIZE 40000000

// #define PYTHON_TEST 1
#define EXPECTED_CRC 0xe2d6af05
#define MAX_SHAVES 12

// Global data
// ----------------------------------------------------------------------------
DynamicContext_t localModule[MAX_SHAVES];
DynamicContextInstances_elm localModulePrivD[MAX_SHAVES];

u32 DDR_DATA gInWidth;
u32 DDR_DATA gInHeight;
u32 DDR_DATA gInChannels;

u32 DDR_DATA gKernelWidth = 3;
u32 DDR_DATA gKernelHeight = 3;

u32 DDR_DATA gOpStrideX = 1;
u32 DDR_DATA gOpStrideY = 1;

t_MvTensorPaddStyle DDR_DATA gPaddStyle = paddStyleCaffe;
u32 DDR_DATA gPadX = 1;
u32 DDR_DATA gPadY = 1;


u32 DDR_DATA gStartShave;
u32 DDR_DATA gLastShave;

u32 gDataPartitionNo = 0;
u32 gInstrPartitionNo = 1;

float DDR_DATA gCallDuration = 0;

fp16 DDR_BSS input[IN_MAX_SIZE];
fp16 DDR_BSS output[OUT_MAX_SIZE];
fp16 DDR_BSS weights[WEIGHTS_SIZE];

t_mvTensorGenData inputStruct;
t_mvTensorGenData outputStruct;
t_MvTensorMyriadResources myriadResources;
t_MvTensorDebugInfo dbgInfo;
t_MvTensorOp opAvgPool3x3;
char debugMsg[MV_TENSOR_DBG_MSG_SIZE];

u8 DDR_BSS leonHeap[LEON_HEAP_SIZE];
dmaTransactionList_t __attribute__((section(".cmx.cdmaDescriptors"))) task[1];
// Functions Definitions
// ----------------------------------------------------------------------------
static void initMvTensorStruct(t_MvTensorParam *mvTensorParam);
static void generateDummyTestData();

// Functions Implementation
// ----------------------------------------------------------------------------

int main(void)
{
//    unitTestInit();
    initClocksAndMemory();
    mvSetHeap((unsigned int)leonHeap, LEON_HEAP_SIZE);

    int shvNo;
    for(shvNo = 0; shvNo < MAX_SHAVES; shvNo++)
    {
        memcpy(&localModule[shvNo], &MODULE_DATA(mvTensor),sizeof(DynamicContext_t));
        memcpy(&localModulePrivD[shvNo], MODULE_DATA(mvTensor).instancesData, sizeof(localModulePrivD[shvNo]));
        localModule[shvNo].instancesData = &localModulePrivD[shvNo];
    }

#ifndef PYTHON_TEST
    unitTestInit();

    for(int lastShv = 0; lastShv < MAX_SHAVES; lastShv++)
    {
        generateDummyTestData();
        gLastShave = lastShv;
#endif

        t_MvTensorParam mvTensorParam;
        initMvTensorStruct(&mvTensorParam);

        printf("START\n");

        for(shvNo = 0; shvNo <= lastShv; shvNo++)
            swcSetupDynShaveAppsComplete(&localModule[shvNo], &getShaveList()[shvNo], getShaveNo());

        mvTensor(&mvTensorParam);

        for(shvNo = 0; shvNo <= lastShv; shvNo++)
            swcCleanupDynShaveApps(&localModule[shvNo]);

        printf("%s\n", mvTensorParam.debugInfo->debugMsg);
        printf("MvTensor done in: %f ms on %d SHAVES\n", mvTensorParam.debugInfo->ms,
                                                         mvTensorParam.myriadResources->lastShave + 1);
        gCallDuration += mvTensorParam.debugInfo->ms;

        DrvLL2CFlushOpOnAddrRange(LL2C_OPERATION_INVALIDATE, 0,
            (u32)output, (u32)output + sizeof(output));

#ifndef PYTHON_TEST
        saveMemoryToFile((u32)(u8*)output, (gInChannels * (gInHeight/gOpStrideY) * (gInWidth/gOpStrideX)) * sizeof(fp16), "outMyriad.bin");
        u32 crc = swcCalcCrc32((u8*)output, (gInChannels * (gInHeight/gOpStrideY) * (gInWidth/gOpStrideX))/2, le_pointer);
        unitTestAssert(crc == EXPECTED_CRC);
    }
    unitTestFinalReport();
#endif

    return 0;
}

static void initMvTensorStruct(t_MvTensorParam *mvTensorParam)
{
    opAvgPool3x3.type = kAvgPool;
    opAvgPool3x3.optMask = MV_TENSOR_DEFAULT_OPT;
    opAvgPool3x3.radixX = gKernelWidth;
    opAvgPool3x3.radixY = gKernelHeight;
    opAvgPool3x3.strideX = gOpStrideX;
    opAvgPool3x3.strideY = gOpStrideY;
    opAvgPool3x3.padX = gPadX;
    opAvgPool3x3.padY = gPadY;
    opAvgPool3x3.paddStyle = gPaddStyle;

    // input init
    inputStruct.data = (void*)input;
    inputStruct.dataType = t_fp16;
    inputStruct.dimX = gInWidth;
    inputStruct.dimY = gInHeight;
    inputStruct.dimZ = gInChannels;
    inputStruct.dimXStride = sizeof(fp16) * gInChannels;
    inputStruct.dimYStride = sizeof(fp16) * gInChannels * gInWidth;
    inputStruct.dimZStride = sizeof(fp16);
    inputStruct.storageOrder = orderYXZ;
    mvTensorParam->input = &inputStruct;

    // output init
    outputStruct.data = (void*)output;
    outputStruct.dataType = t_fp16;
    outputStruct.dimX = gInWidth / gOpStrideX;
    outputStruct.dimY = gInHeight / gOpStrideY;
    outputStruct.dimZ = gInChannels;
    outputStruct.dimZStride = sizeof(fp16);
    outputStruct.dimXStride = outputStruct.dimZStride * outputStruct.dimZ;
    outputStruct.dimYStride = outputStruct.dimXStride * outputStruct.dimX;
    outputStruct.storageOrder = orderYXZ;
    mvTensorParam->output = &outputStruct;

    // weights init
    mvTensorParam->weights = NULL;

    // set operation params
    mvTensorParam->op = &opAvgPool3x3;

    // set post-operation params
    mvTensorParam->postOp = NULL;

    // set myriad resources
    myriadResources.firstShave = gStartShave;
    myriadResources.lastShave = gLastShave;
    myriadResources.dmaLinkAgent = 1;
    myriadResources.dataPartitionNo = gDataPartitionNo;
    myriadResources.instrPartitionNo = gInstrPartitionNo;
    myriadResources.dmaTransactions = &task[0];
    mvTensorParam->myriadResources = &myriadResources;

    mvTensorParam->debugInfo = &dbgInfo;
    mvTensorParam->debugInfo->debugMsg = debugMsg;
}

static void generateDummyTestData()
{
    gInWidth = 28;
    gInHeight = 28;
    gInChannels = 192;
    gStartShave = 0;
    gLastShave = LAST_SHAVE;
    gOpStrideX = 1;
    gOpStrideY = 1;

    u32 i;
    u32 input_size = gInWidth * gInHeight * gInChannels;
    for (i = 0; i < input_size; i++)
        input[i] = 0x2e66;    // 0.1
}
