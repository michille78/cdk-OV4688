///
/// @file
/// @copyright All code copyright Movidius Ltd 2012, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Application configuration Leon file
///

// 1: Includes
// ----------------------------------------------------------------------------

#include "app_config.h"
#include <registersMyriad.h>
#include <DrvTimer.h>
#include <DrvCpr.h>
#include <DrvSvu.h>
#include <DrvRegUtils.h>
#include <DrvShaveL2Cache.h>
#include <DrvDdr.h>
#include <DrvLeonL2C.h>
#include <swcShaveLoader.h>

// 2:  Source Specific #defines and types  (typedef,enum,struct)
// ----------------------------------------------------------------------------

#define CMX_CONFIG_SLICE_7_0       (0x11111111)
#define CMX_CONFIG_SLICE_15_8      (0x11111111)
#define L2CACHE_CFG                (SHAVE_L2CACHE_NORMAL_MODE)

// Enable needed Shave clocks, CMXDMA, Shave L2Cache and UPA Control interfaces
#define APP_UPA_CLOCKS (DEV_UPA_SH0       | \
                        DEV_UPA_SH1       | \
                        DEV_UPA_SH2       | \
                        DEV_UPA_SH3       | \
                        DEV_UPA_SH4       | \
                        DEV_UPA_SH5       | \
                        DEV_UPA_SH6       | \
                        DEV_UPA_SH7       | \
                        DEV_UPA_SH8       | \
                        DEV_UPA_SH9       | \
                        DEV_UPA_SH10      | \
                        DEV_UPA_SH11      | \
                        DEV_UPA_SHAVE_L2  | \
                        DEV_UPA_CDMA      | \
                        DEV_UPA_CTRL      )

#define EXTRACLOCKS   (DEV_MSS_APB_SLV     | \
                       DEV_MSS_APB2_CTRL   | \
                       DEV_MSS_RTBRIDGE    | \
                       DEV_MSS_RTAHB_CTRL  | \
                       DEV_MSS_LRT         | \
                       DEV_MSS_LRT_DSU     | \
                       DEV_MSS_LRT_L2C     | \
                       DEV_MSS_LRT_ICB     | \
                       DEV_MSS_AXI_BRIDGE  | \
                       DEV_MSS_MXI_CTRL  )

// 3: Global Data (Only if absolutely necessary)
// ----------------------------------------------------------------------------
// Sections decoration is required here for downstream tools
u32 __l2_config   __attribute__((section(".l2.mode")))  = L2CACHE_CFG;
CmxRamLayoutCfgType __attribute__((section(".cmx.ctrl"))) __cmx_config = {CMX_CONFIG_SLICE_7_0, CMX_CONFIG_SLICE_15_8};

// 4: Static Local Data
// ----------------------------------------------------------------------------
static swcShaveUnit_t svuList[MAX_SHAVES];

// 5: Static Function Prototypes
// ----------------------------------------------------------------------------
static void generateShaveList()
{
    for (u32 i = 0; i < MAX_SHAVES; i++)
        svuList[i] = i;
}
// 6: Functions Implementation
// ----------------------------------------------------------------------------
int initClocksAndMemory(void)
{
    int i;
    int sc;

    tyAuxClkDividerCfg appAuxClkCfg[] =
    {
        {AUX_CLK_MASK_UART, CLK_SRC_REFCLK0, 96, 625},   // Give the UART an SCLK that allows it to generate an output baud rate of of 115200 Hz (the uart divides by 16)
        {
            0,
            0,
            0,
            0 }, // Null Terminated List
    };

    tySocClockConfig appClockConfig600_266 =
    {
        .refClk0InputKhz = 12000,   // Default 12Mhz input clock
        .refClk1InputKhz = 0, // Assume no secondary oscillator for now
        .targetPll0FreqKhz = 600000,
        .targetPll1FreqKhz = 0, // DDR frequency of 266.5Mhz will be multipled by 2 in DDR_PHY to give 533Mhz with is a rate of 1066 when clocked on each edge
        .clkSrcPll1 = CLK_SRC_REFCLK0, // Supply both PLLS from REFCLK0
        .masterClkDivNumerator = 1,
        .masterClkDivDenominator = 1,
        .cssDssClockEnableMask = DEFAULT_CORE_CSS_DSS_CLOCKS,
        .mssClockEnableMask = EXTRACLOCKS,
        .upaClockEnableMask = APP_UPA_CLOCKS,
        .pAuxClkCfg = appAuxClkCfg,
    };

    LL2CConfig_t ll2cfg =
    {
        .LL2CEnable = 1,
        .LL2CLockedWaysNo = 0,
        .LL2CWayToReplace = 0,
        .busUsage = BUS_WRAPPING_MODE,
        .hitRate = HIT_WRAPPING_MODE,
        .replacePolicy = LRU,
        //.writePolicy = COPY_BACK
        .writePolicy = WRITE_THROUGH
    };

    swcLeonDataCacheFlush();

    // Initialize and invalidate entire Leon L2Cache to set a steady ground
    DrvLL2CInitialize(&ll2cfg);
    DrvLL2CFlushOpOnAllLines(LL2C_OPERATION_INVALIDATE, 0);

    swcLeonSetPIL(0);

    DrvCprInit();
    DrvCprSetupClocks(&appClockConfig600_266);

    DrvCprSysDeviceAction(UPA_DOMAIN, DEASSERT_RESET, APP_UPA_CLOCKS);
    DrvCprSysDeviceAction(MSS_DOMAIN, DEASSERT_RESET, EXTRACLOCKS);

    DrvDdrInitialise(NULL);
    DrvTimerInit();

    // Set the shave L2 Cache mode
    sc = DrvShaveL2CacheSetMode(L2CACHE_CFG);
    if(sc)
        return sc;

    //Set Shave L2 cache partitions
    DrvShaveL2CacheSetupPartition(SHAVEPART16KB);
    DrvShaveL2CacheSetupPartition(SHAVEPART128KB);

    //Allocate Shave L2 cache set partitions
    DrvShaveL2CacheAllocateSetPartitions();

    //Assign allocated partitions to Shaves
    for (i = 0; i < MAX_SHAVES; i++)
    {
        // Use these when absolute addresses are used (static linking)
        DrvShaveL2CacheSetLSUPartId(i, 0);
        DrvShaveL2CacheSetInstrPartId(i, 1);
    }

    // Invalidate Shave L2 cache partitions
    DrvShaveL2CachePartitionInvalidate(0);
    DrvShaveL2CachePartitionInvalidate(1);

    generateShaveList();

    return 0;
}

swcShaveUnit_t* getShaveList()
{
    return svuList;
}

u32 getShaveNo()
{
    return MAX_SHAVES;
}
