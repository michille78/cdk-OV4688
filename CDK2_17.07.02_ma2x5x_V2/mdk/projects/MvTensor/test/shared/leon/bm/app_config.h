///
/// @file
/// @copyright All code copyright Movidius Ltd 2012, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Application configuration Leon header
///

#ifndef _APP_CONFIG_H_
#define _APP_CONFIG_H_

#include <mv_types.h>
#include <swcShaveLoader.h>

#define MAX_SHAVES 12

extern DynamicContext_t MODULE_DATA(mvTensor);

#ifdef __cplusplus
extern "C" {
#endif

//notify other tools of the cmx configuration
extern CmxRamLayoutCfgType __cmx_config;
extern u32 __l2_config ;

/// Get a pointer to an array of indexes corresponding to each Shave in the system
///
/// @return    Pointer to a statically allocated array
swcShaveUnit_t* getShaveList();

/// Get the maximum number of Shaves in the system
///
/// @return    Maximum number of Shaves
u32 getShaveNo();

/// Setup all the clock configurations needed by this application and also the ddr
///
/// @return    0 on success, non-zero otherwise
int initClocksAndMemory(void);

#ifdef __cplusplus
}
#endif

#endif
