MV_SOC_PLATFORM = myriad2
MV_SOC_REV ?= ma2450

MV_COMMON_BASE ?= ../../../../common
MV_TENSOR_BASE ?= ../..

MV_MVDBG_GEN = 2

LinkerScript ?= ../shared/custom.ldscript

ComponentList += Fp16Convert UnitTestVcs VcsHooks
#PipePrint

LEON_COMPONENT_PATHS += $(MV_TENSOR_BASE)/test/shared/leon/bm/
LEON_HEADERS += $(wildcard $(MV_TENSOR_BASE)/test/shared/leon/bm/*.h)

include $(MV_TENSOR_BASE)/mvTensorSources.mk
include $(MV_COMMON_BASE)/generic.mk
include $(MV_TENSOR_BASE)/mvTensorRules.mk

LAST_SHAVE ?= 0
CPPOPT += -D LAST_SHAVE=$(LAST_SHAVE)

