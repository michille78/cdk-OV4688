MV_SOC_PLATFORM = myriad2
MV_SOC_REV ?= ma2450
MV_SOC_OS = rtems
RTEMS_BUILD_NAME =b-prebuilt
RTEMS_USB_LIB_BUILD_TYPE = debug


MV_COMMON_BASE ?= ../../../../common
MV_TENSOR_BASE ?= ../..

MV_MVDBG_GEN = 2

LinkerScript ?= ../shared/custom_mthreads.ldscript

ComponentList += Fp16Convert UnitTestVcs VcsHooks 
#PipePrint

LEON_COMPONENT_PATHS += $(MV_TENSOR_BASE)/test/shared/leon/rtems/
LEON_HEADERS += $(wildcard $(MV_TENSOR_BASE)/test/shared/leon/rtems/*.h)

include $(MV_TENSOR_BASE)/mvTensorSources.mk
include $(MV_COMMON_BASE)/generic.mk
include $(MV_TENSOR_BASE)/mvTensorRules.mk

LAST_SHAVE ?= 0
CPPOPT += -D LAST_SHAVE=$(LAST_SHAVE)

CCOPT      += -Wno-unused-variable -Wno-error
MVCCOPT    += -Wno-unused-variable -Wno-error -ffast-math -fslp-vectorize
DISABLE_ERROR_ON_WARNINGS =yes

TEST_TAGS:="MA2450,MA2150, MDK_COMPONENTS"
