#ifndef __INCEPTION_DEFINES__
#define __INCEPTION_DEFINES__

#include "mvTensor.h"

#define MAX_IN_WIDTH 28
#define MAX_OUT_STRIDE 1024
#define PADDING (2 * MAX_OUT_STRIDE * (MAX_IN_WIDTH + 2))

#define MAX_AUX_PATH2_SIZE 100352
#define MAX_AUX_PATH3_SIZE 25088
#define MAX_AUX_PATH4_SIZE 200704

#define MAX_IN_SIZE 752640
#define MAX_OUT_SIZE (752640 + 2 * PADDING)

#define WEIGHTS_SIZE 961536

#define FIRST_SHAVE 0
#define DMA_LINK_AGENT 1
#define DATA_SHV_L2_PARTITION 0
#define INSTRUCTION_SHV_L2_PARTITION 1

// global data
u32 DDR_DATA kOffsetList[10];

u32 DDR_DATA gStartShave = FIRST_SHAVE;
u32 DDR_DATA gLastShave = LAST_SHAVE;
u32 DDR_DATA gDmaLinkAgent = DMA_LINK_AGENT;

fp16 DDR_DATA input[MAX_IN_SIZE];

fp16 DDR_DATA auxPath2[MAX_AUX_PATH2_SIZE];
fp16 DDR_DATA auxPath3[MAX_AUX_PATH3_SIZE];
fp16 DDR_DATA auxPath4[MAX_AUX_PATH4_SIZE];

fp16 DDR_DATA output [MAX_OUT_SIZE];

fp16 DDR_BSS weights[WEIGHTS_SIZE];

u32 gInChannels = 192;
u32 gConv1x1Path1OutChannels = 64;
u32 gConv1x1Path2OutChannels = 96;
u32 gConv3x3Path2OutChannels = 128;
u32 gConv1x1Path3OutChannels = 16;
u32 gConv5x5Path3OutChannels = 32;
u32 gConv1x1Path4OutChannels = 32;

u32 gInHeight = 28;
u32 gInWidth = 28;


char debugMsg[MV_TENSOR_DBG_MSG_SIZE];

char __attribute__((section(".ddr.bss"))) cache_memory[25 * 1024 * 1024];
char __attribute__((section(".cmx.bss"))) scratch_memory[110 * 1024];

dmaTransactionList_t __attribute__((section(".cmx.cdmaDescriptors"))) task[1];

//###################################### inception struct ################################
typedef struct
{
    void *input;
    void *output;
    void *weights[6];
    void *path2Aux;
    void *path3Aux;
    void *path4Aux;
    u32 inChannels;
    u32 inWidth;
    u32 inHeight;
    u32 conv1x1OutChannels;
    u32 conv3x3ROutChannels;
    u32 conv3x3OutChannels;
    u32 conv5x5ROutChannels;
    u32 conv5x5OutChannels;
    u32 poolPathOutChannels;
    u32 bpp;
    t_MvTensorDataType dataType;
}t_inception;

//########################################## shared structures MvTensor ########################################

static t_MvTensorMyriadResources myriadResourcesInception =
{{{
    .firstShave = FIRST_SHAVE,
    .lastShave = LAST_SHAVE,
    .dmaLinkAgent = DMA_LINK_AGENT,
    .dataPartitionNo = DATA_SHV_L2_PARTITION,
    .instrPartitionNo = INSTRUCTION_SHV_L2_PARTITION,
    .dmaTransactions = &task[0]
}}};

static t_MvTensorDebugInfo tensorDbgInfo = {
    .ms = 0,
    .debugMsg = debugMsg
};

static t_MvMatMulMyriadResources matmulResources =
{
    .cache_memory_size = sizeof(cache_memory),
    .scratch_memory_size = sizeof(scratch_memory),
    .cache_memory_ptr = cache_memory,
    .scratch_memory_ptr = scratch_memory
};

static t_MvTensorOp opConv1x1 =
{{{
    .type = kConv,
    .optMask = MV_TENSOR_DEFAULT_OPT,
    .radixX = 1,
    .radixY = 1,
    .strideX = 1,
    .strideY = 1,
    .padX = 0,
    .padY = 0,
    .paddStyle = paddStyleCaffe,
    .opX = 0,
    .params = NULL
}}};

static t_MvTensorOp opRelu =
{{{
    .type = kRelu,
    .optMask = MV_TENSOR_DEFAULT_OPT,
    .radixX = 1,
    .radixY = 1,
    .strideX = 1,
    .strideY = 1,
    .padX = 0,
    .padY = 0,
    .paddStyle = paddStyleNone,
    .opX = 0,
    .params = NULL
}}};

//################################################ PATH I conv1x1 ######################################################
static t_mvTensorGenData inputForAllPaths;
static t_mvTensorGenData outputPath1;
static t_mvTensorGenData weightsPath1;

//################################################ PATH II conv1x1 ######################################################
static t_mvTensorGenData outputPath2Conv1x1;
static t_mvTensorGenData weightsPath2Conv1x1;

//################################################ PATH II conv3x3 ######################################################
static t_mvTensorGenData outputPath2Conv3x3;
static t_mvTensorGenData weightsPath2Conv3x3;

static t_MvTensorOp opConv3x3 =
{{{
    .type = kConv,
    .optMask = MV_TENSOR_DEFAULT_OPT,
    .radixX = 3,
    .radixY = 3,
    .strideX = 1,
    .strideY = 1,
    .padX = 1,
    .padY = 1,
    .paddStyle = paddStyleCaffe,
    .opX = 0,
    .params = NULL
}}};

//################################################ PATH III conv1x1 ######################################################
static t_mvTensorGenData outputPath3Conv1x1;
static t_mvTensorGenData weightsPath3Conv1x1;

//################################################ PATH III conv5x5 ######################################################
static t_mvTensorGenData outputPath3Conv5x5;
static t_mvTensorGenData weightsPath3Conv5x5;

static t_MvTensorOp opConv5x5 =
{{{
    .type = kConv,
    .optMask = MV_TENSOR_DEFAULT_OPT,
    .radixX = 5,
    .radixY = 5,
    .strideX = 1,
    .strideY = 1,
    .padX = 2,
    .padY = 2,
    .paddStyle = paddStyleCaffe,
    .opX = 0,
    .params = NULL
}}};

//################################################ PATH IV max pool ######################################################
static t_mvTensorGenData outputPath4MaxPool;

static t_MvTensorOp opMaxPool3x3 =
{{{
    .type = kMaxPool,
    .optMask = MV_TENSOR_DEFAULT_OPT,
    .radixX = 3,
    .radixY = 3,
    .strideX = 1,
    .strideY = 1,
    .padX = 1,
    .padY = 1,
    .paddStyle = paddStyleCaffe,
    .opX = 0,
    .params = NULL
}}};

//################################################ PATH IV conv1x1 ######################################################
static t_mvTensorGenData outputPath4Conv1x1;
static t_mvTensorGenData weightsPath4Conv1x1;

//####################################### Array with the 7*9 structs needed for inception ###############################################

static t_MvTensorParam mvTensorParamArray[7]
{
    //conv1x1
    {{{
        .input = &inputForAllPaths,
        .output = &outputPath1,
        .weights = &weightsPath1,
        .accumulation = NULL,
        .biases = NULL,
        .preOp = NULL,
        .op = &opConv1x1,
        .postOp = &opRelu,
        .myriadResources = &myriadResourcesInception,
        .debugInfo = &tensorDbgInfo,
        .matmulResources = &matmulResources
    }}},

    //conv3x3r
    {{{
        .input = &inputForAllPaths,
        .output = &outputPath2Conv1x1,
        .weights = &weightsPath2Conv1x1,
        .accumulation = NULL,
        .biases = NULL,
        .preOp = NULL,
        .op = &opConv1x1,
        .postOp = &opRelu,
        .myriadResources = &myriadResourcesInception,
        .debugInfo = &tensorDbgInfo,
        .matmulResources = &matmulResources
    }}},

    //conv3x3
    {{{
        .input = &outputPath2Conv1x1,
        .output = &outputPath2Conv3x3,
        .weights = &weightsPath2Conv3x3,
        .accumulation = NULL,
        .biases = NULL,
        .preOp = NULL,
        .op = &opConv3x3,
        .postOp = &opRelu,
        .myriadResources = &myriadResourcesInception,
        .debugInfo = &tensorDbgInfo,
        .matmulResources = &matmulResources
    }}},

    //conv5x5r
    {{{
        .input = &inputForAllPaths,
        .output = &outputPath3Conv1x1,
        .weights = &weightsPath3Conv1x1,
        .accumulation = NULL,
        .biases = NULL,
        .preOp = NULL,
        .op = &opConv1x1,
        .postOp = &opRelu,
        .myriadResources = &myriadResourcesInception,
        .debugInfo = &tensorDbgInfo,
        .matmulResources = &matmulResources
    }}},

    //conv5x5
    {{{
        .input = &outputPath3Conv1x1,
        .output = &outputPath3Conv5x5,
        .weights = &weightsPath3Conv5x5,
        .accumulation = NULL,
        .biases = NULL,
        .preOp = NULL,
        .op = &opConv5x5,
        .postOp = &opRelu,
        .myriadResources = &myriadResourcesInception,
        .debugInfo = &tensorDbgInfo,
        .matmulResources = &matmulResources
    }}},

    //maxPool
    {{{
        .input = &inputForAllPaths,
        .output = &outputPath4MaxPool,
        .weights = NULL,
        .accumulation = NULL,
        .biases = NULL,
        .preOp = NULL,
        .op = &opMaxPool3x3,
        .postOp = &opRelu,
        .myriadResources = &myriadResourcesInception,
        .debugInfo = &tensorDbgInfo,
        .matmulResources = &matmulResources
    }}},

    //conv1x1
    {{{
        .input = &outputPath4MaxPool,
        .output = &outputPath4Conv1x1,
        .weights = &weightsPath4Conv1x1,
        .accumulation = NULL,
        .biases = NULL,
        .preOp = NULL,
        .op = &opConv1x1,
        .postOp = &opRelu,
        .myriadResources = &myriadResourcesInception,
        .debugInfo = &tensorDbgInfo,
        .matmulResources = &matmulResources
    }}}

};

#endif // __INCEPTION_DEFINES__
