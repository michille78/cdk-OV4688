///
/// @file
/// @copyright All code copyright Movidius Ltd 2016, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     MvTensor Test application
///

// Includes
// ----------------------------------------------------------------------------
#include <string.h>
#include <stdio.h>

#include "mv_types.h"
#include "app_config.h"
#include "mvHelpersApi.h"
#include "swcCrc.h"
#include <swcShaveLoaderLocal.h>
#include <DrvLeonL2C.h>

#include <UnitTestApi.h>
#include <VcsHooksApi.h>

// MvTensor specific
#include "mvTensor.h"
#include "inceptionDefines.h"

// Source Specific #defines and types  (typedef,enum,struct)
// ----------------------------------------------------------------------------
#define MV_TENSOR_CALLS 7
#define LEON_HEAP_SIZE 80000000
//#define PYTHON_TEST 1
#define EXPECTED_CRC 0x9d4b4baf
#define EXPECTED_CRC_CONV5x5_IM2COL 0x324f072f
#define MAX_SHAVES 12

// Global data
// ----------------------------------------------------------------------------
DynamicContext_t localModule[MAX_SHAVES];
DynamicContextInstances_elm localModulePrivD[MAX_SHAVES];

double DDR_DATA callDurationArray[MV_TENSOR_CALLS];
u8 DDR_BSS leonHeap[LEON_HEAP_SIZE];

// Functions Definitions
// ----------------------------------------------------------------------------
static void inceptionStage(t_inception *inceptionStruct);
static void updateConfig();

#ifndef PYTHON_TEST
static void generateDummyTestData();
#endif

// Functions Implementation
// ----------------------------------------------------------------------------

int main(void)
{
    initClocksAndMemory();
    mvSetHeap((unsigned int)leonHeap, LEON_HEAP_SIZE);

    int shvNo;
    for(shvNo = 0; shvNo < MAX_SHAVES; shvNo++)
    {
        memcpy(&localModule[shvNo], &MODULE_DATA(mvTensor),sizeof(DynamicContext_t));
        memcpy(&localModulePrivD[shvNo], MODULE_DATA(mvTensor).instancesData, sizeof(localModulePrivD[shvNo]));
        localModule[shvNo].instancesData = &localModulePrivD[shvNo];
    }


    updateConfig();

    t_inception inceptionParam;
    inceptionParam.input = input;
    inceptionParam.output = output + PADDING;
    inceptionParam.weights[0] = weights;
    inceptionParam.weights[1] = &weights[gInChannels * gConv1x1Path1OutChannels];
    inceptionParam.weights[2] = &weights[gInChannels * gConv1x1Path1OutChannels + gInChannels * gConv1x1Path2OutChannels];
    inceptionParam.weights[3] = &weights[gInChannels * gConv1x1Path1OutChannels +
                                         gInChannels * gConv1x1Path2OutChannels +
                                         9 * gConv1x1Path2OutChannels * gConv3x3Path2OutChannels];
    inceptionParam.weights[4] = &weights[gInChannels * gConv1x1Path1OutChannels +
                                         gInChannels * gConv1x1Path2OutChannels +
                                         9 * gConv1x1Path2OutChannels * gConv3x3Path2OutChannels +
                                         gInChannels * gConv1x1Path3OutChannels];
    inceptionParam.weights[5] = &weights[gInChannels * gConv1x1Path1OutChannels +
                                         gInChannels * gConv1x1Path2OutChannels +
                                         9 * gConv1x1Path2OutChannels * gConv3x3Path2OutChannels +
                                         gInChannels * gConv1x1Path3OutChannels +
                                         25 * gConv1x1Path3OutChannels * gConv5x5Path3OutChannels];
    inceptionParam.path2Aux = auxPath2;
    inceptionParam.path3Aux = auxPath3;
    inceptionParam.path4Aux = auxPath4;
    inceptionParam.inChannels = gInChannels;
    inceptionParam.inWidth = gInWidth;
    inceptionParam.inHeight = gInHeight;
    inceptionParam.conv1x1OutChannels = gConv1x1Path1OutChannels;// CONV1x1_PATH1_OUT_CHANNELS;
    inceptionParam.conv3x3ROutChannels = gConv1x1Path2OutChannels;//CONV1x1_PATH2_OUT_CHANNELS;
    inceptionParam.conv3x3OutChannels = gConv3x3Path2OutChannels;//CONV3x3_OUT_CHANNELS;
    inceptionParam.conv5x5ROutChannels = gConv1x1Path3OutChannels;//CONV1x1_PATH3_OUT_CHANNELS;
    inceptionParam.conv5x5OutChannels = gConv5x5Path3OutChannels;//CONV5x5_OUT_CHANNELS;
    inceptionParam.poolPathOutChannels = gConv1x1Path4OutChannels;//CONV1x1_PATH4_OUT_CHANNELS;
    inceptionParam.bpp = 2;
    inceptionParam.dataType = t_fp16;

#ifndef PYTHON_TEST
    unitTestInit();
    for(int lastShv = 0; lastShv < MAX_SHAVES; lastShv++)
    {
        gLastShave = lastShv;
        generateDummyTestData();
#endif

        bzero(output, sizeof(output));
        bzero(auxPath2, sizeof(auxPath2));
        bzero(auxPath3, sizeof(auxPath3));
        bzero(auxPath4, sizeof(auxPath4));

        swcSetupDynShaveAppsComplete(&localModule[lastShv], getShaveList(), getShaveNo());

        inceptionStage(&inceptionParam);

        swcCleanupDynShaveApps(&localModule[lastShv]);

        DrvLL2CFlushOpOnAddrRange(LL2C_OPERATION_INVALIDATE, 0,
            (u32)output, (u32)output + sizeof(output));

#ifndef PYTHON_TEST
        u32 crc = swcCalcCrc32((u8*)(output + PADDING), gInWidth * gInHeight * 128, le_pointer);
        unitTestAssert(crc == EXPECTED_CRC);
        printf("___________________________________________\n");
    }

    unitTestFinalReport();
    saveMemoryToFile((u32)(output + PADDING), gInWidth * gInHeight * 256 * 2, "outMyriad.bin");
#endif

    return 0;
}


static void inceptionStage(t_inception *inceptionStruct)
{
    u32 outStride = inceptionStruct->conv1x1OutChannels +
                    inceptionStruct->conv3x3OutChannels +
                    inceptionStruct->conv5x5OutChannels +
                    inceptionStruct->poolPathOutChannels;

    // set values for input struct (this is the input struct for all paths)
    mvTensorParamArray[0].input->data = inceptionStruct->input;
    mvTensorParamArray[0].input->dimX = inceptionStruct->inWidth;
    mvTensorParamArray[0].input->dimY = inceptionStruct->inHeight;
    mvTensorParamArray[0].input->dimZ = inceptionStruct->inChannels;
    mvTensorParamArray[0].input->dimZStride = inceptionStruct->bpp;
    mvTensorParamArray[0].input->dimXStride = mvTensorParamArray[0].input->dimZStride * mvTensorParamArray[0].input->dimZ;
    mvTensorParamArray[0].input->dimYStride = mvTensorParamArray[0].input->dimXStride * mvTensorParamArray[0].input->dimX;
    mvTensorParamArray[0].input->dataType = inceptionStruct->dataType;
    mvTensorParamArray[0].input->storageOrder = orderYXZ;

//###################################################### PATH I ###########################################################
    // set values for output struct - path1 (conv1x1)
    mvTensorParamArray[0].output->data = inceptionStruct->output;
    mvTensorParamArray[0].output->dimX = inceptionStruct->inWidth;
    mvTensorParamArray[0].output->dimY = inceptionStruct->inHeight;
    mvTensorParamArray[0].output->dimZ = inceptionStruct->conv1x1OutChannels;
    mvTensorParamArray[0].output->dimZStride = inceptionStruct->bpp;
    mvTensorParamArray[0].output->dimXStride = mvTensorParamArray[0].output->dimZStride * outStride;
    mvTensorParamArray[0].output->dimYStride = mvTensorParamArray[0].output->dimXStride * mvTensorParamArray[0].output->dimX;
    mvTensorParamArray[0].output->dataType = inceptionStruct->dataType;
    mvTensorParamArray[0].output->storageOrder = orderYXZ;

    // set values for weights struct - path1 (conv1x1)
    mvTensorParamArray[0].weights->data = inceptionStruct->weights[0];
    mvTensorParamArray[0].weights->dimX = 1;
    mvTensorParamArray[0].weights->dimY = inceptionStruct->inChannels;
    mvTensorParamArray[0].weights->dimZ = inceptionStruct->conv1x1OutChannels;
    mvTensorParamArray[0].weights->dimXStride = inceptionStruct->bpp * inceptionStruct->conv1x1OutChannels * inceptionStruct->inChannels;
    mvTensorParamArray[0].weights->dimYStride = inceptionStruct->bpp * inceptionStruct->conv1x1OutChannels;
    mvTensorParamArray[0].weights->dimZStride = inceptionStruct->bpp;
    mvTensorParamArray[0].weights->dataType = inceptionStruct->dataType;
    mvTensorParamArray[0].weights->storageOrder = orderXYZ;

//###################################################### PATH II ###########################################################
    // set values for output struct - path2 (conv1x1)
    mvTensorParamArray[1].output->data = inceptionStruct->path2Aux;
    mvTensorParamArray[1].output->dimX = inceptionStruct->inWidth;
    mvTensorParamArray[1].output->dimY = inceptionStruct->inHeight;
    mvTensorParamArray[1].output->dimZ = inceptionStruct->conv3x3ROutChannels;
    mvTensorParamArray[1].output->dimZStride = inceptionStruct->bpp;
    mvTensorParamArray[1].output->dimXStride = mvTensorParamArray[1].output->dimZStride * mvTensorParamArray[1].output->dimZ;
    mvTensorParamArray[1].output->dimYStride = mvTensorParamArray[1].output->dimXStride * mvTensorParamArray[1].output->dimX;
    mvTensorParamArray[1].output->dataType = inceptionStruct->dataType;
    mvTensorParamArray[1].output->storageOrder = orderYXZ;

    // set values for weights struct - path2 (conv1x1)
    mvTensorParamArray[1].weights->data = inceptionStruct->weights[1];
    mvTensorParamArray[1].weights->dimX = 1;
    mvTensorParamArray[1].weights->dimY = inceptionStruct->inChannels;
    mvTensorParamArray[1].weights->dimZ = inceptionStruct->conv3x3ROutChannels;
    mvTensorParamArray[1].weights->dimXStride = inceptionStruct->bpp * inceptionStruct->conv3x3ROutChannels * inceptionStruct->inChannels;
    mvTensorParamArray[1].weights->dimYStride = inceptionStruct->bpp * inceptionStruct->conv3x3ROutChannels;
    mvTensorParamArray[1].weights->dimZStride = inceptionStruct->bpp;
    mvTensorParamArray[1].weights->dataType = inceptionStruct->dataType;
    mvTensorParamArray[1].weights->storageOrder = orderXYZ;

    // set input - path2 (conv3x3)
    mvTensorParamArray[2].input = mvTensorParamArray[1].output;

    // set values for output struct - path2 (conv1x1)
    mvTensorParamArray[2].output->data = (u8*)inceptionStruct->output + inceptionStruct->conv1x1OutChannels * inceptionStruct->bpp;
    mvTensorParamArray[2].output->dimX = inceptionStruct->inWidth;
    mvTensorParamArray[2].output->dimY = inceptionStruct->inHeight;
    mvTensorParamArray[2].output->dimZ = inceptionStruct->conv3x3OutChannels;
    mvTensorParamArray[2].output->dimZStride = inceptionStruct->bpp;
    mvTensorParamArray[2].output->dimXStride = mvTensorParamArray[2].output->dimZStride * outStride;
    mvTensorParamArray[2].output->dimYStride = mvTensorParamArray[2].output->dimXStride * mvTensorParamArray[2].output->dimX;
    mvTensorParamArray[2].output->dataType = inceptionStruct->dataType;
    mvTensorParamArray[2].output->storageOrder = orderYXZ;

    // set values for weights struct - path2 (conv3x3)
    mvTensorParamArray[2].weights->data = inceptionStruct->weights[2];
    mvTensorParamArray[2].weights->dimX = 3 * 3;
    mvTensorParamArray[2].weights->dimY = inceptionStruct->conv3x3ROutChannels;
    mvTensorParamArray[2].weights->dimZ = inceptionStruct->conv3x3OutChannels;
    mvTensorParamArray[2].weights->dimXStride = inceptionStruct->bpp * inceptionStruct->conv3x3OutChannels * inceptionStruct->conv3x3ROutChannels;
    mvTensorParamArray[2].weights->dimYStride = inceptionStruct->bpp * inceptionStruct->conv3x3OutChannels;
    mvTensorParamArray[2].weights->dimZStride = inceptionStruct->bpp;
    mvTensorParamArray[2].weights->dataType = inceptionStruct->dataType;
    mvTensorParamArray[2].weights->storageOrder = orderXYZ;


//###################################################### PATH III ###########################################################
    // set values for output struct - path3 (conv1x1)
    mvTensorParamArray[3].output->data = inceptionStruct->path3Aux;
    mvTensorParamArray[3].output->dimX = inceptionStruct->inWidth;
    mvTensorParamArray[3].output->dimY = inceptionStruct->inHeight;
    mvTensorParamArray[3].output->dimZ = inceptionStruct->conv5x5ROutChannels;
    mvTensorParamArray[3].output->dimZStride = inceptionStruct->bpp;
    mvTensorParamArray[3].output->dimXStride = mvTensorParamArray[3].output->dimZStride * mvTensorParamArray[3].output->dimZ;
    mvTensorParamArray[3].output->dimYStride = mvTensorParamArray[3].output->dimXStride * mvTensorParamArray[3].output->dimX;
    mvTensorParamArray[3].output->dataType = inceptionStruct->dataType;
    mvTensorParamArray[3].output->storageOrder = orderYXZ;

    // set values for weights struct - path3 (conv1x1)
    mvTensorParamArray[3].weights->data = inceptionStruct->weights[3];
    mvTensorParamArray[3].weights->dimX = 1;
    mvTensorParamArray[3].weights->dimY = inceptionStruct->inChannels;
    mvTensorParamArray[3].weights->dimZ = inceptionStruct->conv5x5ROutChannels;
    mvTensorParamArray[3].weights->dimXStride = inceptionStruct->bpp * inceptionStruct->conv5x5ROutChannels * inceptionStruct->inChannels;
    mvTensorParamArray[3].weights->dimYStride = inceptionStruct->bpp * inceptionStruct->conv5x5ROutChannels;
    mvTensorParamArray[3].weights->dimZStride = inceptionStruct->bpp;
    mvTensorParamArray[3].weights->dataType = inceptionStruct->dataType;
    mvTensorParamArray[3].weights->storageOrder = orderXYZ;

    // set input - path3 (conv5x5)
    mvTensorParamArray[4].input = mvTensorParamArray[3].output;

    // set values for output struct - path3 (conv5x5)
    mvTensorParamArray[4].output->data = (u8*)inceptionStruct->output + (inceptionStruct->conv1x1OutChannels + inceptionStruct->conv3x3OutChannels) * inceptionStruct->bpp;
    mvTensorParamArray[4].output->dimX = inceptionStruct->inWidth;
    mvTensorParamArray[4].output->dimY = inceptionStruct->inHeight;
    mvTensorParamArray[4].output->dimZ = inceptionStruct->conv5x5OutChannels;
    mvTensorParamArray[4].output->dimZStride = inceptionStruct->bpp;
    mvTensorParamArray[4].output->dimXStride = mvTensorParamArray[4].output->dimZStride * outStride;
    mvTensorParamArray[4].output->dimYStride = mvTensorParamArray[4].output->dimXStride * mvTensorParamArray[4].output->dimX;
    mvTensorParamArray[4].output->dataType = inceptionStruct->dataType;
    mvTensorParamArray[4].output->storageOrder = orderYXZ;

    // set values for weights struct - path3 (conv5x5)
    mvTensorParamArray[4].weights->data = inceptionStruct->weights[4];
    mvTensorParamArray[4].weights->dimX = 5 * 5;
    mvTensorParamArray[4].weights->dimY = inceptionStruct->conv5x5ROutChannels;
    mvTensorParamArray[4].weights->dimZ = inceptionStruct->conv5x5OutChannels;
    mvTensorParamArray[4].weights->dimXStride = inceptionStruct->bpp * inceptionStruct->conv5x5OutChannels * inceptionStruct->conv5x5ROutChannels;
    mvTensorParamArray[4].weights->dimYStride = inceptionStruct->bpp * inceptionStruct->conv5x5OutChannels;
    mvTensorParamArray[4].weights->dimZStride = inceptionStruct->bpp;
    mvTensorParamArray[4].weights->dataType = inceptionStruct->dataType;
    mvTensorParamArray[4].weights->storageOrder = orderXYZ;


//###################################################### PATH IV ###########################################################
    // set values for output struct - path3 (conv1x1)
    mvTensorParamArray[5].output->data = inceptionStruct->path4Aux;
    mvTensorParamArray[5].output->dimX = inceptionStruct->inWidth;
    mvTensorParamArray[5].output->dimY = inceptionStruct->inHeight;
    mvTensorParamArray[5].output->dimZ = inceptionStruct->inChannels;
    mvTensorParamArray[5].output->dimZStride = inceptionStruct->bpp;
    mvTensorParamArray[5].output->dimXStride = mvTensorParamArray[5].output->dimZStride * mvTensorParamArray[5].output->dimZ;
    mvTensorParamArray[5].output->dimYStride = mvTensorParamArray[5].output->dimXStride * mvTensorParamArray[5].output->dimX;
    mvTensorParamArray[5].output->dataType = inceptionStruct->dataType;
    mvTensorParamArray[5].output->storageOrder = orderYXZ;

    // set values for weights struct - path3 (conv1x1)
    mvTensorParamArray[5].weights = NULL;

    // set input - path3 (conv5x5)
    mvTensorParamArray[6].input = mvTensorParamArray[5].output;

    // set values for output struct - path3 (conv5x5)
    mvTensorParamArray[6].output->data = (u8*)inceptionStruct->output +
                    (inceptionStruct->conv1x1OutChannels + inceptionStruct->conv3x3OutChannels +inceptionStruct->conv5x5OutChannels)
                    * inceptionStruct->bpp;
    mvTensorParamArray[6].output->dimX = inceptionStruct->inWidth;
    mvTensorParamArray[6].output->dimY = inceptionStruct->inHeight;
    mvTensorParamArray[6].output->dimZ = inceptionStruct->poolPathOutChannels;
    mvTensorParamArray[6].output->dimZStride = inceptionStruct->bpp;
    mvTensorParamArray[6].output->dimXStride = mvTensorParamArray[6].output->dimZStride * outStride;
    mvTensorParamArray[6].output->dimYStride = mvTensorParamArray[6].output->dimXStride * mvTensorParamArray[6].output->dimX;
    mvTensorParamArray[6].output->dataType = inceptionStruct->dataType;
    mvTensorParamArray[6].output->storageOrder = orderYXZ;

    // set values for weights struct - path3 (conv5x5)
    mvTensorParamArray[6].weights->data = inceptionStruct->weights[5];
    mvTensorParamArray[6].weights->dimX = 1;
    mvTensorParamArray[6].weights->dimY = inceptionStruct->inChannels;
    mvTensorParamArray[6].weights->dimZ = inceptionStruct->poolPathOutChannels;
    mvTensorParamArray[6].weights->dimXStride = inceptionStruct->bpp * inceptionStruct->poolPathOutChannels * inceptionStruct->inChannels;
    mvTensorParamArray[6].weights->dimYStride = inceptionStruct->bpp * inceptionStruct->poolPathOutChannels;
    mvTensorParamArray[6].weights->dimZStride = inceptionStruct->bpp;
    mvTensorParamArray[6].weights->dataType = inceptionStruct->dataType;
    mvTensorParamArray[6].weights->storageOrder = orderZYX;

    for(u32 i = 0; i < MV_TENSOR_CALLS; i++)
    {
        mvTensor(&mvTensorParamArray[i]);
        printf("%s\n", mvTensorParamArray[i].debugInfo->debugMsg);
        printf("MvTensor done in: %f ms on %d SHAVES\n", mvTensorParamArray[i].debugInfo->ms, (int)gLastShave + 1);
    }

}


static void updateConfig()
{
    myriadResourcesInception.firstShave   = gStartShave;
    myriadResourcesInception.lastShave    = gLastShave;
    myriadResourcesInception.dmaLinkAgent = gDmaLinkAgent;
}

#ifndef PYTHON_TEST
static void generateDummyTestData()
{
    u32 weightsPath1Size = gInChannels * gConv1x1Path1OutChannels;
    u32 weightsPath2Size = gInChannels * gConv1x1Path2OutChannels + 9 * gConv1x1Path2OutChannels * gConv3x3Path2OutChannels;
    u32 weightsPath3Size = gInChannels * gConv1x1Path3OutChannels + 25 * gConv1x1Path3OutChannels * gConv5x5Path3OutChannels;
    u32 weightsPath4Size = gInChannels * gConv1x1Path4OutChannels;

    for(u32 i = 0; i < MAX_IN_SIZE; i++)
        input[i] = 0x2e66; //0.1

    //conv1x1
    for(u32 i = 0; i < gInChannels * gConv1x1Path1OutChannels; i++)
        weights[i] = 0x3266; //0.2

    //conv3x3_r
    for(u32 i = 0; i < gInChannels * gConv1x1Path2OutChannels; i++)
        weights[gInChannels * gConv1x1Path1OutChannels + i] = 0x34cd; //0.3
    //conv3x3
    for(u32 i = gInChannels * gConv1x1Path2OutChannels; i < weightsPath2Size; i++)
        weights[weightsPath1Size + i] = 0x3666; //0.4

    //conv5x5_r
    for(u32 i = 0; i < gInChannels * gConv1x1Path3OutChannels; i++)
        weights[weightsPath1Size + weightsPath2Size + i] = 0x3800; //0.5
    //conv5x5
    for(u32 i = gInChannels * gConv1x1Path3OutChannels; i < weightsPath3Size; i++)
        weights[weightsPath1Size + weightsPath2Size + i] = 0x38cd; //0.6

    //conv1x1
    for(u32 i = 0; i < weightsPath4Size; i++)
        weights[weightsPath1Size + weightsPath2Size + weightsPath3Size + i] = 0x399a; //0.7

    mvTensorParamArray[0].weights->data = weights;

    mvTensorParamArray[1].weights->data = &weights[weightsPath1Size];
    mvTensorParamArray[2].weights->data = &weights[weightsPath1Size + gInChannels * gConv1x1Path2OutChannels];

    mvTensorParamArray[3].weights->data = &weights[weightsPath1Size + weightsPath2Size];
    mvTensorParamArray[4].weights->data = &weights[weightsPath1Size + weightsPath2Size + gInChannels * gConv1x1Path3OutChannels];

    mvTensorParamArray[6].weights->data = &weights[weightsPath1Size + weightsPath2Size + weightsPath3Size];

    myriadResourcesInception.lastShave = gLastShave;
}
#endif

