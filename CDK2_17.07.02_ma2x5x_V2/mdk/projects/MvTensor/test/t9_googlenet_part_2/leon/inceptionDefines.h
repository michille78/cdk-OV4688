#ifndef __INCEPTION_DEFINES__
#define __INCEPTION_DEFINES__

#include "mvTensor.h"

#define FIRST_SHAVE 0
#define DMA_LINK_AGENT 1
#define DATA_SHV_L2_PARTITION 0
#define INSTRUCTION_SHV_L2_PARTITION 1

#define NO_OF_MV_TENSOR_CALLS_INSIDE_INCEPTION 7
#define INCEPTIONS_NO 9

#define I1_WEIGHTS_SIZE (192*64  + 192* 96+ 96*9*128 + 192*16+16*25*32  + 192*32)
#define I2_WEIGHTS_SIZE (256*128 + 256*128+128*9*192 + 256*32+32*25*96  + 256*64)
#define I3_WEIGHTS_SIZE (480*192 + 480* 96+ 96*9*208 + 480*16+16*25*48  + 480*64)
#define I4_WEIGHTS_SIZE (512*160 + 512*112+112*9*224 + 512*24+24*25*64  + 512*64)
#define I5_WEIGHTS_SIZE (512*128 + 512*128+128*9*256 + 512*24+24*25*64  + 512*64)
#define I6_WEIGHTS_SIZE (512*112 + 512*144+144*9*288 + 512*32+32*25*64  + 512*64)
#define I7_WEIGHTS_SIZE (528*256 + 528*160+160*9*320 + 528*32+32*25*128 + 528*128)
#define I8_WEIGHTS_SIZE (832*256 + 832*160+160*9*320 + 832*32+32*25*128 + 832*128)
#define I9_WEIGHTS_SIZE (832*384 + 832*192+192*9*384 + 832*48+48*25*128 + 832*128)

#define WEIGHTS_SIZE (I1_WEIGHTS_SIZE + I2_WEIGHTS_SIZE + I3_WEIGHTS_SIZE + I4_WEIGHTS_SIZE + \
                      I5_WEIGHTS_SIZE + I6_WEIGHTS_SIZE + I7_WEIGHTS_SIZE + I8_WEIGHTS_SIZE + I9_WEIGHTS_SIZE)
#define IN_SIZE (192*28*28)
#define OUT_SIZE (1024*7*7 + 4 * 1024 * (7+2))

#define AUX2_MAX_SZ 100352
#define AUX3_MAX_SZ  25088
#define AUX4_MAX_SZ 200704
#define MAX_OUT_PADDING 2 * 1024 * (28 + 2)
#define MAX_MV_TENSOR_OUT_SZ (376320 + 2 * MAX_OUT_PADDING)

u32 gStartShave = FIRST_SHAVE;
u32 gLastShave = LAST_SHAVE;
u32 gDmaLinkAgent = DMA_LINK_AGENT;

u32 DDR_DATA inChannels[INCEPTIONS_NO] = {192, 256, 480, 512, 512, 512, 528, 832, 832};
u32 DDR_DATA inWidth[INCEPTIONS_NO] = {28, 28, 14, 14, 14, 14, 14, 7, 7};
u32 DDR_DATA inHeight[INCEPTIONS_NO] = {28, 28, 14, 14, 14, 14, 14, 7, 7};
u32 DDR_DATA outChannels1x1[INCEPTIONS_NO] = {64, 128, 192, 160, 128, 112, 256, 256, 384};
u32 DDR_DATA outChannels3x3_r[INCEPTIONS_NO] = {96, 128, 96, 112, 128, 144, 160, 160, 192};
u32 DDR_DATA outChannels3x3[INCEPTIONS_NO] = {128, 192, 208, 224, 256, 288, 320, 320, 384};
u32 DDR_DATA outChannels5x5_r[INCEPTIONS_NO] = {16, 32, 16, 24, 24, 32, 32, 32, 48};
u32 DDR_DATA outChannels5x5[INCEPTIONS_NO] = {32, 96, 48, 64, 64, 64, 128, 128, 128};
u32 DDR_DATA outChannelsPoolPath[INCEPTIONS_NO] = {32, 64, 64, 64, 64, 64, 128, 128, 128};

u32 DDR_DATA kOffsetList[56];

fp16 DDR_BSS input[IN_SIZE];
fp16 DDR_BSS output[OUT_SIZE];
fp16 DDR_BSS weights[WEIGHTS_SIZE];
fp16 DDR_BSS auxPath2[AUX2_MAX_SZ];
fp16 DDR_BSS auxPath3[AUX3_MAX_SZ];
fp16 DDR_BSS auxPath4[AUX4_MAX_SZ];
fp16 DDR_BSS auxInOut1[MAX_MV_TENSOR_OUT_SZ];
fp16 DDR_BSS auxInOut2[MAX_MV_TENSOR_OUT_SZ];

fp16 *inPtr[INCEPTIONS_NO];
fp16 *outPtr[INCEPTIONS_NO];

char debugMsg[MV_TENSOR_DBG_MSG_SIZE];

char __attribute__((section(".ddr.bss"))) cache_memory[25 * 1024 * 1024];
char __attribute__((section(".cmx.bss"))) scratch_memory[110 * 1024];
dmaTransactionList_t __attribute__((section(".cmx.cdmaDescriptors"))) task[1];

//###################################### inception struct ################################
typedef struct
{
    void *input;
    void *output;
    void *weights[6];
    void *path2Aux;
    void *path3Aux;
    void *path4Aux;
    t_MvTensorDebugInfo *dbgInfoPtr0;
    t_MvTensorDebugInfo *dbgInfoPtr1;
    t_MvTensorDebugInfo *dbgInfoPtr2;
    t_MvTensorDebugInfo *dbgInfoPtr3;
    t_MvTensorDebugInfo *dbgInfoPtr4;
    t_MvTensorDebugInfo *dbgInfoPtr5;
    t_MvTensorDebugInfo *dbgInfoPtr6;
    u32 inChannels;
    u32 inWidth;
    u32 inHeight;
    u32 conv1x1OutChannels;
    u32 conv3x3ROutChannels;
    u32 conv3x3OutChannels;
    u32 conv5x5ROutChannels;
    u32 conv5x5OutChannels;
    u32 poolPathOutChannels;
    u32 bpp;
    t_MvTensorDataType dataType;
}t_inception;

    //########################################################################################


//########################################## shared structures MvTensor ########################################

static t_MvTensorMyriadResources myriadResourcesInception =
{{{
    .firstShave = FIRST_SHAVE,
    .lastShave = LAST_SHAVE,
    .dmaLinkAgent = DMA_LINK_AGENT,
    .dataPartitionNo = DATA_SHV_L2_PARTITION,
    .instrPartitionNo = INSTRUCTION_SHV_L2_PARTITION,
    .dmaTransactions = &task[0]
}}};

static t_MvTensorDebugInfo debugStruct = {
    .ms = 0,
    .debugMsg = debugMsg
};

static t_MvMatMulMyriadResources matmulResources =
{
    .cache_memory_size = sizeof(cache_memory),
    .scratch_memory_size = sizeof(scratch_memory),
    .cache_memory_ptr = cache_memory,
    .scratch_memory_ptr = scratch_memory
};

static t_MvTensorDebugInfo debugInfoMaxPool3b4a;
static t_MvTensorDebugInfo debugInfoMaxPool4e5a;

static t_MvTensorOp opConv1x1 =
{{{
    .type = kConv,
    .optMask = MV_TENSOR_DEFAULT_OPT,
    .radixX = 1,
    .radixY = 1,
    .strideX = 1,
    .strideY = 1,
    .padX = 0,
    .padY = 0,
    .paddStyle = paddStyleCaffe,
    .opX = 0,
    .params = NULL
}}};

static t_MvTensorOp opRelu =
{{{
    .type = kRelu,
    .optMask = MV_TENSOR_DEFAULT_OPT,
    .radixX = 1,
    .radixY = 1,
    .strideX = 1,
    .strideY = 1,
    .padX = 0,
    .padY = 0,
    .paddStyle = paddStyleNone,
    .opX = 0,
    .params = NULL
}}};

//################################################ PATH I conv1x1 ######################################################
 static t_mvTensorGenData inputForAllPaths;
static t_mvTensorGenData outputPath1;
static t_mvTensorGenData weightsPath1;

//################################################ PATH II conv1x1 ######################################################
static t_mvTensorGenData outputPath2Conv1x1;
static t_mvTensorGenData weightsPath2Conv1x1;

//################################################ PATH II conv3x3 ######################################################
static t_mvTensorGenData outputPath2Conv3x3;
static t_mvTensorGenData weightsPath2Conv3x3;

static t_MvTensorOp opConv3x3 =
{{{
    .type = kConv,
    .optMask = MV_TENSOR_DEFAULT_OPT,
    .radixX = 3,
    .radixY = 3,
    .strideX = 1,
    .strideY = 1,
    .padX = 1,
    .padY = 1,
    .paddStyle = paddStyleCaffe,
    .opX = 0,
    .params = NULL
}}};

//################################################ PATH III conv1x1 ######################################################
static t_mvTensorGenData outputPath3Conv1x1;
static t_mvTensorGenData weightsPath3Conv1x1;

//################################################ PATH III conv5x5 ######################################################
static t_mvTensorGenData outputPath3Conv5x5;
static t_mvTensorGenData weightsPath3Conv5x5;

static t_MvTensorOp opConv5x5 =
{{{
    .type = kConv,
    .optMask = MV_TENSOR_DEFAULT_OPT,
    .radixX = 5,
    .radixY = 5,
    .strideX = 1,
    .strideY = 1,
    .padX = 2,
    .padY = 2,
    .paddStyle = paddStyleCaffe,
    .opX = 0,
    .params = NULL
}}};

//################################################ PATH IV max pool ######################################################
static t_mvTensorGenData outputPath4MaxPool;

static t_MvTensorOp opMaxPool3x3 =
{{{
    .type = kMaxPool,
    .optMask = MV_TENSOR_DEFAULT_OPT,
    .radixX = 3,
    .radixY = 3,
    .strideX = 1,
    .strideY = 1,
    .padX = 1,
    .padY = 1,
    .paddStyle = paddStyleCaffe,
    .opX = 0,
    .params = NULL
}}};

//################################################ PATH IV conv1x1 ######################################################
static t_mvTensorGenData outputPath4Conv1x1;
static t_mvTensorGenData weightsPath4Conv1x1;

//################################################ IN/OUT max pool  ######################################################
static t_mvTensorGenData inputMaxPool3b4a;
static t_mvTensorGenData outputMaxPool3b4a;
static t_mvTensorGenData inputMaxPool4e5a;
static t_mvTensorGenData outputMaxPool4e5a;

static t_MvTensorOp opMaxPool3x3Stride2 =
{{{
    .type = kMaxPool,
    .optMask = MV_TENSOR_DEFAULT_OPT,
    .radixX = 3,
    .radixY = 3,
    .strideX = 2,
    .strideY = 2,
    .padX = 0,
    .padY = 0,
    .paddStyle = paddStyleCaffe,
    .opX = 0,
    .params = NULL
}}};

//####################################### Array with the 7 structs needed for inception ###############################################

static t_MvTensorParam mvTensorParamArray[7]
{
    //conv1x1
    {{{
        .input = &inputForAllPaths,
        .output = &outputPath1,
        .weights = &weightsPath1,
        .accumulation = NULL,
        .biases = NULL,
        .preOp = NULL,
        .op = &opConv1x1,
        .postOp = &opRelu,
        .myriadResources = &myriadResourcesInception,
        .debugInfo = NULL,
        .matmulResources = &matmulResources
    }}},

    //conv3x3r
    {{{
        .input = &inputForAllPaths,
        .output = &outputPath2Conv1x1,
        .weights = &weightsPath2Conv1x1,
        .accumulation = NULL,
        .biases = NULL,
        .preOp = NULL,
        .op = &opConv1x1,
        .postOp = &opRelu,
        .myriadResources = &myriadResourcesInception,
        .debugInfo = NULL,
        .matmulResources = &matmulResources
    }}},

    //conv3x3
    {{{
        .input = &outputPath2Conv1x1,
        .output = &outputPath2Conv3x3,
        .weights = &weightsPath2Conv3x3,
        .accumulation = NULL,
        .biases = NULL,
        .preOp = NULL,
        .op = &opConv3x3,
        .postOp = &opRelu,
        .myriadResources = &myriadResourcesInception,
        .debugInfo = NULL,
        .matmulResources = &matmulResources
    }}},

    //conv5x5r
    {{{
        .input = &inputForAllPaths,
        .output = &outputPath3Conv1x1,
        .weights = &weightsPath3Conv1x1,
        .accumulation = NULL,
        .biases = NULL,
        .preOp = NULL,
        .op = &opConv1x1,
        .postOp = &opRelu,
        .myriadResources = &myriadResourcesInception,
        .debugInfo = NULL,
        .matmulResources = &matmulResources
    }}},

    //conv5x5
    {{{
        .input = &outputPath3Conv1x1,
        .output = &outputPath3Conv5x5,
        .weights = &weightsPath3Conv5x5,
        .accumulation = NULL,
        .biases = NULL,
        .preOp = NULL,
        .op = &opConv5x5,
        .postOp = &opRelu,
        .myriadResources = &myriadResourcesInception,
        .debugInfo = NULL,
        .matmulResources = &matmulResources
    }}},

    //maxPool
    {{{
        .input = &inputForAllPaths,
        .output = &outputPath4MaxPool,
        .weights = NULL,
        .accumulation = NULL,
        .biases = NULL,
        .preOp = NULL,
        .op = &opMaxPool3x3,
        .postOp = &opRelu,
        .myriadResources = &myriadResourcesInception,
        .debugInfo = NULL,
        .matmulResources = &matmulResources
    }}},

    //conv1x1
    {{{
        .input = &outputPath4MaxPool,
        .output = &outputPath4Conv1x1,
        .weights = &weightsPath4Conv1x1,
        .accumulation = NULL,
        .biases = NULL,
        .preOp = NULL,
        .op = &opConv1x1,
        .postOp = &opRelu,
        .myriadResources = &myriadResourcesInception,
        .debugInfo = NULL,
        .matmulResources = &matmulResources
    }}}

};

static t_MvTensorParam mvTensorMaxPoolParamArray[2] =
{
    // 3b-4a maxPool3x3 s2
    {{{
        .input = &inputMaxPool3b4a,
        .output = &outputMaxPool3b4a,
        .weights = NULL,
        .accumulation = NULL,
        .biases = NULL,
        .preOp = NULL,
        .op = &opMaxPool3x3Stride2,
        .postOp = NULL,
        .myriadResources = &myriadResourcesInception,
        .debugInfo = &debugInfoMaxPool3b4a,
        .matmulResources = &matmulResources
    }}},

    // 3b-4a maxPool3x3 s2
    {{{
        .input = &inputMaxPool4e5a,
        .output = &outputMaxPool4e5a,
        .weights = NULL,
        .accumulation = NULL,
        .biases = NULL,
        .preOp = NULL,
        .op = &opMaxPool3x3Stride2,
        .postOp = NULL,
        .myriadResources = &myriadResourcesInception,
        .debugInfo = &debugInfoMaxPool4e5a,
        .matmulResources = &matmulResources
    }}}

};


#endif // __INCEPTION_DEFINES__
