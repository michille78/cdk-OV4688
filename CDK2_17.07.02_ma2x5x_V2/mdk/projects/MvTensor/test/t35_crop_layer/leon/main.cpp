///
/// @file
/// @copyright All code copyright Movidius Ltd 2012, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     MvTensor Test application
///

// Includes
// ----------------------------------------------------------------------------
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <DrvLeonL2C.h>
#include <Fp16Convert.h>
#include "mv_types.h"
#include "app_config.h"
#include "mvHelpersApi.h"
#include <swcShaveLoaderLocal.h>
#include <cmath>

// MvTensor specific
#include <UnitTestApi.h>
#include <VcsHooksApi.h>
#include "mvTensor.h"
#include "mvCropParam.h"

// Source Specific #defines and types  (typedef,enum,struct)
// ----------------------------------------------------------------------------
//#define DISPLAY_INPUT_VOLUME
//#define DISPLAY_OUTPUT_VOLUME

#define M_MAX 3136
#define K_MAX 1024
#define N_MAX 1024

#define IN_MAX_SIZE (M_MAX * K_MAX)
#define OUT_MAX_SIZE (M_MAX * N_MAX)
#define WEIGHTS_SIZE (K_MAX * N_MAX)

#define LEON_HEAP_SIZE 80000000

// #define PYTHON_TEST 1

// Global data
// ----------------------------------------------------------------------------
DynamicContext_t localModule[MAX_SHAVES];
DynamicContextInstances_elm localModulePrivD[MAX_SHAVES];

u32 DDR_DATA gInDimX = 4;
u32 DDR_DATA gInDimY = 4;
u32 DDR_DATA gInDimZ = 4;

u32 DDR_DATA gOffsetX = 1;
u32 DDR_DATA gOffsetY = 0;
u32 DDR_DATA gOffsetZ = 2;

u32 DDR_DATA gOutDimX = 2;
u32 DDR_DATA gOutDimY = 2;
u32 DDR_DATA gOutDimZ = 1;

t_CropLayerParams crop_params;

// Output padding != 0 not supported.
u32 outputPadding = 0;

u32 DDR_DATA gPostOpStrideX = 1;
u32 DDR_DATA gPostOpStrideY = 1;

u32 DDR_DATA gStartShave = 0;
u32 DDR_DATA gLastShave = LAST_SHAVE;

u32 gDataPartitionNo = 0;
u32 gInstrPartitionNo = 1;

float DDR_DATA gCallDuration = 0;

t_mvTensorGenData inputStruct;
t_mvTensorGenData outputStruct;
t_MvTensorMyriadResources myriadResources;
t_MvTensorDebugInfo dbgInfo;
t_MvTensorOp op;

fp16 DDR_BSS input[IN_MAX_SIZE]    __attribute__((aligned(32)));
fp16 DDR_BSS output[OUT_MAX_SIZE]  __attribute__((aligned(32)));

u8 DDR_BSS leonHeap[LEON_HEAP_SIZE];

char debugMsg[MV_TENSOR_DBG_MSG_SIZE];

dmaTransactionList_t __attribute__((section(".cmx.cdmaDescriptors"))) task[1];

// Functions Definitions
// ----------------------------------------------------------------------------
static void initMvTensorStruct(t_MvTensorParam *mvTensorParam);

#ifndef PYTHON_TEST
static void generateDummyTestData();
static void checkResult(bool save_to_file);
#endif
// Functions Implementation
// ----------------------------------------------------------------------------

int main(void)
{
    initClocksAndMemory();
    mvSetHeap((unsigned int)leonHeap, LEON_HEAP_SIZE);

    int shvNo;
    for(shvNo = 0; shvNo < MAX_SHAVES; shvNo++)
    {
        memcpy(&localModule[shvNo], &MODULE_DATA(mvTensor),sizeof(DynamicContext_t));
        memcpy(&localModulePrivD[shvNo], MODULE_DATA(mvTensor).instancesData, sizeof(localModulePrivD[shvNo]));
        localModule[shvNo].instancesData = &localModulePrivD[shvNo];
    }

    t_MvTensorParam mvTensorParam;
    initMvTensorStruct(&mvTensorParam);

#ifndef PYTHON_TEST
    unitTestInit();
#endif

    for(shvNo = 0; shvNo <= LAST_SHAVE; shvNo++)
        swcSetupDynShaveAppsComplete(&localModule[shvNo], getShaveList() + shvNo, 1);

#ifndef PYTHON_TEST
        generateDummyTestData();
#endif

        printf("Crop on LEON\n");
        mvTensor(&mvTensorParam);
        printf("%s\n", mvTensorParam.debugInfo->debugMsg);
        printf("MvTensor done in: %fms on LEON\n", mvTensorParam.debugInfo->ms);
        gCallDuration += mvTensorParam.debugInfo->ms;

#ifndef PYTHON_TEST
        checkResult(false);
#endif

    for(shvNo = 0; shvNo <= LAST_SHAVE; shvNo++)
        swcCleanupDynShaveApps(&localModule[shvNo]);

#ifndef PYTHON_TEST
    unitTestFinalReport();
#endif

    return 0;
}

static void initMvTensorStruct(t_MvTensorParam *mvTensorParam)
{
    op.type = kCrop;
    op.optMask = MV_TENSOR_DEFAULT_OPT;
    op.radixX = 1;
    op.radixY = 1;
    op.strideX = 1;
    op.strideY = 1;
    op.padX = 0;
    op.padY = 0;
    op.paddStyle = paddStyleNone;
    op.opX = 0;
    crop_params.offset_dimX = gOffsetX;
    crop_params.offset_dimY = gOffsetY;
    crop_params.offset_dimZ = gOffsetZ;
    op.params = (void *)&crop_params;

    // input init
    inputStruct.data = (void*)input;
    inputStruct.dataType = t_fp16;
    inputStruct.dimX = gInDimX;
    inputStruct.dimY = gInDimY;
    inputStruct.dimZ = gInDimZ;
    inputStruct.storageOrder = orderYXZ;
    inputStruct.dimXStride = sizeof(fp16) * gInDimZ;
    inputStruct.dimYStride = sizeof(fp16) * gInDimZ * gInDimX;
    inputStruct.dimZStride = sizeof(fp16);
    mvTensorParam->input = &inputStruct;

    // output init
    outputStruct.data = (void*)(output + outputPadding);
    outputStruct.dataType = t_fp16;
    outputStruct.dimX = gOutDimX;
    outputStruct.dimY = gOutDimY;
    outputStruct.dimZ = gOutDimZ;
    outputStruct.storageOrder = inputStruct.storageOrder;
    outputStruct.dimZStride = sizeof(fp16);
    outputStruct.dimXStride = sizeof(fp16) * gOutDimZ;
    outputStruct.dimYStride = sizeof(fp16) * gOutDimZ * gInDimX;
    mvTensorParam->output = &outputStruct;

    mvTensorParam->biases = NULL;
    mvTensorParam->weights = NULL;

    // set pre-operation params
    mvTensorParam->preOp = NULL;

    // set operation params
    mvTensorParam->op = &op;

    // set post-operation params
    mvTensorParam->postOp = NULL;

    // set myriad resources
    myriadResources.firstShave = gStartShave;
    myriadResources.lastShave = gLastShave;
    myriadResources.dmaLinkAgent = 1;
    myriadResources.dataPartitionNo = gDataPartitionNo;
    myriadResources.instrPartitionNo = gInstrPartitionNo;
    myriadResources.dmaTransactions = &task[0];
    mvTensorParam->myriadResources = &myriadResources;

    mvTensorParam->debugInfo = &dbgInfo;
    mvTensorParam->debugInfo->debugMsg = debugMsg;
}

#ifndef PYTHON_TEST
static void generateDummyTestData()
{
    memset((void *)output, 0x55, OUT_MAX_SIZE * sizeof(fp16));

#if defined(DISPLAY_INPUT_VOLUME)
    printf("Input volume\n");
#endif
    u32 axis0 = gInDimZ;
    u32 axis1 = gInDimX;
    u32 axis2 = gInDimY;

    for(u32 a2_i = 0; a2_i < axis2; ++a2_i)
    {
        for(u32 a1_i = 0; a1_i < axis1; ++a1_i)
        {
            for(u32 a0_i = 0; a0_i < axis0; ++a0_i)
            {
                u32 index = a0_i + (a1_i * axis0) + (a2_i * axis1 * axis0);

                float in_val = (float)(rand() % 10);

                input[index] = f32Tof16(in_val);

#if defined(DISPLAY_INPUT_VOLUME)
                printf("%2.0f ", f16Tof32(input[index]));
#endif
            }
#if defined(DISPLAY_INPUT_VOLUME)
            printf("|");
#endif
        }
#if defined(DISPLAY_INPUT_VOLUME)
        printf("\n");
#endif
    }

    swcLeonDataCacheFlush();
    DrvLL2CFlushOpOnAllLines(LL2C_OPERATION_WRITE_BACK, 0);
}

void checkResult(bool save_to_file)
{
    DrvLL2CFlushOpOnAddrRange(LL2C_OPERATION_INVALIDATE, 0,
            (u32)output, (u32)output + sizeof(output));

    // save output data
    u32 outputSize = gOutDimX * gOutDimY * gOutDimZ;
    bool test_failed = false;

    if(save_to_file)
        saveMemoryToFile((u32)output, outputSize * sizeof(fp16) , "outMyriad.bin");

#if defined(DISPLAY_OUTPUT_VOLUME)
    printf("Output volume\n");
#endif

    u32 out_axis0 = gOutDimZ;
    u32 out_axis1 = gOutDimX;
    u32 out_axis2 = gOutDimY;

    u32 in_axis0 = gInDimZ;
    u32 in_axis1 = gInDimX;
//    u32 in_axis2 = gInDimY;

    u32 offset_a0 = gOffsetZ;
    u32 offset_a1 = gOffsetX;
    u32 offset_a2 = gOffsetY;

    for(u32 a2_i = 0; a2_i < out_axis2; ++a2_i)
    {
        for(u32 a1_i = 0; a1_i < out_axis1; ++a1_i)
        {
            for(u32 a0_i = 0; a0_i < out_axis0; ++a0_i)
            {
                u32 output_index = a0_i + (a1_i * out_axis0) + (a2_i * out_axis1 * out_axis0);
                u32 input_index = (a0_i + offset_a0) +
                        ((a1_i + offset_a1) * in_axis0) +
                        ((a2_i + offset_a2) * in_axis1 * in_axis0);

                float value = f16Tof32((unsigned int)output[output_index]);
                float gt_value = f16Tof32((unsigned int)input[input_index]);

#if defined(DISPLAY_OUTPUT_VOLUME)
                printf("(%2.0f, %2.0f)", value, gt_value);
//                printf("(%3d, %3d) ", (int)input_index, (int)output_index);
#endif

                if (value != gt_value)
                    test_failed = true;

            }
#if defined(DISPLAY_OUTPUT_VOLUME)
        printf("|");
#endif
        }
#if defined(DISPLAY_OUTPUT_VOLUME)
        printf("\n");
#endif
    }

    if(test_failed)
        unitTestLogFail();
}

#endif
