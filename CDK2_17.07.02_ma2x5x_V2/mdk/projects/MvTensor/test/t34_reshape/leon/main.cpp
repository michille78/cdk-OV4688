///


/// @file
/// @copyright All code copyright Movidius Ltd 2012, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     MvTensor input re-layout test application
///

// Includes
// ----------------------------------------------------------------------------
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <DrvLeonL2C.h>
#include <Fp16Convert.h>
#include "mv_types.h"
#include "app_config.h"
#include "mvHelpersApi.h"
#include "swcCrc.h"
#include <swcShaveLoaderLocal.h>

// MvTensor specific
#include <UnitTestApi.h>
#include <VcsHooksApi.h>
#include "mvTensor.h"

// Source Specific #defines and types  (typedef,enum,struct)
// ----------------------------------------------------------------------------
// #define DISPLAY_INPUT
// #define DISPLAY_OUTPUT

#define NO_TESTS 1

// MAX_CHANNELS corresponding to MAX_WIDHT and MAX_HEIGHT
#define MAX_CHANNELS 3
#define MAX_WIDHT  224
#define MAX_HEIGHT 224

#define IN_MAX_SIZE  (MAX_WIDHT * MAX_HEIGHT * MAX_CHANNELS)
#define OUT_MAX_SIZE (IN_MAX_SIZE)

#define LEON_HEAP_SIZE 80000000

// #define PYTHON_TEST 1

// Global data
// ----------------------------------------------------------------------------
DynamicContext_t localModule[MAX_SHAVES];
DynamicContextInstances_elm localModulePrivD[MAX_SHAVES];

s32 DDR_DATA gInWidth[NO_TESTS]         = {24};
s32 DDR_DATA gInHeight[NO_TESTS]        = {24};
s32 DDR_DATA gInChannels[NO_TESTS]      = {3};

s32 DDR_DATA gOutWidth[NO_TESTS]        = {12};
s32 DDR_DATA gOutHeight[NO_TESTS]       = {24};
s32 DDR_DATA gOutChannels[NO_TESTS]     = {-1};

s32 DDR_DATA gOutWidth_ref[NO_TESTS]    = {12};
s32 DDR_DATA gOutHeight_ref[NO_TESTS]   = {24};
s32 DDR_DATA gOutChannels_ref[NO_TESTS] = {6};

u32 DDR_DATA gStartShave = 0;
u32 DDR_DATA gLastShave = LAST_SHAVE;

u32 gDataPartitionNo = 0;
u32 gInstrPartitionNo = 1;

float DDR_DATA gCallDuration = 0;

t_mvTensorGenData inputStruct;
t_mvTensorGenData outputStruct;
t_MvTensorMyriadResources myriadResources;
t_MvTensorDebugInfo dbgInfo;
t_MvTensorOp op;

fp16 DDR_BSS input[IN_MAX_SIZE]  __attribute__((aligned(32)));
fp16 DDR_BSS output[OUT_MAX_SIZE]  __attribute__((aligned(32)));

u8 DDR_BSS leonHeap[LEON_HEAP_SIZE];
dmaTransactionList_t __attribute__((section(".cmx.cdmaDescriptors"))) task[1];

u32 expected_crc[NO_TESTS] = {0x934d91df};

char debugMsg[MV_TENSOR_DBG_MSG_SIZE];
// Functions Definitions
// ----------------------------------------------------------------------------
static void initMvTensorStruct(t_MvTensorParam *mvTensorParam, s32 test_no);

#ifndef PYTHON_TEST
static void generateDummyTestData(s32 test_no);
static void checkResult(s32 test_no, t_MvTensorParam *mvTensorParam, bool saveOutput);
#endif
// Functions Implementation
// ----------------------------------------------------------------------------

int main(void)
{
    initClocksAndMemory();
    mvSetHeap((unsigned int)leonHeap, LEON_HEAP_SIZE);
    int shvNo;
    for(shvNo = 0; shvNo < MAX_SHAVES; shvNo++)
    {
        memcpy(&localModule[shvNo], &MODULE_DATA(mvTensor),sizeof(DynamicContext_t));
        memcpy(&localModulePrivD[shvNo], MODULE_DATA(mvTensor).instancesData, sizeof(localModulePrivD[shvNo]));
        localModule[shvNo].instancesData = &localModulePrivD[shvNo];
    }

    t_MvTensorParam mvTensorParam;

#ifndef PYTHON_TEST
    unitTestInit();
#endif

    swcSetupDynShaveAppsComplete(&localModule[0], getShaveList(), getShaveNo());

    for(s32 test_i = 0; test_i < NO_TESTS; ++test_i)
    {
#ifndef PYTHON_TEST
        // no need to run multi-shave
        for(int lastShv = 0; lastShv < 1; lastShv++)
        {
            generateDummyTestData(test_i);
            gLastShave = lastShv;
#endif
            initMvTensorStruct(&mvTensorParam, test_i);

            memset((void *)output, 0x55, OUT_MAX_SIZE * sizeof(fp16));

            printf("Reshape %ldx%ldx%ld->%ldx%ldx%ld \n",
                    gInWidth[test_i], gInHeight[test_i], gInChannels[test_i],
                    gOutWidth[test_i], gOutHeight[test_i], gOutChannels[test_i]);

            mvTensor(&mvTensorParam);
            printf("%s\n", mvTensorParam.debugInfo->debugMsg);
            printf("Execution time: %f ms\n", mvTensorParam.debugInfo->ms);
            gCallDuration += mvTensorParam.debugInfo->ms;

#ifndef PYTHON_TEST
            checkResult(test_i, &mvTensorParam, false);
        }
#endif
    }

    swcCleanupDynShaveApps(&localModule[0]);

#ifndef PYTHON_TEST
    unitTestFinalReport();
#endif

    return 0;
}

static void initMvTensorStruct(t_MvTensorParam *mvTensorParam, s32 test_no)
{
    op.type = kReshape;
    op.optMask = MV_TENSOR_DEFAULT_OPT;
    op.radixX  = 1;
    op.radixY  = 1;
    op.strideX = 1;
    op.strideY = 1;
    op.padX    = 0;
    op.padY    = 0;
    op.paddStyle = paddStyleTFValid;
    op.opX     = 0;

    // input init
    inputStruct.data = (void*)input;
    inputStruct.dataType = t_fp16;
    inputStruct.dimX = gInWidth[test_no];
    inputStruct.dimY = gInHeight[test_no];
    inputStruct.dimZ = gInChannels[test_no];
    inputStruct.dimXStride = sizeof(fp16) * inputStruct.dimZ;
    inputStruct.dimYStride = sizeof(fp16) * inputStruct.dimZ * inputStruct.dimX;
    inputStruct.dimZStride = sizeof(fp16);
    inputStruct.storageOrder = orderYXZ;
    mvTensorParam->input = &inputStruct;

    // output init
    outputStruct.data = (void*)(output);
    outputStruct.dataType = t_fp16;
    outputStruct.dimX = gOutWidth[test_no];
    outputStruct.dimY = gOutHeight[test_no];
    outputStruct.dimZ = gOutChannels[test_no];

    outputStruct.dimXStride = sizeof(fp16) * gOutChannels_ref[test_no];
    outputStruct.dimYStride = sizeof(fp16) * gOutChannels_ref[test_no] * gOutWidth_ref[test_no];
    outputStruct.dimZStride = sizeof(fp16);
    outputStruct.storageOrder = orderYXZ;
    mvTensorParam->output = &outputStruct;

    mvTensorParam->weights = NULL;

    mvTensorParam->biases = NULL;

    mvTensorParam->preOp = NULL;

    // set operation params
    mvTensorParam->op = &op;

    // set post-operation params
    mvTensorParam->postOp = NULL;

    // set myriad resources
    myriadResources.firstShave = gStartShave;
    myriadResources.lastShave = gLastShave;
    myriadResources.dmaLinkAgent = 1;
    myriadResources.dataPartitionNo = gDataPartitionNo;
    myriadResources.instrPartitionNo = gInstrPartitionNo;
    myriadResources.dmaTransactions = &task[0];
    mvTensorParam->myriadResources = &myriadResources;

    mvTensorParam->debugInfo = &dbgInfo;
    mvTensorParam->debugInfo->debugMsg = debugMsg;
}

#ifndef PYTHON_TEST

static void generateDummyTestData(s32 test_no)
{
    bzero(input, sizeof(input));

#if defined(DISPLAY_INPUT)
    printf("Input volume - row major format.\n");
#endif
    for(s32 c_i = 0; c_i < gInChannels[test_no]; ++c_i)
    {
        for(s32 h_i = 0; h_i < gInHeight[test_no]; ++h_i)
        {
            for(s32 w_i = 0; w_i < gInWidth[test_no]; ++w_i)
            {
                u32 index = c_i + (w_i * gInChannels[test_no]) + (h_i * gInWidth[test_no] * gInChannels[test_no]);
                float value = 10*c_i + 1*h_i + 0.1*w_i;
                input[index] = f32Tof16(value);

#if defined(DISPLAY_INPUT)
                printf("%2.2f ", f16Tof32(input[index]));
#endif
            }
#if defined(DISPLAY_INPUT)
            printf("\n");
#endif
        }
#if defined(DISPLAY_INPUT)
        printf("\n");
#endif
    }

    swcLeonDataCacheFlush();
    DrvLL2CFlushOpOnAllLines(LL2C_OPERATION_WRITE_BACK, 0);
}

static void checkResult(s32 test_no, t_MvTensorParam *mvTensorParam, bool saveOutput)
{
    DrvLL2CFlushOpOnAddrRange(LL2C_OPERATION_INVALIDATE, 0,
            (u32)output, (u32)output + sizeof(output));

    // save output data
    s32 outHeight   = mvTensorParam->output->dimY;
    s32 outWidth    = mvTensorParam->output->dimX;
    s32 outChannels = mvTensorParam->output->dimZ;
    s32 outputSize = outHeight * outWidth * outChannels;

    unitTestAssert(outWidth == gOutWidth_ref[test_no]);
    unitTestAssert(outHeight == gOutHeight_ref[test_no]);
    unitTestAssert(outChannels == gOutChannels_ref[test_no]);

    u32 crc = swcCalcCrc32((u8*)(output), OUT_MAX_SIZE * sizeof(fp16), le_pointer);
    unitTestAssert(crc == expected_crc[test_no]);

#if defined(DISPLAY_OUTPUT)
    printf("crc = %x\n", (unsigned int)crc);
    printf("Output size: %ldx%ldx%ld \n", outWidth, outHeight, outChannels);
    printf("Ref out size: %ldx%ldx%ld \n", gOutWidth_ref[test_no], gOutHeight_ref[test_no], gOutChannels_ref[test_no]);
#endif

    if(saveOutput)
        saveMemoryToFile((u32)output, outputSize * sizeof(fp16) , "outMyriad.bin");

#if defined(DISPLAY_OUTPUT)
    printf("Output volume - row major format.\n");
    for(s32 c_i = 0; c_i < (s32)outChannels; ++c_i)
    {
        for(s32 h_i = 0; h_i < outHeight; ++h_i)
        {
            for(s32 w_i = 0; w_i < outWidth; ++w_i)
            {
                s32 index = c_i + (w_i * outChannels) + (h_i * outWidth * outChannels);
                printf("%2.2f ", f16Tof32(output[index]));
            }
            printf("\n");
        }
        printf("\n");
    }
#endif
}

#endif
