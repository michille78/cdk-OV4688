///
/// @file
/// @copyright All code copyright Movidius Ltd 2012, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     MvTensor Test application
///

// Includes
// ----------------------------------------------------------------------------
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <DrvLeonL2C.h>
#include <Fp16Convert.h>
#include "mv_types.h"
#include "app_config.h"
#include "mvHelpersApi.h"
#include <swcShaveLoaderLocal.h>
#include <cmath>

// MvTensor specific
#include <UnitTestApi.h>
#include <VcsHooksApi.h>
#include "mvTensor.h"
#include "mvPostOpsParam.h"

// Source Specific #defines and types  (typedef,enum,struct)
// ----------------------------------------------------------------------------
//#define DISPLAY_INPUT_VOLUME
//#define DISPLAY_OUTPUT_VOLUME
#define GET_STATS

#define M_MAX 3136
#define K_MAX 1024
#define N_MAX 1024

#define IN_MAX_SIZE (M_MAX * K_MAX)
#define OUT_MAX_SIZE (M_MAX * N_MAX)
#define WEIGHTS_SIZE (K_MAX * N_MAX)

#define LEON_HEAP_SIZE 80000000

// #define PYTHON_TEST 1

// Global data
// ----------------------------------------------------------------------------
DynamicContext_t localModule[MAX_SHAVES];
DynamicContextInstances_elm localModulePrivD[MAX_SHAVES];

u32 DDR_DATA gInWidth     = 56;
u32 DDR_DATA gInHeight    = 56;
u32 DDR_DATA gInChannels  = 32;
u32 DDR_DATA gOutChannels = gInChannels;

t_PowerLayerParams power_params;
const float test_threshold = 0.003;

float gSampleStart = 0.00001f;
float gSampleStep = 0.00001f;

const u32 used_channels = gOutChannels >> 1;
// On the last gUnusedChannels do not apply the post operation (i.e. simulate stride).
u32 DDR_DATA gUsedChannels = used_channels;

// Output padding != 0 not supported.
u32 outputPadding = 0;

u32 DDR_DATA gPostOpStrideX = 1;
u32 DDR_DATA gPostOpStrideY = 1;

u32 DDR_DATA gStartShave = 0;
u32 DDR_DATA gLastShave = LAST_SHAVE;

u32 gDataPartitionNo = 0;
u32 gInstrPartitionNo = 1;

float DDR_DATA gCallDuration = 0;

t_mvTensorGenData inputStruct;
t_mvTensorGenData outputStruct;
t_MvTensorMyriadResources myriadResources;
t_MvTensorDebugInfo dbgInfo;
t_MvTensorOp op;

fp16 DDR_BSS input[IN_MAX_SIZE]    __attribute__((aligned(32)));
fp16 DDR_BSS output[OUT_MAX_SIZE]  __attribute__((aligned(32)));

u8 DDR_BSS leonHeap[LEON_HEAP_SIZE];

char debugMsg[MV_TENSOR_DBG_MSG_SIZE];

dmaTransactionList_t __attribute__((section(".cmx.cdmaDescriptors"))) task[1];

// Functions Definitions
// ----------------------------------------------------------------------------
static void initMvTensorStruct(t_MvTensorParam *mvTensorParam);

#ifndef PYTHON_TEST
static void generateDummyTestData();
static void checkResult(bool save_to_file);
#endif
// Functions Implementation
// ----------------------------------------------------------------------------

int main(void)
{
    initClocksAndMemory();
    mvSetHeap((unsigned int)leonHeap, LEON_HEAP_SIZE);

    int shvNo;
    for(shvNo = 0; shvNo < MAX_SHAVES; shvNo++)
    {
        memcpy(&localModule[shvNo], &MODULE_DATA(mvTensor),sizeof(DynamicContext_t));
        memcpy(&localModulePrivD[shvNo], MODULE_DATA(mvTensor).instancesData, sizeof(localModulePrivD[shvNo]));
        localModule[shvNo].instancesData = &localModulePrivD[shvNo];
    }

    t_MvTensorParam mvTensorParam;
    initMvTensorStruct(&mvTensorParam);

#ifndef PYTHON_TEST
    unitTestInit();
//    for(int lastShv = 0; lastShv < MAX_SHAVES; lastShv++)
    for(int lastShv = 0; lastShv < 1; lastShv++)
    {
        generateDummyTestData();
        mvTensorParam.myriadResources->lastShave = lastShv;
#endif
        printf("Power. stride = width (%lu:%lu)\n", gStartShave, gLastShave);
        gUsedChannels = gOutChannels;
        mvTensorParam.output->dimZ = gOutChannels;

        for(shvNo = 0; shvNo <= lastShv; shvNo++)
            swcSetupDynShaveAppsComplete(&localModule[shvNo], getShaveList() + shvNo, 1);

        mvTensor(&mvTensorParam);

        for(shvNo = 0; shvNo <= lastShv; shvNo++)
            swcCleanupDynShaveApps(&localModule[shvNo]);

        printf("%s\n", mvTensorParam.debugInfo->debugMsg);
        printf("MvTensor done in: %f ms on %d SHAVES\n", mvTensorParam.debugInfo->ms,
                                                         mvTensorParam.myriadResources->lastShave + 1);
        gCallDuration += mvTensorParam.debugInfo->ms;
#ifndef PYTHON_TEST
        checkResult(false);
    }
    unitTestFinalReport();
#endif

    return 0;
}

static void initMvTensorStruct(t_MvTensorParam *mvTensorParam)
{
    power_params.power = 4.5f;
    power_params.scale = 1.0f;
    power_params.shift = 0;

    op.type = kPower;
    op.optMask = MV_TENSOR_DEFAULT_OPT;
    op.radixX = 1;
    op.radixY = 1;
    op.strideX = 1;
    op.strideY = 1;
    op.padX = 0;
    op.padY = 0;
    op.paddStyle = paddStyleNone;
    op.opX = 0;
    op.params = (void *)&power_params;

    // input init
    inputStruct.data = (void*)input;
    inputStruct.dataType = t_fp16;
    inputStruct.dimX = gInWidth;
    inputStruct.dimY = gInHeight;
    inputStruct.dimZ = gInChannels;
    inputStruct.dimXStride = sizeof(fp16) * gInChannels;
    inputStruct.dimYStride = sizeof(fp16) * gInChannels * gInWidth;
    inputStruct.dimZStride = sizeof(fp16);
    inputStruct.storageOrder = orderYXZ;
    mvTensorParam->input = &inputStruct;

    // output init
    outputStruct.data = (void*)(output + outputPadding);
    outputStruct.dataType = t_fp16;
    outputStruct.dimX = gInWidth;
    outputStruct.dimY = gInHeight;
    outputStruct.dimZ = gOutChannels;
    outputStruct.dimZStride = sizeof(fp16);
    outputStruct.dimXStride = outputStruct.dimZStride * outputStruct.dimZ;
    outputStruct.dimYStride = outputStruct.dimXStride * outputStruct.dimX;
    outputStruct.storageOrder = orderYXZ;
    mvTensorParam->output = &outputStruct;

    mvTensorParam->biases = NULL;
    mvTensorParam->weights = NULL;

    // set pre-operation params
    mvTensorParam->preOp = NULL;

    // set operation params
    mvTensorParam->op = &op;

    // set post-operation params
    mvTensorParam->postOp = NULL;

    // set myriad resources
    myriadResources.firstShave = gStartShave;
    myriadResources.lastShave = gLastShave;
    myriadResources.dmaLinkAgent = 1;
    myriadResources.dataPartitionNo = gDataPartitionNo;
    myriadResources.instrPartitionNo = gInstrPartitionNo;
    myriadResources.dmaTransactions = &task[0];
    mvTensorParam->myriadResources = &myriadResources;

    mvTensorParam->debugInfo = &dbgInfo;
    mvTensorParam->debugInfo->debugMsg = debugMsg;
}

#ifndef PYTHON_TEST
static void generateDummyTestData()
{
    memset((void *)output, 0x55, OUT_MAX_SIZE * sizeof(fp16));

    float current = gSampleStart;
    float input_error_mean = 0.0f;
    float input_error_std  = 0.0f;

#if defined(DISPLAY_INPUT_VOLUME)
    printf("Input volume\n");
#endif
    for(u32 h_i = 0; h_i < gInHeight; ++h_i)
    {
        for(u32 w_i = 0; w_i < gInWidth; ++w_i)
        {
            for(u32 c_i = 0; c_i < gInChannels; ++c_i)
            {
                u32 index = c_i + (w_i * gInChannels) + (h_i * gInWidth * gInChannels);

                input[index] = f32Tof16(current);

                input_error_mean += fabs(current - f16Tof32(input[index]));
                input_error_std  += (current - f16Tof32(input[index])) * (current - f16Tof32(input[index]));
                current += gSampleStep;

#if defined(DISPLAY_INPUT_VOLUME)
                printf("%2.2f ", f16Tof32(input[index]));
#endif
            }
#if defined(DISPLAY_INPUT_VOLUME)
            printf("|");
#endif
        }
#if defined(DISPLAY_INPUT_VOLUME)
        printf("\n");
#endif
    }

#if defined(GET_STATS)
    printf("Sampling interval = [%2.5f, %2.5f]\n", current - gSampleStep, gSampleStart);
    float no_samples = (float)(gInChannels * gInWidth * gInHeight);
    printf("no_samples = %.0f\n", no_samples);
    printf("Input error mean = %.6f\n", input_error_mean / no_samples);
    printf("Input error std  = %.6f\n", sqrtf((input_error_std / no_samples) -
            (input_error_mean / no_samples) * (input_error_mean / no_samples)));
#endif

    swcLeonDataCacheFlush();
    DrvLL2CFlushOpOnAllLines(LL2C_OPERATION_WRITE_BACK, 0);
}

void checkResult(bool save_to_file)
{
    DrvLL2CFlushOpOnAddrRange(LL2C_OPERATION_INVALIDATE, 0,
            (u32)output, (u32)output + sizeof(output));

    // save output data
    u32 outputSize = gOutChannels * gInWidth* gInHeight;

    if(save_to_file)
        saveMemoryToFile((u32)output, outputSize * sizeof(fp16) , "outMyriad.bin");

    float current_sample = gSampleStart;

    float output_error_mean = 0.0f;
    float output_error_std  = 0.0f;
    bool threshold_test_failed = false;

#if defined(DISPLAY_OUTPUT_VOLUME)
    printf("Output volume\n");
#endif
    for(u32 h_i = 0; h_i < gInHeight; ++h_i)
    {
        for(u32 w_i = 0; w_i < gInWidth; ++w_i)
        {
            for(u32 c_i = 0; c_i < gOutChannels; ++c_i)
            {
                u32 index = c_i + (w_i * gOutChannels) + (h_i * gInWidth * gOutChannels);
                float value = f16Tof32((unsigned int)output[index]);

                float gt_value = powf(power_params.shift + power_params.scale * current_sample,
                        power_params.power);

#if defined(DISPLAY_OUTPUT_VOLUME)
                printf("%2.3f ", value);
//                printf("%2.3f(%2.3f) ", value, gt_value);
#endif

                float output_value = f16Tof32((unsigned int)output[index]);
                output_error_mean += fabs(gt_value - output_value);
                output_error_std  += (gt_value - output_value) * (gt_value - output_value);

                if (fabs(value - gt_value) > test_threshold)
                {
                    threshold_test_failed = true;
//                    printf("[%2.3f]%2.3f(%2.3f) | %f\n", current_sample, value, gt_value,
//                            fabs(value - gt_value));
                }

                current_sample += gSampleStep;
            }
#if defined(DISPLAY_OUTPUT_VOLUME)
        printf("|");
#endif
        }
#if defined(DISPLAY_OUTPUT_VOLUME)
        printf("\n");
#endif
    }

#if defined(GET_STATS)
    float no_samples = (float)(gOutChannels * gInWidth * gInHeight);
    printf("Output error mean = %.6f\n", output_error_mean / no_samples);
    printf("Output error std  = %.6f\n", sqrtf((output_error_std / no_samples) -
            (output_error_mean / no_samples) * (output_error_mean / no_samples)));
#endif

    if(threshold_test_failed)
        unitTestLogFail();
}

#endif
