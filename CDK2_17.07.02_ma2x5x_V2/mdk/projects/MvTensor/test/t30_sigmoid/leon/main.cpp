///
/// @file
/// @copyright All code copyright Movidius Ltd 2012, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     MvTensor Test application
///

// Includes
// ----------------------------------------------------------------------------
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <DrvLeonL2C.h>
#include <Fp16Convert.h>
#include "mv_types.h"
#include "app_config.h"
#include "mvHelpersApi.h"
#include <swcShaveLoaderLocal.h>
#include <cmath>

// MvTensor specific
#include <UnitTestApi.h>
#include <VcsHooksApi.h>
#include "mvTensor.h"

// Source Specific #defines and types  (typedef,enum,struct)
// ----------------------------------------------------------------------------
//#define DISPLAY_INPUT_VOLUME
//#define DISPLAY_OUTPUT_VOLUME
//#define GET_STATS

// Makes it easier to read the code
#define FP16_1  0x3C00
#define FP16_0  0x0000

#define IN_ODD_COL_VAL              FP16_1
#define IN_EVEN_COL_VAL             FP16_0

#define M_MAX 3136
#define K_MAX 1024
#define N_MAX 1024

#define IN_MAX_SIZE (M_MAX * K_MAX)
#define OUT_MAX_SIZE (M_MAX * N_MAX)
#define WEIGHTS_SIZE (K_MAX * N_MAX)

#define LEON_HEAP_SIZE 80000000

// #define PYTHON_TEST 1

// Global data
// ----------------------------------------------------------------------------
u32 DDR_DATA gInWidth     = 56;
u32 DDR_DATA gInHeight    = 56;
u32 DDR_DATA gInChannels  = 32;
u32 DDR_DATA gOutChannels = 32;

const float test_threshold = 0.001f;

#if defined(GET_STATS)
u32 gNoSamples = 80000;
float gSampleStart = -4.0f;
float gSampleWidth = 0.0001f;
#endif

//u32 DDR_DATA gInWidth     = 8;
//u32 DDR_DATA gInHeight    = 8;
//u32 DDR_DATA gInChannels  = 2;
//u32 DDR_DATA gOutChannels = 2;

const u32 used_channels = gOutChannels >> 1;
// On the last gUnusedChannels do not apply the post operation (i.e. simulate stride).
u32 DDR_DATA gUsedChannels = used_channels;

// Output padding != 0 not supported.
u32 outputPadding = 0;

u32 DDR_DATA gPostOpStrideX = 1;
u32 DDR_DATA gPostOpStrideY = 1;

u32 DDR_DATA gStartShave = 0;
u32 DDR_DATA gLastShave = LAST_SHAVE;

u32 gDataPartitionNo = 0;
u32 gInstrPartitionNo = 1;

float DDR_DATA gCallDuration = 0;

t_mvTensorGenData inputStruct;
t_mvTensorGenData outputStruct;
t_MvTensorMyriadResources myriadResources;
t_MvTensorDebugInfo dbgInfo;
t_MvTensorOp op;

fp16 DDR_BSS input[IN_MAX_SIZE]    __attribute__((aligned(32)));
fp16 DDR_BSS output[OUT_MAX_SIZE]  __attribute__((aligned(32)));

u8 DDR_BSS leonHeap[LEON_HEAP_SIZE];

char debugMsg[MV_TENSOR_DBG_MSG_SIZE];

DynamicContext_t localModule[MAX_SHAVES];
DynamicContextInstances_elm localModulePrivD[MAX_SHAVES];

dmaTransactionList_t __attribute__((section(".cmx.cdmaDescriptors"))) task[1];

// Functions Definitions
// ----------------------------------------------------------------------------
static void initMvTensorStruct(t_MvTensorParam *mvTensorParam);

#ifndef PYTHON_TEST
static void generateDummyTestData();
static void checkResult(bool save_to_file);
#endif
// Functions Implementation
// ----------------------------------------------------------------------------

int main(void)
{
    initClocksAndMemory();
    mvSetHeap((unsigned int)leonHeap, LEON_HEAP_SIZE);

    t_MvTensorParam mvTensorParam;
    initMvTensorStruct(&mvTensorParam);

    int shvNo;
    for(shvNo = 0; shvNo < MAX_SHAVES; shvNo++)
    {
        memcpy(&localModule[shvNo], &MODULE_DATA(mvTensor),sizeof(DynamicContext_t));
        memcpy(&localModulePrivD[shvNo], MODULE_DATA(mvTensor).instancesData, sizeof(localModulePrivD[shvNo]));
        localModule[shvNo].instancesData = &localModulePrivD[shvNo];
        swcSetupDynShaveAppsComplete(&localModule[shvNo], &(getShaveList()[shvNo]), 1);
    }

#ifndef PYTHON_TEST
    unitTestInit();
#endif


#ifndef PYTHON_TEST
//    for(int lastShv = 0; lastShv < MAX_SHAVES; lastShv++)
    for(int lastShv = 0; lastShv < 1; lastShv++)
    {
        generateDummyTestData();
        mvTensorParam.myriadResources->lastShave = lastShv;
#endif
        printf("Sigmoid. stride = width (%lu:%lu)\n", gStartShave, gLastShave);
        gUsedChannels = gOutChannels;
        mvTensorParam.output->dimZ = gOutChannels;
        mvTensor(&mvTensorParam);
        printf("%s\n", mvTensorParam.debugInfo->debugMsg);
        printf("MvTensor done in: %f ms on %d SHAVES\n", mvTensorParam.debugInfo->ms,
                                                         mvTensorParam.myriadResources->lastShave + 1);
        gCallDuration += mvTensorParam.debugInfo->ms;
#ifndef PYTHON_TEST
        checkResult(false);
    }
#endif

//#ifndef PYTHON_TEST
//    for(int lastShv = 0; lastShv < MAX_SHAVES; lastShv++)
//    {
//        generateDummyTestData();
//        mvTensorParam.myriadResources->lastShave = lastShv;
//#endif
//        bzero(output + outputPadding, gOutChannels * gInWidth * gInHeight * sizeof(fp16));
//        printf("Bias. stride > width (%lu:%lu)\n", gStartShave, gLastShave);
//        gUsedChannels = used_channels;
//        mvTensorParam.output->dimZ = gUsedChannels;
//        mvTensorParam.weights->dimZ = gUsedChannels;
//        mvTensor(&mvTensorParam);
//        printf("%s\n", mvTensorParam.debugInfo->debugMsg);
//        printf("MvTensor done in: %f ms on %d SHAVES\n", mvTensorParam.debugInfo->ms,
//                                                         mvTensorParam.myriadResources->lastShave + 1);
//        gCallDuration += mvTensorParam.debugInfo->ms;
//#ifndef PYTHON_TEST
//        checkResult();
//    }
//#endif

    for(shvNo = 0; shvNo < MAX_SHAVES; shvNo++)
        swcCleanupDynShaveApps(&localModule[shvNo]);

#ifndef PYTHON_TEST
    unitTestFinalReport();
#endif

    return 0;
}

static void initMvTensorStruct(t_MvTensorParam *mvTensorParam)
{
    op.type = kSigmoid;
    op.optMask = MV_TENSOR_DEFAULT_OPT;
    op.radixX = 1;
    op.radixY = 1;
    op.strideX = 1;
    op.strideY = 1;
    op.padX = 0;
    op.padY = 0;
    op.paddStyle = paddStyleNone;
    op.opX = 0;

    // input init
    inputStruct.data = (void*)input;
    inputStruct.dataType = t_fp16;
    inputStruct.dimX = gInWidth;
    inputStruct.dimY = gInHeight;
    inputStruct.dimZ = gInChannels;
    inputStruct.dimXStride = sizeof(fp16) * gInChannels;
    inputStruct.dimYStride = sizeof(fp16) * gInChannels * gInWidth;
    inputStruct.dimZStride = sizeof(fp16);
    inputStruct.storageOrder = orderYXZ;
    mvTensorParam->input = &inputStruct;

    // output init
    outputStruct.data = (void*)(output + outputPadding);
    outputStruct.dataType = t_fp16;
    outputStruct.dimX = gInWidth;
    outputStruct.dimY = gInHeight;
    outputStruct.dimZ = gOutChannels;
    outputStruct.dimZStride = sizeof(fp16);
    outputStruct.dimXStride = outputStruct.dimZStride * outputStruct.dimZ;
    outputStruct.dimYStride = outputStruct.dimXStride * outputStruct.dimX;
    outputStruct.storageOrder = orderYXZ;
    mvTensorParam->output = &outputStruct;

    mvTensorParam->biases = NULL;
    mvTensorParam->weights = NULL;

    // set pre-operation params
    mvTensorParam->preOp = NULL;

    // set operation params
    mvTensorParam->op = &op;

    // set post-operation params
    mvTensorParam->postOp = NULL;

    // set myriad resources
    myriadResources.firstShave = gStartShave;
    myriadResources.lastShave = gLastShave;
    myriadResources.dmaLinkAgent = 1;
    myriadResources.dataPartitionNo = gDataPartitionNo;
    myriadResources.instrPartitionNo = gInstrPartitionNo;
    myriadResources.dmaTransactions = &task[0];
    mvTensorParam->myriadResources = &myriadResources;

    mvTensorParam->debugInfo = &dbgInfo;
    mvTensorParam->debugInfo->debugMsg = debugMsg;
}

#ifndef PYTHON_TEST

static void generateDummyTestData()
{

    memset((void *)output, 0x55, OUT_MAX_SIZE * sizeof(fp16));

    // INPUT
    // O = IN_ODD_COL_VAL
    // E = IN_EVEN_COL_VAL
    //    c0 c1 c2 c3 ... c_Ninput
    // w0  E  E  E  E ... c_Ninput
    // w1  O  O  O  O ... c_Ninput
    // w2  E  E  E  E ... c_Ninput
    // w3  O  O  O  O ... c_Ninput
    // ...
    // Output will be
    // N = c_Ninput
    // Bi = bias for output channel i
    //    c0    c1    c2    c3    ... c_NOutput
    // w0 NE+Bi NE+Bi NE+Bi NE+Bi ... c_NOutput
    // w1 NO+Bi NO+Bi NO+Bi NO+Bi ... c_NOutput
    // w2 NE+Bi NE+Bi NE+Bi NE+Bi ... c_NOutput
    // w3 NO+Bi NO+Bi NO+Bi NO+Bi ... c_NOutput
    // ...

#if defined(GET_STATS)
    float current = gSampleStart;
#endif

#if defined(DISPLAY_INPUT_VOLUME)
    printf("Input volume\n");
#endif
    for(u32 h_i = 0; h_i < gInHeight; ++h_i)
    {
        for(u32 w_i = 0; w_i < gInWidth; ++w_i)
        {
            for(u32 c_i = 0; c_i < gInChannels; ++c_i)
            {
                u32 index = c_i + (w_i * gInChannels) + (h_i * gInWidth * gInChannels);

#if defined(GET_STATS)
                input[index] = f32Tof16(current);
                current += gSampleWidth;
#else
                input[index] = f32Tof16((unsigned int)c_i);
#endif

#if defined(DISPLAY_INPUT_VOLUME)
                printf("%2.2f ", f16Tof32(input[index]));
#endif
            }
#if defined(DISPLAY_INPUT_VOLUME)
            printf("|");
#endif
        }
#if defined(DISPLAY_INPUT_VOLUME)
        printf("\n");
#endif
    }

    swcLeonDataCacheFlush();
    DrvLL2CFlushOpOnAllLines(LL2C_OPERATION_WRITE_BACK, 0);
}

#if defined(GET_STATS)
void gatherStatistics()
{
    float no_samples = (float)(gNoSamples);
    float current = gSampleStart;

    float mean_input = 0.0f;
    float std_input  = 0.0f;

    float mean_output = 0.0f;
    float std_output  = 0.0f;

    for(u32 h_i = 0; h_i < gInHeight; ++h_i)
    {
        for(u32 w_i = 0; w_i < gInWidth; ++w_i)
        {
            for(u32 c_i = 0; c_i < gOutChannels; ++c_i)
            {
                u32 index = c_i + (w_i * gOutChannels) + (h_i * gInWidth * gOutChannels);

                if(index < gNoSamples)
                {
                    fp16 current_half = f32Tof16(current);
                    if(index == (gNoSamples - 1))
                    {
                        printf("Last value = %2.6f\n", current);
                    }

                    mean_input += fabs(current - f16Tof32(current_half));
                    std_input  += (current - f16Tof32(current_half)) * (current - f16Tof32(current_half));

                    float gt_value = 1.0f / (1.0f + exp(-current));
                    float output_value = f16Tof32((unsigned int)output[index]);
                    mean_output += fabs(gt_value - output_value);
                    std_output  += (gt_value - output_value) * (gt_value - output_value);

                    current += gSampleWidth;
                }
            }
        }
    }

    printf("Input Mean = %.6f\n", mean_input / no_samples);
    printf("Input Std  = %.6f\n", sqrtf((std_input / no_samples) - (mean_input / no_samples) * (mean_input / no_samples)));

    printf("Output Mean = %.6f\n", mean_output / no_samples);
    printf("Output Std  = %.6f\n", sqrtf((std_output / no_samples) - (mean_output / no_samples) * (mean_output / no_samples)));

}
#endif

void checkResult(bool save_to_file)
{
    DrvLL2CFlushOpOnAddrRange(LL2C_OPERATION_INVALIDATE, 0,
            (u32)output, (u32)output + sizeof(output));

    // save output data
    u32 outputSize = gOutChannels * gInWidth* gInHeight;

    if(save_to_file)
        saveMemoryToFile((u32)output, outputSize * sizeof(fp16) , "outMyriad.bin");

#if defined(GET_STATS)
    gatherStatistics();
#endif

#if defined(DISPLAY_OUTPUT_VOLUME)
    printf("Output volume\n");
#endif
    for(u32 h_i = 0; h_i < gInHeight; ++h_i)
    {
        for(u32 w_i = 0; w_i < gInWidth; ++w_i)
        {
            for(u32 c_i = 0; c_i < gOutChannels; ++c_i)
            {
                u32 index = c_i + (w_i * gOutChannels) + (h_i * gInWidth * gOutChannels);
                float value = f16Tof32((unsigned int)output[index]);
                float gt_value = 1.0f / (1.0f + exp(-(float)c_i));

#if defined(DISPLAY_OUTPUT_VOLUME)
                printf("%2.3f ", value);
//                printf("%2.3f(%2.3f) ", value, gt_value);
#endif

                if (fabs(value - gt_value) > test_threshold)
                {
                    printf("Expected = %2.5f | Got = %2.5f | Diff = %2.5f\n", gt_value, value, fabs(value - gt_value));
                    unitTestLogFail();
                    return;
                }
            }
#if defined(DISPLAY_OUTPUT_VOLUME)
        printf("|");
#endif
        }
#if defined(DISPLAY_OUTPUT_VOLUME)
        printf("\n");
#endif
    }

}

#endif
