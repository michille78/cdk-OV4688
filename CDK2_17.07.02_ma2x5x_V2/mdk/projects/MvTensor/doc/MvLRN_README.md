# Cross Map Local Response Normalization

## Algorithm

It uses the band matrix algorithm used in Tensorflow, which, according
to comments in Tensorflow, is adequate for nplanes <= 384. It can be
only used with YXZ ordering.

It works in three steps:

First it calculcates the squares of the input
Second it multiplies these squares (organized as a matrix with
width * height rows and nplanes columns) with a square band matrix
with nplanes rows and columns composed of ones and zeroes to obtain a
width * height x nplanes matrix with the partial sums.
Third, it multiplies each member of the input matrix with
(k+alpha/size * sums)^-beta, where sums are the numbers
obtained in the second step (same size matrix as input matrix).

The first and third operations can be evenly divided between the
shaves, so they scale well with the number of shaves. The second
operation is performed using MvMatMul.

## Limitations

The number of nplanes has to be a multiple of 8.

The only other limitations come from MvMatMul, so

width*height < Mmax
nplanes < Nmax
nplanes < Kmax

where Mmax, Nmax and Kmax are MvMatMul limits.
