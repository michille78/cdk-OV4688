# 1. Update repo and clean, clean elfs
make clean
make mrproper

mkdir -p ./cdk_elfs

rm -rf ./cdk_elfs/*.elf
rm -rf ./cdk_elfs/*.mvcmd

# 2. apply cherry picks and patches

# 3. Build Tests and copy elf

# 3.1 Test A214M_ma2150
make clean
make mrproper
make configs/rtems_sensor_sdk_A214M_ma2150
make install_header
make -j4
make configs/rtems_mv182_A214M_ma2150
make -j4
cd mdk/projects/Ipipe2/apps/GuzziSpi_mv182_A214M_ma2150/
make clean
make -j4
make spi-boot
# Copy elf
cp output/GuzziSpi_mv182_A214M_ma2150.elf ../../../../../cdk_elfs/
cp output/GuzziSpi_mv182_A214M_ma2150_ddrinit.mvcmd ../../../../../cdk_elfs/
# Go back to root
cd ../../../../../

# 3.2 Test A208M_B208M_C208M_ma2150
make clean
make mrproper
make configs/rtems_sensor_sdk_A208M_B208M_C208M_ma2150
make install_header
make -j4
make configs/rtems_mv182_A208M_B208M_C208M_ma2150
make -j4
cd mdk/projects/Ipipe2/apps/GuzziSpi_mv182_A208M_B208M_C208M_ma2150/
make clean
make -j4
make spi-boot
# Copy elf
cp output/GuzziSpi_mv182_A208M_B208M_C208M_ma2150.elf ../../../../../cdk_elfs/
cp output/GuzziSpi_mv182_A208M_B208M_C208M_ma2150_ddrinit.mvcmd ../../../../../cdk_elfs/
# Go back to root
cd ../../../../../

# 3.3 Test A208M_B208M_C208S_ma2150
make clean
make mrproper
make configs/rtems_sensor_sdk_A208M_B208M_C208S_ma2150
make install_header
make -j4
make configs/rtems_mv182_A208M_B208M_C208S_ma2150
make -j4
cd mdk/projects/Ipipe2/apps/GuzziSpi_mv182_A208M_B208M_C208S_ma2150/
make clean
make -j4
make spi-boot
# Copy elf
cp output/GuzziSpi_mv182_A208M_B208M_C208S_ma2150.elf ../../../../../cdk_elfs/
cp output/GuzziSpi_mv182_A208M_B208M_C208S_ma2150_ddrinit.mvcmd ../../../../../cdk_elfs/
# Go back to root
cd ../../../../../

# 3.4 Test A208M_B208S_C208S_ma2150
make clean
make mrproper
make configs/rtems_sensor_sdk_A208M_B208S_C208S_ma2150
make install_header
make -j4
make configs/rtems_mv182_A208M_B208S_C208S_ma2150
make -j4
cd mdk/projects/Ipipe2/apps/GuzziSpi_mv182_A208M_B208S_C208S_ma2150/
make clean
make -j4
make spi-boot
# Copy elf
cp output/GuzziSpi_mv182_A208M_B208S_C208S_ma2150.elf ../../../../../cdk_elfs/
cp output/GuzziSpi_mv182_A208M_B208S_C208S_ma2150_ddrinit.mvcmd ../../../../../cdk_elfs/
# Go back to root
cd ../../../../../

# 3.5 Test A214M_B208M_C208S_ma2150
make clean
make mrproper
make configs/rtems_sensor_sdk_A214M_B208M_C208S_ma2150
make install_header
make -j4
make configs/rtems_mv182_A214M_B208M_C208S_ma2150
make -j4
cd mdk/projects/Ipipe2/apps/GuzziSpi_mv182_A214M_B208M_C208S_ma2150/
make clean
make -j4
make spi-boot
# Copy elf
cp output/GuzziSpi_mv182_A214M_B208M_C208S_ma2150.elf ../../../../../cdk_elfs/
cp output/GuzziSpi_mv182_A214M_B208M_C208S_ma2150_ddrinit.mvcmd ../../../../../cdk_elfs/
# Go back to root
cd ../../../../../

#perform clean only if folder exists, otherwise only copy files from cdk_elfs/prebuilt folder
# 3.6 Test depthmap_mv182_A208M_B208S_C208S_ma2150
if test -d mdk/projects/Ipipe2/apps/GuzziSpi_depthmap_mv182_A208M_B208S_C208S_ma2150/
then
	make clean
	make mrproper
	make configs/rtems_sensor_sdk_depthmap_mv182_A208M_B208S_C208S_ma2150
	make install_header
	make -j4
	make configs/depthmap_mv182_A208M_B208S_C208S_ma2150
	make -j4
	cd mdk/projects/Ipipe2/apps/GuzziSpi_depthmap_mv182_A208M_B208S_C208S_ma2150/
	make clean
	make -j4
	make spi-boot
	# Copy elf
	cp output/GuzziSpi_depthmap_mv182_A208M_B208S_C208S_ma2150.elf ../../../../../cdk_elfs/
	cp output/GuzziSpi_depthmap_mv182_A208M_B208S_C208S_ma2150_ddrinit.mvcmd ../../../../../cdk_elfs/
	# Go back to root
	cd ../../../../../
else
	[ -f cdk_elfs/prebuilt/GuzziSpi_depthmap_mv182_A208M_B208S_C208S_ma2150.elf   ] && cp cdk_elfs/prebuilt/GuzziSpi_depthmap_mv182_A208M_B208S_C208S_ma2150.elf cdk_elfs/
	[ -f cdk_elfs/prebuilt/GuzziSpi_depthmap_mv182_A208M_B208S_C208S_ma2150_ddrinit.mvcmd ] && cp cdk_elfs/prebuilt/GuzziSpi_depthmap_mv182_A208M_B208S_C208S_ma2150_ddrinit.mvcmd cdk_elfs/
fi

# 3.7 Test ShaveSAD_mv182_A208M_B208S_C208S_ma2150
# confis are same as depthmap
make clean
make mrproper
make configs/rtems_sensor_sdk_depthmap_mv182_A208M_B208S_C208S_ma2150
make install_header
make -j4
make configs/depthmap_mv182_A208M_B208S_C208S_ma2150
make -j4
cd mdk/projects/Ipipe2/apps/GuzziSpi_mv182_ShaveSAD_A208M_B208S_C208S_ma2150/
make clean
make -j4
make spi-boot
# Copy elf
cp output/GuzziSpi_mv182_ShaveSAD_A208M_B208S_C208S_ma2150.elf ../../../../../cdk_elfs/
cp output/GuzziSpi_mv182_ShaveSAD_A208M_B208S_C208S_ma2150_ddrinit.mvcmd ../../../../../cdk_elfs/
# Go back to root
cd ../../../../../
