/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file osal_stdtypes.h
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#ifndef __OSAL_STDTYPES_H__
#define __OSAL_STDTYPES_H__

#if defined(___LINUX___)
#include "linux/osal_stdtypes.h"
#elif defined(___RTEMS___)
#include "rtems/osal_stdtypes.h"
#elif defined(___ANDROID___)
#include "android/osal_stdtypes.h"
#else
#endif

#define OSAL_PTR2PTR(type, var) ((void*)((type*)&(var)))

#define ARRAY_SIZE(arr)         ((int)(sizeof(arr) / sizeof((arr)[0])))

#define ALIGN(x, a)             ALIGN_MASK(x, (typeof(x))(a) - 1)
#define ALIGN_MASK(x, mask)     (((x) + (mask)) & ~(mask))

#undef offsetof
#ifdef __compiler_offsetof
#define offsetof(TYPE,MEMBER) __compiler_offsetof(TYPE,MEMBER)
#else   // __compiler_offsetof
#ifdef __builtin_offsetof
#define offsetof(TYPE,MEMBER) __builtin_offsetof(TYPE,MEMBER)
#else   // __builtin_offsetof
#define offsetof(TYPE, MEMBER) ((size_t) &((TYPE *)0)->MEMBER)
#endif  // __builtin_offsetof
#endif  // __compiler_offsetof

#ifndef min
#define min(x, y) ({ \
	typeof(x) _min1 = (x); \
	typeof(y) _min2 = (y); \
	(void) (&_min1 == &_min2); \
	_min1 < _min2 ? _min1 : _min2; })
#endif

#ifndef max
#define max(x, y) ({ \
	typeof(x) _max1 = (x); \
	typeof(y) _max2 = (y); \
	(void) (&_max1 == &_max2); \
	_max1 > _max2 ? _max1 : _max2; })
#endif

/**
 * clamp - return a value clamped to a given range with strict typechecking
 * @val: current value
 * @min: minimum allowable value
 * @max: maximum allowable value
 *
 * This macro does strict typechecking of min/max to make sure they are of the
 * same type as val.  See the unnecessary pointer comparisons.
 */
#ifndef clamp
#define clamp(val, min, max) ({ \
	typeof(val) __val = (val); \
	typeof(min) __min = (min); \
	typeof(max) __max = (max); \
	(void) (&__val == &__min); \
	(void) (&__val == &__max); \
	__val = __val < __min ? __min : __val;	\
	__val > __max ? __max : __val; })
#endif

/*
 * ..and if you can't take the strict
 * types, you can specify one yourself.
 *
 * Or not use min/max/clamp at all, of course.
 */
#ifndef min_t
#define min_t(type, x, y) ({ \
	type __min1 = (x); \
	type __min2 = (y); \
	__min1 < __min2 ? __min1 : __min2; })
#endif

#ifndef max_t
#define max_t(type, x, y) ({ \
	type __max1 = (x); \
	type __max2 = (y); \
	__max1 > __max2 ? __max1 : __max2; })
#endif

/**
 * clamp_t - return a value clamped to a given range using a given type
 * @type: the type of variable to use
 * @val: current value
 * @min: minimum allowable value
 * @max: maximum allowable value
 *
 * This macro does no typechecking and uses temporary variables of type
 * 'type' to make all the comparisons.
 */
#ifndef clamp_t
#define clamp_t(type, val, min, max) ({ \
	type __val = (val); \
	type __min = (min); \
	type __max = (max); \
	__val = __val < __min ? __min : __val; \
	__val > __max ? __max : __val; })
#endif

/**
 * clamp_val - return a value clamped to a given range using val's type
 * @val: current value
 * @min: minimum allowable value
 * @max: maximum allowable value
 *
 * This macro does no typechecking and uses temporary variables of whatever
 * type the input argument 'val' is.  This is useful when val is an unsigned
 * type and min and max are literals that will otherwise be assigned a signed
 * integer type.
 */
#ifndef clamp_val
#define clamp_val(val, min, max) ({ \
	typeof(val) __val = (val); \
	typeof(val) __min = (min); \
	typeof(val) __max = (max); \
	__val = __val < __min ? __min : __val; \
	__val > __max ? __max : __val; })
#endif


#ifndef FALSE
#define FALSE 0
#endif

#ifndef TRUE
#define TRUE 1
#endif

#undef NULL
#if defined(__cplusplus)
#define NULL 0
#else
#define NULL ((void *)0)
#endif

#define OSAL_CAST(dst, src) \
    ({dst = (typeof(dst))(&(src));})

#endif // __OSAL_STDTYPES_H__
