#ifndef  __OSAL_TIME_SYMBIAN_H__
#define  __OSAL_TIME_SYMBIAN_H__

struct timeval {
    unsigned long tv_sec;
    unsigned long tv_usec;
};

typedef struct timeval osal_timeval;

#endif /* __OSAL_TIME_SYMBIAN_H__ */

