#ifndef  __OSAL_MATH_SYMBIAN_H__
#define  __OSAL_MATH_SYMBIAN_H__

#define osal_abs(size)          ((size)<0 ?(-(size)):(size))

#endif /* __OSAL_MATH_SYMBIAN_H__ */
