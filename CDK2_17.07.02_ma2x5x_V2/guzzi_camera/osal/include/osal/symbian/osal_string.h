#ifndef  __OSAL_STRING_SYMBIAN_H__
#define  __OSAL_STRING_SYMBIAN_H__

#include <kern_priv.h>

int osal_strcmp_fn (const char *str1, const char *str2);
int osal_strncmp_fn (const char *str1, const char *str2, unsigned int num);
char *osal_strcpy_fn (char *dst, const char *src);
char *osal_strncpy_fn (char *dst, const char *src, unsigned int num);
int osal_memcmp_fn (void *p1, void *p2, size_t num);
int osal_strlen_fn (char *);
int osal_strspn_fn(char *string, char *strCharSet);
int osal_strcat_fn(char *string, char *strCharSet);
char *osal_strchr_fn(char *string, char strCharSet);

#define osal_memcpy(dst, src, size)     memcpy((dst), (src), (size))
#define osal_memmove(dst, src, size)    { Kern::Printf("\nPlease Implement osal_memmove for Symbian !!!\n"); }
#define osal_memset(dst, val, size)     memset((dst), (val), (size))
#define osal_memcmp(dst, src, size)     osal_memcmp_fn((void *)(dst), (void *)(src), (size))

#define osal_strcmp(dst, src)           osal_strcmp_fn((dst), (src))
#define osal_strcpy(dst, src)           osal_strcpy_fn((dst), (src))
#define osal_strncpy(dst, src, size)    osal_strncpy_fn((dst), (src), (size))
#define osal_strspn(dst, src)           osal_strspn_fn((dst), (src))
#define osal_strcat(dst, src)           osal_strcat_fn((dst), (src))
#define osal_strchr(dst, src)           osal_strchr_fn((dst), (src))
#define osal_strlen(str)                osal_strlen_fn((str))

#define osal_strncasecmp                { Kern::Printf("\nPlease Implement osal_strncasecmp  for Symbian !!!\n"); }

#endif // __OSAL_STRING_SYMBIAN_H__

