#ifndef  __OSAL_STDLIB_SYMBIAN_H__
#define  __OSAL_STDLIB_SYMBIAN_H__

#define osal_malloc(size)                        Kern::Alloc((size))
#define osal_free(ptr)                           Kern::Free((ptr))
#define osal_calloc(num, size)                   Kern::Alloc((size) * (num))
#define osal_memalign(num, boundary)             Kern::Alloc((size) * (num))

#endif // __OSAL_STDLIB_SYMBIAN_H__

