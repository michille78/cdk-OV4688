#ifndef  __OSAL_STDIO_SYMBIAN_H__
#define  __OSAL_STDIO_SYMBIAN_H__

#define osal_printf              Kern::Printf
#define osal_fprintf             { Kern::Printf("\nPlease Implement osal_fprintf for Symbian !!!\n"); }
#define osal_fflush              { Kern::Printf("\nPlease Implement osal_fflush for Symbian !!!\n"); }

typedef struct FILE {
    int dummy;
} OSAL_FILE;

OSAL_FILE *osal_fopen(char *fn, char *par);
int osal_fgets(char *buffer, int LINESIZE, OSAL_FILE *file);
int osal_fclose(OSAL_FILE *file);

#endif // __OSAL_STDIO_SYMBIAN_H__

