#ifndef  __OSAL_STDIO_DSPBIOS_H__
#define  __OSAL_STDIO_DSPBIOS_H__

#include <stdio.h>

#define osal_printf(ARGS, ...)
#define osal_sprintf(ARGS, ...)
#define osal_fprintf(mem, ARGS, ...)
#define osal_sprintf(buf, format, ARGS, ...)
char *osal_fopen(char *fname, char *mode);
void osal_fclose(char *mem);
#define OSAL_FILE                  char

#endif // __OSAL_STDIO_DSPBIOS_H__

