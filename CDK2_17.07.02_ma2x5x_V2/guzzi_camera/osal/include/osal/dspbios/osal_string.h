#ifndef  __OSAL_STRING_DSPBIOS_H__
#define  __OSAL_STRING_DSPBIOS_H__

#include <osal/osal_stdtypes.h>
#include <osal/osal_sysdep.h>

#include <string.h>
#include <ctype.h>

#define osal_memcpy(dst, src, size)     memcpy
#define osal_memmove(dst, src, size)    memmove
#define osal_memset(dst, val, size)     memset
#define osal_memcmp(dst, src, size)     memcmp

#define osal_strcpy(dst, src)           strcpy
#define osal_strncpy(dst, src, size)    strncpy
#define osal_strncasecmp                strncasecmp
#define osal_strspn                     strspn
#define osal_strpbrk                    strpbrk
#define osal_strlen                     strlen
#define osal_isspace                    isspace
#define osal_strtok                     strtok
#define osal_strstr                     strstr
#define osal_strcat                     strcat
#define osal_strchr                     strchr
#define osal_strcmp                     strcmp

#endif /* __OSAL_STRING_DSPBIOS_H__ */

