#ifndef  __OSAL_TIME_DSPBIOS_H__
#define  __OSAL_TIME_DSPBIOS_H__

#include <osal/osal_stdtypes.h>
#include <timm_osal_interfaces.h>
#include <time.h>

struct timeval {
    uint32 tv_sec;
    uint32 tv_usec;
};

typedef struct timeval osal_timeval;

#endif /* __OSAL_TIME_DSPBIOS_H__ */

