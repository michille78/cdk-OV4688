#ifndef  __OSAL_STDLIB_DSPBIOS_H__
#define  __OSAL_STDLIB_DSPBIOS_H__

#include <osal/osal_stdtypes.h>
#include <osal/osal_sysdep.h>

#include <platform/osal/timm_osal_stdtypes.h>
#include <platform/osal/timm_osal_error.h>
#include <platform/osal/timm_osal_trace.h>
#include <platform/osal/timm_osal_memory.h>
#include <platform/osal/timm_osal_memory.h>

#define osal_malloc(size)                        TIMM_OSAL_Malloc((size), TIMM_OSAL_TRUE, 0, TIMMOSAL_MEM_SEGMENT_EXT)
#define osal_free(ptr)                           TIMM_OSAL_Free((ptr))
#define osal_memalign(size, boundary)            memalign(size, boundary)
#define osal_mallocaligned(ptr, aligment, size)  posix_memalign(ptr, aligment, size)
static inline void *osal_calloc(num, size)
{
    void   *ptr;

    ptr = osal_malloc((num * size));
    if ( NULL != ptr ) {
        TIMM_OSAL_Memset(ptr, 0, size * num);
    }
    return (ptr);
}

#endif // __OSAL_STDLIB_DSPBIOS_H__

