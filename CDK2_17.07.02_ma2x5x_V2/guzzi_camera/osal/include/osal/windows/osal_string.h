#ifndef  __OSAL_STRING_WINDOWS_H__
#define  __OSAL_STRING_WINDOWS_H__

#include <string.h>

#define osal_memcpy                     memcpy
#define osal_memmove                    memmove
#define osal_memset                     memset
#define osal_memcmp                     memcmp

#define osal_strcpy                     strcpy
#define osal_strncpy                    strncpy
#define osal_strncasecmp                strncasecmp
#define osal_strspn                     strspn
#define osal_strpbrk                    strpbrk
#define osal_strlen                     strlen
#define osal_isspace                    isspace
#define osal_strtok                     strtok
#define osal_strcat                     strcat
#define osal_strchr                     strchr

#endif /* __OSAL_STRING_WINDOWS_H__ */

