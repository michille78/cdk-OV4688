#ifndef  __OSAL_STDLIB_WINDOWS_H__
#define  __OSAL_STDLIB_WINDOWS_H__

#include <stdlib.h>

#define osal_malloc(size)                        malloc((size))
#define osal_free(ptr)                           free((ptr))
#define osal_calloc(num, size)                   calloc(num, size)
#define osal_memalign(size, boundary)            memalign(size, boundary)

#endif // __OSAL_STDLIB_WINDOWS_H__

