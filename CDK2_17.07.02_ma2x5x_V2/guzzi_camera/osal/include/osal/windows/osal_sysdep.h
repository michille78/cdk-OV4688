#ifndef  __OSAL_SYS_DEP_WINDOWS_H__
#define  __OSAL_SYS_DEP_WINDOWS_H__

#include <windows.h>    /* ULONG_PTR...; for Winbase.h :( */
#include <Winbase.h>    /* DLL functions*/
#include <io.h>         /* _read, _write ... */
#include <windef.h>
#include <winsock.h>

#endif /* __OSAL_SYS_DEP_WINDOWS_H__ */

