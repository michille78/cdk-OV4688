#ifndef  __OSAL_STDIO_WINDOWS_H__
#define  __OSAL_STDIO_WINDOWS_H__

#include <stdio.h>

#define osal_printf              printf
#define osal_sprintf             sprintf
#define osal_sscanf              sscanf
#define osal_fprintf             fprintf
#define osal_fopen               fopen
#define osal_fclose              fclose
#define osal_fwrite              fwrite
#define osal_fread               fread
#define osal_fflush              fflush
#define osal_fgets               fgets
#define osal_fseek               fseek
#define osal_ftell               ftell

#endif // __OSAL_STDIO_WINDOWS_H__

