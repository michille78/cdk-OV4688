/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file mms_debug_v2.h
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#ifndef __MMS_DEBUG_V2_H__
#define __MMS_DEBUG_V2_H__


#ifndef __MMS_DEBUG__

#undef mmsdbg
#define mmsdbg(level, format, arg...)           {;}

#undef mmssys
#define mmssys(level, format, arg...)           {;}

#undef mmsinfo
#define mmsinfo(level, format, arg...)          {;}

#undef mmsinfot
#define mmsinfot(level, format, arg...)         {;}

#undef mmsinfodt
#define mmsinfodt(level, tv, format, arg...)    {;}

#undef mmsdbgt
#define mmsdbgt(level, format, arg...)          {;}

#undef mmsdbgdt
#define mmsdbgdt(level, tv, format, arg...)     {;}

#undef mmsdbgadt
#define mmsdbgadt(level, tv, format, arg...)    {;}

#undef mmsdbgtrace
#define mmsdbgtrace(format, arg...)             {;}

#undef  mmsdump
#define mmsdump(addr,buf)                       {;}

#undef mmsdbgdump
#define mmsdbgdump(level, str, addr, count)     {;}

#else

#include <stdio.h>
#include <unistd.h>
#include <limits.h>
#include <osal/osal_sysdep.h>
#include <osal/osal_stdtypes.h>
#include <osal/osal_time.h>

#undef DL_FATAL
#undef DL_ERROR
#undef DL_WARNING
#undef DL_FUNC
#undef DL_MESSAGE
#undef DL_LAYER0
#undef DL_LAYER1
#undef DL_LAYER2
#undef DL_LAYER3
#undef DL_LAYER4
#undef DL_LAYER5
#undef DL_LAYER6
#undef DL_LAYER7
#undef DL_LAYER8
#undef DL_LAYER9
#undef DL_LAYER10
#undef DL_LAYER11
#undef DL_LAYER12
#undef DL_LAYER13
#undef DL_LAYER14
#undef DL_LAYER15
#undef DL_LAYER16
#undef DL_LAYER17
#undef DL_LAYER18
#undef DL_LAYER19
#undef DL_LAYER20
#undef DL_LAYER21
#undef DL_LAYER22
#undef DL_LAYER23
#undef DL_LAYER24
#undef DL_LAYER25

#undef DL_FULL
#undef DL_DEFAULT

#define DL_FATAL       (0)
#define DL_ERROR       (1)
#define DL_WARNING     (2)
#define DL_MESSAGE     (4)
#define DL_LAYER0      (5)
#define DL_LAYER1      (6)
#define DL_LAYER2      (7)
#define DL_LAYER3      (8)
#define DL_LAYER4      (9)
#define DL_LAYER5      (10)
#define DL_LAYER6      (11)
#define DL_LAYER7      (12)
#define DL_LAYER8      (13)
#define DL_LAYER9      (14)
#define DL_LAYER10     (15)
#define DL_LAYER11     (16)
#define DL_LAYER12     (17)
#define DL_LAYER13     (18)
#define DL_LAYER14     (19)
#define DL_LAYER15     (20)
#define DL_LAYER16     (21)
#define DL_LAYER17     (22)
#define DL_LAYER18     (23)
#define DL_LAYER19     (24)
#define DL_LAYER20     (25)
#define DL_LAYER21     (26)
#define DL_LAYER22     (27)
#define DL_LAYER23     (28)
#define DL_LAYER24     (29)
#define DL_LAYER25     (30)

#define DL_FUNC        (32)


#define DL_FULL        31
#define DL_DEFAULT     2

typedef struct {
    FILE *dbgstream;          // Output file stream
    char dbgname[PATH_MAX];   // Log file name
    int sysgbd;               // Debug level: bigger number mean more debug
    int bytecount;            // Current log file size
    int filesize;             // Maximum log file size
    int sync;                 // Flush data after every file write
    int tstamp;               // Use time stamp
} dbgmain_t;

extern dbgmain_t main_debug_struct;

#define __assert(x);

#undef mmsdbg_define_variable

#define mmsdbg_define_variable(_variable_,                                      \
          _default_value_, _group_, _key_, _comment_);

#define TIMEVAL_STRUCT(x)                     osal_timeval (x);
#define TIMEVAL_READ(x)                       gettimeofday(&x, 0);
#define TIMEVAL_PRINT(x) \
            (unsigned int)__tv.tv_sec, (unsigned int)__tv.tv_usec

#define TIME_FORMAT_PRINT() {                                                   \
        int hh, mm, ss, ms;                                                    \
        osal_timeval tv;                                                     \
        if (main_debug_struct.tstamp) {                                         \
            TIMEVAL_READ(tv);                                                  \
            hh = tv.tv_sec/3600%24;                                            \
            mm = (tv.tv_sec%3600)/60;                                          \
            ss = (tv.tv_sec%3600)%60;                                          \
            ms = tv.tv_usec/1000;                                              \
            main_debug_struct.bytecount += fprintf(main_debug_struct.dbgstream,\
                            "[%02d:%02d:%02d.%03d]",                            \
                            hh, mm,                                            \
                            ss, ms);                                           \
        }                                                                      \
    }

#define TIMEVAL_SUB_PRINT(x,y)                                                  \
    ((unsigned int)(((x)->tv_sec-(y)->tv_sec) * 1000000 +                       \
    ((x)->tv_usec-(y)->tv_usec)))

#define MMSGETTHREADID()                      pthread_self()

#undef mmsdbgis
#define mmsdbgis(level) ((level < main_debug_struct.sysgbd) ? 1:0)

#define debug_rotate() {                                                        \
        rewind (main_debug_struct.dbgstream);                                   \
    }

#define debug_size_check(main_debug_struct) {                                   \
        if (main_debug_struct.filesize <= main_debug_struct.bytecount) {        \
            main_debug_struct.bytecount = 0;                                    \
            debug_rotate ();                                                    \
        }                                                                       \
    }

#define dbghead (int)pthread_self(), __FILE__, __LINE__, __FUNCTION__

#define dbg_fflush() {                                                          \
        if (main_debug_struct.sync)                                             \
            fflush(main_debug_struct.dbgstream);                                \
    }

#undef mmsdbg
#define mmsdbg(level, format, arg...) {                                         \
        if (mmsdbgis(level) && main_debug_struct.dbgstream) {                   \
            TIME_FORMAT_PRINT();                                                \
            main_debug_struct.bytecount +=                                      \
            fprintf(main_debug_struct.dbgstream, "%-32s: " format "\n",\
                               __FUNCTION__, ##arg);     \
            dbg_fflush();                                                       \
            debug_size_check(main_debug_struct);                                \
        }                                                                       \
    }

#undef mmssys
#define mmssys(level, format, arg...) {                                         \
        if (mmsdbgis(level) && main_debug_struct.dbgstream) {                   \
            TIME_FORMAT_PRINT();                                                \
            main_debug_struct.bytecount +=                                      \
            fprintf(main_debug_struct.dbgstream, "%-32s: " format " - %s\n",       \
                    __FUNCTION__, ##arg, strerror(errno));                      \
            dbg_fflush();                                                       \
            debug_size_check(main_debug_struct);                                \
        }                                                                       \
    }

#undef mmsinfo
#define mmsinfo(level, format, arg...) {                                        \
        if (mmsdbgis(level) && main_debug_struct.dbgstream) {                   \
            TIME_FORMAT_PRINT();                                                \
            main_debug_struct.bytecount +=                                      \
            fprintf(main_debug_struct.dbgstream, format "\n", ##arg);           \
            dbg_fflush();                                                       \
            debug_size_check(main_debug_struct);                                \
        }                                                                       \
    }

#undef mmsinfot
#define mmsinfot(level, format, arg...) {                                       \
        mmsdbg(level, format, ##arg)                                            \
    }

#undef mmsinfodt
#define mmsinfodt(level, tv, format, arg...) {                                 \
        mmsdbg(level, format, ##arg)                                            \
    }

#undef mmsdbgt
#define mmsdbgt(level, format, arg...) {                                        \
        mmsdbg(level, format, ##arg)                                            \
    }

#undef mmsdbgdt
#define mmsdbgdt(level, tv, format, arg...) {                                  \
		mmsdbg(level, format, ##arg)                                            \
    }

#undef mmsdbgadt
#define mmsdbgadt(level, tv, format, arg...) {                                 \
		mmsdbg(level, format, ##arg)                                            \
    }

#undef mmsdbgtrace
#define mmsdbgtrace(format, arg...) {                                           \
        if (main_debug_struct.dbgstream) {                                      \
            TIME_FORMAT_PRINT();                                                \
            main_debug_struct.bytecount +=                                      \
             fprintf(main_debug_struct.dbgstream, "%s:%d:%-32s: "format"\n",  \
                __FUNCTION__, __LINE__,                                         \
                strerror(errno), ##arg);                                        \
            dbg_fflush();                                                       \
            debug_size_check(main_debug_struct);                                \
        }                                                                       \
    }

#undef  mmsdump
#define mmsdump(addr,buf) {                                                     \
        if (main_debug_struct.dbgstream) {                                      \
            TIME_FORMAT_PRINT();                                                \
            main_debug_struct.bytecount +=                                      \
                 fprintf(main_debug_struct.dbgstream,                           \
                        "%08X: "mmsdbg_DUMPOCTET" - "mmsdbg_DUMPOCTET"\n",      \
                        (int)addr,                                              \
                        ((char*)(buf))[ 0], ((char*)(buf))[ 1],                 \
                        ((char*)(buf))[ 2], ((char*)(buf))[ 3],                 \
                        ((char*)(buf))[ 4], ((char*)(buf))[ 5],                 \
                        ((char*)(buf))[ 6], ((char*)(buf))[ 7],                 \
                        ((char*)(buf))[ 8], ((char*)(buf))[ 9],                 \
                        ((char*)(buf))[10], ((char*)(buf))[11],                 \
                        ((char*)(buf))[12], ((char*)(buf))[13],                 \
                        ((char*)(buf))[14], ((char*)(buf))[15]);                \
            dbg_fflush();                                                       \
            debug_size_check(main_debug_struct);                                \
        }                                                                       \
    }

#undef mmsdbgdump
#define mmsdbgdump(level, str, addr, count) {                                   \
        if (mmsdbgis(level) && main_debug_struct.dbgstream) {                   \
            int __cnt;                                                          \
            char *__dumpdata = (char*)addr;                                     \
            TIME_FORMAT_PRINT();                                                \
            for (__cnt=0; __cnt<(signed)(count); __cnt++) {                     \
                if ((__cnt & 0xF) == 0)                                         \
                    main_debug_struct.bytecount +=                              \
                           fprintf(main_debug_struct.dbgstream, "%s:%08X:",     \
                           str, (int)__dumpdata);                               \
                main_debug_struct.bytecount +=                                  \
                       fprintf(main_debug_struct.dbgstream,                     \
                       " %02X", *__dumpdata++);                                 \
                if ((__cnt & 0xF) == 0xF)                                       \
                    main_debug_struct.bytecount +=                              \
                           fprintf(main_debug_struct.dbgstream, "\n");          \
            }                                                                   \
            if ((__cnt & 0xF) != 0x00)                                          \
                fprintf(main_debug_struct.dbgstream, "\n");                     \
            dbg_fflush();                                                       \
            debug_size_check(main_debug_struct);                                \
        }                                                                       \
    }
#endif

#endif // __MMS_DEBUG_V2_H__
