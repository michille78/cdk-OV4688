/*******************************************************************************
 *                                                                             *
 *  File:           mmslib-debug.h                                             *
 *                                                                             *
 *  Author:         Dinko Pavlinov Mironov                                     *
 *                                                                             *
 *  Project:        Debug functions                                            *
 *                                                                             *
 *  Description:    Debug definitions                                          *
 *                                                                             *
 *  Modification History:                                                      *
 *  --------------------                                                       *
 *                                                                             *
 *  Date        Name           Description                                     *
 *  ----        ----           -----------                                     *
 *  2006-06-13  Dinko Mironov  Initial create                                  *
 *  2006-09-24  Dinko Mironov  Add delta time message                          *
 *  2008-07-21  Dinko Mironov  Add support for Windows compiling               *
 *  2008-08-12  Ivan Evlogiev  Port to Symbian with sutb functions             *
 *  2008-09-01  Ivo Stoyanov  Edit Symbian debug macros                           *
 *  2008-11-26  Ivan Petrov    Port to windows                                 *
 *                                                                             *
 ******************************************************************************/

#ifndef __MMS_LIBRARY_DEBUG_WINDOWS_H__
#define __MMS_LIBRARY_DEBUG_WINDOWS_H__


#include <osal/osal_sysdep.h>
#include <stdio.h>
#include <process.h>



#define mmsdbg_define_output          FILE *stddbg
#define mmsdbg_config_output(std)     stddbg = std

#ifndef __MMS_DEBUG__

#ifdef ___GNUC___

#define FLUSHPRINT(format, arg...)            do { } while(0)

#define mmsdbg_lup(var, level)                do { } while(0)
#define mmsdbg_ldn(var, level)                do { } while(0)
#define mmsdbg(level, format, arg...)         do { } while(0)
#define mmssys(level, format, arg...)         do { } while(0)
#define mmsinfo(level, format, arg...)        do { } while(0)
#define mmsinfot(level, format, arg...)       do { } while(0)
#define mmsinfodt(level, tv1, format, arg...) do { } while(0)
#define mmsdbgt(level, format, arg...)        do { } while(0)
#define mmsdbgdt(level, tv1, format, arg...)  do { } while(0)
#define mmsdbgadt(level, tv1, format, arg...) do { } while(0)
#define mmsdump(addr,buf)                     do { } while(0)
#define mmsdbgdump(level,str,addr,count)      do { } while(0)
#define mmsdbgis(level)                       (0)

#else
#define FLUSHPRINT(format, arg1, arg2...) { }
#define mmsdbg(level, arg1, arg2...) { }
#define mmssys(level,  arg1, arg2...) { }
#define mmsinfo(level,  arg1, arg2...) { }
#define mmsinfot(level,  arg1, arg2...) { }
#define mmsinfodt(level,  arg1, arg2...) { }
#define mmsdbgt(level,  arg1, arg2...) { }
#define mmsdbgdt(level,  arg1, arg2...) { }
#define mmsdbgadt(level,  arg1, arg2...) { }
#define mmsdump(addr, buf) { }
#define mmsdbgdump(level, str, addr, count) { }
#endif /* ___GNUC___ */

#else /* __MMS_DEBUG__ */

#undef mmsdbg_define_variable

#define MMSDBG_MAGIC_CODE   "MMSDBGVariable"

#define mmsdbg_define_variable(_variable_, _default_value_, _group_, _key_, _comment_) \
  static int _variable_ = _default_value_; \
  struct mmsdbg_dynamic_variable_register mmsdbg_reg_##_variable_ = { \
    {MMSDBG_MAGIC_CODE}, \
    _group_, \
    &_variable_, \
    _key_, \
    _comment_, \
  };
#define MMSGETTHREADID()                      _getpid()

// WINDOWS
// IIvanov #define FLUSHPRINT(format, arg...)     printf(format, __VA_ARGS__);

#define mmsdbg(level, format, ...)   {if ((level) & MMSDEBUGLEVEL) {\
        printf("%4d:%s: " format "\n", (int)MMSGETTHREADID(), __FUNCTION__, ##__VA_ARGS__);}}

#define mmssys(level, format, ...) { \
    if ((level) & MMSDEBUGLEVEL) { \
      printf("%s: " format " - %s\n", __FUNCTION__, ##__VA_ARGS__, strerror(errno)); fflush(stddbg); \
    } \
  }

#define mmsinfo(level, format, ...) {\
    if ((level) & MMSDEBUGLEVEL) { \
      printf( format "\n", ##__VA_ARGS__); \
    } \
  }

#define mmsinfot(level, format, ...) {\
    if ((level) & MMSDEBUGLEVEL) { \
      osal_timeval __tv; \
      gettimeofday(&__tv, 0); \
      printf( "%010u:%06u: " format "\n", \
       (unsigned int)__tv.tv_sec, (unsigned int)__tv.tv_usec, ##__VA_ARGS__); \
    } \
  }

#define mmsinfodt(level, tv1, format, ...) {\
    if ((level) & MMSDEBUGLEVEL) { \
      osal_timeval __tv; \
      gettimeofday(&__tv, 0); \
      printf( "%7d: " format "\n", \
       (unsigned int)((__tv.tv_sec-(tv1)->tv_sec)*1000000+(__tv.tv_usec-(tv1)->tv_usec)), ##__VA_ARGS__); \
      memcpy(tv1, &__tv, sizeof(__tv)); \
    } \
  }

#define mmsdbgt(level, format, ...) {\
    if ((level) & MMSDEBUGLEVEL) { \
      osal_timeval __tv; \
      gettimeofday(&__tv, 0); \
      printf( "%4d:%010u:%06u:%s: " format "\n", (int)MMSGETTHREADID(), \
       (unsigned int)__tv.tv_sec, (unsigned int)__tv.tv_usec, __FUNCTION__, ##__VA_ARGS__); \
    } \
  }

#define mmsdbgdt(level, tv1, format, ...) {\
    if ((level) & MMSDEBUGLEVEL) { \
      osal_timeval __tv; \
      gettimeofday(&__tv, 0); \
      printf( "%4d:%7d:%s: " format "\n", (int)MMSGETTHREADID(), \
       (unsigned int)((__tv.tv_sec-(tv1)->tv_sec)*1000000+(__tv.tv_usec-(tv1)->tv_usec)), __FUNCTION__, ##__VA_ARGS__); \
      memcpy(tv1, &__tv, sizeof(__tv)); \
    } \
  }

#define mmsdbgadt(level, tv1, format, ...) {\
    if ((level) & MMSDEBUGLEVEL) { \
      osal_timeval __tv; \
      gettimeofday(&__tv, 0); \
      printf( "%4d:%7d:%s: " format "\n", (int)MMSGETTHREADID(), \
       (unsigned int)((__tv.tv_sec-(tv1)->tv_sec)*1000000+(__tv.tv_usec-(tv1)->tv_usec)), __FUNCTION__, ##__VA_ARGS__); \
    } \
  }

#define mmsdump(addr,buf) { \
    printf( "%08X: "mmsdbg_DUMPOCTET" - "mmsdbg_DUMPOCTET"\n", (int)addr, \
      ((char*)(buf))[ 0], ((char*)(buf))[ 1], ((char*)(buf))[ 2], ((char*)(buf))[ 3],  \
      ((char*)(buf))[ 4], ((char*)(buf))[ 5], ((char*)(buf))[ 6], ((char*)(buf))[ 7],  \
      ((char*)(buf))[ 8], ((char*)(buf))[ 9], ((char*)(buf))[10], ((char*)(buf))[11],  \
      ((char*)(buf))[12], ((char*)(buf))[13], ((char*)(buf))[14], ((char*)(buf))[15]); \
  }

#define mmsdbgdump(level,str,addr,count) {\
    if ((level) & MMSDEBUGLEVEL) { \
      int __cnt; \
      char *__dumpdata = (char*)addr; \
      for (__cnt=0;__cnt<(count);__cnt++) { \
        if ((__cnt & 0xF)==0) printf( "%s:%08X:", str, (int)__dumpdata); \
        printf( " %02X", *__dumpdata++); \
        if ((__cnt & 0xF)==0xF) printf( "\n"); \
      } \
      if ((__cnt & 0xF)!=0x00) printf( "\n"); \
    } \
  }



#endif // __MMS_DEBUG__


//Common definitions
#define mmsdbg_PRINTLLC(x,n)            (((unsigned char*)(x))[n])
#define mmsdbg_DUMPOCTET                "%02X %02X %02X %02X %02X %02X %02X %02X"
#define mmsdbg_DUMPLL                   "%016llX"
#define mmsdbg_PRINTL(x)                (*((unsigned long long *)(x)))

#endif // __MMS_LIBRARY_DEBUG_WINDOWS_H__

