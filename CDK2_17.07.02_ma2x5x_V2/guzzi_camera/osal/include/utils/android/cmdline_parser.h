/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file cmdline_parser.h
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#ifndef __MMS_EXAMP_CMD_LINE_TOOL_H__
#define __MMS_EXAMP_CMD_LINE_TOOL_H__

#include <stdlib.h>
#include <getopt.h>
#include <stdio.h>

extern int vdl_exlib_cmdlinetool;

typedef int cmdline_handle(int val, char *opt, char *arg, int index);
typedef int cmdline_print_help(char optNameShort, char *optNameLong);

#define CMDLINE_NOERROR              (0)
#define CMDLINE_ERROR_COMMON         (-1)
#define CMDLINE_ERROR_MISSED         (-2)
#define CMDLINE_ERROR_UNKNOW         (-3)
#define CMDLINE_ERROR_INVALID_ARG    (-4)
#define CMDLINE_ERROR_PRINT_HELP     (-5)

#define CMDLINE_FLAG_OPT_MANDATORY   (1<<0)
#define CMDLINE_FLAG_OPT_OPTIONAL    (1<<1)
#define CMDLINE_FLAG_VAL_MANDATORY   (1<<2)
#define CMDLINE_FLAG_VAL_OPTIONAL    (1<<3)
#define CMDLINE_FLAG_HELP_HIGH       (1<<4)
#define CMDLINE_FLAG_HELP_LEVEL0     (1<<5)
#define CMDLINE_FLAG_HELP_LEVEL1     (1<<6)
#define CMDLINE_FLAG_HELP_LEVEL2     (1<<7)
#define CMDLINE_FLAG_HELP_LEVEL3     (1<<8)
#define CMDLINE_FLAG_HELP_LEVEL4     (1<<9)
#define CMDLINE_FLAG_HELP_LEVEL5     (1<<10)
#define CMDLINE_FLAG_HELP_LEVEL6     (1<<11)
#define CMDLINE_FLAG_HELP_LEVEL7     (1<<12)
#define CMDLINE_FLAG_HELP_LEVEL8     (1<<13)
#define CMDLINE_FLAG_HELP_LEVEL9     (1<<14)
#define CMDLINE_FLAG_HELP_MASK       (CMDLINE_FLAG_HELP_HIGH|CMDLINE_FLAG_HELP_LEVEL0| \
                                     CMDLINE_FLAG_HELP_LEVEL1|CMDLINE_FLAG_HELP_LEVEL2| \
                                     CMDLINE_FLAG_HELP_LEVEL3|CMDLINE_FLAG_HELP_LEVEL4| \
                                     CMDLINE_FLAG_HELP_LEVEL5|CMDLINE_FLAG_HELP_LEVEL6| \
                                     CMDLINE_FLAG_HELP_LEVEL7|CMDLINE_FLAG_HELP_LEVEL8| \
                                     CMDLINE_FLAG_HELP_LEVEL9)

//Comand line parser functions
int cmdline_reset(void);


int cmdline_set_missed(cmdline_handle *handle);

int cmdline_set_unknow(cmdline_handle *handle);

int cmdline_set_liberty(int flag, char *valName, cmdline_handle *handle,
      char *help, cmdline_print_help *printHelp);

int cmdline_add_option(int flag, char optNameShort, char *optNameLong,
      char *valName, int val, int *valReturn, cmdline_handle *handle,
      char *help, cmdline_print_help *printHelp);

int cmdline_parser(int argc, char **argv);

int cmdline_parser_string(char *str);

int cmdline_parser_env(char *envName);


//Usage helper functions
int cmdline_help_line(int flag, char *help);

int cmdline_help_print(int flag);

int cmdline_help_usage_print(char *filename);

//Terminal control functions
int cmdline_terminal_save_mode(void);

int cmdline_terminal_restore_mode(void);

int cmdline_terminal_set_raw_mode(void);

#endif // __MMS_EXAMP_CMD_LINE_TOOL_H__

