/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file mms_debug.h
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#ifndef __MMS_LIBRARY_DEBUG_LINUX_H__
#define __MMS_LIBRARY_DEBUG_LINUX_H__

#define mmsdbg_define_name(name)
#define mmsdbg_define_output
#define mmsdbg_config_output(std)

#define TIMEVAL_STRUCT(x)                     { }
#define TIMEVAL_READ(x)                       { }

#define FLUSHPRINT(format, arg...)            { }

#define mmsdbg(level, format, arg...)         { }
#define mmssys(level, format, arg...)         { }
#define mmsinfo(level, format, arg...)        { }
#define mmsinfot(level, format, arg...)       { }
#define mmsinfodt(level, tv1, format, arg...) { }
#define mmsdbgt(level, format, arg...)        { }
#define mmsdbgdt(level, tv1, format, arg...)  { }
#define mmsdbgadt(level, tv1, format, arg...) { }
#define mmsdbgtrace(format, arg...)           { }
#define mmsdump(addr,buf)                     { }
#define mmsdbgdump(level,str,addr,count)      { }

#endif // __MMS_LIBRARY_DEBUG_H__
