/*!
 *****************************************************************************
 * \file
 *    framework/fw3a/utils/dspbios/mms_debug.h
 *
 * \brief
 *  TODO: Add description
 *
 * \version 1.0
 *
 *****************************************************************************
 */
#ifndef __MMS_LIBRARY_DEBUG_DSPBIOS_H__
#define __MMS_LIBRARY_DEBUG_DSPBIOS_H__

#include <stdlib.h>
#include <stdio.h>
    /*
    #define mmsdbg_define_output          FILE *stddbg
    #define mmsdbg_config_output(std)     stddbg = std
    extern FILE                           *stddbg;
    */
    extern void   *mmsdbgvarlist;

#undef mmsdbg_define_variable
#define mmsdbg_define_variable(_variable_, _default_value_, _group_, _key_, _comment_) \
    static unsigned long _variable_ = _default_value_; \
    struct mmsdbg_dynamic_variable_register \
        mmsdbg_reg_##_variable_ __attribute__((section("mmsdbgvarlist"))) = { \
        .magic = { MMSDBG_MAGIC_CODE }, \
        .group = _group_, \
        .var   = &_variable_, \
        .key   = _key_, \
        .comment = _comment_, \
    };
#define __MMS_DEBUG__

#ifndef __MMS_DEBUG__

    #define FLUSHPRINT(format, arg ...)            { }

    #define mmsdbg_lup(var, level)                { }
    #define mmsdbg_ldn(var, level)                { }
    #define mmsdbg(level, format, arg ...)         { }
    #define mmssys(level, format, arg ...)         { }
    #define mmsinfo(level, format, arg ...)        { }
    #define mmsinfot(level, format, arg ...)       { }
    #define mmsinfodt(level, tv1, format, arg ...) { }
    #define mmsdbgt(level, format, arg ...)        { }
    #define mmsdbgdt(level, tv1, format, arg ...)  { }
    #define mmsdbgadt(level, tv1, format, arg ...) { }

    #define mmsdump(addr, buf)                     { }
    #define mmsdbgdump(level, str, addr, count)      { }

#else
    #include <osal/osal_stdtypes.h>
    #include <osal/osal_thread.h>

    #define MMSGETTHREADID()     osal_thread_Self()


    #define FLUSHPRINT(format, ...)    TIMM_OSAL_InfoExt(TIMM_OSAL_TRACEGRP_OMXCAM,               \
                                                         format, ##__VA_ARGS__);


    #define mmsdbg(level, format, ...)   { if ((level) & MMSDEBUGLEVEL ) {                \
                                               if ( !(DL_DEFAULT & (level))) {                             \
                                                   TIMM_OSAL_InfoExt(TIMM_OSAL_TRACEGRP_OMXCAM, "%s: " format,    \
                                                                     __FUNCTION__, ##__VA_ARGS__);\
                                               } else {                                                                    \
                                                   TIMM_OSAL_ErrorExt(TIMM_OSAL_TRACEGRP_OMXCAM, "%s: " format,   \
                                                                      __FUNCTION__, ##__VA_ARGS__);\
                                               }                                                                           \
                                           }                                                                               \
}

    #define mmssys(level, format, ...) { \
        if ((level) & MMSDEBUGLEVEL ) { \
            TIMM_OSAL_InfoExt(TIMM_OSAL_TRACEGRP_OMXCAM, "%s: " format " - %s", __FUNCTION__,     \
                              ##__VA_ARGS__, "Unknown"); } }

    #define mmsinfo(level, format, ...) {\
        if ((level) & MMSDEBUGLEVEL ) { \
            TIMM_OSAL_InfoExt(TIMM_OSAL_TRACEGRP_OMXCAM,  format, ##__VA_ARGS__); \
        } \
}

    #define mmsinfot(level, format, ...) {  \
        if ((level) & MMSDEBUGLEVEL ) {      \
            unsigned long    time;               \
            osal_get_time(&time);             \
            TIMM_OSAL_InfoExt(TIMM_OSAL_TRACEGRP_OMXCAM,  "%010u: " format,    \
                              time, ##__VA_ARGS__);   \
        }                                   \
}

    #define mmsinfodt(level, tv1, format, ...) {\
        if ((level) & MMSDEBUGLEVEL ) {          \
            unsigned long    time;                   \
            osal_get_time(&time);                 \
            TIMM_OSAL_InfoExt(TIMM_OSAL_TRACEGRP_OMXCAM,  "%7d: " format,          \
                              time, ##__VA_ARGS__);                \
            tv1 = time;                           \
        }                                       \
}

    #define mmsdbgt(level, format, ...) {       \
        if ((level) & MMSDEBUGLEVEL ) {          \
            unsigned long    time;                   \
            osal_get_time(&time);                 \
            TIMM_OSAL_InfoExt(TIMM_OSAL_TRACEGRP_OMXCAM,  "%010u:%s: " format, \
                              time, __FUNCTION__, ##__VA_ARGS__);  \
        } \
}

    #define mmsdbgdt(level, tv1, format, ...) { \
        if ((level) & MMSDEBUGLEVEL ) {          \
            unsigned long    time;                   \
            osal_get_time(&time);                 \
            TIMM_OSAL_InfoExt(TIMM_OSAL_TRACEGRP_OMXCAM,  "%10d:%s: " format, \
                              time, __FUNCTION__, ##__VA_ARGS__);  \
            tv1 = time;                         \
        }                                       \
}

    #define mmsdbgadt(level, tv1, format, ...) {\
        if ((level) & MMSDEBUGLEVEL ) {          \
            unsigned long    time;                   \
            osal_get_time(&time);                 \
            TIMM_OSAL_InfoExt(TIMM_OSAL_TRACEGRP_OMXCAM,  "%7d:%s: " format, \
                              time, __FUNCTION__, ##__VA_ARGS__); \
        } \
}

    #define mmsdump(addr, buf) { \
        TIMM_OSAL_InfoExt(TIMM_OSAL_TRACEGRP_OMXCAM,  "%08X: " mmsdbg_DUMPOCTET " - " mmsdbg_DUMPOCTET, (int)addr, \
                          ((char *)(buf))[ 0], ((char *)(buf))[ 1], ((char *)(buf))[ 2], ((char *)(buf))[ 3],  \
                          ((char *)(buf))[ 4], ((char *)(buf))[ 5], ((char *)(buf))[ 6], ((char *)(buf))[ 7],  \
                          ((char *)(buf))[ 8], ((char *)(buf))[ 9], ((char *)(buf))[10], ((char *)(buf))[11],  \
                          ((char *)(buf))[12], ((char *)(buf))[13], ((char *)(buf))[14], ((char *)(buf))[15]); \
}

    #define mmsdbgdump(level, str, addr, count) {\
        if ((level) & MMSDEBUGLEVEL ) { \
            int    __cnt; \
            for( __cnt=0; __cnt < (count); __cnt++ ) { \
                char   *ptr = addr;  \
                if ((__cnt & 0xF) == 0 ) { TIMM_OSAL_InfoExt(TIMM_OSAL_TRACEGRP_OMXCAM,  "%s:%08X:", str, (int)addr); } \
                TIMM_OSAL_InfoExt(TIMM_OSAL_TRACEGRP_OMXCAM,  " %02X", *(ptr)++); \
                if ((__cnt & 0xF) == 0xF ) { TIMM_OSAL_InfoExt(TIMM_OSAL_TRACEGRP_OMXCAM,  "\n"); } \
            } \
            if ((__cnt & 0xF) != 0x00 ) { TIMM_OSAL_InfoExt(TIMM_OSAL_TRACEGRP_OMXCAM,  "\n"); } \
        } \
}


#endif // __MMS_DEBUG__

//Common definitions
#define mmsdbg_PRINTLLC(x, n)            (((unsigned char *)(x))[n])
#define mmsdbg_DUMPOCTET                "%02X %02X %02X %02X %02X %02X %02X %02X"
#define mmsdbg_DUMPLL                   "%016llX"
#define mmsdbg_PRINTL(x)                (*((unsigned long long *)(x)))

#endif // __MMS_LIBRARY_DEBUG_DSPBIOS_H__

