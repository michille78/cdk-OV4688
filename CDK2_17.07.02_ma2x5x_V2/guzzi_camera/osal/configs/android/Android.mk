# Copyright (C) 2011 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

LOCAL_PATH := $(abspath $(call my-dir)/../../)

include $(CLEAR_VARS)

CONFIG_OSAL_MSG := FIFO

#LOCAL_ALLOW_UNDEFINED_SYMBOLS := true

LOCAL_CFLAGS += \
    -fno-short-enums \

LOCAL_CFLAGS += \
    -Wno-unused-parameter \
    -Wno-sign-compare \
    -Wno-pointer-arith \
    -Wformat=0 \

LOCAL_CFLAGS += \
    -DQEMU_HARDWARE \
    -D___ANDROID___ \
	-D__MMS_DEBUG__ \

LOCAL_CFLAGS += -DMAILBOX_PATH='"/data/osal_"'

LOCAL_SHARED_LIBRARIES += \
    liblog \

LOCAL_C_INCLUDES += \
    $(LOCAL_PATH)/include \

LOCAL_SRC_FILES += \
    modules/osal/android/osal_main.c \
    modules/osal/android/osal_mutex.c \
    modules/osal/android/osal_fcntl.c \
    modules/osal/android/osal_process.c \
    modules/osal/android/osal_malloc.c \
    modules/osal/android/osal_thread.c \
    modules/osal/android/osal_time.c \
    modules/osal/common/pool.c \
    modules/osal/common/ex_pool.c \
    modules/osal/common/list_pool.c \
    modules/osal/common/pool_unsafe.c \
    modules/utils/android/cmdline_tool/cmdline_debug.c \
    modules/utils/android/cmdline_tool/cmdline_parser.c \

ifeq ($(CONFIG_OSAL_MSG),FIFO)
LOCAL_SRC_FILES += modules/osal/android/osal_mb_fifo.c
endif
ifeq ($(CONFIG_OSAL_MSG),MQUEUE)
LOCAL_SRC_FILES += modules/osal/android/osal_mb_mqueue.c
endif

LOCAL_MODULE := libmms_osal_v1

CONFIG_OSAL_MSG :=

LOCAL_EXPORT_CFLAGS := \
    -D___ANDROID___ \
    -D__MMS_DEBUG__ \

include $(BUILD_SHARED_LIBRARY)

