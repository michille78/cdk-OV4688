#include <osal/osal_sysdep.h>
#include <osal/osal_stdtypes.h>
#include <osal/osal_stdio.h>
#include <osal/osal_stdlib.h>
#include <osal/osal_string.h>
#include <osal/osal_mailbox.h>

#include <utils/mms_debug.h>
#include <utils/rtems/signal_handler.h>

mmsdbg_define_variable(vdl_osal_mb, DL_DEFAULT|DL_MESSAGE, 0,
    "osal_mb", "Test OSAL Mailbox");
#define MMSDEBUGLEVEL vdl_osal_mb

#define MB_NAME         "MAILBOX"
#define TEST_STR        "MAGIC"

char payload[16];

static int msg_snd(struct osal_mailbox *mbox,
    union osal_mbox_cmd *cmd, void *payload, int size)
{
    int err;

    err = osal_mailbox_write(mbox, cmd, payload, size);
    if (err < 0) {
        mmsdbg(DL_WARNING, "Can't send mailbox: %d (size = %d)", err, size);
        if (payload) {
            mmsdbgdump(DL_WARNING, "payload", payload, size);
        }
    } else {
        mmsinfo(DL_MESSAGE, "Snd msg: cmd: %3d; status: %2d; "
            "ack: %d; size: %3d; result: %3d - %s",
            cmd->id, cmd->status, cmd->ack, size, err, (char*)payload);
    }

    return err;
}

static int msg_rcv(struct osal_mailbox *mbox,
    union osal_mbox_cmd *cmd, void *payload, int size)
{
    union osal_mbox_cmd msg;
    int err;

    err = osal_mailbox_read(mbox, &msg, payload, size);
    if (err < 0) {
        mmsdbg(DL_WARNING, "Can't receive message: %d (size = %d)", err, size);
        if (payload) {
            mmsdbgdump(DL_WARNING, "payload", payload, size);
        }
    } else {
        mmsinfo(DL_MESSAGE, "Rcv msg: cmd: %3d; status: %2d; "
            "ack: %d; size: %3d; result: %3d - %s",
            msg.id, msg.status, msg.ack, size, err, (char*)payload);

        if (cmd)
            *cmd = msg;
    }

    return err;

}

int main(void)
{
    struct osal_mailbox *mbox_wr;
    struct osal_mailbox *mbox_rd;
    union osal_mbox_cmd cwr, crd;
    int err;

    mmsdbg_config_output(stdout);

    signal_handler_init();

    osal_memset(payload, 0, sizeof(payload));

    mbox_wr = osal_mailbox_create(MB_NAME, 0);
    if (!mbox_wr) {
        mmsdbg(DL_WARNING, "Can't create mailbox wr: name=%s", MB_NAME);
        return -1;
    }
    mmsinfo(DL_MESSAGE, "Create MB Read: %p", mbox_wr);

    mbox_rd = osal_mailbox_create(MB_NAME, 0);
    if (!mbox_rd) {
        mmsdbg(DL_WARNING, "Can't create mailbox wr: name=%s", MB_NAME);
        return -1;
    }
    mmsinfo(DL_MESSAGE, "Create MB Write: %p", mbox_rd);

    cwr.id = 0x100; cwr.status = 0x10; cwr.ack = 0;
    msg_snd(mbox_wr, &cwr, TEST_STR, sizeof(TEST_STR));
    msg_rcv(mbox_rd, &crd, payload, sizeof(payload));

    cwr.id = 0x101; cwr.status = 0x11; cwr.ack = 0;
    msg_snd(mbox_wr, &cwr, TEST_STR, sizeof(TEST_STR));
    msg_rcv(mbox_rd, &crd, NULL, 0);

    mmsinfo(DL_MESSAGE, "");
    err = system("ls -l /tmp/osal_mbox_*");
    mmsinfo(DL_MESSAGE, "");

    err = osal_mailbox_destroy(mbox_rd);
    mmsinfo(DL_MESSAGE, "Destroy MB Read: err=%d", err);

    mmsinfo(DL_MESSAGE, "");
    err = system("ls -l /tmp/osal_mbox_*");
    mmsinfo(DL_MESSAGE, "");

    msg_snd(mbox_wr, &cwr, TEST_STR, sizeof(TEST_STR));

    err = osal_mailbox_destroy(mbox_wr);
    mmsinfo(DL_MESSAGE, "Destroy MB Write: err=%d", err);

    return 0;
}

