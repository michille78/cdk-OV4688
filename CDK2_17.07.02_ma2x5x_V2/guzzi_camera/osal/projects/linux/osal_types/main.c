#include <osal/osal_stdtypes.h>
#include <osal/osal_stdio.h>

#include <utils/mms_debug.h>

mmsdbg_define_variable(vdl_osal_types, DL_DEFAULT|DL_MESSAGE, 0,
    "osal_types", "Test OSAL Types");
#define MMSDEBUGLEVEL vdl_osal_tpyes

static void check_types(int rsize, int dsize, char *name)
{
    dsize /= 8;

    osal_printf("Size of %-6s is %d bytes (should be %d bytes) \t%s\n",
        name, rsize, dsize, (rsize == dsize) ? "Passed" : "Fail");
}

int main(void)
{
    mmsdbg_config_output(stdout);

    check_types(sizeof(uint8), 8, "uint8");
    check_types(sizeof(uint16), 16, "uint16");
    check_types(sizeof(uint32), 32, "uint32");
    check_types(sizeof(uint64), 64, "uint64");
    check_types(sizeof(u8), 8, "u8");
    check_types(sizeof(u16), 16, "u16");
    check_types(sizeof(u32), 32, "u32");
    check_types(sizeof(u64), 64, "u64");
    check_types(sizeof(Bool), 32, "Bool");
    check_types(sizeof(int8), 8, "int8");
    check_types(sizeof(int16), 16, "int16");
    check_types(sizeof(int32), 32, "int32");
    check_types(sizeof(int64), 64, "int64");
    check_types(sizeof(s8), 8, "s8");
    check_types(sizeof(s16), 16, "s16");
    check_types(sizeof(s32), 32, "s32");
    check_types(sizeof(s64), 64, "s64");
    check_types(sizeof(uint), sizeof(int) * 8, "uint");

    return 0;
}

