/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file profile.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <osal/osal_stdlib.h>
#include <osal/osal_stdtypes.h>
#include <osal/osal_mutex.h>
#include <osal/osal_time.h>
#include <utils/mms_debug.h>
#include <osal/pool_unsafe.h>

typedef struct {
    profile_id_t id;
    profile_time_t time;
    profile_val_t v1;
    profile_val_t v2;
} log_entry_t;

typedef struct {
    int pos;
    int size;
    log_entry_t *entries;
} log_t;

struct profile {
    profile_ready_cb_t *ready_cb;
    void *prv;
    osal_mutex *lock;
    pool_unsafe_t *pool;
    unsigned int entries_per_buffer;
    unsigned int buffers_count;
    log_t log;
    profile_val_t skipped;
    profile_time_t last_time_skipped;
};

profile_t *profile;

mmsdbg_define_variable(
        vdl_profile,
        DL_DEFAULT,
        0,
        "vdl_profile",
        "Profiler."
    );
#define MMSDEBUGLEVEL mmsdbg_use_variable(vdl_profile)

/*
 * ****************************************************************************
 * ** Lock/Unlock *************************************************************
 * ****************************************************************************
 */
static void profile_unlock(profile_t *profile)
{
    osal_mutex_unlock(profile->lock);
}

static void profile_lock(profile_t *profile)
{
    osal_mutex_lock(profile->lock);
}

/*
 * ****************************************************************************
 * ** Dummy callback **********************************************************
 * ****************************************************************************
 */
static void dummy_callback(
        profile_t *profile,
        void *prv,
        void *buffer,
        unsigned int buffer_size
    )
{
    profile_release_ready(profile, buffer);
}

/*
 * ****************************************************************************
 * ** Release ready ************************************************************
 * ****************************************************************************
 */
void profile_release_ready(profile_t *profile, void *buffer)
{
    profile_lock(profile);
    osal_free(buffer);
    profile_unlock(profile);
}

/*
 * ****************************************************************************
 * ** Dump ********************************************************************
 * ****************************************************************************
 */
void profile_dump(profile_t *profile)
{
    log_entry_t *entries;
    unsigned int pos;

    profile_lock(profile);
    pos = profile->log.pos;
    entries = profile->log.entries;
    if (!pos || !entries) {
        profile_unlock(profile);
        return;
    }
    profile->log.entries = pool_unsafe_alloc(profile->pool);
    if (profile->log.entries) {
        profile->log.pos = 0;
        if (profile->skipped) {
            profile->log.entries[profile->log.pos].time = profile->last_time_skipped;
            profile->log.entries[profile->log.pos].id = PROFILE_ID_SKIPPED;
            profile->log.entries[profile->log.pos].v1 = profile->skipped;
            profile->log.entries[profile->log.pos].v2 = 0;
            profile->log.pos++;
            profile->skipped = 0;
        }
    }
    profile_unlock(profile);

    profile->ready_cb(
            profile,
            profile->prv,
            entries,
            pos * sizeof (log_entry_t)
        );
}

/*
 * ****************************************************************************
 * ** Time ********************************************************************
 * ****************************************************************************
 */
profile_time_t profile_get_time(void)
{
    osal_timeval timeval;
    osal_get_time(&timeval);
    return (profile_time_t)timeval;
}

/*
 * ****************************************************************************
 * ** Add *********************************************************************
 * ****************************************************************************
 */
void profile_add_raw(
        profile_t *profile,
        profile_time_t time,
        profile_id_t id,
        profile_val_t v1,
        profile_val_t v2
    )
{
    log_entry_t *entries;

    profile_lock(profile);
    if (profile->log.size <= profile->log.pos) {
        entries = profile->log.entries;
        profile->log.entries = pool_unsafe_alloc(profile->pool);
        if (profile->log.entries) {
            profile->log.pos = 0;
            if (profile->skipped) {
                profile->log.entries[profile->log.pos].time = profile->last_time_skipped;
                profile->log.entries[profile->log.pos].id = PROFILE_ID_SKIPPED;
                profile->log.entries[profile->log.pos].v1 = profile->skipped;
                profile->log.entries[profile->log.pos].v2 = 0;
                profile->log.pos++;
                profile->skipped = 0;
            }
        }
        if (entries) {
            profile_unlock(profile);
            profile->ready_cb(
                    profile,
                    profile->prv,
                    entries,
                    profile->log.size * sizeof (log_entry_t)
                );
            profile_lock(profile);
        }
    }
    if (profile->log.pos < profile->log.size) {
        profile->log.entries[profile->log.pos].time = time;
        profile->log.entries[profile->log.pos].id = id;
        profile->log.entries[profile->log.pos].v1 = v1;
        profile->log.entries[profile->log.pos].v2 = v2;
        profile->log.pos++;
    } else {
        profile->skipped++;
        profile->last_time_skipped = time;
    }
    profile_unlock(profile);
}

/*
 * ****************************************************************************
 * ** Create/Destroy **********************************************************
 * ****************************************************************************
 */
void profile_destroy(profile_t *profile)
{
    if (!profile->log.entries) {
        osal_free(profile->log.entries);
    }
    pool_unsafe_destroy(profile->pool);
    osal_mutex_destroy(profile->lock);
    osal_free(profile);
}

profile_t * profile_create(
        unsigned int entries_per_buffer,
        unsigned int buffers_count,
        profile_ready_cb_t *ready_cb,
        void *prv
    )
{
    profile_t *profile;

    profile = osal_malloc(sizeof (*profile));
    if (!profile) {
        mmsdbg(
                DL_ERROR,
                "Failed to allocate new profile instance: size=%d!",
                sizeof (*profile)
            );
        goto exit1;
    }
    profile->entries_per_buffer = entries_per_buffer;
    profile->buffers_count = buffers_count;
    profile->ready_cb = ready_cb;
    profile->prv = prv;
    if (!profile->ready_cb) {
        profile->ready_cb = dummy_callback;
    }
    profile->skipped = 0;

    profile->lock = osal_mutex_create();
    if (!profile->lock) {
        mmsdbg(DL_ERROR, "Failed to create mutex for profile instance!");
        goto exit2;
    }

    profile->pool = pool_unsafe_create(
            "Profile pool",
            profile->entries_per_buffer * sizeof (log_entry_t),
            profile->buffers_count
        );
    if (!profile->pool) {
        mmsdbg(DL_ERROR, "Failed to create pool for profile instance!");
        goto exit3;
    }

    profile->log.pos = 0;
    profile->log.size = profile->entries_per_buffer;
    profile->log.entries = pool_unsafe_alloc(profile->pool);
    if (!profile->log.entries) {
        mmsdbg(DL_ERROR, "Failed to allocate first profile buffer!");
        goto exit4;
    }

    return profile;
exit4:
    pool_unsafe_destroy(profile->pool);
exit3:
    osal_mutex_destroy(profile->lock);
exit2:
    osal_free(profile);
exit1:
    return NULL;
}

