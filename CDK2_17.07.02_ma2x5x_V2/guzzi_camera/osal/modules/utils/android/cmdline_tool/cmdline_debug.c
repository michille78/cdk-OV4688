/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file cmdline_debug.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#define _GNU_SOURCE

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#include <getopt.h>
#include <termios.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/mount.h>
#include <dirent.h>
#include <fcntl.h>
//#include <pthread.h>

#include <osal/osal_stdtypes.h>
#include <osal/osal_string.h>

#include <utils/mms_debug.h>

#include "utils/linux/cmdline_parser.h"
#include "utils/linux/cmdline_debug.h"

mmsdbg_define_variable(vdl_cmdline_debug, DL_DEFAULT|DL_MESSAGE, 0,
    "auxcmddbg", "Utilities for debuging command line parameters");
#define MMSDEBUGLEVEL vdl_cmdline_debug

// ----------------------------------------------------------------------------
// Data types

typedef struct {
  int64_t val;
  char    str[32];
  char    abriv[16];
} cmd_line_dl_values_t;

#define NULL_DEVICE_FILE "/dev/null"
#define ADD_TO_SET_LIST(format, arg...) \
      sprintf(config_output + strlen(config_output), format, ##arg);

static char *programNameFile;

int cmdline_dbg_flags = 0;

static int mmscmdld_debugIndex = 0x10000000;

#ifdef __MMS_DEBUG__
int mmsCMDLineDBG_NetPort = 6660;
static FILE  *stddebugFile = NULL;
static char  stddebugFile_Name[512] = {0};
char mmsCMDLineDBG_DSTPath[512] = "/tmp/";

// ----------------------------------------------------------------------------
// Private variables

/** This array provides a mechanism to set value of debug variables
    located in various HAL resources and other if any
    This map between IDs (passed via command line) of debug level variables of
    HAL resources and addresses of thes variables */
extern void *mmsdbgvarlist;
extern void *__data_start;
extern void *__bss_start;
static struct mmsdbg_dynamic_variable_register *mmsdbgvarlist_base;
static int mmsdbgvarlist_count = 0;

///a map between debug level values and their human readable ids
static cmd_line_dl_values_t    hal_examp_dl_values[] = {
  { DL_FATAL,    "FATAL", "fatal" },
  { DL_ERROR,    "ERROR", "err" },
  { DL_WARNING,  "WARNING", "warn" },
  { DL_FUNC,     "FUNC", "func" },
  { DL_MESSAGE,  "MESSAGE", "msg" },
  { DL_PRINT,    "PRINT",   "print" },
  { DL_LAYER0,   "0", "0" },
  { DL_LAYER1,   "1", "1" },
  { DL_LAYER2,   "2", "2" },
  { DL_LAYER3,   "3", "3" },
  { DL_LAYER4,   "4", "4" },
  { DL_LAYER5,   "5", "5" },
  { DL_LAYER6,   "6", "6" },
  { DL_LAYER7,   "7", "7" },
  { DL_LAYER8,   "8", "8" },
  { DL_LAYER9,   "9", "9" },
  { DL_LAYER10,  "10", "10" },
  { DL_LAYER11,  "11", "11" },
  { DL_LAYER12,  "12", "12" },
  { DL_LAYER13,  "13", "13" },
  { DL_LAYER14,  "14", "14" },
  { DL_LAYER15,  "15", "15" },
  { DL_LAYER16,  "16", "16" },
  { DL_LAYER17,  "17", "17" },
  { DL_LAYER18,  "18", "18" },
  { DL_LAYER19,  "19", "19" },
  { DL_LAYER20,  "20", "20" },
  { DL_LAYER21,  "21", "21" },
  { DL_LAYER22,  "22", "22" },
  { DL_LAYER23,  "23", "23" },
  { DL_PROFILE,  "PROFILE", "profile" },
  { DL_DEFAULT,  "DEFAULT", "def" },
  { DL_FULL,  "FULL", "full" },
  { 0,  "", "" },
};

/// store terminal settings
//static struct termios           saved_term_attrs;

// ----------------------------------------------------------------------------
// Private functions

static char mmscmdld_CopyFileBuffer[1024];
static int cmdlinedbg_CopyFile(char *src, char *dst)
{
  int fds, fdd, size, err;

  fds = open(src, O_RDONLY);
  if (fds<=0) {
    mmsdbg(DL_ERROR, "OPEN: %s", strerror(errno));
    return errno;
  }

  fdd = open(dst, O_CREAT | O_TRUNC | O_WRONLY, 0666);
  if (fdd<=0) {
    mmsdbg(DL_ERROR, "OPEN: %s", strerror(errno));
    return errno;
  }

  mmsinfo(DL_WARNING, "copying: %s ---> %s", src, dst);
  while ((size = read(fds, mmscmdld_CopyFileBuffer, sizeof(mmscmdld_CopyFileBuffer)))==sizeof(mmscmdld_CopyFileBuffer)) {
    err = write (fdd, mmscmdld_CopyFileBuffer, size);
  }

  if (size>0) {
    err = write (fdd, mmscmdld_CopyFileBuffer, size);
  }
  close(fds);
  close(fdd);

  return 0;
}

static int cmdlinedbg_CopyDir(char *src, char *dst)
{
 DIR *dp;
 struct dirent *d;
 char srcfile[512], dstfile[512];

 /* Recursively copy files in SOURCE.  */
  dp = opendir(src);
  if (dp == NULL) {
    mmsdbg(DL_ERROR, "COPY: %s", strerror(errno))
    return errno;
  }

  while ((d = readdir(dp)) != NULL) {
    if (d->d_type == DT_REG) {

      sprintf(srcfile, "%s/%s", src, d->d_name);
      sprintf(dstfile, "%s/%s", dst, d->d_name);

      cmdlinedbg_CopyFile(srcfile, dstfile);
    }
  }
  closedir(dp);

  return errno;
}

static int32_t cmdlinedbg_val_to_str(int64_t debug_val, char* str)
{
  int32_t j;
  int32_t max  = sizeof(hal_examp_dl_values)/sizeof(hal_examp_dl_values[0]);
  int32_t w = 0;
  int32_t offset = 52;
  int8_t  tmp[512] = {0};
  int32_t cut = 3;
  int8_t   padding[255];

  j = 0;
  while(j < offset) {
    padding[j++] = ' ';
  }
  padding[j] = 0;

  for(j = 0; (j < max) && (j < (int32_t)sizeof(int64_t)*8); j++) {
    if ( debug_val & (1<<j) ) {
      int32_t id_len = strlen(hal_examp_dl_values[j].str);

      strcat((char*)tmp, ",");
      if (w > 30) {
        strcat((char*)tmp, "\n");
        strcat((char*)tmp, (char*)padding);
        w = 0;
      }
      strncat((char*)tmp,hal_examp_dl_values[j].str, cut);
      w += 1 + (id_len < cut ? id_len : cut);
    }
  }

  strcpy(str, (char*)&tmp[1]);

  return 0;
}

static cmd_line_dl_values_t *cmdlinedbg_find_level(char *arg)
{
    cmd_line_dl_values_t *level, *levelfind;
    int depth, len;

    level = hal_examp_dl_values;
    levelfind = NULL;
    depth = 0;
    while (level->val) {
        len = osal_strlen(level->abriv);
        if (osal_strncmp(level->abriv,arg,len)==0) {
            if (len>depth) {
                depth = len;
                levelfind = level;
            }
        }
        level++;
    }

    return levelfind;
}

static int cmdlinedbg_add_debug(struct mmsdbg_dynamic_variable_register *dbg,
    cmd_line_dl_values_t *level, int operationAdd)
{
    int count;

    if (dbg) {
        if (operationAdd) {
            *dbg->var |= (level->val);
        } else {
            *dbg->var &= ~(level->val);
        }
    } else {
        dbg   = mmsdbgvarlist_base;
        count = mmsdbgvarlist_count;
        while (count--) {
            if (operationAdd) {
                *dbg->var |= (level->val);
            } else {
                *dbg->var &= ~(level->val);
            }

            dbg++;
        }
    }

    return 0;
}


static pthread_t mmscmdld_hRemoteThread = 0;
static char* mmscmdld_USBMountDir="/mnt/usb0";
static int cmdlinedbg_hndMountUSB(int val, char *opt, char *arg, int index)
{
  int res;

  val = val;
  opt = opt;
  index = index;

  if (((strcasestr(arg,"mount") != NULL) && (arg[5] == '\0'))
  ||((strcasestr(arg,"m") != NULL) && (arg[1] == '\0'))) {
    mmsinfo(DL_WARNING, "MKDIR: %s", mmscmdld_USBMountDir);
    res = mkdir(mmscmdld_USBMountDir, 0x777);
    if (res) {
      mmsdbg(DL_ERROR, "MKDIR ERROR: %s", strerror(errno))
    }

    mmsinfo(DL_WARNING, "Mouning USB storage");
    res = mount("/dev/sda1", mmscmdld_USBMountDir, "vfat", MS_MGC_VAL, NULL);
    if (res) {
      mmsdbg(DL_ERROR, "MOUNT ERROR: %s", strerror(errno))
    }
  }
  else if (((strcasestr(arg,"umount") != NULL) && (arg[6] == '\0'))
  ||((strcasestr(arg,"u") != NULL) && (arg[1] == '\0'))) {
    mmsinfo(DL_WARNING, "Unmouning USB storage");
    res = umount(mmscmdld_USBMountDir);
    if (res) {
      mmsdbg(DL_ERROR, "MOUNT ERROR: %s", strerror(errno))
    }
  }
  else {
    mmsdbg(DL_ERROR, "ERROR: %s: operation NOT supported", arg);
  }

  return 0;
}

static int cmdlinedbg_hndCopy(int val, char *opt, char *arg, int index)
{
  char dst_dir[1024];
  int res = 0;

  val = val;
  opt = opt;
  index = index;

  sprintf(dst_dir, "%s/%s", mmscmdld_USBMountDir, arg);

  mmsinfo(DL_WARNING, "MKDIR: %s", dst_dir);
  mkdir(dst_dir, 0x777);
  if (res) {
    mmsdbg(DL_ERROR, "MKDIR ERROR: %s", strerror(errno))
  }

  mmsinfo(DL_WARNING, "COPY: %s ---> %s", mmsCMDLineDBG_DSTPath, dst_dir);
  cmdlinedbg_CopyDir(mmsCMDLineDBG_DSTPath, dst_dir);

  return 0;
}

static void* cmdlinedbg_RemoteCtrlThrFxn(void * data)
{
  int socket_fd = -1;
  struct sockaddr_in socket_addr;
  char *packet, *val, *valNext;
  char **argv;
  int size, argc, len;

  mmsdbg(DL_MESSAGE|DL_WARNING, "Create thread for Remote debug control thread\n");
#ifdef CONFIG_HWL_SYSTEM
  mmsSystem_RegisterThread("DBGNET");
#endif

  socket_fd = socket(PF_INET, SOCK_DGRAM, 0);
  if (socket_fd > 0) {
    memset(&socket_addr, 0, sizeof(struct sockaddr_in));
    socket_addr.sin_family      = AF_INET;
    socket_addr.sin_port        = htons(mmsCMDLineDBG_NetPort);
    socket_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    if (bind(socket_fd, (struct sockaddr *)&socket_addr, sizeof(struct sockaddr_in))!=0) {
      mmsdbg(DL_WARNING, "DUMP: socket %d wasn't bind - %s", socket_fd, strerror(errno));
      close(socket_fd);
    }
  } else {
    mmsdbg(DL_WARNING, "DUMP: socket for remote debug control, wasn't create successful - %s", strerror(errno));
  }

  if (socket_fd<=0) {
    mmsdbg(DL_WARNING, "DUMP: Thread for remote debug control are closed");
    return 0;
  }

  packet = malloc(1024);
  argv = malloc(128*sizeof(void*));

  while (socket_fd>0) {
    memset(packet, 0, sizeof(1024));
    size = recv(socket_fd, packet, 1024, 0);
    packet[size] = 0;

    valNext = packet;
    argc = 1;
    argv[0] = "filename";
    do {
      val = valNext;
      valNext = strchr(valNext, ' ');
      if (valNext!=NULL) {
        *valNext++ = 0;
      }

      while (strlen(val)>0) {
        len = strlen(val);
        if (val[len-1]==10) { val[len-1] = 0; continue; }
        if (val[len-1]==13) { val[len-1] = 0; continue; }
        break;
      }
      argv[argc++] = val;
    } while (valNext!=NULL);
    cmdline_parser(argc, argv);

    if (cmdline_dbg_flags & CMDLINE_DBG_FLAG_HELP) {
      cmdline_help_print(CMDLINE_FLAG_HELP_MASK);
      cmdline_dbg_flags &= ~(CMDLINE_DBG_FLAG_HELP);
    }

  }

  return 0;
}

static int cmdlinedbg_hndDBGRemotePort(int val, char *opt, char *arg, int index)
{
  if (arg==NULL) {
    return -1;
  }

  mmsCMDLineDBG_NetPort = atoi(arg);
  if (mmsCMDLineDBG_NetPort<=0) {
    mmsCMDLineDBG_NetPort = 6660;
  }

  return 0;
}

static int cmdlinedbg_hndDBGRemote(int val, char *opt, char *arg, int index)
{
  cmdline_dbg_remote_control();

  return 0;
}

static int cmdlinedbg_hndDBGPath(int val, char *opt, char *arg, int index)
{
  if (arg==NULL) {
    return -1;
  }

  cmdline_dbg_flags |= CMDLINE_DBG_FLAG_PATH;
  strcpy(mmsCMDLineDBG_DSTPath, arg);

  return 0;
}

static int cmdlinedbg_hndPrintLevels(int val, char *opt, char *arg, int index)
{
  cmdline_dbg_flags |= CMDLINE_DBG_FLAG_HELP_DEBUG_VAR;

  return 0;
}

static int cmdlinedbg_hndSetLevels(int val, char *opt, char *arg, int index)
{
    struct mmsdbg_dynamic_variable_register *dbg, *dbgfind = NULL;
    cmd_line_dl_values_t *levelS, *levelE;
    int count, depth, len;
    bool operationAdd = true;

    //Find debug arguemnts
    dbg   = mmsdbgvarlist_base;
    count = mmsdbgvarlist_count;
    depth = 0;
    while (count--) {
        len = osal_strlen(dbg->key);
        if (osal_strncmp(dbg->key,arg,len)==0) {
            if (len>depth) {
                depth = len;
                dbgfind = dbg;
            }
        }

        dbg++;
    }

    if (dbgfind==NULL) {
        len = osal_strlen("all");
        if (osal_strncmp("all",arg,len)==0) {
            if (len>depth) {
                depth = len;
            }
        } else {
            mmsdbg(DL_ERROR, "invalid value: %s", arg);
            goto cmdlinedbg_hndSetLevels_Fail;
        }
    }

    //Skipping variable ID
    arg += depth;
    operationAdd = 1;
    levelS = NULL;
    levelE = NULL;
    while (*arg!=0) {
        if (levelS==NULL) {
            levelS = cmdlinedbg_find_level(arg);
            if (levelS)
                arg += osal_strlen(levelS->abriv);
        } else {
            levelE = cmdlinedbg_find_level(arg);
            if (levelE)
                arg += osal_strlen(levelE->abriv);
        }

        if (levelS==NULL) {
            goto new_level;
        }

        if (levelE!=NULL) {
            if (levelS<levelE) {
                while (levelS<=levelE) {
                    cmdlinedbg_add_debug(dbgfind, levelS, operationAdd);
                    levelS++;
                }
            }
            goto new_level;
        }

        if (*arg=='-') {
            arg++;
            continue;
        }

        cmdlinedbg_add_debug(dbgfind, levelS, operationAdd);

new_level:
        switch (*arg) {
            case '-': operationAdd = 0; break;
            case '+': operationAdd = 1; break;
            default: operationAdd = 1; break;
        }

        if (*arg==0)
            break;

        arg++;

        levelS = NULL;
        levelE = NULL;
    }

    cmdline_dbg_flags |= CMDLINE_DBG_FLAG_DEBUGLAYERS;

    return 0;

cmdlinedbg_hndSetLevels_Fail:
    return -1;
}

static int cmdlinedbg_hndSTDDevice(int val, char *opt, char *arg, int index)
{
  if (arg==NULL) {
    return -1;
  }

  cmdline_dbg_flags |= CMDLINE_DBG_FLAG_DEBUGLAYERS;

  if (cmdline_dbg_flags & CMDLINE_DBG_FLAG_OPEN_LOG_FILE) {
    printf("%4d:%s: Close previously opened log file `%s`\n",(int)MMSGETTHREADID(), __FUNCTION__, stddebugFile_Name);
    fclose(stddbg);
    cmdline_dbg_flags &= ~(CMDLINE_DBG_FLAG_OPEN_LOG_FILE);
    stddebugFile_Name[0] = 0;
    stddebugFile = NULL;
  }

  if (strcmp(arg,"out")==0) {
    mmsdbg_config_output(stdout);
    printf("%4d:%s: DBG info output is directed to STDOUT\n", (int)MMSGETTHREADID(), __FUNCTION__);
    return 0;
  }

  if (strcmp(arg,"err")==0) {
    mmsdbg_config_output(stderr);
    printf("%4d:%s: DBG info output is directed to STDERR\n", (int)MMSGETTHREADID(), __FUNCTION__);
    return 0;
  }

#ifdef ___ANDROID___
  if (strcmp(arg,"logcat")==0) {
    mmsdbg_config_output(NULL);
    printf("%4d:%s: DBG info output is directed to ADB logcat\n", (int)MMSGETTHREADID(), __FUNCTION__);
    return 0;
  }
#endif

  if (strcmp(arg,"none")==0) {
    printf("%4d:%s: DBG info output is directed to /dev/null i.e it DISABLED\n", (int)MMSGETTHREADID(), __FUNCTION__);
    strncpy(stddebugFile_Name, NULL_DEVICE_FILE, sizeof(stddebugFile_Name));
  } else {
    strncpy(stddebugFile_Name, arg, sizeof(stddebugFile_Name));
  }


  stddebugFile = fopen(stddebugFile_Name, "a+");

  if (stddebugFile == NULL) {
    mmsdbg(DL_ERROR, "ERROR: can't append to debug dile `%s` !", stddebugFile_Name);
  }

  cmdline_dbg_flags |= CMDLINE_DBG_FLAG_OPEN_LOG_FILE;
  mmsdbg_config_output(stddebugFile);
  printf("%4d:%s: DBG info output is directed to `%s`\n", (int)MMSGETTHREADID(), __FUNCTION__, stddebugFile_Name);

  return 0;
}
#endif

static int cmdlinedbg_hndHelpPrint(int val, char *opt, char *arg, int index)
{
  cmdline_dbg_flags |= CMDLINE_DBG_FLAG_HELP;

  return CMDLINE_ERROR_PRINT_HELP;
}

// ----------------------------------------------------------------------------
// Public functions

#ifdef __MMS_DEBUG__

int cmdline_dbg_initialize(void *base_register, void *end_register)
{
    mmsdbgvarlist_count = 0;
    if ((!base_register) || (end_register < base_register)){
        return -1;
    }

    mmsdbgvarlist_count = (int)(end_register - base_register);

    if (mmsdbgvarlist_count % sizeof(struct mmsdbg_dynamic_variable_register))
    {
        mmsdbgvarlist_count = 0;
        return -1;
    }
    mmsdbgvarlist_base = base_register;
    mmsdbgvarlist_count /= sizeof(struct mmsdbg_dynamic_variable_register);
    return 0;
}

int cmdline_dbg_register_debug_layers(bool en_short, bool en_long)
{
#ifdef __MMS_DEBUG__
  cmdline_add_option(CMDLINE_FLAG_OPT_OPTIONAL|
      CMDLINE_FLAG_VAL_MANDATORY|CMDLINE_FLAG_HELP_LEVEL9,
      (en_short) ? 'd' : 0, (en_long) ? "debug" : NULL,
      "levels", mmscmdld_debugIndex++, NULL, cmdlinedbg_hndSetLevels,
      "  -d | --debug=val         Sets debug level of HAL resources.\n"
      "                             val = <resID>[operation]<list>\n"
      "                             <resID>       ::= ""all"" reserved for all modules all other\n"
      "                                               possible can be viewed if specify --dbg\n"
      "\n"
      "                             [operation]   ::= <+> | <-> - Enable(default) | Disable.\n"
      "                             <list>        ::= <entry> | <entry>,<list>\n"
      "                             <entry>       ::= <word> | <layer> | <layer_range>\n"
      "                             <word>        ::= fatal|err|warn|func|msg|print|profile|full|def|layers\n"
      "                             <layer_range> ::= <layer>-<layer>\n"
      "                             <layer>       ::= 1|2|...|25\n", NULL);

  cmdline_add_option(CMDLINE_FLAG_OPT_OPTIONAL|CMDLINE_FLAG_HELP_LEVEL9,
      0, "dbg", NULL, mmscmdld_debugIndex++, NULL, cmdlinedbg_hndPrintLevels,
      "  --dbg                    Print table with set debug levels\n", NULL );

  cmdline_add_option(CMDLINE_FLAG_OPT_OPTIONAL|
      CMDLINE_FLAG_VAL_MANDATORY|CMDLINE_FLAG_HELP_LEVEL9,
      0, "dbgstd", "dev", mmscmdld_debugIndex++, NULL, cmdlinedbg_hndSTDDevice,
      "  --dbgstd=word            Set output file. By default all dbg output goes out to "
#ifdef ___ANDROID___
"ADB logcat system"
#else
"STDOUT"
#endif
"\n"
      "                           word can be one of these:\n"
      "                              out                all dbg info will be directed to STDOUT\n"
      "                              err                all dbg info will be directed to STDERR\n"
#ifdef ___ANDROID___
      "                              logcat             all dbg info will be directed to ADB logcat [DEFAULT]\n"
#endif
      "                              none               disable all dbg output\n"
      "                              /path/to/dbg.log   all dbg info will be saved in specified log file.\n"
      "                                                 Relatives path are also accepted\n\n"
      "     Examples for redirecting debug info output:\n\n"
#ifdef ___ANDROID___
      "       # stop all output:\n"
      "         adb shell 'echo --dbgstd=none   | nc -w 1 -u localhost 6660'\n"
      "\n"
      "        redirect dbg info to ADB logcat system:\n"
      "         adb shell 'echo --dbgstd=logcat | nc -w 1 -u localhost 6660'\n"
      "\n"
      "       # redirect dbg info to file:\n"
      "         adb shell 'echo --dbgstd=/path/to/dbg.log | nc -w 1 -u localhost 6660'\n"
      "\n"
      "       # redirect dbg info to STDOUT:\n"
      "         adb shell 'echo --dbgstd=out    | nc -w 1 -u localhost 6660'\n"
      "\n"
      "       # redirect dbg info to STDERR:\n"
      "         adb shell 'echo --dbgstd=err    | nc -w 1 -u localhost 6660'\n"
#else
      "       # stop all output:\n"
      "         echo --dbgstd=none   | nc -q 1 -u remote_ip 6660\n"
      "\n"
      "       # redirect dbg info to file:\n"
      "         echo --dbgstd=/path/to/dbg.log | nc -q 1 -u remote_ip 6660\n"
      "\n"
      "       # redirect dbg info to STDOUT:\n"
      "         echo --dbgstd=out    | nc -q 1 -u remote_ip 6660\n"
      "\n"
      "       # redirect dbg info to STDERR:\n"
      "         echo --dbgstd=err    | nc -q 1 -u remote_ip 6660\n"
#endif

      "\n", NULL );

  cmdline_add_option(CMDLINE_FLAG_OPT_OPTIONAL|
      CMDLINE_FLAG_VAL_MANDATORY|CMDLINE_FLAG_HELP_LEVEL9,
      0, "dbgport", "port", mmscmdld_debugIndex++, NULL, cmdlinedbg_hndDBGRemotePort,
      "  --dbgport=<port>         Set UDP port for receiving dbg command (default is 6660)\n", NULL );

  cmdline_add_option(CMDLINE_FLAG_OPT_OPTIONAL|CMDLINE_FLAG_HELP_LEVEL9,
      0, "dbgremote", NULL, mmscmdld_debugIndex++, NULL, cmdlinedbg_hndDBGRemote,
      "  --dbgremote              Enable support of remote debug control\n", NULL );

  cmdline_add_option(CMDLINE_FLAG_OPT_OPTIONAL|
      CMDLINE_FLAG_VAL_MANDATORY|CMDLINE_FLAG_HELP_LEVEL9,
      0, "dbgpath", "path", mmscmdld_debugIndex++, NULL, cmdlinedbg_hndDBGPath,
      "  --dbgpath=<path>         Some dump macros use this directory prefix to store there files\n"
      "                           which they produced\n", NULL );

 cmdline_add_option(CMDLINE_FLAG_OPT_OPTIONAL|
      CMDLINE_FLAG_VAL_MANDATORY|CMDLINE_FLAG_HELP_LEVEL9,
      0, "dbgusb", "mount|umount", mmscmdld_debugIndex++, NULL, cmdlinedbg_hndMountUSB,
      "  --dbgusb=mount|umount    Mount/umount first USB device\n", NULL );

 cmdline_add_option(CMDLINE_FLAG_OPT_OPTIONAL|
      CMDLINE_FLAG_VAL_MANDATORY|CMDLINE_FLAG_HELP_LEVEL9,
      0, "dbgcp", "dst_dir", mmscmdld_debugIndex++, NULL, cmdlinedbg_hndCopy,
      "  --dbgcp                  Copy dump files produced by MMS library debug\n"
      "                           layers -dvo9 -dmd9 -dam9 -dbb9 from\n"
      "                           user defined dirent (default: /tmp) directory\n"
      "                           to dst_dir in USB device\n", NULL );
#endif

  return 0;
}

int cmdline_dbg_remote_control(void)
{
#ifdef __MMS_DEBUG__
  if (mmscmdld_hRemoteThread==0) {
    if (pthread_create(&mmscmdld_hRemoteThread, NULL, cmdlinedbg_RemoteCtrlThrFxn, NULL) != 0) {
      mmsdbg(DL_WARNING, "Fail to create thread for remote debug control");
    }
  } else {
      mmsdbg(DL_WARNING, "Remote control is already enabled...");
  }
#endif

  return 0;
}
#else
int cmdline_dbg_initialize(void *base_register, void *end_register)
{
    base_register = base_register;
    end_register  = end_register;

    return 0;
}

int cmdline_dbg_register_debug_layers(bool en_short, bool en_long)
{
    return 0;
}

int cmdline_dbg_remote_control(void)
{
    return 0;
}

#endif

int cmdline_dbg_register_helps(bool en_short, bool en_long, char *programName)
{
  programNameFile = programName;

  cmdline_add_option(CMDLINE_FLAG_OPT_OPTIONAL|CMDLINE_FLAG_HELP_HIGH,
      (en_short) ? 'h' : 0, (en_long) ? "help" : NULL, NULL,
      mmscmdld_debugIndex++, NULL, cmdlinedbg_hndHelpPrint,
      (en_short) ?
        ((en_long)
            ? "  -h | --help              Print full help\n"
            : "  -h                       Print full help\n"
        ) : "       --help              Print full help\n", NULL);

  return 0;
}

int cmdline_dbg_print_debug_variables(void)
{
#ifdef __MMS_DEBUG__
  struct mmsdbg_dynamic_variable_register *dbg;
  int count;
  char val_str[256];
#endif

  FLUSHPRINT( "\ntable with ENABLED debug level variables and their current values\n"
          "--------------------------------------------------------------------------------------------------\n"
          "   resID    |         module                                  |hex value|     human readable value\n"
          "--------------------------------------------------------------------------------------------------\n");
#ifdef __MMS_DEBUG__
  dbg   = mmsdbgvarlist_base;
  count = mmsdbgvarlist_count;
  while (count--) {
    cmdlinedbg_val_to_str(*(dbg->var), val_str);
    FLUSHPRINT("%-14s| %-47s | %08X| %s\n", dbg->key, dbg->comment, *(dbg->var), val_str);
    dbg++;
  }
#endif
  FLUSHPRINT( "--------------------------------------------------------------------------------------------------\n\n");

  return 0;
}
