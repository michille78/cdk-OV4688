/*
 * object_track.c
 *
 *  Created on: Oct 19, 2016
 *      Author: aiovtchev
 */

#define MAX_DBG_OBJECTS 1000
typedef struct
{
    void* o[MAX_DBG_OBJECTS];
    int cnt;
    int max_cnt;
}debug_obj_cnt_t;

debug_obj_cnt_t dbg_obj =
{
        .cnt = 0,
        .max_cnt = 0
};

void* search_object = 0x86ccc400; //0x86cd0060;

void debug_obj_add (void* o)
{
int i;
    for (i = 0; i < MAX_DBG_OBJECTS; ++i) {
        if (dbg_obj.o[i] == NULL)
        {
            if (o == search_object)
                printf ("Obj Debug: Allocating object of interest: %p\n", search_object);
            dbg_obj.o[i] = o;
            dbg_obj.cnt++;

            if (dbg_obj.cnt > dbg_obj.max_cnt)
                dbg_obj.max_cnt = dbg_obj.cnt;

            return;
        }
    }
    printf ("OBJ debug internal error - no enough locations\n");
}

void debug_obj_remove(void*o)
{
int i;
    for (i = 0; i < MAX_DBG_OBJECTS; ++i) {
        if (dbg_obj.o[i] == o)
        {
            dbg_obj.o[i] = NULL;
            dbg_obj.cnt--;
            return;
        }
    }
    printf ("OBJ debug remove error - no enough object %p\n", o);
}

void debug_obj_dump(void)
{
int i;
    for (i = 0; i < MAX_DBG_OBJECTS; ++i) {
        if (dbg_obj.o[i] != NULL)
        {
            printf ("OBJ debug object %p\n", dbg_obj.o[i]);
        }
    }
    printf ("OBJ debug num objects: current %d max %d\n", dbg_obj.cnt, dbg_obj.max_cnt);
}

