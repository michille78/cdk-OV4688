/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file mmsCMDLineDebugModules.h
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#ifndef __MMS_EXAMP_CMD_LINE_TOOL_DEBUG_MODULES_H__
#define __MMS_EXAMP_CMD_LINE_TOOL_DEBUG_MODULES_H__

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <getopt.h>

#include "autoconfig.h"

__BEGIN_DECLS

extern int32_t vdl_exlib_cmdlt_debug;
extern int32_t vdl_main_app;

// MMS Library
#ifdef CONFIG_MMS_AVSYNC
#include "mms/mmsAVSync.h"
#endif
#ifdef CONFIG_MMS_BUFFER_LIST
#include "mms/libmms-buffer-list.h"
#endif
#ifdef CONFIG_MMS_CE_CODEC_ENGINE
#include "mms/libmms-ce.h"
#include "mms/libmms-ce-memory.h"
#endif
#ifdef CONFIG_MMS_DAVINCI_RESZ
#include "mms/libmms-davinci-resz.h"
#endif
#ifdef CONFIG_MMS_FRAMEBUFFER
#include "mms/libmms-framebuffer.h"
#endif
#ifdef CONFIG_MMS_HANDLE
#include "mms/libmms-handle.h"
#endif
#ifdef CONFIG_MMS_HARDWARE_OUTPUT
#include "mms/mmsHardwareOutput.h"
#endif
#ifdef CONFIG_MMS_PES_HEADLING
#include "mms/mmsPEShandling.h"
#endif
#ifdef CONFIG_MMS_RECT
#include "mms/libmms-rect.h"
#endif
#ifdef CONFIG_MMS_RESOURCE
#include "mms/mmsResource.h"
#endif
#ifdef CONFIG_MMS_SAMPLERATECONVERTER
#include "mms/mmsSampleRateConverter.h"
#endif
#ifdef CONFIG_MMS_THREAD
#include "mms/libmms-thread.h"
#endif

// HAL resources
#ifdef CONFIG_HWL_AES
#include "mms/mmsAes.h"
#endif
#ifdef CONFIG_HWL_AUDIO_MIXER
#include "mms/mmsAudioMixer.h"
#endif
#ifdef CONFIG_HWL_GRAPHICS
#include "mms/mmsGraphics.h"
#include "mms/mmsBitBltService.h"
#endif
#ifdef CONFIG_HWL_MEDIA_DECODER
#include "mms/mmsMediaDecoder.h"
#endif
#ifdef CONFIG_HWL_MEDIA_ENCODER2
#include "mms/mmsMediaEncoder2.h"
#endif
#ifdef CONFIG_HWL_MEDIA_SOURCE
#include "mms/mmsMediaSource.h"
#endif
#ifdef CONFIG_HWL_RANDOM
#include "mms/mmsRandom.h"
#endif
#ifdef CONFIG_HWL_STREAM_INPUT
#include "mms/mmsStreamInput.h"
#endif
#ifdef CONFIG_HWL_SYSTEM
#include "mms/mmsSystem.h"
#endif
#ifdef CONFIG_HWL_TIME_STAMP
#include "mms/mmsTimeStamp.h"
#endif
#ifdef CONFIG_HWL_VIDEO_LAYER
#include "mms/mmsVideoLayer.h"
#endif
#ifdef CONFIG_HWL_VIDEO_OUTPUT
#include "mms/mmsVideoOutput.h"
#endif
#ifdef CONFIG_HWL_WATCHDOG
#include "mms/mmsWatchdog.h"
#endif

// MMS Example Library
#ifdef CONFIG_MMSLIB_BMPFILE
#include "mmsBMPFile.h"
#endif
#ifdef CONFIG_MMSLIB_CMD_LINE_TOOL
#include "mmsCMDLineTool.h"
#include "mmsCMDLineDebug.h"
#endif
#ifdef CONFIG_MMSLIB_GFXBLITTER
#include "mmsGfxBlitter.h"
#endif
#ifdef CONFIG_MMSLIB_GFXLAYER
#include "mmsGfxLayer.h"
#endif
#ifdef CONFIG_MMSLIB_IR
#include "mmsIr.h"
#endif
#ifdef CONFIG_MMSLIB_PESPARSER
#include "mmsPESParser.h"
#endif
#ifdef CONFIG_MMSLIB_PSHANDLING
#include "mmsPShandling.h"
#endif
#ifdef CONFIG_MMSLIB_RESOURCES
#include "mmsResources.h"
#endif
#ifdef CONFIG_MMSLIB_SIGNAL_CHECK
#include "mmsSignalCheck.h"
#endif
#ifdef CONFIG_MMSLIB_STREAMER
#include "mmsStreamer.h"
#endif

__BEGIN_DECLS

#endif // __MMS_EXAMP_CMD_LINE_TOOL_DEBUG_MODULES_H__
