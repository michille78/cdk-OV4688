/* =============================================================================
*   MultiMedia Solutions AD
*   (c) Copyright 2006-2010, MultiMedia Solutions AD All Rights Reserved.
*
*   Use of this software is controlled by the terms and conditions found
*   in the license agreement under which this software has been supplied.
* =========================================================================== */
/****************************************************************
*  INCLUDE FILES
****************************************************************/

/* ----- System and Platform Files ----------------------------*/
#include <unistd.h>
#include <sys/types.h>
#include <malloc.h>
#include <memory.h>
#include <wait.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <stdio.h>
#include <sys/ioctl.h>
#include <errno.h>
#include <sched.h>
#include <time.h>
#include <sys/time.h>
#include <sys/poll.h>
#include <dlfcn.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/signal.h>
#include <execinfo.h>
#include <pthread.h>

#include <utils/linux/signal_handler.h>

static void sighnd_back_trace(void)
{
    int j, nptrs;
#define SIZE 100
    void *buffer[100];
    char **strings;

    nptrs = backtrace(buffer, SIZE);
    printf("backtrace() returned %d addresses\n", nptrs);

    // The call backtrace_symbols_fd(buffer, nptrs, STDOUT_FILENO)
    // would produce similar output to the following:

    strings = backtrace_symbols(buffer, nptrs);
    if (strings == NULL) {
        perror("backtrace_symbols");
        exit(EXIT_FAILURE);
    }

    for (j = 0; j < nptrs; j++)
        printf("%s\n", strings[j]);

    free(strings);
}

static void sighnd_signal_handler(int signo)
{
    char *signame = "Unknown";
    switch (signo) {
    case SIGHUP:    signame = "SIGHUP"; break;
    case SIGINT:    signame = "SIGINT"; break;
    case SIGQUIT:   signame = "SIGQUIT"; break;
    case SIGILL:    signame = "SIGILL"; break;
    case SIGTRAP:   signame = "SIGTRAP"; break;
    case SIGABRT:   signame = "SIGABRT/SIGIOT"; break;
    //case SIGIOT:    signame = "SIGIOT"; break;
    case SIGBUS:    signame = "SIGBUS"; break;
    case SIGFPE:    signame = "SIGFPE"; break;
    case SIGKILL:   signame = "SIGKILL"; break;
    case SIGUSR1:   signame = "SIGUSR1"; break;
    case SIGSEGV:   signame = "SIGSEGV"; break;
    case SIGUSR2:   signame = "SIGUSR2"; break;
    case SIGPIPE:   signame = "SIGPIPE"; break;
    case SIGALRM:   signame = "SIGALRM"; break;
    case SIGTERM:   signame = "SIGTERM"; break;
    case SIGSTKFLT: signame = "SIGSTKFLT"; break;
    case SIGCHLD:   signame = "SIGCHLD"; break;
    case SIGCONT:   signame = "SIGCONT"; break;
    case SIGSTOP:   signame = "SIGSTOP"; break;
    case SIGTSTP:   signame = "SIGTSTP"; break;
    case SIGTTIN:   signame = "SIGTTIN"; break;
    case SIGTTOU:   signame = "SIGTTOU"; break;
    case SIGURG:    signame = "SIGURG";  break;
    case SIGXCPU:   signame = "SIGXCPU"; break;
    case SIGXFSZ:   signame = "SIGXFSZ"; break;
    case SIGVTALRM: signame = "SIGVTALRM"; break;
    case SIGPROF:   signame = "SIGPROF"; break;
    case SIGWINCH:  signame = "SIGWINCH"; break;
    case SIGIO:     signame = "SIGIO/SIGPOLL"; break;
    //case SIGLOST:  signame = "SIGLOST"; break;
    case SIGPWR:    signame = "SIGPWR"; break;
    case SIGSYS:    signame = "SIGSYS/SIGUNUSED"; break;
    //case SIGUNUSED:signame = "SIGUNUSED"; break;
    //case SIGRTMIN: signame = "SIGRTMIN"; break;
    //case SIGRTMAX: signame = "SIGRTMAX"; break;
    //case SIGSWI:   signame = "SIGSWI"; break;
    default: signame = "unknow name"; break;
    }
    printf("%d(%d):************ Signal Number = %d - %s ************\n",
         (int)pthread_self(), (int)syscall(224), signo, signame);
    fflush(stdout);
    sighnd_back_trace();

    if (0) {
        char str[128];
        int err;

        sprintf(str,"cat /proc/%d/maps", getpid());
        err = system(str);
    }
    exit(-1);
}

static __sighandler_t sig_hndl[255];

void signal_handler_init(void)
{
  sig_hndl[SIGINT] = signal(SIGINT, sighnd_signal_handler);
  sig_hndl[SIGSEGV] = signal(SIGSEGV, sighnd_signal_handler);
  sig_hndl[SIGBUS] = signal(SIGBUS, sighnd_signal_handler);
  sig_hndl[SIGFPE] = signal(SIGFPE, sighnd_signal_handler);
#if 0
  sig_hndl[SIGHUP] = signal(SIGHUP, sighnd_signal_handler);
  sig_hndl[SIGQUIT] = signal(SIGQUIT, sighnd_signal_handler);
  sig_hndl[SIGILL] = signal(SIGILL, sighnd_signal_handler);
  sig_hndl[SIGTRAP] = signal(SIGTRAP, sighnd_signal_handler);
  sig_hndl[SIGABRT] = signal(SIGABRT, sighnd_signal_handler);
  sig_hndl[SIGIOT] = signal(SIGIOT, sighnd_signal_handler);
  sig_hndl[SIGKILL] = signal(SIGKILL, sighnd_signal_handler);
  sig_hndl[SIGUSR1] = signal(SIGUSR1, sighnd_signal_handler);
  sig_hndl[SIGUSR2] = signal(SIGUSR2, sighnd_signal_handler);
  sig_hndl[SIGPIPE] = signal(SIGPIPE, sighnd_signal_handler);
  sig_hndl[SIGALRM] = signal(SIGALRM, sighnd_signal_handler);
  sig_hndl[SIGTERM] = signal(SIGTERM, sighnd_signal_handler);
  sig_hndl[SIGSTKFLT] = signal(SIGSTKFLT, sighnd_signal_handler);
  sig_hndl[SIGCHLD] = signal(SIGCHLD, sighnd_signal_handler);
  sig_hndl[SIGCONT] = signal(SIGCONT, sighnd_signal_handler);
  sig_hndl[SIGSTOP] = signal(SIGSTOP, sighnd_signal_handler);
  sig_hndl[SIGTSTP] = signal(SIGTSTP, sighnd_signal_handler);
  sig_hndl[SIGTTIN] = signal(SIGTTIN, sighnd_signal_handler);
  sig_hndl[SIGTTOU] = signal(SIGTTOU, sighnd_signal_handler);
  sig_hndl[SIGURG] = signal(SIGURG, sighnd_signal_handler);
  sig_hndl[SIGXCPU] = signal(SIGXCPU, sighnd_signal_handler);
  sig_hndl[SIGXFSZ] = signal(SIGXFSZ, sighnd_signal_handler);
  sig_hndl[SIGVTALRM] = signal(SIGVTALRM, sighnd_signal_handler);
  sig_hndl[SIGPROF] = signal(SIGPROF, sighnd_signal_handler);
  sig_hndl[SIGWINCH] = signal(SIGWINCH, sighnd_signal_handler);
  sig_hndl[SIGIO] = signal(SIGIO, sighnd_signal_handler);
  //   sig_hnd[SIGPOLL] = signal(SIGPOLL         SIGIO
  //   sig_hnd[SIGLOST] = signal(SIGLOST, sighnd_signal_handler);
  sig_hndl[SIGPWR] = signal(SIGPWR, sighnd_signal_handler);
  sig_hndl[SIGSYS] = signal(SIGSYS, sighnd_signal_handler);
  sig_hndl[SIGUNUSED] = signal(SIGUNUSED, sighnd_signal_handler);
  sig_hndl[SIGRTMIN] = signal(SIGRTMIN, sighnd_signal_handler);
  sig_hndl[SIGRTMAX] = signal(SIGRTMAX, sighnd_signal_handler);
  //   sig_hndl[SIGSWI] = signal(SIGSWI, sighnd_signal_handler);
#endif

}

