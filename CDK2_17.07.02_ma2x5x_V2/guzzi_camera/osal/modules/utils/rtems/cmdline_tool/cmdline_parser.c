/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file cmdline_parser.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <osal/osal_sysdep.h>
#include <osal/osal_stdlib.h>
#include <osal/osal_stdio.h>
#include <osal/osal_string.h>

#include <utils/mms_debug.h>

#include "utils/rtems/cmdline_parser.h"

#include <unistd.h>

mmsdbg_define_variable(vdl_utils_cmdline_parser, DL_DEFAULT, 0,
   "auxcmdprs", "Utilities for command line parser");
#define MMSDEBUGLEVEL vdl_utils_cmdline_parser

#include <termios.h>

typedef struct {
  int flag;
  int val;
  char *optNameLong;
  char optNameShort;
  char *valName;
  int *valReturn;
  char *help;
  cmdline_handle *handle;
  cmdline_print_help *printHelp;
} mmscmdltTOption;

#define MMSCMDLT_LONG_OPTION_COUNT      (64)

static int mmscmdlt_OptionCount = 0;
static int mmscmdlt_EnvCount = 0;
static char *mmscmdlt_Env[MMSCMDLT_LONG_OPTION_COUNT];
static mmscmdltTOption mmscmdlt_Options[MMSCMDLT_LONG_OPTION_COUNT];
static mmscmdltTOption mmscmdlt_hndUnknow = { handle:NULL };
static mmscmdltTOption mmscmdlt_hndMissed = { handle:NULL };
static mmscmdltTOption mmscmdlt_hndLiberty= { handle:NULL };

/// store terminal settings
static struct termios           saved_term_attrs;

int cmdline_reset(void)
{
  mmscmdlt_OptionCount  = 0;
  mmscmdlt_EnvCount = 0;
  memset(&mmscmdlt_Options, 0, sizeof(mmscmdlt_Options));
  memset(&mmscmdlt_hndUnknow, 0, sizeof(mmscmdlt_hndUnknow));
  memset(&mmscmdlt_hndMissed, 0, sizeof(mmscmdlt_hndMissed));
  memset(&mmscmdlt_hndLiberty, 0, sizeof(mmscmdlt_hndLiberty));
  memset(&mmscmdlt_Env, 0, sizeof(mmscmdlt_Env));

  return 0;
}

int cmdline_set_missed(cmdline_handle *handle)
{
  memset(&mmscmdlt_hndMissed, 0, sizeof(mmscmdltTOption));
  mmscmdlt_hndMissed.handle = handle;

  return 0;
}

int cmdline_set_unknow(cmdline_handle *handle)
{
  memset(&mmscmdlt_hndUnknow, 0, sizeof(mmscmdltTOption));
  mmscmdlt_hndUnknow.handle = handle;

  return 0;
}

int cmdline_set_liberty(int flag, char *valName, cmdline_handle *handle,
      char *help, cmdline_print_help *printHelp)
{
  memset(&mmscmdlt_hndLiberty, 0, sizeof(mmscmdltTOption));
  mmscmdlt_hndLiberty.flag = flag;
  mmscmdlt_hndLiberty.valName = valName;
  mmscmdlt_hndLiberty.handle = handle;
  mmscmdlt_hndLiberty.help = help;
  mmscmdlt_hndLiberty.printHelp = printHelp;

  return 0;
}

int cmdline_add_option(int flag, char optNameShort, char *optNameLong,
      char *valName, int val, int *valReturn, cmdline_handle *handle,
      char *help, cmdline_print_help *printHelp)
{
  mmscmdltTOption *opt;

  if (mmscmdlt_OptionCount>=MMSCMDLT_LONG_OPTION_COUNT) {
    return -1;
  }

  opt = &mmscmdlt_Options[mmscmdlt_OptionCount++];
  opt->flag = flag;
  opt->optNameLong = optNameLong;
  opt->optNameShort = optNameShort;
  opt->valName = valName;
  opt->val = val;
  opt->valReturn = valReturn;
  opt->handle = handle;
  opt->help = help;
  opt->printHelp = printHelp;

  return 0;
}

int cmdline_parser(int argc, char **argv)
{
  char *shortOption;
  char *so;
  struct option *longOption;
  struct option *lo;
  int opt, i, remOptErr, err;
  int option_index = 0;

#ifdef __MMS_DEBUG__
  for (i=0;i<argc;i++) {
    mmsdbg(DL_LAYER0, "argv[%d] = %s", i, argv[i]);
  }
#endif

  remOptErr = opterr;

  optind = 0;
  optopt = 0;
  opterr = 0;

  // Create string for short options
  shortOption = osal_malloc(mmscmdlt_OptionCount*2+1);
  memset(shortOption, 0, mmscmdlt_OptionCount*2+1);

  // Set string for short options
  so = shortOption;
  for (i=0;i<mmscmdlt_OptionCount;i++) {
    if (mmscmdlt_Options[i].optNameShort==0) {
      continue;
    }
    so[strlen(so)] = mmscmdlt_Options[i].optNameShort;
    if (mmscmdlt_Options[i].flag & CMDLINE_FLAG_VAL_OPTIONAL)  so[strlen(so)] = '?';
    if (mmscmdlt_Options[i].flag & CMDLINE_FLAG_VAL_MANDATORY) so[strlen(so)] = ':';
  }
  mmsdbg(DL_LAYER1, "List of Short Options: %s", shortOption);

  // Create array for long options
  longOption = osal_malloc(sizeof(struct option)*(mmscmdlt_OptionCount+1));
  memset(longOption, 0, sizeof(struct option)*mmscmdlt_OptionCount);

  // Set array for long options
  lo = longOption; opt = 0;
  for (i=0;i<mmscmdlt_OptionCount;i++) {
    if (mmscmdlt_Options[i].optNameLong==NULL) {
      continue;
    }
    lo->name = mmscmdlt_Options[i].optNameLong;
    if (mmscmdlt_Options[i].flag & CMDLINE_FLAG_VAL_MANDATORY) lo->has_arg = required_argument;
    else if (mmscmdlt_Options[i].flag & CMDLINE_FLAG_VAL_OPTIONAL) lo->has_arg = optional_argument;
    else lo->has_arg = no_argument;
    lo->val = mmscmdlt_Options[i].val;
    lo->flag = mmscmdlt_Options[i].valReturn;
    mmsdbg(DL_LAYER1, "Add Long Options: %s, val=%d, flag=%p", lo->name, lo->val, lo->flag);
    lo++;
  }

  err = 0;
  while ((opt = getopt_long(argc, argv, shortOption, longOption, &option_index)) != -1) {
    mmsdbg(DL_LAYER1, "opt='%c'-0x%08x; argv=%s, optarg=%s, optopt=0x%08x, optidx=%d",
        opt, opt, argv[optind-1], optarg, optopt, option_index);

    if (opt == ':') {
      if (mmscmdlt_hndMissed.handle) {
        mmsdbg(DL_LAYER1, "Execute Missed handle=%p", mmscmdlt_hndMissed.handle);
        err = mmscmdlt_hndMissed.handle(opt, argv[optind-1], optarg, optind);
      }
    }
    else if (opt == '?') {
      if (mmscmdlt_hndUnknow.handle) {
        mmsdbg(DL_LAYER1, "Execute Unknow handle=%p", mmscmdlt_hndUnknow.handle);
        err = mmscmdlt_hndUnknow.handle(opt, argv[optind-1], optarg, optind-1);
      }
    }
    else {
      //Find option in short lists
      for (i=0;i<mmscmdlt_OptionCount;i++) {
        if (mmscmdlt_Options[i].handle==NULL) {
          continue;
        }

        if ((opt == mmscmdlt_Options[i].val)||
          ((opt!=0)&&(opt == mmscmdlt_Options[i].optNameShort))) {
          mmsdbg(DL_LAYER1, "Execute option handle=%p", mmscmdlt_Options[i].handle);
          err = mmscmdlt_Options[i].handle(opt, argv[optind-1], optarg, optind);
        }
      }
    }
    if (err) {
      break;
    }
  }

  if ((err==0)&&(optind<argc)) {
    for (i=optind;i<argc;i++) {
      if ((mmscmdlt_hndLiberty.handle)&&(strlen(argv[i])>0)) {
        err = mmscmdlt_hndLiberty.handle(0, argv[i], argv[i], i);
      }
    }
  }

  osal_free(shortOption);
  osal_free(longOption);

  opterr = remOptErr;

  return err;
}

int cmdline_parser_string(char *str)
{
    char *args, *argv[MMSCMDLT_LONG_OPTION_COUNT];
    int old_type, new_type, argc, err = 0;

    if (!str)
        goto exit;

    args = osal_calloc(1, osal_strlen(str)+1);
    osal_strcpy(args, str);
    argv[0] = ".";
    argc = 1;
    old_type = 1;
    while (argc < MMSCMDLT_LONG_OPTION_COUNT && *str) {
        new_type = *str == ' ' || *str == '\t';
        if (old_type != new_type) {
            if (new_type)
                *str = 0;
            else
                argv[argc++] = str;
        }
        old_type = new_type;
        str++;
    }

#if 0
{
    int idx;
    for (idx=0 ; idx < argc ; idx++) {
        printf("argv[%d] = %s\n", idx, argv[idx]);
    }
}
#endif

    err = cmdline_parser(argc, argv);

    osal_free(args);

exit:
    return err;
}


int cmdline_parser_env(char *envName)
{
    int err = 0;
    char *env;

    env = getenv(envName);

    mmsdbg(DL_LAYER1, "%s='%s'\n", envName, env);

    if (!env)
        goto exit;

    err = cmdline_parser_string(env);

exit:
    return err;
}


int cmdline_help_line(int flag, char *help)
{
    cmdline_add_option(flag, 0, NULL, NULL, 0, NULL, NULL, help, NULL);

    return 0;
}

int cmdline_help_print(int flag)
{
  mmscmdltTOption *op;
  int i;


  op = &mmscmdlt_hndLiberty;

  if (op->help) {
      FLUSHPRINT("%s\n", op->help);
  }

  if (op->printHelp) {
      op->printHelp(op->optNameShort, op->optNameLong);
  }

  for (i=0;i<mmscmdlt_OptionCount;i++) {
      op = &mmscmdlt_Options[i];
      if ((op->flag & flag)==0) {
          continue;
      }

      if (op->help) {
          FLUSHPRINT("%s\n", op->help);
      }

      if (op->printHelp) {
          op->printHelp(op->optNameShort, op->optNameLong);
      }
  }

  return 0;
}

int cmdline_help_usage_print(char *filename)
{
  int i, flag;
  mmscmdltTOption *opt;

  FLUSHPRINT("Usage: %s", filename);

  for (i=0;i<mmscmdlt_OptionCount;i++) {
    FLUSHPRINT(" ");

    opt = &mmscmdlt_Options[i];

    if (opt->flag & CMDLINE_FLAG_OPT_MANDATORY) FLUSHPRINT("{");
    if (opt->flag & CMDLINE_FLAG_OPT_OPTIONAL) FLUSHPRINT("[");

    flag = 0;
    if (opt->optNameShort!=0) {
      flag |= (1<<0);
      FLUSHPRINT("-%c", opt->optNameShort);
    }
    if (opt->optNameLong) {
      if (flag & (1<<0)) {
        FLUSHPRINT("|");
      }
      FLUSHPRINT("--%s", opt->optNameLong);
    }
    if (opt->flag & (CMDLINE_FLAG_VAL_MANDATORY|CMDLINE_FLAG_VAL_OPTIONAL)) {
      FLUSHPRINT(" ");
      //if (opt->flag & CMDLINE_FLAG_VAL_MANDATORY) FLUSHPRINT("{");
      if (opt->flag & CMDLINE_FLAG_VAL_OPTIONAL) FLUSHPRINT("[");
      FLUSHPRINT("%s", opt->valName);
      if (opt->flag & CMDLINE_FLAG_VAL_OPTIONAL) FLUSHPRINT("]");
      //if (opt->flag & CMDLINE_FLAG_VAL_MANDATORY) FLUSHPRINT("}");
    }

    if (opt->flag & CMDLINE_FLAG_OPT_OPTIONAL) FLUSHPRINT("]");
    if (opt->flag & CMDLINE_FLAG_OPT_MANDATORY) FLUSHPRINT("}");
  }

  if (mmscmdlt_hndLiberty.valName) {
    FLUSHPRINT(" ");

    opt = &mmscmdlt_hndLiberty;
    //if (opt->flag & CMDLINE_FLAG_OPT_MANDATORY) FLUSHPRINT("{");
    if (opt->flag & CMDLINE_FLAG_OPT_OPTIONAL) FLUSHPRINT("[");

    if (opt->valName) FLUSHPRINT("%s", opt->valName);

    if (opt->flag & CMDLINE_FLAG_OPT_OPTIONAL) FLUSHPRINT("]");
    //if (opt->flag & CMDLINE_FLAG_OPT_MANDATORY) FLUSHPRINT("}");
  }

  FLUSHPRINT("\n\n");

  return 0;
}

int cmdline_terminal_save_mode(void)
{
  /* Make sure stdin is a terminal. */
  if (!isatty (STDIN_FILENO)) {
    mmsdbg(DL_WARNING, "STDIN is not a terminal.");
    return 0;
  }

  /* Save the terminal attributes so we can restore them later. */
  tcgetattr (STDIN_FILENO, &saved_term_attrs);

  return 0;
}

int cmdline_terminal_restore_mode(void)
{
  /* Make sure stdin is a terminal. */
  if (!isatty (STDIN_FILENO)) {
    mmsdbg(DL_WARNING, "STDIN is not a terminal.");
    return 0;
  }

  tcsetattr (STDIN_FILENO, TCSANOW, &saved_term_attrs);

  return 0;
}

int cmdline_terminal_set_raw_mode(void)
{
  struct termios tattr;

  /* Make sure stdin is a terminal. */
  if (!isatty (STDIN_FILENO)) {
    mmsdbg(DL_WARNING, "STDIN is not a terminal.");
    return 0;
  }

  /* Set raw terminal mode. */
  tcgetattr (STDIN_FILENO, &tattr);
  /* Clear ICANON and ECHO. */
  tattr.c_lflag &= ~(ICANON|ECHO);
  tattr.c_cc[VMIN] = 1;
  tattr.c_cc[VTIME] = 1;
  tcsetattr (STDIN_FILENO, TCSAFLUSH, &tattr);

  return 0;
}

