/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file pool_unsafe.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <osal/osal_stdlib.h>
#include <osal/osal_stdtypes.h>
#include <osal/osal_list.h>
#include <utils/mms_debug.h>

#include <osal/pool_unsafe.h>

#define OSAL_MEM_PRIVATE
#include <osal/osal_mem_private.h>
#undef OSAL_MEM_PRIVATE

#define HEADER_SIZE_ALIGNMENT 16
#define HEADER_SIZE \
    ALIGN(sizeof (pool_unsafe_header_t), HEADER_SIZE_ALIGNMENT)

#define MAGIC_ALLOCED 0x11AAAA11
#define MAGIC_FREE 0x11DDDD11

typedef struct {
    unsigned int magic;
    pool_unsafe_t *pool;
    struct list_head link;
} pool_unsafe_header_t;

struct pool_unsafe {
    struct list_head free_list;
    struct list_head in_use_list;
    int client_elem_size;
    int elem_full_size;
    int elems_count;
    void *storage;
    int pool_buf_hdr_size;
    int osal_hdr_size;
    char *name;
};

mmsdbg_define_variable(
        vdl_pool_unsafe,
        DL_DEFAULT,
        0,
        "vdl_pool_unsafe",
        "Provides pool allocations."
    );
#define MMSDEBUGLEVEL mmsdbg_use_variable(vdl_pool_unsafe)

/*
 * ****************************************************************************
 * ** Helpers *****************************************************************
 * ****************************************************************************
 */
static osal_mem_header_t * ph_to_oh(pool_unsafe_header_t *ph)
{
    return (osal_mem_header_t *)((char *)ph + HEADER_SIZE);
}

static void mark_free(pool_unsafe_header_t *ph)
{
    ph->magic = MAGIC_FREE;
    osal_mem_mark_free(ph_to_oh(ph));
}

static void mark_alloced(pool_unsafe_header_t *ph)
{
    ph->magic = MAGIC_ALLOCED;
    osal_mem_mark_alloced(ph_to_oh(ph));
}

/*
 * ****************************************************************************
 * ** Free ********************************************************************
 * ****************************************************************************
 */
static void pool_unsafe_free(void *vph, osal_mem_header_t *oh)
{
    pool_unsafe_header_t *ph;

    if (!vph) {
        mmsdbg(DL_ERROR, "POOL NULL buffer handle!");
        return;
    }

    ph = vph;
    if (ph->magic != MAGIC_ALLOCED) {
        mmsdbg(DL_ERROR, "POOL invalid ALLOCED magic: 0x%x!", ph->magic);
        return;
    }

    mark_free(ph);
    list_move(&ph->link, &ph->pool->free_list);
}

/*
 * ****************************************************************************
 * ** Alloc *******************************************************************
 * ****************************************************************************
 */
void * pool_unsafe_alloc(pool_unsafe_t *pool)
{
    pool_unsafe_header_t *ph;

    if (list_empty(&pool->free_list)) {
        return NULL;
    }

    ph = list_entry(pool->free_list.next, pool_unsafe_header_t, link);
    if (ph->magic != MAGIC_FREE) {
        mmsdbg(DL_ERROR, "POOL invalid FREE magic: 0x%x!", ph->magic);
    }

    list_move(&ph->link, &pool->in_use_list);
    mark_alloced(ph);

    return osal_mem_oh_to_client(ph_to_oh(ph));
}

/*
 * ****************************************************************************
 * ** Create/Destroy **********************************************************
 * ****************************************************************************
 */
void pool_unsafe_destroy(pool_unsafe_t *pool)
{
    osal_free(pool->storage);
    osal_free(pool);
}

pool_unsafe_t * pool_unsafe_create(
        char *name,
        unsigned int elem_size,
        unsigned int elems_count
    )
{
    pool_unsafe_t *pool;
    char *p;
    int i;

    pool = osal_malloc(sizeof (*pool));
    if (!pool) {
        mmsdbg(
                DL_ERROR,
                "Failed to allocate new pool (%s) instance: size=%d!",
                name,
                sizeof (*pool)
            );
        goto exit1;
    }

    INIT_LIST_HEAD(&pool->free_list);
    INIT_LIST_HEAD(&pool->in_use_list);

    pool->name = name;
    pool->client_elem_size = elem_size;
    pool->elems_count = elems_count;

    pool->pool_buf_hdr_size = HEADER_SIZE;
    pool->osal_hdr_size = OSAL_MEM_HEADER_SIZE;

    pool->elem_full_size =
              pool->client_elem_size
            + pool->pool_buf_hdr_size
            + pool->osal_hdr_size;

    pool->storage = osal_malloc(
            pool->elem_full_size * pool->elems_count
        );
    if (!pool->storage) {
        mmsdbg(
                DL_ERROR,
                "Failed to allocate pool (%s) storage: size=%d!",
                name,
                pool->elem_full_size * pool->elems_count
            );
        goto exit2;
    }

    p = pool->storage;
    for (i = 0; i < pool->elems_count; i++) {
        pool_unsafe_header_t *ph = (pool_unsafe_header_t *)p;

        osal_mem_header_populate(
                ph_to_oh(ph),
                pool->client_elem_size,
                p,
                pool_unsafe_free,
                NULL
            );

        ph->magic = MAGIC_FREE;
        ph->pool = pool;
        list_add(&ph->link, &pool->free_list);

        p += pool->elem_full_size;
    }

    return pool;
exit2:
    osal_free(pool);
exit1:
    return NULL;
}

