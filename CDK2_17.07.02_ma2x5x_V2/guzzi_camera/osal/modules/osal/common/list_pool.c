/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file list_pool.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <osal/osal_stdlib.h>
#include <osal/osal_stdtypes.h>
#include <osal/osal_mutex.h>
#include <osal/osal_list.h>
#include <osal/osal_assert.h>
#include <utils/mms_debug.h>

#include <osal/list_pool.h>
#include <osal/pool_unsafe.h>

#define OSAL_MEM_PRIVATE
#include <osal/osal_mem_private.h>
#undef OSAL_MEM_PRIVATE

mmsdbg_define_variable(
        vdl_list_pool,
        DL_DEFAULT,
        0,
        "vdl_list_pool",
        "Provides pool from linked list allocations."
    );
#define MMSDEBUGLEVEL mmsdbg_use_variable(vdl_list_pool)


/* --------------------------------- build parameters -----------------------------*/
//#define __LIST_POOL_DEBUG__
#define __LIST_POOL_DESCRIPTION_DEPTH__       8
#define __LIST_POOL_HDR_ALIGNMENT__           16
#define __LIST_POOL_BUFFER_ALIGNMENT__        4
/* --------------------------------------------------------------------------------*/


#define __LIST_POOL_HDR_ALIGNMENT_MASK__   (__LIST_POOL_HDR_ALIGNMENT__ -1)

#define LIST_POOL_HDR_SIZE_ROUND(size)  \
    (((size)+__LIST_POOL_HDR_ALIGNMENT_MASK__)&(~__LIST_POOL_HDR_ALIGNMENT_MASK__))

#define __LIST_POOL_BUFFER_ALIGNMENT_MASK__   (__LIST_POOL_HDR_ALIGNMENT__ -1)

#define LIST_POOL_BUFFER_SIZE_ROUND(size)  \
    (((size)+__LIST_POOL_HDR_ALIGNMENT_MASK__)&(~__LIST_POOL_HDR_ALIGNMENT_MASK__))


#define __LIST_POOL_MAGIC_ALLOCED__           0x33AAAA33
#define __LIST_POOL_MAGIC_FREE__              0x33DDDD33

#define __LIST_POOL_MAGIC_HEAD__              0xCACACECA

#define POOL_MAX_REF_COUNT 32


typedef struct {
    unsigned int magic;
    list_pool_t *pool;
    struct list_head link;
    int ref_count;
}list_pool_hdr_t;

struct pool {
    char   *name;
    struct list_head free_list;
    struct list_head in_use_list;
    osal_mutex *lock;
    osal_sem *free_sem;
    int head_elem_size;
    int head_elem_full_size;
    int head_elems_count;
    void * storage;
    int    list_pool_buf_hdr_size;
    int    osal_hdr_size;

    pool_unsafe_t *elem_storage_pool;
    uint32 elem_head_offset;
};

#define HEADER_SIZE_ALIGNMENT 16
#define HEADER_SIZE \
    ALIGN(sizeof (list_pool_hdr_t), HEADER_SIZE_ALIGNMENT)


inline int get_list_pool_hdr_size(void)
{
    return LIST_POOL_HDR_SIZE_ROUND(sizeof(list_pool_hdr_t));
}

inline void* get_list_pool_hdr_from_osal_ptr(void* posal)
{
uint8 *p = posal;

    p -= get_list_pool_hdr_size();
    return (void*)p;
}

inline void* get_client_ptr_from_list_pool_hdr(void* phdr)
{
uint8 *p = phdr;
    p += get_list_pool_hdr_size() + OSAL_MEM_HEADER_SIZE;
    return (void*)p;
}

inline void* get_osal_ptr_from_list_pool_hdr(void* phdr)
{
uint8 *p = phdr;
    p += get_list_pool_hdr_size();
    return (void*)p;
}

inline list_pool_hdr_t *list_pool_oh_to_ph(void *oh)
{
    return (list_pool_hdr_t *)((char *)oh - get_list_pool_hdr_size());
}

static osal_mem_header_t * ph_to_oh(list_pool_hdr_t *ph)
{
    return (osal_mem_header_t *)((char *)ph + HEADER_SIZE);
}

/* ========================================================================== */
/**
* __list_pool_alloc()
*/
/* ========================================================================== */
static list_pool_head_t * __list_pool_alloc_head(list_pool_t *pool)
{
    list_pool_hdr_t *ph;
    list_pool_head_t *head;
    osal_mutex_lock(pool->lock);
    /* TODO: assert(!list_empty(&pool->free_list)); */

    ph = list_entry(pool->free_list.next, list_pool_hdr_t, link);

    osal_mem_mark_alloced(get_osal_ptr_from_list_pool_hdr(ph));

    list_move(pool->free_list.next, &pool->in_use_list);
    osal_mutex_unlock(pool->lock);
    if (ph->magic != __LIST_POOL_MAGIC_FREE__)
    {
        mmsdbg(DL_ERROR, "POOL (%s) internal error - magic = 0x%x", pool->name, ph->magic);
    }
    ph->magic = __LIST_POOL_MAGIC_ALLOCED__;
    ph->ref_count = 1;
    head = get_client_ptr_from_list_pool_hdr(ph);
    head->magic = __LIST_POOL_MAGIC_HEAD__;
    INIT_LIST_HEAD(&head->list);
    return head;
}

/* ========================================================================== */
/**
* list_pool_alloc()
*/
/* ========================================================================== */
list_pool_head_t * list_pool_alloc_head(list_pool_t *pool)
{
    osal_sem_wait(pool->free_sem);
    return __list_pool_alloc_head(pool);
}

/* ========================================================================== */
/**
* list_pool_try_alloc()
*/
/* ========================================================================== */
list_pool_head_t * list_pool_try_alloc_head(list_pool_t *pool)
{
    if (!osal_sem_try_wait(pool->free_sem)) {
        return __list_pool_alloc_head(pool);
    }
    return NULL;
}

/* ========================================================================== */
/**
* list_pool_alloc_timeout()
*/
/* ========================================================================== */
list_pool_head_t * list_pool_alloc_head_timeout(list_pool_t *pool, uint32 ms)
{
    if (!osal_sem_wait_timeout(pool->free_sem, ms)) {
        return __list_pool_alloc_head(pool);
    }
    mmsdbg(DL_WARNING, "list_pool %s alloc timeout", pool->name);
    return NULL;
}

void * list_pool_append_element(list_pool_t *pool, list_pool_head_t * head)
{
    void *elem;
    struct list_head *new;

    osal_assert(head->magic == __LIST_POOL_MAGIC_HEAD__);

    osal_mutex_lock(pool->lock);
    elem = pool_unsafe_alloc(pool->elem_storage_pool);
    if (elem){
        new = (struct list_head *)((char*)elem+pool->elem_head_offset);
        list_add_tail(new, &head->list);
    } else
        mmsdbg(DL_WARNING, "list_pool %s alloc element fail", pool->name);

    osal_mutex_unlock(pool->lock);
    return elem;
}


static inline void __list_pool_free(list_pool_t *pool, list_pool_hdr_t *ph, void *oh)
{
    list_pool_head_t  * head;
    struct list_head * elem;

    osal_mutex_lock(pool->lock);
    osal_assert(0 < ph->ref_count);
    osal_assert(ph->ref_count <= POOL_MAX_REF_COUNT);
    ph->ref_count--;
    if (0 < ph->ref_count) {
        osal_mutex_unlock(pool->lock);
        return;
    }
    head = (list_pool_head_t *)get_client_ptr_from_list_pool_hdr(ph);
    osal_assert(head->magic == __LIST_POOL_MAGIC_HEAD__);

    while (!list_empty(&head->list))
    {
        elem = head->list.next;
        list_del(elem);
        osal_free((char*)elem - pool->elem_head_offset); // call pool unsafe free
    }
    ph->magic = __LIST_POOL_MAGIC_FREE__;
    osal_mem_mark_free(oh);
    list_move(&ph->link,&pool->free_list);
    osal_mutex_unlock(pool->lock);
    osal_sem_post(pool->free_sem);
}

/* ========================================================================== */
/**
* list_pool_free()
*/
/* ========================================================================== */
static void list_pool_free(void *vph, osal_mem_header_t *oh)
{
list_pool_hdr_t *ph;

    ph = vph; //get_list_pool_hdr_from_osal_ptr(buf);
    if (!ph) {
        mmsdbg(DL_ERROR,"LIST_POOL NULL buffer handle");
        return;
    }
    if (ph->magic != __LIST_POOL_MAGIC_ALLOCED__) {
        mmsdbg(DL_ERROR,"LIST_POOL invalid magic %0x%x", ph->magic);
        return;
    }

    __list_pool_free(ph->pool, ph, oh);
}

/* ========================================================================== */
/**
* list_pool_lock()
*/
/* ========================================================================== */
static void list_pool_lock_head(void *vph, osal_mem_header_t *oh)
{
    list_pool_t *pool;
    list_pool_hdr_t *ph;

    if (!vph) {
        mmsdbg(DL_ERROR, "POOL NULL buffer!");
        return;
    }

    ph = vph;

    if (ph->magic != __LIST_POOL_MAGIC_ALLOCED__) {
        mmsdbg(DL_ERROR,"POOL invalid magic %0x%x", ph->magic);
        return;
    }

    pool = ph->pool;

    osal_mutex_lock(pool->lock);
    osal_assert(0 < ph->ref_count);
    osal_assert(ph->ref_count < POOL_MAX_REF_COUNT);
    ph->ref_count++;
    osal_mutex_unlock(pool->lock);
}

/* ========================================================================== */
/**
* list_pool_destroy()
*/
/* ========================================================================== */
void list_pool_destroy(list_pool_t *pool)
{
    if (!list_empty_careful(&pool->in_use_list)) {
        mmsdbg(DL_FATAL, "CRITICAL ERROR - poll (%s) destroy was called with still active buffers", pool->name);
    }
    pool_unsafe_destroy(pool->elem_storage_pool);
    osal_sem_destroy(pool->free_sem);
    osal_mutex_destroy(pool->lock);
    osal_free(pool->storage);
    osal_free(pool);
}

/* ========================================================================== */
/**
* list_pool_create()
*/
/* ========================================================================== */
list_pool_t * list_pool_create(  char *pool_name,
                                   unsigned int list_count,
                                   unsigned int elem_size,
                                   unsigned int elem_head_offset,
                                   unsigned int elems_count
                               )
{
    list_pool_t *pool;
    char *p;
    int i;

    pool = osal_malloc(sizeof (*pool));
    if (!pool) {
        mmsdbg(
                DL_ERROR,
                "Failed to allocate new pool (%s) instance: size=%d!",
                pool_name,
                sizeof (*pool)
            );
        goto exit1;
    }

    INIT_LIST_HEAD(&pool->free_list);
    INIT_LIST_HEAD(&pool->in_use_list);

    pool->name = pool_name;
    pool->head_elem_size = sizeof(list_pool_head_t);
    pool->head_elems_count = list_count;

    pool->list_pool_buf_hdr_size = HEADER_SIZE;
    pool->osal_hdr_size = OSAL_MEM_HEADER_SIZE;

    pool->head_elem_full_size =
              pool->head_elem_size
            + pool->list_pool_buf_hdr_size
            + pool->osal_hdr_size;

    pool->storage = osal_malloc(
            pool->head_elem_full_size * pool->head_elems_count
        );
    if (!pool->storage) {
        mmsdbg(
                DL_ERROR,
                "Failed to allocate pool (%s) storage: size=%d!",
                pool_name,
                pool->head_elem_full_size * pool->head_elems_count
            );
        goto exit2;
    }

    pool->lock = osal_mutex_create();
    if (!pool->lock) {
        mmsdbg(
                DL_ERROR,
                "Failed to create mutex for pool (%s) instance!",
                pool_name
            );
        goto exit3;
    }

    pool->free_sem = osal_sem_create(pool->head_elems_count);
    if (!pool->free_sem) {
        mmsdbg(
                DL_ERROR,
                "Failed to create semaphore for pool (%s) instance!",
                pool->name
            );
        goto exit4;
    }

    p = pool->storage;
    for (i = 0; i < pool->head_elems_count; i++) {
        list_pool_hdr_t *ph = (list_pool_hdr_t *)p;

        osal_mem_header_populate(
                ph_to_oh(ph),
                pool->head_elem_size,
                p,
                list_pool_free,
                list_pool_lock_head
            );

        ph->magic = __LIST_POOL_MAGIC_FREE__;
        ph->pool = pool;
        list_add(&ph->link, &pool->free_list);

        p += pool->head_elem_full_size;
    }
    pool->elem_head_offset = elem_head_offset;
    pool->elem_storage_pool = pool_unsafe_create("ELEM_STORAGE_POOL",
                                          elem_size,
                                          elems_count);

    return pool;
exit4:
    osal_mutex_destroy(pool->lock);
exit3:
    osal_free(pool->storage);
exit2:
    osal_free(pool);
exit1:
    return NULL;
}

