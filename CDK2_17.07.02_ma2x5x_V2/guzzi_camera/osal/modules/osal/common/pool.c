/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file pool.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <osal/osal_stdlib.h>
#include <osal/osal_stdtypes.h>
#include <osal/osal_mutex.h>
#include <osal/osal_list.h>
#include <osal/osal_assert.h>
#include <utils/mms_debug.h>

#include <osal/pool.h>

#define OSAL_MEM_PRIVATE
#include <osal/osal_mem_private.h>
#undef OSAL_MEM_PRIVATE

#define HEADER_SIZE_ALIGNMENT 16
#define HEADER_SIZE \
    ALIGN(sizeof (pool_header_t), HEADER_SIZE_ALIGNMENT)

#define MAGIC_ALLOCED 0x11AAAA11
#define MAGIC_FREE    0x11DDDD11

#define MAX_REF_COUNT 16

typedef struct {
    unsigned int magic;
    pool_t *pool;
    struct list_head link;
    int ref_count;
} pool_header_t;

struct pool {
    struct list_head free_list;
    struct list_head in_use_list;
    osal_mutex *lock;
    osal_sem *free_sem;
    int client_elem_size;
    int elem_full_size;
    int elems_count;
    void *storage;
    int pool_buf_hdr_size;
    int osal_hdr_size;
    char *name;
};

mmsdbg_define_variable(
        vdl_pool,
        DL_DEFAULT,
        0,
        "vdl_pool",
        "Provides pool allocations."
    );
#define MMSDEBUGLEVEL mmsdbg_use_variable(vdl_pool)

/*
 * ****************************************************************************
 * ** Helpers *****************************************************************
 * ****************************************************************************
 */
static osal_mem_header_t * ph_to_oh(pool_header_t *ph)
{
    return (osal_mem_header_t *)((char *)ph + HEADER_SIZE);
}

static void mark_free(pool_header_t *ph)
{
    ph->magic = MAGIC_FREE;
    osal_mem_mark_free(ph_to_oh(ph));
}

static void mark_alloced(pool_header_t *ph)
{
    ph->magic = MAGIC_ALLOCED;
    osal_mem_mark_alloced(ph_to_oh(ph));
}

/*
 * ****************************************************************************
 * ** Free ********************************************************************
 * ****************************************************************************
 */
static void _pool_free(
        pool_t *pool,
        pool_header_t *ph,
        osal_mem_header_t *oh
    )
{
    osal_mutex_lock(pool->lock);
    osal_assert(0 < ph->ref_count);
    osal_assert(ph->ref_count <= MAX_REF_COUNT);
    ph->ref_count--;
    if (0 < ph->ref_count) {
        osal_mutex_unlock(pool->lock);
        return;
    }
    mark_free(ph);
    list_move(&ph->link, &pool->free_list);
    osal_mutex_unlock(pool->lock);
    osal_sem_post(pool->free_sem);
}

static void pool_free(void *vph, osal_mem_header_t *oh)
{
    pool_header_t *ph;

    if (!vph) {
        mmsdbg(DL_ERROR, "NULL buffer handle");
        return;
    }

    ph = vph;

    if (ph->magic != MAGIC_ALLOCED) {
        mmsdbg(DL_ERROR, "Invalid ALLOCED magic: 0x%x!", ph->magic);
        return;
    }

    _pool_free(ph->pool, ph, oh);
}

/*
 * ****************************************************************************
 * ** Lock ********************************************************************
 * ****************************************************************************
 */
static void pool_lock(void *vph, osal_mem_header_t *oh)
{
    pool_t *pool;
    pool_header_t *ph;

    if (!vph) {
        mmsdbg(DL_ERROR, "NULL buffer!");
        return;
    }

    ph = vph;

    if (ph->magic != MAGIC_ALLOCED) {
        mmsdbg(DL_ERROR, "Invalid ALLOCED magic: 0x%x!", ph->magic);
        return;
    }

    pool = ph->pool;

    osal_mutex_lock(pool->lock);
    osal_assert(0 < ph->ref_count);
    osal_assert(ph->ref_count < MAX_REF_COUNT);
    ph->ref_count++;
    osal_mutex_unlock(pool->lock);
}

/*
 * ****************************************************************************
 * ** Alloc *******************************************************************
 * ****************************************************************************
 */
static void * _pool_alloc(pool_t *pool)
{
    pool_header_t *ph;

    osal_mutex_lock(pool->lock);
    /* TODO: assert(!list_empty(&pool->free)); */
    ph = list_entry(pool->free_list.next, pool_header_t, link);
    list_move(&ph->link, &pool->in_use_list);
    osal_mutex_unlock(pool->lock);

    if (ph->magic != MAGIC_FREE) {
        mmsdbg(DL_ERROR, "Invalid FREE magic: 0x%x!", ph->magic);
    }
    mark_alloced(ph);
    ph->ref_count = 1;

    return osal_mem_oh_to_client(ph_to_oh(ph));
}

void * pool_alloc(pool_t *pool)
{
    osal_sem_wait(pool->free_sem);
    return _pool_alloc(pool);
}

void * pool_try_alloc(pool_t *pool)
{
    if (!osal_sem_try_wait(pool->free_sem)) {
        return _pool_alloc(pool);
    }
    return NULL;
}

void * pool_alloc_timeout(pool_t *pool, uint32 ms)
{
    if (!osal_sem_wait_timeout(pool->free_sem, ms)) {
        return _pool_alloc(pool);
    }
    mmsdbg(DL_WARNING, "Pool (%s) alloc timeout!", pool->name);
    return NULL;
}

/*
 * ****************************************************************************
 * ** Create/Destroy **********************************************************
 * ****************************************************************************
 */
void pool_destroy(pool_t *pool)
{
    if (!list_empty_careful(&pool->in_use_list)) {
        mmsdbg(
                DL_FATAL,
                "Poll (%s) destroy was called with still active buffers!",
                pool->name
            );
    }
    osal_sem_destroy(pool->free_sem);
    osal_mutex_destroy(pool->lock);
    osal_free(pool->storage);
    osal_free(pool);
}

pool_t * pool_create(
        char *pool_name,
        unsigned int elem_size,
        unsigned int elems_count
    )
{
    pool_t *pool;
    char *p;
    int i;

    pool = osal_malloc(sizeof (*pool));
    if (!pool) {
        mmsdbg(
                DL_ERROR,
                "Failed to allocate new pool (%s) instance: size=%d!",
                pool_name,
                sizeof (*pool)
            );
        goto exit1;
    }

    INIT_LIST_HEAD(&pool->free_list);
    INIT_LIST_HEAD(&pool->in_use_list);

    pool->name = pool_name;
    pool->client_elem_size = elem_size;
    pool->elems_count = elems_count;

    pool->pool_buf_hdr_size = HEADER_SIZE;
    pool->osal_hdr_size = OSAL_MEM_HEADER_SIZE;

    pool->elem_full_size =
              pool->client_elem_size
            + pool->pool_buf_hdr_size
            + pool->osal_hdr_size;

    pool->storage = osal_malloc(
            pool->elem_full_size * pool->elems_count
        );
    if (!pool->storage) {
        mmsdbg(
                DL_ERROR,
                "Failed to allocate pool (%s) storage: size=%d!",
                pool_name,
                pool->elem_full_size * pool->elems_count
            );
        goto exit2;
    }

    pool->lock = osal_mutex_create();
    if (!pool->lock) {
        mmsdbg(
                DL_ERROR,
                "Failed to create mutex for pool (%s) instance!",
                pool_name
            );
        goto exit3;
    }

    pool->free_sem = osal_sem_create(pool->elems_count);
    if (!pool->free_sem) {
        mmsdbg(
                DL_ERROR,
                "Failed to create semaphore for pool (%s) instance!",
                pool->name
            );
        goto exit4;
    }

    p = pool->storage;
    for (i = 0; i < pool->elems_count; i++) {
        pool_header_t *ph = (pool_header_t *)p;

        osal_mem_header_populate(
                ph_to_oh(ph),
                pool->client_elem_size,
                p,
                pool_free,
                pool_lock
            );

        ph->magic = MAGIC_FREE;
        ph->pool = pool;
        list_add(&ph->link, &pool->free_list);

        p += pool->elem_full_size;
    }

    return pool;
exit4:
    osal_mutex_destroy(pool->lock);
exit3:
    osal_free(pool->storage);
exit2:
    osal_free(pool);
exit1:
    return NULL;
}

