/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file ex_pool.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <osal/osal_stdlib.h>
#include <osal/osal_stdtypes.h>
#include <osal/osal_mutex.h>
#include <osal/osal_list.h>
#include <osal/osal_assert.h>
#include <utils/mms_debug.h>

#include <osal/ex_pool.h>

#define OSAL_MEM_PRIVATE
#include <osal/osal_mem_private.h>
#undef OSAL_MEM_PRIVATE

mmsdbg_define_variable(
        vdl_ex_pool,
        DL_DEFAULT,
        0,
        "vdl_ex_pool",
        "Provides complex pool allocations."
    );
#define MMSDEBUGLEVEL mmsdbg_use_variable(vdl_ex_pool)

/* --------------------------------- build parameters -----------------------------*/
//#define __EX_POOL_DEBUG__
#define __EX_POOL_DESCRIPTION_DEPTH__       8
#define __EX_POOL_HDR_ALIGNMENT__           16
#define __EX_POOL_BUFFER_ALIGNMENT__        4
/* --------------------------------------------------------------------------------*/


#define __EX_POOL_HDR_ALIGNMENT_MASK__   (__EX_POOL_HDR_ALIGNMENT__ -1)

#define EX_POOL_HDR_SIZE_ROUND(size)  \
    (((size)+__EX_POOL_HDR_ALIGNMENT_MASK__)&(~__EX_POOL_HDR_ALIGNMENT_MASK__))

#define __EX_POOL_BUFFER_ALIGNMENT_MASK__   (__EX_POOL_HDR_ALIGNMENT__ -1)

#define EX_POOL_BUFFER_SIZE_ROUND(size)  \
    (((size)+__EX_POOL_HDR_ALIGNMENT_MASK__)&(~__EX_POOL_HDR_ALIGNMENT_MASK__))


#define __EX_POOL_MAGIC_ALLOCED__           0x22AAAA22
#define __EX_POOL_MAGIC_FREE__              0x22DDDD22

#define POOL_MAX_REF_COUNT 16

typedef struct {
    unsigned int magic;
    ex_pool_t *pool;
    struct list_head node;
    int ref_count;
}ex_pool_hdr_t;

struct pool {
    struct list_head free_list;
    struct list_head in_use_list;
    osal_mutex *lock;
    osal_sem *free_sem;
    int client_elem_size;
    int elem_full_size;
    int elems_count;
    void * storage;
    int    ex_pool_buf_hdr_size;
    int    osal_hdr_size;
    char  *name;
};

inline int get_ex_pool_hdr_size(void)
{
    return EX_POOL_HDR_SIZE_ROUND(sizeof(ex_pool_hdr_t));
}

inline void* get_ex_pool_hdr_from_osal_ptr(void* posal)
{
uint8 *p = posal;

    p -= get_ex_pool_hdr_size();
    return (void*)p;
}

inline void* get_client_ptr_from_ex_pool_hdr(void* phdr)
{
uint8 *p = phdr;
    p += get_ex_pool_hdr_size() + OSAL_MEM_HEADER_SIZE;
    return (void*)p;
}

inline void* get_osal_ptr_from_ex_pool_hdr(void* phdr)
{
uint8 *p = phdr;
    p += get_ex_pool_hdr_size();
    return (void*)p;
}

inline ex_pool_hdr_t *ex_pool_oh_to_ph(void *oh)
{
    return (ex_pool_hdr_t *)((char *)oh - get_ex_pool_hdr_size());
}

#ifdef __EX_POOL_DEBUG__
#include <osal/osal_string.h>
static int xxx = 1;

#define EX_POOL_SET_BUFFER(addr,val,size) osal_memset(addr, val, EX_POOL_BUFFER_SIZE_ROUND(size));

static void _size_traverse (ex_pool_node_t * node, int* total_size, int level )
{
    char lev[30];

    if (level > __EX_POOL_DESCRIPTION_DEPTH__) {
        mmsdbg(DL_ERROR, "EX_POOL Description CRITICAL ERROR!!!.\n"
                           "Potential loop in node list description!!!", level);
        return;
    }
    while ((node->size) && (node->offset >= 0))
    {
        osal_memset(lev, ' ', 4*level);
        lev[4*level] = 0;
        mmsdbg(DL_PRINT, "%s Process V Node -> %p with offs = %d next = %p size = %d rounded to %d",
               lev, node, node->offset, node->next, node->size, EX_POOL_BUFFER_SIZE_ROUND(node->size));

        *total_size += EX_POOL_BUFFER_SIZE_ROUND(node->size);
        if (node->next) {
            mmsdbg(DL_PRINT, "%s Will Explore H Node next -> %p", lev, node->next);
            _size_traverse(node->next, total_size, level+1);
        } else {
            mmsdbg(DL_PRINT, "%s ------- List Terminated --------", lev);
        }
        node++;
    }
}

int ex_pool_calc_element_total_size(ex_pool_node_t * root)
{
    int total_size;
    mmsdbg(DL_ERROR, "ROOT at -> %p with size %d rounded to %d", root,
                       root->size, EX_POOL_BUFFER_SIZE_ROUND(root->size));

    mmsdbg(DL_PRINT, "Root next -> %p", root->next);

    total_size = EX_POOL_BUFFER_SIZE_ROUND(root->size);
    _size_traverse (root->next, &total_size, 1);

    mmsdbg(DL_PRINT, "Total size = %d rounded to %d", total_size,
           EX_POOL_BUFFER_SIZE_ROUND(total_size));

    return EX_POOL_BUFFER_SIZE_ROUND(total_size);
}

#else

#define EX_POOL_SET_BUFFER(addr, val, size)

static void _size_traverse (ex_pool_node_t * node, int* total_size, int level )
{

    if (level > __EX_POOL_DESCRIPTION_DEPTH__) {
        mmsdbg(DL_ERROR, "EX_POOL Description CRITICAL ERROR!!!.\n"
                           "Potential loop in node list description!!!", level);
        return;
    }
    while ((node->size) && (node->offset >= 0))
    {
        *total_size += EX_POOL_BUFFER_SIZE_ROUND(node->size);
        if (node->next)
            _size_traverse(node->next, total_size, level+1);
        node++;
    }
}

static int ex_pool_calc_element_total_size(ex_pool_node_t *const root)
{
    int total_size;

    total_size = EX_POOL_BUFFER_SIZE_ROUND(root->size);
    _size_traverse(root->next, &total_size, 1);
    return EX_POOL_BUFFER_SIZE_ROUND(total_size);
}

#endif


static void* _set_pointers_traverse (ex_pool_node_t * node, int size, void* addr)
{
    uint8 *p_next;
    uint8 *p_node_base;
    uint8 *p_link;

    p_next = p_node_base = addr;
    EX_POOL_SET_BUFFER(p_next, xxx++, size);
    p_next += EX_POOL_BUFFER_SIZE_ROUND(size);
    while ((node->size) && (node->offset >= 0))
    {
        p_link = p_node_base + node->offset; // advance to pointer location in current element
        *((void**)p_link) = p_next; // update pointer inside current node to point next element (buffer)
        if (node->next)
            p_next = _set_pointers_traverse(node->next, node->size, p_next);
        else {
            EX_POOL_SET_BUFFER(p_next, xxx++, node->size);
            p_next += EX_POOL_BUFFER_SIZE_ROUND(node->size);   // reserve space for next element (buffer)
        }

        node++;
    }
    return p_next;
}

static void* ex_pool_set_element_pointers(ex_pool_node_t *const root, void* base_address)
{
    uint8 *p_next;
#ifdef __EX_POOL_DEBUG__
    xxx = 1;
#endif
    p_next = base_address;
    p_next += EX_POOL_BUFFER_SIZE_ROUND(root->size);   // reserve space for root element
    return _set_pointers_traverse(root->next, root->size, base_address);
}




/* ========================================================================== */
/**
* __ex_pool_alloc()
*/
/* ========================================================================== */
static void * __ex_pool_alloc(ex_pool_t *pool)
{
    ex_pool_hdr_t *ph;

    osal_mutex_lock(pool->lock);
    /* TODO: assert(!list_empty(&pool->free)); */

    ph = list_entry(pool->free_list.next, ex_pool_hdr_t, node);

    osal_mem_mark_alloced(get_osal_ptr_from_ex_pool_hdr(ph));

    list_move(pool->free_list.next, &pool->in_use_list);
    osal_mutex_unlock(pool->lock);
    if (ph->magic != __EX_POOL_MAGIC_FREE__)
    {
        mmsdbg(DL_ERROR, "POOL (%s) internal error - magic = 0x%x", pool->name, ph->magic);
    }
    ph->magic = __EX_POOL_MAGIC_ALLOCED__;
    ph->ref_count = 1;

    return get_client_ptr_from_ex_pool_hdr(ph);
}

/* ========================================================================== */
/**
* ex_pool_alloc()
*/
/* ========================================================================== */
void * ex_pool_alloc(ex_pool_t *pool)
{
    osal_sem_wait(pool->free_sem);
    return __ex_pool_alloc(pool);
}

/* ========================================================================== */
/**
* ex_pool_try_alloc()
*/
/* ========================================================================== */
void * ex_pool_try_alloc(ex_pool_t *pool)
{
    if (!osal_sem_try_wait(pool->free_sem)) {
        return __ex_pool_alloc(pool);
    }
    return NULL;
}

/* ========================================================================== */
/**
* ex_pool_alloc_timeout()
*/
/* ========================================================================== */
void * ex_pool_alloc_timeout(ex_pool_t *pool, uint32 ms)
{
    if (!osal_sem_wait_timeout(pool->free_sem, ms)) {
        return __ex_pool_alloc(pool);
    }
    mmsdbg(DL_WARNING, "ex_pool %s alloc timeout", pool->name);
    return NULL;
}

static inline void __ex_pool_free(ex_pool_t *pool, ex_pool_hdr_t *ph, void *oh)
{
    osal_mutex_lock(pool->lock);
    osal_assert(0 < ph->ref_count);
    osal_assert(ph->ref_count <= POOL_MAX_REF_COUNT);
    ph->ref_count--;
    if (0 < ph->ref_count) {
        osal_mutex_unlock(pool->lock);
        return;
    }
    ph->magic = __EX_POOL_MAGIC_FREE__;
    osal_mem_mark_free(oh);
    list_move(&ph->node,&pool->free_list);
    osal_mutex_unlock(pool->lock);
    osal_sem_post(pool->free_sem);
}

/* ========================================================================== */
/**
* ex_pool_free()
*/
/* ========================================================================== */
static void ex_pool_free(void *vph, osal_mem_header_t *oh)
{
ex_pool_hdr_t *ph;

    ph = vph; //get_ex_pool_hdr_from_osal_ptr(buf);
    if (!ph) {
        mmsdbg(DL_ERROR,"EX_POOL NULL buffer handle");
        return;
    }
    if (ph->magic != __EX_POOL_MAGIC_ALLOCED__) {
        mmsdbg(DL_ERROR,"EX_POOL invalid magic %0x%x", ph->magic);
        return;
    }

    __ex_pool_free(ph->pool, ph, oh);
}

/* ========================================================================== */
/**
* ex_pool_lock()
*/
/* ========================================================================== */
static void ex_pool_lock(void *vph, osal_mem_header_t *oh)
{
    ex_pool_t *pool;
    ex_pool_hdr_t *ph;

    if (!vph) {
        mmsdbg(DL_ERROR, "POOL NULL buffer!");
        return;
    }

    ph = vph;

    if (ph->magic != __EX_POOL_MAGIC_ALLOCED__) {
        mmsdbg(DL_ERROR,"POOL invalid magic %0x%x", ph->magic);
        return;
    }

    pool = ph->pool;

    osal_mutex_lock(pool->lock);
    osal_assert(0 < ph->ref_count);
    osal_assert(ph->ref_count < POOL_MAX_REF_COUNT);
    ph->ref_count++;
    osal_mutex_unlock(pool->lock);
}

/* ========================================================================== */
/**
* ex_pool_destroy()
*/
/* ========================================================================== */
void ex_pool_destroy(ex_pool_t *pool)
{
    if (!list_empty_careful(&pool->in_use_list)) {
        mmsdbg(DL_FATAL, "CRITICAL ERROR - poll (%s) destroy was called with still active buffers", pool->name);
    }
    osal_sem_destroy(pool->free_sem);
    osal_mutex_destroy(pool->lock);
    osal_free(pool->storage);
    osal_free(pool);
}

/* ========================================================================== */
/**
* ex_pool_create()
*/
/* ========================================================================== */
ex_pool_t * ex_pool_create(char *pool_name,
                           ex_pool_node_t *const root_descriptor,
                           unsigned int elems_count)
{
    ex_pool_t *pool;
    char *p;
    int i;

    pool = osal_malloc(sizeof (*pool));
    if (!pool) {
        mmsdbg(
                DL_ERROR,
                "Failed to allocate new pool instance: size=%d!",
                sizeof (*pool)
            );
        goto exit1;
    }

    INIT_LIST_HEAD(&pool->free_list);
    INIT_LIST_HEAD(&pool->in_use_list);

    pool->name = pool_name;
    pool->client_elem_size = ex_pool_calc_element_total_size(root_descriptor);
    pool->elems_count = elems_count;

    pool->ex_pool_buf_hdr_size = get_ex_pool_hdr_size();
    pool->osal_hdr_size      = OSAL_MEM_HEADER_SIZE;

    pool->elem_full_size =   pool->client_elem_size +
                                pool->ex_pool_buf_hdr_size +
                                pool->osal_hdr_size;

    pool->storage = osal_calloc(pool->elems_count, pool->elem_full_size);
    if (!pool->storage) {
        mmsdbg(
                DL_ERROR,
                "Failed to allocate pool (%s) storage: size=%d!",
                pool_name,
                pool->elem_full_size * pool->elems_count
            );
        goto exit2;
    }

    pool->lock = osal_mutex_create();
    if (!pool->lock) {
        mmsdbg(
                DL_ERROR,
                "Failed to create mutex for pool instance!"
            );
        goto exit3;
    }

    pool->free_sem = osal_sem_create(pool->elems_count);
    if (!pool->free_sem) {
        mmsdbg(
                DL_ERROR,
                "Failed to create semaphore for pool instance!"
            );
        goto exit4;
    }

    p = pool->storage;
    for (i = 0; i < pool->elems_count; i++) {
        ex_pool_hdr_t *ph = (ex_pool_hdr_t *)p;

        osal_mem_header_populate(get_osal_ptr_from_ex_pool_hdr(ph),
                                 pool->client_elem_size, p, ex_pool_free, ex_pool_lock);

        ex_pool_set_element_pointers(root_descriptor, get_client_ptr_from_ex_pool_hdr(ph));

        ph->magic = __EX_POOL_MAGIC_FREE__;
        ph->pool = pool;
        list_add(&ph->node, &pool->free_list);

        p += pool->elem_full_size;
    }

    return pool;
exit4:
    osal_mutex_destroy(pool->lock);
exit3:
    osal_free(pool->storage);
exit2:
    osal_free(pool);
exit1:
    return NULL;
}

