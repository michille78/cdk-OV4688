/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file osal_time.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <osal/osal_stdtypes.h>
#include <osal/osal_time.h>

#include <utils/mms_debug.h>

#include <unistd.h>
#include <time.h>       /* struct timeval */
#include <sys/time.h>   /* gettimeofday() */

mmsdbg_define_variable(vdl_osal_time, DL_DEFAULT, 0,
    "osaltime", "OSAL Time");
#define MMSDEBUGLEVEL   vdl_osal_time

int osal_time_add(osal_timeval *time, osal_timeval *x, osal_timeval *y)
{
    *time = *x + *y;

    return 0;
}

int osal_time_sub(osal_timeval *time, osal_timeval *x, osal_timeval *y)
{
    *time = *x - *y;
    return 0;
}

int osal_time_inc(osal_timeval *time, int64 usec)
{
    *time += usec;

    return 0;
}

int osal_get_time(osal_timeval *time)
{
    struct timeval tv;
    int ret;

    ret = gettimeofday(&tv, NULL);
    *time = (osal_timeval)tv.tv_sec * 1000000 + tv.tv_usec;

    return ret;
}

int osal_get_timeval(osal_timeval *time)
{
    return osal_get_time(time);
}

int osal_nsleep(unsigned int nsecs)
{
    int                 ret;
    struct timespec     time_val;

    time_val.tv_sec = 0;
    time_val.tv_nsec = nsecs;

    ret = nanosleep(&time_val, NULL);

    return ret;
}

int osal_sleep(unsigned int secs)
{
    sleep(secs);
    return 0;
}

int osal_usleep(unsigned int usecs)
{
    int                 ret;
    struct timespec     time_val;

    time_val.tv_sec  = usecs/1000000;
    time_val.tv_nsec = usecs%1000000 * 1000;

    ret = nanosleep(&time_val, NULL);

    return ret;
}
