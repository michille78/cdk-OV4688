/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file osal_process.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <osal/osal_stdlib.h>
#include <osal/osal_sysdep.h>
#include <osal/osal_process.h>

#include <utils/mms_debug.h>

#include <unistd.h>

mmsdbg_define_variable(vdl_osal_process, DL_DEFAULT, 0,
    "osalproc", "OSAL Process");
#define MMSDEBUGLEVEL   vdl_osal_process

struct osal_process {
    int pid;
};

/* ========================================================================== */
/**
*  osal_process_create()    Function should cerate new process and run it.
*
*  @param   info - struct osal_process_cntx * - handles to newly created process to be used
*           in osal_process_Wait().
*
*  @param   name - const char * - application name "c:\Windows\cmd.exe"
*
*  @param   cmd_line - const char * - application command line
*
*  @return  non-zero on error.
*/
/* ========================================================================== */
struct osal_process *osal_process_create(const char *name,
    const char *cmd_line)
{
    struct osal_process *ctx = NULL;
    int pid, ret;

    ctx = osal_malloc(sizeof(*ctx));
    if (!ctx) {
        return NULL;
    }

    pid = fork();

    switch (pid) {
        case 0:
            ctx->pid = pid;
            ret = execl(name, cmd_line, NULL);
            break;
        case -1:
            ret = -1;
            break;              // error

        default:
            mmsdbg(DL_MESSAGE, "3A process ID %d", pid);
            ret = 0;
            break;
    }

    return ctx;
}


/* ========================================================================== */
/**
*  osal_process_Exit()    wriapper to syscall 'exit'
*
*  @param   status - int - exit status
*
*  @return  never.
*/
/* ========================================================================== */
void osal_process_exit(struct osal_process *ctx, int status)
{
    _exit(status);
}

/* ========================================================================== */
/**
*  osal_process_Wait()    Windows implementation for Linux wait syscall.
*
*  @param   info - struct osal_process_cntx * - handles returned from create_process()
*
*  @return  non-zero on error
*/
/* ========================================================================== */
int osal_process_wait(struct osal_process *ctx)
{
    int               status = 0;
    pid_t             process;

    process = waitpid(ctx->pid, &status, 0);

    return status;
}
