/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file osal_malloc.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#define _GNU_SOURCE
#include <osal/osal_stdlib.h>
#include <osal/osal_stdio.h>

#define OSAL_MEM_PRIVATE
#include <osal/osal_mem_private.h>
#undef OSAL_MEM_PRIVATE

#include <utils/mms_debug.h>

mmsdbg_define_variable(vdl_osal_malloc, DL_DEFAULT, 0,
    "osal_malloc", "OSAL malloc");
#define MMSDEBUGLEVEL   vdl_osal_malloc

/* ========================================================================== */
/**
*  _osal_free()    Simple wrapper.
*
*  @param   p - pointer to memory to be freed. Should be obtained with osal_malloc
*
*  @return  None
*
*/
/* ========================================================================== */
static void _osal_free(void *vph, osal_mem_header_t *oh)
{
    osal_mem_mark_free(oh);
    osal_clean_free(vph);
}

/* ========================================================================== */
/**
*  osal_malloc()    Simple wrapper. Allocates extra memory for osal memory management
*
*  @param   size - original client request
*
*  @return  Client pointer to allocated memory. NULL in case of error
*/
/* ========================================================================== */
void* osal_malloc(size_t size)
{
    osal_mem_header_t *oh;

    size += OSAL_MEM_HEADER_SIZE;

    oh = osal_clean_malloc(size);
    if (!oh) {
        return NULL;
    }

    osal_mem_header_populate(oh, size, oh, _osal_free, NULL);
    return osal_mem_oh_to_client(oh);
}

/* ========================================================================== */
/**
*  osal_free()    Simple wrapper.
*
*  @param   p - pointer to memory to be freed. Should be obtained with osal_malloc
*
*  @return  None
*
*/
/* ========================================================================== */
void osal_free(void *p)
{
    osal_mem_header_t *oh;

    if (!p) {
        mmsdbg(DL_ERROR, "osal_free called with NULL pointer");
        return;
    }

    oh = osal_mem_client_to_oh(p);

    if ((OSAL_MEM_HEADER_MAGIC_ALLOCATED != oh->magic) || (!oh->free))
    {
        mmsdbg(DL_ERROR, "osal_free called with Invalid pointer %p", p);
        mmsdbg(DL_ERROR, "magic = 0x%x free_rotine = %p", oh->magic, oh->free);
        return;
    }
    oh->free(oh->block_address, oh);
}

void osal_mem_lock(void *p)
{
    osal_mem_header_t *oh;

    if (!p) {
        mmsdbg(DL_ERROR, "NULL pointer");
        return;
    }

    oh = osal_mem_client_to_oh(p);

    if (OSAL_MEM_HEADER_MAGIC_ALLOCATED != oh->magic) {
        mmsdbg(
                DL_ERROR,
                "Invalid pointer %p: magic=0x%x!",
                p,
                oh->magic
            );
        return;
    }

    if (!oh->lock) {
        mmsdbg(
                DL_ERROR,
                "Invalid pointer %p is not lockable!",
                p
            );
        return;
    }

    oh->lock(oh->block_address, oh);
}

/**
 * This function allocates a block long enough to contain a vector of count
 * elements, each of size eltsize. Its contents are cleared to zero before
 * calloc returns.
 *
 */
void * osal_calloc(size_t nelem, size_t elsize)
{
    size_t  size = nelem * elsize;
    void * allocation;

    allocation = osal_malloc(size);

    if (allocation)
        memset(allocation, 0, size);

    return allocation;
}

/**
 * The memalign function allocates a block of size bytes whose address is
 * a multiple of boundary. The boundary must be a power of two! The function
 * memalign works by allocating a somewhat larger block, and then returning an
 * address within the block that is on the specified boundary.
 *
 */
void * osal_memalign (size_t boundary, size_t size)
{
    uint8 * p;
    osal_mem_header_t *oh;
    unsigned int extra_bytes;

    extra_bytes = boundary + OSAL_MEM_HEADER_SIZE;
    size += extra_bytes;

    p = osal_clean_malloc(size);
    if (!p) {
        return NULL;
    }

    oh = (osal_mem_header_t *)((size_t)(p + extra_bytes) & (~(boundary-1)));

    oh--;

    osal_mem_header_populate(oh, size, p, _osal_free, NULL);
    return osal_mem_oh_to_client(oh);
}

