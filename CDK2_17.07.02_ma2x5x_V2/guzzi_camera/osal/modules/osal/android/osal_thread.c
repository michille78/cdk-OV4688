/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file osal_thread.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#define _GNU_SOURCE

#include <osal/osal_sysdep.h>               // pipe(), read() ... (all from unistd.h)
#include <osal/osal_stdlib.h>
#include <osal/osal_stdio.h>
#include <osal/osal_string.h>
#include <osal/osal_thread.h>

#include <utils/mms_debug.h>

#include <pthread.h>

mmsdbg_define_variable(vdl_osal_thread, DL_DEFAULT, 0,
    "osalthrd", "OSAL Threads");
#define MMSDEBUGLEVEL   vdl_osal_thread

struct osal_thread {
    // Argument passed to thread func
    void *priv;

    // Thread func
    osal_thread_routine *cb_routine;

    // Custom func called to terminate thread. If NULL the regular OS termination is used
    osal_thread_done *cb_done;

    // child attrib data (keep it until child is alive)
    pthread_attr_t attr;

    // keep thread ID to join at the end
    pthread_t hThread;
};

static pthread_key_t key_ctx;

static void * osal_thread_entry_point(void *arg)
{
    struct osal_thread *ctx;
    int err;

    ctx = arg;

    err = pthread_setspecific(key_ctx, ctx);
    if (err) {
        mmsdbg(DL_ERROR, "Failed to set context key!");
        goto exit1;
    }

    return ctx->cb_routine(ctx->priv);
exit1:
    return NULL;
}

int osal_thread_init(void)
{
    int err;

    err = pthread_key_create(&key_ctx, NULL);
    if (err) {
        mmsdbg(DL_ERROR, "Failed to create context key!")
        goto exit1;
    }

    return 0;
exit1:
    return -1;
}

void osal_thread_exit(void)
{
    int err;
    err = pthread_key_delete(key_ctx);
    if (err) {
        mmsdbg(DL_ERROR, "Failed to delete context key!");
    }
}

/* ========================================================================== */
/**
*  osal_thread_create()    Simple wrapper, which will clreate one POSIX
*           thread.
*
*  @param   child - struct osal_thread * - sructure to hold various
*           new thread parameters
*
*  @param   arg - data to be passed as argument to new thread.
*
*  @param   threadProc - Thread entry point.
*
*  @param   threadDone - Function for propriety closing of this thread
*
*  @return  non-zero on error.
*/
/* ========================================================================== */
osal_thread *osal_thread_create(const char *name, void *priv,
    osal_thread_routine *cb_routine, osal_thread_done *cb_done,
    int flags, int stack_size)
{
    struct osal_thread *ctx;
    size_t stack_size_default;
    int ret = 0;

    if (!cb_routine) {
        return NULL;
    }

    ctx = osal_malloc(sizeof(*ctx));
    if (!ctx) {
        return NULL;
    }

    ctx->priv = priv;
    ctx->cb_routine = cb_routine;
    ctx->cb_done = cb_done;

    ret = pthread_attr_init(&ctx->attr);
    if (ret) {
        goto exit1;
    }

    if (stack_size <= 0) {
        stack_size = 128 * 1024;
    } else {
        ret = pthread_attr_getstacksize(&ctx->attr, &stack_size_default);
        if (ret) {
            mmsdbg(DL_ERROR, "Failed to obtain the default stack size!");
            goto exit2;
        }
        if (stack_size_default < (size_t)stack_size) {
            stack_size = ALIGN(stack_size, 4*1024);
        } else {
            stack_size = stack_size_default;
        }
    }

    ret = pthread_attr_setstacksize(&ctx->attr, stack_size);
    if (ret) {
        mmsdbg(DL_ERROR, "Failed to set stack size!");
        goto exit2;
    }

    if (flags & OSAL_THR_FLAG_SCHED_FIFO) {
        pthread_attr_setschedpolicy(&ctx->attr, SCHED_FIFO);
    }

    ret = pthread_create(&ctx->hThread, &ctx->attr, osal_thread_entry_point, ctx);
    if (ret) {
        goto exit2;
    }

#ifdef pthread_setname_np
    pthread_setname_np(ctx->hThread, name);
#endif

exit2:
    if (ret) {
        (void)pthread_attr_destroy(&ctx->attr);

        mmsdbg(DL_ERROR, "Create thread");
    }

exit1:
    if (ret) {
        osal_free(ctx);
        ctx = NULL;
    }

    return ctx;

}

/* ========================================================================== */
/**
*  osal_thread_destroy()    destroy thread previsiously connected from
*           osal_thread_create.
*
*  @param   child - struct osal_thread * - sructure with parameters of thread
*           to destroy.
*
*  @return  non-zero on error.
*/
/* ========================================================================== */
int osal_thread_destroy(struct osal_thread *ctx)
{
    int err = 0;

    if (!ctx) {
        return -1;
    }

    if (ctx->cb_done) {
        mmsdbg(DL_MESSAGE, "Stopping thread");
        ctx->cb_done(ctx->priv);
    }

    mmsdbg(DL_MESSAGE, "Joining thread");

    err = pthread_join(ctx->hThread, NULL);
    if (err) {
        mmsdbg(DL_ERROR, "Error while join reader");
    }

    (void)pthread_attr_destroy(&ctx->attr);

    osal_free(ctx);

    return err;
}

int osal_thread_priority(struct osal_thread *ctx, uint32 priority)
{
    mmsdbg(DL_WARNING, "Not implemented: priority = %d\n", priority);
    return 0;
}

struct osal_thread * osal_thread_self(void)
{
    return pthread_getspecific(key_ctx);
}
