/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file osal_mutex.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <osal/osal_stdtypes.h>
#include <osal/osal_stdlib.h>
#include <osal/osal_mutex.h>

#include <utils/mms_debug.h>

#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <semaphore.h>
#include <sys/time.h>

mmsdbg_define_variable(vdl_osal_mutex, DL_DEFAULT, 0,
    "osalmutex", "OSAL Mutex");
#define MMSDEBUGLEVEL   vdl_osal_mutex

struct osal_mutex {
    pthread_mutex_t m;
};

struct osal_sem {
    sem_t s;
};

struct osal_cond {
    pthread_cond_t c;
};


osal_mutex *osal_mutex_create(void)
{
    struct osal_mutex *mutex;

    mutex = osal_calloc(1, sizeof(*mutex));
    if (!mutex) {
        return mutex;
    }

    pthread_mutex_init(&mutex->m, NULL);

    return mutex;
}

void osal_mutex_destroy(osal_mutex *mutex)
{
    pthread_mutex_destroy(&mutex->m);
    osal_free(mutex);
}

int osal_mutex_lock(osal_mutex *mutex)
{
    return pthread_mutex_lock(&mutex->m);
}

int osal_mutex_lock_timeout(osal_mutex *mutex, uint32 ms)
{
    struct timespec abs_to;
    struct timeval now;
    uint64 us;

    gettimeofday(&now, NULL);

    us = (uint64)now.tv_sec * 1000000 + (uint64)now.tv_usec + (uint64)ms * 1000;

    abs_to.tv_sec = us / 1000000;
    abs_to.tv_nsec = (us % 1000000) * 1000;

    return pthread_mutex_timedlock(&mutex->m, &abs_to);
}

int osal_mutex_trylock(osal_mutex *mutex)
{
    return pthread_mutex_trylock(&mutex->m);
}

int osal_mutex_unlock(osal_mutex *mutex)
{
    return pthread_mutex_unlock(&mutex->m);
}

/* ==========================================================================
 *                                OSAL SEMAPHORES
 * ========================================================================= */

osal_sem *osal_sem_create(uint32 init_value)
{
    struct osal_sem *sem;

    sem = osal_calloc(1, sizeof(*sem));
    if (!sem) {
        return sem;
    }

    sem_init(&sem->s, 0, init_value);

    return sem;
}

void osal_sem_destroy(osal_sem *sem)
{
    sem_destroy(&sem->s);
    osal_free(sem);
}

int osal_sem_init(osal_sem *sem, uint value)
{
    return sem_init(&sem->s, 0, value);
}

int osal_sem_wait(osal_sem *sem)
{
    return sem_wait(&sem->s);
}

int osal_sem_try_wait(osal_sem *sem)
{
    return sem_trywait(&sem->s);
}

int osal_sem_wait_timeout(osal_sem *sem, uint32 ms)
{
    struct timespec abs_to;
    struct timeval now;
    uint64 us;

    gettimeofday(&now, NULL);

    us = (uint64)now.tv_sec * 1000000 + (uint64)now.tv_usec + (uint64)ms * 1000;

    abs_to.tv_sec = us / 1000000;
    abs_to.tv_nsec = (us % 1000000) * 1000;

    return sem_timedwait(&sem->s, &abs_to);
}

int osal_sem_post(osal_sem *sem)
{
    return sem_post(&sem->s);
}

uint32 osal_sem_value(osal_sem *sem)
{
    int val;

    if (!sem_getvalue(&sem->s, &val))
        return val;
    else
        return -1;
}

/* ==========================================================================
 *                                OSAL Condition
 * ========================================================================= */

osal_cond *osal_cond_create(void)
{
    struct osal_cond *cond;

    cond = osal_calloc(1, sizeof(*cond));
    if (!cond) {
        return cond;
    }

    pthread_cond_init(&cond->c, NULL);

    return cond;
}

void osal_cond_destroy(osal_cond *cond)
{
    pthread_cond_destroy(&cond->c);
    osal_free(cond);
}

int osal_cond_signal(osal_cond *cond)
{
    return pthread_cond_signal(&cond->c);
}

int osal_cond_broadcast(osal_cond *cond)
{
    return pthread_cond_broadcast(&cond->c);
}

int osal_cond_wait(osal_cond *cond, osal_mutex *mutex)
{
    return pthread_cond_wait(&cond->c, &mutex->m);
}

int osal_cond_wait_timeout(osal_cond *cond, osal_mutex *mutex, uint32 ms)
{
    mmsdbg(DL_ERROR, "osal_cond_wait_timeout: Not implemented");
    return -1;
}

