/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file osal_mb_mqueue.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#define _XOPEN_SOURCE 600

#include <osal/osal_mailbox.h>
#include <osal/osal_string.h>
#include <osal/osal_time.h>

#include <utils/mms_debug.h>

mmsdbg_define_variable(vdl_osal_process_msg, DL_DEFAULT, 0,
    "osalmsg", "OSAL Interprocess Message");
#define MMSDEBUGLEVEL   vdl_osal_process_msg

/* ========================================================================== */
/**
*  osal_mailbox_create()    Open "q->name" for sending and receiving. The queue file
*           permissions are set rw for owner and nothing for group/others.
*
*  @param   q - struct osal_mailbox * - Q name.
*
*  @return  non-zero on error.
*/
/* ========================================================================== */
int32 osal_mailbox_create(struct osal_mailbox *q,
                         const char* q_name,
                         uint32 payload_size, uint32 queue_count)
{
    mqd_t             ds;
    struct mq_attr    attr;
    int               ret = 0;

    if (!q) {
        return -1;
    }

    osal_strncpy(q->name, q_name, FW_MBOX_NAME_MAX_SIZE);

    //
    // Attributes for our queue. They can be set only during
    // creating.
    //
    attr.mq_maxmsg  = queue_count;   // max. number of messages in queue at the
                                     //   same time
    attr.mq_msgsize = sizeof(struct osal_message_header)+payload_size; // max. message size
    //
    // Create a new queue named "/my_queue" and open it for sending
    // and receiving. The queue file permissions are set rw for
    // owner and nothing for group/others. Queue limits set to
    // values provided above.
    //
    if ((ds = mq_open((char*)&q->name[0], O_CREAT | O_RDWR, 0777, &attr)) == (mqd_t) - 1) {
        mmsdbg(DL_ERROR, "Error Creating queue %s : %s",
               q->name[0] ? q->name : "null", strerror(errno));
        ret = -1;
    }

    q->fd = ds;
    q->payload_size = payload_size;
    q->message_size = sizeof(struct osal_message_header)+payload_size;
    q->queue_count  = queue_count;

    return ret;
}


/* ========================================================================== */
/**
*  osal_mailbox_close()    close a message queue descriptor
*
*  @param   q - struct osal_mailbox * - what to close
*
*  @return  non-zero on error.
*/
/* ========================================================================== */
int32 osal_mailbox_close(struct osal_mailbox * q)
{
    int ret;

    if (!q) {
        return -1;
    }

    ret = mq_close(q->fd);

    if (0 > ret) {
        mmsdbg(DL_ERROR, "Error Closing queue %s : %s",
               q->name[0] ? q->name : "null", strerror(errno));
    }

    return ret;
}

/* ========================================================================== */
/**
*  osal_mailbox_destroy()    After unlink message queue is
*           removed from system.
*
*  @param   q - struct osal_mailbox * - what to destroy
*
*  @return  non-zero on error.
*/
/* ========================================================================== */
int32 osal_mailbox_destroy(struct osal_mailbox * q)
{
    int               ret = -1;

    if (!q) {
        return ret;
    }

    //
    // ...and finally unlink it. After unlink message queue is
    // removed from system.
    //
    if ((ret = mq_unlink(q->name)) == -1) {
        mmsdbg(DL_ERROR, "Error Unlinking queue %s : %s",
               q->name[0] ? q->name : "null", strerror(errno));

    }

    return ret;
}

/* ========================================================================== */
/**
 *  osal_mailbox_release()    Close and destroy given mailbox.
 *
 *  @param   mbox - struct osal_mailbox * - source mailbox
 *
 *  @param   close_type - enum osal_mailbox_close_type * - how to treat mailbox
 *
 *  OSAL_MAILBOX_CLOSE - Mailbox system structure will continue to exist, but thread that
 *  close the mailbox can not send/receive messages over it. From the other side
 *  if some thread still keep open this mailbox it can read from it.
 *
 *  OSAL_MAILBOX_DESTROY -  Mailbox system structure will be closed first and after that will be
 *  destroyed, NOTE all threads that keep references to this mailbox should call
 *  osal_mailbox_release(x, OSAL_MAILBOX_CLOSE) first
 *
 *  @return  non-zero on error.
 */
/* ========================================================================== */
int32 osal_mailbox_release(struct osal_mailbox * q,
                            enum osal_mailbox_close_type close_type)
{
    int32    ret = 0;

    ret = osal_mailbox_close(q);

    if (OSAL_MAILBOX_DESTROY == close_type)
        ret += osal_mailbox_destroy(q);

    return ret;
}

/* ========================================================================== */
/**
*  osal_mailbox_read()    Read message from mail box queue.
*
*  @param   mbox - struct osal_mailbox * - source mailbox
*
*  @param   cam_msg - struct osal_message * - where to store message
*
*  @return  non-zero on error.
*/
/* ========================================================================== */
int osal_mailbox_read(struct osal_mailbox *ctx,
    union osal_mbox_cmd *cmd, void *payload, int max_size, int timeout)
{
    ssize_t           read_ = 0;
    unsigned          prio;
    int               ret = -1;
    char             *name;

    if (!mbox || !cam_msg) {
        return ret;
    }

    name = mbox->name[0] ? mbox->name : "null";

    read_ = mq_receive(mbox->fd, (char *)cam_msg, mbox->message_size, &prio);

    if (0 > read_) {

        mmsdbg(DL_ERROR, "Error Reading message from %s : %s",
           name ? name : "null", strerror(errno));

    } else if ((size_t) read_ != mbox->message_size) {

        mmsdbg(DL_ERROR, "Message not complete read from %s", name);

    } else
        ret = 0;

    return (ret);

}


/* ========================================================================== */
/**
*  osal_mailbox_readtimed()    Read message from passed mail box Q. If message do not
*           arrive until specifed 'timeout' value in ms this call return -1;
*
*  @param   mbox - struct osal_mailbox * - source mailbox
*
*  @param   cam_msg -  struct osal_message * - where to store message.
*
*  @param   timeout - int - miliseconds, if 0 return immediately.
*
*  @return  non-zero on if there is not readed message.
*/
/* ========================================================================== */
int32 osal_mailbox_readtimed(struct osal_mailbox * mbox,
                                struct osal_message * cam_msg, int32 timeout)
{
    ssize_t           read_ = 0;
    unsigned          prio;
    int               ret = -1;
    char             *name;
    struct timespec   ts;
    struct timeval      tv;
    uint32          nsec;

    if (!mbox || !cam_msg) {
        return ret;
    }

    name = mbox->name[0] ? mbox->name : "null";

    // Specify timeout as msec from now
    (void) gettimeofday(&tv, NULL);

    nsec = (tv.tv_usec + (timeout * 1000)) * 1000;

    ts.tv_sec = tv.tv_sec;

    if (nsec >= (1000 * 1000 * 1000)) {
        nsec -= (1000 * 1000 * 1000);
        ts.tv_sec += 1;
    }

    ts.tv_nsec = nsec;

    // receiving message
    read_ = mq_timedreceive(mbox->fd, (char *)cam_msg,
                            mbox->message_size, &prio, &ts);

    if (0 > read_) {

        mmsdbg(DL_LAYER0, "Error reading message from %s TMO %d : %s", name,
               (int)timeout, strerror(errno));

    } else if ((size_t) read_ != mbox->message_size) {

        mmsdbg(DL_ERROR, "Message not complete read from %s TMO %d",
               name, (int)timeout);

    } else
        ret = 0;

    return (ret);

}


/* ========================================================================== */
/**
*  osal_mailbox_write()    Write message in mail box queue.
*
*  @param   mbox - struct osal_mailbox * - mailbox desination.
*
*  @param   cam_msg - struct osal_message * - message to send
*
*  @return  non-zero on error.
*/
/* ========================================================================== */
int osal_mailbox_write(struct osal_mailbox *ctx,
    union osal_mbox_cmd *cmd, void *payload, int size)
{
    char *name;
    int ret = -1;

    if (!mbox || !cam_msg) {
        return ret;
    }

    name = mbox->name[0] ? mbox->name : "null";

    if ((signed)cam_msg->payloadSize > mbox->payload_size) {
        mmsdbg(DL_ERROR, "Invalid message payload size %d bytes, should be less then or equal %d bytes : %s",
            (int)cam_msg->payloadSize, (int)mbox->payload_size, name);
        return ret;
    }

    ret = (int)mq_send(mbox->fd, (const char *)cam_msg, mbox->message_size, 1);

    if (0 > ret) {

        mmsdbg(DL_ERROR, "Error sending message to %s : %s",
           name ? name : "null", strerror(errno));
    }

    return (ret);

}
