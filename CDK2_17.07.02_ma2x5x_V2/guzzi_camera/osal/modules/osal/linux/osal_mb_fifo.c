/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file osal_mb_fifo.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <osal/osal_sysdep.h>
#include <osal/osal_stdlib.h>
#include <osal/osal_string.h>
#include <osal/osal_mailbox.h>

#include <utils/mms_debug.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/select.h>
#include <sys/time.h>
#include <unistd.h>
#include <poll.h>

mmsdbg_define_variable(vdl_osal_mbox, DL_DEFAULT, 0,
    "osalmbox", "OSAL Interprocess Message");
#define MMSDEBUGLEVEL   vdl_osal_mbox

#define MBOX_NAME_PREFIX "/tmp/osal_mbox_"

struct osal_message_header {
    union osal_mbox_cmd cmd;           /* Command identifier */
    uint16 size;                        /* Paylaod size */
};

struct osal_mailbox {
    char *name;
    int fd;
    int msg_size;
};

static char tmp_buffer[4096];

static void strcat_without_slashes(char *dst, const char *src, int size)
{
    size -= osal_strlen(dst);
    dst += osal_strlen(dst);

    if (size < 1)
        return;

    do {
        *dst++ = (*src == '/') ? '_' : *src;
        size--;
    } while (*++src && size > 1);

    *dst++ = 0;
}

int osal_mailbox_init(void)
{
    return 0;
}

int osal_mailbox_exit(void)
{
    return 0;
}

struct osal_mailbox *osal_mailbox_create(const char *name, uint32 msg_size)
{
    struct osal_mailbox *ctx = NULL;
    int err, len;

    ctx = osal_calloc(1, sizeof(*ctx));
    if (!ctx)
        return NULL;

    len = osal_strlen(MBOX_NAME_PREFIX) + osal_strlen(name) + 2;
    ctx->name = osal_calloc(1, len);
    err = (ctx->name) ? 0 : -1;
    if (err) {
        goto exit;
    }

    osal_strcpy(ctx->name, MBOX_NAME_PREFIX);
    strcat_without_slashes(ctx->name, name, len);

    //
    // Create a new fifo. The fifo file permissions are set rw for
    // owner and nothing for group/others. We are not enforcing
    // queue limits.
    //
    err = mkfifo(ctx->name, 0600);
    if (err) {
        if (errno != EEXIST) {
            mmsdbg(DL_ERROR, "Can't Creating queue %s: %s",
                ctx->name, strerror(errno));
            goto exit;
        } else {
            struct stat fstat;

            stat(ctx->name, &fstat);
            if (!S_ISFIFO(fstat.st_mode)) {
                mmsdbg(DL_ERROR, "Exist file %s is not FIFO", ctx->name);
                goto exit;
            }
            err = 0;
            mmsdbg(DL_LAYER0, "Exist file %s: %s (hopefully created by us)",
                ctx->name, strerror(errno));
        }
    } else {
        mmsdbg(DL_LAYER0, "Created queue %s.", ctx->name);
    }

    //
    // Open the pipe in read/write mode. We cannot enforce reader/writer policies
    // because we don't know when we are called from the reader/writer process.
    //
    ctx->fd = open(ctx->name, O_RDWR);
    if (ctx->fd == -1) {
        mmsdbg(DL_ERROR, "Opening queue %s : %s", ctx->name, strerror(errno));
        err = -1;
        goto exit;
    }

    ctx->msg_size = msg_size;

exit:
    if (err) {
        if (ctx && ctx->name)
            osal_free(ctx->name);

        if (ctx)
            osal_free(ctx);

        ctx = NULL;
    }

    return ctx;
}

int osal_mailbox_destroy(struct osal_mailbox *ctx)
{
    int ret;

    if (!ctx) {
        return -1;
    }

    ret = close(ctx->fd);
    if (ret) {
        mmsdbg(DL_ERROR, "Closing queue %s: %s", ctx->name, strerror(errno));
    }

    ret = unlink(ctx->name);
    if (ret) {
        mmsdbg(DL_LAYER0, "can't unlink %s: %s", ctx->name, strerror(errno));
        ret = 0;
    }

    return ret;
}

int osal_mailbox_write(struct osal_mailbox *ctx,
    union osal_mbox_cmd *cmd, void *payload, int size)
{
    struct osal_message_header hdr;
    size_t wr_size = 0;
    int ret;

    if (!ctx || (!payload && size)) {
        return -1;
    }

    hdr.cmd = *cmd;
    hdr.size = size;

    ret = write(ctx->fd, &hdr, sizeof(hdr));
    if (ret < 0) {
        mmsdbg(DL_ERROR, "Writing message to %s : %s",
            ctx->name, strerror(errno));
        return ret;
    }

    while (hdr.size) {
        ret = write(ctx->fd, payload, hdr.size);
        if (ret < 0) {
            mmsdbg(DL_ERROR, "Writing message to %s : %s",
                ctx->name, strerror(errno));
            break;
        }

        hdr.size -= ret;
        wr_size += ret;
        payload += ret;
    }

    return wr_size;

}

int osal_mailbox_readtimed(struct osal_mailbox *ctx,
    union osal_mbox_cmd *cmd, void *payload, int max_size, int timeout)
{
    struct osal_message_header hdr;
    struct pollfd pfd;
    size_t rd_size = 0;
    int ret, size;

    if (!ctx || !cmd)
        return -2;

    if (ctx->msg_size > 0 && max_size < ctx->msg_size) {
        mmsdbg(DL_WARNING, "Doesn't enough memory for message");
        return -2;
    }

    pfd.fd = ctx->fd;
    pfd.events = POLLIN;

    ret = poll(&pfd, 1, timeout);
    if (ret < 1) {
        return -2;
    } else if (!ret) {
        return -1;
    }

    ret = read(ctx->fd, &hdr, sizeof(hdr));
    if (ret < 0) {
        mmsdbg(DL_ERROR, "Error reading %s: %s", ctx->name, strerror(errno));
        return -2;
    } else if (ret != sizeof(hdr)) {
        mmsdbg(DL_ERROR, "Can't read mbox header %s", ctx->name);
        return -2;
    }

    *cmd = hdr.cmd;

    size = hdr.size;
    if (size > max_size) {
        mmsdbg(DL_WARNING, "Not enough buffer size: %s", ctx->name);
        size = max_size;
    }

    if (!size && hdr.size) {
        payload = tmp_buffer;
        size = sizeof(tmp_buffer);
    }

    while (hdr.size) {
        ret = read(ctx->fd, payload, min_t(uint32, hdr.size, size));
        if (ret < 0) {
            mmsdbg(DL_ERROR, "Error reading %s: %s", ctx->name, strerror(errno));
            break;
        }

        hdr.size -= ret;
        rd_size += ret;
        size -= ret;
        payload += ret;

        if (!size && hdr.size) {
            payload = tmp_buffer;
            size = sizeof(tmp_buffer);
        }
    }

    if (ret < 0) {
        return -2;
    }

    return rd_size;
}

int osal_mailbox_read(struct osal_mailbox *ctx,
    union osal_mbox_cmd *cmd, void *payload, int max_size)
{
    return osal_mailbox_readtimed(ctx, cmd, payload, max_size, -1);
}

