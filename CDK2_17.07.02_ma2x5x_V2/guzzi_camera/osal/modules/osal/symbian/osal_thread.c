/* =============================================================================
*   MultiMedia Solutions AD
*   (c) Copyright 2006-2010, MultiMedia Solutions AD All Rights Reserved.
*
*   Use of this software is controlled by the terms and conditions found
*   in the license agreement under which this software has been supplied.
* =========================================================================== */
/**
*    @file    osal_thread-Symbian.c
*
*    Functions for simple inter process communication
*
*    ^path (TOP)/lib/osal/thread/osal_thread-Linux.c
*
*    @author  Ivan Evlogiev  (MultiMedia Solutions AD)
*
*    @date    12.9.2008
*
*    @version 1.00

*/
/* =============================================================================
*!
*! Revision History
*! ======================================================
*!    12.9.2008    : IEvlogiev  (MultiMedia Solutions AD)
*!
* =========================================================================== */

#include <osal/osal_stdlib.h>
#include <osal/osal_stdio.h>
#include <osal/osal_string.h>
#include <osal/osal_thread.h>

#include <utils/mms_debug.h>

#ifdef __MMS_DEBUG__
#define MMSDEBUGLEVEL   vdl_osal_thread
#else
#define MMSDEBUGLEVEL   0
#endif

int vdl_osal_thread = DL_DEFAULT;


/* ========================================================================== */
/**
*  osal_thread_create()    Simple wrapper, which will clreate one POSIX
*           thread.
*
*  @param   child - struct osal_thread * - sructure to hold various
*           new thread parameters
*
*  @param   arg - data to be passed as argument to new thread.
*
*  @param   threadProc - Thread entry point.
*
*  @param   threadDone - Function for propriety closing of this thread
*
*  @return  non-zero on error.
*/
/* ========================================================================== */
int osal_thread_create(struct osal_thread *child,
    void *priv,
    osal_thread_FuncProc *threadProc,
    osal_thread_FuncDone *threadDone)
{
	threadProc(priv);
    return 0;
}


/* ========================================================================== */
/**
*  osal_thread_destroy()    destroy thread previsiously connected from
*           osal_thread_create.
*
*  @param   child - struct osal_thread * - sructure with parameters of thread
*           to destroy.
*
*  @return  non-zero on error.
*/
/* ========================================================================== */
int osal_thread_destroy(struct osal_thread *child)
{
    return 0;
}
