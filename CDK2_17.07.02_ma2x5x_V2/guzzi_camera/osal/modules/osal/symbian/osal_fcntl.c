/* =============================================================================
*   MultiMedia Solutions AD
*   (c) Copyright 2006-2010, MultiMedia Solutions AD All Rights Reserved.
*
*   Use of this software is controlled by the terms and conditions found
*   in the license agreement under which this software has been supplied.
* =========================================================================== */
/**
*    @file    osal_process-Linux.c
*
*    Contain wrapper functions for process
*
*    ^path (TOP)/lib/osal/process/osal_process-Linux.c
*
*    @author  Ivan Ivanov  (MultiMedia Solutions AD)
*
*    @date    18.7.2008
*
*    @version 1.10   29.07.2008  New implementation of all functions.
*
*    @version 1.00
*/
/* =============================================================================
*!
*! Revision History
*! ======================================================
*!    18.7.2008    : IIvanov  (MultiMedia Solutions AD)
*!     6.8.2008    : DMironov Re-Design
*!
*!
* =========================================================================== */

#include <osal/osal_stdio.h>
#include <osal/osal_sysdep.h>
#include <osal/osal_process.h>
#include <utils/mms_debug.h>
#include "3a_debug.h"

#ifdef __MMS_DEBUG__
#define MMSDEBUGLEVEL   vdl_osal_fctnl
#else
#define MMSDEBUGLEVEL   0
#endif

//int vdl_osal_fctnl = DL_DEFAULT;

/* ========================================================================== */
/**
*  osal_dev_init()    open() wrapper.
*
*  @param   dev_name - const char * - name of the device to open
*
*  @param   flags - unsigned int - application command line
*
*  @return  non-zero on error.
*/
/* ========================================================================== */
int osal_dev_init(const char *dev_name)
{
    return 1;
}


/* ========================================================================== */
/**
*  osal_dev_deinit()    close() wrapper.
*
*  @param   dev_id - int - device file descriptor
*
*  @return  non-zero on error.
*/
/* ========================================================================== */
int osal_dev_deinit(int dev_id)
{
   return 0;
}


OSAL_FILE * osal_fopen(char * fn, char * par)
{
    ERROR_PRINT("Please Implement osal_fopen for Symbian \n");
    return (OSAL_FILE *)NULL;
}

int osal_fclose(OSAL_FILE * file)
{
    ERROR_PRINT("Please Implement osal_fclose for Symbian \n");
    return NULL;
}


int osal_fgets(char * buffer, int LINESIZE, OSAL_FILE * file)
{
    ERROR_PRINT("Please Implement osal_fgets for Symbian \n");
    return NULL;
}
