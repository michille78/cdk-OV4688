/* =============================================================================
*   MultiMedia Solutions AD
*   (c) Copyright 2006-2010, MultiMedia Solutions AD All Rights Reserved.
*
*   Use of this software is controlled by the terms and conditions found
*   in the license agreement under which this software has been supplied.
* =========================================================================== */
/**
*    @file    osal_time-Symbian.c
*
*    Contain wrapper functions for process
*
*    ^path (TOP)/lib/osal/process/osal_time-Linux.c
*
*    @author  Ivan Evlogiev (MultiMedia Solutions AD)
*
*    @date    01.9.2008
*
*    @version 1.00
*/
/* =============================================================================
*!
*! Revision History
*! ======================================================
*!    01.9.2008    : Ivan Evlogiev  (MultiMedia Solutions AD)
*!
*!
* =========================================================================== */

#include <osal/osal_stdio.h>
#include <osal/osal_sysdep.h>
#include <osal/osal_process.h>
#include <utils/mms_debug.h>

#ifdef __MMS_DEBUG__
#define MMSDEBUGLEVEL   vdl_osal_time
#else
#define MMSDEBUGLEVEL   0
#endif

int vdl_osal_time = DL_DEFAULT;

/* ========================================================================== */
/**
*  osal_get_time()    get relative time after system is booting
*
*  @param   time - unsinged long - current time
*
*  @return  non-zero on error.
*/
/* ========================================================================== */
int osal_get_time(unsigned long *time)
{
    *time = (unsigned long) NKern::FastCounter();

    return 0;
}


/* ========================================================================== */
/**
*  osal_nsleep()   nanosleep() wrapper
*
*  @param   nsecs - unsigned int - sleep in nano seconds
*
*  @return  zero on success, negative if error.
*/
/* ========================================================================== */
int osal_nsleep(unsigned int nsecs)
{
    Kern::NanoWait(nsecs);

    return 0;
}

/* ========================================================================== */
/**
*  osal_sleep()    sleep() wrapper.
*
*  @param   secs - unsigned int - in seconds
*
*  @return  non-zero on error
*/
/* ========================================================================== */
void osal_sleep(unsigned int secs)
{
    Kern::NanoWait(secs * 1000000);
}

/* ========================================================================== */
/**
*  osal_usleep()    usleep() wrapper.
*
*  @param   usecs - unsigned int - in micro seconds
*
*  @return  non-zero on error
*/
/* ========================================================================== */
#ifdef MT9X012
int osal_usleep(unsigned int usecs)
{
    Kern::NanoWait(usecs * 1000);

    return 0;
}
#else
int osal_usleep(unsigned int usecs)
{
    NKern::Sleep(usecs / 1000);
    Kern::NanoWait(usecs % 1000);

    return 0;
}
#endif
