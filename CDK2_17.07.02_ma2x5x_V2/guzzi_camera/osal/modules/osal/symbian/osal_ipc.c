/* =============================================================================
*   MultiMedia Solutions AD
*   (c) Copyright 2006-2010, MultiMedia Solutions AD All Rights Reserved.
*
*   Use of this software is controlled by the terms and conditions found
*   in the license agreement under which this software has been supplied.
* =========================================================================== */
/**
*    @file    osal_process_msg-Symbian.c
*
*    Functions for simple inter process communication
*
*    NOTE: Every thread or process should create/open desired queue to be able
*    to read/write into it.
*
*    ^path (TOP)/lib/osal/osal_process_msg/osal_process_msg-Symbain.c
*
*    @author  Ivan Evlogiev  (MultiMedia Solutions AD)
*
*    @date    12.9.2008
*
*    @version 1.00

*/
/* =============================================================================
*!
*! Revision History
*! ======================================================
*!    12.9.2008    : IEvlogiev (Multimedia Solutions Ltd.)
*!
* =========================================================================== */
#include <osal/osal_mailbox.h>
#include <osal/osal_string.h>

#include <utils/mms_debug.h>

#ifdef __MMS_DEBUG__
#define MMSDEBUGLEVEL   vdl_osal_process_msg
#else
#define MMSDEBUGLEVEL   0
#endif

int vdl_osal_process_msg = DL_DEFAULT;

#undef DEBUG_PRINT
#define DEBUG_PRINT(arg1, arg2...)  //Kern::Printf(arg1, ##arg2)

int32 dummy_hllc_rcv    (struct osal_mailbox *, struct osal_message *)    { DEBUG_PRINT("[osal_process_msg] dummy_hllc_rcv"); return 0; }
int32 dummy_hllc_send    (struct osal_mailbox *, struct osal_message *)    { DEBUG_PRINT("[osal_process_msg] dummy_hllc_send"); return 0; }
int32 dummy_d3a_rcv        (struct osal_mailbox *, struct osal_message *)    { DEBUG_PRINT("[osal_process_msg] dummy_d3a_rcv"); return 0; }
int32 dummy_d3a_send    (struct osal_mailbox *, struct osal_message *)    { DEBUG_PRINT("[osal_process_msg] dummy_d3a_send"); return 0; }

struct LUT_member LUT[2]={
    {    MQ_HLLC_IN,        dummy_hllc_rcv,        dummy_hllc_send        },
    {    MQ_HLLC_OUT,    dummy_d3a_rcv,        dummy_d3a_send        }
};


/* ========================================================================== */
/**
*  osal_mailbox_create()    Open "q->name" for sending and receiving. The queue file
*           permissions are set rw for owner and nothing for group/others.
*
*  @param   q - struct osal_mailbox * - Q name.
*
*  @return  non-zero on error.
*/
/* ========================================================================== */
int32 osal_mailbox_create(struct osal_mailbox * q, const char* q_name)
{
    DEBUG_PRINT("[osal_process_msg] + osal_msg_proc_Open - %s", q_name);
    for (uint32 i=0; i<2; i++)
    {
    DEBUG_PRINT("LUT in %s", LUT[i].q_name);
        if (!osal_strcmp(q_name,LUT[i].q_name))
        {
            osal_strcpy(q->name, q_name);
            q->fd=i;
            DEBUG_PRINT("[osal_process_msg] - osal_msg_proc_Open - %s", q_name);
            return 0;
        }
    }
    DEBUG_PRINT("[osal_process_msg] - osal_msg_proc_Open - %s not found", q_name);
    return -1;
}

/* ========================================================================== */
/**
*    @func osal_mailbox_name_create()
*
*     @brief Prepares specific mailbox name for the current program instance.
*             Need to be used for every mailbox.
*
*    @param    mbox_name - char* - Buffer where to store mailbox name.
*    @param    cam_name - char* - Name of the camera device.
*    @param    mbox_suffix - char* - Suffix of the mailbox that makes it unique.
*
*    @return none
*/
/* ========================================================================== */
void osal_mailbox_name_create(char* mbox_name, uint32 mbox_size, char* cam_name, char* mbox_suffix)
{

 //
 // char* ptr;
 // osal_memset(mbox_name, 0, mbox_size);
 // osal_strncpy(mbox_name, cam_name, mbox_size);
 // osal_strcat(mbox_name, mbox_suffix);
 // ptr = osal_strchr(mbox_name, '/');
 // ptr = osal_strchr(ptr + 1, '/');
 //
 // while (NULL != ptr) {
 //
 //     osal_memmove(ptr, ptr + 1, osal_strlen(ptr + 1));
 //     *(ptr + osal_strlen(ptr) - 1) = '\0';
 //     ptr = osal_strchr(ptr, '/');
 // }
    osal_strcpy(mbox_name, mbox_suffix);

}

/* ========================================================================== */
/**
*  osal_msg_proc_Close()    close a message queue descriptor
*
*  @param   q - struct osal_mailbox * - what to close
*
*  @return  non-zero on error.
*/
/* ========================================================================== */
int32 osal_msg_proc_Close(struct osal_mailbox * q)
{
    DEBUG_PRINT("[osal_process_msg] osal_msg_proc_Close - %s", q->name);
    return 0;
}

/* ========================================================================== */
/**
*  osal_mailbox_destroy()    After unlink message queue is
*           removed from system.
*
*  @param   q - struct osal_mailbox * - what to destroy
*
*  @return  non-zero on error.
*/
/* ========================================================================== */
int32 osal_mailbox_destroy(struct osal_mailbox * q)
{
    DEBUG_PRINT("[osal_process_msg] osal_msg_proc_Destroy - %s", q->name);
    return 0;
}

/* ========================================================================== */
/**
 *  osal_mailbox_release()    Close and destroy given mailbox.
 *
 *  @param   mbox - struct osal_mailbox * - source mailbox
 *
 *  @param   close_type - enum osal_mailbox_close_type * - how to treat mailbox
 *
 *  OSAL_MAILBOX_CLOSE - Mailbox system structure will continue to exist, but thread that
 *  close the mailbox can not send/receive messages over it. From the other side
 *  if some thread still keep open this mailbox it can read from it.
 *
 *  OSAL_MAILBOX_DESTROY -  Mailbox system structure will be closed first and after that will be
 *  destroyed, NOTE all threads that keep references to this mailbox should call
 *  osal_mailbox_release(x, OSAL_MAILBOX_CLOSE) first
 *
 *  @return  non-zero on error.
 */
/* ========================================================================== */
int32 osal_mailbox_release(struct osal_mailbox * q,
                    enum osal_mailbox_close_type close_type)
{
    DEBUG_PRINT("[osal_process_msg] osal_msg_proc_Release - %s", q->name);
    return 0;
}

/* ========================================================================== */
/**
*  osal_mailbox_read()    Read message from mail box queue.
*
*  @param   mbox - struct osal_mailbox * - source mailbox
*
*  @param   cam_msg - struct osal_message * - where to store message
*
*  @return  non-zero on error.
*/
/* ========================================================================== */
int32 osal_mailbox_read(struct osal_mailbox * mbox, struct osal_message * cam_msg)
{
    DEBUG_PRINT("[osal_process_msg] + osal_msg_proc_Read - %s, %x", mbox->name, cam_msg->cmd);
    int32 ret = LUT[mbox->fd].rcv_fn(mbox, cam_msg);
    DEBUG_PRINT("[osal_process_msg] - osal_msg_proc_Read - %s, %x", mbox->name, cam_msg->cmd);
    return ret;
}


/* ========================================================================== */
/**
*  osal_mailbox_readtimed()    Read message from passed mail box Q. If message do not
*           arrive until specifed 'timeout' value in ms this call return -1;
*
*  @param   mbox - struct osal_mailbox * - source mailbox
*
*  @param   cam_msg -  struct osal_message * - where to store message.
*
*  @param   timeout - int - miliseconds, if 0 return immediately.
*
*  @return  non-zero on if there is not readed message.
*/
/* ========================================================================== */
int32 osal_mailbox_readtimed(struct osal_mailbox * mbox,
    struct osal_message * cam_msg, int32 timeout)
{
    DEBUG_PRINT("[osal_process_msg] + osal_msg_proc_ReadTimed - %s, %x", mbox->name, cam_msg->cmd);
    int32 ret = LUT[mbox->fd].rcv_fn(mbox, cam_msg);
    DEBUG_PRINT("[osal_process_msg] - osal_msg_proc_ReadTimed - %s, %x", mbox->name, cam_msg->cmd);
    return ret;
}


/* ========================================================================== */
/**
*  osal_mailbox_write()    Write message in mail box queue.
*
*  @param   mbox - struct osal_mailbox * - mailbox desination.
*
*  @param   cam_msg - struct osal_message * - message to send
*
*  @return  non-zero on error.
*/
/* ========================================================================== */
int32 osal_mailbox_write(struct osal_mailbox * mbox,
    struct osal_message * cam_msg)
{
    DEBUG_PRINT("[osal_process_msg] + osal_msg_proc_Send - %s, %x", mbox->name, cam_msg->cmd);
    int32 ret = LUT[mbox->fd].send_fn(mbox,cam_msg);
    DEBUG_PRINT("[osal_process_msg] - osal_msg_proc_Send - %s, %x", mbox->name, cam_msg->cmd);
    return ret;
}



int32 osal_msg_init_mbox(int id, char * name, rcv_fn_t rcv_fn, send_fn_t send_fn)
{
    DEBUG_PRINT("[osal_process_msg] + osal_msg_init_mbox - %s", name);
    osal_strcpy(LUT[id].q_name,name);
    LUT[id].rcv_fn =    rcv_fn;
    LUT[id].send_fn =    send_fn;
    DEBUG_PRINT("[osal_process_msg] - osal_msg_init_mbox - %s", name);
    return 0;
}
