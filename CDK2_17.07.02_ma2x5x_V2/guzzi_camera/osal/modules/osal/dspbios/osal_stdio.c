#ifndef  __OSAL_STDIO_H__
#define  __OSAL_STDIO_H__

#ifdef ___DSPBIOS___
#include <osal/osal_stdtypes.h>
#include <osal/osal_stdlib.h>
#include <osal/osal_sysdep.h>
#include <osal/osal_string.h>
#include <osal/osal_time.h>
#include <stdio.h>
#define osal_printf(ARGS, ...)    TIMM_OSAL_MenuExt(TIMM_OSAL_TRACEGRP_OMXCAM, ARGS, ##__VA_ARGS__)
#define osal_fprintf(mem, ARGS, ...)                            \
    {                                                               \
        if ( NULL != mem ) {                                          \
            char *tmp = osal_malloc(1024);                          \
            if ( tmp != NULL ) {                                      \
                tmp[0] = 0;                                         \
                osal_sprintf(tmp, ARGS, ##__VA_ARGS__);             \
                osal_strcat(mem, tmp);                              \
                osal_free(tmp);                                     \
            }                                                       \
        }                                                           \
    }
#define osal_sprintf             sprintf
        static inline char *osal_fopen(char *fname, char *mode)
        {
            char   *mem = osal_calloc(1, 1048576);
            char   *cur_mod = osal_strstr(mode, "b");
            if ( NULL != mem ) {
                mem[0] = '\0';
                if ( cur_mod != NULL ) {
                    osal_strcat(mem, "binary");
                }
                osal_strcat(mem, fname);
                osal_strcat(mem, "\n");
            }
            return (mem);
        }

        static inline void osal_fclose(char *mem)
        {
            if ( NULL != mem ) {
                char   *tmp = osal_strtok(mem, "\n");
                if ( tmp != NULL ) {
                    char   *filename = tmp;
                    if ( NULL != osal_strstr(tmp, "binary")) {
                        uint16    size = 0;
                        tmp += osal_strlen("binary");
                        filename = tmp;
                        if ( filename != NULL ) {
                            TIMM_OSAL_MenuExt(TIMM_OSAL_TRACEGRP_OMXCAM, "%s - binary", filename);
                            tmp += osal_strlen(filename) + 2;
                            size = (uint16)(*tmp++ & 0xFF);
                            size |= (uint16)((*tmp++ & 0xFF) << 8);

                            while( size != 0 ) {
                                void   *buff = osal_malloc(32 * 2 + 32 + 10);
                                if ( buff != NULL ) {
                                    uint16    i;
                                    char     *cur_buff = buff;

                                    for( i = 0; i < size; i++ ) {
                                        if ( i != 0 && i % 32 == 0 ) {
                                            osal_sprintf(cur_buff, "\0");
                                            osal_nsleep(10000);
                                            TIMM_OSAL_MenuExt(TIMM_OSAL_TRACEGRP_OMXCAM, "%s", buff);
                                            cur_buff = buff;
                                        }
                                        osal_sprintf(cur_buff, "%02X ", *tmp);
                                        cur_buff += 3;
                                        tmp += 1;
                                    }

                                    osal_sprintf(cur_buff, "\0");
                                    osal_nsleep(10000);
                                    TIMM_OSAL_MenuExt(TIMM_OSAL_TRACEGRP_OMXCAM, "%s", buff);
                                    osal_free(buff);
                                    size = (uint16)(*tmp++ & 0xFF);
                                    size |= (uint16)((*tmp++ & 0xFF) << 8);
                                }
                            }
                        }
                    } else {
                        while( NULL != tmp ) {
                            void   *buff = osal_malloc(100);
                            osal_nsleep(10000);
                            TIMM_OSAL_MenuExt(TIMM_OSAL_TRACEGRP_OMXCAM, "%s", tmp);
                            tmp = osal_strtok(NULL, "\n");
                        }
                    }
                    if ( filename != NULL ) {
                        TIMM_OSAL_MenuExt(TIMM_OSAL_TRACEGRP_OMXCAM, "%s-", filename);
                    }
                    osal_free(mem);
                }
            }
        }

#define osal_fflush
#define osal_fgets
        static inline void osal_fwrite(void *buff, uint32 cnt, uint32 size, char *mem)
        {
            if ( mem != NULL ) {
                char   *tmp = osal_strstr(mem, "binary");
                if ( tmp != NULL ) {
                    uint16    cur_size;
                    uint16    size_to_add = cnt * size;
                    tmp += osal_strlen(mem) + 1;
                    cur_size = (uint16)(*tmp++ & 0xFF);
                    cur_size |= (uint16)((*tmp++ & 0xFF) << 8);

                    while( cur_size != 0 ) {
                        tmp += cur_size;
                        cur_size = (uint16)(*tmp++ & 0xFF);
                        cur_size |= (uint16)((*tmp++ & 0xFF) << 8);
                    }

                    tmp -= 2;
                    *tmp++ = (uint8)(size_to_add & 0xFF);
                    *tmp++ = (uint8)((size_to_add >> 8) & 0xFF);
                    osal_memcpy(tmp, buff, size_to_add);
                }
            }
        }

#define OSAL_FILE                  char

#endif

#ifdef ___LINUX___

#include <stdio.h>

#define osal_printf              printf
#define osal_fprintf             fprintf
#define osal_fopen               fopen
#define osal_fclose              fclose
#define osal_fflush              fflush
#define osal_fgets               fgets
#define osal_fseek               fseek
#define osal_ftell               ftell
#define osal_fread               fread
#define osal_fwrite              fwrite

typedef FILE OSAL_FILE;

#endif /* ___LINUX___ */

#ifdef ___ANDROID___

#include <stdio.h>

#define osal_printf              printf
#define osal_fprintf             fprintf
#define osal_fopen               fopen
#define osal_fclose              fclose
#define osal_fflush              fflush
#define osal_fgets               fgets
#define osal_fread               fread

typedef FILE OSAL_FILE;

#endif /* ___ANDROID___ */

#ifdef ___SYMBIAN___

#define osal_printf              Kern::Printf
#define osal_fprintf             { Kern::Printf("\nPlease Implement osal_fprintf for Symbian !!!\n"); }
#define osal_fflush              { Kern::Printf("\nPlease Implement osal_fflush for Symbian !!!\n"); }

typedef struct FILE {
    int dummy;
} OSAL_FILE;

OSAL_FILE *osal_fopen(char *fn, char *par);
int osal_fgets(char *buffer, int LINESIZE, OSAL_FILE *file);
int osal_fclose(OSAL_FILE *file);

#endif  /* ___SYMBIAN___ */

#ifdef ___WINDOWS___
#include <stdio.h>

#define osal_printf              printf
#define osal_fprintf             fprintf
#define osal_fopen               fopen
#define osal_fclose              fclose
#define osal_fwrite              fwrite
#define osal_fread               fread
#define osal_fflush              fflush
#define osal_fgets               fgets
#define osal_fseek               fseek
#define osal_ftell               ftell

typedef FILE OSAL_FILE;

#endif

#endif // __OSAL_STDLIB_H__

