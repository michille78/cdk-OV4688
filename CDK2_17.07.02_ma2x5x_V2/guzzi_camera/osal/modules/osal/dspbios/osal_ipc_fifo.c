/* =============================================================================
*   MultiMedia Solutions Ltd.
*   (c) Copyright 2006-2008, MultiMedia Solutions Ltd. All Rights Reserved.
*
*   Use of this software is controlled by the terms and conditions found
*   in the license agreement under which this software has been supplied.
* =========================================================================== */
/**
*	@file    osal_process_msg-Linux.c
*
*	Functions for simple inter process communication
*
*	NOTE: Every thread or process should create/open desired queue to be able
*	to read/write into it.
*
*	Example:
*
*	Process1:				Process2:
*
*	Open("/comm_mailbox");			Open("/comm_mailbox");
*	Send("/comm_mailbox", message);		Read("/comm_mailbox", message);
*	Close("/comm_mailbox");			Close("/comm_mailbox");
*	Destroy("/comm_mailbox");
*
*	Typical usage:
*
*
*	int main() {
*
*  		osal_message 	message;
* 		osal_mailbox 	mailbox;
*
*		Create a new queue named "/my_mailbox" and open it for sending
*		and receiving. The queue file permissions are set rw for
*		owner and nothing for group/others. Queue limits set to
*		values provided above.
*
*		if (osal_mailbox_create(&mailbox, "/my_mailbox")) {
*			mmsdbg(DL_ERROR, "Creating Mailbox error");
*			return -1;
*		}
*
*		Send a message to the queue with priority 1. Higher the
*		number, higher is the priority. A high priority message is
*		inserted before a low priority message. osal_mbx_list-in osal_mbx_list-out
*		for equal priority messages.
*
*		if (osal_mailbox_write(&mailbox, &message)) {
*
* 			Remove this mailbox from Kernel system
*
* 			osal_mailbox_release(&mailbox, CLOSE_DESTROY);
*			mmsdbg(DL_ERROR, "Sending message error");
*			return -1;
*		}
*
*		mmsdbg(DL_MESSAGE, "Message send");
*
*		Just Close queue, do not remove it from kernel system resources,
* 		in this way it will be available to other processes to read
* 		messages from it.
*
*		if (osal_mailbox_release(&mailbox, OSAL_MAILBOX_CLOSE))
*			mmsdbg(DL_ERROR, "Closing queue error");
*
*		return 0;
*	}
*
*
*	int main() {
*
*  		osal_message 	message;
* 		osal_mailbox 	mailbox;
*
*		Create a new queue named "/my_mailbox" and open it for sending
*		and receiving. The queue file permissions are set rw for
*		owner and nothing for group/others. Queue limits set to
*		values provided above.
*
*		if (osal_mailbox_create(&mailbox, "/my_mailbox")) {
*			mmsdbg(DL_ERROR, "Creating Mailbox error");
*			return -1;
*		}
*
*		Now receive the message from queue. This is a blocking call.
*		The function receives the oldest of the highest priority message(s) from
*		the message queue.
*
*		if (osal_mailbox_read(&mailbox, &message)) {
*			mmsdbg(DL_ERROR, "cannot receive");
*		}
*
*		mmsdbg(DL_MESSAGE, "Message read");
*
*		Remove this mailbox from Kernel system
*
 *		if (osal_mailbox_release(&mailbox, OSAL_MAILBOX_DESTROY))
*			mmsdbg(DL_ERROR, "Closing queue error");
*
* 		return 0;
*	}
*
*
* 	# gcc –o mqueue-1 mqueue-1.c –lrt
*	# gcc –o mqueue-2 mqueue-2.c –lrt
*	# ./mqueue-1
*	# Message sent
*	# ./mqueue-2
*	# Message read
*
*	^path (TOP)/lib/osal/osal_process_msg/osal_process_msg-Linux.c
*
*	@author  Boris Daskalov  (MultiMedia Solutions Ltd.)
*
*	@date    05.01.2009
*
*	@version 1.00

*/
/* =============================================================================
*!
*! Revision History
*! ======================================================
*!	05.01.2009    : BDaskalov Created.
*!
* =========================================================================== */

#include <xdc/runtime/Types.h>

#include <osal/osal_mailbox.h>
#include <osal/osal_string.h>
#include <osal/osal_time.h>
#include <utils/mms_debug.h>

#ifdef __MMS_DEBUG__
#define MMSDEBUGLEVEL   vdl_osal_process_msg
#else
#define MMSDEBUGLEVEL   0
#endif

int vdl_osal_process_msg = DL_DEFAULT;

#define MBOX_NAME_PREFIX "/tmp/3afw_mbox_"

struct osal_mailbox* osal_mbx_list = NULL;

/* ========================================================================== */
/**
*  osal_message_print()    Print message information
*
*  @param   msg - struct osal_message * - message.
*
*  @return  non-zero on error.
*/
/* ========================================================================== */
void  osal_message_print(struct osal_message *msg)
{
    char *msg_str[] = {
        "CAM3A_CMD_START_AEWB", "CAM3A_CMD_STOP_AEWB",
        "CAM3A_CMD_INIT", "CAM3A_CMD_RELEASE",
        "CAM3A_CMD_WRITESETTINGS", "CAM3A_CMD_READSETTINGS",
        "CAM3A_CMD_READSTATUS", "CAM3A_CMD_STAT_RUN",
        "CAM3A_CMD_STAT_READ", "CAM3A_CMD_STAT_PAUSE",
        "CAM3A_CMD_STAT_EXIT", "CAM3A_CMD_AF_STAT_READY",
        "CAM3A_CMD_AWB_STAT_READY"
    };

    char *str = ARRAY_SIZE(msg_str) < (unsigned) msg->cmd ? "unknown" : msg_str[msg->cmd];

    mmsdbg(DL_MESSAGE, "Message id=%4d; status=%4d; size=%4d; cmd=%s",
            (int)msg->cmd, (int)msg->status,
            (int)msg->payloadSize, str);
}


/* ========================================================================== */
/**
*	@func strcat_without_slashes()
*
* 	@brief Helper function to create mailbox filenames. Works like strcat
* 	       but substitutes slashes with underscrores.
*
*	@return none
*/
/* ========================================================================== */

static void strcat_without_slashes(char* dst, const char* src)
{
    dst += osal_strlen(dst);
    do {
        *dst++ = (*src == '/') ? '_' : *src;
    } while( *src++ );
}



/* ========================================================================== */
/**
*	@func osal_mailbox_name_create()
*
* 	@brief Prepares specific mailbox name for the current program instance.
* 			Need to be used for every mailbox.
*
*	@param	mbox_name - char* - Buffer where to store mailbox name.
*	@param	cam_name - char* - Name of the camera device.
*	@param	mbox_suffix - char* - Suffix of the mailbox that makes it unique.
*
*	@return none
*/
/* ========================================================================== */
void osal_mailbox_name_create(char* mbox_name, uint32 mbox_size, char* cam_name, char* mbox_suffix)
{
    __assert( osal_strlen(MBOX_NAME_PREFIX) + osal_strlen(cam_name) + osal_strlen(mbox_suffix) < mbox_size );

    osal_memset(mbox_name, 0, mbox_size);
    osal_strcpy(mbox_name, MBOX_NAME_PREFIX);
    strcat_without_slashes(mbox_name, cam_name);
    strcat_without_slashes(mbox_name, mbox_suffix);
}


/* ========================================================================== */
/**
*  osal_mailbox_create()    Open "q->name" for sending and receiving. The queue file
*           permissions are set rw for owner and nothing for group/others.
*
*  @param   q - struct osal_mailbox * - Q name.
*
*  @return  non-zero on error.
*/
/* ========================================================================== */
int32 osal_mailbox_create(struct osal_mailbox * q, const char* q_name)
{
    struct osal_mailbox* node = osal_mbx_list;
    int32 ret = 0;

    __assert(q);

    if (!q) {
        return -1;
    }

    while (node)
    {
        if (0 == osal_strcmp(node->name, q_name))
            break;
        node = node->next;
    }

    osal_strncpy(q->name, q_name, FW_MBOX_NAME_MAX_SIZE);

    if (node)
    {
        q->pHandle = node->pHandle;
        node->cnt++;
    } else {
        q->pHandle = Mailbox_create (MQ_MSG_SIZE, MQ_MAX_MESSAGES, NULL,NULL);
        q->cnt = 1;
        q->next =  osal_mbx_list;
        osal_mbx_list = q;
    }

    if (TIMM_OSAL_NULL == q->pHandle) {
        ret = -1;
        mmsdbg(DL_ERROR, "Creating queue %s : %s",
               q->name[0] ? q->name : "<empty name string>", " !!! Error !!!");
            goto out;
    } else {
        mmsdbg(DL_LAYER1, "Created queue %s.", q->name[0] ? q->name : "<empty name string>");
    }

out:
    return ret;
}

/* ========================================================================== */
/**
 *  osal_mailbox_release()    Close and destroy given mailbox.
 *
 *  @param   mbox - struct osal_mailbox * - source mailbox
 *
 *  @param   close_type - enum osal_mailbox_close_type * - how to treat mailbox
 *
 *  OSAL_MAILBOX_CLOSE - Mailbox system structure will continue to exist, but thread that
 *  close the mailbox can not send/receive messages over it. From the other side
 *  if some thread still keep open this mailbox it can read from it.
 *
 *  OSAL_MAILBOX_DESTROY -  Mailbox system structure will be closed osal_mbx_list and after that will be
 *  destroyed, NOTE all threads that keep references to this mailbox should call
 *  osal_mailbox_release(x, OSAL_MAILBOX_CLOSE) osal_mbx_list
 *
 *  @return  non-zero on error.
 */
/* ========================================================================== */
int32 osal_mailbox_release(struct osal_mailbox          *q,
                           enum osal_mailbox_close_type  close_type)
{
int32	ret = 0;
struct  osal_mailbox* node = osal_mbx_list;
struct  osal_mailbox* prv_node = NULL;

    if (!q) {
        return -1;
    }
    while (node)
    {
        if (0 == osal_strcmp(node->name, q->name))
        {
            break;
        }
        prv_node = node;
        node = node->next;
    }
    __assert(node);
    if (node)
    {

        if (1 < node->cnt)
        {
            node->cnt--;                      // decrease number of references
        } else {
            Mailbox_delete(&node->pHandle);
            if (prv_node)
                prv_node->next = node->next;                // no more references to this mbx so remove it from list
            else
                osal_mbx_list = node->next;
            q->name[0] = 0;
        }
    } else {
        mmssys(DL_LAYER1, "Error releasing %s.", q->name[0] ? q->name : "<empty name string>");
        ret = -1;
    }

    return ret;
}

/* ========================================================================== */
/**
*  osal_mailbox_read()    Read message from mail box queue.
*
*  @param   mbox - struct osal_mailbox * - source mailbox
*
*  @param   cam_msg - struct osal_message * - where to store message
*
*  @return  non-zero on error.
*/
/* ========================================================================== */
int32 osal_mailbox_read(struct osal_mailbox * mbox,
    struct osal_message * cam_msg)
{
    int               ret = 0;

    __assert(mbox && cam_msg);

    if (!mbox || !cam_msg) {
        return -1;
    }

    ret = (0 == Mailbox_pend (mbox->pHandle, cam_msg , BIOS_WAIT_FOREVER));
    if (ret)
    {
        mmsdbg(DL_ERROR, "Message not complete read from %s => Expect communication crash.", mbox->name);
        ret = -1;
    }

    return (ret);
}


/* ========================================================================== */
/**
*  osal_mailbox_readtimed()    Read message from passed mail box Q. If message do not
*           arrive until specifed 'timeout' value in ms this call return -1;
*
*  @param   mbox - struct osal_mailbox * - source mailbox
*
*  @param   cam_msg -  struct osal_message * - where to store message.
*
*  @param   timeout - int - miliseconds, if 0 return immediately.
*
*  @return  non-zero on if there is not readed message.
*/
/* ========================================================================== */
int32 osal_mailbox_readtimed(struct osal_mailbox *mbox,
                struct osal_message *cam_msg, int32 timeout)
{
    int             ret = -1;
//    Types_FreqHz    freq;

    __assert(mbox && cam_msg);

    if (!mbox || !cam_msg)
        return ret;

 // TODO   BIOS_getCpuFreq(&freq);
    // Convert ms to system ticks
 // TODO  timeout = timeout*(freq.lo/1000);

    ret = Mailbox_pend (mbox->pHandle, cam_msg , timeout);

    ret = (FALSE == ret)? -1:0;

    return (ret);
}


/* ========================================================================== */
/**
*  osal_mailbox_write()    Write message in mail box queue.
*
*  @param   mbox - struct osal_mailbox * - mailbox desination.
*
*  @param   cam_msg - struct osal_message * - message to send
*
*  @return  non-zero on error.
*/
/* ========================================================================== */
int32 osal_mailbox_write(struct osal_mailbox * mbox,
    struct osal_message * cam_msg)
{
    int               ret = -1;

    __assert(mbox && cam_msg);

    if (!mbox || !cam_msg) {
        return ret;
    }


    ret = Mailbox_post(mbox->pHandle,cam_msg,BIOS_WAIT_FOREVER);

    ret = (FALSE == ret)? -1:0;

    return (ret);

}
