/* =============================================================================
*   MultiMedia Solutions Ltd.
*   (c) Copyright 2006-2008, MultiMedia Solutions Ltd. All Rights Reserved.
*
*   Use of this software is controlled by the terms and conditions found
*   in the license agreement under which this software has been supplied.
* =========================================================================== */
/**
*	@file    osal_thread-Linux.c
*
*	Functions for simple inter process communication
*
*	^path (TOP)/lib/osal/thread/osal_thread-Linux.c
*
*	@author  Ivan Ivanov  (MultiMedia Solutions Ltd.)
*
*	@date    25.6.2008
*
*	@version 1.00

*/
/* =============================================================================
*!
*! Revision History
*! ======================================================
*!	25.6.2008    : IIvanov  (MultiMedia Solutions Ltd.)
*!	05.8.2008    : DMironov Add prefix and separated in different file
*!
* =========================================================================== */

#include <osal/osal_stdlib.h>
#include <osal/osal_stdio.h>
#include <osal/osal_string.h>
#include <platform/osal/timm_osal_interfaces.h>
#include <ti/sysbios/knl/Task.h>

#include <osal/osal_sysdep.h>               /* pipe(), read() ... (all from unistd.h) */
#include <osal/osal_thread.h>

#include <utils/mms_debug.h>

#ifdef __MMS_DEBUG__
#define MMSDEBUGLEVEL   vdl_osal_thread
#else
#define MMSDEBUGLEVEL   0
#endif

int vdl_osal_thread = DL_DEFAULT;


/* ========================================================================== */
/**
*  osal_thread_create()    Simple wrapper, which will clreate one thread.
*
*  @param   child - struct osal_thread * - sructure to hold various
*           new thread parameters
*
*  @param   arg - data to be passed as argument to new thread.
*
*  @param   threadProc - Thread entry point.
*
*  @param   threadDone - Function for propriety closing of this thread
*
*  @return  non-zero on error.
*/
/* ========================================================================== */
int osal_thread_create(struct osal_thread *child,
                        char *name, void *priv,
                        osal_thread_func *proc,
                        osal_thread_done *done)
{
TIMM_OSAL_ERRORTYPE  osalError = TIMM_OSAL_ERR_NONE;

    __assert(child && threadProc);

    if (!child || !threadProc) {
        return -1;
    }

    child->priv = priv;
    child->threadProc = threadProc;
    child->threadDone = threadDone;
/*
    TIMM_OSAL_ERRORTYPE TIMM_OSAL_CreateTask (TIMM_OSAL_PTR *pTask,
                                          TIMM_OSAL_TaskProc pFunc,
                                          TIMM_OSAL_U32 uArgc,
                                          TIMM_OSAL_PTR pArgv,
                                          TIMM_OSAL_U32 uStackSize,
                                          TIMM_OSAL_U32 uPriority,
                                          TIMM_OSAL_S8 *pName);
*/
    osalError = TIMM_OSAL_CreateTask(&child->hThread,       //
                                      (TIMM_OSAL_TaskProc)threadProc,           //
                                      1,
                                      priv,
                                      10*1024,
                                      2,
                                      child->pName);

	if ( osalError  != TIMM_OSAL_ERR_NONE)
	{
		//SYS_printf("3A OSL Error: Creating Thread\n");
		//Need to handle this error
	}

    return osalError;
}


/* ========================================================================== */
/**
*  osal_thread_destroy()    destroy thread previsiously connected from
*           osal_thread_destroy.
*
*  @param   child - struct osal_thread * - sructure with parameters of thread
*           to destroy.
*
*  @return  non-zero on error.
*/
/* ========================================================================== */
int osal_thread_destroy(struct osal_thread *child)
{
    int               error = 0;

    __assert(child);

    if (!child) {
        return -1;
    }

    if (NULL!=child->threadDone) {

        mmsdbg(DL_MESSAGE, "Stopping thread");
        child->threadDone(child->priv);
    }
    if (0 != TIMM_OSAL_DeleteTask(child->hThread)) {
            mmsdbg(DL_ERROR, "Error while cancel reader");
            error = -1;
    }

    return error;
}


int osal_thread_Self(void)
{
    return (int)Task_self(); // Returns a handle to the currently executing Task object( );
}

/* ========================================================================== */
/**
*  osal_thread_priority()    chnage thread priority connected from
*           osal_thread_priority.
*
*  @param   child - struct osal_thread * - sructure with parameters of thread
*           to destroy.
*
*  @param   priority - struct uint32 * - new priority
*
*
*  @return  non-zero on error.
*/
/* ========================================================================== */
int osal_thread_priority (struct osal_thread *child, uint32 *priority)
{
    __assert(child);

    if (!child) {
        return -1;
    }

    TIMM_OSAL_setTaskPriority(child->hThread, *priority);

    return 0;
}
