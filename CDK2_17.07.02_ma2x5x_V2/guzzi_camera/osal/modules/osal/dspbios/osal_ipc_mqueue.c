/* =============================================================================
*   MultiMedia Solutions Ltd.
*   (c) Copyright 2006-2008, MultiMedia Solutions Ltd. All Rights Reserved.
*
*   Use of this software is controlled by the terms and conditions found
*   in the license agreement under which this software has been supplied.
* =========================================================================== */
/**
*	@file    osal_process_msg-Linux.c
*
*	Functions for simple inter process communication
*
*	NOTE: Every thread or process should create/open desired queue to be able
*	to read/write into it.
*
*	Example:
*
*	Process1:				Process2:
*
*	Open("/comm_mailbox");			Open("/comm_mailbox");
*	Send("/comm_mailbox", message);		Read("/comm_mailbox", message);
*	Close("/comm_mailbox");			Close("/comm_mailbox");
*	Destroy("/comm_mailbox");
*
*	Typical usage:
*
*
*	int main() {
*
*  		osal_message 	message;
* 		osal_mailbox 	mailbox;
*
*		Create a new queue named "/my_mailbox" and open it for sending
*		and receiving. The queue file permissions are set rw for
*		owner and nothing for group/others. Queue limits set to
*		values provided above.
*
*		if (osal_mailbox_create(&mailbox, "/my_mailbox")) {
*			mmsdbg(DL_ERROR, "Creating Mailbox error");
*			return -1;
*		}
*
*		Send a message to the queue with priority 1. Higher the
*		number, higher is the priority. A high priority message is
*		inserted before a low priority message. First-in First-out
*		for equal priority messages.
*
*		if (osal_mailbox_write(&mailbox, &message)) {
*
* 			Remove this mailbox from Kernel system
*
* 			osal_mailbox_release(&mailbox, CLOSE_DESTROY);
*			mmsdbg(DL_ERROR, "Sending message error");
*			return -1;
*		}
*
*		mmsdbg(DL_MESSAGE, "Message send");
*
*		Just Close queue, do not remove it from kernel system resources,
* 		in this way it will be available to other processes to read
* 		messages from it.
*
*		if (osal_mailbox_release(&mailbox, OSAL_MAILBOX_CLOSE))
*			mmsdbg(DL_ERROR, "Closing queue error");
*
*		return 0;
*	}
*
*
*	int main() {
*
*  		osal_message 	message;
* 		osal_mailbox 	mailbox;
*
*		Create a new queue named "/my_mailbox" and open it for sending
*		and receiving. The queue file permissions are set rw for
*		owner and nothing for group/others. Queue limits set to
*		values provided above.
*
*		if (osal_mailbox_create(&mailbox, "/my_mailbox")) {
*			mmsdbg(DL_ERROR, "Creating Mailbox error");
*			return -1;
*		}
*
*		Now receive the message from queue. This is a blocking call.
*		The function receives the oldest of the highest priority message(s) from
*		the message queue.
*
*		if (osal_mailbox_read(&mailbox, &message)) {
*			mmsdbg(DL_ERROR, "cannot receive");
*		}
*
*		mmsdbg(DL_MESSAGE, "Message read");
*
*		Remove this mailbox from Kernel system
*
 *		if (osal_mailbox_release(&mailbox, OSAL_MAILBOX_DESTROY))
*			mmsdbg(DL_ERROR, "Closing queue error");
*
* 		return 0;
*	}
*
*
* 	# gcc –o mqueue-1 mqueue-1.c –lrt
*	# gcc –o mqueue-2 mqueue-2.c –lrt
*	# ./mqueue-1
*	# Message sent
*	# ./mqueue-2
*	# Message read
*
*	^path (TOP)/lib/osal/osal_process_msg/osal_process_msg-Linux.c
*
*	@author  Ivan Ivanov  (MultiMedia Solutions Ltd.)
*
*	@date    25.6.2008
*
*	@version 1.00

*/
/* =============================================================================
*!
*! Revision History
*! ======================================================
*!	28.8.2008    : IIvanov  Use case comment added
*!	05.8.2008    : DMironov Add prefix and separated in different file
*!	25.6.2008    : IIvanov  (MultiMedia Solutions Ltd.)
*!
* =========================================================================== */

#define _XOPEN_SOURCE 600

#include <osal/osal_mailbox.h>
#include <osal/osal_string.h>
#include <osal/osal_time.h>

#include <utils/mms_debug.h>

#ifdef __MMS_DEBUG__
#define MMSDEBUGLEVEL   vdl_osal_process_msg
#else
#define MMSDEBUGLEVEL   0
#endif

int vdl_osal_process_msg = DL_DEFAULT;

/* ========================================================================== */
/**
*  osal_message_print()    Print message information
*
*  @param   msg - struct osal_message * - message.
*
*  @return  non-zero on error.
*/
/* ========================================================================== */
void  osal_message_print(struct osal_message *msg)
{
    char *msg_str[] = {
        "CAM3A_CMD_START_AEWB", "CAM3A_CMD_STOP_AEWB",
        "CAM3A_CMD_INIT", "CAM3A_CMD_RELEASE",
        "CAM3A_CMD_WRITESETTINGS", "CAM3A_CMD_READSETTINGS",
        "CAM3A_CMD_READSTATUS", "CAM3A_CMD_STAT_RUN",
        "CAM3A_CMD_STAT_READ", "CAM3A_CMD_STAT_PAUSE",
        "CAM3A_CMD_STAT_EXIT", "CAM3A_CMD_AF_STAT_READY",
        "CAM3A_CMD_AWB_STAT_READY"
    };

    char *str = ARRAY_SIZE(msg_str) < (unsigned) msg->cmd ? "unknown" : msg_str[msg->cmd];

    str = str;

    mmsdbg(DL_MESSAGE, "Message id=%4d; status=%4d; size=%4d; cmd=%s",
            (int)msg->cmd, (int)msg->status,
            (int)msg->payloadSize, str);
}


/* ========================================================================== */
/**
*	@func osal_mailbox_name_create()
*
* 	@brief Prepares specific mailbox name for the current program instance.
* 			Need to be used for every mailbox.
*
*	@param	mbox_name - char* - Buffer where to store mailbox name.
*	@param	cam_name - char* - Name of the camera device.
*	@param	mbox_suffix - char* - Suffix of the mailbox that makes it unique.
*
*	@return none
*/
/* ========================================================================== */
void osal_mailbox_name_create(char* mbox_name, uint32 mbox_size, char* cam_name, char* mbox_suffix)
{
    char* ptr;

    osal_memset(mbox_name, 0, mbox_size);
    osal_strncpy(mbox_name, cam_name, mbox_size);
    osal_strcat(mbox_name, mbox_suffix);
    ptr = osal_strchr(mbox_name, '/');
    ptr = osal_strchr(ptr + 1, '/');

    while (NULL != ptr) {

	    osal_memmove(ptr, ptr + 1, osal_strlen(ptr + 1));
	    *(ptr + osal_strlen(ptr) - 1) = '\0';
	    ptr = osal_strchr(ptr, '/');
    }

}


/* ========================================================================== */
/**
*  osal_mailbox_create()    Open "q->name" for sending and receiving. The queue file
*           permissions are set rw for owner and nothing for group/others.
*
*  @param   q - struct osal_mailbox * - Q name.
*
*  @return  non-zero on error.
*/
/* ========================================================================== */
int32 osal_mailbox_create(struct osal_mailbox * q, const char* q_name)
{

    mqd_t             ds;
    struct mq_attr    attr;
    int               ret = 0;

    __assert(q);

    if (!q) {
        return -1;
    }

    osal_strncpy(q->name, q_name, FW_MBOX_NAME_MAX_SIZE);

    /*
     * Attributes for our queue. They can be set only during
     * creating.
     */
    attr.mq_maxmsg = MQ_MAX_MESSAGES;   /* max. number of messages in queue at the
                                          same time */
    attr.mq_msgsize = MQ_MSG_SIZE;      /* max. message size */
    /*
     * Create a new queue named "/my_queue" and open it for sending
     * and receiving. The queue file permissions are set rw for
     * owner and nothing for group/others. Queue limits set to
     * values provided above.
     */
    if ((ds = mq_open((char*)&q->name[0], O_CREAT | O_RDWR, 0777, &attr)) == (mqd_t) - 1) {
        mmsdbg(DL_ERROR, "Error Creating queue %s : %s",
               q->name[0] ? q->name : "null", strerror(errno));
        ret = -1;
    }

    q->fd = ds;

    return ret;
}


/* ========================================================================== */
/**
*  osal_mailbox_close()    close a message queue descriptor
*
*  @param   q - struct osal_mailbox * - what to close
*
*  @return  non-zero on error.
*/
/* ========================================================================== */
int32 osal_mailbox_close(struct osal_mailbox * q)
{
    int ret;

    __assert(q);

    if (!q) {
        return -1;
    }

    ret = mq_close(q->fd);

    if (0 > ret) {
        mmsdbg(DL_ERROR, "Error Closing queue %s : %s",
               q->name[0] ? q->name : "null", strerror(errno));
    }

    return ret;
}

/* ========================================================================== */
/**
*  osal_mailbox_destroy()    After unlink message queue is
*           removed from system.
*
*  @param   q - struct osal_mailbox * - what to destroy
*
*  @return  non-zero on error.
*/
/* ========================================================================== */
int32 osal_mailbox_destroy(struct osal_mailbox * q)
{
    int               ret = -1;

    __assert(q);

    if (!q) {
        return ret;
    }

    /*
     * ...and finally unlink it. After unlink message queue is
     * removed from system.
     */
    if ((ret = mq_unlink(q->name)) == -1) {
        mmsdbg(DL_ERROR, "Error Unlinking queue %s : %s",
               q->name[0] ? q->name : "null", strerror(errno));

    }

    return ret;
}

/* ========================================================================== */
/**
 *  osal_mailbox_release()    Close and destroy given mailbox.
 *
 *  @param   mbox - struct osal_mailbox * - source mailbox
 *
 *  @param   close_type - enum osal_mailbox_close_type * - how to treat mailbox
 *
 *  OSAL_MAILBOX_CLOSE - Mailbox system structure will continue to exist, but thread that
 *  close the mailbox can not send/receive messages over it. From the other side
 *  if some thread still keep open this mailbox it can read from it.
 *
 *  OSAL_MAILBOX_DESTROY -  Mailbox system structure will be closed first and after that will be
 *  destroyed, NOTE all threads that keep references to this mailbox should call
 *  osal_mailbox_release(x, OSAL_MAILBOX_CLOSE) first
 *
 *  @return  non-zero on error.
 */
/* ========================================================================== */
int32 osal_mailbox_release(struct osal_mailbox * q,
			    	        enum osal_mailbox_close_type close_type)
{
	int32	ret = 0;

	ret = osal_mailbox_close(q);

	if (OSAL_MAILBOX_DESTROY == close_type)
		ret += osal_mailbox_destroy(q);

	return ret;
}

/* ========================================================================== */
/**
*  osal_mailbox_read()    Read message from mail box queue.
*
*  @param   mbox - struct osal_mailbox * - source mailbox
*
*  @param   cam_msg - struct osal_message * - where to store message
*
*  @return  non-zero on error.
*/
/* ========================================================================== */
int32 osal_mailbox_read(struct osal_mailbox * mbox,
                        struct osal_message * cam_msg)
{
    ssize_t           read_ = 0;
    unsigned          prio;
    int               ret = -1;
    char             *name;

    __assert(mbox && cam_msg);

    if (!mbox || !cam_msg) {
        return ret;
    }

    name = mbox->name[0] ? mbox->name : "null";

    read_ = mq_receive(mbox->fd, (char *)cam_msg, sizeof(*cam_msg), &prio);

    if (0 > read_) {

        mmsdbg(DL_ERROR, "Error Reading message from %s : %s",
           name ? name : "null", strerror(errno));

    } else if ((size_t) read_ != sizeof(*cam_msg)) {

        mmsdbg(DL_ERROR, "Message not complete read from %s", name);

    } else
        ret = 0;

    return (ret);

}


/* ========================================================================== */
/**
*  osal_mailbox_readtimed()    Read message from passed mail box Q. If message do not
*           arrive until specifed 'timeout' value in ms this call return -1;
*
*  @param   mbox - struct osal_mailbox * - source mailbox
*
*  @param   cam_msg -  struct osal_message * - where to store message.
*
*  @param   timeout - int - miliseconds, if 0 return immediately.
*
*  @return  non-zero on if there is not readed message.
*/
/* ========================================================================== */
int32 osal_mailbox_readtimed(struct osal_mailbox * mbox,
                                struct osal_message * cam_msg, int32 timeout)
{
    struct timespec   ts;
    osal_timeval tv;
    ssize_t read_ = 0;
    unsigned prio;
    int ret = -1;
    char *name;
    uint32 nsec;

    __assert(mbox && cam_msg);

    if (!mbox || !cam_msg) {
        return ret;
    }

    name = mbox->name[0] ? mbox->name : "null";

    /* Specify timeout as msec from now */
    (void) gettimeofday(&tv, NULL);

    nsec = (tv.tv_usec + (timeout * 1000)) * 1000;

    ts.tv_sec = tv.tv_sec;

    if (nsec >= (1000 * 1000 * 1000)) {
	    nsec -= (1000 * 1000 * 1000);
	    ts.tv_sec += 1;
    }

    ts.tv_nsec = nsec;

    /* receiving message */
    read_ = mq_timedreceive(mbox->fd, (char *)cam_msg,
                            sizeof(*cam_msg), &prio, &ts);

    if (0 > read_) {

        mmsdbg(DL_INFO, "Error reading message from %s TMO %d : %s", name,
               (int)timeout, strerror(errno));

    } else if ((size_t) read_ != sizeof(*cam_msg)) {

        mmsdbg(DL_ERROR, "Message not complete read from %s TMO %d",
               name, (int)timeout);

    } else
        ret = 0;

    return (ret);

}


/* ========================================================================== */
/**
*  osal_mailbox_write()    Write message in mail box queue.
*
*  @param   mbox - struct osal_mailbox * - mailbox desination.
*
*  @param   cam_msg - struct osal_message * - message to send
*
*  @return  non-zero on error.
*/
/* ========================================================================== */
int32 osal_mailbox_write(struct osal_mailbox * mbox,
                            struct osal_message * cam_msg)
{
    int               ret = -1;
    char             *name;

    __assert(mbox && cam_msg);

    if (!mbox || !cam_msg) {
        return ret;
    }

    name = mbox->name[0] ? mbox->name : "null";

    ret = (int)mq_send(mbox->fd, (const char *)cam_msg, sizeof(*cam_msg), 1);

    if (0 > ret) {

        mmsdbg(DL_ERROR, "Error sending message to %s : %s",
           name ? name : "null", strerror(errno));
    }

    return (ret);

}
