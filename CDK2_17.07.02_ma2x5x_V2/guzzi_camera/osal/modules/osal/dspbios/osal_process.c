/* =============================================================================
*   MultiMedia Solutions Ltd.
*   (c) Copyright 2006-2008, MultiMedia Solutions Ltd. All Rights Reserved.
*
*   Use of this software is controlled by the terms and conditions found
*   in the license agreement under which this software has been supplied.
* =========================================================================== */
/**
*	@file    osal_process-Linux.c
*
*	Contain wrapper functions for process
*
*	^path (TOP)/lib/osal/process/osal_process-Linux.c
*
*	@author  Ivan Ivanov  (MultiMedia Solutions Ltd.)
*
*	@date    18.7.2008
*
*	@version 1.10   29.07.2008  New implementation of all functions.
*
*	@version 1.00
*/
/* =============================================================================
*!
*! Revision History
*! ======================================================
*!	18.7.2008    : IIvanov  (MultiMedia Solutions Ltd.)
*!	 6.8.2008    : DMironov Re-Design
*!
*!
* =========================================================================== */
#include <osal/osal_stdio.h>
#include <osal/osal_sysdep.h>
#include <osal/osal_process.h>
#include <utils/mms_debug.h>

#ifdef __MMS_DEBUG__
#define MMSDEBUGLEVEL   vdl_osal_process
#else
#define MMSDEBUGLEVEL   0
#endif

int vdl_osal_process = DL_DEFAULT;

/* ========================================================================== */
/**
*  osal_process_Create()    Function should cerate new process and run it.
*
*  @param   info - struct osal_process_cntx * - handles to newly created process to be used
*           in osal_process_Wait().
*
*  @param   name - const char * - application name "c:\Windows\cmd.exe"
*
*  @param   cmd_line - const char * - application command line
*
*  @return  non-zero on error.
*/
/* ========================================================================== */
int osal_process_Create(
                    struct osal_process_cntx *info,
                    const char *name,
                    const char *cmd_line)
{
    int               /*pid,*/ ret=0;

 /*   pid = fork();

    switch (pid) {

        case 0:
            info->pid = pid;
            ret = execl(name, cmd_line, NULL);
            break;
        case -1:
            ret = -1;
            break;              // error

        default:
            mmsdbg(DL_MESSAGE, "3A process ID %d", pid);
            ret = 0;
            break;
    }
*/
    return ret;
}


/* ========================================================================== */
/**
*  osal_process_Exit()    wriapper to syscall 'exit'
*
*  @param   status - int - exit status
*
*  @return  never.
*/
/* ========================================================================== */
void osal_process_Exit(int status)
{
// TODO Port me    _exit(status);
}

/* ========================================================================== */
/**
*  osal_process_Wait()    Windows implementation for Linux wait syscall.
*
*  @param   info - struct osal_process_cntx * - handles returned from create_process()
*
*  @return  non-zero on error
*/
/* ========================================================================== */
int osal_process_Wait(struct osal_process_cntx *info)
{
/*  TODO port me

    int               status = 0;
    pid_t             process;

    process = waitpid(info->pid, &status, 0);

    return status;
*/
    return 0;
}

