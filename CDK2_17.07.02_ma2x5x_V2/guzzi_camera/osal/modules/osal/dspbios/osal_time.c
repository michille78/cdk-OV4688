/* =============================================================================
*   MultiMedia Solutions Ltd.
*   (c) Copyright 2006-2008, MultiMedia Solutions Ltd. All Rights Reserved.
*
*   Use of this software is controlled by the terms and conditions found
*   in the license agreement under which this software has been supplied.
* =========================================================================== */
/**
*	@file    osal_time-Linux.c
*
*	Contain wrapper functions for process
*
*	^path (TOP)/lib/osal/process/osal_time-Linux.c
*
*	@author  Stan (MultiMedia Solutions Ltd.)
*
*	@date    13.8.2008
*
*	@version 1.00
*/
/* =============================================================================
*!
*! Revision History
*! ======================================================
*!	13.8.2008    : Stan  (MultiMedia Solutions Ltd.)
*!
*!
* =========================================================================== */


/*To get tick period*/
#include <ti/sysbios/knl/Clock.h>
#include <platform/osal/timm_osal_interfaces.h>

#include <osal/osal_stdtypes.h>
#include <osal/osal_time.h>
#include <utils/mms_debug.h>
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Semaphore.h>
#include "framework/tools_library/tools_time.h"
#include "drivers/drv_timer/timer_msp.h"

#ifdef __MMS_DEBUG__
#define MMSDEBUGLEVEL   vdl_osal_time
#else
#define MMSDEBUGLEVEL   0
#endif

int vdl_osal_time = DL_DEFAULT;

//#define OSAL_USE_TIMER
//#define OSAL_USE_POOLING

#ifdef OSAL_USE_TIMER
static void osal_timer_callback(MSP_TimerId id, void *hApp, MSP_Timer_Interrupt intId)
{
    TIMM_OSAL_MutexRelease(hApp);
}

static MSP_ERROR_TYPE osal_timer_MSP_callback(MSP_PTR hMSP,
                                   MSP_PTR  pAppData,
                                   MSP_EVENT_TYPE tEvent,
                                   MSP_OPAQUE nEventData1,
                                   MSP_OPAQUE nEventData2)
{
	return MSP_ERROR_NONE;
}
#endif

int timeval_subtract (osal_timeval *result, osal_timeval *x, osal_timeval *y)
{
    /* Perform the carry for the later subtraction by updating y. */
    if (x->tv_usec < y->tv_usec) {
        int nsec = (y->tv_usec - x->tv_usec) / 1000000 + 1;
        y->tv_usec -= 1000000 * nsec;
        y->tv_sec += nsec;
    }

    if (x->tv_usec - y->tv_usec > 1000000) {
        int nsec = (x->tv_usec - y->tv_usec) / 1000000;
        y->tv_usec += 1000000 * nsec;
        y->tv_sec -= nsec;
    }

    /* Compute the time remaining to wait.
     tv_usec is certainly positive. */
    result->tv_sec = x->tv_sec - y->tv_sec;
    result->tv_usec = x->tv_usec - y->tv_usec;

    /* Return 1 if result is negative. */
    return x->tv_sec < y->tv_sec;
}

/* ========================================================================== */
/**
*  osal_get_time()    get relative time after system is booting
*
*  @param   time - unsinged long - current time in uSec
*
*  @return  non-zero on error.
*/
/* ========================================================================== */
int osal_get_time(unsigned long *time)
{
    int                 ret = 0;
    *time = (unsigned long) Tools_Time_getCurrent_1us();
    //*time = Clock_tickPeriod * Clock_getTicks();

    return ret;
}

/* ========================================================================== */
/**
*  osal_nsleep()   nanosleep() wrapper
*
*  @param   nsecs - unsigned int - sleep in nano seconds
*
*  @return  zero on success, negative if error.
*/
/* ========================================================================== */
int osal_nsleep(unsigned int nsecs)
{
// TODO better porting
    nsecs = nsecs/1000;
    if (nsecs < 50)
        nsecs = 50;
    TIMM_OSAL_SleepTask(nsecs);

	return 0;
}

/* ========================================================================== */
/**
*  osal_sleep()    sleep() wrapper.
*
*  @param   secs - unsigned int - in seconds
*
*  @return  non-zero on error
*/
/* ========================================================================== */
int osal_sleep(unsigned int secs)
{
    TIMM_OSAL_SleepTask(secs*1000);
    return 0;
}

/* ========================================================================== */
/**
*  osal_usleep()    usleep() wrapper.
*
*  @param   usecs - unsigned int - in micro seconds
*
*  @return  non-zero on error
*/
/* ========================================================================== */

int osal_usleep(unsigned int usecs)
{
#if defined(OSAL_USE_TIMER)
	uint32 i, status = FALSE;
    TIMM_OSAL_PTR timer_mutex;

	MSP_HANDLE timerHMSP;
	MSP_Timer_ConfigParam timer_cfg;
	MSP_Timer_CmdParam timer_cmd;
    MSP_APPCBPARAM_TYPE appcb;

    appcb.MSP_callback = osal_timer_MSP_callback;

    if (MSP_ERROR_NONE != TIMM_OSAL_MutexCreate(&timer_mutex))
        return 1;

	timer_cfg.timerCallback = osal_timer_callback;
	timer_cfg.arg = timer_mutex;

	MSP_init(&timerHMSP, "MSP.TIMER", MSP_PROFILE_DEFAULT, &appcb);

	if (timerHMSP != NULL) {

		for (i = 0; i < MSP_TIMER_GP_NUMBER_OF_TIMERS; i++) {
			timer_cfg.timerId = (MSP_TimerId)i;
			if (MSP_ERROR_NONE == MSP_open(timerHMSP, &timer_cfg)) {
				status = TRUE;
				break;
			}
			if (MSP_ERROR_NONE != MSP_close(timerHMSP))
				break;
		}

		if (TRUE == status) {
			timer_cmd.autoReload = MSP_FALSE;
			timer_cmd.mode = MSP_TIMER_GP_MODE_TIMER;
			timer_cmd.period = usecs;
			timer_cmd.unit = MSP_TIMER_GP_TIME_IN_us;
			if (MSP_ERROR_NONE == MSP_control(timerHMSP, MSP_CTRLCMD_START, (MSP_PTR)&timer_cmd)) {
				CASH_WA_ENB_INTERRUPTS();
				if (MSP_ERROR_NONE != TIMM_OSAL_MutexObtain(timer_mutex, TIMM_OSAL_SUSPEND))
				    return 1;
				if (MSP_ERROR_NONE != TIMM_OSAL_MutexDelete(timer_mutex))
			        return 1;
			}
		} else {
		    usecs /= 1000;
		    TIMM_OSAL_SleepTask(usecs);
		}
	}
#elif defined(OSAL_USE_POOLING)
	uint64 cur_time = Tools_Time_getCurrent_1us();
	uint64 target_time = cur_time + usecs;
	uint64 end;
	if (target_time < cur_time)
		while (Tools_Time_getCurrent_1us() > target_time)
			CASH_WA_ENB_INTERRUPTS();
	while (Tools_Time_getCurrent_1us() < target_time)
		CASH_WA_ENB_INTERRUPTS();
	end = Tools_Time_getCurrent_1us();
#else
	usecs /= 1000;
    TIMM_OSAL_SleepTask(usecs);
#endif
    return 0;
}


#include "framework/tools_library/tools_cam_timer.h"

/* ========================================================================== */
/**
*  AF_Timer_Create()    Creates required resources for AF Timer support
*
*  @param   void - none
*
*  @return  non-zero on error
*/
/* ========================================================================== */
int AF_Timer_Create(void** TimerHandler)
{
    volatile int wa = 0;
    wa ++;
return Tools_CAM_Timer_Create((TIMM_OSAL_PTR*)TimerHandler);
}

/* ========================================================================== */
/**
*  AF_Timer_delete()    Destroy all resources for AF Timer support
*
*  @param   void - none
*
*  @return  non-zero on error
*/
/* ========================================================================== */
int AF_Timer_Delete(void* TimerHandler)
{
    volatile int wa = 0;
    wa ++;
    return Tools_CAM_Timer_Delete((TIMM_OSAL_PTR)TimerHandler);
}

/* ========================================================================== */
/**
*  AF_Timer_Sleep()
*
*  @param   usecs - unsigned int - in micro seconds
*
*  @return  non-zero on error
*/
/* ========================================================================== */
int AF_Timer_Sleep(void* TimerHandler, unsigned int usecs)
{
    return Tools_CAM_Timer_Sleep((TIMM_OSAL_PTR)TimerHandler, (uint32)usecs);
}
