/* =============================================================================
*   MultiMedia Solutions AD
*   (c) Copyright 2006-2010, MultiMedia Solutions AD All Rights Reserved.
*
*   Use of this software is controlled by the terms and conditions found
*   in the license agreement under which this software has been supplied.
* =========================================================================== */
/**
*    @file    osal_process-Windows.c
*
*    Contain wrapper functions for process
*
*    ^path (TOP)/lib/osal/process/osal_process-Windows.c
*
*    @author  Ivan Ivanov  (MultiMedia Solutions AD)
*
*    @date    18.7.2008
*
*    @version 1.10   29.07.2008  New implementation of all functions.
*
*    @version 1.00
*/
/* =============================================================================
*!
*! Revision History
*! ======================================================
*!    18.7.2008    : IIvanov  (MultiMedia Solutions AD)
*!     6.8.2008    : DMironov Re-Design
*!
*!
* =========================================================================== */
#include <osal/osal_stdio.h>
#include <osal/osal_sysdep.h>
#include <utils/mms_debug.h>

#ifdef __MMS_DEBUG__
#define MMSDEBUGLEVEL   vdl_osal_process
#else
#define MMSDEBUGLEVEL   0
#endif

int vdl_osal_process = DL_DEFAULT;

/* ========================================================================== */
/**
*  osal_get_time()    get relative time after system is booting
*
*  @param   time - unsinged long - current time
*
*  @return  non-zero on error.
*/
/* ========================================================================== */
int osal_get_time(unsigned long *time)
{
#warning NOT IMPLEMENTED
}


/* ========================================================================== */
/**
*  osal_nsleep()   nanosleep() wrapper
*
*  @param   nsecs - unsigned int - sleep in nano seconds
*
*  @return  zero on success, negative if error.
*/
/* ========================================================================== */
int osal_nsleep(unsigned int nsecs)
{
#warning NOT IMPLEMENTED
}

/* ========================================================================== */
/**
*  osal_sleep()    sleep() wrapper.
*
*  @param   secs - unsigned int - in seconds
*
*  @return  non-zero on error
*/
/* ========================================================================== */
void osal_sleep(unsigned int secs)
{
    Sleep(seconds * 1000);
}

/* ========================================================================== */
/**
*  osal_usleep()    usleep() wrapper.
*
*  @param   usecs - unsigned int - in micro seconds
*
*  @return  non-zero on error
*/
/* ========================================================================== */
int osal_usleep(unsigned int usecs)
{
    Sleep(usecs);

    return 0;
}
