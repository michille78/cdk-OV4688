/* =============================================================================
*   MultiMedia Solutions AD
*   (c) Copyright 2006-2010, MultiMedia Solutions AD All Rights Reserved.
*
*   Use of this software is controlled by the terms and conditions found
*   in the license agreement under which this software has been supplied.
* =========================================================================== */
/**
*    @file    osal_process-Windows.c
*
*    Contain wrapper functions for process
*
*    ^path (TOP)/lib/osal/process/osal_process-Windows.c
*
*    @author  Ivan Ivanov  (MultiMedia Solutions AD)
*
*    @date    18.7.2008
*
*    @version 1.10   29.07.2008  New implementation of all functions.
*
*    @version 1.00
*/
/* =============================================================================
*!
*! Revision History
*! ======================================================
*!    18.7.2008    : IIvanov  (MultiMedia Solutions AD)
*!     6.8.2008    : DMironov Re-Design
*!
*!
* =========================================================================== */
#include <osal/osal_stdio.h>
#include <osal/osal_sysdep.h>
#include <utils/mms_debug.h>

#ifdef __MMS_DEBUG__
#define MMSDEBUGLEVEL   vdl_osal_process
#else
#define MMSDEBUGLEVEL   0
#endif

int vdl_osal_process = DL_DEFAULT;

