/* =============================================================================
*   MultiMedia Solutions AD
*   (c) Copyright 2006-2010, MultiMedia Solutions AD All Rights Reserved.
*
*   Use of this software is controlled by the terms and conditions found
*   in the license agreement under which this software has been supplied.
* =========================================================================== */
/**
*    @file    osal_process-Windows.c
*
*    Contain wrapper functions for process
*
*    ^path (TOP)/lib/osal/process/osal_process-Windows.c
*
*    @author  Ivan Ivanov  (MultiMedia Solutions AD)
*
*    @date    18.7.2008
*
*    @version 1.10   29.07.2008  New implementation of all functions.
*
*    @version 1.00
*/
/* =============================================================================
*!
*! Revision History
*! ======================================================
*!    18.7.2008    : IIvanov  (MultiMedia Solutions AD)
*!     6.8.2008    : DMironov Re-Design
*!
*!
* =========================================================================== */
#include <osal/osal_stdio.h>
#include <osal/osal_sysdep.h>
#include <utils/mms_debug.h>

#ifdef __MMS_DEBUG__
#define MMSDEBUGLEVEL   vdl_osal_process
#else
#define MMSDEBUGLEVEL   0
#endif

int vdl_osal_process = DL_DEFAULT;

/* ========================================================================== */
/**
*  osal_process_Create()    Function should cerate new process and run it.
*
*  @param   info - struct osal_process_cntx * - handles to newly created process to be used
*           in osal_process_Wait().
*
*  @param   name - const char * - application name "c:\Windows\cmd.exe"
*
*  @param   cmd_line - const char * - application command line
*
*  @return  non-zero on error.
*/
/* ========================================================================== */
int osal_process_Create(struct osal_process_cntx *info, const char *name,
                   const char *cmd_line)
{
    STARTUPINFO       si;
    PROCESS_INFORMATION pi;
    int ret;

    ZeroMemory(&si, sizeof(si));
    si.cb = sizeof(si);
    ZeroMemory(&pi, sizeof(pi));


    // Start the child process.
    if (!CreateProcess(name, cmd_line, NULL,    // Process handle not
                                                // inheritable
                       NULL,    // Thread handle not inheritable
                       FALSE,   // Set handle inheritance to FALSE
                       0,       // No creation flags
                       NULL,    // Use parent's environment block
                       NULL,    // Use parent's starting directory
                       &si,     // Pointer to STARTUPINFO structure
                       &pi)     // Pointer to PROCESS_INFORMATION structure
        ) {
        mmsdbg(DL_ERROR, "CreateProcess failed (%d)\n", GetLastError());
        ret = -1;

    } else {

        info.process = pi.hProcess;
        info.thread = pi.hThread;
    }

    return ret;
}

/* ========================================================================== */
/**
*  osal_process_Exit()    wriapper to syscall 'exit'
*
*  @param   status - int - exit status
*
*  @return  never.
*/
/* ========================================================================== */
void osal_process_Exit(int status)
{
    exit(status);
}

/* ========================================================================== */
/**
*  osal_process_Wait()    Windows implementation for Linux wait syscall.
*
*  @param   info - struct osal_process_cntx * - handles returned from create_process()
*
*  @return  non-zero on error
*/
/* ========================================================================== */
int osal_process_Wait(struct osal_process_cntx *info)
{
    //
    // WaitForSingleObject Function Waits until the specified object is in
    // the signaled state or the time-out interval elapses.To enter an
    // alertable wait state, use the WaitForSingleObjectEx function. To wait
    // for multiple objects, use the WaitForMultipleObjects.
    DWORD             ret;

    ret = WaitForSingleObject((HANDLE) pid, INFINITE);

    CloseHandle(info.process);
    CloseHandle(info.thread);

    if (WAIT_OBJECT_0 != ret)
        ret = -1;
    else
        ret = 0;

    return ret;
}
