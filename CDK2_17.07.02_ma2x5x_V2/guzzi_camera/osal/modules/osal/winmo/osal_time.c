/* =============================================================================
*   MultiMedia Solutions AD
*   (c) Copyright 2006-2010, MultiMedia Solutions AD All Rights Reserved.
*
*   Use of this software is controlled by the terms and conditions found
*   in the license agreement under which this software has been supplied.
* =========================================================================== */
/**
*	@file    osal_time-Symbian.c
*
*	Contain wrapper functions for process
*
*	^path (TOP)/lib/osal/process/osal_time-Linux.c
*
*	@author  Ivan Evlogiev (MultiMedia Solutions AD)
*
*	@date    01.9.2008
*
*	@version 1.00
*/
/* =============================================================================
*!
*! Revision History
*! ======================================================
*!	01.9.2008    : Ivan Evlogiev  (MultiMedia Solutions AD)
*!
*!
* =========================================================================== */
#include <windows.h>
#include <winbase.h>

#include <osal/osal_stdio.h>
#include <osal/osal_sysdep.h>
#include <osal/osal_process.h>

#include <utils/mms_debug.h>

#include "omap3430_3a.h"

#ifdef __MMS_DEBUG__
#define MMSDEBUGLEVEL   vdl_osal_time
#else
#define MMSDEBUGLEVEL   0
#endif

int vdl_osal_time = DL_DEFAULT;

/* ========================================================================== */
/**
*  osal_get_time()    get relative time after system is booting
*
*  @param   time - unsinged long - current time
*
*  @return  non-zero on error.
*/
/* ========================================================================== */
int osal_get_time(unsigned long *time)
{
#if 0
	QueryPerformanceCounter((LARGE_INTEGER *)time);
	*time *= 31;
#else
	unsigned long loc_time = (unsigned long)QueryPerformanceCounter((LARGE_INTEGER *)time);
	*time = ((loc_time & 0x7fff) * 30519) / 1000;
	if (*time > 1000000)
		*time = 0;
#endif
    return 0;
}


/* ========================================================================== */
/**
*  osal_nsleep()   nanosleep() wrapper
*
*  @param   nsecs - unsigned int - sleep in nano seconds
*
*  @return  zero on success, negative if error.
*/
/* ========================================================================== */
int osal_nsleep(unsigned int nsecs)
{
	RETAILMSG(ZONE_ERROR,(L"Warning  osal_nsleep() NOT IMPLEMENTED"));
	return 0;
}

/* ========================================================================== */
/**
*  osal_sleep()    sleep() wrapper.
*
*  @param   secs - unsigned int - in seconds
*
*  @return  non-zero on error
*/
/* ========================================================================== */
int osal_sleep(unsigned int secs)
{
   Sleep(secs * 1000);
   return 0;
}

/* ========================================================================== */
/**
*  osal_usleep()    usleep() wrapper.
*
*  @param   usecs - unsigned int - in micro seconds
*
*  @return  non-zero on error
*/
/* ========================================================================== */

int osal_usleep(unsigned int usecs)
{
LARGE_INTEGER ts, tc;
DWORD timeval;

   Sleep(usecs / 1000);
//   Sleep((usecs / 1000)+1);
   usecs = usecs % 1000;

   timeval = usecs/31;
   QueryPerformanceCounter(&ts);
   while(TRUE)
   {
	   QueryPerformanceCounter(&tc);
	   if ( (((long long)(tc.HighPart - ts.HighPart)<<32) + (tc.LowPart - ts.LowPart) )  >= timeval) break;
   }

   return 0;
}
