#include <osal/osal_string.h>
#include "omap3430_3a.h"

int osal_strcmp_fn (const char * str1, const char * str2)
{
	unsigned int i;
	for (i=0; str1[i]!='\0'&&str2[i]!='\0'; i++)
	{
		if (str1[i]<str2[i]) return -1;
		if (str1[i]>str2[i]) return 1;
	}
	if (str1[i]<str2[i]) return -1;
		if (str1[i]>str2[i]) return 1;
	return 0;
}
int osal_strncmp_fn (const char * str1, const char * str2, unsigned int num)
{
	unsigned int i;
	for (i=0; i<num; i++)
	{
		if (str1[i]<str2[i]) return -1;
		if (str1[i]>str2[i]) return 1;
	}
	return 0;
}

char * osal_strcpy_fn (char * dst, const char * src)
{
	unsigned int i;
	for (i=0; src[i]!='\0'; i++)
		dst[i]=src[i];
	dst[i]=src[i];
	return dst;
}

char * osal_strncpy_fn (char * dst, const char * src, unsigned int num)
{
	unsigned int i;
	for (i=0; i<num; i++)
		dst[i]=src[i];
	return dst;
}

int osal_memcmp_fn (void * p1, void * p2, size_t num)
{
	unsigned int i;
    char * c1, * c2;
    c1 = (char *) p1;
    c2 = (char *) p2;

	for (i=0; i<num; i++)
	{
		if (*c1<*c2) return -1;
		if (*c1>*c2) return 1;
		c1++;c2++;
	}
//	if (p1[i]<p2[i]) return -1;
//	if (p1[i]>p2[i]) return 1;
	return 0;
}

int osal_strlen_fn( char * str )
{
	int len;

	for( len = 0; str[ len++ ] != '\0'; )
        {}

	return (len-1);
}


int  osal_strspn_fn(char *string, char *strCharSet )
{
	//ERROR_PRINT("Please Implement osal_strspn for Symbian \n");
	return 0;
}

int  osal_strcat_fn(char *string, char *strCharSet ) {
//ERROR_PRINT("Please Implement osal_strcat for Symbian \n");
return 0;
}
char *  osal_strchr_fn(char *string, char strCharSet ) {
//ERROR_PRINT("Please Implement osal_strchr for Symbian \n");
return 0;
}
