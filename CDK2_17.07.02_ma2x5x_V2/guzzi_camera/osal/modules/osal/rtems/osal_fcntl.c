/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file osal_fcntl.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <osal/osal_stdio.h>
#include <osal/osal_sysdep.h>
#include <osal/osal_fcntl.h>

#include <utils/mms_debug.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

mmsdbg_define_variable(vdl_osal_fctnl, DL_DEFAULT, 0,
    "osalfctnl", "OSAL fctnl");
#define MMSDEBUGLEVEL   vdl_osal_fctnl

/* ========================================================================== */
/**
*  osal_dev_init()    open() wrapper.
*
*  @param   dev_name - const char * - name of the device to open
*
*  @return  non-zero on error.
*/
/* ========================================================================== */
int osal_dev_init(const char *dev_name)
{
    int ret;

    ret = open(dev_name, O_RDWR);
    if (ret < 0) {
        mmssys(DL_ERROR, "%s : ", dev_name)
    }

    return ret;
}


/* ========================================================================== */
/**
*  osal_dev_deinit()    close() wrapper.
*
*  @param   dev_id - int - device file descriptor
*
*  @return  non-zero on error.
*/
/* ========================================================================== */
int osal_dev_deinit(int dev_id)
{
   return close(dev_id);
}
