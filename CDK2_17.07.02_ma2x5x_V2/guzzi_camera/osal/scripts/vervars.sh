SCRIPTDIR=$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)
PRJDIR=$(dirname $SCRIPTDIR)
TOPDIR=$(dirname $PRJDIR)

function git_version {
    (cd $PRJDIR && git describe --long --dirty --always 2>/dev/null)
}

