SCRIPTDIR=$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd) # $TOPDIR/algorithms/source/scripts
PRJDIR=$(dirname $SCRIPTDIR) # $TOPDIR/algorithms/source
TOPDIR=$(dirname $(dirname $PRJDIR))

function git_version {
    (cd $PRJDIR && git describe --long --dirty --always 2>/dev/null)
}

