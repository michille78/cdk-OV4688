/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file af.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */


#include <osal/osal_stdlib.h>
#include <osal/osal_string.h>
#include <osal/osal_stdtypes.h>
#include "aca_af.h"
#include "aca_hllc.h"

typedef struct {
    int   af_seq_cnt;      // Iteration sequence counter
    float af_bfp;          // Best Focus Position
    focus_state_t af_state; // Focus state
    hat_cm_lens_move_list_t move_list;
} aca_af_context_t;

int aca_af_create(aca_af_hndl_t *aca_af_hndl)
{
    aca_af_context_t *af_ctx;

    af_ctx = osal_malloc(sizeof(aca_af_context_t));
    if (NULL == af_ctx)
    {
        return 1;
    }

    af_ctx->af_seq_cnt = 0;
    af_ctx->af_bfp = 0;
    af_ctx->af_state = AF_STATE_IDLE;

    af_ctx->move_list.lock = osal_mutex_create();

    *aca_af_hndl = af_ctx;
    return 0;
}

int aca_af_destroy(aca_af_hndl_t aca_af_hndl)
{
    aca_af_context_t *af_ctx = aca_af_hndl;
    osal_mutex_destroy(af_ctx->move_list.lock);
    osal_free(aca_af_hndl);
    return 0;
}

int aca_af_configuration(aca_af_hndl_t aca_af_hndl,
                                 aca_af_config_t* cfg){
    return 0;

}

int aca_af_process(aca_af_hndl_t aca_af_hndl,
                          aca_af_calc_input_t* af_inp,
                          aca_focus_output_t *out)
{

    aca_af_context_t *af_ctx = (aca_af_context_t *) aca_af_hndl;

//    if(af_inp->af_stats->seq_cnt != af_ctx->af_seq_cnt) {
//        af_ctx->af_state = AF_STATE_IDLE; // Waiting for appropriate statistics
//        goto EXIT;
//    }

    af_ctx->af_bfp = (af_inp->af_stats->ts.frame_number % 10) * 0.1;
    af_ctx->af_seq_cnt++;
    af_ctx->af_state = AF_STATE_RUNNING;

    out->focus_out.move = AF_LENS_MOVE_GLOBAL;
    out->focus_out.pos = af_ctx->af_bfp;
    out->focus_out.seq_cnt = af_ctx->af_seq_cnt;
    out->focus_out.status = af_ctx->af_state;
    return 0;

}
