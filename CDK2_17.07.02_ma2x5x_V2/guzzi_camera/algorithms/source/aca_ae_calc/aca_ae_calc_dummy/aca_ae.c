/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file aca_ae.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <osal/osal_stdlib.h>
#include <osal/osal_string.h>
#include <aca_ae_calc.h>

#include <utils/mms_debug.h>
mmsdbg_define_variable(
        vdl_aca_aec,
        DL_DEFAULT,
        0,
        "ACA AE calc",
        "ACA AE calc"
);
#define MMSDEBUGLEVEL mmsdbg_use_variable(vdl_aca_aec)

typedef struct
{
    const char *name;
    dtp_hndl_t hdtp;
    dtp_leaf_data_t leaf;
    dtpdb_static_private_t  dtp_skeys;
    dtpdb_dynamic_private_t dtp_dkeys;
    aca_ae_calc_cfg_t cfg;
    exposure_calc_output_t out;
}aca_ae_ctx_t;

static exposure_calc_output_t def_aca_ae_out =
{
    .req_texp = 88000,
    .illuminance = 800,
    .ave_y_level = 60,
    .converged   = 1,
    .flash_light_ratio = 0x0001000,
    .status = ACA_AE_STATUS_GOOD,
    .ft_flash_required = 0,
};

int aca_ae_calc_create(aca_ae_calc_hndl_t *aca_ae_calc_hndl)
{
    aca_ae_ctx_t *ae_ctx;

    if (NULL == aca_ae_calc_hndl)
    {
        mmsdbg(DL_ERROR, "NULL handle pointer");
        return -1;
    }

    *aca_ae_calc_hndl = NULL; // prepare return value in case of error
    ae_ctx = osal_malloc(sizeof(*ae_ctx));

    if (NULL == ae_ctx)
    {
        mmsdbg(DL_ERROR, "AWB - can't allocate handle");
        return -1;
    }

    osal_memset(ae_ctx, 0, sizeof(*ae_ctx));

    ae_ctx->name = "aca ae calc ctx";

    if (dtpsrv_get_hndl(dtp_srv_hndl,
                        DTP_DB_ID_ACA,
                        DTP_ID_ACA_ALG_AE_CALC,
                        1,
                        dtp_param_order,
                        &ae_ctx->hdtp, &ae_ctx->leaf))
    {
        mmsdbg(DL_ERROR, "AE calc - can't get DTP client handle");
    }

    *aca_ae_calc_hndl = ae_ctx;
    return 0;
}

int aca_ae_calc_destroy(aca_ae_calc_hndl_t aca_ae_calc_hndl)
{
    aca_ae_ctx_t *ae_ctx = aca_ae_calc_hndl;

    if (NULL == ae_ctx)
    {
        mmsdbg(DL_ERROR, "Invalid handle pointer");
        return -6;
    }

    if (ae_ctx->hdtp)
        dtpsrv_free_hndl(ae_ctx->hdtp);
    else
        mmsdbg(DL_ERROR, "NULL DTP handle");

    osal_free(ae_ctx);

    return 0;
}

int aca_ae_calc_config(aca_ae_calc_hndl_t aca_ae_calc_hndl, aca_ae_calc_cfg_t* cfg)
{
    aca_ae_ctx_t *ae_ctx = (aca_ae_ctx_t*)aca_ae_calc_hndl;

    dtpsrv_pre_process(ae_ctx->hdtp, cfg->dtp_s_common, &ae_ctx->dtp_skeys);

    *(&ae_ctx->cfg) = *cfg;
    return 0;
}

int aca_ae_calc_process(aca_ae_calc_hndl_t aca_ae_calc_hndl, aca_ae_calc_input_t* ae_inp,
                        exposure_calc_output_t *out)
{
    aca_ae_ctx_t *ae_ctx = (aca_ae_ctx_t*)aca_ae_calc_hndl;

//    if (dtpsrv_process(ae_ctx->hdtp, awb_inp->dtp_d_common, &ae_ctx->dtp_dkeys, (void**)&ae_dtp))

    ae_ctx->out = def_aca_ae_out; // TODO replace me with real algo

    *out = ae_ctx->out;
    return 0;
}
