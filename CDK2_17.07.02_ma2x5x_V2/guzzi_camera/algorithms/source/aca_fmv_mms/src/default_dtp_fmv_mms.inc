/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file default_dtp_fmv_mms.inc
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 28-May-2014 : Pavlin Georgiev ( MM Solutions AD )
*! Created
* =========================================================================== */

#ifndef _DEFAULT_DTP_FMV_MMS_
#define _DEFAULT_DTP_FMV_MMS_

fmv_mms_dtp_content_t fmv_mms_def_dtp_content = {
    {
        5,  // int32 stab_frm_cnt;
        13, //int32 stab_x_thr;
        13, //int32 stab_y_thr;
        11, //int32 stab_zoom_thr;
        6,  //int32 similar_x_thr;
        6,  //int32 similar_y_thr;
        6,  //int32 similar_zoom_thr;
        30, //int32 max_crl_err;
    } // h3afmv_tuning_data_t;
}; // fmv_mms_def_dtp_content


#endif //_DEFAULT_DTP_FMV_MMS_
