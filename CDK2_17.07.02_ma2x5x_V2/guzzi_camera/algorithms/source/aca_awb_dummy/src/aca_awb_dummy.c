/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file aca_awb_dummy.c
*
* @author ( MM Solutions AD )
 *
 */
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */
#include <aca_awb.h>
#include <osal/osal_string.h>
#include "osal/osal_stdlib.h"
#include <dtp/dtp_server_defs.h>

#include <utils/mms_debug.h>
mmsdbg_define_variable(
        vdl_aca_awb,
        DL_DEFAULT,
        0,
        "ACA AWB",
        "ACA AWB"
);
#define MMSDEBUGLEVEL mmsdbg_use_variable(vdl_aca_awb)

typedef struct
{
    const char *name;
    dtp_hndl_t hdtp;
    dtp_leaf_data_t leaf;
    dtpdb_static_private_t  dtp_skeys;
    dtpdb_dynamic_private_t dtp_dkeys;
    aca_awb_config_t cfg;
    awb_calc_output_t out;
}aca_awb_ctx_t;

static const awb_calc_output_t def_wb =
{
     { // wbal_coef
        .dgain.gain = 1.0,
        .c.b  = 1.57,
        .c.gb = 1.0,
        .c.gr = 1.0,
        .c.r  = 1.3,
        .o.b  = 0.0,
        .o.gb = 0.0,
        .o.gr = 0.0,
        .o.r  = 0.0
     },
    .colour_temp = 5000,
    .converged   = 1,
    .confidence  = 100,
    .state       = AWB_STATE_LOCKED,
};

int aca_awb_create(aca_awb_hndl_t *aca_awb_hndl)
{
    aca_awb_ctx_t *wb_ctx;

    if (NULL == aca_awb_hndl)
    {
        mmsdbg(DL_ERROR, "NULL handle pointer");
        return -1;
    }
    *aca_awb_hndl = NULL;   // prepare return value in case of error

    wb_ctx = osal_malloc(sizeof(aca_awb_ctx_t));
    if (NULL == wb_ctx)
    {
        mmsdbg(DL_ERROR, "AWB - can't allocate handle");
        return -1;
    }


    osal_memset(wb_ctx, 0, sizeof(*wb_ctx));

    wb_ctx->name = "aca wb ctx";

    if (dtpsrv_get_hndl(dtp_srv_hndl,
                        DTP_DB_ID_ACA,
                        DTP_ID_ACA_ALG_AWB_CALC,
                        1,
                        dtp_param_order,
                        &wb_ctx->hdtp, &wb_ctx->leaf))
    {
        mmsdbg(DL_ERROR, "AWB - can't get DTP client handle");
    }

    *aca_awb_hndl = wb_ctx;

    return 0;
}

int aca_awb_destroy(aca_awb_hndl_t aca_awb_hndl)
{
    aca_awb_ctx_t *wb_ctx = aca_awb_hndl;

    if (NULL == wb_ctx)
    {
        mmsdbg(DL_ERROR, "Invalid handle pointer");
        return -6;
    }

    if (wb_ctx->hdtp)
        dtpsrv_free_hndl(wb_ctx->hdtp);
    else
        mmsdbg(DL_ERROR, "NULL DTP handle");

    osal_free(wb_ctx);
    return 0;
}

int aca_awb_configuration(aca_awb_hndl_t aca_awb_hndl, aca_awb_config_t* cfg)
{
    aca_awb_ctx_t *wb_ctx = (aca_awb_ctx_t*)aca_awb_hndl;

    //dtpsrv_pre_process(wb_ctx->hdtp, cfg->dtp_s_common, &wb_ctx->dtp_skeys);

    *(&wb_ctx->cfg) = *cfg;
    return 0;
}

int aca_awb_process(aca_awb_hndl_t aca_awb_hndl, aca_awb_calc_input_t* awb_inp,
                      awb_calc_output_t *out)
{
    aca_awb_ctx_t *wb_ctx = (aca_awb_ctx_t*)aca_awb_hndl;

//    if (dtpsrv_process(wb_ctx->hdtp, awb_inp->dtp_d_common, &wb_ctx->dtp_dkeys, (void**)&wb_dtp))

    wb_ctx->out = def_wb; // TODO replace me with real algo

    *out = wb_ctx->out;
    return 0;
}


