/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file sg_ae_smooth.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include "osal/osal_stdlib.h"
#include "osal/osal_string.h"
#include <sg_ae_smooth.h>
#include <dtp/dtp_server_defs.h>

#include <utils/mms_debug.h>
mmsdbg_define_variable(
        vdl_sg_smooth,
        DL_DEFAULT,
        0,
        "SG smooth",
        "SG smooth"
);
#define MMSDEBUGLEVEL mmsdbg_use_variable(vdl_sg_smooth)

typedef struct
{
    const char *name;
    dtp_hndl_t hdtp;
    dtp_leaf_data_t leaf;
    dtpdb_static_private_t  dtp_skeys;
    dtpdb_dynamic_private_t dtp_dkeys;
    sg_ae_smooth_config_t cfg;
    ae_smooth_output_t out;
}sg_ae_smmoth_ctx_t;

int sg_ae_smooth_create(sg_ae_smooth_hndl_t *sg_ae_smooth_hndl)
{
    sg_ae_smmoth_ctx_t *smooth_ctx;

    if (NULL == sg_ae_smooth_hndl)
    {
        mmsdbg(DL_ERROR, "NULL handle pointer");
        return -1;
    }
    *sg_ae_smooth_hndl = NULL;   // prepare return value in case of error

    smooth_ctx = osal_malloc(sizeof(*smooth_ctx));
    if (!smooth_ctx) {
        mmsdbg(DL_ERROR, "AE Smooth - can't allocate handle");
        return -1;
    }

    osal_memset (smooth_ctx, 0, sizeof(*smooth_ctx));

    smooth_ctx->name = "sg ae smooth ctx";

    if (dtpsrv_get_hndl(dtp_srv_hndl,
                        DTP_DB_ID_SG,
                        DTP_ID_SG_ALG_AE_SMOOTH,
                        1,
                        dtp_param_order,
                        &smooth_ctx->hdtp, &smooth_ctx->leaf))
    {
        mmsdbg(DL_ERROR, "AE Smooth - can't get DTP client handle");
    }

    *sg_ae_smooth_hndl = smooth_ctx;

    return 0;
}

int sg_ae_smooth_destroy(sg_ae_smooth_hndl_t sg_ae_smooth_hndl)
{
    sg_ae_smmoth_ctx_t *smooth_ctx = sg_ae_smooth_hndl;

    if (NULL == smooth_ctx)
    {
        mmsdbg(DL_ERROR, "Invalid handle pointer");
        return -6;
    }

    if (smooth_ctx->hdtp)
        dtpsrv_free_hndl(smooth_ctx->hdtp);
    else
        mmsdbg(DL_ERROR, "NULL DTP handle");

    osal_free(sg_ae_smooth_hndl);
    return 0;
}

int sg_ae_smooth_config(sg_ae_smooth_hndl_t sg_ae_smooth_hndl, sg_ae_smooth_config_t* cfg)
{
    sg_ae_smmoth_ctx_t *smooth_ctx = sg_ae_smooth_hndl;

    dtpsrv_pre_process(smooth_ctx->hdtp, cfg->dtp_s_common, &smooth_ctx->dtp_skeys);

    *(&smooth_ctx->cfg) = *cfg;
    return 0;
}

int sg_ae_smooth_process(sg_ae_smooth_hndl_t sg_ae_smooth_hndl,sg_ae_smooth_input_t* smooth_inp,
                         ae_smooth_output_t *smooth_out)
{
    sg_ae_smmoth_ctx_t *smooth_ctx = sg_ae_smooth_hndl;

//    if (dtpsrv_process(smooth_ctx->hdtp, smooth_inp->dtp_d_common, &smooth_ctx->dtp_dkeys, (void**)&smooth_dtp))

    smooth_ctx->out.texp = smooth_inp->texp;
    smooth_ctx->out.ae_change_ratio = smooth_inp->ae_change_ratio;

    *smooth_out = smooth_ctx->out;
    return 0;
}
