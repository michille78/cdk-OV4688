/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file af_fw_ancillary.h
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#ifndef AF_FW_ANCILLARY_H_
#define AF_FW_ANCILLARY_H_

#include "osal/osal_stdtypes.h"

//! \brief Maximum number of AF iterations
#define ANCILLARY_MAX_DYNAMIC_PAGES    (150)
#define ANCILLARY_FOCUS_AREA_NUMBER (9)

#define MAX_FILT_INFO (520)

//! \brief Dynamic part of Maker note data
struct af_ancillary_dynamic {
    uint32  maker_note_version;             //!< Edition date and version of Maker note data, HEX -> 0xYYMMDDVV
    uint32  exposure_time;                  //!< Exposure time of electronic rolling shutter, msec
    uint16  analog_gain;                    //!< Analog gain, ev

    // padding 2 bytes
    uint16  dummy10;

    uint32  lens_position;                  //!< Lens position after lens movement. Value from photo-reflector sensor
    uint32  focus_value_sum[ANCILLARY_FOCUS_AREA_NUMBER];  //!< Sum of focus values. Separately for each AF point (9 points)                //!< Result of focusing operation. Auto Focus status (same, as in Image ancillary data, AVE Host messaging API [1])
};

struct af_ancillary {

    uint32  maker_note_version;             //!< Edition date and version of Maker note data, HEX -> 0xYYMMDDVV
    uint16  af_alg_ver;                     //!< af algorithm version
    uint8   focus_mode;                     //!< Focus mode. Constants

    uint32  final_lens_position;            //!< Final lens position after auto focusing has been completed. Value from photo-reflector sensor. Actual position, (even if focus failed)
    uint8   af_iterations;                  //!< Number of lens focusing steps
    uint8   framer_counter;                 //!< Number of frames, counted from the moment of AF starting to moment of AF finishing
    uint8   number_skipped_frms;            //!< number of skipped frames for AF iteration, For example, due to long exposure time or lens actuator delay

    uint32  af_window_tag;                  //!< Bitwise window tag selected after AF performing
    uint8   af_status;                      //!< Result of focusing operation. Auto Focus status (same, as in �Image ancillary data�, AVE Host messaging API [1])
    struct af_ancillary_dynamic    dyn[ANCILLARY_MAX_DYNAMIC_PAGES];
};

struct af_filter_info {
    int32 cur_lens_pos;
    int32 fv_sum_1;
    int32 fv_sum_2;
    int32 green_sum;
    int32 fv_1_to_green;
    int32 fv_2_to_green;
};

struct af_filters_data {
    int32   cnt;
    uint32  exposure_time;   /* Current AE request exposure time. */
    float  analog_gain;     /* Current AE request analog gain. */
    struct af_filter_info af_filt_info[MAX_FILT_INFO];
};

#endif /* AF_FW_ANCILLARY_H_ */
