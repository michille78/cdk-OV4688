/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file aca_ae_stab.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <osal/osal_stdlib.h>
#include <osal/osal_string.h>
#include <aca_ae_stab.h>

#include <utils/mms_debug.h>
mmsdbg_define_variable(
        vdl_aca_aes,
        DL_DEFAULT,
        0,
        "ACA AE stab",
        "ACA AE stab"
);
#define MMSDEBUGLEVEL mmsdbg_use_variable(vdl_aca_aes)

typedef struct
{
    const char *name;
    dtp_hndl_t hdtp;
    dtp_leaf_data_t leaf;
    dtpdb_static_private_t  dtp_skeys;
    dtpdb_dynamic_private_t dtp_dkeys;
    aca_ae_stab_cfg_t cfg;
    ae_stab_output_t  out;
}aca_ae_stab_ctx_t;

int aca_ae_stab_create(aca_ae_stab_hndl_t *aca_ae_stab_hndl)
{
    aca_ae_stab_ctx_t *stab_ctx;

    if (NULL == aca_ae_stab_hndl)
    {
        mmsdbg(DL_ERROR, "NULL handle pointer");
        return -1;
    }

    *aca_ae_stab_hndl = NULL; // prepare return value in case of error

    stab_ctx = osal_malloc(sizeof(*stab_ctx));

    if(NULL == stab_ctx)
    {
        mmsdbg(DL_ERROR, "AE stab - can't allocate handle");
        return -1;
    }

    *aca_ae_stab_hndl = stab_ctx;

    osal_memset(stab_ctx, 0, sizeof(*stab_ctx));

    stab_ctx->name = "aca ae stab ctx";

    if (dtpsrv_get_hndl(dtp_srv_hndl,
                        DTP_DB_ID_ACA,
                        DTP_ID_ACA_ALG_AE_STAB,
                        1,
                        dtp_param_order,
                        &stab_ctx->hdtp, &stab_ctx->leaf))
    {
        mmsdbg(DL_ERROR, "AE stab - can't get DTP client handle");
    }

    return 0;
}

int aca_ae_stab_destroy(aca_ae_stab_hndl_t aca_ae_stab_hndl)
{
    aca_ae_stab_ctx_t *stab_ctx = aca_ae_stab_hndl;

    if (NULL == stab_ctx)
    {
        mmsdbg(DL_ERROR, "Invalid handle pointer");
        return -6;
    }

    if (stab_ctx->hdtp)
        dtpsrv_free_hndl(stab_ctx->hdtp);
    else
        mmsdbg(DL_ERROR, "NULL DTP handle");

    osal_free(stab_ctx);
    return 0;
}

int aca_ae_stab_config(aca_ae_stab_hndl_t aca_ae_stab_hndl, aca_ae_stab_cfg_t* cfg)
{
    aca_ae_stab_ctx_t *stab_ctx = aca_ae_stab_hndl;

    dtpsrv_pre_process(stab_ctx->hdtp, cfg->dtp_s_common, &stab_ctx->dtp_skeys);

    *(&stab_ctx->cfg) = *cfg;
    return 0;
}

int aca_ae_stab_process(aca_ae_stab_hndl_t aca_ae_stab_hndl, aca_ae_stab_input_t* ae_inp,
                        ae_stab_output_t *out)
{
    aca_ae_stab_ctx_t *stab_ctx = aca_ae_stab_hndl;

    //    if (dtpsrv_process(ae_ctx->hdtp, awb_inp->dtp_d_common, &ae_ctx->dtp_dkeys, (void**)&ae_dtp))

    stab_ctx->out.texp = ae_inp->texp; // TODO replace me with real algo
    stab_ctx->out.ae_change_ratio = ae_inp->ae_change_ratio;

    *out = *(&stab_ctx->out);
    return 0;
}




