/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file sg_ae_limits.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include "osal/osal_stdlib.h"
#include "osal/osal_string.h"
#include <sg_ae_limits.h>
#include <dtp/dtp_server_defs.h>

#include <utils/mms_debug.h>
mmsdbg_define_variable(
        vdl_sg_limits,
        DL_DEFAULT,
        0,
        "SG limits",
        "SG limits"
);
#define MMSDEBUGLEVEL mmsdbg_use_variable(vdl_sg_limits)



typedef struct
{
    const char *name;
    dtp_hndl_t hdtp;
    dtp_leaf_data_t leaf;
    dtpdb_static_private_t  dtp_skeys;
    dtpdb_dynamic_private_t dtp_dkeys;
    sg_ae_limits_config_t cfg;
    sg_ae_limits_output_t out;
}sg_ael_ctx_t;

multistage_ae_t def_multistage =
{
    {
        34000, 60000, 83000, 0, 0, 0, 0, 0, 0, 0,
    },
    {
        2.0, 4.0, 7.0, 0, 0, 0, 0, 0, 0, 0,
    },
};

int sg_ae_limits_create(sg_ae_limits_hndl_t *sg_ae_limits_hndl)
{
    sg_ael_ctx_t *ael_ctx;

    if (NULL == sg_ae_limits_hndl)
    {
        mmsdbg(DL_ERROR, "NULL handle pointer");
        return -1;
    }
    *sg_ae_limits_hndl = NULL;   // prepare return value in case of error

    ael_ctx = osal_malloc(sizeof(*ael_ctx));
    if (!ael_ctx) {
        mmsdbg(DL_ERROR, "AE Distribution - can't allocate handle");
        return -1;
    }

    osal_memset (ael_ctx, 0, sizeof(*ael_ctx));

    *sg_ae_limits_hndl = ael_ctx;

    ael_ctx->name = "sg ae distr ctx";

    if (dtpsrv_get_hndl(dtp_srv_hndl,
                        DTP_DB_ID_ACA,
                        DTP_ID_SG_ALG_AE_LIMITS,
                        1,
                        dtp_param_order,
                        &ael_ctx->hdtp, &ael_ctx->leaf))
    {
        mmsdbg(DL_ERROR, "AE Distribution - can't get DTP client handle");
    }

    return 0;
}

int sg_ae_limits_destroy(sg_ae_limits_hndl_t sg_ae_limits_hndl)
{
    sg_ael_ctx_t *ael_ctx = sg_ae_limits_hndl;


    if (NULL == ael_ctx)
    {
        mmsdbg(DL_ERROR, "Invalid handle pointer");
        return -6;
    }

    if (ael_ctx->hdtp)
        dtpsrv_free_hndl(ael_ctx->hdtp);
    else
        mmsdbg(DL_ERROR, "NULL DTP handle");

    osal_free(sg_ae_limits_hndl);
    return 0;
}

int sg_ae_limits_config(sg_ae_limits_hndl_t sg_ae_limits_hndl, sg_ae_limits_config_t* cfg)
{
    sg_ael_ctx_t *ael_ctx = sg_ae_limits_hndl;

    if ( NULL == ael_ctx || NULL == cfg )
     {
         mmsdbg(DL_ERROR, "NULL pointer %d", __LINE__);
         return -1;
     }

    dtpsrv_pre_process(ael_ctx->hdtp, cfg->dtp_s_common, &ael_ctx->dtp_skeys);

    *(&ael_ctx->cfg) = *cfg;
    return 0;
}

int sg_ae_limits_process(sg_ae_limits_hndl_t sg_ae_limits_hndl, sg_ae_limits_input_t* limits_inp,
                           sg_ae_limits_output_t *out)
{
    sg_ael_ctx_t *ael_ctx = sg_ae_limits_hndl;

    if ( NULL == ael_ctx || NULL == limits_inp || NULL == limits_inp )
     {
         mmsdbg(DL_ERROR, "NULL pointer %d", __LINE__);
         return -1;
     }

    ael_ctx->out.limits.shutter.max = 33000;
    ael_ctx->out.limits.shutter.min = 100;
    ael_ctx->out.limits.again.max = 8.0f;
    ael_ctx->out.limits.again.min = 1.0f;
    ael_ctx->out.limits.dgain.max = 8.0f;
    ael_ctx->out.limits.dgain.min = 1.0f;
    ael_ctx->out.limits.aperture_exp.max = 33000;
    ael_ctx->out.limits.aperture_exp.min = 100;
    ael_ctx->out.limits.aperture_gain.max = 8.0f;
    ael_ctx->out.limits.aperture_gain.min = 1.0f;
    ael_ctx->out.multistage = def_multistage;
    ael_ctx->out.ae_change_ratio = limits_inp->ae_change_ratio;

    *out = *(&ael_ctx->out);
    return 0;
}
