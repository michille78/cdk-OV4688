/* =============================================================================
* Copyright (c) 2016 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file profile_parser.h
*
* @author Ivaylo Georgiev ( MM Solutions AD )
*
*/

#ifndef __LSC_TUNER_INTERFACE_H__
#define __LSC_TUNER_INTERFACE_H__

#include "hal/hat_lsc.h"

typedef enum {
    LSC_TUNER_OK = 0,
    LSC_TUNER_ALLOCATION_ERROR,
    LSC_TUNER_CONFIGURATION_ERROR,
    LSC_TUNER_CSVR_INTERNAL_ERROR,
} lsc_tuner_error_t;

typedef struct{
    uint32_t width;
    uint32_t height;
    uint32_t sensor_id;
    uint32_t data_interleaved_flag;
    float    *table;
} lsc_profile_t;

typedef struct{
    uint32_t width_lsc;
    uint32_t height_lsc;
    uint32_t width_profile;
    uint32_t height_profile;
    uint32_t sensor_id;
    int32_t  *table;
} lsc_csvr_interpolater_t;

int32 lsc_tuner_create(void **hndl, lsc_profile_t *gold_profile, lsc_profile_t *cur_profile, lsc_csvr_interpolater_t *csvr);
int32 lsc_tuner_exec(void *hndl, hat_lsc_t *dtp_lsc);
int32 lsc_tuner_destroy(void *hndl);

#endif //__LSC_TUNER_INTERFACE_H__