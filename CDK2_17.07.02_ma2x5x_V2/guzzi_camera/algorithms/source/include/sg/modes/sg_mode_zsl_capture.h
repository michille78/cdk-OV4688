/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file sg_mode_zsl_capture.h
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#ifndef SG_MODE_ZSL_CAPTURE_H_
#define SG_MODE_ZSL_CAPTURE_H_

#include <dtp/dtp_server_defs.h>
#include "sg_ae_smooth_types.h"
#include "sg_ae_distribution_types.h"
#include "algo_vpipe.h"
#include "aca_ae_calc_types.h"
#include "aca_ae_stab_types.h"
#include "aca_awb_types.h"
#include "aca_af_types.h"
#include "camera/vcamera_iface/virt_cm/inc/virt_cm.h"
#include "control_logic/include/cl_algo_helper.h"

/**
 *
 */
typedef void* sga_zsl_cap_hndl_t;

/**
 *
 */
typedef struct
{
    dtpdb_static_common_t   *dtp_s_common;
    cam_gzz_cfg_t           *gzz_cfg;
} sga_zsl_cap_config_t;

/**
 *
 */
typedef struct
{
    dtpdb_dynamic_common_t *dtp_d_common;

    exposure_calc_output_t  *ae_out;
    ae_stab_output_t        *ae_stab_out;
    awb_calc_output_t       *awb_out;
    focus_calc_output_t     *focus_out;
    virt_cm_sen_mode_features_t    *vsen_modes;
    aca_afd_calc_output_t   *afd_out;
    sg_ae_smooth_mode_t     ae_smooth_mode;
    sg_ae_distr_calc_modes_t ae_distr_mode;
} sga_zsl_cap_input_t;

/**
 *
 */
typedef struct
{
    ae_smooth_output_t      *ae_smooth;
    ae_distr_calc_output_t  *ae_distr;
    vpipe_ctrl_settings_t    **vpipe;
} sga_zsl_cap_output_t;

/**
 *
 */
int sga_zsl_cap_create(sga_zsl_cap_hndl_t* zsl_cap_hndl);

/**
 *
 */
int sga_zsl_cap_destroy(sga_zsl_cap_hndl_t zsl_cap_hndl);

/**
 *
 */
int sga_zsl_cap_configuration(sga_zsl_cap_hndl_t zsl_cap_hndl,
                                cl_algs_hdl_t* halgs,
                                sga_zsl_cap_config_t* zsl_cap_cfg);

/**
 *
 */
int sga_zsl_cap_process(sga_zsl_cap_hndl_t zsl_cap_hndl,
                                cl_algs_hdl_t* halgs,
                                sga_zsl_cap_input_t* zsl_cap_input,
                                sga_zsl_cap_output_t* template_out);


#endif /* SG_MODE_ZSL_CAPTURE_H_ */
