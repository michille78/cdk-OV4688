/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file sg_mode_afd_preview.h
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#ifndef SG_MODE_AFD_PREVIEW_H_
#define SG_MODE_AFD_PREVIEW_H_

#include <dtp/dtp_server_defs.h>
#include "sg_ae_smooth_types.h"
#include "sg_ae_distribution_types.h"
#include "sg_afd_distribution_types.h"
#include "algo_vpipe.h"
#include "aca_ae_calc_types.h"
#include "aca_ae_stab_types.h"
#include "aca_awb_types.h"
#include "aca_af_types.h"
#include "control_logic/include/cl_algo_helper.h"
/**
 *
 */
typedef void* sga_afd_prv_hndl_t;

/**
 *
 */
typedef struct
{
    dtpdb_static_common_t   *dtp_s_common;
    cam_gzz_cfg_t           *gzz_cfg;
} sga_afd_prv_config_t;

/**
 *
 */
typedef struct
{
    dtpdb_dynamic_common_t *dtp_d_common;

    exposure_calc_output_t  *ae_out;
    ae_stab_output_t        *ae_stab_out;
    awb_calc_output_t       *awb_out;
    focus_calc_output_t     *focus_out;
    virt_cm_sen_mode_features_t    *vsen_modes;
    sg_ae_smooth_mode_t     ae_smooth_mode;
    sg_ae_distr_calc_modes_t ae_distr_mode;
    sg_afd_distr_modes_t     afd_distr_mode;
} sga_afd_prv_input_t;

/**
 *
 */
typedef struct
{
    ae_smooth_output_t      *ae_smooth;
    ae_distr_calc_output_t  *ae_distr;
    vpipe_ctrl_settings_t    **vpipe;
} sga_afd_prv_output_t;

/**
 *
 */
int sga_afd_prv_create(sga_afd_prv_hndl_t* afd_prv_hndl);

/**
 *
 */
int sga_afd_prv_destroy(sga_afd_prv_hndl_t afd_prv_hndl);

/**
 *
 */
int sga_afd_prv_configuration(sga_afd_prv_hndl_t afd_prv_hndl,
                                cl_algs_hdl_t* halgs,
                                sga_afd_prv_config_t* afd_prv_cfg);

/**
 *
 */
int sga_afd_prv_process(sga_afd_prv_hndl_t afd_prv_hndl,
                                cl_algs_hdl_t* halgs,
                                sga_afd_prv_input_t* afd_prv_input,
                                sga_afd_prv_output_t* template_out);

#endif /* SG_MODE_AFD_PREVIEW_H_ */
