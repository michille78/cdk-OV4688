/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file sg_mode_template.h
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#ifndef SG_MODE_TEMPLATE_H_
#define SG_MODE_TEMPLATE_H_

#include <osal/osal_stdtypes.h>
#include <dtp/dtp_server_defs.h>
#include <system_config.h>

/**
 *
 */
typedef void* sga_template_hndl_t;

/**
 *
 */
typedef struct
{
    dtpdb_static_common_t *dtp_s_common;
} sga_template_config_t;

/**
 *
 */
typedef struct
{
    dtpdb_dynamic_common_t  *dtp_d_common;

    exposure_calc_output_t  *ae_out;
    ae_stab_output_t        *ae_stab_out;
    awb_calc_output_t       *awb_out;
    focus_calc_output_t     *focus_out;

} sga_template_input_t;

/**
 *
 */
typedef struct
{
    hat_template_t *template;
    ae_smooth_output_t      *ae_smooth;
    ae_distr_calc_output_t  *ae_distr;
    apipe_settings_t        *apipe;
} sga_template_output_t;

/**
 *
 */
int sga_template_create(sga_template_hndl_t* template_hndl);

/**
 *
 */
int sga_template_destroy(sga_template_hndl_t template_hndl);

/**
 *
 */
int sga_template_config(sga_template_hndl_t template_hndl,
                           sga_template_config_t* template_cfg);

/**
 *
 */
int sga_template_prepare(sga_template_hndl_t    template_hndl,
                            sga_template_input_t*  template_input,
                            sga_template_output_t* template_out);




#endif /* SG_MODE_TEMPLATE_H_ */
