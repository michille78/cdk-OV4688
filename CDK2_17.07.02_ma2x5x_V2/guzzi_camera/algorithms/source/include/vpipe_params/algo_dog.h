/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file algo_dog.h
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#ifndef ALGO_DOG_H_
#define ALGO_DOG_H_

#include <hal/hat_dog.h>
#include <camera/vcamera_iface/vpipe_control/include/vpipe_control.h>
#include <dtp/dtp_server_defs.h>

/**
 *
 */
typedef void* algo_dog_hndl_t;

/**
 *
 */
typedef struct
{
    dtpdb_static_common_t *dtp_s_common;
} algo_dog_config_t;

/**
 *
 */
typedef struct
{
    dtpdb_dynamic_common_t *dtp_d_common;
} algo_dog_input_t;

/**
 *
 */
typedef struct
{
    vpipe_ctrl_dog_t dog;
} algo_dog_output_t;

/**
 *
 */
int algo_dog_create(algo_dog_hndl_t* dog_hndl);

/**
 *
 */
int algo_dog_destroy(algo_dog_hndl_t dog_hndl);

/**
 *
 */
int algo_dog_configuration(algo_dog_hndl_t dog_hndl,
                            algo_dog_config_t* dog_cfg);

/**
 *
 */
int algo_dog_process(algo_dog_hndl_t dog_hndl, algo_dog_input_t* dog_input,
                      algo_dog_output_t* dog_out);

#endif /* ALGO_DOG_H_ */
