/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file algo_vpipe.h
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#ifndef ALGO_VPIPE_H_
#define ALGO_VPIPE_H_

#include "cam_algo_state.h"
#include <dtp/dtp_server_defs.h>
#include <camera/vcamera_iface/vpipe_control/include/vpipe_control.h>
#include "camera/vcamera_iface/virt_cm/inc/virt_cm.h"

/**
 *vpipe_ctrl_settings_t
 */
typedef void* algo_vpipe_hndl_t;

/**
 *
 */
typedef struct
{
    dtpdb_static_common_t *dtp_s_common;
    cam_gzz_cfg_t *cfg;
} algo_vpipe_config_t;

/**
 *
 */
typedef struct
{
    dtpdb_dynamic_common_t *dtp_d_common;

    ae_stab_output_t        *ae_stab_out;
    awb_calc_output_t       *awb_out;
    focus_calc_output_t     *focus_out;
    virt_cm_sen_mode_features_t *vsen_modes;
} algo_vpipe_input_t;

/**
 *
 */
typedef struct
{
    vpipe_ctrl_settings_t **vpipe;
} algo_vpipe_output_t;

/**
 *
 */
int algo_vpipe_create(algo_vpipe_hndl_t* vpipe_hndl, uint32 max_fr);

/**
 *
 */
int algo_vpipe_destroy(algo_vpipe_hndl_t vpipe_hndl);

/**
 *
 */
int algo_vpipe_configuration(algo_vpipe_hndl_t vpipe_hndl,
                             algo_vpipe_config_t* vpipe_cfg);

/**
 *
 */
int algo_vpipe_process(algo_vpipe_hndl_t vpipe_hndl,
                       algo_vpipe_input_t* vpipe_input,
                       algo_vpipe_output_t* vpipe_out);

#endif /* ALGO_VPIPE_H_ */
