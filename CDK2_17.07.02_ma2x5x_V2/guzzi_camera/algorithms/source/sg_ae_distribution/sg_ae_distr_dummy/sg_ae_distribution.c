/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file sg_ae_distribution.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include "osal/osal_stdlib.h"
#include "osal/osal_string.h"
#include <sg_ae_distribution.h>
#include <dtp/dtp_server_defs.h>

#include <utils/mms_debug.h>
mmsdbg_define_variable(
        vdl_sg_distr,
        DL_DEFAULT,
        0,
        "SG distr",
        "SG distr"
);
#define MMSDEBUGLEVEL mmsdbg_use_variable(vdl_sg_distr)



typedef struct
{
    const char *name;
    dtp_hndl_t hdtp;
    dtp_leaf_data_t leaf;
    dtpdb_static_private_t  dtp_skeys;
    dtpdb_dynamic_private_t dtp_dkeys;
    sg_ae_distr_config_t cfg;
    ae_distr_calc_output_t out;
}sg_distr_ae_ctx_t;

int sg_distr_ae_create(sg_distr_ae_hndl_t *sg_distr_ae_hndl)
{
    sg_distr_ae_ctx_t *distr_ctx;

    if (NULL == sg_distr_ae_hndl)
    {
        mmsdbg(DL_ERROR, "NULL handle pointer");
        return -1;
    }
    *sg_distr_ae_hndl = NULL;   // prepare return value in case of error

    distr_ctx = osal_malloc(sizeof(*distr_ctx));
    if (!distr_ctx) {
        mmsdbg(DL_ERROR, "AE Distribution - can't allocate handle");
        return -1;
    }

    osal_memset (distr_ctx, 0, sizeof(*distr_ctx));

    distr_ctx->name = "sg ae distr ctx";

    if (dtpsrv_get_hndl(dtp_srv_hndl,
                        DTP_DB_ID_SG,
                        DTP_ID_SG_ALG_AE_DISTRIB,
                        1,
                        dtp_param_order,
                        &distr_ctx->hdtp, &distr_ctx->leaf))
    {
        mmsdbg(DL_ERROR, "AE Distribution - can't get DTP client handle");
    }

    *sg_distr_ae_hndl = distr_ctx;

    return 0;
}

int sg_distr_ae_destroy(sg_distr_ae_hndl_t sg_distr_ae_hndl)
{
    sg_distr_ae_ctx_t *distr_ctx = sg_distr_ae_hndl;


    if (NULL == distr_ctx)
    {
        mmsdbg(DL_ERROR, "Invalid handle pointer");
        return -6;
    }

    if (distr_ctx->hdtp)
        dtpsrv_free_hndl(distr_ctx->hdtp);
    else
        mmsdbg(DL_ERROR, "NULL DTP handle");

    osal_free(sg_distr_ae_hndl);
    return 0;
}

int sg_distr_ae_config(sg_distr_ae_hndl_t sg_distr_ae_hndl, sg_ae_distr_config_t* cfg)
{
    sg_distr_ae_ctx_t *distr_ctx = sg_distr_ae_hndl;

    dtpsrv_pre_process(distr_ctx->hdtp, cfg->dtp_s_common, &distr_ctx->dtp_skeys);

    *(&distr_ctx->cfg) = *cfg;
    return 0;
}

int sg_distr_ae_process(sg_distr_ae_hndl_t sg_distr_ae_hndl, sg_ae_distr_calc_input_t* distr_inp,
                           ae_distr_calc_output_t *out)
{


    sg_distr_ae_ctx_t *distr_ae_ctx = sg_distr_ae_hndl;
    hat_exp_gain_t *p;

//    if (dtpsrv_process(distr_ae_ctx->hdtp, distr_inp->dtp_d_common, &distr_ae_ctx->dtp_dkeys, (void**)&distr_dtp))

//    distr_ae_ctx->out.exp_gain.current_ISO = 0x00020000;
    p = &distr_ae_ctx->out.exp_gain;
    p->exposure = distr_inp->texp/3;
    p->again = distr_inp->texp / p->exposure;
    p->dgain = 1;

    distr_ae_ctx->out.aperture.shutter.isopen = 1;
    distr_ae_ctx->out.aperture.iris_index = 0;

    distr_ae_ctx->out.flash.duration   = 0;
    distr_ae_ctx->out.flash.power  = 0;
    distr_ae_ctx->out.flash.src  = LIGHT_SRC_MAIN_FLASH;
    distr_ae_ctx->out.flash.start_time = 0;

    distr_ae_ctx->out.pre_gain.gain  = 1.0;
    distr_ae_ctx->out.post_gain.gain = 1.0;

    distr_ae_ctx->out.texp = (p->exposure * p->again *
                              p->dgain * distr_ae_ctx->out.pre_gain.gain);

    *out = *(&distr_ae_ctx->out);
    return 0;
}
