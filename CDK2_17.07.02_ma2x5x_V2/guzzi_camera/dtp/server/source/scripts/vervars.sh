SCRIPTDIR=$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd) # $TOPDIR/dtp/server/source/scripts
PRJDIR=$(dirname $SCRIPTDIR) # $TOPDIR/dtp/server/source
TOPDIR=$(dirname $(dirname $(dirname $PRJDIR)))

function git_version {
    (cd $PRJDIR && git describe --long --dirty --always 2>/dev/null)
}

