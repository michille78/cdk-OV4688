/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file node_pin_logic.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <osal/osal_stdlib.h>
#include <osal/osal_string.h>
#include <osal/osal_mutex.h>
#include <utils/mms_debug.h>
#include <node_pin_logic.h>

struct node_pin_logic {
    int *pins;
    int *pins_initial;
    unsigned int pins_count;
    osal_mutex *pins_lock;
    char *node_name;
};

mmsdbg_define_variable(
        vdl_node_pin_logic,
        DL_DEFAULT,
        0,
        "vdl_node_pin_logic",
        "Pin logic function used for Pipe Nodes."
    );
#define MMSDEBUGLEVEL mmsdbg_use_variable(vdl_node_pin_logic)

int node_pin_logic(node_pin_logic_t *pl, unsigned int pin)
{
    int i;

    if (pl->pins_count < pin) {
        mmsdbg(
                DL_ERROR,
                "NodePinLogic: %s."
                "Failed to execute Pin Logic pin=%d (pin max is \"%d\")",
                pl->node_name,
                pin,
                pl->pins_count
            );
        return 0;
    }

    osal_mutex_lock(pl->pins_lock);
    pl->pins[pin]++;
    for (i = 0; i < pl->pins_count; i++) {
        if (pl->pins[i] < 1) {
            osal_mutex_unlock(pl->pins_lock);
            return 0;
        }
    }
    for (i = 0; i < pl->pins_count; i++) {
        pl->pins[i]--;
    }
    osal_mutex_unlock(pl->pins_lock);

    return 1;
}

void node_pin_logic_set_initial(node_pin_logic_t *pl, unsigned int pin, int val)
{
    if (pl->pins_count <= pin) {
        mmsdbg(
                DL_ERROR,
                "NodePinLogic: %s."
                "Failed to initialize pin=%u to val=%d (pin max is \"%u\")",
                pl->node_name,
                pin,
                val,
                pl->pins_count
            );
        return;
    }
    pl->pins_initial[pin] = val;
}

void node_pin_logic_reset(node_pin_logic_t *pl)
{
    osal_mutex_lock(pl->pins_lock);
    osal_memcpy(
            pl->pins,
            pl->pins_initial,
            pl->pins_count * sizeof (int)
        );
    osal_mutex_unlock(pl->pins_lock);
}

void node_pin_logic_destroy(node_pin_logic_t *pl)
{
    osal_mutex_destroy(pl->pins_lock);
    osal_free(pl->pins);
    osal_free(pl);
}

node_pin_logic_t *node_pin_logic_create(unsigned int count, char *node_name)
{
    node_pin_logic_t *pl;

    pl = osal_malloc(sizeof (*pl));
    if (!pl) {
        mmsdbg(
                DL_ERROR,
                "NodePinLogic: %s."
                " Failed to allocate memory for new instance: size=%d!",
                node_name,
                sizeof (*pl)
            );
        goto exit1;
    }

    pl->pins_count = count;
    pl->node_name = node_name;

    pl->pins = osal_calloc(2 * pl->pins_count, sizeof (int));
    if (!pl->pins) {
        mmsdbg(
                DL_ERROR,
                "NodePinLogic: %s."
                " Failed to allocate memory for Pin Counts: size=%d!",
                pl->node_name,
                2 * pl->pins_count * sizeof (int)
            );
        goto exit2;
    }
    pl->pins_initial = pl->pins + pl->pins_count;

    pl->pins_lock = osal_mutex_create();
    if (!pl->pins_lock) {
        mmsdbg(
                DL_ERROR,
                "NodePinLogic: %s. Failed to create mutex!",
                pl->node_name
            );
        goto exit3;
    }

    return pl;
exit3:
    osal_free(pl->pins);
exit2:
    osal_free(pl);
exit1:
    return NULL;
}

