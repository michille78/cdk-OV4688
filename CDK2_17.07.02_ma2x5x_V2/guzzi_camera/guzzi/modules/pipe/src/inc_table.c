/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file inc_table.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <inc.h>

int inc_auto_cam_algs_create(inc_t *inc, void *paramsr, void *app_res);
int inc_camera_create(inc_t *inc, void *paramsr, void *app_res);
int inc_control_logic_create(inc_t *inc, void *paramsr, void *app_res);
int inc_frame_info_create(inc_t *inc, void *paramsr, void *app_res);
int inc_ipipe_create(inc_t *inc, void *paramsr, void *app_res);
int inc_plugin_create(inc_t *inc, void *paramsr, void *app_res);
int inc_print_to_img_create(inc_t *inc, void *paramsr, void *app_res);

inc_table_entry_t inc_table[] = {
    { "auto_cam_algs"       , &inc_auto_cam_algs_create             },
    { "camera"              , &inc_camera_create                    },
    { "control_logic"       , &inc_control_logic_create             },
    { "frame_info"          , &inc_frame_info_create                },
    { "ipipe"               , &inc_ipipe_create                     },
    { "plugin"              , &inc_plugin_create                    },
    { "print_to_img"        , &inc_print_to_img_create              },

    INC_TABLE_ENTRY_LAST
};

