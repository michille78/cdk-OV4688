/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file node.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <osal/osal_stdlib.h>
#include <osal/osal_string.h>
#include <osal/osal_mutex.h>
#include <osal/osal_assert.h>
#include <utils/mms_debug.h>
#include <stm/include/stm.h>
#include <fifo/include/fifo.h>
#include <osal/pool.h>
#include <func_thread/include/func_thread.h>
#include <inc.h>
#include <node_pin_logic.h>
#include <node.h>

#define NONE_NAME_MAX 256
#define NONE_INC_NAME_MAX INC_NAME_MAX_LEN
#define NODE_QUEUE_LEN_MAX 32

typedef struct {
    fifo_entry_t link;
    void *data;
} data_fifo_entry_t;

struct node {
    stm_t *stm;
    func_thread_t *thread;
    inc_t *inc;
    fifo_t *wait_fifo;
    fifo_t *ready_fifo;
    pool_t *fifo_entry_pool;

    char name[NONE_NAME_MAX];
    void *resources;
    configurator_t *configurator;

    int thread_stack_size;
    int thread_priority;

    char inc_name[NONE_INC_NAME_MAX];
    void *inc_params;

    void *app_private;
    node_callback_t callback;

    node_in_events_t *in_events;
    node_out_events_t *out_events;
    node_in_events_t *in_event_data;

    node_pin_logic_t *input_pin_logic;

    int in_events_count;
    int out_events_count;

    void *start_params;
};

static void node_stm_event_thr_proc(node_t *node, void *data);
static void node_stm_event_start_ok(node_t *node);
static void node_stm_event_start_err(node_t *node);
static void node_stm_event_stop_dene(node_t *node);

static node_t pipe_src_node_storage;
node_t *pipe_src_node = &pipe_src_node_storage;

mmsdbg_define_variable(
        vdl_pipe_node,
        DL_DEFAULT,
        0,
        "vdl_pipe_node",
        "Pipe node."
    );
#define MMSDEBUGLEVEL mmsdbg_use_variable(vdl_pipe_node)

/*
* ***************************************************************************
* ***************************************************************************
* ** INC Callback ***********************************************************
* ***************************************************************************
* ***************************************************************************
*/

static void inc_callback(
        inc_t *inc,
        void *prv,
        unsigned int event_id,
        int num,
        void *ptr
    )
{

/* TODO: Handle fush state */

    node_t *node;
    node_proc_data_t pd;
    pipe_event_id_t pipe_event_id;
    int i;

    node = prv;

    if (node->out_events_count <= event_id) {
        mmsdbg(
                DL_ERROR,
                "Node: %s. Unexpected event_id=%u from INC=%s (max=%u)!",
                node->name,
                event_id,
                node->inc_name,
                node->out_events_count
            );
        return;
    }

    pd.origin = node;
    pd.inc_event_id = node->out_events[event_id].inc_event_id;
    pd.data = ptr;
    for (i = 0; i < node->out_events[event_id].next_count; i++) {
        node_process(node->out_events[event_id].next[i], &pd);
    }

    if (node->out_events[event_id].notify_upper_level) {
        pipe_event_id.type = PIPE_EVENT_INC;
        pipe_event_id.inc = node->out_events[event_id].inc_event_id;
        node->callback(
                node,
                node->app_private,
                pipe_event_id,
                num,
                ptr
            );
    }
}

/*
* ***************************************************************************
* ***************************************************************************
* ** Tread ******************************************************************
* ***************************************************************************
* ***************************************************************************
*/

static void node_thread_flush(func_thread_t *ethr, void *app_prv);
static void node_thread_process(func_thread_t *ethr, void *app_prv);
static void node_thread_start(func_thread_t *ethr, void *app_prv);

static func_thread_handle_t node_thread_fxns[] = {
    node_thread_flush,
    node_thread_process, /* Buffers in ready queue will be flushed through process function */
    node_thread_start
};

static func_thread_handles_t node_thread_handles = {
    .fxns = node_thread_fxns,
    .size = ARRAY_SIZE(node_thread_fxns)
};

static void node_thread_flush(func_thread_t *ethr, void *app_prv)
{
    node_t *node;
    data_fifo_entry_t *fe;
    pipe_event_id_t event_id;

    node = app_prv;

    inc_flush(node->inc);

    for (;;) {
        fifo_lock(node->wait_fifo);
        if (fifo_is_empty(node->wait_fifo)) {
            fifo_unlock(node->wait_fifo);
            break;
        }
        fe = FIFO_ENTRY(fifo_get(node->wait_fifo), data_fifo_entry_t, link);
        fifo_unlock(node->wait_fifo);
        event_id.v = 0;
        event_id.type = PIPE_EVENT_FLUSHING;
        node->callback(
                node,
                node->app_private,
                event_id,
                0,
                fe->data
            );
        osal_free(fe);
    }

    node_pin_logic_reset(node->input_pin_logic);

    osal_assert(fifo_is_empty(node->ready_fifo));

    event_id.v = 0;
    event_id.type = PIPE_EVENT_FLUSH_DONE;
    node->callback(
            node,
            node->app_private,
            event_id,
            0,
            NULL
        );
}

static void node_thread_process(func_thread_t *ethr, void *app_prv)
{
    node_t *node;
    data_fifo_entry_t *fe;
    void *data;

    node = app_prv;

    fifo_lock(node->ready_fifo);
    osal_assert(!fifo_is_empty(node->ready_fifo));
    fe = FIFO_ENTRY(fifo_get(node->ready_fifo), data_fifo_entry_t, link);
    fifo_unlock(node->ready_fifo);
    data = fe->data;
    osal_free(fe);

    node_stm_event_thr_proc(node, data);
}

static void node_thread_start(func_thread_t *ethr, void *app_prv)
{
    node_t *node;
    node = app_prv;
    if (inc_start(node->inc, node->start_params)) {
        mmsdbg(
                DL_ERROR,
                "Node: %s. Failed to start INC=%s",
                node->name,
                node->inc_name
            );
        node_stm_event_start_err(node);
    } else {
        node_stm_event_start_ok(node);
    }
}

/*
* ***************************************************************************
* ***************************************************************************
* ** STM ********************************************************************
* ***************************************************************************
* ***************************************************************************
*/

static int node_stm_action_destroy(
            stm_t *stm,
            void *prv,
            unsigned int arg1,
            void *arg2
        )
{
    node_t *node;
    node = prv;
    inc_destroy(node->inc);
    node_pin_logic_destroy(node->input_pin_logic);
    pool_destroy(node->fifo_entry_pool);
    fifo_destroy(node->ready_fifo);
    fifo_destroy(node->wait_fifo);
    func_thread_destroy(node->thread);
    stm_destroy(node->stm);
    return 0;
}

static int node_stm_action_start(
            stm_t *stm,
            void *prv,
            unsigned int arg1,
            void *arg2
        )
{
    node_t *node;
    node = prv;
    node->start_params = arg2;
    func_thread_exec(node->thread, node_thread_start);
    return 0;
}

static int node_stm_action_start_ok(
            stm_t *stm,
            void *prv,
            unsigned int arg1,
            void *arg2
        )
{
    node_t *node;
    pipe_event_id_t event_id;

    node = prv;

    event_id.v = 0;
    event_id.type = PIPE_EVENT_START_DONE;
    node->callback(
            node,
            node->app_private,
            event_id,
            0,
            NULL
        );

    return 0;
}

static int node_stm_action_start_err(
            stm_t *stm,
            void *prv,
            unsigned int arg1,
            void *arg2
        )
{
    node_t *node;
    pipe_event_id_t event_id;

    node = prv;

    event_id.v = 0;
    event_id.type = PIPE_EVENT_START_ERR;
    node->callback(
            node,
            node->app_private,
            event_id,
            0,
            NULL
        );

    return 0;
}

static int node_stm_action_stop(
            stm_t *stm,
            void *prv,
            unsigned int arg1,
            void *arg2
        )
{
    node_t *node;
    node = prv;
    /* TODO: check for clean port logic */
    osal_assert(fifo_is_empty(node->wait_fifo));
    osal_assert(fifo_is_empty(node->ready_fifo));
    inc_stop(node->inc);
    node_stm_event_stop_dene(node);
    return 0;
}

static int node_stm_action_flush(
            stm_t *stm,
            void *prv,
            unsigned int arg1,
            void *arg2
        )
{
    node_t *node;
    node = prv;
    func_thread_exec(node->thread, node_thread_flush);
    return 0;
}

static int node_stm_action_alter(
            stm_t *stm,
            void *prv,
            unsigned int arg1,
            void *arg2
        )
{
    node_t *node;
    node = prv;
    return inc_config_alter(node->inc, arg2);
}

static int node_stm_action_process(
            stm_t *stm,
            void *prv,
            unsigned int arg1,
            void *arg2
        )
{
    node_t *node;
    node_proc_data_t *in;
    data_fifo_entry_t *fe;
    int i;

    node = prv;
    in = arg2;

    /* Check for acceptable event */
    for (i = 0; i < node->in_events_count; i++) {
        if (NODE_IN_MATCH_DESC(in, node->in_events + i)) {
            break;
        }
    }
    if (node->in_events_count <= i) {
        mmsdbg(DL_ERROR, "Node: %s. Unknown input event!");
        return -1;
    }

    /* Enqueue data events */
    if ((node->in_events + i) == node->in_event_data) {
        fe = pool_alloc(node->fifo_entry_pool);
        fe->data = in->data;
        fifo_lock(node->wait_fifo);
        fifo_put(node->wait_fifo, &fe->link);
        fifo_unlock(node->wait_fifo);
    }

    /* If all events are present -> start new processing */
    if (node_pin_logic(node->input_pin_logic, i)) {
        fifo_lock(node->wait_fifo);
        fe = FIFO_ENTRY(fifo_get(node->wait_fifo), data_fifo_entry_t, link);
        fifo_unlock(node->wait_fifo);

        fifo_lock(node->ready_fifo);
        fifo_put(node->ready_fifo, &fe->link);
        fifo_unlock(node->ready_fifo);

        func_thread_exec(node->thread, node_thread_process);
    }

    return 0;
}

static int node_stm_action_proc_in_flush(
            stm_t *stm,
            void *prv,
            unsigned int arg1,
            void *arg2
        )
{
    node_t *node;
    node_proc_data_t *in;
    pipe_event_id_t event_id;

    node = prv;
    in = arg2;

    event_id.v = 0;
    event_id.type = PIPE_EVENT_FLUSHING;
    node->callback(
            node,
            node->app_private,
            event_id,
            0,
            in->data
        );

    return 0;
}

static int node_stm_action_thr_proc(
            stm_t *stm,
            void *prv,
            unsigned int arg1,
            void *arg2
        )
{
    node_t *node;
    node = prv;
    inc_process(node->inc, arg2);
    return 0;
}

static int node_stm_action_thr_flush(
            stm_t *stm,
            void *prv,
            unsigned int arg1,
            void *arg2
        )
{
    node_t *node;
    pipe_event_id_t event_id;
    node = prv;
    event_id.v = 0;
    event_id.type = PIPE_EVENT_FLUSHING;
    node->callback(
            node,
            node->app_private,
            event_id,
            0,
            arg2 
        );
    return 0;
}

static int node_stm_action_no_action(
            stm_t *stm,
            void *prv,
            unsigned int arg1,
            void *arg2
        )
{
    return 0;
}

static int node_stm_action_err(
            stm_t *stm,
            void *prv,
            unsigned int arg1,
            void *arg2
        )
{
    node_t *node;
    node = prv;
    mmsdbg(DL_ERROR, "Node: %s. Invalid STM state + operation!", node->name);
    return -1;
}

static stm_actions_t node_stm_actions[] = {
    node_stm_action_destroy,
    node_stm_action_start,
    node_stm_action_start_ok,
    node_stm_action_start_err,
    node_stm_action_stop,
    node_stm_action_flush,
    node_stm_action_alter,
    node_stm_action_process,
    node_stm_action_proc_in_flush,
    node_stm_action_thr_proc,
    node_stm_action_thr_flush,
    node_stm_action_no_action,
    node_stm_action_err
};

enum {
    NODE_STM_STATE_IDLE,
    NODE_STM_STATE_IDLE2RUN,
    NODE_STM_STATE_RUN2IDLE,
    NODE_STM_STATE_RUN,
    NODE_STM_STATE_FLUSH,
    NODE_STM_STATE_DESTROY,
    NODE_STM_STATES_NUMBER
};

enum {
    NODE_STM_EVENT_DESTROY,
    NODE_STM_EVENT_START,
    NODE_STM_EVENT_START_OK,
    NODE_STM_EVENT_START_ERR,
    NODE_STM_EVENT_START_NERR,
    NODE_STM_EVENT_STOP,
    NODE_STM_EVENT_STOP_DONE,
    NODE_STM_EVENT_FLUSH,
    NODE_STM_EVENT_FLUSH_DONE,
    NODE_STM_EVENT_ALTER,
    NODE_STM_EVENT_PROCESS,
    NODE_STM_EVENT_THR_PROC,
    NODE_STM_EVENTS_NUMBER
};

enum {
    NODE_STM_ACTION_DESTROY,
    NODE_STM_ACTION_START,
    NODE_STM_ACTION_START_OK,
    NODE_STM_ACTION_START_ERR,
    NODE_STM_ACTION_STOP,
    NODE_STM_ACTION_FLUSH,
    NODE_STM_ACTION_ALTER,
    NODE_STM_ACTION_PROCESS,
    NODE_STM_ACTION_PROC_IN_FLUSH,
    NODE_STM_ACTION_THR_PROC,
    NODE_STM_ACTION_THR_FLUSH,
    NODE_STM_ACTION_NO_ACTION,
    NODE_STM_ACTION_ERR,
    NODE_STM_ACTIONS_NUMBER
};

#define STM_STATE_PREFIX NODE_STM_STATE_
#define STM_ACTIOIN_PREFIX NODE_STM_ACTION_

STM_DESC_BUILD_BEGIN(
            node_stm,
            NODE_STM_STATE_IDLE,
            NODE_STM_STATES_NUMBER,
            NODE_STM_EVENTS_NUMBER,
            NODE_STM_ACTIONS_NUMBER
        )
                           /* |-IDLE----------------|  |-IDLE2RUN------------|  |-RUN2IDLE------------|  |-RUN----------------| */
/* DESTROY       */ STM_E4x(  DESTROY ,      DESTROY,  IDLE2RUN,          ERR,  RUN2IDLE,          ERR,  RUN     ,          ERR  )
/* START         */ STM_E4x(  IDLE2RUN,        START,  IDLE2RUN,          ERR,  RUN2IDLE,          ERR,  RUN     ,          ERR  )
/* START_OK      */ STM_E4x(  IDLE    ,          ERR,  RUN     ,     START_OK,  RUN2IDLE,          ERR,  RUN     ,          ERR  )
/* START_ERR     */ STM_E4x(  IDLE    ,          ERR,  IDLE    ,    START_ERR,  RUN2IDLE,          ERR,  RUN     ,          ERR  )
/* START_NERR    */ STM_E4x(  IDLE    ,          ERR,  IDLE    ,    NO_ACTION,  RUN2IDLE,          ERR,  RUN     ,          ERR  )
/* STOP          */ STM_E4x(  IDLE    ,          ERR,  IDLE2RUN,          ERR,  RUN2IDLE,          ERR,  RUN2IDLE,         STOP  )
/* STOP_DONE     */ STM_E4x(  IDLE    ,          ERR,  IDLE2RUN,          ERR,  IDLE    ,    NO_ACTION,  RUN     ,          ERR  )
/* FLUSH         */ STM_E4x(  IDLE    ,          ERR,  IDLE2RUN,          ERR,  RUN2IDLE,          ERR,  FLUSH   ,        FLUSH  )
/* FLUSH_DONE    */ STM_E4x(  IDLE    ,          ERR,  IDLE2RUN,          ERR,  RUN2IDLE,          ERR,  RUN     ,          ERR  )
/* ALTER         */ STM_E4x(  IDLE    ,        ALTER,  IDLE2RUN,          ERR,  RUN2IDLE,          ERR,  RUN     ,        ALTER  )
/* PROCESS       */ STM_E4x(  IDLE    ,          ERR,  IDLE2RUN,          ERR,  RUN2IDLE,          ERR,  RUN     ,      PROCESS  )
/* THR_PROC      */ STM_E4x(  IDLE    ,          ERR,  IDLE2RUN,          ERR,  RUN2IDLE,          ERR,  RUN     ,     THR_PROC  )

                           /* |-FLUSH---------------|  |-DESTROY------------|                                                   */
/* DESTROY       */ STM_E2x(  FLUSH   ,          ERR,  DESTROY ,          ERR                                                    )
/* START         */ STM_E2x(  FLUSH   ,          ERR,  DESTROY ,          ERR                                                    )
/* START_OK      */ STM_E2x(  FLUSH   ,          ERR,  DESTROY ,          ERR                                                    )
/* START_ERR     */ STM_E2x(  FLUSH   ,          ERR,  DESTROY ,          ERR                                                    )
/* START_NERR    */ STM_E2x(  FLUSH   ,          ERR,  DESTROY ,          ERR                                                    )
/* STOP          */ STM_E2x(  FLUSH   ,          ERR,  DESTROY ,          ERR                                                    )
/* STOP_DONE     */ STM_E2x(  FLUSH   ,          ERR,  DESTROY ,          ERR                                                    )
/* FLUSH         */ STM_E2x(  FLUSH   ,          ERR,  DESTROY ,          ERR                                                    )
/* FLUSH_DONE    */ STM_E2x(  RUN     ,    NO_ACTION,  DESTROY ,          ERR                                                    )
/* ALTER         */ STM_E2x(  FLUSH   ,          ERR,  DESTROY ,          ERR                                                    )
/* PROCESS       */ STM_E2x(  FLUSH   ,PROC_IN_FLUSH,  DESTROY ,          ERR                                                    )
/* THR_PROC      */ STM_E2x(  FLUSH   ,    THR_FLUSH,  DESTROY ,          ERR                                                    )

STM_DESC_BUILD_END(node_stm)

#define NODE_STM_ATOMIC_STATE(EVENT,N,P) \
    stm_event_atomic_state( \
            node->stm, \
            EVENT, \
            node, \
            N, \
            P \
        )

#define NODE_STM_ATOMIC(EVENT,N,P) \
    stm_event_atomic_state( \
            node->stm, \
            EVENT, \
            node, \
            N, \
            P \
        )

static void node_stm_event_thr_proc(node_t *node, void *data)
{
    NODE_STM_ATOMIC_STATE(NODE_STM_EVENT_THR_PROC, 0, data);
}

static void node_stm_event_start_ok(node_t *node)
{
    NODE_STM_ATOMIC_STATE(NODE_STM_EVENT_START_OK, 0, NULL);
}

static void node_stm_event_start_err(node_t *node)
{
    NODE_STM_ATOMIC_STATE(NODE_STM_EVENT_START_ERR, 0, NULL);
}

static void node_stm_event_stop_dene(node_t *node)
{
    NODE_STM_ATOMIC_STATE(NODE_STM_EVENT_STOP_DONE, 0, NULL);
}

/*
* ***************************************************************************
* ***************************************************************************
* ** Interface Part *********************************************************
* ***************************************************************************
* ***************************************************************************
*/

int node_start(node_t *node, void *params)
{
    int err;
    err = NODE_STM_ATOMIC_STATE(NODE_STM_EVENT_START, 0, params);
    if (err) {
        NODE_STM_ATOMIC_STATE(NODE_STM_EVENT_START_NERR, 0, NULL);
    }
    return err;
}

void node_stop(node_t *node)
{
    NODE_STM_ATOMIC_STATE(NODE_STM_EVENT_STOP, 0, NULL);
}

void node_flush(node_t *node)
{
    NODE_STM_ATOMIC_STATE(NODE_STM_EVENT_FLUSH, 0, NULL);
}

void node_flush_done(node_t *node)
{
    NODE_STM_ATOMIC_STATE(NODE_STM_EVENT_FLUSH_DONE, 0, NULL);
}

int node_config_alter(node_t *node, void *config)
{
    return NODE_STM_ATOMIC_STATE(NODE_STM_EVENT_ALTER, 0, config);
}

int node_process(node_t *node, node_proc_data_t *data)
{
    return NODE_STM_ATOMIC(NODE_STM_EVENT_PROCESS, 0, data);
}

void node_destroy(node_t *node)
{
    NODE_STM_ATOMIC_STATE(NODE_STM_EVENT_DESTROY, 0, NULL);
}

int node_create(node_t *node, node_create_params_t *params)
{
    int i;

    osal_strncpy(node->name, params->name, sizeof (node->name)-1);
    node->name[sizeof (node->name)-1] = '\000';
    node->resources = params->resources;
    node->configurator = params->configurator;

    node->thread_stack_size = params->thread_stack_size;
    node->thread_priority = params->thread_priority;

    osal_strncpy(node->inc_name, params->inc_name, sizeof (node->inc_name)-1);
    node->inc_name[sizeof (node->inc_name)-1] = '\000';
    node->inc_params = params->inc_params;

    node->app_private = params->app_private;
    node->callback = params->callback;

    node->in_events = params->in_events;
    node->out_events = params->out_events;
    node->in_events_count = params->in_events_count;
    node->out_events_count = params->out_events_count;
    node->in_event_data = params->in_event_data;

    node->stm = stm_create(
            node->name,
            &node_stm,
            node_stm_actions
        );
    if (!node->stm) {
        mmsdbg(
                DL_ERROR,
                "Node: %s. Failed to create STM!",
                node->name
            );
        goto exit1;
    }

    node->thread = func_thread_create(
            node->name,
            node->thread_stack_size,
            node->thread_priority,
            &node_thread_handles,
            node
        );
    if (!node->thread) {
        mmsdbg(
                DL_ERROR,
                "Node: %s. Failed to create thread!",
                node->name
            );
        goto exit2;
    }

    node->wait_fifo = fifo_create();
    if (!node->wait_fifo) {
        mmsdbg(
                DL_ERROR,
                "Node: %s. Failed to create Wait FIFO!",
                node->name
            );
        goto exit3;
    }

    node->ready_fifo = fifo_create();
    if (!node->ready_fifo) {
        mmsdbg(
                DL_ERROR,
                "Node: %s. Failed to create Ready FIFO!",
                node->name
            );
        goto exit4;
    }

    node->fifo_entry_pool = pool_create(
            "NODE POOL",
            sizeof (data_fifo_entry_t),
            NODE_QUEUE_LEN_MAX
        );
    if (!node->fifo_entry_pool) {
        mmsdbg(
                DL_ERROR,
                "Node: %s. Failed to create FIFO entry pool!",
                node->name
            );
        goto exit5;
    }

    node->input_pin_logic = node_pin_logic_create(
            node->in_events_count,
            node->name
        );
    if (!node->input_pin_logic) {
        mmsdbg(
                DL_ERROR,
                "Node: %s. Failed to create input pin logic!",
                node->name
            );
        goto exit6;
    }
    for (i = 0; i < node->in_events_count; i++) {
        node_pin_logic_set_initial(
                node->input_pin_logic,
                i,
                node->in_events[i].initial_count
            );
    }
    node_pin_logic_reset(node->input_pin_logic);

    node->inc = inc_create(
            node->inc_name,
            node->inc_params,
            node->resources,
            inc_callback,
            node
        );
    if (!node->inc) {
        mmsdbg(
                DL_ERROR,
                "Node: %s. Failed to create INC=\"%s\"!",
                node->name,
                node->inc_name
            );
        goto exit7;
    }

    return 0;
exit7:
    node_pin_logic_destroy(node->input_pin_logic);
exit6:
    pool_destroy(node->fifo_entry_pool);
exit5:
    fifo_destroy(node->ready_fifo);
exit4:
    fifo_destroy(node->wait_fifo);
exit3:
    func_thread_destroy(node->thread);
exit2:
    stm_destroy(node->stm);
exit1:
    return -1;
}

void node_free_handle(node_t *node)
{
    osal_free(node);
}

node_t *node_get_handle(void)
{
    node_t *node;

    node = osal_calloc(1, sizeof (*node));
    if (!node) {
        mmsdbg(
                DL_ERROR,
                "Failed to allocate memory for new Node instance: size=%d!",
                sizeof (*node)
            );
        goto exit1;
    }

    return node;
exit1:
    return NULL;
}

