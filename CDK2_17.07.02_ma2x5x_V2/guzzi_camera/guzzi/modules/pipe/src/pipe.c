/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file pipe.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <osal/osal_stdlib.h>
#include <osal/osal_mutex.h>
#include <osal/osal_list.h>
#include <utils/mms_debug.h>
#include <stm/include/stm.h>
#include <pipe_desc.h>
#include <pipe_event_id.h>
#include <node.h>
#include <pipe.h>

#define MAX_NODES 100

typedef struct {
    struct list_head link;
    node_t *node;
    node_create_params_t params;
    node_desc_t *node_desc;
    pipe_t *pipe;
} pipe_node_t;

typedef struct {
    struct list_head link;
    pipe_node_t *pipe_node;
} pipe_list_entry_t;

struct pipe {
    char *name;

    pipe_desc_t *pipe_desc;

    void *app_private;
    void *resources;
    configurator_t *configurator;
    configurator_client_t *cfgr_client;
    pipe_callback_t callback;

    stm_t *stm;

    pipe_node_t *root_node;

    pipe_node_t *pipe_node_arr;
    int pipe_node_arr_size;

    struct list_head *pipe_list_pre;
    struct list_head *pipe_list_post;

    struct list_head pipe_list_flush;
    osal_mutex *pipe_list_flush_lock;

    struct list_head pipe_list_start_request;
    struct list_head pipe_list_start_complete;
    osal_mutex *pipe_list_start_lock;
};

static int pipe_stm_event_start_nerr(pipe_t *pipe);
static int pipe_stm_event_start_ndone(pipe_t *pipe);
static int pipe_stm_event_start_done(pipe_t *pipe);
static int pipe_stm_event_start_done_as(pipe_t *pipe);

static void node_callback(
        node_t *node,
        void *prv,
        pipe_event_id_t event_id,
        int num,
        void *ptr
    );

/* Pipe source node */
static node_desc_in_event_t pipe_src_node_desc_in_events[] = {
    NODE_DESC_IN_EVENT_NIL
};

static node_desc_t *pipe_src_node_desc_next[] = {
    NODE_DESC_NEXT_NIL
};

static node_desc_out_events_t pipe_src_node_desc_out_events[] = {
    NODE_DESC_OUT_EVENT_NIL
};

node_desc_t pipe_src_node_desc = {
    .name = "Pipe Source Node",

    .in_events = pipe_src_node_desc_in_events,
    .next_desc = pipe_src_node_desc_next,
    .out_events = pipe_src_node_desc_out_events,

    .in_event_data = pipe_src_node_desc_in_events,

    .thread_stack_size = 0,
    .thread_priority = 0,

    .inc_name = "NONE",
    .inc_params = NULL,

    .active = 0
};

mmsdbg_define_variable(
        vdl_pipe,
        DL_DEFAULT,
        0,
        "vdl_pipe",
        "Pipe."
    );
#define MMSDEBUGLEVEL mmsdbg_use_variable(vdl_pipe)

/*
* ***************************************************************************
* ** Misc *******************************************************************
* ***************************************************************************
*/

static int is_on_data_path(node_desc_t *node_desc, node_desc_t *parent_desc)
{
    return node_desc->in_event_data->origin == parent_desc;
}

static pipe_node_t *node_desc_to_pipe_node(
        pipe_node_t *pipe_node_arr,
        int pipe_node_arr_size,
        node_desc_t *node_desc
    )
{
    int i;
    for (i = 0; i < pipe_node_arr_size; i++) {
        if (pipe_node_arr[i].node_desc == node_desc) {
            return pipe_node_arr + i;
        }
    }
    return NULL;
}

static node_t *node_desc_to_node(
        pipe_node_t *pipe_node_arr,
        int pipe_node_arr_size,
        node_desc_t *node_desc
    )
{
    int i;
    if (PIPE_SRC_NODE_DESC == node_desc) {
        return pipe_src_node;
    }
    for (i = 0; i < pipe_node_arr_size; i++) {
        if (pipe_node_arr[i].node_desc == node_desc) {
            return pipe_node_arr[i].node;
        }
    }
    return NULL;
}

/*
* ***************************************************************************
* ** create, destroy node params ********************************************
* ***************************************************************************
*/

typedef struct {
    void *resources;
    configurator_t *configurator;
    node_callback_t callback;
    pipe_node_t *pipe_node_arr;
    int pipe_node_arr_size;
} node_params_ctx_t;

/** Input Events ************************************************************ */
static int desc_in_events_count(node_desc_in_event_t *desc)
{
    int i;
    for (i = 0; !NODE_DESC_IN_EVENT_IS_NIL(desc + i); i++) {
        if (NODE_DESC_IN_EVENTS_MAX < i) {
            return -1;
        }
    }
    return i;
}

static void param_in_events_destroy(
        node_in_events_t *in_events
    )
{
    osal_free(in_events);
}

static node_in_events_t *param_in_events_build(
        node_params_ctx_t *ctx,
        node_desc_in_event_t *desc,
        int desc_size
    )
{
    node_in_events_t *par;
    int i;

    par = osal_malloc(desc_size * sizeof (*par));
    if (!par) {
        mmsdbg(
                DL_ERROR,
                "Failed to allocate new input param array: size=%d",
                desc_size * sizeof (*par)
            );
        goto exit1;
    }

    for (i = 0; i < desc_size; i++) {
        par[i].origin = node_desc_to_node(
                ctx->pipe_node_arr,
                ctx->pipe_node_arr_size,
                desc[i].origin
            );
        if (!par[i].origin) {
            mmsdbg(DL_ERROR, "Node descriptor not found in Pipe Nodes!");
            goto exit2;
        }
        par[i].inc_event_id = desc[i].inc_event_id;
        par[i].initial_count = desc[i].initial_count;
    }

    return par;
exit2:
    osal_free(par);
exit1:
    return NULL;
}

/** Next Nodes ************************************************************** */
static int desc_in_events_find_by_origin_event(
        node_desc_in_event_t *desc_in_events,
        node_desc_t *origin,
        unsigned int inc_event_id
    )
{
    int i;
    for (i = 0; !NODE_DESC_IN_EVENT_IS_NIL(desc_in_events + i); i++) {
        if (NODE_DESC_IN_EVENTS_MAX < i) {
            return -1;
        }
        if (   (desc_in_events[i].origin == origin)
            && (desc_in_events[i].inc_event_id == inc_event_id) )
        {
            return i;
        }
    }
    return -1;
}

static int desc_next_count_by_origin_event(
        node_desc_t **next_desc,
        node_desc_t *origin,
        unsigned int inc_event_id
    )
{
    int i, n;
    n = 0;
    for (i = 0; !NODE_DESC_NEXT_IS_NIL(next_desc[i]); i++) {
        if (NODE_DESC_NEXT_MAX < i) {
            return -1;
        }
        if (0 <= desc_in_events_find_by_origin_event(next_desc[i]->in_events, origin, inc_event_id)) {
            n++;
        }
    }
    return n;
}

static void desc_next_destroy(node_t **next)
{
    osal_free(next);
}

static node_t **desc_next_build_by_origin_event(
        node_params_ctx_t *ctx,
        int next_node_count,
        node_desc_t **next_desc,
        node_desc_t *origin,
        unsigned int inc_event_id
    )
{
    node_t **next;
    int i, n;

    next = osal_malloc(next_node_count * sizeof (*next));
    if (!next) {
        mmsdbg(
                DL_ERROR,
                "Failed to allocate new next node array: size=%d",
                next_node_count * sizeof (*next)
            );
        goto exit1;
    }

    n = 0;
    for (i = 0; !NODE_DESC_NEXT_IS_NIL(next_desc[i]); i++) {
        if (0 <= desc_in_events_find_by_origin_event(next_desc[i]->in_events, origin, inc_event_id)) {
            if (next_node_count <= n) {
                mmsdbg(
                        DL_ERROR,
                        "The number of next nodes for origin=%s, event=%d "
                        "is bigger than expected %d!",
                        origin->name,
                        inc_event_id,
                        next_node_count
                    );
                goto exit2;
            }
            next[n] = node_desc_to_node(
                    ctx->pipe_node_arr,
                    ctx->pipe_node_arr_size,
                    next_desc[i]
                );
            if (!next[n]) {
                mmsdbg(DL_ERROR, "Node descriptor not found in Pipe Nodes!");
                goto exit2;
            }
            n++;
        }
    }

    return next;
exit2:
    osal_free(next);
exit1:
    return NULL;
}

/** Output Events *********************************************************** */
static int desc_out_events_count(node_desc_out_events_t *desc_out_events)
{
    int i;
    for (i = 0; !NODE_DESC_OUT_EVENT_IS_NIL(desc_out_events + i); i++) {
        if (NODE_DESC_OUT_EVENT_MAX < i) {
            return -1;
        }
    }
    return i;
}

static void desc_out_events_destroy(node_out_events_t *out_events, int out_events_count)
{
    int i;
    for (i = 0; i < out_events_count; i++) {
        if (out_events[i].next_count) {
            desc_next_destroy(out_events[i].next);
        }
    }
    osal_free(out_events);
}

static node_out_events_t *desc_out_events_build(
        node_params_ctx_t *ctx,
        node_desc_out_events_t *desc_out_events,
        int desc_out_events_size,
        node_desc_t *origin
    )
{
    node_out_events_t *out_events;
    int i;

    out_events = osal_malloc(desc_out_events_size * sizeof (*out_events));
    if (!out_events) {
        mmsdbg(
                DL_ERROR,
                "Failed to allocate new Out Events param array: size=%d",
                desc_out_events_size * sizeof (*out_events)
            );
        goto exit1;
    }

    for (i = 0; i < desc_out_events_size; i++) {
        out_events[i].inc_event_id = desc_out_events[i].inc_event_id;
        out_events[i].notify_upper_level = desc_out_events[i].notify_upper_level;

        /* Next Nodes */
        out_events[i].next_count = desc_next_count_by_origin_event(
                origin->next_desc,
                origin,
                desc_out_events[i].inc_event_id
            );
        if (0 < out_events[i].next_count) {
            out_events[i].next = desc_next_build_by_origin_event(
                    ctx,
                    out_events[i].next_count,
                    origin->next_desc,
                    origin,
                    desc_out_events[i].inc_event_id
                );
            if (!out_events[i].next) {
                mmsdbg(DL_ERROR, "Failed to convert next nodes!");
                goto exit2;
            }
        } else if (0 == out_events[i].next_count) {
            out_events[i].next = NULL;
        } else {
            mmsdbg(DL_ERROR, "Too many input events!");
            goto exit2;
        }
    }

    return out_events;
exit2:
    for (i--; 0 <= i; i--) {
        if (out_events[i].next_count) {
            desc_next_destroy(out_events[i].next);
        }
    }
    osal_free(out_events);
exit1:
    return NULL;
}

/** Single Node description ************************************************* */
static void node_params_destroy(pipe_node_t *pipe_node)
{
    desc_out_events_destroy(pipe_node->params.out_events, pipe_node->params.out_events_count);
    param_in_events_destroy(pipe_node->params.in_events);
}

static int node_params_create(
        node_params_ctx_t *ctx,
        pipe_node_t *pipe_node
    )
{
    int n;

    /* Input Events */
    pipe_node->params.in_events_count = desc_in_events_count(
            pipe_node->node_desc->in_events
        );
    if (pipe_node->params.in_events_count < 0) {
        mmsdbg(
                DL_ERROR,
                "Pipe: %s; Node: %s. Too many input events!",
                pipe_node->pipe->name,
                pipe_node->node_desc->name
            );
        goto exit1;
    }
    pipe_node->params.in_events = param_in_events_build(
            ctx,
            pipe_node->node_desc->in_events,
            pipe_node->params.in_events_count
        );
    if (!pipe_node->params.in_events) {
        mmsdbg(
                DL_ERROR,
                "Pipe: %s; Node: %s. Failed to convert input events!",
                pipe_node->pipe->name,
                pipe_node->node_desc->name
            );
        goto exit1;
    }

    /* Output Events */
    pipe_node->params.out_events_count = desc_out_events_count(
            pipe_node->node_desc->out_events
        );
    if (pipe_node->params.out_events_count < 0) {
        mmsdbg(
                DL_ERROR,
                "Pipe: %s; Node: %s. Too many output events!",
                pipe_node->pipe->name,
                pipe_node->node_desc->name
            );
        goto exit2;
    }
    pipe_node->params.out_events = desc_out_events_build(
            ctx,
            pipe_node->node_desc->out_events,
            pipe_node->params.out_events_count,
            pipe_node->node_desc
        );
    if (!pipe_node->params.out_events) {
        mmsdbg(
                DL_ERROR,
                "Pipe: %s; Node: %s. Failed to convert Output Events!",
                pipe_node->pipe->name,
                pipe_node->node_desc->name
            );
        goto exit2;
    }

    /* Data Event */
    n =   pipe_node->node_desc->in_event_data
        - pipe_node->node_desc->in_events;
    pipe_node->params.in_event_data = pipe_node->params.in_events + n;

    /* Just copy */
    pipe_node->params.name              = pipe_node->node_desc->name;
    pipe_node->params.thread_stack_size = pipe_node->node_desc->thread_stack_size;
    pipe_node->params.thread_priority   = pipe_node->node_desc->thread_priority;
    pipe_node->params.inc_name          = pipe_node->node_desc->inc_name;
    pipe_node->params.inc_params        = pipe_node->node_desc->inc_params;
    pipe_node->params.active            = pipe_node->node_desc->active;

    pipe_node->params.app_private  = pipe_node;
    pipe_node->params.resources    = ctx->resources;
    pipe_node->params.configurator = ctx->configurator;
    pipe_node->params.callback     = ctx->callback;

    return 0;
exit2:
    param_in_events_destroy(pipe_node->params.in_events);
exit1:
    return -1;
}

/** All Node descriptions *************************************************** */
static void node_array_params_destroy_ctx(node_params_ctx_t *ctx)
{
    int i;
    for (i = ctx->pipe_node_arr_size - 1; 0 <= i; i--) {
        node_params_destroy(ctx->pipe_node_arr + i);
    }
}

static int node_array_params_create_ctx(node_params_ctx_t *ctx)
{
    int i;

    for (i = 0; i < ctx->pipe_node_arr_size; i++) {
        if (node_params_create(ctx, ctx->pipe_node_arr + i)) {
            goto exit1;
        }
    }

    return 0;
exit1:
    for (i--; 0 <= i; i--) {
        node_params_destroy(ctx->pipe_node_arr + i);
    }
    return -1;
}

static void node_array_params_destroy(pipe_t *pipe)
{
    node_params_ctx_t ctx;
    ctx.resources          = pipe->resources;
    ctx.configurator       = pipe->configurator;
    ctx.callback           = node_callback;
    ctx.pipe_node_arr      = pipe->pipe_node_arr;
    ctx.pipe_node_arr_size = pipe->pipe_node_arr_size;
    node_array_params_destroy_ctx(&ctx);
}

static int node_array_params_create(pipe_t *pipe)
{
    node_params_ctx_t ctx;
    ctx.resources          = pipe->resources;
    ctx.configurator       = pipe->configurator;
    ctx.callback           = node_callback;
    ctx.pipe_node_arr      = pipe->pipe_node_arr;
    ctx.pipe_node_arr_size = pipe->pipe_node_arr_size;
    return node_array_params_create_ctx(&ctx);
}

/*
* ***************************************************************************
* ** Node list routines *****************************************************
* ***************************************************************************
*/

/** flush, flush_done ******************************************************* */
static void node_list_flush(
        struct list_head *list,
        struct list_head *done,
        osal_mutex *done_lock
    )
{
    pipe_list_entry_t *c;

    osal_mutex_lock(done_lock);
    list_for_each_entry(c, list, link) {
        list_add(&c->pipe_node->link, done);
    }
    osal_mutex_unlock(done_lock);

    list_for_each_entry(c, list, link) {
        node_flush(c->pipe_node->node);
    }
}

static void node_list_flush_done(struct list_head *list)
{
    pipe_list_entry_t *c;
    list_for_each_entry(c, list, link) {
        node_flush_done(c->pipe_node->node);
    }
}

/** start, stop ************************************************************* */
static void node_list_stop(struct list_head *list)
{
    pipe_list_entry_t *c;
    list_for_each_entry(c, list, link) {
        node_stop(c->pipe_node->node);
    }
}

static void node_list_prv_stop(struct list_head *list)
{
    pipe_node_t *c;
    list_for_each_entry(c, list, link) {
        node_stop(c->node);
    }
}

static int node_list_start(
        struct list_head *list,
        struct list_head *done,
        osal_mutex *done_lock,
        void *params
    )
{
    pipe_list_entry_t *c, *n;
    int err;

    err = 0;
    list_for_each_entry_safe(c, n, list, link) {
        osal_mutex_lock(done_lock);
        list_add(&c->pipe_node->link, done);
        osal_mutex_unlock(done_lock);
        err = node_start(c->pipe_node->node, params);
        if (err) {
            mmsdbg(
                    DL_ERROR,
                    "Pipe: %s; Node: %s. Failed to start node!",
                    c->pipe_node->pipe->name,
                    c->pipe_node->node_desc->name
                );
            osal_mutex_lock(done_lock);
            list_del(&c->pipe_node->link);
            osal_mutex_unlock(done_lock);
            break;
        }
    }

    return err;
}

/** alternate *************************************************************** */
static int node_list_config_alter(struct list_head *list, void *config)
{
    pipe_list_entry_t *c;
    int err;

    list_for_each_entry(c, list, link) {
        err = node_config_alter(c->pipe_node->node, config);
        if (err) {
            return err;
        }
    }
    return 0;
}

/** create, destroy ********************************************************* */
static void node_list_destroy(struct list_head *list)
{
    pipe_list_entry_t *c;

    list_for_each_entry_reverse(c, list, link) {
        node_destroy(c->pipe_node->node);
    }
}

static int node_list_create(struct list_head *list)
{
    pipe_list_entry_t *c, *n;
    LIST_HEAD(done);
    int err;

    list_for_each_entry_safe(c, n, list, link) {
        err = node_create(c->pipe_node->node, &c->pipe_node->params);
        if (err) {
            mmsdbg(
                    DL_ERROR,
                    "Pipe: %s; Node: %s. Failed to create node!",
                    c->pipe_node->pipe->name,
                    c->pipe_node->node_desc->name
                );
            goto exit1;
        }
        list_move_tail(&c->link, &done);
    }
    list_replace(&done, list);

    return 0;
exit1:
    list_for_each_entry_safe_reverse(c, n, &done, link) {
        node_destroy(c->pipe_node->node);
        list_move(&c->link, list);
    }
    return -1;
}

/** get, free handle ******************************************************** */
static void node_list_free_handle(struct list_head *list)
{
    pipe_list_entry_t *c;

    list_for_each_entry_reverse(c, list, link) {
        node_free_handle(c->pipe_node->node);
        c->pipe_node->node = NULL;
    }
}

static int node_list_get_handle(struct list_head *list)
{
    pipe_list_entry_t *c, *n;
    LIST_HEAD(done);

    list_for_each_entry_safe(c, n, list, link) {
        c->pipe_node->node = node_get_handle();
        if (!c->pipe_node->node) {
            mmsdbg(
                    DL_ERROR,
                    "Pipe: %s; Node: %s. Failed to Get Node Handle!",
                    c->pipe_node->pipe->name,
                    c->pipe_node->node_desc->name
                );
            goto exit1;
        }
        list_move_tail(&c->link, &done);
    }
    list_replace(&done, list);

    return 0;
exit1:
    list_for_each_entry_safe_reverse(c, n, &done, link) {
        node_free_handle(c->pipe_node->node);
        list_move(&c->link, list);
    }
    return -1;
}

/*
* ***************************************************************************
* ** Build pre and post lists ***********************************************
* ***************************************************************************
*/

typedef struct {
    struct list_head *list;
    struct list_head *list_entry;
    pipe_node_t *pipe_node_arr;
    int pipe_node_arr_size;
} pipe_to_list_ctx_t;

static int pipe_to_list_post(pipe_to_list_ctx_t *ctx, node_desc_t *node_desc)
{
    pipe_list_entry_t *e;
    node_desc_t **next;

    /* TODO: check recursion level */

    for (next = node_desc->next_desc; *next; next++) {
        if (is_on_data_path(*next, node_desc)) {
            if (pipe_to_list_post(ctx, *next)) {
                return -1;
            }
        }
    }

    if (ctx->list == ctx->list_entry) {
        mmsdbg(
                DL_ERROR,
                "Too many nodes at Node=%s! Max: %d.",
                node_desc->name,
                ctx->pipe_node_arr_size
            );
        return -1;
    }
    e = list_entry(ctx->list_entry, pipe_list_entry_t, link);
    e->pipe_node = node_desc_to_pipe_node(
            ctx->pipe_node_arr,
            ctx->pipe_node_arr_size,
            node_desc
        );
    if (!e->pipe_node) {
        mmsdbg(
                DL_ERROR,
                "Node descriptor (%s) not found in Pipe Nodes!",
                node_desc->name
            );
        return -1;
    }
    ctx->list_entry = ctx->list_entry->next;

    return 0;
}

static int pipe_to_list_pre(pipe_to_list_ctx_t *ctx, node_desc_t *node_desc)
{
    pipe_list_entry_t *e;
    node_desc_t **next;

    if (ctx->list == ctx->list_entry) {
        mmsdbg(
                DL_ERROR,
                "Too many nodes at Node=%s! Max: %d.",
                node_desc->name,
                ctx->pipe_node_arr_size
            );
        return -1;
    }
    e = list_entry(ctx->list_entry, pipe_list_entry_t, link);
    e->pipe_node = node_desc_to_pipe_node(
            ctx->pipe_node_arr,
            ctx->pipe_node_arr_size,
            node_desc
        );
    if (!e->pipe_node) {
        mmsdbg(
                DL_ERROR,
                "Node descriptor (%s) not found in Pipe Nodes!",
                node_desc->name
            );
        return -1;
    }
    ctx->list_entry = ctx->list_entry->next;

    for (next = node_desc->next_desc; *next; next++) {
        if (is_on_data_path(*next, node_desc)) {
            if (pipe_to_list_pre(ctx, *next)) {
                return -1;
            }
        }
    }

    return 0;
}

static void pipe_list_free(struct list_head *list)
{
    pipe_list_entry_t *c, *n;
    list_for_each_entry_safe(c, n, list, link) {
        osal_free(c);
    }
    osal_free(list);
}

static struct list_head *pipe_list_alloc(int count)
{
    struct list_head *list;
    pipe_list_entry_t *entry;
    int i;

    list = osal_malloc(sizeof (*list));
    if (!list) {
        mmsdbg(
                DL_ERROR,
                "Failed to allocate memory for Pipe List: size=%d!",
                sizeof (*list)
            );
        goto exit1;
    }
    INIT_LIST_HEAD(list);

    for (i = 0; i < count; i++) {
        entry = osal_calloc(1, sizeof (*entry));
        if (!entry) {
            mmsdbg(
                    DL_ERROR,
                    "Failed to allocate memory for Pipe List Entry: size=%d!",
                    sizeof (*entry)
                );
            goto exit2;
        }
        list_add(&entry->link, list);
    }

    return list;
exit2:
    {
        pipe_list_entry_t *c, *n;
        list_for_each_entry_safe(c, n, list, link) {
            osal_free(c);
        }
        osal_free(list);
    }
exit1:
    return NULL;
}

static void pipe_list_destroy(struct list_head *list)
{
    pipe_list_free(list);
}

static struct list_head *pipe_list_create(
        node_desc_t *node_desc,
        pipe_node_t *pipe_node_arr,
        int pipe_node_arr_size,
        int (*func)(
                pipe_to_list_ctx_t *ctx,
                node_desc_t *node_desc
            )
    )
{
    struct list_head *list;
    pipe_to_list_ctx_t ctx;

    list = pipe_list_alloc(pipe_node_arr_size);
    if (!list) {
        goto exit1;
    }

    ctx.list = list;
    ctx.list_entry = list->next;
    ctx.pipe_node_arr = pipe_node_arr;
    ctx.pipe_node_arr_size = pipe_node_arr_size;
    if (func(&ctx, node_desc)) {
        goto exit2;
    }

    return list;
exit2:
    pipe_list_free(list);
exit1:
    return NULL;
}

static struct list_head *pipe_list_create_pre(
        node_desc_t *node_desc,
        pipe_node_t *pipe_node_arr,
        int pipe_node_arr_size
    )
{
    return pipe_list_create(
            node_desc,
            pipe_node_arr,
            pipe_node_arr_size,
            pipe_to_list_pre
        );
}

static struct list_head *pipe_list_create_post(
        node_desc_t *node_desc,
        pipe_node_t *pipe_node_arr,
        int pipe_node_arr_size
    )
{
    return pipe_list_create(
            node_desc,
            pipe_node_arr,
            pipe_node_arr_size,
            pipe_to_list_post
        );
}

/*
* ***************************************************************************
* ** Build node array *******************************************************
* ***************************************************************************
*/

typedef struct {
    pipe_t *pipe;
    pipe_node_t *pipe_node_arr;
    int pipe_node_arr_size;
    int pipe_node_arr_idx;
} node_array_fill_ctx_t;

static int node_array_fill(node_array_fill_ctx_t *ctx, node_desc_t *node_desc)
{
    node_desc_t **next;

    if (ctx->pipe_node_arr_size <= ctx->pipe_node_arr_idx) {
        mmsdbg(
                DL_ERROR,
                "Too many nodes at Node=%s! Max: %d.",
                node_desc->name,
                ctx->pipe_node_arr_size
            );
        return -1;
    }
    ctx->pipe_node_arr[ctx->pipe_node_arr_idx].node_desc = node_desc;
    ctx->pipe_node_arr[ctx->pipe_node_arr_idx].pipe = ctx->pipe;
    ctx->pipe_node_arr_idx++;

    for (next = node_desc->next_desc; *next; next++) {
        if (is_on_data_path(*next, node_desc)) {
            if (node_array_fill(ctx, *next)) {
                return -1;
            }
        }
    }

    return 0;
}

static void node_array_destroy(pipe_node_t *pipe_node_arr)
{
    osal_free(pipe_node_arr);
}

static pipe_node_t *node_array_create(
        pipe_t *pipe,
        node_desc_t *node_desc,
        int pipe_node_arr_size
    )
{
    pipe_node_t *pipe_node_arr;
    node_array_fill_ctx_t ctx;

    pipe_node_arr = osal_calloc(pipe_node_arr_size, sizeof (*pipe_node_arr));
    if (!pipe_node_arr) {
        mmsdbg(
                DL_ERROR,
                "Pipe %s. Failed to allocate memory for Node Array: size=%d!",
                pipe->name,
                pipe_node_arr_size * sizeof (*pipe_node_arr)
            );
        goto exit1;
    }

    ctx.pipe = pipe;
    ctx.pipe_node_arr = pipe_node_arr;
    ctx.pipe_node_arr_size = pipe_node_arr_size;
    ctx.pipe_node_arr_idx = 0;

    if (node_array_fill(&ctx, node_desc)) {
        goto exit2;
    }

    return pipe_node_arr;
exit2:
    osal_free(pipe_node_arr);
exit1:
    return NULL;
}

/*
* ***************************************************************************
* ** Count pipe nodes *******************************************************
* ***************************************************************************
*/

typedef struct {
    int count;
    int count_max;
} count_nodes_ctx_t;

static int _count_nodes(count_nodes_ctx_t *ctx, node_desc_t *node_desc)
{
    node_desc_t **next;

    if (ctx->count_max < ctx->count_max) {
        mmsdbg(
                DL_ERROR,
                "Too many nodes at Node=%s! Max: %d.",
                node_desc->name,
                ctx->count_max
            );
        return -1;
    }

    ctx->count++;

    for (next = node_desc->next_desc; *next; next++) {
        if (is_on_data_path(*next, node_desc)) {
            if (_count_nodes(ctx, *next)) {
                return -1;
            }
        }
    }

    return 0;
}

static int count_nodes(node_desc_t *node_desc)
{
    count_nodes_ctx_t ctx;
    ctx.count = 0;
    ctx.count_max = MAX_NODES;
    if (_count_nodes(&ctx, node_desc)) {
        return -1;
    }
    return ctx.count;
}

/*
* ***************************************************************************
* ***************************************************************************
* ** STM ********************************************************************
* ***************************************************************************
* ***************************************************************************
*/

static int pipe_stm_action_destroy(
            stm_t *stm,
            void *prv,
            unsigned int arg1,
            void *arg2
        )
{
    pipe_t *pipe;

    pipe = prv;

    configurator_client_unregister(pipe->configurator, pipe->cfgr_client);
    node_list_destroy(pipe->pipe_list_pre);
    node_array_params_destroy(pipe);
    node_list_free_handle(pipe->pipe_list_pre);
    osal_mutex_destroy(pipe->pipe_list_start_lock);
    osal_mutex_destroy(pipe->pipe_list_flush_lock);
    pipe_list_destroy(pipe->pipe_list_post);
    pipe_list_destroy(pipe->pipe_list_pre);
    node_array_destroy(pipe->pipe_node_arr);
    stm_destroy(pipe->stm);
    osal_free(pipe);

    return 0;
}

static int pipe_stm_action_start_n(
            stm_t *stm,
            void *prv,
            unsigned int arg1,
            void *arg2
        )
{
    pipe_t *pipe;
    int err;

    pipe = prv;

    err = node_list_start(
            pipe->pipe_list_pre,
            &pipe->pipe_list_start_request,
            pipe->pipe_list_start_lock,
            arg2
        );
    if (err) {
        pipe_stm_event_start_nerr(pipe);
    } else {
        pipe_stm_event_start_ndone(pipe);
    }

    return err;
}

static int pipe_stm_action_start_nerr(
            stm_t *stm,
            void *prv,
            unsigned int arg1,
            void *arg2
        )
{
    pipe_t *pipe;
    pipe = prv;
    osal_mutex_lock(pipe->pipe_list_start_lock);
    if (list_empty(&pipe->pipe_list_start_request)) {
        osal_mutex_unlock(pipe->pipe_list_start_lock);
        pipe_stm_event_start_done(pipe);
    }
    osal_mutex_unlock(pipe->pipe_list_start_lock);
    return -1;
}

static int pipe_stm_action_start_ok(
            stm_t *stm,
            void *prv,
            unsigned int arg1,
            void *arg2
        )
{
    pipe_t *pipe;
    pipe_node_t *pipe_node;

    pipe = prv;
    pipe_node = arg2;

    osal_mutex_lock(pipe->pipe_list_start_lock);
    list_move(&pipe_node->link, &pipe->pipe_list_start_complete);
    if (list_empty(&pipe->pipe_list_start_request)) {
        osal_mutex_unlock(pipe->pipe_list_start_lock);
        pipe_stm_event_start_done_as(pipe);
    }
    osal_mutex_unlock(pipe->pipe_list_start_lock);

    return 0;
}

static int pipe_stm_action_start_err(
            stm_t *stm,
            void *prv,
            unsigned int arg1,
            void *arg2
        )
{
    pipe_t *pipe;
    pipe_node_t *pipe_node;

    pipe = prv;
    pipe_node = arg2;

    osal_mutex_lock(pipe->pipe_list_start_lock);
    list_del(&pipe_node->link);
    if (list_empty(&pipe->pipe_list_start_request)) {
        osal_mutex_unlock(pipe->pipe_list_start_lock);
        pipe_stm_event_start_done_as(pipe);
    }
    osal_mutex_unlock(pipe->pipe_list_start_lock);

    return 0;
}

static int pipe_stm_action_start_dok(
            stm_t *stm,
            void *prv,
            unsigned int arg1,
            void *arg2
        )
{
    pipe_t *pipe;
    pipe_event_id_t event_id;

    pipe = prv;

    INIT_LIST_HEAD(&pipe->pipe_list_start_complete);

    event_id.v = 0;
    event_id.type = PIPE_EVENT_START_DONE;
    pipe->callback(
            pipe,
            pipe->app_private,
            event_id,
            0,
            NULL
        );

    return 0;
}

static int pipe_stm_action_start_derr(
            stm_t *stm,
            void *prv,
            unsigned int arg1,
            void *arg2
        )
{
    pipe_t *pipe;
    pipe_event_id_t event_id;

    pipe = prv;

    node_list_prv_stop(&pipe->pipe_list_start_complete);
    INIT_LIST_HEAD(&pipe->pipe_list_start_complete);

    event_id.v = 0;
    event_id.type = PIPE_EVENT_START_ERR;
    pipe->callback(
            pipe,
            pipe->app_private,
            event_id,
            0,
            NULL
        );

    return 0;
}

static int pipe_stm_action_stop(
            stm_t *stm,
            void *prv,
            unsigned int arg1,
            void *arg2
        )
{
    pipe_t *pipe;
    pipe = prv;
    node_list_stop(pipe->pipe_list_pre);
    return 0;
}

static int pipe_stm_action_flush(
            stm_t *stm,
            void *prv,
            unsigned int arg1,
            void *arg2
        )
{
    pipe_t *pipe;
    pipe = prv;
    node_list_flush(
            pipe->pipe_list_pre,
            &pipe->pipe_list_flush,
            pipe->pipe_list_flush_lock
        );
    return 0;
}

static int pipe_stm_action_flush_done(
            stm_t *stm,
            void *prv,
            unsigned int arg1,
            void *arg2
        )
{
    pipe_t *pipe;
    pipe_event_id_t event_id;

    pipe = prv;

    event_id.v = 0;
    event_id.type = PIPE_EVENT_FLUSH_DONE;
    pipe->callback(
            pipe,
            pipe->app_private,
            event_id,
            0,
            NULL
        );
    return 0;
}

static int pipe_stm_action_alter(
            stm_t *stm,
            void *prv,
            unsigned int arg1,
            void *arg2
        )
{
    pipe_t *pipe;
    pipe = prv;
    return node_list_config_alter(pipe->pipe_list_post, arg2);
}

static int pipe_stm_action_process(
            stm_t *stm,
            void *prv,
            unsigned int arg1,
            void *arg2
        )
{
    pipe_t *pipe;
    node_proc_data_t pd;

    pipe = prv;

    pd.origin = PIPE_SRC_NODE;
    pd.inc_event_id = PIPE_SRC_NODE_DESC_EVENT;
    pd.data = arg2;

    return node_process(pipe->root_node->node, &pd);
}

static int pipe_stm_action_no_action(
            stm_t *stm,
            void *prv,
            unsigned int arg1,
            void *arg2
        )
{
    return 0;
}

static int pipe_stm_action_err(
            stm_t *stm,
            void *prv,
            unsigned int arg1,
            void *arg2
        )
{
    pipe_t *pipe;
    pipe = prv;
    mmsdbg(DL_ERROR, "Pipe: %s. Invalid STM state + operation!", pipe->name);
    return -1;
}

static stm_actions_t pipe_stm_actions[] = {
    pipe_stm_action_destroy,
    pipe_stm_action_start_n,
    pipe_stm_action_start_nerr,
    pipe_stm_action_start_ok,
    pipe_stm_action_start_err,
    pipe_stm_action_start_dok,
    pipe_stm_action_start_derr,
    pipe_stm_action_stop,
    pipe_stm_action_flush,
    pipe_stm_action_flush_done,
    pipe_stm_action_alter,
    pipe_stm_action_process,
    pipe_stm_action_no_action,
    pipe_stm_action_err
};

enum {
    PIPE_STM_STATE_IDLE,
    PIPE_STM_STATE_IDLE2RUN_N,
    PIPE_STM_STATE_IDLE2RUN_ERR,
    PIPE_STM_STATE_IDLE2RUN,
    PIPE_STM_STATE_RUN2IDLE,
    PIPE_STM_STATE_RUN,
    PIPE_STM_STATE_FLUSH,
    PIPE_STM_STATE_DESTROY,
    PIPE_STM_STATES_NUMBER
};

enum {
    PIPE_STM_EVENT_DESTROY,
    PIPE_STM_EVENT_START,
    PIPE_STM_EVENT_START_NERR,
    PIPE_STM_EVENT_START_NDONE,
    PIPE_STM_EVENT_START_OK,
    PIPE_STM_EVENT_START_ERR,
    PIPE_STM_EVENT_START_DONE,
    PIPE_STM_EVENT_STOP,
    PIPE_STM_EVENT_STOP_OK,
    PIPE_STM_EVENT_STOP_ERR,
    PIPE_STM_EVENT_FLUSH,
    PIPE_STM_EVENT_FLUSH_DONE,
    PIPE_STM_EVENT_ALTER,
    PIPE_STM_EVENT_PROCESS,
    PIPE_STM_EVENTS_NUMBER
};

enum {
    PIPE_STM_ACTION_DESTROY,
    PIPE_STM_ACTION_START_N,
    PIPE_STM_ACTION_START_NERR,
    PIPE_STM_ACTION_START_OK,
    PIPE_STM_ACTION_START_ERR,
    PIPE_STM_ACTION_START_DOK,
    PIPE_STM_ACTION_START_DERR,
    PIPE_STM_ACTION_STOP,
    PIPE_STM_ACTION_FLUSH,
    PIPE_STM_ACTION_FLUSH_DONE,
    PIPE_STM_ACTION_ALTER,
    PIPE_STM_ACTION_PROCESS,
    PIPE_STM_ACTION_NO_ACTION,
    PIPE_STM_ACTION_ERR,
    PIPE_STM_ACTIONS_NUMBER
};

#define STM_STATE_PREFIX PIPE_STM_STATE_
#define STM_ACTIOIN_PREFIX PIPE_STM_ACTION_

STM_DESC_BUILD_BEGIN(
            pipe_stm,
            PIPE_STM_STATE_IDLE,
            PIPE_STM_STATES_NUMBER,
            PIPE_STM_EVENTS_NUMBER,
            PIPE_STM_ACTIONS_NUMBER
        )
                          /* |-IDLE-----------------|  |-IDLE2RUN_N-----------|  |-IDLE2RUN_ERR---------|  |-IDLE2RUN------------| */
/* DESTROY      */ STM_E4x(  DESTROY     ,   DESTROY,  IDLE2RUN_N  ,       ERR,  IDLE2RUN_ERR,       ERR,  IDLE2RUN    ,       ERR  )
/* START        */ STM_E4x(  IDLE2RUN_N  ,   START_N,  IDLE2RUN_N  ,       ERR,  IDLE2RUN_ERR,       ERR,  IDLE2RUN    ,       ERR  )
/* START_NERR   */ STM_E4x(  IDLE        ,       ERR,  IDLE2RUN_ERR,START_NERR,  IDLE2RUN_ERR,       ERR,  IDLE2RUN    ,       ERR  )
/* START_NDONE  */ STM_E4x(  IDLE        ,       ERR,  IDLE2RUN    , NO_ACTION,  IDLE2RUN_ERR,       ERR,  IDLE2RUN    ,       ERR  )
/* START_OK     */ STM_E4x(  IDLE        ,       ERR,  IDLE2RUN_N  ,       ERR,  IDLE2RUN_ERR,  START_OK,  IDLE2RUN    ,  START_OK  )
/* START_ERR    */ STM_E4x(  IDLE        ,       ERR,  IDLE2RUN_N  ,       ERR,  IDLE2RUN_ERR, START_ERR,  IDLE2RUN_ERR, START_ERR  )
/* START_DONE   */ STM_E4x(  IDLE        ,       ERR,  IDLE2RUN_N  ,       ERR,  IDLE        ,START_DERR,  RUN         , START_DOK  )
/* STOP         */ STM_E4x(  IDLE        ,       ERR,  IDLE2RUN_N  ,       ERR,  IDLE2RUN_ERR,       ERR,  IDLE2RUN    ,       ERR  )
/* STOP_OK      */ STM_E4x(  IDLE        ,       ERR,  IDLE2RUN_N  ,       ERR,  IDLE2RUN_ERR,       ERR,  IDLE2RUN    ,       ERR  )
/* STOP_ERR     */ STM_E4x(  IDLE        ,       ERR,  IDLE2RUN_N  ,       ERR,  IDLE2RUN_ERR,       ERR,  IDLE2RUN    ,       ERR  )
/* FLUSH        */ STM_E4x(  IDLE        ,       ERR,  IDLE2RUN_N  ,       ERR,  IDLE2RUN_ERR,       ERR,  IDLE2RUN    ,       ERR  )
/* FLUSH_DONE   */ STM_E4x(  IDLE        ,       ERR,  IDLE2RUN_N  ,       ERR,  IDLE2RUN_ERR,       ERR,  IDLE2RUN    ,       ERR  )
/* ALTER        */ STM_E4x(  IDLE        ,     ALTER,  IDLE2RUN_N  ,       ERR,  IDLE2RUN_ERR,       ERR,  IDLE2RUN    ,       ERR  )
/* PROCESS      */ STM_E4x(  IDLE        ,       ERR,  IDLE2RUN_N  ,       ERR,  IDLE2RUN_ERR,       ERR,  IDLE2RUN    ,       ERR  )

                          /* |-RUN2IDLE-------------|  |-RUN------------------|  |-FLUSH----------------|  |-DESTROY-------------| */
/* DESTROY      */ STM_E4x(  RUN2IDLE    ,       ERR,  RUN         ,       ERR,  FLUSH       ,       ERR,  DESTROY     ,       ERR  )
/* START        */ STM_E4x(  RUN2IDLE    ,       ERR,  RUN         ,       ERR,  FLUSH       ,       ERR,  DESTROY     ,       ERR  )
/* START_NERR   */ STM_E4x(  RUN2IDLE    ,       ERR,  RUN         ,       ERR,  FLUSH       ,       ERR,  DESTROY     ,       ERR  )
/* START_NDONE  */ STM_E4x(  RUN2IDLE    ,       ERR,  RUN         ,       ERR,  FLUSH       ,       ERR,  DESTROY     ,       ERR  )
/* START_OK     */ STM_E4x(  RUN2IDLE    ,       ERR,  RUN         ,       ERR,  FLUSH       ,       ERR,  DESTROY     ,       ERR  )
/* START_ERR    */ STM_E4x(  RUN2IDLE    ,       ERR,  RUN         ,       ERR,  FLUSH       ,       ERR,  DESTROY     ,       ERR  )
/* START_DONE   */ STM_E4x(  RUN2IDLE    ,       ERR,  RUN         ,       ERR,  FLUSH       ,       ERR,  DESTROY     ,       ERR  )
/* STOP         */ STM_E4x(  RUN2IDLE    ,       ERR,  RUN2IDLE    ,      STOP,  FLUSH       ,       ERR,  DESTROY     ,       ERR  )
/* STOP_OK      */ STM_E4x(  IDLE        , NO_ACTION,  RUN         ,       ERR,  FLUSH       ,       ERR,  DESTROY     ,       ERR  )
/* STOP_ERR     */ STM_E4x(  RUN         , NO_ACTION,  RUN         ,       ERR,  FLUSH       ,       ERR,  DESTROY     ,       ERR  )
/* FLUSH        */ STM_E4x(  RUN2IDLE    ,       ERR,  FLUSH       ,     FLUSH,  FLUSH       ,       ERR,  DESTROY     ,       ERR  )
/* FLUSH_DONE   */ STM_E4x(  RUN2IDLE    ,       ERR,  RUN         ,       ERR,  RUN         ,FLUSH_DONE,  DESTROY     ,       ERR  )
/* ALTER        */ STM_E4x(  RUN2IDLE    ,       ERR,  RUN         ,     ALTER,  FLUSH       ,       ERR,  DESTROY     ,       ERR  )
/* PROCESS      */ STM_E4x(  RUN2IDLE    ,       ERR,  RUN         ,   PROCESS,  FLUSH       ,       ERR,  DESTROY     ,       ERR  )

STM_DESC_BUILD_END(pipe_stm)

#define PIPE_STM_ATOMIC(EVENT,N,P) \
    stm_event_atomic( \
            pipe->stm, \
            EVENT, \
            pipe, \
            N, \
            P \
        )

#define PIPE_STM_ATOMIC_STATE(EVENT,N,P) \
    stm_event_atomic_state( \
            pipe->stm, \
            EVENT, \
            pipe, \
            N, \
            P \
        )

#define PIPE_STM(EVENT,N,P) \
    stm_event( \
            pipe->stm, \
            EVENT, \
            pipe, \
            N, \
            P \
        )

int pipe_stm_event_start_nerr(pipe_t *pipe)
{
    return PIPE_STM(PIPE_STM_EVENT_START_NERR, 0, NULL);
}

int pipe_stm_event_start_ndone(pipe_t *pipe)
{
    return PIPE_STM(PIPE_STM_EVENT_START_NDONE, 0, NULL);
}

int pipe_stm_event_start_done(pipe_t *pipe)
{
    return PIPE_STM(PIPE_STM_EVENT_START_DONE, 0, NULL);
}

int pipe_stm_event_start_done_as(pipe_t *pipe)
{
    return PIPE_STM_ATOMIC_STATE(PIPE_STM_EVENT_START_DONE, 0, NULL);
}

/*
* ***************************************************************************
* ** node callback **********************************************************
* ***************************************************************************
*/

static void node_callback(
        node_t *node,
        void *prv,
        pipe_event_id_t event_id,
        int num,
        void *ptr
    )
{
    pipe_node_t *pipe_node;
    pipe_t *pipe;

    pipe_node = prv;
    pipe = pipe_node->pipe;

    switch (event_id.type) {
        case PIPE_EVENT_INC:
        case PIPE_EVENT_FLUSHING:
            pipe->callback(
                    pipe,
                    pipe->app_private,
                    event_id,
                    num,
                    ptr
                );
            break;
        case PIPE_EVENT_FLUSH_DONE:
            osal_mutex_lock(pipe->pipe_list_flush_lock);
            list_del(&pipe_node->link);
            if (list_empty(&pipe->pipe_list_flush)) {
                osal_mutex_unlock(pipe->pipe_list_flush_lock);
                node_list_flush_done(pipe->pipe_list_pre);
                PIPE_STM_ATOMIC_STATE(PIPE_STM_EVENT_FLUSH_DONE, 0, NULL);
            } else {
                osal_mutex_unlock(pipe->pipe_list_flush_lock);
            }
            break;
        case PIPE_EVENT_START_DONE:
            PIPE_STM_ATOMIC_STATE(PIPE_STM_EVENT_START_OK, 0, prv);
            break;
        case PIPE_EVENT_START_ERR:
            PIPE_STM_ATOMIC_STATE(PIPE_STM_EVENT_START_ERR, 0, prv);
            break;
        default:
            mmsdbg(
                    DL_ERROR,
                    "Pipe: %s. Unknown pipe event type: type=%d; v=0x%x!",
                    pipe->name,
                    event_id.type,
                    event_id.v
                );
    }
}

/*
* ***************************************************************************
* ** configurator client ****************************************************
* ***************************************************************************
*/

static int pipe_cfgr_client_reg(
            configurator_t *c,
            configurator_client_t *client,
            void *client_prv,
            void *config
        )
{
    return 0;
}

static int pipe_cfgr_client_unreg(
            configurator_t *c,
            configurator_client_t *client,
            void *client_prv,
            void *config
        )
{
    return 0;
}

static int pipe_cfgr_client_alter(
            configurator_t *c,
            configurator_client_t *client,
            void *client_prv,
            void *config
        )
{
    pipe_t *pipe;
    pipe = client_prv;
    return PIPE_STM_ATOMIC_STATE(PIPE_STM_EVENT_ALTER, 0, config);
}

static void pipe_cfgr_client_notify(
            configurator_t *c,
            configurator_client_t *client,
            void *client_prv,
            void *config
        )
{
}

static configurator_client_desc_t pipe_cfgr_client_desc = {
    .name = "Pipe",
    .registered = pipe_cfgr_client_reg,
    .unregistered = pipe_cfgr_client_unreg,
    .alternate = pipe_cfgr_client_alter,
    .notify = pipe_cfgr_client_notify
};

/*
* ***************************************************************************
* ***************************************************************************
* ** Interface Part *********************************************************
* ***************************************************************************
* ***************************************************************************
*/

int pipe_start(pipe_t *pipe, void *params)
{
    return PIPE_STM_ATOMIC(PIPE_STM_EVENT_START, 0, params);
}

int pipe_stop(pipe_t *pipe)
{
    int err;
    err = PIPE_STM_ATOMIC_STATE(PIPE_STM_EVENT_STOP, 0, NULL);
    if (err) {
        PIPE_STM_ATOMIC_STATE(PIPE_STM_EVENT_STOP_ERR, 0, NULL);
    } else {
        PIPE_STM_ATOMIC_STATE(PIPE_STM_EVENT_STOP_OK, 0, NULL);
    }
    return err;
}

int pipe_flush(pipe_t *pipe)
{
    return PIPE_STM_ATOMIC_STATE(PIPE_STM_EVENT_FLUSH, 0, NULL);
}

int pipe_process(pipe_t *pipe, void *data)
{
    return PIPE_STM_ATOMIC_STATE(PIPE_STM_EVENT_PROCESS, 0, data);
}

void pipe_destroy(pipe_t *pipe)
{
    PIPE_STM_ATOMIC_STATE(PIPE_STM_EVENT_DESTROY, 0, NULL);
}

pipe_t *pipe_create(pipe_desc_t *pipe_desc, pipe_create_params_t *params)
{
    pipe_t *pipe;
    int err;

    pipe = osal_calloc(1, sizeof (*pipe));
    if (!pipe) {
        mmsdbg(
                DL_ERROR,
                "Pipe: %s. Failed to allocate memory for new instance: size=%d!",
                pipe_desc->name,
                sizeof (*pipe)
            );
        goto exit1;
    }
    INIT_LIST_HEAD(&pipe->pipe_list_flush);
    INIT_LIST_HEAD(&pipe->pipe_list_start_request);
    INIT_LIST_HEAD(&pipe->pipe_list_start_complete);

    pipe->name = pipe_desc->name;

    pipe->pipe_desc = pipe_desc;

    pipe->app_private = params->app_private;
    pipe->resources = params->resources;
    pipe->configurator = params->configurator;
    pipe->callback = params->callback;

    pipe->stm = stm_create(
            pipe->name,
            &pipe_stm,
            pipe_stm_actions
        );
    if (!pipe->stm) {
        mmsdbg(DL_ERROR, "Pipe: %s. Failed to create STM!", pipe->name);
        goto exit2;
    }

    pipe->pipe_node_arr_size = count_nodes(pipe->pipe_desc->root_node);
    if (pipe->pipe_node_arr_size < 0) {
        mmsdbg(DL_ERROR, "Pipe: %s. Node count failed!", pipe->name);
        goto exit3;
    }

    pipe->pipe_node_arr = node_array_create(
            pipe,
            pipe->pipe_desc->root_node,
            pipe->pipe_node_arr_size
        );
    if (!pipe->pipe_node_arr) {
        mmsdbg(
                DL_ERROR,
                "Pipe: %s. Failed to create Node Array!",
                pipe->name
            );
        goto exit3;
    }
    pipe->root_node = pipe->pipe_node_arr;

    pipe->pipe_list_pre = pipe_list_create_pre(
            pipe->pipe_desc->root_node,
            pipe->pipe_node_arr,
            pipe->pipe_node_arr_size
        );
    if (!pipe->pipe_list_pre) {
        mmsdbg(
                DL_ERROR,
                "Pipe: %s. Failed to create Pipe List Pre!",
                pipe->name
            );
        goto exit4;
    }

    pipe->pipe_list_post = pipe_list_create_post(
            pipe->pipe_desc->root_node,
            pipe->pipe_node_arr,
            pipe->pipe_node_arr_size
        );
    if (!pipe->pipe_list_post) {
        mmsdbg(
                DL_ERROR,
                "Pipe: %s. Failed to create Pipe List Post!",
                pipe->name
            );
        goto exit5;
    }

    pipe->pipe_list_flush_lock = osal_mutex_create();
    if (!pipe->pipe_list_flush_lock) {
        mmsdbg(
                DL_ERROR,
                "Pipe: %s. Failed to create mutex for Flush List lock!",
                pipe->name
            );
        goto exit6;
    }

    pipe->pipe_list_start_lock = osal_mutex_create();
    if (!pipe->pipe_list_start_lock) {
        mmsdbg(
                DL_ERROR,
                "Pipe: %s. Failed to create mutex for Start List lock!",
                pipe->name
            );
        goto exit7;
    }

    err = node_list_get_handle(pipe->pipe_list_pre);
    if (err) {
        mmsdbg(
                DL_ERROR,
                "Pipe: %s. Failed to get nodes handle!",
                pipe->name
            );
        goto exit8;
    }

    err = node_array_params_create(pipe);
    if (err) {
        mmsdbg(
                DL_ERROR,
                "Pipe: %s. Failed to create nodes param!",
                pipe->name
            );
        goto exit9;
    }

    err = node_list_create(pipe->pipe_list_pre);
    if (err) {
        mmsdbg(DL_ERROR, "Pipe: %s. Failed to create nodes!", pipe->name);
        goto exit10;
    }

    pipe->cfgr_client = configurator_client_register(
            pipe->configurator,
            &pipe_cfgr_client_desc,
            pipe
        );
    if (!pipe->cfgr_client) {
        mmsdbg(
                DL_ERROR,
                "Pipe: %s. Failed to register in configurator!",
                pipe->name
            );
        goto exit11;
    }

    return pipe;
exit11:
    node_list_destroy(pipe->pipe_list_pre);
exit10:
    node_array_params_destroy(pipe);
exit9:
    node_list_free_handle(pipe->pipe_list_pre);
exit8:
    osal_mutex_destroy(pipe->pipe_list_start_lock);
exit7:
    osal_mutex_destroy(pipe->pipe_list_flush_lock);
exit6:
    pipe_list_destroy(pipe->pipe_list_post);
exit5:
    pipe_list_destroy(pipe->pipe_list_pre);
exit4:
    node_array_destroy(pipe->pipe_node_arr);
exit3:
    stm_destroy(pipe->stm);
exit2:
    osal_free(pipe);
exit1:
    return NULL;
}

