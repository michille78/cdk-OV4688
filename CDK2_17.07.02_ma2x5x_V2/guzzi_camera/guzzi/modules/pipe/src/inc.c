/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file inc.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <osal/osal_stdlib.h>
#include <osal/osal_string.h>
#include <utils/mms_debug.h>
#include <framerequest/camera/camera_frame_request.h>
#include <inc.h>

extern inc_table_entry_t inc_table[];

mmsdbg_define_variable(
        vdl_inc,
        DL_DEFAULT,
        0,
        "inc",
        "Integration Node Component common."
    );
#define MMSDEBUGLEVEL mmsdbg_use_variable(vdl_inc)

static inc_table_entry_t * find_inc_entry_by_name(
        inc_table_entry_t *table,
        char *neme
    )
{
    inc_table_entry_t *e;

    for (e = table; !INC_TABLE_ENTRY_IS_LAST(e); e++) {
        if (!osal_strncmp(neme, e->name, INC_NAME_MAX_LEN)) {
            return e;
        }
    }

    return NULL;
}

int inc_start(inc_t *inc, void *params)
{
    int err;

    err = 0;
    if (inc->inc_start) {
        err = inc->inc_start(inc, params);
    }

    return err;
}

void inc_stop(inc_t *inc)
{
    if (inc->inc_stop) {
        inc->inc_stop(inc);
    }
}

void inc_flush(inc_t *inc)
{
    if (inc->inc_flush) {
        inc->inc_flush(inc);
    }
}

int inc_config_alter(inc_t *inc, void *config)
{
    int err;

    err = 0;
    if (inc->inc_config_alter) {
        err = inc->inc_config_alter(inc, config);
    }

    return err;
}

void inc_process(inc_t *inc, void *data)
{
    if (inc->inc_config && camera_fr_need_to_reconf(data, inc->reconfig_key)) {
        inc->inc_config(inc, data);
    }
    if (inc->inc_process) {
        inc->inc_process(inc, data);
    }
}

void inc_destroy(inc_t *inc)
{
    if (inc->inc_destroy) {
        inc->inc_destroy(inc);
    }
    camera_fr_deinit_reconf_query(inc->reconfig_key);
    osal_free(inc);
}

inc_t *inc_create(
        char *name,
        void *params,
        void *app_res,
        inc_callback_t callback,
        void *client_prv
    )
{
    inc_t *inc;
    inc_table_entry_t *e;
    int err;

    e = find_inc_entry_by_name(inc_table, name);
    if (!e) {
        mmsdbg(DL_ERROR, "Unable to find INC: %s!", name);
        goto exit1;
    }

    inc = osal_calloc(1, sizeof (*inc));
    if (!inc) {
        mmsdbg(
                DL_ERROR,
                "INC: %s. Failed to allocate memory for new instance: size=%d!",
                name,
                sizeof (*inc)
            );
        goto exit1;
    }

    inc->reconfig_key = camera_fr_init_reconf_query(); /* TODO: */
    if (!inc->reconfig_key) {
        mmsdbg(
                DL_ERROR,
                "INC: %s. Failed to initialize reconfig key!",
                name
            );
        goto exit2;
    }

    inc->name = e->name;
    inc->client_prv = client_prv;
    inc->callback = callback;

     err = e->inc_create(
            inc,
            params,
            app_res
        );
    if (err) {
        mmsdbg(DL_ERROR, "INC: %s. Failed to create!", name);
        goto exit3;
    }

    return inc;
exit3:
    camera_fr_deinit_reconf_query(inc->reconfig_key); /* TODO: */
exit2:
    osal_free(inc);
exit1:
    return NULL;
}

