/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file inc.h
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#ifndef _INC_H
#define _INC_H

#include <osal/osal_stdlib.h>
#include <pipe/include/inc_prv.h>

#define INC_RES_OFFSET(TYPE,FIELD) \
    ((unsigned int)&(((TYPE *)0)->FIELD))

#define INC_RES_GET(RES,OFFSET) \
    ((void *)((char *)(RES) + (OFFSET)))
#define INC_RES_GET_PTR(RES,OFFSET) \
    (*(void **)((char *)(RES) + (OFFSET)))
#define INC_RES_GET_INT(RES,OFFSET) \
    (*(int *)((char *)(RES) + (OFFSET)))
#define INC_RES_GET_UINT(RES,OFFSET) \
    (*(unsigned int *)((char *)(RES) + (OFFSET)))

/* Including the last zero */
#define INC_NAME_MAX_LEN 512

typedef struct inc inc_t;
typedef struct inc_table_entry inc_table_entry_t;

typedef void (*inc_callback_t)(
        inc_t *inc,
        void *client_prv,
        unsigned int event_id,
        int num,
        void *ptr
    );

typedef int (*inc_create_func_t)(
        inc_t *inc,
        void *paramsr,
        void *app_res
    );

struct inc {
    char *name;
    inc_prv_t prv;
    inc_callback_t callback;
    void *client_prv;
    int (*inc_start)(inc_t *inc, void *params);
    void (*inc_stop)(inc_t *inc);
    void (*inc_flush)(inc_t *inc);
    int (*inc_config_alter)(inc_t *inc, void *config);
    int (*inc_config)(inc_t *inc, void *data);
    void (*inc_process)(inc_t *inc, void *data);
    void (*inc_destroy)(inc_t *inc);

    void *reconfig_key; /* TODO: */
};

#define INC_TABLE_ENTRY_LAST {NULL, NULL}
#define INC_TABLE_ENTRY_IS_LAST(E) \
    (((E)->name == NULL) && ((E)->inc_create == NULL))

struct inc_table_entry {
    char *name;
    inc_create_func_t inc_create;
};

int inc_start(inc_t *inc, void *params);
void inc_stop(inc_t *inc);
void inc_flush(inc_t *inc);
int inc_config_alter(inc_t *inc, void *config);
void inc_process(inc_t *inc, void *data);
void inc_destroy(inc_t *inc);
inc_t *inc_create(
        char *name,
        void *params,
        void *app_res,
        inc_callback_t callback,
        void *client_prv
    );

#endif /* _INC_H */

