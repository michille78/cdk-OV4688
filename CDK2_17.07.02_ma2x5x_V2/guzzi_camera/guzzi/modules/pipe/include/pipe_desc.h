/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file pipe_desc.h
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#ifndef _PIPE_DESCx_H
#define _PIPE_DESCx_H 

#define PIPE_SRC_NODE_DESC (&pipe_src_node_desc)
#define PIPE_SRC_NODE_DESC_EVENT 0

/* In Event Macro */
#define NODE_DESC_IN_EVENTS_MAX 16

#define NODE_DESC_IN_EVENT_ORIGIN_NIL NULL
#define NODE_DESC_IN_EVENT_ID_NIL 0xFFFF
#define NODE_DESC_IN_EVENT_INIT_NIL 0x1234

#define NODE_DESC_IN_EVENT_NIL \
    { \
        NODE_DESC_IN_EVENT_ORIGIN_NIL, \
        NODE_DESC_IN_EVENT_ID_NIL, \
        NODE_DESC_IN_EVENT_INIT_NIL \
    }

#define NODE_DESC_IN_EVENT_IS_NIL(IN) \
    ( \
           (NODE_DESC_IN_EVENT_ORIGIN_NIL == (IN)->origin) \
        && (NODE_DESC_IN_EVENT_ID_NIL     == (IN)->inc_event_id) \
        && (NODE_DESC_IN_EVENT_INIT_NIL   == (IN)->initial_count) \
    )

/* Out Event Translation Table Macro */
#define NODE_DESC_OUT_EVENT_MAX 16

#define NODE_DESC_OUT_EVENT_ID_NIL 0xFFFF
#define NODE_DESC_OUT_EVENT_NOTIFY_NIL 0x4321

#define NODE_DESC_OUT_EVENT_NIL \
    { \
        NODE_DESC_OUT_EVENT_ID_NIL, \
        NODE_DESC_OUT_EVENT_NOTIFY_NIL \
    }

#define NODE_DESC_OUT_EVENT_IS_NIL(OUT) \
    ( \
           (NODE_DESC_OUT_EVENT_ID_NIL     == (OUT)->inc_event_id) \
        && (NODE_DESC_OUT_EVENT_NOTIFY_NIL == (OUT)->notify_upper_level) \
    )

/* Next Node Macro */
#define NODE_DESC_NEXT_MAX 16
#define NODE_DESC_NEXT_NIL NULL
#define NODE_DESC_NEXT_IS_NIL(N) (NODE_DESC_NEXT_NIL == (N))

/*
* ***************************************************************************
* ** NODE description macros ************************************************
* ***************************************************************************
*/

/* Node desc declare */
#define NODE_DESC_DECLARE(NAME) \
    static node_desc_t NAME;

#define NODE_DESC_EVENT_SOURCE \
        { \
            .origin = PIPE_SRC_NODE_DESC, \
            .inc_event_id = PIPE_SRC_NODE_DESC_EVENT, \
            .initial_count = 0 \
        },

/* Node desc definition */
#define NODE_DESC_BUILD_BEGIN(NAME,INC_NAME,STACK_SIZE,PRIO,INC_PARAMS,DATA_EVENT_NUM) \
    static node_desc_in_event_t NAME##_in_events[]; \
    static node_desc_t *NAME##_next_desc[]; \
    static node_desc_out_events_t NAME##_out_events[]; \
    static node_desc_t NAME = { \
        .name = #NAME, \
        \
        .in_events = NAME##_in_events, \
        .next_desc = NAME##_next_desc, \
        .out_events = NAME##_out_events, \
        \
        .in_event_data = &NAME##_in_events[DATA_EVENT_NUM], \
        \
        .thread_stack_size = STACK_SIZE, \
        .thread_priority = PRIO, \
        \
        .inc_name = INC_NAME, \
        .inc_params = &INC_PARAMS, \
        \
        .active = 1 \
    };
#define NODE_DESC_BUILD_END(NAME)

/* In events */
#define NODE_DESC_IN_EVENTS_BEGIN(NAME) \
    static node_desc_in_event_t NAME##_in_events[] = {
#define NODE_DESC_IN_EVENT(ORIGIN,ID,INIT) \
        { \
            .origin = &ORIGIN, \
            .inc_event_id = ID, \
            .initial_count = INIT \
        },
#define NODE_DESC_IN_EVENTS_END(NAME) \
        NODE_DESC_IN_EVENT_NIL \
    };

/* Next node desc */
#define NODE_DESC_NEXT_BEGIN(NAME) \
    static node_desc_t *NAME##_next_desc[] = {
#define NODE_DESC_NEXT(NEXT) \
        &NEXT,
#define NODE_DESC_NEXT_END(NAME) \
        NODE_DESC_NEXT_NIL \
    };

/* Out events */
#define NODE_DESC_OUT_EVENTS_BEGIN(NAME) \
    static node_desc_out_events_t NAME##_out_events[] = {
#define NODE_DESC_OUT_EVENT(ID,NOTIFY) \
        { \
            .inc_event_id = ID, \
            .notify_upper_level = NOTIFY \
        },
#define NODE_DESC_OUT_EVENTS_END(NAME) \
        NODE_DESC_OUT_EVENT_NIL \
    };

#define PIPE_DESC(NAME, ROOT) \
    static pipe_desc_t NAME = { \
        .name = #NAME, \
        .root_node = &ROOT \
    };

typedef struct pipe_desc pipe_desc_t;
typedef struct node_desc node_desc_t;
typedef struct node_desc_in_event node_desc_in_event_t;
typedef struct node_desc_out_events node_desc_out_events_t;
typedef struct pipe pipe_t;

struct node_desc_in_event {
    node_desc_t *origin;
    unsigned int inc_event_id;
    int initial_count;
};

struct node_desc_out_events {
    unsigned int inc_event_id;
    int notify_upper_level;
};

struct node_desc {
    char *name;

    node_desc_in_event_t *in_events;
    node_desc_t **next_desc;
    node_desc_out_events_t *out_events;

    node_desc_in_event_t *in_event_data;

    int thread_stack_size;
    int thread_priority;

    char *inc_name;
    void *inc_params;

    int active;
};

struct pipe_desc {
    char *name;
    node_desc_t *root_node;
};

extern node_desc_t pipe_src_node_desc;

#endif /* _PIPE_DESC_H */

