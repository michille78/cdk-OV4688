/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file inc_prv.h
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#ifndef _INC_PRV_H
#define _INC_PRV_H

typedef struct inc_control_logic_priv_data  inc_control_logic_priv_data_t;
typedef struct inc_auto_cam_algs_priv_data  inc_auto_cam_algs_priv_data_t;
typedef struct inc_camera_priv_data         inc_camera_priv_data_t;
typedef struct int_frame_info               inc_frame_info_t;
typedef struct inc_ipipe_priv_data          inc_ipipe_priv_data_t;
typedef struct int_print_to_img             int_print_to_img_t;
typedef struct inc_settings_gen_priv_data   inc_settings_gen_priv_data_t;

typedef union {
    void *ptr;
    union {
        inc_control_logic_priv_data_t *control_logic;
        inc_auto_cam_algs_priv_data_t *auto_cam_algs;
        inc_camera_priv_data_t        *camera;
        inc_frame_info_t              *frame_info;
        inc_ipipe_priv_data_t         *ipipe;
        int_print_to_img_t            *print_to_img;
        inc_settings_gen_priv_data_t  *settings_gen;
    } dbg;
} inc_prv_t;

#endif /* _INC_PRV_H */

