/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file pipe.h
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#ifndef _PIPE_H
#define _PIPE_H

#include <configurator/include/configurator.h>
#include <pipe/include/pipe_event_id.h>
#include <pipe/include/pipe_desc.h>

typedef struct pipe pipe_t;
typedef struct pipe_create_params pipe_create_params_t;

typedef void (*pipe_callback_t)(
        pipe_t *pipe,
        void *app_private,
        pipe_event_id_t event_id,
        int num,
        void *ptr
    );

struct pipe_create_params {
    void *app_private;
    void *resources;
    configurator_t *configurator;
    pipe_callback_t callback;
};

int pipe_start(pipe_t *pipe, void *params);
int pipe_stop(pipe_t *pipe);
int pipe_flush(pipe_t *pipe);

int pipe_process(pipe_t *pipe, void *data);

void pipe_destroy(pipe_t *pipe);
pipe_t *pipe_create(pipe_desc_t *pipe_desc, pipe_create_params_t *params);

#endif /* _PIPE_H */

