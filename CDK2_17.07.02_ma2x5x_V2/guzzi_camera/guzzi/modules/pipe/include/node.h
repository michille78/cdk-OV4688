/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file node.h
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#ifndef _NODE_H
#define _NODE_H

#include <configurator/include/configurator.h>
#include <pipe_event_id.h>

#define PIPE_SRC_NODE pipe_src_node

#define NODE_IN_MATCH_DESC(IN,DESC) \
    ( \
           ((IN)->origin == (DESC)->origin) \
        && ((IN)->inc_event_id == (DESC)->inc_event_id) \
    )

typedef struct node node_t;
typedef struct node_create_params node_create_params_t;
typedef struct node_in_events node_in_events_t;
typedef struct node_out_events node_out_events_t;
typedef struct node_proc_data node_proc_data_t;

typedef void (*node_callback_t)(
        node_t *node,
        void *prv,
        pipe_event_id_t event_id,
        int num,
        void *ptr
    );

struct node_proc_data {
    node_t *origin;
    unsigned int inc_event_id;
    void *data;
};

struct node_in_events {
    node_t *origin;
    unsigned int inc_event_id;
    int initial_count;
};

struct node_out_events {
    unsigned int inc_event_id;
    int notify_upper_level;
    node_t **next;
    int next_count;
};

struct node_create_params {
    char *name;
    void *app_private;
    void *resources;
    configurator_t *configurator;

    node_in_events_t *in_events;
    node_out_events_t *out_events;
    int in_events_count;
    int out_events_count;

    node_in_events_t *in_event_data;

    int thread_stack_size;
    int thread_priority;

    char *inc_name;
    void *inc_params;

    int active;

    node_callback_t callback;
};

int node_start(node_t *node, void *params);
void node_stop(node_t *node);
void node_flush(node_t *node);
void node_flush_done(node_t *node);
int node_config_alter(node_t *node, void *config);

int node_process(node_t *node, node_proc_data_t *data);

void node_destroy(node_t *node);
int node_create(node_t *node, node_create_params_t *params);

void node_free_handle(node_t *node);
node_t *node_get_handle(void);

extern node_t *pipe_src_node;

#endif /* _NODE_H */

