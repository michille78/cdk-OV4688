/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file camera_frame_request.h
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#ifndef _CAMERA_FRAME_REQUEST_H
#define _CAMERA_FRAME_REQUEST_H

#include <osal/osal_stdlib.h>
#include <configurator/include/configurator.h>

#define CAMERA_FR_OFFSET(FIELD) \
    ((((char *)&(((camera_fr_t *)0)->FIELD)) - (char *)0) / sizeof (camera_fr_entry_t))

#define CAMERA_FR_GET_ENTRY(FR, OFFSET) \
    (((camera_fr_entry_t *)(FR)) + (OFFSET))

#define CAMERA_FR_ADDRESS(FR, OFFSET) \
    ((void *)CAMERA_FR_GET_ENTRY(FR, OFFSET))

#define CAMERA_FR_FOFFSET_INVALID (0xFFFFFFFF)

#define CAMERA_FR_ARR_SIZE \
    (sizeof (camera_fr_named_entries_t) / sizeof (camera_fr_entry_t))

typedef enum {
    CAMERA_FR_ENTRY_FORMAT__UNUSED,
    CAMERA_FR_ENTRY_FORMAT__UNDEFINED,

    /* By Alloc/Free method */
    CAMERA_FR_ENTRY_FORMAT__NO_FREE,
    CAMERA_FR_ENTRY_FORMAT__OSAL,

    /* By data content */
    CAMERA_FR_ENTRY_FORMAT__CONFIG,
    CAMERA_FR_ENTRY_FORMAT__APP_PRIVATE,

    /* External */
    CAMERA_FR_ENTRY_FORMAT__EXT,
    CAMERA_FR_ENTRY_FORMAT__EXT_SENT,
    CAMERA_FR_ENTRY_FORMAT__EXT_SKIP,

    CAMERA_FR_ENTRY_FORMAT__MAX = 255
} camera_fr_entry_format_t;

typedef struct {
    union {
        unsigned int v;
        struct {
            camera_fr_entry_format_t fmt:8;
            unsigned int port:8;
        };
    };
} camera_fr_entry_com_t;

typedef struct {
    camera_fr_entry_com_t com;
    void *data;
} camera_fr_entry_t;

typedef struct {
    camera_fr_entry_t app_prv;
    camera_fr_entry_t cfg;
    union {
#include <camera/mode/frame_request_named_entries.h>
    };
} camera_fr_named_entries_t;

typedef struct {
    union {
        camera_fr_entry_t arr[0];
        camera_fr_named_entries_t n;
    };
} camera_fr_t;

camera_fr_t *camera_fr_duplicate(camera_fr_t *fr_src);

/* ========================================================================== */
/**
* camera_fr_init_reconf_query()
*/
/* ========================================================================== */
static inline void *camera_fr_init_reconf_query(void)
{
    unsigned int *config_id;
    config_id = osal_malloc(sizeof (*config_id));
    *config_id = 0;
    return config_id;
}

/* ========================================================================== */
/**
* camera_fr_deinit_reconf_query()
*/
/* ========================================================================== */
static inline void camera_fr_deinit_reconf_query(void *reconf)
{
    osal_free(reconf);
}

/* ========================================================================== */
/**
* camera_fr_need_to_reconf()
*/
/* ========================================================================== */
static inline int camera_fr_need_to_reconf(camera_fr_t *fr, void *reconf)
{
    configurator_config_struct_t *cfg;
    unsigned int *config_id;
    int result;

    cfg = fr->n.cfg.data;
    config_id = reconf;

    result = *config_id < cfg->node.seq_num;
    *config_id = cfg->node.seq_num;

    return (result);
}

#endif /* _CAMERA_FRAME_REQUEST_H */

