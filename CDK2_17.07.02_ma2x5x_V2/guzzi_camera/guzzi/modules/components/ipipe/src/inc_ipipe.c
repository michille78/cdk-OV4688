/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file inc_ipipe.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <osal/osal_stdlib.h>
#include <utils/mms_debug.h>
#include <pipe/include/inc.h>
#include <error_handle/include/error_handle.h>

#include "inc_ipipe.h"

mmsdbg_define_variable(
        vdl_inc_ipipe,
        DL_DEFAULT,
        0,
        "inc.ipipe",
        "INC IPIPE"
    );
#define MMSDEBUGLEVEL mmsdbg_use_variable(vdl_inc_ipipe)

/** inc_ipipe_priv_data_t
* Defining the gzz module application data. */
struct inc_ipipe_priv_data {
    uint32      fr_offset_cfg;
};

/* ========================================================================== */
/**
* inc_ipipe_start()
*/
/* ========================================================================== */
static int inc_ipipe_start(inc_t *inc, void *params)
{
    mmsdbg(DL_FUNC, "Enter");

    mmsdbg(DL_FUNC, "Exit Ok");

    return 0;
}

/* ========================================================================== */
/**
* inc_ipipe_stop()
*/
/* ========================================================================== */
static void inc_ipipe_stop(inc_t *inc)
{
    mmsdbg(DL_FUNC, "Enter");

    mmsdbg(DL_FUNC, "Exit Ok");
}

/* ========================================================================== */
/**
* inc_ipipe_config()
*/
/* ========================================================================== */
static int inc_ipipe_config(inc_t *inc, void *data)
{
    mmsdbg(DL_FUNC, "Enter");

    mmsdbg(DL_FUNC, "Exit Ok");

    return 0;
}

/* ========================================================================== */
/**
* inc_ipipe_process()
*/
/* ========================================================================== */
static void inc_ipipe_process(inc_t *inc, void *data)
{
    inc_ipipe_priv_data_t   *pCompPrv;

    pCompPrv = inc->prv.ptr;

    mmsdbg(DL_FUNC, "Enter");

    inc->callback(inc, inc->client_prv, 0, 0, data);

    mmsdbg(DL_FUNC, "Exit Ok");
}

/* ========================================================================== */
/**
* inc_ipipe_destroy()
*/
/* ========================================================================== */
static void inc_ipipe_destroy(inc_t *inc)
{
    mmsdbg(DL_FUNC, "Enter");

    osal_free(inc->prv.ptr);

    mmsdbg(DL_FUNC, "Exit Ok");
}

/* ========================================================================== */
/**
* inc_ipipe_create()
*/
/* ========================================================================== */
int inc_ipipe_create(
        inc_t *inc,
        void *params,
        void *app_res
    )
{
    inc_ipipe_priv_data_t *pCompPrv;
    INC_IPIPE_CPARAMS_T *pStaticParams;

    mmsdbg(DL_FUNC, "Enter");

    pStaticParams = params;

    pCompPrv = osal_malloc(sizeof(inc_ipipe_priv_data_t));
    GOTO_EXIT_IF(NULL == pCompPrv, 1);

    pCompPrv->fr_offset_cfg = pStaticParams->fr_offset_cfg;

    inc->prv.ptr = pCompPrv;

    inc->inc_start = inc_ipipe_start;
    inc->inc_stop = inc_ipipe_stop;
    inc->inc_flush = NULL;
    inc->inc_config_alter = NULL;
    inc->inc_config = inc_ipipe_config;
    inc->inc_process = inc_ipipe_process;
    inc->inc_destroy = inc_ipipe_destroy;

    mmsdbg(DL_FUNC, "Exit Ok");

    return 0;
EXIT_1:
    mmsdbg(DL_FUNC, "Exit Err");

    return -1;
}

