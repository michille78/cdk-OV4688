/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file inC_frame_info.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <osal/osal_stdlib.h>
#include <osal/osal_string.h>
#include <utils/mms_debug.h>

#include <osal/ex_pool.h>

#include <framerequest/camera/camera_frame_request.h>
#include <pipe/include/inc.h>
#include <frame_info/include/inc_frame_info.h>
#include <camera/vcamera_iface/vpipe_control/include/vpipe_control.h>

#include <guzzi/camera3/metadata.h>
#include <guzzi/camera3_capture_result/camera3_capture_result.h>
#include <guzzi/camera3/metadata_enum.h>
#include <cam_config.h>
#include "cam_algo_state.h"

#include <guzzi_event/include/guzzi_event.h>
#include <guzzi_event_global/include/guzzi_event_global.h>

// Better to Enable/Disable from Makefile.config for Your project - not from here
//#define ENABLE_METADATA

#ifdef ENABLE_METADATA

#include "IcTypes.h"

typedef void (*SendOutCbSent)(FrameT *frame, uint32_t outputId, uint32_t frmType);

void sendOutSend(FrameT *frame, uint32_t outputId, uint32_t frmType, SendOutCbSent sendOutCbSent);

#define META_LINE_SIZE      2688
#define META_HEADER_HEIGHT  2
#define META_HEADER_SIZE    (META_LINE_SIZE*META_HEADER_HEIGHT)
#define META_UVC_HEADER     16

#define META_BUFFER_LINES  ((sizeof(guzzi_camera3_metadata_dynamic_t) + (META_LINE_SIZE-1))/META_LINE_SIZE)
#define META_BUFFER_SIZE   (META_LINE_SIZE * META_BUFFER_LINES)


ex_pool_node_t frInfo_desc [] = {
     EX_POOL_NODE_DESC(NULL, (META_HEADER_SIZE+META_UVC_HEADER), FrameT, fbPtr[3]),
     EX_POOL_NODE_DESC(NULL, (META_BUFFER_SIZE), FrameT, fbPtr[0]),
     EX_POOL_LIST_END
};

ex_pool_node_t frInfo_root =
{
     EX_POOL_ROOT(&frInfo_desc, sizeof (FrameT))
};


#define FRAME_INFO_TIMEOUT DEFAULT_ALLOC_TIMEOUT_MS
#define FLOAT_TO_INT (1023*1024) //TODO

struct int_frame_info {
    int camera_id;
    int number_of_buffers;
    ex_pool_t *pool_frame_info;

    unsigned int fr_offset_cfg;
    unsigned int res_offset_cfg;
    unsigned int fr_offset_algo_state;
    unsigned int fr_offset_af_stats;
    unsigned int fr_offset_vpipe;
    unsigned int fr_offset_cm_dyn_data;
    osal_sem* start_stop_tx_client_sync;
    guzzi_event_t *evt_hdl;
};

mmsdbg_define_variable(
        vdl_inc_frame_info,
        DL_DEFAULT,
        0,
        "inc.frame_info",
        "Frame information"
    );
#define MMSDEBUGLEVEL mmsdbg_use_variable(vdl_inc_frame_info)

void callback_sendOutSend_metadata(FrameT *frame, uint32_t outputId, uint32_t frmType)
{
    inc_frame_info_t *prv = frame->appSpecificData;
    UNUSED(outputId);
    UNUSED(frmType);

    osal_free(frame);
    osal_sem_post(prv->start_stop_tx_client_sync);
}

void sendOutSend_metadata(void *prv, FrameT *frame)
{
    inc_frame_info_t *inc_prv = prv;
    frame->appSpecificData = prv;

//    callback_sendOutSend_metadata(frame, inc_prv->camera_id, FRAME_DATA_TYPE_METADATA_PRV);
//    send_static_buffer();

    sendOutSend(frame, inc_prv->camera_id, FRAME_DATA_TYPE_METADATA_PRV, callback_sendOutSend_metadata);
    osal_sem_wait_timeout(inc_prv->start_stop_tx_client_sync, FRAME_INFO_TIMEOUT);
}

guzzi_camera3_metadata_dynamic_t * get_metadata_buffer(FrameT *ft)
{
    if (!ft)
        return NULL;
    if (ft->tSize[0] == 0)
    {
        ft->type = FRAME_T_FORMAT_BINARY;
        ft->nPlanes = 2;

        ft->stride[0] = META_LINE_SIZE;
        ft->height[0] = META_BUFFER_LINES;
        ft->tSize[0]  = META_BUFFER_SIZE;

        ft->stride[3] = META_LINE_SIZE;
        ft->height[3] = META_HEADER_HEIGHT;
        ft->tSize[3]  = META_HEADER_SIZE;

        ft->fbPtr[3]  = (char*)ft->fbPtr[3] + META_UVC_HEADER;
        ft->fbPtr[0]  = (char*)ft->fbPtr[3] + ft->tSize[3];

    }
    return ((guzzi_camera3_metadata_dynamic_t *)ft->fbPtr[0]);
}


static void cb_evt_notify(void *inc_prv, int event_id,
                            uint32 num, void *data)
{
    inc_frame_info_t *prv = inc_prv;

    mmsdbg(DL_FUNC, "Enter num: %d",  num);

    switch (geg_camera_event_get_eid(event_id))
    {
        case CAM_EVT_LRT_STARTED:
            osal_sem_post(prv->start_stop_tx_client_sync);
        break;
        default:
            mmsdbg(DL_ERROR, "Unhandled event");
            return;
    }
}

extern guzzi_camera3_metadata_dynamic_t guzzi_frame_info__temp_dynamic;

static void fill_frame_info(
        inc_frame_info_t *prv,
        camera_fr_t *fr,
        guzzi_camera3_metadata_dynamic_t *info
    )
{
    camera_fr_entry_t *fr_entry;
    cam_cfg_t         *cfg;
    hat_h3a_af_stat_t *p_af;
    vpipe_ctrl_settings_t   *vpipe;
    guzzi_camera3_dynamic_statistics_sharpness_map_t *sharpness =
                                           &info->s.statistics_sharpness_map.v;
    int i,j;
    float step_x, step_y;
    unsigned int xy_pos;

    memcpy(info, &guzzi_frame_info__temp_dynamic, sizeof (*info));

    // Fill Histogram
    info->valid.statistics_histogram = 0;
    info->valid.statistics_histogram_mode = 0;
    info->s.statistics_histogram_mode.v.v =
            GUZZI_CAMERA3_ENUM_STATISTICS_HISTOGRAM_MODE_OFF;

    cfg = CAMERA_FR_GET_ENTRY(fr, prv->fr_offset_cfg)->data;
    vpipe = CAMERA_FR_GET_ENTRY(fr, prv->fr_offset_vpipe)->data;
    if(CAM_STAT_MODE_ON == cfg->cam_gzz_cfg.stats_enable.val.hist) {
        guzzi_camera3_int32_t histogram[] = {
            0, 0, 0,
            10, 10, 10,
            100, 100, 100,
            50, 40, 30,
            1, 1, 1
        };

        info->valid.statistics_histogram = 1;
        info->valid.statistics_histogram_mode = 1;

        info->s.statistics_histogram_mode.v.v =
                GUZZI_CAMERA3_ENUM_STATISTICS_HISTOGRAM_MODE_ON;

        // TODO: fill real histogram;
        osal_memcpy(info->s.statistics_histogram.v.v, histogram, sizeof(histogram));
    }

    // Fill Sharpness
    info->valid.statistics_sharpness_map = 0;
    info->valid.statistics_sharpness_map_mode = 0;
    info->s.statistics_sharpness_map_mode.v.v =
                GUZZI_CAMERA3_ENUM_STATISTICS_SHARPNESS_MAP_MODE_OFF;
    if (CAM_STAT_MODE_ON == cfg->cam_gzz_cfg.stats_enable.val.af_stat) {

        fr_entry = CAMERA_FR_GET_ENTRY(fr, prv->fr_offset_af_stats);
        if (!fr_entry || !fr_entry->data) {
            mmsdbg(DL_ERROR, "Invalid fr entry!");
            return;
        }
        p_af = (hat_h3a_af_stat_t *)fr_entry->data;
        if((0 == p_af->cfg.pax.x) || (0 == p_af->cfg.pax.y) ||
            (0 == sharpness->dim_size_1) || (0 == sharpness->dim_size_2) ||
            (0 == sharpness->dim_size_3)) {
            mmsdbg(DL_ERROR, "Invalid AF sharpness statistic dimension size!");
        } else {
            info->valid.statistics_sharpness_map = 1;
            info->valid.statistics_sharpness_map_mode = 1;
            info->s.statistics_sharpness_map_mode.v.v =
                        GUZZI_CAMERA3_ENUM_STATISTICS_SHARPNESS_MAP_MODE_ON;

            if ((sharpness->dim_size_2 == p_af->cfg.pax.x) &&
                (sharpness->dim_size_3 == p_af->cfg.pax.y)){
                meta_arr_iterate_dim_2(3,2,sharpness,j,i){
                    xy_pos = i * p_af->cfg.pax.x + j;
                    METAIDX3(sharpness,j,i,0) =
                            (int)(FLOAT_TO_INT * p_af->paxels[xy_pos].val_filter_strong.sum);
                    METAIDX3(sharpness,j,i,1) =
                            (int)(FLOAT_TO_INT * p_af->paxels[xy_pos].val_filter_strong.sum);
                    METAIDX3(sharpness,j,i,2) =
                            (int)(FLOAT_TO_INT * p_af->paxels[xy_pos].val_filter_strong.sum);
                }
            } else {
                step_x = (float) p_af->cfg.pax.x / sharpness->dim_size_2;
                step_y = (float) p_af->cfg.pax.y / sharpness->dim_size_3;

                meta_arr_iterate_dim_2(3,2,sharpness,j,i){
                    xy_pos = (int)(i*step_x) * p_af->cfg.pax.x + (int)(j*step_y);
                    METAIDX3(sharpness,j,i,0) =
                            (int)(FLOAT_TO_INT * p_af->paxels[xy_pos].val_filter_strong.sum);
                    METAIDX3(sharpness,j,i,1) =
                            (int)(FLOAT_TO_INT * p_af->paxels[xy_pos].val_filter_strong.sum);
                    METAIDX3(sharpness,j,i,2) =
                            (int)(FLOAT_TO_INT * p_af->paxels[xy_pos].val_filter_strong.sum);
                }
            }
        }
    }

    // Fill Lens Shading map
    info->valid.shading_mode = 0;
    info->valid.statistics_lens_shading_map = 0;
    info->s.shading_mode.v.v =
            GUZZI_CAMERA3_ENUM_STATISTICS_LENS_SHADING_MAP_MODE_OFF;
    if(CAM_STAT_MODE_ON == cfg->cam_gzz_cfg.stats_enable.val.lsc) {
        if((0 == vpipe->lsc.data->lsc_tbl_size.w) ||
            (0 == vpipe->lsc.data->lsc_tbl_size.h) ||
            (vpipe->lsc.data->lsc_tbl_size.w >
                GUZZI_CAMERA3_DYNAMIC_STATISTICS_LENS_SHADING_MAP_DIM_MAX_SIZE_2) ||
            (vpipe->lsc.data->lsc_tbl_size.h >
                GUZZI_CAMERA3_DYNAMIC_STATISTICS_LENS_SHADING_MAP_DIM_MAX_SIZE_3)) {
            mmsdbg(DL_ERROR, "Invalid LSC dimension size!");
        } else {
            info->valid.shading_mode = 1;
            info->valid.statistics_lens_shading_map = 1;


            info->s.shading_mode.v.v =
                        GUZZI_CAMERA3_ENUM_SHADING_MODE_OFF;;
            if(CAM_LSC_AUTO == cfg->cam_gzz_cfg.lsc_mode.val) {
                info->s.shading_mode.v.v =
                        GUZZI_CAMERA3_ENUM_SHADING_MODE_HIGH_QUALITY;
            }

            info->s.statistics_lens_shading_map.v.dim_size_1 = 4;
            info->s.statistics_lens_shading_map.v.dim_size_2 = vpipe->lsc.data->lsc_tbl_size.w;
            info->s.statistics_lens_shading_map.v.dim_size_3 = vpipe->lsc.data->lsc_tbl_size.h;

            osal_memcpy(info->s.statistics_lens_shading_map.v.v,
                            vpipe->lsc.data->lsc_tbl,
                            (sizeof(*vpipe->lsc.data->lsc_tbl) *
                                    vpipe->lsc.data->lsc_tbl_size.w *
                                    vpipe->lsc.data->lsc_tbl_size.h));
        }
    }
}

/* ========================================================================== */
/**
* @fn fill_algo_states()
*/
/* ========================================================================== */
static void fill_algo_states(inc_frame_info_t *prv, void *data,
        guzzi_camera3_metadata_dynamic_t *info_frame)
{
    exposure_calc_output_t  *ae;
    awb_calc_output_t       *wb;
    focus_calc_output_t     *focus;
    cam_algo_state_t        *state;
    cam_cfg_t               *cfg;
    vpipe_ctrl_settings_t   *vpipe;
    ae_distr_calc_output_t  *aedistr;

    cfg = CAMERA_FR_GET_ENTRY(data, prv->fr_offset_cfg)->data;
    state = CAMERA_FR_GET_ENTRY(data, prv->fr_offset_algo_state)->data;
    vpipe = CAMERA_FR_GET_ENTRY(data, prv->fr_offset_vpipe)->data;
    ae = &state->aca.ae_out;
    wb = &state->aca.awb_out;
    focus = &state->aca.focus;
    aedistr = &state->sg.ae_distr;

    /*
     * AE frame info
     */
    info_frame->valid.control_ae_state = TRUE;
    info_frame->s.control_ae_state.v.v =
            GUZZI_CAMERA3_ENUM_CONTROL_AE_STATE_INACTIVE;

    if (CAM_ACA_MODE_AUTO == cfg->cam_gzz_cfg.aca_mode.val &&
            CAM_AE_MODE_OFF != cfg->cam_gzz_cfg.ae_mode.val) {

        if (CAM_ALG_LOCK_OFF == cfg->cam_gzz_cfg.ae_lock.val) {
            info_frame->s.control_ae_state.v.v =
                    GUZZI_CAMERA3_ENUM_CONTROL_AE_STATE_SEARCHING;
            if(aedistr->distr_conv) {
                info_frame->s.control_ae_state.v.v =
                        GUZZI_CAMERA3_ENUM_CONTROL_AE_STATE_CONVERGED;
            }

            if (state->sg.ae_distr.req_flash) {
                info_frame->s.control_ae_state.v.v =
                        GUZZI_CAMERA3_ENUM_CONTROL_AE_STATE_FLASH_REQUIRED;
            }
        } else {
            info_frame->s.control_ae_state.v.v =
                    GUZZI_CAMERA3_ENUM_CONTROL_AE_STATE_LOCKED;
        }
    }
    info_frame->valid.control_ae_precapture_id = TRUE;
    /*
     * Always updated even if AE algorithm ignores the trigger
     * Must be 0 if no CAMERA2_TRIGGER_PRECAPTURE_METERING trigger received yet by HAL.
     */
    info_frame->s.control_ae_precapture_id.v.v = 0;

//    Get from virt_cm_dyn_props_t
//    info_frame->valid.sensor_exposure_time = TRUE;
//    info_frame->s.sensor_exposure_time.v.v =
//            state->sg.ae_distr.exp_gain.exposure * 1000;
//
//    info_frame->valid.sensor_sensitivity = TRUE;
//    info_frame->s.sensor_sensitivity.v.v = //TODO: Venci place check it
//            state->sg.ae_distr.exp_gain.again * 100;
    {
        uint32 i;
        hat_reg_pri_t *_regs = &cfg->cam_gzz_cfg.ae_regions.val;

        info_frame->valid.control_ae_regions = TRUE;
        meta_arr_iterate_dim_1(2, &info_frame->s.control_ae_regions.v, i) {
            METAIDX2(&info_frame->s.control_ae_regions.v, i, 0) = _regs->reg_box[i].x;
            METAIDX2(&info_frame->s.control_ae_regions.v, i, 1) = _regs->reg_box[i].y;
            METAIDX2(&info_frame->s.control_ae_regions.v, i, 2) = _regs->reg_box[i].w;
            METAIDX2(&info_frame->s.control_ae_regions.v, i, 3) = _regs->reg_box[i].h;
            METAIDX2(&info_frame->s.control_ae_regions.v, i, 4) = _regs->priority[i] ;
        }
    }

    /*
     * WB frame info
     */
    info_frame->valid.control_awb_state = TRUE;
    info_frame->s.control_awb_state.v.v =
            GUZZI_CAMERA3_ENUM_CONTROL_AWB_STATE_INACTIVE;

    if (CAM_ACA_MODE_AUTO == cfg->cam_gzz_cfg.aca_mode.val &&
            AWB_MODE_AUTO == cfg->cam_gzz_cfg.wb_mode.val) {

        if (CAM_ALG_LOCK_OFF == cfg->cam_gzz_cfg.wb_lock.val) {
            info_frame->s.control_awb_state.v.v = wb->converged ?
                    GUZZI_CAMERA3_ENUM_CONTROL_AWB_STATE_CONVERGED :
                    GUZZI_CAMERA3_ENUM_CONTROL_AWB_STATE_SEARCHING;
        } else {
            info_frame->s.control_awb_state.v.v =
                    GUZZI_CAMERA3_ENUM_CONTROL_AWB_STATE_LOCKED;
        }
    }

    info_frame->valid.control_awb_mode = TRUE;
    switch (cfg->cam_gzz_cfg.wb_mode.val) {
    case AWB_MODE_MANUAL_KELVINS2GAINS:
    case AWB_MODE_MANUAL_GAINS2KELVINS:
        info_frame->s.control_awb_mode.v.v =
                GUZZI_CAMERA3_ENUM_CONTROL_AWB_MODE_OFF;
        break;
    case AWB_MODE_AUTO:
        info_frame->s.control_awb_mode.v.v =
                GUZZI_CAMERA3_ENUM_CONTROL_AWB_MODE_AUTO;
        break;
    case AWB_MODE_INCANDESCENT:
        info_frame->s.control_awb_mode.v.v =
                GUZZI_CAMERA3_ENUM_CONTROL_AWB_MODE_INCANDESCENT;
        break;
    case AWB_MODE_FLUORESCENT:
        info_frame->s.control_awb_mode.v.v =
                GUZZI_CAMERA3_ENUM_CONTROL_AWB_MODE_FLUORESCENT;
        break;
    case AWB_MODE_WARM_FLUORESCENT:
        info_frame->s.control_awb_mode.v.v =
                GUZZI_CAMERA3_ENUM_CONTROL_AWB_MODE_WARM_FLUORESCENT;
        break;
    case AWB_MODE_DAYLIGHT:
        info_frame->s.control_awb_mode.v.v =
                GUZZI_CAMERA3_ENUM_CONTROL_AWB_MODE_DAYLIGHT;
        break;
    case AWB_MODE_CLOUDY:
        info_frame->s.control_awb_mode.v.v =
                GUZZI_CAMERA3_ENUM_CONTROL_AWB_MODE_CLOUDY_DAYLIGHT;
        break;
    case AWB_MODE_TWILIGHT:
        info_frame->s.control_awb_mode.v.v =
                GUZZI_CAMERA3_ENUM_CONTROL_AWB_MODE_TWILIGHT;
        break;
    default:
        info_frame->valid.control_awb_mode = FALSE;
        mmsdbg(DL_WARNING, "Unknown AWB mode %d",
                cfg->cam_gzz_cfg.wb_mode.val)
        break;
    }

    info_frame->valid.color_correction_gains = TRUE;
    METAIDX1(&info_frame->s.color_correction_gains.v, 0) =
            vpipe->wb.data->c.r;
    METAIDX1(&info_frame->s.color_correction_gains.v, 1) =
            vpipe->wb.data->c.gr;
    METAIDX1(&info_frame->s.color_correction_gains.v, 2) =
            vpipe->wb.data->c.gb;
    METAIDX1(&info_frame->s.color_correction_gains.v, 3) =
            vpipe->wb.data->c.b;


    info_frame->valid.color_correction_transform = TRUE;
    {
#define TRASFORM_MATRIX_DENOM   (1<<10)
        uint32 i, j;
        meta_arr_iterate_dim_2(2, 1,
                &info_frame->s.color_correction_transform.v, i, j) {
            METAIDX2(&info_frame->s.color_correction_transform.v, i, j).denom =
                        TRASFORM_MATRIX_DENOM;
            METAIDX2(&info_frame->s.color_correction_transform.v, i, j).nom =
                        (vpipe->rgb2rgb.data->m.m[i][j] * TRASFORM_MATRIX_DENOM);
        }
    }
    {
        uint32 i;
        hat_reg_pri_t *_regs = &cfg->cam_gzz_cfg.wb_regions.val;

        info_frame->valid.control_awb_regions = TRUE;
        meta_arr_iterate_dim_1(2, &info_frame->s.control_awb_regions.v, i) {
            METAIDX2(&info_frame->s.control_awb_regions.v, i, 0) = _regs->reg_box[i].x;
            METAIDX2(&info_frame->s.control_awb_regions.v, i, 1) = _regs->reg_box[i].y;
            METAIDX2(&info_frame->s.control_awb_regions.v, i, 2) = _regs->reg_box[i].w;
            METAIDX2(&info_frame->s.control_awb_regions.v, i, 3) = _regs->reg_box[i].h;
            METAIDX2(&info_frame->s.control_awb_regions.v, i, 4) = _regs->priority[i] ;
        }
    }
    /*
     * Focus frame info
     */
    info_frame->valid.control_af_state = TRUE;
    info_frame->s.control_af_state.v.v =
            GUZZI_CAMERA3_ENUM_CONTROL_AF_STATE_INACTIVE;

    if (CAM_ACA_MODE_AUTO == cfg->cam_gzz_cfg.aca_mode.val) {
        if(AF_MODE_CONTINUOUS == cfg->cam_gzz_cfg.af_mode.val) {
            switch (focus->status) {
                case AF_STATE_RUNNING:
                case AF_STATE_WAIT_STABLE_SCENE:
                    info_frame->s.control_af_state.v.v =
                            GUZZI_CAMERA3_ENUM_CONTROL_AF_STATE_PASSIVE_SCAN;
                     break;

                case AF_STATE_FAIL:
                    info_frame->s.control_af_state.v.v =
                            GUZZI_CAMERA3_ENUM_CONTROL_AF_STATE_PASSIVE_UNFOCUSED;
                    break;

                case AF_STATE_SUCCESS:
                    info_frame->s.control_af_state.v.v =
                            GUZZI_CAMERA3_ENUM_CONTROL_AF_STATE_PASSIVE_FOCUSED;
                    break;

                default:
                    info_frame->valid.control_af_state = FALSE;
                    mmsdbg(DL_ERROR, "Unknown focus state %d", focus->status);
                    /* no break */
                case AF_STATE_SUSPEND:
                case AF_STATE_IDLE:
                    break;
            }
        }

        if (AF_MODE_SINGLE_FOCUS == cfg->cam_gzz_cfg.af_mode.val) {
            switch (focus->status) {
                case AF_STATE_RUNNING:
                    info_frame->s.control_af_state.v.v =
                            GUZZI_CAMERA3_ENUM_CONTROL_AF_STATE_ACTIVE_SCAN;
                     break;

                case AF_STATE_FAIL:
                    info_frame->s.control_af_state.v.v =
                            GUZZI_CAMERA3_ENUM_CONTROL_AF_STATE_NOT_FOCUSED_LOCKED;
                    break;

                case AF_STATE_SUCCESS:
                    info_frame->s.control_af_state.v.v =
                            GUZZI_CAMERA3_ENUM_CONTROL_AF_STATE_FOCUSED_LOCKED;
                    break;

                default:
                    info_frame->valid.control_af_state = FALSE;
                    mmsdbg(DL_ERROR, "Unknown focus state %d", focus->status);
                    /* no break */
                case AF_STATE_SUSPEND:
                case AF_STATE_IDLE:
                    break;
            }
        }
    }
    info_frame->valid.control_af_mode = TRUE;
    switch (cfg->cam_gzz_cfg.af_mode.val) {
        case AF_MODE_MANUAL:
                info_frame->s.control_af_mode.v.v =
                        GUZZI_CAMERA3_ENUM_CONTROL_AF_MODE_MACRO;
            break;
        case AF_MODE_SINGLE_FOCUS:
            info_frame->s.control_af_mode.v.v =
            GUZZI_CAMERA3_ENUM_CONTROL_AF_MODE_AUTO;
            if (AF_RANGE_EXTENDED == cfg->cam_gzz_cfg.af_range.val) {
                info_frame->s.control_af_mode.v.v =
                    GUZZI_CAMERA3_ENUM_CONTROL_AF_MODE_EDOF;
            }
            break;
        case AF_MODE_CONTINUOUS:
            info_frame->s.control_af_mode.v.v =
                GUZZI_CAMERA3_ENUM_CONTROL_AF_MODE_CONTINUOUS_PICTURE;
            if (AF_RANGE_CUSTOM1 == cfg->cam_gzz_cfg.af_range.val) {
                info_frame->s.control_af_mode.v.v =
                    GUZZI_CAMERA3_ENUM_CONTROL_AF_MODE_CONTINUOUS_VIDEO;
            }
            break;

        default:
        case AF_MODE_OFF:
            info_frame->s.control_af_mode.v.v =
                    GUZZI_CAMERA3_ENUM_CONTROL_AF_MODE_OFF;
            break;
    }
    info_frame->valid.control_af_trigger_id = TRUE;
    /*
     * Always updated even if AF algorithm ignores the trigger
     * Must be 0 if no CAMERA2_TRIGGER_AUTOFOCUS trigger received yet by HAL.
     */
    info_frame->s.control_af_trigger_id.v.v = 0;
    {
        uint32 i;
        hat_reg_pri_t *_regs = &cfg->cam_gzz_cfg.af_regions.val;

        info_frame->valid.control_af_regions = TRUE;
        meta_arr_iterate_dim_1(2, &info_frame->s.control_af_regions.v, i) {
            METAIDX2(&info_frame->s.control_af_regions.v, i, 0) = _regs->reg_box[i].x;
            METAIDX2(&info_frame->s.control_af_regions.v, i, 1) = _regs->reg_box[i].y;
            METAIDX2(&info_frame->s.control_af_regions.v, i, 2) = _regs->reg_box[i].w;
            METAIDX2(&info_frame->s.control_af_regions.v, i, 3) = _regs->reg_box[i].h;
            METAIDX2(&info_frame->s.control_af_regions.v, i, 4) = _regs->priority[i] ;
        }
    }

    /*
     * aca
     */
    info_frame->valid.control_mode = TRUE;
    switch (cfg->cam_gzz_cfg.aca_mode.val) {
    case CAM_ACA_MODE_OFF:
        info_frame->s.control_mode.v.v =
                GUZZI_CAMERA3_ENUM_CONTROL_MODE_OFF;
        break;
    case CAM_ACA_MODE_AUTO:
        info_frame->s.control_mode.v.v =
                GUZZI_CAMERA3_ENUM_CONTROL_MODE_AUTO;
        break;
    case CAM_ACA_MODE_USE_SCENE_MODE:
        info_frame->s.control_mode.v.v =
                CAM_ACA_MODE_USE_SCENE_MODE;
        break;
    default:
        info_frame->valid.control_mode = FALSE;
        mmsdbg(DL_ERROR, "Unknown aca mode %d", focus->status);
        break;
    }

    /*
     * afd
     */
    info_frame->valid.statistics_scene_flicker = TRUE;
    switch(cfg->cam_gzz_cfg.flicker_mode.val) {
    case CAM_FLICKER_MODE_50HZ:
        info_frame->s.statistics_scene_flicker.v.v =
                GUZZI_CAMERA3_ENUM_STATISTICS_SCENE_FLICKER_50HZ;
        break;
    case CAM_FLICKER_MODE_60HZ:
        info_frame->s.statistics_scene_flicker.v.v =
                GUZZI_CAMERA3_ENUM_STATISTICS_SCENE_FLICKER_60HZ;
        break;
    default:
        info_frame->s.statistics_scene_flicker.v.v =
                GUZZI_CAMERA3_ENUM_STATISTICS_SCENE_FLICKER_NONE;
        if (FLICKER_DETECTED == state->aca.afd_out.status) {
            switch (state->aca.afd_out.mains_freq) {
            case MAINS_FREQ_60HZ:
                info_frame->s.statistics_scene_flicker.v.v =
                        GUZZI_CAMERA3_ENUM_STATISTICS_SCENE_FLICKER_60HZ;
                break;
            case MAINS_FREQ_50HZ:
                info_frame->s.statistics_scene_flicker.v.v =
                        GUZZI_CAMERA3_ENUM_STATISTICS_SCENE_FLICKER_50HZ;
                break;
            case MAINS_FREQ_UNKNOWN:
            default:
                break;
            }
        }
        break;
    }
}

/* ========================================================================== */
/**
* @fn fill_cm_info()
*/
/* ========================================================================== */
static void fill_cm_info(

        inc_frame_info_t *prv,
        virt_cm_dyn_props_t *cm_info,
        guzzi_camera3_metadata_dynamic_t *info)
{
    // Check for valid cm data
    if(NULL == cm_info) {
        info->valid.sensor_exposure_time = FALSE;
        info->valid.sensor_sensitivity = FALSE;
        info->valid.sensor_frame_duration = FALSE;
        info->valid.sensor_temperature = FALSE;
        info->valid.sensor_timestamp = FALSE;
        info->valid.lens_aperture = FALSE;
        info->valid.lens_filter_density = FALSE;
        info->valid.lens_focal_length = FALSE;
        info->valid.lens_focus_distance = FALSE;
        info->valid.lens_focus_range = FALSE;
        info->valid.lens_optical_stabilization_mode = FALSE;
        info->valid.lens_state = FALSE;
        info->valid.flash_firing_power = FALSE;
        info->valid.flash_firing_time = FALSE;
        info->valid.flash_mode = FALSE;
        info->valid.flash_state = FALSE;
        return;
    }

    /*
     * Sensor info
     */
    info->valid.sensor_exposure_time = TRUE;
    info->s.sensor_exposure_time.v.v = cm_info->sensor_props.exposure_time_ns;

    info->valid.sensor_sensitivity = TRUE;
    info->s.sensor_sensitivity.v.v = cm_info->sensor_props.sensitivity_iso;

    info->valid.sensor_frame_duration = TRUE;
    info->s.sensor_frame_duration.v.v = cm_info->sensor_props.frameDuration_ns;

    if(cm_info->sensor_props.sensor_temperature_c > 9998.0f){
        info->valid.sensor_temperature = FALSE;
    } else {
        info->valid.sensor_temperature = TRUE;
        info->s.sensor_temperature.v.v = cm_info->sensor_props.sensor_temperature_c;
    }

    //TODO
    info->valid.sensor_timestamp = FALSE;
    info->s.sensor_timestamp.v.v = 0;

    /*
     * Lens info
     */
    info->valid.lens_aperture = TRUE;
    info->s.lens_aperture.v.v = cm_info->lens_props.aperture_size;

    info->valid.lens_filter_density = TRUE;
    info->s.lens_filter_density.v.v = cm_info->lens_props.filter_density;

    info->valid.lens_focal_length = TRUE;
    info->s.lens_focal_length.v.v = cm_info->lens_props.focal_length;

    info->valid.lens_focus_distance = TRUE;
    info->s.lens_focus_distance.v.v = cm_info->lens_props.focus_distance;

    info->valid.lens_focus_range = TRUE;
    info->s.lens_focus_range.v.v[0] = cm_info->lens_props.focus_range.near;
    info->s.lens_focus_range.v.v[1] = cm_info->lens_props.focus_range.far;

    info->valid.lens_optical_stabilization_mode = TRUE;
    info->s.lens_optical_stabilization_mode.v.v = cm_info->lens_props.optical_stab_mode;

    info->valid.lens_state = TRUE;
    info->s.lens_state.v.v = cm_info->lens_props.state;

    /*
     * Flash info
     */
    info->valid.flash_firing_power = TRUE;
    info->s.flash_firing_power.v.v = cm_info->flash_props.firing_power;

    info->valid.flash_firing_time = TRUE;
    info->s.flash_firing_time.v.v = cm_info->flash_props.firing_time_ns;

    info->valid.flash_mode = TRUE;
    info->s.flash_mode.v.v = cm_info->flash_props.mode;

    info->valid.flash_state = TRUE;
    info->s.flash_state.v.v = cm_info->flash_props.state;

}

/* ========================================================================== */
/**
* @fn inc_frame_info_process()
*/
/* ========================================================================== */
static void inc_frame_info_process(inc_t *inc, void *data)
{
    inc_frame_info_t *prv;
    camera_fr_t *fr;
    cam_cfg_t *cfg;
    cam_algo_state_t *algo_state;
    guzzi_camera3_metadata_dynamic_t *info;
    FrameT *ft;

    prv = inc->prv.ptr;
    fr = data;

    cfg = CAMERA_FR_GET_ENTRY(fr, prv->fr_offset_cfg)->data;

    algo_state = CAMERA_FR_GET_ENTRY(fr, prv->fr_offset_algo_state)->data;

    if (algo_state->fr_status) {
        mmsdbg(DL_ERROR, "FrmInfo skip error FR %p", data);
        goto exit1;
    }

    ft = ex_pool_alloc_timeout(prv->pool_frame_info, FRAME_INFO_TIMEOUT);

    info = get_metadata_buffer(ft);
    if (!info) {
        mmsdbg(DL_ERROR, "Failed to allocate frame info buffer!");
        goto exit1;
    }

    fill_frame_info(prv, fr, info);
    fill_algo_states(prv, fr, info);

    fill_cm_info(prv,
            CAMERA_FR_GET_ENTRY(fr, prv->fr_offset_cm_dyn_data)->data,
            info);

    guzzi_camera3_capture_result(
            prv->camera_id,
            GUZZI_CAMERA3_STREAM_ID_METADATA,
            cfg->cam_gzz_cfg.frame_number.val,
            info,
            sizeof (*info)
        );

    sendOutSend_metadata(prv, ft);

exit1:
    inc->callback(
                inc,
                inc->client_prv,
                0,
                0,
                data
            );
}

/* ========================================================================== */
/**
* @fn inc_frame_info_start()
*/
/* ========================================================================== */
static int inc_frame_info_start(inc_t *inc, void *params)
{
    inc_frame_info_t *prv;
    int err;

    prv = inc->prv.ptr;

    prv->pool_frame_info = ex_pool_create(
            "Frame Info",
            &frInfo_root,
            1 //prv->number_of_buffers
        );
    if (!prv->pool_frame_info) {
        mmsdbg(DL_ERROR, "Failed to create frame info pool!");
        goto exit1;
    }

    /*if (0)*/ {// Wait for CAM_EVT_LRT_STARTED event from inc_camera
        err = osal_sem_wait_timeout(prv->start_stop_tx_client_sync, FRAME_INFO_TIMEOUT);
        if(err) {
            mmsdbg(DL_ERROR, "Failed to start frame_info!");
            goto exit2;
        }
    }

    return 0;
exit2:
    ex_pool_destroy(prv->pool_frame_info);
exit1:
    return -1;
}

/* ========================================================================== */
/**
* @fn inc_frame_info_stop()
*/
/* ========================================================================== */
static void inc_frame_info_stop(inc_t *inc)
{
    inc_frame_info_t *prv;

    prv = inc->prv.ptr;
    ex_pool_destroy(prv->pool_frame_info);
}

/* ========================================================================== */
/**
* @fn inc_frame_info_destroy()
*/
/* ========================================================================== */
static void inc_frame_info_destroy(inc_t *inc)
{
    inc_frame_info_t *prv;
    prv = inc->prv.ptr;
    guzzi_event_unreg_recipient(prv->evt_hdl,
        geg_camera_event_mk(prv->camera_id, CAM_EVT_LRT_STARTED),
        cb_evt_notify,
        prv);
    osal_sem_destroy(prv->start_stop_tx_client_sync);
    osal_free(prv);
}

/* ========================================================================== */
/**
* @fn inc_frame_info_create()
*/
/* ========================================================================== */
int inc_frame_info_create(
        inc_t *inc,
        void *params,
        void *app_res
    )
{
    inc_frame_info_t *prv;
    inc_frame_info_params_t *static_params;

    prv = osal_calloc(1, sizeof (*prv));
    if (!prv) {
        mmsdbg(
                DL_ERROR,
                "Failed to allocate memory for new frame_info INC instance: size=%d!",
                sizeof (*prv)
            );
        goto exit1;
    }
    inc->prv.ptr = prv;

    static_params = params;

    prv->start_stop_tx_client_sync = osal_sem_create(0);
    if (NULL == prv->start_stop_tx_client_sync)
    {
        goto exit2;
    }

    prv->fr_offset_cfg = static_params->fr_offset_cfg;
    prv->res_offset_cfg = static_params->res_offset_cfg;
    prv->fr_offset_algo_state = static_params->fr_offset_algo_state;
    prv->fr_offset_af_stats = static_params->fr_offset_af_stats;
    prv->fr_offset_vpipe = static_params->fr_offset_vpipe;
    prv->fr_offset_cm_dyn_data = static_params->fr_offset_cm_dyn_data;

    prv->camera_id = INC_RES_GET_INT(app_res, static_params->res_offset_cametra_id);
    prv->number_of_buffers = static_params->number_of_buffers;

    prv->evt_hdl = guzzi_event_global();
    guzzi_event_reg_recipient(prv->evt_hdl,
                                geg_camera_event_mk(prv->camera_id, CAM_EVT_LRT_STARTED),
                                cb_evt_notify,
                                prv);

    inc->inc_start = inc_frame_info_start;
    inc->inc_stop = inc_frame_info_stop;
    inc->inc_flush = NULL;
    inc->inc_config_alter = NULL;
    inc->inc_config  = NULL;
    inc->inc_process = inc_frame_info_process;
    inc->inc_destroy = inc_frame_info_destroy;

    return 0;

exit2:
    osal_free(prv);
exit1:
    mmsdbg(DL_ERROR, "Failed to create frame_info INC instance!");
    return -1;
}

#else
static void inc_frame_info_process(inc_t *inc, void *data)
{
    inc->callback(
                inc,
                inc->client_prv,
                0,
                0,
                data
            );
}
static int inc_frame_info_start(inc_t *inc, void *params)
{
    return 0;
}

static void inc_frame_info_stop(inc_t *inc)
{
}

static void inc_frame_info_destroy(inc_t *inc)
{
}

int inc_frame_info_create(
        inc_t *inc,
        void *params,
        void *app_res
    )
{
    inc->inc_start = inc_frame_info_start;
    inc->inc_stop = inc_frame_info_stop;
    inc->inc_flush = NULL;
    inc->inc_config_alter = NULL;
    inc->inc_config  = NULL;
    inc->inc_process = inc_frame_info_process;
    inc->inc_destroy = inc_frame_info_destroy;

    return 0;
}



#endif // ENABLE_METADATA
