/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file temp_dynamic.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 29-May-2014 : ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <guzzi/camera3/metadata_enum.h>
#include <guzzi/camera3/metadata.h>

guzzi_camera3_metadata_dynamic_t guzzi_frame_info__temp_dynamic = {
    .valid = {
        .black_level_lock = 1,                      // init
        .color_correction_gains = 1,                // dynamic
        .color_correction_transform = 1,            // dynamic
        .control_ae_precapture_id = 1,              // init
        .control_ae_regions = 1,                    // init
        .control_ae_state = 1,                      // dynamic
        .control_af_mode = 1,                       // init
        .control_af_regions = 1,                    // init
        .control_af_state = 1,                      // dynamic
        .control_af_trigger_id = 1,                 // init
        .control_awb_mode = 1,                      // init
        .control_awb_regions = 1,                   // init
        .control_awb_state = 1,                     // dynamic
        .control_mode = 1,                          // init
        .edge_mode = 1,                             // init
        .flash_firing_power = 1,                    // init
        .flash_firing_time = 1,                     // init
        .flash_mode = 1,                            // init
        .flash_state = 1,                           // init
        .hot_pixel_mode = 1,                        // init
        .jpeg_gps_coordinates = 1,                  // init
        .jpeg_gps_processing_method = 1,            // init
        .jpeg_gps_timestamp = 1,                    // init
        .jpeg_orientation = 1,                      // init
        .jpeg_quality = 1,                          // init
        .jpeg_size = 1,                             // dynamic
        .jpeg_thumbnail_quality = 1,                // init
        .jpeg_thumbnail_size = 1,                   // init
        .led_transmit = 0,                          // no LED-s supported
        .lens_aperture = 1,                         // init
        .lens_filter_density = 1,                   // init
        .lens_focal_length = 1,                     // init
        .lens_focus_distance = 1,                   // dynamic
        .lens_focus_range = 1,                      // dynamic
        .lens_optical_stabilization_mode = 1,       // init
        .lens_state = 1,                            // dynamic
        .noise_reduction_mode = 1,                  // init
        .quirks_partial_result = 1,                 // init
        .request_frame_count = 1,                   // dynamic
        .request_id = 1,                            // dynamic
        .request_metadata_mode = 1,                 // init
        .request_output_streams = 1,                // dynamic
        .scaler_crop_region = 1,                    // init
        .sensor_exposure_time = 1,                  // dynamic
        .sensor_frame_duration = 1,                 // dynamic
        .sensor_sensitivity = 1,                    // dynamic
        .sensor_temperature = 1,                    // dynamic
        .sensor_timestamp = 1,                      // dynamic
        .shading_mode = 1,                          // init
        .statistics_face_detect_mode = 1,           // init
        .statistics_face_ids = 0,                   // face detection is disabled
        .statistics_face_landmarks = 0,             // face detection is disabled
        .statistics_face_rectangles = 0,            // face detection is disabled
        .statistics_face_scores = 0,                // face detection is disabled
        .statistics_histogram = 0,                  // histogram is disabled
        .statistics_histogram_mode = 1,             // init
        .statistics_lens_shading_map = 0,           // lens-shading map is disabled
        .statistics_predicted_color_gains = 1,      // dynamic
        .statistics_predicted_color_transform = 1,  // dynamic
        .statistics_scene_flicker = 1,              // dynamic
        .statistics_sharpness_map = 0,              // sharpness map is disabled
        .statistics_sharpness_map_mode = 1,         // init
        .tonemap_curve_blue = 1,                    // dynamic
        .tonemap_curve_green = 1,                   // dynamic
        .tonemap_curve_red = 1,                     // dynamic
        .tonemap_mode = 1,                          // init
    },
    .s = {
        .black_level_lock = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_BLACK_LEVEL_LOCK,
                        .size = sizeof (guzzi_camera3_dynamic_black_level_lock_identity_t)
                    }
                }
            },
            .v = {
                .v = GUZZI_CAMERA3_ENUM_BLACK_LEVEL_LOCK_OFF /* guzzi_camera3_byte_t */
            }
        },
        .color_correction_gains = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_COLOR_CORRECTION_GAINS,
                        .size = sizeof (guzzi_camera3_dynamic_color_correction_gains_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_DYNAMIC_COLOR_CORRECTION_GAINS_DIM_MAX_SIZE_1,
                .v = {0} /* guzzi_camera3_float_t */
            }
        },
        .color_correction_transform = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_COLOR_CORRECTION_TRANSFORM,
                        .size = sizeof (guzzi_camera3_dynamic_color_correction_transform_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_DYNAMIC_COLOR_CORRECTION_TRANSFORM_DIM_MAX_SIZE_1,
                .dim_size_2 = GUZZI_CAMERA3_DYNAMIC_COLOR_CORRECTION_TRANSFORM_DIM_MAX_SIZE_2,
                .v = {{.nom = 0, .denom = 0}} /* guzzi_camera3_rational_t */
            }
        },
        .control_ae_precapture_id = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_CONTROL_AE_PRECAPTURE_ID,
                        .size = sizeof (guzzi_camera3_dynamic_control_ae_precapture_id_identity_t)
                    }
                }
            },
            .v = {
                .v = 0 /* guzzi_camera3_int32_t */
            }
        },
        .control_ae_regions = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_CONTROL_AE_REGIONS,
                        .size = sizeof (guzzi_camera3_dynamic_control_ae_regions_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_DYNAMIC_CONTROL_AE_REGIONS_DIM_MAX_SIZE_1,
                .dim_size_2 = GUZZI_CAMERA3_DYNAMIC_CONTROL_AE_REGIONS_DIM_MAX_SIZE_2 - 8,
                .v = {
                        0, 0, 4208 - 1, 3120 - 1, 0     // weight = 0 for all regions => no metering
                } /* guzzi_camera3_int32_t */
            }
        },
        .control_ae_state = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_CONTROL_AE_STATE,
                        .size = sizeof (guzzi_camera3_dynamic_control_ae_state_identity_t)
                    }
                }
            },
            .v = {
                .v = 0 /* guzzi_camera3_byte_t */
            }
        },
        .control_af_mode = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_CONTROL_AF_MODE,
                        .size = sizeof (guzzi_camera3_dynamic_control_af_mode_identity_t)
                    }
                }
            },
            .v = {
                .v = GUZZI_CAMERA3_ENUM_CONTROL_AF_MODE_OFF /* guzzi_camera3_byte_t */
            }
        },
        .control_af_regions = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_CONTROL_AF_REGIONS,
                        .size = sizeof (guzzi_camera3_dynamic_control_af_regions_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_DYNAMIC_CONTROL_AF_REGIONS_DIM_MAX_SIZE_1,
                .dim_size_2 = GUZZI_CAMERA3_DYNAMIC_CONTROL_AF_REGIONS_DIM_MAX_SIZE_2 - 8,
                .v = {
                        0, 0, 4208 - 1, 3120 - 1, 0     // weight = 0 for all regions => no metering
                } /* guzzi_camera3_int32_t */
            }
        },
        .control_af_state = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_CONTROL_AF_STATE,
                        .size = sizeof (guzzi_camera3_dynamic_control_af_state_identity_t)
                    }
                }
            },
            .v = {
                .v = 0 /* guzzi_camera3_byte_t */
            }
        },
        .control_af_trigger_id = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_CONTROL_AF_TRIGGER_ID,
                        .size = sizeof (guzzi_camera3_dynamic_control_af_trigger_id_identity_t)
                    }
                }
            },
            .v = {
                .v = 0 /* guzzi_camera3_int32_t */
            }
        },
        .control_awb_mode = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_CONTROL_AWB_MODE,
                        .size = sizeof (guzzi_camera3_dynamic_control_awb_mode_identity_t)
                    }
                }
            },
            .v = {
                .v = GUZZI_CAMERA3_ENUM_CONTROL_AWB_MODE_AUTO /* guzzi_camera3_byte_t */
            }
        },
        .control_awb_regions = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_CONTROL_AWB_REGIONS,
                        .size = sizeof (guzzi_camera3_dynamic_control_awb_regions_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_DYNAMIC_CONTROL_AWB_REGIONS_DIM_MAX_SIZE_1,
                .dim_size_2 = GUZZI_CAMERA3_DYNAMIC_CONTROL_AWB_REGIONS_DIM_MAX_SIZE_2 - 8,
                .v = {
                        0, 0, 4208 - 1, 3120 - 1, 0     // weight = 0 for all regions => no metering
                } /* guzzi_camera3_int32_t */
            }
        },
        .control_awb_state = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_CONTROL_AWB_STATE,
                        .size = sizeof (guzzi_camera3_dynamic_control_awb_state_identity_t)
                    }
                }
            },
            .v = {
                .v = 0 /* guzzi_camera3_byte_t */
            }
        },
        .control_mode = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_CONTROL_MODE,
                        .size = sizeof (guzzi_camera3_dynamic_control_mode_identity_t)
                    }
                }
            },
            .v = {
                .v = GUZZI_CAMERA3_ENUM_CONTROL_MODE_AUTO /* guzzi_camera3_byte_t */
            }
        },
        .edge_mode = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_EDGE_MODE,
                        .size = sizeof (guzzi_camera3_dynamic_edge_mode_identity_t)
                    }
                }
            },
            .v = {
                .v = GUZZI_CAMERA3_ENUM_EDGE_MODE_FAST /* guzzi_camera3_byte_t */
            }
        },
        .flash_firing_power = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_FLASH_FIRING_POWER,
                        .size = sizeof (guzzi_camera3_dynamic_flash_firing_power_identity_t)
                    }
                }
            },
            .v = {
                .v = 0 /* guzzi_camera3_byte_t */
            }
        },
        .flash_firing_time = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_FLASH_FIRING_TIME,
                        .size = sizeof (guzzi_camera3_dynamic_flash_firing_time_identity_t)
                    }
                }
            },
            .v = {
                .v = 0 /* guzzi_camera3_int64_t */
            }
        },
        .flash_mode = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_FLASH_MODE,
                        .size = sizeof (guzzi_camera3_dynamic_flash_mode_identity_t)
                    }
                }
            },
            .v = {
                .v = GUZZI_CAMERA3_ENUM_FLASH_MODE_OFF /* guzzi_camera3_byte_t */
            }
        },
        .flash_state = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_FLASH_STATE,
                        .size = sizeof (guzzi_camera3_dynamic_flash_state_identity_t)
                    }
                }
            },
            .v = {
                .v = GUZZI_CAMERA3_ENUM_FLASH_STATE_UNAVAILABLE /* guzzi_camera3_byte_t */
            }
        },
        .hot_pixel_mode = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_HOT_PIXEL_MODE,
                        .size = sizeof (guzzi_camera3_dynamic_hot_pixel_mode_identity_t)
                    }
                }
            },
            .v = {
                .v = GUZZI_CAMERA3_ENUM_HOT_PIXEL_MODE_FAST /* guzzi_camera3_byte_t */
            }
        },
        .jpeg_gps_coordinates = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_JPEG_GPS_COORDINATES,
                        .size = sizeof (guzzi_camera3_dynamic_jpeg_gps_coordinates_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_DYNAMIC_JPEG_GPS_COORDINATES_DIM_MAX_SIZE_1,
                .v = {
                        42.666699,  // Latitude
                        23.351155,  // Longitude
                        569.0       // Altitude
                } /* guzzi_camera3_double_t */
            }
        },
        .jpeg_gps_processing_method = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_JPEG_GPS_PROCESSING_METHOD,
                        .size = sizeof (guzzi_camera3_dynamic_jpeg_gps_processing_method_identity_t)
                    }
                }
            },
            .v = {
                .v = 0 /* guzzi_camera3_byte_t */
            }
        },
        .jpeg_gps_timestamp = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_JPEG_GPS_TIMESTAMP,
                        .size = sizeof (guzzi_camera3_dynamic_jpeg_gps_timestamp_identity_t)
                    }
                }
            },
            .v = {
                .v = 0 /* guzzi_camera3_int64_t */
            }
        },
        .jpeg_orientation = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_JPEG_ORIENTATION,
                        .size = sizeof (guzzi_camera3_dynamic_jpeg_orientation_identity_t)
                    }
                }
            },
            .v = {
                .v = 0 /* guzzi_camera3_int32_t */
            }
        },
        .jpeg_quality = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_JPEG_QUALITY,
                        .size = sizeof (guzzi_camera3_dynamic_jpeg_quality_identity_t)
                    }
                }
            },
            .v = {
                .v = 95 /* guzzi_camera3_byte_t */
            }
        },
        .jpeg_size = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_JPEG_SIZE,
                        .size = sizeof (guzzi_camera3_dynamic_jpeg_size_identity_t)
                    }
                }
            },
            .v = {
                .v = 0 /* guzzi_camera3_int32_t */
            }
        },
        .jpeg_thumbnail_quality = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_JPEG_THUMBNAIL_QUALITY,
                        .size = sizeof (guzzi_camera3_dynamic_jpeg_thumbnail_quality_identity_t)
                    }
                }
            },
            .v = {
                .v = 60 /* guzzi_camera3_byte_t */
            }
        },
        .jpeg_thumbnail_size = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_JPEG_THUMBNAIL_SIZE,
                        .size = sizeof (guzzi_camera3_dynamic_jpeg_thumbnail_size_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_DYNAMIC_JPEG_THUMBNAIL_SIZE_DIM_MAX_SIZE_1,
                .v = {
                        320, 240
                } /* guzzi_camera3_int32_t */
            }
        },
        .led_transmit = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_LED_TRANSMIT,
                        .size = sizeof (guzzi_camera3_dynamic_led_transmit_identity_t)
                    }
                }
            },
            .v = {
                .v = GUZZI_CAMERA3_ENUM_LED_TRANSMIT_OFF /* guzzi_camera3_byte_t */
            }
        },
        .lens_aperture = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_LENS_APERTURE,
                        .size = sizeof (guzzi_camera3_dynamic_lens_aperture_identity_t)
                    }
                }
            },
            .v = {
                .v = 2.8f /* guzzi_camera3_float_t */
            }
        },
        .lens_filter_density = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_LENS_FILTER_DENSITY,
                        .size = sizeof (guzzi_camera3_dynamic_lens_filter_density_identity_t)
                    }
                }
            },
            .v = {
                .v = 0 /* guzzi_camera3_float_t */
            }
        },
        .lens_focal_length = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_LENS_FOCAL_LENGTH,
                        .size = sizeof (guzzi_camera3_dynamic_lens_focal_length_identity_t)
                    }
                }
            },
            .v = {
                .v = 4.76f /* guzzi_camera3_float_t */
            }
        },
        .lens_focus_distance = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_LENS_FOCUS_DISTANCE,
                        .size = sizeof (guzzi_camera3_dynamic_lens_focus_distance_identity_t)
                    }
                }
            },
            .v = {
                .v = 0 /* guzzi_camera3_float_t */
            }
        },
        .lens_focus_range = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_LENS_FOCUS_RANGE,
                        .size = sizeof (guzzi_camera3_dynamic_lens_focus_range_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_DYNAMIC_LENS_FOCUS_RANGE_DIM_MAX_SIZE_1,
                .v = {0} /* guzzi_camera3_float_t */
            }
        },
        .lens_optical_stabilization_mode = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_LENS_OPTICAL_STABILIZATION_MODE,
                        .size = sizeof (guzzi_camera3_dynamic_lens_optical_stabilization_mode_identity_t)
                    }
                }
            },
            .v = {
                .v = GUZZI_CAMERA3_ENUM_LENS_OPTICAL_STABILIZATION_MODE_OFF /* guzzi_camera3_byte_t */
            }
        },
        .lens_state = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_LENS_STATE,
                        .size = sizeof (guzzi_camera3_dynamic_lens_state_identity_t)
                    }
                }
            },
            .v = {
                .v = 0 /* guzzi_camera3_byte_t */
            }
        },
        .noise_reduction_mode = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_NOISE_REDUCTION_MODE,
                        .size = sizeof (guzzi_camera3_dynamic_noise_reduction_mode_identity_t)
                    }
                }
            },
            .v = {
                .v = GUZZI_CAMERA3_ENUM_NOISE_REDUCTION_MODE_FAST /* guzzi_camera3_byte_t */
            }
        },
        .quirks_partial_result = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_QUIRKS_PARTIAL_RESULT,
                        .size = sizeof (guzzi_camera3_dynamic_quirks_partial_result_identity_t)
                    }
                }
            },
            .v = {
                .v = 0 /* guzzi_camera3_byte_t */
            }
        },
        .request_frame_count = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_REQUEST_FRAME_COUNT,
                        .size = sizeof (guzzi_camera3_dynamic_request_frame_count_identity_t)
                    }
                }
            },
            .v = {
                .v = 0 /* guzzi_camera3_int32_t */
            }
        },
        .request_id = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_REQUEST_ID,
                        .size = sizeof (guzzi_camera3_dynamic_request_id_identity_t)
                    }
                }
            },
            .v = {
                .v = 0 /* guzzi_camera3_int32_t */
            }
        },
        .request_metadata_mode = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_REQUEST_METADATA_MODE,
                        .size = sizeof (guzzi_camera3_dynamic_request_metadata_mode_identity_t)
                    }
                }
            },
            .v = {
                .v = GUZZI_CAMERA3_ENUM_REQUEST_METADATA_MODE_FULL /* guzzi_camera3_byte_t */
            }
        },
        .request_output_streams = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_REQUEST_OUTPUT_STREAMS,
                        .size = sizeof (guzzi_camera3_dynamic_request_output_streams_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_DYNAMIC_REQUEST_OUTPUT_STREAMS_DIM_MAX_SIZE_1,
                .v = {0} /* guzzi_camera3_int32_t */
            }
        },
        .scaler_crop_region = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_SCALER_CROP_REGION,
                        .size = sizeof (guzzi_camera3_dynamic_scaler_crop_region_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_DYNAMIC_SCALER_CROP_REGION_DIM_MAX_SIZE_1,
                .v = {
                        0, 0, 4208, 3120
                } /* guzzi_camera3_int32_t */
            }
        },
        .sensor_exposure_time = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_SENSOR_EXPOSURE_TIME,
                        .size = sizeof (guzzi_camera3_dynamic_sensor_exposure_time_identity_t)
                    }
                }
            },
            .v = {
                .v = 0 /* guzzi_camera3_int64_t */
            }
        },
        .sensor_frame_duration = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_SENSOR_FRAME_DURATION,
                        .size = sizeof (guzzi_camera3_dynamic_sensor_frame_duration_identity_t)
                    }
                }
            },
            .v = {
                .v = 0 /* guzzi_camera3_int64_t */
            }
        },
        .sensor_sensitivity = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_SENSOR_SENSITIVITY,
                        .size = sizeof (guzzi_camera3_dynamic_sensor_sensitivity_identity_t)
                    }
                }
            },
            .v = {
                .v = 0 /* guzzi_camera3_int32_t */
            }
        },
        .sensor_temperature = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_SENSOR_TEMPERATURE,
                        .size = sizeof (guzzi_camera3_dynamic_sensor_temperature_identity_t)
                    }
                }
            },
            .v = {
                .v = 0 /* guzzi_camera3_float_t */
            }
        },
        .sensor_timestamp = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_SENSOR_TIMESTAMP,
                        .size = sizeof (guzzi_camera3_dynamic_sensor_timestamp_identity_t)
                    }
                }
            },
            .v = {
                .v = 0 /* guzzi_camera3_int64_t */
            }
        },
        .shading_mode = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_SHADING_MODE,
                        .size = sizeof (guzzi_camera3_dynamic_shading_mode_identity_t)
                    }
                }
            },
            .v = {
                .v = GUZZI_CAMERA3_ENUM_SHADING_MODE_FAST /* guzzi_camera3_byte_t */
            }
        },
        .statistics_face_detect_mode = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_STATISTICS_FACE_DETECT_MODE,
                        .size = sizeof (guzzi_camera3_dynamic_statistics_face_detect_mode_identity_t)
                    }
                }
            },
            .v = {
                .v = GUZZI_CAMERA3_ENUM_STATISTICS_FACE_DETECT_MODE_OFF /* guzzi_camera3_byte_t */
            }
        },
        .statistics_face_ids = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_STATISTICS_FACE_IDS,
                        .size = sizeof (guzzi_camera3_dynamic_statistics_face_ids_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_DYNAMIC_STATISTICS_FACE_IDS_DIM_MAX_SIZE_1,
                .v = {0} /* guzzi_camera3_int32_t */
            }
        },
        .statistics_face_landmarks = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_STATISTICS_FACE_LANDMARKS,
                        .size = sizeof (guzzi_camera3_dynamic_statistics_face_landmarks_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_DYNAMIC_STATISTICS_FACE_LANDMARKS_DIM_MAX_SIZE_1,
                .dim_size_2 = GUZZI_CAMERA3_DYNAMIC_STATISTICS_FACE_LANDMARKS_DIM_MAX_SIZE_2,
                .v = {0} /* guzzi_camera3_int32_t */
            }
        },
        .statistics_face_rectangles = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_STATISTICS_FACE_RECTANGLES,
                        .size = sizeof (guzzi_camera3_dynamic_statistics_face_rectangles_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_DYNAMIC_STATISTICS_FACE_RECTANGLES_DIM_MAX_SIZE_1,
                .dim_size_2 = GUZZI_CAMERA3_DYNAMIC_STATISTICS_FACE_RECTANGLES_DIM_MAX_SIZE_2,
                .v = {0} /* guzzi_camera3_int32_t */
            }
        },
        .statistics_face_scores = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_STATISTICS_FACE_SCORES,
                        .size = sizeof (guzzi_camera3_dynamic_statistics_face_scores_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_DYNAMIC_STATISTICS_FACE_SCORES_DIM_MAX_SIZE_1,
                .v = {0} /* guzzi_camera3_byte_t */
            }
        },
        .statistics_histogram = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_STATISTICS_HISTOGRAM,
                        .size = sizeof (guzzi_camera3_dynamic_statistics_histogram_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_DYNAMIC_STATISTICS_HISTOGRAM_DIM_MAX_SIZE_1,
                .dim_size_2 = GUZZI_CAMERA3_DYNAMIC_STATISTICS_HISTOGRAM_DIM_MAX_SIZE_2,
                .v = {0} /* guzzi_camera3_int32_t */
            }
        },
        .statistics_histogram_mode = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_STATISTICS_HISTOGRAM_MODE,
                        .size = sizeof (guzzi_camera3_dynamic_statistics_histogram_mode_identity_t)
                    }
                }
            },
            .v = {
                .v = GUZZI_CAMERA3_ENUM_STATISTICS_HISTOGRAM_MODE_OFF /* guzzi_camera3_byte_t */
            }
        },
        .statistics_lens_shading_map = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_STATISTICS_LENS_SHADING_MAP,
                        .size = sizeof (guzzi_camera3_dynamic_statistics_lens_shading_map_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_DYNAMIC_STATISTICS_LENS_SHADING_MAP_DIM_MAX_SIZE_1,
                .dim_size_2 = GUZZI_CAMERA3_DYNAMIC_STATISTICS_LENS_SHADING_MAP_DIM_MAX_SIZE_2,
                .dim_size_3 = GUZZI_CAMERA3_DYNAMIC_STATISTICS_LENS_SHADING_MAP_DIM_MAX_SIZE_3,
                .v = {0} /* guzzi_camera3_float_t */
            }
        },
        .statistics_predicted_color_gains = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_STATISTICS_PREDICTED_COLOR_GAINS,
                        .size = sizeof (guzzi_camera3_dynamic_statistics_predicted_color_gains_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_DYNAMIC_STATISTICS_PREDICTED_COLOR_GAINS_DIM_MAX_SIZE_1,
                .v = {0} /* guzzi_camera3_float_t */
            }
        },
        .statistics_predicted_color_transform = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_STATISTICS_PREDICTED_COLOR_TRANSFORM,
                        .size = sizeof (guzzi_camera3_dynamic_statistics_predicted_color_transform_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_DYNAMIC_STATISTICS_PREDICTED_COLOR_TRANSFORM_DIM_MAX_SIZE_1,
                .dim_size_2 = GUZZI_CAMERA3_DYNAMIC_STATISTICS_PREDICTED_COLOR_TRANSFORM_DIM_MAX_SIZE_2,
                .v = {{.nom = 0, .denom = 0}} /* guzzi_camera3_rational_t */
            }
        },
        .statistics_scene_flicker = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_STATISTICS_SCENE_FLICKER,
                        .size = sizeof (guzzi_camera3_dynamic_statistics_scene_flicker_identity_t)
                    }
                }
            },
            .v = {
                .v = 0 /* guzzi_camera3_byte_t */
            }
        },
        .statistics_sharpness_map = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_STATISTICS_SHARPNESS_MAP,
                        .size = sizeof (guzzi_camera3_dynamic_statistics_sharpness_map_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_DYNAMIC_STATISTICS_SHARPNESS_MAP_DIM_MAX_SIZE_1,
                .dim_size_2 = GUZZI_CAMERA3_DYNAMIC_STATISTICS_SHARPNESS_MAP_DIM_MAX_SIZE_2,
                .dim_size_3 = GUZZI_CAMERA3_DYNAMIC_STATISTICS_SHARPNESS_MAP_DIM_MAX_SIZE_3,
                .v = {0} /* guzzi_camera3_int32_t */
            }
        },
        .statistics_sharpness_map_mode = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_STATISTICS_SHARPNESS_MAP_MODE,
                        .size = sizeof (guzzi_camera3_dynamic_statistics_sharpness_map_mode_identity_t)
                    }
                }
            },
            .v = {
                .v = GUZZI_CAMERA3_ENUM_STATISTICS_SHARPNESS_MAP_MODE_OFF /* guzzi_camera3_byte_t */
            }
        },
        .tonemap_curve_blue = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_TONEMAP_CURVE_BLUE,
                        .size = sizeof (guzzi_camera3_dynamic_tonemap_curve_blue_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_DYNAMIC_TONEMAP_CURVE_BLUE_DIM_MAX_SIZE_1,
                .dim_size_2 = GUZZI_CAMERA3_DYNAMIC_TONEMAP_CURVE_BLUE_DIM_MAX_SIZE_2,
                .v = {0} /* guzzi_camera3_float_t */
            }
        },
        .tonemap_curve_green = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_TONEMAP_CURVE_GREEN,
                        .size = sizeof (guzzi_camera3_dynamic_tonemap_curve_green_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_DYNAMIC_TONEMAP_CURVE_GREEN_DIM_MAX_SIZE_1,
                .dim_size_2 = GUZZI_CAMERA3_DYNAMIC_TONEMAP_CURVE_GREEN_DIM_MAX_SIZE_2,
                .v = {0} /* guzzi_camera3_float_t */
            }
        },
        .tonemap_curve_red = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_TONEMAP_CURVE_RED,
                        .size = sizeof (guzzi_camera3_dynamic_tonemap_curve_red_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_DYNAMIC_TONEMAP_CURVE_RED_DIM_MAX_SIZE_1,
                .dim_size_2 = GUZZI_CAMERA3_DYNAMIC_TONEMAP_CURVE_RED_DIM_MAX_SIZE_2,
                .v = {0} /* guzzi_camera3_float_t */
            }
        },
        .tonemap_mode = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_TONEMAP_MODE,
                        .size = sizeof (guzzi_camera3_dynamic_tonemap_mode_identity_t)
                    }
                }
            },
            .v = {
                .v = GUZZI_CAMERA3_ENUM_TONEMAP_MODE_FAST /* guzzi_camera3_byte_t */
            }
        },
        .metadata_end_marker = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_METADATA_END_MARKER,
                        .size = 0
                    }
                }
            }
        }
    }
};

