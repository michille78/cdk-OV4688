/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file plugin_node.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <osal/osal_stdlib.h>
#include <osal/osal_string.h>
#include <utils/mms_debug.h>
#include <pipe/include/inc.h>
#include <plugin/include/plugin_node.h>

mmsdbg_define_variable(
        vdl_inc_plugin,
        DL_DEFAULT,
        0,
        "inc.plugin",
        "Pluging INC"
    );
#define MMSDEBUGLEVEL mmsdbg_use_variable(vdl_inc_plugin)

/* ========================================================================== */
/**
* inc_plugin_process()
*/
/* ========================================================================== */
static void inc_plugin_process(inc_t *inc, void *data)
{
    inc_plugin_t *prv;

    prv = inc->prv.ptr;

    if (prv->callback) {
        prv->callback(prv->prv, data);
    }
    inc->callback(inc, inc->client_prv, 0, 0, data);
}

/* ========================================================================== */
/**
* inc_plugin_destroy()
*/
/* ========================================================================== */
static void inc_plugin_destroy(inc_t *inc)
{
    osal_free(inc->prv.ptr);
}

/* ========================================================================== */
/**
* inc_plugin_create()
*/
/* ========================================================================== */
int inc_plugin_create(
        inc_t *inc,
        void *params,
        void *app_res
    )
{
    inc_plugin_t *prv;

    prv = osal_malloc(sizeof (*prv));
    if (!prv) {
        mmsdbg(
                DL_ERROR,
                "Failed to allocate memory for new plugin INC instance: size=%d!",
                sizeof (*prv)
            );
        goto exit1;
    }
    osal_memcpy(prv, params, sizeof (*prv));

    inc->prv.ptr = prv;

    inc->inc_start = NULL;
    inc->inc_stop = NULL;
    inc->inc_flush = NULL;
    inc->inc_config_alter = NULL;
    inc->inc_config = NULL;
    inc->inc_process = inc_plugin_process;
    inc->inc_destroy = inc_plugin_destroy;

    return 0;
exit1:
    return -1;
}

