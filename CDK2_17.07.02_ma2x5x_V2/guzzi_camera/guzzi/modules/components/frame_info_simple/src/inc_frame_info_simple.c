/* =============================================================================
* Copyright (c) 2016 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file inc_frame_info_simple.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 29-Jun-2016 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <osal/osal_stdlib.h>
#include <osal/osal_string.h>
#include <utils/mms_debug.h>

#include <osal/pool.h>

#include <framerequest/camera/camera_frame_request.h>
#include <pipe/include/inc.h>
#include <frame_info/include/inc_frame_info.h>
#include <camera/vcamera_iface/vpipe_control/include/vpipe_control.h>

#include <cam_config.h>
#include "cam_algo_state.h"

#include <ipipe_simulator/BayerISP/apps/testIpipe/leon/ic_mipi_tx_client.h>
#include <guzzi_event/include/guzzi_event.h>
#include <guzzi_event_global/include/guzzi_event_global.h>

#include "metadata_simple.h"

#define ENABLE_METADATA

#ifdef ENABLE_METADATA

#define FRAME_INFO_TIMEOUT DEFAULT_ALLOC_TIMEOUT_MS
#define FLOAT_TO_INT (1023*1024) //TODO

struct int_frame_info {
    int camera_id;
    int number_of_buffers;
    pool_t *pool_frame_info;

    unsigned int fr_offset_cfg;
    unsigned int res_offset_cfg;
    unsigned int fr_offset_algo_state;
    unsigned int fr_offset_af_stats;
    unsigned int fr_offset_vpipe;
    unsigned int fr_offset_cm_dyn_data;
    osal_sem* start_stop_tx_client_sync;
    guzzi_event_t *evt_hdl;
};

mmsdbg_define_variable(
        vdl_inc_frame_info_simple,
        DL_DEFAULT,
        0,
        "inc.frame_info_simple",
        "Simple Frame information"
    );
#define MMSDEBUGLEVEL mmsdbg_use_variable(vdl_inc_frame_info_simple)

static void callback_data_sent(void *buffer)
{
    osal_free(buffer);
}

static void cb_evt_notify(void *inc_prv, int event_id,
                            uint32 num, void *data)
{
    inc_frame_info_t *prv = inc_prv;

    mmsdbg(DL_FUNC, "Enter num: %d",  num);

    switch (geg_camera_event_get_eid(event_id))
    {
        case CAM_EVT_LRT_STARTED:
            osal_sem_post(prv->start_stop_tx_client_sync);
        break;
        default:
            mmsdbg(DL_ERROR, "Unhandled event");
            return;
    }
}

/* ========================================================================== */
/**
* @fn simple_fill_algo_states()
*/
/* ========================================================================== */
static void simple_fill_algo_states(inc_frame_info_t *prv, void *data,
        simple_metadata_t *info_frame)
{
    cam_algo_state_t        *state;
    cam_cfg_t               *cfg;

    cfg = CAMERA_FR_GET_ENTRY(data, prv->fr_offset_cfg)->data;
    state = CAMERA_FR_GET_ENTRY(data, prv->fr_offset_algo_state)->data;

    osal_memcpy(&info_frame->algo_awb,
                &state->aca.awb_out,
                sizeof(info_frame->algo_awb));

    osal_memcpy(&info_frame->algo_af,
                &state->aca.focus,
                sizeof(info_frame->algo_af));

    osal_memcpy(&info_frame->algo_ae,
                &state->aca.ae_out,
                sizeof(info_frame->algo_ae));

    osal_memcpy(&info_frame->algo_dist,
                &state->sg.ae_distr,
                sizeof(info_frame->algo_dist));
}

/* ========================================================================== */
/**
* @fn inc_frame_info_process()
*/
/* ========================================================================== */
static void inc_frame_info_process(inc_t *inc, void *data)
{
    inc_frame_info_t *prv;
    camera_fr_t *fr;
    cam_cfg_t *cfg;
    cam_algo_state_t *algo_state;
    simple_metadata_t *info;

    prv = inc->prv.ptr;
    fr = data;

    cfg = CAMERA_FR_GET_ENTRY(fr, prv->fr_offset_cfg)->data;

    algo_state = CAMERA_FR_GET_ENTRY(fr, prv->fr_offset_algo_state)->data;

    if (algo_state->fr_status) {
        mmsdbg(DL_ERROR, "FrmInfo skip error FR %p", data);
        goto exit1;
    }

    info = pool_alloc_timeout(prv->pool_frame_info, FRAME_INFO_TIMEOUT);
    if (!info) {
        mmsdbg(DL_ERROR, "Failed to allocate frame info buffer!");
        goto exit1;
    }
    simple_fill_algo_states(prv, fr, info);
    {
        void simple_metadata_send(void *data, uint32_t data_size, void(*done_cb)(void *));
        simple_metadata_send(info, sizeof(*info), callback_data_sent);
    }
exit1:
    inc->callback(
                inc,
                inc->client_prv,
                0,
                0,
                data
            );
}

/* ========================================================================== */
/**
* @fn inc_frame_info_start()
*/
/* ========================================================================== */
static int inc_frame_info_start(inc_t *inc, void *params)
{
    inc_frame_info_t *prv;

    prv = inc->prv.ptr;

    prv->pool_frame_info = pool_create(
            "Frame Info",
            sizeof (simple_metadata_t),
            prv->number_of_buffers
        );
    if (!prv->pool_frame_info) {
        mmsdbg(DL_ERROR, "Failed to create frame info pool!");
        goto exit1;
    }

    return 0;

exit1:
    return -1;
}

/* ========================================================================== */
/**
* @fn inc_frame_info_stop()
*/
/* ========================================================================== */
static void inc_frame_info_stop(inc_t *inc)
{
    inc_frame_info_t *prv;

    prv = inc->prv.ptr;
    pool_destroy(prv->pool_frame_info);
}

/* ========================================================================== */
/**
* @fn inc_frame_info_destroy()
*/
/* ========================================================================== */
static void inc_frame_info_destroy(inc_t *inc)
{
    inc_frame_info_t *prv;
    prv = inc->prv.ptr;
    guzzi_event_unreg_recipient(prv->evt_hdl,
        geg_camera_event_mk(prv->camera_id, CAM_EVT_LRT_STARTED),
        cb_evt_notify,
        prv);
    osal_sem_destroy(prv->start_stop_tx_client_sync);
    osal_free(prv);
}

/* ========================================================================== */
/**
* @fn inc_frame_info_create()
*/
/* ========================================================================== */
int inc_frame_info_create(
        inc_t *inc,
        void *params,
        void *app_res
    )
{
    inc_frame_info_t *prv;
    inc_frame_info_params_t *static_params;

    prv = osal_calloc(1, sizeof (*prv));
    if (!prv) {
        mmsdbg(
                DL_ERROR,
                "Failed to allocate memory for new frame_info INC instance: size=%d!",
                sizeof (*prv)
            );
        goto exit1;
    }
    inc->prv.ptr = prv;

    static_params = params;

    prv->start_stop_tx_client_sync = osal_sem_create(0);
    if (NULL == prv->start_stop_tx_client_sync)
    {
        goto exit2;
    }

    prv->fr_offset_cfg = static_params->fr_offset_cfg;
    prv->res_offset_cfg = static_params->res_offset_cfg;
    prv->fr_offset_algo_state = static_params->fr_offset_algo_state;
    prv->fr_offset_af_stats = static_params->fr_offset_af_stats;
    prv->fr_offset_vpipe = static_params->fr_offset_vpipe;
    prv->fr_offset_cm_dyn_data = static_params->fr_offset_cm_dyn_data;

    prv->camera_id = INC_RES_GET_INT(app_res, static_params->res_offset_cametra_id);
    prv->number_of_buffers = static_params->number_of_buffers;

    prv->evt_hdl = guzzi_event_global();
    guzzi_event_reg_recipient(prv->evt_hdl,
                                geg_camera_event_mk(prv->camera_id, CAM_EVT_LRT_STARTED),
                                cb_evt_notify,
                                prv);

    inc->inc_start = inc_frame_info_start;
    inc->inc_stop = inc_frame_info_stop;
    inc->inc_flush = NULL;
    inc->inc_config_alter = NULL;
    inc->inc_config  = NULL;
    inc->inc_process = inc_frame_info_process;
    inc->inc_destroy = inc_frame_info_destroy;

    return 0;

exit2:
    osal_free(prv);
exit1:
    return -1;
}

#else
static void inc_frame_info_process(inc_t *inc, void *data)
{
    inc->callback(
                inc,
                inc->client_prv,
                0,
                0,
                data
            );
}
static int inc_frame_info_start(inc_t *inc, void *params)
{
    return 0;
}

static void inc_frame_info_stop(inc_t *inc)
{
}

static void inc_frame_info_destroy(inc_t *inc)
{
}

int inc_frame_info_create(
        inc_t *inc,
        void *params,
        void *app_res
    )
{
    inc->inc_start = inc_frame_info_start;
    inc->inc_stop = inc_frame_info_stop;
    inc->inc_flush = NULL;
    inc->inc_config_alter = NULL;
    inc->inc_config  = NULL;
    inc->inc_process = inc_frame_info_process;
    inc->inc_destroy = inc_frame_info_destroy;

    return 0;
}



#endif // ENABLE_METADATA
