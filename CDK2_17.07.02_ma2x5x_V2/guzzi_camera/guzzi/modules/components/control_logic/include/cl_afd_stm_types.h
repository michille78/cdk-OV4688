/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file cl_afd_stm_types.h
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#ifndef _CL_AFD_STM_TYPES_H
#define _CL_AFD_STM_TYPES_H

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include "cl_common_private.h"

#define REPEAT_AFD_MODE_CNT 4 // Min value is 1.

typedef enum {
     CL_AFD_STATE_IDLE,
     CL_AFD_STATE_STARTUP,
     CL_AFD_STATE_RUNING,
     CL_AFD_STATE_ABORTING,
     MAX_CL_AFD_STATE
} cl_afd_state_t;

typedef enum {
    CL_AFD_CMD_SATRT,
    CL_AFD_CMD_FRAME_REQUEST,
    CL_AFD_CMD_ABORT,
    MAX_CL_AFD_CMD
} cl_afd_cmd_t;

int cl_create_afd_stm (cl_stm_t **norm_hdl);
int cl_destroy_afd_stm (cl_stm_t *hndl);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* _CL_AFD_STM_TYPES_H */
