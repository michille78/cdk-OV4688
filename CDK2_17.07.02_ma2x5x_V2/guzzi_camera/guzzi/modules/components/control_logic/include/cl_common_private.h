/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file cl_common_private.h
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#ifndef _CL_COMMON_PRIVATE_H
#define _CL_COMMON_PRIVATE_H

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include <osal/osal_stdtypes.h>
#include <osal/osal_mutex.h>
#include <osal/list_pool.h>
#include "cam_cl_frame_req.h"
#include "aca_ae_calc_types.h"
#include "aca_ae_stab_types.h"
#include "aca_awb_types.h"
#include "aca_af_types.h"
#include "cl_algo_helper.h"
#include "gzz_effects_types.h"
#include "gzz_scene_types.h"
#include "cam_algo_state.h"
#include "cam_config.h"
#include "IspCommon.h"
#include "ipipe.h"

#define FILL_FR_PARAMS ()

typedef struct {
    uint32                  enb_aca;
    exposure_calc_output_t  *ae_out;
    ae_stab_output_t        *ae_stab;
    awb_calc_output_t       *awb_out;
    focus_calc_output_t     *focus;
    virt_cm_sen_mode_features_t    *vsen_modes;
    dtpdb_dynamic_common_t  *dtp_dyn;
    aca_afd_calc_output_t   *afd_out;
    list_pool_t             *pool_alg_ctrl;
} cl_in_t;

typedef struct {
    dtpdb_static_common_t   *dtp_stat;
    cam_cfg_t               *cur_cam_cfg;
    cam_cfg_t               *new_cam_cfg;
} cl_cfg_t;

typedef enum {
    CL_SEQ_STAT_END,
    CL_SEQ_STAT_PROGRESS,
    CL_SEQ_STAT_NO_DLEAY,
} cl_seq_status_t;

typedef struct {
    sg_algo_out_t           sg;
    cam_fr_buff_ctrl_list_t *cl_req_img;
    cam_fr_buff_ctrl_list_t *cl_req_aca;
    vpipe_ctrl_settings_t   *vpipe;
    hat_light_ctrl_list_t   *light;
    cl_seq_status_t         seq_status;
} cl_out_t;

typedef int (*p_ready_t) (void *);
typedef int (*p_exec_t) (void *, uint32, cl_algs_hdl_t *, cl_in_t *, cl_out_t *);
typedef int (*p_cl_state_exe_t) (void *, cl_algs_hdl_t *, cl_in_t *, cl_out_t *);
typedef int (*p_config_t) (void *, cl_algs_hdl_t *, cl_cfg_t *);

typedef struct {
    void        *private;
    p_ready_t   ready;
    p_config_t  config;
    p_exec_t    exec;
} cl_stm_t;

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* _CL_COMMON_PRIVATE_H */


