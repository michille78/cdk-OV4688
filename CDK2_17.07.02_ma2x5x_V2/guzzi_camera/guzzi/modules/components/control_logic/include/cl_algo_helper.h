/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file cl_algo_helper.h
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#ifndef CL_ALGO_HELPER_H_
#define CL_ALGO_HELPER_H_

#include <guzzi_event/include/guzzi_event.h>
#include "algo_vpipe.h"
#include "sg_ae_distribution.h"
#include "sg_ae_smooth.h"
#include "sg_afd_distribution.h"
#include "sg_ae_limits.h"
#include "flash_generator.h"
#include "sg_lens.h"

typedef struct {
    algo_vpipe_hndl_t           vpipe;
    sg_distr_ae_hndl_t          ae_distr;
    sg_ae_smooth_hndl_t         ae_smooth;
    sg_distr_afd_hndl_t         afd_distr;
    sg_ae_limits_hndl_t         ae_limits;
    fl_gen_hndl_t               fl_gen;
    sg_lens_thread_data_t       *sg_lens;
} cl_algs_hdl_t;

typedef struct {
    guzzi_event_t       *evt_hndl;
    uint32              camera_id;
    uint32              max_fr;
} cl_algs_create_params_t;

static inline float get_max_sensor_fps(virt_cm_sen_mode_features_t *vsen)
{
    return vsen->sen->modes.list[vsen->sen_mode_idx].fps.max;
}

static inline float get_min_sensor_fps(virt_cm_sen_mode_features_t *vsen)
{
    return vsen->sen->modes.list[vsen->sen_mode_idx].fps.min;
}

int cl_algo_sg_algs_create(cl_algs_hdl_t *hdl, cl_algs_create_params_t *params);
void cl_algo_sg_algs_destroy(cl_algs_hdl_t *hdl);

#endif /* CL_ALGO_HELPER_H_ */
