/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file cl_zsl_stm_types.h
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#ifndef _CL_ZSL_STM_TYPES_H
#define _CL_ZSL_STM_TYPES_H

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include <guzzi_event/include/guzzi_event.h>
#include "cl_common_private.h"

typedef enum {
    CL_ZSL_STATE_IDLE,
    CL_ZSL_STATE_CAPTURE,
    MAX_CL_ZSL_STATE
} cl_zsl_state_t;

typedef enum {
    CL_ZSL_CMD_SATRT,
    CL_ZSL_CMD_FRAME_REQUEST,
    MAX_CL_ZSL_CMD
} cl_zsl_cmd_t;

typedef struct {
    guzzi_event_t       *evt_hndl;
    uint32              camera_id;
    uint32              camera_alt_id;
} cl_zsl_create_params_t;

int cl_create_zsl_stm (cl_stm_t **zsl_hdl, cl_zsl_create_params_t *params);
int cl_destroy_zsl_stm (cl_stm_t *hdl);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* _CL_ZSL_STM_TYPES_H */


