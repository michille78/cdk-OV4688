/* =============================================================================
* Copyright (c) 2016 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file cl_custom_stm_types.h
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 10-June-2016 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#ifndef _CL_CUSTOM_STM_TYPES_H
#define _CL_CUSTOM_STM_TYPES_H

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include "cl_common_private.h"

typedef enum {
     CL_CUSTOM_STATE_IDLE,
     CL_CUSTOM_STATE_STARTUP,
     CL_CUSTOM_STATE_PREVIEW,
     CL_CUSTOM_STATE_TEMPORAL_BRACKETING,
     CL_CUSTOM_STATE_EXPOSURE_BRACKETING,
     CL_CUSTOM_STATE_ABORTING,
     MAX_CL_CUSTOM_STATE
} cl_custom_state_t;

typedef enum {
    CL_CUSTOM_CMD_SATRT,
    CL_CUSTOM_CMD_REQUEST,
    CL_CUSTOM_CMD_ABORT,
    MAX_CL_CUSTOM_CMD
} cl_custom_cmd_t;

int cl_create_custom_stm (cl_stm_t **custom_hdl);
int cl_destroy_custom_stm (cl_stm_t *hndl);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* _CL_CUSTOM_STM_TYPES_H */
