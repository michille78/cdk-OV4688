/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file cl_algo_helper.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include "cl_algo_helper.h"

/* ========================================================================== */
/**
* cl_algo_sg_algs_create()
*/
/* ========================================================================== */
int cl_algo_sg_algs_create(cl_algs_hdl_t *hdl, cl_algs_create_params_t *params)
{
    sg_lens_create_params_t lens_params;
    int ret = 0;

    lens_params.evt_hndl = params->evt_hndl;
    lens_params.camera_id = params->camera_id;

    ret = sg_ae_smooth_create(&hdl->ae_smooth);
    ret += sg_ae_limits_create(&hdl->ae_limits);
    ret += sg_distr_ae_create(&hdl->ae_distr);
    ret += sg_distr_afd_create(&hdl->afd_distr);
    ret += algo_vpipe_create(&hdl->vpipe, params->max_fr);
    ret += fl_gen_create(&hdl->fl_gen);
    hdl->sg_lens = sg_lens_create(&lens_params);

    if (!hdl->sg_lens) {
        ret += -1;
    }

    return ret;
}

/* ========================================================================== */
/**
* void cl_algo_sg_algs_destroy()
*/
/* ========================================================================== */
void cl_algo_sg_algs_destroy(cl_algs_hdl_t *hdl)
{
    if (hdl->ae_smooth) {
        sg_ae_smooth_destroy(hdl->ae_smooth);
    }

    if (hdl->ae_limits) {
        sg_ae_limits_destroy(hdl->ae_limits);
    }

    if (hdl->ae_distr) {
        sg_distr_ae_destroy(hdl->ae_distr);
    }

    if (hdl->afd_distr) {
        sg_distr_afd_destroy(hdl->afd_distr);
    }

    if (hdl->vpipe) {
        algo_vpipe_destroy(hdl->vpipe);
    }

    if (hdl->fl_gen) {
        fl_gen_destroy(hdl->fl_gen);
    }

    if (hdl->sg_lens) {
        sg_lens_destroy(hdl->sg_lens);
    }
}
