/* =============================================================================
* Copyright (c) 2016 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file cl_custom_state_machine.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 10-June-2016 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <osal/osal_stdlib.h>
#include <osal/osal_string.h>
#include <utils/mms_debug.h>
#include <error_handle/include/error_handle.h>
#include "cl_custom_stm_types.h"
#include "sg_mode_custom.h"
#include <sg_lens.h>

mmsdbg_define_variable(
        vdl_custom_control_logic,
        DL_DEFAULT,
        0,
        "norm.control_logic",
        "NORM Control Logic"
    );
#define MMSDEBUGLEVEL mmsdbg_use_variable(vdl_custom_control_logic)

typedef struct {
    cl_custom_state_t       curr;
    cl_custom_state_t       next;
    sga_custom_hndl_t       hsg_custom_prv;
    sga_custom_config_t     sga_cfg;
    unsigned int            calc_cnt;
    gzz_3a_mode_t           aca_mode;
    focus_mode_t            focus_mode;
    uint8                   manual_lens_move;
    gzz_lock_type_t         ae_lock;
    gzz_lock_type_t         wb_lock;
    uint32                  repeat_frm;

    gzz_cam_custom_usecase_t        use_case;
    gzz_cam_custom_capture_mode_t   mode;
    uint32                          cnt_shot;
    uint32                          bracketing_shot;
    float                           bracketing_seq[GZZ_CAM_EXP_BRACKETING_SEQUENCE_LENGHT];
    float                           compensation;
} cl_custom_stm_ctx_t;

/* ========================================================================== */
/**
* dummy_state()
*/
/* ========================================================================== */
static int dummy_state (void *nctx, cl_algs_hdl_t *halgs, cl_in_t *in, cl_out_t *out)
{
    mmsdbg(DL_FUNC, "Enter");

    mmsdbg(DL_FUNC, "Exit Ok");
    return 0;
}

/* ========================================================================== */
/**
* custom_start()
*/
/* ========================================================================== */
static int custom_start (void *cctx, cl_algs_hdl_t *halgs, cl_in_t *in, cl_out_t *out)
{
    cl_custom_stm_ctx_t *c_ctx = cctx;
    int32 ret = 0;

    mmsdbg(DL_FUNC, "Enter");

    c_ctx->curr = CL_CUSTOM_STATE_STARTUP;
    c_ctx->next = CL_CUSTOM_STATE_PREVIEW;

    {
        sg_lens_config_t cfg;
        cfg.dtp_s_common = c_ctx->sga_cfg.dtp_s_common;
        cfg.focus_range = c_ctx->sga_cfg.gzz_cfg->af_range.val;
        cfg.frame_time = 1000000 / c_ctx->sga_cfg.gzz_cfg->fps_range.val.max;

        sg_lens_config(halgs->sg_lens, &cfg);
    }

    ret = sga_custom_configuration(c_ctx->hsg_custom_prv, halgs, &c_ctx->sga_cfg, c_ctx->curr);

    mmsdbg(DL_FUNC, "Exit %s", ret ? "Error" : "Ok");
    return ret;
}

/* ========================================================================== */
/**
* custom_start_req()
*/
/* ========================================================================== */
static int custom_start_req (void *cctx, cl_algs_hdl_t *halgs, cl_in_t *in, cl_out_t *out)
{
    cl_custom_stm_ctx_t *c_ctx = cctx;
    sga_custom_input_t  sg_in;
    sga_custom_output_t sg_out;
    cam_cl_algo_ctrl_list_t *entry;
    int32 ret = 0;
    uint32 flags;
    uint32 skip = 1;

    mmsdbg(DL_FUNC, "Enter");

    flags = (in->enb_aca)?(CL_FLAG_FORCE_POST_CALC_EVENT):0;

    sg_in.dtp_d_common  = in->dtp_dyn;
    sg_in.ae_out        = in->ae_out;
    sg_in.ae_stab_out   = in->ae_stab;
    sg_in.awb_out       = in->awb_out;
    sg_in.focus_out     = in->focus;
    sg_in.vsen_modes    = in->vsen_modes;
    sg_in.ae_smooth_mode= SG_AE_SMOOTH_MODE_FAST;
    sg_in.afd_out       = in->afd_out;

    sg_in.ae_distr_mode = SG_AE_DISTR_MODE_PREVIEW;

    sg_out.ae_distr     = &out->sg.ae_distr;
    sg_out.ae_smooth    = &out->sg.ae_smooth;
    sg_out.vpipe        = &out->vpipe;
    sg_out.light        = NULL;


    ret = sga_custom_process(c_ctx->hsg_custom_prv, halgs, &sg_in, &sg_out, c_ctx->compensation);
    skip = (in->vsen_modes->sen->modes.list[in->vsen_modes->sen_mode_idx].num_bad_frames > c_ctx->calc_cnt) ? 1 : 0;

    if ((c_ctx->aca_mode != CAM_ACA_MODE_OFF) && !skip) {
        if (c_ctx->ae_lock == CAM_ALG_LOCK_OFF) {
            entry = list_pool_append_element(in->pool_alg_ctrl, out->cl_req_aca);
            init_algo_ctrl_entry(&entry->ctrl,
                                 CL_BUFF_STATS_AEWB,
                                 CL_ALGO_AE,
                                 ACA_AE_STAB_MODE_NONE,
                                 0,
                                 flags);
        }

        if (c_ctx->wb_lock == CAM_ALG_LOCK_OFF) {

            entry = list_pool_append_element(in->pool_alg_ctrl, out->cl_req_aca);
            init_algo_ctrl_entry(&entry->ctrl,
                                 CL_BUFF_STATS_AEWB,
                                 CL_ALGO_AWB,
                                 AWB_CALC_MODE_PREVIEW_FAST,
                                 AWB_CTRL_MODE_NORMAL,
                                 flags);
        }

        { // FMV
            entry = list_pool_append_element(in->pool_alg_ctrl, out->cl_req_aca);
            init_algo_ctrl_entry(&entry->ctrl,
                                 CL_BUFF_STATS_AEWB,
                                 CL_ALGO_FMV,
                                 0,
                                 0,
                                 flags);
        }
    }

    { // ISP
        entry = list_pool_append_element(in->pool_alg_ctrl, out->cl_req_img);
        cam_cl_algo_ctrl_t *alg;

        init_algo_ctrl_entry(&entry->ctrl,
                             CL_BUFF_ISP,
                             CL_ALGO_ISP,
                             0,
                             0,
                             CL_FLAG_FORCE_PROCESS);
        entry->ctrl.alg_specific.isp.out_flags = DEF_PRV_PIPE_CTRL_FLAGS;

        alg = &entry->ctrl;
        switch (c_ctx->use_case) {
            case CAM_CUSTOM_USECASE_SELECTION_STILL:
                alg->alg_specific.isp.out_flags = IC_PIPECTL_MODE_STILL_VIDEO;
                break;
            case CAM_CUSTOM_USECASE_SELECTION_LOW_POWER_VIDEO:
                alg->alg_specific.isp.out_flags = IC_PIPECTL_MODE_LOW_POWER;
                break;
            default:
            case CAM_CUSTOM_USECASE_SELECTION_VIDEO:
                alg->alg_specific.isp.out_flags = IC_PIPECTL_MODE_VIDEO;
                break;
        }
    }

    if (skip) {
        entry->ctrl.alg_specific.isp.out_flags &= ~IC_PIPECTL_MIPI_ENABLE;
    }
    out->light = sg_out.light; // &out->sg.ae_distr.flash;

    if ((c_ctx->calc_cnt++ > 8) || (sg_in.awb_out->converged && sg_out.ae_distr->distr_conv)) {
        c_ctx->curr = c_ctx->next;
    }

    mmsdbg(DL_FUNC, "Exit %s", ret ? "Error" : "Ok");
    return ret;
}

/* ========================================================================== */
/**
* custom_req()
*/
/* ========================================================================== */
static int custom_req (void *cctx, cl_algs_hdl_t *halgs, cl_in_t *in, cl_out_t *out)
{
    int32 ret = 0;
    cl_custom_stm_ctx_t *c_ctx = cctx;
    sga_custom_input_t  sg_in;
    sga_custom_output_t sg_out;
    cam_cl_algo_ctrl_list_t *entry;
    uint32 flags;
    float ev_step;

    mmsdbg(DL_FUNC, "Enter");

    c_ctx->curr = c_ctx->next;

    flags = (in->enb_aca)?(CL_FLAG_FORCE_POST_CALC_EVENT):0;

    sg_in.dtp_d_common  = in->dtp_dyn;
    sg_in.ae_out        = in->ae_out;
    sg_in.ae_stab_out   = in->ae_stab;
    sg_in.awb_out       = in->awb_out;
    sg_in.focus_out     = in->focus;
    sg_in.vsen_modes    = in->vsen_modes;
    sg_in.afd_out       = in->afd_out;

    if ((c_ctx->curr != CL_CUSTOM_STATE_PREVIEW) && (c_ctx->cnt_shot))
    {
        sg_in.ae_distr_mode = SG_AE_DISTR_MODE_CAPTURE;
        sg_in.ae_smooth_mode= SG_AE_SMOOTH_MODE_FAST;
    } else
    {
        sg_in.ae_distr_mode = SG_AE_DISTR_MODE_PREVIEW;
        sg_in.ae_smooth_mode= SG_AE_SMOOTH_MODE_MEDIUM;
    }
    sg_out.ae_distr     = &out->sg.ae_distr;
    sg_out.ae_smooth    = &out->sg.ae_smooth;
    sg_out.vpipe        = &out->vpipe;
    sg_out.light        = NULL;

    ev_step = (CL_CUSTOM_STATE_EXPOSURE_BRACKETING == c_ctx->curr) ?
            (c_ctx->bracketing_seq[c_ctx->bracketing_shot]) : c_ctx->compensation;

    c_ctx->bracketing_shot++;
    c_ctx->bracketing_shot %= GZZ_CAM_EXP_BRACKETING_SEQUENCE_LENGHT;

    sga_custom_process(c_ctx->hsg_custom_prv, halgs, &sg_in, &sg_out, ev_step);

    if ((c_ctx->calc_cnt % 1 == 0) && (c_ctx->aca_mode != CAM_ACA_MODE_OFF)) {
        if (c_ctx->ae_lock == CAM_ALG_LOCK_OFF) {
            entry = list_pool_append_element(in->pool_alg_ctrl, out->cl_req_aca);
            init_algo_ctrl_entry(&entry->ctrl,
                                 CL_BUFF_STATS_AEWB,
                                 CL_ALGO_AE,
                                 ACA_AE_STAB_MODE_MEDIUM,
                                 0,
                                 flags);
        }

        if (c_ctx->wb_lock == CAM_ALG_LOCK_OFF) {

            entry = list_pool_append_element(in->pool_alg_ctrl, out->cl_req_aca);
            init_algo_ctrl_entry(&entry->ctrl,
                                 CL_BUFF_STATS_AEWB,
                                 CL_ALGO_AWB,
                                 AWB_CALC_MODE_PREVIEW,
                                 AWB_CTRL_MODE_NORMAL,
                                 flags);
        }

        entry = list_pool_append_element(in->pool_alg_ctrl, out->cl_req_aca);
        init_algo_ctrl_entry(&entry->ctrl,
                             CL_BUFF_STATS_AEWB,
                             CL_ALGO_FMV,
                             0,
                             0,
                             flags);

    }

    if ((AF_MODE_CONTINUOUS == c_ctx->focus_mode) && (c_ctx->aca_mode != CAM_ACA_MODE_OFF)) {
        entry = list_pool_append_element(in->pool_alg_ctrl, out->cl_req_aca);
        init_algo_ctrl_entry(&entry->ctrl,
                             CL_BUFF_STATS_AF,
                             CL_ALGO_CAF,
                             0,
                             0,
                             flags);
    }

    if ((c_ctx->manual_lens_move) && (c_ctx->focus_mode == AF_MODE_OFF)) {
        sg_lens_input_t _in;

        _in.dtp_d_common = in->dtp_dyn;
        _in.focus_result.seq_cnt = 0;
        _in.focus_result.move = AF_LENS_MOVE_GLOBAL;
        _in.focus_result.pos =
                c_ctx->sga_cfg.gzz_cfg->lens_focal_len.val;

        sg_lens_process(halgs->sg_lens, &_in, &c_ctx->repeat_frm);
        c_ctx->manual_lens_move = 0;
    }

    { // ZSL
        cam_cl_algo_ctrl_t *alg;

        entry = list_pool_append_element(in->pool_alg_ctrl, out->cl_req_img);
        init_algo_ctrl_entry(&entry->ctrl,
                             CL_BUFF_ISP,
                             CL_ALGO_ISP,
                             0,
                             0,
                             CL_FLAG_FORCE_PROCESS);
        entry->ctrl.alg_specific.isp.out_flags = DEF_PRV_PIPE_CTRL_FLAGS;

        alg = &entry->ctrl;
        switch (c_ctx->use_case) {
            case CAM_CUSTOM_USECASE_SELECTION_STILL:
                alg->alg_specific.isp.out_flags = IC_PIPECTL_MODE_STILL_VIDEO;
                break;
            case CAM_CUSTOM_USECASE_SELECTION_LOW_POWER_VIDEO:
                alg->alg_specific.isp.out_flags = IC_PIPECTL_MODE_LOW_POWER;
                break;
            default:
            case CAM_CUSTOM_USECASE_SELECTION_VIDEO:
                alg->alg_specific.isp.out_flags = IC_PIPECTL_MODE_VIDEO;
                break;
        }

        if (c_ctx->curr != CL_CUSTOM_STATE_PREVIEW) {
            if (c_ctx->cnt_shot--) {
                alg->alg_specific.isp.out_flags = IC_PIPECTL_MODE_STILL_STILL;
            }
            if (0 == c_ctx->cnt_shot) {
                c_ctx->next = CL_CUSTOM_STATE_PREVIEW;
            }
        }

    }
    out->light = sg_out.light; // &out->sg.ae_distr.flash;

    c_ctx->calc_cnt++;

    if (c_ctx->repeat_frm && --c_ctx->repeat_frm) {
        out->seq_status = CL_SEQ_STAT_PROGRESS;
    }


    mmsdbg(DL_FUNC, "Exit %s", ret ? "Error" : "Ok");
    return ret;

}

/* ========================================================================== */
/**
* custom_abort()
*/
/* ========================================================================== */
static int custom_abort (void *cctx, cl_algs_hdl_t *halgs, cl_in_t *in, cl_out_t *out)
{
    cl_custom_stm_ctx_t *c_ctx = cctx;
    int32 ret = 0;

    mmsdbg(DL_FUNC, "Enter");

    c_ctx->curr = CL_CUSTOM_STATE_IDLE;

    mmsdbg(DL_FUNC, "Exit %s", ret ? "Error" : "Ok");
    return ret;
}

/* ========================================================================== */
/**
* custom_abort_req()
*/
/* ========================================================================== */
static int custom_abort_req (void *nctx, cl_algs_hdl_t *halgs, cl_in_t *in, cl_out_t *out)
{
    cl_custom_stm_ctx_t *n_ctx = nctx;
    int32 ret = 0;

    mmsdbg(DL_FUNC, "Enter");

    n_ctx->curr = n_ctx->next;

    mmsdbg(DL_FUNC, "Exit %s", ret ? "Error" : "Ok");
    return ret;
}

static const p_cl_state_exe_t custom_state_table[MAX_CL_CUSTOM_STATE][MAX_CL_CUSTOM_CMD] = {
    {custom_start, custom_start_req, dummy_state},
    {dummy_state, custom_start_req, custom_abort},
    {dummy_state, custom_req, custom_abort},
    {dummy_state, custom_req, custom_abort},
    {dummy_state, custom_req, custom_abort},
    {dummy_state, custom_abort_req, dummy_state},
};

/* ========================================================================== */
/**
* custom_exec()
*/
/* ========================================================================== */
static int custom_exec (void *cctx,
                            cl_custom_cmd_t cmd,
                            cl_algs_hdl_t *halgs,
                            cl_in_t *in,
                            cl_out_t *out)
{
    cl_custom_stm_ctx_t *c_ctx = cctx;
    cl_custom_state_t state = c_ctx->curr;

    if ((state >= MAX_CL_CUSTOM_STATE) || (cmd >= MAX_CL_CUSTOM_CMD)) {
        mmsdbg(DL_ERROR, "Out of range parameters");
        return -1;
    }

    return custom_state_table[state][cmd](c_ctx, halgs, in, out);
}

/* ========================================================================== */
/**
* custom_ready()
*/
/* ========================================================================== */
static int custom_ready (void *cctx)
{
    cl_custom_stm_ctx_t *c_ctx = cctx;
    cl_custom_state_t state = c_ctx->curr;

    return (CL_CUSTOM_STATE_IDLE == state) ? 1 : 0;
}

/* ========================================================================== */
/**
* custom_config()
*/
/* ========================================================================== */
static int custom_config (void *cctx, cl_algs_hdl_t *halgs, cl_cfg_t *cfg)
{
    cl_custom_stm_ctx_t *c_ctx = cctx;
    cam_gzz_cfg_t *gzz_cfg = &cfg->new_cam_cfg->cam_gzz_cfg;
    int ret = 0;

    c_ctx->aca_mode = gzz_cfg->aca_mode.val;
    c_ctx->focus_mode = gzz_cfg->af_mode.val;
    c_ctx->wb_lock = gzz_cfg->wb_lock.val;
    c_ctx->ae_lock = gzz_cfg->ae_lock.val;
    c_ctx->sga_cfg.dtp_s_common= cfg->dtp_stat;
    c_ctx->sga_cfg.gzz_cfg = &cfg->new_cam_cfg->cam_gzz_cfg;

    if (is_gzz_cfg_changed(cfg->cur_cam_cfg,
            cfg->new_cam_cfg, cam_gzz_cfg.lens_focal_len)) {
        c_ctx->manual_lens_move = 1;
    }

    if (is_gzz_cfg_changed(cfg->cur_cam_cfg,
            cfg->new_cam_cfg, cam_gzz_cfg.custom_usecase_selection))
    {
        c_ctx->use_case = cfg->new_cam_cfg->cam_gzz_cfg.custom_usecase_selection.val;
    }

    if (is_gzz_cfg_changed(cfg->cur_cam_cfg,
            cfg->new_cam_cfg, cam_gzz_cfg.custom_capture_mode_selection))
    {
        c_ctx->mode = cfg->new_cam_cfg->cam_gzz_cfg.custom_capture_mode_selection.val;
    }

    if (is_gzz_cfg_changed(cfg->cur_cam_cfg,
            cfg->new_cam_cfg, cam_gzz_cfg.exp_compensation))
    {
        c_ctx->compensation = cfg->new_cam_cfg->cam_gzz_cfg.exp_compensation.val;
    }

    if (is_gzz_cfg_changed(cfg->cur_cam_cfg,
            cfg->new_cam_cfg, cam_gzz_cfg.custom_exposure_bracketing_sequence))
    {
         osal_memcpy(c_ctx->bracketing_seq,
                         cfg->new_cam_cfg->cam_gzz_cfg.custom_exposure_bracketing_sequence.val,
                         sizeof(c_ctx->bracketing_seq));
    }

    if (is_gzz_cfg_changed(cfg->cur_cam_cfg,
            cfg->new_cam_cfg, cam_gzz_cfg.custom_capture_number_shots))
    {
        c_ctx->cnt_shot = cfg->new_cam_cfg->cam_gzz_cfg.custom_capture_number_shots.val;

        if (c_ctx->cnt_shot && c_ctx->use_case == CAM_CUSTOM_USECASE_SELECTION_STILL) {
            switch (c_ctx->mode) {
            case CAM_CUSTOM_CAPTURE_MODE_SELECTION_EXPOSURE_BRACKETING:
                c_ctx->next = CL_CUSTOM_STATE_EXPOSURE_BRACKETING;
                c_ctx->bracketing_shot = 0;
                break;
            case CAM_CUSTOM_CAPTURE_MODE_SELECTION_TEMPORAL_BRACKETING:
            case CAM_CUSTOM_CAPTURE_MODE_SELECTION_LONG_EXPOSURE:
                c_ctx->next = CL_CUSTOM_STATE_TEMPORAL_BRACKETING;
                break;
            default:
                mmsdbg(DL_ERROR, "Unsupported capture mode %d", c_ctx->mode);
                c_ctx->next = CL_CUSTOM_STATE_EXPOSURE_BRACKETING;
                break;
            }
        } else {
            // mmsdbg(DL_ERROR, "Custom capture usecase %d", c_ctx->use_case)
            c_ctx->next = CL_CUSTOM_STATE_PREVIEW;
        }
    }

    if (CL_CUSTOM_STATE_IDLE != c_ctx->curr) {
        ret = sga_custom_configuration(c_ctx->hsg_custom_prv, halgs, &c_ctx->sga_cfg, c_ctx->next);
        if (is_gzz_cfg_changed(cfg->cur_cam_cfg, cfg->new_cam_cfg, cam_gzz_cfg.fps_range) ||
                is_gzz_cfg_changed(cfg->cur_cam_cfg, cfg->new_cam_cfg, cam_gzz_cfg.af_range)) {
                sg_lens_config_t cfg;
                cfg.dtp_s_common = c_ctx->sga_cfg.dtp_s_common;
                cfg.focus_range = c_ctx->sga_cfg.gzz_cfg->af_range.val;
                cfg.frame_time = 1000000 / c_ctx->sga_cfg.gzz_cfg->fps_range.val.max;

                sg_lens_config(halgs->sg_lens, &cfg);
        }
    }

    mmsdbg(DL_FUNC, "Exit %s", ret ? "Error" : "Ok");
    return ret;
}

/* ========================================================================== */
/**
* cl_create_custom_stm()
*/
/* ========================================================================== */
int cl_create_custom_stm (cl_stm_t **custom_hdl)
{
    cl_custom_stm_ctx_t *c_ctx;
    *custom_hdl = osal_calloc(1, sizeof(cl_stm_t));

    if (!*custom_hdl) {
        goto ERROR_1;
    }

    (*custom_hdl)->private = osal_calloc(1, sizeof(cl_custom_stm_ctx_t));

    if (!(*custom_hdl)->private) {
        goto ERROR_2;
    }

    c_ctx = (*custom_hdl)->private;

    if (sga_custom_create(&c_ctx->hsg_custom_prv)) {
        goto ERROR_3;
    }

    (*custom_hdl)->config = custom_config;
    (*custom_hdl)->exec = custom_exec;
    (*custom_hdl)->ready = custom_ready;

    return 0;

ERROR_3:
    osal_free(c_ctx);
ERROR_2:
    osal_free(*custom_hdl);
ERROR_1:
    mmsdbg(DL_ERROR, "Can't allocate memory");
    return -1;
}


/* ========================================================================== */
/**
* cl_destroy_custom_stm()
*/
/* ========================================================================== */
int cl_destroy_custom_stm (cl_stm_t *hndl)
{
    cl_custom_stm_ctx_t *c_ctx = hndl->private;

    sga_custom_destroy(c_ctx->hsg_custom_prv);
    osal_free(hndl->private);
    osal_free(hndl);
    return 0;
}

