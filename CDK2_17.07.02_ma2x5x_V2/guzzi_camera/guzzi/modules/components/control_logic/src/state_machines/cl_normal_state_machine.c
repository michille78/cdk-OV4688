/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file cl_normal_state_machine.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <osal/osal_stdlib.h>
#include <osal/osal_string.h>
#include <utils/mms_debug.h>
#include <error_handle/include/error_handle.h>
#include "cl_normal_stm_types.h"
#include "sg_mode_normal_preview.h"
#include <sg_lens.h>

mmsdbg_define_variable(
        vdl_normal_control_logic,
        DL_DEFAULT,
        0,
        "norm.control_logic",
        "NORM Control Logic"
    );
#define MMSDEBUGLEVEL mmsdbg_use_variable(vdl_normal_control_logic)

typedef struct {
    cl_normal_state_t       curr;
    cl_normal_state_t       next;
    sga_normal_prv_hndl_t   hsg_norm_prv;
    sga_normal_prv_config_t sga_cfg;
    unsigned int            calc_cnt;
    gzz_3a_mode_t           aca_mode;
    focus_mode_t            focus_mode;
    uint8                   manual_lens_move;
    gzz_lock_type_t         ae_lock;
    gzz_lock_type_t         wb_lock;
    uint32                  repeat_frm;
} cl_norm_stm_ctx_t;

/* ========================================================================== */
/**
* dummy_state()
*/
/* ========================================================================== */
static int dummy_state (void *nctx, cl_algs_hdl_t *halgs, cl_in_t *in, cl_out_t *out)
{
    mmsdbg(DL_FUNC, "Enter");

    mmsdbg(DL_FUNC, "Exit Ok");
    return 0;
}

/* ========================================================================== */
/**
* norm_satrt()
*/
/* ========================================================================== */
static int normal_start (void *nctx, cl_algs_hdl_t *halgs, cl_in_t *in, cl_out_t *out)
{
    cl_norm_stm_ctx_t *n_ctx = nctx;
    int32 ret = 0;

    mmsdbg(DL_FUNC, "Enter");

    n_ctx->curr = CL_NORMAL_STATE_STARTUP;
    n_ctx->next = CL_NORMAL_STATE_RUNING;

    {
        sg_lens_config_t cfg;
        cfg.dtp_s_common = n_ctx->sga_cfg.dtp_s_common;
        cfg.focus_range = n_ctx->sga_cfg.gzz_cfg->af_range.val;
        cfg.frame_time = 1000000 / n_ctx->sga_cfg.gzz_cfg->fps_range.val.max;

        sg_lens_config(halgs->sg_lens, &cfg);
    }

    ret = sga_normal_prv_configuration(n_ctx->hsg_norm_prv, halgs, &n_ctx->sga_cfg);

    mmsdbg(DL_FUNC, "Exit %s", ret ? "Error" : "Ok");
    return ret;
}

/* ========================================================================== */
/**
* normal_sart_req()
*/
/* ========================================================================== */
static int normal_start_req (void *nctx, cl_algs_hdl_t *halgs, cl_in_t *in, cl_out_t *out)
{
    cl_norm_stm_ctx_t *n_ctx = nctx;
    sga_normal_prv_input_t  sg_in;
    sga_normal_prv_output_t sg_out;
    cam_cl_algo_ctrl_list_t *entry;
    int32 ret = 0;
    uint32 flags;
    uint32 skip = 1;

    mmsdbg(DL_FUNC, "Enter");

    flags = (in->enb_aca)?(CL_FLAG_FORCE_POST_CALC_EVENT):0;

    sg_in.dtp_d_common  = in->dtp_dyn;
    sg_in.ae_out        = in->ae_out;
    sg_in.ae_stab_out   = in->ae_stab;
    sg_in.awb_out       = in->awb_out;
    sg_in.focus_out     = in->focus;
    sg_in.vsen_modes    = in->vsen_modes;
    sg_in.ae_smooth_mode= SG_AE_SMOOTH_MODE_FAST;
    sg_in.afd_out       = in->afd_out;

    sg_in.ae_distr_mode = SG_AE_DISTR_MODE_PREVIEW;

    sg_out.ae_distr     = &out->sg.ae_distr;
    sg_out.ae_smooth    = &out->sg.ae_smooth;
    sg_out.vpipe        = &out->vpipe;
    sg_out.light        = NULL;

    ret = sga_normal_prv_process(n_ctx->hsg_norm_prv, halgs, &sg_in, &sg_out);
    skip = (in->vsen_modes->sen->modes.list[in->vsen_modes->sen_mode_idx].num_bad_frames > n_ctx->calc_cnt) ? 1 : 0;

    if ((n_ctx->aca_mode != CAM_ACA_MODE_OFF) && !skip) {
        if (n_ctx->ae_lock == CAM_ALG_LOCK_OFF) {
            entry = list_pool_append_element(in->pool_alg_ctrl, out->cl_req_aca);
            init_algo_ctrl_entry(&entry->ctrl,
                                 CL_BUFF_STATS_AEWB,
                                 CL_ALGO_AE,
                                 ACA_AE_STAB_MODE_NONE,
                                 0,
                                 flags);
        }

        if (n_ctx->wb_lock == CAM_ALG_LOCK_OFF) {

            entry = list_pool_append_element(in->pool_alg_ctrl, out->cl_req_aca);
            init_algo_ctrl_entry(&entry->ctrl,
                                 CL_BUFF_STATS_AEWB,
                                 CL_ALGO_AWB,
                                 AWB_CALC_MODE_PREVIEW_FAST,
                                 AWB_CTRL_MODE_NORMAL,
                                 flags);
        }

        { // FMV
            entry = list_pool_append_element(in->pool_alg_ctrl, out->cl_req_aca);
            init_algo_ctrl_entry(&entry->ctrl,
                                 CL_BUFF_STATS_AEWB,
                                 CL_ALGO_FMV,
                                 0,
                                 0,
                                 flags);
        }
    }

    { // ISP
        entry = list_pool_append_element(in->pool_alg_ctrl, out->cl_req_img);
        init_algo_ctrl_entry(&entry->ctrl,
                             CL_BUFF_ISP,
                             CL_ALGO_ISP,
                             0,
                             0,
                             CL_FLAG_FORCE_PROCESS);
        entry->ctrl.alg_specific.isp.out_flags = DEF_PRV_PIPE_CTRL_FLAGS;
    }

    if (skip) {
        entry->ctrl.alg_specific.isp.out_flags &= ~IC_PIPECTL_MIPI_ENABLE;
    }
    out->light = sg_out.light; // &out->sg.ae_distr.flash;

    if ((n_ctx->calc_cnt++ > 8) || (sg_in.awb_out->converged && sg_out.ae_distr->distr_conv)) {
        n_ctx->curr = n_ctx->next;
    }

    mmsdbg(DL_FUNC, "Exit %s", ret ? "Error" : "Ok");
    return ret;
}

/* ========================================================================== */
/**
* normal_sart_req()
*/
/* ========================================================================== */
static int norm_req (void *nctx, cl_algs_hdl_t *halgs, cl_in_t *in, cl_out_t *out)
{
    int32 ret = 0;
    cl_norm_stm_ctx_t *n_ctx = nctx;
    sga_normal_prv_input_t  sg_in;
    sga_normal_prv_output_t sg_out;
    cam_cl_algo_ctrl_list_t *entry;
    uint32 flags;

    mmsdbg(DL_FUNC, "Enter");

    flags = (in->enb_aca)?(CL_FLAG_FORCE_POST_CALC_EVENT):0;

    sg_in.dtp_d_common  = in->dtp_dyn;
    sg_in.ae_out        = in->ae_out;
    sg_in.ae_stab_out   = in->ae_stab;
    sg_in.awb_out       = in->awb_out;
    sg_in.focus_out     = in->focus;
    sg_in.vsen_modes    = in->vsen_modes;
    sg_in.afd_out       = in->afd_out;
    sg_in.ae_smooth_mode= SG_AE_SMOOTH_MODE_MEDIUM;

    sg_in.ae_distr_mode = SG_AE_DISTR_MODE_PREVIEW;

    sg_out.ae_distr     = &out->sg.ae_distr;
    sg_out.ae_smooth    = &out->sg.ae_smooth;
    sg_out.vpipe        = &out->vpipe;
    sg_out.light        = NULL;

    sga_normal_prv_process(n_ctx->hsg_norm_prv, halgs, &sg_in, &sg_out);

    if ((n_ctx->calc_cnt % 1 == 0) && (n_ctx->aca_mode != CAM_ACA_MODE_OFF)) {
        if (n_ctx->ae_lock == CAM_ALG_LOCK_OFF) {
            entry = list_pool_append_element(in->pool_alg_ctrl, out->cl_req_aca);
            init_algo_ctrl_entry(&entry->ctrl,
                                 CL_BUFF_STATS_AEWB,
                                 CL_ALGO_AE,
                                 ACA_AE_STAB_MODE_MEDIUM,
                                 0,
                                 flags);
        }

        if (n_ctx->wb_lock == CAM_ALG_LOCK_OFF) {

            entry = list_pool_append_element(in->pool_alg_ctrl, out->cl_req_aca);
            init_algo_ctrl_entry(&entry->ctrl,
                                 CL_BUFF_STATS_AEWB,
                                 CL_ALGO_AWB,
                                 AWB_CALC_MODE_PREVIEW,
                                 AWB_CTRL_MODE_NORMAL,
                                 flags);
        }

        entry = list_pool_append_element(in->pool_alg_ctrl, out->cl_req_aca);
        init_algo_ctrl_entry(&entry->ctrl,
                             CL_BUFF_STATS_AEWB,
                             CL_ALGO_FMV,
                             0,
                             0,
                             flags);

    }

    if ((AF_MODE_CONTINUOUS == n_ctx->focus_mode) && (n_ctx->aca_mode != CAM_ACA_MODE_OFF)) {
        entry = list_pool_append_element(in->pool_alg_ctrl, out->cl_req_aca);
        init_algo_ctrl_entry(&entry->ctrl,
                             CL_BUFF_STATS_AF,
                             CL_ALGO_CAF,
                             0,
                             0,
                             flags);
    }

    if ((n_ctx->manual_lens_move) && (n_ctx->focus_mode == AF_MODE_OFF)) {
        sg_lens_input_t _in;

        _in.dtp_d_common = in->dtp_dyn;
        _in.focus_result.seq_cnt = 0;
        _in.focus_result.move = AF_LENS_MOVE_GLOBAL;
        _in.focus_result.pos =
                n_ctx->sga_cfg.gzz_cfg->lens_focal_len.val;

        sg_lens_process(halgs->sg_lens, &_in, &n_ctx->repeat_frm);
        n_ctx->manual_lens_move = 0;
    }

    { // ZSL
        entry = list_pool_append_element(in->pool_alg_ctrl, out->cl_req_img);
        init_algo_ctrl_entry(&entry->ctrl,
                             CL_BUFF_ISP,
                             CL_ALGO_ISP,
                             0,
                             0,
                             CL_FLAG_FORCE_PROCESS);
        entry->ctrl.alg_specific.isp.out_flags = DEF_PRV_PIPE_CTRL_FLAGS;
    }
    out->light = sg_out.light; // &out->sg.ae_distr.flash;

    n_ctx->calc_cnt++;

    if (n_ctx->repeat_frm && --n_ctx->repeat_frm) {
        out->seq_status = CL_SEQ_STAT_PROGRESS;
    }

    mmsdbg(DL_FUNC, "Exit %s", ret ? "Error" : "Ok");
    return ret;

}

/* ========================================================================== */
/**
* norm_abort()
*/
/* ========================================================================== */
static int norm_abort (void *nctx, cl_algs_hdl_t *halgs, cl_in_t *in, cl_out_t *out)
{
    cl_norm_stm_ctx_t *n_ctx = nctx;
    int32 ret = 0;

    mmsdbg(DL_FUNC, "Enter");

    n_ctx->curr = CL_NORMAL_STATE_IDLE;

    mmsdbg(DL_FUNC, "Exit %s", ret ? "Error" : "Ok");
    return ret;
}

/* ========================================================================== */
/**
* norm_abort_req_frm()
*/
/* ========================================================================== */
static int norm_abort_req (void *nctx, cl_algs_hdl_t *halgs, cl_in_t *in, cl_out_t *out)
{
    cl_norm_stm_ctx_t *n_ctx = nctx;
    int32 ret = 0;

    mmsdbg(DL_FUNC, "Enter");

    n_ctx->curr = n_ctx->next;

    mmsdbg(DL_FUNC, "Exit %s", ret ? "Error" : "Ok");
    return ret;
}

static const p_cl_state_exe_t norm_state_table[MAX_CL_NORMAL_STATE][MAX_CL_NORMAL_CMD] = {
    {normal_start, normal_start_req, dummy_state},
    {dummy_state, normal_start_req, norm_abort},
    {dummy_state, norm_req, norm_abort},
    {dummy_state, norm_abort_req, dummy_state},
};

/* ========================================================================== */
/**
* norm_exec()
*/
/* ========================================================================== */
static int norm_exec (void *nctx,
                            cl_normal_cmd_t cmd,
                            cl_algs_hdl_t *halgs,
                            cl_in_t *in,
                            cl_out_t *out)
{
    cl_norm_stm_ctx_t *n_ctx = nctx;
    cl_normal_state_t state = n_ctx->curr;

    if ((state >= MAX_CL_NORMAL_STATE) || (cmd >= MAX_CL_NORMAL_CMD)) {
        mmsdbg(DL_ERROR, "Out of range parameters");
        return -1;
    }

    return norm_state_table[state][cmd](n_ctx, halgs, in, out);
}

/* ========================================================================== */
/**
* norm_ready()
*/
/* ========================================================================== */
static int norm_ready (void *nctx)
{
    cl_norm_stm_ctx_t *n_ctx = nctx;
    cl_normal_state_t state = n_ctx->curr;

    return (CL_NORMAL_STATE_IDLE == state) ? 1 : 0;
}

/* ========================================================================== */
/**
* norm_ready()
*/
/* ========================================================================== */
static int norm_config (void *nctx, cl_algs_hdl_t *halgs, cl_cfg_t *cfg)
{
    cl_norm_stm_ctx_t *n_ctx = nctx;
    cam_gzz_cfg_t *gzz_cfg = &cfg->new_cam_cfg->cam_gzz_cfg;
    int ret = 0;

    n_ctx->aca_mode = gzz_cfg->aca_mode.val;
    n_ctx->focus_mode = gzz_cfg->af_mode.val;
    n_ctx->wb_lock = gzz_cfg->wb_lock.val;
    n_ctx->ae_lock = gzz_cfg->ae_lock.val;
    n_ctx->sga_cfg.dtp_s_common= cfg->dtp_stat;
    n_ctx->sga_cfg.gzz_cfg = &cfg->new_cam_cfg->cam_gzz_cfg;

    if (is_gzz_cfg_changed(cfg->cur_cam_cfg,
            cfg->new_cam_cfg, cam_gzz_cfg.lens_focal_len)) {
        n_ctx->manual_lens_move = 1;
    }

    if (CL_NORMAL_STATE_IDLE != n_ctx->curr) {
        ret = sga_normal_prv_configuration(n_ctx->hsg_norm_prv, halgs, &n_ctx->sga_cfg);
        if (is_gzz_cfg_changed(cfg->cur_cam_cfg, cfg->new_cam_cfg, cam_gzz_cfg.fps_range) ||
                is_gzz_cfg_changed(cfg->cur_cam_cfg, cfg->new_cam_cfg, cam_gzz_cfg.af_range)) {
                sg_lens_config_t cfg;
                cfg.dtp_s_common = n_ctx->sga_cfg.dtp_s_common;
                cfg.focus_range = n_ctx->sga_cfg.gzz_cfg->af_range.val;
                cfg.frame_time = 1000000 / n_ctx->sga_cfg.gzz_cfg->fps_range.val.max;

                sg_lens_config(halgs->sg_lens, &cfg);
        }
    }

    mmsdbg(DL_FUNC, "Exit %s", ret ? "Error" : "Ok");
    return ret;
}

/* ========================================================================== */
/**
* cl_create_normal_stm()
*/
/* ========================================================================== */
int cl_create_normal_stm (cl_stm_t **norm_hdl)
{
    cl_norm_stm_ctx_t *n_ctx;
    *norm_hdl = osal_calloc(1, sizeof(cl_stm_t));

    if (!*norm_hdl) {
        goto ERROR_1;
    }

    (*norm_hdl)->private = osal_calloc(1, sizeof(cl_norm_stm_ctx_t));

    if (!(*norm_hdl)->private) {
        goto ERROR_2;
    }

    n_ctx = (*norm_hdl)->private;

    if (sga_normal_prv_create(&n_ctx->hsg_norm_prv)) {
        goto ERROR_3;
    }

    (*norm_hdl)->config = norm_config;
    (*norm_hdl)->exec = norm_exec;
    (*norm_hdl)->ready = norm_ready;

    return 0;

ERROR_3:
    osal_free(n_ctx);
ERROR_2:
    osal_free(*norm_hdl);
ERROR_1:
    mmsdbg(DL_ERROR, "Can't allocate memory");
    return -1;
}


/* ========================================================================== */
/**
* cl_destroy_normal_stm()
*/
/* ========================================================================== */
int cl_destroy_normal_stm (cl_stm_t *hndl)
{
    cl_norm_stm_ctx_t *n_ctx = hndl->private;

    sga_normal_prv_destroy(n_ctx->hsg_norm_prv);
    osal_free(hndl->private);
    osal_free(hndl);
    return 0;
}

