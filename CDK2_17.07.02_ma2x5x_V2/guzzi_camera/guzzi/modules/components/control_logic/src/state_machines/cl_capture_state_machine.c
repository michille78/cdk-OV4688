/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file cl_capture_state_machine.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <osal/osal_stdlib.h>
#include <osal/osal_mutex.h>
#include <osal/osal_string.h>
#include <osal/osal_mutex.h>
#include <error_handle/include/error_handle.h>
#include <cl_capture_stm_types.h>
#include "sg_mode_capture.h"
#include "flash_generator.h"
#include <guzzi_event/include/guzzi_event.h>
#include <guzzi_event_global/include/guzzi_event_global.h>

mmsdbg_define_variable(
        vdl_capture_control_logic,
        DL_DEFAULT,
        0,
        "cap.control_logic",
        "CAP Control Logic"
    );
#define MMSDEBUGLEVEL mmsdbg_use_variable(vdl_capture_control_logic)

typedef union {
    uint32  id;
    struct {
        uint8  seq_num;
        uint8  state;
        uint8  dummy1;
        uint8  dummy2;
    };
} seq_id_t;

typedef struct {
    cl_cap_state_t          curr;
    cl_cap_state_t          next;
    sga_cap_hndl_t          hsg_cap;
    sga_cap_config_t        sga_cfg;
    fl_gen_output_t         out_fl;
    uint32                  flash_seq_num;
    guzzi_event_t           *evt_hdl;
    uint32                  camera_id;
    uint32                  camera_alt_id;
    ae_stab_output_t        ae_stab;
    awb_calc_output_t       awb_out;
    float                   min_fps;

    osal_sem                *sem_req_done;
    osal_mutex              *events_lock;
    cam_capture_request_t   capt_req;
    uint32                  wait_evt_flags;

} cl_cap_stm_ctx_t;


/* ========================================================================== */
/**
* void _cap_evt_notify()
*/
/* ========================================================================== */
void _cap_evt_notify (void *_prv, int event_id,
                            uint32 num, void *data)
{
    cl_cap_stm_ctx_t    *c_ctx = _prv;
    seq_id_t            *seq_id = (seq_id_t *)&num;

    mmsdbg(DL_FUNC, "Enter");

    if (!seq_id->id) {
        //it is non capture related event
        return;
    }

    mmsdbg(DL_MESSAGE, "Enter num:  %#x evt: %s",  seq_id->id, guzzi_event_global_id2str(event_id));

    osal_mutex_lock(c_ctx->events_lock);

    switch (geg_camera_event_get_eid(event_id)) {
    case CAM_EVT_AWB_READY:
        osal_memcpy(&c_ctx->awb_out, data, sizeof(c_ctx->awb_out));
        break;

    case CAM_EVT_BUFF_LOCKED:
        osal_memcpy(&c_ctx->capt_req, data, sizeof(c_ctx->capt_req));
        break;

    case CAM_EVT_AE_STAB_READY:
        osal_memcpy(&c_ctx->ae_stab, data, sizeof(c_ctx->ae_stab));
        break;

    default:
        break;
    }

    c_ctx->wait_evt_flags &= ~(1 << geg_camera_event_get_eid(event_id));
    osal_mutex_unlock(c_ctx->events_lock);
    if (!c_ctx->wait_evt_flags) {
        osal_sem_post(c_ctx->sem_req_done);
    }

    mmsdbg(DL_FUNC, "Exit %s", 0 ? "Error" : "Ok");
    return;
}

/* ========================================================================== */
/**
* void cl_cap_unreg_evt()
*/
/* ========================================================================== */
static void cl_cap_unreg_evt (cl_cap_stm_ctx_t *c_ctx)
{
    guzzi_event_unreg_recipient(c_ctx->evt_hdl, geg_camera_event_mk(c_ctx->camera_alt_id, CAM_EVT_AWB_READY),     _cap_evt_notify, c_ctx);
    guzzi_event_unreg_recipient(c_ctx->evt_hdl, geg_camera_event_mk(c_ctx->camera_alt_id, CAM_EVT_AE_READY),      _cap_evt_notify, c_ctx);
    guzzi_event_unreg_recipient(c_ctx->evt_hdl, geg_camera_event_mk(c_ctx->camera_alt_id, CAM_EVT_AE_STAB_READY), _cap_evt_notify, c_ctx);
    guzzi_event_unreg_recipient(c_ctx->evt_hdl, geg_camera_event_mk(c_ctx->camera_id, CAM_EVT_FR_ISP_DONE),       _cap_evt_notify, c_ctx);
    guzzi_event_unreg_recipient(c_ctx->evt_hdl, geg_camera_event_mk(c_ctx->camera_id, CAM_EVT_FR_ISP_REACHED),    _cap_evt_notify, c_ctx);
    guzzi_event_unreg_recipient(c_ctx->evt_hdl, geg_camera_event_mk(c_ctx->camera_id, CAM_EVT_BUFF_LOCKED),       _cap_evt_notify, c_ctx);
}

/* ========================================================================== */
/**
* void cl_cap_reg_evt()
*/
/* ========================================================================== */
static int cl_cap_reg_evt (cl_cap_stm_ctx_t *c_ctx)
{
    if(guzzi_event_reg_recipient(c_ctx->evt_hdl, geg_camera_event_mk(c_ctx->camera_alt_id, CAM_EVT_AWB_READY), _cap_evt_notify, c_ctx)) {
        goto EXIT_1;
    }

    if(guzzi_event_reg_recipient(c_ctx->evt_hdl, geg_camera_event_mk(c_ctx->camera_alt_id, CAM_EVT_AE_READY), _cap_evt_notify, c_ctx)) {
        goto EXIT_2;
    }

    if(guzzi_event_reg_recipient(c_ctx->evt_hdl, geg_camera_event_mk(c_ctx->camera_alt_id, CAM_EVT_AE_STAB_READY), _cap_evt_notify, c_ctx)) {
        goto EXIT_3;
    }

    if(guzzi_event_reg_recipient(c_ctx->evt_hdl, geg_camera_event_mk(c_ctx->camera_id, CAM_EVT_FR_ISP_DONE), _cap_evt_notify, c_ctx)) {
        goto EXIT_4;
    }

    if(guzzi_event_reg_recipient(c_ctx->evt_hdl, geg_camera_event_mk(c_ctx->camera_id, CAM_EVT_FR_ISP_REACHED), _cap_evt_notify, c_ctx)) {
        goto EXIT_5;
    }

    if(guzzi_event_reg_recipient(c_ctx->evt_hdl, geg_camera_event_mk(c_ctx->camera_id, CAM_EVT_BUFF_LOCKED), _cap_evt_notify, c_ctx)) {
        goto EXIT_6;
    }
    return 0;

EXIT_6:
    guzzi_event_unreg_recipient(c_ctx->evt_hdl, geg_camera_event_mk(c_ctx->camera_id, CAM_EVT_FR_ISP_REACHED), _cap_evt_notify, c_ctx);

EXIT_5:
    guzzi_event_unreg_recipient(c_ctx->evt_hdl, geg_camera_event_mk(c_ctx->camera_id, CAM_EVT_FR_ISP_DONE), _cap_evt_notify, c_ctx);

EXIT_4:
    guzzi_event_unreg_recipient(c_ctx->evt_hdl, geg_camera_event_mk(c_ctx->camera_alt_id, CAM_EVT_AE_STAB_READY), _cap_evt_notify, c_ctx);

EXIT_3:
    guzzi_event_unreg_recipient(c_ctx->evt_hdl, geg_camera_event_mk(c_ctx->camera_alt_id, CAM_EVT_AE_READY), _cap_evt_notify, c_ctx);

EXIT_2:
    guzzi_event_unreg_recipient(c_ctx->evt_hdl, geg_camera_event_mk(c_ctx->camera_alt_id, CAM_EVT_AWB_READY), _cap_evt_notify, c_ctx);

EXIT_1:
    return -1;
}

/* ========================================================================== */
/**
* _capt_req()
*/
/* ========================================================================== */
static int _capt_req (cl_cap_stm_ctx_t *c_ctx,
                            cl_algs_hdl_t *halgs,
                            cl_in_t *in,
                            cl_out_t *out)
{
    int32 ret = 0;
    sga_cap_input_t     sg_in;
    sga_cap_output_t    sg_out;

    mmsdbg(DL_FUNC, "Enter");

    sg_in.dtp_d_common  = in->dtp_dyn;
    sg_in.ae_out        = in->ae_out;

    if (CL_CAP_STATE_MAIN_FLASH_CAPTURE == c_ctx->curr) {
        sg_in.ae_stab_out   = &c_ctx->ae_stab;
        sg_in.awb_out = &c_ctx->awb_out;
        sg_in.ae_distr_mode = SG_AE_DISTR_MODE_PRE_FLASH;
        sg_in.ae_smooth_mode= SG_AE_SMOOTH_MODE_FAST;
        mmsdbg(DL_MESSAGE, "Flash total exp %d", c_ctx->ae_stab.texp);
    } else {
        sg_in.ae_stab_out   = in->ae_stab;
        sg_in.awb_out = in->awb_out;
        sg_in.ae_distr_mode = SG_AE_DISTR_MODE_CAPTURE_FLASH;
        sg_in.ae_smooth_mode= SG_AE_SMOOTH_MODE_FAST;
        mmsdbg(DL_MESSAGE, "Preflash total exp %d", in->ae_stab->texp);
    }

    sg_in.focus_out     = in->focus;
    sg_in.vsen_modes    = in->vsen_modes;
    sg_in.afd_out       = in->afd_out;

    sg_out.ae_distr     = &out->sg.ae_distr;
    sg_out.ae_smooth    = &out->sg.ae_smooth;
    sg_out.vpipe        = &out->vpipe;
    sg_out.light        = NULL;

    sga_cap_process(c_ctx->hsg_cap, halgs, &sg_in, &sg_out);

    mmsdbg(DL_FUNC, "Exit %s", ret ? "Error" : "Ok");
    return ret;
}

static int _capt_flash_req (cl_cap_stm_ctx_t *c_ctx,
                            cl_algs_hdl_t *halgs,
                            cl_in_t *in,
                            cl_out_t *out)
{
    int ret = 0;
    fl_gen_input_t  in_fl;
    mmsdbg(DL_FUNC, "Enter");

    osal_memset(&c_ctx->out_fl, 0x00, sizeof(c_ctx->out_fl));

    in_fl.dtp_d_common = in->dtp_dyn;

#ifdef GZZ_FLASH_SIM
    // TODO: remove me
    extern uint32 dbg_frame_duration;
    in_fl.frame_duration = dbg_frame_duration;
#else
    in_fl.frame_duration = 1000 / c_ctx->min_fps; // ms
#endif

    switch (c_ctx->curr)
    {
    case CL_CAP_STATE_RED_EYE_CAPTURE:
        in_fl.seq_type = FL_GEN_TYPE_RED_EYE;
        fl_gen_process(halgs->fl_gen, &in_fl, &c_ctx->out_fl);
        break;
    case CL_CAP_STATE_PRE_FALSH_CAPTURE:
        in_fl.seq_type = FL_GEN_TYPE_PRE_FLASH;
        fl_gen_process(halgs->fl_gen, &in_fl, &c_ctx->out_fl);
        break;
    case CL_CAP_STATE_MAIN_FLASH_CAPTURE:
        in_fl.seq_type = FL_GEN_TYPE_MAIN_FLASH;
        fl_gen_process(halgs->fl_gen, &in_fl, &c_ctx->out_fl);
        break;
    default:
        // Main capture without flash
        mmsdbg(DL_WARNING, "Flash will not be used");
        break;
    }

    out->light = c_ctx->out_fl.fl_ctrl;
    mmsdbg(DL_FUNC, "Exit %s", ret ? "Error" : "Ok");
    return ret;
}


/* ========================================================================== */
/**
* dummy_state()
*/
/* ========================================================================== */
static int dummy_state (void *cctx, cl_algs_hdl_t *halgs, cl_in_t *in, cl_out_t *out)
{
    mmsdbg(DL_FUNC, "Enter");

    mmsdbg(DL_FUNC, "Exit Ok");
    return 0;
}

/* ========================================================================== */
/**
* cap_start()
*/
/* ========================================================================== */
static int cap_start(void *cctx, cl_algs_hdl_t *halgs, cl_in_t *in, cl_out_t *out)
{
    cl_cap_stm_ctx_t *c_ctx = cctx;
    int32 ret = 0;

    mmsdbg(DL_FUNC, "Enter");

    c_ctx->flash_seq_num = 0;
    ret = sga_cap_configuration(c_ctx->hsg_cap, halgs, &c_ctx->sga_cfg);
    c_ctx->min_fps = get_min_sensor_fps(in->vsen_modes);

    osal_sem_init(c_ctx->sem_req_done, 1);

    switch (c_ctx->sga_cfg.gzz_cfg->ae_mode.val) {
    case CAM_AE_MODE_ON_AUTO_FLASH:
    case CAM_AE_MODE_ON_ALWAYS_FLASH:
        c_ctx->curr = CL_CAP_STATE_PRE_FALSH_CAPTURE;
        c_ctx->next = CL_CAP_STATE_MAIN_FLASH_CAPTURE;
        break;
    case CAM_AE_MODE_ON_AUTO_FLASH_REDEYE:
        c_ctx->curr = CL_CAP_STATE_RED_EYE_CAPTURE;
        c_ctx->next = CL_CAP_STATE_MAIN_FLASH_CAPTURE;
        break;
    default:
        c_ctx->curr = CL_CAP_STATE_MAIN_CAPTURE;
        break;
    }

   mmsdbg(DL_FUNC, "Exit %s", ret ? "Error" : "Ok");
    return ret;
}

/* ========================================================================== */
/**
* pre_cap_frm_req()
*/
/* ========================================================================== */
static int pre_cap_frm_req(void *cctx, cl_algs_hdl_t *halgs, cl_in_t *in, cl_out_t *out)
{
    cl_cap_stm_ctx_t *c_ctx = cctx;
    int32 ret = 0;
    cam_cl_algo_ctrl_list_t *entry;
    cam_cl_algo_ctrl_t      *fl_algo_req;
    cam_cl_algo_ctrl_t      **fr_req;

    mmsdbg(DL_FUNC, "Enter");

    ret =  osal_sem_wait_timeout(c_ctx->sem_req_done, DEFAULT_ALLOC_TIMEOUT_MS);
    if (ret) {
        mmsdbg(DL_ERROR, "sem_req_done timeout after %d ms", DEFAULT_ALLOC_TIMEOUT_MS);
    }

    if (!c_ctx->flash_seq_num) {
        //TODO
        c_ctx->awb_out = *in->awb_out;
        _capt_req(c_ctx, halgs, in, out);
        _capt_flash_req(c_ctx, halgs, in, out);
    } else {
        _capt_req(c_ctx, halgs, in, out);
    }

    fr_req = c_ctx->out_fl.frame_ctrl[c_ctx->flash_seq_num].algo_flash_ctrl;

    while (*fr_req) {
        fl_algo_req = *fr_req++;

        if (MAX_CL_ALGO_ACA > fl_algo_req->aca_Algo) {
            entry = list_pool_append_element(in->pool_alg_ctrl, out->cl_req_aca);
        } else {
            entry = list_pool_append_element(in->pool_alg_ctrl, out->cl_req_img);
        }

        init_algo_ctrl_entry(&entry->ctrl,
                                fl_algo_req->buff_type,
                                fl_algo_req->aca_Algo,
                                fl_algo_req->nMode,
                                fl_algo_req->nSub_mode,
                                fl_algo_req->processing_flags);
        entry->ctrl.alg_specific = fl_algo_req->alg_specific;

        ((seq_id_t *)&entry->ctrl.id)->state = c_ctx->curr;
        ((seq_id_t *)&entry->ctrl.id)->seq_num = c_ctx->flash_seq_num;
    }

    osal_mutex_lock(c_ctx->events_lock);
    c_ctx->wait_evt_flags =
        c_ctx->out_fl.frame_ctrl[c_ctx->flash_seq_num].end_seq_flags;
    osal_mutex_unlock(c_ctx->events_lock);

    c_ctx->flash_seq_num ++;
    out->seq_status = CL_SEQ_STAT_PROGRESS;

    if (c_ctx->flash_seq_num == c_ctx->out_fl.frames) {
        // sequence end
        out->seq_status = CL_SEQ_STAT_NO_DLEAY;
        c_ctx->flash_seq_num = 0;
        c_ctx->curr = c_ctx->next;
    }

    mmsdbg(DL_FUNC, "Exit %s", ret ? "Error" : "Ok");
    return ret;
}

/* ========================================================================== */
/**
* flash_cap_frm_req()
*/
/* ========================================================================== */
static int flash_cap_frm_req(void *cctx, cl_algs_hdl_t *halgs, cl_in_t *in, cl_out_t *out)
{
    cl_cap_stm_ctx_t *c_ctx = cctx;
    int32 ret = 0;
    cam_cl_algo_ctrl_list_t *entry;
    cam_cl_algo_ctrl_t      *fl_algo_req;
    cam_cl_algo_ctrl_t      **fr_req;

    mmsdbg(DL_FUNC, "Enter");

    ret =  osal_sem_wait_timeout(c_ctx->sem_req_done, DEFAULT_ALLOC_TIMEOUT_MS);
    if (ret) {
        mmsdbg(DL_ERROR, "sem_req_done timeout after %d ms", DEFAULT_ALLOC_TIMEOUT_MS);
    }

    _capt_req(c_ctx, halgs, in, out);
    if (!c_ctx->flash_seq_num) {
        _capt_flash_req(c_ctx, halgs, in, out);
    }

    fr_req = c_ctx->out_fl.frame_ctrl[c_ctx->flash_seq_num].algo_flash_ctrl;

    while (*fr_req) {
        fl_algo_req = *fr_req++;

        if (MAX_CL_ALGO_ACA > fl_algo_req->aca_Algo) {
            entry = list_pool_append_element(in->pool_alg_ctrl, out->cl_req_aca);
        } else {
            entry = list_pool_append_element(in->pool_alg_ctrl, out->cl_req_img);
        }

        init_algo_ctrl_entry(&entry->ctrl,
                                fl_algo_req->buff_type,
                                fl_algo_req->aca_Algo,
                                fl_algo_req->nMode,
                                fl_algo_req->nSub_mode,
                                fl_algo_req->processing_flags);
        entry->ctrl.alg_specific = fl_algo_req->alg_specific;

        if ((CL_ALGO_ZSL_QUEUE == fl_algo_req->img_Algo) &&
                (c_ctx->capt_req.buff_hndl)){
            entry->ctrl.alg_specific.capt_request.buff_hndl =
                    c_ctx->capt_req.buff_hndl;

        }

        ((seq_id_t *)&entry->ctrl.id)->state = c_ctx->curr;
        ((seq_id_t *)&entry->ctrl.id)->seq_num = c_ctx->flash_seq_num;
    }

    osal_mutex_lock(c_ctx->events_lock);
    c_ctx->wait_evt_flags =
        c_ctx->out_fl.frame_ctrl[c_ctx->flash_seq_num].end_seq_flags;
    osal_mutex_unlock(c_ctx->events_lock);

    c_ctx->flash_seq_num ++;
    out->seq_status = CL_SEQ_STAT_PROGRESS;

    if (c_ctx->flash_seq_num == c_ctx->out_fl.frames) {
        // sequence end
        c_ctx->flash_seq_num = 0;
        out->seq_status = CL_SEQ_STAT_NO_DLEAY;
        c_ctx->curr = CL_CAP_STATE_IDLE;
    }

    mmsdbg(DL_FUNC, "Exit %s", ret ? "Error" : "Ok");
    return ret;
}

/* ========================================================================== */
/**
* cap_frm_req()
*/
/* ========================================================================== */
static int main_cap_frm_req(void *cctx, cl_algs_hdl_t *halgs, cl_in_t *in, cl_out_t *out)
{
    cl_cap_stm_ctx_t *c_ctx = cctx;
    int32 ret = 0;
    cam_cl_algo_ctrl_list_t *entry;

    mmsdbg(DL_FUNC, "Enter");

    _capt_req(c_ctx, halgs, in, out);

    { // ISP
        entry = list_pool_append_element(in->pool_alg_ctrl, out->cl_req_img);
        init_algo_ctrl_entry(&entry->ctrl,
                             CL_BUFF_ISP,
                             CL_ALGO_ISP,
                             0,
                             0,
                             CL_FLAG_FORCE_PROCESS);
        entry->ctrl.alg_specific.isp.out_flags = DEF_ZSL_PIPE_CTRL_FLAGS;
    }
    out->seq_status = CL_SEQ_STAT_NO_DLEAY;
    c_ctx->curr = CL_CAP_STATE_IDLE;

    mmsdbg(DL_FUNC, "Exit %s", ret ? "Error" : "Ok");
    return ret;
}

static const p_cl_state_exe_t cap_state_table[MAX_CL_CAP_STATE][MAX_CL_CAP_CMD] = {
    {cap_start, dummy_state},               // CL_CAP_STATE_IDLE
    {dummy_state, pre_cap_frm_req},         // CL_CAP_STATE_RED_EYE_CAPTURE
    {dummy_state, pre_cap_frm_req},         // CL_CAP_STATE_PRE_FALSH_CAPTURE
    {dummy_state, flash_cap_frm_req},       // CL_CAP_STATE_MAIN_FLASH_CAPTURE
    {dummy_state, main_cap_frm_req},        // CL_CAP_STATE_MAIN_CAPTURE
};

/* ========================================================================== */
/**
* cap_exec()
*/
/* ========================================================================== */
static int cap_exec (void *cctx,
                            cl_cap_cmd_t cmd,
                            cl_algs_hdl_t *halgs,
                            cl_in_t *in,
                            cl_out_t *out)
{
    cl_cap_stm_ctx_t *c_ctx = cctx;
    cl_cap_state_t state = c_ctx->curr;

    if ((state >= MAX_CL_CAP_STATE) || (cmd >= MAX_CL_CAP_CMD)) {
        mmsdbg(DL_ERROR, "Out of range parameters");
        return -1;
    }

    return cap_state_table[state][cmd](c_ctx, halgs, in, out);
}

/* ========================================================================== */
/**
* cap_ready()
*/
/* ========================================================================== */
static int cap_ready (void *cctx)
{
    cl_cap_stm_ctx_t *c_ctx = cctx;
    cl_cap_state_t state = c_ctx->curr;

    return (CL_CAP_STATE_IDLE == state) ? 1 : 0;
}

/* ========================================================================== */
/**
* cap_ready()
*/
/* ========================================================================== */
static int cap_config (void *cctx, cl_algs_hdl_t *halgs, cl_cfg_t *cfg)
{
    cl_cap_stm_ctx_t *c_ctx = cctx;
    int ret = 0;

    c_ctx->sga_cfg.dtp_s_common= cfg->dtp_stat;
    c_ctx->sga_cfg.gzz_cfg = &cfg->new_cam_cfg->cam_gzz_cfg;

    if(CL_CAP_STATE_IDLE != c_ctx->curr) {
        ret = sga_cap_configuration(c_ctx->hsg_cap, halgs, &c_ctx->sga_cfg);
    }

    mmsdbg(DL_FUNC, "Exit %s", ret ? "Error" : "Ok");
    return ret;
}

/* ========================================================================== */
/**
* cl_create_capture_stm()
*/
/* ========================================================================== */
int cl_create_capture_stm (cl_stm_t **cap_hdl, cl_capture_create_params_t *params)
{
    *cap_hdl = osal_calloc(1, sizeof(cl_stm_t));
    cl_cap_stm_ctx_t *c_ctx;

    if (!*cap_hdl) {
        goto ERROR_2;
    }

    (*cap_hdl)->private = osal_calloc(1, sizeof(cl_cap_stm_ctx_t));

    if (!(*cap_hdl)->private) {
        goto ERROR_1;
    }

    c_ctx = (*cap_hdl)->private;

    c_ctx->evt_hdl = params->evt_hndl;
    c_ctx->camera_id = params->camera_id;

    if (sga_cap_create(&c_ctx->hsg_cap)){
        goto ERROR_3;
    }

    c_ctx->sem_req_done = osal_sem_create(0);
    if (!c_ctx->sem_req_done) {
        goto ERROR_4;
    }

    c_ctx->events_lock = osal_mutex_create();
    if (!c_ctx->events_lock) {
        goto ERROR_5;
    }

    if (cl_cap_reg_evt(c_ctx)) {
        goto ERROR_6;
    }

    (*cap_hdl)->config = cap_config;
    (*cap_hdl)->exec = cap_exec;
    (*cap_hdl)->ready = cap_ready;

    return 0;

ERROR_6:
    osal_mutex_destroy(c_ctx->events_lock);
ERROR_5:
    osal_sem_destroy(c_ctx->sem_req_done);
ERROR_4:
    sga_cap_destroy(c_ctx->hsg_cap);
ERROR_3:
    osal_free((*cap_hdl)->private);
ERROR_1:
    osal_free(*cap_hdl);
ERROR_2:
    mmsdbg(DL_ERROR, "Can't allocate memory");
    return -1;
}

/* ========================================================================== */
/**
* cl_destroy_capture_stm()
*/
/* ========================================================================== */
int cl_destroy_capture_stm (cl_stm_t *hndl)
{
    cl_cap_stm_ctx_t *c_ctx = hndl->private;

    cl_cap_unreg_evt(c_ctx);
    osal_mutex_destroy(c_ctx->events_lock);
    osal_sem_destroy(c_ctx->sem_req_done);
    sga_cap_destroy(c_ctx->hsg_cap);
    osal_free(hndl->private);
    osal_free(hndl);
    return 0;
}

