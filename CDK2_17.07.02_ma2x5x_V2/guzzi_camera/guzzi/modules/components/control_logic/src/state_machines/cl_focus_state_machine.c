/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file cl_focus_state_machine.c
*
* @author Nayden Kanchev ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 12-Jun-2014 : Nayden Kanchev ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <osal/osal_stdlib.h>
#include <osal/osal_string.h>
#include <utils/mms_debug.h>
#include <error_handle/include/error_handle.h>
#include "cl_preview_stm_types.h"
#include "cl_focus_stm_types.h"
#include "sg_mode_focus_preview.h"
#include <sg_lens.h>

mmsdbg_define_variable(
        vdl_focus_control_logic,
        DL_DEFAULT,
        0,
        "focus.control_logic",
        "FOCUS Control Logic"
    );
#define MMSDEBUGLEVEL mmsdbg_use_variable(vdl_focus_control_logic)

#define CL_AF_WAIT_CONVERGE_FRMS    (5)

typedef struct {
    cl_focus_state_t       curr;
    cl_focus_state_t       next;
    sga_focus_prv_hndl_t   hsg_focus_prv;
    sga_focus_prv_config_t sga_cfg;
    unsigned int          calc_cnt;
    cl_seq_status_t         seq_stat;
} cl_focus_stm_ctx_t;

/* ========================================================================== */
/**
* dummy_state()
*/
/* ========================================================================== */
static int dummy_state (void *nctx, cl_algs_hdl_t *halgs, cl_in_t *in, cl_out_t *out)
{
    mmsdbg(DL_FUNC, "Enter");

    mmsdbg(DL_FUNC, "Exit Ok");
    return 0;
}

/* ========================================================================== */
/**
* focus_satrt()
*/
/* ========================================================================== */
static int focus_start (void *nctx, cl_algs_hdl_t *halgs, cl_in_t *in, cl_out_t *out)
{
    int32               ret = 0;
    cl_focus_stm_ctx_t  *n_ctx = nctx;

    mmsdbg(DL_FUNC, "Enter");

    n_ctx->curr = CL_FOCUS_STATE_STARTUP;
    n_ctx->next = CL_FOCUS_STATE_RUNING;
    ret = sga_focus_prv_configuration(n_ctx->hsg_focus_prv, halgs, &n_ctx->sga_cfg);
    n_ctx->calc_cnt = 0;
    n_ctx->seq_stat = CL_SEQ_STAT_END;

    if (AF_MODE_SINGLE_FOCUS == n_ctx->sga_cfg.gzz_cfg->af_mode.val) {
        sg_lens_init(halgs->sg_lens);
    }

    mmsdbg(DL_FUNC, "Exit %s", ret ? "Error" : "Ok");
    return ret;
}

/* ========================================================================== */
/**
* focus_sart_req()
*/
/* ========================================================================== */
static int focus_start_req (void *nctx, cl_algs_hdl_t *halgs, cl_in_t *in, cl_out_t *out)
{
    cl_focus_stm_ctx_t *n_ctx = nctx;
    sga_focus_prv_input_t  sg_in;
    sga_focus_prv_output_t sg_out;
    cam_cl_algo_ctrl_list_t *entry;
    int32 ret = 0;
    uint32 flags;

    mmsdbg(DL_FUNC, "Enter");

    flags = (in->enb_aca)?(CL_FLAG_FORCE_POST_CALC_EVENT):0;

    sg_in.dtp_d_common  = in->dtp_dyn;
    sg_in.ae_out        = in->ae_out;
    sg_in.ae_stab_out   = in->ae_stab;
    sg_in.awb_out       = in->awb_out;
    sg_in.focus_out     = in->focus;
    sg_in.vsen_modes    = in->vsen_modes;
    sg_in.ae_smooth_mode= SG_AE_SMOOTH_MODE_FAST;
    sg_in.afd_out       = in->afd_out;

    sg_in.ae_distr_mode = SG_AE_DISTR_MODE_PREVIEW;

    sg_out.ae_distr     = &out->sg.ae_distr;
    sg_out.ae_smooth    = &out->sg.ae_smooth;
    sg_out.vpipe        = &out->vpipe;

    ret = sga_focus_prv_process(n_ctx->hsg_focus_prv, halgs, &sg_in, &sg_out);

    mmsdbg(DL_FUNC, "Enter");

    { // ZSL Queue
        entry = list_pool_append_element(in->pool_alg_ctrl, out->cl_req_img);
        init_algo_ctrl_entry(&entry->ctrl,
                             CL_BUFF_ISP,
                             CL_ALGO_ISP,
                             0,
                             0,
                             CL_FLAG_FORCE_PROCESS);
        entry->ctrl.alg_specific.isp.out_flags = DEF_PRV_PIPE_CTRL_FLAGS;
    }

    n_ctx->calc_cnt++;
    if (sg_out.ae_distr->distr_conv || !(CL_AF_WAIT_CONVERGE_FRMS > n_ctx->calc_cnt)) {
        {
/*
            if (sg_out.ae_distr->distr_conv) {
                n_ctx->seq_stat = CL_SEQ_STAT_NO_DLEAY;
             }
*/
            // AF
            entry = list_pool_append_element(in->pool_alg_ctrl, out->cl_req_aca);
            init_algo_ctrl_entry(&entry->ctrl,
                                 CL_BUFF_STATS_AF,
                                 CL_ALGO_CAF,
                                 0,
                                 0,
                                 flags);
        }
        { // FMV
            entry = list_pool_append_element(in->pool_alg_ctrl, out->cl_req_aca);
            init_algo_ctrl_entry(&entry->ctrl,
                                 CL_BUFF_STATS_AEWB,
                                 CL_ALGO_FMV,
                                 0,
                                 0,
                                 flags);
        }
        n_ctx->curr = n_ctx->next;
    }else{
        { // AE
            entry = list_pool_append_element(in->pool_alg_ctrl, out->cl_req_aca);
            init_algo_ctrl_entry(&entry->ctrl,
                                 CL_BUFF_STATS_AEWB,
                                 CL_ALGO_AE,
                                 ACA_AE_STAB_MODE_LOW,
                                 0,
                                 flags);
        }
        { // AWB
            entry = list_pool_append_element(in->pool_alg_ctrl, out->cl_req_aca);
            init_algo_ctrl_entry(&entry->ctrl,
                                 CL_BUFF_STATS_AEWB,
                                 CL_ALGO_AWB,
                                 AWB_CALC_MODE_PREVIEW_FAST,
                                 AWB_CTRL_MODE_NORMAL,
                                 flags);
        }
    }
    out->seq_status =  n_ctx->seq_stat;
    mmsdbg(DL_FUNC, "Exit %s", ret ? "Error" : "Ok");
    return ret;
}

/* ========================================================================== */
/**
* focus_sart_req()
*/
/* ========================================================================== */
static int focus_req (void *nctx, cl_algs_hdl_t *halgs, cl_in_t *in, cl_out_t *out)
{
    int32 ret = 0;
    cl_focus_stm_ctx_t *n_ctx = nctx;
    sga_focus_prv_input_t  sg_in;
    sga_focus_prv_output_t sg_out;
    cam_cl_algo_ctrl_list_t *entry;
    uint32 flags;

    mmsdbg(DL_FUNC, "Enter");

    flags = (in->enb_aca)?(CL_FLAG_FORCE_POST_CALC_EVENT):0;

    sg_in.dtp_d_common  = in->dtp_dyn;
    sg_in.ae_out        = in->ae_out;
    sg_in.ae_stab_out   = in->ae_stab;
    sg_in.awb_out       = in->awb_out;
    sg_in.focus_out     = in->focus;
    sg_in.vsen_modes    = in->vsen_modes;
    sg_in.ae_smooth_mode= SG_AE_SMOOTH_MODE_FAST;
    sg_in.afd_out       = in->afd_out;

    sg_in.ae_distr_mode = SG_AE_DISTR_MODE_PREVIEW;

    sg_out.ae_distr     = &out->sg.ae_distr;
    sg_out.ae_smooth    = &out->sg.ae_smooth;
    sg_out.vpipe        = &out->vpipe;

    sga_focus_prv_process(n_ctx->hsg_focus_prv, halgs, &sg_in, &sg_out);


    mmsdbg(DL_FUNC, "Enter");

    { // AF
        entry = list_pool_append_element(in->pool_alg_ctrl, out->cl_req_aca);
        init_algo_ctrl_entry(&entry->ctrl,
                             CL_BUFF_STATS_AF,
                             CL_ALGO_CAF,
                             0,
                             0,
                             flags);
    }
    { // FMV
        entry = list_pool_append_element(in->pool_alg_ctrl, out->cl_req_aca);
        init_algo_ctrl_entry(&entry->ctrl,
                             CL_BUFF_STATS_AEWB,
                             CL_ALGO_FMV,
                             0,
                             0,
                             flags);
    }
    { // ZSL Queue
        entry = list_pool_append_element(in->pool_alg_ctrl, out->cl_req_img);
        init_algo_ctrl_entry(&entry->ctrl,
                             CL_BUFF_ISP,
                             CL_ALGO_ISP,
                             0,
                             0,
                             flags);
        entry->ctrl.alg_specific.isp.out_flags = DEF_PRV_PIPE_CTRL_FLAGS;
    }

    out->seq_status =  n_ctx->seq_stat;
    mmsdbg(DL_FUNC, "Exit %s", ret ? "Error" : "Ok");
    return ret;
}

/* ========================================================================== */
/**
* focus_abort()
*/
/* ========================================================================== */
static int focus_abort (void *nctx, cl_algs_hdl_t *halgs, cl_in_t *in, cl_out_t *out)
{
    cl_focus_stm_ctx_t *n_ctx = nctx;
    int32 ret = 0;

    mmsdbg(DL_FUNC, "Enter");

    n_ctx->curr = CL_FOCUS_STATE_IDLE;

    mmsdbg(DL_FUNC, "Exit %s", ret ? "Error" : "Ok");
    return ret;
}

/* ========================================================================== */
/**
* focus_abort_req_frm()
*/
/* ========================================================================== */
static int focus_abort_req (void *nctx, cl_algs_hdl_t *halgs, cl_in_t *in, cl_out_t *out)
{
    cl_focus_stm_ctx_t *n_ctx = nctx;
    int32 ret = 0;

    mmsdbg(DL_FUNC, "Enter");

    n_ctx->curr = n_ctx->next;

    mmsdbg(DL_FUNC, "Exit %s", ret ? "Error" : "Ok");
    return ret;
}

static const p_cl_state_exe_t focus_state_table[MAX_CL_FOCUS_STATE][MAX_CL_FOCUS_CMD] = {
    {focus_start, focus_start_req, focus_abort},      //IDLE,
    {dummy_state, focus_start_req, focus_abort},    //STARTUP,
    {dummy_state, focus_req, focus_abort},          //RUNING,
    {dummy_state, focus_abort_req, focus_abort},    //ABORTING,
};

/* ========================================================================== */
/**
* focus_exec()
*/
/* ========================================================================== */
static int focus_exec (void *nctx,
                            cl_focus_cmd_t cmd,
                            cl_algs_hdl_t *halgs,
                            cl_in_t *in,
                            cl_out_t *out)
{
    cl_focus_stm_ctx_t *n_ctx = nctx;
    cl_focus_state_t state = n_ctx->curr;

    if ((state >= MAX_CL_FOCUS_STATE) || (cmd >= MAX_CL_FOCUS_CMD)) {
        mmsdbg(DL_ERROR, "Out of range parameters");
        return -1;
    }

    return focus_state_table[state][cmd](n_ctx, halgs, in, out);
}

/* ========================================================================== */
/**
* focus_ready()
*/
/* ========================================================================== */
static int focus_ready (void *nctx)
{

    cl_focus_stm_ctx_t *n_ctx = nctx;
    cl_focus_state_t state = n_ctx->curr;

    return (CL_FOCUS_STATE_IDLE == state) ? 1 : 0;
}

/* ========================================================================== */
/**
* focus_ready()
*/
/* ========================================================================== */
static int focus_config (void *nctx, cl_algs_hdl_t *halgs, cl_cfg_t *cfg)
{
    cl_focus_stm_ctx_t *n_ctx = nctx;
    int ret = 0;

    n_ctx->sga_cfg.dtp_s_common= cfg->dtp_stat;
    n_ctx->sga_cfg.gzz_cfg     = &cfg->new_cam_cfg->cam_gzz_cfg;

    if (CL_FOCUS_STATE_IDLE != n_ctx->curr) {
        sga_focus_prv_configuration(n_ctx->hsg_focus_prv, halgs, &n_ctx->sga_cfg);
    }

    mmsdbg(DL_FUNC, "Exit %s", ret ? "Error" : "Ok");
    return ret;
}

/* ========================================================================== */
/**
* cl_create_focus_stm()
*/
/* ========================================================================== */
int cl_create_focus_stm (cl_stm_t **focus_hdl)
{
    cl_focus_stm_ctx_t *n_ctx;
    *focus_hdl = osal_calloc(1, sizeof(cl_stm_t));

    if (!*focus_hdl) {
        goto ERROR_1;
    }

    (*focus_hdl)->private = osal_calloc(1, sizeof(cl_focus_stm_ctx_t));

    if (!(*focus_hdl)->private) {
        goto ERROR_2;
    }

    n_ctx = (*focus_hdl)->private;
    if (sga_focus_prv_create(&n_ctx->hsg_focus_prv)) {
        goto ERROR_3;
    }

    (*focus_hdl)->config = focus_config;
    (*focus_hdl)->exec = focus_exec;
    (*focus_hdl)->ready = focus_ready;

    return 0;

ERROR_3:
    osal_free(n_ctx);
ERROR_2:
    osal_free(*focus_hdl);
ERROR_1:
    mmsdbg(DL_ERROR, "Can't allocate memory");
    return -1;
}


/* ========================================================================== */
/**
* cl_destroy_focus_stm()
*/
/* ========================================================================== */
int cl_destroy_focus_stm (cl_stm_t *hndl)
{
    cl_focus_stm_ctx_t *n_ctx = hndl->private;

    sga_focus_prv_destroy(n_ctx->hsg_focus_prv);
    osal_free(hndl->private);
    osal_free(hndl);
    return 0;
}

