/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file cl_system_state_machine.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <osal/osal_stdlib.h>
#include <utils/mms_debug.h>
#include <error_handle/include/error_handle.h>
#include "cl_system_stm_types.h"
#include "cl_preview_stm_types.h"
#include "cl_capture_stm_types.h"
#include "cl_zsl_stm_types.h"
#include "cl_custom_stm_types.h"

mmsdbg_define_variable(
        vdl_system_control_logic,
        DL_DEFAULT,
        0,
        "sys.control_logic",
        "SYS Control Logic"
    );
#define MMSDEBUGLEVEL mmsdbg_use_variable(vdl_system_control_logic)

#define AFD_START_UP_DELAY  (100)
#define AFD_RESTART_DELAY   (10000)
#define AFD_TRY_CNT         (4)

typedef enum {
    CL_SYS_STAT_ALG_IDLE,
    CL_SYS_STAT_ALG_STARTING,
    CL_SYS_STAT_ALG_RUNNING,
    CL_SYS_STAT_ALG_PENDING,
} cl_sys_stat_alg_t;

typedef struct {
        uint8               afd_initial_run;
        uint8               afd_try_cnt;
        uint32              afd_iteration;
        cl_sys_stat_alg_t   state;
} afd_start_up_ctx_t;

typedef struct {
    cl_sys_state_t  curr;
    cl_sys_state_t  next;
    cl_stm_t    *hndl_prv;
    cl_stm_t    *hndl_cap;
    cl_stm_t    *hndl_zsl;
    cl_stm_t    *hndl_custom;
    gzz_flicker_mode_t  flicker_mode;
    focus_mode_t        focus_mode;
    gzz_af_trigger_t    af_trigger;
    cl_sys_stat_alg_t   foc_state;
    afd_start_up_ctx_t  afd_ctx;
} cl_sys_stm_ctx_t;

/* ========================================================================== */
/**
* do_afd_need_run()
*/
/* ========================================================================== */
static void do_afd_need_run (void *sctx, cl_algs_hdl_t *halgs, cl_in_t *in, cl_out_t *out)
{
    cl_sys_stm_ctx_t *sys_ctx = sctx;
    cl_stm_t *prv = sys_ctx->hndl_prv;


    // Do not start AFD first AFD_START_UP_DELAY frames or Focus is running
    if ((sys_ctx->afd_ctx.afd_iteration++ < AFD_START_UP_DELAY ||
            (AF_STATE_RUNNING == in->focus->status)) &&
            (CL_SYS_STAT_ALG_IDLE == sys_ctx->afd_ctx.state)) {
        return;
    }

    if ((CL_SYS_STAT_ALG_STARTING == sys_ctx->afd_ctx.state) &&
            (FLICKER_IN_PROGRESS == in->afd_out->status)) {
        // wait for results
        sys_ctx->afd_ctx.state = CL_SYS_STAT_ALG_RUNNING;
    }

    // After first flicker process Reinit flicker after AFD_RESTART_DELAY frames
    if ((sys_ctx->afd_ctx.afd_iteration > AFD_RESTART_DELAY) &&
            (CL_SYS_STAT_ALG_IDLE == sys_ctx->afd_ctx.state))
    {
        sys_ctx->afd_ctx.afd_try_cnt = 0;
        sys_ctx->afd_ctx.afd_iteration = 0;
        sys_ctx->afd_ctx.state = CL_SYS_STAT_ALG_PENDING;
    }

    // Switch to AFD state machine if this is initial RUN
    if ((sys_ctx->afd_ctx.afd_iteration > AFD_START_UP_DELAY)
         && (!sys_ctx->afd_ctx.afd_initial_run)
         && (AF_STATE_RUNNING != in->focus->status)
         && (CAM_FLICKER_MODE_AUTO == sys_ctx->flicker_mode)
         && (CL_SYS_STAT_ALG_IDLE == sys_ctx->afd_ctx.state)) {
            sys_ctx->afd_ctx.afd_iteration = 0;
            sys_ctx->afd_ctx.afd_try_cnt = 0;
            sys_ctx->afd_ctx.afd_initial_run = 1;
            sys_ctx->afd_ctx.state = CL_SYS_STAT_ALG_PENDING;
    }

    if ((CL_SYS_STAT_ALG_RUNNING == sys_ctx->afd_ctx.state) &&
            (FLICKER_IN_PROGRESS != in->afd_out->status)) {

        sys_ctx->afd_ctx.state = CL_SYS_STAT_ALG_IDLE;
        if (((FLICKER_NO_DETECT == in->afd_out->status) ||
                (FLICKER_ERROR == in->afd_out->status)) &&
                (sys_ctx->afd_ctx.afd_try_cnt < AFD_TRY_CNT)) {
            // Restart AFD state machine
            sys_ctx->afd_ctx.state = CL_SYS_STAT_ALG_PENDING;
        } else {
            prv->exec(prv->private, CL_PRV_CMD_NORMAL_START, halgs, in, out);
        }
    }

    if (sys_ctx->afd_ctx.state == CL_SYS_STAT_ALG_PENDING) {
        sys_ctx->afd_ctx.afd_try_cnt ++;
        sys_ctx->afd_ctx.state = CL_SYS_STAT_ALG_STARTING;
        prv->exec(prv->private, CL_PRV_CMD_AFD_START, halgs, in, out);
    }
}

/* ========================================================================== */
/**
* do_af_need_run()
*/
/* ========================================================================== */
static void do_af_need_run (void *sctx, cl_algs_hdl_t *halgs, cl_in_t *in, cl_out_t *out)
{
    cl_sys_stm_ctx_t *sys_ctx = sctx;
    cl_stm_t *prv = sys_ctx->hndl_prv;

    if(AF_MODE_CONTINUOUS == sys_ctx->focus_mode) {
        // Switch to focus state machine if focus is enabled and it requires processing data
        if ((AF_STATE_RUNNING == in->focus->status) &&
                (CL_SYS_STAT_ALG_IDLE == sys_ctx->foc_state)) {
            sys_ctx->foc_state = CL_SYS_STAT_ALG_RUNNING;
            prv->exec(prv->private, CL_PRV_CMD_AF_START, halgs, in, out);
        }

        // Switch to normal state machine if focus is ready
        if ((AF_STATE_RUNNING != in->focus->status) &&
                (CL_SYS_STAT_ALG_RUNNING == sys_ctx->foc_state)) {
            sys_ctx->foc_state = CL_SYS_STAT_ALG_IDLE;
            prv->exec(prv->private, CL_PRV_CMD_NORMAL_START, halgs, in, out);
        }
    }

    if (AF_MODE_SINGLE_FOCUS == sys_ctx->focus_mode) {
        if ((CAM_AF_TRIGGER_START == sys_ctx->af_trigger)/* &&
                (CL_SYS_STAT_ALG_IDLE == sys_ctx->foc_state)*/) {
            //Clear trigger flag
            sys_ctx->af_trigger = CAM_AF_TRIGGER_IDLE;
            sys_ctx->foc_state = CL_SYS_STAT_ALG_PENDING;
            prv->exec(prv->private, CL_PRV_CMD_AF_START, halgs, in, out);
        }

        // wait single focus to start
        if ((CL_SYS_STAT_ALG_PENDING == sys_ctx->foc_state) &&
                (AF_STATE_RUNNING == in->focus->status)) {
            sys_ctx->foc_state = CL_SYS_STAT_ALG_RUNNING;
        }

        // Switch to normal state machine if focus is ready
        if (((in->focus->status == AF_STATE_FAIL) || (in->focus->status == AF_STATE_SUCCESS)) &&
                CL_SYS_STAT_ALG_RUNNING == sys_ctx->foc_state) {
            sys_ctx->foc_state = CL_SYS_STAT_ALG_IDLE;
            prv->exec(prv->private, CL_PRV_CMD_NORMAL_START, halgs, in, out);
        }
    }

    if (AF_MODE_OFF == sys_ctx->focus_mode) {
        //Clear trigger flag - do nothing
        sys_ctx->af_trigger = CAM_AF_TRIGGER_IDLE;
    }
}

/* ========================================================================== */
/**
* dummy_state()
*/
/* ========================================================================== */
static int dummy_state (void *sctx, cl_algs_hdl_t *halgs, cl_in_t *in, cl_out_t *out)
{
    mmsdbg(DL_FUNC, "Enter");

    mmsdbg(DL_FUNC, "Exit Ok");
    return 0;
}

/* ========================================================================== */
/**
* to_preview()
*/
/* ========================================================================== */
static int to_preview (void *sctx, cl_algs_hdl_t *halgs, cl_in_t *in, cl_out_t *out)
{
    int32 ret = -1;
    cl_sys_stm_ctx_t *sys_ctx = sctx;
    cl_stm_t *prv = sys_ctx->hndl_prv;

    mmsdbg(DL_FUNC, "Enter");

    if (!prv->exec(prv->private, CL_PRV_CMD_NORMAL_START, halgs, in, out)) {
        ret = 0;
        sys_ctx->curr = CL_SYS_STATE_PREVIEW;
    }

    mmsdbg(DL_FUNC, "Exit %s", ret ? "Error" : "Ok");
    return ret;
}

/* ========================================================================== */
/**
* to_capture()
*/
/* ========================================================================== */
static int to_capture (void *sctx, cl_algs_hdl_t *halgs, cl_in_t *in, cl_out_t *out)
{
    int32 ret = -1;
    cl_sys_stm_ctx_t *sys_ctx = sctx;
    cl_stm_t *cap = sys_ctx->hndl_cap;

    mmsdbg(DL_FUNC, "Enter");

    if (!cap->exec(cap->private, CL_CAP_CMD_SATRT, halgs, in, out)) {
        ret = 0;
        sys_ctx->curr = CL_SYS_STATE_CAPTURE;
    }

    mmsdbg(DL_FUNC, "Exit %s", ret ? "Error" : "Ok");
    return ret;
}

/* ========================================================================== */
/**
* to_bracketing()
*/
/* ========================================================================== */
static int to_bracketing (void *sctx, cl_algs_hdl_t *halgs, cl_in_t *in, cl_out_t *out)
{
    int32 ret = -1;
    cl_sys_stm_ctx_t *sys_ctx = sctx;

    mmsdbg(DL_FUNC, "Enter");

    sys_ctx->next = CL_SYS_STATE_BRACKETING;

    mmsdbg(DL_FUNC, "Exit %s", ret ? "Error" : "Ok");
    return ret;
}

/* ========================================================================== */
/**
* abort_prv()
*/
/* ========================================================================== */
static int abort_prv (void *sctx, cl_algs_hdl_t *halgs, cl_in_t *in, cl_out_t *out)
{
    int32 ret = -1;
    cl_sys_stm_ctx_t *sys_ctx = sctx;
    cl_stm_t *prv = sys_ctx->hndl_prv;

    mmsdbg(DL_FUNC, "Enter");

    if (!prv->exec(prv->private, CL_PRV_CMD_ABORT, halgs, in, out)) {
        ret = 0;
        sys_ctx->curr = CL_SYS_STATE_IDLE;
    }

    mmsdbg(DL_FUNC, "Exit %s", ret ? "Error" : "Ok");
    return ret;
}

/* ========================================================================== */
/**
* prv_to_cap()
*/
/* ========================================================================== */
static int prv_to_cap (void *sctx, cl_algs_hdl_t *halgs, cl_in_t *in, cl_out_t *out)
{
    int32 ret = -1;
    cl_sys_stm_ctx_t *sys_ctx = sctx;
    cl_stm_t *cap = sys_ctx->hndl_cap;

    mmsdbg(DL_FUNC, "Enter");

    if (!cap->exec(cap->private, CL_CAP_CMD_SATRT, halgs, in, out)) {
        ret = 0;
        sys_ctx->curr = CL_SYS_STATE_CAPTURE;
        sys_ctx->next = CL_SYS_STATE_PREVIEW;
    }

    mmsdbg(DL_FUNC, "Exit %s", ret ? "Error" : "Ok");
    return ret;
}


/* ========================================================================== */
/**
* prv_to_zsl()
*/
/* ========================================================================== */
static int prv_to_zsl (void *sctx, cl_algs_hdl_t *halgs, cl_in_t *in, cl_out_t *out)
{
    int32 ret = -1;
    cl_sys_stm_ctx_t *sys_ctx = sctx;
    cl_stm_t *zsl = sys_ctx->hndl_zsl;

    mmsdbg(DL_FUNC, "Enter");

    if (!zsl->exec(zsl->private, CL_ZSL_CMD_SATRT, halgs, in, out)) {
        ret = 0;
        sys_ctx->curr = CL_SYS_STATE_ZSL;
        sys_ctx->next = CL_SYS_STATE_PREVIEW;
    }

    mmsdbg(DL_FUNC, "Exit %s", ret ? "Error" : "Ok");
    return ret;
}

/* ========================================================================== */
/**
* prv_to_brack()
*/
/* ========================================================================== */
static int prv_to_brack (void *sctx, cl_algs_hdl_t *halgs, cl_in_t *in, cl_out_t *out)
{
    int32 ret = -1;
    cl_sys_stm_ctx_t *sys_ctx = sctx;
    cl_stm_t *prv = sys_ctx->hndl_prv;

    mmsdbg(DL_FUNC, "Enter");

    if (!prv->exec(prv->private, CL_PRV_CMD_ABORT, halgs, in, out)) {
        ret = 0;
        sys_ctx->next = CL_SYS_STATE_BRACKETING;
    }

    mmsdbg(DL_FUNC, "Exit %s", ret ? "Error" : "Ok");
    return ret;
}

/* ========================================================================== */
/**
* abort_cap()
*/
/* ========================================================================== */
static int abort_cap (void *sctx, cl_algs_hdl_t *halgs, cl_in_t *in, cl_out_t *out)
{
    int32 ret = -1;
    cl_sys_stm_ctx_t *sys_ctx = sctx;

    mmsdbg(DL_FUNC, "Enter");

    sys_ctx->curr = CL_SYS_STATE_IDLE;

    mmsdbg(DL_FUNC, "Exit %s", ret ? "Error" : "Ok");
    return ret;
}

/* ========================================================================== */
/**
* cap_to_prv()
*/
/* ========================================================================== */
static int cap_to_prv (void *sctx, cl_algs_hdl_t *halgs, cl_in_t *in, cl_out_t *out)
{
    int32 ret = -1;
    cl_sys_stm_ctx_t *sys_ctx = sctx;

    mmsdbg(DL_FUNC, "Enter");

    sys_ctx->next = CL_SYS_STATE_PREVIEW;

    mmsdbg(DL_FUNC, "Exit %s", ret ? "Error" : "Ok");
    return ret;
}

/* ========================================================================== */
/**
* cap_to_brack()
*/
/* ========================================================================== */
static int cap_to_brack (void *sctx, cl_algs_hdl_t *halgs, cl_in_t *in, cl_out_t *out)
{
    int32 ret = -1;
    cl_sys_stm_ctx_t *sys_ctx = sctx;

    mmsdbg(DL_FUNC, "Enter");

    sys_ctx->next = CL_SYS_STATE_BRACKETING;

    mmsdbg(DL_FUNC, "Exit %s", ret ? "Error" : "Ok");
    return ret;
}

/* ========================================================================== */
/**
* abort_brack()
*/
/* ========================================================================== */
static int abort_brack (void *sctx, cl_algs_hdl_t *halgs, cl_in_t *in, cl_out_t *out)
{
    int32 ret = -1;
    cl_sys_stm_ctx_t *sys_ctx = sctx;

    mmsdbg(DL_FUNC, "Enter");

    sys_ctx->curr = CL_SYS_STATE_IDLE;

    mmsdbg(DL_FUNC, "Exit %s", ret ? "Error" : "Ok");
    return ret;
}

/* ========================================================================== */
/**
* brack_to_prv()
*/
/* ========================================================================== */
static int brack_to_prv (void *sctx, cl_algs_hdl_t *halgs, cl_in_t *in, cl_out_t *out)
{
    int32 ret = -1;
    cl_sys_stm_ctx_t *sys_ctx = sctx;

    mmsdbg(DL_FUNC, "Enter");

    sys_ctx->next = CL_SYS_STATE_PREVIEW;

    mmsdbg(DL_FUNC, "Exit %s", ret ? "Error" : "Ok");
    return ret;
}

/* ========================================================================== */
/**
* brack_to_cap()
*/
/* ========================================================================== */
static int brack_to_cap (void *sctx, cl_algs_hdl_t *halgs, cl_in_t *in, cl_out_t *out)
{
    int32 ret = -1;
    cl_sys_stm_ctx_t *sys_ctx = sctx;

    mmsdbg(DL_FUNC, "Enter");

    sys_ctx->next = CL_SYS_STATE_CAPTURE;

    mmsdbg(DL_FUNC, "Exit %s", ret ? "Error" : "Ok");
    return ret;
}

/* ========================================================================== */
/**
* prv_frame_req()
*/
/* ========================================================================== */
static int prv_frame_req (void *sctx, cl_algs_hdl_t *halgs, cl_in_t *in, cl_out_t *out)
{
    int32 ret = -1;
    cl_sys_stm_ctx_t *sys_ctx = sctx;
    cl_stm_t *prv = sys_ctx->hndl_prv;

    mmsdbg(DL_FUNC, "Enter");

    do_af_need_run(sctx, halgs, in, out);
    do_afd_need_run(sctx, halgs, in, out);

    if (!prv->exec(prv->private, CL_PRV_CMD_FRAME_REQUEST, halgs, in, out)) {
        ret = 0;
        if (prv->ready(prv->private)) {
            sys_ctx->curr = sys_ctx->next;
        }
    }

    mmsdbg(DL_FUNC, "Exit %s", ret ? "Error" : "Ok");
    return ret;
}

/* ========================================================================== */
/**
* cap_frame_req()
*/
/* ========================================================================== */
static int cap_frame_req (void *sctx, cl_algs_hdl_t *halgs, cl_in_t *in, cl_out_t *out)
{
    int32 ret = -1;
    cl_sys_stm_ctx_t *sys_ctx = sctx;
    cl_stm_t *cap = sys_ctx->hndl_cap;

    mmsdbg(DL_FUNC, "Enter");

    if (!cap->exec(cap->private, CL_CAP_CMD_FRAME_REQUEST, halgs, in, out)) {
        ret = 0;
        if (cap->ready(cap->private)) {
            sys_ctx->curr = sys_ctx->next;
        }
    }

    mmsdbg(DL_FUNC, "Exit %s", ret ? "Error" : "Ok");
    return ret;
}

/* ========================================================================== */
/**
* brack_frame_req()
*/
/* ========================================================================== */
static int brack_frame_req (void *sctx, cl_algs_hdl_t *halgs, cl_in_t *in, cl_out_t *out)
{
    int32 ret = -1;
    mmsdbg(DL_FUNC, "Enter");


    mmsdbg(DL_FUNC, "Exit %s", ret ? "Error" : "Ok");
    return ret;
}

/* ========================================================================== */
/**
* idle_frame_req()
*/
/* ========================================================================== */
static int idle_frame_req (void *sctx, cl_algs_hdl_t *halgs, cl_in_t *in, cl_out_t *out)
{
    int32 ret = 0;
    cl_sys_stm_ctx_t *sys_ctx = sctx;

    mmsdbg(DL_FUNC, "Enter");

    sys_ctx->curr = sys_ctx->next;

    mmsdbg(DL_FUNC, "Exit %s", ret ? "Error" : "Ok");
    return ret;
}

/* ========================================================================== */
/**
* zsl_frame_req()
*/
/* ========================================================================== */
static int zsl_frame_req (void *sctx, cl_algs_hdl_t *halgs, cl_in_t *in, cl_out_t *out)
{
    int32 ret = -1;
    cl_sys_stm_ctx_t *sys_ctx = sctx;
    cl_stm_t *zsl = sys_ctx->hndl_zsl;

    mmsdbg(DL_FUNC, "Enter");

    if (!zsl->exec(zsl->private, CL_ZSL_CMD_FRAME_REQUEST, halgs, in, out)) {
        ret = 0;
        if (zsl->ready(zsl->private)) {
            sys_ctx->curr = sys_ctx->next;
        }
    }

    mmsdbg(DL_FUNC, "Exit %s", ret ? "Error" : "Ok");
    return ret;
}

/* ========================================================================== */
/**
* to_custom()
*/
/* ========================================================================== */
static int to_custom (void *sctx, cl_algs_hdl_t *halgs, cl_in_t *in, cl_out_t *out)
{
    int32 ret = -1;
    cl_sys_stm_ctx_t *sys_ctx = sctx;
    cl_stm_t *custom = sys_ctx->hndl_custom;

    mmsdbg(DL_FUNC, "Enter");

    if (!custom->exec(custom->private, CL_CUSTOM_CMD_SATRT, halgs, in, out)) {
        ret = 0;
        sys_ctx->curr = CL_SYS_STATE_CUSTOM;
    }

    mmsdbg(DL_FUNC, "Exit %s", ret ? "Error" : "Ok");
    return ret;
}

/* ========================================================================== */
/**
* custom_frame_req()
*/
/* ========================================================================== */
static int custom_frame_req (void *sctx, cl_algs_hdl_t *halgs, cl_in_t *in, cl_out_t *out)
{
    int32 ret = -1;
    cl_sys_stm_ctx_t *sys_ctx = sctx;
    cl_stm_t *custom = sys_ctx->hndl_custom;

    mmsdbg(DL_FUNC, "Enter");

    if (!custom->exec(custom->private, CL_CUSTOM_CMD_REQUEST, halgs, in, out)) {
        ret = 0;
        if (custom->ready(custom->private)) {
            sys_ctx->curr = sys_ctx->next;
        }
    }

    mmsdbg(DL_FUNC, "Exit %s", ret ? "Error" : "Ok");
    return ret;
}

static const p_cl_state_exe_t sys_state_table[MAX_CL_SYS_STATE][MAX_CL_SYS_CMD] = {
    {dummy_state, to_preview,   to_capture,   to_bracketing, dummy_state, idle_frame_req,   to_custom},    // CL_SYS_STATE_IDLE
    {abort_prv,   dummy_state,  prv_to_cap,   prv_to_brack,  prv_to_zsl,  prv_frame_req,    to_custom},    // CL_SYS_STATE_PREVIEW
    {abort_cap,   cap_to_prv,   dummy_state,  cap_to_brack,  dummy_state, cap_frame_req,    to_custom},    // CL_SYS_STATE_CAPTURE
    {abort_brack, brack_to_prv, brack_to_cap, dummy_state,   dummy_state, brack_frame_req,  to_custom},    // CL_SYS_STATE_BRACKETING
    {dummy_state, dummy_state,  dummy_state,  dummy_state,   dummy_state, zsl_frame_req,    to_custom},    // CL_SYS_STATE_ZSL
    {dummy_state, dummy_state,  dummy_state,  dummy_state,   dummy_state, custom_frame_req, dummy_state},  // CL_SYS_STATE_CUSTOM
    // abort        preview        capture     bracketing       ZSL          frame_req        custom
};

/* ========================================================================== */
/**
* sys_exec()
*/
/* ========================================================================== */
static int sys_exec (void *sctx,
                            cl_sys_cmd_t cmd,
                            cl_algs_hdl_t *halgs,
                            cl_in_t *in,
                            cl_out_t *out)
{
    cl_sys_stm_ctx_t *sys_ctx = sctx;
    cl_sys_state_t state = sys_ctx->curr;

    if ((state >= MAX_CL_SYS_STATE) || (cmd >= MAX_CL_SYS_CMD)) {
        mmsdbg(DL_ERROR, "Out of range parameters");
        return -1;
    }

    return sys_state_table[state][cmd](sys_ctx, halgs, in, out);
}

/* ========================================================================== */
/**
* sys_ready()
*/
/* ========================================================================== */
static int sys_ready (void *sctx)
{
    cl_sys_stm_ctx_t *sys_ctx = sctx;
    cl_sys_state_t state = sys_ctx->curr;

    return (CL_SYS_STATE_IDLE == state) ? 1 : 0;
}

/* ========================================================================== */
/**
* sys_ready()
*/
/* ========================================================================== */
static int sys_config (void *sctx, cl_algs_hdl_t *halgs, cl_cfg_t *cfg)
{
    cl_sys_stm_ctx_t *sys_ctx = sctx;
    cl_stm_t *prv = sys_ctx->hndl_prv;
    cl_stm_t *cap = sys_ctx->hndl_cap;
    cl_stm_t *zsl = sys_ctx->hndl_zsl;
    cl_stm_t *custom = sys_ctx->hndl_custom;

    int ret = 0;

    if (is_gzz_cfg_changed(cfg->cur_cam_cfg,
            cfg->new_cam_cfg, cam_gzz_cfg.af_trigger)) {
        sys_ctx->af_trigger =
                cfg->new_cam_cfg->cam_gzz_cfg.af_trigger.val;
    }
    if (is_gzz_cfg_changed(cfg->cur_cam_cfg,
                                cfg->new_cam_cfg,
                                cam_gzz_cfg.flicker_mode)) {
        sys_ctx->flicker_mode =
                cfg->new_cam_cfg->cam_gzz_cfg.flicker_mode.val;
    }

    if (is_gzz_cfg_changed(cfg->cur_cam_cfg,
                                cfg->new_cam_cfg,
                                cam_gzz_cfg.af_mode)) {
        sys_ctx->focus_mode =
                cfg->new_cam_cfg->cam_gzz_cfg.af_mode.val;
    }

    ret = cap->config(cap->private, halgs, cfg);
    ret += zsl->config(zsl->private, halgs, cfg);
    ret += prv->config(prv->private, halgs, cfg);
    ret += custom->config(custom->private, halgs, cfg);

    mmsdbg(DL_FUNC, "Exit %s", ret ? "Error" : "Ok");
    return ret;
}

/* ========================================================================== */
/**
* cl_create_system_stm()
*/
/* ========================================================================== */
int cl_create_system_stm (cl_stm_t **sys_hdl, cl_system_create_params_t *params)
{
    int ret = -1;
    cl_sys_stm_ctx_t *sys_ctx;
    cl_capture_create_params_t capture_params;
    cl_zsl_create_params_t zsl_params;

    *sys_hdl = osal_calloc(1, sizeof(cl_stm_t));

    if (!*sys_hdl) {
        goto ERROR_2;
    }

    (*sys_hdl)->private = osal_calloc(1, sizeof(cl_sys_stm_ctx_t));

    if (!(*sys_hdl)->private) {
        goto ERROR_1;
    }

    (*sys_hdl)->config = sys_config;
    (*sys_hdl)->exec = sys_exec;
    (*sys_hdl)->ready = sys_ready;

    capture_params.evt_hndl = params->evt_hndl;
    capture_params.camera_id = params->camera_id;

    zsl_params.evt_hndl = params->evt_hndl;
    zsl_params.camera_id = params->camera_id;
    zsl_params.camera_alt_id = params->camera_id;

    sys_ctx = (*sys_hdl)->private;
    ret = cl_create_preview_stm (&sys_ctx->hndl_prv);
    ret += cl_create_capture_stm (&sys_ctx->hndl_cap, &capture_params);
    ret += cl_create_zsl_stm (&sys_ctx->hndl_zsl, &zsl_params);
    ret += cl_create_custom_stm (&sys_ctx->hndl_custom);

    return ret;

ERROR_1:
    osal_free(*sys_hdl);
ERROR_2:
    mmsdbg(DL_ERROR, "Can't allocate memory");
    return ret;
}


/* ========================================================================== */
/**
* cl_destroy_system_stm()
*/
/* ========================================================================== */
int cl_destroy_system_stm (cl_stm_t *hndl)
{
    cl_sys_stm_ctx_t *sys_ctx = hndl->private;

    cl_destroy_preview_stm (sys_ctx->hndl_prv);
    cl_destroy_capture_stm (sys_ctx->hndl_cap);
    cl_destroy_zsl_stm (sys_ctx->hndl_zsl);
    cl_destroy_custom_stm (sys_ctx->hndl_custom);

    osal_free(hndl->private);
    osal_free(hndl);
    return 0;
}

