/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file cl_afd_state_machine.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <osal/osal_stdlib.h>
#include <osal/osal_string.h>
#include <utils/mms_debug.h>
#include <error_handle/include/error_handle.h>
#include "cl_preview_stm_types.h"
#include "cl_afd_stm_types.h"
#include "sg_mode_afd_preview.h"

mmsdbg_define_variable(
        vdl_afd_control_logic,
        DL_DEFAULT,
        0,
        "afd.control_logic",
        "AFD Control Logic"
    );
#define MMSDEBUGLEVEL mmsdbg_use_variable(vdl_afd_control_logic)

typedef struct {
    cl_afd_state_t          curr;
    cl_afd_state_t          next;
    sga_afd_prv_hndl_t      hsg_afd_prv;
    sga_afd_prv_config_t    sga_cfg;
    sg_afd_distr_modes_t    dist_mode;
    uint32                  calc_cnt;
    ae_stab_output_t        ae_stab;
} cl_afd_stm_ctx_t;

/* ========================================================================== */
/**
* dummy_state()
*/
/* ========================================================================== */
static int dummy_state (void *nctx, cl_algs_hdl_t *halgs, cl_in_t *in, cl_out_t *out)
{
    mmsdbg(DL_FUNC, "Enter");

    mmsdbg(DL_FUNC, "Exit Ok");
    return 0;
}

/* ========================================================================== */
/**
* afd_satrt()
*/
/* ========================================================================== */
static int afd_start (void *nctx, cl_algs_hdl_t *halgs, cl_in_t *in, cl_out_t *out)
{
    cl_afd_stm_ctx_t *n_ctx = nctx;
    int32 ret = 0;

    mmsdbg(DL_FUNC, "Enter");

    n_ctx->curr = CL_AFD_STATE_STARTUP;
    n_ctx->next = CL_AFD_STATE_RUNING;
    n_ctx->calc_cnt = 0;
    ret = sga_afd_prv_configuration(n_ctx->hsg_afd_prv, halgs, &n_ctx->sga_cfg);

    mmsdbg(DL_FUNC, "Exit %s", ret ? "Error" : "Ok");
    return ret;
}

/* ========================================================================== */
/**
* afd_sart_req()
*/
/* ========================================================================== */
static int afd_start_req (void *nctx, cl_algs_hdl_t *halgs, cl_in_t *in, cl_out_t *out)
{
    cl_afd_stm_ctx_t *n_ctx = nctx;
    sga_afd_prv_input_t  sg_in;
    sga_afd_prv_output_t sg_out;
    cam_cl_algo_ctrl_list_t *entry;
    int32 ret = 0;
    uint32 flags;

    mmsdbg(DL_FUNC, "Enter");

    n_ctx->dist_mode = SG_AFD_DISTR_MODE_50Hz_GOOD;

    flags = (in->enb_aca)?(CL_FLAG_FORCE_PROCESS|CL_FLAG_FORCE_POST_CALC_EVENT):0;
    sg_in.dtp_d_common  = in->dtp_dyn;
    sg_in.ae_out        = in->ae_out;
    sg_in.ae_stab_out   = in->ae_stab;
    sg_in.awb_out       = in->awb_out;
    sg_in.focus_out     = in->focus;
    sg_in.vsen_modes    = in->vsen_modes;
    sg_in.ae_smooth_mode= SG_AE_SMOOTH_MODE_FAST;

    sg_in.ae_distr_mode = SG_AE_DISTR_MODE_PREVIEW;
    sg_in.afd_distr_mode = n_ctx->dist_mode;

    sg_out.ae_distr     = &out->sg.ae_distr;
    sg_out.ae_smooth    = &out->sg.ae_smooth;
    sg_out.vpipe        = &out->vpipe;

    n_ctx->ae_stab = *(in->ae_stab);

    ret += sga_afd_prv_process(n_ctx->hsg_afd_prv, halgs, &sg_in, &sg_out);

    { // AFD
        entry = list_pool_append_element(in->pool_alg_ctrl, out->cl_req_aca);
        init_algo_ctrl_entry(&entry->ctrl,
                             CL_BUFF_STATS_AEWB,
                             CL_ALGO_AFD,
                             n_ctx->dist_mode,
                             0,
                             flags);
    }
    // Skip AWB process

    // Skip Focus process

    { // ZSL Queue
        entry = list_pool_append_element(in->pool_alg_ctrl, out->cl_req_img);
        init_algo_ctrl_entry(&entry->ctrl,
                             CL_BUFF_ISP,
                             CL_ALGO_ISP,
                             0,
                             0,
                             (CL_FLAG_FORCE_PROCESS));
        entry->ctrl.alg_specific.isp.out_flags = DEF_PRV_PIPE_CTRL_FLAGS;
    }
#if defined(__sparc)
    n_ctx->dist_mode = SG_AFD_DISTR_MODE_50Hz_BAD;
#endif
    n_ctx->calc_cnt = 0;
    n_ctx->curr = n_ctx->next;

    mmsdbg(DL_FUNC, "Exit %s", ret ? "Error" : "Ok");
    return ret;
}

/* ========================================================================== */
/**
* afd_sart_req()
*/
/* ========================================================================== */
static int afd_req (void *nctx, cl_algs_hdl_t *halgs, cl_in_t *in, cl_out_t *out)
{
    int32 ret = 0;
    cl_afd_stm_ctx_t *n_ctx = nctx;
    sga_afd_prv_input_t  sg_in;
    sga_afd_prv_output_t sg_out;
    cam_cl_algo_ctrl_list_t *entry;
    uint32 flags;

    mmsdbg(DL_FUNC, "Enter");

    flags = (in->enb_aca)?(CL_FLAG_FORCE_PROCESS|CL_FLAG_FORCE_POST_CALC_EVENT):0;
    sg_in.dtp_d_common  = in->dtp_dyn;
    sg_in.ae_out        = in->ae_out;
    sg_in.ae_stab_out   = &n_ctx->ae_stab;
    sg_in.awb_out       = in->awb_out;
    sg_in.focus_out     = in->focus;
    sg_in.vsen_modes    = in->vsen_modes;
    sg_in.ae_smooth_mode= SG_AE_SMOOTH_MODE_FAST;

    sg_in.ae_distr_mode = SG_AE_DISTR_MODE_PREVIEW;
    sg_in.afd_distr_mode = n_ctx->dist_mode;

    sg_out.ae_distr     = &out->sg.ae_distr;
    sg_out.ae_smooth    = &out->sg.ae_smooth;
    sg_out.vpipe        = &out->vpipe;

    sga_afd_prv_process(n_ctx->hsg_afd_prv, halgs, &sg_in, &sg_out);

    { // AFD
        entry = list_pool_append_element(in->pool_alg_ctrl, out->cl_req_aca);
        init_algo_ctrl_entry(&entry->ctrl,
                             CL_BUFF_STATS_AEWB,
                             CL_ALGO_AFD,
                             n_ctx->dist_mode,
                             0,
                             flags);
    }

    // Skip AWB process

    // Skip Focus process

    { // ZSL Queue
        entry = list_pool_append_element(in->pool_alg_ctrl, out->cl_req_img);
        init_algo_ctrl_entry(&entry->ctrl,
                             CL_BUFF_ISP,
                             CL_ALGO_ISP,
                             0,
                             0,
                             (CL_FLAG_FORCE_PROCESS));
        entry->ctrl.alg_specific.isp.out_flags = DEF_PRV_PIPE_CTRL_FLAGS;
    }

    n_ctx->calc_cnt++;
#if defined(__sparc)
    if(1) {
#else
    if(!(n_ctx->calc_cnt % REPEAT_AFD_MODE_CNT)) {
#endif
        switch (n_ctx->dist_mode)
        {
        case SG_AFD_DISTR_MODE_50Hz_GOOD:
            n_ctx->dist_mode = SG_AFD_DISTR_MODE_50Hz_BAD;
            break;
        case SG_AFD_DISTR_MODE_50Hz_BAD:
            n_ctx->dist_mode = SG_AFD_DISTR_MODE_60Hz_GOOD;
            break;
        case SG_AFD_DISTR_MODE_60Hz_GOOD:
            n_ctx->dist_mode = SG_AFD_DISTR_MODE_60Hz_BAD;
            break;
        default:
            n_ctx->curr =  CL_AFD_STATE_IDLE;
            break;
        }
    }

    mmsdbg(DL_FUNC, "Exit %s", ret ? "Error" : "Ok");
    return ret;
}

/* ========================================================================== */
/**
* afd_abort()
*/
/* ========================================================================== */
static int afd_abort (void *nctx, cl_algs_hdl_t *halgs, cl_in_t *in, cl_out_t *out)
{
    cl_afd_stm_ctx_t *n_ctx = nctx;
    int32 ret = 0;

    mmsdbg(DL_FUNC, "Enter");

    n_ctx->curr = CL_AFD_STATE_IDLE;

    mmsdbg(DL_FUNC, "Exit %s", ret ? "Error" : "Ok");
    return ret;
}

/* ========================================================================== */
/**
* afd_abort_req_frm()
*/
/* ========================================================================== */
static int afd_abort_req (void *nctx, cl_algs_hdl_t *halgs, cl_in_t *in, cl_out_t *out)
{
    cl_afd_stm_ctx_t *n_ctx = nctx;
    int32 ret = 0;

    mmsdbg(DL_FUNC, "Enter");

    n_ctx->curr = n_ctx->next;

    mmsdbg(DL_FUNC, "Exit %s", ret ? "Error" : "Ok");
    return ret;
}

static const p_cl_state_exe_t afd_state_table[MAX_CL_AFD_STATE][MAX_CL_AFD_CMD] = {
    {afd_start, dummy_state, afd_abort},        //IDLE,
    {dummy_state, afd_start_req, afd_abort},    //STARTUP,
    {afd_start, afd_req, afd_abort},            //RUNING,
    {dummy_state, afd_abort_req, afd_abort},    //ABORTING,
};

/* ========================================================================== */
/**
* afd_exec()
*/
/* ========================================================================== */
static int afd_exec (void *nctx,
                            cl_afd_cmd_t cmd,
                            cl_algs_hdl_t *halgs,
                            cl_in_t *in,
                            cl_out_t *out)
{
    cl_afd_stm_ctx_t *n_ctx = nctx;
    cl_afd_state_t state = n_ctx->curr;

    if ((state >= MAX_CL_AFD_STATE) || (cmd >= MAX_CL_AFD_CMD)) {
        mmsdbg(DL_ERROR, "Out of range parameters");
        return -1;
    }

    return afd_state_table[state][cmd](n_ctx, halgs, in, out);
}

/* ========================================================================== */
/**
* afd_ready()
*/
/* ========================================================================== */
static int afd_ready (void *nctx)
{
    cl_afd_stm_ctx_t *n_ctx = nctx;
    cl_afd_state_t state = n_ctx->curr;

    return (CL_AFD_STATE_IDLE == state) ? 1 : 0;
}

/* ========================================================================== */
/**
* afd_ready()
*/
/* ========================================================================== */
static int afd_config (void *nctx, cl_algs_hdl_t *halgs, cl_cfg_t *cfg)
{
    cl_afd_stm_ctx_t *n_ctx = nctx;
    int ret = 0;

    n_ctx->sga_cfg.dtp_s_common = cfg->dtp_stat;
    n_ctx->sga_cfg.gzz_cfg = &cfg->new_cam_cfg->cam_gzz_cfg;

    if (CL_AFD_STATE_IDLE != n_ctx->curr) {
        ret = sga_afd_prv_configuration(n_ctx->hsg_afd_prv, halgs, &n_ctx->sga_cfg);
    }

    mmsdbg(DL_FUNC, "Exit %s", ret ? "Error" : "Ok");
    return ret;
}

/* ========================================================================== */
/**
* cl_create_afd_stm()
*/
/* ========================================================================== */
int cl_create_afd_stm (cl_stm_t **afd_hdl)
{
    cl_afd_stm_ctx_t *n_ctx;
    *afd_hdl = osal_calloc(1, sizeof(cl_stm_t));

    if (!*afd_hdl) {
        goto ERROR_2;
    }

    (*afd_hdl)->private = osal_calloc(1, sizeof(cl_afd_stm_ctx_t));

    if (!(*afd_hdl)->private) {
        goto ERROR_1;
    }

    n_ctx = (*afd_hdl)->private;
    if(sga_afd_prv_create(&n_ctx->hsg_afd_prv)) {
        goto ERROR_3;
    }

    (*afd_hdl)->config = afd_config;
    (*afd_hdl)->exec = afd_exec;
    (*afd_hdl)->ready = afd_ready;

    return 0;

ERROR_3:
    osal_free(n_ctx);
ERROR_2:
    osal_free(*afd_hdl);
ERROR_1:
    mmsdbg(DL_ERROR, "Can't allocate memory");
    return -1;
}


/* ========================================================================== */
/**
* cl_destroy_afd_stm()
*/
/* ========================================================================== */
int cl_destroy_afd_stm (cl_stm_t *hndl)
{
    cl_afd_stm_ctx_t *n_ctx  = hndl->private;

    sga_afd_prv_destroy(n_ctx->hsg_afd_prv);
    osal_free(hndl->private);
    osal_free(hndl);
    return 0;
}

