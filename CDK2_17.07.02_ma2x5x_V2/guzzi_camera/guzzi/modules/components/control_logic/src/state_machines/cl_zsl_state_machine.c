/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file cl_zsl_state_machine.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <osal/osal_stdlib.h>
#include <osal/osal_assert.h>
#include <utils/mms_debug.h>
#include <error_handle/include/error_handle.h>
#include <cl_zsl_stm_types.h>
#include <osal/osal_string.h>
#include "sg_mode_zsl_capture.h"
#include <guzzi_event/include/guzzi_event.h>
#include <guzzi_event_global/include/guzzi_event_global.h>

mmsdbg_define_variable(
        vdl_zsl_control_logic,
        DL_DEFAULT,
        0,
        "zsl.control_logic",
        "ZSL Control Logic"
    );
#define MMSDEBUGLEVEL mmsdbg_use_variable(vdl_zsl_control_logic)

typedef struct {
    cl_zsl_state_t          curr;
    cl_zsl_state_t          next;
    sga_zsl_cap_hndl_t      hsg_zsl_cap;
    sga_zsl_cap_config_t    sga_cfg;
    guzzi_event_t           *evt_hdl;
    uint32                  camera_id;
    cam_capture_request_t   zsl_req;
    osal_mutex              *events_lock;
    osal_sem                *sem_zsl_lock;
} cl_zsl_stm_ctx_t;

/* ========================================================================== */
/**
* void _zsl_evt_notify()
*/
/* ========================================================================== */
void _zsl_evt_notify (void *_prv, int event_id,
                            uint32 num, void *data)
{
    cl_zsl_stm_ctx_t *z_ctx  = _prv;

    mmsdbg(DL_FUNC, "Enter");

    if (num) {
        //it is non ZSL buff lock
        return;
    }

    if (CAM_EVT_BUFF_LOCKED == geg_camera_event_get_eid(event_id)) {
        osal_memcpy(&z_ctx->zsl_req, data, sizeof(z_ctx->zsl_req));
        osal_sem_post(z_ctx->sem_zsl_lock);
    }

    mmsdbg(DL_FUNC, "Exit %s", 0 ? "Error" : "Ok");
    return;
}

/* ========================================================================== */
/**
* void cl_zsl_unreg_evt()
*/
/* ========================================================================== */
static void cl_zsl_unreg_evt (cl_zsl_stm_ctx_t *z_ctx)
{
    guzzi_event_unreg_recipient(z_ctx->evt_hdl, geg_camera_event_mk(z_ctx->camera_id, CAM_EVT_BUFF_LOCKED), _zsl_evt_notify, z_ctx);
}

/* ========================================================================== */
/**
* void cl_zsl_reg_evt()
*/
/* ========================================================================== */
static int cl_zsl_reg_evt (cl_zsl_stm_ctx_t *z_ctx)
{
    if(guzzi_event_reg_recipient(z_ctx->evt_hdl, geg_camera_event_mk(z_ctx->camera_id, CAM_EVT_BUFF_LOCKED),
                                     _zsl_evt_notify, z_ctx)) {
        goto ERROR_1;
    }

    return 0;
ERROR_1:
    return -1;
}

/* ========================================================================== */
/**
* dummy_state()
*/
/* ========================================================================== */
static int dummy_state (void *zctx, cl_algs_hdl_t *halgs, cl_in_t *in, cl_out_t *out)
{
    mmsdbg(DL_FUNC, "Enter");

    mmsdbg(DL_FUNC, "Exit Ok");
    return 0;
}

/* ========================================================================== */
/**
* zsl_start()
*/
/* ========================================================================== */
static int zsl_start(void *zctx, cl_algs_hdl_t *halgs, cl_in_t *in, cl_out_t *out)
{
    cl_zsl_stm_ctx_t *z_ctx = zctx;
    int32 ret = 0;

    mmsdbg(DL_FUNC, "Enter");

    sga_zsl_cap_configuration(z_ctx->hsg_zsl_cap, halgs, &z_ctx->sga_cfg);

    void los_ipipe_LockZSL(uint32 srcIdx);

    osal_sem_init(z_ctx->sem_zsl_lock, 0);
    z_ctx->curr = CL_ZSL_STATE_CAPTURE;
    los_ipipe_LockZSL(z_ctx->camera_id);

    mmsdbg(DL_FUNC, "Exit %s", ret ? "Error" : "Ok");
    return ret;
}

/* ========================================================================== */
/**
* zsl_frm_req()
*/
/* ========================================================================== */
static int zsl_frm_req(void *zctx, cl_algs_hdl_t *halgs, cl_in_t *in, cl_out_t *out)
{
    cl_zsl_stm_ctx_t *z_ctx = zctx;
    int32 ret = 0;
    int err;
    sga_zsl_cap_input_t  sg_in;
    sga_zsl_cap_output_t sg_out;
    cam_cl_algo_ctrl_list_t *entry;
    cam_cl_algo_ctrl_t   *alg;
    uint32 flags;

    mmsdbg(DL_FUNC, "Enter");

    flags = (in->enb_aca)?(CL_FLAG_FORCE_POST_CALC_EVENT):0;
    sg_in.dtp_d_common  = in->dtp_dyn;
    sg_in.ae_out        = in->ae_out;
    sg_in.ae_stab_out   = in->ae_stab;
    sg_in.awb_out       = in->awb_out;
    sg_in.focus_out     = in->focus;
    sg_in.vsen_modes    = in->vsen_modes;
    sg_in.afd_out       = in->afd_out;
    sg_in.ae_smooth_mode= SG_AE_SMOOTH_MODE_FAST;
    sg_in.ae_smooth_mode= SG_AE_SMOOTH_MODE_MEDIUM; //SG_AE_SMOOTH_MODE_FAST;

    sg_in.ae_distr_mode = SG_AE_DISTR_MODE_PREVIEW; // SG_AE_DISTR_MODE_CAPTURE;

    sg_out.ae_distr     = &out->sg.ae_distr;
    sg_out.ae_smooth    = &out->sg.ae_smooth;
    sg_out.vpipe        = &out->vpipe;

    sga_zsl_cap_process(z_ctx->hsg_zsl_cap, halgs, &sg_in, &sg_out);

    err = osal_sem_wait_timeout(z_ctx->sem_zsl_lock, DEFAULT_ALLOC_TIMEOUT_MS);
    if (err) {
        mmsdbg(DL_ERROR, "Zsl buffer lock timeout %d ms", DEFAULT_ALLOC_TIMEOUT_MS);
    }
    osal_assert(!err);

    osal_mutex_lock(z_ctx->events_lock);
    if (z_ctx->zsl_req.buff_hndl) {
        entry = list_pool_append_element(in->pool_alg_ctrl, out->cl_req_img);
        alg = &entry->ctrl;

        alg->img_Algo  = CL_ALGO_ZSL_QUEUE;
        alg->nMode     = 0;
        alg->nSub_mode = 0;
        alg->processing_flags = flags;
        alg->buff_type = CL_BUFF_ZSL_QUEUE;
        alg->alg_specific.isp.out_flags = DEF_ZSL_PIPE_CTRL_FLAGS;
        alg->alg_specific.capt_request.buff_hndl = z_ctx->zsl_req.buff_hndl;
        alg->alg_specific.capt_request.flags = 0;
    } else {
        entry = list_pool_append_element(in->pool_alg_ctrl, out->cl_req_img);
        init_algo_ctrl_entry(&entry->ctrl,
                             CL_BUFF_ISP,
                             CL_ALGO_ISP,
                             0,
                             0,
                             CL_FLAG_FORCE_PROCESS);
        entry->ctrl.alg_specific.isp.out_flags = DEF_ZSL_PIPE_CTRL_FLAGS;
        mmsdbg(DL_ERROR, "Null Zsl buffer");
    }

    osal_mutex_unlock(z_ctx->events_lock);
    out->seq_status = CL_SEQ_STAT_NO_DLEAY;
    z_ctx->curr = CL_ZSL_STATE_IDLE;

    mmsdbg(DL_FUNC, "Exit %s", ret ? "Error" : "Ok");
    return ret;
}

static const p_cl_state_exe_t zsl_state_table[MAX_CL_ZSL_STATE][MAX_CL_ZSL_CMD] = {
    {zsl_start, dummy_state},           // CL_ZSL_STATE_IDLE
    {dummy_state, zsl_frm_req},         // CL_ZSL_STATE_CAPTURE
};

/* ========================================================================== */
/**
* zsl_exec()
*/
/* ========================================================================== */
static int zsl_exec (void *zctx,
                            cl_zsl_cmd_t cmd,
                            cl_algs_hdl_t *halgs,
                            cl_in_t *in,
                            cl_out_t *out)
{
    cl_zsl_stm_ctx_t *z_ctx = zctx;
    cl_zsl_state_t state = z_ctx->curr;

    if ((state >= MAX_CL_ZSL_STATE) || (cmd >= MAX_CL_ZSL_CMD)) {
        mmsdbg(DL_ERROR, "Out of range parameters");
        return -1;
    }

    return zsl_state_table[state][cmd](z_ctx, halgs, in, out);
}

/* ========================================================================== */
/**
* zsl_ready()
*/
/* ========================================================================== */
static int zsl_ready (void *zctx)
{
    cl_zsl_stm_ctx_t *z_ctx = zctx;
    cl_zsl_state_t state = z_ctx->curr;

    return (CL_ZSL_STATE_IDLE == state) ? 1 : 0;
}

/* ========================================================================== */
/**
* zsl_ready()
*/
/* ========================================================================== */
static int zsl_config (void *zctx, cl_algs_hdl_t *halgs, cl_cfg_t *cfg)
{
    int ret = 0;
    cl_zsl_stm_ctx_t *z_ctx = zctx;

    z_ctx->sga_cfg.dtp_s_common = cfg->dtp_stat;
    z_ctx->sga_cfg.gzz_cfg = &cfg->new_cam_cfg->cam_gzz_cfg;

    if (CL_ZSL_STATE_CAPTURE == z_ctx->curr) {
        sga_zsl_cap_configuration(z_ctx->hsg_zsl_cap, halgs, &z_ctx->sga_cfg);
    }

    mmsdbg(DL_FUNC, "Exit %s", ret ? "Error" : "Ok");
    return ret;
}
/* ========================================================================== */
/**
* cl_create_zsl_stm()
*/
/* ========================================================================== */
int cl_create_zsl_stm (cl_stm_t **zsl_hdl, cl_zsl_create_params_t *params)
{
    *zsl_hdl = osal_calloc(1, sizeof(cl_stm_t));
    cl_zsl_stm_ctx_t *z_ctx;

    if (!*zsl_hdl) {
        goto ERROR_1;
    }

    (*zsl_hdl)->private = osal_calloc(1, sizeof(cl_zsl_stm_ctx_t));

    if (!(*zsl_hdl)->private) {
        goto ERROR_2;
    }

    z_ctx = (*zsl_hdl)->private;

    z_ctx->evt_hdl = params->evt_hndl;
    z_ctx->camera_id = params->camera_id;

    if (sga_zsl_cap_create(&z_ctx->hsg_zsl_cap)){
        goto ERROR_3;
    }

    z_ctx->sem_zsl_lock = osal_sem_create(0);
    if (!z_ctx->sem_zsl_lock) {
        goto ERROR_4;
    }

    z_ctx->events_lock = osal_mutex_create();
    if (!z_ctx->events_lock) {
        goto ERROR_5;
    }

    if (cl_zsl_reg_evt(z_ctx)) {
        goto ERROR_6;
    }

    (*zsl_hdl)->config = zsl_config;
    (*zsl_hdl)->exec = zsl_exec;
    (*zsl_hdl)->ready = zsl_ready;

    return 0;

ERROR_6:
    osal_mutex_destroy(z_ctx->events_lock);
ERROR_5:
    osal_sem_destroy(z_ctx->sem_zsl_lock);
ERROR_4:
    sga_zsl_cap_destroy(z_ctx->hsg_zsl_cap);
ERROR_3:
    osal_free(z_ctx);
ERROR_2:
    osal_free(*zsl_hdl);
ERROR_1:
    mmsdbg(DL_ERROR, "Can't allocate memory");
    return -1;
}

/* ========================================================================== */
/**
* cl_destroy_zsl_stm()
*/
/* ========================================================================== */
int cl_destroy_zsl_stm (cl_stm_t *hdl)
{
    cl_zsl_stm_ctx_t *z_ctx = hdl->private;

    cl_zsl_unreg_evt(z_ctx);
    osal_mutex_destroy(z_ctx->events_lock);
    osal_sem_destroy(z_ctx->sem_zsl_lock);
    sga_zsl_cap_destroy(z_ctx->hsg_zsl_cap);
    osal_free(hdl->private);
    osal_free(hdl);
    return 0;
}

