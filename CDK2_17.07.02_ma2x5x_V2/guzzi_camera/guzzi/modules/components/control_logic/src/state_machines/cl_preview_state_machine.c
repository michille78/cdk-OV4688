/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file cl_preview_state_machine.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <osal/osal_stdlib.h>
#include <utils/mms_debug.h>
#include <error_handle/include/error_handle.h>
#include "cl_preview_stm_types.h"
#include "cl_normal_stm_types.h"
#include "cl_afd_stm_types.h"
#include "cl_focus_stm_types.h"

mmsdbg_define_variable(
        vdl_preview_control_logic,
        DL_DEFAULT,
        0,
        "prv.control_logic",
        "PRV Control Logic"
    );
#define MMSDEBUGLEVEL mmsdbg_use_variable(vdl_preview_control_logic)

typedef struct {
    cl_prv_state_t      curr;
    cl_prv_state_t      next;
    cl_stm_t            *hndl_norm;
    cl_stm_t            *hndl_afd;
    cl_stm_t            *hndl_focus;
} cl_prv_stm_ctx_t;

/* ========================================================================== */
/**
* dummy_state()
*/
/* ========================================================================== */
static int dummy_state (void *pctx, cl_algs_hdl_t *halgs, cl_in_t *in, cl_out_t *out)
{
    mmsdbg(DL_FUNC, "Enter");

    mmsdbg(DL_FUNC, "Exit Ok");
    return 0;
}

/* ========================================================================== */
/**
* prv_nomal()
*/
/* ========================================================================== */
static int prv_nomal (void *pctx, cl_algs_hdl_t *halgs, cl_in_t *in, cl_out_t *out)
{
    cl_prv_stm_ctx_t *p_ctx = pctx;
    cl_stm_t *norm = p_ctx->hndl_norm;

    int32 ret = -1;

    mmsdbg(DL_FUNC, "Enter");

    if (!norm->exec(norm->private, CL_NORMAL_CMD_SATRT, halgs, in, out)) {
        ret = 0;
        p_ctx->curr = CL_PRV_STATE_NORMAL;
    }

    mmsdbg(DL_FUNC, "Exit %s", ret ? "Error" : "Ok");
    return ret;
}

/* ========================================================================== */
/**
* prv_foc()
*/
/* ========================================================================== */
static int prv_foc (void *pctx, cl_algs_hdl_t *halgs, cl_in_t *in, cl_out_t *out)
{
    cl_prv_stm_ctx_t *p_ctx = pctx;
    cl_stm_t *focus  = p_ctx->hndl_focus;
    int32 ret = -1;

    mmsdbg(DL_FUNC, "Enter");

    if (!focus->exec(focus->private, CL_FOCUS_CMD_START, halgs, in, out)) {
        ret = 0;
        p_ctx->curr = CL_PRV_STATE_FOCUS;
        p_ctx->next = CL_PRV_STATE_IDLE;
    }

    mmsdbg(DL_FUNC, "Exit %s", ret ? "Error" : "Ok");
    return ret;
}

/* ========================================================================== */
/**
* prv_afd()
*/
/* ========================================================================== */
static int prv_afd (void *pctx, cl_algs_hdl_t *halgs, cl_in_t *in, cl_out_t *out)
{
    cl_prv_stm_ctx_t *p_ctx = pctx;
    cl_stm_t *afd  = p_ctx->hndl_afd;
    int32 ret = -1;

    mmsdbg(DL_FUNC, "Enter");

    if (!afd->exec(afd->private, CL_AFD_CMD_SATRT, halgs, in, out)) {
        ret = 0;
        p_ctx->curr = CL_PRV_STATE_FLICKER;
        p_ctx->next = CL_PRV_STATE_IDLE;
    }

    mmsdbg(DL_FUNC, "Exit %s", ret ? "Error" : "Ok");
    return ret;
}

/* ========================================================================== */
/**
* normal_abort()
*/
/* ========================================================================== */
static int normal_abort (void *pctx, cl_algs_hdl_t *halgs, cl_in_t *in, cl_out_t *out)
{
    cl_prv_stm_ctx_t *p_ctx = pctx;
    cl_stm_t *norm = p_ctx->hndl_norm;

    int32 ret = -1;

    mmsdbg(DL_FUNC, "Enter");

    if (!norm->exec(norm->private, CL_NORMAL_CMD_ABORT, halgs, in, out)) {
        ret = 0;
        p_ctx->curr = CL_PRV_STATE_IDLE;
    }

    mmsdbg(DL_FUNC, "Exit %s", ret ? "Error" : "Ok");
    return ret;
}

/* ========================================================================== */
/**
* normal_to_af()
*/
/* ========================================================================== */
static int normal_to_af (void *pctx, cl_algs_hdl_t *halgs, cl_in_t *in, cl_out_t *out)
{
    cl_prv_stm_ctx_t *p_ctx = pctx;
    cl_stm_t *focus  = p_ctx->hndl_focus;
    cl_stm_t *norm = p_ctx->hndl_norm;
    int32 ret = -1;

    mmsdbg(DL_FUNC, "Enter");

    norm->exec(norm->private, CL_NORMAL_CMD_ABORT, halgs, in, out);
    if (!focus->exec(focus->private, CL_FOCUS_CMD_START, halgs, in, out)) {
        ret = 0;
        p_ctx->curr = CL_PRV_STATE_FOCUS;
        p_ctx->next = CL_PRV_STATE_NORMAL;
    }

    mmsdbg(DL_FUNC, "Exit %s", ret ? "Error" : "Ok");
    return ret;
}

/* ========================================================================== */
/**
* normal_to_afd()
*/
/* ========================================================================== */
static int normal_to_afd (void *pctx, cl_algs_hdl_t *halgs, cl_in_t *in, cl_out_t *out)
{
    cl_prv_stm_ctx_t *p_ctx = pctx;
    cl_stm_t *afd  = p_ctx->hndl_afd;
    cl_stm_t *norm = p_ctx->hndl_norm;
    int32 ret = -1;

    mmsdbg(DL_FUNC, "Enter");

    norm->exec(norm->private, CL_NORMAL_CMD_ABORT, halgs, in, out);
    if (!afd->exec(afd->private, CL_AFD_CMD_SATRT, halgs, in, out)) {
        ret = 0;
        p_ctx->curr = CL_PRV_STATE_FLICKER;
        p_ctx->next = CL_PRV_STATE_NORMAL;
    }

    mmsdbg(DL_FUNC, "Exit %s", ret ? "Error" : "Ok");
    return ret;
}

/* ========================================================================== */
/**
* af_abort()
*/
/* ========================================================================== */
static int af_abort (void *pctx, cl_algs_hdl_t *halgs, cl_in_t *in, cl_out_t *out)
{
    cl_prv_stm_ctx_t *p_ctx = pctx;
    cl_stm_t *focus  = p_ctx->hndl_focus;
    cl_stm_t *norm = p_ctx->hndl_norm;
    cl_stm_t *afd  = p_ctx->hndl_afd;
    int32 ret = -1;

    mmsdbg(DL_FUNC, "Enter");

    ret = focus->exec(focus->private, CL_FOCUS_CMD_ABORT, halgs, in, out);
    switch (p_ctx->next) {
    case CL_PRV_STATE_NORMAL:
        p_ctx->curr = p_ctx->next;
        norm->exec(norm->private, CL_NORMAL_CMD_SATRT, halgs, in, out);
        break;
    case CL_PRV_STATE_FLICKER:
        p_ctx->curr = p_ctx->next;
        afd->exec(afd->private, CL_AFD_CMD_SATRT, halgs, in, out);
        break;
    default:
        p_ctx->curr = CL_PRV_STATE_IDLE;
        break;
    }


    mmsdbg(DL_FUNC, "Exit %s", ret ? "Error" : "Ok");
    return ret;
}

/* ========================================================================== */
/**
* af_to_normal()
*/
/* ========================================================================== */
static int af_to_normal (void *pctx, cl_algs_hdl_t *halgs, cl_in_t *in, cl_out_t *out)
{
    cl_prv_stm_ctx_t *p_ctx = pctx;
    cl_stm_t *focus  = p_ctx->hndl_focus;
    cl_stm_t *norm = p_ctx->hndl_norm;
    int32 ret = -1;

    mmsdbg(DL_FUNC, "Enter");
    focus->exec(focus->private, CL_FOCUS_CMD_ABORT, halgs, in, out);
    if (!norm->exec(norm->private, CL_NORMAL_CMD_SATRT, halgs, in, out)) {
        ret = 0;
        p_ctx->next = CL_PRV_STATE_IDLE;
        p_ctx->curr = CL_PRV_STATE_NORMAL;
    }

    mmsdbg(DL_FUNC, "Exit %s", ret ? "Error" : "Ok");
    return ret;
}

/* ========================================================================== */
/**
* af_to_afd()
*/
/* ========================================================================== */
static int af_to_afd (void *pctx, cl_algs_hdl_t *halgs, cl_in_t *in, cl_out_t *out)
{
    cl_prv_stm_ctx_t *p_ctx = pctx;
    cl_stm_t *focus  = p_ctx->hndl_focus;
    cl_stm_t *afd  = p_ctx->hndl_afd;
    int32 ret = -1;

    mmsdbg(DL_FUNC, "Enter");

    focus->exec(focus->private, CL_FOCUS_CMD_ABORT, halgs, in, out);
    if (!afd->exec(afd->private, CL_AFD_CMD_SATRT, halgs, in, out)) {
        ret = 0;
    }

    p_ctx->curr = CL_PRV_STATE_FLICKER;
    p_ctx->next = CL_PRV_STATE_NORMAL;

    mmsdbg(DL_FUNC, "Exit %s", ret ? "Error" : "Ok");
    return ret;
}

/* ========================================================================== */
/**
* afd_abort()
*/
/* ========================================================================== */
static int afd_abort (void *pctx, cl_algs_hdl_t *halgs, cl_in_t *in, cl_out_t *out)
{
    cl_prv_stm_ctx_t *p_ctx = pctx;
    cl_stm_t *afd  = p_ctx->hndl_afd;
    cl_stm_t *focus  = p_ctx->hndl_focus;
    cl_stm_t *norm = p_ctx->hndl_norm;
    int32 ret = -1;

    mmsdbg(DL_FUNC, "Enter");

    afd->exec(afd->private, CL_AFD_CMD_ABORT, halgs, in, out);
    switch (p_ctx->next) {
    case CL_PRV_STATE_NORMAL:
        p_ctx->curr = p_ctx->next;
        norm->exec(norm->private, CL_NORMAL_CMD_SATRT, halgs, in, out);
        break;
    case CL_PRV_STATE_FOCUS:
        p_ctx->curr = p_ctx->next;
        focus->exec(focus->private, CL_FOCUS_CMD_START, halgs, in, out);
        break;
    default:
        p_ctx->curr = CL_PRV_STATE_IDLE;
        break;
    }
    mmsdbg(DL_FUNC, "Exit %s", ret ? "Error" : "Ok");
    return ret;
}

/* ========================================================================== */
/**
* afd_abort()
*/
/* ========================================================================== */
static int afd_to_afd (void *pctx, cl_algs_hdl_t *halgs, cl_in_t *in, cl_out_t *out)
{
    cl_prv_stm_ctx_t *p_ctx = pctx;
    cl_stm_t *afd  = p_ctx->hndl_afd;
    int32 ret = -1;

    mmsdbg(DL_FUNC, "Enter");

    afd->exec(afd->private, CL_AFD_CMD_SATRT, halgs, in, out);

    mmsdbg(DL_FUNC, "Exit %s", ret ? "Error" : "Ok");
    return ret;
}

/* ========================================================================== */
/**
* afd_to_normal()
*/
/* ========================================================================== */
static int afd_to_normal(void *pctx, cl_algs_hdl_t *halgs, cl_in_t *in, cl_out_t *out)
{
    cl_prv_stm_ctx_t *p_ctx = pctx;
    cl_stm_t *norm = p_ctx->hndl_norm;
    cl_stm_t *afd  = p_ctx->hndl_afd;
    int32 ret = -1;

    mmsdbg(DL_FUNC, "Enter");

    afd->exec(afd->private, CL_AFD_CMD_ABORT, halgs, in, out);
    if (!norm->exec(norm->private, CL_NORMAL_CMD_SATRT, halgs, in, out)) {
        ret = 0;
    }

    p_ctx->curr = CL_PRV_STATE_NORMAL;
    p_ctx->next = CL_PRV_STATE_NORMAL;

    mmsdbg(DL_FUNC, "Exit %s", ret ? "Error" : "Ok");
    return ret;
}

/* ========================================================================== */
/**
* afd_to_af()
*/
/* ========================================================================== */
static int afd_to_af (void *pctx, cl_algs_hdl_t *halgs, cl_in_t *in, cl_out_t *out)
{
    cl_prv_stm_ctx_t *p_ctx = pctx;
    cl_stm_t *focus  = p_ctx->hndl_focus;
    cl_stm_t *afd  = p_ctx->hndl_afd;
    int32 ret = -1;

    mmsdbg(DL_FUNC, "Enter");

    afd->exec(afd->private, CL_AFD_CMD_ABORT, halgs, in, out);
    if (!focus->exec(focus->private, CL_FOCUS_CMD_START, halgs, in, out)) {
        ret = 0;
    }

    p_ctx->curr = CL_PRV_STATE_FOCUS;
    p_ctx->next = CL_PRV_STATE_NORMAL;

    mmsdbg(DL_FUNC, "Exit %s", ret ? "Error" : "Ok");
    return ret;
}

/* ========================================================================== */
/**
* prv_frm_req()
*/
/* ========================================================================== */
static int prv_frm_req (void *pctx, cl_algs_hdl_t *halgs, cl_in_t *in, cl_out_t *out)
{
    cl_prv_stm_ctx_t *p_ctx = pctx;
    cl_stm_t *norm = p_ctx->hndl_norm;
    int32 ret = -1;

    mmsdbg(DL_FUNC, "Enter");

    if (!norm->exec(norm->private, CL_NORMAL_CMD_FRAME_REQUEST, halgs, in, out)) {
        ret = 0;
        if (norm->ready(norm->private)) {
            p_ctx->curr = p_ctx->next;
        }
    }

    mmsdbg(DL_FUNC, "Exit %s", ret ? "Error" : "Ok");
    return ret;
}

/* ========================================================================== */
/**
* prv_frm_req()
*/
/* ========================================================================== */
static int afd_frm_req (void *pctx, cl_algs_hdl_t *halgs, cl_in_t *in, cl_out_t *out)
{
    cl_prv_stm_ctx_t *p_ctx = pctx;
    cl_stm_t *afd = p_ctx->hndl_afd;

    int32 ret = -1;

    mmsdbg(DL_FUNC, "Enter");


    if (!afd->exec(afd->private, CL_AFD_CMD_FRAME_REQUEST, halgs, in, out)) {
        ret = 0;
        if (afd->ready(afd->private)) {
            p_ctx->curr = p_ctx->next;
        }
    }

    mmsdbg(DL_FUNC, "Exit %s", ret ? "Error" : "Ok");
    return ret;
}

/* ========================================================================== */
/**
* prv_frm_req()
*/
/* ========================================================================== */
static int focus_frm_req (void *pctx, cl_algs_hdl_t *halgs, cl_in_t *in, cl_out_t *out)
{
    cl_prv_stm_ctx_t *p_ctx = pctx;
    cl_stm_t *focus = p_ctx->hndl_focus;

    int32 ret = -1;

    mmsdbg(DL_FUNC, "Enter");

    if (!focus->exec(focus->private, CL_FOCUS_CMD_FRAME_REQUEST, halgs, in, out)) {
        ret = 0;
        if (focus->ready(focus->private)) {
            p_ctx->curr = p_ctx->next;
        }
    }

    mmsdbg(DL_FUNC, "Exit %s", ret ? "Error" : "Ok");
    return ret;
}

/* ========================================================================== */
/**
* prv_idle_frm_req()
*/
/* ========================================================================== */
static int prv_idle_frm_req (void *pctx, cl_algs_hdl_t *halgs, cl_in_t *in, cl_out_t *out)
{
    cl_prv_stm_ctx_t *p_ctx = pctx;
    int32 ret = 0;

    mmsdbg(DL_FUNC, "Enter");

    p_ctx->curr = p_ctx->next;

    mmsdbg(DL_FUNC, "Exit %s", ret ? "Error" : "Ok");
    return ret;
}

static const p_cl_state_exe_t prv_state_table[MAX_CL_PRV_STATE][MAX_CL_PRV_CMD] = {
    {dummy_state, prv_nomal, prv_foc, prv_afd, prv_idle_frm_req},
    {normal_abort, dummy_state, normal_to_af, normal_to_afd, prv_frm_req}, //CL_PRV_STATE_NORMAL
    {af_abort, af_to_normal, dummy_state, af_to_afd, focus_frm_req},
    {afd_abort, afd_to_normal, afd_to_af, afd_to_afd, afd_frm_req},
};

/* ========================================================================== */
/**
* prv_exec()
*/
/* ========================================================================== */
static int prv_exec (void *pctx,
                            cl_prv_cmd_t cmd,
                            cl_algs_hdl_t *halgs,
                            cl_in_t *in,
                            cl_out_t *out)
{
    cl_prv_stm_ctx_t *p_ctx = pctx;
    cl_prv_state_t state = p_ctx->curr;

    if ((state >= MAX_CL_PRV_STATE) || (cmd >= MAX_CL_PRV_CMD)) {
        mmsdbg(DL_ERROR, "Out of range parameters");
        return -1;
    }

    return prv_state_table[state][cmd](p_ctx, halgs, in, out);
}

/* ========================================================================== */
/**
* prv_ready()
*/
/* ========================================================================== */
static int prv_ready (void *pctx)
{
    cl_prv_stm_ctx_t *p_ctx = pctx;
    cl_prv_state_t state = p_ctx->curr;

    return (CL_PRV_STATE_IDLE == state) ? 1 : 0;
}
/* ========================================================================== */
/**
* prv_ready()
*/
/* ========================================================================== */
static int prv_config (void *pctx, cl_algs_hdl_t *halgs, cl_cfg_t *cfg)
{
    cl_prv_stm_ctx_t *p_ctx = pctx;
    int ret = 0;

    ret = p_ctx->hndl_afd->config(p_ctx->hndl_afd->private, halgs, cfg);
    ret += p_ctx->hndl_focus->config(p_ctx->hndl_focus->private, halgs, cfg);
    ret += p_ctx->hndl_norm->config(p_ctx->hndl_norm->private, halgs, cfg);

    mmsdbg(DL_FUNC, "Exit %s", ret ? "Error" : "Ok");
    return ret;
}
/* ========================================================================== */
/**
* cl_create_preview_stm()
*/
/* ========================================================================== */
int cl_create_preview_stm (cl_stm_t **prv_hdl)
{
    int ret = -1;
    cl_prv_stm_ctx_t *p_ctx;

    *prv_hdl = osal_calloc(1, sizeof(cl_stm_t));

    if (!*prv_hdl) {
        goto ERROR_2;
    }


    (*prv_hdl)->private = osal_calloc(1, sizeof(cl_prv_stm_ctx_t));

    if (!(*prv_hdl)->private) {
        goto ERROR_1;
    }

    (*prv_hdl)->config = prv_config;
    (*prv_hdl)->exec = prv_exec;
    (*prv_hdl)->ready = prv_ready;

    p_ctx= (*prv_hdl)->private;
    ret = cl_create_normal_stm(&p_ctx->hndl_norm);
    ret = cl_create_afd_stm(&p_ctx->hndl_afd);
    ret = cl_create_focus_stm(&p_ctx->hndl_focus);

    return ret;

ERROR_1:
    osal_free(*prv_hdl);
ERROR_2:
    mmsdbg(DL_ERROR, "Can't allocate memory");
    return ret;
}


/* ========================================================================== */
/**
* cl_destroy_preview_stm()
*/
/* ========================================================================== */
int cl_destroy_preview_stm (cl_stm_t *prv_hdl)
{
    cl_prv_stm_ctx_t *p_ctx = prv_hdl->private;

    cl_destroy_normal_stm(p_ctx->hndl_norm);
    cl_destroy_afd_stm(p_ctx->hndl_afd);
    cl_destroy_focus_stm(p_ctx->hndl_focus);

    osal_free(prv_hdl->private);
    osal_free(prv_hdl);
    return 0;
}

