/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file inc_control_logic.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <osal/osal_stdlib.h>
#include <osal/osal_string.h>
#include <osal/osal_mutex.h>
#include <utils/mms_debug.h>
#include <framerequest/camera/camera_frame_request.h>
#include <configurator/include/configurator.h>

#include <pipe/include/inc.h>
#include <osal/pool.h>
#include <osal/osal_list.h>
#include <osal/osal_assert.h>
#include <error_handle/include/error_handle.h>
#include <guzzi_event/include/guzzi_event.h>
#include <guzzi_event_global/include/guzzi_event_global.h>

#include "inc_control_logic.h"
#include "cl_system_stm_types.h"

#include <fifo/include/fifo.h>
#include <sg_lens.h>

#define CL_POOL_ALGOS_COUNT     (5) // average number of algorithms controlled by frame


#if (MS_TEST_SCENARIO==360)

    #define CL_WAIT_EVT_AE_STAB_READY  CAM_EVT_AE_MERGER_READY
    #define CL_INPUT_AE_STAB_EVENT  geg_camera_event_mk(0xFF, CL_WAIT_EVT_AE_STAB_READY)

    #define CL_WAIT_EVT_AWB_READY  CAM_EVT_AWB_MERGER_READY
    #define CL_INPUT_AWB_EVENT  geg_camera_event_mk(cl_prv->camera_alt_id, CL_WAIT_EVT_AWB_READY)

#else // (MS_TEST_SCENARIO==360)

    #define CL_WAIT_EVT_AE_STAB_READY  CAM_EVT_AE_STAB_READY
    #define CL_INPUT_AE_STAB_EVENT  geg_camera_event_mk(cl_prv->camera_alt_id, CL_WAIT_EVT_AE_STAB_READY)

    #define CL_WAIT_EVT_AWB_READY  CAM_EVT_AWB_READY
    #define CL_INPUT_AWB_EVENT  geg_camera_event_mk(cl_prv->camera_alt_id, CL_WAIT_EVT_AWB_READY)

#endif // (MS_TEST_SCENARIO==360)




mmsdbg_define_variable(
        vdl_inc_control_logic,
        DL_DEFAULT,
        0,
        "inc.control_logic",
        "INC Control Logic"
    );
#define MMSDEBUGLEVEL mmsdbg_use_variable(vdl_inc_control_logic)

void set_aca_awb_default(awb_calc_output_t *awb_out)
{
    awb_out->colour_temp = 6500;
    awb_out->wbal_coef.c.b  = 1.7;
    awb_out->wbal_coef.c.gb = 1.0;
    awb_out->wbal_coef.c.gr = 1.0;
    awb_out->wbal_coef.c.r  = 1.4;

    awb_out->wbal_coef.dgain.gain  = 1.0;
}

void set_aca_focus_default(focus_calc_output_t *focus)
{
    focus->move     = AF_LENS_MOVE_GLOBAL;
    focus->pos      = 0.0;
    focus->seq_cnt  = 0;
    focus->status   = AF_STATE_IDLE;
}

void dump_output(cam_algo_state_t *o)
{
    mmsdbg(DL_MESSAGE, "\n"
            "ae_smooth %d"
            "\ndistib:"
            "exposure %d "
            "again %f "
            "dgain %f "
            "texp %d "
            "sens exp %d "
            "sens again %f "
            "sens dgain %f",
            o->sg.ae_smooth.texp,
            o->sg.ae_distr.exp_gain.exposure,
            o->sg.ae_distr.exp_gain.again,
            o->sg.ae_distr.exp_gain.dgain,
            o->sg.ae_distr.texp,
            o->cam.sensor.exposure,
            o->cam.sensor.again,
            o->cam.sensor.dgain
            );
}

/** inc_control_logic_priv_data_t
* Defining the gzz module application data. */
struct inc_control_logic_priv_data {
    const char *name;
    uint32                  fr_offset_cfg;
    uint32                  fr_offset_cl_img;
    uint32                  fr_offset_cl_aca;
    uint32                  fr_offset_lights;
    uint32                  fr_offset_algo_state;
    uint32                  fr_offset_vpipe;
    uint32                  max_fr;
    uint32                  res_offset_cfg;
    uint32                  res_offset_cametra_id;
    guzzi_event_t           *evt_hdl;
    uint32                  camera_id;
    uint32                  camera_alt_id;
    cl_stm_t                *sys_stm;
    cl_algs_hdl_t           halgs;
    list_pool_t             *sys_fr_poll_hdl;
    pool_t                  *alg_state_poll_hdl;
    pool_t                  *cl_out_poll_hdl;
    fifo_t                  *fifo_sensor_out;
    fifo_t                  *fifo_pipe_out;

    struct {
        osal_mutex          *events_lock;
        aca_algo_out_t       aca_out_rcv;    // used as a placeholder for incoming aca outputs
        virt_cm_sen_mode_features_t sen_features;
        hat_cm_component_ids_t modules_id;
    };

    dtpdb_static_common_t   dtp_static;
    dtpdb_dynamic_common_t  dtp_dyn;
    cam_cfg_t               *active_cfg;

    osal_sem                *sem_cam_detected;
};

uint32 get_aca_exp_default(void)
{
    return 60000;
}

uint32 get_exp_default_from_sensor(inc_control_logic_priv_data_t *prv_dat)
{
    return prv_dat->sen_features.sen->modes.list[prv_dat->sen_features.sen_mode_idx].exposure.max;
}

/**
 * cl_out_queue_t
 *
 */
typedef struct {
    fifo_entry_t            entry;
    cl_out_t                cl_out;
} cl_out_queue_t;

static void map_custom_modes (uint32 *dtp_mode, cam_gzz_cfg_t *cfg)
{
    if (CAM_CAPTURE_INTENT_CUSTOM == cfg->cam_mode.val) {
        switch (cfg->custom_usecase_selection.val) {
            case CAM_CUSTOM_USECASE_SELECTION_STILL:
                *dtp_mode = CAM_CAPTURE_INTENT_STILL_CAPTURE;
                break;
            case CAM_CUSTOM_USECASE_SELECTION_VIDEO:
            case CAM_CUSTOM_USECASE_SELECTION_LOW_POWER_VIDEO:
            default:
                *dtp_mode = CAM_CAPTURE_INTENT_VIDEO_RECORD;
                break;
        }
    } else {
        *dtp_mode = cfg->cam_mode.val;
    }
}

/* ========================================================================== */
/**
* void cl_config_apply()
*/
/* ========================================================================== */
static int cl_config_apply (inc_control_logic_priv_data_t   *cl_prv,
                                cam_cfg_t  *active_cfg, cam_cfg_t *new_cfg)
{
    int ret     = 0;
    cl_in_t     cl_in;
    cl_cfg_t    cl_cfg;
    cl_stm_t    *sys= cl_prv->sys_stm;
    virt_cm_sen_mode_features_t     sen_features;
    hat_cm_component_ids_t modules_id;

    mmsdbg(DL_FUNC, "Enter");

    osal_mutex_lock(cl_prv->events_lock);
    osal_memcpy(&sen_features, &cl_prv->sen_features, sizeof(sen_features));
    osal_memcpy(&modules_id, &cl_prv->modules_id, sizeof(modules_id));
    osal_mutex_unlock(cl_prv->events_lock);
    cl_in.vsen_modes = &sen_features;

    if (is_gzz_cfg_changed(active_cfg, new_cfg, cam_gzz_cfg)) {
        // TODO init gcfg
        cl_prv->dtp_static.camera_id    = modules_id.cm_idx;

        cl_prv->dtp_static.effect       = new_cfg->cam_gzz_cfg.eff_type.val;   // CAM_EFFECT_NONE;
        cl_prv->dtp_static.scene        = new_cfg->cam_gzz_cfg.scene_mode.val; // CAM_SCENE_AUTO;

        map_custom_modes(&cl_prv->dtp_static.mode,
                            &new_cfg->cam_gzz_cfg);

        cl_cfg.dtp_stat = &cl_prv->dtp_static;
        cl_cfg.new_cam_cfg = new_cfg;
        cl_cfg.cur_cam_cfg = active_cfg;

        ret = sys->config(sys->private, &cl_prv->halgs, &cl_cfg);
    }

    if (is_gzz_cfg_changed(active_cfg, new_cfg, cam_gzz_cfg.cam_mode)) {
        switch (new_cfg->cam_gzz_cfg.cam_mode.val) {
        case CAM_CAPTURE_INTENT_PREVIEW:
        case CAM_CAPTURE_INTENT_VIDEO_RECORD:
            sys->exec(sys->private, CL_SYS_CMD_START_PREVIEW,
                        &cl_prv->halgs, &cl_in, NULL);
            break;
        case CAM_CAPTURE_INTENT_CUSTOM:
            sys->exec(sys->private, CL_SYS_CMD_START_CUSTOM,
                        &cl_prv->halgs, &cl_in, NULL);
            break;
        case CAM_CAPTURE_INTENT_STILL_CAPTURE:
            sys->exec(sys->private, CL_SYS_CMD_START_CAPTURE,
                        &cl_prv->halgs, &cl_in, NULL);
            break;
        case CAM_CAPTURE_INTENT_ZERO_SHUTTER_LAG:
        case CAM_CAPTURE_INTENT_VIDEO_SNAPSHOT:
            sys->exec(sys->private, CL_SYS_CMD_START_ZSL,
                        &cl_prv->halgs, &cl_in, NULL);
            break;
        default:
            mmsdbg(DL_ERROR, "Unsupported mode");
            break;
        }
    }

    mmsdbg(DL_FUNC, "Exit %s", ret ? "Error" : "Ok");
    return ret;
}

/* ========================================================================== */
/**
* void generate_fr_output()
*/
/* ========================================================================== */
static void generate_fr_output (cam_algo_state_t *fr,
                                    cl_out_queue_t *pipe_out,
                                    cl_out_queue_t *sens_out,
                                    cl_out_queue_t *cl_out)
{
    //get all parameter from delayed output
    fr->sg = pipe_out->cl_out.sg;
    fr->cam.sensor =
            sens_out->cl_out.sg.ae_distr.exp_gain;
    // put non delayed sensor
    fr->cam.sensor.exposure =
            cl_out->cl_out.sg.ae_distr.exp_gain.exposure;
}

/* ========================================================================== */
/**
* fr_process()
*/
/* ========================================================================== */
static int fr_process (inc_control_logic_priv_data_t   *cl_prv,
                            cl_out_queue_t **fr_out, cam_algo_state_t *algo_state)
{
    cl_stm_t                        *sys    = cl_prv->sys_stm;
    cl_in_t                         cl_in;
    cam_fr_buff_ctrl_list_t         *cl_req_aca;
    cam_fr_buff_ctrl_list_t         *cl_req_img;
    cl_out_queue_t                  *out;
    int                             ret;
    virt_cm_sen_mode_features_t     sen_features;

    // allocate buffers

    cl_req_img      = LIST_POOL_ALLOC_WHILE_TIMEOUT(cl_prv->sys_fr_poll_hdl);
    cl_req_aca      = LIST_POOL_ALLOC_WHILE_TIMEOUT(cl_prv->sys_fr_poll_hdl);
    out             = POOL_ALLOC_WHILE_TIMEOUT(cl_prv->cl_out_poll_hdl);
    osal_memset(out, 0x00, sizeof(*out));

    osal_mutex_lock(cl_prv->events_lock);
    osal_memcpy(&sen_features, &cl_prv->sen_features, sizeof(sen_features));
    osal_mutex_unlock(cl_prv->events_lock);

    cl_prv->dtp_dyn.falsh_ratio   = algo_state->aca.ae_out.flash_light_ratio;
    cl_prv->dtp_dyn.color_temp    = algo_state->aca.awb_out.colour_temp;
    cl_prv->dtp_dyn.obj_distance  = 100;    // TODO should come from lens driver

    cl_in.enb_aca = (cl_prv->camera_id == cl_prv->camera_alt_id);
    cl_in.dtp_dyn   = &cl_prv->dtp_dyn;
    cl_in.ae_out    = &algo_state->aca.ae_out;
    cl_in.ae_stab   = &algo_state->aca.ae_stab;
    cl_in.awb_out   = &algo_state->aca.awb_out;
    cl_in.focus     = &algo_state->aca.focus;
    cl_in.vsen_modes= &sen_features;
    cl_in.afd_out   = &algo_state->aca.afd_out;

    cl_in.pool_alg_ctrl = cl_prv->sys_fr_poll_hdl;

    out->cl_out.cl_req_img = cl_req_img;
    out->cl_out.cl_req_aca = cl_req_aca;

    ret = sys->exec(sys->private, CL_SYS_CMD_FRAME_REQUEST, &cl_prv->halgs, &cl_in, &out->cl_out);

    *fr_out         = out;
    return ret;
}

/* ========================================================================== */
/**
* void inc_cl_evt_notify()
*/
/* ========================================================================== */
void inc_cl_evt_notify (void *inc_prv, int event_id,
                            uint32 num, void *data)
{
    inc_control_logic_priv_data_t *cl_prv = inc_prv;

    if (num) {
        // TODO
        /*
         *  it is a capture sequence events
         *  we will skip it
         */
        return;
    }

    mmsdbg(DL_FUNC, "Enter num: %d evt: %s",  num, guzzi_event_global_id2str(event_id));
    osal_mutex_lock(cl_prv->events_lock);
    switch (geg_camera_event_get_eid(event_id)) {
        case CL_WAIT_EVT_AWB_READY:
            osal_memcpy(&cl_prv->aca_out_rcv.awb_out, data, sizeof(cl_prv->aca_out_rcv.awb_out));
            break;
        case CAM_EVT_AE_READY:
            osal_memcpy(&cl_prv->aca_out_rcv.ae_out, data, sizeof(cl_prv->aca_out_rcv.ae_out));
            break;
        case CL_WAIT_EVT_AE_STAB_READY:
            osal_memcpy(&cl_prv->aca_out_rcv.ae_stab, data, sizeof(cl_prv->aca_out_rcv.ae_stab));
            break;
        case CAM_EVT_AFD_READY:
            osal_memcpy(&cl_prv->aca_out_rcv.afd_out, data, sizeof(cl_prv->aca_out_rcv.afd_out));
            break;
        case CAM_EVT_FMV_READY:
            osal_memcpy(&cl_prv->aca_out_rcv.fmv_out, data, sizeof(cl_prv->aca_out_rcv.fmv_out));
            break;
        case CAM_EVT_AF_READY:
            osal_memcpy(&cl_prv->aca_out_rcv.focus, (void*)(&((aca_focus_output_t *)data)->focus_out), sizeof(cl_prv->aca_out_rcv.focus));

            {
                sg_lens_input_t in;

                in.dtp_d_common = &cl_prv->dtp_dyn;
                in.focus_result = cl_prv->aca_out_rcv.focus;

                sg_lens_thr_process(cl_prv->halgs.sg_lens, &in);
            }
            break;
        case CAM_EVT_SEN_MODE_CHANGE:
            osal_memcpy(&cl_prv->sen_features, data, sizeof(cl_prv->sen_features));
            cl_prv->aca_out_rcv.ae_out.req_texp = get_exp_default_from_sensor(cl_prv);
            cl_prv->aca_out_rcv.ae_stab.texp = get_exp_default_from_sensor(cl_prv);
            break;
        case CAM_EVT_DETECTED_CAMERA:
            osal_memcpy(&cl_prv->modules_id, data, sizeof(cl_prv->modules_id));
            osal_sem_post(cl_prv->sem_cam_detected);
            break;

        default:
            mmsdbg(DL_ERROR, "Unhandled event");
            break;
    }
    osal_mutex_unlock(cl_prv->events_lock);
    mmsdbg(DL_FUNC, "Exit %s", 0 ? "Error" : "Ok");
    return;
}

/* ========================================================================== */
/**
* void cl_uregister_evt()
*/
/* ========================================================================== */
static void cl_unregister_evt (inc_control_logic_priv_data_t   *cl_prv)
{
    guzzi_event_unreg_recipient(cl_prv->evt_hdl, CL_INPUT_AWB_EVENT, inc_cl_evt_notify, cl_prv);
    guzzi_event_unreg_recipient(cl_prv->evt_hdl, geg_camera_event_mk(cl_prv->camera_alt_id, CAM_EVT_AE_READY),  inc_cl_evt_notify, cl_prv);
    guzzi_event_unreg_recipient(cl_prv->evt_hdl, CL_INPUT_AE_STAB_EVENT, inc_cl_evt_notify, cl_prv);
    guzzi_event_unreg_recipient(cl_prv->evt_hdl, geg_camera_event_mk(cl_prv->camera_id, CAM_EVT_SEN_MODE_CHANGE), inc_cl_evt_notify, cl_prv);
    guzzi_event_unreg_recipient(cl_prv->evt_hdl, geg_camera_event_mk(cl_prv->camera_id, CAM_EVT_DETECTED_CAMERA), inc_cl_evt_notify, cl_prv);
    guzzi_event_unreg_recipient(cl_prv->evt_hdl, geg_camera_event_mk(cl_prv->camera_alt_id, CAM_EVT_AFD_READY), inc_cl_evt_notify, cl_prv);
    guzzi_event_unreg_recipient(cl_prv->evt_hdl, geg_camera_event_mk(cl_prv->camera_id, CAM_EVT_FMV_READY), inc_cl_evt_notify, cl_prv);
    guzzi_event_unreg_recipient(cl_prv->evt_hdl, geg_camera_event_mk(cl_prv->camera_id, CAM_EVT_AF_READY), inc_cl_evt_notify, cl_prv);
}

/* ========================================================================== */
/**
* void cl_uregister_evt()
*/
/* ========================================================================== */
static int cl_register_evt (inc_control_logic_priv_data_t   *cl_prv)
{
    if(guzzi_event_reg_recipient(cl_prv->evt_hdl, CL_INPUT_AWB_EVENT,
            inc_cl_evt_notify, cl_prv)) {
        goto ERROR_1;
    }

    if(guzzi_event_reg_recipient(cl_prv->evt_hdl, geg_camera_event_mk(cl_prv->camera_alt_id, CAM_EVT_AE_READY),
            inc_cl_evt_notify, cl_prv)) {
        goto ERROR_2;
    }

    if(guzzi_event_reg_recipient(cl_prv->evt_hdl, CL_INPUT_AE_STAB_EVENT,
            inc_cl_evt_notify, cl_prv)) {
        goto ERROR_3;
    }

    if(guzzi_event_reg_recipient(cl_prv->evt_hdl, geg_camera_event_mk(cl_prv->camera_id, CAM_EVT_SEN_MODE_CHANGE)
            , inc_cl_evt_notify, cl_prv)) {
        goto ERROR_4;
    }

    if(guzzi_event_reg_recipient(cl_prv->evt_hdl, geg_camera_event_mk(cl_prv->camera_alt_id, CAM_EVT_AFD_READY),
            inc_cl_evt_notify, cl_prv)) {
        goto ERROR_5;
    }

    if(guzzi_event_reg_recipient(cl_prv->evt_hdl, geg_camera_event_mk(cl_prv->camera_id, CAM_EVT_FMV_READY),
            inc_cl_evt_notify, cl_prv)) {
        goto ERROR_6;
    }

    if(guzzi_event_reg_recipient(cl_prv->evt_hdl, geg_camera_event_mk(cl_prv->camera_id, CAM_EVT_AF_READY),
            inc_cl_evt_notify, cl_prv)) {
        goto ERROR_7;
    }

    if(guzzi_event_reg_recipient(cl_prv->evt_hdl, geg_camera_event_mk(cl_prv->camera_id, CAM_EVT_DETECTED_CAMERA),
            inc_cl_evt_notify, cl_prv)) {
        goto ERROR_8;
    }

    return 0;

ERROR_8:
    guzzi_event_unreg_recipient(cl_prv->evt_hdl, geg_camera_event_mk(cl_prv->camera_id,CAM_EVT_AF_READY), inc_cl_evt_notify, cl_prv);

ERROR_7:
    guzzi_event_unreg_recipient(cl_prv->evt_hdl, geg_camera_event_mk(cl_prv->camera_id,CAM_EVT_FMV_READY), inc_cl_evt_notify, cl_prv);

ERROR_6:
    guzzi_event_unreg_recipient(cl_prv->evt_hdl, geg_camera_event_mk(cl_prv->camera_alt_id,CAM_EVT_AFD_READY), inc_cl_evt_notify, cl_prv);

ERROR_5:
    guzzi_event_unreg_recipient(cl_prv->evt_hdl, geg_camera_event_mk(cl_prv->camera_id,CAM_EVT_SEN_MODE_CHANGE), inc_cl_evt_notify, cl_prv);

ERROR_4:
    guzzi_event_unreg_recipient(cl_prv->evt_hdl, CL_INPUT_AE_STAB_EVENT, inc_cl_evt_notify, cl_prv);

ERROR_3:
    guzzi_event_unreg_recipient(cl_prv->evt_hdl, geg_camera_event_mk(cl_prv->camera_alt_id,CAM_EVT_AE_READY), inc_cl_evt_notify, cl_prv);

ERROR_2:
    guzzi_event_unreg_recipient(cl_prv->evt_hdl, geg_camera_event_mk(cl_prv->camera_alt_id,CAM_EVT_AWB_READY), inc_cl_evt_notify, cl_prv);

ERROR_1:
    return -1;
}
/* ========================================================================== */
/**
* inc_control_logic_start()
*/
/* ========================================================================== */
static int inc_control_logic_start(inc_t *inc, void *params)
{
    int err;
    inc_control_logic_priv_data_t   *cl_prv = inc->prv.ptr;
    cl_out_queue_t                  *cl_out;
    cam_algo_state_t                cl_algo_state;
    uint32                          delay_exp_gain, delay_global;

    mmsdbg(DL_FUNC, "Enter");

    err = osal_sem_wait_timeout(cl_prv->sem_cam_detected, DEFAULT_ALLOC_TIMEOUT_MS);
    if (err) {
        mmsdbg(DL_ERROR, "Camera detect timeout %d ms", DEFAULT_ALLOC_TIMEOUT_MS);
    }
    osal_assert(!err);

    cl_prv->active_cfg = INC_RES_GET_PTR(params, cl_prv->res_offset_cfg);
    configurator_config_lock(cl_prv->active_cfg);

    sg_lens_start(cl_prv->halgs.sg_lens);
    cl_config_apply(cl_prv, NULL, cl_prv->active_cfg);


    osal_mutex_lock(cl_prv->events_lock);
    delay_exp_gain = cl_prv->sen_features.sen->exp_gain_delay;
    delay_global = cl_prv->sen_features.sen->global_delay;
    osal_memcpy(&cl_algo_state.aca, &cl_prv->aca_out_rcv, sizeof(cl_prv->aca_out_rcv));
    osal_mutex_unlock(cl_prv->events_lock);

    if (fifo_is_empty(cl_prv->fifo_sensor_out)) {
        while (delay_exp_gain--) {
            fr_process(cl_prv, &cl_out, &cl_algo_state);
            fifo_put(cl_prv->fifo_sensor_out, &cl_out->entry);
        }
    }

    if (fifo_is_empty(cl_prv->fifo_pipe_out)) {
        while (delay_global--) {
            fr_process(cl_prv, &cl_out, &cl_algo_state);
            fifo_put(cl_prv->fifo_pipe_out, &cl_out->entry);
        }
    }

    mmsdbg(DL_FUNC, "Exit Ok");

    return 0;
}

/* ========================================================================== */
/**
* inc_control_logic_stop()
*/
/* ========================================================================== */
static void inc_control_logic_stop(inc_t *inc)
{
    inc_control_logic_priv_data_t   *cl_prv = inc->prv.ptr;
    cl_stm_t                        *sys= cl_prv->sys_stm;

    mmsdbg(DL_FUNC, "Enter");

    sg_lens_stop(cl_prv->halgs.sg_lens);
    if (cl_prv->active_cfg) {
        configurator_config_unlock(cl_prv->active_cfg);
    }

    sys->exec(sys->private,  CL_SYS_CMD_ABORT_CURRENT, &cl_prv->halgs, NULL, NULL);

    mmsdbg(DL_FUNC, "Exit Ok");
}

/* ========================================================================== */
/**
* inc_control_logic_flush()
*/
/* ========================================================================== */
static void inc_control_logic_flush(inc_t *inc)
{
    inc_control_logic_priv_data_t   *cl_prv;
    cl_out_queue_t                  *out;

    mmsdbg(DL_FUNC, "Enter");

    GOTO_EXIT_IF(!inc, 1);
    cl_prv = inc->prv.ptr;
    GOTO_EXIT_IF(!cl_prv, 1);

    while(!fifo_is_empty(cl_prv->fifo_sensor_out)) {
        out = FIFO_ENTRY(fifo_get(cl_prv->fifo_sensor_out),
                cl_out_queue_t,
                entry);
        osal_free(out->cl_out.cl_req_img);
        osal_free(out->cl_out.cl_req_aca);
        osal_free(out->cl_out.vpipe);
        osal_free(out);
    }

    while(!fifo_is_empty(cl_prv->fifo_pipe_out)) {
        out = FIFO_ENTRY(fifo_get(cl_prv->fifo_pipe_out),
                cl_out_queue_t,
                entry);
        osal_free(out->cl_out.cl_req_img);
        osal_free(out->cl_out.cl_req_aca);
        osal_free(out->cl_out.vpipe);
        osal_free(out);
    }

EXIT_1:
    mmsdbg(DL_FUNC, "Exit Ok");
}

/* ========================================================================== */
/**
* inc_control_logic_config()
*/
/* ========================================================================== */
static int inc_control_logic_config(inc_t *inc, void *data)
{
    inc_control_logic_priv_data_t   *cl_prv = inc->prv.ptr;
    camera_fr_t                     *pFR = data;
    cam_cfg_t                       *new_cfg, *active_cfg = cl_prv->active_cfg;

    PROFILE_ADD(PROFILE_ID_SYS_CL_PROCESS, 0xBEBE, 0);
    mmsdbg(DL_FUNC, "Enter");
    new_cfg = CAMERA_FR_GET_ENTRY(pFR, cl_prv->fr_offset_cfg)->data;
    configurator_config_lock(new_cfg);

    cl_config_apply (cl_prv, active_cfg, new_cfg);

    configurator_config_unlock(cl_prv->active_cfg);
    cl_prv->active_cfg = new_cfg;

    mmsdbg(DL_FUNC, "Exit Ok");
    PROFILE_ADD(PROFILE_ID_SYS_CL_PROCESS, 0xEDED, 0);
    return 0;
}

/* ========================================================================== */
/**
* control_logic_process()
*/
/* ========================================================================== */
static int _control_logic_process(inc_t *inc, void *data)
{
    inc_control_logic_priv_data_t   *cl_prv = inc->prv.ptr;
    camera_fr_t                     *pFR    = data;
    cam_algo_state_t                *cl_algo_state;
    camera_fr_entry_t               *cl_fr_entry;
    cl_out_queue_t                  *cl_out, *pipe_out, *sens_out;
    int ret = 0;

    mmsdbg(DL_FUNC, "Enter");

    // cl_algo_state = &cl_prv->TMP_algo_state;
    // g_frame_request.p_cam_algo_state = cl_algo_state;   // TODO should be moved to FR creation

    cl_algo_state   = POOL_ALLOC_WHILE_TIMEOUT(cl_prv->alg_state_poll_hdl);
    // get latest data from ACA
    cl_algo_state->fr_status = 0;
    osal_mutex_lock(cl_prv->events_lock);
    osal_memcpy(&cl_algo_state->aca, &cl_prv->aca_out_rcv, sizeof(cl_prv->aca_out_rcv));
    osal_mutex_unlock(cl_prv->events_lock);

    fr_process(cl_prv, &cl_out, cl_algo_state);
    if (CL_SEQ_STAT_END == cl_out->cl_out.seq_status) {
        fifo_put(cl_prv->fifo_sensor_out, &cl_out->entry);
        sens_out = FIFO_ENTRY(fifo_get(cl_prv->fifo_sensor_out),
                                    cl_out_queue_t,
                                    entry);

        fifo_put(cl_prv->fifo_pipe_out, &sens_out->entry);
        pipe_out = FIFO_ENTRY(fifo_get(cl_prv->fifo_pipe_out),
                                    cl_out_queue_t,
                                    entry);
    } else {
        pipe_out = sens_out = cl_out; // for capture we don't want any delay
    }
    generate_fr_output (cl_algo_state, pipe_out, sens_out, cl_out);

    cl_fr_entry =   CAMERA_FR_GET_ENTRY(pFR, cl_prv->fr_offset_vpipe);
    cl_fr_entry->data = pipe_out->cl_out.vpipe;
    cl_fr_entry->com.fmt = CAMERA_FR_ENTRY_FORMAT__OSAL;

    //insert algo states in frame request
    cl_fr_entry = CAMERA_FR_GET_ENTRY(pFR, cl_prv->fr_offset_algo_state);
    cl_fr_entry->data = cl_algo_state;
    cl_fr_entry->com.fmt = CAMERA_FR_ENTRY_FORMAT__OSAL;

    // insert CL sequence data in frame request
    cl_fr_entry = CAMERA_FR_GET_ENTRY(pFR, cl_prv->fr_offset_cl_img);
    cl_fr_entry->data = pipe_out->cl_out.cl_req_img;
    cl_fr_entry->com.fmt = CAMERA_FR_ENTRY_FORMAT__OSAL;

    // insert CL sequence data in frame request
    cl_fr_entry = CAMERA_FR_GET_ENTRY(pFR, cl_prv->fr_offset_cl_aca);
    cl_fr_entry->data = pipe_out->cl_out.cl_req_aca;
    cl_fr_entry->com.fmt = CAMERA_FR_ENTRY_FORMAT__OSAL;

    if (cl_out->cl_out.light)
    {
        cl_fr_entry = CAMERA_FR_GET_ENTRY(pFR, cl_prv->fr_offset_lights);
        cl_fr_entry->data = cl_out->cl_out.light;
        cl_fr_entry->com.fmt = CAMERA_FR_ENTRY_FORMAT__OSAL;
    }

    {
        hat_exp_gain_t *sens = &cl_algo_state->cam.sensor;
        PROFILE_ADD(PROFILE_ID_ALGOS_CL_EXP_GAIN, sens->exposure, (sens->again*100));
    }

    ret = (cl_out->cl_out.seq_status == CL_SEQ_STAT_PROGRESS);

    osal_free(pipe_out);

    dump_output(cl_algo_state);

    return ret;
}

#ifdef GZZ_FLASH_SIM
// TODO: remove me
uint32 dbg_frame_duration = 50;
#endif
static void inc_control_logic_process(inc_t *inc, void *data)
{
//    inc_control_logic_priv_data_t   *cl_prv = inc->prv.ptr;
    camera_fr_t *pFR = data;
    camera_fr_t *ins_FR;
    PROFILE_ADD(PROFILE_ID_SYS_CL_PROCESS, 0xBBBB, 0);
    while (_control_logic_process(inc, pFR))
    {
        ins_FR = camera_fr_duplicate(pFR);
        inc->callback(inc, inc->client_prv, 0, 0, pFR);
        pFR = ins_FR;
    }

#ifdef GZZ_FLASH_SIM
    {// TODO: remove me
        static osal_timeval tv_old;
        osal_timeval tv_now;

        osal_get_timeval(&tv_now);

        dbg_frame_duration = tv_now - tv_old;

        tv_old = tv_now;
    }
#endif

    PROFILE_ADD(PROFILE_ID_SYS_CL_PROCESS, 0xEEEE, 0);
    inc->callback(inc, inc->client_prv, 0, 0, pFR);
    mmsdbg(DL_FUNC, "Exit Ok");
}
/* ========================================================================== */
/**
* inc_control_logic_destroy()
*/
/* ========================================================================== */
static void inc_control_logic_destroy(inc_t *inc)
{
    inc_control_logic_priv_data_t   *cl_prv;

    mmsdbg(DL_FUNC, "Enter");

    GOTO_EXIT_IF(!inc, 1);
    cl_prv = inc->prv.ptr;
    GOTO_EXIT_IF(!cl_prv, 1);

    cl_unregister_evt(cl_prv);

    fifo_destroy(cl_prv->fifo_pipe_out);
    fifo_destroy(cl_prv->fifo_sensor_out);
    pool_destroy(cl_prv->cl_out_poll_hdl);
    list_pool_destroy(cl_prv->sys_fr_poll_hdl);
    pool_destroy(cl_prv->alg_state_poll_hdl);

    osal_mutex_destroy(cl_prv->events_lock);
    osal_sem_destroy(cl_prv->sem_cam_detected);
    cl_algo_sg_algs_destroy(&cl_prv->halgs);
    cl_destroy_system_stm(cl_prv->sys_stm);
    osal_free(inc->prv.ptr);
    return;
EXIT_1:
    mmsdbg(DL_ERROR, "Exit ERROR");
}

/* ========================================================================== */
/**
* inc_control_logic_create()
*/
/* ========================================================================== */
int inc_control_logic_create(
        inc_t *inc,
        void *params,
        void *app_res
    )
{
    inc_control_logic_priv_data_t *cl_prv;
    INC_CONTROL_LOGIC_CPARAMS_T *pStaticParams;
    int ret;

    mmsdbg(DL_FUNC, "Enter");

    pStaticParams = params;

    cl_prv = osal_malloc(sizeof(inc_control_logic_priv_data_t));
    if (!cl_prv) {
        goto EXIT_1;
    }

    osal_memset(cl_prv, 0, sizeof(*cl_prv));

    cl_prv->name = "Inc CL prv";
    cl_prv->fr_offset_cfg = pStaticParams->fr_offset_cfg;
    cl_prv->fr_offset_cl_img  = pStaticParams->fr_offset_cl_img;
    cl_prv->fr_offset_cl_aca  = pStaticParams->fr_offset_cl_aca;
    cl_prv->fr_offset_lights  = pStaticParams->fr_offset_lights;
    cl_prv->fr_offset_algo_state = pStaticParams->fr_offset_algo_state;
    cl_prv->fr_offset_vpipe  = pStaticParams->fr_offset_vpipe;
    cl_prv->max_fr            = pStaticParams->max_fr;
    cl_prv->res_offset_cfg = pStaticParams->res_offset_cfg;
    cl_prv->res_offset_cametra_id = pStaticParams->res_offset_cametra_id;

    cl_prv->evt_hdl = guzzi_event_global();
    cl_prv->camera_id = INC_RES_GET_UINT(app_res, cl_prv->res_offset_cametra_id);
    cl_prv->camera_alt_id = cl_prv->camera_id;

#if (MS_TEST_SCENARIO==1)     // MSS
    cl_prv->camera_alt_id = 0;
#elif (MS_TEST_SCENARIO==2)     // XMS
    if (cl_prv->camera_id == 2) {
        cl_prv->camera_alt_id = 1;
    }
#elif (MS_TEST_SCENARIO==3)     // MSMSMS... Every odd camera will be slave to even
    if (cl_prv->camera_id & 1) { // Check for odd camera index
        cl_prv->camera_alt_id = cl_prv->camera_id - 1;
    }
#endif

    cl_prv->events_lock = osal_mutex_create();
    if (!cl_prv->events_lock) {
        goto EXIT_2;
    }

    cl_prv->sem_cam_detected = osal_sem_create(0);
    if (!cl_prv->sem_cam_detected) {
        goto EXIT_3;
    }

    if (cl_register_evt(cl_prv)) {
        goto EXIT_3;
    }

    cl_algs_create_params_t algo_params;
    algo_params.evt_hndl = cl_prv->evt_hdl;
    algo_params.camera_id = cl_prv->camera_id;
    algo_params.max_fr = cl_prv->max_fr;
    ret = cl_algo_sg_algs_create(&cl_prv->halgs, &algo_params);
    if(ret) {
        goto EXIT_4;
    }

    cl_system_create_params_t stm_params;
    stm_params.evt_hndl = cl_prv->evt_hdl;
    stm_params.camera_id = cl_prv->camera_id;
    ret = cl_create_system_stm(&cl_prv->sys_stm, &stm_params);
    if (ret) {
        goto EXIT_5;
    }

    cl_prv->aca_out_rcv.ae_out.req_texp = get_aca_exp_default();
    cl_prv->aca_out_rcv.ae_stab.texp = get_aca_exp_default();

    set_aca_awb_default(&cl_prv->aca_out_rcv.awb_out);
    set_aca_focus_default(&cl_prv->aca_out_rcv.focus);

    inc->prv.ptr = cl_prv;

    cl_prv->sys_fr_poll_hdl = list_pool_create("CL FR LIST POOL",
                                               (2*cl_prv->max_fr), // 2 control lists img and aca
                                               sizeof(cam_cl_algo_ctrl_list_t),
                                               offsetof(cam_cl_algo_ctrl_list_t, link),
                                               cl_prv->max_fr*CL_POOL_ALGOS_COUNT);
    if (!cl_prv->sys_fr_poll_hdl) {
        goto EXIT_6;
    }

    cl_prv->alg_state_poll_hdl = pool_create("CL ALG STATE", sizeof(cam_algo_state_t),
                                             cl_prv->max_fr);
    if (!cl_prv->alg_state_poll_hdl) {
        goto EXIT_7;
    }

    cl_prv->cl_out_poll_hdl = pool_create("CL OUT QUEUE", sizeof(cl_out_queue_t),
                                          cl_prv->max_fr);
    if (!cl_prv->cl_out_poll_hdl) {
        goto EXIT_8;
    }

    cl_prv->fifo_sensor_out = fifo_create();
    if (!cl_prv->fifo_sensor_out) {
        goto EXIT_9;
    }

    cl_prv->fifo_pipe_out = fifo_create();
    if (!cl_prv->fifo_pipe_out) {
        goto EXIT_10;
    }

    inc->inc_start = inc_control_logic_start;
    inc->inc_stop = inc_control_logic_stop;
    inc->inc_flush = inc_control_logic_flush;
    inc->inc_config_alter = NULL;
    inc->inc_config = inc_control_logic_config;
    inc->inc_process = inc_control_logic_process;
    inc->inc_destroy = inc_control_logic_destroy;

    mmsdbg(DL_FUNC, "Exit Ok");
    return 0;

EXIT_10:
    fifo_destroy(cl_prv->fifo_sensor_out);
EXIT_9:
    pool_destroy(cl_prv->cl_out_poll_hdl);
EXIT_8:
    pool_destroy(cl_prv->alg_state_poll_hdl);
EXIT_7:
    list_pool_destroy(cl_prv->sys_fr_poll_hdl);
EXIT_6:
    cl_destroy_system_stm(cl_prv->sys_stm);
EXIT_5:
    cl_algo_sg_algs_destroy(&cl_prv->halgs);
EXIT_4:
    cl_unregister_evt(cl_prv);
EXIT_3:
    osal_mutex_destroy(cl_prv->events_lock);
EXIT_2:
    osal_free(cl_prv);
EXIT_1:
    mmsdbg(DL_ERROR, "Exit Err");
    return -1;
}

