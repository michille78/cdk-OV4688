/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file aca_ae_merger_thread.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <osal/osal_stdlib.h>
#include <osal/osal_string.h>
#include <osal/osal_mutex.h>
#include <osal/osal_assert.h>
#include <utils/mms_debug.h>
#include <error_handle/include/error_handle.h>
#include <cam_cl_frame_req.h>
#include <func_thread/include/func_thread.h>
#include <guzzi_event/include/guzzi_event.h>
#include <guzzi_event_global/include/guzzi_event_global.h>
#include <osal/ex_pool.h>
#include <aca_ae_merger_thread.h>

mmsdbg_define_variable(
        vdl_thr_algo_ae_merger,
        (DL_DEFAULT),
        0,
        "thr.algo_ae_merger",
        "THR ALGO AE_MERGER"
    );

#define MMSDEBUGLEVEL mmsdbg_use_variable(vdl_thr_algo_ae_merger)

#define AE_MERGER_THREAD_STACK_SIZE   (10 * 1024)
#define AE_MERGER_THREAD_PRIORITY     (3)
#define AE_MERGER_THREAD_TIMEOUT      (300)
#define AE_MERGER_NUM_POOL_ENTRIES    (3)


struct ae_merger_common_data {
    func_thread_t               *ae_merger_thr;
    guzzi_event_t               *evt_hndl;
    aca_ae_merger_hndl_t        alg_hndl;
    osal_sem                    *sem;
    ex_pool_t                   *pool_ae_merger;
};

#define MAX_AE_MERGE_CAMERAS    6

static uint32 g_ae_merger_weights[MAX_AE_MERGE_CAMERAS];


typedef struct
{
    int num_total;
    int num_valid;
    per_camera_ae_merger_data_t *in_data;
} ae_merger_calc_data_t;

ex_pool_node_t ae_merger_desc [] = {
     EX_POOL_NODE_DESC(NULL, (sizeof(per_camera_ae_merger_data_t) * MAX_AE_MERGE_CAMERAS), ae_merger_calc_data_t, in_data),
     EX_POOL_LIST_END
};

ex_pool_node_t ae_merger_root =
{
     EX_POOL_ROOT(&ae_merger_desc, sizeof (ae_merger_calc_data_t))
};

struct {
    int ref_counter;
    struct ae_merger_common_data g_ctx;
 
    ae_merger_calc_data_t *entry;
    ae_merger_calc_data_t *for_process_entry;
}g_ae_merger;

struct ae_merger_priv_data
{
    uint32  camera_id;
    struct ae_merger_common_data *pctx;
//    aca_ae_merger_config_t       cfg;
};

static void ae_merger_init_entry(ae_merger_calc_data_t * e)
{
int i;
    e->num_total = MAX_AE_MERGE_CAMERAS;
    e->num_valid = 0;
    for (i = 0; i < MAX_AE_MERGE_CAMERAS; ++i) {
        e->in_data[i].valid = 0;
    }
}
void ae_merger_prepare_for_processing(ae_merger_calc_data_t *for_process_entry)
{
    int i;

    for (i = 0; i < for_process_entry->num_total; ++i)
    {
        for_process_entry->in_data[i].weight = g_ae_merger_weights[i];
    }
}

int ae_merger_add_data(ae_merger_priv_data_t *prv, ae_stab_output_t *data)
{
int ret = 0;
ae_merger_calc_data_t *e = NULL;
per_camera_ae_merger_data_t *c;


    if ((g_ae_merger.ref_counter > 0) && (NULL != prv) && (prv->camera_id < MAX_AE_MERGE_CAMERAS))
    {

        if (NULL == g_ae_merger.entry)
        {
            g_ae_merger.entry = ex_pool_alloc_timeout(g_ae_merger.g_ctx.pool_ae_merger, AE_MERGER_THREAD_TIMEOUT);
            if (NULL == g_ae_merger.entry)
            {
                mmsdbg(DL_ERROR, "Pool timed out");
                return 0;
            }
            ae_merger_init_entry(g_ae_merger.entry);
        }
        e = g_ae_merger.entry;
        c = &e->in_data[prv->camera_id];

        osal_memcpy(&c->v, data, sizeof(ae_stab_output_t));
        if (c->valid)
        {
            mmsdbg(DL_ERROR, "Camera instance %d too slow", (int)prv->camera_id);
        } else {
            c->valid = 1;
            e->num_valid++;
        }

        if (e->num_valid >= g_ae_merger.ref_counter)
        {
            if (NULL == g_ae_merger.for_process_entry)
            {
                g_ae_merger.for_process_entry = g_ae_merger.entry;
                ae_merger_prepare_for_processing(g_ae_merger.for_process_entry);
                ret = 1;
            } else {
                mmsdbg(DL_ERROR, "Algorithm too slow");
                osal_free(g_ae_merger.entry);
            }
            g_ae_merger.entry = NULL;
        }
    } else
    {
        mmsdbg(DL_ERROR, "Internal error");
    }

    return ret;
}



void ae_merger_thread_exec(func_thread_t *ethr, void *prv);

static func_thread_handle_t ae_merger_thread_fxns[] = {
    ae_merger_thread_exec,
};

static func_thread_handles_t ae_merger_thread_handles = {
    .fxns = ae_merger_thread_fxns,
    .size = ARRAY_SIZE(ae_merger_thread_fxns)
};

void ae_merger_thread_exec(func_thread_t *ethr, void *prv)
{

    ae_merger_priv_data_t      *p = prv;

    mmsdbg(DL_FUNC, "AE Merger Exec");
    //    PROFILE_ADD(PROFILE_ID_ALGOS_AWB_MERGER_RUN, 0, 0);
    
    if((!p) || (!p->pctx)){
        mmsdbg(DL_ERROR, "Null pointer");
        return;
    }
 
    {
        aca_ae_merger_calc_output_t  out_ae_merger;
        aca_ae_merger_calc_input_t   in_ae_merger;

        in_ae_merger.num_entries = g_ae_merger.for_process_entry->num_total;
        in_ae_merger.p_in_data   = g_ae_merger.for_process_entry->in_data;

        aca_ae_merger_process(p->pctx->alg_hndl, &in_ae_merger, &out_ae_merger);

        guzzi_event_send(
                p->pctx->evt_hndl,
                geg_camera_event_mk(0xFF, CAM_EVT_AE_MERGER_READY),
                0,
                &out_ae_merger.out_data
            );
    }
    osal_free(g_ae_merger.for_process_entry);
    g_ae_merger.for_process_entry = NULL;
}

/* ========================================================================== */
/**
* aca_ae_merger_thr_process()
*/
/* ========================================================================== */
int aca_ae_merger_thr_process(
                                ae_merger_priv_data_t *prv,
                                ae_stab_output_t      *in_data
                             )
{
    if (ae_merger_add_data(prv, in_data))
    {
        func_thread_exec(prv->pctx->ae_merger_thr, ae_merger_thread_exec);
    }
    return 0;
}

int aca_ae_merger_thr_start(ae_merger_priv_data_t* prv)
{
    return 0;
}

int aca_ae_merger_thr_stop(ae_merger_priv_data_t* prv)
{
    return 0;
}

int aca_ae_merger_thr_config(ae_merger_priv_data_t* prv, void *new_cfg)
{
    aca_ae_merger_config_t *cfg = new_cfg;

    g_ae_merger_weights[prv->camera_id]  = cfg->weight;

    return 0;
}


////////////////////////////////////////////////////////////////////////////////////


void aca_ae_merger_evt_notify_callback (void *inc_prv, int event_id,
                            uint32 num, void *data)
{
    ae_merger_priv_data_t *ae_merger_prv = inc_prv;

    if (num) {
        // TODO
        /*
         *  it is a non ZSL capture sequence events
         *  we will skip it
         */
        return;
    }

    mmsdbg(DL_FUNC, "Enter num: %d evt: %s",  num, guzzi_event_global_id2str(event_id));

    if (osal_sem_wait_timeout(ae_merger_prv->pctx->sem, AE_MERGER_THREAD_TIMEOUT))
    {
        mmsdbg(DL_ERROR, "Timed out");
        return;
    }

    switch (geg_camera_event_get_eid(event_id)) {
            case CAM_EVT_AE_STAB_READY:
                aca_ae_merger_thr_process(ae_merger_prv, data);
                break;
    }
    osal_sem_post(ae_merger_prv->pctx->sem);
    mmsdbg(DL_FUNC, "Exit %s", 0 ? "Error" : "Ok");
    return;
}


static void ae_merger_unregister_evt (ae_merger_priv_data_t   *ae_merger_prv)
{
    guzzi_event_unreg_recipient(ae_merger_prv->pctx->evt_hndl,
                                geg_camera_event_mk(ae_merger_prv->camera_id, CAM_EVT_AE_STAB_READY),
                                aca_ae_merger_evt_notify_callback,
                                ae_merger_prv);
}

/* ========================================================================== */
/**
* void cl_uregister_evt()
*/
/* ========================================================================== */
static int ae_merger_register_evt (ae_merger_priv_data_t   *ae_merger_prv)
{
    if(guzzi_event_reg_recipient(ae_merger_prv->pctx->evt_hndl,
                                 geg_camera_event_mk(ae_merger_prv->camera_id, CAM_EVT_AE_STAB_READY),
                                 aca_ae_merger_evt_notify_callback,
                                 ae_merger_prv))
    {
        goto ERROR_1;
    }

    return 0;

ERROR_1:
    return -1;
}

ae_merger_priv_data_t* aca_ae_merger_thr_create(ae_merger_thread_create_params_t *params)
 {
    ae_merger_priv_data_t *prv;
    struct ae_merger_common_data *pctx;

    prv = osal_calloc(1, sizeof(*prv));
    osal_assert(prv != NULL);

    pctx = prv->pctx = &g_ae_merger.g_ctx;
    prv->camera_id = params->camera_id;

    if (g_ae_merger.ref_counter == 0)
    {
        mmsdbg(DL_ERROR, "AE Merger: First camera instance %d", (int)params->camera_id);

        osal_memset(&g_ae_merger.g_ctx, 0, sizeof(g_ae_merger.g_ctx));

        pctx->evt_hndl = params->evt_hndl;
        pctx->sem = osal_sem_create(1);
        osal_assert(pctx->sem != NULL);

        pctx->ae_merger_thr = func_thread_create("ACA AE_MERGER THREAD",
                                                  AE_MERGER_THREAD_STACK_SIZE,
                                                  AE_MERGER_THREAD_PRIORITY,
                                                 &ae_merger_thread_handles,
                                                  prv);
        osal_assert(pctx->ae_merger_thr != NULL);

        pctx->pool_ae_merger = ex_pool_create("CAM AE MERGER POOL", &ae_merger_root, AE_MERGER_NUM_POOL_ENTRIES);
        osal_assert(pctx->pool_ae_merger != NULL);

        aca_ae_merger_create(&pctx->alg_hndl);
        osal_assert(pctx->alg_hndl != NULL);

        g_ae_merger.entry = NULL;
        g_ae_merger.for_process_entry = NULL;

    }
    ae_merger_register_evt(prv);
    g_ae_merger.ref_counter++;
    return prv;
 }

void aca_ae_merger_thr_destroy(ae_merger_priv_data_t* prv)
{

    if ((g_ae_merger.ref_counter) && (prv) && (prv->pctx))
    {
        ae_merger_unregister_evt(prv);
        g_ae_merger.ref_counter--;
        if (g_ae_merger.ref_counter == 0)
        {
            mmsdbg(DL_ERROR, "AE merger: Last camera instance");

            if (prv->pctx->pool_ae_merger)
            {
                if (g_ae_merger.entry)
                {
                    osal_free(g_ae_merger.entry);
                }
                g_ae_merger.entry = NULL;

                if (g_ae_merger.for_process_entry)
                {
                    mmsdbg(DL_ERROR, "ERROR: for_process_entry is still valid");
                    osal_free(g_ae_merger.for_process_entry);
                }
                g_ae_merger.for_process_entry = NULL;

                ex_pool_destroy(prv->pctx->pool_ae_merger);
                prv->pctx->pool_ae_merger = NULL;
            }
            func_thread_destroy(prv->pctx->ae_merger_thr);
            osal_sem_destroy(prv->pctx->sem);
            aca_ae_merger_destroy(prv->pctx->alg_hndl);
        }
        osal_free(prv);
    } else {
        mmsdbg(DL_ERROR, "Invalid instance");
    }
}

