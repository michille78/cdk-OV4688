/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file inc_auto_camera_algs.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <osal/osal_stdlib.h>
#include <osal/osal_string.h>
#include <osal/osal_list.h>
#include <osal/osal_assert.h>
#include <utils/mms_debug.h>
#include <pipe/include/inc.h>
#include <framerequest/camera/camera_frame_request.h>
#include <error_handle/include/error_handle.h>
#include <osal/ex_pool.h>
#include <osal/list_pool.h>
#include <guzzi_event/include/guzzi_event.h>
#include <guzzi_event_global/include/guzzi_event_global.h>
#include <cam_config.h>
#include <configurator/include/configurator.h>

#include "inc_auto_camera_algs.h"
#include "aca_afd.h"
#include "aca_af_types.h"
#include "cam_cl_frame_req.h"
#include "cam_algo_state.h"
#include "camera/vcamera_iface/vpipe_control/include/vpipe_control.h"
#include "aca_awb_thread.h"
#include "aca_ae_thread.h"
#include "aca_afd_thread.h"
#include "aca_fmv_thread.h"
#include "aca_af_thread.h"

#if (MS_TEST_SCENARIO==360)
    #include "aca_ae_merger_thread.h"
    #include "aca_awb_merger_thread.h"
#endif // (MS_TEST_SCENARIO==360)


mmsdbg_define_variable(
        vdl_inc_auto_camera_algs,
        (DL_DEFAULT),
        0,
        "inc.auto_camera_algs",
        "INC Auto Camera Algs"
    );
#define MMSDEBUGLEVEL mmsdbg_use_variable(vdl_inc_auto_camera_algs)

/** inc_auto_cam_algs_priv_data_t
* Defining the gzz module application data.
*
*/
struct inc_auto_cam_algs_priv_data {
    const char          *name;
    uint32              fr_offset_cfg;
    uint32              fr_cl_aca_offset;
    uint32              fr_offset_aewb_stats;
    uint32              fr_offset_af_stats;
    uint32              fr_offset_algo_state;
    uint32              fr_offset_vpipe;
    uint32              max_fr;
    uint32              res_offset_cfg;
    uint32              res_offset_cametra_id;

    af_thread_data_t    *af_data;
    ae_thread_data_t    *ae_data;
    awb_thread_data_t   *awb_data;
    afd_thread_data_t   *afd_data;
    fmv_thread_data_t   *fmv_data;
#if (MS_TEST_SCENARIO==360)
    ae_merger_priv_data_t   *ae_merger_data;
    awb_merger_priv_data_t  *awb_merger_data;
#endif // (MS_TEST_SCENARIO==360)

    osal_mutex          *lock_cfg;
    cam_cfg_t           *active_cfg;

    guzzi_event_t       *evt_hndl;
    uint32              camera_id;
    struct {
        osal_mutex                  *events_lock;
        virt_cm_sen_mode_features_t sen_features;
        hat_size_t                  full_size;
        hat_cm_component_ids_t      modules_id;
    };

    dtpdb_static_common_t   dtp_scommn_keys;

    osal_sem                *sem_cam_detected;
};

/* ========================================================================== */
/**
* void _aca_evt_notify()
*/
/* ========================================================================== */
void _aca_evt_notify (void *_prv, int event_id,
                            uint32 num, void *data)
{
    inc_auto_cam_algs_priv_data_t *prv = _prv;

    mmsdbg(DL_FUNC, "Enter num: %d evt: %s",  num, guzzi_event_global_id2str(event_id));

    osal_mutex_lock(prv->events_lock);

    switch (geg_camera_event_get_eid(event_id)) {
    case CAM_EVT_SEN_MODE_CHANGE:
        osal_memcpy(&prv->sen_features, data, sizeof(prv->sen_features));
        break;
    case CAM_EVT_DETECTED_CAMERA:
        osal_memcpy(&prv->modules_id, data, sizeof(prv->modules_id));
        osal_sem_post(prv->sem_cam_detected);
        break;
    default:
        mmsdbg(DL_WARNING, "Wrong evt: %s",  guzzi_event_global_id2str(event_id));
        break;
    }
    osal_mutex_unlock(prv->events_lock);
}

/* ========================================================================== */
/**
* void _get_aca_regs()
*/
/* ========================================================================== */
void _get_aca_regs (hat_reg_pri_t *regs_aca,
                                        hat_reg_pri_t *regs_gzz,
                                        hat_size_t *full_size,
                                        hat_rect_t *crop_reg)
{
    uint32              i, j = 0, _max_priority = 0;
    hat_rect_float_t    *_gzz;

    osal_memset(regs_aca, 0x00, sizeof(*regs_aca));

    for (i= 0; i < regs_gzz->reg_num; i++) {
        _gzz = &regs_gzz->reg_box[i];
        if ((crop_reg->x <= _gzz->y) &&
                (crop_reg->y <= _gzz->y) &&
                ((crop_reg->x + crop_reg->w) >= (_gzz->x + _gzz->w)) &&
                ((crop_reg->y + crop_reg->h) >= (_gzz->y + _gzz->h))) {

            regs_aca->priority[j]   = regs_gzz->priority[i];
            regs_aca->reg_box[j].x  = regs_gzz->reg_box[i].x / full_size->w;
            regs_aca->reg_box[j].y  = regs_gzz->reg_box[i].y / full_size->h;
            regs_aca->reg_box[j].w  = regs_gzz->reg_box[i].w / full_size->w;
            regs_aca->reg_box[j].h  = regs_gzz->reg_box[i].h / full_size->h;

            if (_max_priority < regs_aca->priority[j]) {
                _max_priority = regs_aca->priority[j];
            }

            j ++;
        }
    }

    regs_aca->reg_num = j;
    regs_aca->priority_max = _max_priority;
}

/* ========================================================================== */
/**
* void cl_config_apply()
*/
/* ========================================================================== */
static int _aca_config_apply (inc_auto_cam_algs_priv_data_t *prv,
                                    cam_cfg_t                   *active_cfg,
                                    cam_cfg_t                   *new_cfg)
{

    mmsdbg(DL_FUNC, "Enter");

    osal_mutex_lock(prv->events_lock);
    prv->dtp_scommn_keys.camera_id = prv->modules_id.cm_idx;
    osal_mutex_unlock(prv->events_lock);

    if (  (is_gzz_cfg_changed(active_cfg, new_cfg, cam_gzz_cfg.wb_mode))
        ||(is_gzz_cfg_changed(active_cfg, new_cfg, cam_gzz_cfg.manual_color_temp))
        ||(is_gzz_cfg_changed(active_cfg, new_cfg, cam_gzz_cfg.manual_rgb2rgb))
       )
    {
        aca_awb_config_t cfg;

        cfg.dtp_s_common = &prv->dtp_scommn_keys;

        cfg.manual_colour_temp = new_cfg->cam_gzz_cfg.manual_color_temp.val;
        cfg.manual_wbal_coef = new_cfg->cam_gzz_cfg.wb_gains.val;
        cfg.wb_mode = new_cfg->cam_gzz_cfg.wb_mode.val;
        cfg.manual_rgb2rgb = prv->active_cfg->cam_gzz_cfg.manual_rgb2rgb.val;

        aca_awb_thr_config(prv->awb_data, &cfg);
    }

    if ((is_gzz_cfg_changed(active_cfg, new_cfg, cam_gzz_cfg.af_mode))  ||
        (is_gzz_cfg_changed(active_cfg, new_cfg, cam_gzz_cfg.af_range)) ||
        (is_gzz_cfg_changed(active_cfg, new_cfg, cam_gzz_cfg.af_weght)) ||
        (is_gzz_cfg_changed(active_cfg, new_cfg, cam_gzz_cfg.af_regions)) ||
        (is_gzz_cfg_changed(active_cfg, new_cfg, cam_gzz_cfg.af_trigger)) ||
        (is_gzz_cfg_changed(active_cfg, new_cfg, cam_gzz_cfg.lens_focal_len))){

        {
            aca_af_config_t cfg;

            cfg.dtp_s_common = &prv->dtp_scommn_keys;
            cfg.mode = new_cfg->cam_gzz_cfg.af_mode.val;
            cfg.range = new_cfg->cam_gzz_cfg.af_range.val;
            cfg.spot_weighting = new_cfg->cam_gzz_cfg.af_weght.val;
            cfg.manual_lens = new_cfg->cam_gzz_cfg.lens_focal_len.val;

            aca_af_thr_config(prv->af_data, &cfg);
        }

        {
            aca_fmv_config_t cfg;

            cfg.dtp_s_common = &prv->dtp_scommn_keys;
            aca_fmv_thr_config(prv->fmv_data, &cfg);
        }
    }

    if (is_gzz_cfg_changed(active_cfg, new_cfg, cam_gzz_cfg.ae_mode)) {
        aca_thr_config_t cfg;
        aca_ae_calc_cfg_t ae_calc;
        aca_ae_stab_cfg_t ae_stab;


        ae_calc.dtp_s_common = &prv->dtp_scommn_keys;
        //TODO:
        ae_calc.metering = ACA_AE_SPOTWEIGHTING_NORMAL;
        ae_calc.sensitivity = prv->sen_features.sen->modes.list[prv->sen_features.sen_mode_idx].sensitivity;
        ae_calc.iso_base   = prv->sen_features.sen->base_ISO;

        ae_stab.dtp_s_common = &prv->dtp_scommn_keys;
        //TODO
        ae_stab.mode = ACA_AE_STAB_MODE_NONE;

        cfg.ae_calc = &ae_calc;
        cfg.ae_stab = &ae_stab;

        aca_ae_thr_config(prv->ae_data, &cfg);
    }

    if (is_gzz_cfg_changed(active_cfg, new_cfg, cam_gzz_cfg.flicker_mode)) {
        aca_afd_config_t cfg;

        cfg.dtp_s_common = &prv->dtp_scommn_keys;
        aca_afd_thr_config(prv->afd_data, &cfg);
    }

#if (MS_TEST_SCENARIO==360)
    if (is_gzz_cfg_changed(active_cfg, new_cfg, cam_gzz_cfg.exposure_merger)) {
        aca_ae_merger_config_t cfg;

        cfg.camera_id = prv->camera_id;
        cfg.weight = new_cfg->cam_gzz_cfg.exposure_merger.val;
        cfg.dtp_s_common = &prv->dtp_scommn_keys;

        aca_ae_merger_thr_config(prv->ae_merger_data, &cfg);
    }

    if (is_gzz_cfg_changed(active_cfg, new_cfg, cam_gzz_cfg.white_balance_merger)) {
        aca_awb_merger_config_t cfg;

        cfg.dtp_s_common = &prv->dtp_scommn_keys;
        cfg.camera_id = prv->camera_id;
        cfg.weight = new_cfg->cam_gzz_cfg.white_balance_merger.val;

        aca_awb_merger_thr_config(prv->awb_merger_data, &cfg);
    }
#endif // (MS_TEST_SCENARIO==360)

    mmsdbg(DL_FUNC, "Exit %s", 0 ? "Error" : "Ok");
    return 0;
}

/* ========================================================================== */
/**
* _prepare_afd_process()
*/
/* ========================================================================== */

/* ========================================================================== */
/**
* inc_auto_cam_algs_start()
*/
/* ========================================================================== */
static int inc_auto_cam_algs_start(inc_t *inc, void *params)
{
    int err;
    inc_auto_cam_algs_priv_data_t   *pCompPrv;

    pCompPrv = inc->prv.ptr;

    mmsdbg(DL_FUNC, "Enter");

    pCompPrv->active_cfg = INC_RES_GET_PTR(params, pCompPrv->res_offset_cfg);
    configurator_config_lock(pCompPrv->active_cfg);

    err = osal_sem_wait_timeout(pCompPrv->sem_cam_detected, DEFAULT_ALLOC_TIMEOUT_MS);
    if (err) {
        mmsdbg(DL_ERROR, "Camera detect timeout %d ms", DEFAULT_ALLOC_TIMEOUT_MS);
    }
    osal_assert(!err);

    _aca_config_apply(pCompPrv, NULL, pCompPrv->active_cfg);

    aca_af_thr_start(pCompPrv->af_data);
    aca_ae_thr_start(pCompPrv->ae_data);
    aca_awb_thr_start(pCompPrv->awb_data);
    aca_afd_thr_start(pCompPrv->afd_data);
    aca_fmv_thr_start(pCompPrv->fmv_data);

    mmsdbg(DL_FUNC, "Exit Ok");

    return 0;
}

/* ========================================================================== */
/**
* inc_auto_cam_algs_stop()
*/
/* ========================================================================== */
static void inc_auto_cam_algs_stop(inc_t *inc)
{
    inc_auto_cam_algs_priv_data_t   *pCompPrv;

    pCompPrv = inc->prv.ptr;

    mmsdbg(DL_FUNC, "Enter");

    if (pCompPrv->active_cfg)
        configurator_config_unlock(pCompPrv->active_cfg);

    aca_fmv_thr_stop(pCompPrv->fmv_data);
    aca_afd_thr_stop(pCompPrv->afd_data);
    aca_awb_thr_stop(pCompPrv->awb_data);
    aca_ae_thr_stop(pCompPrv->ae_data);
    aca_af_thr_stop(pCompPrv->af_data);

    mmsdbg(DL_FUNC, "Exit Ok");
}

/* ========================================================================== */
/**
* inc_auto_cam_algs_config()
*/
/* ========================================================================== */
static int inc_auto_cam_algs_config(inc_t *inc, void *data)
{
    inc_auto_cam_algs_priv_data_t   *pCompPrv = inc->prv.ptr;
    camera_fr_t                     *pFR = data;
    cam_cfg_t                       *new_cfg;

    mmsdbg(DL_FUNC, "Enter");

    new_cfg = CAMERA_FR_GET_ENTRY(pFR, pCompPrv->fr_offset_cfg)->data;
    osal_mutex_lock(pCompPrv->lock_cfg);
    configurator_config_lock(new_cfg);
    _aca_config_apply(pCompPrv, pCompPrv->active_cfg, new_cfg);
    configurator_config_unlock(pCompPrv->active_cfg);
    pCompPrv->active_cfg = new_cfg;
    osal_mutex_unlock(pCompPrv->lock_cfg);
    mmsdbg(DL_FUNC, "Exit Ok");

    return 0;
}

/* ========================================================================== */
/**
* inc_auto_cam_algs_process()
*/
/* ========================================================================== */
static void inc_auto_cam_algs_process(inc_t *inc, void *data)
{
    inc_auto_cam_algs_priv_data_t   *pCompPrv;
    cam_fr_buff_ctrl_list_t         *cl_req;
    hat_h3a_aewb_stat_t             *aewb_stat;
    hat_h3a_af_stat_t               *af_stat;
    cam_algo_state_t                *fr_algo_state;
    vpipe_ctrl_settings_t           *vpipe;
    cam_cl_algo_ctrl_list_t         *ent;
    cam_cl_algo_ctrl_t  *algo_ctrl;

    pCompPrv = inc->prv.ptr;
    aewb_stat       = CAMERA_FR_GET_ENTRY(data, pCompPrv->fr_offset_aewb_stats)->data;
    af_stat         = CAMERA_FR_GET_ENTRY(data, pCompPrv->fr_offset_af_stats)->data;
    cl_req          = CAMERA_FR_GET_ENTRY(data, pCompPrv->fr_cl_aca_offset)->data;
    fr_algo_state   = CAMERA_FR_GET_ENTRY(data, pCompPrv->fr_offset_algo_state)->data;
    vpipe           = CAMERA_FR_GET_ENTRY(data, pCompPrv->fr_offset_vpipe)->data;

    osal_mutex_lock(pCompPrv->events_lock);
    pCompPrv->full_size = pCompPrv->sen_features.sen->modes.list[pCompPrv->sen_features.sen_mode_idx].active;
    osal_mutex_unlock(pCompPrv->events_lock);

    if (fr_algo_state->fr_status)
    {
//        mmsdbg(DL_ERROR, "ACA skip error FR %p", data);
        inc->callback(inc, inc->client_prv, 0, 0, data);
        return;
    }
    mmsdbg(DL_FUNC, "Enter");

    PROFILE_ADD(PROFILE_ID_ALGOS_ACA_EXP_GAIN,
                    fr_algo_state->sg.ae_distr.exp_gain.exposure,
                    (fr_algo_state->sg.ae_distr.exp_gain.again*100));

    list_for_each_entry(ent, &cl_req->list, link)
    {
        algo_ctrl = &ent->ctrl;

        switch (algo_ctrl->aca_Algo) {
            case CL_ALGO_AE:
                PROFILE_ADD(PROFILE_ID_ALGOS_AE_RUN_INPUT, aewb_stat, aewb_stat->paxels);
                aca_ae_thr_process(pCompPrv->ae_data,
                                    aewb_stat,
                                    fr_algo_state,
                                    vpipe->wb.data,
                                    algo_ctrl,
                                    &pCompPrv->active_cfg->cam_gzz_cfg.ae_regions.val,
                                    &pCompPrv->full_size,
                                    &pCompPrv->active_cfg->cam_gzz_cfg.sensor_crop.val);
                PROFILE_ADD(PROFILE_ID_ALGOS_AE_RUN_INPUT, 0, 0);
                break;

            case CL_ALGO_AWB:
                aca_awb_thr_process(pCompPrv->awb_data,
                                    aewb_stat,
                                    fr_algo_state,
                                    vpipe->wb.data,
                                    algo_ctrl,
                                    &pCompPrv->active_cfg->cam_gzz_cfg.wb_regions.val,
                                    &pCompPrv->full_size,
                                    &pCompPrv->active_cfg->cam_gzz_cfg.sensor_crop.val);
                break;

            case CL_ALGO_AF:
            /* no break */
            case CL_ALGO_CAF:
                aca_af_thr_process(pCompPrv->af_data,
                                    af_stat,
                                    fr_algo_state,
                                    vpipe->wb.data,
                                    algo_ctrl,
                                    &pCompPrv->active_cfg->cam_gzz_cfg.af_regions.val,
                                    &pCompPrv->full_size,
                                    &pCompPrv->active_cfg->cam_gzz_cfg.sensor_crop.val);
                break;

            case CL_ALGO_AFD:
                aca_afd_thr_process(pCompPrv->afd_data,
                                    aewb_stat,
                                    fr_algo_state,
                                    vpipe->wb.data,
                                    algo_ctrl,
                                    &pCompPrv->active_cfg->cam_gzz_cfg.af_regions.val,
                                    &pCompPrv->full_size,
                                    &pCompPrv->active_cfg->cam_gzz_cfg.sensor_crop.val);
                break;

            case CL_ALGO_FMV:
                aca_fmv_thr_process(pCompPrv->fmv_data,
                                    aewb_stat,
                                    fr_algo_state,
                                    vpipe->wb.data,
                                    algo_ctrl,
                                    &pCompPrv->active_cfg->cam_gzz_cfg.af_regions.val,
                                    &pCompPrv->full_size,
                                    &pCompPrv->active_cfg->cam_gzz_cfg.sensor_crop.val);
                break;

            default:
            mmsdbg(DL_WARNING, "No target ACA algorithm Algo %d Buff %d",
                    algo_ctrl->aca_Algo,
                    algo_ctrl->buff_type);
                break;
        }
    }

    inc->callback(inc, inc->client_prv, 0, 0, data);

    mmsdbg(DL_FUNC, "Exit Ok");
}

/* ========================================================================== */
/**
* inc_auto_cam_algs_destroy()
*/
/* ========================================================================== */
static void inc_auto_cam_algs_destroy(inc_t *inc)
{
    inc_auto_cam_algs_priv_data_t *pCompPrv = inc->prv.ptr;

    mmsdbg(DL_FUNC, "Enter");

    guzzi_event_unreg_recipient(
            pCompPrv->evt_hndl,
            geg_camera_event_mk(pCompPrv->camera_id, CAM_EVT_SEN_MODE_CHANGE),
            _aca_evt_notify,
            pCompPrv
        );
    guzzi_event_unreg_recipient(
            pCompPrv->evt_hndl,
            geg_camera_event_mk(pCompPrv->camera_id, CAM_EVT_DETECTED_CAMERA),
            _aca_evt_notify,
            pCompPrv
        );

    osal_sem_destroy(pCompPrv->sem_cam_detected);

    if(pCompPrv->lock_cfg)
        osal_mutex_destroy(pCompPrv->lock_cfg);

    if(pCompPrv->fmv_data)
        aca_fmv_thr_destroy(pCompPrv->fmv_data);

    if(pCompPrv->afd_data)
        aca_afd_thr_destroy(pCompPrv->afd_data);

    if(pCompPrv->awb_data)
        aca_awb_thr_destroy(pCompPrv->awb_data);

    if(pCompPrv->ae_data)
        aca_ae_thr_destroy(pCompPrv->ae_data);

    if(pCompPrv->af_data)
        aca_af_thr_destroy(pCompPrv->af_data);

#if (MS_TEST_SCENARIO==360)
    if(pCompPrv->ae_merger_data)
        aca_ae_merger_thr_destroy(pCompPrv->ae_merger_data);

    if(pCompPrv->awb_merger_data)
        aca_awb_merger_thr_destroy(pCompPrv->awb_merger_data);
#endif // (MS_TEST_SCENARIO==360)

    if(pCompPrv->events_lock)
        osal_mutex_destroy(pCompPrv->events_lock);

    if(inc->prv.ptr)
        osal_free(inc->prv.ptr);

    mmsdbg(DL_FUNC, "Exit Ok");
}

/* ========================================================================== */
/**
* inc_auto_cam_algs_create()
*/
/* ========================================================================== */
int inc_auto_cam_algs_create(
        inc_t *inc,
        void *params,
        void *app_res
    )
{
    inc_auto_cam_algs_priv_data_t *pCompPrv;
    INC_AUTO_CAM_ALGS_CPARAMS_T *pStaticParams;
    int ret;

    mmsdbg(DL_FUNC, "Enter");

    pStaticParams = params;

    pCompPrv = osal_malloc(sizeof(inc_auto_cam_algs_priv_data_t));
    GOTO_EXIT_IF(NULL == pCompPrv, 1);
    osal_memset(pCompPrv, 0, sizeof(*pCompPrv));
    pCompPrv->name = "Inc ACA prv";

    inc->prv.ptr = pCompPrv;

    pCompPrv->fr_offset_cfg         = pStaticParams->fr_offset_cfg;
    pCompPrv->fr_cl_aca_offset      = pStaticParams->fr_offset_cl_aca;
    pCompPrv->fr_offset_algo_state  = pStaticParams->fr_offset_algo_state;
    pCompPrv->fr_offset_aewb_stats  = pStaticParams->fr_offset_aewb_stats;
    pCompPrv->fr_offset_af_stats    = pStaticParams->fr_offset_af_stats;
    pCompPrv->fr_offset_vpipe       = pStaticParams->fr_offset_vpipe;
    pCompPrv->max_fr                = pStaticParams->max_fr;
    pCompPrv->res_offset_cfg        = pStaticParams->res_offset_cfg;
    pCompPrv->res_offset_cametra_id = pStaticParams->res_offset_cametra_id;

    pCompPrv->evt_hndl = guzzi_event_global();
    pCompPrv->camera_id = INC_RES_GET_UINT(app_res, pCompPrv->res_offset_cametra_id);

    pCompPrv->lock_cfg = osal_mutex_create();
    GOTO_EXIT_IF(NULL == pCompPrv->lock_cfg, 2);

    af_thread_create_params_t af_params;
    af_params.evt_hndl = pCompPrv->evt_hndl;
    af_params.camera_id = pCompPrv->camera_id;
    pCompPrv->af_data = aca_af_thr_create(&af_params);
    GOTO_EXIT_IF(NULL == pCompPrv->af_data, 2);

    ae_thread_create_params_t ae_params;
    ae_params.evt_hndl = pCompPrv->evt_hndl;
    ae_params.camera_id = pCompPrv->camera_id;
    pCompPrv->ae_data = aca_ae_thr_create(&ae_params);
    GOTO_EXIT_IF(NULL == pCompPrv->ae_data, 2);

    awb_thread_create_params_t awb_params;
    awb_params.evt_hndl = pCompPrv->evt_hndl;
    awb_params.camera_id = pCompPrv->camera_id;
    pCompPrv->awb_data = aca_awb_thr_create(&awb_params);
    GOTO_EXIT_IF(NULL == pCompPrv->awb_data, 2);

    afd_thread_create_params_t afd_params;
    afd_params.evt_hndl = pCompPrv->evt_hndl;
    afd_params.camera_id = pCompPrv->camera_id;
    pCompPrv->afd_data = aca_afd_thr_create(&afd_params);
    GOTO_EXIT_IF(NULL == pCompPrv->afd_data, 2);

    fmv_thread_create_params_t fmv_params;
    fmv_params.evt_hndl = pCompPrv->evt_hndl;
    fmv_params.camera_id = pCompPrv->camera_id;
    pCompPrv->fmv_data = aca_fmv_thr_create(&fmv_params);
    GOTO_EXIT_IF(NULL == pCompPrv->fmv_data, 2);

#if (MS_TEST_SCENARIO==360)
    ae_merger_thread_create_params_t ae_merger_params;
    ae_merger_params.evt_hndl = pCompPrv->evt_hndl;
    ae_merger_params.camera_id = pCompPrv->camera_id;
    pCompPrv->ae_merger_data = aca_ae_merger_thr_create(&ae_merger_params);
    GOTO_EXIT_IF(NULL == pCompPrv->ae_merger_data, 2);

    awb_merger_thread_create_params_t awb_merger_params;
    awb_merger_params.evt_hndl = pCompPrv->evt_hndl;
    awb_merger_params.camera_id = pCompPrv->camera_id;
    pCompPrv->awb_merger_data = aca_awb_merger_thr_create(&awb_merger_params);
    GOTO_EXIT_IF(NULL == pCompPrv->awb_merger_data, 2);
#endif

    pCompPrv->events_lock = osal_mutex_create();
    GOTO_EXIT_IF(NULL == pCompPrv->events_lock, 2);

    ret = guzzi_event_reg_recipient(
            pCompPrv->evt_hndl,
            geg_camera_event_mk(pCompPrv->camera_id, CAM_EVT_SEN_MODE_CHANGE),
            _aca_evt_notify,
            pCompPrv
        );
    GOTO_EXIT_IF(ret, 2);

    ret = guzzi_event_reg_recipient(
            pCompPrv->evt_hndl,
            geg_camera_event_mk(pCompPrv->camera_id, CAM_EVT_DETECTED_CAMERA),
            _aca_evt_notify,
            pCompPrv
        );
    GOTO_EXIT_IF(ret, 3);

    pCompPrv->sem_cam_detected = osal_sem_create(0);
    GOTO_EXIT_IF(NULL == pCompPrv->sem_cam_detected, 4);

    inc->inc_start = inc_auto_cam_algs_start;
    inc->inc_stop = inc_auto_cam_algs_stop;
    inc->inc_flush = NULL;
    inc->inc_config_alter = NULL;
    inc->inc_config = inc_auto_cam_algs_config;
    inc->inc_process = inc_auto_cam_algs_process;
    inc->inc_destroy = inc_auto_cam_algs_destroy;

    mmsdbg(DL_FUNC, "Exit Ok");
    return 0;
EXIT_4:
    guzzi_event_unreg_recipient(
            pCompPrv->evt_hndl,
            geg_camera_event_mk(pCompPrv->camera_id, CAM_EVT_DETECTED_CAMERA),
            _aca_evt_notify,
            pCompPrv
        );
EXIT_3:
    guzzi_event_unreg_recipient(
            pCompPrv->evt_hndl,
            geg_camera_event_mk(pCompPrv->camera_id, CAM_EVT_SEN_MODE_CHANGE),
            _aca_evt_notify,
            pCompPrv
        );
EXIT_2:
    inc_auto_cam_algs_destroy(inc);
EXIT_1:
    mmsdbg(DL_FUNC, "Exit Err");
    return -1;
}

