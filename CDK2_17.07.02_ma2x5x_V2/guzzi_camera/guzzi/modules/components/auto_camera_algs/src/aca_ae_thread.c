/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file aca_ae_thread.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <osal/osal_stdlib.h>
#include <osal/osal_mutex.h>
#include <utils/mms_debug.h>
#include <error_handle/include/error_handle.h>
#include <cam_cl_frame_req.h>
#include <func_thread/include/func_thread.h>
#include <guzzi_event/include/guzzi_event.h>
#include <guzzi_event_global/include/guzzi_event_global.h>
#include <aca_ae_thread.h>

#define AE_THREAD_STACK_SIZE   (10 * 1024)
#define AE_THREAD_PRIORITY     (3)

struct ae_thread_data {
    func_thread_t           *ae_thr;
    guzzi_event_t           *evt_hndl;
    uint32                  camera_id;
    aca_ae_calc_hndl_t      alg_hndl;
    aca_ae_stab_hndl_t      stab_alg_hndl;
    osal_sem                *sem;
    osal_mutex              *lock_cfg;
    Bool                    pending_stats;
    aca_ae_calc_input_t     in;
    Bool                    pending_cfg;
    aca_ae_calc_cfg_t       cfg;
    Bool                    pending_stab_cfg;
    aca_ae_stab_cfg_t       stab_cfg;

    uint32                  alg_req_id;
    uint32                  flags;
};

mmsdbg_define_variable(
        vdl_thr_algo_ae,
        (DL_DEFAULT),
        0,
        "thr.algo_ae",
        "THR ALGO AE"
    );

#define MMSDEBUGLEVEL mmsdbg_use_variable(vdl_thr_algo_ae)

void ae_thread_exec(func_thread_t *ethr, void *prv);

static func_thread_handle_t ae_thread_fxns[] = {
    ae_thread_exec,
};

static func_thread_handles_t ae_thread_handles = {
    .fxns = ae_thread_fxns,
    .size = ARRAY_SIZE(ae_thread_fxns)
};

void ae_thread_exec(func_thread_t *ethr, void *prv)
{
    struct ae_thread_data   *p = prv;
    exposure_calc_output_t  out_calc;
    ae_stab_output_t        out_stab;
    aca_ae_stab_input_t     in_stab;

    PROFILE_ADD(PROFILE_ID_ALGOS_AE_RUN, 0, 0);
    mmsdbg(DL_FUNC, "Enter");
    if(!p) {
        mmsdbg(DL_ERROR, "Null pointer");
        return;
    }

    osal_mutex_lock(p->lock_cfg);

    if (p->pending_cfg) {
        p->pending_cfg = 0;
        aca_ae_calc_config(p->alg_hndl, &p->cfg);
        mmsdbg(DL_FUNC, "AE configured");
    }

    if (p->pending_stab_cfg) {
        p->pending_stab_cfg = 0;
        aca_ae_stab_config(p->stab_alg_hndl, &p->stab_cfg);
        mmsdbg(DL_FUNC, "AE Stab configured");
    }

    osal_mutex_unlock(p->lock_cfg);

    aca_ae_calc_process(p->alg_hndl, &p->in, &out_calc);

    if (p->flags & CL_FLAG_FORCE_POST_CALC_EVENT)
    {
        guzzi_event_send(
                p->evt_hndl,
                geg_camera_event_mk(p->camera_id, CAM_EVT_AE_READY),
                p->alg_req_id,
                &out_calc
            );
    }
    mmsdbg(DL_MESSAGE, "AE out: %d Y = %d\n", out_calc.req_texp, out_calc.ave_y_level);

    // TODO ae_stab_in.dtp_d_common
    // TODO ae_stab_in.mode
    in_stab.texp            = out_calc.req_texp;
    in_stab.ae_change_ratio = out_calc.ae_change_ratio;
    aca_ae_stab_process(p->stab_alg_hndl, &in_stab, &out_stab);
    if (p->flags & CL_FLAG_FORCE_POST_CALC_EVENT)
    {
        guzzi_event_send(
                p->evt_hndl,
                geg_camera_event_mk(p->camera_id, CAM_EVT_AE_STAB_READY),
                p->alg_req_id,
                &out_stab
            );
    }
    PROFILE_ADD(PROFILE_ID_ALGOS_AE_RUN, out_calc.ave_y_level, out_stab.texp);
    mmsdbg(DL_MESSAGE, "AE stab out: %d\n", out_stab.texp);

    p->pending_stats = 0;
    if (p->in.h3a) {
        osal_free(p->in.h3a);
        p->in.h3a = NULL;
    }
    osal_sem_post(p->sem);
    mmsdbg(DL_FUNC, "Exit");
}

/* ========================================================================== */
/**
* aca_ae_thr_process()
*/
/* ========================================================================== */

int aca_ae_thr_process(ae_thread_data_t     *prv,
                                    hat_h3a_aewb_stat_t     *aewb_stat,
                                    cam_algo_state_t        *algo_state,
                                    const hat_wbal_coef_t   *wbal,
                                    cam_cl_algo_ctrl_t      *algo_ctrl,
                                    hat_reg_pri_t           *regs,
                                    hat_size_t              *size,
                                    hat_rect_t              *crop)
 {
    if (CL_FLAG_FORCE_PROCESS & algo_ctrl->processing_flags)
    {
        osal_sem_wait(prv->sem);
    } else {
        GOTO_EXIT_NOPRINT_IF (osal_sem_try_wait(prv->sem), 1);
    }

    if (!prv->pending_stats) {
        osal_mem_lock(aewb_stat);
        prv->pending_stats = 1;
        aca_ae_calc_input_t     *in = &prv->in;
        aca_ae_stab_cfg_t       *stab_cfg = &prv->stab_cfg;

        void _get_aca_regs (hat_reg_pri_t *regs_aca,
                                                hat_reg_pri_t *regs_gzz,
                                                hat_size_t *full_size,
                                                hat_rect_t *crop_reg);
        _get_aca_regs(&in->exp_region,
                        regs,
                        size,
                        crop);

        in->applied  = algo_state->sg.ae_distr.exp_gain;
        in->pre_gain = algo_state->sg.ae_distr.pre_gain.gain;

        in->wbal_coef= *wbal;
        in->h3a      = aewb_stat;
        in->aperture.iris_index = algo_state->sg.ae_distr.aperture.iris_index;
        in->aperture.shutter = algo_state->sg.ae_distr.aperture.shutter;

        prv->flags      = algo_ctrl->processing_flags;
        prv->alg_req_id = algo_ctrl->id;

        in->flash_state.flash = 0;

        // TODO: algo_ctrl->nSub_mode;  // Operating sub-mode - Locked...
        if (stab_cfg->mode != algo_ctrl->nMode) {
            prv->pending_stab_cfg = 1;
            stab_cfg->mode = algo_ctrl->nMode;      // Operating mode - fast, normal...
        }

        if (CL_FLAG_WAIT_PROCESS_END & algo_ctrl->processing_flags)
        {
            ae_thread_exec(NULL, prv);
        }
        else
        {
            func_thread_exec(prv->ae_thr, ae_thread_exec);
        }
    } else {
        osal_sem_init(prv->sem, 1);
        mmsdbg(DL_WARNING, "Internal error processing flags: 0x%x",
               algo_ctrl->processing_flags);
    }

    return 0;

EXIT_1:
    mmsdbg(DL_FUNC, "Skip AE process");
    return 0;
}

int aca_ae_thr_start(ae_thread_data_t* prv)
{
    aca_ae_calc_create(&prv->alg_hndl);
    aca_ae_stab_create(&prv->stab_alg_hndl);

    // Get default algorithm values
    prv->flags = CL_FLAG_FORCE_POST_CALC_EVENT | CL_FLAG_FORCE_PROCESS;
    osal_sem_wait(prv->sem);
    ae_thread_exec(NULL, prv);

    return 0;
}

int aca_ae_thr_stop(ae_thread_data_t* prv)
{
    aca_ae_stab_destroy(prv->stab_alg_hndl);
    aca_ae_calc_destroy(prv->alg_hndl);

    return 0;
}

int aca_ae_thr_config(ae_thread_data_t* prv, void *new_cfg)
{
    aca_thr_config_t *cfg = new_cfg;

    osal_mutex_lock(prv->lock_cfg);

    prv->pending_cfg = 1;
    prv->cfg = *(cfg->ae_calc);

    prv->pending_stab_cfg = 1;
    prv->stab_cfg = *(cfg->ae_stab);

    osal_mutex_unlock(prv->lock_cfg);

    return 0;
}

ae_thread_data_t* aca_ae_thr_create(ae_thread_create_params_t *params)
 {
    ae_thread_data_t *prv;
    prv = osal_calloc(1, sizeof(*prv));

    prv->evt_hndl = params->evt_hndl;
    prv->camera_id = params->camera_id;
    prv->sem = osal_sem_create(1);
    prv->lock_cfg = osal_mutex_create();
    // prv->pending_cfg = 1; TODO: 
    prv->pending_stab_cfg = 1;

    prv->ae_thr = func_thread_create("ACA AE THREAD",
            AE_THREAD_STACK_SIZE,
            AE_THREAD_PRIORITY,
            &ae_thread_handles,
            prv);

    return prv;
 }

void aca_ae_thr_destroy(ae_thread_data_t* prv)
{
    if (prv) {
        func_thread_destroy(prv->ae_thr);
        osal_sem_destroy(prv->sem);
        osal_mutex_destroy(prv->lock_cfg);
        osal_free(prv);
    } else
        mmsdbg(DL_ERROR, "Invalid instance");
}
