/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file aca_afd_thread.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <osal/osal_stdlib.h>
#include <osal/osal_mutex.h>
#include <utils/mms_debug.h>
#include <error_handle/include/error_handle.h>
#include <cam_cl_frame_req.h>
#include <func_thread/include/func_thread.h>
#include <guzzi_event/include/guzzi_event.h>
#include <guzzi_event_global/include/guzzi_event_global.h>

#include <aca_afd_thread.h>

#define AFD_THREAD_STACK_SIZE   (10 * 1024)
#define AFD_THREAD_PRIORITY     (3)

struct afd_thread_data {
    func_thread_t           *afd_thr;
    guzzi_event_t           *evt_hndl;
    uint32                  camera_id;
    aca_afd_hndl_t          alg_hndl;
    osal_sem                *sem;
    osal_mutex              *lock_cfg;
    Bool                    pending_stats;
    aca_afd_calc_input_t    in;
    Bool                    pending_cfg;
    aca_afd_config_t        cfg;

    uint32                  alg_req_id;
    uint32                  flags;

};

mmsdbg_define_variable(
        vdl_thr_algo_afd,
        (DL_DEFAULT),
        0,
        "thr.algo_afd",
        "THR ALGO AFD"
    );

#define MMSDEBUGLEVEL mmsdbg_use_variable(vdl_thr_algo_afd)

void afd_thread_exec(func_thread_t *ethr, void *prv);

static func_thread_handle_t afd_thread_fxns[] = {
    afd_thread_exec,
};

static func_thread_handles_t afd_thread_handles = {
    .fxns = afd_thread_fxns,
    .size = ARRAY_SIZE(afd_thread_fxns)
};

void afd_thread_exec(func_thread_t *ethr, void *prv)
{
    afd_thread_data_t      *p = prv;
    aca_afd_calc_output_t  out_afd;

    mmsdbg(DL_FUNC, "Enter");
    PROFILE_ADD(PROFILE_ID_ALGOS_AFD_RUN, 0, 0);

    if(!p) {
        mmsdbg(DL_ERROR, "Null pointer");
        return;
    }

    osal_mutex_lock(p->lock_cfg);
    if (p->pending_cfg) {
        aca_afd_configuration(p->alg_hndl, &p->cfg);
        p->pending_cfg = 0;
        mmsdbg(DL_FUNC, "AFD configured");
    }
    osal_mutex_unlock(p->lock_cfg);

    aca_afd_process(p->alg_hndl, &p->in, &out_afd);
    if (p->flags & CL_FLAG_FORCE_POST_CALC_EVENT)
    {
        guzzi_event_send(
                p->evt_hndl,
                geg_camera_event_mk(p->camera_id, CAM_EVT_AFD_READY),
                p->alg_req_id,
                &out_afd
            );
    }

    PROFILE_ADD(PROFILE_ID_ALGOS_AFD_RUN, out_afd.status, out_afd.mains_freq);
    mmsdbg(DL_MESSAGE, "AFD status: %d, Mains Frequency %d\n", out_afd.status, out_afd.mains_freq);

    p->pending_stats = 0;

    if (p->in.h3a) {
    	osal_free(p->in.h3a);
    	p->in.h3a = NULL;
    }

    osal_sem_post(p->sem);
    mmsdbg(DL_FUNC, "Exit");
}

/* ========================================================================== */
/**
* aca_afd_thr_process()
*/
/* ========================================================================== */
int aca_afd_thr_process(afd_thread_data_t     *prv,
                                    hat_h3a_aewb_stat_t     *aewb_stat,
                                    cam_algo_state_t        *algo_state,
                                    const hat_wbal_coef_t   *wbal,
                                    cam_cl_algo_ctrl_t      *algo_ctrl,
                                    hat_reg_pri_t           *regs,
                                    hat_size_t              *size,
                                    hat_rect_t              *crop)
{
    if (CL_FLAG_FORCE_PROCESS & algo_ctrl->processing_flags)
    {
        osal_sem_wait(prv->sem);
    } else {
        GOTO_EXIT_NOPRINT_IF (osal_sem_try_wait(prv->sem), 1);
    }
    if (!prv->pending_stats) {
        osal_mem_lock(aewb_stat);
        prv->pending_stats = 1;
        aca_afd_calc_input_t *in = &prv->in;

        in->exp_gain = algo_state->sg.ae_distr.exp_gain;
        in->h3a = aewb_stat;
        in->calc_mode = algo_ctrl->nMode;

        prv->flags      = algo_ctrl->processing_flags;
        prv->alg_req_id = algo_ctrl->id;

        //in->dtp_d_common = cfg_dtp;

        if (CL_FLAG_WAIT_PROCESS_END & algo_ctrl->processing_flags)
        {
            afd_thread_exec(NULL, prv);
        }
        else
        {
            func_thread_exec(prv->afd_thr, afd_thread_exec);
        }
    } else {
        osal_sem_init(prv->sem, 1);
        mmsdbg(DL_WARNING, "Internal error processing flags: 0x%x",
               algo_ctrl->processing_flags);
    }

    return 0;

EXIT_1:
    mmsdbg(DL_FUNC, "Skip AFD process");
    return 0;
}

int aca_afd_thr_start(afd_thread_data_t* prv)
{
    aca_afd_create(&prv->alg_hndl);
    return 0;
}

int aca_afd_thr_stop(afd_thread_data_t* prv)
{
    aca_afd_destroy(prv->alg_hndl);
    return 0;
}

int aca_afd_thr_config(afd_thread_data_t* prv, void *new_cfg)
{
    aca_afd_config_t *cfg = new_cfg;

    osal_mutex_lock(prv->lock_cfg);

    prv->pending_cfg = 1;
    prv->cfg = *cfg;

    osal_mutex_unlock(prv->lock_cfg);

    return 0;
}

afd_thread_data_t* aca_afd_thr_create(afd_thread_create_params_t *params)
 {
    afd_thread_data_t *prv;
    prv = osal_calloc(1, sizeof(*prv));

    prv->evt_hndl = params->evt_hndl;
    prv->camera_id = params->camera_id;
    prv->sem = osal_sem_create(1);
    prv->lock_cfg = osal_mutex_create();
    prv->pending_cfg = 1;

    prv->afd_thr = func_thread_create("ACA AFD THREAD",
            AFD_THREAD_STACK_SIZE,
            AFD_THREAD_PRIORITY,
            &afd_thread_handles,
            prv);

    return prv;
 }

void aca_afd_thr_destroy(afd_thread_data_t* prv)
{
    if (prv) {
        func_thread_destroy(prv->afd_thr);
        osal_sem_destroy(prv->sem);
        osal_mutex_destroy(prv->lock_cfg);
        osal_free(prv);
    } else
        mmsdbg(DL_ERROR, "Invalid instance");
}
