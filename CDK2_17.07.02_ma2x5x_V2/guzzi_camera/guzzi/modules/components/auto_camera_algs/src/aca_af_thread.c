/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file aca_af_thread.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <math.h>
#include <osal/osal_stdlib.h>
#include <osal/osal_mutex.h>
#include <utils/mms_debug.h>
#include <error_handle/include/error_handle.h>
#include <cam_cl_frame_req.h>
#include <func_thread/include/func_thread.h>
#include <guzzi_event/include/guzzi_event.h>
#include <guzzi_event_global/include/guzzi_event_global.h>
#include <aca_af_thread.h>
#include <aca_af.h>

#include "aca_af_types.h"

#define AF_THREAD_STACK_SIZE   (10 * 1024)
#define AF_THREAD_PRIORITY     (3)

struct af_thread_data {
    func_thread_t           *af_thr;
    guzzi_event_t           *evt_hndl;
    uint32                  camera_id;
    aca_af_hndl_t           alg_hndl;
    osal_sem                *sem;
    osal_mutex              *lock_cfg;
    Bool                    pending_stats;
    aca_af_calc_input_t     in;
    Bool                    pending_cfg;
    aca_af_config_t         cfg;

    uint32                  alg_req_id;
    uint32                  flags;
    aca_focus_output_t      output;
};

mmsdbg_define_variable(
        vdl_thr_algo_af,
        (DL_DEFAULT),
        0,
        "thr.algo_af",
        "THR ALGO af"
    );

#define MMSDEBUGLEVEL mmsdbg_use_variable(vdl_thr_algo_af)

void af_thread_exec(func_thread_t *ethr, void *prv);

static func_thread_handle_t af_thread_fxns[] = {
    af_thread_exec,
};

static func_thread_handles_t af_thread_handles = {
    .fxns = af_thread_fxns,
    .size = ARRAY_SIZE(af_thread_fxns)
};


void af_thread_exec(func_thread_t *ethr, void *p)
{
    af_thread_data_t    *prv = p;

    PROFILE_ADD(PROFILE_ID_ALGOS_AF_RUN, 0, 0);
    mmsdbg(DL_FUNC, "Enter");
    if(!prv) {
        mmsdbg(DL_ERROR, "Null pointer");
        return;
    }

    osal_mutex_lock(prv->lock_cfg);
    // Apply new AF configuration
    if (prv->pending_cfg) {
        aca_af_configuration(prv->alg_hndl, &prv->cfg);
        prv->pending_cfg = 0;
        mmsdbg(DL_FUNC, "AF configured");
    }
    osal_mutex_unlock(prv->lock_cfg);

    aca_af_process(prv->alg_hndl, &prv->in, &prv->output);

    if (prv->flags & CL_FLAG_FORCE_POST_CALC_EVENT)
    {
        guzzi_event_send(
                prv->evt_hndl,
                geg_camera_event_mk(prv->camera_id, CAM_EVT_AF_READY),
                prv->alg_req_id,
                &prv->output
            );
    }

    PROFILE_ADD(PROFILE_ID_ALGOS_AF_RUN,
                         prv->output.focus_out.move,
                         prv->output.focus_out.pos * 1023);

    prv->pending_stats = 0;

    if (prv->in.af_stats) {
        osal_free(prv->in.af_stats);
        prv->in.af_stats = NULL;
    }
    osal_sem_post(prv->sem);
    mmsdbg(DL_FUNC, "Exit");
}

/* ========================================================================== */
/**
* aca_af_thr_process()
*/
/* ========================================================================== */
int aca_af_thr_process(af_thread_data_t     *prv,
                                    hat_h3a_af_stat_t       *af_stat,
                                    cam_algo_state_t        *algo_state,
                                    const hat_wbal_coef_t   *wbal,
                                    cam_cl_algo_ctrl_t      *algo_ctrl,
                                    hat_reg_pri_t           *regs,
                                    hat_size_t              *size,
                                    hat_rect_t              *crop)
 {
    if (CL_FLAG_FORCE_PROCESS & algo_ctrl->processing_flags)
    {
        osal_sem_wait(prv->sem);
    } else {
        GOTO_EXIT_NOPRINT_IF (osal_sem_try_wait(prv->sem), 1);
    }
    if (!prv->pending_stats) {
        osal_mem_lock(af_stat);
        prv->pending_stats = 1;
        prv->in.af_stats = af_stat;

        prv->in.exp_prms.ae_conv_sts = algo_state->sg.ae_distr.distr_conv;
        prv->in.exp_prms.req_total_exposure = algo_state->aca.ae_out.req_texp;
        prv->in.exp_prms.applied_settings   = algo_state->sg.ae_distr.exp_gain;

        void _get_aca_regs (hat_reg_pri_t *regs_aca,
                                                hat_reg_pri_t *regs_gzz,
                                                hat_size_t *full_size,
                                                hat_rect_t *crop_reg);
        _get_aca_regs(&prv->in.focus_region,
                        regs,
                        size,
                        crop);

        prv->in.fmv_input.cam_pos_x    = algo_state->aca.fmv_out.cam_pos_x;
        prv->in.fmv_input.cam_pos_y    = algo_state->aca.fmv_out.cam_pos_y;
        prv->in.fmv_input.scene_num    = algo_state->aca.fmv_out.scene_num;
        prv->in.fmv_input.cam_zoom     = algo_state->aca.fmv_out.cam_zoom;
        prv->in.fmv_input.bad_match    = algo_state->aca.fmv_out.bad_match;
        prv->in.fmv_input.stable_scene = algo_state->aca.fmv_out.stable_scene;

        prv->flags      = algo_ctrl->processing_flags;
        prv->alg_req_id = algo_ctrl->id;

        PROFILE_ADD(PROFILE_ID_ALGOS_AF_INPUT, (af_stat->paxels->lens_position * 1023), af_stat->seq_cnt);

        if (CL_FLAG_WAIT_PROCESS_END & algo_ctrl->processing_flags)
        {
            af_thread_exec(NULL, prv);
        }
        else
        {
            func_thread_exec(prv->af_thr, af_thread_exec);
        }

    } else {
        osal_sem_init(prv->sem, 1);
        mmsdbg(DL_WARNING, "Internal error processing flags: 0x%x",
               algo_ctrl->processing_flags);
    }

    return 0;

EXIT_1:
    mmsdbg(DL_FUNC, "Skip AF process");
    return 0;
}

int aca_af_thr_start(af_thread_data_t* prv)
{
    aca_af_create(&prv->alg_hndl);

    return 0;
}

int aca_af_thr_stop(af_thread_data_t* prv)
{
    aca_af_destroy(prv->alg_hndl);

    return 0;
}

int aca_af_thr_config(af_thread_data_t* prv, void *new_cfg)
{
    aca_af_config_t *cfg = new_cfg;

    osal_mutex_lock(prv->lock_cfg);
    prv->pending_cfg = 1;
    prv->cfg = *cfg;
    osal_mutex_unlock(prv->lock_cfg);

    return 0;
}

af_thread_data_t* aca_af_thr_create(af_thread_create_params_t *params)
 {
    af_thread_data_t *prv;
    prv = osal_calloc(1, sizeof(*prv));

    prv->evt_hndl = params->evt_hndl;
    prv->camera_id = params->camera_id;
    prv->sem = osal_sem_create(1);
    prv->lock_cfg = osal_mutex_create();
    prv->pending_cfg = 1;

    prv->af_thr = func_thread_create("ACA AF THREAD",
            AF_THREAD_STACK_SIZE,
            AF_THREAD_PRIORITY,
            &af_thread_handles,
            prv);

    return prv;
 }

void aca_af_thr_destroy(af_thread_data_t* prv)
{
    if (prv) {
        func_thread_destroy(prv->af_thr);
        osal_sem_destroy(prv->sem);
        osal_mutex_destroy(prv->lock_cfg);
        osal_free(prv);
    } else
        mmsdbg(DL_ERROR, "Invalid instance");
}
