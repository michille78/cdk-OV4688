/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file aca_awb_merger_thread.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <osal/osal_stdlib.h>
#include <osal/osal_string.h>
#include <osal/osal_mutex.h>
#include <osal/osal_assert.h>
#include <utils/mms_debug.h>
#include <error_handle/include/error_handle.h>
#include <cam_cl_frame_req.h>
#include <func_thread/include/func_thread.h>
#include <guzzi_event/include/guzzi_event.h>
#include <guzzi_event_global/include/guzzi_event_global.h>
#include <osal/ex_pool.h>
#include <aca_awb_merger_thread.h>

mmsdbg_define_variable(
        vdl_thr_algo_awb_merger,
        (DL_DEFAULT),
        0,
        "thr.algo_awb_merger",
        "THR ALGO AWB_MERGER"
    );

#define MMSDEBUGLEVEL mmsdbg_use_variable(vdl_thr_algo_awb_merger)

#define AWB_MERGER_THREAD_STACK_SIZE   (10 * 1024)
#define AWB_MERGER_THREAD_PRIORITY     (3)
#define AWB_MERGER_THREAD_TIMEOUT      (300)
#define AWB_MERGER_NUM_POOL_ENTRIES    (3)


struct awb_merger_common_data {
    func_thread_t               *awb_merger_thr;
    guzzi_event_t               *evt_hndl;
    aca_awb_merger_hndl_t        alg_hndl;
    osal_sem                    *sem;
    ex_pool_t                   *pool_awb_merger;
};

#define MAX_AWB_MERGE_CAMERAS    6

static uint32 g_awb_merger_weights[MAX_AWB_MERGE_CAMERAS];


typedef struct
{
    int num_total;
    int num_valid;
    per_camera_awb_merger_data_t *in_data;
} awb_merger_calc_data_t;

ex_pool_node_t awb_merger_desc [] = {
     EX_POOL_NODE_DESC(NULL, (sizeof(per_camera_awb_merger_data_t) * MAX_AWB_MERGE_CAMERAS), awb_merger_calc_data_t, in_data),
     EX_POOL_LIST_END
};

ex_pool_node_t awb_merger_root =
{
     EX_POOL_ROOT(&awb_merger_desc, sizeof (awb_merger_calc_data_t))
};

struct {
    int ref_counter;
    struct awb_merger_common_data g_ctx;

    awb_merger_calc_data_t *entry;
    awb_merger_calc_data_t *for_process_entry;
}g_awb_merger;

struct awb_merger_priv_data
{
    uint32  camera_id;
    struct awb_merger_common_data *pctx;
//    aca_awb_merger_config_t       cfg;
};

static void awb_merger_init_entry(awb_merger_calc_data_t * e)
{
int i;
    e->num_total = MAX_AWB_MERGE_CAMERAS;
    e->num_valid = 0;
    for (i = 0; i < MAX_AWB_MERGE_CAMERAS; ++i) {
        e->in_data[i].valid = 0;
    }
}

void awb_merger_prepare_for_processing(awb_merger_calc_data_t *for_process_entry)
{
    int i;

    for (i = 0; i < for_process_entry->num_total; ++i)
    {
        for_process_entry->in_data[i].weight = g_awb_merger_weights[i];
    }
}


int awb_merger_add_data(awb_merger_priv_data_t *prv, awb_calc_output_t *data)
{
int ret = 0;
awb_merger_calc_data_t *e = NULL;
per_camera_awb_merger_data_t *c;


    if ((g_awb_merger.ref_counter > 0) && (NULL != prv) && (prv->camera_id < MAX_AWB_MERGE_CAMERAS))
    {

        if (NULL == g_awb_merger.entry)
        {
            g_awb_merger.entry = ex_pool_alloc_timeout(g_awb_merger.g_ctx.pool_awb_merger, AWB_MERGER_THREAD_TIMEOUT);
            if (NULL == g_awb_merger.entry)
            {
                mmsdbg(DL_ERROR, "Pool timed out");
                return 0;
            }
            awb_merger_init_entry(g_awb_merger.entry);
        }
        e = g_awb_merger.entry;
        c = &e->in_data[prv->camera_id];

        osal_memcpy(&c->v, data, sizeof(awb_calc_output_t));
        if (c->valid)
        {
            mmsdbg(DL_ERROR, "Camera instance %d too slow", (int)prv->camera_id);
        } else {
            c->valid = 1;
            e->num_valid++;
        }

        if (e->num_valid >= g_awb_merger.ref_counter)
        {
            if (NULL == g_awb_merger.for_process_entry)
            {
                g_awb_merger.for_process_entry = g_awb_merger.entry;
                awb_merger_prepare_for_processing(g_awb_merger.for_process_entry);
                ret = 1;
            } else {
                mmsdbg(DL_ERROR, "Algorithm too slow");
                osal_free(g_awb_merger.entry);
            }
            g_awb_merger.entry = NULL;
        }
    } else
    {
        mmsdbg(DL_ERROR, "Internal error");
    }

    return ret;
}



void awb_merger_thread_exec(func_thread_t *ethr, void *prv);

static func_thread_handle_t awb_merger_thread_fxns[] = {
    awb_merger_thread_exec,
};

static func_thread_handles_t awb_merger_thread_handles = {
    .fxns = awb_merger_thread_fxns,
    .size = ARRAY_SIZE(awb_merger_thread_fxns)
};

void awb_merger_thread_exec(func_thread_t *ethr, void *prv)
{

    awb_merger_priv_data_t      *p = prv;

    mmsdbg(DL_FUNC, "AWB Merger Exec");
//    PROFILE_ADD(PROFILE_ID_ALGOS_AWB_MERGER_RUN, 0, 0);

    if((!p) || (!p->pctx)){
        mmsdbg(DL_ERROR, "Null pointer");
        return;
    }

    {
        aca_awb_merger_calc_output_t out_awb_merger;
        awb_calc_output_t  awb_merged[MAX_AWB_MERGE_CAMERAS];
        aca_awb_merger_calc_input_t   in_awb_merger;
        int cam;

        in_awb_merger.num_entries = g_awb_merger.for_process_entry->num_total;
        in_awb_merger.p_in_data   = g_awb_merger.for_process_entry->in_data;

        out_awb_merger.out_data = awb_merged;
        aca_awb_merger_process(p->pctx->alg_hndl, &in_awb_merger, &out_awb_merger);

        for (cam = 0; cam < in_awb_merger.num_entries; ++cam)
        {
            if (in_awb_merger.p_in_data[cam].valid)
            {
                guzzi_event_send(
                        p->pctx->evt_hndl,
                        geg_camera_event_mk(cam, CAM_EVT_AWB_MERGER_READY),
                        0,
                        &awb_merged[cam]
                    );
            }
        }
    }
    osal_free(g_awb_merger.for_process_entry);
    g_awb_merger.for_process_entry = NULL;
}

/* ========================================================================== */
/**
* aca_awb_merger_thr_process()
*/
/* ========================================================================== */
int aca_awb_merger_thr_process(
                                awb_merger_priv_data_t *prv,
                                awb_calc_output_t      *in_data
                             )
{
    if (awb_merger_add_data(prv, in_data))
    {
        func_thread_exec(prv->pctx->awb_merger_thr, awb_merger_thread_exec);
    }
    return 0;
}

int aca_awb_merger_thr_start(awb_merger_priv_data_t* prv)
{
    return 0;
}

int aca_awb_merger_thr_stop(awb_merger_priv_data_t* prv)
{
    return 0;
}

int aca_awb_merger_thr_config(awb_merger_priv_data_t* prv, void *new_cfg)
{
    aca_awb_merger_config_t *cfg = new_cfg;

    g_awb_merger_weights[prv->camera_id]  = cfg->weight;

    return 0;
}


////////////////////////////////////////////////////////////////////////////////////


void aca_awb_merger_evt_notify_callback (void *inc_prv, int event_id,
                            uint32 num, void *data)
{
    awb_merger_priv_data_t *awb_merger_prv = inc_prv;

    if (num) {
        // TODO
        /*
         *  it is a non ZSL capture sequence events
         *  we will skip it
         */
        return;
    }

    mmsdbg(DL_FUNC, "Enter num: %d evt: %s",  num, guzzi_event_global_id2str(event_id));

    if (osal_sem_wait_timeout(awb_merger_prv->pctx->sem, AWB_MERGER_THREAD_TIMEOUT))
    {
        mmsdbg(DL_ERROR, "Timed out");
        return;
    }

    switch (geg_camera_event_get_eid(event_id)) {
            case CAM_EVT_AWB_READY:
                aca_awb_merger_thr_process(awb_merger_prv, data);
                break;
    }
    osal_sem_post(awb_merger_prv->pctx->sem);
    mmsdbg(DL_FUNC, "Exit %s", 0 ? "Error" : "Ok");
    return;
}


static void awb_merger_unregister_evt (awb_merger_priv_data_t   *awb_merger_prv)
{
    guzzi_event_unreg_recipient(awb_merger_prv->pctx->evt_hndl,
                                geg_camera_event_mk(awb_merger_prv->camera_id, CAM_EVT_AWB_READY),
                                aca_awb_merger_evt_notify_callback,
                                awb_merger_prv);
}

/* ========================================================================== */
/**
* void cl_uregister_evt()
*/
/* ========================================================================== */
static int awb_merger_register_evt (awb_merger_priv_data_t   *awb_merger_prv)
{
    if(guzzi_event_reg_recipient(awb_merger_prv->pctx->evt_hndl,
                                 geg_camera_event_mk(awb_merger_prv->camera_id, CAM_EVT_AWB_READY),
                                 aca_awb_merger_evt_notify_callback,
                                 awb_merger_prv))
    {
        goto ERROR_1;
    }

    return 0;

ERROR_1:
    return -1;
}

awb_merger_priv_data_t* aca_awb_merger_thr_create(awb_merger_thread_create_params_t *params)
 {
    awb_merger_priv_data_t *prv;
    struct awb_merger_common_data *pctx;

    prv = osal_calloc(1, sizeof(*prv));
    osal_assert(prv != NULL);

    pctx = prv->pctx = &g_awb_merger.g_ctx;
    prv->camera_id = params->camera_id;

    if (g_awb_merger.ref_counter == 0)
    {
        mmsdbg(DL_ERROR, "AEWB Merger: First camera instance %d", (int)params->camera_id);

        osal_memset(&g_awb_merger.g_ctx, 0, sizeof(g_awb_merger.g_ctx));

        pctx->evt_hndl = params->evt_hndl;
        pctx->sem = osal_sem_create(1);
        osal_assert(pctx->sem != NULL);

        pctx->awb_merger_thr = func_thread_create("ACA AWB_MERGER THREAD",
                                                  AWB_MERGER_THREAD_STACK_SIZE,
                                                  AWB_MERGER_THREAD_PRIORITY,
                                                 &awb_merger_thread_handles,
                                                  prv);
        osal_assert(pctx->awb_merger_thr != NULL);

        pctx->pool_awb_merger = ex_pool_create("CAM AE MERGER POOL", &awb_merger_root, AWB_MERGER_NUM_POOL_ENTRIES);
        osal_assert(pctx->pool_awb_merger != NULL);

        aca_awb_merger_create(&pctx->alg_hndl);
        osal_assert(pctx->alg_hndl != NULL);

        g_awb_merger.entry = NULL;
        g_awb_merger.for_process_entry = NULL;

    }
    awb_merger_register_evt(prv);
    g_awb_merger.ref_counter++;
    return prv;
 }

void aca_awb_merger_thr_destroy(awb_merger_priv_data_t* prv)
{

    if ((g_awb_merger.ref_counter) && (prv) && (prv->pctx))
    {
        awb_merger_unregister_evt(prv);
        g_awb_merger.ref_counter--;
        if (g_awb_merger.ref_counter == 0)
        {
            mmsdbg(DL_ERROR, "AEWB Merger: Last camera instance");

            if (prv->pctx->pool_awb_merger)
            {
                if (g_awb_merger.entry)
                {
                    osal_free(g_awb_merger.entry);
                }
                g_awb_merger.entry = NULL;

                if (g_awb_merger.for_process_entry)
                {
                    mmsdbg(DL_ERROR, "ERROR: for_process_entry is still valid");
                    osal_free(g_awb_merger.for_process_entry);
                }
                g_awb_merger.for_process_entry = NULL;


                ex_pool_destroy(prv->pctx->pool_awb_merger);
                prv->pctx->pool_awb_merger = NULL;
            }
            func_thread_destroy(prv->pctx->awb_merger_thr);
            osal_sem_destroy(prv->pctx->sem);
            aca_awb_merger_destroy(prv->pctx->alg_hndl);
        }
        osal_free(prv);
    } else {
        mmsdbg(DL_ERROR, "Invalid instance");
    }
}

