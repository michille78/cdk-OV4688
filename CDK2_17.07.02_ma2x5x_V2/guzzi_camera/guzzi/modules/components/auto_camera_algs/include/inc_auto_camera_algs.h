/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file inc_auto_camera_algs.h
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#ifndef _INC_AUTO_CAM_ALGS_H
#define _INC_AUTO_CAM_ALGS_H

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include <osal/osal_stdtypes.h>
#include <pipe/include/inc.h>

typedef struct {
    uint32      fr_offset_cfg;
    uint32      fr_offset_cl_img;
    uint32      fr_offset_cl_aca;
    uint32      fr_offset_algo_state;
    uint32      fr_offset_aewb_stats;
    uint32      fr_offset_af_stats;
    uint32      fr_offset_vpipe;
    uint32      res_offset_cfg;
    uint32      res_offset_cametra_id;
    uint32      max_fr;
} INC_AUTO_CAM_ALGS_CPARAMS_T;

int inc_auto_cam_algs_create(
        inc_t *inc,
        void *paramsr,
        void *app_res
    );

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* _INC_AUTO_CAM_ALGS_H */

