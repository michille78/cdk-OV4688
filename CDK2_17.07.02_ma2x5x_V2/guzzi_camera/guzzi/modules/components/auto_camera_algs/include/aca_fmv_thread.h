/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file aca_fmv_thread.h
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#ifndef ACA_FMV_THREAD_H
#define ACA_FMV_THREAD_H

#include "cam_cl_frame_req.h"
#include <framerequest/camera/camera_frame_request.h>
#include "aca_fmv.h"
#include "cam_algo_state.h"

typedef struct fmv_thread_data fmv_thread_data_t;

typedef struct {
    guzzi_event_t       *evt_hndl;
    uint32              camera_id;
} fmv_thread_create_params_t;

int aca_fmv_thr_process(fmv_thread_data_t     *prv,
                                    hat_h3a_aewb_stat_t     *fmv_stat,
                                    cam_algo_state_t        *algo_state,
                                    const hat_wbal_coef_t   *wbal,
                                    cam_cl_algo_ctrl_t      *algo_ctrl,
                                    hat_reg_pri_t           *regs_af,
                                    hat_size_t              *size,
                                    hat_rect_t              *crop);

fmv_thread_data_t* aca_fmv_thr_create(fmv_thread_create_params_t *params);
void aca_fmv_thr_destroy(fmv_thread_data_t *prv);
int aca_fmv_thr_start(fmv_thread_data_t *prv);
int aca_fmv_thr_stop(fmv_thread_data_t *prv);
int aca_fmv_thr_config(fmv_thread_data_t *prv, void *cfg);


#endif /* ACA_FMV_THREAD_H */
