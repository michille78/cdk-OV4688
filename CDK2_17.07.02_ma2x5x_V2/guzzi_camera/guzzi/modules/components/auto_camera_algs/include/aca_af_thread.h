/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file aca_af_thread.h
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#ifndef ACA_AF_THREAD_H
#define ACA_AF_THREAD_H

#include "cam_cl_frame_req.h"
#include <framerequest/camera/camera_frame_request.h>
#include "hal/hat_h3a_af.h"
#include "cam_algo_state.h"
#include <guzzi_event/include/guzzi_event.h>

typedef struct af_thread_data af_thread_data_t;

typedef struct {
    guzzi_event_t       *evt_hndl;
    uint32              camera_id;
} af_thread_create_params_t;

int aca_af_thr_process(af_thread_data_t     *prv,
                                    hat_h3a_af_stat_t       *af_stat,
                                    cam_algo_state_t        *algo_state,
                                    const hat_wbal_coef_t   *wbal,
                                    cam_cl_algo_ctrl_t      *algo_ctrl,
                                    hat_reg_pri_t           *regs_af,
                                    hat_size_t              *size,
                                    hat_rect_t              *crop);

af_thread_data_t* aca_af_thr_create(af_thread_create_params_t *params);
void aca_af_thr_destroy(af_thread_data_t* prv);
int aca_af_thr_start(af_thread_data_t* prv);
int aca_af_thr_stop(af_thread_data_t* prv);
int aca_af_thr_config(af_thread_data_t* prv, void *cfg);

#endif /* ACA_AF_THREAD_H */
