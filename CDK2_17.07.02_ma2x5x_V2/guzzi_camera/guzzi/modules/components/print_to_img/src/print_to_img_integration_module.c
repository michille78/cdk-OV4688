/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file print_to_img_integration_module.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <osal/osal_stdlib.h>
#include <osal/osal_string.h>
#include <utils/mms_debug.h>

#include <framerequest/camera/camera_frame_request.h>
#include <pipe/include/inc.h>
#include <print_to_img/include/print_to_img_integration_module.h>

struct int_print_to_img {
    uint32       fr_offset_img;
    char         *text;
    unsigned int ypos;
    unsigned int frame_number;
    char text_buf[256];
};

mmsdbg_define_variable(
        vdl_inc_print_to_img,
        DL_DEFAULT,
        0,
        "inc.print_to_img",
        "Print to image INC"
    );
#define MMSDEBUGLEVEL mmsdbg_use_variable(vdl_inc_print_to_img)

#define DRAW_TEXT_FONT_HEIGHT 8
#define DRAW_TEXT_FONT_WIDTH 8
#define DRAW_TEXT_PIXEL_SIZE_X 1
#define DRAW_TEXT_PIXEL_SIZE_Y 1
#define DRAW_TEXT_BG_COLOR 0x1080
#define DRAW_TEXT_FG_COLOR 0xE080
typedef unsigned char font_t;
static font_t    draw_text_font[] =
{
#include "8x8cp850.hhh"
};
/* ========================================================================== */
/**
* @fn drawText() TODO:
*
*  @see
*/
/* ========================================================================== */
static void drawText(void *buf, char *text, int xpos, int ypos, int ppln)
{
    font_t   *fnt = draw_text_font;
    int       fnth = DRAW_TEXT_FONT_HEIGHT;
    int       fntw = DRAW_TEXT_FONT_WIDTH;

    int    i, j, k;

    int    strl = strlen(text);
    int    xpix = DRAW_TEXT_PIXEL_SIZE_X;
    int    ypix = DRAW_TEXT_PIXEL_SIZE_Y;

    int    line_inc = ppln - xpix * strl * fntw;

    unsigned short int   *b = (unsigned short int *)buf + ppln * ypos + xpos;

    for( i = 0; i < fnth * ypix; i++ ) {
        for( j = 0; j < strl; j++ ) {
            unsigned int    sl = fnt[((int)text[j]) * fnth + i / ypix];

            for( k = fntw * xpix - 1; 0 <= k; k-- ) {
                *(b++) = (sl & (1 << k / xpix)) ? DRAW_TEXT_FG_COLOR : DRAW_TEXT_BG_COLOR;
            }
        }

        b += line_inc;
    }
}

/* ========================================================================== */
/**
* @fn inc_print_to_img_process()
*
*  @see
*/
/* ========================================================================== */
static void inc_print_to_img_process(inc_t *inc, void *data)
{
    int_print_to_img_t  *prv;
    camera_fr_t         *fr;
    camera_fr_entry_t   *fr_entry;
    void *buff;
    int ppln;

    if (!data) {
        mmsdbg(DL_ERROR, "Invalid data!");
        goto exit1;
    }
    fr = data;

    if (!inc->prv.ptr) {
        mmsdbg(DL_ERROR, "Invalid private data!");
        goto exit1;
    }
    prv = inc->prv.ptr;

    fr_entry = CAMERA_FR_GET_ENTRY(fr, prv->fr_offset_img);
    if (!fr_entry || !fr_entry->data) {
        mmsdbg(DL_ERROR, "Invalid fr entry!");
        goto exit1;
    }
    buff = fr_entry->data; /* TODO: */
    ppln = 0; /* TODO: */
    //if (AIBH_IMG_FORMAT_UYVY != ) {
    //    mmsdbg(DL_ERROR, "Unsupported format!");
    //    goto exit1;
    //}

    sprintf(
            prv->text_buf,
            "F: %4d - %s | P=%p Y=%d",
            ++prv->frame_number,
            prv->text,
            prv,
            prv->ypos
        );
    drawText(
            buff,
            prv->text_buf,
            0,
            DRAW_TEXT_FONT_HEIGHT * prv->ypos,
            ppln
        );

exit1:
    inc->callback(
                inc,
                inc->client_prv,
                0,
                0,
                data
            );
}

/* ========================================================================== */
/**
* @fn inc_print_to_img_destroy()
*
*  @see
*/
/* ========================================================================== */
static void inc_print_to_img_destroy(inc_t *inc)
{
    int_print_to_img_t *prv;
    prv = inc->prv.ptr;
    osal_free(prv);
}

/* ========================================================================== */
/**
* @fn inc_print_to_img_create()
*
*  @see
*/
/* ========================================================================== */
int inc_print_to_img_create(
        inc_t *inc,
        void *params,
        void *app_res
    )
{
    int_print_to_img_t *prv;
    inc_print_to_img_params_t *static_params;

    prv = osal_malloc(sizeof (*prv));
    if (!prv) {
        mmsdbg(
                DL_ERROR,
                "Failed to allocate memory for new print_to_img INC instance: size=%d!",
                sizeof (*prv)
            );
        goto exit1;
    }
    inc->prv.ptr = prv;

    static_params = params;

    prv->fr_offset_img = static_params->fr_offset_img;
    prv->text          = static_params->text;
    prv->ypos          = static_params->ypos;

    inc->inc_start = NULL;
    inc->inc_stop = NULL;
    inc->inc_flush = NULL;
    inc->inc_config_alter = NULL;
    inc->inc_config  = NULL;
    inc->inc_process = inc_print_to_img_process;
    inc->inc_destroy = inc_print_to_img_destroy;

    return 0;
exit1:
    return -1;
}

