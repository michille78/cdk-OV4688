/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file inc_camera.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <osal/osal_stdlib.h>
#include <osal/osal_thread.h>
#include <osal/osal_mutex.h>
#include <osal/osal_string.h>
#include <osal/osal_assert.h>
#include <utils/mms_debug.h>
#include <pipe/include/inc.h>
#include <framerequest/camera/camera_frame_request.h>
#include <error_handle/include/error_handle.h>
#include <osal/osal_list.h>

#include "fifo/include/fifo_sem.h"
#include "inc_camera.h"

#include <osal/ex_pool.h>
#include <osal/list_pool.h>
#include <osal/pool.h>
#include <hal/hat_h3a_aewb.h>
#include <hal/hat_h3a_af.h>
#include <camera/vcamera_iface/vpipe_control/include/vpipe_control.h>
#include <camera/vcamera_iface/virt_cm/inc/virt_cm.h>

#include "cam_algo_state.h"
#include <guzzi_event/include/guzzi_event.h>
#include <guzzi_event_global/include/guzzi_event_global.h>
#include "cam_config.h"
#include "IspCommon.h"
#include "ipipe.h"
#include <aca_hllc_types.h>

inc_camera_priv_data_t *pCamPrv; // for debug purposes
static uint32 last_mov_ts = 0;
const char *lens_descr[] = {
    "LENS_MOVE_ABORT",
    "LENS_MOVE_ERROR",
    "LENS_MOVE_LAST",
    "LENS_MOVE_PROGRESS"
};

mmsdbg_define_variable(
        vdl_inc_camera,
        DL_DEFAULT,
        0,
        "inc.camera",
        "INC Camera"
    );

#define MMSDEBUGLEVEL mmsdbg_use_variable(vdl_inc_camera)

ex_pool_node_t aewb_desc [] = {
     EX_POOL_NODE_DESC(NULL, (sizeof(hat_h3a_aewb_entry_t) * HAT_H3A_AEWB_PAX_MAX), hat_h3a_aewb_stat_t, paxels),
     EX_POOL_LIST_END
};

ex_pool_node_t af_desc [] = {
     EX_POOL_NODE_DESC(NULL, (sizeof(hat_h3a_af_stat_entry_t) * HAT_H3A_AF_PAX_MAX), hat_h3a_af_stat_t, paxels),
     EX_POOL_LIST_END
};

ex_pool_node_t aewb_root =
{
     EX_POOL_ROOT(&aewb_desc, sizeof (hat_h3a_aewb_stat_t))
};

ex_pool_node_t af_root =
{
     EX_POOL_ROOT((&af_desc), sizeof (hat_h3a_af_stat_t))
};

typedef enum {
    INC_CAM_FLAGS_CLEAR = 0,
    INC_CAM_CAPTURE_FLAG = 1 << 0,
} inc_cam_flags_t;

typedef enum{
    PRC_FRM_STATE_UNKNOWN,
    PRC_FRM_STATE_STARTED,
    PRC_FRM_STATE_LINE_REACHED,
    PRC_FRM_STATE_END,
}processing_frame_state_t;

typedef struct {
    struct list_head node;
    void*           fd;
    void*           hw_config;
    void*           inc_camera_prv;
    osal_timeval    frame_start_ts;
    inc_cam_flags_t flags;
}cam_fd_entry_t;

/** inc_camera_priv_data_t
* Defining the gzz module application data. */
struct inc_camera_priv_data {
    uint32      config_fr_cnt;
    uint32      fr_offset_cfg;
    uint32      fr_cl_img_offset;
    uint32      fr_offset_lights;
    uint32      fr_offset_aewb_stats;
    uint32      fr_offset_af_stats;
    uint32      fr_offset_algo_state;
    uint32      fr_offset_vpipe;
    uint32      fr_offset_cm_dyn_data;
    uint32      max_fr;
    uint32      res_offset_cfg;
    uint32      res_offset_cametra_id;

    void        *vpipe_hndl;
    virt_cm_hndl_t virt_cm_hndl;

    ex_pool_t   *pool_aewb;     // pool for aewb stats buffers
    ex_pool_t   *pool_af;       // pool for af stats buffers

    pool_t      *pool_cam_fd;  // simple pool for cam_fd_entry_t
    pool_t      *pool_cm_dyn_data; // simple pool for virt_cm_dyn_props_t
    osal_sem    *fd_list_lock;

    struct list_head fd_received_list;
    struct list_head fd_pending_list;
    struct list_head fd_in_process_list;
    osal_sem    *fd_flush_lock;     // lock flush until all outstanding FR are not processed

    uint32      ipipe_running;

    virt_cm_sen_mode_features_t sen_features;
    uint32              sensor_read_row_time;
    hat_size_t           in_size;

    hat_h3a_aewb_cfg_t h3a_aewb_cfg;
    hat_h3a_af_cfg_t   h3a_af_cfg;

    guzzi_event_t *evt_hdl;
    uint32 camera_id;

    struct {
        unsigned int gzz;
        unsigned int cam_mode;
        unsigned int cam_resloution;
        unsigned int sen_crop;
        unsigned int fr_duration;
        unsigned int fps_range;
        unsigned int aperture;
        unsigned int filter_density;
        unsigned int focal_length;
        unsigned int o_stab_mode;
        unsigned int usecase;
    }cfg_seq_num;

    int sen_started;

    aca_hllc_calc_output_t hllc_out;
    lens_movement_data_t   lens_movement;

//    osal_mutex  *events_lock;
};

/* =============  Forward definitions ============== */

static int inc_cam_lens_move_done_cb (void* user_data,
                                LENS_MOVE_DESCR_T descr,
                                float micro_step_start,
                                float micro_step_end,
                                uint32 time_sleep_us);

static int prepare_stats_buffs (inc_camera_priv_data_t *prv, void *data);

static int _camera_config_internal(inc_camera_priv_data_t  *pCompPrv, cam_cfg_t *p_cfg);

static int inc_camera_process_finish(inc_t *inc,cam_fd_entry_t *ent);

static void inc_cam_evt_notify (void *inc_prv, int event_id,
                            uint32 num, void *data);

static int lens_mov_populate_entry(inc_camera_priv_data_t *prv,
                                LENS_MOVE_DESCR_T descr,
                                float micro_step_start,
                                float micro_step_end,
                                uint32 step_duration_us);

int vpipe_convert_aewb_stats (vpipe_ctrl_hndl_t hndl,
                              void *hw_cfg,
                              void* vstats,
                              uint32 row_time);
int vpipe_convert_af_stats (vpipe_ctrl_hndl_t hndl,
                            void *hw_cfg,
                            void* vstats,
                            uint32 row_time,
                            lens_movement_data_t *p_lm);

/* =============  Forward definitions END ============ */

static inline void generate_events(inc_camera_priv_data_t *prv, cam_fd_entry_t *ent, uint8 evt_filter, uint32 event)
{
    cam_fr_buff_ctrl_list_t *cl_req;
    cam_cl_algo_ctrl_list_t *list_ent;
    cam_cl_algo_ctrl_t      *algo_ctrl;

    cl_req = CAMERA_FR_GET_ENTRY(ent->fd, prv->fr_cl_img_offset)->data;

    if (!list_empty(&cl_req->list))
    {
        list_for_each_entry(list_ent, &cl_req->list, link)
        {
            algo_ctrl = &list_ent->ctrl;

            if (algo_ctrl->processing_flags & evt_filter)
            {
                guzzi_event_send(prv->evt_hdl, event, algo_ctrl->id, algo_ctrl);
            }
        }
    }
}

static inline osal_timeval get_timestamp_usec(void)
{
    osal_timeval tv;
    osal_get_timeval(&tv);
    return (tv);
}

static int inc_cam_lens_move_done_cb (void* user_data,
                                LENS_MOVE_DESCR_T descr,
                                float micro_step_start,
                                float micro_step_end,
                                uint32 time_sleep_us)
{
    inc_camera_priv_data_t *prv = user_data;
    int ret;

    osal_assert(prv);

    osal_mutex_lock_timeout(prv->lens_movement.lens_lock, DEFAULT_ALLOC_TIMEOUT_MS);
    prv->lens_movement.last_state = descr;
    ret = lens_mov_populate_entry(prv, descr, micro_step_start, micro_step_end, time_sleep_us);
    osal_mutex_unlock(prv->lens_movement.lens_lock);
    return ret;
}

static void inc_cam_evt_notify (void *inc_prv, int event_id,
                            uint32 num, void *data)
{
    inc_camera_priv_data_t *cam_prv = inc_prv;
    int flag_start_move = 0;

    mmsdbg(DL_FUNC, "Enter num: %d evt: %s",  num, guzzi_event_global_id2str(event_id));
//    osal_mutex_lock(cam_prv->events_lock);
    switch (geg_camera_event_get_eid(event_id)) {
    case CAM_EVT_MOVE_LENS:
        osal_memcpy(&cam_prv->hllc_out, data, sizeof(cam_prv->hllc_out));
        break;
    default:
        mmsdbg(DL_ERROR, "Unhandled event");
        return;
    }
//    osal_mutex_unlock(cam_prv->events_lock);

    switch (geg_camera_event_get_eid(event_id)) {
        case CAM_EVT_MOVE_LENS:

            osal_mutex_lock_timeout(cam_prv->lens_movement.lens_lock, DEFAULT_ALLOC_TIMEOUT_MS);
                cam_prv->lens_movement.seq_cnt = cam_prv->hllc_out.seq_cnt;
                cam_prv->lens_movement.abort   = (cam_prv->hllc_out.action == HLLC_ACTION_ABORT)?1:0;

                if (cam_prv->lens_movement.last_state != LENS_MOVE_PROGRESS)
                {
                    flag_start_move = 1;
                }

            osal_mutex_unlock(cam_prv->lens_movement.lens_lock);

            if (cam_prv->hllc_out.action == HLLC_ACTION_MOVE2POS)
            {
                hat_cm_lens_micro_step_t* step = hat_cm_lens_move_list_get_tail(cam_prv->hllc_out.step_list);
                mmsdbg(DL_MESSAGE, "Mov2Pos cnt %d pos %f time %d",
                       hat_cm_lens_move_list_num(cam_prv->hllc_out.step_list),
                       step->lens_pos,
                       step->time_sleep_us
                       );
                if (hat_cm_lens_move_list_num(cam_prv->hllc_out.step_list))
                {

                    if (flag_start_move)
                    {
                        virt_cm_lens_move_to_pos(cam_prv->virt_cm_hndl,
                                                     cam_prv->hllc_out.step_list,
                                                     inc_cam_lens_move_done_cb , cam_prv);
                    } else {
                        mmsdbg(DL_ERROR, "Lens move command received while lens is in progress");
                    }
                }

            }
            break;
        default:
            mmsdbg(DL_ERROR, "Unhandled event");
            return;
    }
}

static int lens_mov_populate_entry(inc_camera_priv_data_t *prv,
                                LENS_MOVE_DESCR_T descr,
                                float micro_step_start,
                                float micro_step_end,
                                uint32 step_duration_us)
{
    lens_mov_entry_t *p_ent, *p_ent_prev;


    p_ent = &prv->lens_movement.entries[prv->lens_movement.head&LENS_MOV_MASK];
    // advance to next entry
    prv->lens_movement.head++;
    if ((prv->lens_movement.head == prv->lens_movement.tail_end) ||
        (prv->lens_movement.head == prv->lens_movement.tail_st))
    {
        prv->lens_movement.head--;
        mmsdbg (DL_ERROR, "Lens movement history is full");
        return -1;
    }

    p_ent->descr = descr;
    p_ent->start_ts  = get_timestamp_usec();
    p_ent->end_ts    = p_ent->start_ts + step_duration_us;
    p_ent->s_pos = micro_step_start;
    p_ent->e_pos = micro_step_end;
    p_ent->pos_to_time_move = (float)step_duration_us*(micro_step_end + micro_step_start)/2;
    p_ent->pos_to_time_stop = 0.0;

    // If we have previous move calculate pos_to_time_stop
    if(prv->lens_movement.head >= 2)
    {
        p_ent_prev = &prv->lens_movement.entries[(prv->lens_movement.head - 2)&LENS_MOV_MASK];
        p_ent->pos_to_time_stop = (float)(p_ent->start_ts - p_ent_prev->end_ts) *
                                  (p_ent->s_pos + p_ent_prev->e_pos)/2;
    }
    prv->lens_movement.actual_lens_position = micro_step_end;
    PROFILE_ADD(PROFILE_ID_SENSOR_WRITE_SETTINGS, (uint32)(prv->lens_movement.actual_lens_position*1023),0);
    mmsdbg(DL_MESSAGE, "%s: -----> ts %jd delta %jd type %s start %f end %f time %d usec\n",
           __func__,
           p_ent->start_ts,
           p_ent->start_ts - last_mov_ts,
           lens_descr[descr],
           micro_step_start,
           micro_step_end,
           step_duration_us);

    last_mov_ts = p_ent->start_ts;

    return 0;
}


/* ========================================================================== */
/**
* _camera_config_internal()
*/
/* ========================================================================== */
static int _camera_config_internal(inc_camera_priv_data_t  *pCompPrv, cam_cfg_t *p_cfg)
{
    int cfg_change_flag = 0;

    mmsdbg(DL_FUNC, "Enter");

    if (pCompPrv->cfg_seq_num.gzz != p_cfg->cam_gzz_cfg.ctrl.seq_num) {
        pCompPrv->cfg_seq_num.gzz = p_cfg->cam_gzz_cfg.ctrl.seq_num;

        if (pCompPrv->cfg_seq_num.cam_mode != p_cfg->cam_gzz_cfg.cam_mode.ctrl.seq_num) {
            pCompPrv->cfg_seq_num.cam_mode = p_cfg->cam_gzz_cfg.cam_mode.ctrl.seq_num;
            cfg_change_flag = 1;
            virt_cm_sen_config(pCompPrv->virt_cm_hndl, VSEN_CFG_INTENT,
                             &p_cfg->cam_gzz_cfg.cam_mode.val);
        }

        if (pCompPrv->cfg_seq_num.usecase != p_cfg->cam_gzz_cfg.custom_usecase_selection.ctrl.seq_num) {
            pCompPrv->cfg_seq_num.usecase  = p_cfg->cam_gzz_cfg.custom_usecase_selection.ctrl.seq_num;
            cfg_change_flag = 1;
            virt_cm_sen_config(pCompPrv->virt_cm_hndl, VSEN_CFG_USECASE,
                             &p_cfg->cam_gzz_cfg.custom_usecase_selection.val);
        }



        if (pCompPrv->cfg_seq_num.cam_resloution != p_cfg->cam_gzz_cfg.stream.ctrl.seq_num) {
            pCompPrv->cfg_seq_num.cam_resloution = p_cfg->cam_gzz_cfg.stream.ctrl.seq_num;
            cfg_change_flag = 1;
            virt_cm_sen_config(pCompPrv->virt_cm_hndl, VSEN_CFG_RESOLUTION,
                             &p_cfg->cam_gzz_cfg.stream.val[0].size);
        }
        if (pCompPrv->cfg_seq_num.sen_crop != p_cfg->cam_gzz_cfg.sensor_crop.ctrl.seq_num) {
            pCompPrv->cfg_seq_num.sen_crop = p_cfg->cam_gzz_cfg.sensor_crop.ctrl.seq_num;
            cfg_change_flag = 1;
            virt_cm_sen_config(pCompPrv->virt_cm_hndl, VSEN_CFG_CROP,
                             &p_cfg->cam_gzz_cfg.sensor_crop.val);
        }
        if ((p_cfg->cam_gzz_cfg.ae_mode.val != CAM_AE_MODE_OFF) && (p_cfg->cam_gzz_cfg.aca_mode.val != CAM_ACA_MODE_OFF))
        {
            if (pCompPrv->cfg_seq_num.fps_range != p_cfg->cam_gzz_cfg.fps_range.ctrl.seq_num)
            {
                pCompPrv->cfg_seq_num.fps_range = p_cfg->cam_gzz_cfg.fps_range.ctrl.seq_num;
                cfg_change_flag = 1;
                virt_cm_sen_config(pCompPrv->virt_cm_hndl, VSEN_CFG_FPS_RANGE  ,
                                 &p_cfg->cam_gzz_cfg.fps_range.val);
            }
        }
        else
        {
            if (pCompPrv->cfg_seq_num.fr_duration != p_cfg->cam_gzz_cfg.fr_duration.ctrl.seq_num)
            {
                hat_range_float_t fps_range;

                cfg_change_flag = 1;

                pCompPrv->cfg_seq_num.fr_duration = p_cfg->cam_gzz_cfg.fr_duration.ctrl.seq_num;
                fps_range.min = fps_range.max = 1000000000.0 / p_cfg->cam_gzz_cfg.fr_duration.val;
                virt_cm_sen_config(pCompPrv->virt_cm_hndl, VSEN_CFG_FPS_RANGE  ,
                                       &fps_range);
            }
        }
        if (pCompPrv->cfg_seq_num.aperture != p_cfg->cam_gzz_cfg.aperture.ctrl.seq_num) {
            pCompPrv->cfg_seq_num.aperture = p_cfg->cam_gzz_cfg.aperture.ctrl.seq_num;
            virt_cm_lens_config(pCompPrv->virt_cm_hndl, VLENS_CFG_APERTURE,
                             &p_cfg->cam_gzz_cfg.aperture.val);
        }
        if (pCompPrv->cfg_seq_num.filter_density != p_cfg->cam_gzz_cfg.filter_density.ctrl.seq_num) {
            pCompPrv->cfg_seq_num.filter_density = p_cfg->cam_gzz_cfg.filter_density.ctrl.seq_num;
            virt_cm_lens_config(pCompPrv->virt_cm_hndl, VLENS_CFG_DENS_FILT,
                             &p_cfg->cam_gzz_cfg.filter_density.val);
        }
        if (pCompPrv->cfg_seq_num.focal_length != p_cfg->cam_gzz_cfg.focal_length.ctrl.seq_num) {
            pCompPrv->cfg_seq_num.focal_length = p_cfg->cam_gzz_cfg.focal_length.ctrl.seq_num;
            virt_cm_lens_config(pCompPrv->virt_cm_hndl, VLENS_CFG_FOC_LENGTH,
                             &p_cfg->cam_gzz_cfg.focal_length.val);
        }
        if (pCompPrv->cfg_seq_num.o_stab_mode != p_cfg->cam_gzz_cfg.optical_stab_mode.ctrl.seq_num) {
            pCompPrv->cfg_seq_num.o_stab_mode = p_cfg->cam_gzz_cfg.optical_stab_mode.ctrl.seq_num;
            virt_cm_lens_config(pCompPrv->virt_cm_hndl, VLENS_CFG_O_STAB_MODE,
                             &p_cfg->cam_gzz_cfg.optical_stab_mode.val);
        }
        if (cfg_change_flag)
        {
            int old_idx;

            old_idx = pCompPrv->sen_features.sen_mode_idx;
            virt_cm_get_mode_features(pCompPrv->virt_cm_hndl, &pCompPrv->sen_features);

            if (old_idx != pCompPrv->sen_features.sen_mode_idx)
            {
                const hat_sensor_mode_t *sen_mode;
                sen_mode = &pCompPrv->sen_features.sen->modes.list[pCompPrv->sen_features.sen_mode_idx];
                pCompPrv->sensor_read_row_time = sen_mode->row_time; //*sen_mode->field_of_view.h)/sen_mode->active.h;

                guzzi_event_send(
                        pCompPrv->evt_hdl,
                        geg_camera_event_mk(pCompPrv->camera_id, CAM_EVT_SEN_MODE_CHANGE),
                        0,
                        &pCompPrv->sen_features
                    );
            }
        }
    }

    mmsdbg(DL_FUNC, "Exit Ok");

    return 0;
}

/* ========================================================================== */
/**
* prepare_stats_buffs()
*/
/* ========================================================================== */
static int prepare_stats_buffs (inc_camera_priv_data_t *prv, void *data)
{
    camera_fr_entry_t   *cl_fr_entry;
    vpipe_ctrl_settings_t   *vpipe;
    hat_h3a_aewb_stat_t *aewb_stat;
    hat_h3a_af_stat_t   *af_stat;

    int ret = 0;

    cl_fr_entry = CAMERA_FR_GET_ENTRY(data, prv->fr_offset_aewb_stats);
    vpipe       = CAMERA_FR_GET_ENTRY(data, prv->fr_offset_vpipe)->data;

    aewb_stat = ex_pool_alloc_timeout(prv->pool_aewb, DEFAULT_ALLOC_TIMEOUT_MS);

    if(aewb_stat)
    {
        aewb_stat->cfg = *vpipe->h3a_aewb.data;
        cl_fr_entry->data = aewb_stat;
        cl_fr_entry->com.fmt = CAMERA_FR_ENTRY_FORMAT__OSAL;
    } else
    {
        mmsdbg(DL_ERROR, "AEWB FIFO Timeout");
        cl_fr_entry->data = NULL;
        cl_fr_entry->com.fmt = CAMERA_FR_ENTRY_FORMAT__UNUSED;
    }

    cl_fr_entry = CAMERA_FR_GET_ENTRY(data, prv->fr_offset_af_stats);
    af_stat = ex_pool_alloc_timeout(prv->pool_af, DEFAULT_ALLOC_TIMEOUT_MS);

    if(af_stat)
    {
        af_stat->cfg = *vpipe->h3a_af.data;
        cl_fr_entry->data = af_stat;
        cl_fr_entry->com.fmt = CAMERA_FR_ENTRY_FORMAT__OSAL;
    } else
    {
        mmsdbg(DL_ERROR, "AF FIFO Timeout");
        cl_fr_entry->data = NULL;
        cl_fr_entry->com.fmt = CAMERA_FR_ENTRY_FORMAT__UNUSED;
    }

    return ret;
}

void inc_cam_frame_start( void* p_prv,
                             uint32 sourceInstance,
                             uint32 seqNo,
                             osal_timeval ts,
                             void *userData)
{
    cam_fd_entry_t          *ent = userData;
    inc_t *inc = ent->inc_camera_prv; //p_prv;
    inc_camera_priv_data_t *prv = inc->prv.ptr;
    cam_algo_state_t        *algo_state;

    algo_state = CAMERA_FR_GET_ENTRY(ent->fd, prv->fr_offset_algo_state)->data;
/*
    if (algo_state->cam.light_ctrl.num_light_entries)
    {
        virt_cm_lights_exec_seq (prv->virt_cm_hndl, &algo_state->cam.light_ctrl);
    }
*/
    ent->frame_start_ts = ts;

     virt_cm_sensor_exp_gain(prv->virt_cm_hndl, algo_state->cam.sensor.exposure, algo_state->cam.sensor.again);

    osal_mutex_lock_timeout(prv->lens_movement.lens_lock, DEFAULT_ALLOC_TIMEOUT_MS);

    osal_mutex_unlock(prv->lens_movement.lens_lock);
}

void inc_cam_frame_line_reached( void* p_prv,
                                     uint32 sourceInstance,
                                     uint32 seqNo,
                                     osal_timeval ts,
                                     void *userData)
{
    cam_fd_entry_t          *ent = userData;
    inc_t *inc = ent->inc_camera_prv; //p_prv;
    inc_camera_priv_data_t *prv = inc->prv.ptr;

    osal_mutex_lock_timeout(prv->lens_movement.lens_lock, DEFAULT_ALLOC_TIMEOUT_MS);
    osal_mutex_unlock(prv->lens_movement.lens_lock);

}


void inc_cam_frame_end(  void* p_prv,
                            uint32 sourceInstance,
                            uint32 seqNo,
                            osal_timeval ts,
                            void *userData)
{
    cam_fd_entry_t          *ent = userData;
    inc_t *inc = ent->inc_camera_prv; //p_prv;
    inc_camera_priv_data_t *prv = inc->prv.ptr;

    if (!ent)
    {
        mmsdbg(DL_ERROR, "Invalid FR");
        return;
    }

    generate_events(
            prv,
            ent,
            CL_FLAG_FORCE_POST_CALC_EVENT,
            geg_camera_event_mk(prv->camera_id, CAM_EVT_FR_ISP_DONE)
        );

    osal_mutex_lock_timeout(prv->lens_movement.lens_lock, DEFAULT_ALLOC_TIMEOUT_MS);
    osal_mutex_unlock(prv->lens_movement.lens_lock);
}



void inc_cam_ipipe_buff_locked(void* p_prv, void *userData,
                                  unsigned int sourceInstance,
                                  void*         buffZsl,
                                  osal_timeval  ts,
                                  unsigned int seqNo)
{
    cam_capture_request_t   req;
    cam_fd_entry_t          *ent;
    inc_t                   *inc; //p_prv;
    inc_camera_priv_data_t  *prv;

    cam_fr_buff_ctrl_list_t *cl_req;
    cam_cl_algo_ctrl_list_t *list_ent;
    cam_cl_algo_ctrl_t      *algo_ctrl;

    req.buff_hndl = buffZsl;
//    req.seq_no    = seqNo;
//    req.source    = sourceInstance;
//    req.ts        = ts;
    req.flags     = 0;

    if (1 /* TODO: */ || !userData) {
        // Regular zsl capture
        guzzi_event_send(
                guzzi_event_global(),
                geg_camera_event_mk(sourceInstance, CAM_EVT_BUFF_LOCKED),
                0,
                &req
            );
        return;
    }

    ent = userData;
    inc = ent->inc_camera_prv;
    prv = inc->prv.ptr;

    // Flash capture
    cl_req = CAMERA_FR_GET_ENTRY(ent->fd, prv->fr_cl_img_offset)->data;

    list_for_each_entry(list_ent, &cl_req->list, link)
    {
        algo_ctrl = &list_ent->ctrl;
        mmsdbg (DL_MESSAGE, "Processing - algo %d Mode %d subMode %d buf_type %d\n",
                algo_ctrl->img_Algo,
                algo_ctrl->nMode,
                algo_ctrl->nSub_mode,
                algo_ctrl->buff_type);

        if (CL_ALGO_ISP == algo_ctrl->img_Algo) {
            guzzi_event_send(
                    prv->evt_hdl,
                    geg_camera_event_mk(prv->camera_id, CAM_EVT_BUFF_LOCKED),
                    algo_ctrl->id,
                    &req
                );
            return;
        }
    }

    mmsdbg (DL_ERROR, "Wrong zsl / flash sequence");
}

void inc_cam_stats_ready(void* p_prv, unsigned int seqNo, void* p_cfg_prv)
{
    cam_fd_entry_t          *ent = p_cfg_prv;
    inc_t                   *inc = ent->inc_camera_prv; //p_prv;
    inc_camera_priv_data_t  *prv = inc->prv.ptr;
    camera_fr_entry_t       *cl_fr_entry;
    hat_h3a_aewb_stat_t     *aewb_stat;
    hat_h3a_af_stat_t       *af_stat;

    if ((inc == NULL) ||(ent == NULL) || (ent->fd == NULL))
    {
        mmsdbg(DL_ERROR, "Ipipe AWB stats internal error");
        return;
    }

    if(ent->flags & INC_CAM_CAPTURE_FLAG) {
        ;
    } else
    {
        // Convert stats from movidius to Guzzi format
        cl_fr_entry = CAMERA_FR_GET_ENTRY(ent->fd, prv->fr_offset_aewb_stats);
        aewb_stat = cl_fr_entry->data;

        // Convert stats from movidius to Guzzi format
        cl_fr_entry = CAMERA_FR_GET_ENTRY(ent->fd, prv->fr_offset_af_stats);
        af_stat   = cl_fr_entry->data;

        if (aewb_stat) {
            mmsdbg (DL_MESSAGE, "AEWB Stats: (%d) received", seqNo);

            aewb_stat->ts.frame_number = seqNo;
            aewb_stat->ts.time = 0; /* TODO: get from lrt event? */ //prv->cur_processing_frame.start_ts;
            vpipe_convert_aewb_stats (prv->vpipe_hndl,
                                      ent->hw_config,
                                      aewb_stat,
                                      prv->sensor_read_row_time
                                      );
        } else
            mmsdbg (DL_ERROR, "AEWB Stats - internal error ent %p cl_fr_ent %p ", ent, cl_fr_entry );

        if (af_stat)
        {
            mmsdbg (DL_MESSAGE, "AF Stats: (%d) received", seqNo);

            af_stat->seq_cnt = prv->lens_movement.seq_cnt;
            af_stat->ts.frame_number = seqNo;
            af_stat->ts.time = ent->frame_start_ts; /* TODO: get from lrt event? */ //prv->cur_processing_frame.start_ts;
            af_stat->exposure = 0; /* TODO: get from fd? */ //prv->cur_processing_frame.exposure;

            vpipe_convert_af_stats (prv->vpipe_hndl,
                                    ent->hw_config,
                                    af_stat,
                                    prv->sensor_read_row_time,
                                    &prv->lens_movement
                                    );
        } else
            mmsdbg (DL_ERROR, "AF Stats - internal error");
    }
    inc_camera_process_finish(inc, ent);
    if (ent->hw_config)
        osal_free(ent->hw_config);
    osal_free(ent);
}

void inc_cam_capture_ready(void* p_prv, unsigned int seqNo, void* p_cfg_prv)
{
    cam_fd_entry_t *ent = p_cfg_prv;

    if ((p_prv == NULL) ||(ent == NULL) || (ent->fd == NULL))
    {
        mmsdbg(DL_ERROR, "Ipipe stats internal error");
        return;
    }

    if(ent->flags & INC_CAM_CAPTURE_FLAG) {
        mmsdbg (DL_MESSAGE, "End ZSL capture");
        inc_camera_process_finish(p_prv, ent);

        if (ent->hw_config)
            osal_free(ent->hw_config);
        osal_free(ent);
    }
}


void inc_cam_terminate_fr(void *p_prv, uint32_t sourceInstance, void *userData)
{
    cam_fd_entry_t          *ent;
    inc_t *inc;
    inc_camera_priv_data_t *prv;
    cam_algo_state_t *algo_state;

    ent = userData;
    if ((ent == NULL) || (ent->fd == NULL))
    {
        mmsdbg(DL_ERROR, "CamDrv internal error");
        return;
    }
    inc = ent->inc_camera_prv;
    prv = inc->prv.ptr;
    algo_state = CAMERA_FR_GET_ENTRY(ent->fd, prv->fr_offset_algo_state)->data;

    algo_state->fr_status = 1; // mark error
    inc_camera_process_finish(inc, ent);
    if (ent->hw_config)
        osal_free(ent->hw_config);
    osal_free(ent);
}

/* ========================================================================== */
/**
* inc_camera_start()
*/
/* ========================================================================== */
static int inc_camera_start(inc_t *inc, void *params)
{
    inc_camera_priv_data_t   *pCompPrv;
    const hat_sensor_mode_t *sen_mode;
    cam_cfg_t *cfg;
    uint32  idx;
    int     ret;
    hat_cm_component_ids_t component_ids;
    dtpdb_static_common_t  dtp_static_comm;
    dtpdb_static_private_t dtp_static_prv;

    mmsdbg(DL_FUNC, "Enter");

    pCompPrv = inc->prv.ptr;
    cfg = INC_RES_GET_PTR(params, pCompPrv->res_offset_cfg);

    ret = _camera_config_internal(pCompPrv, cfg);

    virt_cm_get_mode_features(pCompPrv->virt_cm_hndl, &pCompPrv->sen_features);

//    virt_cm_set_operating_mode(pCompPrv->virt_cm_hndl, &pCompPrv->sen_features, pCompPrv->sen_features.sen_mode_idx);

    virt_cm_get_all_camera_comp_ids(pCompPrv->virt_cm_hndl, &component_ids);
    guzzi_event_send(
            pCompPrv->evt_hdl,
            geg_camera_event_mk(pCompPrv->camera_id, CAM_EVT_DETECTED_CAMERA),
            0,
            &component_ids
        );

    idx = pCompPrv->sen_features.sen_mode_idx;
    sen_mode = &pCompPrv->sen_features.sen->modes.list[idx];
    pCompPrv->in_size.w = sen_mode->active.w;
    pCompPrv->in_size.h = sen_mode->active.h;

    osal_memset(&dtp_static_comm, 0x00, sizeof(dtp_static_comm));
    osal_memset(&dtp_static_prv, 0x00, sizeof(dtp_static_prv));
    dtp_static_comm.camera_id = component_ids.cm_idx;

    {
        hat_rect_t fov_dummy;

        // crop parameters all have to be 0 - latest ma2x5x  ISP PC Simulator
        osal_memset(&fov_dummy, 0x00, sizeof(fov_dummy));
        vpipe_ctrl_start(
                            pCompPrv->vpipe_hndl,
                            pCompPrv->in_size,
                            /*sen_mode->field_of_view,*/
                            fov_dummy,
                            sen_mode->format,
                            &dtp_static_comm,
                            &dtp_static_prv
                            );
    }

    guzzi_event_send(
            pCompPrv->evt_hdl,
            geg_camera_event_mk(pCompPrv->camera_id, CAM_EVT_LRT_STARTED),
            0,
            NULL
        );

    mmsdbg(DL_FUNC, "Exit Ok");

    return 0;
}

/* ========================================================================== */
/**
* inc_camera_stop()
*/
/* ========================================================================== */
static void inc_camera_stop(inc_t *inc)
{
    inc_camera_priv_data_t   *pCompPrv;
    pCompPrv = inc->prv.ptr;
    mmsdbg(DL_FUNC, "Enter");

    virt_cm_stop(pCompPrv->virt_cm_hndl);
    vpipe_ctrl_stop(pCompPrv->vpipe_hndl);
    pCompPrv->ipipe_running = 0;

    mmsdbg(DL_FUNC, "Exit Ok");
}

/* ========================================================================== */
/**
* inc_camera_stop()
*/
/* ========================================================================== */
static void inc_camera_flush(inc_t *inc)
{
    inc_camera_priv_data_t   *prv;
    cam_fd_entry_t *ent;
    cam_algo_state_t        *algo_state;
    int wait_empty;

    prv = inc->prv.ptr;
    mmsdbg(DL_FUNC, "Enter");

    osal_sem_wait_timeout(prv->fd_list_lock, DEFAULT_ALLOC_TIMEOUT_MS);

    while(!list_empty(&prv->fd_received_list))
    {
        ent = (cam_fd_entry_t*)prv->fd_received_list.next;
        algo_state = CAMERA_FR_GET_ENTRY(ent->fd, prv->fr_offset_algo_state)->data;
        list_del(prv->fd_received_list.next); // delete from in_process list
        inc->callback(inc, inc->client_prv, 0, 0, ent->fd);

        mmsdbg (DL_WARNING, "CLEAR Received FRs ent: %p fd: %p hw_cfg: %p\n", ent, ent->fd, ent->hw_config);

        if (ent->hw_config)
            osal_free(ent->hw_config);
        osal_free(ent);
    }

    if ((list_empty(&prv->fd_pending_list)) && (list_empty(&prv->fd_in_process_list))) {
        wait_empty = 0;
    } else {
        wait_empty = 1;
        osal_sem_init(prv->fd_flush_lock, 0);
    }

    osal_sem_post(prv->fd_list_lock);

    if (wait_empty) {
        int err = osal_sem_wait_timeout(prv->fd_flush_lock, 500);
        if (err)
        {
            mmsdbg (DL_ERROR, "Sem wait err: %d time: %d ", 500 ); // DEFAULT_ALLOC_TIMEOUT_MS*1/3);
        }
    }

    list_for_each_entry(ent, &prv->fd_pending_list, node)
    {
//        ent = (cam_fd_entry_t*)prv->fd_pending_list.next;
        mmsdbg (DL_ERROR, "Pending FRs ent: %p fd: %p hw_cfg: %p\n", ent, ent->fd, ent->hw_config);
    }

//    list_for_each_entry(ent, &prv->fd_in_process_list, node)
    while(!list_empty(&prv->fd_in_process_list))
    {
        ent = (cam_fd_entry_t*)prv->fd_in_process_list.next;
        mmsdbg (DL_ERROR, "In process FRs ent: %p fd: %p hw_cfg: %p\n", ent, ent->fd, ent->hw_config);

        list_del(prv->fd_in_process_list.next); // delete from in_process list
        inc->callback(inc, inc->client_prv, 0, 0, ent->fd);

        mmsdbg (DL_WARNING, "CLEAR Received FRs ent: %p fd: %p hw_cfg: %p\n", ent, ent->fd, ent->hw_config);

        if (ent->hw_config)
            osal_free(ent->hw_config);
        osal_free(ent);
    }

    osal_sem_post(prv->fd_flush_lock);

    mmsdbg(DL_FUNC, "Exit Ok");
}

/* ========================================================================== */
/**
* inc_camera_config()
*/
/* ========================================================================== */
static int inc_camera_config(inc_t *inc, void *data)
{
    inc_camera_priv_data_t          *pCompPrv;
    camera_fr_t                     *pFR = data;
    cam_cfg_t                       *p_cfg;
    int ret;

    mmsdbg(DL_FUNC, "Enter");

    PROFILE_ADD(PROFILE_ID_SYS_CAM_PROCESS, 0xBEBE, (uint32)data);

    pCompPrv = inc->prv.ptr;
    p_cfg = CAMERA_FR_GET_ENTRY(pFR, pCompPrv->fr_offset_cfg)->data;

    ret = _camera_config_internal(pCompPrv, p_cfg);

    PROFILE_ADD(PROFILE_ID_SYS_CAM_PROCESS, 0xEDED, (uint32)data);

    mmsdbg(DL_FUNC, "Exit Ok");

    return ret;
}

static int inc_cam_is_capture_req(inc_camera_priv_data_t  *prv, cam_fd_entry_t *ent)
{
    cam_fr_buff_ctrl_list_t *cl_req;
    cam_cl_algo_ctrl_list_t *list_ent;
    cam_cl_algo_ctrl_t      *algo_ctrl;

    cl_req = CAMERA_FR_GET_ENTRY(ent->fd, prv->fr_cl_img_offset)->data;

    if (list_empty(&cl_req->list))
    {
        return 0;
    } else
    {
        list_for_each_entry(list_ent, &cl_req->list, link)
        {
            algo_ctrl = &list_ent->ctrl;
            if (CL_ALGO_ZSL_QUEUE == algo_ctrl->img_Algo)
            {
                return 1;
            }
        }
    }
    return 0;
}

static void inc_cam_cfg_vpipe(inc_camera_priv_data_t  *prv, void *data)
{
    vpipe_ctrl_settings_t   *vpipe;
    cam_cfg_t               *p_cfg;
    cam_fr_buff_ctrl_list_t *cl_req;
    cam_cl_algo_ctrl_list_t *list_ent;
    cam_cl_algo_ctrl_t      *algo_ctrl;
    cam_algo_state_t        *algo_state;
    cam_fd_entry_t *ent = data;

    vpipe  = CAMERA_FR_GET_ENTRY(ent->fd, prv->fr_offset_vpipe)->data;
    cl_req = CAMERA_FR_GET_ENTRY(ent->fd, prv->fr_cl_img_offset)->data;
    p_cfg  = CAMERA_FR_GET_ENTRY(ent->fd, prv->fr_offset_cfg)->data;
    algo_state = CAMERA_FR_GET_ENTRY(ent->fd, prv->fr_offset_algo_state)->data;
    prv->config_fr_cnt++;

    prepare_stats_buffs(prv, ent->fd);

    if (list_empty(&cl_req->list))
    {
        cam_isp_params_t isp_params;
        isp_params.en_flags  = DEF_PRV_ISP_ENABLE_FLAGS;
        isp_params.flags     = DEF_PRV_ISP_GROUP_FLAGS;
        isp_params.out_flags = DEF_PRV_PIPE_CTRL_FLAGS;

        mmsdbg (DL_ERROR, "Missing algo control List");

        ent->hw_config = vpipe_ctrl_config(prv->vpipe_hndl,
                                           p_cfg->cam_gzz_cfg.frame_number.val,
                                           prv->config_fr_cnt, // TODO cl_req->seq_num,
                                           vpipe,
                                           &algo_state->sg.ae_distr.post_gain,
                                           &isp_params,
                                           ent);

    } else
    {
        generate_events(
                prv,
                ent,
                CL_FLAG_FORCE_PRE_CALC_EVENT,
                geg_camera_event_mk(prv->camera_id, CAM_EVT_FR_ISP_REACHED)
            );

        list_for_each_entry(list_ent, &cl_req->list, link)
        {
            algo_ctrl = &list_ent->ctrl;
            mmsdbg (DL_MESSAGE, "Processing - algo %d Mode %d subMode %d buf_type %d\n",
                    algo_ctrl->img_Algo,
                    algo_ctrl->nMode,
                    algo_ctrl->nSub_mode,
                    algo_ctrl->buff_type);

            switch (algo_ctrl->img_Algo)
            {
            case CL_ALGO_ZSL_QUEUE:
                ent->hw_config = vpipe_ctrl_config_capture(prv->vpipe_hndl,
                                                           p_cfg->cam_gzz_cfg.frame_number.val,
                                                           prv->config_fr_cnt, // TODO cl_req->seq_num,
                                                           vpipe,
                                                           &algo_state->sg.ae_distr.post_gain,
                                                           &algo_ctrl->alg_specific.capt_request,
                                                           ent);
                        break;
            case CL_ALGO_ISP:
                ent->hw_config = vpipe_ctrl_config(prv->vpipe_hndl,
                                                   p_cfg->cam_gzz_cfg.frame_number.val,
                                                   prv->config_fr_cnt, // TODO cl_req->seq_num,
                                                   vpipe,
                                                   &algo_state->sg.ae_distr.post_gain,
                                                   &algo_ctrl->alg_specific.isp,
                                                   ent);

                break;
            default:
                mmsdbg (DL_ERROR, "Unknown algo %d Mode %d subMode %d buf_type %d\n",
                        algo_ctrl->img_Algo,
                        algo_ctrl->nMode,
                        algo_ctrl->nSub_mode,
                        algo_ctrl->buff_type);
                break;
            }
        }
    }
}

/* ========================================================================== */
/**
* inc_camera_process()
*/
/* ========================================================================== */
static void inc_camera_process(inc_t *inc, void *data)
{
    inc_camera_priv_data_t  *pCompPrv;
    pCompPrv = inc->prv.ptr;
    cam_fd_entry_t *ent;
    cam_cfg_t               *p_cfg;
    cam_algo_state_t        *algo_state;
    int capture_flag = 0;


    mmsdbg(DL_FUNC, "Enter");
    PROFILE_ADD(PROFILE_ID_SYS_CAM_PROCESS, 0xBBBB, (uint32)data);

    algo_state = CAMERA_FR_GET_ENTRY(data, pCompPrv->fr_offset_algo_state)->data;

    if (algo_state->fr_status) {
        mmsdbg(DL_ERROR, "CamDrv skip error FR %p", data);
        inc->callback(inc, inc->client_prv, 0, 0, data);
        goto exit1;
    }

    ent = pool_alloc_timeout(pCompPrv->pool_cam_fd, DEFAULT_ALLOC_TIMEOUT_MS);

    if (NULL == ent) {
        mmsdbg(DL_ERROR, "Can't alloc cam_entry");
        inc->callback(inc, inc->client_prv, 0, 0, data);
        return;
    }

    ent->fd = data;
    ent->hw_config = NULL;
    ent->inc_camera_prv = inc;
    ent->flags = INC_CAM_FLAGS_CLEAR;

    p_cfg  = CAMERA_FR_GET_ENTRY(ent->fd, pCompPrv->fr_offset_cfg)->data;
    if (IS_CAPTURE_MODE(p_cfg->cam_gzz_cfg.cam_mode.val)) {
        capture_flag = inc_cam_is_capture_req(pCompPrv, ent);
        ent->flags = INC_CAM_CAPTURE_FLAG;
    }

    osal_sem_wait_timeout(pCompPrv->fd_list_lock, DEFAULT_ALLOC_TIMEOUT_MS);
    if (capture_flag)
    {
        list_add(&ent->node, &pCompPrv->fd_in_process_list);
        inc_cam_cfg_vpipe(pCompPrv, ent);
        osal_sem_post(pCompPrv->fd_list_lock);
    } else if (list_empty(&pCompPrv->fd_pending_list) && list_empty(&pCompPrv->fd_received_list))
    {
        list_add(&ent->node, &pCompPrv->fd_pending_list);

        inc_cam_cfg_vpipe(pCompPrv, ent);
        osal_sem_post(pCompPrv->fd_list_lock);


        if (pCompPrv->sen_started == 0)
        {
            virt_cm_set_operating_mode(pCompPrv->virt_cm_hndl, &pCompPrv->sen_features, pCompPrv->sen_features.sen_mode_idx);
            pCompPrv->sen_started = 1;
            {
                cam_algo_state_t        *algo_state;

                algo_state = CAMERA_FR_GET_ENTRY(ent->fd, pCompPrv->fr_offset_algo_state)->data;
                virt_cm_sensor_exp_gain(pCompPrv->virt_cm_hndl, algo_state->cam.sensor.exposure, algo_state->cam.sensor.again);
            }

            virt_cm_start(pCompPrv->virt_cm_hndl);
            //start streaming
            virt_cm_set_stream_mode(pCompPrv->virt_cm_hndl, 1);
        }

    } else {
        list_add(&ent->node, &pCompPrv->fd_received_list);
        osal_sem_post(pCompPrv->fd_list_lock);
    }
exit1:
    PROFILE_ADD(PROFILE_ID_SYS_CAM_PROCESS, 0xEEEE, (uint32)data);
    mmsdbg(DL_FUNC, "Exit Ok");
}

/* ========================================================================== */
/**
* inc_cam_ipipe_cfg_ready()
*/
/* ========================================================================== */
void inc_cam_ipipe_cfg_ready(void* p_prv, unsigned int seqNo, void* p_cfg_prv)
{
    cam_fd_entry_t *ent = p_cfg_prv;
    inc_t *inc = ent->inc_camera_prv; //p_prv;
    inc_camera_priv_data_t *prv = inc->prv.ptr;
    vpipe_ctrl_settings_t   *vpipe;
//    hat_light_ctrl_list_t   *light_ctrl;

    /*
    light_ctrl = CAMERA_FR_GET_ENTRY(ent->fd, prv->fr_offset_lights)->data;

    if ((light_ctrl) && (!list_empty(&light_ctrl->list)))
    {  // we have to lock list - it is executed in another thread
        osal_mem_lock(light_ctrl); // it will be unlocked after light list execution
        virt_cm_lights_exec_seq (prv->virt_cm_hndl, light_ctrl);
    }
*/
    osal_sem_wait_timeout(prv->fd_list_lock, DEFAULT_ALLOC_TIMEOUT_MS);

    list_move(&ent->node, &prv->fd_in_process_list);

    if (list_empty(&prv->fd_received_list))
    {
        osal_sem_post(prv->fd_list_lock);
    } else {
        ent = (cam_fd_entry_t*)prv->fd_received_list.prev;
        list_move(&ent->node, &prv->fd_pending_list);

        vpipe = CAMERA_FR_GET_ENTRY(ent->fd, prv->fr_offset_vpipe)->data;

        inc_cam_cfg_vpipe(prv, ent);

        osal_sem_post(prv->fd_list_lock);
    }
}

static int inc_camera_process_finish(inc_t *inc,cam_fd_entry_t *ent)
{
    int                     ret = 0;
    inc_camera_priv_data_t *prv = inc->prv.ptr;

    virt_cm_dyn_props_t             *cm_dyn_props;
    camera_fr_entry_t               *fr_entry;


    mmsdbg(DL_FUNC, "Enter");

    cm_dyn_props = pool_alloc_timeout(prv->pool_cm_dyn_data, DEFAULT_ALLOC_TIMEOUT_MS);
    virt_cm_get_dynamic_properties(prv->virt_cm_hndl, cm_dyn_props);

    fr_entry = CAMERA_FR_GET_ENTRY(ent->fd, prv->fr_offset_cm_dyn_data);
    fr_entry->data = cm_dyn_props;
    fr_entry->com.fmt = CAMERA_FR_ENTRY_FORMAT__OSAL;


    inc->callback(inc, inc->client_prv, 0, 0, ent->fd);

    osal_sem_wait_timeout(prv->fd_list_lock, DEFAULT_ALLOC_TIMEOUT_MS);
    list_del(&ent->node); // delete from in_process list
    if (list_empty(&prv->fd_in_process_list)&&list_empty(&prv->fd_pending_list))
    {
        osal_sem_post(prv->fd_flush_lock);
    }
    osal_sem_post(prv->fd_list_lock);

    mmsdbg(DL_FUNC, "Exit Ok");

    return (ret);
}
/* ========================================================================== */
/**
* inc_camera_destroy()
*/
/* ========================================================================== */
static void inc_camera_destroy(inc_t *inc)
{
    inc_camera_priv_data_t *pCompPrv = inc->prv.ptr;
    mmsdbg(DL_FUNC, "Enter");

    guzzi_event_unreg_recipient(
            pCompPrv->evt_hdl,
            geg_camera_event_mk(pCompPrv->camera_id, CAM_EVT_MOVE_LENS),
            inc_cam_evt_notify,
            pCompPrv
        );
    vpipe_ctrl_destroy(pCompPrv->vpipe_hndl);
    virt_cm_destroy(pCompPrv->virt_cm_hndl);
    ex_pool_destroy(pCompPrv->pool_aewb);
    ex_pool_destroy(pCompPrv->pool_af);
    pool_destroy(pCompPrv->pool_cam_fd);
    pool_destroy(pCompPrv->pool_cm_dyn_data);
    osal_sem_destroy(pCompPrv->fd_list_lock);
    osal_sem_destroy(pCompPrv->fd_flush_lock);
    osal_mutex_destroy(pCompPrv->lens_movement.lens_lock);


    osal_free(pCompPrv);

    mmsdbg(DL_FUNC, "Exit Ok");
}

inc_camera_priv_data_t *pCamPrv;
/* ========================================================================== */
/**
* inc_camera_create()
*/
/* ========================================================================== */
inc_camera_priv_data_t *pCompPrv_global;

int inc_camera_create(
        inc_t *inc,
        void *params,
        void *app_res
    )
{
    inc_camera_priv_data_t *pCompPrv;
    INC_CAMERA_CPARAMS_T *pStaticParams;
    cam_cfg_t *cfg;
    mmsdbg(DL_FUNC, "Enter");

    pStaticParams = params;

    pCompPrv = osal_malloc(sizeof(inc_camera_priv_data_t));
    GOTO_EXIT_IF(NULL == pCompPrv, 1);
    osal_memset (pCompPrv, 0, sizeof(*pCompPrv));
    pCompPrv_global = pCompPrv;

    pCompPrv->fr_offset_cfg         = pStaticParams->fr_offset_cfg;
    pCompPrv->fr_cl_img_offset      = pStaticParams->fr_offset_cl_img;
    pCompPrv->fr_offset_lights      = pStaticParams->fr_offset_lights;
    pCompPrv->fr_offset_aewb_stats  = pStaticParams->fr_offset_aewb_stats;
    pCompPrv->fr_offset_af_stats    = pStaticParams->fr_offset_af_stats;
    pCompPrv->fr_offset_algo_state  = pStaticParams->fr_offset_algo_state;
    pCompPrv->max_fr                = pStaticParams->max_fr;
    pCompPrv->res_offset_cfg        = pStaticParams->res_offset_cfg;
    pCompPrv->res_offset_cametra_id = pStaticParams->res_offset_cametra_id;
    pCompPrv->fr_offset_vpipe       = pStaticParams->fr_offset_vpipe;
    pCompPrv->fr_offset_cm_dyn_data = pStaticParams->fr_offset_cm_dyn_data;

    pCompPrv->pool_aewb = ex_pool_create("CAM AEWB STATS POOL", &aewb_root, pCompPrv->max_fr);
    GOTO_EXIT_IF(NULL == pCompPrv->pool_aewb, 2);

    pCompPrv->pool_af = ex_pool_create("CAM AF STATS POOL", &af_root, pCompPrv->max_fr);
    GOTO_EXIT_IF(NULL == pCompPrv->pool_af, 3);

    pCompPrv->pool_cam_fd = pool_create("CAM FD POOL", sizeof(cam_fd_entry_t), pCompPrv->max_fr);
    GOTO_EXIT_IF(NULL == pCompPrv->pool_cam_fd, 4);

    pCompPrv->pool_cm_dyn_data = pool_create("CAM CM DYN DATA", sizeof(virt_cm_dyn_props_t), pCompPrv->max_fr);
    GOTO_EXIT_IF(NULL == pCompPrv->pool_cm_dyn_data, 5);

    pCompPrv->lens_movement.lens_lock = osal_mutex_create();

    INIT_LIST_HEAD(&pCompPrv->fd_received_list);
    INIT_LIST_HEAD(&pCompPrv->fd_pending_list);
    INIT_LIST_HEAD(&pCompPrv->fd_in_process_list);
    pCompPrv->fd_list_lock = osal_sem_create(1);
    pCompPrv->fd_flush_lock = osal_sem_create(1);

    pCompPrv->sen_started = 0;

    pCompPrv->config_fr_cnt = 0;



    inc->prv.ptr = pCompPrv;

    inc->inc_start = inc_camera_start;
    inc->inc_stop = inc_camera_stop;
    inc->inc_flush = inc_camera_flush;
    inc->inc_config_alter = NULL;
    inc->inc_config = inc_camera_config;
    inc->inc_process = inc_camera_process;
    inc->inc_destroy = inc_camera_destroy;

    cfg = INC_RES_GET_PTR(app_res, pCompPrv->res_offset_cfg);

    pCompPrv->sen_features.sen_mode_idx = -1;
    pCompPrv->camera_id = INC_RES_GET_UINT(app_res, pCompPrv->res_offset_cametra_id);
    vpipe_ctrl_create(&pCompPrv->vpipe_hndl, inc, pCompPrv->camera_id);
    GOTO_EXIT_IF(NULL == pCompPrv->vpipe_hndl, 6);
    virt_cm_create(&pCompPrv->virt_cm_hndl, pCompPrv->camera_id);
    GOTO_EXIT_IF(NULL == pCompPrv->virt_cm_hndl, 6);

    pCompPrv->evt_hdl = guzzi_event_global();
    guzzi_event_reg_recipient(
            pCompPrv->evt_hdl,
            geg_camera_event_mk(pCompPrv->camera_id, CAM_EVT_MOVE_LENS),
            inc_cam_evt_notify,
            pCompPrv
        );

    pCompPrv->cfg_seq_num.gzz            = 0;
    pCompPrv->cfg_seq_num.cam_mode       = 0;
    pCompPrv->cfg_seq_num.cam_resloution = 0;
    pCompPrv->cfg_seq_num.sen_crop       = 0;
    pCompPrv->cfg_seq_num.fr_duration    = 0;
    pCompPrv->cfg_seq_num.fps_range      = 0;
    pCompPrv->cfg_seq_num.aperture       = 0;
    pCompPrv->cfg_seq_num.filter_density = 0;
    pCompPrv->cfg_seq_num.focal_length   = 0;
    pCompPrv->cfg_seq_num.o_stab_mode    = 0;

    mmsdbg(DL_FUNC, "Exit Ok");

    return 0;

EXIT_6:
    pool_destroy(pCompPrv->pool_cm_dyn_data);
EXIT_5:
    pool_destroy(pCompPrv->pool_cam_fd);
EXIT_4:
    ex_pool_destroy(pCompPrv->pool_af);
EXIT_3:
    ex_pool_destroy(pCompPrv->pool_aewb);
EXIT_2:
    osal_free(inc->prv.ptr);
EXIT_1:
    mmsdbg(DL_FUNC, "Exit Err");

    return -1;
}


