/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file sensor_features.h
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#ifndef SENSOR_FEATURES_H_
#define SENSOR_FEATURES_H_

#include <osal/osal_stdtypes.h>

typedef struct {
   uint32 mNominator;
   uint32 mDenominator;
}rational_t;

/**
 *
 * Area of raw data which corresponds to only active pixels;
 * smaller or equal to pixelArraySize.
 *
 * Top left of full pixel array is (0,0)
 */
typedef struct {
    uint32 xmin;
    uint32 ymin;
    uint32 width;
    uint32 height;
}activeArraySize_t;

/**
 * Range of valid sensitivities
 */
typedef struct {
    uint32 min;      // >100
    uint32 max;      // <1600
} sensitivityRange_t;

/**
 *
 * in case of RGB Sensor is not Bayer; output has 3 16-bit values
 *  for each pixel, instead of just 1 16-bit value per pixel
 *
 */
typedef enum {
    RGGB,
    GRBG,
    GBRG,
    BGGR,
    RGB
} colorFilterArrangement_types_t;

/**
 * Arrangement of color filters on sensor;
 * represents the colors in the top-left 2x2 section of the sensor,
 * in reading order
 *
 */
typedef struct {
    colorFilterArrangement_types_t cfa_arrangement;
}colorFilterArrangement_t;

/**
 * Range of valid exposure times [nanoseconds]
 */
typedef struct {
    int64 min;  // Min <= 100e3 (100 us)
    int64 max;  // Max >= 30e9 (30 sec)
}exposureTimeRange_t;

/**
 * Maximum possible frame duration [nanoseconds]
 * (minimum frame rate)
 * Minimum duration is a function of resolution, processing settings.
 *
 * @see android.scaler.availableProcessedMinDurations
 * android.scaler.availableJpegMinDurations
 * android.scaler.availableRawMinDurations
 */
typedef struct {
    int64 duration;  // >= 30e9
}maxFrameDuration_t;

/**
 * The physical dimensions of the full pixel array [millimeters]
 * Needed for FOV calculation for old API
 *
 */
typedef struct {
    float width;  // millimeters
    float height; // millimeters
}physicalSize_t;


/**
 * Dimensions of full pixel array, possibly including black calibration pixels
 *
 * Maximum output resolution for raw format must match this in
 * android.scaler.info.availableSizesPerFormat
 *
 */
typedef struct {
    int32 width;    // [pixels]
    int32 height;   // [pixels]
} pixelArraySize_t;

/**
 * Maximum raw value output by sensor
 *
 * Defines sensor bit depth (10-14 bits is expected
 */
typedef struct {
    int32 max_pix; // > 1024 (10-bit output)
}whiteLevel_t;

/**
 * Gain factor from electrons to raw units when ISO=100
 *
 */
typedef struct {
    rational_t base_gain;
}baseGainFacto_t;

/**
 * A fixed black level offset for each of the Bayer mosaic channel
 */
typedef struct {
    int32 channel[4];
}blackLevelPattern_t;

/**
 * Per-device calibration on top of color space transform 1
 * 3x3 matrix in row-major-order
 */
typedef struct {
    rational_t calib_transform1[3][3];
}calibrationTransform1_t;

/**
 * Per-device calibration on top of color space transform 2
 * 3x3 matrix in row-major-order
 *  */
typedef struct {
    rational_t calib_transform2[3][3];
}calibrationTransform2_t;

/**
 * Linear mapping from XYZ (D50) color space to reference linear sensor
 * color, for first reference illuminant
 *
 *  3x3 matrix in row-major-order
 *
 *  Use as follows:
 *   XYZ = inv(transform) * clip( (raw - black level(raw) ) /
 *         ( white level - max black level) ).
 * * At least in the simple case
 */
typedef struct {
    rational_t color_transform1[3][3];
}colorTransform1_t;


/**
 * Linear mapping from XYZ (D50) color space to reference linear sensor
 * color, for first reference illuminant
 *  3x3 matrix in row-major-order
 */
typedef struct {
    rational_t color_transform2[3][3];
}colorTransform2_t;


/**
 * Used by DNG for better WB adaptation
 *  3x3 matrix in row-major-order
 */
typedef struct {
    rational_t fw_matrix1[3][3];
}forwardMatrix1_t;

/**
 * Used by DNG for better WB adaptation
 *  3x3 matrix in row-major-order
 */
typedef struct {
    rational_t fw_matrix2[3][3];
}forwardMatrix2_t;

/**
 * Maximum sensitivity that is implemented purely through analog gain
 *
 * For android.sensor.sensitivity values less than or equal to this,
 * all applied gain must be analog. For values above this, it can be a
 * mix of analog and digital
 */
typedef struct {
    int32 again;
} maxAnalogSensitivity_t;

/**
 * float constants A, B for the noise variance model
 *
 * Estimation of sensor noise characteristics
 *
 * var(raw pixel value) = electrons * (baseGainFactor * iso/100)^2 +
 *                         A * (baseGainFactor * iso/100)^2 + B
 *
 * A represents sensor read noise before analog amplification;
 * B represents noise from A/D conversion and other circuits after
 * amplification. Both noise sources are assumed to be gaussian,
 * independent, and not to vary across the sensor
 */
typedef struct {
    float A;
    float B;
}noiseModelCoefficients_t;

/**
 * Clockwise angle through which the output image needs to be rotated to be
 * upright on the device screen in its native orientation. Also defines the
 * direction of rolling shutter readout, which is from top to bottom in the
 * sensor's coordinate system
 * [degrees clockwise rotation, only multiples of 90]
 *
 */
typedef struct {
    int32 orientation; // 0,90,180,270
} orientation_t;



typedef enum {
    DAYLIGHT                = 1,
    FLUORESCENT             = 2,
    TUNGSTEN                = 3,  // Incandescent light
    FLASH                   = 4,
    FINE_WEATHER            = 9,
    CLOUDY_WEATHER          = 10,
    SHADE                   = 11,
    DAYLIGHT_FLUORESCENT    = 12, // D 5700 - 7100K
    DAY_WHITE_FLUORESCENT   = 13, // N 4600 - 5400K
    COOL_WHITE_FLUORESCENT  = 14, // W 3900 - 4500K
    WHITE_FLUORESCENT       = 15, // WW 3200 - 3700K
    STANDARD_A              = 17,
    STANDARD_B              = 18,
    STANDARD_C              = 19,
    D55                     = 20,
    D65                     = 21,
    D75                     = 22,
    D50                     = 23,
    ISO_STUDIO_TUNGSTEN     = 24
}illuminant_types_t;

/**
 * Light source used to define transform 1
 */
typedef struct {
    illuminant_types_t illuminant_type;
}referenceIlluminant1;

/**
 * Light source used to define transform 2
 */
typedef struct {
    illuminant_types_t illuminant_type;
}referenceIlluminant2;


typedef struct {
    activeArraySize_t           active;
    sensitivityRange_t          sens_range;
    colorFilterArrangement_t    cfa;
    exposureTimeRange_t         exp_range;
    maxFrameDuration_t          max_fr_duration;
    physicalSize_t              phys_size;
    pixelArraySize_t            resolution;
    whiteLevel_t                white_level;
    baseGainFacto_t             base_gain;
    blackLevelPattern_t         black_pattern;
    calibrationTransform1_t     calib_tr1;
    calibrationTransform2_t     calib_tr2;
    colorTransform1_t           col_tr1;
    colorTransform2_t           col_tr2;
    forwardMatrix1_t            fw_matrix1;
    forwardMatrix2_t            fw_matrix2;
    maxAnalogSensitivity_t      max_again;
    noiseModelCoefficients_t    noise_model;
    orientation_t               orientation;
    referenceIlluminant1        ref_illuminant1;
    referenceIlluminant2        ref_illuminant2;
}sensor_info_t;

#endif /* SENSOR_FEATURES_H_ */
