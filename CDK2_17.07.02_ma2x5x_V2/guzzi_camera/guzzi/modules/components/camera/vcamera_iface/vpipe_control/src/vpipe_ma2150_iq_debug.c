/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file vpipe_control.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <osal/osal_stdlib.h>
#include <osal/osal_string.h>
#include <utils/mms_debug.h>
#include <camera/vcamera_iface/vpipe_control/include/vpipe_control.h>
#include <hal/hat_h3a_aewb.h>
#include <camera/vcamera_iface/vpipe_convert/include/vpipe_conv.h>

#include <osal/ex_pool.h>

// Better to Enable/Disable from Makefile.config for Your project - not from here
#define ENABLE_IQ_DEBUG_DATA

#ifdef ENABLE_IQ_DEBUG_DATA

mmsdbg_define_variable(
        vdl_iq_debug,
        DL_DEFAULT,
        0,
        "iq_debug",
        "IQ DEBUG"
    );
#define MMSDEBUGLEVEL mmsdbg_use_variable(vdl_iq_debug)

#define IQ_DEBUG_TIMEOUT   DEFAULT_ALLOC_TIMEOUT_MS


#define IPIPE_LSC_PADDING 4

#ifndef MAX_LSC_SIZE
#define     MAX_LSC_SIZE             (sizeof(uint16_t) * (64+IPIPE_LSC_PADDING)*64*4)
#endif

#ifndef MAX_GAMMA_SIZE
#define     MAX_GAMMA_SIZE             (sizeof(uint16_t) * 512*4)
#endif

#ifndef MAX_AE_AWB_STATS_SIZE
#define     MAX_AE_AWB_STATS_SIZE      (sizeof(icAeAwbStats))
#endif

#ifndef MAX_AF_STATS_SIZE
#define     MAX_AF_STATS_SIZE          (sizeof(icAfStats))
#endif

#ifndef MAX_LTM_SIZE
#define     MAX_LTM_SIZE               (4 * 16 * 16 * 16)
#endif

typedef struct {
    icIspConfig ispCfg;
    uint8_t lsc[MAX_LSC_SIZE];
    uint8_t Gamma[MAX_GAMMA_SIZE];
    icAeAwbStats ae_wb;
    icAfStats    af;
    uint16_t lut3D[MAX_LTM_SIZE];
} iq_isp_data_t;

typedef struct
{
    iq_isp_data_t   color;
} dbg_iq_t;

typedef struct
{
    uint32_t        frameCount;
    uint32_t        infoDatasSize;
    uint32_t        frameWidth;
    uint32_t        frameHeight;
    uint32_t        x1;
    uint32_t        y1;
    uint32_t        x2;
    uint32_t        y2;
    dbg_iq_t        iq;
} dbg_iq_frame_t;

typedef struct
{
    ex_pool_t *iq_debug_pool;
    osal_sem  *tx_sync;
} iq_debug_ctx_t;


typedef void (*SendOutCbSent)(FrameT *frame, uint32_t outputId, uint32_t frmType);

void sendOutSend(FrameT *frame, uint32_t outputId, uint32_t frmType, SendOutCbSent sendOutCbSent);

#define IQ_DEBUG_LINE_SIZE      1936
#define IQ_DEBUG_HEADER_HEIGHT  2
#define IQ_DEBUG_HEADER_SIZE    (IQ_DEBUG_LINE_SIZE*IQ_DEBUG_HEADER_HEIGHT)
#define IQ_DEBUG_UVC_HEADER     16

#define IQ_DEBUG_BUFFER_LINES  ((sizeof(dbg_iq_frame_t) + (IQ_DEBUG_LINE_SIZE-1))/IQ_DEBUG_LINE_SIZE)
#define IQ_DEBUG_BUFFER_SIZE   (IQ_DEBUG_LINE_SIZE * IQ_DEBUG_BUFFER_LINES)


ex_pool_node_t iq_debug_pool_desc [] = {
     EX_POOL_NODE_DESC(NULL, (IQ_DEBUG_HEADER_SIZE+IQ_DEBUG_UVC_HEADER), FrameT, fbPtr[3]),
     EX_POOL_NODE_DESC(NULL, (IQ_DEBUG_BUFFER_SIZE), FrameT, fbPtr[0]),
     EX_POOL_LIST_END
};

ex_pool_node_t iq_debug_pool_root =
{
     EX_POOL_ROOT(&iq_debug_pool_desc, sizeof (FrameT))
};

static void isp2iq_debug(iq_isp_data_t *iq_debug, icIspConfig *p_cfg)
{
    memcpy(&iq_debug->ispCfg, p_cfg                    , sizeof (iq_debug->ispCfg));
    memcpy(&iq_debug->lsc   , p_cfg->lsc.pLscTable     , sizeof (iq_debug->lsc   ));
    memcpy(&iq_debug->Gamma , p_cfg->gamma.table       , sizeof (iq_debug->Gamma ));
    memcpy(&iq_debug->ae_wb , p_cfg->aeAwbStats        , sizeof (iq_debug->ae_wb ));
    memcpy(&iq_debug->af    , p_cfg->afStats           , sizeof (iq_debug->af    ));
    memcpy(&iq_debug->lut3D , p_cfg->colorCombine.lut3D, sizeof (iq_debug->lut3D ));
}

dbg_iq_frame_t * get_iq_debug_buffer(FrameT *ft)
{
    if (!ft)
        return NULL;
    if (ft->tSize[0] == 0)
    {
        ft->type = FRAME_T_FORMAT_BINARY;
        ft->nPlanes = 2;

        ft->stride[0] = IQ_DEBUG_LINE_SIZE;
        ft->height[0] = IQ_DEBUG_BUFFER_LINES;
        ft->tSize[0]  = IQ_DEBUG_BUFFER_SIZE;

        ft->stride[3] = IQ_DEBUG_LINE_SIZE;
        ft->height[3] = IQ_DEBUG_HEADER_HEIGHT;
        ft->tSize[3]  = IQ_DEBUG_HEADER_SIZE;

        ft->fbPtr[3]  = (char*)ft->fbPtr[3] + IQ_DEBUG_UVC_HEADER;
        ft->fbPtr[0]  = (char*)ft->fbPtr[3] + ft->tSize[3];

    }
    return ((dbg_iq_frame_t *)ft->fbPtr[0]);
}

static void callback_sendOutSend_metadata(FrameT *frame, uint32_t outputId, uint32_t frmType)
{
    iq_debug_ctx_t *ctx = frame->appSpecificData;
    UNUSED(outputId);
    UNUSED(frmType);

    osal_free(frame);
    osal_sem_post(ctx->tx_sync);
}

void iq_debug_send(void *prv, int camera_id, hat_size_t *s, hat_rect_t *c, icIspConfig *p_cfg)
{
    iq_debug_ctx_t *ctx = prv;
    dbg_iq_frame_t *iq_debug;
    FrameT *frame;

    frame = ex_pool_alloc_timeout(ctx->iq_debug_pool, IQ_DEBUG_TIMEOUT);
    if (frame == NULL) return;

    frame->appSpecificData = prv;

    iq_debug = get_iq_debug_buffer(frame);

    iq_debug->frameCount = 0;
    iq_debug->infoDatasSize = sizeof (dbg_iq_frame_t);
    iq_debug->frameWidth    = s->w;
    iq_debug->frameHeight   = s->h;
    iq_debug->x1            = c->x;
    iq_debug->y1            = c->y;
    iq_debug->x2            = c->x + c->w;
    iq_debug->y2            = c->y + c->h;

    isp2iq_debug(&iq_debug->iq.color, p_cfg);

//    callback_sendOutSend_metadata(frame, inc_prv->camera_id, FRAME_DATA_TYPE_IQ_DEBUG_DATA);
    sendOutSend(frame, camera_id, FRAME_DATA_TYPE_IQ_DEBUG_DATA, callback_sendOutSend_metadata);

    osal_sem_wait_timeout(ctx->tx_sync, IQ_DEBUG_TIMEOUT);
}


void* iq_debug_create(void)
{
    iq_debug_ctx_t *ctx;
    ctx = osal_malloc(sizeof(iq_debug_ctx_t));
    if (ctx == NULL) return NULL;

    ctx->iq_debug_pool = ex_pool_create("IQ Debug", &iq_debug_pool_root, 1);
    if (ctx->iq_debug_pool == NULL) goto exit1;

    ctx->tx_sync = osal_sem_create(0);

    if (ctx->tx_sync == NULL) goto exit2;

    return ctx;

exit2:
    ex_pool_destroy(ctx->iq_debug_pool);

exit1:
    osal_free(ctx);
    mmsdbg(DL_ERROR, "Failed to create IQ DEBUG!");
    return NULL;
}

void iq_debug_destroy(void* prv)
{
    iq_debug_ctx_t *ctx;
    ctx = (iq_debug_ctx_t *)prv;
    if (!ctx) return;

    if (ctx->iq_debug_pool) ex_pool_destroy(ctx->iq_debug_pool);
    if (ctx->tx_sync)       osal_sem_destroy(ctx->tx_sync);
    osal_free(ctx);
}

#else
void* iq_debug_create(void)
{
    return NULL;
}
void  iq_debug_send(void *prv, int camera_id, hat_size_t *s, icIspConfig *p_cfg)
{
    UNUSED(prv);
    UNUSED(camera_id);
    UNUSED(s);
    UNUSED(p_cfg);
}
void  iq_debug_destroy(void* prv)
{
    UNUSED(prv);
}

#endif // ENABLE_IQ_DEBUG_DATA
