/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file vpipe_control.h
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#ifndef __VPIPE_CONTROL_H_
#define __VPIPE_CONTROL_H_

#include <dtp/dtp_server_defs.h>

#include <hal/hat_dcsub.h>
#include <hal/hat_dpc.h>
#include <hal/hat_grgb.h>
#include <hal/hat_lsc.h>
#include <hal/hat_wbal.h>
#include <hal/hat_cfai.h>
#include <hal/hat_matrix.h>
#include <hal/hat_gamma.h>
#include <hal/hat_luma_nf.h>
#include <hal/hat_sharpening.h>
#include <hal/hat_chroma_nf.h>
#include <hal/hat_ltm.h>
#include <hal/hat_dog.h>
#include <hal/hat_h3a_aewb.h>
#include <hal/hat_h3a_af.h>
#include <cam_cl_frame_req.h>
#include <camera/vcamera_iface/virt_cm/inc/virt_cm.h>
#include <osal/osal_mutex.h>


typedef void* vpipe_ctrl_hndl_t;

#define MAX_LENS_MOV_ENTRIES  (128)
#define LENS_MOV_MASK  (MAX_LENS_MOV_ENTRIES-1)

typedef struct {
    LENS_MOVE_DESCR_T    descr;
    osal_timeval        start_ts;
    osal_timeval        end_ts;
    float               s_pos;
    float               e_pos;
    float               pos_to_time_move;
    float               pos_to_time_stop;
}lens_mov_entry_t;

typedef struct {
    osal_mutex        *lens_lock;
    uint32            seq_cnt;          // scan iteration - populated by focus algorithm
    uint32            abort;            // abort current scan iteration - populated by focus algorithm
    LENS_MOVE_DESCR_T last_state;       // Lens driver state
    float            last_know_position_st;  // Lens driver position on exposure start
    float            last_know_position_end; // Lens driver position on exposure end
    uint32            head;
    uint32            tail_st;          //tail at exposure start
    uint32            tail_end;         //tail at exposure end
    float             actual_lens_position; // real (current) Lens position
    lens_mov_entry_t  entries[MAX_LENS_MOV_ENTRIES];
}lens_movement_data_t;

typedef struct {
    const hat_dcsub_t          *data;
} vpipe_ctrl_dcsub_t;

typedef struct {
    const hat_dpc_t            *data;
} vpipe_ctrl_dpc_t;

typedef struct {
    const hat_grgb_t           *data;
} vpipe_ctrl_grgb_t;

typedef struct {
    const hat_lsc_t            *data;
} vpipe_ctrl_lsc_t;

typedef struct {
    const hat_wbal_coef_t      *data;
} vpipe_ctrl_wb_t;

typedef struct {
    const hat_cfai_t           *data;
} vpipe_ctrl_cfai_t;

typedef struct {
    const hat_rgb2rgb_t        *data;
} vpipe_ctrl_rgb2rgb_t;

typedef struct {
    const hat_gamma_t          *data;
} vpipe_ctrl_gamma_t;

typedef struct {
    const hat_rgb2yuv_t        *data;
} vpipe_ctrl_rgb2yuv_t;

typedef struct {
    const hat_luma_nf_t        *data;
} vpipe_ctrl_lnf_t;

typedef struct {
    const hat_sharpening_t     *data;
} vpipe_ctrl_sharpening_t;

typedef struct {
    const hat_chroma_nf_t      *data;
} vpipe_ctrl_cnf_t;

typedef struct {
    const hat_ltm_t           *data;
} vpipe_ctrl_ltm_t;

typedef struct {
    const hat_dog_t           *data;
} vpipe_ctrl_dog_t;

typedef struct {
    const hat_h3a_aewb_cfg_t    *data;
} vpipe_ctrl_h3a_aewb_t;

typedef struct {
    const hat_h3a_af_cfg_t     *data;
} vpipe_ctrl_h3a_af_t;

typedef struct {
    vpipe_ctrl_dcsub_t       dcsub;
    vpipe_ctrl_dpc_t         dpc;
    vpipe_ctrl_grgb_t        grgb;
    vpipe_ctrl_lsc_t         lsc;
    vpipe_ctrl_wb_t          wb;
    vpipe_ctrl_cfai_t        cfai;
    vpipe_ctrl_rgb2rgb_t     rgb2rgb;
    vpipe_ctrl_gamma_t       gamma;
    vpipe_ctrl_rgb2yuv_t     rgb2yuv;
    vpipe_ctrl_lnf_t         lnf;
    vpipe_ctrl_sharpening_t  sharpening;
    vpipe_ctrl_cnf_t         cnf;
    vpipe_ctrl_ltm_t         ltm;
    vpipe_ctrl_dog_t         dog;
    vpipe_ctrl_h3a_aewb_t    h3a_aewb;
    vpipe_ctrl_h3a_af_t      h3a_af;

    struct {
       uint32_t exp;
       uint32_t gain;
       uint32_t colour_temp;
    } env;
} vpipe_ctrl_settings_t;

void* vpipe_ctrl_config (vpipe_ctrl_hndl_t hndl,
                       uint32 frame_number,
                       uint32 config_seq_no,
                       vpipe_ctrl_settings_t *vpipe_cfg,
                       hat_dgain_t *post_gain,
                       cam_isp_params_t* isp,
                       void* cfg_private);

void* vpipe_ctrl_config_capture (vpipe_ctrl_hndl_t hndl,
                       uint32 frame_number,
                       uint32 config_seq_no,
                       vpipe_ctrl_settings_t *vpipe_cfg,
                       hat_dgain_t *post_gain,
                       cam_capture_request_t* capt,
                       void* cfg_private);

int vpipe_ctrl_destroy (vpipe_ctrl_hndl_t hndl);
int vpipe_ctrl_create (vpipe_ctrl_hndl_t *hndl, void* private_data, int cam_id);

int vpipe_ctrl_start (vpipe_ctrl_hndl_t hndl,
                            hat_size_t in_size,
                            hat_rect_t crop,
                            hat_pix_fmt_t fmt,
                            void *dtp_static_comm,
                            void *dtp_static_prv);

int vpipe_ctrl_stop (vpipe_ctrl_hndl_t hndl);

#endif /* __VPIPE_CONTROL_H_ */

