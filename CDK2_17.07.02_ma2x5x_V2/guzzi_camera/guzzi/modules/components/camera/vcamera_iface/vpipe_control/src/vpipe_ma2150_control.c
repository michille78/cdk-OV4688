/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file vpipe_control.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */
#if defined(__sparc)
#include <bsp.h>
#endif

#include <osal/osal_stdlib.h>
#include <osal/osal_string.h>
#include <utils/mms_debug.h>
#include <camera/vcamera_iface/vpipe_control/include/vpipe_control.h>

#include <hal/hat_h3a_aewb.h>
#include <camera/vcamera_iface/vpipe_convert/include/vpipe_conv.h>
#include "cam_cl_frame_req.h"

#include <osal/ex_pool.h>

mmsdbg_define_variable(
        vdl_vpipe_params,
        DL_DEFAULT,
        0,
        "vpipe.params",
        "VPIPE PARAMS"
    );
#define MMSDEBUGLEVEL mmsdbg_use_variable(vdl_vpipe_params)

#define BLACK_LEVEL 16

void los_start(void *);
void los_configIsp(void *iconf, int ispIdx);
void los_stop(void);
void los_ipipe_TriggerCapture(void* buff, void *iconf, uint32_t srcIdx);

void* iq_debug_create(void);
void iq_debug_send(void *prv, int camera_id, hat_size_t *s, hat_rect_t *c, icIspConfig *p_cfg);
void  iq_debug_destroy(void* prv);

#ifdef GZZ_PIPE_FR_NUM_MAX
#define IPIPE_NUM_CONFIGS  (GZZ_PIPE_FR_NUM_MAX+2)
#else
#define IPIPE_NUM_CONFIGS 5
#endif

ex_pool_node_t aewb_ipipe_cfg_desc [] = {
     EX_POOL_NODE_DESC(NULL, (sizeof(uint16_t) * (64+IPIPE_LSC_PADDING)*64*4), icIspConfig, lsc.pLscTable),
     EX_POOL_NODE_DESC(NULL, (sizeof(uint16_t) * 512*4), icIspConfig, gamma.table),
     EX_POOL_NODE_DESC(NULL, (sizeof(icAeAwbStats)), icIspConfig, aeAwbStats),
     EX_POOL_NODE_DESC(NULL, (sizeof(icAfStats)),    icIspConfig, afStats),
     EX_POOL_NODE_DESC(NULL, (sizeof(uint16_t) * 4 * 16 * 16 * 16), icIspConfig, colorCombine.lut3D),
     EX_POOL_LIST_END
};

ex_pool_node_t aewb_ipipe_cfg_root =
{
// DD: For efficiency, we'd like the icIspConfig struct to be in CMX.  On
// the other hand, the Stats structs are large and should be in DDR.  How
// can we control the location of these structures?
     EX_POOL_ROOT(&aewb_ipipe_cfg_desc, sizeof (icIspConfig))
};

/*
 * ipipe_ctx_t
 * instance data
 */
typedef struct {
    const char *name;
    void*       private_data;

    hat_size_t in_size;
    hat_rect_t crop;

    hat_pix_fmt_t format;

    ex_pool_t   *pool_ipipe_cfg;

    ipipe_conv_hndl_t conv_hndl;

    int lrt_plugin_id;

    void *iq_debug;

} vpipe_ctx_t;

static int __attribute__((optimize("-O3"))) h3a_stats_ipipe_convert(hat_pix_fmt_t fmt,
                            hat_h3a_aewb_stat_t *p_h3a_aeawb_stats,
                            icIspConfig *cfg,
                            uint32_t row_time)
{
    uint32_t        pw;
    uint32_t        ph;
    uint32_t        nx;
    uint32_t        ny;
    register AeAwbPatchStats *_pax;
    register hat_h3a_aewb_entry_t *p_pax;

    uint32_t        i, n, pix_in_pax;
    float           divd;
    uint32_t         rel_ts;
    float           sum[4];
    float           usat[4];

    pw = cfg->aeAwbConfig.patchWidth;
    ph = cfg->aeAwbConfig.patchHeight;
    nx = cfg->aeAwbConfig.nPatchesX;
    ny = cfg->aeAwbConfig.nPatchesY;

    _pax = cfg->aeAwbStats;
    p_pax = p_h3a_aeawb_stats->paxels;

    pix_in_pax = pw * ph;
    pix_in_pax >>= 2; //Downshift to get per channel number of pixels.

    if ( !nx || !ny) {
       mmsdbg (DL_ERROR, "number of paxels %dx%d\n", nx , ny);
       return -1;
    }

    rtems_cache_invalidate_multiple_data_lines(_pax, (sizeof(*_pax) * nx * ny));

    //Stats data is taken from the 8 MSB for a specific channel.
    divd = (float)(pix_in_pax * ((1<<8)-1));

    rel_ts = row_time*cfg->aeAwbConfig.firstPatchY+row_time*cfg->aeAwbConfig.patchHeight/2;

    for (i = 0; i < ny; i++)
    {
        for (n = 0; n < nx; n++)
        {
            p_pax->time_st= rel_ts;
            sum[0] = (float) ((_pax->accum[0] + _pax->altAccum[0])) / divd;
            sum[1] = (float) ((_pax->accum[1] + _pax->altAccum[1])) / divd;
            sum[2] = (float) ((_pax->accum[2] + _pax->altAccum[2])) / divd;
            sum[3] = (float) ((_pax->accum[3] + _pax->altAccum[3])) / divd;

            usat[0] = 1 - ((float)_pax->count[0] / (float)(pix_in_pax));
            usat[1] = 1 - ((float)_pax->count[1] / (float)(pix_in_pax));
            usat[2] = 1 - ((float)_pax->count[2] / (float)(pix_in_pax));
            usat[3] = 1 - ((float)_pax->count[3] / (float)(pix_in_pax));

            switch (fmt.fmt.order.all) {
            case HAT_PORDBYR_B_Gb_Gr_R:
                p_pax->b = sum[0];
                p_pax->gb = sum[1];
                p_pax->gr = sum[2];
                p_pax->r = sum[3];

                p_pax->b_usat = usat[0];
                p_pax->gb_usat = usat[1];
                p_pax->gr_usat = usat[2];
                p_pax->r_usat = usat[3];
                break;
            case HAT_PORDBYR_R_Gr_Gb_B:
            default:
                p_pax->r = sum[0];
                p_pax->gr = sum[1];
                p_pax->gb = sum[2];
                p_pax->b = sum[3];

                p_pax->r_usat = usat[0];
                p_pax->gr_usat = usat[1];
                p_pax->gb_usat = usat[2];
                p_pax->b_usat = usat[3];
                break;
            case HAT_PORDBYR_Gb_B_R_Gr:
                p_pax->gb = sum[0];
                p_pax->b = sum[1];
                p_pax->r = sum[2];
                p_pax->gr = sum[3];

                p_pax->gb_usat = usat[0];
                p_pax->b_usat = usat[1];
                p_pax->r_usat = usat[2];
                p_pax->gr_usat = usat[3];
                break;
            case HAT_PORDBYR_Gr_R_B_Gb:
                p_pax->gr = sum[0];
                p_pax->r = sum[1];
                p_pax->b = sum[2];
                p_pax->gb = sum[3];

                p_pax->gr_usat = usat[0];
                p_pax->r_usat = usat[1];
                p_pax->b_usat = usat[2];
                p_pax->gb_usat = usat[3];
                break;
            }

            #if 0
                mmsdbg (DL_PRINT, " r = %f, gr = %f, gb = %f, b = %f,       r_usat = %f\n",
                        p_pax->r, p_pax->gr, p_pax->gb, p_pax->b, p_pax->r_usat
                        );
            #endif
            _pax++;
            p_pax++;
        }
        rel_ts += row_time*cfg->aeAwbConfig.patchHeight;
    }
    return 0;
}

static float get_last_know_end_pos(lens_movement_data_t *p_lm)
{
    float pos = 0.0;

    osal_mutex_lock_timeout(p_lm->lens_lock, DEFAULT_ALLOC_TIMEOUT_MS);
    pos = p_lm->last_know_position_end;
    osal_mutex_unlock(p_lm->lens_lock);

    return pos;
}

static int find_lens_move_tails(lens_movement_data_t *p_lm, osal_timeval start_ts, osal_timeval end_ts)
{
    int res = 1;
    int8 is_in_range;
    uint32 tmp_tail = p_lm->head;
    lens_mov_entry_t *p_ent;

    osal_mutex_lock_timeout(p_lm->lens_lock, DEFAULT_ALLOC_TIMEOUT_MS);

    if(p_lm->head != 0)
    {
        do
        {
           tmp_tail--;
           p_ent = &p_lm->entries[tmp_tail&LENS_MOV_MASK];
           if((end_ts > p_ent->end_ts)) {
               res = 0;
               break;
           }
           is_in_range = ((((tmp_tail & LENS_MOV_MASK) != (p_lm->head & LENS_MOV_MASK)) && (p_lm->head >= MAX_LENS_MOV_ENTRIES)) ||
           ((p_lm->head < MAX_LENS_MOV_ENTRIES) && (tmp_tail != 0)));
        }while(is_in_range);

        p_lm->tail_end = tmp_tail;
        if(start_ts < end_ts){
            tmp_tail++;
            do
            {
               tmp_tail--;
               p_ent = &p_lm->entries[tmp_tail&LENS_MOV_MASK];
               if((start_ts > p_ent->end_ts)) {
                   res = 0;
                   break;
               }
               is_in_range = ((((tmp_tail & LENS_MOV_MASK) != (p_lm->head & LENS_MOV_MASK)) && (p_lm->head >= MAX_LENS_MOV_ENTRIES)) ||
               ((p_lm->head < MAX_LENS_MOV_ENTRIES) && (tmp_tail != 0)));
            }while(is_in_range);
            p_lm->tail_st = tmp_tail;
        } else {
            p_lm->tail_st = p_lm->tail_end;
            res = 2;
        }
    }

    osal_mutex_unlock(p_lm->lens_lock);
    return res;
}

static float find_lens_position(lens_movement_data_t *p_lm,
                                     float *last_know_position,
                                     osal_timeval ts,
                                     uint32 *tail)
{
    lens_mov_entry_t *p_ent;
    float pos;
    uint32 tmp_tail = *tail;

    if (p_lm->head != tmp_tail)
    {
        for (;p_lm->head != tmp_tail; tmp_tail++)
        {
            p_ent = &p_lm->entries[tmp_tail&LENS_MOV_MASK];

            if (p_ent->start_ts >= ts)
                break;

            if (p_ent->end_ts < ts) {
                *last_know_position = p_ent->e_pos;
                mmsdbg (DL_MESSAGE, "MOV: SKIP s %jd e %jd ts %jd --> delta %d frame delta %d %d \n",
                        p_ent->start_ts,
                        p_ent->end_ts,
                        ts,
                        p_ent->end_ts-p_ent->start_ts,
                        p_ent->end_ts - ts,
                        p_ent->start_ts - ts
                        );
                continue;
            } else {
                if (p_ent->end_ts > p_ent->start_ts)
                {
                    *last_know_position = (p_ent->e_pos - p_ent->s_pos) *
                                                ((float)(ts-p_ent->start_ts) / (float)(p_ent->end_ts - p_ent->start_ts));
                    *last_know_position += p_ent->s_pos;
                } else {
                    mmsdbg(DL_ERROR, "Lens movement internal error end_ts %f < start_ts",
                           p_ent->end_ts,
                           p_ent->start_ts);
                }
                break;
            }
        }
        if (*last_know_position > 1.0)
        {
            mmsdbg(DL_ERROR, "Lens movement internal error last %f > 1", *last_know_position);
            *last_know_position = 1.0;
        }
        if (*last_know_position < 0.0)
        {
            mmsdbg(DL_ERROR, "Lens movement internal error last %f < 0", *last_know_position);
            *last_know_position = 0.0;
        }
    }

    pos = *last_know_position;
    *tail = tmp_tail;

    return pos;
}

static float calc_lens_position(lens_movement_data_t *p_lm,
                                    osal_timeval ts_start,
                                    osal_timeval ts_end)
{
    float start_pos, end_pos, calc_pos;
    uint32 tail_start_tmp, tail_end_tmp;
    float sum;
    lens_mov_entry_t *p_ent1, *p_ent2;

    osal_mutex_lock_timeout(p_lm->lens_lock, DEFAULT_ALLOC_TIMEOUT_MS);
    start_pos = find_lens_position(p_lm, &p_lm->last_know_position_st,ts_start, &p_lm->tail_st);
    end_pos = find_lens_position(p_lm, &p_lm->last_know_position_end, ts_end, &p_lm->tail_end);

    tail_start_tmp = p_lm->tail_st;
    if(p_lm->head == tail_start_tmp) {
        tail_start_tmp--;
    }
    tail_end_tmp = p_lm->tail_end;
    if(p_lm->head == tail_end_tmp) {
        tail_end_tmp--;
    }

    if (tail_end_tmp != tail_start_tmp) {
        p_ent1 = &p_lm->entries[tail_start_tmp&LENS_MOV_MASK];

        if(ts_start <= p_ent1->start_ts) {
            sum = (float)(p_ent1->start_ts - ts_start)*(p_ent1->s_pos + start_pos)/2;
            p_ent2 = p_ent1;
        } else if(ts_start > p_ent1->end_ts) {
            p_ent2 = &p_lm->entries[(++tail_start_tmp)&LENS_MOV_MASK];
            sum = (float)(p_ent2->start_ts - ts_start)*(p_ent2->s_pos + start_pos)/2;
        } else {
            p_ent2 = &p_lm->entries[(++tail_start_tmp)&LENS_MOV_MASK];
            sum = (float)(p_ent1->end_ts - ts_start)*(p_ent1->e_pos + start_pos)/2;
            sum += p_ent2->pos_to_time_stop;
        }
        while((tail_end_tmp) != tail_start_tmp)
        {
            p_ent1 = p_ent2;
            p_ent2 = &p_lm->entries[(++tail_start_tmp)&LENS_MOV_MASK];
            sum += p_ent1->pos_to_time_move;
            sum += p_ent2->pos_to_time_stop;
        }

        if(ts_end >= p_ent2->end_ts) {
            sum += p_ent2->pos_to_time_move;
            sum += (float)(ts_end - p_ent2->end_ts)*(end_pos + p_ent2->e_pos)/2;
        } else if (ts_end >= p_ent2->start_ts) {
            sum += (float)(ts_end - p_ent2->start_ts)*(end_pos + p_ent2->s_pos)/2;
        } else {
            sum -= (float)(p_ent2->start_ts - ts_end)*(p_ent2->s_pos + end_pos)/2;
        }
        calc_pos = sum/(ts_end - ts_start);
    }
    else {
        calc_pos = (start_pos + end_pos)/2;
    }
    osal_mutex_unlock(p_lm->lens_lock);


    return calc_pos;
}

static int __attribute__((optimize("-O3"))) af_stats_ipipe_convert(hat_h3a_af_stat_t *p_h3a_af_stats,
                               icIspConfig *cfg,
                               uint32_t row_time,
                               lens_movement_data_t *p_lm)
{
    AfPatchStats *patch;
    hat_h3a_af_stat_entry_t *p_pax;

    uint32_t        nx;
    uint32_t        ny;

    uint32_t        pw;
    uint32_t        ph;

    uint32_t        i, n, pix_in_pax, nvals;
    float           divd, divad, ph_div;

    osal_timeval    rel_ts, patch_time;
    osal_timeval    abs_ts_start,abs_ts_end;

    float           patch_lens_position = 0;
    uint32 tmp_tail;
//    lens_mov_entry_t *p_ent;

//    osal_mutex_lock_timeout(p_lm->lens_lock, DEFAULT_ALLOC_TIMEOUT_MS);
//    tmp_tail = p_lm->head - 1;
//    p_ent = &p_lm->entries[tmp_tail&LENS_MOV_MASK];
//    patch_lens_position = p_ent->e_pos;
//    osal_mutex_unlock(p_lm->lens_lock);

    p_h3a_af_stats->ts.time -= (row_time * 8)/1000; // patch ts because of LRT opipe - taken on 8 line IRQ_RATE

    p_h3a_af_stats->actual_lens_position = p_lm->actual_lens_position;


    PROFILE_ADD(PROFILE_ID_SENSOR_READ_FRAME, (uint32)(p_h3a_af_stats->actual_lens_position*1023), (uint32)(p_lm->actual_lens_position*1023));

    nx = cfg->afConfig.nPatchesX;
    ny = cfg->afConfig.nPatchesY;
    n = nx * ny;
    if ( 0 == n ) {
        mmsdbg(DL_WARNING, "Number of paxels == 0");
        return -1;
    }

    pw = cfg->afConfig.patchWidth;
    ph = cfg->afConfig.patchHeight;
    pix_in_pax = pw * ph;

    p_pax = p_h3a_af_stats->paxels;
    memset (p_pax, 0, sizeof(*p_pax) * n);
    if (0 == ph) {
        return 1;
    }

    patch = cfg->afStats;
    rtems_cache_invalidate_multiple_data_lines(patch, sizeof(*patch) * n);

    p_h3a_af_stats->exposure = cfg->env.exp;

    //TODO: Rethink normalized filtered values.
    divad = (float)((1<<cfg->pipelineBits)-1);
    patch_time = (row_time*cfg->afConfig.patchHeight)/1000;      // convert to usec
//    patch_time += p_h3a_af_stats->exposure / ny;

    rel_ts = (row_time*cfg->afConfig.firstPatchY)/1000;      // convert to usec
    rel_ts += (p_h3a_af_stats->exposure+patch_time)/2;      // go at the middle of the patch

//    rel_ts += ((row_time*(cfg->afConfig.patchHeight))/1000)/2; // add half paxel row time
    divd = (float)(pix_in_pax * ((1<<cfg->pipelineBits) - 1) / 2); // pix in pax include all color pixels(gr,gb,r,b), so we divide to 4.

    ph_div = (float)ph*divad;

//    abs_ts_start = p_h3a_af_stats->ts.time - p_h3a_af_stats->exposure;
//    abs_ts_end = p_h3a_af_stats->ts.time;

    abs_ts_start = p_h3a_af_stats->ts.time - p_h3a_af_stats->exposure;
    abs_ts_end = p_h3a_af_stats->ts.time + patch_time*ny + rel_ts;

    PROFILE_ADD(PROFILE_ID_AF_STATS_FRAME, (uint32_t)abs_ts_start, (uint32_t)abs_ts_end);
//    find_lens_move_tails(p_lm, abs_ts_start + rel_ts, abs_ts_end + rel_ts);
    find_lens_move_tails(p_lm, abs_ts_start, abs_ts_end);
    {
        lens_mov_entry_t *p_ent;
        p_ent = &p_lm->entries[p_lm->tail_st&LENS_MOV_MASK];

        PROFILE_ADD_RAW((uint32_t)p_ent->start_ts,
                PROFILE_ID_AF_STATS_FRAME_S_BOUND,
                p_h3a_af_stats->ts.frame_number,
                (uint32_t)(p_ent->e_pos*1000)
                );

        p_ent = &p_lm->entries[p_lm->tail_end&LENS_MOV_MASK];

        PROFILE_ADD_RAW((uint32_t)p_ent->start_ts,
                PROFILE_ID_AF_STATS_FRAME_E_BOUND,
                p_h3a_af_stats->ts.frame_number,
                (uint32_t)(p_ent->e_pos*1000)
                );
    }

    for (i = 0; i < ny; i++)
    {
//        patch_lens_position = calc_lens_position(p_lm,
//                                                 abs_ts_start + rel_ts,
//                                                 abs_ts_end + rel_ts);
        patch_lens_position = find_lens_position(p_lm, &p_lm->last_know_position_st, abs_ts_start + rel_ts, &p_lm->tail_st);

        PROFILE_ADD(PROFILE_ID_AF_STATS_PATCH_ROW, (uint32_t)(patch_lens_position*1000), (uint32_t)abs_ts_start + rel_ts);
        PROFILE_ADD_RAW((uint32_t)(abs_ts_start + rel_ts), PROFILE_ID_AF_STATS_PATCH_ROW_ABS,
                p_h3a_af_stats->ts.frame_number,
                (uint32_t)(patch_lens_position*1000));

        for (n = 0; n < nx; n++)
        {
            p_pax->time_st= rel_ts;
            p_pax->lens_position = patch_lens_position;
            p_pax->avr_pixel = (float)(patch->sum_all_green)/divd;

            nvals = patch->filter1_number_of_used_pixels_green;
            if (nvals) {
                p_pax->val_filter_strong.sum = (float)((patch->filter1_sum_green)/(float)nvals)/divad;
                p_pax->val_filter_strong.max = (float)(patch->filter1_sum_max_green)/ph_div;
            }

            nvals = patch->filter2_number_of_used_pixels_green;
            if (nvals) {
                p_pax->val_filter_weak.sum = (float)((patch->filter2_sum_green)/(float)nvals)/divad;
                p_pax->val_filter_weak.max = (float)(patch->filter2_sum_max_green)/ph_div;
            }

            if ((i == 10)&&(n == 10))
            {
                PROFILE_ADD_RAW((uint32_t)(abs_ts_start + rel_ts), PROFILE_ID_AF_STATS_PATCH_FV,
                        (uint32_t)(p_pax->val_filter_strong.sum*1000000),
                        (uint32_t)(p_pax->val_filter_weak.sum*1000000));
            }

    #if 0
           printf("%3d   avr = %f,  sF1 = %f, mF1 = %f,   sF2 = %f, mF2 = %f\n",
                   i,
                   p_pax->avr_pixel,
                   p_pax->val_filter_strong.sum,
                   p_pax->val_filter_strong.max,
                   p_pax->val_filter_weak.sum,
                   p_pax->val_filter_weak.max
                   );
    #endif

            p_pax++;
            patch++;
        }
        rel_ts += patch_time;
    }
    p_h3a_af_stats->curr_lens_pos = patch_lens_position;
    return 0;
}

int vpipe_convert_aewb_stats (vpipe_ctrl_hndl_t hndl,
                              void *hw_cfg,
                              void* vstats,
                              uint32_t row_time)
{
    vpipe_ctx_t *ctx = hndl;
    icIspConfig *p_cfg = hw_cfg;

    return h3a_stats_ipipe_convert(ctx->format, vstats, p_cfg, row_time);
}

int vpipe_convert_af_stats (vpipe_ctrl_hndl_t hndl,
                            void *hw_cfg,
                            void* vstats,
                            uint32_t row_time,
                            lens_movement_data_t *p_lm)
{
    icIspConfig *p_cfg = hw_cfg;

    return af_stats_ipipe_convert(vstats, p_cfg, row_time, p_lm);
}

/**
 * vpipe_ctrl_config_capture - apply capture supplied settings in hardware.
 *
 * @param hndl - vpipe_ctrl_hndl_t - instance data.
 *
 * @param vpipe_cfg - vpipe_ctrl_settings_t * - pointer to configuration.
 *
 * @return int - non-zero on error.
 */
void* vpipe_ctrl_config_capture (vpipe_ctrl_hndl_t hndl,
                       uint32 frame_number,
                       uint32 config_seq_no,
                       vpipe_ctrl_settings_t *vpipe_cfg,
                       hat_dgain_t *post_gain,
                       cam_capture_request_t* capt,
                       void* cfg_private)
{
    vpipe_ctx_t *ctx = hndl;
    icIspConfig *p_cfg;

    mmsdbg(DL_FUNC, "Enter");

    if (!vpipe_cfg && !hndl) {
        mmsdbg(DL_ERROR, "Null pointer");
        goto exit1;
    }
    p_cfg = ex_pool_alloc_timeout(ctx->pool_ipipe_cfg, DEFAULT_ALLOC_TIMEOUT_MS);
    if (!p_cfg) {
        mmsdbg(DL_ERROR, "Can't alloc icIspConfig");
        goto exit1;
    }

    /* TODO:
     *  VPIPE settings should be converted to
     *  hardware specific values.
     *  Converted values will be applied in corresponding
     *  control register in hardware(IPIPE)
     */
    vpipe_convert_to_ipipe(ctx->conv_hndl, ctx->format, &ctx->in_size, vpipe_cfg, post_gain, p_cfg, 1, cfg_private);

    p_cfg->frameCount = frame_number;
    p_cfg->frameId = config_seq_no;
    p_cfg->userData = cfg_private;

    los_ipipe_TriggerCapture(capt->buff_hndl, p_cfg, ctx->lrt_plugin_id);
    iq_debug_send(ctx->iq_debug, ctx->lrt_plugin_id, &ctx->in_size, &ctx->crop, p_cfg);

    return p_cfg;
exit1:
    mmsdbg(DL_FUNC, "Exit %s", 0 ? "Error" : "Ok");
    return NULL;
}

/**
 * vpipe_ctrl_config - apply supplied settings in hardware.
 *
 * @param hndl - vpipe_ctrl_hndl_t - instance data.
 *
 * @param vpipe_cfg - vpipe_ctrl_settings_t * - pointer to configuration.
 *
 * @return int - non-zero on error.
 */
void* vpipe_ctrl_config (vpipe_ctrl_hndl_t hndl,
                         uint32 frame_number,
                         uint32 config_seq_no,
                       vpipe_ctrl_settings_t *vpipe_cfg,
                       hat_dgain_t *post_gain,
                       cam_isp_params_t* isp,
                       void* cfg_private)
{
    icIspConfig *p_cfg;
    vpipe_ctx_t *ctx = hndl;

    mmsdbg(DL_FUNC, "Enter");

    if (!vpipe_cfg && !hndl) {
        mmsdbg(DL_ERROR, "Null pointer");
        goto exit1;
    }
    p_cfg = ex_pool_alloc_timeout(ctx->pool_ipipe_cfg, DEFAULT_ALLOC_TIMEOUT_MS);
    if (!p_cfg) {
        mmsdbg(DL_ERROR, "Can't alloc icIspConfig");
        goto exit1;
    }

    p_cfg->frameCount = frame_number;
    p_cfg->frameId = config_seq_no;
    /* TODO:
     *  VPIPE settings should be converted to
     *  hardware specific values.
     *  Converted values will be applied in corresponding
     *  control register in hardware(IPIPE)
     */
    vpipe_convert_to_ipipe(ctx->conv_hndl, ctx->format, &ctx->in_size,
                            vpipe_cfg, post_gain, p_cfg, 0, cfg_private);

    p_cfg->dirtyFlags  = isp->flags;
    p_cfg->enableFlags = isp->en_flags;
    p_cfg->pipeControl = isp->out_flags;

    p_cfg->userData = cfg_private;

    los_configIsp(p_cfg, ctx->lrt_plugin_id);
    return p_cfg;

exit1:
    mmsdbg(DL_FUNC, "Exit %s", 0 ? "Error" : "Ok");
    return NULL;
}

/**
 * vpipe_ctrl_destroy - destroy vpipe instance.
 *
 * @param hndl - vpipe_ctrl_hndl_t -  instance data.
 *
 * @return int - non-zero on error.
 */
int vpipe_ctrl_destroy (vpipe_ctrl_hndl_t hndl)
{
    vpipe_ctx_t *ctx = hndl;
    int     ret = 0;

    mmsdbg(DL_FUNC, "Enter");

    if (!hndl) {
        ret = -1;
        mmsdbg(DL_ERROR, "Null pointer");
        goto exit1;
    }
    los_stop();
    iq_debug_destroy(ctx->iq_debug);
    vpipe_conv_destroy(ctx->conv_hndl);
    ex_pool_destroy(ctx->pool_ipipe_cfg);
    osal_free(hndl);
exit1:
    mmsdbg(DL_FUNC, "Exit %s", 0 ? "Error" : "Ok");
    return ret;
}

/**
 * vpipe_ctrl_create - create vpipe instance.
 *
 * @param hndl - apipe_hndl_t * - pointer to instance data.
 *
 * @return int - non-zero on error.
 */
int vpipe_ctrl_create (vpipe_ctrl_hndl_t *hndl, void* private_data, int cam_id)
{
    int         ret = 0, cnt;
    vpipe_ctx_t *ctx;
    icIspConfig *p[IPIPE_NUM_CONFIGS];

    mmsdbg(DL_FUNC, "Enter");
    ctx = osal_malloc(sizeof(*ctx));

    if (!ctx) {
        ret = -1;
        mmsdbg(DL_ERROR, "Can't allocate memory");
        goto exit1;
    }
    ctx->name = "IpipeCtrl";
    ctx->private_data = private_data;

    ctx->pool_ipipe_cfg = ex_pool_create("CAM IPIPE CONFIGURATIONS",
                                         &aewb_ipipe_cfg_root, IPIPE_NUM_CONFIGS);
    if (NULL == ctx->pool_ipipe_cfg) {
        ret = -1;
        mmsdbg(DL_ERROR, "Can't allocate pool_ipipe_cfg");
        goto exit1;
    }

    if (vpipe_conv_create(&ctx->conv_hndl)) {
        ret = -1;
        goto exit2;
    }
    ctx->iq_debug = iq_debug_create();
    for (cnt = 0; cnt < IPIPE_NUM_CONFIGS; ++cnt)
    {
        p[cnt] = ex_pool_alloc_timeout(ctx->pool_ipipe_cfg, DEFAULT_ALLOC_TIMEOUT_MS);
        vpipe_conv_get_default_ipipe_conf(ctx->conv_hndl, p[cnt]);
    }
    for (cnt = 0; cnt < IPIPE_NUM_CONFIGS; ++cnt)
    {
        osal_free(p[cnt]);
    }

    ctx->lrt_plugin_id = cam_id;

    *hndl = ctx;

     los_start(ctx->private_data);

    return ret;

exit2:
    ex_pool_destroy(ctx->pool_ipipe_cfg);
    osal_free(ctx);
exit1:
    return ret;
}


int vpipe_ctrl_start (vpipe_ctrl_hndl_t hndl,
                            hat_size_t in_size,
                            hat_rect_t crop,
                            hat_pix_fmt_t fmt,
                            void *dtp_static_comm,
                            void *dtp_static_prv)
{
    vpipe_ctx_t *ctx = hndl;

    if (!hndl) {
        mmsdbg(DL_ERROR, "Null pointer");
        return -1;
    }

    ctx->format = fmt;
    ctx->in_size = in_size;
    ctx->crop = crop;

    vpipe_conv_config(ctx->conv_hndl, dtp_static_comm, dtp_static_prv);
    return 0;
}

int vpipe_ctrl_stop (vpipe_ctrl_hndl_t hndl)
{
 //   vpipe_ctx_t *ctx = hndl;
    return 0;
}


