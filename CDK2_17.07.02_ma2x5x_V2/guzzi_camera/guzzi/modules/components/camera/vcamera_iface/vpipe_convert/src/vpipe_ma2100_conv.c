/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file vpipe_conv.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <osal/osal_stdlib.h>
#include <osal/osal_string.h>
#include <utils/mms_debug.h>
#include <camera/vcamera_iface/vpipe_convert/include/vpipe_conv.h>
#include <math.h>

#include "IspCommon.h"
#include "ipipe.h"
#include "ipipe_params.h"

#include"ipipe_chroma_median.h"
#include"ipipe_chroma_nf.h"
#include"ipipe_demosaic.h"
#include"ipipe_dpc.h"
#include"ipipe_gen_chroma.h"
#include"ipipe_grgb.h"
#include"ipipe_ltm.h"
#include"ipipe_luma_nf.h"
#include"ipipe_sharpening.h"
#include"ipipe_focus_filters.h"

/* Ipipe defaults come from these files */
#include "isp_params_video.h"
#include "isp_params_pp.h"

#include <hal/hat_light.h>

mmsdbg_define_variable(
        vdl_vpipe_convert,
        DL_DEFAULT,
        0,
        "VPIPE conv",
        "VPIPE conv"
);
#define MMSDEBUGLEVEL mmsdbg_use_variable(vdl_vpipe_convert)



extern light_simulator_t light_simulator;

/*
 * ipipe_ctx_t
 * instance data
 */
typedef struct {
    const char *name;
    dtp_hndl_t hdtp_dpc;
    dtp_hndl_t hdtp_grgb;
    dtp_hndl_t hdtp_demosaic;
    dtp_hndl_t hdtp_gen_chroma;
    dtp_hndl_t hdtp_lumma_nf;
    dtp_hndl_t hdtp_ltm;
    dtp_hndl_t hdtp_chroma_median;
    dtp_hndl_t hdtp_chroma_nf;
    dtp_hndl_t hdtp_sharpening;
    dtp_hndl_t hdtp_foc_filters;

    dtp_leaf_data_t leaf_dpc;
    dtp_leaf_data_t leaf_grgb;
    dtp_leaf_data_t leaf_demosaic;
    dtp_leaf_data_t leaf_gen_chroma;
    dtp_leaf_data_t leaf_lumma_nf;
    dtp_leaf_data_t leaf_ltm;
    dtp_leaf_data_t leaf_chroma_median;
    dtp_leaf_data_t leaf_chroma_nf;
    dtp_leaf_data_t leaf_sharpening;
    dtp_leaf_data_t leaf_foc_filters;

} ipipe_ctx_t;

// BASE_ISO (SENSOR_ISO)
/* VP: For now this is BASE ISO  -> corresponds to gain = 1.0 */
#define SENSOR_ISO      100

#define MAX_AWB_GAIN        (8.0)
#define MAX_RGB2RGB_GAIN    (4.0)

#define NUM_TUNED_CONFIGS   (5)
// TODO This should be in tuning data
#define CHROMA_DENOISE_REF_CT            4000
#define CHROMA_DENOISE_MIN_CT            2000
#define CHROMA_DENOISE_MAX_CT            8000

typedef struct {
    int             iso;
    ipipe_params    prev;
    ipipe_params    still;
    const char      *fname;
} tuned_cfg_t;


static inline float clampf(float x, float a, float b)
{
    return x < a ? a : (x > b ? b : x);
}

static inline int clampi(int x, int a, int b)
{
    return x < a ? a : (x > b ? b : x);
}

static inline unsigned clampu(unsigned x, unsigned a, unsigned b)
{
    return x < a ? a : (x > b ? b : x);
}

static uint16_t f32_to_f16(float f)
{
	uint32_t	in;
	int		    expo;
	uint32_t	*p32 = (uint32_t *)&f;

	in = *p32;
	expo = (in & 0x7f800000) >> 23;
	expo = expo - 127 + 15;
	if (expo > 31) {
		expo = 31;
	}
	if (expo < 0) {
		expo = 0;
	}
	return (in & (1<<31)) >> 16 | expo << 10 | (in & 0x007fffff) >> 13;
}

static uint32_t
interp_f2(uint32_t c0, uint32_t c1, float alpha)
{
    int c0i, c1i, c, c_out = 0;
    int i;

    /*
     * Convert kernel co-efficients from log2 values to integers, interpolate,
     * then convert back to log2 bit-packed form.
     */
    for (i = 0; i < 16; i++) {
        c0i = 1 << ((c0 >> (i*2)) & 3);
        c1i = 1 << ((c1 >> (i*2)) & 3);
        c = clampu(c0i * (1.0-alpha) + c1i * alpha, 0, 8);
        if (c <= 1) {
            c = 0;
        } else if (c <= 2) {
            c = 1;
        } else if (c <= 4) {
            c = 2;
        } else if (c <= 8) {
            c = 3;
        }
        c_out |= c << (i*2);
    }

    return c_out;
}

/* Interpolate "Horizontal Enables" in Chroma Denoise filter */
static unsigned
interp_hEnab(unsigned e0, unsigned e1, float alpha)
{
    int kw0, kw1, kw;

    /* Compute kernel widths */
    kw0 = (e0 & 1) * 23 + ((e0>>1) & 1) * 17 + ((e0>>2) & 1) * 13;
    kw1 = (e1 & 1) * 23 + ((e1>>1) & 1) * 17 + ((e1>>2) & 1) * 13;

    kw = clampu(kw0 * (1.0-alpha) + kw1 * alpha, 0, 53);

    if (kw <= 13) {
        return 4;
    } else if (kw <= 17) {
        return 2;
    } else if (kw <= 23) {
        return 1;
    } else if (kw <= 30) {
        return 6;
    } else if (kw <= 36) {
        return 5;
    } else if (kw <= 40) {
        return 3;
    } else {
        return 7;
    }
}

static unsigned
interp_median_ksize(unsigned k0, unsigned k1, float alpha)
{
    int    k;

    k = clampu(k0 * (1.0-alpha) + k1 * alpha + 0.5, 0, 7);
    k = ((k-1) / 2) * 2 + 1;
    return  k < 3 ? 0 : k;
}

static void
luma_denoise_ref_gen_lut_dist(icIspConfig *conf,
  float angle_of_view, int image_width, int image_height)
{
    float   angle;
    int     w2 = image_width/2;
    int     h2 = image_height/2;
    int     maxval, shift, dx2_plus_dy2;
    int     i;
    float   cornerval;
    uint8_t *lut = conf->lumaDenoiseRef.lutDist;

	/* Get half the angle, in radians */
    angle = (angle_of_view/2) / 360 * 2*M_PI;

	/* Maximum value that can be put into LUT */
    maxval = w2*w2 + h2*h2;

    /*
     * Find how many bits to shift values right by, so that we can use
	 * an 8-bit LUT.
     */
	shift = floorf(log2f(maxval)) + 1 - 8;
	cornerval = sqrtf(w2*w2+h2*h2);

    /*
     * LUT is indexed at runtime by (dx^2+dy^2) >> shift, where dx, dy
     * is the x/y displacement of the current pixel from the image centre.
     */
    for (i = 0; i < 255; i++) {
        dx2_plus_dy2 = i << shift;
        lut[i] = powf(cosf(sqrtf(dx2_plus_dy2) / cornerval * angle), 4) * 255 + .5;
    }
    conf->lumaDenoiseRef.shift = shift;
}

static void
luma_denoise_ref_gen_lut_gamma(icIspConfig *conf, float gamma)
{
    uint8_t *lut = conf->lumaDenoiseRef.lutGamma;
    int     i;

    for (i = 0; i < 256; i++) {
        lut[i] = powf((float)i / 255, gamma) * 255 + .5;
    }
}

static void
luma_denoise_gen_lut_bitpos(icIspConfig *conf, float strength)
{
    uint8_t     *lut = conf->lumaDenoise.lut;
    uint32_t     *bitpos = &conf->lumaDenoise.bitpos;
    float       alpha, divisor, sigma, npot, l;
    int         i, bp;

    if (strength < .001f) {
        /* Avoid division by 0 */
        strength = .001f;
    }
    if (strength > 2047.0f) {
        /* Limit to prevent 'bitpos' > 11 */
        strength = 2047.0f;
    }
    bp = (int)floorf(log2f(strength));   /* MSB position */
    if (bp < 0) {
        bp = 0;
    }

    npot = (float)(1<<*bitpos);             /* nearest power of two (rounding down) */

    *bitpos = (int32_t)bp;

    alpha = (strength - npot) / npot;
    divisor = 4*(1-alpha) + 8*(alpha);
    sigma = .05f;
    if (strength < 1) {
        /* Reduce sigma when 0 < strength < 1 */
        sigma = sigma * strength;
    }
    for (i = 0; i < 32; i++) {
        l = (float)i / 31 / divisor;
        l = expf(-(powf(l, 2)) / (2*(powf(sigma,2))));    /* Gaussian */

        /* LUT entries will be quantized to 16 possible values */
        lut[i] = (uint8_t)(l*16-1 + .5f);
    }
}

static void
sharpen_gen_coeffs(float sigma, uint16_t *coeffs)
{
    int     i, j;
    float   denom = 2 * sigma*sigma;
    float   k[4], sum = 0;

    for (i = 0; i < 4; i++) {
        j = 3 - i;
        k[i] = expf(-(float)(j*j) / denom);
        sum += k[i];
    }

    // Coefficients should sum to 1
    sum = sum + (sum - k[3]);
    for (i = 0; i < 4; i++) {
        coeffs[i] = f32_to_f16(k[i] / sum);
    }
}

/*
 * Default configuration - all the settings needed to configure working
 * Preprocessor and Preview pipeline.  Can be used to produce the first
 * frames on the preview display even if DTP settings have not yet been
 * downloaded.
 */
void vpipe_conv_get_default_ipipe_conf(ipipe_conv_hndl_t hndl, icIspConfig *conf)
{
    unsigned int    i;
    float           maxval;

    /*
     * DD: This function should be told how many bits the sensor is configured
     * to output - for now it's hardcoded for 10 bits!
     */
#define PIPELINE_BITS   10
    maxval = (1<<PIPELINE_BITS) - 1;
    conf->pipelineBits = PIPELINE_BITS;

    conf->bayerOrder   = IC_BAYER_FORMAT_BGGR; // TODO: Fix me with real value

    conf->raw.clampGr = maxval;
    conf->raw.clampR  = maxval;
    conf->raw.clampB  = maxval;
    conf->raw.clampGb = maxval;

    /*
     * Use Raw filter gains to recover full [0, 1023] range after
     * Black Level subtract.
     */
    conf->raw.gainGr = maxval / (maxval-ISPC_BLACK_LEVEL_GR) * 256;
    conf->raw.gainR  = maxval / (maxval-ISPC_BLACK_LEVEL_R ) * 256;
    conf->raw.gainB  = maxval / (maxval-ISPC_BLACK_LEVEL_B ) * 256;
    conf->raw.gainGb = maxval / (maxval-ISPC_BLACK_LEVEL_GB) * 256;

    conf->blc.gr = ISPC_BLACK_LEVEL_GR;
    conf->blc.r  = ISPC_BLACK_LEVEL_R;
    conf->blc.b  = ISPC_BLACK_LEVEL_B;
    conf->blc.gb = ISPC_BLACK_LEVEL_GB;

    conf->lsc.lscWidth  = ISPC_LSC_GAIN_MAP_W;
    conf->lsc.lscHeight = ISPC_LSC_GAIN_MAP_H;
    osal_memcpy(conf->lsc.pLscTable, ispcLscMesh, sizeof(ispcLscMesh));

    conf->raw.grgbImbalPlatDark = ISPC_GRGB_IMBAL_PLAT_DARK;
    conf->raw.grgbImbalDecayDark = ISPC_GRGB_IMBAL_DECAY_DARK;
    conf->raw.grgbImbalPlatBright = ISPC_GRGB_IMBAL_PLAT_BRIGHT;
    conf->raw.grgbImbalDecayBright = ISPC_GRGB_IMBAL_DECAY_BRIGHT;
    conf->raw.grgbImbalThr = ISPC_GRGB_IMBAL_THRESHOLD;

    conf->raw.dpcAlphaHotG = ISPC_BAD_PIX_ALPHA_G_HOT;
    conf->raw.dpcAlphaHotRb = ISPC_BAD_PIX_ALPHA_RB_HOT;
    conf->raw.dpcAlphaColdG = ISPC_BAD_PIX_ALPHA_G_COLD;
    conf->raw.dpcAlphaColdRb = ISPC_BAD_PIX_ALPHA_RB_COLD;

    conf->raw.dpcNoiseLevel  = ISPC_BAD_PIX_NOISE_LEVEL;

    conf->raw.outputBits = ISPC_RAW_OUTPUT_BITS;

    /* White balance */
    conf->wbGains.gainR = ISPC_GAIN_R;
    conf->wbGains.gainG = ISPC_GAIN_G;
    conf->wbGains.gainB = ISPC_GAIN_B;

    conf->chromaGen.epsilon = ISPC_VIDEO_CHROMA_GEN_EPSILON;

    conf->lumaDenoise.alpha = ISPC_VIDEO_LUMA_DNS_ALPHA;
    conf->lumaDenoise.strength = ISPC_VIDEO_LUMA_DNS_STRENGTH;
    conf->lumaDenoise.f2 = ISPC_VIDEO_LUMA_DNS_F2;
    luma_denoise_gen_lut_bitpos(conf, ISPC_VIDEO_LUMA_DNS_STRENGTH);

    conf->lumaDenoiseRef.angle_of_view = ISPC_VIDEO_LUMA_DNS_REF_ANGLE_OF_VIEW;
    conf->lumaDenoiseRef.gamma = ISPC_VIDEO_LUMA_DNS_REF_GAMMA;
    osal_memcpy(conf->lumaDenoiseRef.lutDist, ispcVideoYDnsDistLut, 256);
    osal_memcpy(conf->lumaDenoiseRef.lutGamma, ispcVideoYDnsGammaLut, 256);
    conf->lumaDenoiseRef.shift = ISPC_VIDEO_LUMA_DNS_REF_SHIFT;

    conf->chromaDenoise.th_r = ISPC_VIDEO_CHROMA_DNS_TH_R;
    conf->chromaDenoise.th_g = ISPC_VIDEO_CHROMA_DNS_TH_G;
    conf->chromaDenoise.th_b = ISPC_VIDEO_CHROMA_DNS_TH_B;
    conf->chromaDenoise.limit = ISPC_VIDEO_CHROMA_DNS_LIMIT;
    conf->chromaDenoise.hEnab = ISPC_VIDEO_CHROMA_DNS_H_ENAB;

    conf->median.kernelSize = ISPC_VIDEO_CHROMA_MEDIAN_SIZE;

    conf->greyDesat.slope = ISPC_VIDEO_GREY_DESAT_SLOPE;
    conf->greyDesat.offset = ISPC_VIDEO_GREY_DESAT_OFFSET;

    for (i = 0; i < 2; i++) {
        conf->lowpass.coefs[i] = ispcVideoLowpassKernel[i];
    }

    conf->sharpen.sigma = f32_to_f16(ISPC_VIDEO_SHARP_SIGMA);
    conf->sharpen.strength_darken = f32_to_f16(ISPC_VIDEO_SHARP_STRENGTH_DARKEN);
    conf->sharpen.strength_lighten = f32_to_f16(ISPC_VIDEO_SHARP_STRENGTH_LIGHTEN);
    conf->sharpen.alpha = f32_to_f16(ISPC_VIDEO_SHARP_ALPHA);
    conf->sharpen.overshoot = f32_to_f16(ISPC_VIDEO_SHARP_OVERSHOOT);
    conf->sharpen.undershoot = f32_to_f16(ISPC_VIDEO_SHARP_UNDERSHOOT);
    conf->sharpen.rangeStop0 = f32_to_f16(ISPC_VIDEO_SHARP_RANGE_STOP_0);
    conf->sharpen.rangeStop1 = f32_to_f16(ISPC_VIDEO_SHARP_RANGE_STOP_1);
    conf->sharpen.rangeStop2 = f32_to_f16(ISPC_VIDEO_SHARP_RANGE_STOP_2);
    conf->sharpen.rangeStop3 = f32_to_f16(ISPC_VIDEO_SHARP_RANGE_STOP_3);
    conf->sharpen.minThr = f32_to_f16(ISPC_VIDEO_SHARP_MIN_THR);

    conf->colorCombine.desat_mul = ISPC_VIDEO_DESAT_MUL;
    conf->colorCombine.desat_t1 = ISPC_VIDEO_DESAT_T1;

    conf->gamma.rangetop = 0;

    conf->userData = NULL;
    conf->pipeControl = DEF_PRV_PIPE_CTRL_FLAGS;
}

int vpipe_conv_dcsub(icIspConfig *conf, vpipe_ctrl_dcsub_t vdcsub, unsigned int max_raw_val)
{
    const  hat_dcsub_t *dcsub = vdcsub.data;

    conf->blc.gr = (uint32_t)(dcsub->ofs.gr * (float)max_raw_val+0.5);
    conf->blc.r  = (uint32_t)(dcsub->ofs.r  * (float)max_raw_val+0.5);
    conf->blc.b  = (uint32_t)(dcsub->ofs.b  * (float)max_raw_val+0.5);
    conf->blc.gb = (uint32_t)(dcsub->ofs.gb * (float)max_raw_val+0.5);
/*
    if (conf->waitSeqNo%50 == 0)
        light_simulator.flash.cnt = 4;

    if (light_simulator.flash.cnt)
    {
        light_simulator.flash.cnt--;
    } else {
        light_simulator.flash.mult = 1.0;
    }
*/
    if (light_simulator.flash.mult == 0)
        light_simulator.flash.mult = 1.0;

    conf->raw.gainGr = (uint32_t)(light_simulator.flash.mult*dcsub->mult.gr * 256.0+0.5);
    conf->raw.gainR  = (uint32_t)(light_simulator.flash.mult*dcsub->mult.r  * 256.0+0.5);
    conf->raw.gainB  = (uint32_t)(light_simulator.flash.mult*dcsub->mult.b  * 256.0+0.5);
    conf->raw.gainGb = (uint32_t)(light_simulator.flash.mult*dcsub->mult.gb * 256.0+0.5);

    return 0;
}

int vpipe_conv_wb(icIspConfig *conf, vpipe_ctrl_wb_t vwb)
{
    const hat_wbal_coef_t *wb = vwb.data;

    conf->wbGains.gainR = (uint32_t)(wb->c.r * 256.0);
    conf->wbGains.gainG = (uint32_t)((wb->c.gr + wb->c.gb) * 128.0);
    conf->wbGains.gainB = (uint32_t)(wb->c.b * 256.0);
    return 0;
}

int vpipe_conv_rgb2rgb(icIspConfig *conf, vpipe_ctrl_rgb2rgb_t vrgb2rgb)
{
    uint32 i, j, t;
    const hat_rgb2rgb_t *rgb2rgb = vrgb2rgb.data;

    t = 0;
    for (i=0;i<3;i++) {
        for (j=0;j<3;j++) {
            conf->colorCombine.ccm[t++] = rgb2rgb->m.m[i][j];
        }
    }

    return 0;
}

static int gen_gamma_lut(uint16_t *inLut, uint16_t *outLut, int lutSize, int normFactor)
{
    unsigned            i, idx;
    unsigned            offLsbTab[11] = {10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0};
    unsigned            offLsb;
    unsigned            epr = lutSize/16;   // Entries / Range
    unsigned            rsi;                // Range Size Indicator
    uint16_t            fpNum, lutOutput;
    float               f32;

    rsi = roundf(logf(epr)/logf(2));        // rsi = log2(epr)
    offLsb = offLsbTab[rsi];

    for (i = 0; i < lutSize; i++) {
        // Build the fp16 number that corresponds to Range, Offset, Interp=0
        // Meaning: the number that would cause lut to select cell "i"
        fpNum = i<<offLsb;

        // Convert the fp16 number above to fp32
        *(uint32_t *)&f32 =
          ((fpNum>>10) - 15 + 127) << 23 | (fpNum & 0x3ff) << (23-10);

        // Map through the input LUT, and store it to location "i"
        // We output each value 4 times, because "In Channel Mode the
        // LBRC read client is set up to read from up to 4 different
        // planes at a time."
        idx = f32*lutSize;
        if (idx > lutSize-1) {
            idx = lutSize-1;
        }
        lutOutput = f32_to_f16((float)inLut[idx] / normFactor);
        outLut[i*4+0] = lutOutput; //Red
        outLut[i*4+1] = lutOutput; //Green
        outLut[i*4+2] = lutOutput; //Blue
        outLut[i*4+3] = lutOutput; //Pad
    }

    return 0;
}

int vpipe_conv_gamma(icIspConfig *conf, vpipe_ctrl_gamma_t vgamma)
{
#define LUTSIZE     512
#define NORM_FACTOR 1024
    // 10->8bit gamma
    // only R gamma is used
    const hat_gamma_t *gamma = vgamma.data;
    uint32 i, j, jmin, jmax;
    uint32 n = gamma->r.tbl_size-1;
    float base, inc;
    uint16_t tmp[LUTSIZE];

    for (i=0;i<n;i++) {
        jmin = (uint32)((gamma->r.tbl+i)->i * (LUTSIZE-1));
        jmax = (uint32)((gamma->r.tbl+i+1)->i * (LUTSIZE-1));
        if(jmin>=jmax) continue; // skip equal from rounding
        base = (gamma->r.tbl+i)->o;
        inc = ((gamma->r.tbl+i+1)->o - (gamma->r.tbl+i)->o)/(float)(jmax - jmin);
        for (j=jmin;j<=jmax;j++) {
            tmp[j] = (uint16_t)((base + (float)(j-jmin) * inc ) * NORM_FACTOR+0.5);
            //printf(" j = %d , o = %d\n", j, tmp[j]);
        }
    }

    gen_gamma_lut(tmp, conf->gamma.table, LUTSIZE, NORM_FACTOR);

    conf->gamma.size = LUTSIZE;

    return 0;
}

#define IPIPE_LSC_PADDING_MASK (IPIPE_LSC_PADDING-1)

int vpipe_conv_lsc(hat_pix_fmt_t fmt, icIspConfig *conf, vpipe_ctrl_lsc_t vlsc)
{
    uint16_t *p_r, *p_gr, *p_gb, *p_b;
    uint32 x, y;
    uint16_t *pLSC = conf->lsc.pLscTable;
    uint32 w = vlsc.data->lsc_tbl_size.w;
    uint32 h = vlsc.data->lsc_tbl_size.h;

    uint32 line_x_sz = ((2*w + IPIPE_LSC_PADDING_MASK) & (~IPIPE_LSC_PADDING_MASK));
    hat_bayer_float_t *pi = vlsc.data->lsc_tbl;


    switch (fmt.fmt.order.all)
    {
    case HAT_PORDBYR_B_Gb_Gr_R:
        p_b  = pLSC;
        p_gb = pLSC + 1;
        p_gr = pLSC + line_x_sz;
        p_r  = pLSC + line_x_sz + 1;
        break;

    case HAT_PORDBYR_R_Gr_Gb_B:
        p_r  = pLSC;
        p_gr = pLSC + 1;
        p_gb = pLSC + line_x_sz;
        p_b  = pLSC + line_x_sz + 1;
        break;

    case HAT_PORDBYR_Gb_B_R_Gr:
        p_gb = pLSC;
        p_b  = pLSC + 1;
        p_r  = pLSC + line_x_sz;
        p_gr = pLSC + line_x_sz + 1;
        break;

    case HAT_PORDBYR_Gr_R_B_Gb:
        p_gr = pLSC;
        p_r  = pLSC + 1;
        p_b  = pLSC + line_x_sz;
        p_gb = pLSC + line_x_sz + 1;
        break;

    default:
        // HAT_PORDBYR_R_Gr_Gb_B
        p_r  = pLSC;
        p_gr = pLSC + 1;
        p_gb = pLSC + line_x_sz;
        p_b  = pLSC + line_x_sz + 1;
        break;
    }

    for (y = 0; y < h; y++) {
        for (x = 0; x < w; x++) {
            *p_r  = pi->r  * 256; //256;
            *p_gr = pi->gr * 256; //256;
            *p_gb = pi->gb * 256; //256;
            *p_b  = pi->b  * 256; //256;

            p_r  += 2;
            p_gr += 2;
            p_gb += 2;
            p_b  += 2;

            pi++;
        }
        p_r  += 2 * (line_x_sz - w);
        p_gr += 2 * (line_x_sz - w);
        p_gb += 2 * (line_x_sz - w);
        p_b  += 2 * (line_x_sz - w);
    }

    conf->lsc.lscWidth = 2 * w;
    conf->lsc.lscStride = line_x_sz;
    conf->lsc.lscHeight = 2 * h;
    return 0;
}

void vpipe_conv_h3a_aewb(icIspConfig *conf, vpipe_ctrl_h3a_aewb_t h3a, hat_size_t *win_dim)
{
    /* All sizes must be even */
    conf->aeAwbConfig.firstPatchX = ((uint32)((win_dim->w) * h3a.data->win.x) / 2) * 2;
    conf->aeAwbConfig.firstPatchY = ((uint32)((win_dim->h) * h3a.data->win.y) / 2) * 2;
    conf->aeAwbConfig.nPatchesX   = h3a.data->pax.x;
    conf->aeAwbConfig.nPatchesY   = h3a.data->pax.y;

    conf->aeAwbConfig.patchWidth  = ((uint32)((float)win_dim->w * h3a.data->win.w/(float)h3a.data->pax.x) / 2) * 2; /* EVEN */
    conf->aeAwbConfig.patchHeight = ((uint32)((float)win_dim->h * h3a.data->win.h/(float)h3a.data->pax.y) / 2) * 2; /* EVEN */
#if defined(__sparc)
    conf->aeAwbConfig.patchGapX   = conf->aeAwbConfig.patchWidth;
    conf->aeAwbConfig.patchGapY   = conf->aeAwbConfig.patchHeight;
#else
    conf->aeAwbConfig.patchGapX   = 0;
    conf->aeAwbConfig.patchGapY   = 0;
#endif // __sparc
    /* Following is hardcoded for 10-bit Bayer! */
    conf->aeAwbConfig.satThresh   = ((1<<10)-1)*h3a.data->unsat_threshold;
}

void vpipe_conv_h3a_af(icIspConfig *conf, vpipe_ctrl_h3a_af_t h3a, hat_size_t *win_dim)
{
    uint32 i;

    // TODO - MUST read from DTP
    int32_t     f1Coeffs[] = {31,19,-32,31,63,31,-50,-35,35,-70,35};  // 0.4-0.7
    int32_t     f2Coeffs[] = {35,11,-29,8,17,8,78,-39,119,-238,119};  // 0.2-0.5

    /* All sizes must be even */
    conf->afConfig.nPatchesX   = h3a.data->pax.x;                                    /* Number paxels in horizontal direction */
    conf->afConfig.nPatchesY   = h3a.data->pax.y;                                    /* Number paxels in vertical direction */
    conf->afConfig.nSkipRows = 0; /* every 4 row is used in calculations */
    conf->afConfig.initialSubtractionValue = 128; // TODO: VPetrov
    conf->afConfig.f1Threshold = 0;
    conf->afConfig.f2Threshold = 0;

#if !defined(__sparc)
    conf->afConfig.firstPatchX = ((uint32)((win_dim->w) * h3a.data->win.x) / 2) * 2;   /* X coord of top-left pixel of first patch */
    conf->afConfig.firstPatchY = ((uint32)((win_dim->h) * h3a.data->win.y) / 2) * 2;   /* Y coord of top-left pixel of first patch */
    conf->afConfig.patchWidth  = ((uint32)((float)win_dim->w * h3a.data->win.w/(float)h3a.data->pax.x) / 2) * 2;   /* Size of paxel horizontally */
    conf->afConfig.patchHeight = ((uint32)((float)win_dim->h * h3a.data->win.h/(float)h3a.data->pax.y) / 2) * 2;  /* Size of paxel vertically */
#else
    conf->afConfig.firstPatchX = (((uint32)((win_dim->w) * h3a.data->win.x) / 2) * 2) >> 1;   /* X coord of top-left pixel of first patch */
    conf->afConfig.firstPatchY = (((uint32)((win_dim->h) * h3a.data->win.y) / 2) * 2) >> 2;   /* Y coord of top-left pixel of first patch */
    conf->afConfig.patchWidth  = (((uint32)((float)win_dim->w * h3a.data->win.w/(float)h3a.data->pax.x) / 2) * 2) >> 1;   /* Size of paxel horizontally */
    conf->afConfig.patchHeight = (((uint32)((float)win_dim->h * h3a.data->win.h/(float)h3a.data->pax.y) / 2) * 2) >> 2;  /* Size of paxel vertically */
#endif

    for (i = 0; i < 11; i++) {
        conf->afConfig.f1Coeffs[i] = f1Coeffs[i];
        conf->afConfig.f2Coeffs[i] = f2Coeffs[i];
    }
}

int vpipe_conv_rgb2yuv(icIspConfig *conf, ipipe_params *params, vpipe_ctrl_rgb2yuv_t rgb2yuv)
{
    uint32 i, j, t;
    const hat_rgb2yuv_t *rgb2yuvd = rgb2yuv.data;

    t = 0;
    for (i=0;i<3;i++) {
        for (j=0;j<3;j++) {
            conf->colorConvert.mat[t++] = rgb2yuvd->m.m[i][j];
        }
    }
   	conf->colorConvert.offset[0] = rgb2yuvd->o.y;
   	conf->colorConvert.offset[1] = rgb2yuvd->o.u;
   	conf->colorConvert.offset[2] = rgb2yuvd->o.v;


    return 0;
}

/**
 *
 */
static int interpol_1D_ipipe_dpc(ipipe_dpc_t *p_in0, ipipe_dpc_t *p_in1, float k0, float k1,
                                icIspConfig *conf)
{
    conf->raw.dpcAlphaHotG      = p_in0->dpcAlphaHotG * k0 + p_in1->dpcAlphaHotG * k1;
    conf->raw.dpcAlphaHotRb     = p_in0->dpcAlphaHotRb * k0 + p_in1->dpcAlphaHotRb * k1;
    conf->raw.dpcAlphaColdG     = p_in0->dpcAlphaColdG * k0 + p_in1->dpcAlphaColdG * k1;
    conf->raw.dpcAlphaColdRb    = p_in0->dpcAlphaColdRb * k0 + p_in1->dpcAlphaColdRb * k1;
    conf->raw.dpcNoiseLevel     = p_in0->dpcNoiseLevel * k0 + p_in1->dpcNoiseLevel * k1;

    return 0;
}

/**
 *
 */
static int interpol_1D_ipipe_grgb(ipipe_grgb_t *p_in0, ipipe_grgb_t *p_in1, float k0, float k1,
                                icIspConfig *conf)
{
    conf->raw.grgbImbalPlatDark     = p_in0->grgbImbalPlatDark * k0 + p_in1->grgbImbalPlatDark * k1;
    conf->raw.grgbImbalDecayDark    = p_in0->grgbImbalDecayDark * k0 + p_in1->grgbImbalDecayDark * k1;
    conf->raw.grgbImbalPlatBright   = p_in0->grgbImbalPlatBright * k0 + p_in1->grgbImbalPlatBright * k1;
    conf->raw.grgbImbalDecayBright  = p_in0->grgbImbalDecayBright * k0 + p_in1->grgbImbalDecayBright * k1;
    conf->raw.grgbImbalThr          = p_in0->grgbImbalThr * k0 + p_in1->grgbImbalThr * k1;

    return 0;
}

/**
 *
 */
static int interpol_1D_ipipe_demosaic(ipipe_demosaic_t *p_in0, ipipe_demosaic_t *p_in1, float k0, float k1,
                                icIspConfig *conf)
{
    conf->demosaic.dewormGradientMul = p_in0->dewormGradientMul * k0 + p_in1->dewormGradientMul * k1;
    conf->demosaic.dewormSlope       = p_in0->dewormSlope * k0 + p_in1->dewormSlope * k1;
    conf->demosaic.dewormOffset      = p_in0->dewormOffset * k0 + p_in1->dewormOffset * k1;

    return 0;
}

/**
 *
 */
static int interpol_1D_ipipe_gen_chroma(ipipe_gen_chroma_t *p_in0, ipipe_gen_chroma_t *p_in1, float k0, float k1,
                                icIspConfig *conf)
{
    conf->purpleFlare.strength   = p_in0->purpleFlare.strength * k0 + p_in1->purpleFlare.strength * k1;

    conf->chromaGen.epsilon      = p_in0->chromaGen.epsilon * k0 + p_in1->chromaGen.epsilon * k1;
    conf->chromaGen.scaleR       = p_in0->chromaGen.scaleR  * k0 + p_in1->chromaGen.scaleR  * k1;
    conf->chromaGen.scaleG       = p_in0->chromaGen.scaleG  * k0 + p_in1->chromaGen.scaleG  * k1;
    conf->chromaGen.scaleB       = p_in0->chromaGen.scaleB  * k0 + p_in1->chromaGen.scaleB  * k1;


    // Gray desaturation
    conf->greyDesat.slope        = p_in0->greyDesat.slope * k0 + p_in1->greyDesat.slope * k1 + 0.5;
    conf->greyDesat.offset       = p_in0->greyDesat.offset * k0 + p_in1->greyDesat.offset * k1;

    return 0;
}

/**
 *
 */
static int interpol_1D_ipipe_luma_nf(ipipe_luma_nf_t *p_in0, ipipe_luma_nf_t *p_in1, float k0, float k1, hat_size_t *size,
                                icIspConfig *conf)
{
    /* Luma Denoise filter */
    conf->lumaDenoise.alpha = p_in0->lumaDenoise.alpha * k0 + p_in1->lumaDenoise.alpha * k1;
    conf->lumaDenoise.f2 = interp_f2(p_in0->lumaDenoise.f2, p_in1->lumaDenoise.f2, k1);

    /* For debug/reference only */
    conf->lumaDenoise.strength = p_in0->lumaDenoise.strength * k0 + p_in1->lumaDenoise.strength * k1;

    luma_denoise_gen_lut_bitpos(conf, conf->lumaDenoise.strength);

    /* For debug/reference only */
    conf->lumaDenoiseRef.angle_of_view = p_in0->lumaDenoiseRef.angle_of_view * k0 + p_in1->lumaDenoiseRef.angle_of_view * k1;
    conf->lumaDenoiseRef.gamma = p_in0->lumaDenoiseRef.gamma * k0 + p_in1->lumaDenoiseRef.gamma * k1;

    luma_denoise_ref_gen_lut_dist(conf,
            conf->lumaDenoiseRef.angle_of_view, size->w, size->h);
    luma_denoise_ref_gen_lut_gamma(conf, conf->lumaDenoiseRef.gamma);

    /* Add Random Noise filter */
    conf->randomNoise.strength = p_in0->randomNoise.strength * k0 + p_in1->randomNoise.strength * k1;

    return 0;
}

/**
 *
 */
static int interpol_1D_ipipe_ltm(ipipe_ltm_t *p_in0, ipipe_ltm_t *p_in1, float k0, float k1,
                                icIspConfig *conf)
{
    int i, x ,j;

    /* Pad from 9x15 to 10x16 by adding an extra row and column */
    for (i = 0, j = 0; i < 15*10; i += 10, j += 9) {
        for (x = 0; x < 9; x++) {
            conf->ltm.curves[i+x] = f32_to_f16(p_in0->ltm.curves[j+x] * k0 + p_in1->ltm.curves[j+x] * k1);
        }
        conf->ltm.curves[i+9] = conf->ltm.curves[i+8];
    }
    for (x = 0; x < 10; x++) {
        conf->ltm.curves[10*15+x] = conf->ltm.curves[10*14+x];
    }


    conf->ltmLBFilter.th1   = p_in0->ltmLBFilter.th1 * k0 + p_in1->ltmLBFilter.th1 * k1;
    conf->ltmLBFilter.th2   = p_in0->ltmLBFilter.th2 * k0 + p_in1->ltmLBFilter.th2 * k1;
    conf->ltmLBFilter.limit = p_in0->ltmLBFilter.limit * k0 + p_in1->ltmLBFilter.limit * k1;

    return 0;
}

/**
 *
 */
static int interpol_1D_ipipe_chroma_nf(ipipe_chroma_nf_t *p_in0, ipipe_chroma_nf_t *p_in1, float k0, float k1,
                                icIspConfig *conf)
{
    // Chroma Denoise
    conf->chromaDenoise.th_r = p_in0->chromaDenoise.th_r * k0 + p_in1->chromaDenoise.th_r * k1;
    conf->chromaDenoise.th_g = p_in0->chromaDenoise.th_g * k0 + p_in1->chromaDenoise.th_g * k1;
    conf->chromaDenoise.th_b = p_in0->chromaDenoise.th_b * k0 + p_in1->chromaDenoise.th_b * k1;
    conf->chromaDenoise.limit = p_in0->chromaDenoise.limit * k0 + p_in1->chromaDenoise.limit * k1;
    conf->chromaDenoise.hEnab = interp_hEnab(p_in0->chromaDenoise.hEnab,
                                             p_in1->chromaDenoise.hEnab, k1);

    // Low pass coefficients
    conf->lowpass.coefs[0] = p_in0->lowpass.coefs[0] * k0 + p_in1->lowpass.coefs[0] * k1;
    conf->lowpass.coefs[1] = p_in0->lowpass.coefs[1] * k0 + p_in1->lowpass.coefs[1] * k1;

    // Color combine
    conf->colorCombine.desat_mul = p_in0->colorCombine.desat_mul * k0 + p_in1->colorCombine.desat_mul * k1;
    conf->colorCombine.desat_t1  = p_in0->colorCombine.desat_t1 * k0 + p_in1->colorCombine.desat_t1 * k1;



    return 0;
}

/**
 *
 */
static int interpol_1D_ipipe_chroma_median(ipipe_chroma_median_t *p_in0, ipipe_chroma_median_t *p_in1, float k0, float k1,
                                icIspConfig *conf)
{
    conf->median.kernelSize = interp_median_ksize(p_in0->median.kernelSize,
                                                  p_in1->median.kernelSize, k1);
    conf->medianMix.slope = p_in0->medianMix.slope * k0 + p_in1->medianMix.slope * k1;
    conf->medianMix.offset = p_in0->medianMix.offset * k0 + p_in1->medianMix.offset * k1;

    return 0;
}

/**
 *
 */
static int interpol_1D_ipipe_shrapening(ipipe_shrapening_t *p_in0, ipipe_shrapening_t *p_in1, float k0, float k1,
                                icIspConfig *conf)
{
    conf->sharpen.sigma = p_in0->sigma * k0 + p_in1->sigma * k1;
    conf->sharpen.strength_darken = f32_to_f16(p_in0->strength_darken * k0 + p_in1->strength_darken * k1);
    conf->sharpen.strength_lighten = f32_to_f16(p_in0->strength_lighten * k0 + p_in1->strength_lighten * k1);
    conf->sharpen.alpha = f32_to_f16(p_in0->alpha * k0 + p_in1->alpha * k1);
    conf->sharpen.overshoot = f32_to_f16(p_in0->overshoot * k0 + p_in1->overshoot * k1);
    conf->sharpen.undershoot = f32_to_f16(p_in0->undershoot * k0 + p_in1->undershoot * k1);
    conf->sharpen.rangeStop0 = f32_to_f16(p_in0->rangeStop0 * k0 + p_in1->rangeStop0 * k1);
    conf->sharpen.rangeStop1 = f32_to_f16(p_in0->rangeStop1 * k0 + p_in1->rangeStop1 * k1);
    conf->sharpen.rangeStop2 = f32_to_f16(p_in0->rangeStop2 * k0 + p_in1->rangeStop2 * k1);
    conf->sharpen.rangeStop3 = f32_to_f16(p_in0->rangeStop3 * k0 + p_in1->rangeStop3 * k1);
    conf->sharpen.minThr = f32_to_f16(p_in0->minThr * k0 + p_in1->minThr * k1);

    return 0;
}

int generate_ipipe_params(ipipe_ctx_t *ctx,
                            icIspConfig *conf,
                            vpipe_ctrl_settings_t *vpipe_cfg,
                            hat_size_t *size,
                            int32 capture)
{
    dtp_out_data_t *p_dtp_out;
    dtpdb_dynamic_common_t dyn_comm;
    dtpdb_dynamic_private_t dyn_priv;

    osal_memset(&dyn_comm, 0x00, sizeof(dyn_comm));
    {
        // DPC settings
        ipipe_dpc_t *_p;
        osal_memset(&dyn_priv, 0x00, sizeof(dyn_priv));
        dyn_priv.params[0] = vpipe_cfg->dpc.data->dpc_strength * 1000;
        if (dtpsrv_process(ctx->hdtp_dpc, &dyn_comm,
                &dyn_priv, (void**)&p_dtp_out)) {
            goto exit1;
        }

        if (p_dtp_out->data_type_id == DTP_DAT_TYPE_FIXED) {
            _p = p_dtp_out->d_fixed.p_data0;

            conf->raw.dpcAlphaHotG      = _p->dpcAlphaHotG;
            conf->raw.dpcAlphaHotRb     = _p->dpcAlphaHotRb;
            conf->raw.dpcAlphaColdG     = _p->dpcAlphaColdG;
            conf->raw.dpcAlphaColdRb    = _p->dpcAlphaColdRb;
            conf->raw.dpcNoiseLevel     = _p->dpcNoiseLevel;
        } else if (p_dtp_out->data_type_id == DTP_DAT_TYPE_1D_INTERP) {
            {
                uint32 in;
                if(dtpsrv_get_dynamic_param_value(ctx->hdtp_dpc, p_dtp_out->d_1D_interp.param0_id,
                                                    &dyn_comm, &dyn_priv,
                                                    &in)) {  goto exit1; }
                {
                    float k0, k1;

                    dtpsrv_interp_1D_calc_k(in, p_dtp_out, &k0, &k1);
                    // 1D interpolation
                    interpol_1D_ipipe_dpc(p_dtp_out->d_1D_interp.p_data0, p_dtp_out->d_1D_interp.p_data1,
                                          k0, k1, conf);
                }
            }
        } else {
            mmsdbg(DL_ERROR, "data_type_id ipipe dpc - Not supported yet !!!");
            goto exit1;
        }
    }

    {
        // GRGB settings
        ipipe_grgb_t *_p;
        osal_memset(&dyn_priv, 0x00, sizeof(dyn_priv));
        dyn_priv.params[0] = vpipe_cfg->grgb.data->grgb_strength * 1000;
        if (dtpsrv_process(ctx->hdtp_grgb, &dyn_comm,
                &dyn_priv, (void**)&p_dtp_out)) {
            goto exit1;
        }

        if (p_dtp_out->data_type_id == DTP_DAT_TYPE_FIXED) {
            _p = p_dtp_out->d_fixed.p_data0;
            conf->raw.grgbImbalPlatDark     = _p->grgbImbalPlatDark;
            conf->raw.grgbImbalDecayDark    = _p->grgbImbalDecayDark;
            conf->raw.grgbImbalPlatBright   = _p->grgbImbalPlatBright;
            conf->raw.grgbImbalDecayBright  = _p->grgbImbalDecayBright;
            conf->raw.grgbImbalThr          = _p->grgbImbalThr;
        } else if (p_dtp_out->data_type_id == DTP_DAT_TYPE_1D_INTERP) {
            {
                uint32 in;
                if(dtpsrv_get_dynamic_param_value(ctx->hdtp_grgb, p_dtp_out->d_1D_interp.param0_id,
                                                    &dyn_comm, &dyn_priv,
                                                    &in)) {  goto exit1; }
                {
                    float k0, k1;

                    dtpsrv_interp_1D_calc_k(in, p_dtp_out, &k0, &k1);
                    // 1D interpolation
                    interpol_1D_ipipe_grgb(p_dtp_out->d_1D_interp.p_data0, p_dtp_out->d_1D_interp.p_data1,
                                          k0, k1, conf);
                }
            }
        } else {
            mmsdbg(DL_ERROR, "data_type_id ipipe grgb - Not supported yet !!!");
            goto exit1;
        }
    }

    {
        // Demosaic settings
        ipipe_demosaic_t *_p;
        osal_memset(&dyn_priv, 0x00, sizeof(dyn_priv));
        dyn_priv.params[0] = vpipe_cfg->cfai.data->filter_strength * 1000;
        dyn_priv.params[1] = vpipe_cfg->cfai.data->sharpness_strength * 1000;
        if (dtpsrv_process(ctx->hdtp_demosaic, &dyn_comm,
                &dyn_priv, (void**)&p_dtp_out)) {
            goto exit1;
        }

        if (p_dtp_out->data_type_id == DTP_DAT_TYPE_FIXED) {
            _p = p_dtp_out->d_fixed.p_data0;

            conf->demosaic.dewormGradientMul = _p->dewormGradientMul;
            conf->demosaic.dewormSlope       = _p->dewormSlope;
            conf->demosaic.dewormOffset      = _p->dewormOffset;
        } else if (p_dtp_out->data_type_id == DTP_DAT_TYPE_1D_INTERP) {
            {
                uint32 in;
                if(dtpsrv_get_dynamic_param_value(ctx->hdtp_demosaic, p_dtp_out->d_1D_interp.param0_id,
                                                    &dyn_comm, &dyn_priv,
                                                    &in)) {  goto exit1; }
                {
                    float k0, k1;

                    dtpsrv_interp_1D_calc_k(in, p_dtp_out, &k0, &k1);
                    // 1D interpolation
                    interpol_1D_ipipe_demosaic(p_dtp_out->d_1D_interp.p_data0, p_dtp_out->d_1D_interp.p_data1,
                                               k0, k1, conf);
                }
            }
        } else {
            mmsdbg(DL_ERROR, "data_type_id - Not supported yet !!!");
            goto exit1;
        }
    }

    {
        // Generate chroma  - depends on Chroma nf global_strength
        ipipe_gen_chroma_t *_p;
        osal_memset(&dyn_priv, 0x00, sizeof(dyn_priv));
        dyn_priv.params[0] = vpipe_cfg->cnf.data->global_strength * 1000;
        if (dtpsrv_process(ctx->hdtp_gen_chroma, &dyn_comm,
                &dyn_priv, (void**)&p_dtp_out)) {
            goto exit1;
        }

        if (p_dtp_out->data_type_id == DTP_DAT_TYPE_FIXED) {
            _p = p_dtp_out->d_fixed.p_data0;

            // Purple flare
            conf->purpleFlare.strength   = _p->purpleFlare.strength;

            // Chroma generation
            conf->chromaGen.epsilon      = _p->chromaGen.epsilon;
            conf->chromaGen.scaleR       = _p->chromaGen.scaleR;
            conf->chromaGen.scaleG       = _p->chromaGen.scaleG;
            conf->chromaGen.scaleB       = _p->chromaGen.scaleB;

            // Gray desaturation
            conf->greyDesat.slope        = _p->greyDesat.slope;
            conf->greyDesat.offset       = _p->greyDesat.offset;

        } else if (p_dtp_out->data_type_id == DTP_DAT_TYPE_1D_INTERP) {
            {
                uint32 in;
                if(dtpsrv_get_dynamic_param_value(ctx->hdtp_gen_chroma, p_dtp_out->d_1D_interp.param0_id,
                                                    &dyn_comm, &dyn_priv,
                                                    &in)) {  goto exit1; }
                {
                    float k0, k1;

                    dtpsrv_interp_1D_calc_k(in, p_dtp_out, &k0, &k1);
                    // 1D interpolation
                    interpol_1D_ipipe_gen_chroma(p_dtp_out->d_1D_interp.p_data0, p_dtp_out->d_1D_interp.p_data1,
                                      k0, k1, conf);
                }
            }
        } else {
            mmsdbg(DL_ERROR, "data_type_id - Not supported yet !!!");
            goto exit1;
        }
        conf->greyDesat.slope        = 1; //_p->greyDesat.slope;
        conf->greyDesat.offset       = 256;//_p->greyDesat.offset;
    }

    {
        // Luma denoise settings
        ipipe_luma_nf_t *_p;
        osal_memset(&dyn_priv, 0x00, sizeof(dyn_priv));
        dyn_priv.params[0] = vpipe_cfg->lnf.data->global_strength * 1000;
        if (dtpsrv_process(ctx->hdtp_lumma_nf, &dyn_comm,
                &dyn_priv, (void**)&p_dtp_out)) {
            goto exit1;
        }

        if (p_dtp_out->data_type_id == DTP_DAT_TYPE_FIXED) {
            _p = p_dtp_out->d_fixed.p_data0;

            /* Luma Denoise filter */
            conf->lumaDenoise.alpha = _p->lumaDenoise.alpha;
            conf->lumaDenoise.f2 = _p->lumaDenoise.f2;

            luma_denoise_gen_lut_bitpos(conf, _p->lumaDenoise.strength);

            /* For debug/reference only */
            conf->lumaDenoise.strength = _p->lumaDenoise.strength;

            /* Gen Denoise Ref filter */
            luma_denoise_ref_gen_lut_dist(conf,
              _p->lumaDenoiseRef.angle_of_view, size->w, size->h);

            luma_denoise_ref_gen_lut_gamma(conf, _p->lumaDenoiseRef.gamma);

            /* For debug/reference only */
            conf->lumaDenoiseRef.angle_of_view = _p->lumaDenoiseRef.angle_of_view;
            conf->lumaDenoiseRef.gamma = _p->lumaDenoiseRef.gamma;

            /* Add Random Noise filter */
            conf->randomNoise.strength = _p->randomNoise.strength;

        } else if (p_dtp_out->data_type_id == DTP_DAT_TYPE_1D_INTERP) {
            {
                uint32 in;
                if(dtpsrv_get_dynamic_param_value(ctx->hdtp_lumma_nf, p_dtp_out->d_1D_interp.param0_id,
                                                    &dyn_comm, &dyn_priv,
                                                    &in)) {  goto exit1; }
                {
                    float k0, k1;

                    dtpsrv_interp_1D_calc_k(in, p_dtp_out, &k0, &k1);
                    // 1D interpolation
                    interpol_1D_ipipe_luma_nf(p_dtp_out->d_1D_interp.p_data0, p_dtp_out->d_1D_interp.p_data1,
                                      k0, k1, size, conf);
                }
            }
        } else {
            mmsdbg(DL_ERROR, "data_type_id - Not supported yet !!!");
            goto exit1;
        }
    }

    {
        // Local Tone Mapping settings - depends on ltm_strength
        ipipe_ltm_t *_p;
        osal_memset(&dyn_priv, 0x00, sizeof(dyn_priv));
        dyn_priv.params[0] = vpipe_cfg->ltm.data->ltm_strength * 1000;
        if (dtpsrv_process(ctx->hdtp_ltm, &dyn_comm,
                &dyn_priv, (void**)&p_dtp_out)) {
            goto exit1;
        }

        if (p_dtp_out->data_type_id == DTP_DAT_TYPE_FIXED) {
            _p = p_dtp_out->d_fixed.p_data0;

            int i,j,x;

            for (i = 0, j = 0; i < 15*10; i += 10, j += 9) {
                for (x = 0; x < 9; x++) {
                    conf->ltm.curves[i+x] = f32_to_f16(_p->ltm.curves[j+x]);
                }
                conf->ltm.curves[i+9] = conf->ltm.curves[i+8];
            }
            for (x = 0; x < 10; x++) {
                conf->ltm.curves[10*15+x] = conf->ltm.curves[10*14+x];
            }

            conf->ltmLBFilter.th1   = _p->ltmLBFilter.th1;
            conf->ltmLBFilter.th2   = _p->ltmLBFilter.th2;
            conf->ltmLBFilter.limit = _p->ltmLBFilter.limit;

        } else if (p_dtp_out->data_type_id == DTP_DAT_TYPE_1D_INTERP) {
            {
                uint32 in;
                if(dtpsrv_get_dynamic_param_value(ctx->hdtp_ltm, p_dtp_out->d_1D_interp.param0_id,
                                                    &dyn_comm, &dyn_priv,
                                                    &in)) {  goto exit1; }
                {
                    float k0, k1;

                    dtpsrv_interp_1D_calc_k(in, p_dtp_out, &k0, &k1);
                    // 1D interpolation
                    interpol_1D_ipipe_ltm(p_dtp_out->d_1D_interp.p_data0, p_dtp_out->d_1D_interp.p_data1,
                                      k0, k1, conf);
                }
            }
        } else {
            mmsdbg(DL_ERROR, "data_type_id - Not supported yet !!!");
            goto exit1;
        }
    }

    {
        // Chroma nf settings
        ipipe_chroma_nf_t *_p;
        osal_memset(&dyn_priv, 0x00, sizeof(dyn_priv));
        dyn_priv.params[0] = vpipe_cfg->cnf.data->global_strength * 1000;
        dyn_priv.params[1] = vpipe_cfg->cnf.data->radial_strength * 1000;
        if (dtpsrv_process(ctx->hdtp_chroma_nf, &dyn_comm,
                &dyn_priv, (void**)&p_dtp_out)) {
            goto exit1;
        }

        if (p_dtp_out->data_type_id == DTP_DAT_TYPE_FIXED) {
            _p = p_dtp_out->d_fixed.p_data0;

            conf->chromaDenoise.th_r = _p->chromaDenoise.th_r;
            conf->chromaDenoise.th_g = _p->chromaDenoise.th_g;
            conf->chromaDenoise.th_b = _p->chromaDenoise.th_b;
            conf->chromaDenoise.limit = _p->chromaDenoise.limit;
            conf->chromaDenoise.hEnab = _p->chromaDenoise.hEnab;

            // Low pass coefficients
            conf->lowpass.coefs[0] = _p->lowpass.coefs[0];
            conf->lowpass.coefs[1] = _p->lowpass.coefs[1];

            // Color combine
            conf->colorCombine.desat_mul = _p->colorCombine.desat_mul;
            conf->colorCombine.desat_t1  = _p->colorCombine.desat_t1;

        } else if (p_dtp_out->data_type_id == DTP_DAT_TYPE_1D_INTERP) {
            {
                uint32 in;
                if(dtpsrv_get_dynamic_param_value(ctx->hdtp_chroma_nf, p_dtp_out->d_1D_interp.param0_id,
                                                    &dyn_comm, &dyn_priv,
                                                    &in)) {  goto exit1; }
                {
                    float k0, k1;

                    dtpsrv_interp_1D_calc_k(in, p_dtp_out, &k0, &k1);
                    // 1D interpolation
                    interpol_1D_ipipe_chroma_nf(p_dtp_out->d_1D_interp.p_data0, p_dtp_out->d_1D_interp.p_data1,
                                      k0, k1, conf);
                }
            }
        } else {
            mmsdbg(DL_ERROR, "data_type_id - Not supported yet !!!");
            goto exit1;
        }
    }

    {
        // Chroma median settings
        ipipe_chroma_median_t *_p;
        osal_memset(&dyn_priv, 0x00, sizeof(dyn_priv));
        // TODO:
        dyn_priv.params[0] = vpipe_cfg->cnf.data->global_strength * 1000;
        dyn_priv.params[1] = vpipe_cfg->cnf.data->radial_strength * 1000;
        if (dtpsrv_process(ctx->hdtp_chroma_median, &dyn_comm,
                &dyn_priv, (void**)&p_dtp_out)) {
            goto exit1;
        }

        if (p_dtp_out->data_type_id == DTP_DAT_TYPE_FIXED) {
            _p = p_dtp_out->d_fixed.p_data0;

            conf->median.kernelSize = _p->median.kernelSize;
            conf->medianMix.slope = _p->medianMix.slope;
            conf->medianMix.offset = _p->medianMix.offset;
        } else if (p_dtp_out->data_type_id == DTP_DAT_TYPE_1D_INTERP) {
            {
                uint32 in;
                if(dtpsrv_get_dynamic_param_value(ctx->hdtp_chroma_median, p_dtp_out->d_1D_interp.param0_id,
                                                    &dyn_comm, &dyn_priv,
                                                    &in)) {  goto exit1; }
                {
                    float k0, k1;

                    dtpsrv_interp_1D_calc_k(in, p_dtp_out, &k0, &k1);
                    // 1D interpolation
                    interpol_1D_ipipe_chroma_median(p_dtp_out->d_1D_interp.p_data0, p_dtp_out->d_1D_interp.p_data1,
                                      k0, k1, conf);
                }
            }
        } else {
            mmsdbg(DL_ERROR, "data_type_id - Not supported yet !!!");
            goto exit1;
        }
    }

    {
        // Sharpening settings
        ipipe_shrapening_t *_p;
        osal_memset(&dyn_priv, 0x00, sizeof(dyn_priv));
        dyn_priv.params[0] = vpipe_cfg->sharpening.data->global_strength * 1000;
        dyn_priv.params[1] = vpipe_cfg->sharpening.data->radial_strength * 1000;
        if (dtpsrv_process(ctx->hdtp_sharpening, &dyn_comm,
                &dyn_priv, (void**)&p_dtp_out)) {
            goto exit1;
        }

        if (p_dtp_out->data_type_id == DTP_DAT_TYPE_FIXED) {
            _p = p_dtp_out->d_fixed.p_data0;

            conf->sharpen.sigma = _p->sigma;
            sharpen_gen_coeffs(conf->sharpen.sigma,
                                conf->sharpen.sharpen_coeffs);
            conf->sharpen.strength_darken = f32_to_f16(_p->strength_darken);
            conf->sharpen.strength_lighten = f32_to_f16(_p->strength_lighten);
            conf->sharpen.alpha = f32_to_f16(_p->alpha);
            conf->sharpen.overshoot = f32_to_f16(_p->overshoot);
            conf->sharpen.undershoot = f32_to_f16(_p->undershoot);
            conf->sharpen.rangeStop0 = f32_to_f16(_p->rangeStop0);
            conf->sharpen.rangeStop1 = f32_to_f16(_p->rangeStop1);
            conf->sharpen.rangeStop2 = f32_to_f16(_p->rangeStop2);
            conf->sharpen.rangeStop3 = f32_to_f16(_p->rangeStop3);
            conf->sharpen.minThr = f32_to_f16(_p->minThr);
        } else if (p_dtp_out->data_type_id == DTP_DAT_TYPE_1D_INTERP) {
            {
                uint32 in;
                if(dtpsrv_get_dynamic_param_value(ctx->hdtp_sharpening, p_dtp_out->d_1D_interp.param0_id,
                                                    &dyn_comm, &dyn_priv,
                                                    &in)) {  goto exit1; }
                {
                    float k0, k1;

                    dtpsrv_interp_1D_calc_k(in, p_dtp_out, &k0, &k1);
                    // 1D interpolation
                    interpol_1D_ipipe_shrapening(p_dtp_out->d_1D_interp.p_data0, p_dtp_out->d_1D_interp.p_data1,
                                      k0, k1, conf);
                    sharpen_gen_coeffs(conf->sharpen.sigma,
                                        conf->sharpen.sharpen_coeffs);
                }
            }
        } else {
            mmsdbg(DL_ERROR, "data_type_id - Not supported yet !!!");
            goto exit1;
        }
    }

    {
        // Focus filters settings
        // ONLY FIXED, FIXED 1D, FIXED 2D is used
        ipipe_focus_filters_t *_p;
        osal_memset(&dyn_priv, 0x00, sizeof(dyn_priv));
        // TODO:
        dyn_priv.params[0] = vpipe_cfg->h3a_af.data->filter_strong.filter_strength * 1000;
        dyn_priv.params[1] = vpipe_cfg->h3a_af.data->filter_weak.filter_strength * 1000;
        if (dtpsrv_process(ctx->hdtp_foc_filters, &dyn_comm,
                &dyn_priv, (void**)&p_dtp_out)) {
            goto exit1;
        }

        if (p_dtp_out->data_type_id == DTP_DAT_TYPE_FIXED) {
            int32 i;
            _p = p_dtp_out->d_fixed.p_data0;

            conf->afConfig.f1Threshold = _p->f1Threshold;
            conf->afConfig.f2Threshold = _p->f2Threshold;

            for (i = 0; i < 11; i++) {
                conf->afConfig.f1Coeffs[i] = _p->f1Coeffs[i];
                conf->afConfig.f2Coeffs[i] = _p->f2Coeffs[i];
            }

        } else {
            mmsdbg(DL_ERROR, "data_type_id - Not supported yet !!!");
            goto exit1;
        }
    }

    return 0;
exit1:
    return -1;
}

int vpipe_convert_to_ipipe(ipipe_conv_hndl_t hndl,
                                hat_pix_fmt_t fmt,
                                hat_size_t *in_size,
                                vpipe_ctrl_settings_t *vpipe_cfg,
                                hat_dgain_t *post_gain,
                                icIspConfig *conf,
                                int capture,
                                void *cfg_private)
{
    PROFILE_ADD(PROFILE_ID_DTP_IPIPE_CONVERT, 0xBBBB, 0);
/* TODO */
/* DD: current gains - harcoded for now - needs to come from the framework! */
/*
 * DD: Need to know the frame width/height to compute some parameters -
 * e.g. the distance-based LUT for the genDenoiseRef filter.  Hard-coded
 * for now!
 */
#define IMAGE_WIDTH     4208
#define IMAGE_HEIGHT    3120
    ipipe_ctx_t     *ctx = hndl;
    int             width =  IMAGE_WIDTH;
    int             height = IMAGE_HEIGHT;
    ipipe_params    p;

    if (capture) {
        conf->enableFlags = DEF_CAPT_ISP_ENABLE_FLAGS;
        conf->dirtyFlags = DEF_CAPT_ISP_GROUP_FLAGS;
    } else {
        conf->enableFlags = DEF_PRV_ISP_ENABLE_FLAGS;
        conf->dirtyFlags = DEF_PRV_ISP_GROUP_FLAGS;
        width >>= 1;
        height >>= 1;
    }

    /* Generic parameter sets where everything is pretty standard */
    vpipe_conv_dcsub(conf, vpipe_cfg->dcsub, 1023);
    vpipe_conv_wb(conf, vpipe_cfg->wb);
    vpipe_conv_rgb2rgb(conf, vpipe_cfg->rgb2rgb);
    vpipe_conv_gamma(conf, vpipe_cfg->gamma);
    vpipe_conv_lsc(fmt, conf, vpipe_cfg->lsc);
    vpipe_conv_rgb2yuv(conf, &p, vpipe_cfg->rgb2yuv);

    /* Stats configuration */
    vpipe_conv_h3a_aewb(conf, vpipe_cfg->h3a_aewb, in_size);
    vpipe_conv_h3a_af(conf, vpipe_cfg->h3a_af, in_size);
    conf->env.exp = vpipe_cfg->env.exp;
    conf->env.gain = vpipe_cfg->env.gain;
    conf->env.colour_temp = vpipe_cfg->env.colour_temp;

    generate_ipipe_params(ctx, conf, vpipe_cfg, in_size, capture);

    // TODO: Use these parameters from DCC - not implemented yet
    conf->greyDesat.grey_cr = 255/3;
    conf->greyDesat.grey_cg = 255/3;
    conf->greyDesat.grey_cb = 255/3;

    switch (fmt.fmt.order.all)
    {
    case HAT_PORDBYR_Gr_R_B_Gb:
        conf->bayerOrder   = IC_BAYER_FORMAT_GRBG;
        break;
    case HAT_PORDBYR_R_Gr_Gb_B:
        conf->bayerOrder   = IC_BAYER_FORMAT_RGGB;
        break;
    case HAT_PORDBYR_Gb_B_R_Gr:
        conf->bayerOrder   = IC_BAYER_FORMAT_GBRG;
        break;
    default:
        mmsdbg(DL_ERROR, "Unsupported color pattern!" );
        /* no break */
    case HAT_PORDBYR_B_Gb_Gr_R:
        conf->bayerOrder   = IC_BAYER_FORMAT_BGGR;
        break;
    }

    PROFILE_ADD(PROFILE_ID_DTP_IPIPE_CONVERT, 0xEEEE, 0);
    return 0;
}

int vpipe_conv_config(ipipe_conv_hndl_t hndl,
                        void *static_common, void *static_private)
{
    int ret = 0;
    ipipe_ctx_t *ctx = hndl;

    ret += dtpsrv_pre_process(ctx->hdtp_dpc, static_common, static_private);
    ret += dtpsrv_pre_process(ctx->hdtp_grgb, static_common, static_private);
    ret += dtpsrv_pre_process(ctx->hdtp_demosaic, static_common, static_private);
    ret += dtpsrv_pre_process(ctx->hdtp_gen_chroma, static_common, static_private);
    ret += dtpsrv_pre_process(ctx->hdtp_lumma_nf, static_common, static_private);
    ret += dtpsrv_pre_process(ctx->hdtp_ltm, static_common, static_private);
    ret += dtpsrv_pre_process(ctx->hdtp_chroma_median, static_common, static_private);
    ret += dtpsrv_pre_process(ctx->hdtp_chroma_nf, static_common, static_private);
    ret += dtpsrv_pre_process(ctx->hdtp_sharpening, static_common, static_private);
    ret += dtpsrv_pre_process(ctx->hdtp_foc_filters, static_common, static_private);

    mmsdbg(DL_FUNC, "Exit %s", ret ? "Error" : "Ok");
    return ret;
}

void vpipe_conv_destroy(ipipe_conv_hndl_t hndl)
{
    ipipe_ctx_t *ctx = hndl;

    if(ctx->hdtp_dpc) {
        dtpsrv_free_hndl(ctx->hdtp_dpc);
    }

    if(ctx->hdtp_grgb) {
        dtpsrv_free_hndl(ctx->hdtp_grgb);
    }

    if(ctx->hdtp_demosaic) {
        dtpsrv_free_hndl(ctx->hdtp_demosaic);
    }

    if(ctx->hdtp_gen_chroma) {
        dtpsrv_free_hndl(ctx->hdtp_gen_chroma);
    }

    if(ctx->hdtp_lumma_nf) {
        dtpsrv_free_hndl(ctx->hdtp_lumma_nf);
    }

    if(ctx->hdtp_ltm) {
        dtpsrv_free_hndl(ctx->hdtp_ltm);
    }

    if(ctx->hdtp_chroma_median) {
        dtpsrv_free_hndl(ctx->hdtp_chroma_median);
    }

    if(ctx->hdtp_chroma_nf) {
        dtpsrv_free_hndl(ctx->hdtp_chroma_nf);
    }

    if(ctx->hdtp_sharpening) {
        dtpsrv_free_hndl(ctx->hdtp_sharpening);
    }

    if(ctx->hdtp_foc_filters) {
        dtpsrv_free_hndl(ctx->hdtp_foc_filters);
    }

    osal_free(hndl);
}

int vpipe_conv_create(ipipe_conv_hndl_t *hndl)
{
    ipipe_ctx_t *ctx;
    ctx = osal_calloc(1, sizeof(*ctx));

    if (!ctx) {
        mmsdbg(DL_ERROR, "Can't allocate memory");
        goto EXIT_1;
    }

    *hndl = ctx;
    ctx->name = "IpipeConvert";

    if (dtpsrv_get_hndl(dtp_srv_hndl,
                        DTP_DB_ID_IPIPE,
                        DTP_ID_IPIPE_DPC,
                        1,
                        dtp_param_order,
                        &ctx->hdtp_dpc, &ctx->leaf_dpc))
    {
        mmsdbg(DL_ERROR, "IPIPE_DPC - can't get DTP client handle");
        goto EXIT_2;
    }

    if (dtpsrv_get_hndl(dtp_srv_hndl,
                        DTP_DB_ID_IPIPE,
                        DTP_ID_IPIPE_GRGB,
                        1,
                        dtp_param_order,
                        &ctx->hdtp_grgb, &ctx->leaf_grgb))
    {
        mmsdbg(DL_ERROR, "IPIPE_GRGB - can't get DTP client handle");
        goto EXIT_2;
    }

    if (dtpsrv_get_hndl(dtp_srv_hndl,
                        DTP_DB_ID_IPIPE,
                        DTP_ID_IPIPE_DEMOSAIC,
                        1,
                        dtp_param_order,
                        &ctx->hdtp_demosaic, &ctx->leaf_demosaic))
    {
        mmsdbg(DL_ERROR, "IPIPE_DEMOSAIC - can't get DTP client handle");
        goto EXIT_2;
    }

    if (dtpsrv_get_hndl(dtp_srv_hndl,
                        DTP_DB_ID_IPIPE,
                        DTP_ID_IPIPE_GEN_CHROMA,
                        1,
                        dtp_param_order,
                        &ctx->hdtp_gen_chroma, &ctx->leaf_gen_chroma))
    {
        mmsdbg(DL_ERROR, "IPIPE_GEN_CHROMA - can't get DTP client handle");
        goto EXIT_2;
    }

    if (dtpsrv_get_hndl(dtp_srv_hndl,
                        DTP_DB_ID_IPIPE,
                        DTP_ID_IPIPE_LUMMA_NF,
                        1,
                        dtp_param_order,
                        &ctx->hdtp_lumma_nf, &ctx->leaf_lumma_nf))
    {
        mmsdbg(DL_ERROR, "IPIPE_LUMMA_NF - can't get DTP client handle");
        goto EXIT_2;
    }

    if (dtpsrv_get_hndl(dtp_srv_hndl,
                        DTP_DB_ID_IPIPE,
                        DTP_ID_IPIPE_LTM,
                        2,                  /* Ask MA2150 and use fixed LTM for MA2100 */
                        dtp_param_order,
                        &ctx->hdtp_ltm, &ctx->leaf_ltm))
    {
        mmsdbg(DL_ERROR, "IPIPE_LTM - can't get DTP client handle");
        goto EXIT_2;
    }

    if (dtpsrv_get_hndl(dtp_srv_hndl,
                        DTP_DB_ID_IPIPE,
                        DTP_ID_IPIPE_CHROMA_MEDIAN,
                        1,
                        dtp_param_order,
                        &ctx->hdtp_chroma_median, &ctx->leaf_chroma_median))
    {
        mmsdbg(DL_ERROR, "IPIPE_CHROMA_MEDIAN - can't get DTP client handle");
        goto EXIT_2;
    }

    if (dtpsrv_get_hndl(dtp_srv_hndl,
                        DTP_DB_ID_IPIPE,
                        DTP_ID_IPIPE_CHROMA_NF,
                        1,
                        dtp_param_order,
                        &ctx->hdtp_chroma_nf, &ctx->leaf_chroma_nf))
    {
        mmsdbg(DL_ERROR, "IPIPE_CHROMA_NF - can't get DTP client handle");
        goto EXIT_2;
    }

    if (dtpsrv_get_hndl(dtp_srv_hndl,
                        DTP_DB_ID_IPIPE,
                        DTP_ID_IPIPE_SHARPENING,
                        1,
                        dtp_param_order,
                        &ctx->hdtp_sharpening, &ctx->leaf_sharpening))
    {
        mmsdbg(DL_ERROR, "IPIPE_SHARPENING - can't get DTP client handle");
        goto EXIT_2;
    }

    if (dtpsrv_get_hndl(dtp_srv_hndl,
                        DTP_DB_ID_IPIPE,
                        DTP_ID_IPIPE_FOCUS_FILTERS,
                        1,
                        dtp_param_order,
                        &ctx->hdtp_foc_filters, &ctx->leaf_foc_filters))
    {
        mmsdbg(DL_ERROR, "DTP_ID_IPIPE_FOCUS_FILTERS - can't get DTP client handle");
        goto EXIT_2;
    }

    return 0;

EXIT_2:
    vpipe_conv_destroy(*hndl);
EXIT_1:
    return -1;

}
