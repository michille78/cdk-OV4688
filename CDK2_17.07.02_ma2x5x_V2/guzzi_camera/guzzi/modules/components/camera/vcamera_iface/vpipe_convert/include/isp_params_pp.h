// GENERATED TEST FILE (OCTAVE dump) !

#define ISPC_INPUT_IMAGE	"General_Scene_300lux_150msec_gain1.raw"

//Input image dimensions:
#define ISPC_BAYER_W	2104
#define ISPC_BAYER_H	1560
#define ISPC_BAYER_BITS	10

#define ISPC_BLACK_LEVEL_GR	64
#define ISPC_BLACK_LEVEL_R 	64
#define ISPC_BLACK_LEVEL_B 	64
#define ISPC_BLACK_LEVEL_GB	64

//gains are 8.8 fixed point format
#define ISPC_LSC_GAIN_MAP_W	60
#define ISPC_LSC_GAIN_MAP_H	44

extern uint16_t ispcLscMesh[ISPC_LSC_GAIN_MAP_W * ISPC_LSC_GAIN_MAP_H];

// Bad pixel suppression
#define ISPC_BAD_PIX_ALPHA_G_HOT	0x6	// 4 bits
#define ISPC_BAD_PIX_ALPHA_RB_HOT	0x6	// 4 bits
#define ISPC_BAD_PIX_ALPHA_G_COLD	0x6	// 4 bits
#define ISPC_BAD_PIX_ALPHA_RB_COLD	0x6	// 4 bits
#define ISPC_BAD_PIX_NOISE_LEVEL	0x0000	//16 bits

// GR-GB imbalance filter
#define ISPC_GRGB_IMBAL_PLAT_DARK	  20
#define ISPC_GRGB_IMBAL_DECAY_DARK	  25
#define ISPC_GRGB_IMBAL_PLAT_BRIGHT	 100
#define ISPC_GRGB_IMBAL_DECAY_BRIGHT	 120
#define ISPC_GRGB_IMBAL_THRESHOLD	  16	// 8 bits

#define ISPC_RAW_GAIN_GR	273	// 16 bits
#define ISPC_RAW_GAIN_G 	273	// 16 bits
#define ISPC_RAW_GAIN_B 	273	// 16 bits
#define ISPC_RAW_GAIN_GB	273	// 16 bits

#define ISPC_RAW_CLAMP_GR	1023	// 16 bits
#define ISPC_RAW_CLAMP_R 	1023	// 16 bits
#define ISPC_RAW_CLAMP_B 	1023	// 16 bits
#define ISPC_RAW_CLAMP_GB	1023	// 16 bits

#define ISPC_RAW_OUTPUT_BITS	10

// AWB digital gains are 8.8 fixed-point format
#define ISPC_GAIN_R	0x01de	// 8.8 bits
#define ISPC_GAIN_G	0x0100	// 8.8 bits
#define ISPC_GAIN_B	0x016e	// 8.8 bits

