#if !defined(__sparc)

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

#include "ipipe_params.h"

#define INI_TYPE_INT		1
#define INI_TYPE_STRING		2
#define INI_TYPE_FLOAT		3

#ifdef _WIN32
#define STRDUP	_strdup
#define MATCH(a, b)    (!_stricmp((a), (b)))
#else
#define STRDUP	strdup
#define MATCH(a, b)    (!strcasecmp((a), (b)))
#endif

typedef int (*ini_callback_t)(const char *section, const char *name, const void *val, int size, int type);

/* Warning - the config file parser is *not* re-entrant! */
static ipipe_params *g_params;
static char         *g_section_name;
static int          g_lno;

/* Eat leading whitespace */
static char * eat_ws(char *c)
{
	int i, l = strlen(c);

	for (i = 0; i < l; i++) {
		if (*c != '\t' && *c != ' ') {
			break;
		}
		c++;
	}

	return c;
}

static int parse_g_section_name(char *c)
{
	c++;	/* Skip '[' */
	c = eat_ws(c);

	if (g_section_name != NULL) {
		free(g_section_name);
	}
	g_section_name = STRDUP(c);
	if (g_section_name == NULL) {
		perror("strdup");
		abort();
	}
	c = g_section_name;
	while (*c != ']' && *c != '\0') {
		c++;
	}

	if (c == '\0') {
		return -1;
	}

	while ((*c == ' ' || *c == '\t') && c != g_section_name) {
		c--;
	}
	*c = '\0';

	return 0;
}

/* Supress trailing whitespace */
static void clobber_ws_tr(char *ins)
{
	char	*c;

	c = ins + strlen(ins) - 1;
	while (c != ins) {
		if (*c == '\n' || *c == '\r' || *c == ' ' || *c == '\t') {
			*c = '\0';
			c--;
		} else {
			break;
		}
	}
}

static int cb(ini_callback_t callback, char *section, char *name, void *val, int size, int type)
{
	clobber_ws_tr(section);
	clobber_ws_tr(name);
	if (type == INI_TYPE_STRING) {
		clobber_ws_tr((char *)val);
	}

	return callback(section, name, val, size, type);
}

static int parse_scalar(char *section,
  char *name, char *val, ini_callback_t callback)
{
	char	*c, *r;
	float	f;
	int	i;

	if (isdigit(*val) || *val == '-' || *val == '.') {
		c = val;
		while (*c != '\0' && (isdigit(*c) || *c == '-')) {
			c++;
		}
		if (*c == '.') {
			f = strtod(val, &r);
			if (r == val) {
				fprintf(stderr,
				    "Couldn't parse floating point value\n");
				return -1;
			}
			cb(callback, section, name, &f, 1, INI_TYPE_FLOAT);
		} else {
			if (val[0] == '0' && val[1] == 'x') {
				i = strtoul(val, &r, 0);
			} else {
				i = strtol(val, &r, 0);
			}
			if (r == val) {
				fprintf(stderr,
				    "Couldn't parse integer value\n");
				return -1;
			}
			cb(callback, section, name, &i, 1, INI_TYPE_INT);
		}
	} else {
		cb(callback, section, name, val, strlen(val), INI_TYPE_STRING);
	}

	return 0;
}

static int parse_array(FILE *fi,
  char *g_section_name, char *name, ini_callback_t callback)
{
	char	*c, *c2;
	int     arraysize = 0, i, *iptr = NULL;
	int	    isfloat = 0, rc;
	char	buf[1024];
	char	*r;
	float	f, *fptr = NULL;
	long	fpos;

	while (fpos = ftell(fi), fgets(buf, sizeof (buf), fi) != NULL) {
		g_lno++;
		buf[sizeof (buf) - 1] = '\0';
		c = buf;
		c = eat_ws(c);
		if (*c == ';') {
			/* Skip comment line */
			continue;
		}
		if (!isdigit(*c) && *c != '.' && *c != '-') {
			/*
			 * This line is not a continuation of the array...
			 * undo last fgets.
			 */
			fseek(fi, fpos, SEEK_SET);
			g_lno--;
			break;
		}
		/*
		 * An array must be either all floats, or all ints.  For an
		 * array of floats, all values *must* contain a decimal point.
		 * Look at the first value to determine if it's an array
		 * of floats or ints.
		 */
		if (arraysize == 0) {
			c2 = c;
			while (isdigit(*c2) || *c2 == '-') {
				c2++;
			}
			if (*c2 == '.') {
				isfloat = 1;
			}
		}

		while (1) {
			c = eat_ws(c);
			if (!isdigit(*c) && *c != '.') {
				if (*c == '\0' || *c == '\r' || *c == '\n') {
					break;
				}
			}

			if (isfloat) {
				f = strtod(c, &r);
				if (r == c) {
					fprintf(stderr,
					    "Couldn't parse floating point value\n");
					return -1;
				}
				fptr = (float *)realloc(fptr,
				    (arraysize+1) * sizeof (float));
				if (fptr == NULL) {
					perror("realloc");
					abort();
				}
				fptr[arraysize] = f;
			} else {
				if (c[0] == '0' && c[1] == 'x') {
					i = strtoul(c, &r, 0);
				} else {
					i = strtol(c, &r, 0);
				}
				if (r == c) {
					fprintf(stderr,
					    "Couldn't parse integer value\n");
					return -1;
				}
				iptr = (int *)realloc(iptr,
				    (arraysize+1) * sizeof (int));
				if (iptr == NULL) {
					perror("realloc");
					abort();
				}
				iptr[arraysize] = i;
			}

			c = r;

			arraysize++;
		}
	}

	if (arraysize == 0) {
		fprintf(stderr, "Expecting array values\n");
		return -1;
	}

	if (isfloat) {
		rc = cb(callback, g_section_name, name, fptr, arraysize, INI_TYPE_FLOAT);
		free(fptr);
	} else {
		rc = cb(callback, g_section_name, name, iptr, arraysize, INI_TYPE_INT);
		free(iptr);
	}

	return rc;
}

static int parse_value(FILE *f, char *c, ini_callback_t callback)
{
	char	*c2, *name = c;

	while (*c != '\n' && *c != '\r' && *c != '\0' && *c != '=') {
		c++;
	}

	if (*c != '=') {
		fprintf(stderr, "Expected \"=\" in value assignment\n");
		return -1;
	}

	c2 = c-1;
	while ((*c2 == '\t' || *c2 == ' ') && c2 != name) {
		c2--;
	}
	c2[1] = '\0';

	/* Scalar value (on same line) or array (on following line(s))? */
	c2 = c = eat_ws(c+1);
	while (*c != '\0' && *c != '\n' && *c != '\r') {
		if (*c == '.' || *c == '_' || isalnum(*c)) {
			return parse_scalar(g_section_name, name, c2, callback);
		}
		c++;
	}

	return parse_array(f, g_section_name, name, callback);
}

static int parse_line(FILE *f, char *c, ini_callback_t callback)
{
	int	i, l = strlen(c);

	for (i = 0; i < l; i++) {
		if (c[i] == ';') {
			c[i] = '\0';	/* Remove comment */
		}
	}

	c = eat_ws(c);

	if (*c == '\n' || *c == '\r' || *c == '\0') {
		/* Comment or blank line */
		return 0;
	}

	if (*c == '[') {
		if (parse_g_section_name(c) == -1) {
			return -1;
		}
		return 0;
	}

	if (g_section_name == NULL) {
		fprintf(stderr, "Expecting section name\n");
		return -1;
	}

	if (parse_value(f, c, callback) == -1) {
		return -1;
	}

	return 0;
}

static int callback(const char *section,
  const char *name, const void *val, int size, int type)
{
    int    error = 0, i;

    switch (type) {
    case INI_TYPE_INT:
        i = *(int *)val;
        if (MATCH(section, "general")) {
            /* Ignore section */
        } else if (MATCH(section, "lsc")) {
            /* Ignore section */
        } else if (MATCH(section, "WB")) {
            /* Ignore section */
        } else if (MATCH(section, "AWB Stats")) {
            /* Ignore section */
        } else if (MATCH(section, "wbGains")) {
            /* Ignore section */
        } else if (MATCH(section, "raw")) {
            if (MATCH(name, "output_bits")) {
                /* Ignore parameter */
            } else if (MATCH(name, "gain_gr")) {
                /* Ignore parameter */
            } else if (MATCH(name, "gain_r")) {
                /* Ignore parameter */
            } else if (MATCH(name, "gain_b")) {
                /* Ignore parameter */
            } else if (MATCH(name, "gain_gb")) {
                /* Ignore parameter */
            } else if (MATCH(name, "sat_gr")) {
                /* Ignore parameter */
            } else if (MATCH(name, "sat_r")) {
                /* Ignore parameter */
            } else if (MATCH(name, "sat_b")) {
                /* Ignore parameter */
            } else if (MATCH(name, "sat_gb")) {
                /* Ignore parameter */
            } else if (MATCH(name, "dpc_alpha_g_hot")) {
                g_params->raw.dpcAlphaHotG = i;
            } else if (MATCH(name, "dpc_alpha_rb_hot")) {
                g_params->raw.dpcAlphaHotRb = i;
            } else if (MATCH(name, "dpc_alpha_g_cold")) {
                g_params->raw.dpcAlphaColdG = i;
            } else if (MATCH(name, "dpc_alpha_rb_cold")) {
                g_params->raw.dpcAlphaColdRb = i;
            } else if (MATCH(name, "dpc_noise_level")) {
                g_params->raw.dpcNoiseLevel = i;
            } else if (MATCH(name, "grgb_imbal_plat_dark")) {
                g_params->raw.grgbImbalPlatDark = i;
            } else if (MATCH(name, "grgb_imbal_decay_dark")) {
                g_params->raw.grgbImbalDecayDark = i;
            } else if (MATCH(name, "grgb_imbal_plat_bright")) {
                g_params->raw.grgbImbalPlatBright = i;
            } else if (MATCH(name, "grgb_imbal_decay_bright")) {
                g_params->raw.grgbImbalDecayBright = i;
            } else if (MATCH(name, "grgb_imbal_threshold")) {
                g_params->raw.grgbImbalThr = i;
            } else {
                error = 1;
            }
        } else if (MATCH(section, "demosaic")) {
            if (MATCH(name, "mix_slope")) {
                g_params->demosaic.dewormSlope = i;
            } else if (MATCH(name, "mix_offset")) {
                g_params->demosaic.dewormOffset = i;
            } else if (MATCH(name, "mix_gradient_mul")) {
                g_params->demosaic.dewormGradientMul = i;
            } else {
                error = 1;
            }
        } else if (MATCH(section, "Purple Flare")) {
            if (MATCH(name, "strength")) {
                g_params->purpleFlare.strength = i;
            } else {
                error = 1;
            }
        } else if (MATCH(section, "Chroma Gen")) {
            // Patch to match the simulator
            if (MATCH(name, "epsilon")) {
                g_params->chromaGen.epsilon = (float)i;
            } else {
                error = 1;
            }
        } else if (MATCH(section, "median")) {
            if (MATCH(name, "kernel_size")) {
                g_params->median.kernelSize = i;
            } else {
                error = 1;
            }
        } else if (MATCH(section, "Luma Denoise")) {
            if (MATCH(name, "alpha")) {
                g_params->lumaDenoise.alpha = i;
            } else if (MATCH(name, "f2")) {
                g_params->lumaDenoise.f2 = i;
            } else {
                error = 1;
            }
        } else if (MATCH(section, "Chroma Denoise")) {
            if (MATCH(name, "th_r")) {
                g_params->chromaDenoise.th_r = i;
            } else if (MATCH(name, "th_g")) {
                g_params->chromaDenoise.th_g = i;
            } else if (MATCH(name, "th_b")) {
                g_params->chromaDenoise.th_b = i;
            } else if (MATCH(name, "limit")) {
                g_params->chromaDenoise.limit = i;
            } else if (MATCH(name, "h_enab")) {
                g_params->chromaDenoise.hEnab = i;
            } else {
                error = 1;
            }
        } else if (MATCH(section, "Filter Ltm")) {
            if (MATCH(name, "th1")) {
                g_params->ltmLBFilter.th1 = i;
            } else if (MATCH(name, "th2")) {
                g_params->ltmLBFilter.th2 = i;
            } else if (MATCH(name, "limit")) {
                g_params->ltmLBFilter.limit = i;
            } else {
                error = 1;
            }
        } else if (MATCH(section, "Grey Desaturate")) {
            if (MATCH(name, "offset")) {
                g_params->greyDesat.offset = i;
            } else if (MATCH(name, "slope")) {
                g_params->greyDesat.slope = i;
            } else {
                error = 1;
            }
        } else if (MATCH(section, "Color Combine")) {
        	// Patch to match the simulator
        	if (MATCH(name, "desat_t1")) {
                g_params->colorCombine.desat_t1 = (float)i/255.0;
            } else if (MATCH(name, "desat_mul")) {
                g_params->colorCombine.desat_mul = (float)i;
            } else {
                error = 1;
            }
        } else if (MATCH(section, "Gamma")) {
            /* Ignore section */
        } else {
            error = 1;
        }
        break;

    case INI_TYPE_FLOAT: {
        float    f = *(float *)val;

        if (MATCH(section, "Mipi RX")) {
            /* Ignore section */
        } else if (MATCH(section, "Chroma Gen")) {
            if (MATCH(name, "scale")) {
                if (size != 3) {
                    fprintf(stderr, "\"scale\" should be a 3-value array)\n");
                    error = 1;
                } else {
                    g_params->chromaGen.scaleR = ((float *)val)[0];
                    g_params->chromaGen.scaleG = ((float *)val)[1];
                    g_params->chromaGen.scaleB = ((float *)val)[2];
                }
            } else {
                error = 1;
            }
        } else if (MATCH(section, "Luma Denoise")) {
            if (MATCH(name, "strength")) {
                g_params->lumaDenoise.strength = f;
            } else {
                error = 1;
            }
        } else if (MATCH(section, "Luma Denoise Ref")) {
            if (MATCH(name, "angle_of_view")) {
                g_params->lumaDenoiseRef.angle_of_view = f;
            } else if (MATCH(name, "curve")) {
                g_params->lumaDenoiseRef.gamma = f;
            } else {
                error = 1;
            }
        } else if (MATCH(section, "Sharpen")) {
            if (MATCH(name, "strength_darken")) {
                  g_params->sharpen.strength_darken = f;
            } else if (MATCH(name, "strength_lighten")) {
                  g_params->sharpen.strength_lighten = f;
            } else if (MATCH(name, "sigma")) {
                  g_params->sharpen.sigma = f;
            } else if (MATCH(name, "alpha")) {
                  g_params->sharpen.alpha = f;
            }else if (MATCH(name, "overshoot")) {
                  g_params->sharpen.overshoot = f;
            } else if (MATCH(name, "undershoot")) {
                  g_params->sharpen.undershoot = f;
            } else if (MATCH(name, "range_stop_0")) {
                  g_params->sharpen.rangeStop0 = f;
            } else if (MATCH(name, "range_stop_1")) {
                  g_params->sharpen.rangeStop1 = f;
            } else if (MATCH(name, "range_stop_2")) {
                  g_params->sharpen.rangeStop2 = f;
            } else if (MATCH(name, "range_stop_3")) {
                  g_params->sharpen.rangeStop3 = f;
            } else if (MATCH(name, "min_thr")) {
                  g_params->sharpen.minThr = f;
            } else {
                error = 1;
            }

        } else if (MATCH(section, "Random Noise")) {
            if (MATCH(name, "strength")) {
                g_params->randomNoise.strength = f;
            } else {
                error = 1;
            }

        } else if (MATCH(section, "Median Mix")) {
            if (MATCH(name, "slope")) {
                g_params->medianMix.slope = f;
            } else if (MATCH(name, "offset")) {
                g_params->medianMix.offset = f;
            } else {
                error = 1;
            }

        } else if (MATCH(section, "LowPass")) {
            if (MATCH(name, "convolution")) {
                if (size != 2) {
                    fprintf(stderr, "Convolution should be a 2-value array\n");
                    error = 1;
                } else {
                    for (i = 0; i < 9; i++) {
                        g_params->lowpass.coefs[i] = ((float *)val)[i];
                    }
                }
            } else {
                error = 1;
            }
        } else if (MATCH(section, "Color Combine")) {
            if (MATCH(name, "ccm")) {
                /* Ignore parameter */
            } else {
                error = 1;
            }
        } else if (MATCH(section, "Local Tone Mapping")) {
            if (MATCH(name, "curves"))
            {
                //if (size != (int)(sizeof(g_params->ltm.ltmCurves) / sizeof(cfg->ltm.ltmCurves[0])))
                //local hack: I know .text file only has 15x9 entries, but for ipipe purposes it's defined as 16x10
                if (size != 15 * 9)
                {
                    fprintf(stderr, "Local Tone Mapping curves not of expected size\n");
                    error = 1;
		        }
                else {
		            for (i = 0; i < size; i++) {
		              g_params->ltm.curves[i] = ((float *)val)[i];
		            }
		        }
            } else {
                error = 1;
            }
        } else if (MATCH(section, "Gamma")) {
            /* Ignore section */
        } else if (MATCH(section, "Color Convert")) {
            /* Ignore section */
        } else {
            error = 1;
        }
        break;
    }

    case INI_TYPE_STRING:
        if (MATCH(section, "general")) {
            /* Ignore section */
        } else {
            error = 1;
        }
        break;

    default:
        fprintf(stderr, "Unknown data type %d\n", type);
        return -1;
    }

    if (error) {
        fprintf(stderr, "Unrecognized or bad parameter %s in section %s\n",
            name, section);
    }

    return 0;
}

int
parseIniFile(ipipe_params *params, const char *fname)
{
	FILE	*f = fopen(fname, "rb");
	char	buf[1024];
	int     rc = 0;

	if (f == NULL) {
        perror(fname);
		return -1;
	}

	g_lno = 0;
    g_section_name = NULL;
    g_params = params;

	while (fgets(buf, sizeof (buf), f) != NULL) {
		g_lno++;
		buf[sizeof (buf) - 1] = '\0';
		if (parse_line(f, buf, callback) == -1) {
			fprintf(stderr, "Parse error on line %d\n", g_lno);
			rc = -1;
			break;
		}
	}

	if (g_section_name != NULL) {
		free(g_section_name);
	}
    fclose(f);
	return rc;
}

#endif /* __sparc */
