#include <stdint.h>

/*
 * IPIPE parameters tuned for a given Analog Gain are read from config files
 * into this structure.  This is a temporary measure until the DTP is
 * available.
 */
typedef struct {
   /* RAW filter */
    struct {
        /* GrGb imbalance */
        unsigned    grgbImbalPlatDark;
        unsigned    grgbImbalDecayDark;
        unsigned    grgbImbalPlatBright;
        unsigned    grgbImbalDecayBright;
        unsigned    grgbImbalThr;

        /* Defect pixel correction */
        unsigned    dpcAlphaHotG;
        unsigned    dpcAlphaHotRb;
        unsigned    dpcAlphaColdG;
        unsigned    dpcAlphaColdRb;
        unsigned    dpcNoiseLevel;
    } raw;

    /* Demosaic */
    struct {
        unsigned    dewormGradientMul;  /* Fixed Point 1.7 */
        unsigned    dewormSlope;        /* Fixed point 8.8 */
        int         dewormOffset;       /* Signed integer */
    } demosaic;

    /* Purple Flare*/
    struct {
        unsigned strength;
    } purpleFlare;

    /* Chroma Plane generation */
    struct {
        float       epsilon;
        float       scaleR;
        float       scaleG;
        float       scaleB;
    } chromaGen;

    /* Weighted averaging filter on Luma channel */
    struct {
        unsigned    alpha;
        float       strength; /* reference value; we'll be using derived bitpos and lut below*/
        uint32_t    f2;       /* Spatial weight control */
    } lumaDenoise;

    /* Generate reference plane for Denoise */
    struct {
        float       angle_of_view;
        float       gamma;
    } lumaDenoiseRef;

    /* Add Random Noise to Luma channel */
    struct {
        float       strength;
    } randomNoise;

    struct {
       float        curves[15*9];
    } ltm;

    /* Local brightness map generation filter for LTM */
    struct {
        unsigned    th1;
        unsigned    th2;
        unsigned    limit;
    } ltmLBFilter;

    /* Chroma filter on 21 lines */
    struct {
        unsigned    th_r;
        unsigned    th_g;
        unsigned    th_b;
        unsigned    limit;
        unsigned    hEnab;
    } chromaDenoise;

    /* Median filter on Chroma channels */
    struct {
        unsigned    kernelSize;     /* Must be 0, 3, 5 or 7 */
    } median;

    /* Mixing of median filtered output with pre-median filtered */
    struct {
        float       slope;
        float       offset;
    } medianMix;

    /* Gaussian filter on Chroma channels */
    struct {
        float       coefs[2];      /* Mirror the left element to produce a 3x1 1D convolution kernel */
    } lowpass;

    /* Grey pixel desaturation */
    struct {
        unsigned    slope;
        int         offset;
    } greyDesat;

    /* Luma channel sharpening */
    struct {
        float       sigma;
        float       strength_darken;
        float       strength_lighten;
        float       alpha;
        float       overshoot;
        float       undershoot;
        float       rangeStop0;
        float       rangeStop1;
        float       rangeStop2;
        float       rangeStop3;
        float       minThr;
    } sharpen;

    struct {
        float       desat_mul;
        float       desat_t1;
    } colorCombine;
} ipipe_params;

int parseIniFile(ipipe_params *params, const char *fname);
