/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file vpipe_conv.h
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#ifndef VPIPE_CONV_H_
#define VPIPE_CONV_H_

#include <camera/vcamera_iface/vpipe_control/include/vpipe_control.h>
#include "IspCommon.h"
#include "ipipe.h"

#if !defined(__sparc)
    #define IPIPE_LSC_PADDING 1
#else
    #define IPIPE_LSC_PADDING 4
#endif

typedef void* ipipe_conv_hndl_t;

void vpipe_conv_get_default_ipipe_conf(ipipe_conv_hndl_t hndl, icIspConfig *conf);
int vpipe_convert_to_ipipe(ipipe_conv_hndl_t hndl,
                                hat_pix_fmt_t fmt,
                                hat_size_t *in_size,
                                vpipe_ctrl_settings_t *vpipe_cfg,
                                hat_dgain_t *post_gain,
                                icIspConfig *conf,
                                int capture,
                                void *cfg_private);
int vpipe_conv_config(ipipe_conv_hndl_t hndl,
                        void *static_common, void *static_private);
void vpipe_conv_destroy(ipipe_conv_hndl_t hndl);
int vpipe_conv_create(ipipe_conv_hndl_t *hndl);

#endif /* VPIPE_CONV_H_ */
