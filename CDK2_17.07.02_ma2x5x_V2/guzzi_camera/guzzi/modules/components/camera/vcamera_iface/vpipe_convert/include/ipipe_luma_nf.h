/* =============================================================================
 * Copyright (c) 2013-2014 MM Solutions AD
 * All rights reserved. Property of MM Solutions AD.
 *
 * This source code may not be used against the terms and conditions stipulated
 * in the licensing agreement under which it has been supplied, or without the
 * written permission of MM Solutions. Rights to use, copy, modify, and
 * distribute or disclose this source code and its documentation are granted only
 * through signed licensing agreement, provided that this copyright notice
 * appears in all copies, modifications, and distributions and subject to the
 * following conditions:
 * THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
 * WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
 * ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
 * NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
 * INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
 * PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
 * THIS SOURCE CODE AND ITS DOCUMENTATION.
 * =========================================================================== */
/**
 * @file ipipe_luma_nf.h
 *
 * @author ( MM Solutions AD )
 *
 */
/* -----------------------------------------------------------------------------
 *!
 *! Revision History
 *! ===================================
 *! 05-Nov-2013 : Author ( MM Solutions AD )
 *! Created
 * =========================================================================== */

#ifndef IPIPE_LUMA_NF_H_
#define IPIPE_LUMA_NF_H_

typedef struct
{
    unsigned alpha;
    float strength; /* reference value; we'll be using derived bitpos and lut below*/
    unsigned f2; /* Spatial weight control */
} ipipe_lumaDenoise_t;

typedef struct
{
    float angle_of_view;
    float gamma;
} ipipe_lumaDenoiseRef_t;

typedef struct
{
    float strength;
} ipipe_randomNoise_t;

typedef struct
{
    ipipe_lumaDenoise_t lumaDenoise;
    ipipe_lumaDenoiseRef_t lumaDenoiseRef;
    ipipe_randomNoise_t randomNoise;
} ipipe_luma_nf_t;

#endif //IPIPE_LUMA_NF_H_
