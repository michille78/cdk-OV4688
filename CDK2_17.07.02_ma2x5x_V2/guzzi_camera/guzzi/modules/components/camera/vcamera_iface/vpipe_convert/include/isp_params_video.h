// GENERATED TEST FILE (OCTAVE dump) !

//Input image dimensions:
#define ISPC_W_VIDEO	2104
#define ISPC_H_VIDEO	1560
#define ISPC_BITS_VIDEO	8

// Chroma generation
#define ISPC_VIDEO_CHROMA_GEN_EPSILON       	0.003922
#define ISPC_VIDEO_CHROMA_SCALE_R           	1.250000
#define ISPC_VIDEO_CHROMA_SCALE_G           	2.250000
#define ISPC_VIDEO_CHROMA_SCALE_B           	1.750000

// Median
#define ISPC_VIDEO_CHROMA_MEDIAN_SIZE       	0

extern float ispcVideoLowpassKernel[9];

// LowPass
// Sharpen
#define ISPC_VIDEO_SHARP_SIGMA           	2.500000
#define ISPC_VIDEO_SHARP_STRENGTH_DARKEN 	0.600000
#define ISPC_VIDEO_SHARP_STRENGTH_LIGHTEN	1.000000
#define ISPC_VIDEO_SHARP_ALPHA           	0.700000
#define ISPC_VIDEO_SHARP_OVERSHOOT       	1.100000
#define ISPC_VIDEO_SHARP_UNDERSHOOT      	1.000000
#define ISPC_VIDEO_SHARP_RANGE_STOP_0    	1.000000
#define ISPC_VIDEO_SHARP_RANGE_STOP_1    	5.000000
#define ISPC_VIDEO_SHARP_RANGE_STOP_2    	250.000000
#define ISPC_VIDEO_SHARP_RANGE_STOP_3    	255.000000
#define ISPC_VIDEO_SHARP_MIN_THR         	0.005000

// Luma denoise params:
#define ISPC_VIDEO_LUMA_DNS_STRENGTH	15.000000
#define ISPC_VIDEO_LUMA_DNS_ALPHA   	76	// 8bit value
#define ISPC_VIDEO_LUMA_DNS_STRENGTH_SEC 0.00000

// The following coefs are mirrored by the HW to obtain 7x7 matrix
// The 16 coefs represent the top-left square of a 7x7 symetrical matrix
// Each entry is 2 bits, and represents a left shift applied to
// the weight at the corresponding location.
#define ISPC_VIDEO_LUMA_DNS_F2         0x00000000

extern uint8_t ispcVideoYDnsDistLut[256];
extern uint8_t ispcVideoYDnsGammaLut[256];

#define ISPC_VIDEO_LUMA_DNS_REF_ANGLE_OF_VIEW	90
#define ISPC_VIDEO_LUMA_DNS_REF_GAMMA	0
#define ISPC_VIDEO_LUMA_DNS_REF_SHIFT	15

// Chroma denoise params:
#define ISPC_VIDEO_CHROMA_DNS_TH_R    4
#define ISPC_VIDEO_CHROMA_DNS_TH_G    7
#define ISPC_VIDEO_CHROMA_DNS_TH_B    5
#define ISPC_VIDEO_CHROMA_DNS_LIMIT   3
#define ISPC_VIDEO_CHROMA_DNS_H_ENAB  1

// Grey desaturate params:
#define ISPC_VIDEO_GREY_DESAT_OFFSET -2
#define ISPC_VIDEO_GREY_DESAT_SLOPE  30

extern float ispcVideoCCM[9];

#define ISPC_VIDEO_DESAT_T1  0.003922
#define ISPC_VIDEO_DESAT_MUL 255.000000

extern float ispcVideoGammaTable[4096];
