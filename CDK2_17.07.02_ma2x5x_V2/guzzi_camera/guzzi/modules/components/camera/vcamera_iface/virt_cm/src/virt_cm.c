/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file virt_cm.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <osal/osal_stdlib.h>
#include <utils/mms_debug.h>
#include "../inc/virt_cm.h"

#include <osal/osal_string.h>


#include <hal/hat_h3a_aewb.h>
#include "IspCommon.h"
#include "ipipe.h"

#include <hal/hal_camera_module/hai_cm_driver.h>

mmsdbg_define_variable(
        vdl_virt_cm,
        DL_DEFAULT,
        0,
        "virt_cm",
        "Virt CM"
    );
#define MMSDEBUGLEVEL mmsdbg_use_variable(vdl_virt_cm)

PLAT_PH_SOCKET_T platform_cm_cam_to_sock(uint32 cam_id);

int virt_cm_sensor_set_frame_duration(virt_cm_hndl_t hndl, uint32 duration);
int virt_cm_lens_set_aperture (virt_cm_hndl_t hndl, float *aperture);
int virt_cm_lens_set_dens_filter (virt_cm_hndl_t hndl, float *density_filter);
int virt_cm_lens_set_focal_length (virt_cm_hndl_t hndl, float *focal_lenght);
int virt_cm_lens_set_opt_stab_mode (virt_cm_hndl_t hndl, HAT_CM_LENS_OPT_STAB_MODE_T o_stab_mode);

void los_ConfigureSource(int srcIdx, icSourceConfig *sconf, int pipeId);
int  los_startSource(int srcIdx);
int  los_stopSource (int srcIdx);

#define ISEN_FRAME_WIDTH   856
#define ISEN_FRAME_HEIGHT  480

static void isen_get_default_source_conf(icSourceConfig *sconf)
{
    sconf->cameraOutputSize.w = ISEN_FRAME_WIDTH;
    sconf->cameraOutputSize.h = ISEN_FRAME_HEIGHT;
    sconf->cropWindow.x1 = 0;
    sconf->cropWindow.y1 = 0;
    sconf->cropWindow.x2 = ISEN_FRAME_WIDTH-1;
    sconf->cropWindow.y2 = ISEN_FRAME_HEIGHT-1;
    sconf->bayerFormat   = IC_BAYER_FORMAT_GBRG; //IC_BAYER_FORMAT_RGGB;
    sconf->bitsPerPixel  = 10;
}

icBayerFormat isen_fmt_conv_v2i(hat_pix_fmt_t vformat)
{
    switch (vformat.fmt.order.all)
    {
    case HAT_PORDBYR_B_Gb_Gr_R:
        return IC_BAYER_FORMAT_BGGR;
    case HAT_PORDBYR_R_Gr_Gb_B:
        return IC_BAYER_FORMAT_RGGB;
    case HAT_PORDBYR_Gb_B_R_Gr:
        return IC_BAYER_FORMAT_GBRG;
    case HAT_PORDBYR_Gr_R_B_Gb:
        return IC_BAYER_FORMAT_GRBG;
    default:
        mmsdbg(DL_ERROR, "Invalid sensor format (0x%x)", vformat.fmt.order.all);
    }
    return IC_BAYER_FORMAT_GBRG;
}

/*
 * ipipe_ctx_t
 * instance data
 */
typedef struct {
    const char *name;
    void*       private_data;
    hat_cm_driver_handle_t cm_drv_hndl;

    /* Mandatory data migrated from the isensor */
    icSourceConfig       src_cfg;

    virt_cm_sen_select_mode_t mode_params;
    hat_sen_config_t vsen_cfg;
    int         sen_state;

    int lrt_plugin_id;
} virt_cm_ctx_t;


/**
 * virt_cm_sen_config - apply supplied settings in hardware.
 *
 * @param hndl - virt_cm_hndl_t - instance data.
 *
 * @param cfg_type - virt_cm_sen_cfg_type_t - configuration type.
 *
 * @param cfg - void * - pointer to configuration.
 *
 * @return int - non-zero on error.
 */
int virt_cm_sen_config (virt_cm_hndl_t hndl, virt_cm_sen_cfg_type_t cfg_type, void* cfg)
{
    virt_cm_ctx_t *ctx = hndl;
    int     ret = 0;

    mmsdbg(DL_FUNC, "Enter");

    if (!hndl) {
        ret = -1;
        mmsdbg(DL_ERROR, "Null pointer");
        goto exit1;
    }

    switch (cfg_type)
    {
    case VSEN_CFG_INTENT:
        osal_memcpy (&ctx->mode_params.intent, cfg, sizeof(gzz_capture_intent_t));
        break;
    case VSEN_CFG_RESOLUTION:
        osal_memcpy (&ctx->mode_params.resolution, cfg, sizeof(hat_size_t));
        break;
    case VSEN_CFG_CROP:
        osal_memcpy (&ctx->mode_params.crop, cfg, sizeof(hat_rect_t));
        break;
    case VSEN_CFG_FPS_RANGE:
        osal_memcpy (&ctx->mode_params.fps_range, cfg, sizeof(hat_range_float_t));
        break;
    case VSEN_CFG_USECASE:
        osal_memcpy (&ctx->mode_params.usecase, cfg, sizeof(gzz_cam_custom_usecase_t));
        break;
    default:
        mmsdbg(DL_FUNC, "Invalid config (%d)", cfg_type);
    }

    mmsdbg(DL_FUNC, "Exit %s", "Ok");
    return ret;
exit1:
    mmsdbg(DL_FUNC, "Exit %s", "Error");
    return ret;
}

int virt_cm_sensor_exp_gain (virt_cm_hndl_t hndl, uint32 exp_time, float again)
{
    virt_cm_ctx_t *ctx = hndl;
    int            ret = 0;
    hat_cm_sensor_config_t cfg;

    mmsdbg(DL_FUNC, "Enter");

    if (!hndl) {
        ret = -1;
        mmsdbg(DL_ERROR, "Null pointer");
        goto exit1;
    }


    if (exp_time > ctx->vsen_cfg.frame_duration.max)
    {
        mmsdbg(DL_ERROR,"Requested exposure %d exceeds max limit %d --> clipping",
               exp_time , ctx->vsen_cfg.frame_duration.max);
        exp_time = ctx->vsen_cfg.frame_duration.max;
    }

    cfg.cfg_type                = SENSOR_CFG_SET_EXP_GAIN;
    cfg.cfg.exp_gain.exposure   = exp_time;
    cfg.cfg.exp_gain.again      = again;
    cfg.cfg.exp_gain.dgain      = 1.0;

    PROFILE_ADD(PROFILE_ID_SENSOR_EXP_GAIN, exp_time, (again*100));

    ret = ctx->cm_drv_hndl->hal_cm_config(ctx->cm_drv_hndl, CM_DRV_COMP_SENSOR, &cfg);
    if (ret) {
        mmsdbg(DL_ERROR, "Error on set_exp_gain!");
        goto exit1;
    }

    if (ctx->vsen_cfg.frame_duration.min != ctx->vsen_cfg.frame_duration.max)
    {
        if(exp_time < ctx->vsen_cfg.frame_duration.min)
        {
            mmsdbg(DL_MESSAGE,"Requested exposure %d exceeds min limit %d --> clipping",
                   exp_time , ctx->vsen_cfg.frame_duration.min);
            exp_time = ctx->vsen_cfg.frame_duration.min;
        }
        if (ctx->vsen_cfg.current_frame_duration != exp_time)
        {
            ctx->vsen_cfg.current_frame_duration = exp_time;
            virt_cm_sensor_set_frame_duration (hndl, ctx->vsen_cfg.current_frame_duration);
        }
    }

exit1:
    mmsdbg(DL_FUNC, "Exit %s", 0 ? "Error" : "Ok");
    return ret;
}

/**
 * virt_cm_lens_config - apply supplied settings in hardware.
 *
 * @param hndl - virt_cm_hndl_t - instance data.
 *
 * @param cfg_type - virt_cm_lens_cfg_type_t - configuration type.
 *
 * @param cfg - void * - pointer to configuration.
 *
 * @return int - non-zero on error.
 */
int virt_cm_lens_config (virt_cm_hndl_t hndl, virt_cm_lens_cfg_type_t cfg_type, void* cfg)
{
    int     ret = 0;
    HAT_CM_LENS_OPT_STAB_MODE_T o_stab_mode;

    mmsdbg(DL_FUNC, "Enter");

    if (!hndl) {
        ret = -1;
        mmsdbg(DL_ERROR, "Null pointer");
        goto exit1;
    }

    switch (cfg_type)
    {
    case VLENS_CFG_APERTURE:
        ret = virt_cm_lens_set_aperture(hndl, (float*)cfg);
        if (ret) {
            goto exit1;
        }
        break;
    case VLENS_CFG_DENS_FILT:
        ret = virt_cm_lens_set_dens_filter(hndl, (float*)cfg);
        if (ret) {
            goto exit1;
        }
        break;
    case VLENS_CFG_FOC_LENGTH:
        ret = virt_cm_lens_set_focal_length(hndl, (float*)cfg);
        if (ret) {
            goto exit1;
        }
        break;
    case VLENS_CFG_O_STAB_MODE:
        o_stab_mode = LENS_OPT_STAB_MODE_OFF;
        if(CAM_LENS_OPTICAL_STABILIZATION_MODE_ON == *((gzz_lens_optical_stab_mode_t*)cfg)) {
            o_stab_mode = LENS_OPT_STAB_MODE_ON;
        }
        ret = virt_cm_lens_set_opt_stab_mode(hndl, o_stab_mode);
        if (ret) {
            goto exit1;
        }
        break;
    default:
        mmsdbg(DL_FUNC, "Invalid config (%d)", cfg_type);
    }

    mmsdbg(DL_FUNC, "Exit %s", "Ok");
    return ret;
exit1:
    mmsdbg(DL_FUNC, "Exit %s", "Error");
    return ret;
}

int virt_cm_lens_move_to_pos (virt_cm_hndl_t hndl,
    hat_cm_lens_move_list_t* move_list,
    hat_cm_lens_move_cb_t micro_move_done_cb,
    void* user_data)
{
    virt_cm_ctx_t *ctx = hndl;
    int            ret = 0;
    hat_cm_lens_config_t cfg;

    mmsdbg(DL_FUNC, "Enter");

    if (!hndl) {
        ret = -1;
        mmsdbg(DL_ERROR, "Null pointer");
        goto exit1;
    }

    cfg.cfg_type                            = LENS_CFG_MOVE_TO_POS;
    cfg.cfg.move_to_pos.move_list           = move_list;
    cfg.cfg.move_to_pos.micro_step_done_cb  = micro_move_done_cb;
    cfg.cfg.move_to_pos.user_data           = user_data;
    ret = ctx->cm_drv_hndl->hal_cm_config(ctx->cm_drv_hndl, CM_DRV_COMP_LENS, &cfg);

    if (ret) {
        mmsdbg(DL_ERROR, "Error on move_to_pos!");
        goto exit1;
    }

exit1:
    mmsdbg(DL_FUNC, "Exit %s", 0 ? "Error" : "Ok");
    return ret;
}

int virt_cm_lens_move (virt_cm_hndl_t hndl,
    hat_cm_lens_move_list_t* move_list,
    hat_cm_lens_move_cb_t micro_move_done_cb,
    void* user_data)
{
    virt_cm_ctx_t *ctx = hndl;
    int            ret = 0;
    hat_cm_lens_config_t cfg;

    mmsdbg(DL_FUNC, "Enter");

    if (!hndl) {
        ret = -1;
        mmsdbg(DL_ERROR, "Null pointer");
        goto exit1;
    }

    cfg.cfg_type                     = LENS_CFG_MOVE;
    cfg.cfg.move.move_list           = move_list;
    cfg.cfg.move.micro_step_done_cb  = micro_move_done_cb;
    cfg.cfg.move.user_data           = user_data;
    ret = ctx->cm_drv_hndl->hal_cm_config(ctx->cm_drv_hndl, CM_DRV_COMP_LENS, &cfg);

    if (ret) {
        mmsdbg(DL_ERROR, "Error on move!");
        goto exit1;
    }

exit1:
    mmsdbg(DL_FUNC, "Exit %s", 0 ? "Error" : "Ok");
    return ret;
}

int virt_cm_lens_meas_pos (virt_cm_hndl_t hndl, float* pos)
{
    virt_cm_ctx_t *ctx = hndl;
    int            ret = 0;
    hat_cm_lens_query_t qry;

    mmsdbg(DL_FUNC, "Enter");

    if (!hndl) {
        ret = -1;
        mmsdbg(DL_ERROR, "Null pointer");
        goto exit1;
    }

    qry.query_type    = LENS_QUERY_MEASURE_POS;
    qry.query.pos_val = pos;
    ret = ctx->cm_drv_hndl->query(ctx->cm_drv_hndl, CM_DRV_COMP_LENS, &qry);

    if (ret) {
        mmsdbg(DL_ERROR, "Error on meas_pos!");
        goto exit1;
    }

exit1:
    mmsdbg(DL_FUNC, "Exit %s", 0 ? "Error" : "Ok");
    return ret;
}

int virt_cm_lens_init (virt_cm_hndl_t hndl)
{
    virt_cm_ctx_t *ctx = hndl;
    int            ret = 0;
    hat_cm_lens_control_t ctrl;

    mmsdbg(DL_FUNC, "Enter");

    if (!hndl) {
        ret = -1;
        mmsdbg(DL_ERROR, "Null pointer");
        goto exit1;
    }

    ctrl.ctrl_type = LENS_CTRL_INIT;
    ret = ctx->cm_drv_hndl->hal_cm_control(ctx->cm_drv_hndl, CM_DRV_COMP_LENS, &ctrl);

    if (ret) {
        mmsdbg(DL_ERROR, "Error on init!");
        goto exit1;
    }

exit1:
    mmsdbg(DL_FUNC, "Exit %s", 0 ? "Error" : "Ok");
    return ret;
}

int virt_cm_lens_abort (virt_cm_hndl_t hndl)
{
    virt_cm_ctx_t *ctx = hndl;
    int            ret = 0;
    hat_cm_lens_control_t ctrl;

    mmsdbg(DL_FUNC, "Enter");

    if (!hndl) {
        ret = -1;
        mmsdbg(DL_ERROR, "Null pointer");
        goto exit1;
    }

    ctrl.ctrl_type = LENS_CTRL_ABORT;
    ret = ctx->cm_drv_hndl->hal_cm_control(ctx->cm_drv_hndl, CM_DRV_COMP_LENS, &ctrl);

    if (ret) {
        mmsdbg(DL_ERROR, "Error on lens_abort!");
        goto exit1;
    }

exit1:
    mmsdbg(DL_FUNC, "Exit %s", 0 ? "Error" : "Ok");
    return ret;
}

int virt_cm_lens_set_aperture (virt_cm_hndl_t hndl, float* aperture)
{
    virt_cm_ctx_t *ctx = hndl;
    int            ret = 0;
    hat_cm_lens_control_t ctrl;

    mmsdbg(DL_FUNC, "Enter");

    if (!hndl) {
        ret = -1;
        mmsdbg(DL_ERROR, "Null pointer");
        goto exit1;
    }

    ctrl.ctrl_type = LENS_CTRL_SET_APERTURE;
    ctrl.ctrl.aperture = aperture;
    ret = ctx->cm_drv_hndl->hal_cm_control(ctx->cm_drv_hndl, CM_DRV_COMP_LENS, &ctrl);

    if (ret) {
        mmsdbg(DL_ERROR, "Error on set_aperture!");
        goto exit1;
    }

exit1:
    mmsdbg(DL_FUNC, "Exit %s", 0 ? "Error" : "Ok");
    return ret;
}

int virt_cm_lens_set_dens_filter (virt_cm_hndl_t hndl, float* density_filter)
{
    virt_cm_ctx_t *ctx = hndl;
    int            ret = 0;
    hat_cm_lens_control_t ctrl;

    mmsdbg(DL_FUNC, "Enter");

    if (!hndl) {
        ret = -1;
        mmsdbg(DL_ERROR, "Null pointer");
        goto exit1;
    }

    ctrl.ctrl_type = LENS_CTRL_SET_DENSITY_FILTER;
    ctrl.ctrl.dens_filter = density_filter;
    ret = ctx->cm_drv_hndl->hal_cm_control(ctx->cm_drv_hndl, CM_DRV_COMP_LENS, &ctrl);

    if (ret) {
        mmsdbg(DL_ERROR, "Error on set_dens_filter!");
        goto exit1;
    }

exit1:
    mmsdbg(DL_FUNC, "Exit %s", 0 ? "Error" : "Ok");
    return ret;
}

int virt_cm_lens_set_focal_length (virt_cm_hndl_t hndl, float* focal_lenght)
{
    virt_cm_ctx_t *ctx = hndl;
    int            ret = 0;
    hat_cm_lens_control_t ctrl;

    mmsdbg(DL_FUNC, "Enter");

    if (!hndl) {
        ret = -1;
        mmsdbg(DL_ERROR, "Null pointer");
        goto exit1;
    }

    ctrl.ctrl_type = LENS_CTRL_SET_FOCAL_LENGHT;
    ctrl.ctrl.focal_lenght = focal_lenght;
    ret = ctx->cm_drv_hndl->hal_cm_control(ctx->cm_drv_hndl, CM_DRV_COMP_LENS, &ctrl);

    if (ret) {
        mmsdbg(DL_ERROR, "Error on set_focal_lengh!");
        goto exit1;
    }

exit1:
    mmsdbg(DL_FUNC, "Exit %s", 0 ? "Error" : "Ok");
    return ret;
}

int virt_cm_lens_set_opt_stab_mode (virt_cm_hndl_t hndl, HAT_CM_LENS_OPT_STAB_MODE_T o_stab_mode)
{
    virt_cm_ctx_t *ctx = hndl;
    int            ret = 0;
    hat_cm_lens_control_t ctrl;

    mmsdbg(DL_FUNC, "Enter");

    if (!hndl) {
        ret = -1;
        mmsdbg(DL_ERROR, "Null pointer");
        goto exit1;
    }

    ctrl.ctrl_type = LENS_CTRL_SET_OPTIC_STAB_MODE;
    ctrl.ctrl.o_stab_mode = o_stab_mode;
    ret = ctx->cm_drv_hndl->hal_cm_control(ctx->cm_drv_hndl, CM_DRV_COMP_LENS, &ctrl);

    if (ret) {
        mmsdbg(DL_ERROR, "Error on set_opt_stab_mode!");
        goto exit1;
    }

exit1:
    mmsdbg(DL_FUNC, "Exit %s", 0 ? "Error" : "Ok");
    return ret;
}

int virt_cm_sensor_set_frame_duration (virt_cm_hndl_t hndl, uint32 duration)
{
    virt_cm_ctx_t *ctx = hndl;
    int            ret = 0;
    hat_cm_sensor_config_t cfg;

    mmsdbg(DL_FUNC, "Enter");

    if (!hndl) {
        ret = -1;
        mmsdbg(DL_ERROR, "Null pointer");
        goto exit1;
    }

    mmsdbg(DL_MESSAGE, "Duration = %f", duration);

    cfg.cfg_type    = SENSOR_CFG_SET_FRAME_DURATION;
    cfg.cfg.frm_duration = duration;
    ret = ctx->cm_drv_hndl->hal_cm_config(ctx->cm_drv_hndl, CM_DRV_COMP_SENSOR, &cfg);
    if (ret) {
        mmsdbg(DL_ERROR, "Error on set_duration!");
        goto exit1;
    }

exit1:
    mmsdbg(DL_FUNC, "Exit %s", 0 ? "Error" : "Ok");
    return ret;
}

int virt_cm_lights_exec_seq (virt_cm_hndl_t hndl, hat_light_ctrl_list_t *ctrl)
{
    virt_cm_ctx_t *ctx = hndl;
    int            ret = 0;
    hat_cm_light_control_t control;

    mmsdbg(DL_FUNC, "Enter");

    if (!hndl || !ctrl) {
        ret = -1;
        mmsdbg(DL_ERROR, "Null pointer");
        goto exit1;
    }

    mmsdbg(DL_MESSAGE, "Start Flashing");

    control.ctrl_type = LIGHT_CTRL_POWER_CMD;
    control.ctrl.pwr_cmd = ctrl;
    ret = ctx->cm_drv_hndl->hal_cm_control(ctx->cm_drv_hndl, CM_DRV_COMP_LIGHT, &control);
    if (ret) {
        mmsdbg(DL_ERROR, "Error on light sequence!");
        goto exit1;
    }

    mmsdbg(DL_MESSAGE, "End Flash");

exit1:
    mmsdbg(DL_FUNC, "Exit %s", 0 ? "Error" : "Ok");
    return ret;
}


int virt_cm_lights_get_features (virt_cm_hndl_t hndl, hat_flash_bounds_t** features)
{
    virt_cm_ctx_t *ctx = hndl;
    int            ret = 0;
    hat_cm_light_query_t qry;

    mmsdbg(DL_FUNC, "Enter");

    if (!hndl) {
        ret = -1;
        mmsdbg(DL_ERROR, "Null pointer");
        goto exit1;
    }

    mmsdbg(DL_MESSAGE, "Get features");

    qry.query_type    = LIGHT_QUERY_GET_FEATURES;
    qry.query.features = features;
    ret = ctx->cm_drv_hndl->query(ctx->cm_drv_hndl, CM_DRV_COMP_LIGHT, &qry);
    if (ret) {
        mmsdbg(DL_ERROR, "Error on light get features!");
        goto exit1;
    }

exit1:
    mmsdbg(DL_FUNC, "Exit %s", 0 ? "Error" : "Ok");
    return ret;
}

/**
 * vsen_destroy - destroy vsen instance.
 *
 * @param hndl - vsen_hndl_t -  instance data.
 *
 * @return int - non-zero on error.
 */
int virt_cm_destroy (virt_cm_hndl_t hndl)
{
    virt_cm_ctx_t *ctx = hndl;
    int     ret = 0;

    mmsdbg(DL_FUNC, "Enter");

    if (!hndl) {
        ret = -1;
        mmsdbg(DL_ERROR, "Null pointer");
        goto exit1;
    }

    {
        ret = ctx->cm_drv_hndl->close(ctx->cm_drv_hndl);
        if (ret) {
            mmsdbg(DL_ERROR, "Error on CM driver close!");
            goto exit1;
        }
        ret = ctx->cm_drv_hndl->destroy(ctx->cm_drv_hndl);
        if (ret) {
            mmsdbg(DL_ERROR, "Error on CM driver destroy!");
            goto exit1;
        }
    }

    osal_free(hndl);
exit1:
    mmsdbg(DL_FUNC, "Exit %s", 0 ? "Error" : "Ok");
    return ret;
}

static void virt_cm_cam_to_sock(hat_cm_driver_cr_prms_t *cm_create, uint32 cam_id)
{
    if (cam_id >= PH_SOCKET_MAX)
    {
        cam_id = 0;
    }
    cm_create->id = platform_cm_cam_to_sock(cam_id);
    cm_create->name = "Camera Module";
}

/**
 * virt_cm_detect - All sensors detect.
 *
 * @return int - non-zero on error.
 */
int virt_cm_detect()
{
    int ret = 0;
    hat_cm_driver_handle_t cm_drv_hndl = NULL;
    hat_cm_driver_cr_prms_t cm_create;
    int i;

    for(i=0; i < PH_SOCKET_MAX; i++) {
        mmsdbg(DL_ERROR, "Detecting Sensor on Socket %d\n"
                         "===================================================", i);
        virt_cm_cam_to_sock(&cm_create, i);
        ret = hai_cm_driver_create(&cm_drv_hndl, &cm_create);
        if (ret) {
            mmsdbg(DL_ERROR, "Error on CM driver create!");
            goto exit1;
        }
        ret = cm_drv_hndl->detect(cm_drv_hndl);
        if (ret) {
            mmsdbg(DL_ERROR, "\n\nNone Sensor detected on socket: %d !\n", i);
        }
        ret = cm_drv_hndl->destroy(cm_drv_hndl);
        if (ret) {
            mmsdbg(DL_ERROR, "Error on CM driver destroy!");
            goto exit1;
        }
    }

    return 0;
exit1:
    mmsdbg(DL_FUNC, "Exit %s", 0 ? "Error" : "Ok");
    return ret;
}

/**
 * vsen_create - create vsen instance.
 *
 * @param hndl - apipe_hndl_t * - pointer to instance data.
 *
 * @return int - non-zero on error.
 */
int virt_cm_create (virt_cm_hndl_t *hndl, uint32 cam_id)
{
    int         ret = 0;
    virt_cm_ctx_t *ctx;
    hat_cm_driver_cr_prms_t cm_create;

    PROFILE_ADD(PROFILE_ID_CAMERA_TIME, 0x10BB, 0x00);

    mmsdbg(DL_FUNC, "Enter");

    ctx = osal_malloc(sizeof(*ctx));

    if (!ctx) {
        ret = -1;
        mmsdbg(DL_ERROR, "Can't allocate memory");
        goto exit1;
    }

    ctx->name = "virt_cm";

    isen_get_default_source_conf(&ctx->src_cfg);

    ctx->sen_state = 0;

    PROFILE_ADD(PROFILE_ID_CAMERA_TIME, 0x101C, 0x00);

    virt_cm_cam_to_sock(&cm_create, cam_id);

    ret = hai_cm_driver_create(&ctx->cm_drv_hndl, &cm_create);
    if (ret) {
        mmsdbg(DL_ERROR, "Error on CM driver create!");
        goto exit1;
    }
    PROFILE_ADD(PROFILE_ID_CAMERA_TIME, 0x102C, 0x00);

    ret = ctx->cm_drv_hndl->detect(ctx->cm_drv_hndl);
    if (ret) {
        mmsdbg(DL_ERROR, "Error on CM driver detect!");
        goto exit1;
    }
    PROFILE_ADD(PROFILE_ID_CAMERA_TIME, 0x103C, 0x00);

    ret = ctx->cm_drv_hndl->open(ctx->cm_drv_hndl);
    if (ret) {
        mmsdbg(DL_ERROR, "Error on CM driver open!");
        goto exit1;
    }
    PROFILE_ADD(PROFILE_ID_CAMERA_TIME, 0x10EE, 0x00);

    ctx->lrt_plugin_id = cam_id; /* TODO: Map cam_id ot plugin id? */

    *hndl = ctx;

exit1:
    mmsdbg(DL_FUNC, "Exit %s", 0 ? "Error" : "Ok");
    return ret;
}

/**
 * virt_cm_get_all_camera_comp_ids - get camera components ids.
 *
 * @param hndl - virt_cm_hndl_t - instance data.
 *
 * @param component_ids - hat_cm_component_ids_t* - componets ids.
 *
 * @return int - non-zero on error.
 */
int virt_cm_get_all_camera_comp_ids(virt_cm_hndl_t hndl, /*OUT*/ hat_cm_component_ids_t* component_ids)
{
    int         ret = 0;
    virt_cm_ctx_t *ctx = hndl;
    hat_cm_camera_query_t camera_qry;


    camera_qry.query_type      = CAMERA_QUERY_GET_MOD_IDS;
    camera_qry.query.component_ids = component_ids;
    ret = ctx->cm_drv_hndl->query(ctx->cm_drv_hndl, CM_DRV_COMP_CAMERA, &camera_qry);
    if (ret) {
        mmsdbg(DL_ERROR, "Error on virt_cm_get_all_camera_comp_ids!");
        goto exit1;
    }

exit1:
    mmsdbg(DL_FUNC, "Exit %s", 0 ? "Error" : "Ok");
    return ret;
}

static int virt_cm_get_camera_imgif_ctx(virt_cm_hndl_t hndl, /*OUT*/ int* csi_ctx)
{
    int ret = 0;
    virt_cm_ctx_t *ctx = hndl;
    hat_cm_camera_query_t camera_qry;

    camera_qry.query_type      = CAMERA_QUERY_GET_IMGIF_CTX;
    ret = ctx->cm_drv_hndl->query(ctx->cm_drv_hndl, CM_DRV_COMP_CAMERA, (void*)&camera_qry);
    if (ret) {
        mmsdbg(DL_ERROR, "Error on virt_cm_get_camera_imgif_ctx!");
        goto exit1;
    }
    *csi_ctx = camera_qry.query.imgif_ctx;
exit1:
    mmsdbg(DL_FUNC, "Exit %s", 0 ? "Error" : "Ok");
    return ret;
}

int virt_cm_get_dynamic_properties(virt_cm_hndl_t hndl, /*OUT*/ virt_cm_dyn_props_t* props)
{
    int         ret = 0;
    virt_cm_ctx_t *ctx = hndl;
    hat_cm_sensor_query_t sensor_qry;
    hat_cm_lens_query_t   lens_qry;
    hat_cm_light_query_t  light_qry;

    sensor_qry.query_type      = SENSOR_QUERY_GET_DYN_PROPS;
    sensor_qry.query.dyn_props = &props->sensor_props;
    ret = ctx->cm_drv_hndl->query(ctx->cm_drv_hndl, CM_DRV_COMP_SENSOR, &sensor_qry);
    if (ret) {
        mmsdbg(DL_ERROR, "Error on get_dyn_props!");
        goto exit1;
    }

    lens_qry.query_type      = LENS_QUERY_GET_DYN_PROPS;
    lens_qry.query.dyn_props = &props->lens_props;
    ret = ctx->cm_drv_hndl->query(ctx->cm_drv_hndl, CM_DRV_COMP_LENS, &lens_qry);
    if (ret) {
        mmsdbg(DL_ERROR, "Error on get_dyn_props!");
        goto exit1;
    }

    light_qry.query_type      = LIGHT_QUERY_GET_DYN_PROPS;
    light_qry.query.dyn_props = &props->flash_props;
    ret = ctx->cm_drv_hndl->query(ctx->cm_drv_hndl, CM_DRV_COMP_LIGHT, &light_qry);
    if (ret) {
        mmsdbg(DL_ERROR, "Error on get_dyn_props!");
        goto exit1;
    }

exit1:
    mmsdbg(DL_FUNC, "Exit %s", 0 ? "Error" : "Ok");
    return ret;
}

int virt_cm_get_mode_features(virt_cm_hndl_t hndl, /*OUT*/ virt_cm_sen_mode_features_t *features)
{
    int         ret = 0;
    virt_cm_ctx_t *ctx = hndl;
    hat_cm_sensor_select_mode_t mode_sel;
    hat_cm_sensor_query_t qry;

    mode_sel.intent = ctx->mode_params.intent;
    mode_sel.resolution = ctx->mode_params.resolution;
    mode_sel.crop = ctx->mode_params.crop;
    mode_sel.fps_range = ctx->mode_params.fps_range;
    mode_sel.pipeline_id = ctx->mode_params.usecase;

    qry.query_type                  = SENSOR_QUERY_SELECT_MODE;
    qry.query.sel_mode.mode_rqst    = &mode_sel;
    qry.query.sel_mode.mode_index   = &features->sen_mode_idx;
    ret = ctx->cm_drv_hndl->query(ctx->cm_drv_hndl, CM_DRV_COMP_SENSOR, &qry);
    if (ret) {
        mmsdbg(DL_ERROR, "Error on select_mode!");
        goto exit1;
    }

    qry.query_type      = SENSOR_QUERY_GET_FEATURES;
    qry.query.features  = &features->sen;
    ret = ctx->cm_drv_hndl->query(ctx->cm_drv_hndl, CM_DRV_COMP_SENSOR, &qry);
    if (ret) {
        mmsdbg(DL_ERROR, "Error on get_features!");
        goto exit1;
    }

exit1:
    mmsdbg(DL_FUNC, "Exit %s", 0 ? "Error" : "Ok");
    return ret;
}

int virt_cm_set_stream_mode(virt_cm_hndl_t hndl, int mode) {
    int            ret = 0;
    virt_cm_ctx_t *ctx = hndl;
    hat_cm_sensor_control_t cntr;

    cntr.ctrl_type = SENSOR_CTRL_START_STOP;
    if(mode) {
        cntr.ctrl.mode = SENSOR_STREAM_MODE_ON;
    }
    else {
        cntr.ctrl.mode = SENSOR_STREAM_MODE_OFF;
    }
    ret = ctx->cm_drv_hndl->hal_cm_control(ctx->cm_drv_hndl, CM_DRV_COMP_SENSOR, &cntr);
    if (ret) {
        mmsdbg(DL_ERROR, "Error on set_mode!");
        goto exit1;
    }
exit1:
    mmsdbg(DL_FUNC, "Exit %s", 0 ? "Error" : "Ok");
    return ret;
}

int virt_cm_set_operating_mode(virt_cm_hndl_t hndl, virt_cm_sen_mode_features_t *features, uint32 sen_mode_idx)
{
    int            ret = 0;
    virt_cm_ctx_t *ctx = hndl;
    hat_cm_sensor_config_t cfg;
#if 0
    hat_cm_sensor_select_mode_t mode_sel;
#endif

    PROFILE_ADD(PROFILE_ID_CAMERA_TIME, 0x20BB, 0x00);

    if (!hndl) {
        mmsdbg(DL_ERROR, "Null pointer");
        return -1;
    }

#if 0
    // In case if sensor is asked for a proper mode
    mode_sel.intent = ctx->mode_params.intent;
    mode_sel.resolution = ctx->mode_params.resolution;
    mode_sel.crop = ctx->mode_params.crop;
    mode_sel.fr_duration = ctx->mode_params.fr_duration;
    mode_sel.fps_range = ctx->mode_params.fps_range;

    ret = ctx->vsen_hndl.select_mode(&ctx->vsen_hndl, &mode_sel, &ctx->vsen_cfg.sensor_mode_idx);
    if (ret) {
        mmsdbg(DL_ERROR, "Error on select_mode!");
        goto exit1;
    }
#else
    // In case of forced setting a mode from the external world
    ctx->vsen_cfg.sensor_mode_idx = sen_mode_idx;
#endif
    ctx->vsen_cfg.frame_duration.max = ROUND_FLOAT_X_TO_INT(1000000.0/ctx->mode_params.fps_range.min);
    ctx->vsen_cfg.frame_duration.min = ROUND_FLOAT_X_TO_INT(1000000.0/ctx->mode_params.fps_range.max);

    cfg.cfg_type    = SENSOR_CFG_SET_MODE;
    cfg.cfg.sen_cfg = &ctx->vsen_cfg;
    ret = ctx->cm_drv_hndl->hal_cm_config(ctx->cm_drv_hndl, CM_DRV_COMP_SENSOR, &cfg);
    if (ret) {
        mmsdbg(DL_ERROR, "Error on set_mode!");
        goto exit1;
    }
    // Start with frame duration in the middle of the range - works for fixed fps (i.e. min=max)
    ctx->vsen_cfg.current_frame_duration = (ctx->vsen_cfg.frame_duration.max +
                                            ctx->vsen_cfg.frame_duration.min)/2;

    virt_cm_sensor_set_frame_duration(hndl, ctx->vsen_cfg.current_frame_duration);

    PROFILE_ADD(PROFILE_ID_CAMERA_TIME, 0x20EE, 0x00);

// Copy of vsen_config();
//=============================================================================
    int idx;
    const hat_sensor_mode_t *mode;

    if (ctx->sen_state)
    {
        ret = -2;
        mmsdbg(DL_ERROR, "Config sensor while running");
        goto exit1;
    }

    idx = ctx->vsen_cfg.sensor_mode_idx;
    if (idx >= features->sen->modes.num)
    {
        ret = -3;
        mmsdbg(DL_ERROR, "Invalid sensor mode (%d)", idx);
        goto exit1;
    }
    mode = &features->sen->modes.list[idx];

    ctx->src_cfg.cameraOutputSize.w = mode->field_of_view.w; //features->sen->valid_pixels.w;
    ctx->src_cfg.cameraOutputSize.h = mode->field_of_view.h; //features->sen->valid_pixels.h;
    ctx->src_cfg.cropWindow.x1 = mode->field_of_view.x;
    ctx->src_cfg.cropWindow.y1 = mode->field_of_view.y;
    ctx->src_cfg.cropWindow.x2 = mode->field_of_view.w+mode->field_of_view.x; //VSEN_FRAME_WIDTH-1;
    ctx->src_cfg.cropWindow.y2 = mode->field_of_view.h+mode->field_of_view.y; //VSEN_FRAME_HEIGHT-1;
    ctx->src_cfg.bayerFormat   = isen_fmt_conv_v2i(mode->format); //IC_BAYER_FORMAT_GBRG; //IC_BAYER_FORMAT_RGGB;
    ctx->src_cfg.bitsPerPixel  = mode->format.bpc; // 10;

    int imgif_ctx = 0;
    ret = virt_cm_get_camera_imgif_ctx(hndl, (void*)&imgif_ctx);
    if (ret) {
        mmsdbg(DL_ERROR, "Error in get imgif!");
        goto exit1;
    }
    ctx->src_cfg.mipiRxData.controllerNo = imgif_ctx;

    ctx->src_cfg.mipiRxData.noLanes      = mode->mipi_rx_cfg.lines;
    ctx->src_cfg.mipiRxData.dataMode     = mode->mipi_rx_cfg.data_mode;
    ctx->src_cfg.mipiRxData.dataType     = mode->mipi_rx_cfg.data_type;
    ctx->src_cfg.mipiRxData.laneRateMbps = mode->mipi_rx_cfg.rate;
    {
        int platform_map_ctrlNo_2_rxDevice(uint32_t controllerNo);
        ctx->src_cfg.mipiRxData.recNrl       = platform_map_ctrlNo_2_rxDevice(ctx->src_cfg.mipiRxData.controllerNo);
    }
    los_ConfigureSource(ctx->lrt_plugin_id, &ctx->src_cfg, mode->pipeline_id);
//=============================================================================

exit1:
    mmsdbg(DL_FUNC, "Exit %s", 0 ? "Error" : "Ok");
    return ret;
}

int virt_cm_start (virt_cm_hndl_t hndl)
{
    int         ret = 0;
    virt_cm_ctx_t *ctx = hndl;

    if (!hndl) {
        ret = -1;
        mmsdbg(DL_ERROR, "Null pointer");
        goto exit1;
    }

// Copy of vsen_start();
//=============================================================================
    PROFILE_ADD(PROFILE_ID_LOS_START_SRC, 0xBBBB, ctx->lrt_plugin_id);
    los_startSource (ctx->lrt_plugin_id);
    PROFILE_ADD(PROFILE_ID_LOS_START_SRC, 0xEEEE, ctx->lrt_plugin_id);
//=============================================================================

    ctx->sen_state = 1;

exit1:
    mmsdbg(DL_FUNC, "Exit %s", 0 ? "Error" : "Ok");
    return ret;
}

int virt_cm_stop (virt_cm_hndl_t hndl)
{
    virt_cm_ctx_t *ctx = hndl;

// Copy of  vsen_stop();
//=============================================================================
    los_stopSource (ctx->lrt_plugin_id);
//=============================================================================

    ctx->sen_state = 0;
    return 0;
}
