/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file configurator.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <osal/osal_stdlib.h>
#include <osal/osal_string.h>
#include <osal/osal_list.h>
#include <osal/osal_mutex.h>
#include <utils/mms_debug.h>

#include <stm/include/stm.h>
#include <osal/pool.h>
#include <configurator.h>

#define CONFIGURATOR_ALLOC_TIMEOUT DEFAULT_ALLOC_TIMEOUT_MS

struct configurator {
    configurator_plugin_desc_t *plugin;
    void *plugin_prv;
    struct list_head clients_list;
    int config_struct_size;

    stm_t *stm;

    pool_t *pool_config;
    pool_t *pool_clients;

    osal_mutex *lock_configuring;
    osal_mutex *lock_config;
    osal_mutex *lock_clients;

    void *config_host;
    void *config_alter;
    void *config_new_host;
    void *config_new_alter;
};

struct configurator_client {
    struct list_head list_node;
    configurator_t *configurator;
    configurator_client_desc_t desc;
    void *prv;
};

typedef struct  {
    configurator_t *c;
    void *host_config_struct;
} configurator_config_struct_container_t;

#define CALC_CONF_STRUCT_SIZE(SIZE) \
    (sizeof (configurator_config_struct_container_t) + (SIZE))

mmsdbg_define_variable(
        vdl_configurator,
        DL_DEFAULT,
        0,
        "configurator",
        "Configurator: validates and propagates host settings."
    );
#define MMSDEBUGLEVEL mmsdbg_use_variable(vdl_configurator)

/*
* ***************************************************************************
* ***************************************************************************
* ** Config struct routines *************************************************
* ***************************************************************************
* ***************************************************************************
*/

static void *config_struct_alloc(configurator_t *c)
{
    configurator_config_struct_container_t *config_struct_container;

    config_struct_container = pool_alloc_timeout(
            c->pool_config,
            CONFIGURATOR_ALLOC_TIMEOUT
        );
    if (!config_struct_container) {
        mmsdbg(DL_ERROR, "Timeout during configuration struct alloc!");
        goto exit1;
    }
    config_struct_container->c = c;
    config_struct_container->host_config_struct = NULL;

    return config_struct_container + 1;
exit1:
    return NULL;
}

static void config_struct_lock(void *config_struct)
{
    configurator_config_struct_container_t *config_struct_container;
    config_struct_container = config_struct;
    config_struct_container = config_struct_container - 1;
    osal_mem_lock(config_struct_container);
    if (config_struct_container->host_config_struct) {
        config_struct_lock(config_struct_container->host_config_struct);
    }
}

static void config_struct_free(void *config_struct)
{
    configurator_config_struct_container_t *config_struct_container;
    config_struct_container = config_struct;
    config_struct_container = config_struct_container - 1;
    if (config_struct_container->host_config_struct) {
        config_struct_free(config_struct_container->host_config_struct);
    }
    osal_free(config_struct_container);
}

static void *config_struct_get_host(void *config_struct)
{
    configurator_config_struct_container_t *config_struct_container;
    config_struct_container = config_struct;
    config_struct_container = config_struct_container - 1;
    return config_struct_container->host_config_struct;
}

static void config_struct_set_host(void *config_struct, void *host)
{
    configurator_config_struct_container_t *config_struct_container;
    config_struct_container = config_struct;
    config_struct_container = config_struct_container - 1;
    config_struct_lock(host);
    config_struct_container->host_config_struct = host;
}

unsigned int config_struct_get_seq_num(void *config_struct)
{
    configurator_config_struct_node_t *config_struct_node;
    config_struct_node = config_struct;
    return config_struct_node->seq_num;
}

/*
* ***************************************************************************
* ***************************************************************************
* ** Misc *******************************************************************
* ***************************************************************************
* ***************************************************************************
*/

static int alternate(configurator_t *c, void *alter)
{
    struct list_head *list_node;
    configurator_client_t *client;
    unsigned int seq_num;
    int err;

    err = 0;
    do {
        seq_num = config_struct_get_seq_num(alter);
        list_for_each(list_node, &c->clients_list) {
            client = list_entry(list_node, configurator_client_t, list_node);
            err = client->desc.alternate(c, client, client->prv, alter);
            if (err) {
                mmsdbg(DL_ERROR, "Alternate error: client=\"%s\"!", client->desc.name);
                goto exit1;
            }
        }
    } while (seq_num != config_struct_get_seq_num(alter));

exit1:
    return err;
}

/*
* ***************************************************************************
* ***************************************************************************
* ** Implement begin, end, cancel, set, get, reg and unreg ******************
* ***************************************************************************
* ***************************************************************************
*/

static int config_set_begin(configurator_t *c)
{
    void *new_host, *new_alter;

    new_host = config_struct_alloc(c);
    if (!new_host) {
        mmsdbg(DL_ERROR, "Failed to allocate new Host config!");
        goto exit1;
    }

    new_alter = config_struct_alloc(c);
    if (!new_alter) {
        mmsdbg(DL_ERROR, "Failed to allocate new Alternate config!");
        goto exit2;
    } 
    config_struct_set_host(new_alter, new_host);

    osal_mutex_lock(c->lock_configuring);

    /* Holding c->lock_configuring is enough to protect c->config_host */
    osal_memcpy(new_host, c->config_host, c->config_struct_size);

    c->config_new_host = new_host;
    c->config_new_alter = new_alter;

    return 0;
exit2:
    config_struct_free(new_host);
exit1:
    return -1;
}

static int config_set_end(configurator_t *c)
{
    void *new_host, *new_alter;
    void *host, *alter;
    struct list_head *list_node;
    configurator_client_t *client;
    int err;

    host = c->config_host;
    alter = c->config_alter;

    new_host = c->config_new_host;
    new_alter = c->config_new_alter;

    osal_memcpy(new_alter, new_host, c->config_struct_size);

    err = alternate(c, new_alter);
    if (err) {
        osal_mutex_unlock(c->lock_configuring);
        config_struct_free(new_host);
        config_struct_free(new_alter);
        mmsdbg(DL_ERROR, "Alternate error!");
        goto exit1;
    }

    config_struct_lock(new_alter); /* Lock for notify below */

    osal_mutex_lock(c->lock_config);
    c->config_host = new_host;
    c->config_alter = new_alter;
    osal_mutex_unlock(c->lock_config);

    osal_mutex_unlock(c->lock_configuring);

    config_struct_free(alter);
    config_struct_free(host);

    osal_mutex_lock(c->lock_clients);
    list_for_each(list_node, &c->clients_list) {
        client = list_entry(list_node, configurator_client_t, list_node);
        client->desc.notify(c, client, client->prv, new_alter);
    }
    osal_mutex_unlock(c->lock_clients);

    config_struct_free(new_alter);

    return 0;
exit1:
    return err;
}

static void config_set_cancel(configurator_t *c)
{
    void *new_host, *new_alter;
    new_host = c->config_new_host;
    new_alter = c->config_new_alter;
    osal_mutex_unlock(c->lock_configuring);
    config_struct_free(new_host);
    config_struct_free(new_alter);
}

static int config_set(configurator_t *c, unsigned int index, void *data)
{
    return c->plugin->set(c, c->plugin_prv, c->config_new_host, index, data);
}

static int config_get(configurator_t *c, unsigned int index, void *data)
{
    void *host;
    int err;

    osal_mutex_lock(c->lock_config);
    host = c->config_host; /* TODO: use Hc or Hn */
    config_struct_lock(host);
    osal_mutex_unlock(c->lock_config);

    err = c->plugin->get(c, c->plugin_prv, host, index, data);

    config_struct_free(host);

    return err;
}

static configurator_client_t *register_client(
        configurator_t *c,
        configurator_client_desc_t *client_desc,
        void *client_prv
    )
{
    configurator_client_t *client, *cli;
    struct list_head *list_node;
    void *new_alter, *alter;
    void *host;
    int err;

    client = pool_alloc_timeout(c->pool_clients, CONFIGURATOR_ALLOC_TIMEOUT);
    if (!client) {
        mmsdbg(DL_ERROR, "Failed to allocate new client structure!");
        goto exit1;
    }

    new_alter = config_struct_alloc(c);
    if (!new_alter) {
        mmsdbg(DL_ERROR, "Failed to allocate Alternate config!");
        goto exit2;
    }

    client->configurator = c;
    osal_memcpy(&client->desc, client_desc, sizeof (client->desc));
    client->prv = client_prv;

    /* TODO: Already in the list? */
    osal_mutex_lock(c->lock_clients);
    list_add(&client->list_node, &c->clients_list);
    osal_mutex_unlock(c->lock_clients);

    osal_mutex_lock(c->lock_config);
    host = c->config_host;
    config_struct_lock(host);
    osal_mutex_unlock(c->lock_config);
    client->desc.registered(c, client, client->prv, host);
    config_struct_free(host);

    osal_mutex_lock(c->lock_configuring);
    /* Holding c->lock_configuring is enough to protect c->config_host */
    config_struct_set_host(new_alter, c->config_host);
    osal_memcpy(
            new_alter,
            c->config_host,
            c->config_struct_size
        );
    err = alternate(c, new_alter);
    if (err) {
        osal_mutex_unlock(c->lock_configuring);
        mmsdbg(DL_ERROR, "Alternate error!");
        goto exit3;
    }

    config_struct_lock(new_alter); /* Lock for notify below */ 
    alter = c->config_alter;
    c->config_alter = new_alter;

    osal_mutex_unlock(c->lock_configuring);

    config_struct_free(alter);

    osal_mutex_lock(c->lock_clients);
    list_for_each(list_node, &c->clients_list) {
    	cli = list_entry(list_node, configurator_client_t, list_node);
    	cli->desc.notify(c, cli, cli->prv, new_alter);
    }
    osal_mutex_unlock(c->lock_clients);

    config_struct_free(new_alter);

    return client;
exit3:
    config_struct_free(new_alter);
exit2:
    osal_free(client);
exit1:
    return NULL;
}

static int unregister_client(configurator_t *c, configurator_client_t *client)
{
    struct list_head *list_node;
    void *new_alter, *alter;
    void *host;
    configurator_client_t *cli;
    int err;

    new_alter = config_struct_alloc(c);
    if (!new_alter) {
        mmsdbg(DL_ERROR, "Failed to allocate Alternate config!");
        err = -1;
        goto exit1;
    }

    /* TODO: Already in the list? */
    osal_mutex_lock(c->lock_clients);
    list_del(&client->list_node);
    osal_mutex_unlock(c->lock_clients);

    osal_mutex_lock(c->lock_config);
    host = c->config_host;
    config_struct_lock(host);
    osal_mutex_unlock(c->lock_config);
    client->desc.unregistered(c, client, client->prv, host);
    config_struct_free(host);

    osal_mutex_lock(c->lock_configuring);
    /* Holding c->lock_configuring is enough to protect c->config_host */
    config_struct_set_host(new_alter, c->config_host);
    osal_memcpy(
            new_alter,
            c->config_host,
            c->config_struct_size
        );
    err = alternate(c, new_alter);
    if (err) {
        osal_mutex_unlock(c->lock_configuring);
        mmsdbg(DL_ERROR, "Alternate error!");
        goto exit2;
    }

    config_struct_lock(new_alter); /* Lock for notify below */ 
    alter = c->config_alter;
    c->config_alter = new_alter;

    osal_mutex_unlock(c->lock_configuring);

    config_struct_free(alter);

    osal_mutex_lock(c->lock_clients);
    list_for_each(list_node, &c->clients_list) {
    	cli = list_entry(list_node, configurator_client_t, list_node);
    	cli->desc.notify(c, cli, cli->prv, new_alter);
    }
    osal_mutex_unlock(c->lock_clients);

    config_struct_free(new_alter);
    osal_free(client);

    return 0;
exit2:
    config_struct_free(new_alter);
exit1:
    return err;
}

/*
* ***************************************************************************
* ***************************************************************************
* ** STM ********************************************************************
* ***************************************************************************
* ***************************************************************************
*/

static int configurator_stm_action_destroy(
            stm_t *stm,
            void *prv,
            unsigned int arg1,
            void *arg2
        )
{
    configurator_t *c;
    c = prv;
    config_struct_free(c->config_alter);
    config_struct_free(c->config_host);
    osal_mutex_destroy(c->lock_clients);
    osal_mutex_destroy(c->lock_config);
    osal_mutex_destroy(c->lock_configuring);
    pool_destroy(c->pool_clients);
    pool_destroy(c->pool_config);
    stm_destroy(c->stm);
    osal_free(c);
    return 0;
}

static int configurator_stm_action_get(
            stm_t *stm,
            void *prv,
            unsigned int arg1,
            void *arg2
        )
{
    return config_get(prv, arg1, arg2);
}

static int configurator_stm_action_set_begin(
            stm_t *stm,
            void *prv,
            unsigned int arg1,
            void *arg2
        )
{
    return config_set_begin(prv);
}

static int configurator_stm_action_set_end(
            stm_t *stm,
            void *prv,
            unsigned int arg1,
            void *arg2
        )
{
    return config_set_end(prv);
}

static int configurator_stm_action_set_cancel(
            stm_t *stm,
            void *prv,
            unsigned int arg1,
            void *arg2
        )
{
    config_set_cancel(prv);
    return 0;
}

static int configurator_stm_action_set(
            stm_t *stm,
            void *prv,
            unsigned int arg1,
            void *arg2
        )
{
    return config_set(prv, arg1, arg2);
}

static int configurator_stm_action_no_action(
            stm_t *stm,
            void *prv,
            unsigned int arg1,
            void *arg2
        )
{
    return 0;
}

static int configurator_stm_action_err(
            stm_t *stm,
            void *prv,
            unsigned int arg1,
            void *arg2
        )
{
    mmsdbg(DL_ERROR, "Invalid configurator state + operation!");
    return -1;
}

static stm_actions_t configurator_stm_actions[] = {
    configurator_stm_action_destroy,
    configurator_stm_action_get,
    configurator_stm_action_set_begin,
    configurator_stm_action_set_end,
    configurator_stm_action_set_cancel,
    configurator_stm_action_set,
    configurator_stm_action_no_action,
    configurator_stm_action_err
};

enum {
    CONFIGURATOR_STM_STATE_IDLE,
    CONFIGURATOR_STM_STATE_BEGIN,
    CONFIGURATOR_STM_STATE_CONFIG,
    CONFIGURATOR_STM_STATE_SET,
    CONFIGURATOR_STM_STATE_SET_ERR,
    CONFIGURATOR_STM_STATE_END,
    CONFIGURATOR_STM_STATE_DESTROY,
    CONFIGURATOR_STM_STATES_NUMBER
};

enum {
    CONFIGURATOR_STM_EVENT_DESTROY,
    CONFIGURATOR_STM_EVENT_GET,
    CONFIGURATOR_STM_EVENT_SET_BEGIN,
    CONFIGURATOR_STM_EVENT_SET_BEGIN_OK,
    CONFIGURATOR_STM_EVENT_SET_BEGIN_ERR,
    CONFIGURATOR_STM_EVENT_SET_END,
    CONFIGURATOR_STM_EVENT_SET_END_OK,
    CONFIGURATOR_STM_EVENT_SET_END_ERR,
    CONFIGURATOR_STM_EVENT_SET_CANCEL,
    CONFIGURATOR_STM_EVENT_SET,
    CONFIGURATOR_STM_EVENT_SET_OK,
    CONFIGURATOR_STM_EVENT_SET_ERR,
    CONFIGURATOR_STM_EVENTS_NUMBER
};

enum {
    CONFIGURATOR_STM_ACTION_DESTROY,
    CONFIGURATOR_STM_ACTION_GET,
    CONFIGURATOR_STM_ACTION_SET_BEGIN,
    CONFIGURATOR_STM_ACTION_SET_END,
    CONFIGURATOR_STM_ACTION_SET_CANCEL,
    CONFIGURATOR_STM_ACTION_SET,
    CONFIGURATOR_STM_ACTION_NO_ACTION,
    CONFIGURATOR_STM_ACTION_ERR,
    CONFIGURATOR_STM_ACTIONS_NUMBER
};

#define STM_STATE_PREFIX CONFIGURATOR_STM_STATE_
#define STM_ACTIOIN_PREFIX CONFIGURATOR_STM_ACTION_

STM_DESC_BUILD_BEGIN(
            configurator_stm,
            CONFIGURATOR_STM_STATE_IDLE,
            CONFIGURATOR_STM_STATES_NUMBER,
            CONFIGURATOR_STM_EVENTS_NUMBER,
            CONFIGURATOR_STM_ACTIONS_NUMBER
        )
                           /* |-IDLE------------|  |-BEGIN-----------|  |-CONFIG----------|  |-SET------------| */
/* DESTROY       */ STM_E4x(  DESTROY,   DESTROY,  BEGIN  ,       ERR,  CONFIG ,       ERR,  SET    ,       ERR  )
/* GET           */ STM_E4x(  IDLE   ,       GET,  BEGIN  ,       GET,  CONFIG ,       GET,  SET    ,       GET  )
/* SET_BEGIN     */ STM_E4x(  BEGIN  , SET_BEGIN,  BEGIN  ,       ERR,  CONFIG ,       ERR,  SET    ,       ERR  )
/* SET_BEGIN_OK  */ STM_E4x(  IDLE   ,       ERR,  CONFIG , NO_ACTION,  CONFIG ,       ERR,  SET    ,       ERR  )
/* SET_BEGIN_ERR */ STM_E4x(  IDLE   ,       ERR,  IDLE   , NO_ACTION,  CONFIG ,       ERR,  SET    ,       ERR  )
/* SET_END       */ STM_E4x(  IDLE   ,       ERR,  BEGIN  ,       ERR,  END    ,   SET_END,  SET    ,       ERR  )
/* SET_END_OK    */ STM_E4x(  IDLE   ,       ERR,  BEGIN  ,       ERR,  CONFIG ,       ERR,  SET    , NO_ACTION  )
/* SET_END_ERR   */ STM_E4x(  IDLE   ,       ERR,  BEGIN  ,       ERR,  CONFIG ,       ERR,  SET    , NO_ACTION  )
/* SET_CANCEL    */ STM_E4x(  IDLE   ,       ERR,  BEGIN  ,       ERR,  IDLE   ,SET_CANCEL,  SET    ,       ERR  )
/* SET           */ STM_E4x(  IDLE   ,       ERR,  BEGIN  ,       ERR,  SET    ,       SET,  SET    ,       ERR  )
/* SET_OK        */ STM_E4x(  IDLE   ,       ERR,  BEGIN  ,       ERR,  CONFIG ,       SET,  CONFIG , NO_ACTION  )
/* SET_ERR       */ STM_E4x(  IDLE   ,       ERR,  BEGIN  ,       ERR,  CONFIG ,       SET,  SET_ERR, NO_ACTION  )

                           /* |-SET_ERR---------|  |-END-------------|  |-DESTROY--------|                      */
/* DESTROY       */ STM_E3x(  SET_ERR,       ERR,  END    ,       ERR,  DESTROY,       ERR                       )
/* GET           */ STM_E3x(  SET_ERR,       GET,  END    ,       GET,  DESTROY,       ERR                       )
/* SET_BEGIN     */ STM_E3x(  SET_ERR,       ERR,  END    ,       ERR,  DESTROY,       ERR                       )
/* SET_BEGIN_OK  */ STM_E3x(  SET_ERR,       ERR,  END    ,       ERR,  DESTROY,       ERR                       )
/* SET_BEGIN_ERR */ STM_E3x(  SET_ERR,       ERR,  END    ,       ERR,  DESTROY,       ERR                       )
/* SET_END       */ STM_E3x(  SET_ERR,       ERR,  END    ,       ERR,  DESTROY,       ERR                       )
/* SET_END_OK    */ STM_E3x(  SET_ERR,       ERR,  IDLE   , NO_ACTION,  DESTROY,       ERR                       )
/* SET_END_ERR   */ STM_E3x(  SET_ERR,       ERR,  IDLE   , NO_ACTION,  DESTROY,       ERR                       )
/* SET_CANCEL    */ STM_E3x(  IDLE   ,SET_CANCEL,  END    ,       ERR,  DESTROY,       ERR                       )
/* SET           */ STM_E3x(  SET_ERR,       ERR,  END    ,       ERR,  DESTROY,       ERR                       )
/* SET_OK        */ STM_E3x(  SET_ERR,       ERR,  END    ,       ERR,  DESTROY,       ERR                       )
/* SET_ERR       */ STM_E3x(  SET_ERR,       ERR,  END    ,       ERR,  DESTROY,       ERR                       )

STM_DESC_BUILD_END(configurator_stm)

#define CFGR_STM(EVENT,N,P) \
    stm_event_atomic_state( \
            c->stm, \
            EVENT, \
            c, \
            N, \
            P \
        )

/*********************************************
* Client Interface ***************************
**********************************************/
configurator_client_t *configurator_client_register(
        configurator_t *c,
        configurator_client_desc_t *client_desc,
        void *client_prv
    )
{
    return register_client(c, client_desc, client_prv);
}

int configurator_client_unregister(
        configurator_t *c,
        configurator_client_t *client
    )
{
    return unregister_client(c, client);
}

void configurator_config_lock(void *config)
{
    config_struct_lock(config);
}

void configurator_config_unlock(void *config)
{
    config_struct_free(config);
}

/*********************************************
* Source Interface ***************************
**********************************************/
int configurator_get(configurator_t *c, unsigned int index, void *data)
{
    return CFGR_STM(CONFIGURATOR_STM_EVENT_GET, index, data);
}

int configurator_set_begin(configurator_t *c)
{
    int err;
    err = CFGR_STM(CONFIGURATOR_STM_EVENT_SET_BEGIN, 0, NULL);
    if (err) {
        CFGR_STM(CONFIGURATOR_STM_EVENT_SET_BEGIN_ERR, 0, NULL);
    } else {
        CFGR_STM(CONFIGURATOR_STM_EVENT_SET_BEGIN_OK, 0, NULL);
    }
    return err;
}

int configurator_set_end(configurator_t *c)
{
    int err;
    err = CFGR_STM(CONFIGURATOR_STM_EVENT_SET_END, 0, NULL);
    if (err) {
        CFGR_STM(CONFIGURATOR_STM_EVENT_SET_END_ERR, 0, NULL);
    } else {
        CFGR_STM(CONFIGURATOR_STM_EVENT_SET_END_OK, 0, NULL);
    }
    return err;
}

void configurator_set_cancel(configurator_t *c)
{
    CFGR_STM(CONFIGURATOR_STM_EVENT_SET_CANCEL, 0, NULL);
}

int configurator_set(configurator_t *c, unsigned int index, void *data)
{
    int err;
    err = CFGR_STM(CONFIGURATOR_STM_EVENT_SET, index, data);
    if (err) {
        CFGR_STM(CONFIGURATOR_STM_EVENT_SET_ERR, 0, NULL);
    } else {
        CFGR_STM(CONFIGURATOR_STM_EVENT_SET_OK, 0, NULL);
    }
    return err;
}

/*********************************************
* Common Interface ***************************
**********************************************/
void configurator_config_touch(configurator_config_struct_node_t *node)
{
    configurator_config_struct_node_t *n;
    unsigned int offset;

    n = node;
    n->seq_num++;
    while (n->parrent_offset != CONFIGURATOR_CONFIG_STRUCT_ROOT_NODE) {
        n = (configurator_config_struct_node_t *)((char *)n - n->parrent_offset);
        n->seq_num++;
    }
    offset = (char *)node - (char *)n;

    n = config_struct_get_host(n);
    if (n) {
        n = (configurator_config_struct_node_t *)((char *)n + offset);
        n->seq_num++;
        while (n->parrent_offset != CONFIGURATOR_CONFIG_STRUCT_ROOT_NODE) {
            n = (configurator_config_struct_node_t *)((char *)n - n->parrent_offset);
            n->seq_num++;
        }
    }
}

void configurator_destroy(configurator_t *c)
{
    CFGR_STM(CONFIGURATOR_STM_EVENT_DESTROY, 0, NULL);
}

configurator_t *configurator_create(
        configurator_create_param_t *create_param
    )
{
    configurator_t *c;
    int err;

    c = osal_malloc(sizeof(*c));
    if (!c) {
        mmsdbg(
                DL_ERROR,
                "Failed to allocate memory for new Configurator instance: size=%d!",
                sizeof (*c)
            );
        goto exit1;
    }
    osal_memset(c, 0, sizeof (*c));

    c->plugin = create_param->plugin;
    c->plugin_prv = create_param->plugin_prv;
    c->config_struct_size = create_param->plugin->config_struct_size;

    INIT_LIST_HEAD(&c->clients_list);

    c->stm = stm_create(
            "Configurator",
            &configurator_stm,
            configurator_stm_actions
        );
    if (!c->stm) {
        mmsdbg(DL_ERROR, "Failed to create Configurator STM!");
        goto exit2;
    }

    c->pool_config = pool_create("CONFIG POOL",
            CALC_CONF_STRUCT_SIZE(c->config_struct_size),
            create_param->num_of_configs
        );
    if (!c->pool_config) {
        mmsdbg(DL_ERROR, "Failed to create pool for configuration structs!");
        goto exit3;
    }

    c->pool_clients = pool_create("CONFIG CLIETN POOL",
            sizeof (configurator_client_t),
            create_param->num_of_configs
        );
    if (!c->pool_clients) {
        mmsdbg(DL_ERROR, "Failed to create pool for clients!");
        goto exit4;
    }

    c->lock_configuring = osal_mutex_create();
    if (!c->lock_configuring) {
        mmsdbg(DL_ERROR, "Failed to create configuring mutex!");
        goto exit5;
    }

    c->lock_config = osal_mutex_create();
    if (!c->lock_config) {
        mmsdbg(DL_ERROR, "Failed to create config mutex!");
        goto exit6;
    }

    c->lock_clients = osal_mutex_create();
    if (!c->lock_clients) {
        mmsdbg(DL_ERROR, "Failed to create clients mutex!");
        goto exit7;
    }

    c->config_host = config_struct_alloc(c);
    if (!c->config_host) {
        mmsdbg(DL_ERROR, "Failed to allocate initial config!");
        goto exit8;
    }

    c->config_alter = config_struct_alloc(c);
    if (!c->config_alter) {
        mmsdbg(DL_ERROR, "Failed to allocate initial Alter config!");
        goto exit9;
    }

    err = c->plugin->set_to_default(c, c->plugin_prv, c->config_host);
    if (err) {
        mmsdbg(DL_ERROR, "Failed to set defaults!");
        goto exit10;
    }
    osal_memcpy(
            c->config_alter,
            c->config_host,
            c->config_struct_size
        );

    return c;
exit10:
    config_struct_free(c->config_alter);
exit9:
    config_struct_free(c->config_host);
exit8:
    osal_mutex_destroy(c->lock_clients);
exit7:
    osal_mutex_destroy(c->lock_config);
exit6:
    osal_mutex_destroy(c->lock_configuring);
exit5:
    pool_destroy(c->pool_clients);
exit4:
    pool_destroy(c->pool_config);
exit3:
    stm_destroy(c->stm);
exit2:
    osal_free(c);
exit1:
    return NULL;
}

