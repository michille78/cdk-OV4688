/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file camera3.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <osal/osal_stdio.h>
#include <osal/osal_stdlib.h>
#include <osal/osal_mutex.h>
#include <osal/osal_string.h>
#include <osal/osal_time.h>
#include <utils/mms_debug.h>

#include <camera.h>
#include <camera_config_index.h>
#include <camera_config_struct.h>

#include <guzzi/camera3/camera3.h>

typedef struct {
    guzzi_camera3_controls_stream_config_id_t id;
    guzzi_camera3_controls_stream_config_format_t format;
    guzzi_camera3_controls_stream_config_width_t width;
    guzzi_camera3_controls_stream_config_height_t height;
    guzzi_camera3_controls_stream_config_type_t type;
} config_camera_streams_t;

typedef struct {
    guzzi_camera3_controls_capture_request_guzzi_fr_id_t guzzi_fr_id;
    guzzi_camera3_controls_capture_request_frame_number_t frame_number;
    guzzi_camera3_controls_capture_request_active_t active;
} capture_request_t;

struct guzzi_camera3 {
    camera_t *camera;
    osal_sem *camera_command_sem;
    int camera_start_err;

    guzzi_camera3_calback_notify_t *callback_notify;
    void *client_prv;

    config_camera_streams_t streams;
    int active_streams_count;

    capture_request_t capture_request;

    int camera_started;
};

mmsdbg_define_variable(
        vdl_guzzi_camera3,
        DL_DEFAULT,
        0,
        "vdl_guzzi_camera3",
        "Guzzi Camera3 interface."
    );
#define MMSDEBUGLEVEL mmsdbg_use_variable(vdl_guzzi_camera3)

extern guzzi_camera3_metadata_static_t guzzi_camera3_metadata_static_init;
extern guzzi_camera3_metadata_controls_t guzzi_camera3_metadata_controls_init;

/*
 * ****************************************************************************
 * ** Request static/default **************************************************
 * ****************************************************************************
 */
static void camera_callback(
        camera_t *camera,
        void *prv,
        camera_event_t event,
        int num,
        void *ptr
    )
{
    guzzi_camera3_t *gc3;
    osal_timeval time;

    gc3 = prv;

    switch (event.type) {
        case CAMERA_EVENT_ERROR:
            mmsdbg(DL_ERROR, "Camera CB ERROR event.");
            break;
        case CAMERA_EVENT_ERROR_START:
            mmsdbg(DL_ERROR, "Camera CB ERROR_START event.");
            gc3->camera_start_err = -1;
            osal_sem_post(gc3->camera_command_sem);
            break;
        case CAMERA_EVENT_START_DONE:
            mmsdbg(DL_PRINT, "Camera CB START_DONE event.");
            osal_sem_post(gc3->camera_command_sem);
            break;
        case CAMERA_EVENT_STOP_DONE:
            mmsdbg(DL_PRINT, "Camera CB STOP_DONE event.");
            osal_sem_post(gc3->camera_command_sem);
            break;
        case CAMERA_EVENT_FLUSH_DONE:
            mmsdbg(DL_PRINT, "Camera CB FLUSH_DONE event.");
            osal_sem_post(gc3->camera_command_sem);
            break;
        case CAMERA_EVENT_PROCESS_DONE:
            mmsdbg(DL_ERROR, "Camera CB PROCESS_DONE event.");
            break;
        case CAMERA_EVENT_BUFFER_FLUSH:
            mmsdbg(DL_ERROR, "Camera CB BUFFER_FLUSH event.");
            break;
        case CAMERA_EVENT_GENERIC:
            mmsdbg(DL_ERROR, "Camera CB GENERIC event: sub_type=%d, num=%d.", event.sub_type, num);
            static guzzi_camera3_event_shutter_t shutter;

            osal_get_time(&time);

            shutter.frame_number += 1;
            shutter.timestamp = time;

            gc3->callback_notify(
                    gc3,
                    gc3->client_prv,
                    GUZZI_CAMERA3_EVENT_SHUTTER,
                    0,
                    &shutter
                );
            break;
        default:
            mmsdbg(DL_ERROR, "Unkonw camera event (%d)!", event.v);
    }
}

/*
 * ****************************************************************************
 * ** Local manage active strams **********************************************
 * ****************************************************************************
 */
static int manage_active_streams(
        guzzi_camera3_t *gc3,
        guzzi_camera3_stream_configuration_t *stream_configuration
    )
{
    int i;

    if (GUZZI_CAMERA3_STREAMS_MAX < stream_configuration->num_streams) {
        mmsdbg(
                DL_ERROR,
                "Too many streams: num_streams=%d, max=%d!",
                stream_configuration->num_streams,
                GUZZI_CAMERA3_STREAMS_MAX
            );
        return -1;
    }

    for (i = 0; i < stream_configuration->num_streams; i++) {
        gc3->streams.format.v[i] = stream_configuration->streams[i].format;
        gc3->streams.width.v[i]  = stream_configuration->streams[i].width;
        gc3->streams.height.v[i] = stream_configuration->streams[i].height;
        gc3->streams.type.v[i]   = stream_configuration->streams[i].type;
        gc3->streams.id.v[i]     = stream_configuration->streams[i].id;

        gc3->streams.format.dim_size_1 = \
            GUZZI_CAMERA3_CONTROLS_STREAM_CONFIG_FORMAT_DIM_MAX_SIZE_1;
        gc3->streams.width.dim_size_1 = \
            GUZZI_CAMERA3_CONTROLS_STREAM_CONFIG_WIDTH_DIM_MAX_SIZE_1;
        gc3->streams.height.dim_size_1 = \
            GUZZI_CAMERA3_CONTROLS_STREAM_CONFIG_HEIGHT_DIM_MAX_SIZE_1;
        gc3->streams.type.dim_size_1 = \
            GUZZI_CAMERA3_CONTROLS_STREAM_CONFIG_TYPE_DIM_MAX_SIZE_1;
        gc3->streams.id.dim_size_1 = \
            GUZZI_CAMERA3_CONTROLS_STREAM_CONFIG_ID_DIM_MAX_SIZE_1;
    }
    gc3->active_streams_count = stream_configuration->num_streams;

    return 0;
}

/*
 * ****************************************************************************
 * ** Config camera streams ***************************************************
 * ****************************************************************************
 */
static int config_camera_streams(guzzi_camera3_t *gc3)
{
    int err;

#define SET(IDX,DATA) \
    do { \
        err = camera_config_set(gc3->camera, IDX, DATA); \
        if (err) { \
            mmsdbg(DL_ERROR, "Failed to set camera stream config: " #IDX); \
            camera_config_set_cancel(gc3->camera); \
            return -1; \
        } \
    } while (0)

    err = camera_config_set_begin(gc3->camera);
    if (err) {
        mmsdbg(DL_ERROR, "Failed to begin camera config!");
        return -1;
    }

    SET(GUZZI_CAMERA3_INDEX_CONTROLS_STREAM_CONFIG_ID    , &gc3->streams.id    );
    SET(GUZZI_CAMERA3_INDEX_CONTROLS_STREAM_CONFIG_FORMAT, &gc3->streams.format);
    SET(GUZZI_CAMERA3_INDEX_CONTROLS_STREAM_CONFIG_WIDTH , &gc3->streams.width );
    SET(GUZZI_CAMERA3_INDEX_CONTROLS_STREAM_CONFIG_HEIGHT, &gc3->streams.height);
    SET(GUZZI_CAMERA3_INDEX_CONTROLS_STREAM_CONFIG_TYPE  , &gc3->streams.type  );

    err = camera_config_set_end(gc3->camera);
    if (err) {
        mmsdbg(DL_ERROR, "Failed to end camera config!");
        return -1;
    }

#undef SET

    return 0;
}

/*
 * ****************************************************************************
 * ** Set active streams ******************************************************
 * ****************************************************************************
 */
static void set_active_streams(
        guzzi_camera3_t *gc3,
        guzzi_camera3_capture_request_t *capture_request
    )
{
    int i, j;

    gc3->capture_request.active.dim_size_1 =
        GUZZI_CAMERA3_CONTROLS_CAPTURE_REQUEST_ACTIVE_DIM_MAX_SIZE_1;
    for (i = 0; i < gc3->capture_request.active.dim_size_1; i++) {
        gc3->capture_request.active.v[i] =
            GUZZI_CAMERA3_ENUM_CAPTURE_REQUEST_ACTIVE_OFF;
    }

    for (i = 0; capture_request->streams_id[i]; i++) {
        if (GUZZI_CAMERA3_STREAMS_MAX <= i) {
            mmsdbg(
                    DL_ERROR,
                    "Too many streams: max=%d!",
                    GUZZI_CAMERA3_STREAMS_MAX
                );
            return;
        }
        for (j = 0; j < gc3->active_streams_count; j++) {
            if (gc3->streams.id.v[j] == capture_request->streams_id[i]) {
                gc3->capture_request.active.v[i] =
                    GUZZI_CAMERA3_ENUM_CAPTURE_REQUEST_ACTIVE_ON;
                break;
            }
        }
        if (gc3->active_streams_count <= j) {
            mmsdbg(
                    DL_ERROR,
                    "Stream id=%d is not active!",
                    capture_request->streams_id[i]
                );
        }
    }

    return;
}

/*
 * ****************************************************************************
 * ** Local start/stop ********************************************************
 * ****************************************************************************
 */
static int start_camera(guzzi_camera3_t *gc3)
{
    int err;

    err = camera_start(gc3->camera);
    if (err) {
        mmsdbg(DL_ERROR, "Failed to start camera!");
        goto exit1;
    }
    osal_sem_wait(gc3->camera_command_sem);
    err = gc3->camera_start_err;
    if (err) {
        mmsdbg(DL_ERROR, "Failed to start camera!");
        goto exit1;
    }

exit1:
    return err;
}

static void stop_camera(guzzi_camera3_t *gc3)
{
    camera_flush(gc3->camera);
    osal_sem_wait(gc3->camera_command_sem);
    camera_stop(gc3->camera);
    osal_sem_wait(gc3->camera_command_sem);
}

/*
 * ****************************************************************************
 * ****************************************************************************
 * ** Interface Part **********************************************************
 * ****************************************************************************
 * ****************************************************************************
 */

/*
 * ****************************************************************************
 * ** Get camera id ***********************************************************
 * ****************************************************************************
 */
int guzzi_camera3_get_id(guzzi_camera3_t *gc3)
{
    return camera_get_id(gc3->camera);
}

/*
 * ****************************************************************************
 * ** Get number of cameras ***************************************************
 * ****************************************************************************
 */
int guzzi_camera3_get_number_of_cameras(void)
{
    return 1;
}

/*
 * ****************************************************************************
 * ** Get camera info *********************************************************
 * ****************************************************************************
 */
int guzzi_camera3_get_info(
        int camera_id,
        guzzi_camera3_info_t *gc3_info
    )
{
    gc3_info->facing = GUZZI_CAMERA3_FACING_BACK;
    gc3_info->orientation = 0;
    osal_memcpy(
            gc3_info->metadata_static,
            &guzzi_camera3_metadata_static_init,
            sizeof (*gc3_info->metadata_static)
        );
    return 0;
}

/*
 * ****************************************************************************
 * ** Request default *********************************************************
 * ****************************************************************************
 */
int guzzi_camera3_request_defaults(
        guzzi_camera3_t *gc3,
        guzzi_camera3_metadata_controls_t *metadata,
        guzzi_camera3_template_t type
    )
{
    osal_memcpy(
            metadata,
            &guzzi_camera3_metadata_controls_init,
            sizeof (*metadata)
        );
    return 0;
}

/*
 * ****************************************************************************
 * ** Stream Config ***********************************************************
 * ****************************************************************************
 */
int guzzi_camera3_config_streams(
        guzzi_camera3_t *gc3,
        guzzi_camera3_stream_configuration_t *stream_configuration
    )
{
    int err;

    err = manage_active_streams(gc3, stream_configuration);
    if (err) {
        return err;
    }

    if (!gc3->camera_started && gc3->active_streams_count) {
        err = config_camera_streams(gc3);
        if (err) {
            return err;
        }
        err = start_camera(gc3);
        if (err) {
            return err;
        }
        gc3->camera_started = 1;
    }
    if (gc3->camera_started && !gc3->active_streams_count) {
        stop_camera(gc3);
        gc3->camera_started = 0;
        return 0;
    }

    return 0;
}

/*
 * ****************************************************************************
 * ** Capture Request *********************************************************
 * ****************************************************************************
 */
int guzzi_camera3_capture_request(
        guzzi_camera3_t *gc3,
        guzzi_camera3_capture_request_t *capture_request
    )
{
    guzzi_camera3_metadata_t *metadata;
    int err;

#define SET(IDX,DATA) \
    do { \
        err = camera_config_set(gc3->camera, IDX, DATA); \
        if (err) { \
            mmsdbg(DL_ERROR, "Failed to set camera config!"); \
            camera_config_set_cancel(gc3->camera); \
            return -1; \
        } \
    } while (0)


    PROFILE_ADD(PROFILE_ID_GUZZI_CAMERA3_CAP_REQ, 0, 0);

    err = camera_config_set_begin(gc3->camera);
    if (err) {
        mmsdbg(DL_ERROR, "Failed to begin camera config!");
        return -1;
    }

    gc3->capture_request.guzzi_fr_id.v++;
    gc3->capture_request.frame_number.v = capture_request->frame_number;
    set_active_streams(gc3, capture_request);

    SET(GUZZI_CAMERA3_INDEX_CONTROLS_CAPTURE_REQUEST_GUZZI_FR_ID , &gc3->capture_request.guzzi_fr_id);
    SET(GUZZI_CAMERA3_INDEX_CONTROLS_CAPTURE_REQUEST_FRAME_NUMBER, &gc3->capture_request.frame_number);
    SET(GUZZI_CAMERA3_INDEX_CONTROLS_CAPTURE_REQUEST_ACTIVE      , &gc3->capture_request.active);

    for_each_metadata(capture_request->settings, metadata) {
        if ((char *)capture_request->settings + sizeof (guzzi_camera3_metadata_controls_struct_t) <= (char *)metadata) {
            mmsdbg(
                    DL_ERROR,
                    "Settings buffer bigger than expected: "
                    "sizeof (guzzi_camera3_metadata_controls_struct_t) = %dbytes!",
                    sizeof (guzzi_camera3_metadata_controls_struct_t)
                );
            camera_config_set_cancel(gc3->camera);
            return -1;
        }
        if (0 == metadata->identity.size) {
            mmsdbg(DL_ERROR, "Wrong metadata entry: metadata->identity.size == 0!");
            camera_config_set_cancel(gc3->camera);
            return -1;
        }
        SET(metadata->identity.index, metadata->v);
    }

    err = camera_config_set_end(gc3->camera);
    if (err) {
        mmsdbg(DL_ERROR, "Failed to end camera config!");
        return -1;
    }

#undef SET

    return 0;
}

void guzzi_camera3_flush(guzzi_camera3_t *gc3)
{
    camera_flush(gc3->camera);
    osal_sem_wait(gc3->camera_command_sem);
}

/*
 * ****************************************************************************
 * ** Create/Destroy **********************************************************
 * ****************************************************************************
 */
void guzzi_camera3_destroy(guzzi_camera3_t *gc3)
{
    camera_destroy(gc3->camera);
    osal_sem_destroy(gc3->camera_command_sem);
    osal_free(gc3);
}

guzzi_camera3_t *guzzi_camera3_create(
        int camera_id,
        guzzi_camera3_calback_notify_t *callback_notify,
        void *client_prv
    )
{
    guzzi_camera3_t *gc3;
    camera_create_params_t create_params;

    gc3 = osal_calloc(1, sizeof (*gc3));
    if (!gc3) {
        mmsdbg(
                DL_ERROR,
                "Failed to allocate memory for new instance: size=%d!",
                sizeof (*gc3)
            );
        goto exit1;
    }

    gc3->camera_command_sem = osal_sem_create(0);
    if (!gc3->camera_command_sem) {
        mmsdbg(DL_ERROR, "Failed to create camera command semphore!");
        goto exit2;
    }

    create_params.camera_id = camera_id;
    create_params.mode = CAMERA_MODE_MOV_CONFIG_DRIVEN;
    create_params.dtp_server = NULL;
    create_params.callback = camera_callback;
    create_params.app_prv = gc3;
    gc3->camera = camera_create(&create_params);
    if (!gc3->camera) {
        mmsdbg(DL_ERROR, "Failed to create camera!");
        goto exit3;
    }

    gc3->camera_start_err = 0;
    gc3->camera_started = 0;
    gc3->callback_notify = callback_notify;
    gc3->client_prv = client_prv;
    gc3->capture_request.guzzi_fr_id.v = 0;

    return gc3;
exit3:
    osal_sem_destroy(gc3->camera_command_sem);
exit2:
    osal_free(gc3);
exit1:
    return NULL;
}

