#if defined(X11_DISPLAY_ENABLE)
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>

#include "crr_x11_disp.h"

#define max(x, y) (((x) > (y)) ? (x) : (y))
#define min(x, y) (((x) < (y)) ? (x) : (y))

typedef struct {
    char *display_name;
    unsigned int width;
    unsigned int height;
    crr_disp_image_format_t format;

    char *rgb32;

    Display *display;
    Visual *visual;
    Window window;
    XImage *ximage;
} x11_disp_prv_t;

static void yuv2rgb(unsigned char *rgb, int y, int u, int v)
{
    int r, g, b;
    r = y            + 1.402f*u;
    g = y - 0.344f*v - 0.714f*u;
    b = y + 1.772f*v           ;
    rgb[3] = 255;
    rgb[2] = min(max(r, 0), 255);
    rgb[1] = min(max(g, 0), 255);
    rgb[0] = min(max(b, 0), 255);
}

static void conv_uyvy_to_rgb24(
        void *rgb24,
        void *uyvy,
        unsigned int width,
        unsigned int height
    )
{
    unsigned int l, c;
    int y1, y2, u, v;
    unsigned int line_inc_rgb, line_inc_yuv;
    unsigned char *rgb, *yuv;

    rgb = rgb24; yuv = uyvy;
    line_inc_rgb = 0; line_inc_yuv = 0;

    for (l = 0; l < height; l++) {
        for (c = 0; c < width/2; c++) {
            y1 = yuv[1]; y2 = yuv[3];
            u  = yuv[0]; v  = yuv[2];

            u -= 128; v -= 128;

            yuv2rgb(rgb, y1, u, v);
            rgb += 4;
            yuv2rgb(rgb, y2, u, v);
            rgb += 4;

            yuv += 4;
        }
        rgb += line_inc_rgb;
        yuv += line_inc_yuv;
    }
}

static void conv_nv12_to_rgb24(
        void *rgb24,
        void *nv12,
        unsigned int width,
        unsigned int height
    )
{
    unsigned int l, c;
    int y11, y12, y21, y22, u, v;
    unsigned int line_inc_rgb, line_inc_yuv;
    unsigned char *rgb1, *rgb2, *yuv1, *yuv2, *uv;

    rgb1 = rgb24;
    rgb2 = rgb1 + 4*width;
    yuv1 = nv12;
    yuv2 = yuv1 + width;
    uv   = yuv1 + width*height;
    line_inc_rgb = 4*width; line_inc_yuv = width;

    for (l = 0; l < height/2; l++) {
        for (c = 0; c < width/2; c++) {
            y11 = yuv1[1]; y12 = yuv1[2];
            y21 = yuv2[1]; y22 = yuv2[2];
            u   = uv[0]  ; v   = uv[1];

            u -= 128; v -= 128;

            yuv2rgb(rgb1, y11, u, v);
            rgb1 += 4;
            yuv2rgb(rgb1, y12, u, v);
            rgb1 += 4;
            yuv2rgb(rgb2, y21, u, v);
            rgb2 += 4;
            yuv2rgb(rgb2, y22, u, v);
            rgb2 += 4;

            yuv1 += 2; yuv2 += 2; uv += 2;
        }
        rgb1 += line_inc_rgb;
        rgb2 += line_inc_rgb;
        yuv1 += line_inc_yuv;
        yuv2 += line_inc_yuv;
    }
}

void crr_x11_disp_show(
        crr_x11_disp_t disp,
        void *img
    )
{
    x11_disp_prv_t *x;

    x = disp;

    switch (x->format) {
        case DISP_IMAGE_FORMAT_UYVY:
            conv_uyvy_to_rgb24(
                    x->ximage->data,
                    img,
                    x->ximage->width,
                    x->ximage->height
                );
            break;
        case DISP_IMAGE_FORMAT_NV12:
            conv_nv12_to_rgb24(
                    x->ximage->data,
                    img,
                    x->ximage->width,
                    x->ximage->height
                );
            break;
        case DISP_IMAGE_FORMAT_RGBA8888:
            memcpy(
                    x->ximage->data,
                    img,
                    4 * x->width * x->height
                );
            break;
        default:
            break;
    }

    XPutImage(
            x->display,                 /* display  */
            x->window,                  /* d        */
            DefaultGC(x->display, 0),   /* gc       */
            x->ximage,                  /* image    */
            0,                          /* src_x    */
            0,                          /* src_y    */
            0,                          /* dest_x   */
            0,                          /* dest_y   */
            x->width,                   /* width    */
            x->height                   /* height   */
        );
}

void crr_x11_disp_destroy(crr_x11_disp_t disp)
{
    x11_disp_prv_t *x;
    x = disp;
    //free(x->rgb32);
    XDestroyImage(x->ximage);
    XDestroyWindow(x->display, x->window);
    XCloseDisplay(x->display);
    free(x);
}

crr_x11_disp_t crr_x11_disp_create(crr_x11_disp_create_t *create)
{
    x11_disp_prv_t *x;

    x = malloc(sizeof(x11_disp_prv_t));

    x->display_name = create->display_name;
    x->width = create->width;
    x->height = create->height;
    x->format = create->format;

    x->rgb32 = malloc(
            4 * x->width * x->height
        );

    x->display = XOpenDisplay(x->display_name);
    x->visual = DefaultVisual(x->display, 0);
    x->window = XCreateSimpleWindow(
            x->display,                 /* display      */
            RootWindow(x->display, 0),  /* parent       */
            0,                          /* x            */
            0,                          /* y            */
            x->width,                   /* width        */
            x->height,                  /* height       */
            1,                          /* border_width */
            0,                          /* border       */
            0                           /* background   */
        );
    x->ximage = XCreateImage(
            x->display, /* display          */
            x->visual,  /* visual           */
            24,         /* depth            */
            ZPixmap,    /* format           */
            0,          /* offset           */
            x->rgb32,   /* data             */
            x->width,   /* width            */
            x->height,  /* height           */
            32,         /* bitmap_pad       */
            0           /* bytes_per_line   */
        );
    XSelectInput(
            x->display,
            x->window,
            ButtonPressMask | ExposureMask
        );
    XMapWindow(x->display, x->window);

    XEvent ev;
    XNextEvent(x->display, &ev);

    return x;
}

#else   // X11_DISPLAY_ENABLE

#include "crr_x11_disp.h"

void crr_x11_disp_show(crr_x11_disp_t disp, void *img)
{
}

void crr_x11_disp_destroy(crr_x11_disp_t disp)
{
}

crr_x11_disp_t crr_x11_disp_create(crr_x11_disp_create_t *create){
    return (crr_x11_disp_t)0;
}
#endif   // X11_DISPLAY_ENABLE
