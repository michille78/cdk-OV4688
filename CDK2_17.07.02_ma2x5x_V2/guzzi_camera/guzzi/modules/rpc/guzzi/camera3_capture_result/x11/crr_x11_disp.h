#ifndef _CRR_X11_DISP_H
#define _CRR_X11_DISP_H

typedef void * crr_x11_disp_t;

typedef enum {
    DISP_IMAGE_FORMAT_UYVY,
    DISP_IMAGE_FORMAT_NV12,
    DISP_IMAGE_FORMAT_RGBA8888
} crr_disp_image_format_t;

typedef struct {
    char *display_name;
    unsigned int width;
    unsigned int height;
    crr_disp_image_format_t format;
} crr_x11_disp_create_t;

void crr_x11_disp_show(crr_x11_disp_t disp, void *img);
void crr_x11_disp_destroy(crr_x11_disp_t disp);
crr_x11_disp_t crr_x11_disp_create(crr_x11_disp_create_t *create);

#endif /* _CRR_X11_DISP_H */

