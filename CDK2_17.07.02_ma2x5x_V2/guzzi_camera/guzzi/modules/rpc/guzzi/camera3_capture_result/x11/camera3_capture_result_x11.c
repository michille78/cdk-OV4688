/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <utils/mms_debug.h>
#include <guzzi/camera3_capture_result/camera3_capture_result.h>

#include <gzz_cam_config.h>
#include "crr_x11_disp.h"

mmsdbg_define_variable(
        vdl_camera3_capture_result_x11,
        DL_DEFAULT,
        0,
        "vdl_camera3_capture_result_x11",
        "Guzzi Camera3 capture result X11."
    );
#define MMSDEBUGLEVEL mmsdbg_use_variable(vdl_camera3_capture_result_x11)

#define CAMERA_ID_INVALID (-1)

typedef struct {
    int camera_id;
    gzz_cam_stream_t streams[GZZ_CAM_CONFIG_MAX_STREAMS];
    crr_x11_disp_t x11[GZZ_CAM_CONFIG_MAX_STREAMS];
} stream_info_t;

#define MAX_CAMERAS 8
static stream_info_t streams_info[MAX_CAMERAS];

void guzzi_camera3_capture_result__x11_configure_streams(
        int camera_id,
        gzz_cam_stream_t *streams
    )
{
    stream_info_t *stream_info;
    int i, j;

    for (i = 0; i < ARRAY_SIZE(streams_info); i++) {
        stream_info = streams_info + i;
        if (camera_id == stream_info->camera_id) {
            stream_info->camera_id = CAMERA_ID_INVALID;
            for (j = 0; j < ARRAY_SIZE(stream_info->x11); j++) {
                if (stream_info->x11[j]) {
                    crr_x11_disp_destroy(stream_info->x11[j]);
                    stream_info->x11[j] = NULL;
                }
            }
        }
    }

    stream_info = NULL;
    for (i = 0; i < ARRAY_SIZE(streams_info); i++) {
        if (CAMERA_ID_INVALID == streams_info[i].camera_id) {
            stream_info = streams_info + i;
            break;
        }
    }
    if (ARRAY_SIZE(streams_info) <= i) {
        mmsdbg(DL_ERROR, "Unable to find empty camera slot!");
        return;
    }

    stream_info->camera_id = camera_id;
    memcpy(
            stream_info->streams,
            streams,
            sizeof (stream_info->streams)
        );
}

void guzzi_camera3_capture_result__x11_delete(void)
{
    stream_info_t *stream_info;
    int i, j;
    for (i = 0; i < ARRAY_SIZE(streams_info); i++) {
        stream_info = streams_info + i;
        if (CAMERA_ID_INVALID != stream_info->camera_id) {
            stream_info->camera_id = CAMERA_ID_INVALID;
            for (j = 0; j < ARRAY_SIZE(stream_info->x11); j++) {
                if (stream_info->x11[j]) {
                    crr_x11_disp_destroy(stream_info->x11[j]);
                    stream_info->x11[j] = NULL;
                }
            }
        }
    }
}

void guzzi_camera3_capture_result__x11_init(void)
{
    int i;
    for (i = 0; i < ARRAY_SIZE(streams_info); i++) {
        streams_info[i].camera_id = CAMERA_ID_INVALID;
    }
}

/*
 * ****************************************************************************
 * ** Capture Result **********************************************************
 * ****************************************************************************
 */
void guzzi_camera3_capture_result(
        int camera_id,
        unsigned int stream_id,
        unsigned int frame_number,
        void *data,
        unsigned int data_size
    )
{
    stream_info_t *stream_info;
    crr_x11_disp_create_t x11_disp_create;
    int i;

    if (GUZZI_CAMERA3_STREAM_ID_METADATA == stream_id) {
        return;
    }

    for (i = 0; i < ARRAY_SIZE(streams_info); i++) {
        stream_info = streams_info + i;
        if (camera_id == streams_info->camera_id) {
            break;
        }
    }
    if (ARRAY_SIZE(streams_info) <= i) {
        mmsdbg(DL_ERROR, "Unable to find requested camera!");
        return;
    }

    for (i = 0; i < ARRAY_SIZE(stream_info->streams); i++) {
        if (stream_id == stream_info->streams[i].id) {
            break;
        }
    }
    if (ARRAY_SIZE(stream_info->streams) <= i) {
        mmsdbg(DL_ERROR, "Unable to find requested stream!");
        return;
    }

    if (!stream_info->x11[i]) {
        x11_disp_create.display_name = NULL;
        x11_disp_create.width = stream_info->streams[i].size.w;
        x11_disp_create.height = stream_info->streams[i].size.h;
        x11_disp_create.format = DISP_IMAGE_FORMAT_RGBA8888;
        stream_info->x11[i] = crr_x11_disp_create(&x11_disp_create);
    }

    crr_x11_disp_show(stream_info->x11[i], data);
}

