/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file configurator.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <stdio.h>
#include <string.h>
#include <stdarg.h>

typedef struct {
    char *str;
    int size;
    int epilog_pos;
    char *section_begin;
    char *section_end;
} version_info_t;

extern char linker_version_info_begin[];
extern char linker_version_info_end[];

static version_info_t version_info_storage;

#define EPILOG "]\n"
#define EPILOG_LEN (sizeof(EPILOG) - 1)

char version_info_b[] __attribute__( (section("version_info_prolog"), aligned(1)) ) = {
    '\n', '[', '\n'
}; 

char version_info_e[] __attribute__( (section("version_info_epilog"), aligned(1)) ) = EPILOG;

static void epilog_append(version_info_t *vi)
{
    strcat(vi->str, EPILOG);
    vi->epilog_pos = strlen(vi->str) - EPILOG_LEN;
}

static int left(version_info_t *vi)
{
    return vi->size - strlen(vi->str);
}

int version_info_cat(char *str)
{
    version_info_t *vi;
    vi = &version_info_storage;
    if (left(vi) < strlen(str)+1) {
        return -1;
    }
    strcpy(vi->str + vi->epilog_pos, str);
    epilog_append(vi);
    return 0;
}

int version_info_printf(const char *fmt, ...)
{
    version_info_t *vi;
    int left_orig;
    va_list ap;
    int n;

    vi = &version_info_storage;

    left_orig = left(vi);

    va_start(ap, fmt);
    n = vsnprintf(vi->str + vi->epilog_pos, left_orig, fmt, ap);
    va_end(ap);

    if ((n < 0) || (left_orig <= n)) {
        vi->str[vi->epilog_pos] = '\000';
        epilog_append(vi);
        return -1;
    }

    epilog_append(vi);

    return 0;
}

char *version_info_get(void)
{
    version_info_t *vi;
    vi = &version_info_storage;
    return vi->str;
}

int version_info_init(void)
{
    version_info_t *vi;

    vi = &version_info_storage;

    vi->section_begin = linker_version_info_begin;
    vi->section_end = linker_version_info_end;
    vi->str = vi->section_begin;
    vi->size = vi->section_end - vi->section_begin;
    vi->epilog_pos = strlen(vi->str) - EPILOG_LEN;

    return 0;
}

