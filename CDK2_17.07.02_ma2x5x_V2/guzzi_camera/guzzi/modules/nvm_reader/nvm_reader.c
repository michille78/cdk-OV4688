/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file nvm_reader.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <osal/osal_stdtypes.h>
#include <osal/osal_stdlib.h>
#include <utils/mms_debug.h>

#include <platform/inc/plat_socket_pub.h>

#include <hal/hal_camera_module/hai_cm_driver.h>
#include <hal/hal_camera_module/hat_cm_driver.h>
#include <hal/hal_camera_module/hat_cm_nvm.h>
#include <hal/hal_camera_module/hat_cm_nvm_data_struct.h>

#include <guzzi/nvm_reader.h>

struct guzzi_nvm_reader {
    hat_cm_driver_handle_t camera_module[PH_SOCKET_MAX];
};

mmsdbg_define_variable(
        vdl_guzzi_nvm_reader,
        DL_DEFAULT,
        0,
        "vdl_guzzi_nvm_reader",
        "Guzzi NVM Reader."
    );
#define MMSDEBUGLEVEL mmsdbg_use_variable(vdl_guzzi_nvm_reader)

/*
 * ****************************************************************************
 * ** Number of cameras *******************************************************
 * ****************************************************************************
 */
int guzzi_nvm_reader_get_number_of_cameras(guzzi_nvm_reader_t *nvm_reader)
{
    int i, cnt = 0;

    for (i = PH_SOCKET_MAX - 1; (PH_SOCKET_DUMMY+1) <= i; i--) {
        if (nvm_reader->camera_module[i])
            cnt++;
    }

    return cnt;
}

/*
 * ****************************************************************************
 * ** Read ********************************************************************
 * ****************************************************************************
 */
nvm_info_t * guzzi_nvm_reader_read(
        guzzi_nvm_reader_t *nvm_reader,
        int camera_id  /* [1, number_of_cameras] */
    )
{
    hat_cm_nvm_query_t nvm_read_query;
    hat_cm_driver_handle_t camera_module;
    int err;

    if ((camera_id <= PH_SOCKET_DUMMY) || (PH_SOCKET_MAX <= camera_id)) {
        mmsdbg(
                DL_ERROR,
                "Unsupported camera ID: %d (min=%d; max=%d)!",
                camera_id,
                PH_SOCKET_DUMMY+1,
                PH_SOCKET_MAX-1
            );
        goto exit1;
    }

    camera_module = nvm_reader->camera_module[camera_id];

    err = camera_module->open(camera_module);
    if (err) {
        mmsdbg(DL_ERROR, "Failed to open Camera Module!");
        goto exit1;
    }

    nvm_read_query.type = NVM_QUERY_GET_NVM_DATA;
    err = camera_module->query(
            camera_module,
            CM_DRV_COMP_NVM,
            &nvm_read_query
        );
    if (err) {
        mmsdbg(DL_ERROR, "Failed to query NVM (camera_id=%d)!", camera_id);
        goto exit2;
    }

    camera_module->close(camera_module);

    return nvm_read_query.nvm_info;
exit2:
    camera_module->close(camera_module);
exit1:
    return NULL;
}

/*
 * ****************************************************************************
 * ** Create/Destroy **********************************************************
 * ****************************************************************************
 */
void guzzi_nvm_reader_destroy(guzzi_nvm_reader_t *nvm_reader)
{
    int i;
    for (i = PH_SOCKET_MAX - 1; (PH_SOCKET_DUMMY+1) <= i; i--) {
        if (nvm_reader->camera_module[i])
            nvm_reader->camera_module[i]->destroy(nvm_reader->camera_module[i]);
    }
    osal_free(nvm_reader);
}

guzzi_nvm_reader_t *guzzi_nvm_reader_create(void)
{
    guzzi_nvm_reader_t *nvm_reader;
    hat_cm_driver_cr_prms_t socket;
    int i;
    int err;

    nvm_reader = osal_calloc(1, sizeof (*nvm_reader));
    if (!nvm_reader) {
        mmsdbg(
                DL_ERROR,
                "Failed to allocate memory for new instance: size=%d!",
                sizeof (*nvm_reader)
            );
        goto exit1;
    }

    socket.name = "NVM Reader Camera";
    for (i = PH_SOCKET_DUMMY+1; i < PH_SOCKET_MAX; i++) {
        socket.id = i;
        err = hai_cm_driver_create(
                nvm_reader->camera_module + i,
                &socket
            );
        if (err) {
            mmsdbg(DL_ERROR, "Failed to create Camera Module Driver!");
            nvm_reader->camera_module[i]->destroy(nvm_reader->camera_module[i]);
            nvm_reader->camera_module[i] = NULL;
            continue;
        }

        err = nvm_reader->camera_module[i]->detect(nvm_reader->camera_module[i]);
        if (err) {
            mmsdbg(DL_ERROR, "Failed to detect Camera Module!");
            nvm_reader->camera_module[i]->destroy(nvm_reader->camera_module[i]);
            nvm_reader->camera_module[i] = NULL;
            continue;
        }
    }

    return nvm_reader;

exit1:
    return NULL;
}
