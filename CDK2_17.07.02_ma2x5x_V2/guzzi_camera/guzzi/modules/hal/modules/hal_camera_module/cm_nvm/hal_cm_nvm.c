/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file hal_cm_nvm.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <hal/hal_camera_module/hat_cm_nvm.h>
#include <hal/hal_camera_module/hat_cm_nvm_prvt.h>

#include <hal/hal_camera_module/hai_cm_socket.h>

#include <osal/osal_stdlib.h>

#include <error_handle/include/error_handle.h>
#include <utils/mms_debug.h>
mmsdbg_define_variable(
        hal_cm_nvm,
        DL_DEFAULT,
        0,
        "hal_cm_nvm",
        "HAL CM nvm"
    );
#define MMSDEBUGLEVEL mmsdbg_use_variable(hal_cm_nvm)



static int hai_cm_nvm_set_power_mode(hat_cm_nvm_handle_t hndl,
    HAT_CM_NVM_POWER_MODE_T mode)
{
    int err = 0;
    hat_cm_nvm_prvt_data_t *prvt;

    prvt = (hat_cm_nvm_prvt_data_t *)hndl->prvt;

    switch (mode) {
        case NVM_POWER_MODE_OFF:
            err = prvt->prms.ph_nvm_hndl->nvm_power_off(prvt->prms.socket_hndl,
                (void*)prvt->prms.nvm_features, (void*)execute_cmd_strip);
            GOTO_EXIT_IF(0 != err, 1);

            prvt->prms.pwr_mode = mode;
            break;

        case NVM_POWER_MODE_ON:
            err = prvt->prms.ph_nvm_hndl->nvm_power_on(prvt->prms.socket_hndl,
                (void*)prvt->prms.nvm_features, (void*)execute_cmd_strip);
            GOTO_EXIT_IF(0 != err, 1);

            prvt->prms.pwr_mode = mode;
            break;

        default:
            err = -1;
            GOTO_EXIT_IF(0 != err, 1);
    }

    return 0;

EXIT_1:
    mmsdbg(DL_ERROR, "//VIV HAL: Exit Err");
    return -1;
}

static int hai_cm_nvm_init(hat_cm_nvm_handle_t hndl)
{
    int err = 0;
    hat_cm_nvm_prvt_data_t *prvt;

    prvt = (hat_cm_nvm_prvt_data_t *)hndl->prvt;

    err = prvt->prms.ph_nvm_hndl->nvm_init(prvt->prms.socket_hndl,
        (void*)prvt->prms.nvm_features, (void*)execute_cmd_strip);
    GOTO_EXIT_IF(0 != err, 1);

    return 0;

EXIT_1:
    mmsdbg(DL_ERROR, "//VIV HAL: Exit Err");
    return -1;
}

static int hai_cm_nvm_read(hat_cm_nvm_handle_t hndl, void* buff, uint32 start, uint32 size)
{
int err = 0;
    hat_cm_nvm_prvt_data_t *prvt;

    prvt = (hat_cm_nvm_prvt_data_t *)hndl->prvt;

    err = prvt->prms.ph_nvm_hndl->nvm_read( prvt->prms.socket_hndl,
                                            prvt->prms.nvm_features,
                                            (void*)execute_cmd_entry,
                                            buff,
                                            start,
                                            size);

    return err;
}

static int hai_cm_nvm_write(hat_cm_nvm_handle_t hndl, void* buff, uint32 start, uint32 size)
{
    int err = 0;
    hat_cm_nvm_prvt_data_t *prvt;

    prvt = (hat_cm_nvm_prvt_data_t *)hndl->prvt;

    err = prvt->prms.ph_nvm_hndl->nvm_write(prvt->prms.socket_hndl,
                                            prvt->prms.nvm_features,
                                            (void*)execute_cmd_entry,
                                            buff,
                                            start,
                                            size);
    GOTO_EXIT_IF(0 != err, 1);

    return 0;

EXIT_1:
    mmsdbg(DL_ERROR, "Can't write to nvm");
    return -1;
}

static int hai_cm_nvm_get_features(hat_cm_nvm_handle_t hndl,
    hat_nvm_features_t** features)
{
    hat_cm_nvm_prvt_data_t *prvt;

    prvt = (hat_cm_nvm_prvt_data_t *)hndl->prvt;

    *features = (hat_nvm_features_t *)prvt->prms.nvm_features;

    return 0;
}

static int hai_cm_nvm_get_power_mode(hat_cm_nvm_handle_t hndl,
    HAT_CM_NVM_POWER_MODE_T *mode)
{
    hat_cm_nvm_prvt_data_t *prvt;

    prvt = (hat_cm_nvm_prvt_data_t *)hndl->prvt;

    *mode = prvt->prms.pwr_mode;

    return 0;
}



/* ========================================================================== */
/**
* hai_cm_nvm_get_state()
*/
/* ========================================================================== */
static int hai_cm_nvm_get_state(hat_cm_nvm_handle_t hndl,
    HAT_CM_NVM_STATE_T *state)
{
    hat_cm_nvm_prvt_data_t *prvt;

    prvt = (hat_cm_nvm_prvt_data_t *)hndl->prvt;

    mmsdbg(DL_MESSAGE, "//VIV HAL: NVM state: %d", prvt->prms.state);

    *state = prvt->prms.state;

    return 0;
}

/* ========================================================================== */
/**
* hai_cm_nvm_open()
*/
/* ========================================================================== */
static int hai_cm_nvm_open(hat_cm_nvm_handle_t hndl,
    void *sock_hndl, void *ph_nvm_hndl, void* nvm_features)
{
    hat_cm_nvm_prvt_data_t *prvt;
    prvt = (hat_cm_nvm_prvt_data_t *)hndl->prvt;

    prvt->prms.socket_hndl = (hat_cm_socket_handle_t)sock_hndl;
    GOTO_EXIT_IF(NULL == sock_hndl, 1);

    prvt->prms.ph_nvm_hndl = (hat_cm_ph_nvm_handle_t)ph_nvm_hndl;
    GOTO_EXIT_IF(NULL == ph_nvm_hndl, 1);

    prvt->prms.nvm_features = (const hat_nvm_features_t*)nvm_features;
    GOTO_EXIT_IF(NULL == nvm_features, 1);

    mmsdbg(DL_MESSAGE, "//VIV HAL: %s, ID %d linked to physical nvm %010p and virtual "
        "socket handle %010p",
        prvt->cr_prms.name, prvt->cr_prms.id, prvt->prms.ph_nvm_hndl,
        prvt->prms.socket_hndl);

    prvt->prms.state = NVM_STATE_OPENED;

    return 0;

EXIT_1:
    mmsdbg(DL_ERROR, "//VIV HAL: Exit Err");
    return -1;
}

/* ========================================================================== */
/**
* hai_cm_nvm_config()
*/
/* ========================================================================== */
static int hai_cm_nvm_config(hat_cm_nvm_handle_t hndl, void* cfg_prms)
{
    int err = 0;

    switch (((hat_cm_nvm_config_t*)cfg_prms)->cfg_type) {
        case NVM_CFG_DUMMY:
            mmsdbg(DL_WARNING, "//VIV HAL: NVM_CFG_DUMMY nvm config!");
            break;
        default:
            mmsdbg(DL_ERROR, "//VIV HAL: No such a nvm config!");
            err = -1;
            GOTO_EXIT_IF(0 != err, 1);
    }

    return 0;

EXIT_1:
    mmsdbg(DL_ERROR, "//VIV HAL: Exit Err");
    return -1;
}

/* ========================================================================== */
/**
* hai_cm_nvm_control()
*/
/* ========================================================================== */
static int hai_cm_nvm_control(hat_cm_nvm_handle_t hndl, void* ctrl_prms)
{
    int err = 0;

    switch (((hat_cm_nvm_control_t*)ctrl_prms)->ctrl_type) {
        case NVM_CTRL_DUMMY:
            mmsdbg(DL_WARNING, "//VIV HAL: NVM_CTRL_DUMMY nvm control!");
            break;
        case NVM_CTRL_POWER:
            err = hndl->set_power_mode(hndl,
                ((hat_cm_nvm_control_t*)ctrl_prms)->ctrl.power);
            GOTO_EXIT_IF(0 != err, 1);
            break;
        case NVM_CTRL_INIT:
            err = hndl->init(hndl);
            GOTO_EXIT_IF(0 != err, 1);
            break;
        case NVM_CTRL_WRITE:
            err = hndl->write(hndl,
                              ((hat_cm_nvm_control_t*)ctrl_prms)->ctrl.io.buf,
                              ((hat_cm_nvm_control_t*)ctrl_prms)->ctrl.io.start,
                              ((hat_cm_nvm_control_t*)ctrl_prms)->ctrl.io.size
                              );
            GOTO_EXIT_IF(0 != err, 1);
            break;
        case NVM_CTRL_READ:
            err = hndl->read(hndl,
                             ((hat_cm_nvm_control_t*)ctrl_prms)->ctrl.io.buf,
                             ((hat_cm_nvm_control_t*)ctrl_prms)->ctrl.io.start,
                             ((hat_cm_nvm_control_t*)ctrl_prms)->ctrl.io.size
                             );
            GOTO_EXIT_IF(0 != err, 1);
            break;
        default:
            mmsdbg(DL_ERROR, "//VIV HAL: No such a nvm control!");
            err = -1;
            GOTO_EXIT_IF(0 != err, 1);
    }

    return 0;

EXIT_1:
    mmsdbg(DL_ERROR, "//VIV HAL: Exit Err");
    return -1;
}

/* ========================================================================== */
/**
* hai_cm_nvm_process()
*/
/* ========================================================================== */
static int hai_cm_nvm_process(hat_cm_nvm_handle_t hndl, void* process_prms)
{
    int err = 0;

    switch (((hat_cm_nvm_process_t*)process_prms)->process_type) {
        case NVM_PROCESS_DUMMY:
            mmsdbg(DL_WARNING, "//VIV HAL: NVM_PROCESS_DUMMY nvm control!");
            break;
        default:
            mmsdbg(DL_ERROR, "//VIV HAL: No such a nvm process!");
            err = -1;
            GOTO_EXIT_IF(0 != err, 1);
    }

    return 0;

EXIT_1:
    mmsdbg(DL_ERROR, "//VIV HAL: Exit Err");
    return -1;
}

/* ========================================================================== */
/**
* hai_cm_nvm_query()
*/
/* ========================================================================== */
static int hai_cm_nvm_query(hat_cm_nvm_handle_t hndl, void* query_prms)
{
    int err = 0;

    switch (((hat_cm_nvm_query_t*)query_prms)->type) {
        case NVM_QUERY_DUMMY:
            mmsdbg(DL_WARNING, "//VIV HAL: NVM_QUERY_DUMMY nvm query!");
            break;
        case NVM_QUERY_GET_FEATURES:
            err = hndl->get_features(hndl,
                ((hat_cm_nvm_query_t*)query_prms)->features);
            GOTO_EXIT_IF(0 != err, 1);
            break;
/*
        case NVM_QUERY_GET_NVM_DATA:
            err = hndl->read(
                    hndl,
                    &((hat_cm_nvm_query_t*)query_prms)->nvm_info
                );
            GOTO_EXIT_IF(0 != err, 1);
            break;
*/
        default:
            mmsdbg(DL_ERROR, "//VIV HAL: No such a nvm query!");
            err = -1;
            GOTO_EXIT_IF(0 != err, 1);
    }

    return 0;

EXIT_1:
    mmsdbg(DL_ERROR, "//VIV HAL: Exit Err");
    return -1;
}

/* ========================================================================== */
/**
* hai_cm_nvm_close()
*/
/* ========================================================================== */
static int hai_cm_nvm_close(hat_cm_nvm_handle_t hndl)
{
    hat_cm_nvm_prvt_data_t *prvt;

    prvt = (hat_cm_nvm_prvt_data_t *)hndl->prvt;

    prvt->prms.socket_hndl = NULL;
    prvt->prms.ph_nvm_hndl = NULL;

    prvt->prms.state = NVM_STATE_CLOSED;

    return 0;
}

/* ========================================================================== */
/**
* hai_cm_nvm_destroy()
*/
/* ========================================================================== */
static int hai_cm_nvm_destroy(hat_cm_nvm_handle_t hndl)
{
    hat_cm_nvm_prvt_data_t *prvt;

    mmsdbg(DL_MESSAGE, "//VIV HAL: Enter");

    prvt = (hat_cm_nvm_prvt_data_t *)hndl->prvt;

    mmsdbg(DL_MESSAGE, "//VIV HAL: %s, ID %d going to destroy at %010p",
        prvt->cr_prms.name, prvt->cr_prms.id, hndl);

    prvt->cr_prms.id = 0;
    prvt->cr_prms.name= NULL;
    prvt->prms.state = NVM_STATE_UNKNOWN;

    osal_free(hndl->prvt);

    hndl->prvt = NULL;

    mmsdbg(DL_MESSAGE, "//VIV HAL: Exit Ok");

    return 0;
}

/* ========================================================================== */
/**
* hai_cm_nvm_create()
*/
/* ========================================================================== */
int hai_cm_nvm_create(hat_cm_nvm_handle_t hndl,
    hat_cm_nvm_cr_prms_t *cr_prms)
{
    hat_cm_nvm_prvt_data_t *prvt;

    prvt = osal_malloc(sizeof(hat_cm_nvm_prvt_data_t));
    GOTO_EXIT_IF(NULL == prvt, 1);

    prvt->cr_prms.id = cr_prms->id;
    prvt->cr_prms.name = cr_prms->name;
    prvt->prms.state = NVM_STATE_CREATED;

    hndl->prvt = (void *)prvt;

    hndl->set_power_mode    = hai_cm_nvm_set_power_mode;
    hndl->init              = hai_cm_nvm_init;
    hndl->read              = hai_cm_nvm_read;
    hndl->write             = hai_cm_nvm_write;

    hndl->get_features      = hai_cm_nvm_get_features;
    hndl->get_power_mode    = hai_cm_nvm_get_power_mode;
    hndl->get_state         = hai_cm_nvm_get_state;

    hndl->open              = hai_cm_nvm_open;
    hndl->config            = hai_cm_nvm_config;
    hndl->control           = hai_cm_nvm_control;
    hndl->process           = hai_cm_nvm_process;
    hndl->query             = hai_cm_nvm_query;
    hndl->close             = hai_cm_nvm_close;
    hndl->destroy           = hai_cm_nvm_destroy;

    mmsdbg(DL_MESSAGE, "//VIV HAL: %s, ID %d created at %010p",
        prvt->cr_prms.name, prvt->cr_prms.id, hndl);
    mmsdbg(DL_MESSAGE, "//VIV HAL: Exit Ok");

    return 0;

EXIT_1:
    mmsdbg(DL_ERROR, "//VIV HAL: Exit Err");
    return -1;
}
