/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file hal_cm_lights_prvt.h
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! Jun 10, 2014 : Author aiovtchev
*! Created
* =========================================================================== */



#ifndef HAL_CM_LIGHTS_PRVT_H_
#define HAL_CM_LIGHTS_PRVT_H_
#include <hal/hal_camera_module/hat_cm_lights.h>
#include <hal/hal_camera_module/hat_cm_socket.h>
#include <osal/osal_mutex.h>
#include <hal/modules/hal_camera_module/lights/lights_phys.h>
#include <func_thread/include/func_thread.h>


typedef struct {
    osal_mutex                 *lock;
    void                       *user_data;
    hat_cm_lights_cb_t          lights_cb;

    hat_cm_ph_lights_handle_t   ph_lights_hndl;
    hat_cm_socket_handle_t      socket_hndl;

    hat_light_ctrl_list_t       *ctrl;
    uint32                      running_flag;
} hat_cm_lights_thread_data_t;

typedef struct {
    HAT_CM_LIGHTS_STATE_T       state;
    hat_cm_flash_dyn_props_t    dyn_props;
    HAT_CM_LIGHTS_POWER_MODE_T  pwr_mode;
    const hat_lights_features_t *lights_features;
    func_thread_t               *thread;
    hat_cm_lights_thread_data_t thread_data;
} hat_cm_lights_prms_t;

typedef struct {
    hat_cm_lights_cr_prms_t cr_prms;
    hat_cm_lights_prms_t    prms;
} hat_cm_lights_prvt_data_t;


#endif /* HAL_CM_LIGHTS_PRVT_H_ */
