/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file hal_cm_lights.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! Jun 10, 2014 : Author aiovtchev
*! Created
* =========================================================================== */
#include <osal/osal_stdlib.h>
#include <osal/list_pool.h>
#include "hal_cm_lights_prvt.h"
#include <hal/hal_camera_module/hai_cm_socket.h>
#include <utils/mms_debug.h>
#include <error_handle/include/error_handle.h>

mmsdbg_define_variable(
        hal_cm_lights,
        DL_DEFAULT,
        0,
        "hal_cm_lights",
        "HAL CM lights"
    );
#define MMSDEBUGLEVEL mmsdbg_use_variable(hal_cm_lights)

static void lights_process_do(func_thread_t *ethr, void *app_prv);

static func_thread_handle_t lights_thread_fxns[] = {
    lights_process_do,
};

static func_thread_handles_t lights_thread_handles = {
    .fxns = lights_thread_fxns,
    .size = ARRAY_SIZE(lights_thread_fxns)
};

static void lights_process_do(func_thread_t *ethr, void *app_prv)
{
    hat_cm_lights_thread_data_t       *p = app_prv;
    hat_light_ctrl_entry_t *lights_entry;
    hat_light_ctrl_list_entry_t *ent;

    mmsdbg(DL_FUNC, "Enter");
    if(!ethr || !p || !p->ctrl) {
        mmsdbg(DL_ERROR, "Null pointer");
        return;
    }
    if (list_empty(&p->ctrl->list))
    {
        mmsdbg(DL_ERROR, "Empty list with light sequence");
        return;
    }
    ent = list_first_entry(&p->ctrl->list, hat_light_ctrl_list_entry_t, link);
    lights_entry = &ent->ctrl;
    if (lights_entry->start_time > 20)
        osal_usleep(lights_entry->start_time);

    list_for_each_entry(ent, &p->ctrl->list, link)
    {
        lights_entry = &ent->ctrl;
        p->ph_lights_hndl->set_power_state(p->socket_hndl,
                                           execute_cmd_strip,
                                           lights_entry);
        if (lights_entry->duration > 20)
            osal_usleep(lights_entry->duration);
    }

    osal_free(p->ctrl);   // unlock lights list - should be locked from inc_camera
    osal_mutex_lock(p->lock);
        p->running_flag = 0;
    osal_mutex_unlock(p->lock);

    mmsdbg(DL_FUNC, "Exit");
}


static int hai_cm_lights_power_command(hat_cm_lights_handle_t  hndl,
                                       hat_light_ctrl_list_t   *ctrl)
{
    hat_cm_lights_prvt_data_t *prvt = (hat_cm_lights_prvt_data_t *)hndl->prvt;
    int err = 0;
    hat_light_ctrl_list_entry_t *ent;
//    hat_light_ctrl_entry_t *lights_entry;

    osal_mutex_lock(prvt->prms.thread_data.lock);

    if (prvt->prms.thread_data.running_flag)
    {
        osal_free(ctrl);   // unlock lights list - should be locked from inc_camera
        osal_mutex_unlock(prvt->prms.thread_data.lock);
        mmsdbg(DL_ERROR, "light is still active - skip requested light sequence!");
        GOTO_EXIT_IF(1, 1);
    }

    prvt->prms.thread_data.running_flag = 1;

    mmsdbg(DL_ERROR, "start light sequence!");
    // Copy light control sequence locally
    prvt->prms.thread_data.ctrl = ctrl;

    list_for_each_entry(ent, &ctrl->list, link)
    {
        if ((ent->ctrl.src == LIGHT_SRC_MAIN_FLASH)||
            (ent->ctrl.src == LIGHT_SRC_MAIN_RED_EYE))
        {
            prvt->prms.dyn_props.firing_power = ent->ctrl.power / 10;
            prvt->prms.dyn_props.firing_time_ns = ent->ctrl.duration * 1000;
            prvt->prms.dyn_props.mode = LIGHTS_DYN_MODE_SINGLE;
            prvt->prms.dyn_props.state = LIGHTS_DYN_STATE_FIRED;
        }
        else if ((ent->ctrl.src == LIGHT_SRC_TORCH)||
            (ent->ctrl.src == LIGHT_SRC_VIDEO_LIGHT))
        {
            prvt->prms.dyn_props.firing_power = ent->ctrl.power / 10;
            prvt->prms.dyn_props.firing_time_ns = ent->ctrl.duration * 1000;
            prvt->prms.dyn_props.mode = LIGHTS_DYN_MODE_TOURCH;
            prvt->prms.dyn_props.state = LIGHTS_DYN_STATE_FIRED;
        }
    }

    err = func_thread_exec(prvt->prms.thread, lights_process_do);
    if (err) {
        osal_mutex_unlock(prvt->prms.thread_data.lock);
        mmsdbg(DL_ERROR, "Error executing func_thread_exec()!");
        GOTO_EXIT_IF(1, 1);
    }


    osal_mutex_unlock(prvt->prms.thread_data.lock);

    return 0;

EXIT_1:
    mmsdbg(DL_ERROR, "//DBG HAL: Exit Err");
    return -1;

}


static int hai_cm_lights_get_dyn_props(hat_cm_lights_handle_t hndl,
                                        hat_cm_flash_dyn_props_t* dyn_props)
{
    hat_cm_lights_prvt_data_t *prvt;

    prvt = (hat_cm_lights_prvt_data_t *)hndl->prvt;

    *dyn_props = prvt->prms.dyn_props;

    if(prvt->prms.dyn_props.state != LIGHTS_DYN_STATE_UNAVAILABLE){
        prvt->prms.dyn_props.state = LIGHTS_DYN_STATE_READY;
        prvt->prms.dyn_props.mode = LIGHTS_DYN_MODE_OFF;
        prvt->prms.dyn_props.firing_power = 0;
        prvt->prms.dyn_props.firing_time_ns = 0;
    }

    return 0;
}

static int hai_cm_lights_get_features(hat_cm_lights_handle_t hndl,
                                      hat_flash_bounds_t** features)
{
    hat_cm_lights_prvt_data_t *prvt = (hat_cm_lights_prvt_data_t *)hndl->prvt;
    int err = 0;
    hat_flash_bounds_t *lights_features;

    err = prvt->prms.thread_data.ph_lights_hndl->get_features((void**)&lights_features);
    GOTO_EXIT_IF(0 != err, 1);

    *features = lights_features;

    return 0;

EXIT_1:
    mmsdbg(DL_ERROR, "//DBG HAL: Exit Err");
    *features = NULL;
    return -1;
}


/* ========================================================================== */
/**
* hai_cm_lights_get_state()
*/
/* ========================================================================== */
static int hai_cm_lights_get_state(hat_cm_lights_handle_t hndl,
    HAT_CM_LIGHTS_STATE_T *state)
{
    hat_cm_lights_prvt_data_t *prvt;

    prvt = (hat_cm_lights_prvt_data_t *)hndl->prvt;

    mmsdbg(DL_MESSAGE, "//DBG HAL: Light state: %d", prvt->prms.state);

    *state = prvt->prms.state;

    return 0;
}

/* ========================================================================== */
/**
* hai_cm_lights_open()
*/
/* ========================================================================== */
static int hai_cm_lights_open(hat_cm_lights_handle_t hndl,
    void *sock_hndl, void *ph_light_hndl, void* lights_features)
{
    hat_cm_lights_prvt_data_t *prvt;
    prvt = (hat_cm_lights_prvt_data_t *)hndl->prvt;

    prvt->prms.thread_data.socket_hndl = (hat_cm_socket_handle_t)sock_hndl;
    GOTO_EXIT_IF(NULL == sock_hndl, 1);

    prvt->prms.thread_data.ph_lights_hndl = (hat_cm_ph_lights_handle_t)ph_light_hndl;
    GOTO_EXIT_IF(NULL == ph_light_hndl, 1);

    prvt->prms.lights_features = (const hat_lights_features_t*)lights_features;
    GOTO_EXIT_IF(NULL == lights_features, 1);

    prvt->prms.thread_data.ph_lights_hndl->init(prvt->prms.thread_data.socket_hndl, lights_features, execute_cmd_strip);

    mmsdbg(DL_MESSAGE, "//DBG HAL: %s, ID %d linked to physical lights %010p and virtual "
        "socket handle %010p",
        prvt->cr_prms.name, prvt->cr_prms.id, prvt->prms.thread_data.ph_lights_hndl,
        prvt->prms.thread_data.socket_hndl);

    prvt->prms.dyn_props.state = LIGHTS_DYN_STATE_UNAVAILABLE;
    prvt->prms.dyn_props.mode = LIGHTS_DYN_MODE_OFF;
    prvt->prms.dyn_props.firing_power = 0;
    prvt->prms.dyn_props.firing_time_ns = 0;
    // Check if lights are available.
    if (prvt->prms.lights_features->flash.spec_features.available ||
        prvt->prms.lights_features->red_eye.spec_features.available ||
        prvt->prms.lights_features->torch.spec_features.available ||
        prvt->prms.lights_features->video.spec_features.available)
    {
       prvt->prms.dyn_props.state = LIGHTS_DYN_STATE_READY;
    }

    prvt->prms.state = LIGHTS_STATE_OPENED;

    return 0;

EXIT_1:
    mmsdbg(DL_ERROR, "//DBG HAL: Exit Err");
    return -1;
}

/* ========================================================================== */
/**
* hai_cm_lights_config()
*/
/* ========================================================================== */
static int hai_cm_lights_config(hat_cm_lights_handle_t hndl, void* cfg_prms)
{
    int err = 0;

    switch (((hat_cm_light_config_t*)cfg_prms)->cfg_type) {
        case LIGHT_CFG_DUMMY:
            mmsdbg(DL_WARNING, "//VIV HAL: LIGHT_CFG_DUMMY light config!");
            break;
        default:
            mmsdbg(DL_ERROR, "//VIV HAL: No such a light config!");
            err = -1;
            GOTO_EXIT_IF(0 != err, 1);
    }

    return 0;

EXIT_1:
    mmsdbg(DL_ERROR, "//VIV HAL: Exit Err");
    return -1;
}

/* ========================================================================== */
/**
* hai_cm_lights_control()
*/
/* ========================================================================== */
static int hai_cm_lights_control(hat_cm_lights_handle_t hndl, void* ctrl_prms)
{
    int err = 0;

    switch (((hat_cm_light_control_t*)ctrl_prms)->ctrl_type) {
        case LIGHT_CTRL_DUMMY:
            mmsdbg(DL_WARNING, "//VIV HAL: LIGHT_CTRL_DUMMY light control!");
            break;
        case LIGHT_CTRL_POWER_CMD:
            err = hndl->power_cmd(hndl,
                ((hat_cm_light_control_t*)ctrl_prms)->ctrl.pwr_cmd);
            break;
        default:
            mmsdbg(DL_ERROR, "//VIV HAL: No such a light control!");
            err = -1;
            GOTO_EXIT_IF(0 != err, 1);
    }

    return 0;

EXIT_1:
    mmsdbg(DL_ERROR, "//VIV HAL: Exit Err");
    return -1;
}

/* ========================================================================== */
/**
* hai_cm_lights_process()
*/
/* ========================================================================== */
static int hai_cm_lights_process(hat_cm_lights_handle_t hndl, void* process_prms)
{
    int err = 0;

    switch (((hat_cm_light_process_t*)process_prms)->process_type) {
        case LIGHT_PROCESS_DUMMY:
            mmsdbg(DL_WARNING, "//VIV HAL: LIGHT_PROCESS_DUMMY light process!");
            break;
        default:
            mmsdbg(DL_ERROR, "//VIV HAL: No such a light process!");
            err = -1;
            GOTO_EXIT_IF(0 != err, 1);
    }

    return 0;

EXIT_1:
    mmsdbg(DL_ERROR, "//VIV HAL: Exit Err");
    return -1;
}

/* ========================================================================== */
/**
* hai_cm_lights_query()
*/
/* ========================================================================== */
static int hai_cm_lights_query(hat_cm_lights_handle_t hndl, void* query_prms)
{
    int err = 0;

    switch (((hat_cm_light_query_t*)query_prms)->query_type) {
        case LIGHT_QUERY_DUMMY:
            mmsdbg(DL_WARNING, "//VIV HAL: LIGHT_QUERY_DUMMY light query!");
            break;
        case LIGHT_QUERY_GET_DYN_PROPS:
            err = hndl->get_dyn_props(hndl,
                ((hat_cm_light_query_t*)query_prms)->query.dyn_props);
            GOTO_EXIT_IF(0 != err, 1);
            break;
        case LIGHT_QUERY_GET_FEATURES:
            err = hndl->get_features(hndl,
                ((hat_cm_light_query_t*)query_prms)->query.features);
            break;
        default:
            mmsdbg(DL_ERROR, "//VIV HAL: No such a light query!");
            err = -1;
            GOTO_EXIT_IF(0 != err, 1);
    }

    return 0;

EXIT_1:
    mmsdbg(DL_ERROR, "//VIV HAL: Exit Err");
    return -1;
}

/* ========================================================================== */
/**
* hai_cm_lights_close()
*/
/* ========================================================================== */
static int hai_cm_lights_close(hat_cm_lights_handle_t hndl)
{
    hat_cm_lights_prvt_data_t *prvt;

    prvt = (hat_cm_lights_prvt_data_t *)hndl->prvt;

    prvt->prms.thread_data.socket_hndl = NULL;
    prvt->prms.thread_data.ph_lights_hndl = NULL;

    prvt->prms.state = LIGHTS_STATE_CLOSED;

    return 0;
}

/* ========================================================================== */
/**
* hai_cm_lights_destroy()
*/
/* ========================================================================== */
static int hai_cm_lights_destroy(hat_cm_lights_handle_t hndl)
{
    hat_cm_lights_prvt_data_t *prvt = hndl->prvt;

    func_thread_destroy(prvt->prms.thread);

    osal_mutex_destroy(prvt->prms.thread_data.lock);

    osal_free(hndl->prvt);

    hndl->prvt = NULL;

    return 0;
}
/* ========================================================================== */
/**
* hai_cm_lights_create()
*/
/* ========================================================================== */
int hai_cm_lights_create(hat_cm_lights_handle_t hndl,
    hat_cm_lights_cr_prms_t *cr_prms)
{
    hat_cm_lights_prvt_data_t *prvt;

    prvt = osal_malloc(sizeof(hat_cm_lights_prvt_data_t));
    GOTO_EXIT_IF(NULL == prvt, 1);


    prvt->prms.thread_data.lock = osal_mutex_create();
    prvt->prms.thread_data.running_flag = 0;
    GOTO_EXIT_IF(NULL == prvt->prms.thread_data.lock, 1);

    prvt->prms.thread = func_thread_create("Light Thread",
                                              1 * 1024,
                                              5,
                                              &lights_thread_handles,
                                              &prvt->prms.thread_data);

    GOTO_EXIT_IF(NULL == prvt->prms.thread, 2);

    hndl->prvt          = (void *)prvt;
    hndl->open          = hai_cm_lights_open;
    hndl->config        = hai_cm_lights_config;
    hndl->control       = hai_cm_lights_control;
    hndl->process       = hai_cm_lights_process;
    hndl->query         = hai_cm_lights_query;
    hndl->close         = hai_cm_lights_close;
    hndl->destroy       = hai_cm_lights_destroy;

    hndl->get_dyn_props = hai_cm_lights_get_dyn_props;
    hndl->get_features  = hai_cm_lights_get_features;
    hndl->get_state     = hai_cm_lights_get_state;
    hndl->power_cmd     = hai_cm_lights_power_command;

    prvt->prms.state = LIGHTS_STATE_CREATED;

    return 0;

EXIT_2:
    osal_mutex_destroy(prvt->prms.thread_data.lock);
EXIT_1:
    mmsdbg(DL_ERROR, "//DBG HAL: Exit Err");
    return -1;
}

