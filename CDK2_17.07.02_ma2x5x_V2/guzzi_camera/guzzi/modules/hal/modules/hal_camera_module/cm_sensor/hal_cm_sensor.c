/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file hal_cm_sensor.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <hal/hal_camera_module/hat_cm_sensor.h>
#include <hal/hal_camera_module/hat_cm_sensor_prvt.h>

#include <hal/hal_camera_module/hai_cm_socket.h>

#include <osal/osal_stdlib.h>

#include <error_handle/include/error_handle.h>
#include <utils/mms_debug.h>

#include <gzz_cam_config.h>

mmsdbg_define_variable(
        hal_cm_sensor,
        DL_DEFAULT,
        0,
        "hal_cm_sensor",
        "HAL CM sensor"
    );
#define MMSDEBUGLEVEL mmsdbg_use_variable(hal_cm_sensor)

//Because of HW limitation requested weight and height comes with half of real values.
//We need to multiply this values to have real requested size.
#define RQST_SM_MULT (2)

static int hai_cm_sensor_set_event_cb(hat_cm_sensor_handle_t hndl)
{
    return 0;
}

static int hai_cm_sensor_set_power_mode(hat_cm_sensor_handle_t hndl,
    HAT_CM_SENSOR_POWER_MODE_T mode)
{
    int err = 0;
    hat_cm_sensor_prvt_data_t *prvt;

    prvt = (hat_cm_sensor_prvt_data_t *)hndl->prvt;

    switch (mode) {
        case SENSOR_POWER_MODE_OFF:
            if (prvt->prms.ph_sen_hndl->power_off) {
                err = prvt->prms.ph_sen_hndl->power_off(prvt->prms.socket_hndl,
                    (void*)prvt->prms.sens_features, (void*)execute_cmd_strip);
                GOTO_EXIT_IF(0 != err, 1);
            }
            prvt->prms.pwr_mode = mode;
            break;

        case SENSOR_POWER_MODE_ON:
            if (prvt->prms.ph_sen_hndl->power_on) {
                err = prvt->prms.ph_sen_hndl->power_on(prvt->prms.socket_hndl,
                    (void*)prvt->prms.sens_features, (void*)execute_cmd_strip);
                GOTO_EXIT_IF(0 != err, 1);
            }

            prvt->prms.pwr_mode = mode;
            break;

        default:
            err = -1;
            GOTO_EXIT_IF(0 != err, 1);
    }

    return 0;

EXIT_1:
    mmsdbg(DL_ERROR, "//VIV HAL: Exit Err");
    return -1;
}

static int hai_cm_sensor_init(hat_cm_sensor_handle_t hndl, uint32 init_data)
{
    int err = 0;
    hat_cm_sensor_prvt_data_t *prvt = (hat_cm_sensor_prvt_data_t *)hndl->prvt;
    uint32 i, init_idx = SENSOR_INIT_MAX;
    const hat_sensor_oper_entry_t *init =
        prvt->prms.sens_features->operations.init;

    for (i = 0; i < SENSOR_INIT_MAX; i++) {
        if((init[i].data == init_data || !init[i].data || !init_data)
                && init[i].oper) {
            init_idx = i;
            break;
        }
    }

    if (init_idx == SENSOR_INIT_MAX) {
        mmsdbg(DL_ERROR, "Missing Sensor init script!?");
        goto EXIT_1;
    }

    if (prvt->prms.ph_sen_hndl->init) {
        err = prvt->prms.ph_sen_hndl->init(prvt->prms.socket_hndl,
            (void*)prvt->prms.sens_features, (void*)execute_cmd_strip, init_idx);
        GOTO_EXIT_IF(0 != err, 1);
    }

    return 0;

EXIT_1:
    mmsdbg(DL_ERROR, "//VIV HAL: Exit Err");
    return -1;
}

static int hai_cm_sensor_select_mode(hat_cm_sensor_handle_t hndl,
    hat_cm_sensor_select_mode_t* mode_rqst, uint32* mode_index)
{
    hat_cm_sensor_prvt_data_t *prvt;
    hat_sensor_mode_t         *mode_list;
    uint32                     mode_num;
    int i;
    const hat_sensor_mode_t   *curr_mode;

    prvt = (hat_cm_sensor_prvt_data_t *)hndl->prvt;

    mode_list = (hat_sensor_mode_t *)prvt->prms.sens_features->modes.list;
    mode_num = prvt->prms.sens_features->modes.num;

    *mode_index = 0;

    for (i = 0; i < mode_num; i++)
    {
        curr_mode = &mode_list[i];
        if (CAM_CAPTURE_INTENT_CUSTOM == mode_rqst->intent)
        {
            if (mode_rqst->pipeline_id == curr_mode->pipeline_id)
            {
                *mode_index = i; // return mode index
                mmsdbg(DL_ERROR, "Suitable sensor mode: %d by pipeline", i, mode_rqst->pipeline_id);
                return 0;
            }
        } else
        if (
            (((mode_rqst->crop.w + mode_rqst->crop.x)*RQST_SM_MULT) <= curr_mode->active.w) &&
            (((mode_rqst->crop.h + mode_rqst->crop.y)*RQST_SM_MULT) <= curr_mode->active.h) &&
            (mode_rqst->fps_range.max <= curr_mode->fps.max) &&
            (mode_rqst->fps_range.min >= curr_mode->fps.min)
            )
        {
            *mode_index = i; // return mode index
            mmsdbg(DL_MESSAGE, "//VIV HAL: Suitable sensor mode: %d", i);
            return 0;
        }
    }

    mmsdbg(DL_ERROR, "No suitable sensor mode. Selecting default one - 0 for strat %dx%d at %dx%d fps min %f max %f",
           mode_rqst->crop.w,
           mode_rqst->crop.h,
           mode_rqst->crop.x,
           mode_rqst->crop.y,
           mode_rqst->fps_range.min,
           mode_rqst->fps_range.max);

    return 0;
}

static int hai_cm_sensor_set_mode(hat_cm_sensor_handle_t hndl,
    hat_sen_config_t* sen_cfg)
{
    int err = 0;
    hat_cm_sensor_prvt_data_t *prvt;
    hat_sensor_mode_t *curr_sen_mode;

    prvt = (hat_cm_sensor_prvt_data_t *)hndl->prvt;

    PROFILE_ADD(PROFILE_ID_SENSOR_TIME, 0x30BB, 0x00);

    if (prvt->prms.ph_sen_hndl->set_mode) {
        err = prvt->prms.ph_sen_hndl->set_mode(prvt->prms.socket_hndl,
            (void*)prvt->prms.sens_features,
            (void*)execute_cmd_strip, sen_cfg);
        GOTO_EXIT_IF(0 != err, 1);
    }

    curr_sen_mode = (hat_sensor_mode_t*)
        &(prvt->prms.sens_features->modes.list[sen_cfg->sensor_mode_idx]);

    //TODO: Fill in other parameters also
    prvt->prms.ft_curr.fps          = curr_sen_mode->fps.max;
    prvt->prms.ft_curr.pix_clk_hz   = curr_sen_mode->pixel_clock;
    prvt->prms.ft_curr.linetime_ns  = curr_sen_mode->row_time;
    prvt->prms.ft_curr.frametime_us = curr_sen_mode->frame_time;
    prvt->prms.ft_curr.ppln         = curr_sen_mode->ppln;
    prvt->prms.ft_curr.lpfr         = curr_sen_mode->lpfr;
    prvt->prms.sen_specific_params  = curr_sen_mode->sensor_specific_data;

    prvt->prms.fps_range.min        = curr_sen_mode->fps.min;
    prvt->prms.fps_range.max        = curr_sen_mode->fps.max;

    prvt->prms.exp_range.min        = curr_sen_mode->exposure.min;
    prvt->prms.exp_range.max        = curr_sen_mode->exposure.max;

    prvt->prms.again_range.min      = prvt->prms.sens_features->again.minimum;
    prvt->prms.again_range.max      = prvt->prms.sens_features->again.maximum;

    prvt->prms.dgain_range.min      = prvt->prms.sens_features->dgain.minimum;
    prvt->prms.dgain_range.max      = prvt->prms.sens_features->dgain.maximum;

    PROFILE_ADD(PROFILE_ID_SENSOR_TIME, 0x30EE, 0x00);

    return 0;

EXIT_1:
    mmsdbg(DL_ERROR, "//VIV HAL: Exit Err");
    return -1;
}

static int hai_cm_sensor_set_stream_mode(hat_cm_sensor_handle_t hndl,
    HAT_CM_SENSOR_STREAM_MODE_T mode)
{
    int err = 0;
    hat_cm_sensor_prvt_data_t *prvt;

    prvt = (hat_cm_sensor_prvt_data_t *)hndl->prvt;

    switch (mode) {
        case SENSOR_STREAM_MODE_OFF:
            if (prvt->prms.ph_sen_hndl->stream_off) {
                err = prvt->prms.ph_sen_hndl->stream_off(prvt->prms.socket_hndl,
                    (void*)prvt->prms.sens_features, (void*)execute_cmd_strip);
                GOTO_EXIT_IF(0 != err, 1);
            }

            prvt->prms.pwr_mode = mode;
            break;

        case SENSOR_STREAM_MODE_ON:
            if (prvt->prms.ph_sen_hndl->stream_on) {
                err = prvt->prms.ph_sen_hndl->stream_on(prvt->prms.socket_hndl,
                    (void*)prvt->prms.sens_features, (void*)execute_cmd_strip);
                GOTO_EXIT_IF(0 != err, 1);
            }

            prvt->prms.pwr_mode = mode;
            break;

        default:
            err = -1;
            GOTO_EXIT_IF(0 != err, 1);
    }

    return 0;

EXIT_1:
    mmsdbg(DL_ERROR, "//VIV HAL: Exit Err");
    return -1;
}

static int hai_cm_sensor_set_exp_gain(hat_cm_sensor_handle_t hndl,
    hat_exp_gain_t exp_gain)
{
    int                           err = 0;
    hat_cm_sensor_prvt_data_t    *prvt;
    uint32                        exp_time_lines;

    prvt = (hat_cm_sensor_prvt_data_t *)hndl->prvt;

    if (exp_gain.exposure < prvt->prms.exp_range.min) {
        mmsdbg(DL_ERROR, "//VIV HAL: Error - exposure %d is out of range [%d..%d]",
            exp_gain.exposure,
            prvt->prms.exp_range.min,
            prvt->prms.exp_range.max);
        exp_gain.exposure = prvt->prms.exp_range.min;
    } else if (exp_gain.exposure > prvt->prms.exp_range.max) {
        mmsdbg(DL_ERROR, "//VIV HAL: Error - exposure %d is out of range [%d..%d]",
            exp_gain.exposure,
            prvt->prms.exp_range.min,
            prvt->prms.exp_range.max);
        exp_gain.exposure = prvt->prms.exp_range.max;
    }



    if (exp_gain.again < prvt->prms.again_range.min) {
        mmsdbg(DL_ERROR, "//VIV HAL: Error - analog gain %f is out of range [%f..%f]",
            exp_gain.again,
            prvt->prms.again_range.min,
            prvt->prms.again_range.max);
        exp_gain.again = prvt->prms.again_range.min;
    } else if (exp_gain.again > prvt->prms.again_range.max) {
        mmsdbg(DL_ERROR, "//VIV HAL: Error - analog gain %f is out of range [%f..%f]",
            exp_gain.again,
            prvt->prms.again_range.min,
            prvt->prms.again_range.max);
        exp_gain.again = prvt->prms.again_range.max;
    }

    //TODO:
    //if ((exp_gain.dgain < prvt->prms.dgain_range.min) ||
    //    (exp_gain.dgain > prvt->prms.dgain_range.max)) {
    //    mmsdbg(DL_ERROR, "//VIV HAL: Error - digital gain %f is out of range [%f..%f]",
    //        exp_gain.dgain,
    //        prvt->prms.dgain_range.min,
    //        prvt->prms.dgain_range.max);
    //    err = -1;
    //    GOTO_EXIT_IF(0 != err, 1);
    //}

    if (prvt->prms.ph_sen_hndl->overload_set_exp_gain)
    {
        err = prvt->prms.ph_sen_hndl->overload_set_exp_gain(prvt->prms.socket_hndl,
                                                      &prvt->prms,
                                                      execute_cmd_strip,
                                                      &exp_gain);
        GOTO_EXIT_IF(0 != err, 1);
    } else {
        // Convert exposure to lines
        exp_time_lines = ((exp_gain.exposure * 1000) / prvt->prms.ft_curr.linetime_ns);

        // Set grouped parameter hold
        if (prvt->prms.ph_sen_hndl->set_grped_prm_hold) {
            err = prvt->prms.ph_sen_hndl->set_grped_prm_hold(prvt->prms.socket_hndl,
                (void*)prvt->prms.sens_features,
                (void*)execute_cmd_strip, 1);
            GOTO_EXIT_IF(0 != err, 1);
        }

        // Set exposure
        if (prvt->prms.ph_sen_hndl->set_exp) {
            err = prvt->prms.ph_sen_hndl->set_exp(prvt->prms.socket_hndl,
                (void*)execute_cmd_strip, exp_time_lines);
            GOTO_EXIT_IF(0 != err, 1);
        }

        // Set analog gain
        if (prvt->prms.ph_sen_hndl->set_gain) {
            err = prvt->prms.ph_sen_hndl->set_gain(prvt->prms.socket_hndl,
                (void*)execute_cmd_strip, exp_gain.again);
            GOTO_EXIT_IF(0 != err, 1);
        }

        //TODO: Set digital gain
        //err = prvt->prms.ph_sen_hndl->set_dgain(prvt->prms.socket_hndl,
        //    (void*)execute_cmd_strip, exp_gain.dgain);
        //GOTO_EXIT_IF(0 != err, 1);

        // Reset grouped parameter hold
        if (prvt->prms.ph_sen_hndl->set_grped_prm_hold) {
            err = prvt->prms.ph_sen_hndl->set_grped_prm_hold(prvt->prms.socket_hndl,
                (void*)prvt->prms.sens_features,
                (void*)execute_cmd_strip, 0);
            GOTO_EXIT_IF(0 != err, 1);
        }
    }

    prvt->prms.exp_gain_curr = exp_gain;

    return 0;

EXIT_1:
    mmsdbg(DL_ERROR, "//VIV HAL: Exit Err");
    return -1;
}

// Set frame duration [usec]
static int hai_cm_sensor_set_duration(hat_cm_sensor_handle_t hndl, uint32 duration)
{
    int err = 0;
    hat_cm_sensor_prvt_data_t *prvt;
    uint32 lpfr;

    prvt = (hat_cm_sensor_prvt_data_t *)hndl->prvt;

    // Calculate LPFR based on the new FPS and mode's linetime
    lpfr = (duration * 1000) / prvt->prms.ft_curr.linetime_ns;

    // Set LPFR
    if (prvt->prms.ph_sen_hndl->overload_set_duration)
    {
        err = prvt->prms.ph_sen_hndl->overload_set_duration(prvt->prms.socket_hndl,
                                                      &prvt->prms,
                                                      execute_cmd_strip,
                                                      duration);
        GOTO_EXIT_IF(0 != err, 1);
    }
    else if (prvt->prms.ph_sen_hndl->set_lpfr)
    {
        err = prvt->prms.ph_sen_hndl->set_lpfr(prvt->prms.socket_hndl,
            (void*)prvt->prms.sens_features,
            (void*)execute_cmd_strip, lpfr);
        GOTO_EXIT_IF(0 != err, 1);
    } else {
        return 0;
    }

    // Update LPFR related parameters
    prvt->prms.ft_curr.fps = 1000000.0/duration;
    prvt->prms.ft_curr.frametime_us =
        (lpfr * prvt->prms.ft_curr.linetime_ns) / 1000;
    prvt->prms.ft_curr.lpfr = lpfr;

    return 0;

EXIT_1:
    mmsdbg(DL_ERROR, "//VIV HAL: Exit Err");
    return -1;
}


static int hai_cm_sensor_get_dyn_props(hat_cm_sensor_handle_t hndl,
    hat_cm_sensor_dyn_props_t* dyn_props)
{
    hat_cm_sensor_prvt_data_t *prvt;
    int err = 0;
    prvt = (hat_cm_sensor_prvt_data_t *)hndl->prvt;

    prvt->prms.dyn_props.sensitivity_iso = prvt->prms.exp_gain_curr.again *
                                           prvt->prms.sens_features->base_ISO;

    prvt->prms.dyn_props.exposure_time_ns =
                                    prvt->prms.exp_gain_curr.exposure * 1000LL;

    //TODO
    prvt->prms.dyn_props.sensor_temperature_c = 0.0f;
    if(prvt->prms.sens_features->temp_sensor_supp &&
        prvt->prms.ph_sen_hndl->get_temperature) {
        err = prvt->prms.ph_sen_hndl->get_temperature(prvt->prms.socket_hndl,
                (void*)prvt->prms.sens_features,
                (void*)execute_cmd_strip,
                &prvt->prms.dyn_props.sensor_temperature_c);
        GOTO_EXIT_IF(0 != err, 1);
    }

    prvt->prms.dyn_props.frameDuration_ns = prvt->prms.ft_curr.lpfr *
                                            prvt->prms.ft_curr.linetime_ns;

    //TODO Dummy values for now
    prvt->prms.dyn_props.exp_start_timestamp_ns = 0;


    *dyn_props = prvt->prms.dyn_props;

    return 0;
EXIT_1:
    mmsdbg(DL_ERROR, "//VIV HAL: Exit Err");
    return -1;
}

static int hai_cm_sensor_get_features(hat_cm_sensor_handle_t hndl,
    hat_sensor_features_t** features)
{
    hat_cm_sensor_prvt_data_t *prvt;

    prvt = (hat_cm_sensor_prvt_data_t *)hndl->prvt;

    *features = (hat_sensor_features_t *)prvt->prms.sens_features;

    return 0;
}

static int hai_cm_sensor_get_stream_mode(hat_cm_sensor_handle_t hndl,
    HAT_CM_SENSOR_STREAM_MODE_T *mode)
{
    hat_cm_sensor_prvt_data_t *prvt;

    prvt = (hat_cm_sensor_prvt_data_t *)hndl->prvt;

    *mode = prvt->prms.stream_mode;

    return 0;
}

static int hai_cm_sensor_get_power_mode(hat_cm_sensor_handle_t hndl,
    HAT_CM_SENSOR_POWER_MODE_T *mode)
{
    hat_cm_sensor_prvt_data_t *prvt;

    prvt = (hat_cm_sensor_prvt_data_t *)hndl->prvt;

    *mode = prvt->prms.pwr_mode;

    return 0;
}

static int hai_cm_sensor_get_mode(hat_cm_sensor_handle_t hndl)
{
    return 0;
}

static int hai_cm_sensor_get_exp_gain(hat_cm_sensor_handle_t hndl,
    hat_exp_gain_t *exp_gain)
{
    hat_cm_sensor_prvt_data_t *prvt;

    prvt = (hat_cm_sensor_prvt_data_t *)hndl->prvt;

    *exp_gain = prvt->prms.exp_gain_curr;

    return 0;
}

static int hai_cm_sensor_get_fps(hat_cm_sensor_handle_t hndl)
{
    return 0;
}





/* ========================================================================== */
/**
* hai_cm_sensor_get_state()
*/
/* ========================================================================== */
static int hai_cm_sensor_get_state(hat_cm_sensor_handle_t hndl,
    HAT_CM_SENSOR_STATE_T *state)
{
    hat_cm_sensor_prvt_data_t *prvt;

    prvt = (hat_cm_sensor_prvt_data_t *)hndl->prvt;

    mmsdbg(DL_MESSAGE, "//VIV HAL: Sensor state: %d", prvt->prms.state);

    *state = prvt->prms.state;

    return 0;
}

/* ========================================================================== */
/**
* hai_cm_sensor_open()
*/
/* ========================================================================== */
static int hai_cm_sensor_open(hat_cm_sensor_handle_t hndl,
    void *sock_hndl, void *ph_sen_hndl, void* sens_features)
{
    hat_cm_sensor_prvt_data_t *prvt;
    prvt = (hat_cm_sensor_prvt_data_t *)hndl->prvt;
    prvt->prms.socket_hndl = (hat_cm_socket_handle_t)sock_hndl;
    GOTO_EXIT_IF(NULL == sock_hndl, 1);

    prvt->prms.ph_sen_hndl = (hat_cm_ph_sen_handle_t)ph_sen_hndl;
    GOTO_EXIT_IF(NULL == ph_sen_hndl, 1);

    prvt->prms.sens_features = (const hat_sensor_features_t*)sens_features;
    GOTO_EXIT_IF(NULL == sens_features, 1);



    mmsdbg(DL_PRINT, "//VIV HAL: %s, ID %d linked to physical sensor %010p and virtual "
        "socket handle %010p",
        prvt->cr_prms.name, prvt->cr_prms.id, prvt->prms.ph_sen_hndl,
        prvt->prms.socket_hndl);

    prvt->prms.state = SENSOR_STATE_OPENED;

    PROFILE_ADD(PROFILE_ID_SENSOR_TIME, 0x20BB, 0x00);
    //power up sensor
    {
        hat_cm_sensor_config_t cfg;
        cfg.cfg_type    = SENSOR_CFG_POWER_ON;
        cfg.cfg.sen_cfg = NULL;
        hndl->sen_obj_config(hndl,&cfg);
    }
    PROFILE_ADD(PROFILE_ID_SENSOR_TIME, 0x20C1, 0x00);
    //start sensor init
    {
        hat_cm_sensor_config_t cfg;
        int data;
        prvt->prms.socket_hndl->hal_socket_query(prvt->prms.socket_hndl,
            HAT_HW_QUERY_SENSOR_CLK, (void*)&data);
        cfg.cfg_type    = SENSOR_CFG_INIT;
        cfg.cfg.init_data = data;
        hndl->sen_obj_config(hndl,&cfg);
    }
    PROFILE_ADD(PROFILE_ID_SENSOR_TIME, 0x20EE, 0x00);
    return 0;

EXIT_1:
    mmsdbg(DL_ERROR, "//VIV HAL: Exit Err");
    return -1;
}

/* ========================================================================== */
/**
* hai_cm_sensor_config()
*/
/* ========================================================================== */
static int hai_cm_sensor_config(hat_cm_sensor_handle_t hndl, void* cfg)
{
    hat_cm_sensor_config_t* cfg_prms = cfg;
    int err = 0;

    switch (cfg_prms->cfg_type) {
        case SENSOR_CFG_DUMMY:
            mmsdbg(DL_WARNING, "//VIV HAL: SENSOR_CFG_DUMMY sensor config!");
            break;
        case SENSOR_CFG_SET_EXP_GAIN:
            err = hndl->set_exp_gain(hndl, cfg_prms->cfg.exp_gain);
            GOTO_EXIT_IF(0 != err, 1);
            break;
        case SENSOR_CFG_SET_FRAME_DURATION:
            err = hndl->set_frm_duration(hndl, cfg_prms->cfg.frm_duration);
            GOTO_EXIT_IF(0 != err, 1);
            break;
        case SENSOR_CFG_SET_MODE:
            err = hndl->set_mode(hndl, cfg_prms->cfg.sen_cfg);
            GOTO_EXIT_IF(0 != err, 1);
            break;
        case SENSOR_CFG_POWER_ON:
            err = hndl->set_power_mode(hndl, SENSOR_POWER_MODE_ON);
            GOTO_EXIT_IF(0 != err, 1);
            break;
        case SENSOR_CFG_POWER_OFF:
            err = hndl->set_power_mode(hndl, SENSOR_POWER_MODE_OFF);
            GOTO_EXIT_IF(0 != err, 1);
            break;
        case SENSOR_CFG_INIT:
            err = hndl->hal_cm_sen_init(hndl, cfg_prms->cfg.init_data);
            GOTO_EXIT_IF(0 != err, 1);
            break;
        default:
            mmsdbg(DL_ERROR, "//VIV HAL: No such a sensor config!");
            err = -1;
            GOTO_EXIT_IF(0 != err, 1);
    }

    return 0;

EXIT_1:
    mmsdbg(DL_ERROR, "//VIV HAL: Exit Err");
    return -1;
}

/* ========================================================================== */
/**
* hai_cm_sensor_control()
*/
/* ========================================================================== */
static int hai_cm_sensor_control(hat_cm_sensor_handle_t hndl, void* ctrl_prms)
{
    int err = 0;

    switch (((hat_cm_sensor_control_t*)ctrl_prms)->ctrl_type) {
        case SENSOR_CTRL_DUMMY:
            mmsdbg(DL_WARNING, "//VIV HAL: SENSOR_CTRL_DUMMY sensor control!");
            break;
        case SENSOR_CTRL_START_STOP:
            err = hndl->set_stream_mode(hndl,
                ((hat_cm_sensor_control_t*)ctrl_prms)->ctrl.mode);
            GOTO_EXIT_IF(0 != err, 1);
            break;
        default:
            mmsdbg(DL_ERROR, "//VIV HAL: No such a sensor control!");
            err = -1;
            GOTO_EXIT_IF(0 != err, 1);
    }

    return 0;

EXIT_1:
    mmsdbg(DL_ERROR, "//VIV HAL: Exit Err");
    return -1;
}

/* ========================================================================== */
/**
* hai_cm_sensor_process()
*/
/* ========================================================================== */
static int hai_cm_sensor_process(hat_cm_sensor_handle_t hndl, void* process_prms)
{
    int err = 0;

    switch (((hat_cm_sensor_process_t*)process_prms)->process_type) {
        case SENSOR_PROCESS_DUMMY:
            mmsdbg(DL_WARNING, "//VIV HAL: SENSOR_PROCESS_DUMMY sensor control!");
            break;
        default:
            mmsdbg(DL_ERROR, "//VIV HAL: No such a sensor process!");
            err = -1;
            GOTO_EXIT_IF(0 != err, 1);
    }

    return 0;

EXIT_1:
    mmsdbg(DL_ERROR, "//VIV HAL: Exit Err");
    return -1;
}

/* ========================================================================== */
/**
* hai_cm_sensor_query()
*/
/* ========================================================================== */
static int hai_cm_sensor_query(hat_cm_sensor_handle_t hndl, void* query_prms)
{
    int err = 0;

    switch (((hat_cm_sensor_query_t*)query_prms)->query_type) {
        case SENSOR_QUERY_DUMMY:
            mmsdbg(DL_WARNING, "//VIV HAL: SENSOR_QUERY_DUMMY sensor query!");
            break;
        case SENSOR_QUERY_GET_DYN_PROPS:
            err = hndl->get_dyn_props(hndl,
                ((hat_cm_sensor_query_t*)query_prms)->query.dyn_props);
            GOTO_EXIT_IF(0 != err, 1);
            break;
        case SENSOR_QUERY_GET_FEATURES:
            err = hndl->get_features(hndl,
                ((hat_cm_sensor_query_t*)query_prms)->query.features);
            GOTO_EXIT_IF(0 != err, 1);
            break;
        case SENSOR_QUERY_SELECT_MODE:
            err = hndl->select_mode(hndl,
                ((hat_cm_sensor_query_t*)query_prms)->query.sel_mode.mode_rqst,
                ((hat_cm_sensor_query_t*)query_prms)->query.sel_mode.mode_index);
            GOTO_EXIT_IF(0 != err, 1);
            break;
        default:
            mmsdbg(DL_ERROR, "//VIV HAL: No such a sensor query!");
            err = -1;
            GOTO_EXIT_IF(0 != err, 1);
    }

    return 0;

EXIT_1:
    mmsdbg(DL_ERROR, "//VIV HAL: Exit Err");
    return -1;
}

/* ========================================================================== */
/**
* hai_cm_sensor_close()
*/
/* ========================================================================== */
static int hai_cm_sensor_close(hat_cm_sensor_handle_t hndl)
{
    hat_cm_sensor_prvt_data_t *prvt;

    prvt = (hat_cm_sensor_prvt_data_t *)hndl->prvt;
    PROFILE_ADD(PROFILE_ID_SENSOR_STREAM_OFF, 0x00BB, 0x00); //Stream off begin
    {
        hndl->set_stream_mode(hndl,
                SENSOR_STREAM_MODE_OFF);
    }
    PROFILE_ADD(PROFILE_ID_SENSOR_STREAM_OFF, 0x00EE, 0x00); //Stream off end

    PROFILE_ADD(PROFILE_ID_SENSOR_STOP, 0x00BB, 0x00);//Sensor stop begin
    {
        hat_cm_sensor_config_t cfg;
        cfg.cfg_type    = SENSOR_CFG_POWER_OFF;
        cfg.cfg.sen_cfg = NULL;
        hndl->sen_obj_config(hndl,&cfg);
    }
    PROFILE_ADD(PROFILE_ID_SENSOR_STOP, 0x00EE, 0x00);//Sensor stop end

    prvt->prms.socket_hndl = NULL;
    prvt->prms.ph_sen_hndl = NULL;

    prvt->prms.state = SENSOR_STATE_CLOSED;

    return 0;
}

/* ========================================================================== */
/**
* hai_cm_sensor_destroy()
*/
/* ========================================================================== */
static int hai_cm_sensor_destroy(hat_cm_sensor_handle_t hndl)
{
    hat_cm_sensor_prvt_data_t *prvt;

    mmsdbg(DL_MESSAGE, "//VIV HAL: Enter");

    if (NULL == hndl)
    {
        mmsdbg(DL_ERROR, "//VIV HAL: NULL handle");
        return -1;
    }
    prvt = (hat_cm_sensor_prvt_data_t *)hndl->prvt;
    if (NULL == prvt)
    {
        mmsdbg(DL_ERROR, "//VIV HAL: NULL pointer");
        return -1;
    }
    else
    {
        mmsdbg(DL_MESSAGE, "//VIV HAL: %s, ID %d going to destroy at %010p",
            prvt->cr_prms.name, prvt->cr_prms.id, hndl);

        osal_free(hndl->prvt);
    }
    hndl->prvt = NULL;

    mmsdbg(DL_MESSAGE, "//VIV HAL: Exit Ok");

    return 0;
}

/* ========================================================================== */
/**
* hai_cm_sensor_create()
*/
/* ========================================================================== */
int hai_cm_sensor_create(hat_cm_sensor_handle_t hndl,
    hat_cm_sensor_cr_prms_t *cr_prms)
{
    hat_cm_sensor_prvt_data_t *prvt;

    PROFILE_ADD(PROFILE_ID_SENSOR_TIME, 0x10BB, 0x00);

    prvt = osal_calloc(1, sizeof(hat_cm_sensor_prvt_data_t));
    GOTO_EXIT_IF(NULL == prvt, 1);

    prvt->cr_prms.id = cr_prms->id;
    prvt->cr_prms.name = cr_prms->name;
    prvt->prms.state = SENSOR_STATE_CREATED;
    prvt->prms.fps_range.min = 10;
    prvt->prms.fps_range.max = 30;
    hndl->prvt = (void *)prvt;

    hndl->set_event_cb      = hai_cm_sensor_set_event_cb;
    hndl->set_power_mode    = hai_cm_sensor_set_power_mode;
    hndl->hal_cm_sen_init   = hai_cm_sensor_init;
    hndl->select_mode       = hai_cm_sensor_select_mode;
    hndl->set_mode          = hai_cm_sensor_set_mode;
    hndl->set_stream_mode   = hai_cm_sensor_set_stream_mode;
    hndl->set_exp_gain      = hai_cm_sensor_set_exp_gain;
    hndl->set_frm_duration  = hai_cm_sensor_set_duration;

    hndl->get_dyn_props     = hai_cm_sensor_get_dyn_props;
    hndl->get_features      = hai_cm_sensor_get_features;
    hndl->get_stream_mode   = hai_cm_sensor_get_stream_mode;
    hndl->get_power_mode    = hai_cm_sensor_get_power_mode;
    hndl->get_mode          = hai_cm_sensor_get_mode;
    hndl->get_exp_gain      = hai_cm_sensor_get_exp_gain;
    hndl->get_fps           = hai_cm_sensor_get_fps;
    hndl->get_state         = hai_cm_sensor_get_state;

    hndl->open              = hai_cm_sensor_open;
    hndl->sen_obj_config    = hai_cm_sensor_config;
    hndl->control           = hai_cm_sensor_control;
    hndl->process           = hai_cm_sensor_process;
    hndl->query             = hai_cm_sensor_query;
    hndl->close             = hai_cm_sensor_close;
    hndl->destroy           = hai_cm_sensor_destroy;

    mmsdbg(DL_MESSAGE, "//VIV HAL: %s, ID %d created at %010p",
        prvt->cr_prms.name, prvt->cr_prms.id, hndl);
    mmsdbg(DL_MESSAGE, "//VIV HAL: Exit Ok");

    PROFILE_ADD(PROFILE_ID_SENSOR_TIME, 0x10EE, 0x00);

    return 0;

EXIT_1:
    mmsdbg(DL_ERROR, "//VIV HAL: Exit Err");
    return -1;
}
