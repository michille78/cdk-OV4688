/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file dtp_cm_db_default.h
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#ifndef __DTP_CM_DB_DEFAULT_H__
#define __DTP_CM_DB_DEFAULT_H__

#include <hal/hal_camera_module/hat_cm_dtp_data.h>

static const hat_cm_socket_command_entry_t dummy_cm_det_seq[] = {
    CMD_ENTRY_DUMMY(),
};

static const hat_dtp_cm_desc_t cm_detect_list[] = {
    { // Dummy camera module
        .cm_dtp_id      = 0, //CM_DUMMY
        .cm_name        = "Dummy camera module",
        .sen_dtp_id     = 0, //SENSOR_DUMMY
        .lens_dtp_id    = 0, //LENS_DUMMY
        .lights_dtp_id  = 0, //LIGHTS_DUMMY
        .nvm_dtp_id     = 0, //NVM_DUMMY
        .detect_seq = {
            ARR_SIZE(dummy_cm_det_seq),
            dummy_cm_det_seq,
        },
        .cm_features = {
            .cp = {
                .sensor = {
                    .address = 0,
                    .register_size = 2,
                    .slave_addr_bit_size = 7,
                },
                .lens = {
                    .address = 0,
                    .register_size = 2,
                    .slave_addr_bit_size = 7,
                },
                .light = {
                    .address = 0,
                    .register_size = 2,
                    .slave_addr_bit_size = 7,
                },
                .nvm = {
                    .address = 0,
                    .register_size = 2,
                    .slave_addr_bit_size = 7,
                },
            },
        },
    },
};

static hat_dtp_cm_db_t dtp_cm_db_default = {
    //uint32 cm_list_size;
    (sizeof(cm_detect_list)/sizeof(cm_detect_list[0])),
    //hat_dtp_cm_desc_t (*cm_list);
    cm_detect_list,
};

#endif //__DTP_CM_DB_DEFAULT_H__
