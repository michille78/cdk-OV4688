/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file hal_cm_driver.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <hal/hal_camera_module/hat_cm_driver.h>
#include <hal/hal_camera_module/hat_cm_driver_prvt.h>

#include <hal/hal_camera_module/hai_cm_socket.h>
#include <hal/hal_camera_module/hai_cm_sensor.h>
#include <hal/hal_camera_module/hai_cm_lens.h>
#include <hal/hal_camera_module/hai_cm_nvm.h>

#include "dtp_cm_db_default.h"
#include "dtp_sensor_db_default.h"
#include "dtp_lens_db_default.h"
#include "dtp_lights_db_default.h"
#include "dtp_nvm_db_default.h"

#include <osal/osal_stdlib.h>

#include <error_handle/include/error_handle.h>

#include <utils/mms_debug.h>

mmsdbg_define_variable(
        hal_cm_driver,
        DL_DEFAULT,
        0,
        "hal_cm_driver",
        "HAL CM driver"
    );
#define MMSDEBUGLEVEL mmsdbg_use_variable(hal_cm_driver)

static int hai_cm_driver_get_imgif_ctx(hat_cm_driver_handle_t hndl,
    int* imgif_ctx)
{
    int err = 0;
    hat_cm_driver_prvt_data_t *prvt;

    prvt = (hat_cm_driver_prvt_data_t *)hndl->prvt;
    err = prvt->prms.socket_obj.hal_socket_query(&prvt->prms.socket_obj,
        HAT_HW_QUERY_CAMIF_CTX, (void*)imgif_ctx);

    return 0;
}

static int hai_cm_driver_get_camera_ids(hat_cm_driver_handle_t hndl, hat_cm_component_ids_t* component_ids)
{
    int err = 0;
    hat_cm_driver_prvt_data_t *prvt;

    prvt = (hat_cm_driver_prvt_data_t *)hndl->prvt;

    if (!prvt->prms.dtp_data.cm) {
        mmsdbg(DL_ERROR, "//VIV HAL: Camera Module - can't read DTP data");
        GOTO_EXIT_IF(0 != err, 1);
    }

    component_ids->cm_idx = prvt->prms.dtp_data.cm->cm_dtp_id;
    component_ids->sensor_idx = prvt->prms.dtp_data.cm->sen_dtp_id;
    component_ids->lens_idx = prvt->prms.dtp_data.cm->lens_dtp_id;
    component_ids->lights_idx = prvt->prms.dtp_data.cm->lights_dtp_id;
    component_ids->nvm_idx = prvt->prms.dtp_data.cm->nvm_dtp_id;

    return 0;

EXIT_1:
    mmsdbg(DL_ERROR, "//VIV HAL: Exit Err");
    return -1;
}

static int hai_cm_driver_camera_query(hat_cm_driver_handle_t hndl, void* query_prms)
{
    int err = 0;

    switch (((hat_cm_camera_query_t*)query_prms)->query_type) {
        case CAMERA_QUERY_DUMMY:
            mmsdbg(DL_WARNING, "//VIV HAL: CAMERA_QUERY_DUMMY sensor query!");
            break;
        case CAMERA_QUERY_GET_MOD_IDS:
            err = hai_cm_driver_get_camera_ids(hndl,
                    ((hat_cm_camera_query_t*)query_prms)->query.component_ids);
            GOTO_EXIT_IF(0 != err, 1);
            break;
        case CAMERA_QUERY_GET_IMGIF_CTX:
            err = hai_cm_driver_get_imgif_ctx(hndl,
                    &((hat_cm_camera_query_t*)query_prms)->query.imgif_ctx);
            GOTO_EXIT_IF(0 != err, 1);
            break;

        default:
            mmsdbg(DL_ERROR, "//VIV HAL: No such a camera query!");
            err = -1;
            GOTO_EXIT_IF(0 != err, 1);
    }

    return 0;

EXIT_1:
    mmsdbg(DL_ERROR, "//VIV HAL: Exit Err");
    return -1;
}

/* ========================================================================== */
/**
* hai_cm_driver_get_state()
*/
/* ========================================================================== */
static int hai_cm_driver_get_state(hat_cm_driver_handle_t hndl,
    HAT_CM_DRIVER_STATE_T *state)
{
    hat_cm_driver_prvt_data_t *prvt;

    prvt = (hat_cm_driver_prvt_data_t *)hndl->prvt;

    mmsdbg(DL_MESSAGE, "//VIV HAL: Socket state: %d", prvt->prms.state);

    *state = prvt->prms.state;

    return 0;
}

/* ========================================================================== */
/**
* hai_cm_driver_load_dtp()
*/
/* ========================================================================== */
static int hai_cm_driver_load_dtp(hat_cm_driver_handle_t hndl)
{
    int err = 0;
    hat_cm_driver_prvt_data_t *prvt;

    prvt = (hat_cm_driver_prvt_data_t *)hndl->prvt;

    hat_cm_driver_dtp_db_t* dtp_db_ptr = &(prvt->prms.dtp_db);

    dtp_db_ptr->cm.dtp_data      = NULL;
    dtp_db_ptr->sensor.dtp_data  = NULL;
    dtp_db_ptr->lens.dtp_data    = NULL;
    dtp_db_ptr->lights.dtp_data   = NULL;
    dtp_db_ptr->nvm.dtp_data     = NULL;

    // Try to load Camera Module DTP
    if (dtpsrv_get_hndl(dtp_srv_hndl, DTP_DB_ID_CD, DTP_ID_CD_CAMERA_MODULE,
        1,
        dtp_param_order,
        &dtp_db_ptr->cm.hndl_dtp, &dtp_db_ptr->cm.leaf)
    ) {
        mmsdbg(DL_ERROR, "Camera Module - can't get DTP client handle");
        dtp_db_ptr->cm.dtp_data = &dtp_cm_db_default;
    } else {
        if (dtpsrv_process(dtp_db_ptr->cm.hndl_dtp, NULL, NULL,
            (void**)&dtp_db_ptr->cm.dtp_data)) {
            mmsdbg(DL_ERROR, "Camera Module - can't process DTP client handle");
            dtp_db_ptr->cm.dtp_data = &dtp_cm_db_default;
        }
    }

    return (err);
}

/* ========================================================================== */
/**
* hai_cm_driver_unload_dtp()
*/
/* ========================================================================== */
static int hai_cm_driver_unload_dtp(hat_cm_driver_handle_t hndl)
{
    int err = 0;
    hat_cm_driver_prvt_data_t *prvt;

    prvt = (hat_cm_driver_prvt_data_t *)hndl->prvt;

    hat_cm_driver_dtp_db_t* dtp_db_ptr = &(prvt->prms.dtp_db);

    if (dtpsrv_free_hndl(dtp_db_ptr->cm.hndl_dtp)) {
        mmsdbg(DL_ERROR, "CM - can't free DTP handle!");
    }

    if (dtpsrv_free_hndl(dtp_db_ptr->sensor.hndl_dtp)) {
        mmsdbg(DL_ERROR, "Sensor - can't free DTP handle!");
    }

    if (dtpsrv_free_hndl(dtp_db_ptr->lens.hndl_dtp)) {
        mmsdbg(DL_ERROR, "Lens - can't free DTP handle!");
    }

    if (dtpsrv_free_hndl(dtp_db_ptr->lights.hndl_dtp)) {
        mmsdbg(DL_ERROR, "Light - can't free DTP handle!");
    }

    if (dtpsrv_free_hndl(dtp_db_ptr->nvm.hndl_dtp)) {
        mmsdbg(DL_ERROR, "NVM - can't free DTP handle!");
    }

    return (err);
}

/* ========================================================================== */
/**
* hai_cm_deattach()
*/
/* ========================================================================== */
static int hai_cm_deattach(hat_cm_driver_handle_t hndl)
{
    int err = 0;
    hat_cm_driver_prvt_data_t*  prvt = (hat_cm_driver_prvt_data_t *)(hndl->prvt);
    hat_cm_driver_dtp_db_t* dtp_db_ptr = &(prvt->prms.dtp_db);

    err += dtpsrv_free_hndl(dtp_db_ptr->nvm.hndl_dtp);
    err += dtpsrv_free_hndl(dtp_db_ptr->lights.hndl_dtp);
    err += dtpsrv_free_hndl(dtp_db_ptr->lens.hndl_dtp);
    err += dtpsrv_free_hndl(dtp_db_ptr->sensor.hndl_dtp);

    return err;
}
/* ========================================================================== */
/**
* hai_cm_attach()
*/
/* ========================================================================== */
static int hai_cm_attach(hat_cm_driver_handle_t hndl, const hat_dtp_cm_desc_t *cam_desc)
{
    hat_cm_driver_prvt_data_t*  prvt;
    dtpdb_dynamic_private_t     dtp_dyn_prv;
    int sensor_idx = 0,
        lens_idx = 0,
        lights_idx = 0,
        nvm_idx = 0;

    prvt = (hat_cm_driver_prvt_data_t *)(hndl->prvt);
    hat_cm_driver_dtp_db_t* dtp_db_ptr = &(prvt->prms.dtp_db);

    sensor_idx   = cam_desc->sen_dtp_id;
    lens_idx     = cam_desc->lens_dtp_id;
    lights_idx   = cam_desc->lights_dtp_id;
    nvm_idx      = cam_desc->nvm_dtp_id;

    // Init pointers to DTP data which this CM driver instance will work with
    prvt->prms.dtp_data.cm = (hat_dtp_cm_desc_t *)cam_desc;

    // Try to load Sensor DTP
    if (dtpsrv_get_hndl(dtp_srv_hndl, DTP_DB_ID_CD, DTP_ID_CD_SENSOR,
        1,
        dtp_param_order,
        &dtp_db_ptr->sensor.hndl_dtp, &dtp_db_ptr->sensor.leaf)
    ) {
        mmsdbg(DL_ERROR, "Sensor - can't get DTP client handle");
        dtp_db_ptr->sensor.dtp_data = (hat_dtp_sensor_desc_t*)&dtp_sensor_db_default;
    } else {
        dtp_dyn_prv.params[0] = sensor_idx;
        if (dtpsrv_process(dtp_db_ptr->sensor.hndl_dtp, NULL, &dtp_dyn_prv,
            (void**)&dtp_db_ptr->sensor.dtp_data)) {
            mmsdbg(DL_ERROR, "Sensor - can't process DTP client handle");
            dtp_db_ptr->sensor.dtp_data = (hat_dtp_sensor_desc_t*)&dtp_sensor_db_default;
        }
    }
    prvt->prms.dtp_data.sensor = (hat_dtp_sensor_desc_t*)dtp_db_ptr->sensor.dtp_data;

    // Try to load Lens DTP
    if (dtpsrv_get_hndl(dtp_srv_hndl, DTP_DB_ID_CD, DTP_ID_CD_LENS,
        1,
        dtp_param_order,
        &dtp_db_ptr->lens.hndl_dtp, &dtp_db_ptr->lens.leaf)
    ) {
        mmsdbg(DL_ERROR, "Lens - can't get DTP client handle");
        dtp_db_ptr->lens.dtp_data = (hat_dtp_lens_desc_t*)&dtp_lens_db_default;
    } else {
        dtp_dyn_prv.params[0] = lens_idx;
        if (dtpsrv_process(dtp_db_ptr->lens.hndl_dtp, NULL, &dtp_dyn_prv,
            (void**)&dtp_db_ptr->lens.dtp_data)) {
            mmsdbg(DL_ERROR, "Lens - can't process DTP client handle");
            dtp_db_ptr->lens.dtp_data = (hat_dtp_lens_desc_t*)&dtp_lens_db_default;
        }
    }
    prvt->prms.dtp_data.lens = (hat_dtp_lens_desc_t*)dtp_db_ptr->lens.dtp_data;

    // Try to load Flash DTP
    if (dtpsrv_get_hndl(dtp_srv_hndl, DTP_DB_ID_CD, DTP_ID_CD_LIGHTS,
        1,
        dtp_param_order,
        &dtp_db_ptr->lights.hndl_dtp, &dtp_db_ptr->lights.leaf)
    ) {
        mmsdbg(DL_ERROR, "Flash - can't get DTP client handle");
        dtp_db_ptr->lights.dtp_data = (hat_dtp_lights_desc_t*)&dtp_lights_db_default;
    } else {
        dtp_dyn_prv.params[0] = lights_idx;
        if (dtpsrv_process(dtp_db_ptr->lights.hndl_dtp, NULL, &dtp_dyn_prv,
            (void**)&dtp_db_ptr->lights.dtp_data)) {
            mmsdbg(DL_ERROR, "Flash - can't process DTP client handle");
            dtp_db_ptr->lights.dtp_data = (hat_dtp_lights_desc_t*)&dtp_lights_db_default;
        }
    }
    prvt->prms.dtp_data.lights = (hat_dtp_lights_desc_t*)dtp_db_ptr->lights.dtp_data;


    // Try to load NVM DTP
    if (dtpsrv_get_hndl(dtp_srv_hndl, DTP_DB_ID_CD, DTP_ID_CD_NVM,
        1,
        dtp_param_order,
        &dtp_db_ptr->nvm.hndl_dtp, &dtp_db_ptr->nvm.leaf)
    ) {
        mmsdbg(DL_ERROR, "NVM - can't get DTP client handle");
        dtp_db_ptr->nvm.dtp_data = (hat_dtp_nvm_desc_t*)&dtp_nvm_db_default;
    } else {
        dtp_dyn_prv.params[0] = nvm_idx;
        if (dtpsrv_process(dtp_db_ptr->nvm.hndl_dtp, NULL, &dtp_dyn_prv,
            (void**)&dtp_db_ptr->nvm.dtp_data)) {
            mmsdbg(DL_ERROR, "NVM - can't process DTP client handle");
            dtp_db_ptr->nvm.dtp_data = (hat_dtp_nvm_desc_t*)&dtp_nvm_db_default;
        }
    }
    prvt->prms.dtp_data.nvm = (hat_dtp_nvm_desc_t*)dtp_db_ptr->nvm.dtp_data;

    // Init pointers to CM component physical driver handles
    prvt->prms.phys_handles.sensor = hal_get_sensor_driver(sensor_idx);
    prvt->prms.phys_handles.lens   = hal_get_lens_driver(lens_idx);
    prvt->prms.phys_handles.lights = hal_get_lights_driver(lights_idx);
    prvt->prms.phys_handles.nvm    = hal_get_nvm_driver(nvm_idx);

    return 0;
}

/* ========================================================================== */
/**
* hai_cm_driver_check_socket_mask()
*/
/* ========================================================================== */
static int hai_cm_driver_check_socket_mask(hat_cm_driver_handle_t hndl,
    uint32 cm_num)
{
    hat_cm_driver_prvt_data_t*  prvt;
    hat_dtp_cm_db_t*            cm_detect_list;

    prvt = (hat_cm_driver_prvt_data_t *)(hndl->prvt);
    cm_detect_list = (hat_dtp_cm_db_t*)(prvt->prms.dtp_db.cm.dtp_data);

    return ( !cm_detect_list->cm_entries[cm_num].socket_mask ||
             (
                (1 << prvt->cr_prms.id) &
                cm_detect_list->cm_entries[cm_num].socket_mask
             )
           );
}

/* ========================================================================== */
/**
* hai_cm_driver_detect_()
*/
/* ========================================================================== */
hat_cm_driver_detected_t detected_socket;
static int hai_cm_driver_detect_(hat_cm_driver_handle_t hndl)
{
    int                         err = 0;
    hat_cm_driver_prvt_data_t*  prvt;
    uint32                      cm_num, cm_count = 0;
    hat_dtp_cm_db_t*            cm_detect_list;
    hat_cm_socket_cmd_strip_result_t res;

    prvt = (hat_cm_driver_prvt_data_t *)(hndl->prvt);

    cm_detect_list = (hat_dtp_cm_db_t*)(prvt->prms.dtp_db.cm.dtp_data);

    cm_count = cm_detect_list->entry_count;

    for (cm_num = 0; cm_num < cm_count; cm_num++)
    {
        res.detected = 0;
        hai_cm_attach(hndl, &cm_detect_list->cm_entries[cm_num]);

        err += prvt->prms.socket_obj.hal_socket_open(&prvt->prms.socket_obj,
          (hat_cm_socket_open_params_t *)&cm_detect_list->cm_entries[cm_num].cm_features.cp);

        if (hai_cm_driver_check_socket_mask(hndl, cm_num))
            err += execute_cmd_strip(&prvt->prms.socket_obj,
                cm_detect_list->cm_entries[cm_num].detect_seq.entries,
                &res);

        GOTO_EXIT_IF(0 != err, 1);

        if(res.detected) {
            mmsdbg(DL_ERROR, "\n\nSensor (%s) idx %d is detected.\n",
                cm_detect_list->cm_entries[cm_num].cm_name, cm_num);
            detected_socket.cm_entries[prvt->cr_prms.id] =
                &cm_detect_list->cm_entries[cm_num];
            detected_socket.module_desc[prvt->cr_prms.id] = prvt->prms.dtp_data;
            return 0;
        }

        if (cm_num < cm_count-1) {
            // TODO: Better handling,
            // last dtp handles will be detached when camera exit
            err += hai_cm_deattach(hndl);
        }
    }

EXIT_1:
    return -1;
}

/* ========================================================================== */
/**
* hai_cm_driver_detect()
*/
/* ========================================================================== */
static int hai_cm_driver_detect(hat_cm_driver_handle_t hndl)
{
    int                         err = 0;
    hat_cm_driver_prvt_data_t*  prvt;
    prvt = (hat_cm_driver_prvt_data_t *)(hndl->prvt);
    int id;

    GOTO_EXIT_IF(prvt == NULL, 1);

    id = prvt->cr_prms.id;
    GOTO_EXIT_IF(id >= PH_SOCKET_MAX, 1);

    if (detected_socket.cm_entries[id] == NULL) {
        /* Full detect sequence */
        err =  hai_cm_driver_detect_(hndl);
        GOTO_EXIT_IF(0 != err, 1);
    } else {
        /* Already detected sensor */
        err = hai_cm_attach(hndl, detected_socket.cm_entries[id]);
        GOTO_EXIT_IF(0 != err, 1);
        err = prvt->prms.socket_obj.hal_socket_open(&prvt->prms.socket_obj,
          (hat_cm_socket_open_params_t *)&detected_socket.cm_entries[id]->
                cm_features.cp);
        GOTO_EXIT_IF(0 != err, 2);
    }

    return 0;
EXIT_2:
    err = hai_cm_deattach(hndl);
EXIT_1:
    mmsdbg(DL_ERROR, "HAL: Exit Err: %d", err);
    return -1;
}

/* ========================================================================== */
/**
* hai_cm_driver_open()
*/
/* ========================================================================== */
static int hai_cm_driver_open(hat_cm_driver_handle_t hndl)
{
    int err = 0;
    hat_cm_driver_prvt_data_t *prvt;
    hat_cm_sensor_cr_prms_t   sensor_cr_prms;
    hat_cm_lens_cr_prms_t     lens_cr_prms;
    hat_cm_lights_cr_prms_t   lights_cr_prms;
    hat_cm_nvm_cr_prms_t      nvm_cr_prms;
    void*                     this;

    prvt = (hat_cm_driver_prvt_data_t *)hndl->prvt;

    PROFILE_ADD(PROFILE_ID_CAMERA_TIME, 0x13BB, 0x00);
    // Create a sensor object for this socket
    sensor_cr_prms.id   = prvt->prms.dtp_data.sensor->sensor_id;
    sensor_cr_prms.name = prvt->prms.dtp_data.sensor->sensor_name;
    err = hai_cm_sensor_create(&prvt->prms.sensor_obj, &sensor_cr_prms);
    GOTO_EXIT_IF(0 != err, 1);

    this = (void*)&prvt->prms.sensor_obj;
    GOTO_EXIT_IF(NULL == this, 1);
    PROFILE_ADD(PROFILE_ID_CAMERA_TIME, 0x131C, 0x00);
    // Open the sensor object and pass to it Socket handle
    // (needed for HW resource operations) and a handle for the
    // low-level simple physical sensor driver and DTP sensor features
    err = ((hat_cm_sensor_obj_t*)this)->open(this,
        &prvt->prms.socket_obj,
        prvt->prms.phys_handles.sensor,
        &prvt->prms.dtp_data.sensor->sensor_features);
    GOTO_EXIT_IF(0 != err, 1);

    PROFILE_ADD(PROFILE_ID_CAMERA_TIME, 0x132C, 0x00);
    // Create a lens object for this socket
    lens_cr_prms.id   = prvt->prms.dtp_data.lens->lens_id;
    lens_cr_prms.name = prvt->prms.dtp_data.lens->lens_name;
    err = hai_cm_lens_create(&prvt->prms.lens_obj, &lens_cr_prms);
    GOTO_EXIT_IF(0 != err, 1);

    this = (void*)&prvt->prms.lens_obj;
    GOTO_EXIT_IF(NULL == this, 1);

    PROFILE_ADD(PROFILE_ID_CAMERA_TIME, 0x133C, 0x00);
    // Open the lens object and pass to it Socket handle
    // (needed for HW resource operations) and a handle for the
    // low-level simple physical lens driver and DTP lens features
    err = ((hat_cm_lens_obj_t*)this)->cm_lens_open(this,
        &prvt->prms.socket_obj,
        prvt->prms.phys_handles.lens,
        &prvt->prms.dtp_data.lens->lens_features);
    GOTO_EXIT_IF(0 != err, 1);

    PROFILE_ADD(PROFILE_ID_CAMERA_TIME, 0x134C, 0x00);
    // Create a lights object for this socket
    lights_cr_prms.id   = prvt->prms.dtp_data.lights->lights_id;
    lights_cr_prms.name = prvt->prms.dtp_data.lights->lights_name;
    err = hai_cm_lights_create(&prvt->prms.lights_obj, &lights_cr_prms);
    GOTO_EXIT_IF(0 != err, 1);

    this = (void*)&prvt->prms.lights_obj;
    GOTO_EXIT_IF(NULL == this, 1);

    PROFILE_ADD(PROFILE_ID_CAMERA_TIME, 0x135C, 0x00);
    // Open the lights object and pass to it Socket handle
    // (needed for HW resource operations) and a handle for the
    // low-level simple physical light driver and DTP lights features
    err = ((hat_cm_lights_obj_t*)this)->open(this,
        &prvt->prms.socket_obj,
        prvt->prms.phys_handles.lights,
        &prvt->prms.dtp_data.lights->lights_features);
    GOTO_EXIT_IF(0 != err, 1);

    PROFILE_ADD(PROFILE_ID_CAMERA_TIME, 0x136C, 0x00);
    // Create a nvm object for this socket
    nvm_cr_prms.id   = prvt->prms.dtp_data.nvm->nvm_id;
    nvm_cr_prms.name = prvt->prms.dtp_data.nvm->nvm_name;
    err = hai_cm_nvm_create(&prvt->prms.nvm_obj, &nvm_cr_prms);
    GOTO_EXIT_IF(0 != err, 1);

    this = (void*)&prvt->prms.nvm_obj;
    GOTO_EXIT_IF(NULL == this, 1);

    PROFILE_ADD(PROFILE_ID_CAMERA_TIME, 0x137C, 0x00);
    // Open the nvm object and pass to it Socket handle
    // (needed for HW resource operations) and a handle for the
    // low-level simple physical nvm driver and DTP nvm features
    err = ((hat_cm_nvm_obj_t*)this)->open(this,
        &prvt->prms.socket_obj,
        prvt->prms.phys_handles.nvm,
        &prvt->prms.dtp_data.nvm->nvm_features);
    GOTO_EXIT_IF(0 != err, 1);

    prvt->prms.state = CM_DRV_STATE_OPENED;

    PROFILE_ADD(PROFILE_ID_CAMERA_TIME, 0x13EE, 0x00);

    return 0;

EXIT_1:
    mmsdbg(DL_ERROR, "//VIV HAL: Exit Err");
    return -1;
}

/* ========================================================================== */
/**
* hai_cm_driver_config()
*/
/* ========================================================================== */
static int hai_cm_driver_config(hat_cm_driver_handle_t hndl,
    HAT_CM_DRIVER_COMPONENT_T comp, void* cfg_prms)
{
    int err = 0;
    hat_cm_driver_prvt_data_t *prvt;

    prvt = (hat_cm_driver_prvt_data_t *)hndl->prvt;

    switch(comp) {
        case CM_DRV_COMP_DUMMY:
            mmsdbg(DL_WARNING, "//VIV HAL: CM_DRV_COMP_DUMMY config!");
            break;
        case CM_DRV_COMP_SENSOR:
            err = prvt->prms.sensor_obj.sen_obj_config(&prvt->prms.sensor_obj,
                                                       (hat_cm_sensor_config_t*)cfg_prms);
            GOTO_EXIT_IF(0 != err, 1);
            break;
        case CM_DRV_COMP_LENS:
            err = prvt->prms.lens_obj.cm_lens_config(&prvt->prms.lens_obj, (hat_cm_lens_config_t*)cfg_prms);
            GOTO_EXIT_IF(0 != err, 1);
            break;
        case CM_DRV_COMP_LIGHT:
            err = prvt->prms.lights_obj.config(&prvt->prms.lights_obj, cfg_prms);
            GOTO_EXIT_IF(0 != err, 1);
            break;
        case CM_DRV_COMP_NVM:
            err = prvt->prms.nvm_obj.config(&prvt->prms.nvm_obj, cfg_prms);
            GOTO_EXIT_IF(0 != err, 1);
            break;
        default:
            mmsdbg(DL_ERROR, "//VIV HAL: No such a component!");
            err = -1;
            GOTO_EXIT_IF(0 != err, 1);
    }

    return 0;

EXIT_1:
    mmsdbg(DL_ERROR, "//VIV HAL: Exit Err");
    return -1;
}

/* ========================================================================== */
/**
* hai_cm_driver_control()
*/
/* ========================================================================== */
static int hai_cm_driver_control(hat_cm_driver_handle_t hndl,
    HAT_CM_DRIVER_COMPONENT_T comp, void* ctrl_prms)
{
    int err = 0;
    hat_cm_driver_prvt_data_t *prvt;

    prvt = (hat_cm_driver_prvt_data_t *)hndl->prvt;

    switch(comp) {
        case CM_DRV_COMP_DUMMY:
            mmsdbg(DL_WARNING, "//VIV HAL: CM_DRV_COMP_DUMMY control!");
            break;
        case CM_DRV_COMP_SENSOR:
            err = prvt->prms.sensor_obj.control(&prvt->prms.sensor_obj, ctrl_prms);
            GOTO_EXIT_IF(0 != err, 1);
            break;
        case CM_DRV_COMP_LENS:
            err = prvt->prms.lens_obj.cm_lens_control(&prvt->prms.lens_obj, ctrl_prms);
            GOTO_EXIT_IF(0 != err, 1);
            break;
        case CM_DRV_COMP_LIGHT:
            err = prvt->prms.lights_obj.control(&prvt->prms.lights_obj, ctrl_prms);
            GOTO_EXIT_IF(0 != err, 1);
            break;
        case CM_DRV_COMP_NVM:
            err = prvt->prms.nvm_obj.control(&prvt->prms.nvm_obj, ctrl_prms);
            GOTO_EXIT_IF(0 != err, 1);
            break;
        default:
            mmsdbg(DL_ERROR, "//VIV HAL: No such a component!");
            err = -1;
            GOTO_EXIT_IF(0 != err, 1);
    }

    return 0;

EXIT_1:
    mmsdbg(DL_ERROR, "//VIV HAL: Exit Err");
    return -1;
}

/* ========================================================================== */
/**
* hai_cm_driver_process()
*/
/* ========================================================================== */
static int hai_cm_driver_process(hat_cm_driver_handle_t hndl,
    HAT_CM_DRIVER_COMPONENT_T comp, void* process_prms)
{
    int err = 0;
    hat_cm_driver_prvt_data_t *prvt;

    prvt = (hat_cm_driver_prvt_data_t *)hndl->prvt;

    switch(comp) {
        case CM_DRV_COMP_DUMMY:
            mmsdbg(DL_WARNING, "//VIV HAL: CM_DRV_COMP_DUMMY process!");
            break;
        case CM_DRV_COMP_SENSOR:
            err = prvt->prms.sensor_obj.process(&prvt->prms.sensor_obj, process_prms);
            GOTO_EXIT_IF(0 != err, 1);
            break;
        case CM_DRV_COMP_LENS:
            err = prvt->prms.lens_obj.cm_lens_process(&prvt->prms.lens_obj, process_prms);
            GOTO_EXIT_IF(0 != err, 1);
            break;
        case CM_DRV_COMP_LIGHT:
            err = prvt->prms.lights_obj.process(&prvt->prms.lights_obj, process_prms);
            GOTO_EXIT_IF(0 != err, 1);
            break;
        case CM_DRV_COMP_NVM:
            err = prvt->prms.nvm_obj.process(&prvt->prms.nvm_obj, process_prms);
            GOTO_EXIT_IF(0 != err, 1);
            break;
        default:
            mmsdbg(DL_ERROR, "//VIV HAL: No such a component!");
            err = -1;
            GOTO_EXIT_IF(0 != err, 1);
    }

    return 0;

EXIT_1:
    mmsdbg(DL_ERROR, "//VIV HAL: Exit Err");
    return -1;
}

/* ========================================================================== */
/**
* hai_cm_driver_query()
*/
/* ========================================================================== */
static int hai_cm_driver_query(hat_cm_driver_handle_t hndl,
    HAT_CM_DRIVER_COMPONENT_T comp, void* query_prms)
{
    int err = 0;
    hat_cm_driver_prvt_data_t *prvt;

    prvt = (hat_cm_driver_prvt_data_t *)hndl->prvt;

    switch(comp) {
        case CM_DRV_COMP_DUMMY:
            mmsdbg(DL_WARNING, "//VIV HAL: CM_DRV_COMP_DUMMY query!");
            break;
        case CM_DRV_COMP_SENSOR:
            err = prvt->prms.sensor_obj.query(&prvt->prms.sensor_obj, query_prms);
            GOTO_EXIT_IF(0 != err, 1);
            break;
        case CM_DRV_COMP_LENS:
            err = prvt->prms.lens_obj.cm_lens_query(&prvt->prms.lens_obj, query_prms);
            GOTO_EXIT_IF(0 != err, 1);
            break;
        case CM_DRV_COMP_LIGHT:
            err = prvt->prms.lights_obj.query(&prvt->prms.lights_obj, query_prms);
            GOTO_EXIT_IF(0 != err, 1);
            break;
        case CM_DRV_COMP_NVM:
            err = prvt->prms.nvm_obj.query(&prvt->prms.nvm_obj, query_prms);
            GOTO_EXIT_IF(0 != err, 1);
            break;
        case CM_DRV_COMP_CAMERA:
            err = hai_cm_driver_camera_query(hndl, query_prms);
            GOTO_EXIT_IF(0 != err, 1);
            break;
        default:
            mmsdbg(DL_ERROR, "//VIV HAL: No such a component!");
            err = -1;
            GOTO_EXIT_IF(0 != err, 1);
    }

    return 0;

EXIT_1:
    mmsdbg(DL_ERROR, "//VIV HAL: Exit Err");
    return -1;
}

/* ========================================================================== */
/**
* hai_cm_driver_close()
*/
/* ========================================================================== */
static int hai_cm_driver_close(hat_cm_driver_handle_t hndl)
{
    int err = 0;
    hat_cm_driver_prvt_data_t *prvt;

    mmsdbg(DL_MESSAGE, "//VIV HAL: Enter");

    prvt = (hat_cm_driver_prvt_data_t *)hndl->prvt;

    // Close the nvm object
    err = prvt->prms.nvm_obj.close(&prvt->prms.nvm_obj);
    GOTO_EXIT_IF(0 != err, 1);

    // Destroy the nvm object
    err = prvt->prms.nvm_obj.destroy(&prvt->prms.nvm_obj);
    GOTO_EXIT_IF(0 != err, 1);

    // Close the lights object
    err = prvt->prms.lights_obj.close(&prvt->prms.lights_obj);
    GOTO_EXIT_IF(0 != err, 1);

    // Destroy the lights object
    err = prvt->prms.lights_obj.destroy(&prvt->prms.lights_obj);
    GOTO_EXIT_IF(0 != err, 1);

    // Close the lens object
    err = prvt->prms.lens_obj.cm_lens_close(&prvt->prms.lens_obj);
    GOTO_EXIT_IF(0 != err, 1);

    // Destroy the lens object
    err = prvt->prms.lens_obj.cm_lens_destroy(&prvt->prms.lens_obj);
    GOTO_EXIT_IF(0 != err, 1);

    // Close the sensor object
    err = prvt->prms.sensor_obj.close(&prvt->prms.sensor_obj);
    GOTO_EXIT_IF(0 != err, 1);

    // Destroy the sensor object
    err = prvt->prms.sensor_obj.destroy(&prvt->prms.sensor_obj);
    GOTO_EXIT_IF(0 != err, 1);

    prvt->prms.state = CM_DRV_STATE_CLOSED;

    mmsdbg(DL_MESSAGE, "//VIV HAL: Exit Ok");

    return 0;

EXIT_1:
    mmsdbg(DL_ERROR, "//VIV HAL: Exit Err");
    return -1;
}

/* ========================================================================== */
/**
* hai_cm_driver_destroy()
*/
/* ========================================================================== */
static int hai_cm_driver_destroy(hat_cm_driver_handle_t hndl)
{
    int err = 0;
    hat_cm_driver_prvt_data_t *prvt;

    mmsdbg(DL_MESSAGE, "//VIV HAL: Enter");

    prvt = (hat_cm_driver_prvt_data_t *)hndl->prvt;

    mmsdbg(DL_MESSAGE, "//VIV HAL: %s, ID %d going to destroy at %010p",
        prvt->cr_prms.name, prvt->cr_prms.id, hndl);

    // Close CM socket object for this CM driver
    err = prvt->prms.socket_obj.hal_socket_close(&prvt->prms.socket_obj);
    GOTO_EXIT_IF(0 != err, 1);

    // Destroy CM socket object for this CM driver
    err = prvt->prms.socket_obj.hal_socket_destroy(&prvt->prms.socket_obj);
    GOTO_EXIT_IF(0 != err, 1);

    // Unload CM DTP database
    err = hndl->unload_dtp(hndl);
    GOTO_EXIT_IF(0 != err, 1);

    prvt->cr_prms.id = 0;
    prvt->cr_prms.name= NULL;
    prvt->prms.state = CM_DRV_STATE_UNKNOWN;

    osal_free(hndl->prvt);

    osal_free(hndl);

    mmsdbg(DL_MESSAGE, "//VIV HAL: Exit Ok");

    return 0;

EXIT_1:
    mmsdbg(DL_ERROR, "//VIV HAL: Exit Err");
    return -1;
}

/* ========================================================================== */
/**
* hai_cm_driver_create()
*/
/* ========================================================================== */
int hai_cm_driver_create(hat_cm_driver_handle_t* hndl,
    hat_cm_driver_cr_prms_t *cr_prms)
{
    int err = 0;
    hat_cm_driver_prvt_data_t *prvt;
    hat_cm_socket_cr_prms_t   socket_cr_prms;
    hat_cm_driver_handle_t    cm_hndl = NULL;

    cm_hndl = osal_malloc(sizeof(hat_cm_driver_obj_t));
    GOTO_EXIT_IF(NULL == cm_hndl, 1);
    cm_hndl->prvt = osal_malloc(sizeof(hat_cm_driver_prvt_data_t));
    GOTO_EXIT_IF(NULL == cm_hndl->prvt, 1);

    prvt = cm_hndl->prvt;

    prvt->cr_prms.id    = cr_prms->id;
    prvt->cr_prms.name  = cr_prms->name;

    cm_hndl->get_state  =   hai_cm_driver_get_state;
    cm_hndl->load_dtp   =   hai_cm_driver_load_dtp;
    cm_hndl->unload_dtp =   hai_cm_driver_unload_dtp;
    cm_hndl->detect     =   hai_cm_driver_detect;
    cm_hndl->open       =   hai_cm_driver_open;
    cm_hndl->hal_cm_config =   hai_cm_driver_config;
    cm_hndl->hal_cm_control  =   hai_cm_driver_control;
    cm_hndl->process    =   hai_cm_driver_process;
    cm_hndl->query      =   hai_cm_driver_query;
    cm_hndl->close      =   hai_cm_driver_close;
    cm_hndl->destroy    =   hai_cm_driver_destroy;

    // Load entire CM DTP database
    err = hai_cm_driver_load_dtp(cm_hndl);
    GOTO_EXIT_IF(0 != err, 1);

    // Create a socket for this CM driver
    socket_cr_prms.id   = cr_prms->id;
    socket_cr_prms.name = cr_prms->name;
    err = hai_cm_socket_create(&prvt->prms.socket_obj, &socket_cr_prms);
    GOTO_EXIT_IF(0 != err, 1);

    // Open the socket to search for its HW resources
    err = prvt->prms.socket_obj.hal_socket_create(&prvt->prms.socket_obj);
    GOTO_EXIT_IF(0 != err, 1);

    prvt->prms.state    = CM_DRV_STATE_CREATED;

    *hndl = cm_hndl;

    mmsdbg(DL_MESSAGE, "//VIV HAL: %s, ID %d created at %010p",
        prvt->cr_prms.name, prvt->cr_prms.id, hndl);
    mmsdbg(DL_MESSAGE, "//VIV HAL: Exit Ok");

    return 0;

EXIT_1:
    mmsdbg(DL_ERROR, "//VIV HAL: Exit Err");
    return -1;
}

PLAT_PH_SOCKET_T platform_cm_cam_to_sock(uint32 cam_id);
int hai_cm_driver_get_camera_limits(int camera_id,
                                    hat_camera_limits_t *limits)
{
    hat_cm_driver_dtp_data_t *module_descr;
    const hat_sensor_mode_t *sm;
    int socket;
    int last_mode_idx;
    socket = platform_cm_cam_to_sock(camera_id);
    if (detected_socket.cm_entries[socket] == NULL) {
        mmsdbg(DL_ERROR, "Requested camera %d was not detected", camera_id);
        return -1;
    }
    module_descr = &detected_socket.module_desc[socket];
    last_mode_idx = 0;  //module_descr->sensor->sensor_features.modes.num-1;
    sm = &module_descr->sensor->sensor_features.modes.list[last_mode_idx];
    limits->maxWidth  = sm->active.w;
    limits->maxHeight = sm->active.h;
    limits->maxBpp    = sm->format.bpc;
    limits->maxPixels = limits->maxWidth * limits->maxHeight;
    return 0;
}


