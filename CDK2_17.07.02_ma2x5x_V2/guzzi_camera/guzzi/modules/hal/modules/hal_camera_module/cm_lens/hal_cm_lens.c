/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file hal_cm_lens.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include "../../../../libs/error_handle/include/error_handle.h"
#include "../../../../libs/func_thread/include/func_thread.h"
#include "../../../../osal/include/osal/osal_mutex.h"
#include "../../../../osal/include/osal/osal_stdlib.h"
#include "../../../../osal/include/osal/osal_stdtypes.h"
#include "../../../../osal/include/osal/osal_time.h"
#include "../../../../osal/include/utils/dummy/mms_debug.h"
#include "../../../../osal/include/utils/mms_debug.h"
#include "../../../include/hal/hal_camera_module/hat_cm_lens.h"
#include "../../../include/hal/hal_camera_module/hat_cm_lens_prvt.h"
#include "../../../include/hal/hal_camera_module/hat_cm_socket.h"
#include "../../../include/hal/hal_camera_module/hai_cm_socket.h"
#include <hal/modules/hal_camera_module/lenses/lens_phys.h>

mmsdbg_define_variable(
        hal_cm_lens,
        DL_DEFAULT,
        0,
        "hal_cm_lens",
        "HAL CM lens"
    );
#define MMSDEBUGLEVEL mmsdbg_use_variable(hal_cm_lens)



int lens_move_cb(hat_cm_lens_thread_data_t *p, LENS_MOVE_DESCR_T descr, hat_cm_lens_micro_step_t *step)
{
    int err = 0;

    if (p->move_done_cb) {
        if (step == NULL)
        {
            err = p->move_done_cb(p->user_data,
                                  descr,
                                  p->curr_pos,
                                  p->curr_pos,
                                  0);
        } else
        {
            err = p->move_done_cb(p->user_data,
                                  descr,
                                  p->curr_pos,
                                  step->lens_pos,
                                  step->time_sleep_us);
        }
    }
    return err;
}

static void lens_move_to_pos_do(func_thread_t *ethr, void *app_prv)
{
    hat_cm_lens_thread_data_t   *p = app_prv;
    int lens_abort = 0;
    float pos_to_move_to = 0.0;
    hat_cm_lens_micro_step_t *step;

    mmsdbg(DL_FUNC, "Enter");
    if(!ethr || !p ) {
        mmsdbg(DL_ERROR, "Null pointer");
        return;
    }

    osal_mutex_lock(p->move_list_lock);
    step = hat_cm_lens_move_list_pop_first(p->move_list);
    while (step != NULL)
    {
        osal_mutex_lock(p->abort_lock);
        lens_abort = p->abort;
        osal_mutex_unlock(p->abort_lock);

        if (!lens_abort) {
            // Inform client for lens movement
            if (lens_move_cb(p, LENS_MOVE_PROGRESS, step))
                goto ERR;

            pos_to_move_to = step->lens_pos;

            if (pos_to_move_to < 0.0) {
                pos_to_move_to = 0.0;
                mmsdbg(DL_WARNING, "Lens reached 0.0, ignore current move!");
            }
            if (pos_to_move_to > 1.0) {
                pos_to_move_to = 1.0;
                mmsdbg(DL_WARNING, "Lens reached 1.0, ignore current move!");
            }

            if (p->ph_lens_hndl->af_move_to_pos(p->socket_hndl,
                                                  (void*)execute_cmd_strip,
                                                  pos_to_move_to))
            {
                goto ERR;
            }

            if (osal_usleep(step->time_sleep_us))
                goto ERR;

            PROFILE_ADD(PROFILE_ID_SENSOR_LENS, (pos_to_move_to*1023), step->time_sleep_us);

            p->curr_pos = pos_to_move_to;
        } else {
            mmsdbg(DL_ERROR, "Lens movement aborted!");
            if (lens_move_cb(p, LENS_MOVE_ABORT, NULL))
                goto ERR;
            osal_mutex_lock(p->abort_lock);
            p->abort = 0;
            osal_mutex_unlock(p->abort_lock);
            break;
        }
        step = hat_cm_lens_move_list_pop_next(p->move_list);
    }
    PROFILE_ADD(PROFILE_ID_SENSOR_LENS, 0, 0);
    lens_move_cb(p, LENS_MOVE_LAST, NULL);
    osal_mutex_unlock(p->move_list_lock);

    mmsdbg(DL_FUNC, "Exit");
    return;

ERR:
    lens_move_cb(p, LENS_MOVE_ERROR, NULL);
    osal_mutex_unlock(p->move_list_lock);
    mmsdbg(DL_ERROR, "Error executing af_move_to_pos()!");
    mmsdbg(DL_FUNC, "Exit");
}

static void lens_rel_move_do(func_thread_t *ethr, void *app_prv)
{
    hat_cm_lens_thread_data_t   *p = app_prv;
    int lens_abort = 0;

    float pos_to_move_to = 0.0;
    hat_cm_lens_micro_step_t *step;

    mmsdbg(DL_FUNC, "Enter");
    if(!ethr || !p) {
        mmsdbg(DL_ERROR, "Null pointer");
        return;
    }

    osal_mutex_lock(p->move_list_lock);
    step = hat_cm_lens_move_list_pop_first(p->move_list);
    while (step != NULL)
    {

        osal_mutex_lock(p->abort_lock);
        lens_abort = p->abort;
        osal_mutex_unlock(p->abort_lock);

        if (!lens_abort) {
            // Inform client for lens movement
            if (lens_move_cb(p, LENS_MOVE_PROGRESS, step))
                goto ERR;

            pos_to_move_to = p->curr_pos + step->lens_pos;

            if (pos_to_move_to < 0.0) {
                pos_to_move_to = 0.0;
                mmsdbg(DL_WARNING, "Lens reached 0.0, ignore current move!");
            }
            if (pos_to_move_to > 1.0) {
                pos_to_move_to = 1.0;
                mmsdbg(DL_WARNING, "Lens reached 1.0, ignore current move!");
            }

            if (p->ph_lens_hndl->af_move_to_pos(p->socket_hndl,
                                                  (void*)execute_cmd_strip,
                                                  pos_to_move_to))
            {
                goto ERR;
            }

            PROFILE_ADD(PROFILE_ID_SENSOR_LENS, (pos_to_move_to*1023), step->time_sleep_us);
            if (osal_usleep(step->time_sleep_us))
                goto ERR;

            p->curr_pos = pos_to_move_to;
        } else {
            mmsdbg(DL_WARNING, "Lens movement aborted!");
            if (lens_move_cb(p, LENS_MOVE_ABORT, NULL))
                goto ERR;
            osal_mutex_lock(p->abort_lock);
            p->abort = 0;
            osal_mutex_unlock(p->abort_lock);
            break;
        }
        step = hat_cm_lens_move_list_pop_next(p->move_list);
    }
    lens_move_cb(p, LENS_MOVE_LAST, NULL);
    PROFILE_ADD(PROFILE_ID_SENSOR_LENS, 0, 0);
    osal_mutex_unlock(p->move_list_lock);

    mmsdbg(DL_FUNC, "Exit");
    return;

ERR:
    lens_move_cb(p, LENS_MOVE_ERROR, NULL);
    osal_mutex_unlock(p->move_list_lock);
    mmsdbg(DL_ERROR, "Error executing af_move_to_pos()!");
    mmsdbg(DL_FUNC, "Exit");
}

static void lens_move_to_def_pos_do(func_thread_t *ethr, void *app_prv)
{
    hat_cm_lens_thread_data_t   *p = app_prv;
    int err = 0;
    hat_cm_lens_micro_step_t *step;

    mmsdbg(DL_FUNC, "Enter");
    if(!ethr || !p) {
        mmsdbg(DL_ERROR, "Null pointer");
        return;
    }

    osal_mutex_lock(p->move_list_lock);

    step = hat_cm_lens_move_list_pop_first(p->move_list);
    while (step != NULL)
    {
        lens_move_cb(p, LENS_MOVE_PROGRESS, step);
        err = p->ph_lens_hndl->af_move_to_pos(
            p->socket_hndl,
            (void*)execute_cmd_strip,
            step->lens_pos);

        if (err) {
            osal_mutex_unlock(p->move_list_lock);
            mmsdbg(DL_ERROR, "Error executing af_move_to_pos()!");
            lens_move_cb(p, LENS_MOVE_ABORT, NULL);
            return;
        }

        p->curr_pos = step->lens_pos;

        err = osal_usleep(step->time_sleep_us);

        if (err) {
            osal_mutex_unlock(p->move_list_lock);
            mmsdbg(DL_ERROR, "Error executing osal_usleep()!");
            lens_move_cb(p, LENS_MOVE_ERROR, NULL);
            return;
        }
        step = hat_cm_lens_move_list_pop_next(p->move_list);
    }
    lens_move_cb(p, LENS_MOVE_LAST, NULL);
    osal_mutex_unlock(p->move_list_lock);

    mmsdbg(DL_FUNC, "Exit");
}

static void lens_pos_get(func_thread_t *ethr, void *app_prv)
{
    hat_cm_lens_thread_data_t   *p = app_prv;

    mmsdbg(DL_FUNC, "Enter");
    if(!ethr || !p) {
        mmsdbg(DL_ERROR, "Null pointer");
        return;
    }

    osal_mutex_lock(p->curr_pos_lock);
    p->curr_pos_to_client = p->curr_pos;
    osal_mutex_unlock(p->curr_pos_lock);

    mmsdbg(DL_FUNC, "Exit");
}

static func_thread_handle_t lens_thread_fxns[] = {
    lens_move_to_pos_do,
    lens_move_to_def_pos_do,
    lens_pos_get,
};

static func_thread_handles_t lens_thread_handles = {
    .fxns = lens_thread_fxns,
    .size = ARRAY_SIZE(lens_thread_fxns)
};




/* ========================================================================== */
/**
* hai_cm_lens_set_event_cb()
*/
/* ========================================================================== */
static int hai_cm_lens_set_event_cb(hat_cm_lens_handle_t hndl)
{
    return 0;
}

/* ========================================================================== */
/**
* hai_cm_lens_set_power_mode()
*/
/* ========================================================================== */
static int hai_cm_lens_set_power_mode(hat_cm_lens_handle_t hndl,
    HAT_CM_LENS_POWER_MODE_T mode)
{
    int err = 0;
    hat_cm_lens_prvt_data_t *prvt;

    prvt = (hat_cm_lens_prvt_data_t *)hndl->prvt;

    switch (mode) {
        case LENS_POWER_MODE_OFF:
            err = prvt->prms.ph_lens_hndl->power_off(prvt->prms.socket_hndl,
                (void*)prvt->prms.lens_features, (void*)execute_cmd_strip);
            GOTO_EXIT_IF(0 != err, 1);
            prvt->prms.pwr_mode = mode;
            break;

        case LENS_POWER_MODE_ON:
            err = prvt->prms.ph_lens_hndl->power_on(prvt->prms.socket_hndl,
                (void*)prvt->prms.lens_features, (void*)execute_cmd_strip);
            GOTO_EXIT_IF(0 != err, 1);
            prvt->prms.pwr_mode = mode;
            break;

        default:
            err = -1;
            GOTO_EXIT_IF(0 != err, 1);
    }

    return 0;

EXIT_1:
    mmsdbg(DL_ERROR, "//VIV HAL: Exit Err");
    return -1;
}

/* ========================================================================== */
/**
* hai_cm_lens_init()
*/
/* ========================================================================== */
static int hai_cm_lens_init(hat_cm_lens_handle_t hndl)
{
    int err = 0;
    hat_cm_lens_prvt_data_t *prvt;

    prvt = (hat_cm_lens_prvt_data_t *)hndl->prvt;

    // Init lens chip
    err = prvt->prms.ph_lens_hndl->init(prvt->prms.socket_hndl,
        (void*)prvt->prms.lens_features, (void*)execute_cmd_strip);
    GOTO_EXIT_IF(0 != err, 1);

    osal_mutex_lock(prvt->prms.thread_data.move_list_lock);

    //TODO: Get default position/time from the Tuning data
    // Prepare move_list to go to default initial position
    osal_mutex_lock(prvt->prms.lens_move_list.lock);
    prvt->prms.lens_move_list.head = 0;
    prvt->prms.lens_move_list.tail = 0;
    osal_mutex_unlock(prvt->prms.lens_move_list.lock);
    hat_cm_lens_move_list_push(&prvt->prms.lens_move_list, 0.0, 10000);
    prvt->prms.thread_data.move_list = &prvt->prms.lens_move_list;

    // Try to set lens to default position
    err = func_thread_exec(prvt->prms.thread, lens_move_to_def_pos_do);
    if (err) {
        osal_mutex_unlock(prvt->prms.thread_data.move_list_lock);
        mmsdbg(DL_ERROR, "Error executing func_thread_exec()!");
        GOTO_EXIT_IF(0 != err, 1);
    }

    osal_mutex_unlock(prvt->prms.thread_data.move_list_lock);

    return 0;

EXIT_1:
    mmsdbg(DL_ERROR, "//VIV HAL: Exit Err");
    return -1;
}

/* ========================================================================== */
/**
* hai_cm_lens_move_to_pos()
*/
/* ========================================================================== */
static int hai_cm_lens_move_to_pos(hat_cm_lens_handle_t hndl,
    hat_cm_lens_move_list_t* move_list,
    hat_cm_lens_move_cb_t micro_step_done_cb,
    void *user_data)
{
    int err = 0;
    hat_cm_lens_prvt_data_t *prvt;

    prvt = (hat_cm_lens_prvt_data_t *)hndl->prvt;

    if (!osal_mutex_trylock(prvt->prms.thread_data.move_list_lock)) {

        // Copy move_list locally
        prvt->prms.thread_data.move_list    = move_list;
        prvt->prms.thread_data.move_done_cb = micro_step_done_cb;
        prvt->prms.thread_data.user_data    = user_data;
        prvt->prms.thread_data.abort        = 0;

        err = func_thread_exec(prvt->prms.thread, lens_move_to_pos_do);
        if (err) {
            osal_mutex_unlock(prvt->prms.thread_data.move_list_lock);
            mmsdbg(DL_ERROR, "Error executing func_thread_exec()!");
            GOTO_EXIT_IF(0 != err, 1);
        }

        osal_mutex_unlock(prvt->prms.thread_data.move_list_lock);
    } else {
        mmsdbg(DL_ERROR, "Lens is still moving - skip requested lens move!");
    }

    return 0;

EXIT_1:
    mmsdbg(DL_ERROR, "//VIV HAL: Exit Err");
    return -1;
}

/* ========================================================================== */
/**
* hai_cm_lens_move()
*/
/* ========================================================================== */
static int hai_cm_lens_move(hat_cm_lens_handle_t hndl,
    hat_cm_lens_move_list_t* move_list,
    hat_cm_lens_move_cb_t micro_step_done_cb,
    void *user_data)
{
    int err = 0;
    hat_cm_lens_prvt_data_t *prvt;

    prvt = (hat_cm_lens_prvt_data_t *)hndl->prvt;

    if (!osal_mutex_trylock(prvt->prms.thread_data.move_list_lock)) {

        // Copy move_list locally
        prvt->prms.thread_data.move_list    = move_list;
        prvt->prms.thread_data.move_done_cb = micro_step_done_cb;
        prvt->prms.thread_data.user_data    = user_data;
        prvt->prms.thread_data.abort        = 0;

        err = func_thread_exec(prvt->prms.thread, lens_rel_move_do);
        if (err) {
            osal_mutex_unlock(prvt->prms.thread_data.move_list_lock);
            mmsdbg(DL_ERROR, "Error executing func_thread_exec()!");
            GOTO_EXIT_IF(0 != err, 1);
        }

        osal_mutex_unlock(prvt->prms.thread_data.move_list_lock);
    } else {
        mmsdbg(DL_ERROR, "Lens is still moving - skip requested lens move!");
    }

    return 0;

EXIT_1:
    mmsdbg(DL_ERROR, "//VIV HAL: Exit Err");
    return -1;
}

/* ========================================================================== */
/**
* hai_cm_lens_meas_pos()
*/
/* ========================================================================== */
static int hai_cm_lens_meas_pos(hat_cm_lens_handle_t hndl, float *pos_val)
{
    int err = 0;
    hat_cm_lens_prvt_data_t *prvt;

    prvt = (hat_cm_lens_prvt_data_t *)hndl->prvt;

    err = func_thread_exec(prvt->prms.thread, lens_pos_get);
    GOTO_EXIT_IF(0 != err, 1);

    osal_mutex_lock(prvt->prms.thread_data.curr_pos_lock);
    *pos_val = prvt->prms.thread_data.curr_pos_to_client;
    osal_mutex_unlock(prvt->prms.thread_data.curr_pos_lock);

    return 0;

EXIT_1:
    mmsdbg(DL_ERROR, "//VIV HAL: Exit Err");
    return -1;
}

/* ========================================================================== */
/**
* hai_cm_lens_park()
*/
/* ========================================================================== */
static int hai_cm_lens_park(hat_cm_lens_handle_t hndl)
{
    return 0;
}

/* ========================================================================== */
/**
* hai_cm_lens_abort()
*/
/* ========================================================================== */
static int hai_cm_lens_abort(hat_cm_lens_handle_t hndl)
{
    int err = 0;
    hat_cm_lens_prvt_data_t *prvt;

    prvt = (hat_cm_lens_prvt_data_t *)hndl->prvt;

    // Set abort flag to inform lens thread to stop movement
    osal_mutex_lock(prvt->prms.thread_data.abort_lock);
    prvt->prms.thread_data.abort = 1;
    osal_mutex_unlock(prvt->prms.thread_data.abort_lock);

    osal_mutex_lock(prvt->prms.thread_data.move_list_lock);

    //TODO: Get default position from the Tuning data
    // Prepare move_list to go to default initial position
    osal_mutex_lock(prvt->prms.lens_move_list.lock);
    prvt->prms.lens_move_list.head = 0;
    prvt->prms.lens_move_list.tail = 0;
    osal_mutex_unlock(prvt->prms.lens_move_list.lock);
    hat_cm_lens_move_list_push(&prvt->prms.lens_move_list, 0.0, 10000);
    prvt->prms.thread_data.move_list = &prvt->prms.lens_move_list;
    // Try to set lens to default position
    err = func_thread_exec(prvt->prms.thread, lens_move_to_def_pos_do);
    if (err) {
        osal_mutex_unlock(prvt->prms.thread_data.move_list_lock);
        mmsdbg(DL_ERROR, "Error executing func_thread_exec()!");
        GOTO_EXIT_IF(0 != err, 1);
    }

    osal_mutex_unlock(prvt->prms.thread_data.move_list_lock);

    return 0;

EXIT_1:
    mmsdbg(DL_ERROR, "//VIV HAL: Exit Err");
    return -1;
}

/* ========================================================================== */
/**
* hai_cm_lens_set_shutter()
*/
/* ========================================================================== */
static int hai_cm_lens_set_shutter(hat_cm_lens_handle_t hndl, int set)
{
    int err = 0;
    hat_cm_lens_prvt_data_t *prvt;

    prvt = (hat_cm_lens_prvt_data_t *)hndl->prvt;

    err = prvt->prms.ph_lens_hndl->set_shutter(prvt->prms.socket_hndl,
        (void*)execute_cmd_strip, set);
    GOTO_EXIT_IF(0 != err, 1);

    return 0;

EXIT_1:
    mmsdbg(DL_ERROR, "//VIV HAL: Exit Err");
    return -1;
}

/* ========================================================================== */
/**
* hai_cm_lens_set_aperture()
*/
/* ========================================================================== */
static int hai_cm_lens_set_aperture(hat_cm_lens_handle_t hndl, float* aper)
{
    int err = 0;
    hat_cm_lens_prvt_data_t *prvt;

    prvt = (hat_cm_lens_prvt_data_t *)hndl->prvt;

    err = prvt->prms.ph_lens_hndl->set_aperture(prvt->prms.socket_hndl,
        (void*)execute_cmd_strip, aper);
    GOTO_EXIT_IF(0 != err, 1);

    return 0;

EXIT_1:
    mmsdbg(DL_ERROR, "//VIV HAL: Exit Err");
    return -1;
}

/* ========================================================================== */
/**
* hai_cm_lens_set_dens_filter()
*/
/* ========================================================================== */
static int hai_cm_lens_set_dens_filter(hat_cm_lens_handle_t hndl, float* filter)
{
    int err = 0;
    hat_cm_lens_prvt_data_t *prvt;

    prvt = (hat_cm_lens_prvt_data_t *)hndl->prvt;

    err = prvt->prms.ph_lens_hndl->set_dens_filter(prvt->prms.socket_hndl,
        (void*)execute_cmd_strip, filter);
    GOTO_EXIT_IF(0 != err, 1);

    return 0;

EXIT_1:
    mmsdbg(DL_ERROR, "//VIV HAL: Exit Err");
    return -1;
}

/* ========================================================================== */
/**
* hai_cm_lens_set_o_stab()
*/
/* ========================================================================== */
static int hai_cm_lens_set_o_stab(hat_cm_lens_handle_t hndl, int mode)
{
    int err = 0;
    hat_cm_lens_prvt_data_t *prvt;

    prvt = (hat_cm_lens_prvt_data_t *)hndl->prvt;

    err = prvt->prms.ph_lens_hndl->set_o_stab(prvt->prms.socket_hndl,
        (void*)execute_cmd_strip, mode);
    GOTO_EXIT_IF(0 != err, 1);

    return 0;

EXIT_1:
    mmsdbg(DL_ERROR, "//VIV HAL: Exit Err");
    return -1;
}

/* ========================================================================== */
/**
* hai_cm_lens_foc_lenght()
*/
/* ========================================================================== */
static int hai_cm_lens_foc_lenght(hat_cm_lens_handle_t hndl, float* focal_lenght)
{
    int err = 0;
    hat_cm_lens_prvt_data_t *prvt;

    prvt = (hat_cm_lens_prvt_data_t *)hndl->prvt;

    err = prvt->prms.ph_lens_hndl->set_foc_lenght(prvt->prms.socket_hndl,
        (void*)execute_cmd_strip, focal_lenght);
    GOTO_EXIT_IF(0 != err, 1);

    return 0;

EXIT_1:
    mmsdbg(DL_ERROR, "//VIV HAL: Exit Err");
    return -1;
}

/* ========================================================================== */
/**
* hai_cm_lens_nvm_data_set()
*/
/* ========================================================================== */
static int hai_cm_lens_nvm_data_set(hat_cm_lens_handle_t hndl, void* data)
{
    return 0;
}

/* ========================================================================== */
/**
* hai_cm_lens_tuning_data_set()
*/
/* ========================================================================== */
static int hai_cm_lens_tuning_data_set(hat_cm_lens_handle_t hndl, void* data)
{
    return 0;
}

/* ========================================================================== */
/**
* hai_cm_lens_get_state()
*/
/* ========================================================================== */
static int hai_cm_lens_get_state(hat_cm_lens_handle_t hndl,
    HAT_CM_LENS_STATE_T *state)
{
    hat_cm_lens_prvt_data_t *prvt;

    prvt = (hat_cm_lens_prvt_data_t *)hndl->prvt;

    mmsdbg(DL_MESSAGE, "//VIV HAL: Lens state: %d", prvt->prms.state);

    *state = prvt->prms.state;

    return 0;
}

/* ========================================================================== */
/**
* hai_cm_lens_get_power_mode()
*/
/* ========================================================================== */
static int hai_cm_lens_get_power_mode(hat_cm_lens_handle_t hndl,
    HAT_CM_LENS_POWER_MODE_T *mode)
{
    hat_cm_lens_prvt_data_t *prvt;

    prvt = (hat_cm_lens_prvt_data_t *)hndl->prvt;

    *mode = prvt->prms.pwr_mode;

    return 0;
}

static int hai_cm_lens_get_dyn_props(hat_cm_lens_handle_t hndl,
    hat_cm_lens_dyn_props_t* dyn_props)
{
    hat_cm_lens_prvt_data_t *prvt = (hat_cm_lens_prvt_data_t *)hndl->prvt;

    const hat_lens_features_t *features = prvt->prms.lens_features;
    int err = 0;
    int optical_stab_mode = 0; //OFF
    float inf_pos, mac_pos, curr_pos, distance;


    prvt->prms.dyn_props.aperture_size = 1.0;
    if(features->available_apertures_size > 1) {
        err = prvt->prms.ph_lens_hndl->get_aperture(prvt->prms.socket_hndl,
            (void*)execute_cmd_strip, &prvt->prms.dyn_props.aperture_size);
        GOTO_EXIT_IF(0 != err, 1);
    } else if (features->available_apertures_size == 1) {
        prvt->prms.dyn_props.aperture_size = features->available_apertures[0];
    }

    prvt->prms.dyn_props.filter_density = 1.0;
    if(features->available_filt_densities_size > 1) {
        err = prvt->prms.ph_lens_hndl->get_dens_filter(prvt->prms.socket_hndl,
            (void*)execute_cmd_strip, &prvt->prms.dyn_props.filter_density);
        GOTO_EXIT_IF(0 != err, 1);
    } else if (features->available_filt_densities_size == 1){
        prvt->prms.dyn_props.filter_density = features->available_filt_densities[0];
    }

    prvt->prms.dyn_props.focal_length = 1.0;
    if(features->available_focal_lengths_size > 1) {
        err = prvt->prms.ph_lens_hndl->get_foc_lenght(prvt->prms.socket_hndl,
            (void*)execute_cmd_strip, &prvt->prms.dyn_props.focal_length);
        GOTO_EXIT_IF(0 != err, 1);
    } else if (features->available_focal_lengths_size == 1){
        prvt->prms.dyn_props.focal_length = features->available_focal_lengths[0];
    }

    err = prvt->prms.ph_lens_hndl->get_o_stab(prvt->prms.socket_hndl,
        (void*)execute_cmd_strip, &optical_stab_mode);
    GOTO_EXIT_IF(0 != err, 1);

    prvt->prms.dyn_props.optical_stab_mode = optical_stab_mode;

    // Calculate distance
    //TODO read infinity and macro pos from EEPROM
    inf_pos = features->inf_pos;
    mac_pos = features->mac_pos;

    err = hai_cm_lens_meas_pos(hndl, &curr_pos);
    GOTO_EXIT_IF(0 != err, 1);

    distance = 0.0; // infinity

    //Check borders
    if(curr_pos < inf_pos) {
        curr_pos = inf_pos;
    }
    if(curr_pos > mac_pos) {
        curr_pos = mac_pos;
    }

    //Calculate distance to target in [diopter]
    if(inf_pos < mac_pos) {
        distance = (curr_pos - inf_pos) / (mac_pos - inf_pos) * 10;
    }

    prvt->prms.dyn_props.state = 0; // STATIONARY
    if (prvt->prms.dyn_props.focus_distance != distance)
    {
        prvt->prms.dyn_props.state = 1; // MOVING

        prvt->prms.dyn_props.focus_distance = distance;
        prvt->prms.dyn_props.focus_range.near = distance + features->dist_tol;
        prvt->prms.dyn_props.focus_range.far = 0.0;
        if(distance > features->dist_tol) {
            prvt->prms.dyn_props.focus_range.far = distance - features->dist_tol;
        }
    }

    *dyn_props = prvt->prms.dyn_props;


    return 0;

EXIT_1:
    mmsdbg(DL_ERROR, "//VIV HAL: Exit Err");
    return -1;
}

/* ========================================================================== */
/**
* hai_cm_lens_get_features()
*/
/* ========================================================================== */
static int hai_cm_lens_get_features(hat_cm_lens_handle_t hndl,
    hat_lens_features_t** features)
{
    hat_cm_lens_prvt_data_t *prvt;

    prvt = (hat_cm_lens_prvt_data_t *)hndl->prvt;

    *features = (hat_lens_features_t *)prvt->prms.lens_features;

    return 0;
}

/* ========================================================================== */
/**
* hai_cm_lens_open()
*/
/* ========================================================================== */
static int hai_cm_lens_open(hat_cm_lens_handle_t hndl,
    void *sock_hndl, void *ph_lens_hndl, void* lens_features)
{
    hat_cm_lens_prvt_data_t *prvt;
    prvt = (hat_cm_lens_prvt_data_t *)hndl->prvt;

    prvt->prms.socket_hndl = (hat_cm_socket_handle_t)sock_hndl;
    GOTO_EXIT_IF(NULL == sock_hndl, 1);

    prvt->prms.ph_lens_hndl = (hat_cm_ph_lens_handle_t)ph_lens_hndl;
    GOTO_EXIT_IF(NULL == ph_lens_hndl, 1);

    prvt->prms.lens_features = (const hat_lens_features_t*)lens_features;
    GOTO_EXIT_IF(NULL == lens_features, 1);

    mmsdbg(DL_MESSAGE, "//VIV HAL: %s, ID %d linked to physical lens %010p and virtual "
        "socket handle %010p",
        prvt->cr_prms.name, prvt->cr_prms.id, prvt->prms.ph_lens_hndl,
        prvt->prms.socket_hndl);

    // Copy socket_hndl & ph_lens_hndl to thread_data also!!!
    prvt->prms.thread_data.socket_hndl  = prvt->prms.socket_hndl;
    prvt->prms.thread_data.ph_lens_hndl = prvt->prms.ph_lens_hndl;

    prvt->prms.state = LENS_STATE_OPENED;
#if 0
    //power up Lens
    {
        hat_cm_lens_control_t ctrl;
        ctrl.ctrl_type    = LENS_CFG_POWER_ON;
        ctrl.ctrl.init_params = NULL;
        hndl->cm_lens_control(hndl,&cfg);
    }
#endif
    //start sensor init
    {
        hat_cm_lens_control_t ctrl;
        int data;
        prvt->prms.socket_hndl->hal_socket_query(prvt->prms.socket_hndl,
            HAT_HW_QUERY_SENSOR_CLK, (void*)&data);
        ctrl.ctrl_type    = LENS_CTRL_INIT;
        ctrl.ctrl.init_params = NULL;
        hndl->cm_lens_control(hndl,&ctrl);
    }
    PROFILE_ADD(PROFILE_ID_SENSOR_TIME, 0x20EE, 0x00);


    return 0;

EXIT_1:
    mmsdbg(DL_ERROR, "//VIV HAL: Exit Err");
    return -1;
}

/* ========================================================================== */
/**
* hai_cm_lens_config()
*/
/* ========================================================================== */
static int hai_cm_lens_config(hat_cm_lens_handle_t hndl, hat_cm_lens_config_t* cfg_prms)
{
    int err = 0;

    switch (cfg_prms->cfg_type) {
        case LENS_CFG_DUMMY:
            mmsdbg(DL_WARNING, "//VIV HAL: LENS_CFG_DUMMY lens config!");
            break;
        case LENS_CFG_MOVE_TO_POS:
            err = hndl->move_to_pos(hndl,
                                    cfg_prms->cfg.move_to_pos.move_list,
                                    cfg_prms->cfg.move_to_pos.micro_step_done_cb,
                                    cfg_prms->cfg.move_to_pos.user_data);
            GOTO_EXIT_IF(0 != err, 1);
            break;
        case LENS_CFG_MOVE:
            err = hndl->move(hndl,
                            cfg_prms->cfg.move.move_list,
                            cfg_prms->cfg.move.micro_step_done_cb,
                            cfg_prms->cfg.move.user_data);
            GOTO_EXIT_IF(0 != err, 1);
            break;
        default:
            mmsdbg(DL_ERROR, "//VIV HAL: No such a lens config!");
            err = -1;
            GOTO_EXIT_IF(0 != err, 1);
    }

    return 0;

EXIT_1:
    mmsdbg(DL_ERROR, "//VIV HAL: Exit Err");
    return -1;
}

/* ========================================================================== */
/**
* hai_cm_lens_control()
*/
/* ========================================================================== */
static int hai_cm_lens_control(hat_cm_lens_handle_t hndl, void* ctrl_prms)
{
    int err = 0;

    switch (((hat_cm_lens_control_t*)ctrl_prms)->ctrl_type) {
        case LENS_CTRL_DUMMY:
            mmsdbg(DL_WARNING, "//VIV HAL: LENS_CTRL_DUMMY lens control!");
            break;
        case LENS_CTRL_INIT:
            err = hndl->cm_lens_init(hndl);
            GOTO_EXIT_IF(0 != err, 1);
            break;
        case LENS_CTRL_ABORT:
            err = hndl->cm_lens_abort(hndl);
            GOTO_EXIT_IF(0 != err, 1);
            break;
        case LENS_CTRL_SET_APERTURE:
            err = hndl->set_aperture(hndl,
                ((hat_cm_lens_control_t*)ctrl_prms)->ctrl.aperture);
            GOTO_EXIT_IF(0 != err, 1);
            break;
        case LENS_CTRL_SET_DENSITY_FILTER:
            err = hndl->set_dens_filter(hndl,
                ((hat_cm_lens_control_t*)ctrl_prms)->ctrl.dens_filter);
            GOTO_EXIT_IF(0 != err, 1);
            break;
        case LENS_CTRL_SET_OPTIC_STAB_MODE:
            err = hndl->set_o_stab(hndl,
                ((hat_cm_lens_control_t*)ctrl_prms)->ctrl.o_stab_mode);
            GOTO_EXIT_IF(0 != err, 1);
            break;
        case LENS_CTRL_SET_FOCAL_LENGHT:
            err = hndl->set_foc_lenght(hndl,
                ((hat_cm_lens_control_t*)ctrl_prms)->ctrl.focal_lenght);
            GOTO_EXIT_IF(0 != err, 1);
            break;
        default:
            mmsdbg(DL_ERROR, "//VIV HAL: No such a lens control!");
            err = -1;
            GOTO_EXIT_IF(0 != err, 1);
    }

    return 0;

EXIT_1:
    mmsdbg(DL_ERROR, "//VIV HAL: Exit Err");
    return -1;
}

/* ========================================================================== */
/**
* hai_cm_lens_process()
*/
/* ========================================================================== */
static int hai_cm_lens_process(hat_cm_lens_handle_t hndl, void* process_prms)
{
    int err = 0;

    switch (((hat_cm_lens_process_t*)process_prms)->process_type) {
        case LENS_PROCESS_DUMMY:
            mmsdbg(DL_WARNING, "//VIV HAL: LENS_PROCESS_DUMMY lens process!");
            break;
        default:
            mmsdbg(DL_ERROR, "//VIV HAL: No such a lens process!");
            err = -1;
            GOTO_EXIT_IF(0 != err, 1);
    }

    return 0;

EXIT_1:
    mmsdbg(DL_ERROR, "//VIV HAL: Exit Err");
    return -1;
}

/* ========================================================================== */
/**
* hai_cm_lens_query()
*/
/* ========================================================================== */
static int hai_cm_lens_query(hat_cm_lens_handle_t hndl, void* query_prms)
{
    int err = 0;

    switch (((hat_cm_lens_query_t*)query_prms)->query_type) {
        case LENS_QUERY_DUMMY:
            mmsdbg(DL_WARNING, "//VIV HAL: LENS_QUERY_DUMMY lens query!");
            break;
        case LENS_QUERY_GET_DYN_PROPS:
            err = hndl->cm_lens_get_dyn_props(hndl,
                ((hat_cm_lens_query_t*)query_prms)->query.dyn_props);
            GOTO_EXIT_IF(0 != err, 1);
            break;
        case LENS_QUERY_GET_FEATURES:
            err = hndl->cm_lens_get_features(hndl,
                ((hat_cm_lens_query_t*)query_prms)->query.features);
            GOTO_EXIT_IF(0 != err, 1);
            break;
        case LENS_QUERY_MEASURE_POS:
            err = hndl->meas_pos(hndl,
                ((hat_cm_lens_query_t*)query_prms)->query.pos_val);
            GOTO_EXIT_IF(0 != err, 1);
            break;
        default:
            mmsdbg(DL_ERROR, "//VIV HAL: No such a lens query!");
            err = -1;
            GOTO_EXIT_IF(0 != err, 1);
    }

    return 0;

EXIT_1:
    mmsdbg(DL_ERROR, "//VIV HAL: Exit Err");
    return -1;
}

/* ========================================================================== */
/**
* hai_cm_lens_close()
*/
/* ========================================================================== */
static int hai_cm_lens_close(hat_cm_lens_handle_t hndl)
{
    hat_cm_lens_prvt_data_t *prvt;

    prvt = (hat_cm_lens_prvt_data_t *)hndl->prvt;

    prvt->prms.socket_hndl = NULL;
    prvt->prms.ph_lens_hndl = NULL;

    prvt->prms.state = LENS_STATE_CLOSED;

    return 0;
}

/* ========================================================================== */
/**
* hai_cm_lens_destroy()
*/
/* ========================================================================== */
static int hai_cm_lens_destroy(hat_cm_lens_handle_t hndl)
{
    hat_cm_lens_prvt_data_t *prvt;

    mmsdbg(DL_MESSAGE, "//VIV HAL: Enter");

    prvt = (hat_cm_lens_prvt_data_t *)hndl->prvt;

    mmsdbg(DL_MESSAGE, "//VIV HAL: %s, ID %d going to destroy at %010p",
        prvt->cr_prms.name, prvt->cr_prms.id, hndl);

    func_thread_destroy(prvt->prms.thread);
    osal_mutex_destroy(prvt->prms.thread_data.curr_pos_lock);
    osal_mutex_destroy(prvt->prms.thread_data.move_list_lock);
    osal_mutex_destroy(prvt->prms.thread_data.abort_lock);
    osal_mutex_destroy(prvt->prms.lens_move_list.lock);

    prvt->cr_prms.id = 0;
    prvt->cr_prms.name= NULL;
    prvt->prms.state = LENS_STATE_UNKNOWN;

    osal_free(hndl->prvt);

    hndl->prvt = NULL;

    mmsdbg(DL_MESSAGE, "//VIV HAL: Exit Ok");

    return 0;
}

/* ========================================================================== */
/**
* hai_cm_lens_create()
*/
/* ========================================================================== */
int hai_cm_lens_create(hat_cm_lens_handle_t hndl,
    hat_cm_lens_cr_prms_t *cr_prms)
{
    hat_cm_lens_prvt_data_t *prvt;

    prvt = osal_malloc(sizeof(hat_cm_lens_prvt_data_t));
    GOTO_EXIT_IF(NULL == prvt, 1);
    memset (prvt, 0, sizeof (hat_cm_lens_prvt_data_t));

    prvt->cr_prms.id = cr_prms->id;
    prvt->cr_prms.name = cr_prms->name;
    prvt->prms.state = LENS_STATE_CREATED;

    prvt->prms.lens_move_list.lock = osal_mutex_create();

    prvt->prms.thread_data.abort_lock     = osal_mutex_create();
    prvt->prms.thread_data.move_list_lock = osal_mutex_create();
    prvt->prms.thread_data.curr_pos_lock  = osal_mutex_create();
    prvt->prms.thread_data.curr_pos       = 0.0;

    prvt->prms.thread = func_thread_create(
        "Lens Thread",
        1 * 1024,
        220,
        &lens_thread_handles,
        &prvt->prms.thread_data
        );
    GOTO_EXIT_IF(NULL == prvt->prms.thread, 2);

    hndl->prvt = (void *)prvt;

    hndl->set_event_cb      = hai_cm_lens_set_event_cb;
    hndl->cm_lens_set_power_mode    = hai_cm_lens_set_power_mode;
    hndl->cm_lens_init      = hai_cm_lens_init;
    hndl->move_to_pos       = hai_cm_lens_move_to_pos;
    hndl->move              = hai_cm_lens_move;
    hndl->meas_pos          = hai_cm_lens_meas_pos;
    hndl->park              = hai_cm_lens_park;
    hndl->cm_lens_abort     = hai_cm_lens_abort;
    hndl->set_shutter       = hai_cm_lens_set_shutter;
    hndl->set_aperture      = hai_cm_lens_set_aperture;
    hndl->set_dens_filter   = hai_cm_lens_set_dens_filter;
    hndl->set_o_stab        = hai_cm_lens_set_o_stab;
    hndl->set_foc_lenght    = hai_cm_lens_foc_lenght;
    hndl->nvm_data_set      = hai_cm_lens_nvm_data_set;
    hndl->tuning_data_set   = hai_cm_lens_tuning_data_set;

    hndl->cm_lens_get_state         = hai_cm_lens_get_state;
    hndl->cm_lens_get_power_mode    = hai_cm_lens_get_power_mode;
    hndl->cm_lens_get_dyn_props     = hai_cm_lens_get_dyn_props;
    hndl->cm_lens_get_features      = hai_cm_lens_get_features;

    hndl->cm_lens_open      = hai_cm_lens_open;
    hndl->cm_lens_config    = hai_cm_lens_config;
    hndl->cm_lens_control   = hai_cm_lens_control;
    hndl->cm_lens_process   = hai_cm_lens_process;
    hndl->cm_lens_query     = hai_cm_lens_query;
    hndl->cm_lens_close     = hai_cm_lens_close;
    hndl->cm_lens_destroy   = hai_cm_lens_destroy;

    mmsdbg(DL_MESSAGE, "//VIV HAL: %s, ID %d created at %010p",
        prvt->cr_prms.name, prvt->cr_prms.id, hndl);
    mmsdbg(DL_MESSAGE, "//VIV HAL: Exit Ok");

    return 0;

EXIT_2:
    osal_mutex_destroy(prvt->prms.lens_move_list.lock);
    osal_mutex_destroy(prvt->prms.thread_data.abort_lock);
    osal_mutex_destroy(prvt->prms.thread_data.move_list_lock);
    osal_mutex_destroy(prvt->prms.thread_data.curr_pos_lock);
EXIT_1:
    mmsdbg(DL_ERROR, "//VIV HAL: Exit Err");
    return -1;
}
