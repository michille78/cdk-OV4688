/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file hal_cm_socket.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <hal/hal_camera_module/hat_cm_socket.h>
#include <hal/hal_camera_module/hat_cm_socket_prvt.h>

#include <osal/osal_stdlib.h>

#include <error_handle/include/error_handle.h>

#include <utils/mms_debug.h>

mmsdbg_define_variable(
        hal_cm_socket,
        DL_DEFAULT,
        0,
        "hal_cm_socket",
        "HAL CM socket"
    );
#define MMSDEBUGLEVEL mmsdbg_use_variable(hal_cm_socket)

int execute_cmd_entry(hat_cm_socket_handle_t socket_hndl,
    hat_cm_socket_command_entry_t* cmd_entry,
    hat_cm_socket_cmd_entry_result_t* res)
{
    int err = 0;
    uint32 read_val = 0;

    res->read_val = 0;
    res->match = 0;

    switch (cmd_entry->type) {
        case COMMAND_ENTRY_TYPE_DUMMY:
            mmsdbg(DL_MESSAGE, "//VIV HAL: Execute DUMMY cmd_entry");
            GOTO_EXIT_IF(0 != err, 1);
            break;
        case COMMAND_ENTRY_TYPE_DELAY:
            err = osal_usleep(cmd_entry->desc.delay);
            mmsdbg(DL_MESSAGE, "//VIV HAL: Delay %dus", cmd_entry->desc.delay);
            GOTO_EXIT_IF(0 != err, 1);
            break;

        case COMMAND_ENTRY_TYPE_INTENT:
            switch (cmd_entry->desc.intent.type) {
                case RSRC_INTENT_TYPE_ACTION:
                    err = socket_hndl->hal_socket_rsrc_set(socket_hndl,
                        &(cmd_entry->desc.intent.desc.action));
                    GOTO_EXIT_IF(0 != err, 1);
                    break;

                case RSRC_INTENT_TYPE_COMMAND:
                    switch (cmd_entry->desc.intent.desc.command.dir) {
                        case CMD_DIR_WR:
                            err = socket_hndl->hal_socket_write(socket_hndl,
                                &(cmd_entry->desc.intent.desc.command));
                            GOTO_EXIT_IF(0 != err, 1);
                            break;
                        case CMD_DIR_RD:
                            err = socket_hndl->hal_socket_read(socket_hndl,
                                &(cmd_entry->desc.intent.desc.command));
                            GOTO_EXIT_IF(0 != err, 1);
                            res->read_val = read_val;
                            break;
                        default:
                            err = -1;
                            GOTO_EXIT_IF(0 != err, 1);
                    }
                    break;

                case RSRC_INTENT_TYPE_CHECK: {
                    hat_cm_socket_rsrc_command_descr_t cmd_descr;
                    uint8 val[4];
                    int i;

                    if (ARRAY_SIZE(val) < cmd_entry->desc.intent.desc.check.read_cmd.cmd.i2c_cmd.num) {
                        mmsdbg(DL_ERROR, "Max len to check is %d!", ARRAY_SIZE(val));
                        res->match = 0;
                        break;
                    }

                    cmd_descr.comp = cmd_entry->desc.intent.desc.check.read_cmd.comp;
                    cmd_descr.rsrc_type = cmd_entry->desc.intent.desc.check.read_cmd.rsrc_type;
                    cmd_descr.hw_type = cmd_entry->desc.intent.desc.check.read_cmd.hw_type;
                    cmd_descr.dir = cmd_entry->desc.intent.desc.check.read_cmd.dir;
                    cmd_descr.cmd.i2c_cmd.reg = cmd_entry->desc.intent.desc.check.read_cmd.cmd.i2c_cmd.reg;
                    cmd_descr.cmd.i2c_cmd.num = cmd_entry->desc.intent.desc.check.read_cmd.cmd.i2c_cmd.num;
                    cmd_descr.cmd.i2c_cmd.val = val;

                    err = socket_hndl->hal_socket_read(socket_hndl, &(cmd_descr));
                    GOTO_EXIT_IF(0 != err, 1);

                    res->match = 1;
                    for (i = 0; i < cmd_descr.cmd.i2c_cmd.num; i++) {
                        if (cmd_entry->desc.intent.desc.check.expected_val[i] != cmd_descr.cmd.i2c_cmd.val[i]) {
                            res->match = 0;
                            break;
                        }
                    }

                    }
                    break;
                case RSRC_INTENT_TYPE_CONFIG:
                {
                    hat_cm_socket_rsrc_config_descr_t *sock_cfg;
                    sock_cfg = &cmd_entry->desc.intent.desc.config;
//                    printf("\nIntent config Addr: %x reg_size %d Addr_bit_size %d\n",
//                            (unsigned int)sock_cfg->config.i2c_cfg.address,
//                            (int)sock_cfg->config.i2c_cfg.register_size,
//                            (int)sock_cfg->config.i2c_cfg.slave_addr_bit_size
//                            );
                    socket_hndl->hal_socket_rsrc_cfg(socket_hndl, sock_cfg);
                }
                break;
                default:
                    err = -1;
                    GOTO_EXIT_IF(0 != err, 1);
            }
            break;

        case COMMAND_ENTRY_TYPE_SMART_FUNC:
            err = cmd_entry->desc.smart_func.func(
                cmd_entry->desc.smart_func.in,
                cmd_entry->desc.smart_func.out);
            GOTO_EXIT_IF(0 != err, 1);
            break;

        case COMMAND_ENTRY_TYPE_END:
            // Do nothing
            break;

        default:
            mmsdbg(DL_ERROR, "//VIV HAL: Error - try to execute %d cmd_entry", cmd_entry->type);
            err = -1;
            GOTO_EXIT_IF(0 != err, 1);
    }

    return 0;

EXIT_1:
    mmsdbg(DL_ERROR, "//VIV HAL: Exit Err");
    return -1;
}

int execute_cmd_strip(hat_cm_socket_handle_t socket_hndl,
    const hat_cm_socket_command_entry_t* cmd_strip,
    hat_cm_socket_cmd_strip_result_t* res)
{
    int err = 0;
    int i = 0;
    hat_cm_socket_cmd_entry_result_t cmd_res;
    hat_cm_socket_command_entry_t*   strip;
    int det_res = 0;

    strip = (hat_cm_socket_command_entry_t*)cmd_strip;

    while (COMMAND_ENTRY_TYPE_END != strip[i].type) {
        cmd_res.read_val = 0;
        cmd_res.match = 0;
        err = execute_cmd_entry(socket_hndl, &strip[i], &cmd_res);
        GOTO_EXIT_IF(0 != err, 1);

        // If one of the check conditions in the script is matched -
        // CM check is successful
        det_res = det_res || cmd_res.match;

        i++;
    }

    if (det_res) {
        res->detected = 1;
    } else {
        res->detected = 0;
    }

    return 0;

EXIT_1:
    mmsdbg(DL_ERROR, "//VIV HAL: Exit Err");
    return -1;
}

static int get_hw_hndl_from_phys_socket(hat_cm_socket_handle_t hndl,
    plat_socket_rsrc_selector_t *sel,
    PLAT_SOCKET_HW_RSRC_TYPE_T *hw_type,
    void **hw_hndl)
{
    int err = 0;
    hat_cm_socket_prvt_data_t *prvt;
    prvt = (hat_cm_socket_prvt_data_t *)hndl->hal_socket_prvt;

    /* Selector fully describes a physical socket resource to select */
    sel->ph_socket = prvt->cr_prms.id;

    err = plat_socket_hw_hndl_get(sel, (int *)hw_type, hw_hndl);
    GOTO_EXIT_IF(0 != err, 1);

    mmsdbg(DL_MESSAGE, "//VIV HAL: Socket name: %s\n"
            "VIRT_SOCKET: %d\n"
            "PH_SOCKET: %d\n"
            "SOCK_COMP: %d\n"
            "COMP_RSRC: %d\n"
            "hw_type: %d\n"
            "hw_hndl: %010p",
            prvt->cr_prms.name, prvt->cr_prms.id,
            sel->ph_socket, sel->sock_comp, sel->comp_rsrc,
            *hw_type,
            *hw_hndl);

    return 0;

EXIT_1:
    mmsdbg(DL_ERROR, "//VIV HAL: Exit Err");
    return -1;
}



static int get_hw_resources(hat_cm_socket_handle_t hndl,
    plat_socket_rsrc_selector_t *sel,
    hat_cm_socket_hw_rsrc_t *hw_rsrc)
{
    int                          err = 0;
    PLAT_SOCKET_HW_RSRC_TYPE_T   hw_type;
    void                        *hw_hndl;

    err = get_hw_hndl_from_phys_socket(hndl, sel,
        &hw_type, &hw_hndl);
    GOTO_EXIT_IF(0 != err, 1);

    switch (hw_type) {
        case HW_RSRC_DUMMY:
            hw_rsrc->hw_type = HW_RSRC_DUMMY;
            hw_rsrc->hw_desc.dummy_hndl =
                (hat_cm_socket_hw_rsrc_dummy_hndl_t)hw_hndl;
            break;
        case HW_RSRC_POWER:
            hw_rsrc->hw_type = HW_RSRC_POWER;
            hw_rsrc->hw_desc.power_hndl =
                (hat_cm_socket_hw_rsrc_power_hndl_t)hw_hndl;
            break;
        case HW_RSRC_GPIO:
            hw_rsrc->hw_type = HW_RSRC_GPIO;
            hw_rsrc->hw_desc.gpio_hndl =
                (hat_cm_socket_hw_rsrc_gpio_hndl_t)hw_hndl;
            break;
        case HW_RSRC_I2C:
            hw_rsrc->hw_type = HW_RSRC_I2C;
            hw_rsrc->hw_desc.i2c_hndl =
                (hat_cm_socket_hw_rsrc_i2c_hndl_t)hw_hndl;
            break;
        case HW_RSRC_CLOCK:
            hw_rsrc->hw_type = HW_RSRC_CLOCK;
            hw_rsrc->hw_desc.clock_hndl =
                (hat_cm_socket_hw_rsrc_clock_hndl_t)hw_hndl;
            break;
        case HW_RSRC_CSI2:
            hw_rsrc->hw_type = HW_RSRC_CSI2;
            hw_rsrc->hw_desc.csi2_hndl =
                (hat_cm_socket_hw_rsrc_csi2_hndl_t)hw_hndl;
            break;
        default:
            err = -1;
            GOTO_EXIT_IF(0 != err, 1);
    }

    return 0;

EXIT_1:
    mmsdbg(DL_ERROR, "//VIV HAL: Exit Err");
    return -1;
}



static int get_dummy_resources(hat_cm_socket_handle_t hndl,
    plat_socket_rsrc_selector_t *sel,
    hat_cm_socket_dummy_rsrc_t *dummy_rsrc)
{
    int err = 0;

    switch (dummy_rsrc->type) {
        case DUMM_RSRC_DUMMY:
            sel->comp_rsrc = DUMM_RSRC_DUMMY;
            err = get_hw_resources(hndl, sel,
                &dummy_rsrc->rsrc.dummy_hw_rsrc);
            GOTO_EXIT_IF(0 != err, 1);
            break;
        case DUMM_RSRC_POWER:
            sel->comp_rsrc = DUMM_RSRC_POWER;
            err = get_hw_resources(hndl, sel,
                &dummy_rsrc->rsrc.power_hw_rsrc);
            GOTO_EXIT_IF(0 != err, 1);
            break;
        default:
            err = -1;
            GOTO_EXIT_IF(0 != err, 1);
    }

    return 0;

EXIT_1:
    mmsdbg(DL_ERROR, "//VIV HAL: Exit Err");
    return -1;
}

static int get_sensor_resources(hat_cm_socket_handle_t hndl,
    plat_socket_rsrc_selector_t *sel,
    hat_cm_socket_sensor_rsrc_t *sen_rsrc)
{
    int err = 0;

    switch (sen_rsrc->type) {
        case SEN_RSRC_DUMMY:
            sel->comp_rsrc = SEN_RSRC_DUMMY;
            err = get_hw_resources(hndl, sel,
                &sen_rsrc->rsrc.dummy_hw_rsrc);
            GOTO_EXIT_IF(0 != err, 1);
            break;
        case SEN_RSRC_POWER_1:
            sel->comp_rsrc = SEN_RSRC_POWER_1;
            err = get_hw_resources(hndl, sel,
                &sen_rsrc->rsrc.power1_hw_rsrc);
            GOTO_EXIT_IF(0 != err, 1);
            break;
        case SEN_RSRC_POWER_2:
            sel->comp_rsrc = SEN_RSRC_POWER_2;
            err = get_hw_resources(hndl, sel,
                &sen_rsrc->rsrc.power2_hw_rsrc);
            GOTO_EXIT_IF(0 != err, 1);
            break;
        case SEN_RSRC_POWER_3:
            sel->comp_rsrc = SEN_RSRC_POWER_3;
            err = get_hw_resources(hndl, sel,
                &sen_rsrc->rsrc.power3_hw_rsrc);
            GOTO_EXIT_IF(0 != err, 1);
            break;
        case SEN_RSRC_CLOCK:
            sel->comp_rsrc = SEN_RSRC_CLOCK;
            err = get_hw_resources(hndl, sel,
                &sen_rsrc->rsrc.clock_hw_rsrc);
            GOTO_EXIT_IF(0 != err, 1);
            break;
        case SEN_RSRC_ENABLE:
            sel->comp_rsrc = SEN_RSRC_ENABLE;
            err = get_hw_resources(hndl, sel,
                &sen_rsrc->rsrc.enable_hw_rsrc);
            GOTO_EXIT_IF(0 != err, 1);
            break;
        case SEN_RSRC_RESET:
            sel->comp_rsrc = SEN_RSRC_RESET;
            err = get_hw_resources(hndl, sel,
                &sen_rsrc->rsrc.reset_hw_rsrc);
            GOTO_EXIT_IF(0 != err, 1);
            break;
        case SEN_RSRC_XSHTDWN:
            sel->comp_rsrc = SEN_RSRC_XSHTDWN;
            err = get_hw_resources(hndl, sel,
                &sen_rsrc->rsrc.xshtdwn_hw_rsrc);
            GOTO_EXIT_IF(0 != err, 1);
            break;
        case SEN_RSRC_SYNC:
            sel->comp_rsrc = SEN_RSRC_SYNC;
            err = get_hw_resources(hndl, sel,
                &sen_rsrc->rsrc.sync_hw_rsrc);
            GOTO_EXIT_IF(0 != err, 1);
            break;
        case SEN_RSRC_CCI:
            sel->comp_rsrc = SEN_RSRC_CCI;
            err = get_hw_resources(hndl, sel,
                &sen_rsrc->rsrc.cci_hw_rsrc);
            GOTO_EXIT_IF(0 != err, 1);
            break;
        case SEN_RSRC_IMGIF:
            sel->comp_rsrc = SEN_RSRC_IMGIF;
            err = get_hw_resources(hndl, sel,
                &sen_rsrc->rsrc.imgif_hw_rsrc);
            GOTO_EXIT_IF(0 != err, 1);
            break;
        default:
            err = -1;
            GOTO_EXIT_IF(0 != err, 1);
    }

    return 0;

EXIT_1:
    mmsdbg(DL_ERROR, "//VIV HAL: Exit Err");
    return -1;
}

static int get_lens_resources(hat_cm_socket_handle_t hndl,
    plat_socket_rsrc_selector_t *sel,
    hat_cm_socket_lens_rsrc_t *lens_rsrc)
{
    int err = 0;

    switch (lens_rsrc->type) {
        case LENS_RSRC_DUMMY:
            sel->comp_rsrc = LENS_RSRC_DUMMY;
            err = get_hw_resources(hndl, sel,
                &lens_rsrc->rsrc.dummy_hw_rsrc);
            GOTO_EXIT_IF(0 != err, 1);
            break;
        case LENS_RSRC_POWER:
            sel->comp_rsrc = LENS_RSRC_POWER;
            err = get_hw_resources(hndl, sel,
                &lens_rsrc->rsrc.power_hw_rsrc);
            GOTO_EXIT_IF(0 != err, 1);
            break;
        case LENS_RSRC_CLOCK:
            sel->comp_rsrc = LENS_RSRC_CLOCK;
            err = get_hw_resources(hndl, sel,
                &lens_rsrc->rsrc.clock_hw_rsrc);
            GOTO_EXIT_IF(0 != err, 1);
            break;
        case LENS_RSRC_ENABLE:
            sel->comp_rsrc = LENS_RSRC_ENABLE;
            err = get_hw_resources(hndl, sel,
                &lens_rsrc->rsrc.enable_hw_rsrc);
            GOTO_EXIT_IF(0 != err, 1);
            break;
        case LENS_RSRC_CCI:
            sel->comp_rsrc = LENS_RSRC_CCI;
            err = get_hw_resources(hndl, sel,
                &lens_rsrc->rsrc.cci_hw_rsrc);
            GOTO_EXIT_IF(0 != err, 1);
            break;
        default:
            err = -1;
            GOTO_EXIT_IF(0 != err, 1);
    }

    return 0;

EXIT_1:
    mmsdbg(DL_ERROR, "//VIV HAL: Exit Err");
    return -1;
}

static int get_flash_resources(hat_cm_socket_handle_t hndl,
    plat_socket_rsrc_selector_t *sel,
    hat_cm_socket_flash_rsrc_t *flash_rsrc)
{
    int err = 0;

    switch (flash_rsrc->type) {
        case FLASH_RSRC_DUMMY:
            sel->comp_rsrc = FLASH_RSRC_DUMMY;
            err = get_hw_resources(hndl, sel,
                &flash_rsrc->rsrc.dummy_hw_rsrc);
            GOTO_EXIT_IF(0 != err, 1);
            break;
        case FLASH_RSRC_POWER:
            sel->comp_rsrc = FLASH_RSRC_POWER;
            err = get_hw_resources(hndl, sel,
                &flash_rsrc->rsrc.power_hw_rsrc);
            GOTO_EXIT_IF(0 != err, 1);
            break;
        case FLASH_RSRC_CLOCK:
            sel->comp_rsrc = FLASH_RSRC_CLOCK;
            err = get_hw_resources(hndl, sel,
                &flash_rsrc->rsrc.clock_hw_rsrc);
            GOTO_EXIT_IF(0 != err, 1);
            break;
        case FLASH_RSRC_TORCH:
            sel->comp_rsrc = FLASH_RSRC_TORCH;
            err = get_hw_resources(hndl, sel,
                &flash_rsrc->rsrc.strobe_hw_rsrc);
            GOTO_EXIT_IF(0 != err, 1);
            break;
        case FLASH_RSRC_STROBE:
            sel->comp_rsrc = FLASH_RSRC_STROBE;
            err = get_hw_resources(hndl, sel,
                &flash_rsrc->rsrc.strobe_hw_rsrc);
            GOTO_EXIT_IF(0 != err, 1);
            break;
        case FLASH_RSRC_CCI:
            sel->comp_rsrc = FLASH_RSRC_CCI;
            err = get_hw_resources(hndl, sel,
                &flash_rsrc->rsrc.cci_hw_rsrc);
            GOTO_EXIT_IF(0 != err, 1);
            break;
        default:
            err = -1;
            GOTO_EXIT_IF(0 != err, 1);
    }

    return 0;

EXIT_1:
    mmsdbg(DL_ERROR, "//VIV HAL: Exit Err");
    return -1;
}

static int get_nvm_resources(hat_cm_socket_handle_t hndl,
    plat_socket_rsrc_selector_t *sel,
    hat_cm_socket_nvm_rsrc_t *nvm_rsrc)
{
    int err = 0;

    switch (nvm_rsrc->type) {
        case NVM_RSRC_DUMMY:
            sel->comp_rsrc = NVM_RSRC_DUMMY;
            err = get_hw_resources(hndl, sel,
                &nvm_rsrc->rsrc.dummy_hw_rsrc);
            GOTO_EXIT_IF(0 != err, 1);
            break;
        case NVM_RSRC_POWER:
            sel->comp_rsrc = NVM_RSRC_POWER;
            err = get_hw_resources(hndl, sel,
                &nvm_rsrc->rsrc.power_hw_rsrc);
            GOTO_EXIT_IF(0 != err, 1);
            break;
        case NVM_RSRC_CLOCK:
            sel->comp_rsrc = NVM_RSRC_CLOCK;
            err = get_hw_resources(hndl, sel,
                &nvm_rsrc->rsrc.clock_hw_rsrc);
            GOTO_EXIT_IF(0 != err, 1);
            break;
        case NVM_RSRC_ENABLE:
            sel->comp_rsrc = NVM_RSRC_ENABLE;
            err = get_hw_resources(hndl, sel,
                &nvm_rsrc->rsrc.enable_hw_rsrc);
            GOTO_EXIT_IF(0 != err, 1);
            break;
        case NVM_RSRC_CCI:
            sel->comp_rsrc = NVM_RSRC_CCI;
            err = get_hw_resources(hndl, sel,
                &nvm_rsrc->rsrc.cci_hw_rsrc);
            GOTO_EXIT_IF(0 != err, 1);
            break;
        default:
            err = -1;
            GOTO_EXIT_IF(0 != err, 1);
    }

    return 0;

EXIT_1:
    mmsdbg(DL_ERROR, "//VIV HAL: Exit Err");
    return -1;
}



static int get_component_resources(hat_cm_socket_handle_t hndl,
    int rsrc_begin, int rsrc_end,
    hat_cm_socket_rsrc_t *comp_rsrc)
{
    int err = 0;
    plat_socket_rsrc_selector_t sel;
    int rsrc_cnt;

    switch (comp_rsrc->comp) {
        case SOCKET_COMP_DUMMY:
            sel.sock_comp = SOCKET_COMP_DUMMY;
            for (rsrc_cnt = rsrc_begin; rsrc_cnt < rsrc_end; rsrc_cnt++) {
                comp_rsrc->rsrc.dummy_resources[rsrc_cnt].type = rsrc_cnt;
                err = get_dummy_resources(hndl, &sel,
                    &(comp_rsrc->rsrc.dummy_resources[rsrc_cnt]));
                GOTO_EXIT_IF(0 != err, 1);
            }
            break;
        case SOCKET_COMP_SENSOR:
            sel.sock_comp = SOCKET_COMP_SENSOR;
            for (rsrc_cnt = rsrc_begin; rsrc_cnt < rsrc_end; rsrc_cnt++) {
                comp_rsrc->rsrc.sensor_resources[rsrc_cnt].type = rsrc_cnt;
                err = get_sensor_resources(hndl, &sel,
                    &(comp_rsrc->rsrc.sensor_resources[rsrc_cnt]));
                GOTO_EXIT_IF(0 != err, 1);
            }
            break;
        case SOCKET_COMP_LENS:
            sel.sock_comp = SOCKET_COMP_LENS;
            for (rsrc_cnt = rsrc_begin; rsrc_cnt < rsrc_end; rsrc_cnt++) {
                comp_rsrc->rsrc.lens_resources[rsrc_cnt].type = rsrc_cnt;
                err = get_lens_resources(hndl, &sel,
                    &(comp_rsrc->rsrc.lens_resources[rsrc_cnt]));
                GOTO_EXIT_IF(0 != err, 1);
            }
            break;
        case SOCKET_COMP_FLASH:
            sel.sock_comp = SOCKET_COMP_FLASH;
            for (rsrc_cnt = rsrc_begin; rsrc_cnt < rsrc_end; rsrc_cnt++) {
                comp_rsrc->rsrc.flash_resources[rsrc_cnt].type = rsrc_cnt;
                err = get_flash_resources(hndl, &sel,
                    &(comp_rsrc->rsrc.flash_resources[rsrc_cnt]));
                GOTO_EXIT_IF(0 != err, 1);
            }
            break;
        case SOCKET_COMP_NVM:
            sel.sock_comp = SOCKET_COMP_NVM;
            for (rsrc_cnt = rsrc_begin; rsrc_cnt < rsrc_end; rsrc_cnt++) {
                comp_rsrc->rsrc.nvm_resources[rsrc_cnt].type = rsrc_cnt;
                err = get_nvm_resources(hndl, &sel,
                    &(comp_rsrc->rsrc.nvm_resources[rsrc_cnt]));
                GOTO_EXIT_IF(0 != err, 1);
            }
            break;
        default:
            err = -1;
            GOTO_EXIT_IF(0 != err, 1);
    }

    return 0;

EXIT_1:
    mmsdbg(DL_ERROR, "//VIV HAL: Exit Err");
    return -1;
}




/* ========================================================================== */
/**
* hai_cm_socket_get_state()
*/
/* ========================================================================== */
static int hai_cm_socket_get_state(hat_cm_socket_handle_t hndl,
    HAT_CM_SOCKET_STATE_T *state)
{
    hat_cm_socket_prvt_data_t *prvt;

    prvt = (hat_cm_socket_prvt_data_t *)hndl->hal_socket_prvt;

    mmsdbg(DL_MESSAGE, "//VIV HAL: Socket state: %d", prvt->prms.state);

    *state = prvt->prms.state;

    return 0;
}

/* ========================================================================== */
/**
* _cm_socket_create()
*/
/* ========================================================================== */
static int _cm_socket_create(hat_cm_socket_handle_t hndl)
{
    int err = 0;
    hat_cm_socket_prvt_data_t *prvt;
    hat_cm_socket_prms_t      *socket_prms;
    int i;
    hat_cm_socket_rsrc_t      *comp_rsrc;

    prvt = (hat_cm_socket_prvt_data_t *)hndl->hal_socket_prvt;
    socket_prms = (hat_cm_socket_prms_t *)&(prvt->prms);

    /* Fill every CM socket component */
    for (i = SOCKET_COMP_DUMMY; i < SOCKET_COMP_MAX; i++) {
        /* Assign an ID for every CM socket component */
        socket_prms->socket_resources[i].comp = i;
        comp_rsrc = &(socket_prms->socket_resources[i]);
        switch (i) {
            case SOCKET_COMP_DUMMY:
                err = get_component_resources(hndl,
                    DUMM_RSRC_DUMMY, DUMM_RSRC_MAX,
                    comp_rsrc);
                GOTO_EXIT_IF(0 != err, 1);
                break;
            case SOCKET_COMP_SENSOR:
                err = get_component_resources(hndl,
                    SEN_RSRC_DUMMY, SEN_RSRC_MAX,
                    comp_rsrc);
                GOTO_EXIT_IF(0 != err, 1);
                break;
            case SOCKET_COMP_LENS:
                err = get_component_resources(hndl,
                    LENS_RSRC_DUMMY, LENS_RSRC_MAX,
                    comp_rsrc);
                GOTO_EXIT_IF(0 != err, 1);
                break;
            case SOCKET_COMP_FLASH:
                err = get_component_resources(hndl,
                    FLASH_RSRC_DUMMY, FLASH_RSRC_MAX,
                    comp_rsrc);
                GOTO_EXIT_IF(0 != err, 1);
                break;
            case SOCKET_COMP_NVM:
                err = get_component_resources(hndl,
                    NVM_RSRC_DUMMY, NVM_RSRC_MAX,
                    comp_rsrc);
                GOTO_EXIT_IF(0 != err, 1);
                break;
            default:
                err = -1;
                GOTO_EXIT_IF(0 != err, 1);
        }
    }

    prvt->prms.state = SOCKET_STATE_OPENED;

    return 0;

EXIT_1:
    mmsdbg(DL_ERROR, "//VIV HAL: Exit Err");
    return -1;
}

/* ========================================================================== */
/**
* get_hw_handle()
*/
/* ========================================================================== */
static int get_hw_handle(hat_cm_socket_handle_t hndl,
    PLAT_SOCKET_COMPONENT_T comp,
    int rsrc_type,
    void **hw_hndl,
    PLAT_SOCKET_HW_RSRC_TYPE_T *hw_res_type)
{
    int err = 0;
    hat_cm_socket_prvt_data_t    *prvt;
    hat_cm_socket_rsrc_t         *socket_comp;
    PLAT_SOCKET_HW_RSRC_TYPE_T    hw_type;
    hat_cm_socket_hw_rsrc_desc_t *hw_desc;
    void *comp_rsrc;

    prvt = (hat_cm_socket_prvt_data_t *)hndl->hal_socket_prvt;
    socket_comp = &prvt->prms.socket_resources[comp];

    switch (comp) {
        case SOCKET_COMP_DUMMY:
            comp_rsrc = (void*)&(socket_comp->rsrc.dummy_resources[rsrc_type].rsrc);
            switch (rsrc_type) {
                case DUMM_RSRC_DUMMY:
                    hw_type = ((hat_cm_socket_dumm_rsrc_desc_t *)comp_rsrc)->dummy_hw_rsrc.hw_type;
                    hw_desc = &(((hat_cm_socket_dumm_rsrc_desc_t *)comp_rsrc)->dummy_hw_rsrc.hw_desc);
                    break;
                case DUMM_RSRC_POWER:
                    hw_type = ((hat_cm_socket_dumm_rsrc_desc_t *)comp_rsrc)->power_hw_rsrc.hw_type;
                    hw_desc = &(((hat_cm_socket_dumm_rsrc_desc_t *)comp_rsrc)->power_hw_rsrc.hw_desc);
                    break;
                case DUMM_RSRC_MAX:
                default:
                    err = -1;
                    mmsdbg(DL_MESSAGE, "//VIV HAL: Error: No such a Dummy component resource %d!", rsrc_type);
                    GOTO_EXIT_IF(0 != err, 1);
            }
            break;

        case SOCKET_COMP_SENSOR:
            comp_rsrc = (void*)&(socket_comp->rsrc.sensor_resources[rsrc_type].rsrc);
            switch (rsrc_type) {
                case SEN_RSRC_DUMMY:
                    hw_type = ((hat_cm_socket_sens_rsrc_desc_t *)comp_rsrc)->dummy_hw_rsrc.hw_type;
                    hw_desc = &(((hat_cm_socket_sens_rsrc_desc_t *)comp_rsrc)->dummy_hw_rsrc.hw_desc);
                    break;
                case SEN_RSRC_POWER_1:
                    hw_type = ((hat_cm_socket_sens_rsrc_desc_t *)comp_rsrc)->power1_hw_rsrc.hw_type;
                    hw_desc = &(((hat_cm_socket_sens_rsrc_desc_t *)comp_rsrc)->power1_hw_rsrc.hw_desc);
                    break;
                case SEN_RSRC_POWER_2:
                    hw_type = ((hat_cm_socket_sens_rsrc_desc_t *)comp_rsrc)->power2_hw_rsrc.hw_type;
                    hw_desc = &(((hat_cm_socket_sens_rsrc_desc_t *)comp_rsrc)->power2_hw_rsrc.hw_desc);
                    break;
                case SEN_RSRC_POWER_3:
                    hw_type = ((hat_cm_socket_sens_rsrc_desc_t *)comp_rsrc)->power3_hw_rsrc.hw_type;
                    hw_desc = &(((hat_cm_socket_sens_rsrc_desc_t *)comp_rsrc)->power3_hw_rsrc.hw_desc);
                    break;
                case SEN_RSRC_CLOCK:
                    hw_type = ((hat_cm_socket_sens_rsrc_desc_t *)comp_rsrc)->clock_hw_rsrc.hw_type;
                    hw_desc = &(((hat_cm_socket_sens_rsrc_desc_t *)comp_rsrc)->clock_hw_rsrc.hw_desc);
                    break;
                case SEN_RSRC_ENABLE:
                    hw_type = ((hat_cm_socket_sens_rsrc_desc_t *)comp_rsrc)->enable_hw_rsrc.hw_type;
                    hw_desc = &(((hat_cm_socket_sens_rsrc_desc_t *)comp_rsrc)->enable_hw_rsrc.hw_desc);
                    break;
                case SEN_RSRC_RESET:
                    hw_type = ((hat_cm_socket_sens_rsrc_desc_t *)comp_rsrc)->reset_hw_rsrc.hw_type;
                    hw_desc = &(((hat_cm_socket_sens_rsrc_desc_t *)comp_rsrc)->reset_hw_rsrc.hw_desc);
                    break;
                case SEN_RSRC_XSHTDWN:
                    hw_type = ((hat_cm_socket_sens_rsrc_desc_t *)comp_rsrc)->xshtdwn_hw_rsrc.hw_type;
                    hw_desc = &(((hat_cm_socket_sens_rsrc_desc_t *)comp_rsrc)->xshtdwn_hw_rsrc.hw_desc);
                    break;
                case SEN_RSRC_SYNC:
                    hw_type = ((hat_cm_socket_sens_rsrc_desc_t *)comp_rsrc)->sync_hw_rsrc.hw_type;
                    hw_desc = &(((hat_cm_socket_sens_rsrc_desc_t *)comp_rsrc)->sync_hw_rsrc.hw_desc);
                    break;
                case SEN_RSRC_CCI:
                    hw_type = ((hat_cm_socket_sens_rsrc_desc_t *)comp_rsrc)->cci_hw_rsrc.hw_type;
                    hw_desc = &(((hat_cm_socket_sens_rsrc_desc_t *)comp_rsrc)->cci_hw_rsrc.hw_desc);
                    break;
                case SEN_RSRC_IMGIF:
                    hw_type = ((hat_cm_socket_sens_rsrc_desc_t *)comp_rsrc)->imgif_hw_rsrc.hw_type;
                    hw_desc = &(((hat_cm_socket_sens_rsrc_desc_t *)comp_rsrc)->imgif_hw_rsrc.hw_desc);
                    break;
                case SEN_RSRC_MAX:
                default:
                    err = -1;
                    mmsdbg(DL_MESSAGE, "//VIV HAL: Error: No such a Sensor component resource %d!", rsrc_type);
                    GOTO_EXIT_IF(0 != err, 1);
            }
            break;

        case SOCKET_COMP_LENS:
            comp_rsrc = (void*)&(socket_comp->rsrc.lens_resources[rsrc_type].rsrc);
            switch (rsrc_type) {
                case LENS_RSRC_DUMMY:
                    hw_type = ((hat_cm_socket_lens_rsrc_desc_t *)comp_rsrc)->dummy_hw_rsrc.hw_type;
                    hw_desc = &(((hat_cm_socket_lens_rsrc_desc_t *)comp_rsrc)->dummy_hw_rsrc.hw_desc);
                    break;
                case LENS_RSRC_POWER:
                    hw_type = ((hat_cm_socket_lens_rsrc_desc_t *)comp_rsrc)->power_hw_rsrc.hw_type;
                    hw_desc = &(((hat_cm_socket_lens_rsrc_desc_t *)comp_rsrc)->power_hw_rsrc.hw_desc);
                    break;
                case LENS_RSRC_CLOCK:
                    hw_type = ((hat_cm_socket_lens_rsrc_desc_t *)comp_rsrc)->clock_hw_rsrc.hw_type;
                    hw_desc = &(((hat_cm_socket_lens_rsrc_desc_t *)comp_rsrc)->clock_hw_rsrc.hw_desc);
                    break;
                case LENS_RSRC_ENABLE:
                    hw_type = ((hat_cm_socket_lens_rsrc_desc_t *)comp_rsrc)->enable_hw_rsrc.hw_type;
                    hw_desc = &(((hat_cm_socket_lens_rsrc_desc_t *)comp_rsrc)->enable_hw_rsrc.hw_desc);
                    break;
                case LENS_RSRC_CCI:
                    hw_type = ((hat_cm_socket_lens_rsrc_desc_t *)comp_rsrc)->cci_hw_rsrc.hw_type;
                    hw_desc = &(((hat_cm_socket_lens_rsrc_desc_t *)comp_rsrc)->cci_hw_rsrc.hw_desc);
                    break;
                case LENS_RSRC_MAX:
                default:
                    err = -1;
                    mmsdbg(DL_MESSAGE, "//VIV HAL: Error: No such a Lens component resource %d!", rsrc_type);
                    GOTO_EXIT_IF(0 != err, 1);
            }
            break;

        case SOCKET_COMP_FLASH:
            comp_rsrc = (void*)&(socket_comp->rsrc.flash_resources[rsrc_type].rsrc);
            switch (rsrc_type) {
                case FLASH_RSRC_DUMMY:
                    hw_type = ((hat_cm_socket_flash_rsrc_desc_t *)comp_rsrc)->dummy_hw_rsrc.hw_type;
                    hw_desc = &(((hat_cm_socket_flash_rsrc_desc_t *)comp_rsrc)->dummy_hw_rsrc.hw_desc);
                    break;
                case FLASH_RSRC_POWER:
                    hw_type = ((hat_cm_socket_flash_rsrc_desc_t *)comp_rsrc)->power_hw_rsrc.hw_type;
                    hw_desc = &(((hat_cm_socket_flash_rsrc_desc_t *)comp_rsrc)->power_hw_rsrc.hw_desc);
                    break;
                case FLASH_RSRC_CLOCK:
                    hw_type = ((hat_cm_socket_flash_rsrc_desc_t *)comp_rsrc)->clock_hw_rsrc.hw_type;
                    hw_desc = &(((hat_cm_socket_flash_rsrc_desc_t *)comp_rsrc)->clock_hw_rsrc.hw_desc);
                    break;
                case FLASH_RSRC_TORCH:
                    hw_type = ((hat_cm_socket_flash_rsrc_desc_t *)comp_rsrc)->strobe_hw_rsrc.hw_type;
                    hw_desc = &(((hat_cm_socket_flash_rsrc_desc_t *)comp_rsrc)->strobe_hw_rsrc.hw_desc);
                    break;
                case FLASH_RSRC_STROBE:
                    hw_type = ((hat_cm_socket_flash_rsrc_desc_t *)comp_rsrc)->torch_hw_rsrc.hw_type;
                    hw_desc = &(((hat_cm_socket_flash_rsrc_desc_t *)comp_rsrc)->torch_hw_rsrc.hw_desc);
                    break;
                case FLASH_RSRC_CCI:
                    hw_type = ((hat_cm_socket_flash_rsrc_desc_t *)comp_rsrc)->cci_hw_rsrc.hw_type;
                    hw_desc = &(((hat_cm_socket_flash_rsrc_desc_t *)comp_rsrc)->cci_hw_rsrc.hw_desc);
                    break;
                case FLASH_RSRC_MAX:
                default:
                    err = -1;
                    mmsdbg(DL_MESSAGE, "//VIV HAL: Error: No such a flash component resource %d!", rsrc_type);
                    GOTO_EXIT_IF(0 != err, 1);
            }
            break;

        case SOCKET_COMP_NVM:
            comp_rsrc = (void*)&(socket_comp->rsrc.nvm_resources[rsrc_type].rsrc);
            switch (rsrc_type) {
                case NVM_RSRC_DUMMY:
                    hw_type = ((hat_cm_socket_nvm_rsrc_desc_t *)comp_rsrc)->dummy_hw_rsrc.hw_type;
                    hw_desc = &(((hat_cm_socket_nvm_rsrc_desc_t *)comp_rsrc)->dummy_hw_rsrc.hw_desc);
                    break;
                case NVM_RSRC_POWER:
                    hw_type = ((hat_cm_socket_nvm_rsrc_desc_t *)comp_rsrc)->power_hw_rsrc.hw_type;
                    hw_desc = &(((hat_cm_socket_nvm_rsrc_desc_t *)comp_rsrc)->power_hw_rsrc.hw_desc);
                    break;
                case NVM_RSRC_CLOCK:
                    hw_type = ((hat_cm_socket_nvm_rsrc_desc_t *)comp_rsrc)->clock_hw_rsrc.hw_type;
                    hw_desc = &(((hat_cm_socket_nvm_rsrc_desc_t *)comp_rsrc)->clock_hw_rsrc.hw_desc);
                    break;
                case NVM_RSRC_ENABLE:
                    hw_type = ((hat_cm_socket_nvm_rsrc_desc_t *)comp_rsrc)->enable_hw_rsrc.hw_type;
                    hw_desc = &(((hat_cm_socket_nvm_rsrc_desc_t *)comp_rsrc)->enable_hw_rsrc.hw_desc);
                    break;
                case NVM_RSRC_CCI:
                    hw_type = ((hat_cm_socket_nvm_rsrc_desc_t *)comp_rsrc)->cci_hw_rsrc.hw_type;
                    hw_desc = &(((hat_cm_socket_nvm_rsrc_desc_t *)comp_rsrc)->cci_hw_rsrc.hw_desc);
                    break;
                case NVM_RSRC_MAX:
                default:
                    err = -1;
                    mmsdbg(DL_MESSAGE, "//VIV HAL: Error: No such a Lens component resource %d!", rsrc_type);
                    GOTO_EXIT_IF(0 != err, 1);
            }
            break;

        case SOCKET_COMP_MAX:
        default:
            err = -1;
            mmsdbg(DL_MESSAGE, "//VIV HAL: Error: No such a CM component %d!", comp);
            GOTO_EXIT_IF(0 != err, 1);
    }

    *hw_res_type = hw_type;

    switch (hw_type) {
        case HW_RSRC_DUMMY:
            *hw_hndl = hw_desc->dummy_hndl;
            break;
        case HW_RSRC_POWER:
            *hw_hndl = hw_desc->power_hndl;
            break;
        case HW_RSRC_GPIO:
            *hw_hndl = hw_desc->gpio_hndl;
            break;
        case HW_RSRC_I2C:
            *hw_hndl = hw_desc->i2c_hndl;
            break;
        case HW_RSRC_CLOCK:
            *hw_hndl = hw_desc->clock_hndl;
            break;
        case HW_RSRC_CSI2:
            *hw_hndl = hw_desc->csi2_hndl;
            break;
        case HW_RSRC_MAX:
        default:
            err = -1;
            mmsdbg(DL_MESSAGE, "//VIV HAL: Error: No such a HW resource %d!", hw_type);
            GOTO_EXIT_IF(0 != err, 1);
    }

    return 0;

EXIT_1:
    mmsdbg(DL_ERROR, "//VIV HAL: Exit Err");
    return -1;
}

/* ========================================================================== */
/**
* hai_cm_socket_query()
*/
/* ========================================================================== */
static int hai_cm_socket_query(hat_cm_socket_handle_t hndl,
        hat_cm_socket_query_params_t query, void* data)
{

    void                            *hw_hndl;
    PLAT_SOCKET_HW_RSRC_TYPE_T       hw_type;

    switch (query){
        case HAT_HW_QUERY_CAMIF_CTX:
            get_hw_handle(hndl, SOCKET_COMP_SENSOR, SEN_RSRC_IMGIF,
                &hw_hndl, &hw_type);
            if (HW_RSRC_CSI2 == hw_type) {
                *(int *)data = ((hat_cm_socket_csi2_hndl_t *)hw_hndl)->ctx_num;
            } else {
                *(int *)data = 0;
                mmsdbg(DL_ERROR, "Missing IMGIF RES.");
            }
            break;

        case HAT_HW_QUERY_SENSOR_CLK:
            get_hw_handle(hndl, SOCKET_COMP_SENSOR, SEN_RSRC_CLOCK,
                &hw_hndl, &hw_type);
            if (HW_RSRC_CLOCK == hw_type) {
                *(int *)data = ((hat_cm_socket_clock_hndl_t *)hw_hndl)->freq;
            } else {
                *(int *)data = 0;
                mmsdbg(DL_ERROR, "Missing CLOCK RES.");
            }
            break;

        default:
            return -1;
            break;
    }

    return 0;
}
/* ========================================================================== */
/**
* hai_cm_socket_open()
*/
/* ========================================================================== */
static int hai_cm_socket_open(hat_cm_socket_handle_t hndl,
        hat_cm_socket_open_params_t *params)
{
    void                            *hw_hndl;
    PLAT_SOCKET_HW_RSRC_TYPE_T       hw_type;

    get_hw_handle(hndl, SOCKET_COMP_SENSOR, SEN_RSRC_CCI, &hw_hndl, &hw_type);

    if (HW_RSRC_I2C == hw_type)
    {
        ((hat_cm_socket_i2c_dev_t *)hw_hndl)->regdev(hw_hndl, &params->sensor);
    }

    get_hw_handle(hndl, SOCKET_COMP_LENS, LENS_RSRC_CCI, &hw_hndl, &hw_type);

    if (HW_RSRC_I2C == hw_type)
    {
        ((hat_cm_socket_i2c_dev_t *)hw_hndl)->regdev(hw_hndl, &params->lens);
    }

    get_hw_handle(hndl, SOCKET_COMP_FLASH, FLASH_RSRC_CCI, &hw_hndl, &hw_type);

    if (HW_RSRC_I2C == hw_type)
    {
        ((hat_cm_socket_i2c_dev_t *)hw_hndl)->regdev(hw_hndl, &params->light);
    }

    get_hw_handle(hndl, SOCKET_COMP_NVM, NVM_RSRC_CCI, &hw_hndl, &hw_type);

    if (HW_RSRC_I2C == hw_type)
    {
        ((hat_cm_socket_i2c_dev_t *)hw_hndl)->regdev(hw_hndl, &params->nvm);
    }
    return 0;

}

static int hal_cm_rsrc_gpio_set(hat_cm_socket_gpio_dev_t *hw_hndl,
    hat_cm_socket_rsrc_action_descr_t *action)
{
    if (action->act == HAT_HW_RES_ON) {
        if (!hw_hndl->cnt) {
            hw_hndl->gpio_set(hw_hndl, action->act);
        }
        hw_hndl->cnt++;
    } else {
        if (!hw_hndl->cnt)
            return 0;
        hw_hndl->cnt--;
        if (!hw_hndl->cnt) {
            hw_hndl->gpio_set(hw_hndl, action->act);
        }
    }
    return 0;
}

static int hal_cm_rsrc_clock_set(hat_cm_socket_clock_hndl_t *hw_hndl,
    hat_cm_socket_rsrc_action_descr_t *action)
{
    if (action->act == HAT_HW_RES_ON) {
        if (!hw_hndl->cnt) {
            hw_hndl->clock_set(hw_hndl, action->act);
        }
        hw_hndl->cnt++;
    } else {
        if (!hw_hndl->cnt)
            return 0;
        hw_hndl->cnt--;
        if (!hw_hndl->cnt) {
            hw_hndl->clock_set(hw_hndl, action->act);
        }
    }
    return 0;
}

static int hal_cm_rsrc_power_set(hat_cm_socket_power_hndl_t *hw_hndl,
    hat_cm_socket_rsrc_action_descr_t *action)
{
    if (action->act == HAT_HW_RES_ON) {
        if (!hw_hndl->cnt) {
            hw_hndl->power_set(hw_hndl, action->act);
        }
        hw_hndl->cnt++;
    } else {
        if (!hw_hndl->cnt)
            return 0;
        hw_hndl->cnt--;
        if (!hw_hndl->cnt) {
            hw_hndl->power_set(hw_hndl, action->act);
        }
    }
    return 0;
}
/* ========================================================================== */
/**
* hai_cm_socket_rsrc_set()
*/
/* ========================================================================== */
static int hai_cm_socket_rsrc_set(hat_cm_socket_handle_t hndl,
    hat_cm_socket_rsrc_action_descr_t *action)
{
    int err = 0;
    hat_cm_socket_prvt_data_t       *prvt;
    hat_cm_socket_rsrc_t            *socket_comp;
    void                            *hw_hndl;
    PLAT_SOCKET_HW_RSRC_TYPE_T       hw_type;

    prvt = (hat_cm_socket_prvt_data_t *)hndl->hal_socket_prvt;
    socket_comp = &prvt->prms.socket_resources[action->comp];

    /* Sanity check */
    if (action->comp != socket_comp->comp) {
        err = -1;
        mmsdbg(DL_ERROR, "//VIV HAL: Error: Requested component %d does NOT match with Socket component %d!", action->comp, socket_comp->comp);
        GOTO_EXIT_IF(0 != err, 1);
    }

    err = get_hw_handle(hndl,
        action->comp, action->rsrc_type,
        &hw_hndl,
        &hw_type);
    GOTO_EXIT_IF(0 != err, 1);

    /* Going to execute HW rsrc_set() */
    switch (hw_type) {
        case HW_RSRC_DUMMY:
            ((hat_cm_socket_hw_rsrc_dummy_hndl_t)hw_hndl)->dummy_set(action->act);
            break;
        case HW_RSRC_POWER:
            hal_cm_rsrc_power_set(hw_hndl, action);
            break;
        case HW_RSRC_GPIO:
            hal_cm_rsrc_gpio_set(hw_hndl, action);
            break;
        case HW_RSRC_I2C:
            mmsdbg(DL_MESSAGE, "//VIV HAL: No rsrc_set() defined for I2C!");
            break;
        case HW_RSRC_CLOCK:
            hal_cm_rsrc_clock_set(hw_hndl, action);
            break;
        case HW_RSRC_CSI2:
            mmsdbg(DL_MESSAGE, "//VIV HAL: No rsrc_set() defined for CSI2!");
            break;
        case HW_RSRC_MAX:
        default:
            err = -1;
            mmsdbg(DL_MESSAGE, "//VIV HAL: Error: No such a HW resource %d!", hw_type);
            GOTO_EXIT_IF(0 != err, 1);
    }

    return 0;

EXIT_1:
    mmsdbg(DL_ERROR, "//VIV HAL: Exit Err");
    return -1;
}

/* ========================================================================== */
/**
* hai_cm_socket_write()
*/
/* ========================================================================== */
static int hai_cm_socket_write(hat_cm_socket_handle_t hndl,
    hat_cm_socket_rsrc_command_descr_t *cmd)
{
    int err = 0;
    hat_cm_socket_prvt_data_t       *prvt;
    hat_cm_socket_rsrc_t            *socket_comp;
    void                            *hw_hndl;
    PLAT_SOCKET_HW_RSRC_TYPE_T       hw_type;

    prvt = (hat_cm_socket_prvt_data_t *)hndl->hal_socket_prvt;
    socket_comp = &prvt->prms.socket_resources[cmd->comp];

    /* Sanity check */
    if (cmd->comp != socket_comp->comp) {
        err = -1;
        mmsdbg(DL_ERROR, "//VIV HAL: Error: Requested component %d does NOT match with Socket component %d!", cmd->comp, cmd->comp);
        GOTO_EXIT_IF(0 != err, 1);
    }

    err = get_hw_handle(hndl,
        cmd->comp, cmd->rsrc_type,
        &hw_hndl,
        &hw_type);
    GOTO_EXIT_IF(0 != err, 1);

    /* Going to execute HW write */
    switch (hw_type) {
        case HW_RSRC_DUMMY:
            mmsdbg(DL_MESSAGE, "//VIV HAL: No write() defined for DUMMY resource!");
            break;
        case HW_RSRC_POWER:
            mmsdbg(DL_MESSAGE, "//VIV HAL: No write() defined for POWER resource!");
            break;
        case HW_RSRC_GPIO:
            mmsdbg(DL_MESSAGE, "//VIV HAL: No write() defined for GPIO resource!");
            break;
        case HW_RSRC_I2C:
            ((hat_cm_socket_i2c_dev_t *)hw_hndl)->write(
                    hw_hndl,
                    cmd->cmd.i2c_cmd.reg,
                    cmd->cmd.i2c_cmd.num,
                    cmd->cmd.i2c_cmd.val
                );
            break;
        case HW_RSRC_CLOCK:
            mmsdbg(DL_MESSAGE, "//VIV HAL: No write() defined for CLOCK resource!");
            break;
        case HW_RSRC_CSI2:
            mmsdbg(DL_MESSAGE, "//VIV HAL: No write() defined for CSI2 resource!");
            break;
        case HW_RSRC_MAX:
        default:
            err = -1;
            mmsdbg(DL_MESSAGE, "//VIV HAL: Error: No such a HW resource %d!", hw_type);
            GOTO_EXIT_IF(0 != err, 1);
    }

    return 0;

EXIT_1:
    mmsdbg(DL_ERROR, "//VIV HAL: Exit Err");
    return -1;
}

/* ========================================================================== */
/**
* hai_cm_socket_read()
*/
/* ========================================================================== */
static int hai_cm_socket_read(hat_cm_socket_handle_t hndl,
    hat_cm_socket_rsrc_command_descr_t *cmd)
{
    int err = 0;
    hat_cm_socket_prvt_data_t       *prvt;
    hat_cm_socket_rsrc_t            *socket_comp;
    void                            *hw_hndl;
    PLAT_SOCKET_HW_RSRC_TYPE_T       hw_type;

    prvt = (hat_cm_socket_prvt_data_t *)hndl->hal_socket_prvt;
    socket_comp = &prvt->prms.socket_resources[cmd->comp];

    /* Sanity check */
    if (cmd->comp != socket_comp->comp) {
        err = -1;
        mmsdbg(DL_ERROR, "//VIV HAL: Error: Requested component %d does NOT match with Socket component %d!", cmd->comp, cmd->comp);
        GOTO_EXIT_IF(0 != err, 1);
    }

    err = get_hw_handle(hndl,
        cmd->comp, cmd->rsrc_type,
        &hw_hndl,
        &hw_type);
    GOTO_EXIT_IF(0 != err, 1);

    /* Going to execute HW read */
    switch (hw_type) {
        case HW_RSRC_DUMMY:
            mmsdbg(DL_MESSAGE, "//VIV HAL: No read() defined for DUMMY resource!");
            break;
        case HW_RSRC_POWER:
            mmsdbg(DL_MESSAGE, "//VIV HAL: No read() defined for POWER resource!");
            break;
        case HW_RSRC_GPIO:
            mmsdbg(DL_MESSAGE, "//VIV HAL: No read() defined for GPIO resource!");
            break;
        case HW_RSRC_I2C:
            err = ((hat_cm_socket_i2c_dev_t *)hw_hndl)->read(
                    hw_hndl,
                    cmd->cmd.i2c_cmd.reg,
                    cmd->cmd.i2c_cmd.num,
                    cmd->cmd.i2c_cmd.val
                );
            break;
        case HW_RSRC_CLOCK:
            mmsdbg(DL_MESSAGE, "//VIV HAL: No read() defined for CLOCK resource!");
            break;
        case HW_RSRC_CSI2:
            mmsdbg(DL_MESSAGE, "//VIV HAL: No read() defined for CSI2 resource!");
            break;
        case HW_RSRC_MAX:
        default:
            err = -1;
            mmsdbg(DL_MESSAGE, "//VIV HAL: Error: No such a HW resource %d!", hw_type);
            GOTO_EXIT_IF(0 != err, 1);
    }

    return 0;

EXIT_1:
    mmsdbg(DL_ERROR, "//VIV HAL: Exit Err");
    return -1;
}


static int hai_cm_socket_rsrc_config(hat_cm_socket_handle_t hndl,
        hat_cm_socket_rsrc_config_descr_t *sock_cfg)
{
    int err = 0;
    hat_cm_socket_prvt_data_t       *hal_socket_prvt;
    hat_cm_socket_rsrc_t            *socket_comp;
    void                            *hw_hndl;
    PLAT_SOCKET_HW_RSRC_TYPE_T       hw_type;

    hal_socket_prvt = (hat_cm_socket_prvt_data_t *)hndl->hal_socket_prvt;
    socket_comp = &hal_socket_prvt->prms.socket_resources[sock_cfg->comp];

    /* Sanity check */
    if (sock_cfg->comp != socket_comp->comp) {
        err = -1;
        mmsdbg(DL_ERROR, "//Requested component %d does NOT match with Socket component %d!", sock_cfg->comp, socket_comp->comp);
        GOTO_EXIT_IF(0 != err, 1);
    }

    err = get_hw_handle(hndl,
                        sock_cfg->comp, sock_cfg->rsrc_type,
                        &hw_hndl,
                        &hw_type);
    GOTO_EXIT_IF(0 != err, 1);

    /* Going to execute HW write */
    switch (hw_type) {
        case HW_RSRC_DUMMY:
            mmsdbg(DL_MESSAGE, "No config() defined for DUMMY resource!");
            break;
        case HW_RSRC_POWER:
            mmsdbg(DL_MESSAGE, "No config() defined for POWER resource!");
            break;
        case HW_RSRC_GPIO:
            mmsdbg(DL_MESSAGE, "No config() defined for GPIO resource!");
            break;
        case HW_RSRC_I2C:
            ((hat_cm_socket_i2c_dev_t *)hw_hndl)->regdev(
                    hw_hndl,
                    &sock_cfg->rsrc_cfg.i2c_cfg
                );
            break;
        case HW_RSRC_CLOCK:
            mmsdbg(DL_MESSAGE, "No config() defined for CLOCK resource!");
            break;
        case HW_RSRC_CSI2:
            mmsdbg(DL_MESSAGE, "No config() defined for CSI2 resource!");
            break;
        case HW_RSRC_MAX:
        default:
            err = -1;
            mmsdbg(DL_MESSAGE, "Error: No such a HW resource %d!", hw_type);
            GOTO_EXIT_IF(0 != err, 1);
    }

    return 0;

EXIT_1:
    mmsdbg(DL_ERROR, "//VIV HAL: Exit Err");
    return -1;
}

/* ========================================================================== */
/**
* hai_cm_socket_cci_event_set()
*/
/* ========================================================================== */
static int hai_cm_socket_cci_event_set(hat_cm_socket_handle_t hndl,
    PLAT_SOCKET_COMPONENT_T comp,
    hat_cm_socket_cci_event_prms_t *cci_event_prms)
{
    return 0;
}

/* ========================================================================== */
/**
* hai_cm_socket_imgif_event_set()
*/
/* ========================================================================== */
static int hai_cm_socket_imgif_event_set(hat_cm_socket_handle_t hndl,
    PLAT_SOCKET_COMPONENT_T comp,
    hat_cm_socket_imgif_event_prms_t *imgif_event_prms)
{
    return 0;
}

/* ========================================================================== */
/**
* hai_cm_socket_imgif_buffer_set()
*/
/* ========================================================================== */
static int hai_cm_socket_imgif_buffer_set(hat_cm_socket_handle_t hndl,
    PLAT_SOCKET_COMPONENT_T comp,
    hat_cm_socket_imgif_buffer_prms_t *imgif_buffer_prms)
{
    return 0;
}

/* ========================================================================== */
/**
* hai_cm_socket_close()
*/
/* ========================================================================== */
static int hai_cm_socket_close(hat_cm_socket_handle_t hndl)
{
    hat_cm_socket_prvt_data_t *prvt;

    mmsdbg(DL_MESSAGE, "//VIV HAL: Enter");

    prvt = (hat_cm_socket_prvt_data_t *)hndl->hal_socket_prvt;

    prvt->prms.state = SOCKET_STATE_CLOSED;

    mmsdbg(DL_MESSAGE, "//VIV HAL: Exit Ok");

    return 0;
}

/* ========================================================================== */
/**
* hai_cm_socket_destroy()
*/
/* ========================================================================== */
static int hai_cm_socket_destroy(hat_cm_socket_handle_t hndl)
{
    hat_cm_socket_prvt_data_t *prvt;

    mmsdbg(DL_MESSAGE, "//VIV HAL: Enter");

    prvt = (hat_cm_socket_prvt_data_t *)hndl->hal_socket_prvt;

    mmsdbg(DL_MESSAGE, "//VIV HAL: %s, ID %d going to destroy at %010p",
        prvt->cr_prms.name, prvt->cr_prms.id, hndl);

    prvt->cr_prms.id = 0;
    prvt->cr_prms.name= NULL;
    prvt->prms.state = SOCKET_STATE_UNKNOWN;

    osal_free(hndl->hal_socket_prvt);

    hndl->hal_socket_prvt = NULL;

    hndl->hal_socket_get_state = NULL;
    hndl->hal_socket_create = NULL;
    hndl->hal_socket_rsrc_set = NULL;
    hndl->hal_socket_write = NULL;
    hndl->hal_socket_read = NULL;
    hndl->hal_socket_cci_event_set = NULL;
    hndl->hal_socket_imgif_event_set = NULL;
    hndl->hal_socket_imgif_buffer_set = NULL;
    hndl->hal_socket_close = NULL;
    hndl->hal_socket_destroy = NULL;

    mmsdbg(DL_MESSAGE, "//VIV HAL: Exit Ok");

    return 0;
}

/* ========================================================================== */
/**
* hai_cm_socket_create()
*/
/* ========================================================================== */
int hai_cm_socket_create(hat_cm_socket_handle_t hndl,
    hat_cm_socket_cr_prms_t *cr_prms)
{
    hat_cm_socket_prvt_data_t *prvt;

    prvt = osal_malloc(sizeof(hat_cm_socket_prvt_data_t));
    GOTO_EXIT_IF(NULL == prvt, 1);

    prvt->cr_prms.id = cr_prms->id;
    prvt->cr_prms.name= cr_prms->name;
    prvt->prms.state = SOCKET_STATE_CREATED;

    hndl->hal_socket_prvt = (void *)prvt;

    hndl->hal_socket_get_state = hai_cm_socket_get_state;

    hndl->hal_socket_create = _cm_socket_create;
    hndl->hal_socket_rsrc_set = hai_cm_socket_rsrc_set;
    hndl->hal_socket_rsrc_cfg = hai_cm_socket_rsrc_config;
    hndl->hal_socket_write = hai_cm_socket_write;
    hndl->hal_socket_read = hai_cm_socket_read;
    hndl->hal_socket_cci_event_set = hai_cm_socket_cci_event_set;
    hndl->hal_socket_imgif_event_set = hai_cm_socket_imgif_event_set;
    hndl->hal_socket_imgif_buffer_set = hai_cm_socket_imgif_buffer_set;
    hndl->hal_socket_close = hai_cm_socket_close;
    hndl->hal_socket_destroy = hai_cm_socket_destroy;
    hndl->hal_socket_open = hai_cm_socket_open;
    hndl->hal_socket_query = hai_cm_socket_query;

    mmsdbg(DL_MESSAGE, "//VIV HAL: %s, ID %d created at %010p",
            prvt->cr_prms.name, prvt->cr_prms.id, hndl);
    mmsdbg(DL_MESSAGE, "//VIV HAL: Exit Ok");

    return 0;

EXIT_1:
    mmsdbg(DL_ERROR, "//VIV HAL: Exit Err");
    return -1;
}
