/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file stm.h
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#ifndef _STM_H
#define _STM_H

typedef struct stm stm_t;

typedef int (*stm_action_func_t)(
            stm_t *stm,
            void *prv,
            unsigned int arg1,
            void *arg2
        );

typedef struct {
    unsigned int new_state;
    unsigned int action;
} stm_state_action_t;

typedef stm_action_func_t stm_actions_t;
typedef stm_state_action_t stm_state_action_table_t;

typedef struct {
    unsigned int max_states_number;
    unsigned int max_events_number;
    unsigned int max_actions_number;
    unsigned int row_size;
    unsigned int frame_size;
    stm_state_action_table_t *table;
} stm_state_action_table_desc_t;

typedef struct {
    unsigned int init_state;
    stm_state_action_table_desc_t *state_action_table_desc;
} stm_descriptor_t;

#define STM_DESC_DECLARE(NAME) \
    extern stm_descriptor_t NAME;

#define STM_DESC_BUILD_BEGIN(NAME, iSTATE, STATES_MAX, EVENTS_MAX, ACTIONS_MAX) \
    stm_state_action_table_t NAME##_StateActionTable[]; \
    stm_state_action_table_desc_t NAME##_StateActionTableDesc = { \
        .max_states_number = STATES_MAX, \
        .max_events_number = EVENTS_MAX, \
        .max_actions_number = ACTIONS_MAX, \
        .row_size = STM_E_ROW_SIZE, \
        .frame_size = (STM_E_ROW_SIZE) * (EVENTS_MAX), \
        .table = NAME##_StateActionTable \
    }; \
    stm_descriptor_t NAME = { \
        .init_state = iSTATE, \
        .state_action_table_desc = &NAME##_StateActionTableDesc \
    }; \
    stm_state_action_table_t NAME##_StateActionTable[] = {

#define STM_DESC_BUILD_END(NAME) \
        {0xFFFFFFFF, 0xFFFFFFFF} \
    };

#define STM_E_ROW_SIZE 4

#define __STM_E1x(S1,A1, STATE_PREFIX, ACTIOIN_PREFIX) \
    {STATE_PREFIX##S1, ACTIOIN_PREFIX##A1}, \
    {0xFFFFFFFF, 0xFFFFFFFF}, \
    {0xFFFFFFFF, 0xFFFFFFFF}, \
    {0xFFFFFFFF, 0xFFFFFFFF},
#define __STM_E2x(S1,A1, S2,A2, STATE_PREFIX, ACTIOIN_PREFIX) \
    {STATE_PREFIX##S1, ACTIOIN_PREFIX##A1}, \
    {STATE_PREFIX##S2, ACTIOIN_PREFIX##A2}, \
    {0xFFFFFFFF, 0xFFFFFFFF}, \
    {0xFFFFFFFF, 0xFFFFFFFF},
#define __STM_E3x(S1,A1, S2,A2, S3,A3, STATE_PREFIX, ACTIOIN_PREFIX) \
    {STATE_PREFIX##S1, ACTIOIN_PREFIX##A1}, \
    {STATE_PREFIX##S2, ACTIOIN_PREFIX##A2}, \
    {STATE_PREFIX##S3, ACTIOIN_PREFIX##A3}, \
    {0xFFFFFFFF, 0xFFFFFFFF},
#define __STM_E4x(S1,A1, S2,A2, S3,A3, S4,A4, STATE_PREFIX, ACTIOIN_PREFIX) \
    {STATE_PREFIX##S1, ACTIOIN_PREFIX##A1}, \
    {STATE_PREFIX##S2, ACTIOIN_PREFIX##A2}, \
    {STATE_PREFIX##S3, ACTIOIN_PREFIX##A3}, \
    {STATE_PREFIX##S4, ACTIOIN_PREFIX##A4},

#define _STM_E1x(S1,A1, STATE_PREFIX, ACTIOIN_PREFIX) \
    __STM_E1x(S1,A1, STATE_PREFIX, ACTIOIN_PREFIX)
#define _STM_E2x(S1,A1, S2,A2, STATE_PREFIX, ACTIOIN_PREFIX) \
    __STM_E2x(S1,A1, S2,A2, STATE_PREFIX, ACTIOIN_PREFIX)
#define _STM_E3x(S1,A1, S2,A2, S3,A3, STATE_PREFIX, ACTIOIN_PREFIX) \
    __STM_E3x(S1,A1, S2,A2, S3,A3, STATE_PREFIX, ACTIOIN_PREFIX)
#define _STM_E4x(S1,A1, S2,A2, S3,A3, S4,A4, STATE_PREFIX, ACTIOIN_PREFIX) \
    __STM_E4x(S1,A1, S2,A2, S3,A3, S4,A4, STATE_PREFIX, ACTIOIN_PREFIX)

#define STM_E1x(S1,A1) \
    _STM_E1x(S1,A1, STM_STATE_PREFIX, STM_ACTIOIN_PREFIX)
#define STM_E2x(S1,A1, S2,A2) \
    _STM_E2x(S1,A1, S2,A2, STM_STATE_PREFIX, STM_ACTIOIN_PREFIX)
#define STM_E3x(S1,A1, S2,A2, S3,A3) \
    _STM_E3x(S1,A1, S2,A2, S3,A3, STM_STATE_PREFIX, STM_ACTIOIN_PREFIX)
#define STM_E4x(S1,A1, S2,A2, S3,A3, S4,A4) \
    _STM_E4x(S1,A1, S2,A2, S3,A3, S4,A4, STM_STATE_PREFIX, STM_ACTIOIN_PREFIX)

#define STM_E1(S1,A1) \
    {S1,A1}, \
    {0xFFFFFFFF, 0xFFFFFFFF}, \
    {0xFFFFFFFF, 0xFFFFFFFF}, \
    {0xFFFFFFFF, 0xFFFFFFFF},
#define STM_E2(S1,A1, S2,A2) \
    {S1,A1}, \
    {S2,A2}, \
    {0xFFFFFFFF, 0xFFFFFFFF}, \
    {0xFFFFFFFF, 0xFFFFFFFF},
#define STM_E3(S1,A1, S2,A2, S3,A3) \
    {S1,A1}, \
    {S2,A2}, \
    {S3,A3}, \
    {0xFFFFFFFF, 0xFFFFFFFF},
#define STM_E4(S1,A1, S2,A2, S3,A3, S4,A4) \
    {S1,A1}, {S2,A2}, {S3,A3}, {S4,A4},

void stm_lock(stm_t *stm);
void stm_unlock(stm_t *stm);

int stm_event_atomic_state(
            stm_t *stm,
            unsigned int event,
            void *prv,
            unsigned int arg1,
            void *arg2
        );
int stm_event_atomic(
            stm_t *stm,
            unsigned int event,
            void *prv,
            unsigned int arg1,
            void *arg2
        );
int stm_event(
            stm_t *stm,
            unsigned int event,
            void *prv,
            unsigned int arg1,
            void *arg2
        );
stm_t * stm_create(
            char *name,
            stm_descriptor_t *stm_desc,
            stm_actions_t *actions
        );
void stm_destroy(
            stm_t *stm
        );

#endif /* _STM_H */

