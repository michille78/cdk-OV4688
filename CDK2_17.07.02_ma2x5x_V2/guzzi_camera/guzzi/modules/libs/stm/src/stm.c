/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file stm.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <osal/osal_stdlib.h>
#include <osal/osal_string.h>
#include <osal/osal_mutex.h>
#include <utils/mms_debug.h>
#include <stm.h>

#define STM_NAME_MAX 256

#define STM_TBL_EVENT_IS_VALID(TBL, EVENT) \
    ((EVENT) < (TBL)->max_events_number)

#define STM_TBL_GET_ELEM(TBL, STATE, EVENT) \
    ( \
          (TBL)->table \
        + ((STATE) / (TBL)->row_size) * (TBL)->frame_size \
        + (EVENT) * (TBL)->row_size \
        + (STATE) % (TBL)->row_size \
    )

struct stm {
    char name[STM_NAME_MAX];
    unsigned int last_state;
    unsigned int current_state;
    osal_mutex *lock;
    stm_actions_t *actions;
    stm_state_action_table_desc_t *state_action_table_desc;
};

mmsdbg_define_variable(
        vdl_stm,
        DL_DEFAULT,
        0,
        "vdl_stm",
        "STate Machine module."
    );
#define MMSDEBUGLEVEL mmsdbg_use_variable(vdl_stm)

void stm_lock(stm_t *stm)
{
    osal_mutex_lock(stm->lock);
}

void stm_unlock(stm_t *stm)
{
    osal_mutex_unlock(stm->lock);
}

int stm_event_atomic_state(
            stm_t *stm,
            unsigned int event,
            void *prv,
            unsigned int arg1,
            void *arg2
        )
{
    stm_state_action_t *state_action;

    if (!STM_TBL_EVENT_IS_VALID(stm->state_action_table_desc, event)) {
        mmsdbg(DL_ERROR, "Invalid STM event (in \"%s\" STM)!", stm->name);
        return -1;
    }

    osal_mutex_lock(stm->lock);
    state_action = STM_TBL_GET_ELEM(
                    stm->state_action_table_desc,
                    stm->current_state,
                    event
                );
    stm->last_state= stm->current_state;
    stm->current_state = state_action->new_state;
    osal_mutex_unlock(stm->lock);
    return stm->actions[state_action->action](stm, prv, arg1, arg2);
}

int stm_event_atomic(
            stm_t *stm,
            unsigned int event,
            void *prv,
            unsigned int arg1,
            void *arg2
        )
{
    int err;
    osal_mutex_lock(stm->lock);
    err = stm_event(stm, event, prv, arg1, arg2);
    osal_mutex_unlock(stm->lock);
    return err;
}

int stm_event(
            stm_t *stm,
            unsigned int event,
            void *prv,
            unsigned int arg1,
            void *arg2
        )
{
    stm_state_action_t *state_action;

    if (!STM_TBL_EVENT_IS_VALID(stm->state_action_table_desc, event)) {
        mmsdbg(DL_ERROR, "Invalid STM event (in \"%s\" STM)!", stm->name);
        return -1;
    }

    state_action = STM_TBL_GET_ELEM(
                    stm->state_action_table_desc,
                    stm->current_state,
                    event
                );
    stm->last_state= stm->current_state;
    stm->current_state = state_action->new_state;
    return stm->actions[state_action->action](stm, prv, arg1, arg2);
}

stm_t * stm_create(
            char *name,
            stm_descriptor_t *stm_desc,
            stm_actions_t *actions
        )
{
    stm_t *stm;

    stm = osal_malloc(sizeof(*stm));
    if (!stm) {
        mmsdbg(
                DL_ERROR,
                "Failed to allocate memory for new instance: name=\"%s\", size=%d!",
                name,
                sizeof (*stm)
            );
        goto exit1;
    }

    stm->actions = osal_malloc(
              sizeof(stm_actions_t)
            * stm_desc->state_action_table_desc->max_actions_number
        );
    if (!stm->actions) {
        mmsdbg(
                DL_ERROR,
                "Failed to allocate memory for \"stm->actions\": name=\"%s\", size=%d!",
                name,
                  sizeof(stm_actions_t)
                * stm_desc->state_action_table_desc->max_actions_number
            );
        goto exit2;
    }

    stm->lock = osal_mutex_create();
    if (!stm->lock) {
        mmsdbg(
                DL_ERROR,
                "Failed to create mutex for STM instance: name=\"%s\"!",
                name
            );
        goto exit3;
    }

    osal_strncpy(stm->name, name, sizeof (stm->name)-1);
    stm->name[sizeof (stm->name)-1] = '\000';

    osal_memcpy(
            stm->actions,
            actions,
              sizeof(stm_actions_t)
            * stm_desc->state_action_table_desc->max_actions_number
        );

    stm->current_state = stm_desc->init_state;
    stm->last_state= stm->current_state;
    stm->state_action_table_desc = stm_desc->state_action_table_desc;

    return stm;
exit3:
    osal_free(stm->actions);
exit2:
    osal_free(stm);
exit1:
    return NULL;
}

void stm_destroy(
            stm_t *stm
        )
{
    osal_mutex_destroy(stm->lock);
    osal_free(stm->actions);
    osal_free(stm);
}

