/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file func_thread.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <osal/osal_stdio.h>
#include <osal/osal_stdlib.h>
#include <osal/osal_string.h>
#include <osal/osal_assert.h>
#include <osal/osal_mutex.h>
#include <osal/osal_thread.h>
#include <utils/mms_debug.h>

#include <func_thread.h>

#define FUNC_THREAD_NAME_MAX 256

mmsdbg_define_variable(
        vdl_func_thread,
        DL_DEFAULT,
        0,
        "vdl_func_thread",
        "Module to handle events in separate thread."
    );
#define MMSDEBUGLEVEL mmsdbg_use_variable(vdl_func_thread)

typedef struct {
    func_thread_handle_t func;
    int pending;
} func_pend_t;

typedef struct {
    osal_mutex *lock;
    func_pend_t *func_pend;
    int func_pend_size;
} handles_t;

struct func_thread {
    char name[FUNC_THREAD_NAME_MAX];
    handles_t handles;
    struct osal_thread *thread;
    osal_sem *thread_sem;
    int thread_exit_flag;
    void *app_prv;
};

/* ========================================================================== */
/**
* exec_highest_pending()
*
*/
/* ========================================================================== */
static void exec_highest_pending(func_thread_t *ethr)
{
    int i;
    osal_mutex_lock(ethr->handles.lock);
    for (i = ethr->handles.func_pend_size; i;) {
        i--;
        osal_assert(0 <= ethr->handles.func_pend[i].pending);
        if (ethr->handles.func_pend[i].pending) {
            ethr->handles.func_pend[i].pending--;
            osal_mutex_unlock(ethr->handles.lock);
            ethr->handles.func_pend[i].func(ethr, ethr->app_prv);
            return;
        }
    }
    osal_mutex_unlock(ethr->handles.lock);
}

/* ========================================================================== */
/**
* func_thread_func()
*
*/
/* ========================================================================== */
static void * func_thread_func(void *arg)
{
    func_thread_t *ethr;

    ethr = arg;

    for (;;) {
        osal_sem_wait(ethr->thread_sem);
        if (ethr->thread_exit_flag) {
            break;
        }
        exec_highest_pending(ethr);
    }

    return NULL;
}

/* ========================================================================== */
/**
* func_thread_done_cb()
*
*/
/* ========================================================================== */
static int func_thread_done_cb(void *arg)
{
    func_thread_t *ethr;
    ethr = arg;
    ethr->thread_exit_flag = 1;
    osal_sem_post(ethr->thread_sem);
    return 0;
}

/* ========================================================================== */
/**
* func_thread_id()
*
*/
/* ========================================================================== */
struct osal_thread * func_thread_id(func_thread_t *ethr)
{
    return ethr->thread;
}

/* ========================================================================== */
/**
* func_thread_exec()
*
*/
/* ========================================================================== */
int func_thread_exec(func_thread_t *ethr, func_thread_handle_t func)
{
    int i;

    for (i = 0; i < ethr->handles.func_pend_size; i++) {
        if (ethr->handles.func_pend[i].func == func) {
            osal_mutex_lock(ethr->handles.lock);
            ethr->handles.func_pend[i].pending++;
            osal_mutex_unlock(ethr->handles.lock);
            osal_sem_post(ethr->thread_sem);
            return 0;
        }
    }

    mmsdbg(DL_ERROR, "%s: Wrong function to trigger!", ethr->name);

    return -1;
}

/* ========================================================================== */
/**
* func_thread_destroy()
*
*/
/* ========================================================================== */
void func_thread_destroy(func_thread_t *ethr)
{
    osal_thread_destroy(ethr->thread);
    osal_sem_destroy(ethr->thread_sem);
    osal_mutex_destroy(ethr->handles.lock);
    osal_free(ethr->handles.func_pend);
    osal_free(ethr);
}

/* ========================================================================== */
/**
* func_thread_create()
*
*/
/* ========================================================================== */
func_thread_t *func_thread_create(
        char *name,
        int stack_size,
        int prio,
        func_thread_handles_t *hdls,
        void *app_prv
    )
{
    func_thread_t *ethr;
    int i;

    ethr = osal_malloc(sizeof(*ethr));
    if (!ethr) {
        mmsdbg(
                DL_ERROR,
                "Failed to allocate memory for new instance: size=%d!",
                sizeof (*ethr)
            );
        goto exit1;
    }

    ethr->handles.func_pend_size = hdls->size;
    ethr->handles.func_pend = osal_malloc(
            ethr->handles.func_pend_size * sizeof (func_pend_t)
        );
    if (!ethr->handles.func_pend) {
        mmsdbg(
                DL_ERROR,
                "Failed to allocate memory for \"func_pend\" array: size=%d!",
                ethr->handles.func_pend_size * sizeof (func_pend_t)
            );
        goto exit2;
    }
    for (i = 0; i < ethr->handles.func_pend_size; i++) {
        ethr->handles.func_pend[i].func = hdls->fxns[i];
        ethr->handles.func_pend[i].pending = 0;
    }

    osal_strncpy(ethr->name, name, sizeof (ethr->name)-1);
    ethr->name[sizeof (ethr->name)-1] = '\000';
    ethr->app_prv = app_prv;

    ethr->handles.lock = osal_mutex_create();
    if (!ethr->handles.lock) {
        mmsdbg(
                DL_ERROR,
                "Failed to create mutex \"handles.lock\"!"
            );
        goto exit3;
    }

    ethr->thread_sem = osal_sem_create(0);
    if (!ethr->thread_sem) {
        mmsdbg(
                DL_ERROR,
                "Failed to create semaphore \"thread_sem\"!"
            );
        goto exit4;
    }

    ethr->thread_exit_flag = 0;
    ethr->thread = osal_thread_create(
            ethr->name,
            ethr,
            func_thread_func,
            func_thread_done_cb,
            0,
            stack_size
        );
    if (!ethr->thread) {
        mmsdbg(
                DL_ERROR,
                "Failed to create thread: name=\"%s\", stack_size=%d!",
                ethr->name,
                stack_size
            );
        goto exit5;
    }
    osal_thread_priority(ethr->thread, prio);

    return ethr;
exit5:
    osal_sem_destroy(ethr->thread_sem);
exit4:
    osal_mutex_destroy(ethr->handles.lock);
exit3:
    osal_free(ethr->handles.func_pend);
exit2:
    osal_free(ethr);
exit1:
    return NULL;
}

