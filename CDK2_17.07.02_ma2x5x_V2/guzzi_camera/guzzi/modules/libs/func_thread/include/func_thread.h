/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file func_thread.h
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#ifndef _FUNC_THREAD_H
#define _FUNC_THREAD_H

#include <osal/osal_thread.h>

typedef struct func_thread func_thread_t;

typedef void (*func_thread_handle_t)(
        func_thread_t *ethr,
        void *app_prv
    );

typedef struct {
    func_thread_handle_t *fxns;
    int size;
} func_thread_handles_t;

/* ========================================================================== */
/**
* func_thread_id()
*
*/
/* ========================================================================== */
struct osal_thread * func_thread_id(func_thread_t *ethr);

/* ========================================================================== */
/**
* func_thread_exec()
*
*/
/* ========================================================================== */
int func_thread_exec(func_thread_t *ethr, func_thread_handle_t func);

/* ========================================================================== */
/**
* func_thread_destroy()
*
*/
/* ========================================================================== */
void func_thread_destroy(func_thread_t *ethr);

/* ========================================================================== */
/**
* func_thread_create()
*
*/
/* ========================================================================== */
func_thread_t *func_thread_create(
        char *name,
        int stack_size,
        int prio,
        func_thread_handles_t *hdls,
        void *app_prv
    );

#endif /* _FUNC_THREAD_H */

