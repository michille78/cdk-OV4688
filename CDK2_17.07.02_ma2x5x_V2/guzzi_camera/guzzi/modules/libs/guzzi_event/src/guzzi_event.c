/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file guzzi_event.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <osal/osal_stdio.h>
#include <osal/osal_stdtypes.h>
#include <osal/osal_stdlib.h>
#include <osal/osal_string.h>
#include <osal/osal_list.h>
#include <osal/osal_mutex.h>
#include <utils/mms_debug.h>
#include "guzzi_event.h"

typedef struct {
    struct list_head link;
    void *recip_prv;
    guzzi_event_notify_t notify;
} recip_list_node_t;

typedef struct {
    struct list_head recip_list;
    osal_mutex *lock;
    unsigned int notify_count;
    int created;
    int event_id;
} events_table_entry_t;

typedef struct {
    events_table_entry_t *entries;
    int entries_num;
    osal_mutex *lock;
} events_table_t;

struct guzzi_event {
    events_table_t events_table;
};

mmsdbg_define_variable(
        vdl_guzzi_event,
        DL_DEFAULT,
        0,
        "vdl_guzzi_event",
        "Camera event dispatcher module."
    );
#define MMSDEBUGLEVEL mmsdbg_use_variable(vdl_guzzi_event)

static int event_table_entry_init(events_table_entry_t *entry)
{
    entry->lock = osal_mutex_create();
    if (!entry->lock) {
        goto exit1;
    }

    INIT_LIST_HEAD(&entry->recip_list);

    entry->notify_count = 0;
    entry->created = 1;

    return 0;
exit1:
    return -1;
}

static void event_table_entry_deinit(events_table_entry_t *entry)
{
    if (entry->created == 0)
        return;

    osal_mutex_destroy(entry->lock);
    entry->created = 0;
}

guzzi_event_t * guzzi_event_create(
        guzzi_event_create_params_t *create_params
    )
{
    guzzi_event_t *evt;

    evt = osal_malloc(sizeof (*evt));
    if (!evt) {
        mmsdbg(
                DL_ERROR,
                "Failed to allocate memory for new instance: size=%d!",
                sizeof (*evt)
            );
        goto exit1;
    }

    evt->events_table.entries_num = create_params->max_events;

    evt->events_table.entries = osal_malloc(
            evt->events_table.entries_num * sizeof (events_table_entry_t)
        );
    if (!evt->events_table.entries) {
        mmsdbg(
                DL_ERROR,
                "Failed to allocate memory for new event table: size=%d!",
                evt->events_table.entries_num * sizeof (events_table_entry_t)
            );
        goto exit2;
    }
    osal_memset(evt->events_table.entries, 0, evt->events_table.entries_num * sizeof (events_table_entry_t));

    evt->events_table.lock = osal_mutex_create();
    if (!evt->events_table.lock) {
        goto exit3;
    }

    return evt;
exit3:
    osal_free(evt->events_table.entries);
exit2:
    osal_free(evt);
exit1:
    return NULL;
}

void guzzi_event_destroy(guzzi_event_t *evt)
{
    int i;

    for (i = 0; i < evt->events_table.entries_num; i++) {
        event_table_entry_deinit(evt->events_table.entries + i);
    }
    osal_mutex_destroy(evt->events_table.lock);
    osal_free(evt->events_table.entries);
    osal_free(evt);
}

int guzzi_event_reg_recipient(
        guzzi_event_t *evt,
        int event_id,
        guzzi_event_notify_t notify,
        void *prv
    )
{
    events_table_entry_t *entry;
    recip_list_node_t *node;
    int i;
    int err;

    node = osal_malloc(sizeof (*node));
    if (!node) {
        mmsdbg(
                DL_ERROR,
                "Failed to allocate memory for new node: size=%d!",
                sizeof (*node)
            );
        goto exit1;
    }

    osal_mutex_lock(evt->events_table.lock);
    err = 0;
    entry = NULL;
    for (i = 0; i <= evt->events_table.entries_num; i++) {
        if (!evt->events_table.entries[i].created) {
            entry = evt->events_table.entries + i;
        }
        if (event_id == evt->events_table.entries[i].event_id) {
            entry = evt->events_table.entries + i;
            break;
        }
    }
    if (!entry) {
        osal_mutex_unlock(evt->events_table.lock);
        mmsdbg(
                DL_ERROR,
                "Entry table is full (entries_num=%d)!",
                evt->events_table.entries_num
            );
        goto exit2;
    }
    if (!entry->created) {
        err = event_table_entry_init(entry);
    }
    osal_mutex_unlock(evt->events_table.lock);
    if (err) {
        mmsdbg(DL_ERROR, "Event table entry create failed!");
        goto exit2;
    }

    entry->event_id = event_id;

    INIT_LIST_HEAD(&node->link);
    node->recip_prv = prv;
    node->notify = notify;

    osal_mutex_lock(entry->lock);
    /* TODO: Already in the list? */
    list_add(&node->link, &entry->recip_list);
    osal_mutex_unlock(entry->lock);

    return 0;
exit2:
    osal_free(node);
exit1:
    return -1;
}

int guzzi_event_unreg_recipient(
        guzzi_event_t *evt,
        int event_id,
        guzzi_event_notify_t notify,
        void *prv
    )
{
    events_table_entry_t *entry;
    recip_list_node_t *node;
    struct list_head *el;
    int found;
    int i;

    entry = NULL;

    osal_mutex_lock(evt->events_table.lock);
    for (i = 0; i <= evt->events_table.entries_num; i++) {
        if (   evt->events_table.entries[i].created
            && (evt->events_table.entries[i].event_id == event_id) )
        {
            entry = evt->events_table.entries + i;
            break;
        }
    }
    if (!entry) {
        osal_mutex_unlock(evt->events_table.lock);
        mmsdbg(
                DL_ERROR,
                "UnReg: EventID not found. ID=%d; Notify=%p",
                event_id,
                notify
            );
        goto exit1;
    }
    found = 0;

    osal_mutex_lock(entry->lock);   // Ensure list is not used for sending evens while we remove this entry
    list_for_each(el, &entry->recip_list) {
        node = list_entry(el, recip_list_node_t, link);
        if ((node->notify == notify) && (node->recip_prv == prv)) {
            found = 1;
            break;
        }
    }
    if (!found) {
        osal_mutex_unlock(evt->events_table.lock);
        osal_mutex_unlock(entry->lock);
        mmsdbg(
                DL_ERROR,
                "UnReg: Recipient not found. ID=%d; Notify=%p",
                event_id,
                notify
            );
        goto exit1;
    }
    list_del(&node->link);

    osal_mutex_unlock(entry->lock);

    if (list_empty(&entry->recip_list)) {
        event_table_entry_deinit(entry);
    }

    osal_mutex_unlock(evt->events_table.lock);

    osal_free(node);

    return 0;
exit1:
    return -1;
}

int guzzi_event_send(
        guzzi_event_t *evt,
        int event_id,
        uint32 num,
        void *data
    )
{
    events_table_entry_t *entry;
    recip_list_node_t *node;
    struct list_head *el;
    int i;

    entry = NULL;
    for (i = 0; i <= evt->events_table.entries_num; i++) {
        if (   evt->events_table.entries[i].created
            && (evt->events_table.entries[i].event_id == event_id) )
        {
            entry = evt->events_table.entries + i;
            break;
        }
    }
    if (!entry) {
        mmsdbg(
                DL_ERROR,
                "Send: Event ID=%x no registered recipient",
                event_id
            );
        goto exit1;
    }

    osal_mutex_lock(entry->lock);
    list_for_each(el, &entry->recip_list) {
        node = list_entry(el, recip_list_node_t, link);
        node->notify(
                node->recip_prv,
                event_id,
                num,
                data
            );
    }
    osal_mutex_unlock(entry->lock);

    return 0;
exit1:
    return -1;
}

