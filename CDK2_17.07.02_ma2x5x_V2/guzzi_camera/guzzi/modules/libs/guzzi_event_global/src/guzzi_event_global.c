/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file guzzi_event_global.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <osal/osal_stdtypes.h>
#include <guzzi_event/include/guzzi_event.h>
#include "guzzi_event_global.h"

#ifndef GUZZI_EVENT_GLOBAL_EVENTS_MAX
#define GUZZI_EVENT_GLOBAL_EVENTS_MAX 100
#endif

static guzzi_event_t *guzzi_event_global_handle;

static const char* id2str[] = {
    "CAM_EVT_SOF",
    "CAM_EVT_AFD_READY",
    "CAM_EVT_AF_READY",
    "CAM_EVT_AE_READY",
    "CAM_EVT_AE_STAB_READY",
    "CAM_EVT_AWB_READY",
    "CAM_EVT_SEN_MODE_CHANGE",
    "CAM_EVT_DETECTED_CAMERA",
    "CAM_EVT_BUFF_LOCKED",
    "CAM_EVT_FMV_READY",
    "CAM_EVT_FR_ISP_REACHED",
    "CAM_EVT_FR_ISP_DONE",
    "CAM_EVT_LRT_STARTED",
    "CAM_EVT_MOVE_LENS"
};

const char * guzzi_event_global_id2str(int id)
{
    if (ARRAY_SIZE(id2str) <= geg_camera_event_get_eid(id)) {
        return "GUZZI_EVENT_GLOBAL_INVALID";
    }
    return id2str[id];
}

guzzi_event_t *guzzi_event_global(void)
{
    return guzzi_event_global_handle;
}

void guzzi_event_global_destroy(void)
{
    guzzi_event_destroy(guzzi_event_global_handle);
}

void guzzi_event_global_ctreate(void)
{
    guzzi_event_create_params_t params;
    params.max_events = GUZZI_EVENT_GLOBAL_EVENTS_MAX;
    guzzi_event_global_handle = guzzi_event_create(&params);
}

