/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file fifo.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <osal/osal_stdlib.h>
#include <osal/osal_mutex.h>
#include <utils/mms_debug.h>
#include <fifo.h>

struct fifo {
    fifo_entry_t *head;
    fifo_entry_t *tail;
    osal_mutex *lock;
};

mmsdbg_define_variable(
        vdl_fifo,
        DL_DEFAULT,
        0,
        "vdl_fifo",
        "Simple FIFO implementation."
    );
#define MMSDEBUGLEVEL mmsdbg_use_variable(vdl_fifo)

void fifo_lock(fifo_t *fifo)
{
    osal_mutex_lock(fifo->lock);
}

void fifo_unlock(fifo_t *fifo)
{
    osal_mutex_unlock(fifo->lock);
}

int fifo_is_empty(fifo_t *fifo)
{
    return fifo->head == NULL;
}

void fifo_put(fifo_t *fifo, fifo_entry_t *newe)
{
    newe->next = NULL;
    if (NULL == fifo->tail) {
        fifo->head = newe;
    } else {
        fifo->tail->next = newe;
    }
    fifo->tail = newe;
}

fifo_entry_t *fifo_get(fifo_t *fifo)
{
    fifo_entry_t *e;

    if (!fifo->head) {
        mmsdbg(DL_ERROR, "Dequeue from empty queue!");
        return NULL;
    }

    e = fifo->head;
    fifo->head = e->next;
    if (NULL == fifo->head) {
        fifo->tail = NULL;
    }
    e->next = NULL;

    return e;
}

void fifo_destroy(fifo_t *fifo)
{
    if (!fifo_is_empty(fifo)) {
        mmsdbg(DL_ERROR, "Destroying non empty queue!");
    }
    osal_mutex_destroy(fifo->lock);
    osal_free(fifo);
}

fifo_t *fifo_create(void)
{
    fifo_t *fifo;

    fifo = osal_malloc(sizeof (*fifo));
    if (!fifo) {
        mmsdbg(
                DL_ERROR,
                "Failed to allocate memory for new Fifo instance: size=%d!",
                sizeof (*fifo)
            );
        goto exit1;
    }

    fifo->lock = osal_mutex_create();
    if (!fifo->lock) {
        mmsdbg(DL_ERROR, "Failed to fifo mutex lock!");
        goto exit2;
    }

    fifo->tail = NULL;
    fifo->head = NULL;

    return fifo;
exit2:
    osal_free(fifo);
exit1:
    return NULL;
}

