/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file fifo_sem.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <osal/osal_stdtypes.h>
#include <osal/osal_stdlib.h>
#include <osal/osal_mutex.h>
#include <utils/mms_debug.h>
#include <fifo.h>
#include <fifo_sem.h>

struct fifo_sem {
    fifo_t *fifo;
    osal_sem *count_sem;
    char *name;
};

mmsdbg_define_variable(
        vdl_fifo_sem,
        DL_DEFAULT,
        0,
        "vdl_fifo_sem",
        "FIFO with counting semaphore."
    );
#define MMSDEBUGLEVEL mmsdbg_use_variable(vdl_fifo_sem)

static inline fifo_entry_t *fifo_get_safe(fifo_t *fifo)
{
    fifo_entry_t *e;
    fifo_lock(fifo);
    e = fifo_get(fifo);
    fifo_unlock(fifo);
    return e;
}

int fifo_sem_is_empty(fifo_sem_t *fifo)
{
    return fifo_is_empty(fifo->fifo);
}

void fifo_sem_put(fifo_sem_t *fifo, fifo_entry_t *newe)
{
    fifo_put(fifo->fifo, newe);
    osal_sem_post(fifo->count_sem);
}

fifo_entry_t *fifo_sem_get(fifo_sem_t *fifo)
{
    osal_sem_wait(fifo->count_sem);
    return fifo_get_safe(fifo->fifo);
}

fifo_entry_t *fifo_sem_try_get(fifo_sem_t *fifo)
{
    if (!osal_sem_try_wait(fifo->count_sem)) {
        return fifo_get_safe(fifo->fifo);
    }
    return NULL;
}

fifo_entry_t *fifo_sem_get_timeout(fifo_sem_t *fifo, uint32 ms)
{
    if (!osal_sem_wait_timeout(fifo->count_sem, ms)) {
        return fifo_get_safe(fifo->fifo);
    }
    mmsdbg(DL_WARNING, "fifo sem alloc timeout");
    return NULL;
}

void fifo_sem_destroy(fifo_sem_t *fifo)
{
    osal_sem_destroy(fifo->count_sem);
    fifo_destroy(fifo->fifo);
    osal_free(fifo);
}

fifo_sem_t *fifo_sem_create(void)
{
    fifo_sem_t *fifo;

    fifo = osal_malloc(sizeof (*fifo));
    if (!fifo) {
        mmsdbg(
                DL_ERROR,
                "Failed to allocate memory for new Sem FIFO instance: size=%d!",
                sizeof (*fifo)
            );
        goto exit1;
    }

    fifo->fifo = fifo_create();
    if (!fifo->fifo) {
        mmsdbg(
                DL_ERROR,
                "Failed to create base fifo for Sem FIFO instance!"
            );
        goto exit2;
    }

    fifo->count_sem = osal_sem_create(0);
    if (!fifo->count_sem) {
        mmsdbg(
                DL_ERROR,
                "Failed to create semaphore for Sem FIFO instance!"
            );
        goto exit3;
    }

    return fifo;
exit3:
    fifo_destroy(fifo->fifo);
exit2:
    osal_free(fifo);
exit1:
    return NULL;
}

