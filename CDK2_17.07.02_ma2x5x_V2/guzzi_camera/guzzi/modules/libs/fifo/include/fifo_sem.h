/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file fifo_sem.h
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#ifndef _FIFO_SEM_H
#define _FIFO_SEM_H

#include <osal/osal_stdtypes.h>
#include <fifo/include/fifo.h>

typedef struct fifo_sem fifo_sem_t;

int fifo_sem_is_empty(fifo_sem_t *fifo);
void fifo_sem_put(fifo_sem_t *fifo, fifo_entry_t *newe);
fifo_entry_t *fifo_sem_get(fifo_sem_t *fifo);
fifo_entry_t *fifo_sem_try_get(fifo_sem_t *fifo);
fifo_entry_t *fifo_sem_get_timeout(fifo_sem_t *fifo, uint32 ms);
void fifo_sem_destroy(fifo_sem_t *fifo);
fifo_sem_t *fifo_sem_create(void);

#endif /* _FIFO_SEM_H */

