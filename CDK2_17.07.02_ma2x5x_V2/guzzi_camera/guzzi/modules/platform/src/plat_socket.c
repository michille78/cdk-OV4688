/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file plat_socket.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <platform/inc/plat_socket.h>
#include <platform/inc/plat_hw_resource.h>

#include <error_handle/include/error_handle.h>

#include <utils/mms_debug.h>

mmsdbg_define_variable(
        plat_socket,
        DL_DEFAULT,
        0,
        "plat_socket",
        "Platform socket"
    );
#define MMSDEBUGLEVEL mmsdbg_use_variable(plat_socket)

extern plat_socket_obj_t *plat_sockets;

static int plat_socket_dummy_set(HAT_HW_RESOURCE_ACTION_T action)
{
    mmsdbg(DL_MESSAGE, "RES PLAT: DUMMY: DUMMY set %d executed!", action);

    return 0;
}

static int plat_socket_dummy_init(plat_res_desc_dummy_t *prms, hat_cm_socket_dummy_hndl_t *hndl)
{
    mmsdbg(DL_MESSAGE, "RES PLAT: DUMMY: Create DUMMY with params: %s %d",
        prms->name,
        prms->id);

    //TODO: - The real function should call DUMMY platform driver with
    // these parameters and return handle to all DUMMY operations
    hndl->dummy_set = plat_socket_dummy_set;

    mmsdbg(DL_MESSAGE, "RES PLAT: DUMMY: DUMMY handle created at %010p", hndl);
    return 0;
}

static plat_dummy_t dummy = {
    .init_data = {
        .name   = "Dummy",
        .id     = 3003,
    },
    .init_function = plat_socket_dummy_init,
    .hndl = {
        .dummy_set = NULL,
    }
};
static plat_resource_t res_dummy = { HW_RSRC_DUMMY, PLAT_RES_CLOSE, &dummy };
static plat_res_desc_t comp_res_dummy = {  &res_dummy };

static plat_res_desc_t comp_dumm_res[DUMM_RSRC_MAX];
static plat_res_desc_t comp_sen_res[SEN_RSRC_MAX];
static plat_res_desc_t comp_lens_res[LENS_RSRC_MAX];
static plat_res_desc_t comp_flash_res[FLASH_RSRC_MAX];
static plat_res_desc_t comp_nvm_res[NVM_RSRC_MAX];

/* ========================================================================== */
/**
* plat_socket_hw_hndl_get()
*/
/* ========================================================================== */
int plat_socket_hw_hndl_get(plat_socket_rsrc_selector_t* sel, int *hw_type,
    void** hw_hndl)
{
    int err = 0;

    int ph_sock = sel->ph_socket;
    int sock_comp = sel->sock_comp;
    int comp_res = sel->comp_rsrc;

    plat_socket_res_desc_t* socket_resorce_descriptors;
    plat_res_desc_t*    rsrc_arr;
    plat_res_desc_t     rsrc;

    int rsrc_type;
    void *rsrc_hndl;

    mmsdbg(DL_MESSAGE, "//VIV PLAT: Try to get HW handle to: ph_sock %d sock_comp %d comp_res %d",
        ph_sock, sock_comp, comp_res);

    socket_resorce_descriptors = plat_sockets[ph_sock].soc_res;

    if (!socket_resorce_descriptors) {
        mmsdbg(DL_ERROR, "//VIV PLAT: ERROR: socket_resorce_descriptors is NULL!");
        err = -1;
        GOTO_EXIT_IF(0 != err, 1);
    }

    switch (sock_comp) {
        case SOCKET_COMP_DUMMY:
            rsrc_arr = *(socket_resorce_descriptors->dummy_res);
            break;
        case SOCKET_COMP_SENSOR:
            rsrc_arr = *(socket_resorce_descriptors->sensor_res);
            break;
        case SOCKET_COMP_LENS:
            rsrc_arr = *(socket_resorce_descriptors->lens_res);
            break;
        case SOCKET_COMP_FLASH:
            rsrc_arr = *(socket_resorce_descriptors->flash_res);
            break;
        case SOCKET_COMP_NVM:
            rsrc_arr = *(socket_resorce_descriptors->nvm_res);
            break;
        case SOCKET_COMP_MAX:
        default:
            err = -1;
            GOTO_EXIT_IF(0 != err, 1);
    }

    if (!rsrc_arr) {
        mmsdbg(DL_ERROR, "//VIV PLAT: ERROR: rsrc_arr is NULL!");
        err = -1;
        GOTO_EXIT_IF(0 != err, 1);
    }

    rsrc = rsrc_arr[comp_res];

    rsrc_type = rsrc.res_desc->type;

    *hw_type = rsrc_type;

    switch (rsrc_type) {
        case HW_RSRC_DUMMY:
            rsrc_hndl = &(((plat_dummy_t*)(rsrc.res_desc->init))->hndl);
            break;
        case HW_RSRC_POWER:
            rsrc_hndl = &(((plat_power_dev_desc_t*)(rsrc.res_desc->init))->hndl);
            break;
        case HW_RSRC_GPIO:
            rsrc_hndl = (((plat_gpio_dev_desc_t*)(rsrc.res_desc->init))->instance);
            break;
        case HW_RSRC_I2C:
            rsrc_hndl = (((plat_i2c_dev_desc_t*)(rsrc.res_desc->init))->instance);
            break;
        case HW_RSRC_CLOCK:
            rsrc_hndl = &(((plat_clock_t*)(rsrc.res_desc->init))->hndl);
            break;
        case HW_RSRC_CSI2:
            rsrc_hndl = &(((plat_csi2_t*)(rsrc.res_desc->init))->hndl);
            break;
        case HW_RSRC_MAX:
        default:
            err = -1;
            GOTO_EXIT_IF(0 != err, 1);
    }

    *hw_hndl = rsrc_hndl;

    return 0;

EXIT_1:
    mmsdbg(DL_ERROR, "Exit Err");
    return -1;
}

/* ========================================================================== */
/**
* plat_socket_detect()
*/
/* ========================================================================== */
int plat_socket_detect(plat_socket_handle_t hndl)
{
    return 0;
}

/* ========================================================================== */
/**
* plat_socket_open()
*/
/* ========================================================================== */
int plat_socket_open(plat_socket_obj_t *socket, hat_cm_socket_open_params_t *params)
{
    int err = 0;
    int comp, res, res_min, res_max, res_type;
    plat_res_desc_t rsrc;
    plat_res_desc_t *socket_comp_rsrcs = NULL;
    void *open = NULL;
    hat_socket_comp_cfg_t *comp_params;

    for (comp = SOCKET_COMP_DUMMY; comp < SOCKET_COMP_MAX; comp++) {

        switch (comp) {
            case SOCKET_COMP_DUMMY:
                res_min = DUMM_RSRC_DUMMY;
                res_max = DUMM_RSRC_MAX;
                socket_comp_rsrcs = &((*(socket->soc_res->dummy_res))[res_min]);
                comp_params = NULL;
                break;
            case SOCKET_COMP_SENSOR:
                res_min = SEN_RSRC_DUMMY;
                res_max = SEN_RSRC_MAX;
                socket_comp_rsrcs = &((*(socket->soc_res->sensor_res))[res_min]);
                comp_params = &params->sensor;
                break;
            case SOCKET_COMP_LENS:
                res_min = LENS_RSRC_DUMMY;
                res_max = LENS_RSRC_MAX;
                socket_comp_rsrcs = &((*(socket->soc_res->lens_res))[res_min]);
                comp_params = &params->lens;
                break;
            case SOCKET_COMP_FLASH:
                res_min = FLASH_RSRC_DUMMY;
                res_max = FLASH_RSRC_MAX;
                socket_comp_rsrcs = &((*(socket->soc_res->flash_res))[res_min]);
                comp_params = &params->light;
                break;
            case SOCKET_COMP_NVM:
                res_min = NVM_RSRC_DUMMY;
                res_max = NVM_RSRC_MAX;
                socket_comp_rsrcs = &((*(socket->soc_res->nvm_res))[res_min]);
                comp_params = &params->nvm;
                break;
            case SOCKET_COMP_MAX:
            default:
                res_min = 0;
                res_max = 0;
                mmsdbg(DL_ERROR, "//VIV PLAT: ERROR: No such a CM component!");
                err = -1;
                GOTO_EXIT_IF(0 != err, 1);
                break;
        }

        if (!socket_comp_rsrcs) {
            mmsdbg(DL_ERROR, "//VIV PLAT: ERROR: socket_comp_rsrcs is NULL!");
            err = -1;
            GOTO_EXIT_IF(0 != err, 1);
        }

        for (res = res_min; res < res_max; res++) {

            rsrc = socket_comp_rsrcs[res];

            res_type = rsrc.res_desc->type;
            open     = rsrc.res_desc->init;

            mmsdbg(DL_MESSAGE, "//VIV PLAT: >>>>>>>>> comp %d, res %d, res_type %d, init = %010p", comp, res, res_type, open);

            switch (res_type) {
                case HW_RSRC_DUMMY:
                    //TODO
                    break;
                case HW_RSRC_POWER:
                    //TODO
                    break;
                case HW_RSRC_GPIO:
                    //TODO
                    break;
                case HW_RSRC_I2C:
                    if(((plat_i2c_dev_desc_t*)open)->instance->regdev(((plat_i2c_dev_desc_t*)open)->instance, comp_params))
                    {
                        mmsdbg(DL_ERROR, "Failed to deinit I2C hw instance!");
                        err++;
                    }
                    break;
                case HW_RSRC_CLOCK:
                    //TODO
                    break;
                case HW_RSRC_CSI2:
                    //TODO
                    break;
                case HW_RSRC_MAX:
                default:
                    mmsdbg(DL_ERROR, "//VIV PLAT: ERROR: No such a resource type!");
                    err = -1;
                    GOTO_EXIT_IF(0 != err, 1);
                    break;
            }
        }
    }

    GOTO_EXIT_IF(0 != err, 1);

    return 0;

EXIT_1:
    mmsdbg(DL_ERROR, "//VIV PLAT: Exit Err");
    return -1;
}

/* ========================================================================== */
/**
* plat_socket_close()
*/
/* ========================================================================== */
int plat_socket_close(plat_socket_handle_t hndl)
{
    return 0;
}

/* ========================================================================== */
/**
* plat_socket_deinit()
*/
/* ========================================================================== */
int plat_socket_deinit(plat_socket_obj_t *socket)
{
    int err = 0;
    int comp, res, res_min, res_max, res_type;
    plat_res_desc_t rsrc;
    plat_res_desc_t *socket_comp_rsrcs = NULL;
    void *init = NULL;

    for (comp = SOCKET_COMP_DUMMY; comp < SOCKET_COMP_MAX; comp++) {

        switch (comp) {
            case SOCKET_COMP_DUMMY:
                res_min = DUMM_RSRC_DUMMY;
                res_max = DUMM_RSRC_MAX;
                socket_comp_rsrcs = &((*(socket->soc_res->dummy_res))[res_min]);
                break;
            case SOCKET_COMP_SENSOR:
                res_min = SEN_RSRC_DUMMY;
                res_max = SEN_RSRC_MAX;
                socket_comp_rsrcs = &((*(socket->soc_res->sensor_res))[res_min]);
                break;
            case SOCKET_COMP_LENS:
                res_min = LENS_RSRC_DUMMY;
                res_max = LENS_RSRC_MAX;
                socket_comp_rsrcs = &((*(socket->soc_res->lens_res))[res_min]);
                break;
            case SOCKET_COMP_FLASH:
                res_min = FLASH_RSRC_DUMMY;
                res_max = FLASH_RSRC_MAX;
                socket_comp_rsrcs = &((*(socket->soc_res->flash_res))[res_min]);
                break;
            case SOCKET_COMP_NVM:
                res_min = NVM_RSRC_DUMMY;
                res_max = NVM_RSRC_MAX;
                socket_comp_rsrcs = &((*(socket->soc_res->nvm_res))[res_min]);
                break;
            case SOCKET_COMP_MAX:
            default:
                res_min = 0;
                res_max = 0;
                mmsdbg(DL_ERROR, "//VIV PLAT: ERROR: No such a CM component!");
                err = -1;
                GOTO_EXIT_IF(0 != err, 1);
                break;
        }

        if (!socket_comp_rsrcs) {
            mmsdbg(DL_ERROR, "//VIV PLAT: ERROR: socket_comp_rsrcs is NULL!");
            err = -1;
            GOTO_EXIT_IF(0 != err, 1);
        }

        for (res = res_min; res < res_max; res++) {

            rsrc = socket_comp_rsrcs[res];

            res_type = rsrc.res_desc->type;
            init     = rsrc.res_desc->init;

            mmsdbg(DL_MESSAGE, "//VIV PLAT: >>>>>>>>> comp %d, res %d, res_type %d, init = %010p", comp, res, res_type, init);

            switch (res_type) {
                case HW_RSRC_DUMMY:
                    //TODO
                    break;
                case HW_RSRC_POWER:
                    //TODO
                    break;
                case HW_RSRC_GPIO:

                        if(((plat_gpio_dev_desc_t*)init)->deinit(
                                ((plat_gpio_dev_desc_t*)init)->instance))
                        {
                            mmsdbg(DL_ERROR, "Failed to deinit GPIO hw instance!");
                            err++;
                        }
                    break;
                case HW_RSRC_I2C:
                    if(((plat_i2c_dev_desc_t*)init)->deinit(
                            ((plat_i2c_dev_desc_t*)init)->instance))
                    {
                        mmsdbg(DL_ERROR, "Failed to deinit I2C hw instance!");
                        err++;
                    }
                    break;
                case HW_RSRC_CLOCK:
                    //TODO
                    break;
                case HW_RSRC_CSI2:
                    //TODO
                    break;
                case HW_RSRC_MAX:
                default:
                    mmsdbg(DL_ERROR, "//VIV PLAT: ERROR: No such a resource type!");
                    err = -1;
                    GOTO_EXIT_IF(0 != err, 1);
                    break;
            }
        }
    }

    GOTO_EXIT_IF(0 != err, 1);

    return 0;

EXIT_1:
    mmsdbg(DL_ERROR, "//VIV PLAT: Exit Err");
    return -1;
}

#define RES_EXEC_INIT(exec_func, init_prms, result_handle, error)  \
    if ((exec_func != NULL) && (init_prms != NULL)) {              \
        error = exec_func(init_prms, result_handle);               \
    } else {                                                       \
        mmsdbg(DL_ERROR, "//VIV PLAT: Error: NULL init routine or init params!"); \
    }

int plat_socket_init(plat_socket_obj_t *socket)
{
    int err = 0;
    int comp, res, res_min, res_max, res_type;
    plat_res_desc_t rsrc;
    plat_res_desc_t *socket_comp_rsrcs = NULL;
    void *init = NULL;

    socket->state   = PLAT_SOCKET_STATE_UNKNOWN;

    socket->detect  = plat_socket_detect;
    socket->open    = plat_socket_open;
    socket->close   = plat_socket_close;
    socket->deinit  = plat_socket_deinit;

    for (comp = SOCKET_COMP_DUMMY; comp < SOCKET_COMP_MAX; comp++) {

        switch (comp) {
            case SOCKET_COMP_DUMMY:
                res_min = DUMM_RSRC_DUMMY;
                res_max = DUMM_RSRC_MAX;
                if (socket->soc_res->dummy_res)
		    socket_comp_rsrcs = &((*(socket->soc_res->dummy_res))[res_min]);
		else {
		    socket_comp_rsrcs = comp_dumm_res;
		    socket->soc_res->dummy_res = &comp_dumm_res;
		}
                break;
            case SOCKET_COMP_SENSOR:
                res_min = SEN_RSRC_DUMMY;
                res_max = SEN_RSRC_MAX;
                if (socket->soc_res->sensor_res)
		    socket_comp_rsrcs = &((*(socket->soc_res->sensor_res))[res_min]);
		else {
		    socket_comp_rsrcs = comp_sen_res;
		    socket->soc_res->sensor_res = &comp_sen_res;
		}
                break;
            case SOCKET_COMP_LENS:
                res_min = LENS_RSRC_DUMMY;
                res_max = LENS_RSRC_MAX;

                if (socket->soc_res->lens_res)
		    socket_comp_rsrcs = &((*(socket->soc_res->lens_res))[res_min]);
		else {
		    socket_comp_rsrcs = comp_lens_res;
		    socket->soc_res->lens_res = &comp_lens_res;
		}
                break;
            case SOCKET_COMP_FLASH:
                res_min = FLASH_RSRC_DUMMY;
                res_max = FLASH_RSRC_MAX;
                if (socket->soc_res->flash_res)
		    socket_comp_rsrcs = &((*(socket->soc_res->flash_res))[res_min]);
		else {
		    socket_comp_rsrcs = comp_flash_res;
		    socket->soc_res->flash_res = &comp_flash_res;
		}
                break;
            case SOCKET_COMP_NVM:
                res_min = NVM_RSRC_DUMMY;
                res_max = NVM_RSRC_MAX;
                if (socket->soc_res->nvm_res)
		    socket_comp_rsrcs = &((*(socket->soc_res->nvm_res))[res_min]);
		else {
		    socket_comp_rsrcs = comp_nvm_res;
		    socket->soc_res->nvm_res = &comp_nvm_res;
		}
                break;
            case SOCKET_COMP_MAX:
            default:
                res_min = 0;
                res_max = 0;
                mmsdbg(DL_ERROR, "//VIV PLAT: ERROR: No such a CM component!");
                err = -1;
                GOTO_EXIT_IF(0 != err, 1);
                break;
        }

        if (!socket_comp_rsrcs) {
            mmsdbg(DL_ERROR, "//VIV PLAT: ERROR: socket_comp_rsrcs is NULL!");
            err = -1;
            GOTO_EXIT_IF(0 != err, 1);
        }

        for (res = res_min; res < res_max; res++) {

            rsrc = socket_comp_rsrcs[res];

            if (rsrc.res_desc == NULL) {
                mmsdbg(DL_MESSAGE, "RES %d is NULL", res);
                socket_comp_rsrcs[res] = rsrc = comp_res_dummy;
            }

            res_type = rsrc.res_desc->type;
            init     = rsrc.res_desc->init;

            mmsdbg(DL_MESSAGE, "//VIV PLAT: >>>>>>>>> comp %d, res %d, res_type %d, init = %010p", comp, res, res_type, init);

            switch (res_type) {
                case HW_RSRC_DUMMY:
                    mmsdbg(DL_MESSAGE, "//VIV PLAT: init = %010p", ((plat_dummy_t*)init)->init_function);
                    RES_EXEC_INIT(
                          ((plat_dummy_t*)init)->init_function,
                        &(((plat_dummy_t*)init)->init_data),
                        &(((plat_dummy_t*)init)->hndl),
                        err);
                    break;
                case HW_RSRC_POWER:
                    mmsdbg(DL_MESSAGE, "//VIV PLAT: init = %010p", ((plat_power_dev_desc_t*)init)->init_function);
                    RES_EXEC_INIT(
                          ((plat_power_dev_desc_t*)init)->init_function,
                        &(((plat_power_dev_desc_t*)init)->params),
                        &(((plat_power_dev_desc_t*)init)->hndl),
                        err);
                    break;
                case HW_RSRC_GPIO:
                    if (!((plat_gpio_dev_desc_t*)init)->instance)
                        ((plat_gpio_dev_desc_t*)init)->instance =
                            ((plat_gpio_dev_desc_t*)init)->init(
                                    &((plat_gpio_dev_desc_t*)init)->params
                                );
                    if (!((plat_gpio_dev_desc_t*)init)->instance) {
                        mmsdbg(DL_ERROR, "Failed to init GPIO hw instance!");
                        /* TODO: Error handle */
                    }
                    break;
                case HW_RSRC_I2C:
                    ((plat_i2c_dev_desc_t*)init)->instance = 
                        ((plat_i2c_dev_desc_t*)init)->init(
                                &((plat_i2c_dev_desc_t*)init)->params
                            );
                    if (!((plat_i2c_dev_desc_t*)init)->instance) {
                        mmsdbg(DL_ERROR, "Failed to init I2C hw instance!");
                        /* TODO: Error handle */
                    }
                    break;
                case HW_RSRC_CLOCK:
                    mmsdbg(DL_MESSAGE, "//VIV PLAT: init = %010p", ((plat_clock_t*)init)->init_function);
                    if (!(((plat_clock_t*)init)->hndl.clock_set))
                    {
                        RES_EXEC_INIT(
                            ((plat_clock_t*)init)->init_function,
                            &(((plat_clock_t*)init)->init_data),
                            &(((plat_clock_t*)init)->hndl),
                            err);
                    }
                    break;
                case HW_RSRC_CSI2:
                    mmsdbg(DL_MESSAGE, "//VIV PLAT: init = %010p", ((plat_csi2_t*)init)->init_function);
                    RES_EXEC_INIT(
                          ((plat_csi2_t*)init)->init_function,
                        &(((plat_csi2_t*)init)->init_data),
                        &(((plat_csi2_t*)init)->hndl),
                        err);
                    break;
                case HW_RSRC_MAX:
                default:
                    mmsdbg(DL_ERROR, "//VIV PLAT: ERROR: No such a resource type!");
                    err = -1;
                    GOTO_EXIT_IF(0 != err, 1);
                    break;
            }
        }
    }

    return 0;

EXIT_1:
    mmsdbg(DL_ERROR, "//VIV PLAT: Exit Err");
    return -1;
}
