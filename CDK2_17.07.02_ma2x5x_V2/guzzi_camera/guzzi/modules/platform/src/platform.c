/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file platform.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <platform/inc/platform_all.h>

#include <hal/hat_types.h>

#include <utils/mms_debug.h>
mmsdbg_define_variable(
        test_platform,
        DL_DEFAULT,
        0,
        "test_platform",
        "Camera module test application."
    );
#define MMSDEBUGLEVEL mmsdbg_use_variable(test_platform)

void dbg_break(void)
{

}

int board_platform_init(plat_socket_obj_t **sockets);
int board_platform_deinit(plat_socket_obj_t *sockets);


plat_socket_obj_t *plat_sockets;

int guzzi_platform_init(void)
{
    int err;

    err = board_platform_init(&plat_sockets);

    return err;
}



int guzzi_platform_deinit(void)
{
    int err;

    err = board_platform_deinit(plat_sockets);

    return err;
}

