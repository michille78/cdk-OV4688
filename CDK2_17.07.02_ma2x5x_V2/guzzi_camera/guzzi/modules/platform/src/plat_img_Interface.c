/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file plat_img_Interface.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <platform.h>
#include <hal/hat_sensor_img_interface.h>



static inline int ccp_cfg(hat_img_if_client_t h, hat_img_if_physical_cfg_t* cfg)
{
    PLAT_FXN_ENTER();
    return 0;
}
static inline int ccp_ctx_cfg(hat_img_if_client_t h, uint32 ctx, hat_img_if_context_cfg_t* cfg)
{
    PLAT_FXN_ENTER();
    return 0;
}

static inline int ccp_ctx_start(hat_img_if_client_t h, uint32 ctx, void* buff1, void* buff2)
{
    PLAT_FXN_ENTER();
    return 0;
}

static inline int ccp_ctx_imm_stop(hat_img_if_client_t h, uint32 ctx, void** buff1, void** buff2)
{
    PLAT_FXN_ENTER();
    return 0;
}

static inline int ccp_ctx_set_buffer(hat_img_if_client_t h, uint32 ctx, void* new_buff)
{
    PLAT_FXN_ENTER();
    return 0;
}

static inline int ccp_ctx_enable_event(hat_img_if_client_t h, uint32 ctx, int32 notificationLine)
{
    PLAT_FXN_ENTER();
    return 0;
}

static inline int ccp_ctx_disable_event(hat_img_if_client_t h, uint32 ctx, int32 notificationLine)
{
    PLAT_FXN_ENTER();
    return 0;
}

hat_img_if_object_t if_ccp_1 = {
    NULL,                   //hat_img_if_client_t             client_handle;

    ccp_cfg,               // hat_img_if_cfg_t                interface_cfg;
    ccp_ctx_cfg,           // hat_img_if_cfg_ctx_t            ctx_cfg;
    ccp_ctx_start,         // hat_img_if_start_ctx_t          ctx_start;
    ccp_ctx_imm_stop,      // hat_img_if_immediate_stop_ctx_t ctx_imm_stop;
    ccp_ctx_set_buffer,    // hat_img_if_set_buff_t           ctx_set_buffer;
    ccp_ctx_enable_event,  // hat_img_if_enbable_evtent_t     ctx_enable_event;
    ccp_ctx_disable_event  // hat_img_if_disable_event_t      ctx_disable_event;
};


struct ccp_obj {
   const char *name;
   const int num_ctx;
};

struct ccp_obj ccp_1_obj = {
        "CCP 1 Object",
        1
};



static inline int csi_cfg(hat_img_if_client_t h, hat_img_if_physical_cfg_t* cfg)
{
    PLAT_FXN_ENTER();
    return 0;
}
static inline int csi_ctx_cfg(hat_img_if_client_t h, uint32 ctx, hat_img_if_context_cfg_t* cfg)
{
    PLAT_FXN_ENTER();
    return 0;
}

static inline int csi_ctx_start(hat_img_if_client_t h, uint32 ctx, void* buff1, void* buff2)
{
    PLAT_FXN_ENTER();
    return 0;
}

static inline int csi_ctx_imm_stop(hat_img_if_client_t h, uint32 ctx, void** buff1, void** buff2)
{
    PLAT_FXN_ENTER();
    return 0;
}

static inline int csi_ctx_set_buffer(hat_img_if_client_t h, uint32 ctx, void* new_buff)
{
    PLAT_FXN_ENTER();
    return 0;
}

static inline int csi_ctx_enable_event(hat_img_if_client_t h, uint32 ctx, int32 notificationLine)
{
    PLAT_FXN_ENTER();
    return 0;
}

static inline int csi_ctx_disable_event(hat_img_if_client_t h, uint32 ctx, int32 notificationLine)
{
    PLAT_FXN_ENTER();
    return 0;
}

hat_img_if_object_t if_csi_1 = {
    NULL,                   //hat_img_if_client_t             client_handle;

    csi_cfg,               // hat_img_if_cfg_t                interface_cfg;
    csi_ctx_cfg,           // hat_img_if_cfg_ctx_t            ctx_cfg;
    csi_ctx_start,         // hat_img_if_start_ctx_t          ctx_start;
    csi_ctx_imm_stop,      // hat_img_if_immediate_stop_ctx_t ctx_imm_stop;
    csi_ctx_set_buffer,    // hat_img_if_set_buff_t           ctx_set_buffer;
    csi_ctx_enable_event,  // hat_img_if_enbable_evtent_t     ctx_enable_event;
    csi_ctx_disable_event  // hat_img_if_disable_event_t      ctx_disable_event;
};

struct csi_obj {
   const char *name;
   const int num_ctx;
};

struct csi_obj csi_1_obj = {
        "CSI 1 Object",
        2
};

int hat_img_if_create(hat_img_if_hndl_t *h, hat_img_if_create_params_t cfg)
{
    PLAT_FXN_ENTER();

    if (cfg.img_if_identity == 0x00010001)
    {
        if_csi_1.client_handle = (hat_img_if_client_t)&ccp_1_obj;
        *h = &if_csi_1;
    } else {
        if_ccp_1.client_handle = (hat_img_if_client_t)&ccp_1_obj;
        *h = &if_ccp_1;
    }

    PLAT_FXN_EXIT();
    return 0;
}

int hat_img_if_destroy(hat_img_if_hndl_t h)
{
    PLAT_FXN_ENTER();

    if (h->client_handle != NULL) {
        h->client_handle = NULL;
    }
    PLAT_FXN_EXIT();
    return 0;
}

