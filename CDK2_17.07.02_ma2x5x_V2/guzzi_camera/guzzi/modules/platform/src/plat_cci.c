/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file plat_cci.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <hal/hat_sen_socket.h>
#include <hal/hat_i2c_resource.h>
#include <hal/hat_gpio_resource.h>


typedef enum
{
    CCI_TYPE_NONE = 0,

    CCI_TYPE_I2C,

    CCI_TYPE_MAX
}cci_types_t;

typedef enum
{
    CCI_CH_NONE = 0,

    CCI_CH_1,
    CCI_CH_2,
    CCI_CH_3,
    CCI_CH_4,

    MAX_CCI_CH
}cci_channels_t;

typedef struct
{
    cci_channels_t virtual_ch;
    cci_types_t    type;
    uint32         physical_ch;
}cci_link_t;



typedef enum
{
    IMG_IFACE_TYPE_NONE = 0,

    IMG_IFACE_TYPE_CSI,
    IMG_IFACE_TYPE_CCP,
    IMG_IFACE_TYPE_CCDC,

    MAX_IMG_IFACE_TYPE
} img_iface_type_t;

typedef enum
{
    IMG_IFACE_NONE = 0,

    IMG_IFACE_1,
    IMG_IFACE_2,
    IMG_IFACE_3,
    IMG_IFACE_4,

    MAX_IMG_IFACE
} img_ifaces_t;

typedef struct
{
    img_ifaces_t     virtual_ch;
    img_iface_type_t type;
    uint32           physical_ch;
}img_iface_link_t;


typedef struct {
    // socket descr control
    img_iface_link_t  iface;
    cci_link_t        cci;
} plat_sensor_decription;

#define PLAT_MAX_SENSORS 4

plat_sensor_decription sensors [PLAT_MAX_SENSORS] = {
        {
                {IMG_IFACE_1, IMG_IFACE_TYPE_CSI, 1},
                {CCI_CH_2,    CCI_TYPE_I2C,       2}
        }, // sensor 0
//---------------------------------------------------
        {
                {IMG_IFACE_2, IMG_IFACE_TYPE_CSI, 2},
                {CCI_CH_2,    CCI_TYPE_I2C,       2}
        }, // sensor 0
//---------------------------------------------------
        {
                {IMG_IFACE_NONE, IMG_IFACE_TYPE_NONE, 0},
                {CCI_CH_NONE,    CCI_TYPE_NONE,       0}
        }, // sensor 2
//---------------------------------------------------
        {
                {IMG_IFACE_4, IMG_IFACE_TYPE_CSI, 0},
                {CCI_CH_2,    CCI_TYPE_I2C,       0}
        }  // sensor 3
};





