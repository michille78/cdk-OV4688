/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file plat_i2c.h
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#ifndef __PLATFORM_I2C_H__
#define __PLATFORM_I2C_H__

#include <hal/hat_types.h>

// I2C subsystem manipulation APIs

/**
 * Generic opaque handle representing I2C sub-system.
 * It is returned by driver on hat_i2c_create.
 *
 * @see plat_i2c_create
 */
typedef void* plat_i2c_hndl_t;

/**
 * Create parameters for I2C sub-system.
 *
 * @param num_channels number of I2C channels
 *
 * @see plat_i2c_create
 */
typedef struct {
    int num_channels;    // max number of I2C channel instances
}plat_i2c_create_params_t;

/**
 * Create I2C sub-system
 *
 * @param[out] ih returns handle representing I2C sub-system - NULL on error
 * @param prms Create parameters for I2C sub-system.
 * @return 0 on success
 */
int plat_i2c_create(plat_i2c_hndl_t* ih, plat_i2c_create_params_t prms);
/**
 * Destroys I2C sub-system.
 *
 * @param ih[out] I2C sub-system handle - NULL on error
 * @return 0 on success
 */
int plat_i2c_destroy(plat_i2c_hndl_t ih);

#endif // __PLATFORM_I2C_H__
