/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file plat_i2c_bus.h
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#ifndef __PLATFORM_I2C_BUS_H__
#define __PLATFORM_I2C_BUS_H__

#include <hal/hat_types.h>

/**
 * Generic opaque handle representing I2C bus (channel).
 * It is returned by driver on plat_bus_i2c_create.
 *
 * @see plat_i2c_bus_create
 */
typedef void* plat_i2c_bus_hndl_t;

/**
 * Create parameters for I2C bus (channel).
 *
 * @param channel_idx    channel index
 * @param channel_speed  channel speed [Hz]
 * *
 * @see plat_i2c_bus_create
 */
typedef struct {
    int channel_idx;    // channel index indicating I2C channel instance
    int channel_speed;  // I2C instance clock speed [Hz]
} plat_i2c_bus_create_params_t;

/**
 * Creates I2C bus.
 *
 * @param ih I2C sub-system handle
 * @param bh[out] I2C bus handle - NULL on error
 * @param prms Create parameters for I2C bus (channel).
 * @return 0 on success
 */
int plat_i2c_bus_create(plat_i2c_hndl_t ih, plat_i2c_bus_hndl_t *bh, plat_i2c_bus_create_params_t prms);

/**
 *  Destroys I2C bus.
 *
 * @param bh I2C bus handle
 * @return 0 on success
 */
int plat_i2c_bus_destroy(plat_i2c_bus_hndl_t bh);

//------------  I2C bus (channel) manipulation ---------------

/**
 * I2C Bus callback invoked after each transaction
 *
 * @param reg_addr Transaction register address
 * @param num      number of bytes processed
 * @param data     pointer to processed data
 */
typedef void (*plat_bus_callback_t) (uint32 reg_addr, uint32 num, void* data);

/**
 *  Acquire I2C bus.
 *
 * @param bh I2C bus handle
 * @return 0 on success
 */
int plat_bus_i2c_acquire(plat_i2c_bus_hndl_t bh);

/**
 *  Release I2C bus.
 *
 * @param bh I2C bus handle
 * @return 0 on success
 */
int plat_bus_i2c_release(plat_i2c_bus_hndl_t bh);


/**
 * I2C Bus register parameters
 *
 * @param dev_address     device I2C address
 * @param dev_address_len I2C address length in Bits
 */
typedef struct {
    uint32  dev_address;        // device address
    uint8   dev_address_len;    // [Bits]
} plat_i2c_dev_register_param_t;

/**
 * Generic opaque handle - represents couple bus/device
 * It is returned by driver on plat_bus_i2c_register_dev
 *
 * @see plat_bus_i2c_register_dev
 */
typedef void* plat_i2c_bus_dev_hndl_t;

/**
 *  Registers new device on I2C bus
 *
 * @param bh bus handle
 * @param[out] bdh bus device handle
 * @param param device parameters
 * @param callback pointer to callback if read/write are asynchronous
 *                 NULL if read/write should be blocking
 * @return
 */
int plat_bus_i2c_register_dev (plat_i2c_bus_hndl_t bh,
                                  plat_i2c_bus_dev_hndl_t *bdh,
                                  plat_i2c_dev_register_param_t param,
                                  plat_bus_callback_t callback);

/**
 * Bus/device write byte(s) starting at register address
 *
 * @param bdh bdh  bus device handle
 * @param reg_addr register address
 * @param num      number of bytes to be written
 * @param data     pointer to data to be written
 * @return 0 on success
 *
 * @pre This API could be executed in blocking or non blocking mode
 * @see plat_bus_i2c_register_dev
 */
int plat_bus_i2c_write(plat_i2c_bus_dev_hndl_t bdh, uint32 reg_addr, uint32 num, const void* data);

/**
 * Bus/device read byte(s) starting from register address
 *
 * @param bdh bdh  bus device handle
 * @param reg_addr register address
 * @param num      number of bytes to be read
 * @param data     pointer to store read data
 * @return 0 on success
 *
 * @pre This API could be executed in blocking or non blocking mode
 * @see plat_bus_i2c_register_dev
 */
int plat_bus_i2c_read(plat_i2c_bus_dev_hndl_t bdh, uint32 reg_addr, uint32 num, void* data);

/**
 * Unregister device
 *
 * @param bdh bus device handle
 * @return 0 on success
 */
int plat_bus_i2c_unregister_dev(plat_i2c_bus_dev_hndl_t bdh);

#endif // __PLATFORM_I2C_BUS_H__
