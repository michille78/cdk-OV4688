/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file plat_i2c_device.h
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#ifndef __PLATFORM_I2C_DEVICE_H__
#define __PLATFORM_I2C_DEVICE_H__

#include <hal/hat_types.h>

// ----------- I2C Device manipulation  -------------------------
/**
 * Generic opaque driver handle representing device
 * It is returned by driver on plat_bus_i2c_dev_create
 *
 * @see plat_bus_i2c_dev_create
 */
typedef void* plat_i2c_dev_hndl_t;

/**
 * Generic opaque handle - client provides for each I2C device
 * The driver will return it back to I2C client with callback
 * when transaction finish.
 *
 * @see plat_dev_i2c_open
 */
typedef void* plat_i2c_client_hndl_t;

/**
 * Generic opaque handle - client provides for each I2C transaction
 * The driver will return it back to I2C client with callback
 * when transaction finish.
 *
 * @see plat_dev_i2c_write
 * @see plat_dev_i2c_read
 * @see plat_dev_callback_t
 */
typedef void* plat_i2c_transaction_hndl_t;

/**
 * I2C Device create parameters
 *
 * @param dev_address     device I2C address
 * @param dev_address_len I2C address length in Bits
 */
typedef struct {
    uint32  dev_address;        // device address
    uint8   dev_address_len;    // [Bits]
} plat_i2c_dev_create_param_t;

/**
 * Creates a new device on I2C Bus
 *
 * @param bh Bus handle
 * @param h[out] Device handle
 * @param param  create parameters
 * @return 0 on success
 */
int plat_dev_i2c_dev_create(plat_i2c_bus_hndl_t  bh, plat_i2c_dev_hndl_t* h, plat_i2c_dev_create_param_t param);

/**
 * Destroy I2C device
 *
 * @param h Device handle
 * @return 0 on success
 */
int plat_dev_i2c_dev_destroy(plat_i2c_dev_hndl_t h);
/**
 * Device callback - invoked at the end of transaction (read or write)
 * or on error
 *
 * @param h Device handle
 * @param status transaction status. 0 for success
 * @param client_prv client private data
 * @param trans_prv  transaction private data
 */
typedef void (*plat_dev_callback_t)(plat_i2c_dev_hndl_t h, uint32 status, plat_i2c_client_hndl_t client_prv, plat_i2c_transaction_hndl_t trans_prv);

/**
 * Opens an I2C device created with plat_dev_i2c_dev_create
 *
 * @param h Device handle
 * @param callback Pointer to callback API
 * @param client_prv client private data
 * @return 0 on success
 *
 * @see plat_dev_callback
 * @see plat_dev_i2c_dev_create
 */
int plat_dev_i2c_open (plat_i2c_dev_hndl_t* h, plat_dev_callback_t callback, plat_i2c_client_hndl_t client_prv);     // return 0 for success

/**
 * Writes num bytes pointed by data on I2C Device
 *
 * @param h Device handle
 * @param reg_addr register start address where data should be written
 * @param num Number of bytes that should be written
 * @param data Pionter to data that should be written
 * @param trans_prv Client private data specific to this transaction
 * @return 0 on success
 */
int plat_dev_i2c_write(plat_i2c_dev_hndl_t  h, uint32 reg_addr, uint32 num, const void* data, plat_i2c_transaction_hndl_t trans_prv);

/**
 * Reads num bytes from I2C Device
 *
 * @param h Device handle
 * @param reg_addr register start address to read data from
 * @param num Number of bytes to read
 * @param data Pionter to data where to store read data
 * @param trans_prv Client private data specific to this transaction
 * @return 0 on success
 */

int plat_dev_i2c_read(plat_i2c_dev_hndl_t  h, uint32 reg_addr, uint32 num, void* data, plat_i2c_transaction_hndl_t trans_prv);

/**
 * Close I2C Device
 *
 * @param h Device handle
 * @return 0 on success
 */
int plat_dev_i2c_close(plat_i2c_dev_hndl_t h);

#endif // __PLATFORM_I2C_DEVICE_H__
