/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file camera_config_plug.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <osal/osal_string.h>
#include <osal/osal_stdlib.h>
#include <utils/mms_debug.h>
#include <configurator/include/configurator.h>

#include <guzzi/camera_external/struct_helpers.h>
#include <guzzi/camera3/metadata.h>
#include <guzzi/camera3/metadata_enum.h>

#include <camera_config_plug.h>
#include <camera_config_external.h>
#include <camera_scene_effect_cfg.h>

#define ADDR_INC(ADDR,INC) ((void *)((char *)(ADDR) + (INC)))

mmsdbg_define_variable(
        vdl_camera_config_plug,
        DL_DEFAULT,
        0,
        "camera.config.plug",
        "Camera's configuration manager plugin"
    );
#define MMSDEBUGLEVEL mmsdbg_use_variable(vdl_camera_config_plug)

/* ========================================================================== */
/**
* camera_config_plug_create_scene_cfg_data()
*/
/* ========================================================================== */
static void *camera_config_plug_create_scene_cfg_data(void)
{
    scene_cfg_data_t *scene_data;


    scene_data = osal_calloc(1, sizeof (*scene_data));

    if (!scene_data) {
        mmsdbg(
                DL_ERROR,
                "Failed to allocate memory for scene private data: size=%d!",
                sizeof (*scene_data)
            );
        goto exit1;
    }

    if (dtpsrv_get_hndl(dtp_srv_hndl,
                        DTP_DB_ID_SYS,
                        DTP_ID_SYS_SCENE,
                        1,
                        dtp_param_order,
                        &scene_data->hdtp, &scene_data->leaf))
    {
        mmsdbg(DL_ERROR, "Scene data - can't get DTP client handle");
        osal_free(scene_data);
    }

exit1:
    return scene_data;
}

/* ========================================================================== */
/**
* camera_config_plug_destroy_scene_cfg_data()
*/
/* ========================================================================== */
void camera_config_plug_destroy_scene_cfg_data(void *scene_cfg_data)
{
    scene_cfg_data_t * scene_data = scene_cfg_data;

    if (scene_data->hdtp)
        dtpsrv_free_hndl(scene_data->hdtp);
    else
        mmsdbg(DL_ERROR, "NULL DTP handle");

    osal_free(scene_data);

    return;
}


/* ========================================================================== */
/**
* camera_config_plug_create_effect_cfg_data()
*/
/* ========================================================================== */
static void *camera_config_plug_create_effect_cfg_data(void)
{
    effect_cfg_data_t *data;


    data = osal_calloc(1, sizeof (*data));

    if (!data) {
        mmsdbg(
                DL_ERROR,
                "Failed to allocate memory for effect private data: size=%d!",
                sizeof (*data)
            );
        goto exit1;
    }

    if (dtpsrv_get_hndl(dtp_srv_hndl,
                        DTP_DB_ID_SYS,
                        DTP_ID_SYS_EFFECTS,
                        1,
                        dtp_param_order,
                        &data->hdtp, &data->leaf))
    {
        mmsdbg(DL_ERROR, "Effect data - can't get DTP client handle");
        osal_free(data);
    }

exit1:
    return data;
}

/* ========================================================================== */
/**
* camera_config_plug_destroy_effect_cfg_data()
*/
/* ========================================================================== */
void camera_config_plug_destroy_effect_cfg_data(void *data)
{
    effect_cfg_data_t* _data = data;

    if (_data->hdtp)
        dtpsrv_free_hndl(_data->hdtp);
    else
        mmsdbg(DL_ERROR, "NULL DTP handle");

    osal_free(_data);

    return;
}
/* ========================================================================== */
/**
* set_default_values()
*/
/* ========================================================================== */
static int set_default_values(
        configurator_t *c,
        void *plugin_prv,
        camera_config_plug_cfg_t *cam_cfg
    )
{
    cam_gzz_cfg_t *internal;
    camera_config_external_t *external;
    camera_external_struct_helper_t *helper;
    int i;
    int err;

    internal = &cam_cfg->cam_gzz_cfg;
    external = &cam_cfg->external;

    internal->frame_number.val = 0;

    for (i = 0; i < GZZ_CAM_CONFIG_MAX_STREAMS; i++) {
        internal->stream.val[i].format.fmt.order.all = HAT_PORDBYR_Gr_R_B_Gb;
        internal->stream.val[i].format.fmt.reserved0 = 0;
        internal->stream.val[i].format.fmt.xydec     = HAT_XYDEC_NO;
        internal->stream.val[i].format.fmt.uvpkd     = HAT_UVPKD_PACKED;
        internal->stream.val[i].format.fmt.pkd       = HAT_PIXPKD_PACKED;
        internal->stream.val[i].format.fmt.fmt       = HAT_PIXCOL_BAYER;
        internal->stream.val[i].format.bpc           = 10;
        internal->stream.val[i].format.compression   = HAT_COMPRESSION_NONE;
        internal->stream.val[i].size.w = 856/2;
        internal->stream.val[i].size.h = 480/2;
        internal->stream.val[i].type = 0;
        internal->stream.val[i].id = i+1;

        internal->stream_cntrl.val[i] = CAM_STREAM_OFF;
    }

    internal->fr_duration.val      = 66000;
    internal->fps_range.val.min    = 10.0;
    internal->fps_range.val.max    = 15.0;

    internal->rgb2rgb_mode.val = CAM_RGB2RGB_AUTO;
//    internal->manual_rgb2rgb.val. = ;
    internal->gamma_mode.val   = CAM_GAMMA_AUTO;
//    internal->manual_gamma.val = ;
    internal->lsc_mode.val     = CAM_LSC_AUTO;
//    internal->lsc_tbl.val = ;
//    internal->lsc_strength.val = ;
    internal->ee_type.val      = CAM_EDGE_AUTO;
//    internal->ee_strength.val = ;
    internal->cfai_mode.val = CAM_CFAI_NORMAL;
    internal->ldc_mode.val  = CAM_LDC_MODE_OFF;
//    internal->ldc_strength.val = ;
    internal->dpc_mode.val = CAM_DPC_MODE_ON;
    internal->bl_lock.val      = CAM_BLACK_LEVEL_LOCK_OFF;
    internal->nf_mode.val      = CAM_NF_MODE_ON;
//    internal->nf_strength.val = ;
    internal->flicker_mode.val = CAM_FLICKER_MODE_AUTO;
    internal->flash_mode.val   = CAM_FLASH_AUTO;
//    internal->flash_pwr.val = ;
//    internal->flash_duration.val = ;
//    internal->led_ctrl;
    internal->aca_mode.val     = CAM_ACA_MODE_AUTO;
    internal->exp_compensation.val = 0.0;
//    internal->manual_exposure.val = ;
    internal->ae_mode.val      = CAM_AE_MODE_ON_AUTO_FLASH;

    internal->ae_lock.val      = CAM_ALG_LOCK_OFF;
    internal->ae_regions.val.reg_num = 0;
//    internal->pre_capture;
//    internal->wb_gains.val = ;
    internal->wb_mode.val      = AWB_MODE_AUTO;
    internal->wb_lock.val      = CAM_ALG_LOCK_OFF;
//    internal->wb_regions.val = ;
    internal->af_range.val     = AF_RANGE_NORMAL;
    internal->af_mode.val      = AF_MODE_SINGLE_FOCUS;
//    internal->af_weght.val = ;
//    internal->af_regions.val = ;
    internal->af_trigger.val   = CAM_AF_TRIGGER_IDLE;
    internal->eff_type.val     = CAM_EFFECT_NONE;
    internal->scene_mode.val   = CAM_SCENE_NONE;
    internal->contrast.val     = 0.0;
    internal->saturation.val   = 0.0;
    internal->sharpness.val    = 0.0;
    internal->brightness.val   = 0.0;
    internal->manual_color_temp.val = 5800;
    internal->vstab_mode.val   = CAM_VIDEO_STABILIZATION_MODE_OFF;
    internal->fd_mode.val      = CAM_FACE_DETECT_MODE_OFF;
    internal->cam_mode.val     = CAM_CAPTURE_INTENT_PREVIEW;
    internal->optical_stab_mode.val = CAM_LENS_OPTICAL_STABILIZATION_MODE_OFF;
    internal->aperture.val = 0.0;

    internal->meta_mode.val            = CAM_METADATA_MODE_NONE;

    internal->stats_enable.val.af_stat = CAM_STAT_MODE_OFF;
    internal->stats_enable.val.hist    = CAM_STAT_MODE_OFF;
    internal->stats_enable.val.lsc     = CAM_STAT_MODE_OFF;

    internal->exposure_merger.val      = 0;

    internal->white_balance_merger.val = 0;

    internal->custom_usecase_selection.val = CAM_CUSTOM_USECASE_SELECTION_STILL;
    internal->custom_capture_mode_selection.val = CAM_CUSTOM_CAPTURE_MODE_SELECTION_TEMPORAL_BRACKETING;

    for (i = 0; i < GZZ_CAM_EXP_BRACKETING_SEQUENCE_LENGHT; i++) {
        internal->custom_exposure_bracketing_sequence.val[i] = 0;
    }
    internal->custom_capture_number_shots.val = 0;

    internal->hue.val = 0;

    /* TODO: read defaults from DTP in to cam_cfg->external */
    external->control_mode.v = GUZZI_CAMERA3_ENUM_CONTROL_MODE_AUTO;
    external->control_capture_intent.v = GUZZI_CAMERA3_ENUM_CONTROL_CAPTURE_INTENT_PREVIEW;

    external->control_awb_mode.v = GUZZI_CAMERA3_ENUM_CONTROL_AWB_MODE_AUTO;
    external->control_awb_lock.v = GUZZI_CAMERA3_ENUM_CONTROL_AWB_LOCK_OFF;

    external->control_af_trigger.v = GUZZI_CAMERA3_ENUM_CONTROL_AF_TRIGGER_IDLE;
    external->control_af_mode.v = GUZZI_CAMERA3_ENUM_CONTROL_AF_MODE_AUTO;

    external->sensor_sensitivity.v = 100; //iso ??
    external->sensor_exposure_time.v = 10000000; //ns
    external->sensor_frame_duration.v = 100000000; //ns

    external->control_ae_mode.v = GUZZI_CAMERA3_ENUM_CONTROL_AE_MODE_ON_AUTO_FLASH;
    external->control_ae_target_fps_range.dim_size_1 = 2;
    external->control_ae_target_fps_range.v[0] = 10.0;
    external->control_ae_target_fps_range.v[1] = 15.0;
    external->control_ae_lock.v = GUZZI_CAMERA3_ENUM_CONTROL_AE_LOCK_OFF;
    external->control_ae_exposure_compensation.v = 0;
    external->control_ae_antibanding_mode.v = GUZZI_CAMERA3_ENUM_CONTROL_AE_ANTIBANDING_MODE_50HZ;
    external->statistics_histogram_mode.v = GUZZI_CAMERA3_ENUM_STATISTICS_HISTOGRAM_MODE_OFF;
    external->statistics_sharpness_map_mode.v = GUZZI_CAMERA3_ENUM_STATISTICS_SHARPNESS_MAP_MODE_OFF;
    external->statistics_lens_shading_map_mode.v = GUZZI_CAMERA3_ENUM_STATISTICS_LENS_SHADING_MAP_MODE_OFF;

    external->flash_mode.v = GUZZI_CAMERA3_ENUM_FLASH_MODE_SINGLE;
    external->flash_firing_time.v = 1000;
    external->flash_firing_power.v = 5;

    {
        guzzi_camera3_controls_tonemap_curve_blue_t gamma =  {
            .dim_size_1 = 2,
            .dim_size_2 = 5,
            .v = {
                0.0,    0.0,
                0.25,   0.3,
                0.5,    0.6,
                0.75,   0.85,
                1.0,    1.0,
            },
        };
        osal_memcpy(&external->tonemap_curve_blue, &gamma, sizeof(gamma));
        osal_memcpy(&external->tonemap_curve_green, &gamma, sizeof(gamma));
        osal_memcpy(&external->tonemap_curve_red, &gamma, sizeof(gamma));
    }
    external->tonemap_mode.v = GUZZI_CAMERA3_ENUM_TONEMAP_MODE_HIGH_QUALITY;

    {
        guzzi_camera3_controls_color_correction_transform_t color_trasform = {
            .dim_size_1 = GUZZI_CAMERA3_CONTROLS_COLOR_CORRECTION_TRANSFORM_DIM_MAX_SIZE_1,
            .dim_size_2 = GUZZI_CAMERA3_CONTROLS_COLOR_CORRECTION_TRANSFORM_DIM_MAX_SIZE_2,
            .v = {
                    {.nom = 1, .denom = 1}, {.nom = 0, .denom = 1}, {.nom = 0, .denom = 1},
                    {.nom = 0, .denom = 1}, {.nom = 1, .denom = 1}, {.nom = 0, .denom = 1},
                    {.nom = 0, .denom = 1}, {.nom = 0, .denom = 1}, {.nom = 1, .denom = 1}
            } /* guzzi_camera3_rational_t */
        };
        external->color_correction_transform = color_trasform;

        const guzzi_camera3_controls_color_correction_gains_t gains = {
            .dim_size_1 = 4,
            .v = {
                1.70, 1.0, 1.0, 1.50, // r, gr, gb, b
            },
        };
        external->color_correction_gains = gains;

        external->color_correction_mode.v = GUZZI_CAMERA3_ENUM_COLOR_CORRECTION_MODE_HIGH_QUALITY;
    }

    external->shading_mode.v = GUZZI_CAMERA3_ENUM_SHADING_MODE_HIGH_QUALITY;
    external->hot_pixel_mode.v = GUZZI_CAMERA3_ENUM_HOT_PIXEL_MODE_HIGH_QUALITY;
    external->demosaic_mode.v = GUZZI_CAMERA3_ENUM_DEMOSAIC_MODE_HIGH_QUALITY;
    external->noise_reduction_mode.v = GUZZI_CAMERA3_ENUM_NOISE_REDUCTION_MODE_HIGH_QUALITY;
    external->edge_mode.v = GUZZI_CAMERA3_ENUM_EDGE_MODE_HIGH_QUALITY;

    external->scaler_crop_region.v[0] = 0;
    external->scaler_crop_region.v[1] = 0;
    external->scaler_crop_region.v[2] = 856/2;
    external->scaler_crop_region.v[3] = 480/2;

    external->lens_aperture.v = 0.0;
    external->lens_optical_stabilization_mode.v = GUZZI_CAMERA3_ENUM_LENS_OPTICAL_STABILIZATION_MODE_OFF;

    external->z_custom_usecase_selection.v = GUZZI_CAMERA3_ENUM_Z_CUSTOM_USECASE_SELECTION_STILL;
    external->z_custom_capture_mode_selection.v = GUZZI_CAMERA3_ENUM_Z_CUSTOM_CAPTURE_MODE_SELECTION_TEMPORAL_BRACKETING;

    external->z_custom_exposure_bracketing_sequence.dim_size_1 = GUZZI_CAMERA3_CONTROLS_Z_CUSTOM_EXPOSURE_BRACKETING_SEQUENCE_SIZE;
    for (i = 0; i < GZZ_CAM_EXP_BRACKETING_SEQUENCE_LENGHT; i++) {
        external->z_custom_exposure_bracketing_sequence.v[i] = 0;
    }
    external->z_custom_capture_number_shots.v = 0;

    external->z_exposure_merger_weight.v = 0;
    external->z_white_balance_merger_weight.v = 0;
    external->z_wb_manual_temperature.v = 5800;
    external->z_custom_crtl_hue.v = 0;

    for_each_ext_helper(helper) {
        err = helper->convert(c, plugin_prv, cam_cfg);
        if (err) {
            mmsdbg(
                    DL_ERROR,
                    "Failed to convert default settings (for index=%s)!",
                    helper->index_name
                );
            goto exit1;
        }
    }

    return 0;
exit1:
    return -1;
}

/* ========================================================================== */
/**
* init_struct()
*/
/* ========================================================================== */
static void init_struct(camera_config_plug_cfg_t *cam_cfg)
{
#define INIT_CTRL(PARENT, CHILD) \
    CHILD.ctrl.seq_num = 1; \
    CHILD.ctrl.parrent_offset = (uint8 *)(&(CHILD)) - (uint8 *)(&(PARENT))

#define INIT_CFGR_ENTRY(PARENT, CHILD) \
    INIT_CTRL(cam_cfg->PARENT, cam_cfg->PARENT.CHILD)

    cam_cfg->ctrl.seq_num = 1;
    cam_cfg->ctrl.parrent_offset = CONFIGURATOR_CONFIG_STRUCT_ROOT_NODE;

    /* Internal */
    INIT_CTRL(*cam_cfg, cam_cfg->cam_gzz_cfg);

    INIT_CFGR_ENTRY(cam_gzz_cfg, frame_number);
    INIT_CFGR_ENTRY(cam_gzz_cfg, stream);
    INIT_CFGR_ENTRY(cam_gzz_cfg, stream_cntrl);
    INIT_CFGR_ENTRY(cam_gzz_cfg, fr_duration);
    INIT_CFGR_ENTRY(cam_gzz_cfg, fps_range);
    INIT_CFGR_ENTRY(cam_gzz_cfg, sensor_crop);
    INIT_CFGR_ENTRY(cam_gzz_cfg, rgb2rgb_mode);
    INIT_CFGR_ENTRY(cam_gzz_cfg, manual_rgb2rgb);
    INIT_CFGR_ENTRY(cam_gzz_cfg, gamma_mode);
    INIT_CFGR_ENTRY(cam_gzz_cfg, manual_gamma);
    INIT_CFGR_ENTRY(cam_gzz_cfg, lsc_mode);
    INIT_CFGR_ENTRY(cam_gzz_cfg, lsc_tbl);
    INIT_CFGR_ENTRY(cam_gzz_cfg, lsc_strength);
    INIT_CFGR_ENTRY(cam_gzz_cfg, ee_type);
    INIT_CFGR_ENTRY(cam_gzz_cfg, ee_strength);
    INIT_CFGR_ENTRY(cam_gzz_cfg, cfai_mode);
    INIT_CFGR_ENTRY(cam_gzz_cfg, ldc_mode);
    INIT_CFGR_ENTRY(cam_gzz_cfg, ldc_strength);
    INIT_CFGR_ENTRY(cam_gzz_cfg, dpc_mode);
    INIT_CFGR_ENTRY(cam_gzz_cfg, bl_lock);
    INIT_CFGR_ENTRY(cam_gzz_cfg, nf_mode);
    INIT_CFGR_ENTRY(cam_gzz_cfg, nf_strength);
    INIT_CFGR_ENTRY(cam_gzz_cfg, flicker_mode);
    INIT_CFGR_ENTRY(cam_gzz_cfg, flash_mode);
    INIT_CFGR_ENTRY(cam_gzz_cfg, flash_pwr);
    INIT_CFGR_ENTRY(cam_gzz_cfg, flash_duration);
    INIT_CFGR_ENTRY(cam_gzz_cfg, aca_mode);
    INIT_CFGR_ENTRY(cam_gzz_cfg, exp_compensation);
    INIT_CFGR_ENTRY(cam_gzz_cfg, manual_exposure);
    INIT_CFGR_ENTRY(cam_gzz_cfg, ae_mode);
    INIT_CFGR_ENTRY(cam_gzz_cfg, ae_lock);
    INIT_CFGR_ENTRY(cam_gzz_cfg, ae_regions);
    INIT_CFGR_ENTRY(cam_gzz_cfg, wb_gains);
    INIT_CFGR_ENTRY(cam_gzz_cfg, manual_color_temp);
    INIT_CFGR_ENTRY(cam_gzz_cfg, wb_mode);
    INIT_CFGR_ENTRY(cam_gzz_cfg, wb_lock);
    INIT_CFGR_ENTRY(cam_gzz_cfg, wb_regions);
    INIT_CFGR_ENTRY(cam_gzz_cfg, af_range);
    INIT_CFGR_ENTRY(cam_gzz_cfg, af_mode);
    INIT_CFGR_ENTRY(cam_gzz_cfg, af_weght);
    INIT_CFGR_ENTRY(cam_gzz_cfg, af_regions);
    INIT_CFGR_ENTRY(cam_gzz_cfg, af_trigger);
    INIT_CFGR_ENTRY(cam_gzz_cfg, eff_type);
    INIT_CFGR_ENTRY(cam_gzz_cfg, scene_mode);
    INIT_CFGR_ENTRY(cam_gzz_cfg, contrast);
    INIT_CFGR_ENTRY(cam_gzz_cfg, saturation);
    INIT_CFGR_ENTRY(cam_gzz_cfg, sharpness);
    INIT_CFGR_ENTRY(cam_gzz_cfg, brightness);
    INIT_CFGR_ENTRY(cam_gzz_cfg, vstab_mode);
    INIT_CFGR_ENTRY(cam_gzz_cfg, fd_mode);
    INIT_CFGR_ENTRY(cam_gzz_cfg, cam_mode);
    INIT_CFGR_ENTRY(cam_gzz_cfg, optical_stab_mode);
    INIT_CFGR_ENTRY(cam_gzz_cfg, meta_mode);
    INIT_CFGR_ENTRY(cam_gzz_cfg, stats_enable);
    INIT_CFGR_ENTRY(cam_gzz_cfg, lens_focal_len);
    INIT_CFGR_ENTRY(cam_gzz_cfg, aperture);

    INIT_CFGR_ENTRY(cam_gzz_cfg, custom_usecase_selection);
    INIT_CFGR_ENTRY(cam_gzz_cfg, custom_capture_mode_selection);
    INIT_CFGR_ENTRY(cam_gzz_cfg, custom_exposure_bracketing_sequence);
    INIT_CFGR_ENTRY(cam_gzz_cfg, custom_capture_number_shots);

    INIT_CFGR_ENTRY(cam_gzz_cfg, exposure_merger);
    INIT_CFGR_ENTRY(cam_gzz_cfg, white_balance_merger);

    INIT_CFGR_ENTRY(cam_gzz_cfg, manual_color_temp);
    INIT_CFGR_ENTRY(cam_gzz_cfg, hue);
}

/* ========================================================================== */
/**
* camera_config_plug_get()
*
*/
/* ========================================================================== */
static int camera_config_plug_get(
            configurator_t *c,
            void *plugin_prv,
            void *config,
            unsigned int index,
            void *param
        )
{
    camera_config_plug_cfg_t *cam_cfg;
    camera_external_struct_helper_t *helper;

    cam_cfg = config;

    helper = camera_external_index_to_helper((guzzi_camera3_metadata_index_t)index);
    if (!helper) {
        mmsdbg(
                DL_ERROR,
                "Camera config failed (invalid index=0x%x)!",
                index
            );
        goto exit1;
    }

    osal_memcpy(
            param,
            ADDR_INC(&cam_cfg->external, helper->offset),
            helper->size
        );

    return 0;
exit1:
    return -1;
}

/* ========================================================================== */
/**
* camera_config_plug_set()
*
*/
/* ========================================================================== */
static int camera_config_plug_set(
            configurator_t *c,
            void *plugin_prv,
            void *config,
            unsigned int index,
            void *param
        )
{
    camera_config_plug_cfg_t *cam_cfg;
    camera_external_struct_helper_t *helper;

    cam_cfg = config;

    helper = camera_external_index_to_helper((guzzi_camera3_metadata_index_t)index);
    if (!helper) {
        mmsdbg(
                DL_ERROR,
                "Camera config failed (invalid index=0x%x)!",
                index
            );
        goto exit1;
    }

    osal_memcpy(
            ADDR_INC(&cam_cfg->external, helper->offset),
            param,
            helper->size
        );

    return helper->convert(c, plugin_prv, config);
exit1:
    return -1;
}

/* ========================================================================== */
/**
* camera_config_plug_set_to_default()
*
*/
/* ========================================================================== */
static int camera_config_plug_set_to_default(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    int ret = 0;
    camera_config_plugin_prv_t *config_plugin_prv_p;
    config_plugin_prv_p = (camera_config_plugin_prv_t *)plugin_prv;

    osal_memset(config, 0, sizeof (camera_config_plug_cfg_t));
    init_struct(config);
    config_plugin_prv_p->scene_cfg_data =
            camera_config_plug_create_scene_cfg_data();
    if(!config_plugin_prv_p->scene_cfg_data) {
        ret--;
    }

    config_plugin_prv_p->effect_cfg_data =
            camera_config_plug_create_effect_cfg_data();
    if(!config_plugin_prv_p->effect_cfg_data) {
        ret--;
    }

    ret -= set_default_values(c, plugin_prv, config);

    return ret;
}

configurator_plugin_desc_t camera_config_plug = {
    .config_struct_size = sizeof (camera_config_plug_cfg_t),
    .get = camera_config_plug_get,
    .set = camera_config_plug_set,
    .set_to_default = camera_config_plug_set_to_default
};

