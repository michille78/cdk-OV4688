/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file camera_config_convert.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <osal/osal_stdtypes.h>
#include <utils/mms_debug.h>
#include <configurator/include/configurator.h>

#include <camera_config_external.h>
#include <camera_config_plug.h>
#include <camera_scene_effect_cfg.h>

mmsdbg_define_variable(
        vdl_camera_auto_config_convert,
        DL_DEFAULT,
        0,
        "vdl_camera_auto_config_convert",
        "Camera auto config convert."
    );
#define MMSDEBUGLEVEL mmsdbg_use_variable(vdl_camera_auto_config_convert)

#include <guzzi/camera3/metadata.h>
extern guzzi_camera3_metadata_static_t guzzi_camera3_metadata_static_init;
static float convert_exposure_compensation_to_internal(int32 external)
{
float internal = 0;

    if ((external < guzzi_camera3_metadata_static_init.s.control_ae_compensation_range.v.v[0]) ||
        (external > guzzi_camera3_metadata_static_init.s.control_ae_compensation_range.v.v[1]))
    {
        mmsdbg(DL_ERROR, "Exposure compensation wrong value = %d supported range [%d - %d]",
                external,
                guzzi_camera3_metadata_static_init.s.control_ae_compensation_range.v.v[0],
                guzzi_camera3_metadata_static_init.s.control_ae_compensation_range.v.v[1]);
    } else {
        internal = (float)(external* guzzi_camera3_metadata_static_init.s.control_ae_compensation_step.v.v.nom) /
                           guzzi_camera3_metadata_static_init.s.control_ae_compensation_step.v.v.denom;
    }
    return internal;
}

static int32 convert_exposure_compensation_to_external(float internal)
{
    float external;

    external = (int32)(internal * guzzi_camera3_metadata_static_init.s.control_ae_compensation_step.v.v.denom /
                       guzzi_camera3_metadata_static_init.s.control_ae_compensation_step.v.v.nom);

    return external;
}


int camera_external_convert_black_level_lock(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    // mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_color_correction_gains(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    int ret = 0;
    camera_config_plug_cfg_t *cfg = config;
    hat_wbal_coef_t *manual_wb = &cfg->cam_gzz_cfg.wb_gains.val;

    manual_wb->dgain.gain = 1.0;

    manual_wb->c.r = METAIDX1(&cfg->external.color_correction_gains, 0);
    manual_wb->c.gr= METAIDX1(&cfg->external.color_correction_gains, 1);
    manual_wb->c.gb= METAIDX1(&cfg->external.color_correction_gains, 2);
    manual_wb->c.b = METAIDX1(&cfg->external.color_correction_gains, 3);

    manual_wb->o.r = 0.0;
    manual_wb->o.gr= 0.0;
    manual_wb->o.gb= 0.0;
    manual_wb->o.b = 0.0;

    CONFIGURATOR_CONFIG_TOUCH(cfg->cam_gzz_cfg.wb_gains);
    return ret;
}

int camera_external_convert_color_correction_mode(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    int ret = 0;
    camera_config_plug_cfg_t *cfg = config;

    guzzi_camera3_enum_color_correction_mode_t color_corr_mode =
            (guzzi_camera3_enum_color_correction_mode_t)cfg->external.color_correction_mode.v;

    switch (color_corr_mode) {
    case GUZZI_CAMERA3_ENUM_COLOR_CORRECTION_MODE_TRANSFORM_MATRIX:
        cfg->cam_gzz_cfg.rgb2rgb_mode.val = CAM_RGB2RGB_MANUAL;
        break;
    default:
        mmsdbg(DL_ERROR, "Unsupported mode", __FUNCTION__);
        ret = -1;
        /* no break */
    case GUZZI_CAMERA3_ENUM_COLOR_CORRECTION_MODE_FAST:
    case GUZZI_CAMERA3_ENUM_COLOR_CORRECTION_MODE_HIGH_QUALITY:
        cfg->cam_gzz_cfg.rgb2rgb_mode.val = CAM_RGB2RGB_AUTO;
        break;
    }

    CONFIGURATOR_CONFIG_TOUCH(cfg->cam_gzz_cfg.rgb2rgb_mode);
    return ret;
}

int camera_external_convert_color_correction_transform(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    volatile int ret = 0, i, j;
    guzzi_camera3_rational_t rational;

    camera_config_plug_cfg_t *cfg = config;
    hat_rgb2rgb_t *manual_rgb2rgb = &cfg->cam_gzz_cfg.manual_rgb2rgb.val;

    meta_arr_iterate_dim_2(2, 1, &cfg->external.color_correction_transform, i, j) {
        rational = METAIDX2(&cfg->external.color_correction_transform, i, j);

        manual_rgb2rgb->m.m[i][j] = (float)rational.nom / rational.denom;
    }

    manual_rgb2rgb->o.r = 0.0;
    manual_rgb2rgb->o.g = 0.0;
    manual_rgb2rgb->o.b = 0.0;

    CONFIGURATOR_CONFIG_TOUCH(cfg->cam_gzz_cfg.manual_rgb2rgb);
    return ret;
}

int camera_external_convert_control_ae_antibanding_mode(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    int ret = 0;
    camera_config_plug_cfg_t *cfg = config;

    guzzi_camera3_enum_control_ae_antibanding_mode_t afd_mode =
            (guzzi_camera3_enum_control_ae_antibanding_mode_t)
                cfg->external.control_ae_antibanding_mode.v;

    switch (afd_mode) {
    case GUZZI_CAMERA3_ENUM_CONTROL_AE_ANTIBANDING_MODE_AUTO:
        cfg->cam_gzz_cfg.flicker_mode.val = CAM_FLICKER_MODE_AUTO;
        break;
    case GUZZI_CAMERA3_ENUM_CONTROL_AE_ANTIBANDING_MODE_60HZ:
        cfg->cam_gzz_cfg.flicker_mode.val = CAM_FLICKER_MODE_60HZ;
        break;
    case GUZZI_CAMERA3_ENUM_CONTROL_AE_ANTIBANDING_MODE_50HZ:
        cfg->cam_gzz_cfg.flicker_mode.val = CAM_FLICKER_MODE_50HZ;
        break;
    default:
        mmsdbg(DL_ERROR, "Unsupported mode", __FUNCTION__);
        ret = -1;
        /* no break */
    case GUZZI_CAMERA3_ENUM_CONTROL_AE_ANTIBANDING_MODE_OFF:
        cfg->cam_gzz_cfg.flicker_mode.val = CAM_FLICKER_MODE_OFF;
        break;
    }

    CONFIGURATOR_CONFIG_TOUCH(cfg->cam_gzz_cfg.flicker_mode);
    return ret;
}

int camera_external_convert_control_ae_exposure_compensation(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    int          ret = 0;
    camera_config_plug_cfg_t    *cfg = config;
    float   *ev = &cfg->cam_gzz_cfg.exp_compensation.val;

    *ev = convert_exposure_compensation_to_internal(cfg->external.control_ae_exposure_compensation.v);

    CONFIGURATOR_CONFIG_TOUCH(cfg->cam_gzz_cfg.exp_compensation);
    return ret;
}

int camera_external_convert_control_ae_lock(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    int ret = 0;
    camera_config_plug_cfg_t *cfg = config;

    guzzi_camera3_enum_control_ae_lock_t ae_lock =
            (guzzi_camera3_enum_control_ae_lock_t) cfg->external.control_ae_lock.v;

    switch (ae_lock) {
    case GUZZI_CAMERA3_ENUM_CONTROL_AE_LOCK_ON:
        cfg->cam_gzz_cfg.ae_lock.val = CAM_ALG_LOCK_ON;
        break;
    default:
        mmsdbg(DL_ERROR, "Unsupported mode", __FUNCTION__);
        ret = -1;
        /* no break */
    case GUZZI_CAMERA3_ENUM_CONTROL_AE_LOCK_OFF:
        cfg->cam_gzz_cfg.ae_lock.val = CAM_ALG_LOCK_OFF;
        break;
    }

    CONFIGURATOR_CONFIG_TOUCH(cfg->cam_gzz_cfg.ae_lock);
    return ret;
}

int camera_external_convert_control_ae_mode(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    int ret = 0;
    camera_config_plug_cfg_t *cfg = config;

    guzzi_camera3_enum_control_ae_mode_t ae_mode =
            (guzzi_camera3_enum_control_ae_mode_t) cfg->external.control_ae_mode.v;

    switch (ae_mode) {
    case GUZZI_CAMERA3_ENUM_CONTROL_AE_MODE_OFF:
        cfg->cam_gzz_cfg.ae_mode.val = CAM_AE_MODE_OFF;
        break;
    case GUZZI_CAMERA3_ENUM_CONTROL_AE_MODE_ON_AUTO_FLASH_REDEYE:
        cfg->cam_gzz_cfg.ae_mode.val = CAM_AE_MODE_ON_AUTO_FLASH_REDEYE;
        break;
    case GUZZI_CAMERA3_ENUM_CONTROL_AE_MODE_ON_ALWAYS_FLASH:
        cfg->cam_gzz_cfg.ae_mode.val = CAM_AE_MODE_ON_ALWAYS_FLASH;
        break;
    case GUZZI_CAMERA3_ENUM_CONTROL_AE_MODE_ON_AUTO_FLASH:
        cfg->cam_gzz_cfg.ae_mode.val = CAM_AE_MODE_ON_AUTO_FLASH;
        break;
    default:
        mmsdbg(DL_ERROR, "Unsupported mode", __FUNCTION__);
        ret = -1;
        /* no break */
    case GUZZI_CAMERA3_ENUM_CONTROL_AE_MODE_ON:
        cfg->cam_gzz_cfg.ae_mode.val = CAM_AE_MODE_ON;
        break;
    }

    CONFIGURATOR_CONFIG_TOUCH(cfg->cam_gzz_cfg.ae_mode);
    return ret;
}

int camera_external_convert_control_ae_precapture_trigger(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    // mmsdbg(DL_WARNING, "Function \"%s()\" skipped !", __FUNCTION__);
    return 0;
}

int camera_external_convert_control_ae_regions(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    int             ret = 0, i;
    camera_config_plug_cfg_t *cfg = config;
    hat_reg_pri_t   *ae_region = &cfg->cam_gzz_cfg.ae_regions.val;

    ae_region->priority_max = 0;

    meta_arr_iterate_dim_1(2, &cfg->external.control_ae_regions, i) {
        ae_region->reg_box[i].x = METAIDX2(&cfg->external.control_ae_regions, i, 0);
        ae_region->reg_box[i].y = METAIDX2(&cfg->external.control_ae_regions, i, 1);
        ae_region->reg_box[i].w = METAIDX2(&cfg->external.control_ae_regions, i, 2);
        ae_region->reg_box[i].h = METAIDX2(&cfg->external.control_ae_regions, i, 3);
        ae_region->priority[i]  = METAIDX2(&cfg->external.control_ae_regions, i, 4);

        if (ae_region->priority_max < ae_region->priority[i]) {
            ae_region->priority_max = ae_region->priority[i];
        }
    }

    ae_region->reg_num = i;
    CONFIGURATOR_CONFIG_TOUCH(cfg->cam_gzz_cfg.ae_regions);
    return ret;
}

int camera_external_convert_control_ae_target_fps_range(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    int                 ret = 0;
    camera_config_plug_cfg_t *cfg = config;
    hat_range_float_t   *fps_range = &cfg->cam_gzz_cfg.fps_range.val;

    fps_range->min = METAIDX1(&cfg->external.control_ae_target_fps_range, 0);
    fps_range->max = METAIDX1(&cfg->external.control_ae_target_fps_range, 1);

    CONFIGURATOR_CONFIG_TOUCH(cfg->cam_gzz_cfg.fps_range);
    return ret;

}

int camera_external_convert_control_af_mode(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    int ret = 0;
    camera_config_plug_cfg_t *cfg = config;

    guzzi_camera3_enum_control_af_mode_t foc_mode =
            (guzzi_camera3_enum_control_af_mode_t) cfg->external.control_af_mode.v;

    switch (foc_mode) {
    case GUZZI_CAMERA3_ENUM_CONTROL_AF_MODE_AUTO:
        cfg->cam_gzz_cfg.af_mode.val = AF_MODE_SINGLE_FOCUS;
        cfg->cam_gzz_cfg.af_range.val = AF_RANGE_NORMAL;
        break;
    case GUZZI_CAMERA3_ENUM_CONTROL_AF_MODE_MACRO:
        cfg->cam_gzz_cfg.af_mode.val = AF_MODE_MANUAL;
        cfg->cam_gzz_cfg.af_range.val = AF_RANGE_MACRO;
        break;
    case GUZZI_CAMERA3_ENUM_CONTROL_AF_MODE_CONTINUOUS_VIDEO:
        cfg->cam_gzz_cfg.af_mode.val = AF_MODE_CONTINUOUS;
        cfg->cam_gzz_cfg.af_range.val = AF_RANGE_CUSTOM1;
        break;
    case GUZZI_CAMERA3_ENUM_CONTROL_AF_MODE_CONTINUOUS_PICTURE:
        cfg->cam_gzz_cfg.af_mode.val = AF_MODE_CONTINUOUS;
        cfg->cam_gzz_cfg.af_range.val = AF_RANGE_CUSTOM2;
        break;
    case GUZZI_CAMERA3_ENUM_CONTROL_AF_MODE_EDOF:
        cfg->cam_gzz_cfg.af_mode.val = AF_MODE_SINGLE_FOCUS;
        cfg->cam_gzz_cfg.af_range.val = AF_RANGE_EXTENDED;
        break;
    default:
        mmsdbg(DL_ERROR, "Unsupported mode", __FUNCTION__);
        ret = -1;
        /* no break */
    case GUZZI_CAMERA3_ENUM_CONTROL_AF_MODE_OFF:
        cfg->cam_gzz_cfg.af_mode.val = AF_MODE_OFF;
        break;
    }

    CONFIGURATOR_CONFIG_TOUCH(cfg->cam_gzz_cfg.af_mode);
    CONFIGURATOR_CONFIG_TOUCH(cfg->cam_gzz_cfg.af_range);
    return ret;
}

int camera_external_convert_control_af_regions(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    int             ret = 0, i;
    camera_config_plug_cfg_t *cfg = config;
    hat_reg_pri_t   *af_region = &cfg->cam_gzz_cfg.af_regions.val;

    af_region->priority_max = 0;

    meta_arr_iterate_dim_1(2, &cfg->external.control_af_regions, i) {
        af_region->reg_box[i].x = METAIDX2(&cfg->external.control_af_regions, i, 0);
        af_region->reg_box[i].y = METAIDX2(&cfg->external.control_af_regions, i, 1);
        af_region->reg_box[i].w = METAIDX2(&cfg->external.control_af_regions, i, 2);
        af_region->reg_box[i].h = METAIDX2(&cfg->external.control_af_regions, i, 3);
        af_region->priority[i]  = METAIDX2(&cfg->external.control_af_regions, i, 4);

        if (af_region->priority_max < af_region->priority[i]) {
            af_region->priority_max = af_region->priority[i];
        }
    }

    af_region->reg_num = i;
    CONFIGURATOR_CONFIG_TOUCH(cfg->cam_gzz_cfg.af_regions);
    return ret;
}

int camera_external_convert_control_af_trigger(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    int ret = 0;
    camera_config_plug_cfg_t *cfg = config;

    guzzi_camera3_enum_control_af_trigger_t trigger =
            (guzzi_camera3_enum_control_af_trigger_t)cfg->external.control_af_trigger.v;

    switch (trigger) {
    case GUZZI_CAMERA3_ENUM_CONTROL_AF_TRIGGER_START:
        cfg->cam_gzz_cfg.af_trigger.val = CAM_AF_TRIGGER_START;
        break;
    case GUZZI_CAMERA3_ENUM_CONTROL_AF_TRIGGER_CANCEL:
        cfg->cam_gzz_cfg.af_trigger.val = CAM_AF_TRIGGER_CANCEL;
        break;
    default:
        mmsdbg(DL_ERROR, "Unsupported mode", __FUNCTION__);
        ret = -1;
        /* no break */
    case GUZZI_CAMERA3_ENUM_CONTROL_AF_TRIGGER_IDLE:
        cfg->cam_gzz_cfg.af_trigger.val = CAM_AF_TRIGGER_IDLE;
        break;
    }

    CONFIGURATOR_CONFIG_TOUCH(cfg->cam_gzz_cfg.af_trigger);
    return ret;
}

int camera_external_convert_control_awb_lock(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    int ret = 0;
    camera_config_plug_cfg_t *cfg = config;

    guzzi_camera3_enum_control_awb_lock_t awb_lock =
            (guzzi_camera3_enum_control_awb_lock_t) cfg->external.control_awb_lock.v;

    switch (awb_lock) {
    case GUZZI_CAMERA3_ENUM_CONTROL_AWB_LOCK_ON:
        cfg->cam_gzz_cfg.wb_lock.val = CAM_ALG_LOCK_ON;
        break;
    default:
        mmsdbg(DL_ERROR, "Unsupported mode", __FUNCTION__);
        ret = -1;
        /* no break */
    case GUZZI_CAMERA3_ENUM_CONTROL_AWB_LOCK_OFF:
        cfg->cam_gzz_cfg.wb_lock.val = CAM_ALG_LOCK_OFF;
        break;
    }

    CONFIGURATOR_CONFIG_TOUCH(cfg->cam_gzz_cfg.wb_lock);
    return ret;
}

int camera_external_convert_control_awb_mode(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    int ret = 0;
    camera_config_plug_cfg_t *cfg = config;
    guzzi_camera3_enum_control_awb_mode_t m =
            (guzzi_camera3_enum_control_awb_mode_t)cfg->external.control_awb_mode.v;

    switch (m) {
    case GUZZI_CAMERA3_ENUM_CONTROL_AWB_MODE_OFF:
        if (cfg->external.color_correction_mode.v == GUZZI_CAMERA3_ENUM_COLOR_CORRECTION_MODE_FAST )
            cfg->cam_gzz_cfg.wb_mode.val = AWB_MODE_MANUAL_KELVINS2GAINS;
        else
            cfg->cam_gzz_cfg.wb_mode.val = AWB_MODE_MANUAL_GAINS2KELVINS;
        break;
    case GUZZI_CAMERA3_ENUM_CONTROL_AWB_MODE_INCANDESCENT:
        cfg->cam_gzz_cfg.wb_mode.val = AWB_MODE_INCANDESCENT;
        break;
    case GUZZI_CAMERA3_ENUM_CONTROL_AWB_MODE_FLUORESCENT:
        cfg->cam_gzz_cfg.wb_mode.val = AWB_MODE_FLUORESCENT;
        break;
    case GUZZI_CAMERA3_ENUM_CONTROL_AWB_MODE_WARM_FLUORESCENT:
        cfg->cam_gzz_cfg.wb_mode.val = AWB_MODE_WARM_FLUORESCENT;
        break;
    case GUZZI_CAMERA3_ENUM_CONTROL_AWB_MODE_DAYLIGHT:
        cfg->cam_gzz_cfg.wb_mode.val = AWB_MODE_DAYLIGHT;
        break;
    case GUZZI_CAMERA3_ENUM_CONTROL_AWB_MODE_CLOUDY_DAYLIGHT:
        cfg->cam_gzz_cfg.wb_mode.val = AWB_MODE_CLOUDY;
        break;
    case GUZZI_CAMERA3_ENUM_CONTROL_AWB_MODE_TWILIGHT:
        cfg->cam_gzz_cfg.wb_mode.val = AWB_MODE_TWILIGHT;
        break;
    case GUZZI_CAMERA3_ENUM_CONTROL_AWB_MODE_SHADE:
        cfg->cam_gzz_cfg.wb_mode.val = AWB_MODE_SHADOW;
        break;
    default:
        mmsdbg(DL_ERROR, "Unsupported mode", __FUNCTION__);
        ret = -1;
        /* no break */
    case GUZZI_CAMERA3_ENUM_CONTROL_AWB_MODE_AUTO:
        cfg->cam_gzz_cfg.wb_mode.val = AWB_MODE_AUTO;
        break;
    }

    CONFIGURATOR_CONFIG_TOUCH(cfg->cam_gzz_cfg.wb_mode);
    return ret;
}

int camera_external_convert_control_awb_regions(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    int             ret = 0, i;
    camera_config_plug_cfg_t *cfg = config;
    hat_reg_pri_t   *wb_region = &cfg->cam_gzz_cfg.wb_regions.val;

    wb_region->priority_max = 0;

    meta_arr_iterate_dim_1(2, &cfg->external.control_awb_regions, i) {
        wb_region->reg_box[i].x = METAIDX2(&cfg->external.control_awb_regions, i, 0);
        wb_region->reg_box[i].y = METAIDX2(&cfg->external.control_awb_regions, i, 1);
        wb_region->reg_box[i].w = METAIDX2(&cfg->external.control_awb_regions, i, 2);
        wb_region->reg_box[i].h = METAIDX2(&cfg->external.control_awb_regions, i, 3);
        wb_region->priority[i]  = METAIDX2(&cfg->external.control_awb_regions, i, 4);

        if (wb_region->priority_max < wb_region->priority[i]) {
            wb_region->priority_max = wb_region->priority[i];
        }
    }

    wb_region->reg_num = i;
    CONFIGURATOR_CONFIG_TOUCH(cfg->cam_gzz_cfg.wb_regions);
    return ret;
}

int camera_external_convert_control_capture_intent(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    int ret = 0;
    camera_config_plug_cfg_t *cfg = config;

    guzzi_camera3_enum_control_capture_intent_t intent =
            (guzzi_camera3_enum_control_capture_intent_t) cfg->external.control_capture_intent.v;

    switch (intent) {
    case GUZZI_CAMERA3_ENUM_CONTROL_CAPTURE_INTENT_CUSTOM:
        cfg->cam_gzz_cfg.cam_mode.val = CAM_CAPTURE_INTENT_CUSTOM;
        break;
    case GUZZI_CAMERA3_ENUM_CONTROL_CAPTURE_INTENT_STILL_CAPTURE:
        cfg->cam_gzz_cfg.cam_mode.val = CAM_CAPTURE_INTENT_STILL_CAPTURE;
        break;
    case GUZZI_CAMERA3_ENUM_CONTROL_CAPTURE_INTENT_VIDEO_RECORD:
        cfg->cam_gzz_cfg.cam_mode.val = CAM_CAPTURE_INTENT_VIDEO_RECORD;
        break;
    case GUZZI_CAMERA3_ENUM_CONTROL_CAPTURE_INTENT_VIDEO_SNAPSHOT:
        cfg->cam_gzz_cfg.cam_mode.val = CAM_CAPTURE_INTENT_VIDEO_SNAPSHOT;
        break;
    case GUZZI_CAMERA3_ENUM_CONTROL_CAPTURE_INTENT_ZERO_SHUTTER_LAG:
        cfg->cam_gzz_cfg.cam_mode.val = CAM_CAPTURE_INTENT_ZERO_SHUTTER_LAG;
        break;
    default:
        mmsdbg(DL_ERROR, "Unsupported mode", __FUNCTION__)
        ;
        ret = -1;
        /* no break */
    case GUZZI_CAMERA3_ENUM_CONTROL_CAPTURE_INTENT_PREVIEW:
        cfg->cam_gzz_cfg.cam_mode.val = CAM_CAPTURE_INTENT_PREVIEW;
        break;
    }

    CONFIGURATOR_CONFIG_TOUCH(cfg->cam_gzz_cfg.cam_mode);
    return ret;
}

int camera_external_convert_control_effect_mode(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    int ret = 0;
    camera_config_plug_cfg_t    *cfg = config;

    guzzi_camera3_enum_control_effect_mode_t effect =
            (guzzi_camera3_enum_control_effect_mode_t) cfg->external.control_effect_mode.v;

    switch (effect) {
    case GUZZI_CAMERA3_ENUM_CONTROL_EFFECT_MODE_MONO:
        cfg->cam_gzz_cfg.eff_type.val = CAM_EFFECT_GRAYSCALE;
        break;
    case GUZZI_CAMERA3_ENUM_CONTROL_EFFECT_MODE_NEGATIVE:
        cfg->cam_gzz_cfg.eff_type.val = CAM_EFFECT_NEGATIVE;
        break;
    case GUZZI_CAMERA3_ENUM_CONTROL_EFFECT_MODE_SOLARIZE:
        cfg->cam_gzz_cfg.eff_type.val = CAM_EFFECT_SOLARIZE;
        break;
    case GUZZI_CAMERA3_ENUM_CONTROL_EFFECT_MODE_SEPIA:
        cfg->cam_gzz_cfg.eff_type.val = CAM_EFFECT_SEPIA;
        break;
    case GUZZI_CAMERA3_ENUM_CONTROL_EFFECT_MODE_POSTERIZE:
        cfg->cam_gzz_cfg.eff_type.val = CAM_EFFECT_POSTERIZE;
        break;
    case GUZZI_CAMERA3_ENUM_CONTROL_EFFECT_MODE_WHITEBOARD:
        cfg->cam_gzz_cfg.eff_type.val = CAM_EFFECT_WHITE_BOARD;
        break;
    case GUZZI_CAMERA3_ENUM_CONTROL_EFFECT_MODE_BLACKBOARD:
        cfg->cam_gzz_cfg.eff_type.val = CAM_EFFECT_BLACK_BOARD;
        break;
    case GUZZI_CAMERA3_ENUM_CONTROL_EFFECT_MODE_AQUA:
        cfg->cam_gzz_cfg.eff_type.val = CAM_EFFECT_AQUA;
        break;
    default:
        ret = -1;
        mmsdbg(DL_ERROR, "Unsupported effect", __FUNCTION__)
        /* no break */
    case GUZZI_CAMERA3_ENUM_CONTROL_EFFECT_MODE_OFF:
        cfg->cam_gzz_cfg.eff_type.val = CAM_EFFECT_NONE;
        break;
    }

    CONFIGURATOR_CONFIG_TOUCH(cfg->cam_gzz_cfg.eff_type);

    dtp_effects_t *eff = NULL;
    effect_cfg_data_t    *data = ((camera_config_plugin_prv_t *)plugin_prv)->effect_cfg_data;

    data->dtp_dkeys.par_0 = cfg->cam_gzz_cfg.eff_type.val;
    //TODO Add DTP pre process
    if (dtpsrv_process(data->hdtp, NULL, &data->dtp_dkeys, (void**)&eff)) {
        mmsdbg(DL_ERROR, "Effect mode - can't process DTP");
        ret = -1;
    } else {
        cfg->cam_gzz_cfg.contrast.val = eff->contrast;
        CONFIGURATOR_CONFIG_TOUCH(cfg->cam_gzz_cfg.contrast);
        cfg->cam_gzz_cfg.saturation.val = eff->saturation;
        CONFIGURATOR_CONFIG_TOUCH(cfg->cam_gzz_cfg.saturation);
        cfg->cam_gzz_cfg.sharpness.val = eff->sharpness;
        CONFIGURATOR_CONFIG_TOUCH(cfg->cam_gzz_cfg.sharpness);
        cfg->cam_gzz_cfg.brightness.val = eff->brightness;
        CONFIGURATOR_CONFIG_TOUCH(cfg->cam_gzz_cfg.brightness);
        cfg->external.control_ae_exposure_compensation.v =
                convert_exposure_compensation_to_external(eff->compensation);
        camera_external_convert_control_ae_exposure_compensation( c, plugin_prv, config);
    }
    return ret;
}

int camera_external_convert_control_mode(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    int ret = 0;
    camera_config_plug_cfg_t *cfg = config;

    guzzi_camera3_enum_control_mode_t mode =
            (guzzi_camera3_enum_control_mode_t)cfg->external.control_mode.v;

    switch (mode) {
    case GUZZI_CAMERA3_ENUM_CONTROL_MODE_OFF:
        cfg->cam_gzz_cfg.aca_mode.val = CAM_ACA_MODE_OFF;
        break;
    case GUZZI_CAMERA3_ENUM_CONTROL_MODE_USE_SCENE_MODE:
        cfg->cam_gzz_cfg.aca_mode.val = CAM_ACA_MODE_USE_SCENE_MODE;
        break;
    default:
        mmsdbg(DL_ERROR, "Unsupported mode", __FUNCTION__);
        ret = -1;
        /* no break */
    case GUZZI_CAMERA3_ENUM_CONTROL_MODE_AUTO:
        cfg->cam_gzz_cfg.aca_mode.val = CAM_ACA_MODE_AUTO;
        break;
    }

    CONFIGURATOR_CONFIG_TOUCH(cfg->cam_gzz_cfg.aca_mode);
    return ret;
}

int camera_external_convert_control_scene_mode(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    int ret = 0;
    camera_config_plug_cfg_t *cfg = config;
    guzzi_camera3_enum_control_scene_mode_t scene_mode =
            (guzzi_camera3_enum_control_mode_t)cfg->external.control_scene_mode.v;

    if(cfg->cam_gzz_cfg.aca_mode.val != CAM_ACA_MODE_USE_SCENE_MODE) {
        cfg->cam_gzz_cfg.scene_mode.val = CAM_SCENE_NONE;
        CONFIGURATOR_CONFIG_TOUCH(cfg->cam_gzz_cfg.scene_mode);
    }
    else {
        switch (scene_mode) {
        case GUZZI_CAMERA3_ENUM_CONTROL_SCENE_MODE_ACTION:
            cfg->cam_gzz_cfg.scene_mode.val = CAM_SCENE_ACTION;
            break;
        case GUZZI_CAMERA3_ENUM_CONTROL_SCENE_MODE_PORTRAIT:
            cfg->cam_gzz_cfg.scene_mode.val = CAM_SCENE_PORTRAIT;
            break;
        case GUZZI_CAMERA3_ENUM_CONTROL_SCENE_MODE_LANDSCAPE:
            cfg->cam_gzz_cfg.scene_mode.val = CAM_SCENE_LANDSCAPE;
            break;
        case GUZZI_CAMERA3_ENUM_CONTROL_SCENE_MODE_NIGHT:
            cfg->cam_gzz_cfg.scene_mode.val = CAM_SCENE_NIGHT;
            break;
        case GUZZI_CAMERA3_ENUM_CONTROL_SCENE_MODE_NIGHT_PORTRAIT:
            cfg->cam_gzz_cfg.scene_mode.val = CAM_SCENE_NIGHT_PORTRAIT;
            break;
        case GUZZI_CAMERA3_ENUM_CONTROL_SCENE_MODE_THEATRE:
            cfg->cam_gzz_cfg.scene_mode.val = CAM_SCENE_THEATRE;
            break;
        case GUZZI_CAMERA3_ENUM_CONTROL_SCENE_MODE_BEACH:
            cfg->cam_gzz_cfg.scene_mode.val = CAM_SCENE_BEACH;
            break;
        case GUZZI_CAMERA3_ENUM_CONTROL_SCENE_MODE_SNOW:
            cfg->cam_gzz_cfg.scene_mode.val = CAM_SCENE_SNOW;
            break;
        case GUZZI_CAMERA3_ENUM_CONTROL_SCENE_MODE_SUNSET:
            cfg->cam_gzz_cfg.scene_mode.val = CAM_SCENE_SUNSET;
            break;
        case GUZZI_CAMERA3_ENUM_CONTROL_SCENE_MODE_STEADYPHOTO:
            cfg->cam_gzz_cfg.scene_mode.val = CAM_SCENE_STEADYPHOTO;
            break;
        case GUZZI_CAMERA3_ENUM_CONTROL_SCENE_MODE_FIREWORKS:
            cfg->cam_gzz_cfg.scene_mode.val = CAM_SCENE_FIREWORKS;
            break;
        case GUZZI_CAMERA3_ENUM_CONTROL_SCENE_MODE_SPORTS:
            cfg->cam_gzz_cfg.scene_mode.val = CAM_SCENE_SPORT;
            break;
        case GUZZI_CAMERA3_ENUM_CONTROL_SCENE_MODE_PARTY:
            cfg->cam_gzz_cfg.scene_mode.val = CAM_SCENE_PARTY;
            break;
        case GUZZI_CAMERA3_ENUM_CONTROL_SCENE_MODE_CANDLELIGHT:
            cfg->cam_gzz_cfg.scene_mode.val = CAM_SCENE_CANDLELIGHT;
            break;
        case GUZZI_CAMERA3_ENUM_CONTROL_SCENE_MODE_BARCODE:
            cfg->cam_gzz_cfg.scene_mode.val = CAM_SCENE_BARCODE;
            break;
        default:
            mmsdbg(DL_ERROR, "Unsupported mode", __FUNCTION__);
            ret = -1;
            /* no break */
        case GUZZI_CAMERA3_ENUM_CONTROL_SCENE_MODE_FACE_PRIORITY://TODO
            cfg->cam_gzz_cfg.scene_mode.val = CAM_SCENE_NONE;
            break;
        }
        CONFIGURATOR_CONFIG_TOUCH(cfg->cam_gzz_cfg.scene_mode);

        scene_mode_settings *scene_settings = NULL;
        camera_config_plugin_prv_t *prv_data = plugin_prv;
        scene_cfg_data_t    *scene_data = (scene_cfg_data_t*)prv_data->scene_cfg_data;

        if(cfg->cam_gzz_cfg.scene_mode.val != CAM_SCENE_NONE) {
            scene_data->dtp_dkeys.par_0 = cfg->cam_gzz_cfg.scene_mode.val;
            if (dtpsrv_process(scene_data->hdtp, NULL, &scene_data->dtp_dkeys, (void**)&scene_settings)) {
                mmsdbg(DL_ERROR, "Scene mode - can't process DTP");
                ret = -1;
            }
            else {
                cfg->external.control_ae_mode.v = scene_settings->ae_mode;
                camera_external_convert_control_ae_mode( c, plugin_prv, config);
                cfg->external.control_awb_mode.v = scene_settings->awb_mode;
                camera_external_convert_control_awb_mode( c, plugin_prv, config);
                cfg->external.control_af_mode.v = scene_settings->af_mode;
                camera_external_convert_control_af_mode( c, plugin_prv, config);
                cfg->external.control_effect_mode.v = scene_settings->effect;
                camera_external_convert_control_effect_mode( c, plugin_prv, config);
                cfg->external.control_ae_exposure_compensation.v =
                        convert_exposure_compensation_to_external(scene_settings->ev_compensation);
                camera_external_convert_control_ae_exposure_compensation( c, plugin_prv, config);
                cfg->cam_gzz_cfg.contrast.val = scene_settings->contrast;
                CONFIGURATOR_CONFIG_TOUCH(cfg->cam_gzz_cfg.contrast);
                cfg->cam_gzz_cfg.saturation.val = scene_settings->saturation;
                CONFIGURATOR_CONFIG_TOUCH(cfg->cam_gzz_cfg.saturation);
                cfg->cam_gzz_cfg.sharpness.val = scene_settings->sharpness;
                CONFIGURATOR_CONFIG_TOUCH(cfg->cam_gzz_cfg.sharpness);
                cfg->cam_gzz_cfg.brightness.val = scene_settings->brightness;
                CONFIGURATOR_CONFIG_TOUCH(cfg->cam_gzz_cfg.brightness);
            }
        }
    }

    return 0;
}

int camera_external_convert_control_video_stabilization_mode(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    // mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_demosaic_mode(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    int ret = 0;
    camera_config_plug_cfg_t *cfg = config;

    guzzi_camera3_enum_demosaic_mode_t demosaic =
            (guzzi_camera3_enum_demosaic_mode_t)cfg->external.demosaic_mode.v;

    switch (demosaic) {
    case GUZZI_CAMERA3_ENUM_DEMOSAIC_MODE_FAST:
        cfg->cam_gzz_cfg.dpc_mode.val = CAM_CFAI_FAST;
        break;
    default:
        mmsdbg(DL_ERROR, "Unsupported mode", __FUNCTION__);
        ret = -1;
        /* no break */
    case GUZZI_CAMERA3_ENUM_DEMOSAIC_MODE_HIGH_QUALITY:
        cfg->cam_gzz_cfg.cfai_mode.val = CAM_CFAI_HQ;
        break;
    }

    CONFIGURATOR_CONFIG_TOUCH(cfg->cam_gzz_cfg.cfai_mode);
    return ret;
}

int camera_external_convert_edge_mode(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    int ret = 0;
    camera_config_plug_cfg_t *cfg = config;

    guzzi_camera3_enum_edge_mode_t edge_mode =
            (guzzi_camera3_enum_edge_mode_t)cfg->external.edge_mode.v;

    switch (edge_mode) {
    case GUZZI_CAMERA3_ENUM_EDGE_MODE_OFF:
        cfg->cam_gzz_cfg.ee_type.val = CAM_EDGE_MANUAL;
        break;
    default:
        mmsdbg(DL_ERROR, "Unsupported mode", __FUNCTION__);
        ret = -1;
        /* no break */
    case GUZZI_CAMERA3_ENUM_EDGE_MODE_FAST:
    case GUZZI_CAMERA3_ENUM_EDGE_MODE_HIGH_QUALITY:
        cfg->cam_gzz_cfg.ee_type.val = CAM_EDGE_AUTO;
        break;
    }

    CONFIGURATOR_CONFIG_TOUCH(cfg->cam_gzz_cfg.ee_type);
    return ret;
}

int camera_external_convert_edge_strength(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    int ret = 0;
    camera_config_plug_cfg_t *cfg = config;

    cfg->cam_gzz_cfg.ee_strength.val =
            cfg->external.edge_strength.v / 10.0;

    CONFIGURATOR_CONFIG_TOUCH(cfg->cam_gzz_cfg.ee_strength);
    return ret;
}

int camera_external_convert_flash_firing_power(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    int                 ret = 0;
    camera_config_plug_cfg_t *cfg = config;

    /*
     * HAL3.2 range 0..10
     * gzz range 0..100%
     */
    cfg->cam_gzz_cfg.flash_pwr.val =
            cfg->external.flash_firing_power.v * 10;

    CONFIGURATOR_CONFIG_TOUCH(cfg->cam_gzz_cfg.flash_pwr);
    return ret;
}

int camera_external_convert_flash_firing_time(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    int                 ret = 0;
    camera_config_plug_cfg_t *cfg = config;

    /*
     * Firing time of flash relative to start of exposure [ns]
     */
    cfg->cam_gzz_cfg.flash_firing_time.val =
            cfg->external.flash_firing_time.v / 1000;

    CONFIGURATOR_CONFIG_TOUCH(cfg->cam_gzz_cfg.flash_duration);
    return ret;
}

int camera_external_convert_flash_mode(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    int ret = 0;
    camera_config_plug_cfg_t *cfg = config;

    guzzi_camera3_enum_flash_mode_t flash_mode =
            (guzzi_camera3_enum_flash_mode_t)cfg->external.flash_mode.v;

    switch (flash_mode) {
    case GUZZI_CAMERA3_ENUM_FLASH_MODE_TORCH:
        cfg->cam_gzz_cfg.flash_mode.val = CAM_FLASH_FORCE_ON;
        break;
    case GUZZI_CAMERA3_ENUM_FLASH_MODE_SINGLE:
        cfg->cam_gzz_cfg.flash_mode.val = CAM_FLASH_AUTO;
        break;
    default:
        mmsdbg(DL_ERROR, "Unsupported mode", __FUNCTION__);
        ret = -1;
        /* no break */
    case GUZZI_CAMERA3_ENUM_FLASH_MODE_OFF:
        cfg->cam_gzz_cfg.flash_mode.val = CAM_FLASH_FORCE_OFF;
        break;
    }

    CONFIGURATOR_CONFIG_TOUCH(cfg->cam_gzz_cfg.flash_mode);
    return ret;
}

int camera_external_convert_geometric_mode(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    // mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_geometric_strength(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
//    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_hot_pixel_mode(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    int ret = 0;
    camera_config_plug_cfg_t *cfg = config;

    guzzi_camera3_enum_hot_pixel_mode_t flash_mode =
            (guzzi_camera3_enum_hot_pixel_mode_t)cfg->external.hot_pixel_mode.v;

    switch (flash_mode) {
    case GUZZI_CAMERA3_ENUM_HOT_PIXEL_MODE_OFF:
        cfg->cam_gzz_cfg.dpc_mode.val = CAM_DPC_MODE_OFF;
        break;
    default:
        mmsdbg(DL_ERROR, "Unsupported mode", __FUNCTION__);
        ret = -1;
        /* no break */
    case GUZZI_CAMERA3_ENUM_HOT_PIXEL_MODE_FAST:
    case GUZZI_CAMERA3_ENUM_HOT_PIXEL_MODE_HIGH_QUALITY:
        cfg->cam_gzz_cfg.dpc_mode.val = CAM_DPC_MODE_ON;
        break;
    }

    CONFIGURATOR_CONFIG_TOUCH(cfg->cam_gzz_cfg.dpc_mode);
    return ret;
}

int camera_external_convert_jpeg_gps_coordinates(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
 //   mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_jpeg_gps_processing_method(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
//    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_jpeg_gps_timestamp(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
//    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_jpeg_orientation(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
 //   mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_jpeg_quality(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
 //   mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_jpeg_thumbnail_quality(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
//    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_jpeg_thumbnail_size(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
//    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_led_transmit(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
//    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_lens_aperture(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    int ret = 0;
    camera_config_plug_cfg_t *cfg = config;

    cfg->cam_gzz_cfg.aperture.val =
            (float)(cfg->external.lens_aperture.v);

    CONFIGURATOR_CONFIG_TOUCH(cfg->cam_gzz_cfg.aperture);
    return ret;
}

int camera_external_convert_lens_filter_density(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
//    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
//    return 0;

    /* ======  USED AS MANUAL COLOR TEMPERATURE ===============
     *    if manual color temperature = 0  - use color temperature from external algorithm
     *    if manual color temperature != 0 - use manual color temperature
     *    external parameter is float, internal parameter is uint32
     */
    int ret = 0;
    camera_config_plug_cfg_t *cfg = config;

    // TODO: better conversion to guzzi internal config
    cfg->cam_gzz_cfg.manual_color_temp.val =
            (uint32)(cfg->external.lens_filter_density.v);

    CONFIGURATOR_CONFIG_TOUCH(cfg->cam_gzz_cfg.manual_color_temp);
    return ret;

}

int camera_external_convert_lens_focal_length(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    /*
     * TODO: check me again
     * GUZZI_CAMERA3_INDEX_CONTROLS_LENS_FOCAL_LENGTH
     * guzzi_camera3_controls_lens_focal_length_t
     *
     */

    int ret = 0;
    camera_config_plug_cfg_t *cfg = config;

    cfg->cam_gzz_cfg.lens_focal_len.val = cfg->external.lens_focal_length.v;

    CONFIGURATOR_CONFIG_TOUCH(cfg->cam_gzz_cfg.lens_focal_len);
    return ret;
}

int camera_external_convert_lens_focus_distance(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
//    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_lens_optical_stabilization_mode(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    int ret = 0;
    camera_config_plug_cfg_t *cfg = config;

    cfg->cam_gzz_cfg.optical_stab_mode.val =
            (gzz_lens_optical_stab_mode_t)(cfg->external.lens_optical_stabilization_mode.v);

    CONFIGURATOR_CONFIG_TOUCH(cfg->cam_gzz_cfg.optical_stab_mode);
    return ret;
}

int camera_external_convert_noise_reduction_mode(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    int ret = 0;
    camera_config_plug_cfg_t *cfg = config;

    guzzi_camera3_enum_noise_reduction_mode_t noise_mode =
            (guzzi_camera3_enum_noise_reduction_mode_t)cfg->external.noise_reduction_mode.v;

    switch (noise_mode) {
    case GUZZI_CAMERA3_ENUM_NOISE_REDUCTION_MODE_OFF:
        cfg->cam_gzz_cfg.nf_mode.val = CAM_NF_MODE_OFF;
        break;
    default:
        mmsdbg(DL_ERROR, "Unsupported mode", __FUNCTION__);
        ret = -1;
        /* no break */
    case GUZZI_CAMERA3_ENUM_NOISE_REDUCTION_MODE_FAST:
    case GUZZI_CAMERA3_ENUM_NOISE_REDUCTION_MODE_HIGH_QUALITY:
        cfg->cam_gzz_cfg.nf_mode.val = CAM_NF_MODE_ON;
        break;
    }

    CONFIGURATOR_CONFIG_TOUCH(cfg->cam_gzz_cfg.nf_mode);
    return ret;
}

int camera_external_convert_noise_reduction_strength(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    int ret = 0;
    camera_config_plug_cfg_t *cfg = config;

    cfg->cam_gzz_cfg.nf_strength.val =
            cfg->external.noise_reduction_strength.v / 10.0;

    CONFIGURATOR_CONFIG_TOUCH(cfg->cam_gzz_cfg.nf_strength);
    return ret;
}

int camera_external_convert_request_frame_count(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    // mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_request_id(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    // mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_request_input_streams(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    // mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_request_metadata_mode(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    // mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_request_output_streams(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    // mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_request_type(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    // mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_scaler_crop_region(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    camera_config_plug_cfg_t *cfg = config;

    cfg->cam_gzz_cfg.sensor_crop.val.x = cfg->external.scaler_crop_region.v[0];
    cfg->cam_gzz_cfg.sensor_crop.val.y = cfg->external.scaler_crop_region.v[1];
    cfg->cam_gzz_cfg.sensor_crop.val.w = cfg->external.scaler_crop_region.v[2];
    cfg->cam_gzz_cfg.sensor_crop.val.h = cfg->external.scaler_crop_region.v[3];

    CONFIGURATOR_CONFIG_TOUCH(cfg->cam_gzz_cfg.sensor_crop);

    return 0;
}

int camera_external_convert_sensor_exposure_time(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    int ret = 0;
    camera_config_plug_cfg_t *cfg = config;

    // TODO: better conversion to guzzi internal config
    cfg->cam_gzz_cfg.manual_exposure.val.exposure =
            cfg->external.sensor_exposure_time.v / 1000;

    CONFIGURATOR_CONFIG_TOUCH(cfg->cam_gzz_cfg.manual_exposure);
    return ret;
}

int camera_external_convert_sensor_frame_duration(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    int ret = 0;
    camera_config_plug_cfg_t *cfg = config;

    cfg->cam_gzz_cfg.fr_duration.val = cfg->external.sensor_frame_duration.v;

    CONFIGURATOR_CONFIG_TOUCH(cfg->cam_gzz_cfg.fr_duration);
    return ret;
}

int camera_external_convert_sensor_sensitivity(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    int ret = 0;
    camera_config_plug_cfg_t *cfg = config;

    // TODO: better conversion to guzzi internal config
    cfg->cam_gzz_cfg.manual_exposure.val.dgain = 1;
    cfg->cam_gzz_cfg.manual_exposure.val.again =
            cfg->external.sensor_sensitivity.v / 100.0;

    CONFIGURATOR_CONFIG_TOUCH(cfg->cam_gzz_cfg.manual_exposure);
    return ret;
}

int camera_external_convert_shading_mode(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    int ret = 0;
    camera_config_plug_cfg_t *cfg = config;

    guzzi_camera3_enum_shading_mode_t shading_mode =
            (guzzi_camera3_enum_shading_mode_t)cfg->external.shading_mode.v;

    switch (shading_mode) {
    case GUZZI_CAMERA3_ENUM_SHADING_MODE_OFF:
        cfg->cam_gzz_cfg.lsc_mode.val = CAM_LSC_MANUAL;
        break;
    default:
        mmsdbg(DL_ERROR, "Unsupported mode", __FUNCTION__);
        ret = -1;
        /* no break */
    case GUZZI_CAMERA3_ENUM_SHADING_MODE_FAST:
    case GUZZI_CAMERA3_ENUM_SHADING_MODE_HIGH_QUALITY:
        cfg->cam_gzz_cfg.lsc_mode.val = CAM_LSC_AUTO;
        break;
    }

    CONFIGURATOR_CONFIG_TOUCH(cfg->cam_gzz_cfg.lsc_mode);
    return ret;
}

int camera_external_convert_shading_strength(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    int ret = 0;
    camera_config_plug_cfg_t *cfg = config;

    cfg->cam_gzz_cfg.lsc_strength.val.lsc_strength.b =
    cfg->cam_gzz_cfg.lsc_strength.val.lsc_strength.gb =
    cfg->cam_gzz_cfg.lsc_strength.val.lsc_strength.gr =
    cfg->cam_gzz_cfg.lsc_strength.val.lsc_strength.r =
            cfg->external.shading_strength.v / 10.0;

    CONFIGURATOR_CONFIG_TOUCH(cfg->cam_gzz_cfg.lsc_strength);
    return ret;
}

int camera_external_convert_statistics_face_detect_mode(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    // mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_statistics_histogram_mode(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    int ret = 0;
    camera_config_plug_cfg_t *cfg = config;

    guzzi_camera3_enum_statistics_histogram_mode_t hist_mode =
            (guzzi_camera3_enum_statistics_histogram_mode_t)cfg->external.statistics_histogram_mode.v;

    switch (hist_mode) {
    case GUZZI_CAMERA3_ENUM_STATISTICS_HISTOGRAM_MODE_ON:
        cfg->cam_gzz_cfg.stats_enable.val.hist = CAM_STAT_MODE_ON;
        break;
    default:
        mmsdbg(DL_ERROR, "Unsupported mode", __FUNCTION__);
        ret = -1;
        /* no break */
    case GUZZI_CAMERA3_ENUM_STATISTICS_HISTOGRAM_MODE_OFF:
        cfg->cam_gzz_cfg.stats_enable.val.hist = CAM_STAT_MODE_OFF;
        break;
    }

    CONFIGURATOR_CONFIG_TOUCH(cfg->cam_gzz_cfg.stats_enable);
    return ret;
}

int camera_external_convert_statistics_lens_shading_map_mode(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    int ret = 0;
    camera_config_plug_cfg_t *cfg = config;

    guzzi_camera3_enum_statistics_lens_shading_map_mode_t lsc_mode =
            (guzzi_camera3_enum_statistics_lens_shading_map_mode_t)cfg->external.statistics_lens_shading_map_mode.v;

    switch (lsc_mode) {
    case GUZZI_CAMERA3_ENUM_STATISTICS_LENS_SHADING_MAP_MODE_ON:
        cfg->cam_gzz_cfg.stats_enable.val.lsc = CAM_STAT_MODE_ON;
        break;
    default:
        mmsdbg(DL_ERROR, "Unsupported mode", __FUNCTION__);
        ret = -1;
        /* no break */
    case GUZZI_CAMERA3_ENUM_STATISTICS_LENS_SHADING_MAP_MODE_OFF:
        cfg->cam_gzz_cfg.stats_enable.val.lsc = CAM_STAT_MODE_OFF;
        break;
    }

    CONFIGURATOR_CONFIG_TOUCH(cfg->cam_gzz_cfg.stats_enable);
    return ret;
}

int camera_external_convert_statistics_sharpness_map_mode(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    int ret = 0;
    camera_config_plug_cfg_t *cfg = config;

    guzzi_camera3_enum_statistics_sharpness_map_mode_t sharp_mode =
            (guzzi_camera3_enum_statistics_sharpness_map_mode_t)cfg->external.statistics_sharpness_map_mode.v;

    switch (sharp_mode) {
    case GUZZI_CAMERA3_ENUM_STATISTICS_SHARPNESS_MAP_MODE_ON:
        cfg->cam_gzz_cfg.stats_enable.val.af_stat = CAM_STAT_MODE_ON;
        break;
    default:
        mmsdbg(DL_ERROR, "Unsupported mode", __FUNCTION__);
        ret = -1;
        /* no break */
    case GUZZI_CAMERA3_ENUM_STATISTICS_SHARPNESS_MAP_MODE_OFF:
        cfg->cam_gzz_cfg.stats_enable.val.af_stat = CAM_STAT_MODE_OFF;
        break;
    }

    CONFIGURATOR_CONFIG_TOUCH(cfg->cam_gzz_cfg.stats_enable);
    return ret;
}

int camera_external_convert_tonemap_curve_blue(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    int ret = 0;
    camera_config_plug_cfg_t *cfg = config;

    guzzi_camera3_controls_tonemap_curve_blue_t *gamma = &cfg->external.tonemap_curve_blue;

    cfg->cam_gzz_cfg.manual_gamma.val.b.tbl_size = gamma->dim_size_2;
    cfg->cam_gzz_cfg.manual_gamma.val.b.tbl = (hat_gamma_point_t *)gamma->v;

    CONFIGURATOR_CONFIG_TOUCH(cfg->cam_gzz_cfg.manual_gamma);
    return ret;
}

int camera_external_convert_tonemap_curve_green(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    int ret = 0;
    camera_config_plug_cfg_t *cfg = config;

    guzzi_camera3_controls_tonemap_curve_green_t *gamma = &cfg->external.tonemap_curve_green;

    cfg->cam_gzz_cfg.manual_gamma.val.g.tbl_size = gamma->dim_size_2;
    cfg->cam_gzz_cfg.manual_gamma.val.g.tbl = (hat_gamma_point_t *)gamma->v;

    CONFIGURATOR_CONFIG_TOUCH(cfg->cam_gzz_cfg.manual_gamma);
    return ret;
}

int camera_external_convert_tonemap_curve_red(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    int ret = 0;
    camera_config_plug_cfg_t *cfg = config;

    guzzi_camera3_controls_tonemap_curve_red_t *gamma = &cfg->external.tonemap_curve_red;

    cfg->cam_gzz_cfg.manual_gamma.val.r.tbl_size = gamma->dim_size_2;
    cfg->cam_gzz_cfg.manual_gamma.val.r.tbl = (hat_gamma_point_t *)gamma->v;

    CONFIGURATOR_CONFIG_TOUCH(cfg->cam_gzz_cfg.manual_gamma);
    return ret;
}

int camera_external_convert_tonemap_mode(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    int ret = 0;
    camera_config_plug_cfg_t *cfg = config;

    guzzi_camera3_enum_tonemap_mode_t gamma_mode =
            (guzzi_camera3_enum_tonemap_mode_t)cfg->external.tonemap_mode.v;

    switch (gamma_mode) {
    case GUZZI_CAMERA3_ENUM_TONEMAP_MODE_CONTRAST_CURVE:
        cfg->cam_gzz_cfg.gamma_mode.val = CAM_GAMMA_MANUAL;
        break;
    case GUZZI_CAMERA3_ENUM_TONEMAP_MODE_FAST:
        cfg->cam_gzz_cfg.gamma_mode.val = CAM_GAMMA_AUTO;
        break;
    default:
        mmsdbg(DL_ERROR, "Unsupported mode", __FUNCTION__);
        ret = -1;
        /* no break */
    case GUZZI_CAMERA3_ENUM_TONEMAP_MODE_HIGH_QUALITY:
        cfg->cam_gzz_cfg.gamma_mode.val = CAM_GAMMA_AUTO;
        break;
    }

    CONFIGURATOR_CONFIG_TOUCH(cfg->cam_gzz_cfg.gamma_mode);
    return ret;
}

/*
 * ****************************************************************************
 * ** Guzzi specific **********************************************************
 * ****************************************************************************
 */
int camera_external_convert_capture_request_active(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    camera_config_plug_cfg_t *cfg;
    int i;

    cfg = config;

    if (ARRAY_SIZE(cfg->cam_gzz_cfg.stream_cntrl.val) < META_ARR_SIZE_1(&cfg->external.capture_request_active)) {
        mmsdbg(DL_ERROR, "Too many streams!");
        return -1;
    }

    for (i = 0; i < META_DIM_SIZE_1(&cfg->external.capture_request_active); i++) {
        cfg->cam_gzz_cfg.stream_cntrl.val[i] = CAM_STREAM_OFF;
    }
    meta_arr_iterate_dim_1(1, &cfg->external.capture_request_active, i) {
        if (METAIDX1(&cfg->external.capture_request_active, i) == GUZZI_CAMERA3_ENUM_CAPTURE_REQUEST_ACTIVE_ON) {
            cfg->cam_gzz_cfg.stream_cntrl.val[i] = CAM_STREAM_ON;
        }
    }
    CONFIGURATOR_CONFIG_TOUCH(cfg->cam_gzz_cfg.stream);

    return 0;
}

int camera_external_convert_capture_request_frame_number(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    camera_config_plug_cfg_t *cfg;
    cfg = config;
    cfg->cam_gzz_cfg.frame_number.val =
        (uint32)cfg->external.capture_request_frame_number.v;
    CONFIGURATOR_CONFIG_TOUCH(cfg->cam_gzz_cfg.frame_number);
    return 0;
}

int camera_external_convert_capture_request_guzzi_fr_id(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    camera_config_plug_cfg_t *cfg;
    cfg = config;
    cfg->cam_gzz_cfg.request_id =
        (uint32)cfg->external.capture_request_guzzi_fr_id.v;
    CONFIGURATOR_CONFIG_TOUCH(cfg->cam_gzz_cfg);
    return 0;
}

int camera_external_convert_stream_config_format(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    camera_config_plug_cfg_t *cfg;
    int i;

    cfg = config;

    if (ARRAY_SIZE(cfg->cam_gzz_cfg.stream.val) < META_ARR_SIZE_1(&cfg->external.stream_config_format)) {
        mmsdbg(DL_ERROR, "Too many streams!");
        return -1;
    }

    meta_arr_iterate_dim_1(1, &cfg->external.stream_config_format, i) {
        switch (METAIDX1(&cfg->external.stream_config_format, i)) {
            case GUZZI_CAMERA3_ENUM_STREAM_CONFIG_FORMAT_IMAGE_FORMAT_RAW10:
                cfg->cam_gzz_cfg.stream.val[i].format.compression =
                    HAT_COMPRESSION_NONE;
                cfg->cam_gzz_cfg.stream.val[i].format.bpc =
                    10;
                cfg->cam_gzz_cfg.stream.val[i].format.fmt.order.all =
                    HAT_PORDBYR_Gr_R_B_Gb;
                cfg->cam_gzz_cfg.stream.val[i].format.fmt.xydec =
                    HAT_XYDEC_NO;
                cfg->cam_gzz_cfg.stream.val[i].format.fmt.uvpkd =
                    HAT_UVPKD_PACKED;
                cfg->cam_gzz_cfg.stream.val[i].format.fmt.pkd =
                    HAT_PIXPKD_PACKED;
                cfg->cam_gzz_cfg.stream.val[i].format.fmt.fmt =
                    HAT_PIXCOL_BAYER;
                break;
            case GUZZI_CAMERA3_ENUM_STREAM_CONFIG_FORMAT_IMAGE_FORMAT_UYVY:
                cfg->cam_gzz_cfg.stream.val[i].format.compression =
                    HAT_COMPRESSION_NONE;
                cfg->cam_gzz_cfg.stream.val[i].format.bpc =
                    8;
                cfg->cam_gzz_cfg.stream.val[i].format.fmt.order.all =
                    HAT_PORDYCBCR_U_Y0_V_Y1;
                cfg->cam_gzz_cfg.stream.val[i].format.fmt.xydec =
                    HAT_XYDEC_X;
                cfg->cam_gzz_cfg.stream.val[i].format.fmt.uvpkd =
                    HAT_UVPKD_PACKED;
                cfg->cam_gzz_cfg.stream.val[i].format.fmt.pkd =
                    HAT_PIXPKD_PACKED;
                cfg->cam_gzz_cfg.stream.val[i].format.fmt.fmt =
                    HAT_PIXCOL_YUV;
                break;
            case GUZZI_CAMERA3_ENUM_STREAM_CONFIG_FORMAT_IMAGE_FORMAT_NV12:
                cfg->cam_gzz_cfg.stream.val[i].format.compression =
                    HAT_COMPRESSION_NONE;
                cfg->cam_gzz_cfg.stream.val[i].format.bpc =
                    8;
                cfg->cam_gzz_cfg.stream.val[i].format.fmt.order.all =
                    HAT_PORDYUV_Y_U_V;
                cfg->cam_gzz_cfg.stream.val[i].format.fmt.xydec =
                    HAT_XYDEC_XY;
                cfg->cam_gzz_cfg.stream.val[i].format.fmt.uvpkd =
                    HAT_UVPKD_LINE;
                cfg->cam_gzz_cfg.stream.val[i].format.fmt.pkd =
                    HAT_PIXPKD_PACKED;
                cfg->cam_gzz_cfg.stream.val[i].format.fmt.fmt =
                    HAT_PIXCOL_YUV;
                break;
            default :
                mmsdbg(DL_ERROR, "Too many streams!");
                return -1;
        }
    }
    CONFIGURATOR_CONFIG_TOUCH(cfg->cam_gzz_cfg.stream);

    return 0;
}

int camera_external_convert_stream_config_height(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    camera_config_plug_cfg_t *cfg;
    int i;

    cfg = config;

    if (ARRAY_SIZE(cfg->cam_gzz_cfg.stream.val) < META_ARR_SIZE_1(&cfg->external.stream_config_height)) {
        mmsdbg(DL_ERROR, "Too many streams!");
        return -1;
    }

    meta_arr_iterate_dim_1(1, &cfg->external.stream_config_height, i) {
        cfg->cam_gzz_cfg.stream.val[i].size.h =
            (uint32)METAIDX1(&cfg->external.stream_config_height, i);
    }
    CONFIGURATOR_CONFIG_TOUCH(cfg->cam_gzz_cfg.stream);

    return 0;
}

int camera_external_convert_stream_config_id(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    camera_config_plug_cfg_t *cfg;
    int i;

    cfg = config;

    if (ARRAY_SIZE(cfg->cam_gzz_cfg.stream.val) < META_ARR_SIZE_1(&cfg->external.stream_config_id)) {
        mmsdbg(DL_ERROR, "Too many streams!");
        return -1;
    }

    meta_arr_iterate_dim_1(1, &cfg->external.stream_config_id, i) {
        cfg->cam_gzz_cfg.stream.val[i].id =
            (uint32)METAIDX1(&cfg->external.stream_config_id, i);
    }
    CONFIGURATOR_CONFIG_TOUCH(cfg->cam_gzz_cfg.stream);

    return 0;
}

int camera_external_convert_stream_config_type(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    camera_config_plug_cfg_t *cfg;
    int i;

    cfg = config;

    if (ARRAY_SIZE(cfg->cam_gzz_cfg.stream.val) < META_ARR_SIZE_1(&cfg->external.stream_config_type)) {
        mmsdbg(DL_ERROR, "Too many streams!");
        return -1;
    }

    meta_arr_iterate_dim_1(1, &cfg->external.stream_config_type, i) {
        cfg->cam_gzz_cfg.stream.val[i].type =
            (uint32)METAIDX1(&cfg->external.stream_config_type, i);
    }
    CONFIGURATOR_CONFIG_TOUCH(cfg->cam_gzz_cfg.stream);

    return 0;
}

int camera_external_convert_stream_config_width(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    camera_config_plug_cfg_t *cfg;
    int i;

    cfg = config;

    if (ARRAY_SIZE(cfg->cam_gzz_cfg.stream.val) < META_ARR_SIZE_1(&cfg->external.stream_config_width)) {
        mmsdbg(DL_ERROR, "Too many streams!");
        return -1;
    }

    meta_arr_iterate_dim_1(1, &cfg->external.stream_config_width, i) {
        cfg->cam_gzz_cfg.stream.val[i].size.w =
            (uint32)METAIDX1(&cfg->external.stream_config_width, i);
    }
    CONFIGURATOR_CONFIG_TOUCH(cfg->cam_gzz_cfg.stream);

    return 0;
}

int camera_external_convert_z_custom_capture_number_shots(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    camera_config_plug_cfg_t *cfg;

    cfg = config;

//    mmsdbg(DL_ERROR, "Function \"%s()\"!", __FUNCTION__);

    cfg->cam_gzz_cfg.custom_capture_number_shots.val =
            (uint32) cfg->external.z_custom_capture_number_shots.v;

    CONFIGURATOR_CONFIG_TOUCH(cfg->cam_gzz_cfg.custom_capture_number_shots);

    return 0;
}

int camera_external_convert_z_custom_capture_mode_selection(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    camera_config_plug_cfg_t *cfg;

    cfg = config;

//    mmsdbg(DL_ERROR, "Function \"%s()\"!", __FUNCTION__);

    guzzi_camera3_enum_z_custom_capture_mode_selection_t mode =
            (guzzi_camera3_enum_z_custom_capture_mode_selection_t) cfg->external.z_custom_capture_mode_selection.v;

    switch (mode)
    {
        case GUZZI_CAMERA3_ENUM_Z_CUSTOM_CAPTURE_MODE_SELECTION_TEMPORAL_BRACKETING:
            cfg->cam_gzz_cfg.custom_capture_mode_selection.val = CAM_CUSTOM_CAPTURE_MODE_SELECTION_TEMPORAL_BRACKETING;
            break;
        case GUZZI_CAMERA3_ENUM_Z_CUSTOM_CAPTURE_MODE_SELECTION_EXPOSURE_BRACKETING:
            cfg->cam_gzz_cfg.custom_capture_mode_selection.val = CAM_CUSTOM_CAPTURE_MODE_SELECTION_EXPOSURE_BRACKETING;
            break;
        case GUZZI_CAMERA3_ENUM_Z_CUSTOM_CAPTURE_MODE_SELECTION_LONG_EXPOSURE:
            cfg->cam_gzz_cfg.custom_capture_mode_selection.val = CAM_CUSTOM_CAPTURE_MODE_SELECTION_LONG_EXPOSURE;
            break;
        default:
            mmsdbg(DL_ERROR, "ERROR in %s: Invalid mode selection %d!", __FUNCTION__, mode);
    }

    CONFIGURATOR_CONFIG_TOUCH(cfg->cam_gzz_cfg.custom_capture_mode_selection);
    return 0;
}


int camera_external_convert_z_custom_exposure_bracketing_sequence(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    camera_config_plug_cfg_t *cfg;
    int i;

    cfg = config;

//    mmsdbg(DL_ERROR, "Function \"%s()\"!", __FUNCTION__);

    if (ARRAY_SIZE(cfg->cam_gzz_cfg.custom_exposure_bracketing_sequence.val) < META_ARR_SIZE_1(&cfg->external.z_custom_exposure_bracketing_sequence)) {
        mmsdbg(DL_ERROR, "Too many streams!");
        return -1;
    }

    for (i = 0; i < META_ARR_SIZE_1(&cfg->external.z_custom_exposure_bracketing_sequence); i++) {
        cfg->cam_gzz_cfg.custom_exposure_bracketing_sequence.val[i] =
                convert_exposure_compensation_to_internal(cfg->external.z_custom_exposure_bracketing_sequence.v[i]);
    }
    CONFIGURATOR_CONFIG_TOUCH(cfg->cam_gzz_cfg.custom_exposure_bracketing_sequence);

    return 0;
}

int camera_external_convert_z_custom_usecase_selection(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    camera_config_plug_cfg_t *cfg;

    cfg = config;

//    mmsdbg(DL_ERROR, "Function \"%s()\"!", __FUNCTION__);

    guzzi_camera3_enum_z_custom_usecase_selection_t usecase =
            (guzzi_camera3_enum_z_custom_usecase_selection_t) cfg->external.z_custom_usecase_selection.v;

    switch (usecase)
    {
        case GUZZI_CAMERA3_ENUM_Z_CUSTOM_USECASE_SELECTION_STILL:
            cfg->cam_gzz_cfg.custom_usecase_selection.val = CAM_CUSTOM_USECASE_SELECTION_STILL;
            break;

        case GUZZI_CAMERA3_ENUM_Z_CUSTOM_USECASE_SELECTION_VIDEO:
            cfg->cam_gzz_cfg.custom_usecase_selection.val = CAM_CUSTOM_USECASE_SELECTION_VIDEO;
            break;

        case GUZZI_CAMERA3_ENUM_Z_CUSTOM_USECASE_SELECTION_LOW_POWER_VIDEO:
            cfg->cam_gzz_cfg.custom_usecase_selection.val = CAM_CUSTOM_USECASE_SELECTION_LOW_POWER_VIDEO;
            break;
        default:
            mmsdbg(DL_ERROR, "ERROR in %s: Invalid usecase selection %d!", __FUNCTION__, usecase);
    }

    CONFIGURATOR_CONFIG_TOUCH(cfg->cam_gzz_cfg.custom_usecase_selection);

    return 0;
}

int camera_external_convert_z_custom_crtl_brigthness(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    camera_config_plug_cfg_t *cfg = config;

    cfg->cam_gzz_cfg.brightness.val = (float)cfg->external.z_custom_crtl_brigthness.v / 10.0;
    CONFIGURATOR_CONFIG_TOUCH(cfg->cam_gzz_cfg.brightness);

    return 0;
}

int camera_external_convert_z_custom_crtl_contrast(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    camera_config_plug_cfg_t *cfg = config;

    cfg->cam_gzz_cfg.contrast.val = (float)cfg->external.z_custom_crtl_contrast.v / 10.0;
    CONFIGURATOR_CONFIG_TOUCH(cfg->cam_gzz_cfg.contrast);

    return 0;
}

int camera_external_convert_z_custom_crtl_saturation(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    camera_config_plug_cfg_t *cfg = config;

    cfg->cam_gzz_cfg.saturation.val = (float)cfg->external.z_custom_crtl_saturation.v / 10.0;
    CONFIGURATOR_CONFIG_TOUCH(cfg->cam_gzz_cfg.saturation);

    return 0;
}


int camera_external_convert_z_exposure_merger_weight(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    camera_config_plug_cfg_t *cfg;
    cfg = config;
    cfg->cam_gzz_cfg.exposure_merger.val =
        (uint32)cfg->external.z_exposure_merger_weight.v;
    CONFIGURATOR_CONFIG_TOUCH(cfg->cam_gzz_cfg.exposure_merger);
    return 0;
}

int camera_external_convert_z_white_balance_merger_weight(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    camera_config_plug_cfg_t *cfg;
    cfg = config;
    cfg->cam_gzz_cfg.white_balance_merger.val =
        (uint32)cfg->external.z_white_balance_merger_weight.v;
    CONFIGURATOR_CONFIG_TOUCH(cfg->cam_gzz_cfg.white_balance_merger);
    return 0;
}

int camera_external_convert_z_custom_crtl_hue(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    camera_config_plug_cfg_t *cfg = config;

    cfg->cam_gzz_cfg.hue.val = (float)cfg->external.z_custom_crtl_hue.v / 100;
    CONFIGURATOR_CONFIG_TOUCH(cfg->cam_gzz_cfg.hue);

    return 0;
}

int camera_external_convert_z_wb_manual_temperature(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    camera_config_plug_cfg_t *cfg = config;

    cfg->cam_gzz_cfg.manual_color_temp.val = cfg->external.z_wb_manual_temperature.v;
    CONFIGURATOR_CONFIG_TOUCH(cfg->cam_gzz_cfg.manual_color_temp);
    return 0;
}


