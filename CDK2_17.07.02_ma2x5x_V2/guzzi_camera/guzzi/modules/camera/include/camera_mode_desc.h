/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file camera_mode_desc.h
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#ifndef _CAMERA_MODE_DESC_H
#define _CAMERA_MODE_DESC_H

#include <pipe/include/pipe_desc.h>

#define CM_PORTS_MAX 32
#define CM_PIPES_MAX 32

/* Port descriptor */
#define CM_PIPE_PORT_ENTRY(PORT, WAIT, SND_EVENT, FR_POS) \
    { \
        .port = PORT, \
        .wait = WAIT, \
        .send_back_event = SND_EVENT, \
        .pos_in_fr = FR_POS \
    },

#define CM_PIPE_PORT_NIL_ENTRY() \
    { \
        .port = -1, \
        .wait = -1, \
        .send_back_event = 0xBAD0, \
        .pos_in_fr = -1 \
    }

#define CM_PIPE_PORT_IS_NIL_ENTRY(E) \
    ( \
           ((E)->port == -1) \
        && ((E)->wait == -1) \
        && ((E)->send_back_event == 0xBAD0) \
        && ((E)->pos_in_fr == -1) \
    )

#define CM_PIPE_PORT_FOR_EACH(E,PIPE_PORT) \
        for ((E) = (PIPE_PORT); !CM_PIPE_PORT_IS_NIL_ENTRY(E); (E)++)

typedef struct {
    int port;
    int wait;
    unsigned int send_back_event;
    int pos_in_fr;
} cm_port_desc_t;

/* Pipe descriptor */
#define CM_PIPE_DESC_BUILD_BEGIN(PIPE_DESC, EVENT, FR_NUM) \
    { \
        .fr_num = FR_NUM, \
        .pipe_desc = &PIPE_DESC, \
        .last_event = EVENT, \
        .port_desc = {

#define CM_PIPE_DESC_BUILD_END() \
            CM_PIPE_PORT_NIL_ENTRY() \
        } /* end of port_desc */ \
    },

#define CM_PIPE_DESC_NIL() \
    {\
        .fr_num = -1, \
        .last_event = 0xBAD0 \
    }

#define CM_PIPE_DESC_IS_NIL(E) \
    ( \
           ((E)->fr_num == -1) \
        && ((E)->last_event == 0xBAD0) \
    )

#define CM_PIPE_DESC_FOR_EACH(N,PIPE_DESC) \
        for ((N) = (PIPE_DESC); !CM_PIPE_DESC_IS_NIL(N); (N)++)

typedef struct {
    int fr_num;
    pipe_desc_t *pipe_desc;
    unsigned int last_event;
    cm_port_desc_t port_desc[CM_PORTS_MAX];
} cm_pipe_desc_t;

/* Mode descriptor */
#define CM_DESC_BUILD_BEGIN(NAME) \
    static cm_mode_desc_t NAME = { \
        .pipe_desc = {

#define CM_DESC_BUILD_END(NAME) \
            CM_PIPE_DESC_NIL() \
        } /* end of pipe_desc */ \
    };
typedef struct {
    cm_pipe_desc_t pipe_desc[CM_PIPES_MAX];
} cm_mode_desc_t;

#endif /* _CAMERA_MODE_DESC_H */

