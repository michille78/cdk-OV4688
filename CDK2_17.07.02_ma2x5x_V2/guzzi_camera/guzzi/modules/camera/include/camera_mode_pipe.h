/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file camera_mode_pipe.h
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#ifndef _CAMERA_MODE_PIPE_H
#define _CAMERA_MODE_PIPE_H

#include <osal/osal_mutex.h>
#include <osal/osal_list.h>

#include <framerequest/camera/camera_frame_request.h>
#include <configurator/include/configurator.h>
#include <pipe/include/pipe_event_id.h>
#include <pipe/include/pipe.h>

#include <camera_mode.h>
#include <camera_mode_desc.h>
#include <camera_mode_fr.h>
#include <camera_mode_pipe.h>

typedef struct cm_pipe cm_pipe_t;

typedef struct {
    struct list_head link_ready;
    cm_pipe_t *cm_pipe;
    cm_pipe_desc_t *desc;
    cm_fr_t *cm_fr;
    pipe_t *pipe;
    osal_sem *event_sem;
    int start_err;
    camera_mode_prv_t prv;
} cm_pipe_info_t;

typedef camera_fr_t * cm_pipe_info_fr_duplicate_t(
        cm_fr_t *cm_fr,
        cm_pipe_info_t *cm_pipe_info,
        void *prv,
        camera_fr_t *fr_src
    );

cm_pipe_info_t *cm_pipe_info_get(cm_pipe_t *cm_pipe);
int cm_pipe_info_size_get(cm_pipe_t *cm_pipe);

cm_pipe_info_t *cm_pipe_desc2info(cm_pipe_t *cm_pipe, pipe_desc_t *pipe_desc);

int cm_pipe_ready_to_process_single_by_info(cm_pipe_info_t *cm_pipe_info);
int cm_pipe_ready_to_process_single(
        cm_pipe_t *cm_pipe,
        pipe_desc_t *pipe_desc
    );
void cm_pipe_ready_to_process(cm_pipe_t *cm_pipe, struct list_head *list);

int cm_pipe_count_descs(cm_pipe_desc_t *desc);

void cm_handle_port_entries_on_inc_event(
        cm_pipe_info_t *cm_pipe_info,
        camera_fr_t *fr,
        unsigned int event
    );
void cm_pipe_callback(
        pipe_t *pipe,
        void *app_prv,
        pipe_event_id_t event_id,
        int num,
        void *ptr
    );

int cm_pipe_start_single_by_info(cm_pipe_info_t *cm_pipe_info, void *res);
void cm_pipe_stop_single_by_info(cm_pipe_info_t *cm_pipe_info);
void cm_pipe_flush_single_by_info(cm_pipe_info_t *cm_pipe_info);

int cm_pipe_start_single(cm_pipe_t *cm_pipe, pipe_desc_t *pipe_desc, void *res);
void cm_pipe_stop_single(cm_pipe_t *cm_pipe, pipe_desc_t *pipe_desc);
void cm_pipe_flush_single(cm_pipe_t *cm_pipe, pipe_desc_t *pipe_desc);

int cm_pipe_start_all(cm_pipe_t *cm_pipe, void *res);
void cm_pipe_stop_all(cm_pipe_t *cm_pipe);
void cm_pipe_flush_all(cm_pipe_t *cm_pipe);

void cm_pipe_destroy(cm_pipe_t *cm_pipe);
cm_pipe_t *cm_pipe_create(
        camera_t *camera,
        configurator_t *configurator,
        cm_pipe_desc_t *desc,
        void *res,
        cm_pipe_info_fr_duplicate_t *fr_duplicate,
        pipe_callback_t callback,
        void *prv
    );

#endif /* _CAMERA_MODE_PIPE_H */

