/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file camera_mode.h
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#ifndef _CAMERA_MODE_H
#define _CAMERA_MODE_H

#include <osal/osal_stdio.h>

#include <configurator/include/configurator.h>
#include <camera_internal.h>
#include <camera_mode_prv.h>

#define CAMERA_MODE_TABLE_ENTRY(MODE,FUNC) \
    { \
        .name = #MODE, \
        .mode = MODE, \
        .create_func = FUNC \
    },

#define CAMERA_MODE_TABLE_ENTRY_LAST \
    { \
        .name = NULL, \
        .mode = CAMERA_MODE_MAX, \
        .create_func = NULL \
    }

#define CAMERA_MODE_TABLE_ENTRY_IS_LAST(E) \
    ( \
           ((E)->name == NULL) \
        && ((E)->mode == CAMERA_MODE_MAX) \
        && ((E)->create_func == NULL) \
    )

typedef struct camera_mode camera_mode_t;

typedef struct {
    camera_modes_t mode;
    camera_t *camera;
    configurator_t *configurator;
} camera_mode_params_t;

typedef int camera_mode_create_func_t(
        camera_mode_t *cm,
        camera_mode_params_t *params
    );

typedef struct {
    char *name;
    camera_modes_t mode;
    camera_mode_create_func_t *create_func;
} camera_mode_table_entry_t;

struct camera_mode {
    char *name;
    camera_mode_prv_t prv;
    int (*mode_start)(camera_mode_t *cm);
    void (*mode_stop)(camera_mode_t *cm);
    void (*mode_flush)(camera_mode_t *cm);
    int (*mode_process)(camera_mode_t *cm);
    void (*mode_destroy)(camera_mode_t *cm);
};

int camera_mode_start(camera_mode_t *cm);
void camera_mode_stop(camera_mode_t *cm);
void camera_mode_flush(camera_mode_t *cm);
int camera_mode_process(camera_mode_t *cm);
void camera_mode_destroy(camera_mode_t *cm);
camera_mode_t *camera_mode_create(camera_mode_params_t *params);

#endif /* _CAMERA_MODE_H */

