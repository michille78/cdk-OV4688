/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file camera_scene_effect_cfg.h
*
* @Pavlin Georgiev ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 01-Jul-2014 : Pavlin Georgiev ( MM Solutions AD )
*! Created
* =========================================================================== */

#ifndef CAMERA_SCENE_EFFECT_CFG_H_
#define CAMERA_SCENE_EFFECT_CFG_H_

#include "dtp/dtp_server_defs.h"

typedef struct {
    dtp_hndl_t hdtp;
    dtp_leaf_data_t leaf;
    dtpdb_dynamic_private_t dtp_dkeys;
} scene_cfg_data_t;

typedef struct {
    dtp_hndl_t hdtp;
    dtp_leaf_data_t leaf;
    dtpdb_dynamic_private_t dtp_dkeys;
} effect_cfg_data_t;

#endif /* CAMERA_SCENE_EFFECT_CFG_H_ */
