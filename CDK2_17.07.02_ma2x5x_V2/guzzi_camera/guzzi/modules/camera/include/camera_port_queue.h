/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file camera_port_queue.h
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#ifndef _CAMERA_PORT_QUEUE_H
#define _CAMERA_PORT_QUEUE_H

typedef struct camera_port_queue camera_port_queue_t;

typedef struct {
    int num_of_ports;
    int num_of_buffs;
} camera_port_queue_params_t;

int camera_port_queue_req_deq(camera_port_queue_t *pq, int port, void **buf);
int camera_port_queue_req_enq(camera_port_queue_t *pq, int port, void * buf);
int camera_port_queue_req_is_empty(camera_port_queue_t *pq, int port);

int camera_port_queue_rdy_deq(camera_port_queue_t *pq, int port, void **buf);
int camera_port_queue_rdy_enq(camera_port_queue_t *pq, int port, void * buf);
int camera_port_queue_rdy_is_empty(camera_port_queue_t *pq, int port);

void camera_port_queue_destroy(camera_port_queue_t *pq);
camera_port_queue_t * camera_port_queue_create(camera_port_queue_params_t *params);

#endif /* _CAMERA_PORT_QUEUE_H */

