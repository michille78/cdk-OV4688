/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file camera_mode_fr.h
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#ifndef _CAMERA_MODE_FR_H
#define _CAMERA_MODE_FR_H

#include <framerequest/camera/camera_frame_request.h>
#include <camera_internal.h>

typedef struct cm_fr cm_fr_t;

typedef camera_fr_t * cm_fr_duplicate_t(
        cm_fr_t *cm_fr,
        void *prv,
        camera_fr_t *fr_src
    );

void cm_fr_port_entry_set(camera_fr_t *fr, int n, int port);
camera_fr_t *cm_fr_make(cm_fr_t *cm_fr);

void cm_fr_process_done_on_port_entry(camera_fr_t *fr, int n);

void cm_fr_entry_flush(camera_fr_t *fr, int n);
void cm_fr_flush(camera_fr_t *fr);

void cm_fr_entry_destroy(camera_fr_entry_t *fr_entry);
void cm_fr_destroy(camera_fr_t *fr);

void cm_flushed_fr_list_add_unique(camera_fr_t *fr);
void cm_flushed_fr_list_for_each(
        cm_fr_t *cm_fr,
        void (*func)(camera_fr_t *fr)
    );
void cm_fr_list_elem_flush(camera_fr_t *fr);
void cm_fr_list_elem_free(camera_fr_t *fr);

void cm_fr_module_destroy(cm_fr_t *cm_fr);
cm_fr_t *cm_fr_module_create(
        camera_t *camera,
        int num,
        cm_fr_duplicate_t *fr_duplicate,
        void *prv
    );

#endif /* _CAMERA_MODE_FR_H */

