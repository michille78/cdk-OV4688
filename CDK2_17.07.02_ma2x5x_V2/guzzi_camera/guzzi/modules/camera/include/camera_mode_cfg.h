/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file camera_mode_cfg.h
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#ifndef _CAMERA_MODE_CFG_H
#define _CAMERA_MODE_CFG_H

#include <configurator/include/configurator.h>
#include <cam_config.h>

typedef struct cm_cfg cm_cfg_t;

typedef int cm_cfg_alter_func_t(cm_cfg_t *cm_cfg, void *prv, void *config);
typedef void cm_cfg_notify_func_t(cm_cfg_t *cm_cfg, void *prv);

cam_cfg_t *cm_cfg_get_cur(cm_cfg_t *cm_cfg);
void cm_cfg_destroy(cm_cfg_t *cm_cfg);
cm_cfg_t *cm_cfg_create(
        configurator_t *configurator,
        cm_cfg_alter_func_t *alter_func,
        cm_cfg_notify_func_t *notify_func,
        void *prv
    );

#endif /* _CAMERA_MODE_CFG_H */

