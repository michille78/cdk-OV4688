/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file camera_port_queue.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <osal/osal_stdlib.h>
#include <osal/osal_mutex.h>
#include <osal/osal_list.h>
#include <utils/mms_debug.h>
#include <osal/pool.h>
#include <camera_port_queue.h>

#define ALLOC_TIMEOUT DEFAULT_ALLOC_TIMEOUT_MS /* ms */

typedef struct {
    struct list_head link;
    void *buf;
} port_qu_elem_t;

typedef struct {
    struct list_head list;
    osal_mutex *lock;
    int count;
} port_qu_t;

struct camera_port_queue {
    int num_of_ports;
    int num_of_buffs;
    port_qu_t **req_qu;
    port_qu_t **rdy_qu;
    pool_t *elems_pool;
};

mmsdbg_define_variable(
        vdl_camera_port_queue,
        DL_DEFAULT,
        0,
        "vdl_camera_port_queue",
        "Module to handle camera ports."
    );
#define MMSDEBUGLEVEL mmsdbg_use_variable(vdl_camera_port_queue)

/* ========================================================================== */
/**
* port_q_create()
*/
/* ========================================================================== */
static port_qu_t * port_q_create(void)
{
    port_qu_t *q;

    q = osal_malloc(sizeof (*q));
    if (!q) {
        mmsdbg(
                DL_ERROR,
                "Failed to allocate new port queue: size=%d!",
                sizeof (*q)
            );
        goto exit1;
    }

    q->lock = osal_mutex_create();
    if (!q->lock) {
        mmsdbg(
                DL_ERROR,
                "Failed to create mutex for port queue!"
            );
        goto exit2;
    }

    INIT_LIST_HEAD(&q->list);
    q->count = 0;

    return q;
exit2:
    osal_free(q);
exit1:
    return NULL;
}

/* ========================================================================== */
/**
* port_q_destroy()
*/
/* ========================================================================== */
static void port_q_destroy(port_qu_t *q)
{
    osal_mutex_destroy(q->lock);
    osal_free(q);
}

/* ========================================================================== */
/**
* port_q_array_create()
*/
/* ========================================================================== */
static port_qu_t ** port_q_array_create(int n)
{
    port_qu_t **q_arr;
    int i;

    q_arr = osal_malloc(n * sizeof (*q_arr));
    if (!q_arr) {
        mmsdbg(
                DL_ERROR,
                "Failed to allocate new port queue array: size=%d!",
                n * sizeof (*q_arr)
            );
        goto exit1;
    }

    for (i = 0; i < n; i++) {
        q_arr[i] = port_q_create();
        if (!q_arr[i]) {
            mmsdbg(DL_ERROR, "Failed to crete port queue!");
            for (i--; i; i--) port_q_destroy(q_arr[i]);
            goto exit2;
        }
    }

    return q_arr;
exit2:
    osal_free(q_arr);
exit1:
    return NULL;
}

/* ========================================================================== */
/**
* port_q_array_destroy()
*/
/* ========================================================================== */
static void port_q_array_destroy(port_qu_t **q_arr, int n)
{
    int i;
    for (i = 0; i < n; i++) {
        port_q_destroy(q_arr[i]);
    }
    osal_free(q_arr);
}

/* ========================================================================== */
/**
* port_q_deq()
*/
/* ========================================================================== */
static int port_q_deq(camera_port_queue_t *pq, int port, void **buf, port_qu_t **qq)
{
    port_qu_t *q;
    port_qu_elem_t *e;

    if ((port < 0) && (pq->num_of_ports <= port)) {
        mmsdbg(
                DL_ERROR,
                "Expecting port in range [0-%d]. But received request for: %d!",
                pq->num_of_ports-1,
                port
            );
        goto exit1;
    }

    q = qq[port];

    osal_mutex_lock(q->lock);
    if (list_empty(&q->list)) {
        osal_mutex_unlock(q->lock);
        mmsdbg(
                DL_ERROR,
                "Dequeue from empty port (port=%d)!",
                port
            );
        goto exit1;
    }
    e = (port_qu_elem_t *)(q->list.next);
    list_del(&e->link);
    q->count--;
    osal_mutex_unlock(q->lock);

    *buf = e->buf;
    osal_free(e);

    return 0;
exit1:
    return -1;
}

/* ========================================================================== */
/**
* port_q_enq()
*/
/* ========================================================================== */
static int port_q_enq(camera_port_queue_t *pq, int port, void * buf, port_qu_t **qq)
{
    port_qu_t *q;
    port_qu_elem_t *e;

    if ((port < 0) && (pq->num_of_ports <= port)) {
        mmsdbg(
                DL_ERROR,
                "Expecting port in range [0-%d]. But received request for: %d!",
                pq->num_of_ports-1,
                port
            );
        goto exit1;
    }

    q = qq[port];

    e = pool_alloc_timeout(pq->elems_pool, ALLOC_TIMEOUT);
    if (!e) {
        mmsdbg(
                DL_ERROR,
                "Failed to allocate port queue element (timeout=%d)!",
                ALLOC_TIMEOUT
            )
        goto exit1;
    }
    e->buf = buf;

    osal_mutex_lock(q->lock);
    list_add_tail(&e->link, &q->list);
    q->count++;
    osal_mutex_unlock(q->lock);

    return 0;
exit1:
    return -1;
}

/* ========================================================================== */
/**
* port_q_is_empty()
*/
/* ========================================================================== */
static int port_q_is_empty(camera_port_queue_t *pq, int port, port_qu_t **qq)
{
    if ((port < 0) && (pq->num_of_ports <= port)) {
        mmsdbg(
                DL_ERROR,
                "Expecting port in range [0-%d]. But received request for: %d!",
                pq->num_of_ports-1,
                port
            );
        return -1;
    }
    return list_empty(&qq[port]->list);
}

/*
* ***************************************************************************
* ***************************************************************************
* ** Intrface Part **********************************************************
* ***************************************************************************
* ***************************************************************************
*/

/* ========================================================================== */
/**
* camera_port_queue_req_deq()
*/
/* ========================================================================== */
int camera_port_queue_req_deq(camera_port_queue_t *pq, int port, void **buf)
{
    return port_q_deq(pq, port, buf, pq->req_qu);
}

/* ========================================================================== */
/**
* camera_port_queue_req_enq()
*/
/* ========================================================================== */
int camera_port_queue_req_enq(camera_port_queue_t *pq, int port, void * buf)
{
    return port_q_enq(pq, port, buf, pq->req_qu);
}

/* ========================================================================== */
/**
* camera_port_queue_req_is_empty()
*/
/* ========================================================================== */
int camera_port_queue_req_is_empty(camera_port_queue_t *pq, int port)
{
    return port_q_is_empty(pq, port, pq->req_qu);
}


/* ========================================================================== */
/**
* camera_port_queue_rdy_deq()
*/
/* ========================================================================== */
int camera_port_queue_rdy_deq(camera_port_queue_t *pq, int port, void **buf)
{
    return port_q_deq(pq, port, buf, pq->rdy_qu);
}

/* ========================================================================== */
/**
* camera_port_queue_rdy_enq()
*/
/* ========================================================================== */
int camera_port_queue_rdy_enq(camera_port_queue_t *pq, int port, void * buf)
{
    return port_q_enq(pq, port, buf, pq->rdy_qu);
}

/* ========================================================================== */
/**
* camera_port_queue_rdy_is_empty()
*/
/* ========================================================================== */
int camera_port_queue_rdy_is_empty(camera_port_queue_t *pq, int port)
{
    return port_q_is_empty(pq, port, pq->rdy_qu);
}

/* ========================================================================== */
/**
* camera_port_queue_destroy()
*/
/* ========================================================================== */
void camera_port_queue_destroy(camera_port_queue_t *pq)
{
    pool_destroy(pq->elems_pool);
    port_q_array_destroy(pq->rdy_qu, pq->num_of_ports); /* TODO: store pq->num_of_ports in array */
    port_q_array_destroy(pq->req_qu, pq->num_of_ports);
    osal_free(pq);
}

/* ========================================================================== */
/**
* camera_port_queue_create()
*/
/* ========================================================================== */
camera_port_queue_t * camera_port_queue_create(camera_port_queue_params_t *params)
{
    camera_port_queue_t *pq;

    pq = osal_malloc(sizeof (*pq));
    if (!pq) {
        mmsdbg(
                DL_ERROR,
                "Failed to allocate memory for new instance: size=%d!",
                sizeof (*pq)
            );
        goto exit1;
    }

    pq->num_of_ports = params->num_of_ports;
    pq->num_of_buffs = params->num_of_buffs;

    pq->req_qu = port_q_array_create(pq->num_of_ports);
    if (!pq->req_qu) {
        mmsdbg(DL_ERROR, "Failed to create request port queue array!");
        goto exit2;
    }

    pq->rdy_qu = port_q_array_create(pq->num_of_ports);
    if (!pq->rdy_qu) {
        mmsdbg(DL_ERROR, "Failed to create ready port queue array!");
        goto exit3;
    }

    /* TODO: Use osal pool if available */
    pq->elems_pool = pool_create("PORT POOL", sizeof (port_qu_elem_t), pq->num_of_buffs);
    if (!pq->elems_pool) {
        mmsdbg(DL_ERROR, "Failed to create pool for port queue!");
        goto exit4;
    }

    return pq;
exit4:
    port_q_array_destroy(pq->rdy_qu, pq->num_of_ports);
exit3:
    port_q_array_destroy(pq->req_qu, pq->num_of_ports);
exit2:
    osal_free(pq);
exit1:
    return NULL;
}

