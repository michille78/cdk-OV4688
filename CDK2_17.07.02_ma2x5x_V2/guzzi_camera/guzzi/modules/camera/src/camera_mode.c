/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file camera_mode.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <osal/osal_stdlib.h>
#include <utils/mms_debug.h>

#include <camera_mode.h>

mmsdbg_define_variable(
        vdl_camera_mode,
        DL_DEFAULT,
        0,
        "vdl_camera_mode",
        "Camera Mode."
    );
#define MMSDEBUGLEVEL mmsdbg_use_variable(vdl_camera_mode)

extern camera_mode_table_entry_t camera_mode_table[];

static camera_mode_table_entry_t * find_mode_entry(
        camera_mode_table_entry_t *table,
        camera_modes_t mode
    )
{
    camera_mode_table_entry_t *e;

    for (e = table; !CAMERA_MODE_TABLE_ENTRY_IS_LAST(e); e++) {
        if (e->mode == mode) {
            return e;
        }
    }

    return NULL;
}

/*
* ***************************************************************************
* ** Intrface Part **********************************************************
* ***************************************************************************
*/
int camera_mode_start(camera_mode_t *cm)
{
    if (cm->mode_start) {
        return cm->mode_start(cm);
    }
    return 0;
}

void camera_mode_stop(camera_mode_t *cm)
{
    if (cm->mode_stop) {
        cm->mode_stop(cm);
    }
}

void camera_mode_flush(camera_mode_t *cm)
{
    if (cm->mode_flush) {
        cm->mode_flush(cm);
    }
}

int camera_mode_process(camera_mode_t *cm)
{
    if (cm->mode_process) {
        return cm->mode_process(cm);
    }
    return 0;
}

void camera_mode_destroy(camera_mode_t *cm)
{
    if (cm->mode_destroy) {
        cm->mode_destroy(cm);
    }
    osal_free(cm);
}

camera_mode_t *camera_mode_create(camera_mode_params_t *params)
{
    camera_mode_t *cm;
    camera_mode_table_entry_t *e;
    int err;

    e = find_mode_entry(camera_mode_table, params->mode);
    if (!e) {
        mmsdbg(DL_ERROR, "Unable to find Camera Mode: 0x%x!", params->mode);
        goto exit1;
    }

    cm = osal_calloc(1, sizeof (*cm));
    if (!cm) {
        mmsdbg(
                DL_ERROR,
                "Failed to allocate memory for new "
                "Camera Mode instance: size=%d!",
                sizeof (*cm)
            );
        goto exit1;
    }
    cm->name = e->name;

    err = e->create_func(cm, params);
    if (err) {
        mmsdbg(
                DL_ERROR,
                "Failed to create Camera Mode: %s!",
                cm->name
            );
        goto exit2;
    }

    return cm;
exit2:
    osal_free(cm);
exit1:
    return NULL;
}

