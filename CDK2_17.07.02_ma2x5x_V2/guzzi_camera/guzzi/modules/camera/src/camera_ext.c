/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file camera_ext.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <osal/osal_stdlib.h>
#include <utils/mms_debug.h>

#include <configurator/include/configurator.h>
#include <camera.h>
#include <camera_internal.h>
#include <camera_ext.h>
#include <camera_mode.h>

struct camera_ext {
    camera_t *camera;
    configurator_t *configurator;
    camera_mode_t *camera_mode;
};

mmsdbg_define_variable(
        vdl_camera_ext,
        DL_DEFAULT,
        0,
        "vdl_camera_ext",
        "Camera component extension."
    );
#define MMSDEBUGLEVEL mmsdbg_use_variable(vdl_camera_ext)

int camera_ext_start(camera_ext_t *camera_ext)
{
    int err;
    err = camera_mode_start(camera_ext->camera_mode);
    if (err) {
        mmsdbg(DL_ERROR, "Failed to start camera mode!");
    }
    return err;
}

void camera_ext_stop(camera_ext_t *camera_ext)
{
    camera_mode_stop(camera_ext->camera_mode);
}

void camera_ext_flush(camera_ext_t *camera_ext)
{
    camera_mode_flush(camera_ext->camera_mode);
}

int camera_ext_process(camera_ext_t *camera_ext)
{
    return camera_mode_process(camera_ext->camera_mode);
}

void camera_ext_destroy(camera_ext_t *camera_ext)
{
    camera_mode_destroy(camera_ext->camera_mode);
    osal_free(camera_ext);
}

camera_ext_t *camera_ext_create(camera_ext_create_params_t *params)
{
    camera_ext_t *camera_ext;
    camera_mode_params_t camera_mode_params;

    camera_ext = osal_malloc(sizeof(*camera_ext));
    if (!camera_ext) {
        mmsdbg(
                DL_ERROR,
                "Failed to allocate memory for new camera extension instance: size=%d!",
                sizeof (*camera_ext)
            );
        goto exit1;
    }

    camera_ext->camera = params->camera;
    camera_ext->configurator = params->configurator;

    camera_mode_params.mode = params->mode;
    camera_mode_params.camera = camera_ext->camera;
    camera_mode_params.configurator = camera_ext->configurator;
    camera_ext->camera_mode = camera_mode_create(&camera_mode_params);
    if (!camera_ext->camera_mode) {
        mmsdbg(DL_ERROR, "Failed to create camera mode!");
        goto exit2;
    }

    return camera_ext;
exit2:
    osal_free(camera_ext);
exit1:
    return NULL;
}

