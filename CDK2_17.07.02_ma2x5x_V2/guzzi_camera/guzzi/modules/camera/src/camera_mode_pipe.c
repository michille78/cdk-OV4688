/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file camera_mode_pipe.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <osal/osal_stdlib.h>
#include <osal/osal_mutex.h>
#include <osal/osal_assert.h>
#include <osal/osal_list.h>
#include <utils/mms_debug.h>

#include <framerequest/camera/camera_frame_request.h>
#include <configurator/include/configurator.h>
#include <pipe/include/pipe_desc.h>
#include <pipe/include/pipe_event_id.h>
#include <pipe/include/pipe.h>

#include <camera_mode.h>
#include <camera_mode_fr.h>
#include <camera_mode_pipe.h>

#define CM_MOV_TIMEOUT DEFAULT_ALLOC_TIMEOUT_MS
#define MAX_PIPES 8

struct cm_pipe {
    camera_t *camera;
    configurator_t *configurator;
    cm_pipe_desc_t *desc;
    void *res;
    cm_pipe_info_fr_duplicate_t *fr_duplicate;
    pipe_callback_t callback;
    void *prv;
    cm_pipe_info_t *pipe_info_arr;
    int pipe_info_arr_size;
};

mmsdbg_define_variable(
        vdl_camera_mode_pipe,
        DL_DEFAULT,
        0,
        "vdl_camera_mode_pipe",
        "Common Pipe routines used in camera mode."
    );
#define MMSDEBUGLEVEL mmsdbg_use_variable(vdl_camera_mode_pipe)

/*
 * *****************************************************************************
 * ** Frame request duplication - propagate adding pipe info *******************
 * *****************************************************************************
 */
static camera_fr_t *pipe_info_fr_duplicate(
        cm_fr_t *cm_fr,
        void *prv,
        camera_fr_t *fr_src
    )
{
    cm_pipe_t *cm_pipe;
    cm_pipe_info_t *cm_pipe_info;

    cm_pipe_info = prv;
    cm_pipe = cm_pipe_info->cm_pipe;

    if (!cm_pipe->fr_duplicate) {
        mmsdbg(
                DL_ERROR,
                "Frame request duplication is "
                "not supported in this usecase!"
            );
        return NULL;
    }

    return cm_pipe->fr_duplicate(
            cm_pipe_info->cm_fr,
            cm_pipe_info,
            cm_pipe->prv,
            fr_src
        );
}

/*
* ***************************************************************************
* ** Pipe module pipe_info create/destroy ***********************************
* ***************************************************************************
*/
static void pipe_info_destroy_single(cm_pipe_info_t *cm_pipe_info)
{
    pipe_destroy(cm_pipe_info->pipe);
    osal_sem_destroy(cm_pipe_info->event_sem);
    cm_fr_module_destroy(cm_pipe_info->cm_fr);
}

static int pipe_info_create_single(
        cm_pipe_t *cm_pipe,
        cm_pipe_info_t *cm_pipe_info,
        cm_pipe_desc_t *desc
    )
{
    pipe_create_params_t pipe_create_params;

    cm_pipe_info->cm_pipe = cm_pipe;
    cm_pipe_info->desc = desc;
    cm_pipe_info->start_err = 0;
    cm_pipe_info->prv.ptr = cm_pipe->prv;

    cm_pipe_info->cm_fr = cm_fr_module_create(
            cm_pipe->camera,
            cm_pipe_info->desc->fr_num,
            pipe_info_fr_duplicate,
            cm_pipe_info
        );
    if (!cm_pipe_info->cm_fr) {
        mmsdbg(
                DL_ERROR,
                "Failed to create Camera Mode Pipe Frame Request!"
            );
        goto exit1;
    }

    cm_pipe_info->event_sem = osal_sem_create(0);
    if (!cm_pipe_info->event_sem) {
        mmsdbg(
                DL_ERROR,
                "Failed to create Camera Mode Pipe semaphore!"
            );
        goto exit2;
    }

    pipe_create_params.app_private = cm_pipe_info;
    pipe_create_params.resources = cm_pipe->res;
    pipe_create_params.configurator = cm_pipe->configurator;
    pipe_create_params.callback = cm_pipe->callback;
    cm_pipe_info->pipe = pipe_create(
            cm_pipe_info->desc->pipe_desc,
            &pipe_create_params
        );
    if (!cm_pipe_info->pipe) {
        mmsdbg(DL_ERROR, "Failed to create pipe!");
        goto exit3; /* TODO: */
    }

    return 0;
exit3:
    osal_sem_destroy(cm_pipe_info->event_sem);
exit2:
    cm_fr_module_destroy(cm_pipe_info->cm_fr);
exit1:
    return -1;
}

static void pipe_info_destroy(cm_pipe_t *cm_pipe)
{
    int i;
    for (i = cm_pipe->pipe_info_arr_size - 1; 0 <= i; i--) {
        pipe_info_destroy_single(cm_pipe->pipe_info_arr + i);
    }
}

static int pipe_info_create(cm_pipe_t *cm_pipe)
{
    int i;
    int err;

    for (i = 0; i < cm_pipe->pipe_info_arr_size; i++) {
        err = pipe_info_create_single(
                cm_pipe,
                cm_pipe->pipe_info_arr + i,
                cm_pipe->desc + i
            );
        if (err) {
            mmsdbg(DL_ERROR, "Failed to create Camera Mode Pipe Info!");
            goto exit1;
        }
    }

    return 0;
exit1:
    while (i--) {
        pipe_info_destroy_single(cm_pipe->pipe_info_arr + i);
    }
    return -1;
}

/*
* ***************************************************************************
* ** Pipe module obtain pipe info struct ************************************
* ***************************************************************************
*/
cm_pipe_info_t *cm_pipe_info_get(cm_pipe_t *cm_pipe)
{
    return cm_pipe->pipe_info_arr;
}

int cm_pipe_info_size_get(cm_pipe_t *cm_pipe)
{
    return cm_pipe->pipe_info_arr_size;
}

/*
* ***************************************************************************
* ** Pipe module misc *******************************************************
* ***************************************************************************
*/
cm_pipe_info_t *cm_pipe_desc2info(cm_pipe_t *cm_pipe, pipe_desc_t *pipe_desc)
{
    int i;
    for (i = 0; i < cm_pipe->pipe_info_arr_size; i++) {
        if (cm_pipe->pipe_info_arr[i].desc->pipe_desc == pipe_desc) {
            return cm_pipe->pipe_info_arr + i;
        }
    }
    return 0;
}

int cm_pipe_ready_to_process_single_by_info(cm_pipe_info_t *cm_pipe_info)
{
    cm_port_desc_t *port_desc;
    int port_empty;

    CM_PIPE_PORT_FOR_EACH(port_desc, cm_pipe_info->desc->port_desc) {
        port_empty = camera_i_port_q_req_is_empty(
                cm_pipe_info->cm_pipe->camera,
                port_desc->port
            );
        if (port_desc->wait && port_empty) {
            return 0;
        }
    }

    return 1;
}

int cm_pipe_ready_to_process_single(
        cm_pipe_t *cm_pipe,
        pipe_desc_t *pipe_desc
    )
{
    cm_pipe_info_t *cm_pipe_info;
    cm_pipe_info = cm_pipe_desc2info(cm_pipe, pipe_desc);
    if (!cm_pipe_info) {
        mmsdbg(DL_ERROR, "Unable to find pipe desc in pipe info!");
        return -1; 
    }
    return cm_pipe_ready_to_process_single_by_info(cm_pipe_info);
}

void cm_pipe_ready_to_process(cm_pipe_t *cm_pipe, struct list_head *list)
{
    int i;
    for (i = 0; i < cm_pipe->pipe_info_arr_size; i++) {
        if (cm_pipe_ready_to_process_single_by_info(cm_pipe->pipe_info_arr + i)) {
            list_add(&cm_pipe->pipe_info_arr[i].link_ready, list);
        }
    }
}

int cm_pipe_count_descs(cm_pipe_desc_t *desc)
{
    int i;
    for (i = 0; !CM_PIPE_DESC_IS_NIL(desc + i); i++) {
        if (MAX_PIPES <= i) {
            mmsdbg(
                    DL_ERROR,
                    "Too many pipes in pipe descriptor (max=%d)!",
                    MAX_PIPES
                );
            return -1;
        }
    }
    return i;
}

/*
* ***************************************************************************
* ** Pipe module callback ***************************************************
* ***************************************************************************
*/

void cm_handle_port_entries_on_inc_event(
        cm_pipe_info_t *cm_pipe_info,
        camera_fr_t *fr,
        unsigned int event
    )
{
    cm_port_desc_t *port_desc;
    CM_PIPE_PORT_FOR_EACH(port_desc, cm_pipe_info->desc->port_desc) {
        if (port_desc->send_back_event == event) {
            cm_fr_process_done_on_port_entry(fr, port_desc->pos_in_fr);
        }
    }
}

void cm_pipe_callback(
        pipe_t *pipe,
        void *app_prv,
        pipe_event_id_t event_id,
        int num,
        void *ptr
    )
{
    cm_pipe_t *cm_pipe;
    cm_pipe_info_t *cm_pipe_info;
    camera_fr_t *fr;

    cm_pipe_info = app_prv;
    cm_pipe = cm_pipe_info->cm_pipe;

    switch (event_id.type) {
        case PIPE_EVENT_INC:
            fr = ptr;
            cm_handle_port_entries_on_inc_event(cm_pipe_info, fr, event_id.inc);
            if (cm_pipe_info->desc->last_event == event_id.inc) {
                cm_fr_destroy(fr);
            }
            break;
        case PIPE_EVENT_FLUSHING:
            cm_flushed_fr_list_add_unique(ptr);
            break;
        case PIPE_EVENT_FLUSH_DONE:
            osal_sem_post(cm_pipe_info->event_sem);
            break;
        case PIPE_EVENT_START_DONE:
            osal_sem_post(cm_pipe_info->event_sem);
            break;
        case PIPE_EVENT_START_ERR:
            cm_pipe_info->start_err = -1;
            mmsdbg(
                    DL_ERROR,
                    "Pipe event start error: name=%s!",
                    cm_pipe_info->desc->pipe_desc->name
                );
            osal_sem_post(cm_pipe_info->event_sem);
            break;
        default:
            mmsdbg(
                    DL_ERROR,
                    "Unknown event from Pipe: pipe_name=%s, event=0x%x!",
                    cm_pipe_info->desc->pipe_desc->name,
                    event_id.v
                );
    }
}

/*
* ***************************************************************************
* ** Pipe module start/stop/flush single by info ****************************
* ***************************************************************************
*/
int cm_pipe_start_single_by_info(cm_pipe_info_t *cm_pipe_info, void *res)
{
    cm_pipe_info->start_err = 0;
    int err;

    err = pipe_start(cm_pipe_info->pipe, res);
    if (err) {
        mmsdbg(
                DL_ERROR,
                "Failed to start Pipe: name=%s!",
                cm_pipe_info->desc->pipe_desc->name
            );
        goto exit1;
    }
    err = osal_sem_wait_timeout(
            cm_pipe_info->event_sem,
            CM_MOV_TIMEOUT
        );
    if (err) { /* TODO: Use abort/exit call */
        mmsdbg(
                DL_ERROR,
                "Pipe start timeout: pipe_name=%s, timeout=%dms!",
                cm_pipe_info->desc->pipe_desc->name,
                CM_MOV_TIMEOUT
            );
    }
    osal_assert(!err);
    if (cm_pipe_info->start_err) {
        mmsdbg(
                DL_ERROR,
                "Failed to start Pipe: name=%s!",
                cm_pipe_info->desc->pipe_desc->name
            );
        goto exit1;
    }

    return 0;
exit1:
    return -1;
}

void cm_pipe_stop_single_by_info(cm_pipe_info_t *cm_pipe_info)
{
    pipe_stop(cm_pipe_info->pipe);
}

void cm_pipe_flush_single_by_info(cm_pipe_info_t *cm_pipe_info)
{
    int err;

    pipe_flush(cm_pipe_info->pipe);
    err = osal_sem_wait_timeout(cm_pipe_info->event_sem, CM_MOV_TIMEOUT);
    if (err) { /* TODO: Use abort/exit call */
        mmsdbg(
                DL_ERROR,
                "Pipe flush timeout: pipe_name=%s, timeout=%dms!",
                cm_pipe_info->desc->pipe_desc->name,
                CM_MOV_TIMEOUT
            );
    }
    osal_assert(!err);

    cm_flushed_fr_list_for_each(cm_pipe_info->cm_fr, cm_fr_list_elem_flush);
    cm_flushed_fr_list_for_each(cm_pipe_info->cm_fr, cm_fr_list_elem_free);
}

/*
* ***************************************************************************
* ** Pipe module start/stop/flush single by descriptor **********************
* ***************************************************************************
*/
int cm_pipe_start_single(cm_pipe_t *cm_pipe, pipe_desc_t *pipe_desc, void *res)
{
    cm_pipe_info_t *cm_pipe_info;
    cm_pipe_info = cm_pipe_desc2info(cm_pipe, pipe_desc);
    if (!cm_pipe_info) {
        mmsdbg(DL_ERROR, "Unable to find pipe desc in pipe info!");
        return -1; 
    }
    return cm_pipe_start_single_by_info(cm_pipe_info, res);
}

void cm_pipe_stop_single(cm_pipe_t *cm_pipe, pipe_desc_t *pipe_desc)
{
    cm_pipe_info_t *cm_pipe_info;
    cm_pipe_info = cm_pipe_desc2info(cm_pipe, pipe_desc);
    if (!cm_pipe_info) {
        mmsdbg(DL_ERROR, "Unable to find pipe desc in pipe info!");
    }
    cm_pipe_stop_single_by_info(cm_pipe_info);
}

void cm_pipe_flush_single(cm_pipe_t *cm_pipe, pipe_desc_t *pipe_desc)
{
    cm_pipe_info_t *cm_pipe_info;
    cm_pipe_info = cm_pipe_desc2info(cm_pipe, pipe_desc);
    if (!cm_pipe_info) {
        mmsdbg(DL_ERROR, "Unable to find pipe desc in pipe info!");
    }
    cm_pipe_flush_single_by_info(cm_pipe_info);
}

/*
* ***************************************************************************
* ** Pipe module start/stop/flush all ***************************************
* ***************************************************************************
*/
int cm_pipe_start_all(cm_pipe_t *cm_pipe, void *res)
{
    int i;
    int err;

    for (i = 0; i < cm_pipe->pipe_info_arr_size; i++) {
        cm_pipe->pipe_info_arr[i].start_err = 0;

        err = pipe_start(cm_pipe->pipe_info_arr[i].pipe, res);
        if (err) {
            mmsdbg(
                    DL_ERROR,
                    "Failed to start Pipe: name=%s!",
                    cm_pipe->pipe_info_arr[i].desc->pipe_desc->name
                );
            goto exit1;
        }
        err = osal_sem_wait_timeout(
                cm_pipe->pipe_info_arr[i].event_sem,
                CM_MOV_TIMEOUT
            );
        if (err) { /* TODO: Use abort/exit call */
            mmsdbg(
                    DL_ERROR,
                    "Pipe start timeout: pipe_name=%s, timeout=%dms!",
                    cm_pipe->pipe_info_arr[i].desc->pipe_desc->name,
                    CM_MOV_TIMEOUT
                );
        }
        osal_assert(!err);
        if (cm_pipe->pipe_info_arr[i].start_err) {
            mmsdbg(
                    DL_ERROR,
                    "Failed to start Pipe: name=%s!",
                    cm_pipe->pipe_info_arr[i].desc->pipe_desc->name
                );
            goto exit1;
        }
    }

    return 0;
exit1:
    while (i--) {
        pipe_stop(cm_pipe->pipe_info_arr[i].pipe);
    }
    return -1;
}

void cm_pipe_stop_all(cm_pipe_t *cm_pipe)
{
    int i;

    for (i = cm_pipe->pipe_info_arr_size - 1; 0 <= i; i--) {
        pipe_stop(cm_pipe->pipe_info_arr[i].pipe);
    }
}

void cm_pipe_flush_all(cm_pipe_t *cm_pipe)
{
    int i;
    int err;

    for (i = 0; i < cm_pipe->pipe_info_arr_size; i++) {
        pipe_flush(cm_pipe->pipe_info_arr[i].pipe);
        err = osal_sem_wait_timeout(
                cm_pipe->pipe_info_arr[i].event_sem,
                CM_MOV_TIMEOUT
            );
        if (err) { /* TODO: Use abort/exit call */
            mmsdbg(
                    DL_ERROR,
                    "Pipe flush timeout: pipe_name=%s, timeout=%dms!",
                    cm_pipe->pipe_info_arr[i].desc->pipe_desc->name,
                    CM_MOV_TIMEOUT
                );
        }
        osal_assert(!err);
    }

    for (i = cm_pipe->pipe_info_arr_size - 1; 0 <= i; i--) {
        cm_flushed_fr_list_for_each(
                cm_pipe->pipe_info_arr[i].cm_fr,
                cm_fr_list_elem_flush
            );
        cm_flushed_fr_list_for_each(
                cm_pipe->pipe_info_arr[i].cm_fr,
                cm_fr_list_elem_free
            );
    }
}

/*
* ***************************************************************************
* ** Pipe module create/destroy *********************************************
* ***************************************************************************
*/
void cm_pipe_destroy(cm_pipe_t *cm_pipe)
{
    pipe_info_destroy(cm_pipe);
    osal_free(cm_pipe->pipe_info_arr);
    osal_free(cm_pipe);
}

cm_pipe_t *cm_pipe_create(
        camera_t *camera,
        configurator_t *configurator,
        cm_pipe_desc_t *desc,
        void *res,
        cm_pipe_info_fr_duplicate_t *fr_duplicate,
        pipe_callback_t callback,
        void *prv
    )
{
    cm_pipe_t *cm_pipe;
    int err;

    cm_pipe = osal_calloc(1, sizeof (*cm_pipe));
    if (!cm_pipe) {
        mmsdbg(
                DL_ERROR,
                "Failed to allocate memory for new "
                "Camera Mode Pipe instance: size=%d!",
                sizeof (*cm_pipe)
            );
        goto exit1;
    }
    cm_pipe->camera = camera;
    cm_pipe->configurator = configurator;
    cm_pipe->desc = desc;
    cm_pipe->res = res;
    cm_pipe->fr_duplicate = fr_duplicate;
    cm_pipe->callback = callback;
    cm_pipe->prv = prv;
    if (!cm_pipe->callback) {
        cm_pipe->callback = cm_pipe_callback;
    }

    cm_pipe->pipe_info_arr_size = cm_pipe_count_descs(desc);
    if (cm_pipe->pipe_info_arr_size <= 0) {
        mmsdbg(DL_ERROR, "Failed to count camera mode pipes!");
        goto exit2;
    }

    cm_pipe->pipe_info_arr = osal_calloc(
            cm_pipe->pipe_info_arr_size,
            sizeof (cm_pipe_info_t)
        );
    if (!cm_pipe->pipe_info_arr) {
        mmsdbg(
                DL_ERROR,
                "Failed to allocate memory for pipes info: size=%d!",
                cm_pipe->pipe_info_arr_size * sizeof (cm_pipe_info_t)
            );
        goto exit2;
    }

    err = pipe_info_create(cm_pipe);
    if (err) {
        mmsdbg(DL_ERROR, "Failed to create Camera Mode Pipe Info!");
        goto exit3;
    }

    return cm_pipe;
exit3:
    osal_free(cm_pipe->pipe_info_arr);
exit2:
    osal_free(cm_pipe);
exit1:
    return NULL;
}

