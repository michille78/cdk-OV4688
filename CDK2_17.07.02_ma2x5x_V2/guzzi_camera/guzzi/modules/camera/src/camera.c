/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file camera.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <osal/osal_stdio.h>
#include <osal/osal_stdlib.h>
#include <osal/osal_stdtypes.h>
#include <osal/osal_mutex.h>
#include <osal/osal_thread.h>
#include <osal/osal_assert.h>
#include <utils/mms_debug.h>

#include <libs/stm/include/stm.h>
#include <configurator/include/configurator.h>
#include <func_thread/include/func_thread.h>

#include <camera_port_queue.h>
#include <camera_config_plug.h>
#include <camera.h>
#include <camera_internal.h>
#include <camera_ext.h>

#define CAMERA_TIMEOUT_MS DEFAULT_ALLOC_TIMEOUT_MS
#define CAMERA_PORTS_COUNT 8
#define CAMERA_PORT_BUFFERS_COUNT 64

#ifdef GZZ_PIPE_FR_NUM_MAX
#define CAMERA_MAX_NUM_OF_CONFIGS  (GZZ_PIPE_FR_NUM_MAX+6)
#else
#define CAMERA_MAX_NUM_OF_CONFIGS 10
#endif


struct camera {
    int camera_id;
    void *app_prv;
    camera_callback_t callback;
    configurator_t *configurator;
    camera_config_plugin_prv_t config_plugin_prv;
    func_thread_t *thread;
    stm_t *stm;
    camera_port_queue_t *port_q;
    int ports_count;
    int port_buffers_count;
    camera_ext_t *ext;
    struct {
        camera_event_t event;
        int num;
        void *ptr;
        osal_sem *sem_begin;
    } notify;
    struct {
        camera_event_t event;
        int num;
        void *ptr;
        osal_sem *sem_end;
        osal_mutex *lock;
    } notify_wait;
};

mmsdbg_define_variable(
        vdl_camera,
        DL_DEFAULT,
        0,
        "vdl_camera",
        "Camera component."
    );
#define MMSDEBUGLEVEL mmsdbg_use_variable(vdl_camera)

static void camera_thread_start(func_thread_t *ethr, void *app_prv);
static void camera_thread_stop(func_thread_t *ethr, void *app_prv);
static void camera_thread_flush(func_thread_t *ethr, void *app_prv);
static void camera_thread_process(func_thread_t *ethr, void *app_prv);
static void camera_thread_i_process_done(func_thread_t *ethr, void *app_prv);

/*
* ***************************************************************************
* ***************************************************************************
* ** Camera STM *************************************************************
* ***************************************************************************
* ***************************************************************************
*/

static int camera_stm_action_cfg_get(
            stm_t *stm,
            void *prv,
            unsigned int arg1,
            void *arg2
        )
{
    camera_t *camera;
    camera = prv;
    return configurator_get(camera->configurator, arg1, arg2);
}

static int camera_stm_action_cfg_set_beg(
            stm_t *stm,
            void *prv,
            unsigned int arg1,
            void *arg2
        )
{
    camera_t *camera;
    camera = prv;
    return configurator_set_begin(camera->configurator);
}

static int camera_stm_action_cfg_set(
            stm_t *stm,
            void *prv,
            unsigned int arg1,
            void *arg2
        )
{
    camera_t *camera;
    camera = prv;
    return configurator_set(camera->configurator, arg1, arg2);
}

static int camera_stm_action_cfg_set_end(
            stm_t *stm,
            void *prv,
            unsigned int arg1,
            void *arg2
        )
{
    camera_t *camera;
    camera = prv;
    return configurator_set_end(camera->configurator);
}

static int camera_stm_action_cfg_set_can(
            stm_t *stm,
            void *prv,
            unsigned int arg1,
            void *arg2
        )
{
    camera_t *camera;
    camera = prv;
    configurator_set_cancel(camera->configurator);
    return 0;
}

static int camera_stm_action_start(
            stm_t *stm,
            void *prv,
            unsigned int arg1,
            void *arg2
        )
{
    camera_t *camera;
    camera = prv;
    return func_thread_exec(camera->thread, camera_thread_start);
}

static int camera_stm_action_start_ok(
            stm_t *stm,
            void *prv,
            unsigned int arg1,
            void *arg2
        )
{
    camera_t *camera;
    camera_event_t event;

    camera = prv;
    event.type = CAMERA_EVENT_START_DONE;
    camera->callback(
            camera,
            camera->app_prv,
            event,
            0,
            NULL
        );

    return 0;
}

static int camera_stm_action_start_err(
            stm_t *stm,
            void *prv,
            unsigned int arg1,
            void *arg2
        )
{
    camera_t *camera;
    camera_event_t event;

    camera = prv;
    event.type = CAMERA_EVENT_ERROR_START;

    mmsdbg(DL_ERROR, "Failed to start camera!");

    camera->callback(
            camera,
            camera->app_prv,
            event,
            0,
            NULL
        );

    return 0;
}

static int camera_stm_action_stop(
            stm_t *stm,
            void *prv,
            unsigned int arg1,
            void *arg2
        )
{
    camera_t *camera;
    camera = prv;
    return func_thread_exec(camera->thread, camera_thread_stop);
}

static int camera_stm_action_stop_done(
            stm_t *stm,
            void *prv,
            unsigned int arg1,
            void *arg2
        )
{
    camera_t *camera;
    camera_event_t event;

    camera = prv;
    event.type = CAMERA_EVENT_STOP_DONE;
    camera->callback(
            camera,
            camera->app_prv,
            event,
            0,
            NULL
        );

    return 0;
}

static int camera_stm_action_flush(
            stm_t *stm,
            void *prv,
            unsigned int arg1,
            void *arg2
        )
{
    camera_t *camera;
    camera = prv;
    return func_thread_exec(camera->thread, camera_thread_flush);
}

static int camera_stm_action_flush_done(
            stm_t *stm,
            void *prv,
            unsigned int arg1,
            void *arg2
        )
{
    camera_t *camera;
    camera_event_t event;

    camera = prv;
    event.type = CAMERA_EVENT_FLUSH_DONE;
    camera->callback(
            camera,
            camera->app_prv,
            event,
            0,
            NULL
        );

    return 0;
}

static int camera_stm_action_process(
            stm_t *stm,
            void *prv,
            unsigned int arg1,
            void *arg2
        )
{
    camera_t *camera;
    int port;
    void *buf;
    int err;

    camera = prv;
    port = arg1;
    buf = arg2;

    err = camera_port_queue_req_enq(camera->port_q, port, buf);
    if (err) {
        mmsdbg(
                DL_ERROR,
                "Failed to enqueue to port \"%d\"!",
                port
            );
        goto exit1;
    }

    err = func_thread_exec(camera->thread, camera_thread_process);
    if (err) {
        mmsdbg(
                DL_ERROR,
                "Failed to activate func thread for \"camera_thread_process\"!"
            );
        goto exit1;
    }

exit1:
    return err;
}

static int camera_stm_action_process_done(
            stm_t *stm,
            void *prv,
            unsigned int arg1,
            void *arg2
        )
{
    camera_t *camera;
    camera_event_t event;
    int port;
    void *buf;

    camera = prv;
    port = arg1;
    buf = arg2;
    event.type = CAMERA_EVENT_PROCESS_DONE;

    camera->callback(
            camera,
            camera->app_prv,
            event,
            port,
            buf
        );
    return 0;
}

static int camera_stm_action_process_thread(stm_t *stm,
                                                  void *prv,
                                                  unsigned int arg1,
                                                  void *arg2)
{
    camera_t *camera;
    camera = prv;
    return camera_ext_process(camera->ext);
}

static int camera_stm_action_buffer_flush(
            stm_t *stm,
            void *prv,
            unsigned int arg1,
            void *arg2
        )
{
    camera_t *camera;
    camera_event_t event;
    int port;
    void *buf;

    camera = prv;
    port = arg1;
    buf = arg2;
    event.type = CAMERA_EVENT_BUFFER_FLUSH;

    camera->callback(
            camera,
            camera->app_prv,
            event,
            port,
            buf
        );
    return 0;
}

static int camera_stm_action_pq_req_is_empty(
            stm_t *stm,
            void *prv,
            unsigned int arg1,
            void *arg2
        )
{
    camera_t *camera;
    int port;

    camera = prv;
    port = arg1;

    return camera_port_queue_req_is_empty(camera->port_q, port);
}

static int camera_stm_action_pq_req_empty(
            stm_t *stm,
            void *prv,
            unsigned int arg1,
            void *arg2
        )
{
    return 1;
}

static int camera_stm_action_pq_req_enq(
            stm_t *stm,
            void *prv,
            unsigned int arg1,
            void *arg2
        )
{
    camera_t *camera;
    int port;
    void *buf;

    camera = prv;
    port = arg1;
    buf = arg2;

    return camera_port_queue_req_enq(camera->port_q, port, buf);
}

static int camera_stm_action_pq_req_enq_proc(
            stm_t *stm,
            void *prv,
            unsigned int arg1,
            void *arg2
        )
{
    camera_t *camera;
    int port;
    void *buf;
    int err;

    camera = prv;
    port = arg1;
    buf = arg2;

    err = camera_port_queue_req_enq(camera->port_q, port, buf);
    if (err) {
        mmsdbg(
                DL_ERROR,
                "Failed to enqueue to port \"%d\"!",
                port
            );
        goto exit1;
    }

    err = func_thread_exec(camera->thread, camera_thread_process);
    if (err) {
        mmsdbg(
                DL_ERROR,
                "Failed to activate func thread for \"camera_thread_process\"!"
            );
        goto exit1;
    }

exit1:
    return err;
}


static int camera_stm_action_pq_req_deq(
            stm_t *stm,
            void *prv,
            unsigned int arg1,
            void *arg2
        )
{
    camera_t *camera;
    int port;
    void **buf;

    camera = prv;
    port = arg1;
    buf = arg2;

    return camera_port_queue_req_deq(camera->port_q, port, buf);
}

static int camera_stm_action_i_process_done(
            stm_t *stm,
            void *prv,
            unsigned int arg1,
            void *arg2
        )
{
    camera_t *camera;
    int port;
    void *buf;
    int err;

    camera = prv;
    port = arg1;
    buf = arg2;

    err = camera_port_queue_rdy_enq(camera->port_q, port, buf);
    if (err) {
        mmsdbg(
                DL_ERROR,
                "Failed to enqueue ready buffer to port \"%d\"!",
                port
            );
        goto exit1;
    }

    err = func_thread_exec(camera->thread, camera_thread_i_process_done);
    if (err) {
        mmsdbg(
                DL_ERROR,
                "Failed to activate func thread for \"camera_thread_i_process_done\"!"
            );
        goto exit1;
    }

exit1:
    return err;
}

static int camera_stm_action_destroy(
            stm_t *stm,
            void *prv,
            unsigned int arg1,
            void *arg2
        )
{
    camera_t *camera;
    camera = prv;
    camera_ext_destroy(camera->ext);
    osal_mutex_destroy(camera->notify_wait.lock);
    osal_sem_destroy(camera->notify_wait.sem_end);
    osal_sem_destroy(camera->notify.sem_begin);
    camera_port_queue_destroy(camera->port_q);
    stm_destroy(camera->stm);
    configurator_destroy(camera->configurator);
    if (camera->config_plugin_prv.scene_cfg_data) {
        camera_config_plug_destroy_scene_cfg_data
                (camera->config_plugin_prv.scene_cfg_data);
    }

    if(camera->config_plugin_prv.effect_cfg_data) {
            camera_config_plug_destroy_effect_cfg_data
                (camera->config_plugin_prv.effect_cfg_data);
    }

    func_thread_destroy(camera->thread);
    osal_free(camera);
    return 0;
}

static int camera_stm_action_no_action(
            stm_t *stm,
            void *prv,
            unsigned int arg1,
            void *arg2
        )
{
    return 0;
}

static int camera_stm_action_err(
            stm_t *stm,
            void *prv,
            unsigned int arg1,
            void *arg2
        )
{
    mmsdbg(DL_ERROR, "Invalid camera state + operation!");
    return -1;
}

enum {
    CAMERA_STM_STATE_IDLE,
    CAMERA_STM_STATE_IDLE2RUN,
    CAMERA_STM_STATE_RUN,
    CAMERA_STM_STATE_RUN2IDLE,
    CAMERA_STM_STATE_FLUSHING,
    CAMERA_STM_STATES_NUMBER
};

enum {
    CAMERA_STM_EVENT_CFG_GET,
    CAMERA_STM_EVENT_CFG_SET_BEG,
    CAMERA_STM_EVENT_CFG_SET,
    CAMERA_STM_EVENT_CFG_SET_END,
    CAMERA_STM_EVENT_CFG_SET_CAN,
    CAMERA_STM_EVENT_START,
    CAMERA_STM_EVENT_START_OK,
    CAMERA_STM_EVENT_START_ERR,
    CAMERA_STM_EVENT_STOP,
    CAMERA_STM_EVENT_STOP_DONE,
    CAMERA_STM_EVENT_FLUSH,
    CAMERA_STM_EVENT_FLUSH_DONE,
    CAMERA_STM_EVENT_PROCESS,
    CAMERA_STM_EVENT_PROCESS_DONE,
    CAMERA_STM_EVENT_PROCESS_THREAD,
    CAMERA_STM_EVENT_PQ_REQ_IS_EMPTY,
    CAMERA_STM_EVENT_PQ_REQ_ENQ,
    CAMERA_STM_EVENT_PQ_REQ_DEQ,
    CAMERA_STM_EVENT_I_PROCESS_DONE,
    CAMERA_STM_EVENT_DESTROY,
    CAMERA_STM_EVENTS_NUMBER
};

enum {
    CAMERA_STM_ACTION_CFG_GET,
    CAMERA_STM_ACTION_CFG_SET_BEG,
    CAMERA_STM_ACTION_CFG_SET,
    CAMERA_STM_ACTION_CFG_SET_END,
    CAMERA_STM_ACTION_CFG_SET_CAN,
    CAMERA_STM_ACTION_START,
    CAMERA_STM_ACTION_START_OK,
    CAMERA_STM_ACTION_START_ERR,
    CAMERA_STM_ACTION_STOP,
    CAMERA_STM_ACTION_STOP_DONE,
    CAMERA_STM_ACTION_FLUSH,
    CAMERA_STM_ACTION_FLUSH_DONE,
    CAMERA_STM_ACTION_PROCESS,
    CAMERA_STM_ACTION_PROCESS_DONE,
    CAMERA_STM_ACTION_PROCESS_THREAD,
    CAMERA_STM_ACTION_BUFFER_FLUSH,
    CAMERA_STM_ACTION_PQ_REQ_IS_EMPTY,
    CAMERA_STM_ACTION_PQ_REQ_EMPTY,
    CAMERA_STM_ACTION_PQ_REQ_ENQ,
    CAMERA_STM_ACTION_PQ_REQ_ENQ_PROC,
    CAMERA_STM_ACTION_PQ_REQ_DEQ,
    CAMERA_STM_ACTION_I_PROCESS_DONE,
    CAMERA_STM_ACTION_DESTROY,
    CAMERA_STM_ACTION_NO_ACTION,
    CAMERA_STM_ACTION_ERR,
    CAMERA_STM_ACTIONS_NUMBER
};

static stm_actions_t camera_stm_actions[] = {
    camera_stm_action_cfg_get,
    camera_stm_action_cfg_set_beg,
    camera_stm_action_cfg_set,
    camera_stm_action_cfg_set_end,
    camera_stm_action_cfg_set_can,
    camera_stm_action_start,
    camera_stm_action_start_ok,
    camera_stm_action_start_err,
    camera_stm_action_stop,
    camera_stm_action_stop_done,
    camera_stm_action_flush,
    camera_stm_action_flush_done,
    camera_stm_action_process,
    camera_stm_action_process_done,
    camera_stm_action_process_thread,
    camera_stm_action_buffer_flush,
    camera_stm_action_pq_req_is_empty,
    camera_stm_action_pq_req_empty,
    camera_stm_action_pq_req_enq,
    camera_stm_action_pq_req_enq_proc,
    camera_stm_action_pq_req_deq,
    camera_stm_action_i_process_done,
    camera_stm_action_destroy,
    camera_stm_action_no_action,
    camera_stm_action_err
};

#define STM_STATE_PREFIX CAMERA_STM_STATE_
#define STM_ACTIOIN_PREFIX CAMERA_STM_ACTION_

STM_DESC_BUILD_BEGIN(
            camera_stm,
            CAMERA_STM_STATE_IDLE,
            CAMERA_STM_STATES_NUMBER,
            CAMERA_STM_EVENTS_NUMBER,
            CAMERA_STM_ACTIONS_NUMBER
        )
                             /* IDLE-------------------|     IDLE2RUN----------------|     RUN---------------------|     RUN2IDLE---------------| */
/* CFG_GET         */ STM_E4x(  IDLE    ,       CFG_GET,     IDLE2RUN,            ERR,     RUN     ,        CFG_GET,     RUN2IDLE,            ERR  )
/* CFG_SET_BEG     */ STM_E4x(  IDLE    ,   CFG_SET_BEG,     IDLE2RUN,            ERR,     RUN     ,    CFG_SET_BEG,     RUN2IDLE,            ERR  )
/* CFG_SET         */ STM_E4x(  IDLE    ,       CFG_SET,     IDLE2RUN,            ERR,     RUN     ,        CFG_SET,     RUN2IDLE,            ERR  )
/* CFG_SET_END     */ STM_E4x(  IDLE    ,   CFG_SET_END,     IDLE2RUN,            ERR,     RUN     ,    CFG_SET_END,     RUN2IDLE,            ERR  )
/* CFG_SET_CAN     */ STM_E4x(  IDLE    ,   CFG_SET_CAN,     IDLE2RUN,            ERR,     RUN     ,    CFG_SET_CAN,     RUN2IDLE,            ERR  )
/* START           */ STM_E4x(  IDLE2RUN,         START,     IDLE2RUN,            ERR,     RUN     ,            ERR,     RUN2IDLE,            ERR  )
/* START_OK        */ STM_E4x(  IDLE    ,           ERR,     RUN     ,       START_OK,     RUN     ,            ERR,     RUN2IDLE,            ERR  )
/* START_ERR       */ STM_E4x(  IDLE    ,           ERR,     IDLE    ,      START_ERR,     RUN     ,            ERR,     RUN2IDLE,            ERR  )
/* STOP            */ STM_E4x(  IDLE    ,           ERR,     IDLE2RUN,            ERR,     RUN2IDLE,           STOP,     RUN2IDLE,            ERR  )
/* STOP_DONE       */ STM_E4x(  IDLE    ,           ERR,     IDLE2RUN,            ERR,     RUN     ,            ERR,     IDLE    ,      STOP_DONE  )
/* FLUSH           */ STM_E4x(  IDLE    ,           ERR,     IDLE2RUN,            ERR,     FLUSHING,          FLUSH,     RUN2IDLE,            ERR  )
/* FLUSH_DONE      */ STM_E4x(  IDLE    ,           ERR,     IDLE2RUN,            ERR,     RUN     ,            ERR,     RUN2IDLE,            ERR  )
/* PROCESS         */ STM_E4x(  IDLE    ,           ERR,     IDLE2RUN,            ERR,     RUN     ,        PROCESS,     RUN2IDLE,            ERR  )
/* PROCESS_DONE    */ STM_E4x(  IDLE    ,           ERR,     IDLE2RUN,            ERR,     RUN     ,   PROCESS_DONE,     RUN2IDLE,            ERR  )
/* PROCESS_THREAD  */ STM_E4x(  IDLE    ,           ERR,     IDLE2RUN,            ERR,     RUN     , PROCESS_THREAD,     RUN2IDLE,      NO_ACTION  )
/* PQ_REQ_IS_EMPTY */ STM_E4x(  IDLE    ,           ERR,     IDLE2RUN,            ERR,     RUN     ,PQ_REQ_IS_EMPTY,     RUN2IDLE,   PQ_REQ_EMPTY  )
/* PQ_REQ_ENQ      */ STM_E4x(  IDLE    ,           ERR,     IDLE2RUN,PQ_REQ_ENQ_PROC,     RUN     ,PQ_REQ_ENQ_PROC,     RUN2IDLE,     PQ_REQ_ENQ  )
/* PQ_REQ_DEQ      */ STM_E4x(  IDLE    ,           ERR,     IDLE2RUN,            ERR,     RUN     ,     PQ_REQ_DEQ,     RUN2IDLE,            ERR  )
/* I_PROCESS_DONE  */ STM_E4x(  IDLE    ,           ERR,     IDLE2RUN,            ERR,     RUN     , I_PROCESS_DONE,     RUN2IDLE, I_PROCESS_DONE  )
/* DESTROY         */ STM_E4x(  IDLE    ,       DESTROY,     IDLE2RUN,            ERR,     RUN     ,            ERR,     RUN2IDLE,            ERR  )

                             /* FLUSHING---------------| */
/* CFG_GET         */ STM_E1x(  FLUSHING,            ERR  )
/* CFG_SET_BEG     */ STM_E1x(  FLUSHING,            ERR  )
/* CFG_SET         */ STM_E1x(  FLUSHING,            ERR  )
/* CFG_SET_END     */ STM_E1x(  FLUSHING,            ERR  )
/* CFG_SET_CAN     */ STM_E1x(  FLUSHING,            ERR  )
/* START           */ STM_E1x(  FLUSHING,            ERR  )
/* START_OK        */ STM_E1x(  FLUSHING,            ERR  )
/* START_ERR       */ STM_E1x(  FLUSHING,            ERR  )
/* STOP            */ STM_E1x(  FLUSHING,            ERR  )
/* STOP_DONE       */ STM_E1x(  FLUSHING,            ERR  )
/* FLUSH           */ STM_E1x(  FLUSHING,            ERR  )
/* FLUSH_DONE      */ STM_E1x(  RUN     ,     FLUSH_DONE  )
/* PROCESS         */ STM_E1x(  FLUSHING,            ERR  )
/* PROCESS_DONE    */ STM_E1x(  FLUSHING,   BUFFER_FLUSH  )
/* PROCESS_THREAD  */ STM_E1x(  FLUSHING,      NO_ACTION  )
/* PQ_REQ_IS_EMPTY */ STM_E1x(  FLUSHING,PQ_REQ_IS_EMPTY  )
/* PQ_REQ_ENQ      */ STM_E1x(  FLUSHING,     PQ_REQ_ENQ  )
/* PQ_REQ_DEQ      */ STM_E1x(  FLUSHING,     PQ_REQ_DEQ  )
/* I_PROCESS_DONE  */ STM_E1x(  FLUSHING, I_PROCESS_DONE  )
/* DESTROY         */ STM_E1x(  FLUSHING,            ERR  )

STM_DESC_BUILD_END(camera_stm)

/*
* ***************************************************************************
* ***************************************************************************
* ** Camera Thread **********************************************************
* ***************************************************************************
* ***************************************************************************
*/

static void camera_thread_process(
        func_thread_t *ethr,
        void *app_prv
    )
{
    camera_t *camera;
    int err;

    camera = app_prv;

    err = stm_event_atomic_state(
            camera->stm,
            CAMERA_STM_EVENT_PROCESS_THREAD,
            camera,
            0,
            NULL
        );
    if (err) {
        mmsdbg(DL_ERROR, "Error occur during process in camera thread!");
    }
}

static void camera_thread_i_process_done(
        func_thread_t *ethr,
        void *app_prv
    )
{
    camera_t *camera;
    void *buf;
    int port;
    int err;

    camera = app_prv;

    for (port = 0; port < camera->ports_count; port++) {
        if (!camera_port_queue_rdy_is_empty(camera->port_q, port)) {
            err = camera_port_queue_rdy_deq(camera->port_q, port, &buf);
            if (err) {
                mmsdbg(
                        DL_ERROR,
                        "Unable to dequeue from non empty port (%d)!",
                        port
                    );
                break;
            }
            err = stm_event_atomic_state(
                    camera->stm,
                    CAMERA_STM_EVENT_PROCESS_DONE,
                    camera,
                    port,
                    buf
                );
            if (err) {
                mmsdbg(
                        DL_ERROR,
                        "Unable to notify for ready buffer=%p on port=%d!",
                        buf,
                        port
                    );
            }
            break;
        }
    }
}

static void camera_thread_notify(
        func_thread_t *ethr,
        void *app_prv
    )
{
    camera_t *camera;
    camera_event_t event;
    int num;
    void *ptr;

    camera = app_prv;

    event = camera->notify.event;
    num = camera->notify.num;
    ptr = camera->notify.ptr;
    osal_assert(osal_sem_value(camera->notify.sem_begin) == 0);
    osal_sem_post(camera->notify.sem_begin);

    camera->callback(
            camera,
            camera->app_prv,
            event,
            num,
            ptr
        );
}

static void camera_thread_notify_wait(
        func_thread_t *ethr,
        void *app_prv
    )
{
    camera_t *camera;
    camera_event_t event;
    int num;
    void *ptr;

    camera = app_prv;

    event = camera->notify_wait.event;
    num = camera->notify_wait.num;
    ptr = camera->notify_wait.ptr;

    camera->callback(
            camera,
            camera->app_prv,
            event,
            num,
            ptr
        );

    osal_assert(osal_sem_value(camera->notify_wait.sem_end) == 0);
    osal_sem_post(camera->notify_wait.sem_end);
}

static void camera_thread_start(
        func_thread_t *ethr,
        void *app_prv
    )
{
    camera_t *camera;
    int err;

    camera = app_prv;

    err = camera_ext_start(camera->ext);
    if (err) {
        stm_event_atomic_state(
                camera->stm,
                CAMERA_STM_EVENT_START_ERR,
                camera,
                0,
                NULL
            );
    } else {
        stm_event_atomic_state(
                camera->stm,
                CAMERA_STM_EVENT_START_OK,
                camera,
                0,
                NULL
            );
    }
}

static void camera_thread_stop(
        func_thread_t *ethr,
        void *app_prv
    )
{
    camera_t *camera;
    camera_event_t event;
    void *buf;
    int port;
    int err;

    camera = app_prv;

    camera_ext_stop(camera->ext);

    for (port = 0; port < camera->ports_count; port++) {
        while (!camera_port_queue_rdy_is_empty(camera->port_q, port)) {
            err = camera_port_queue_rdy_deq(camera->port_q, port, &buf);
            if (err) {
                mmsdbg(
                        DL_ERROR,
                        "Unable to dequeue from non empty port (%d)! Continue with other ports.",
                        port
                    );
                break;
            }
            event.type = CAMERA_EVENT_PROCESS_DONE;
            camera->callback(
                    camera,
                    camera->app_prv,
                    event,
                    port,
                    buf
                );
        }
        while (!camera_port_queue_req_is_empty(camera->port_q, port)) {
            err = camera_port_queue_req_deq(camera->port_q, port, &buf);
            if (err) {
                mmsdbg(
                        DL_ERROR,
                        "Unable to dequeue from non empty port (%d)! Continue with other ports.",
                        port
                    );
                break;
            }
            event.type = CAMERA_EVENT_BUFFER_FLUSH;
            camera->callback(
                    camera,
                    camera->app_prv,
                    event,
                    port,
                    buf
                );
        }
    }

    stm_event_atomic_state(
            camera->stm,
            CAMERA_STM_EVENT_STOP_DONE,
            camera,
            0,
            NULL
        );
}

static void camera_thread_flush(
        func_thread_t *ethr,
        void *app_prv
    )
{
    camera_t *camera;
    camera_event_t event;
    void *buf;
    int port;
    int err;

    camera = app_prv;

    camera_ext_flush(camera->ext);

    for (port = 0; port < camera->ports_count; port++) {
        while (!camera_port_queue_rdy_is_empty(camera->port_q, port)) {
            err = camera_port_queue_rdy_deq(camera->port_q, port, &buf);
            if (err) {
                mmsdbg(
                        DL_ERROR,
                        "Unable to dequeue from non empty port (%d)! Continue with other ports.",
                        port
                    );
                break;
            }
            event.type = CAMERA_EVENT_BUFFER_FLUSH;
            camera->callback(
                    camera,
                    camera->app_prv,
                    event,
                    port,
                    buf
                );
        }
        while (!camera_port_queue_req_is_empty(camera->port_q, port)) {
            err = camera_port_queue_req_deq(camera->port_q, port, &buf);
            if (err) {
                mmsdbg(
                        DL_ERROR,
                        "Unable to dequeue from non empty port (%d)! Continue with other ports.",
                        port
                    );
                break;
            }
            event.type = CAMERA_EVENT_BUFFER_FLUSH;
            camera->callback(
                    camera,
                    camera->app_prv,
                    event,
                    port,
                    buf
                );
        }
    }

    stm_event_atomic_state(
            camera->stm,
            CAMERA_STM_EVENT_FLUSH_DONE,
            camera,
            0,
            NULL
        );
}

static func_thread_handle_t camera_thread_fxns[] = {
    camera_thread_flush,
    camera_thread_process,
    camera_thread_i_process_done,
    camera_thread_notify,
    camera_thread_notify_wait,
    camera_thread_start,
    camera_thread_stop,
};

static func_thread_handles_t camera_thread_handles = {
    .fxns = camera_thread_fxns,
    .size = ARRAY_SIZE(camera_thread_fxns)
};

/*
* ***************************************************************************
* ***************************************************************************
* ** Internal Interface Part ************************************************
* ***************************************************************************
* ***************************************************************************
*/

int camera_i_port_q_req_is_empty(camera_t *camera, int port)
{
    return stm_event_atomic_state(
            camera->stm,
            CAMERA_STM_EVENT_PQ_REQ_IS_EMPTY,
            camera,
            port,
            NULL
        );
}

int camera_i_port_q_req_enq(camera_t *camera, int port, void *buf)
{
    return stm_event_atomic_state(
            camera->stm,
            CAMERA_STM_EVENT_PQ_REQ_ENQ,
            camera,
            port,
            buf
        );
}

int camera_i_port_q_req_deq(camera_t *camera, int port, void **buf)
{
    return stm_event_atomic_state(
            camera->stm,
            CAMERA_STM_EVENT_PQ_REQ_DEQ,
            camera,
            port,
            buf
        );
}

int camera_i_process_done(camera_t *camera, int port, void *buf)
{

    return stm_event_atomic_state(
            camera->stm,
            CAMERA_STM_EVENT_I_PROCESS_DONE,
            camera,
            port,
            buf
        );
}

void camera_i_notify(camera_t *camera, unsigned int sub_type, int num, void *ptr)
{
    camera_event_t event;
    int err;

    event.type = CAMERA_EVENT_GENERIC;
    event.sub_type = sub_type;

    if (osal_thread_self() == func_thread_id(camera->thread)) {
        camera->callback(
                camera,
                camera->app_prv,
                event,
                num,
                ptr
            );
    } else {
        osal_assert(osal_sem_value(camera->notify.sem_begin) <= 1);
        err = osal_sem_wait_timeout(camera->notify.sem_begin, CAMERA_TIMEOUT_MS);
        if (err) {
            /* TODO: assert here? */
            mmsdbg(
                    DL_ERROR,
                    "Camera notification timeout: timeout=%dms!",
                    CAMERA_TIMEOUT_MS
                );
            return;
        }
        camera->notify.event = event;
        camera->notify.num = num;
        camera->notify.ptr = ptr;
        func_thread_exec(camera->thread, camera_thread_notify);
    }
}

void camera_i_notify_wait(camera_t *camera, unsigned int sub_type, int num, void *ptr)
{
    camera_event_t event;
    int err;

    event.type = CAMERA_EVENT_GENERIC;
    event.sub_type = sub_type;

    if (osal_thread_self() == func_thread_id(camera->thread)) {
        camera->callback(
                camera,
                camera->app_prv,
                event,
                num,
                ptr
            );
    } else {
        osal_mutex_lock(camera->notify_wait.lock);
        camera->notify_wait.event = event;
        camera->notify_wait.num = num;
        camera->notify_wait.ptr = ptr;
        osal_assert(osal_sem_value(camera->notify_wait.sem_end) == 0);
        func_thread_exec(camera->thread, camera_thread_notify_wait);
        err = osal_sem_wait_timeout(camera->notify_wait.sem_end, CAMERA_TIMEOUT_MS);
        if (err) {
            /* TODO: assert here? */
            mmsdbg(
                    DL_ERROR,
                    "Camera notification wait timeout: timeout=%dms!",
                    CAMERA_TIMEOUT_MS
                );
        }
        osal_mutex_unlock(camera->notify_wait.lock);
    }
}

int camera_i_get_id(camera_t *camera)
{
    return camera->camera_id;
}

/*
* ***************************************************************************
* ***************************************************************************
* ** Application Interface Part *********************************************
* ***************************************************************************
* ***************************************************************************
*/

/* ========================================================================== */
/**
* camera_get_id()
*
*/
/* ========================================================================== */
int camera_get_id(camera_t *camera)
{
    return camera->camera_id;
}

/* ========================================================================== */
/**
* camera_config_get()
*
*/
/* ========================================================================== */
int camera_config_get(
        camera_t *camera,
        camera_config_index_t index,
        void *config
    )
{
    return stm_event_atomic_state(
            camera->stm,
            CAMERA_STM_EVENT_CFG_GET,
            camera,
            index,
            config
        );
}

/* ========================================================================== */
/**
* camera_config_set_begin()
*
*/
/* ========================================================================== */
int camera_config_set_begin(
        camera_t *camera
    )
{
    return stm_event_atomic_state(
            camera->stm,
            CAMERA_STM_EVENT_CFG_SET_BEG,
            camera,
            0,
            NULL
        );
}

/* ========================================================================== */
/**
* camera_config_set()
*
*/
/* ========================================================================== */
int camera_config_set(
        camera_t *camera,
        camera_config_index_t index,
        void *config
    )
{
    return stm_event_atomic_state(
            camera->stm,
            CAMERA_STM_EVENT_CFG_SET,
            camera,
            index,
            config
        );
}

/* ========================================================================== */
/**
* camera_config_set_end()
*
*/
/* ========================================================================== */
int camera_config_set_end(
        camera_t *camera
    )
{
    return stm_event_atomic_state(
            camera->stm,
            CAMERA_STM_EVENT_CFG_SET_END,
            camera,
            0,
            NULL
        );
}

/* ========================================================================== */
/**
* camera_config_set_cancel()
*
*/
/* ========================================================================== */
int camera_config_set_cancel(
        camera_t *camera
    )
{
    return stm_event_atomic_state(
            camera->stm,
            CAMERA_STM_EVENT_CFG_SET_CAN,
            camera,
            0,
            NULL
        );
}

/* ========================================================================== */
/**
* camera_start()
*
*/
/* ========================================================================== */
int camera_start(camera_t *camera)
{
    return stm_event_atomic_state(
            camera->stm,
            CAMERA_STM_EVENT_START,
            camera,
            0,
            NULL
        );
}

/* ========================================================================== */
/**
* camera_stop()
*
*/
/* ========================================================================== */
int camera_stop(camera_t *camera)
{
    return stm_event_atomic_state(
            camera->stm,
            CAMERA_STM_EVENT_STOP,
            camera,
            0,
            NULL
        );
}

/* ========================================================================== */
/**
* camera_flush()
*
*/
/* ========================================================================== */
int camera_flush(camera_t *camera)
{
    return stm_event_atomic_state(
            camera->stm,
            CAMERA_STM_EVENT_FLUSH,
            camera,
            0,
            NULL
        );
}

/* ========================================================================== */
/**
* camera_process()
*
*/
/* ========================================================================== */
int camera_process(
        camera_t *camera,
        int port,
        void *buf
    )
{
    return stm_event_atomic_state(
            camera->stm,
            CAMERA_STM_EVENT_PROCESS,
            camera,
            port,
            buf
        );
}

/* ========================================================================== */
/**
* camera_destroy()
*
*/
/* ========================================================================== */
void camera_destroy(camera_t *camera)
{
    stm_event_atomic_state(
            camera->stm,
            CAMERA_STM_EVENT_DESTROY,
            camera,
            0,
            NULL
        );
}

/* ========================================================================== */
/**
* camera_create()
*
*/
/* ========================================================================== */
camera_t *camera_create(camera_create_params_t *params)
{
    camera_t *camera;
    configurator_create_param_t cfgr_params;
    camera_port_queue_params_t port_q_params;
    camera_ext_create_params_t ext_params;

    camera = osal_calloc(1, sizeof (*camera));
    if (!camera) {
        mmsdbg(
                DL_ERROR,
                "Failed to allocate memory for new camera instance: size=%d!",
                sizeof (*camera)
            );
        goto exit1;
    }

    camera->camera_id = params->camera_id;
    camera->callback = params->callback;
    camera->app_prv = params->app_prv;

    camera->thread = func_thread_create(
            "Camera Thread",
            8 * 1024,
            5,
            &camera_thread_handles,
            camera
        );
    if (!camera->thread) {
        mmsdbg(DL_ERROR, "Failed to create camera thread!");
        goto exit2;
    }

    camera->config_plugin_prv.scene_cfg_data = NULL;
    cfgr_params.plugin = &camera_config_plug;
    cfgr_params.plugin_prv = &camera->config_plugin_prv;
    cfgr_params.num_of_configs = CAMERA_MAX_NUM_OF_CONFIGS;
    camera->configurator = configurator_create(&cfgr_params);
    if (!camera->configurator) {
        mmsdbg(DL_ERROR, "Failed to create camera configurator!");
        goto exit3;
    }

    camera->stm = stm_create(
            "Camera",
            &camera_stm,
            camera_stm_actions
        );
    if (!camera->stm) {
        mmsdbg(DL_ERROR, "Failed to create camera STM!");
        goto exit4;
    }

    camera->ports_count = CAMERA_PORTS_COUNT;
    camera->port_buffers_count = CAMERA_PORT_BUFFERS_COUNT;

    port_q_params.num_of_ports = camera->ports_count;
    port_q_params.num_of_buffs = camera->port_buffers_count;
    camera->port_q = camera_port_queue_create(&port_q_params);
    if (!camera->port_q) {
        mmsdbg(DL_ERROR, "Failed to create camera port queue!");
        goto exit5;
    }

    camera->notify.sem_begin = osal_sem_create(1);
    if (!camera->notify.sem_begin) {
        mmsdbg(DL_ERROR, "Failed to create notify begin semaphore!");
        goto exit6;
    }

    camera->notify_wait.sem_end = osal_sem_create(0);
    if (!camera->notify_wait.sem_end) {
        mmsdbg(DL_ERROR, "Failed to create notify wait end semaphore!");
        goto exit7;
    }

    camera->notify_wait.lock = osal_mutex_create();
    if (!camera->notify_wait.lock) {
        mmsdbg(DL_ERROR, "Failed to create notify wait lock!");
        goto exit8;
    }

    ext_params.mode = params->mode;
    ext_params.camera = camera;
    ext_params.configurator = camera->configurator;
    camera->ext = camera_ext_create(&ext_params);
    if (!camera->ext) {
        mmsdbg(DL_ERROR, "Failed to create camera extensions!");
        goto exit9;
    }

    return camera;
exit9:
    osal_mutex_destroy(camera->notify_wait.lock);
exit8:
    osal_sem_destroy(camera->notify_wait.sem_end);
exit7:
    osal_sem_destroy(camera->notify.sem_begin);
exit6:
    camera_port_queue_destroy(camera->port_q);
exit5:
    stm_destroy(camera->stm);
exit4:
    configurator_destroy(camera->configurator);
exit3:
    if(camera->config_plugin_prv.scene_cfg_data) {
        camera_config_plug_destroy_scene_cfg_data
                (camera->config_plugin_prv.scene_cfg_data);
    }

    if(camera->config_plugin_prv.effect_cfg_data) {
            camera_config_plug_destroy_effect_cfg_data
                (camera->config_plugin_prv.effect_cfg_data);
    }

    func_thread_destroy(camera->thread);
exit2:
    osal_free(camera);
exit1:
    return NULL;
}

