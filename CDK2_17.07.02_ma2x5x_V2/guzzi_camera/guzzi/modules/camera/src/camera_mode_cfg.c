/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file camera_mode_cfg.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <osal/osal_stdlib.h>
#include <osal/osal_mutex.h>
#include <utils/mms_debug.h>

#include <configurator/include/configurator.h>

#include <cam_config.h>
#include <camera_mode_cfg.h>

struct cm_cfg {
    configurator_t *configurator;
    configurator_client_t *configurator_client;
    cam_cfg_t *cur_cfg;
    osal_mutex *cur_cfg_lock;

    cm_cfg_alter_func_t *alter_func;
    cm_cfg_notify_func_t *notify_func;
    void *prv;
};

mmsdbg_define_variable(
        vdl_camera_mode_cfg,
        DL_DEFAULT,
        0,
        "vdl_camera_mode_cfg",
        "Common Config routines used in camera mode."
    );
#define MMSDEBUGLEVEL mmsdbg_use_variable(vdl_camera_mode_cfg)

/*
* ***************************************************************************
* ** CFG Private ************************************************************
* ***************************************************************************
*/
static int cm_cfg_client_reg(
            configurator_t *c,
            configurator_client_t *client,
            void *client_prv,
            void *config
        )
{
    cm_cfg_t *cm_cfg;

    cm_cfg = client_prv;

    configurator_config_lock(config);

    osal_mutex_lock(cm_cfg->cur_cfg_lock);
    cm_cfg->cur_cfg = config;
    osal_mutex_unlock(cm_cfg->cur_cfg_lock);

    return 0;
}

static int cm_cfg_client_unreg(
            configurator_t *c,
            configurator_client_t *client,
            void *client_prv,
            void *config
        )
{
    cm_cfg_t *cm_cfg;
    cam_cfg_t *cur_cfg;

    cm_cfg = client_prv;

    osal_mutex_lock(cm_cfg->cur_cfg_lock);
    if (cm_cfg->cur_cfg) {
        cur_cfg = cm_cfg->cur_cfg;
        cm_cfg->cur_cfg = NULL;
        osal_mutex_unlock(cm_cfg->cur_cfg_lock);
        configurator_config_unlock(cur_cfg);
    } else {
        osal_mutex_unlock(cm_cfg->cur_cfg_lock);
    }

    return 0;
}

static int cm_cfg_client_alter(
            configurator_t *c,
            configurator_client_t *client,
            void *client_prv,
            void *config
        )
{
    cm_cfg_t *cm_cfg;
    cm_cfg = client_prv;
    if (cm_cfg->alter_func) {
        return cm_cfg->alter_func(cm_cfg, cm_cfg->prv, config);
    }
    return 0;
}

static void cm_cfg_client_notify(
            configurator_t *c,
            configurator_client_t *client,
            void *client_prv,
            void *config
        )
{
    cm_cfg_t *cm_cfg;
    cam_cfg_t *old_cfg;

    cm_cfg = client_prv;

    configurator_config_lock(config);

    osal_mutex_lock(cm_cfg->cur_cfg_lock);
    old_cfg = cm_cfg->cur_cfg;
    cm_cfg->cur_cfg = config;
    osal_mutex_unlock(cm_cfg->cur_cfg_lock);

    configurator_config_unlock(old_cfg);

    if (cm_cfg->notify_func) {
        cm_cfg->notify_func(cm_cfg, cm_cfg->prv);
    }
}

static configurator_client_desc_t cm_cfg_client_desc = {
    .name = "Camera Mode",
    .registered = cm_cfg_client_reg,
    .unregistered = cm_cfg_client_unreg,
    .alternate = cm_cfg_client_alter,
    .notify = cm_cfg_client_notify
};

/*
* ***************************************************************************
* ** CFG Interface create/destroy/get_cur ***********************************
* ***************************************************************************
*/
cam_cfg_t *cm_cfg_get_cur(cm_cfg_t *cm_cfg)
{
    cam_cfg_t *cur;

    osal_mutex_lock(cm_cfg->cur_cfg_lock);
    cur = cm_cfg->cur_cfg;
    configurator_config_lock(cur);
    osal_mutex_unlock(cm_cfg->cur_cfg_lock);

    return cur;
}

void cm_cfg_destroy(cm_cfg_t *cm_cfg)
{
    configurator_client_unregister(
            cm_cfg->configurator,
            cm_cfg->configurator_client
        );
    osal_mutex_destroy(cm_cfg->cur_cfg_lock);
    osal_free(cm_cfg);
}

cm_cfg_t *cm_cfg_create(
        configurator_t *configurator,
        cm_cfg_alter_func_t *alter_func,
        cm_cfg_notify_func_t *notify_func,
        void *prv
    )
{
    cm_cfg_t *cm_cfg;

    cm_cfg = osal_calloc(1, sizeof (*cm_cfg));
    if (!cm_cfg) {
        mmsdbg(
                DL_ERROR,
                "Failed to allocate memory for new "
                "Camera Mode Config instance: size=%d!",
                sizeof (*cm_cfg)
            );
        goto exit1;
    }
    cm_cfg->configurator = configurator;
    cm_cfg->alter_func = alter_func;
    cm_cfg->notify_func = notify_func;
    cm_cfg->prv = prv;

    cm_cfg->cur_cfg_lock = osal_mutex_create();
    if (!cm_cfg->cur_cfg_lock) {
        mmsdbg(
                DL_ERROR,
                "Failed to create current config lock mutex!"
            );
        goto exit2;
    }

    cm_cfg->configurator_client = configurator_client_register(
            cm_cfg->configurator,
            &cm_cfg_client_desc,
            cm_cfg
        );
    if (!cm_cfg->configurator_client) {
        mmsdbg(
                DL_ERROR,
                "Failed to register in configurator!"
            );
        goto exit3;
    }

    return cm_cfg;
exit3:
    osal_mutex_destroy(cm_cfg->cur_cfg_lock);
exit2:
    osal_free(cm_cfg);
exit1:
    return NULL;
}

