/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file camera_mode_fr.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <osal/osal_stdlib.h>
#include <osal/osal_string.h>
#include <osal/osal_mutex.h>
#include <osal/osal_list.h>
#include <utils/mms_debug.h>

#include <osal/pool.h>
#include <framerequest/camera/camera_frame_request.h>

#include <camera_internal.h>
#include <camera_mode_fr.h>

#define CM_MOV_TIMEOUT DEFAULT_ALLOC_TIMEOUT_MS

typedef struct cm_fr cm_fr_t;

typedef struct {
    struct list_head link_flushed;
    cm_fr_t *cm_fr;
    camera_fr_t *fr;
} cm_fr_prv_t;

struct cm_fr {
    camera_t *camera;
    int num;
    cm_fr_duplicate_t *fr_duplicate;
    void *prv;
    pool_t *pool;
    pool_t *pool_prv;
    struct list_head flushed_list;
    osal_mutex *flushed_list_lock;
};

mmsdbg_define_variable(
        vdl_camera_mode_fr,
        DL_DEFAULT,
        0,
        "vdl_camera_mode_fr",
        "Common Frame Request routines used in camera mode."
    );
#define MMSDEBUGLEVEL mmsdbg_use_variable(vdl_camera_mode_fr)

/*
* ***************************************************************************
* ** FR Private *************************************************************
* ***************************************************************************
*/
static void fr_set_prv(camera_fr_t *fr, void *prv)
{
    fr->n.app_prv.com.fmt = CAMERA_FR_ENTRY_FORMAT__APP_PRIVATE;
    fr->n.app_prv.data = prv;
}

static void *fr_get_prv(camera_fr_t *fr)
{
    return fr->n.app_prv.data;
}

/*
* ***************************************************************************
* ** FR Make ****************************************************************
* ***************************************************************************
*/
void cm_fr_port_entry_set(camera_fr_t *fr, int n, int port)
{
    cm_fr_prv_t *fr_prv;
    void *buf;
    int err;

    fr_prv = fr_get_prv(fr);

    if (!camera_i_port_q_req_is_empty(fr_prv->cm_fr->camera, port)) {
        err = camera_i_port_q_req_deq(fr_prv->cm_fr->camera, port, &buf);
        if (err) {
            mmsdbg(
                    DL_ERROR,
                    "Failed to dequeue from request port queue (port=%d)!",
                    port
                );
            return; /* TODO: Abort this frame request? */
        }
        fr->arr[n].com.fmt = CAMERA_FR_ENTRY_FORMAT__EXT;
        fr->arr[n].com.port = port;
        fr->arr[n].data = buf;
    }
}

camera_fr_t *cm_fr_make(cm_fr_t *cm_fr)
{
    camera_fr_t *fr;
    cm_fr_prv_t *fr_prv;

    fr = pool_alloc_timeout(cm_fr->pool, CM_MOV_TIMEOUT);
    if (!fr) {
        mmsdbg(DL_ERROR, "Failed to allocate new frame request!");
        goto exit1;
    }
    osal_memset(fr, 0, sizeof (*fr));

    fr_prv = pool_alloc_timeout(cm_fr->pool_prv, CM_MOV_TIMEOUT);
    if (!fr_prv) {
        mmsdbg(DL_ERROR, "Failed to allocate new frame request private!");
        goto exit2;
    }

    INIT_LIST_HEAD(&fr_prv->link_flushed);
    fr_prv->cm_fr = cm_fr;
    fr_prv->fr = fr;
    fr_set_prv(fr, fr_prv);

    return fr;
exit2:
    osal_free(fr);
exit1:
    return NULL;
}

/*
 * *****************************************************************************
 * ** Duplicate frame request **************************************************
 * *****************************************************************************
 */
camera_fr_t *camera_fr_duplicate(camera_fr_t *fr_src)
{
    cm_fr_t *cm_fr;
    cm_fr_prv_t *fr_src_prv;

    fr_src_prv = fr_get_prv(fr_src);
    cm_fr = fr_src_prv->cm_fr;

    if (!cm_fr->fr_duplicate) {
        mmsdbg(
                DL_ERROR,
                "Frame request duplication is "
                "not supported in this usecase!"
            );
        return NULL;
    }

    return cm_fr->fr_duplicate(cm_fr, cm_fr->prv, fr_src);
}

/*
* ***************************************************************************
* ** FR Process done ********************************************************
* ***************************************************************************
*/
void cm_fr_process_done_on_port_entry(camera_fr_t *fr, int n)
{
    cm_fr_prv_t *fr_prv;
    int err;

    fr_prv = fr_get_prv(fr);

    switch (fr->arr[n].com.fmt) {
        case CAMERA_FR_ENTRY_FORMAT__UNUSED:
            break;
        case CAMERA_FR_ENTRY_FORMAT__EXT:
            err = camera_i_process_done(
                    fr_prv->cm_fr->camera,
                    fr->arr[n].com.port,
                    fr->arr[n].data
                );
            if (err) {
                mmsdbg(DL_ERROR, "Failed to return AIBH buffer to host!");
            }
            fr->arr[n].com.fmt = CAMERA_FR_ENTRY_FORMAT__EXT_SENT;
            break;
        case CAMERA_FR_ENTRY_FORMAT__EXT_SENT:
            break;
        case CAMERA_FR_ENTRY_FORMAT__EXT_SKIP:
            err = camera_i_port_q_req_enq(
                    fr_prv->cm_fr->camera,
                    fr->arr[n].com.port,
                    fr->arr[n].data
                );
            if (err) {
                mmsdbg(DL_ERROR, "Failed re-enqueue skipped AIBH buffer!");
            }
            fr->arr[n].com.fmt = CAMERA_FR_ENTRY_FORMAT__EXT_SENT;
            break;
        default:
            mmsdbg(
                    DL_ERROR,
                    "Unexpected Frame Request type: %d!",
                    fr->arr[n].com.v
                );
    }
}

/*
* ***************************************************************************
* ** FR Flush ***************************************************************
* ***************************************************************************
*/
void cm_fr_entry_flush(camera_fr_t *fr, int n)
{
    cm_fr_prv_t *fr_prv;
    int err;

    fr_prv = fr_get_prv(fr);

    switch (fr->arr[n].com.fmt) {
        case CAMERA_FR_ENTRY_FORMAT__UNUSED:
        case CAMERA_FR_ENTRY_FORMAT__UNDEFINED:
        case CAMERA_FR_ENTRY_FORMAT__NO_FREE:
        case CAMERA_FR_ENTRY_FORMAT__OSAL:
        case CAMERA_FR_ENTRY_FORMAT__APP_PRIVATE:
        case CAMERA_FR_ENTRY_FORMAT__CONFIG:
        case CAMERA_FR_ENTRY_FORMAT__EXT_SENT:
            break;
        case CAMERA_FR_ENTRY_FORMAT__EXT:
        case CAMERA_FR_ENTRY_FORMAT__EXT_SKIP:
            err = camera_i_port_q_req_enq(
                    fr_prv->cm_fr->camera,
                    fr->arr[n].com.port,
                    fr->arr[n].data
                );
            if (err) {
                mmsdbg(
                        DL_ERROR,
                        "Failed to re-enqueue flushed external buffer!"
                    );
            }
            fr->arr[n].com.fmt = CAMERA_FR_ENTRY_FORMAT__EXT_SENT;
            break;
        default:
            mmsdbg(
                    DL_ERROR,
                    "Unexpected Frame Request type: %d!",
                    fr->arr[n].com.v
                );
    }
}

void cm_fr_flush(camera_fr_t *fr)
{
    cm_fr_prv_t *fr_prv;
    int i;

    fr_prv = fr_get_prv(fr);

    for (i = 0; i < CAMERA_FR_ARR_SIZE; i++) {
        cm_fr_entry_flush(fr, i);
    }
}

/*
* ***************************************************************************
* ** FR Destroy *************************************************************
* ***************************************************************************
*/
void cm_fr_entry_destroy(camera_fr_entry_t *fr_entry)
{
    switch (fr_entry->com.fmt) {
        case CAMERA_FR_ENTRY_FORMAT__UNUSED:
        case CAMERA_FR_ENTRY_FORMAT__UNDEFINED:
        case CAMERA_FR_ENTRY_FORMAT__NO_FREE:
            break;
        case CAMERA_FR_ENTRY_FORMAT__OSAL:
            osal_free(fr_entry->data);
            break;
        case CAMERA_FR_ENTRY_FORMAT__APP_PRIVATE:
            osal_free(fr_entry->data);
            break;
        case CAMERA_FR_ENTRY_FORMAT__CONFIG:
            configurator_config_unlock(fr_entry->data);
            break;
        case CAMERA_FR_ENTRY_FORMAT__EXT:
        case CAMERA_FR_ENTRY_FORMAT__EXT_SENT:
        case CAMERA_FR_ENTRY_FORMAT__EXT_SKIP:
            break;
        default:
            mmsdbg(
                    DL_ERROR,
                    "Unexpected Frame Request type: %d!",
                    fr_entry->com.v
                );
    }
}

void cm_fr_destroy(camera_fr_t *fr)
{
    int i;
    for (i = 0; i < CAMERA_FR_ARR_SIZE; i++) {
        cm_fr_entry_destroy(fr->arr + i);
    }
    osal_free(fr);
}

/*
* ***************************************************************************
* ** Flushed list ***********************************************************
* ***************************************************************************
*/
void cm_flushed_fr_list_add_unique(camera_fr_t *fr)
{
    cm_fr_t *cm_fr;
    cm_fr_prv_t *fr_prv;

    fr_prv = fr_get_prv(fr);
    cm_fr = fr_prv->cm_fr;

    osal_mutex_lock(cm_fr->flushed_list_lock);
    list_for_each_entry(fr_prv, &cm_fr->flushed_list, link_flushed) {
        if (fr_prv->fr == fr) {
            osal_mutex_unlock(cm_fr->flushed_list_lock);
            return;
        }
    }
    fr_prv = fr_get_prv(fr);
    list_add(&fr_prv->link_flushed, &cm_fr->flushed_list);
    osal_mutex_unlock(cm_fr->flushed_list_lock);
}

void cm_flushed_fr_list_for_each(
        cm_fr_t *cm_fr,
        void (*func)(camera_fr_t *fr)
    )
{
    cm_fr_prv_t *c, *n;
    osal_mutex_lock(cm_fr->flushed_list_lock);
    list_for_each_entry_safe(c, n, &cm_fr->flushed_list, link_flushed) {
        func(c->fr);
    }
    osal_mutex_unlock(cm_fr->flushed_list_lock);
}

void cm_fr_list_elem_flush(camera_fr_t *fr)
{
    cm_fr_flush(fr);
}

void cm_fr_list_elem_free(camera_fr_t *fr)
{
    cm_fr_prv_t *fr_prv;
    fr_prv = fr_get_prv(fr);
    list_del(&fr_prv->link_flushed);
    cm_fr_destroy(fr_prv->fr);
}

/*
* ***************************************************************************
* ** FR module create/destroy ***********************************************
* ***************************************************************************
*/
void cm_fr_module_destroy(cm_fr_t *cm_fr)
{
    osal_mutex_destroy(cm_fr->flushed_list_lock);
    pool_destroy(cm_fr->pool_prv);
    pool_destroy(cm_fr->pool);
    osal_free(cm_fr);
}

cm_fr_t *cm_fr_module_create(
        camera_t *camera,
        int num,
        cm_fr_duplicate_t *fr_duplicate,
        void *prv
    )
{
    cm_fr_t *cm_fr;

    cm_fr = osal_calloc(1, sizeof (*cm_fr));
    if (!cm_fr) {
        mmsdbg(
                DL_ERROR,
                "Failed to allocate memory for new "
                "Camera Mode Frame Request instance: size=%d!",
                sizeof (*cm_fr)
            );
        goto exit1;
    }
    cm_fr->camera = camera;
    cm_fr->num = num;
    cm_fr->fr_duplicate = fr_duplicate;
    cm_fr->prv = prv;

    cm_fr->pool = pool_create("CM FR POOL", sizeof (camera_fr_t), cm_fr->num);
    if (!cm_fr->pool) {
        mmsdbg(
                DL_ERROR,
                "Failed to create frame request pool!"
            );
        goto exit2;
    }

    cm_fr->pool_prv = pool_create("CM FR POOL PRV", sizeof (cm_fr_prv_t), cm_fr->num);
    if (!cm_fr->pool_prv) {
        mmsdbg(
                DL_ERROR,
                "Failed to create frame request private pool!"
            );
        goto exit3;
    }

    INIT_LIST_HEAD(&cm_fr->flushed_list);
    cm_fr->flushed_list_lock = osal_mutex_create();
    if (!cm_fr->flushed_list_lock) {
        mmsdbg(
                DL_ERROR,
                "Failed to create flushed list lock mutex!"
            );
        goto exit4;
    }

    return cm_fr;
exit4:
    pool_destroy(cm_fr->pool_prv);
exit3:
    pool_destroy(cm_fr->pool);
exit2:
    osal_free(cm_fr);
exit1:
    return NULL;
}

