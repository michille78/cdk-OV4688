/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file mov.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <osal/osal_stdlib.h>
#include <osal/osal_string.h>
#include <osal/osal_list.h>
#include <utils/mms_debug.h>

#include <framerequest/camera/camera_frame_request.h>
#include <configurator/include/configurator.h>
#include <pipe/include/pipe_desc.h>
#include <pipe/include/pipe_event_id.h>
#include <pipe/include/pipe.h>

#include <control_logic/include/inc_control_logic.h>
#include <auto_camera_algs/include/inc_auto_camera_algs.h>
#include <camera/include/inc_camera.h>
#include <frame_info/include/inc_frame_info.h>

#include <cam_config.h>
#include <camera_mode.h>
#include <camera_mode_desc.h>
#include <camera_mode_fr.h>
#include <camera_mode_cfg.h>
#include <camera_mode_pipe.h>
#include <camera_internal.h>

#define MOV__PORT__EXT 0
#define MOV__PORT__CFG 1

#define MOV__PIPE_NOTIFY_ID__FRAME_READY 0x1
#define MOV__PIPE_NOTIFY_ID__CFG 0x2

#define MOV_CAP_RES_OFFSET(FIELD) \
    INC_RES_OFFSET(mov_capture_res_t, FIELD)

#ifdef GZZ_PIPE_FR_NUM_MAX
#define MOV_PIPE_FR_NUM_MAX     GZZ_PIPE_FR_NUM_MAX
#define MOV_METADATA_BUFF_NUM_MAX  (GZZ_PIPE_FR_NUM_MAX-1)
#else
#define MOV_PIPE_FR_NUM_MAX 6
#define MOV_METADATA_BUFF_NUM_MAX  5
#endif


typedef struct {
    int camera_id;
    int mov_pipe_fr_num_max;
    cam_cfg_t *cfg;
} mov_capture_res_t;

struct camera_mode_mov {
    camera_modes_t mode;
    camera_t *camera;
    configurator_t *configurator;
    cm_cfg_t *cfg;
    cm_pipe_t *pipes;
    mov_capture_res_t res;
    unsigned int last_request_id;
};

mmsdbg_define_variable(
        vdl_camrea_mode_mov_config_driven,
        DL_DEFAULT,
        0,
        "vdl_camrea_mode_mov_config_driven",
        "Camera Mode Mov."
    );
#define MMSDEBUGLEVEL mmsdbg_use_variable(vdl_camrea_mode_mov_config_driven)

/*
* ***************************************************************************
* ** Nodes params ***********************************************************
* ***************************************************************************
*/

typedef enum mov_fr_type {
    MOV_FR_TYPE_NORMAL,
    MOV_FR_TYPE_DUPLICATE,
} mov_fr_type_t;

static INC_CONTROL_LOGIC_CPARAMS_T control_logic_params = {
    CAMERA_FR_OFFSET(n.cfg),                                /* fr_offset_cfg        */
    CAMERA_FR_OFFSET(n.mov.cl_fr_ctrl_img),                 /* fr_offset_img     */
    CAMERA_FR_OFFSET(n.mov.cl_fr_ctrl_aca),                 /* fr_offset_aca     */
    CAMERA_FR_OFFSET(n.mov.cl_lights_ctrl),                 /* fr_offset_lights     */
    CAMERA_FR_OFFSET(n.mov.algo_state),                     /* fr_offset_algo_state */
    CAMERA_FR_OFFSET(n.mov.vpipe),                          /* fr_offset_vpipe      */
    MOV_CAP_RES_OFFSET(cfg),                                /* res_offset_cfg       */
    MOV_CAP_RES_OFFSET(camera_id),                          /* res_offset_cametra_id*/
    MOV_PIPE_FR_NUM_MAX,                                    /* max_fr               */
};

static INC_CAMERA_CPARAMS_T camera_drv_params = {
    CAMERA_FR_OFFSET(n.cfg),                                /* fr_offset_cfg        */
    CAMERA_FR_OFFSET(n.mov.cl_fr_ctrl_img),                 /* fr_offset_img        */
    CAMERA_FR_OFFSET(n.mov.cl_fr_ctrl_aca),                 /* fr_offset_aca        */
    CAMERA_FR_OFFSET(n.mov.cl_lights_ctrl),                 /* fr_offset_lights     */
    CAMERA_FR_OFFSET(n.mov.algo_state),                     /* fr_offset_algo_state */
    CAMERA_FR_OFFSET(n.mov.aewb_stats),                     /* fr_offset_aewb_stats */
    CAMERA_FR_OFFSET(n.mov.af_stats),                       /* fr_offset_af_stats   */
    CAMERA_FR_OFFSET(n.mov.vpipe),                          /* fr_offset_vpipe      */
    CAMERA_FR_OFFSET(n.mov.cm_dyn_data),                    /* virt_cm_dyn_props_t  */
    MOV_CAP_RES_OFFSET(cfg),                                /* res_offset_cfg       */
    MOV_CAP_RES_OFFSET(camera_id),                          /* res_offset_cametra_id*/
    MOV_PIPE_FR_NUM_MAX,                                    /* max_fr               */
};

static INC_AUTO_CAM_ALGS_CPARAMS_T aca_params = {
    CAMERA_FR_OFFSET(n.cfg),                                /* fr_offset_cfg        */
    CAMERA_FR_OFFSET(n.mov.cl_fr_ctrl_img),                 /* fr_img_offset     */
    CAMERA_FR_OFFSET(n.mov.cl_fr_ctrl_aca),                 /* fr_aca_offset     */
    CAMERA_FR_OFFSET(n.mov.algo_state),                     /* fr_offset_algo_state */
    CAMERA_FR_OFFSET(n.mov.aewb_stats),                     /* fr_offset_aewb_stats */
    CAMERA_FR_OFFSET(n.mov.af_stats),                       /* fr_offset_af_stats   */
    CAMERA_FR_OFFSET(n.mov.vpipe),                          /* fr_offset_vpipe      */
    MOV_CAP_RES_OFFSET(cfg),                                /* res_offset_cfg       */
    MOV_CAP_RES_OFFSET(camera_id),                          /* res_offset_cametra_id*/
    MOV_PIPE_FR_NUM_MAX,                                    /* max_fr               */
};

static inc_frame_info_params_t frame_info_params = {
    CAMERA_FR_OFFSET(n.cfg),                                /* fr_offset_cfg        */
    CAMERA_FR_OFFSET(n.mov.algo_state),                     /* fr_offset_algo_state */
    CAMERA_FR_OFFSET(n.mov.af_stats),                       /* fr_offset_af_stats   */
    CAMERA_FR_OFFSET(n.mov.vpipe),                          /* fr_offset_vpipe      */
    CAMERA_FR_OFFSET(n.mov.cm_dyn_data),                    /* virt_cm_dyn_props_t  */
    MOV_CAP_RES_OFFSET(cfg),                                /* res_offset_cfg       */
    MOV_CAP_RES_OFFSET(camera_id),                          /* res_offset_cametra_id*/
    MOV_METADATA_BUFF_NUM_MAX                               /* number_of_buffers    */
};

#include "mov_pipe.c"

/* *INDENT-OFF* */

CM_DESC_BUILD_BEGIN(CM_DESC_MOV)
    CM_PIPE_DESC_BUILD_BEGIN(mov_pipe, MOV__PIPE_NOTIFY_ID__FRAME_READY, MOV_PIPE_FR_NUM_MAX)
        CM_PIPE_PORT_ENTRY(
                MOV__PORT__CFG,
                1,
                MOV__PIPE_NOTIFY_ID__CFG,
                CAMERA_FR_OFFSET(n.cfg))
    CM_PIPE_DESC_BUILD_END()
CM_DESC_BUILD_END(CM_DESC_MOV)

/* *INDENT-ON* */

static void pipe_callback(
        pipe_t *pipe,
        void *app_prv,
        pipe_event_id_t event_id,
        int num,
        void *ptr
    )
{
    camera_mode_t *cm;
    camera_mode_mov_t *prv;
    cm_pipe_info_t *cm_pipe_info;
    camera_fr_t *fr;
    cam_cfg_t * cfg;

    cm_pipe_info = app_prv;
    cm = cm_pipe_info->prv.ptr;
    prv = cm->prv.ptr;

    switch (event_id.type) {
        case PIPE_EVENT_INC:
            fr = ptr;
            cfg = fr->n.cfg.data;
            if ((MOV__PIPE_NOTIFY_ID__FRAME_READY == event_id.inc) &&
                (CAM_CAPTURE_INTENT_ZERO_SHUTTER_LAG != cfg->cam_gzz_cfg.cam_mode.val) &&
                (MOV_FR_TYPE_DUPLICATE != fr->n.mov.mov_fr_type.data)){
                camera_i_notify(
                        prv->camera,
                        1,
                        0,
                        NULL
                    );
            }
            break;
    }
    cm_pipe_callback(pipe, app_prv, event_id, num, ptr);
}

/*
 * *****************************************************************************
 * ** UC Specific frame duplication ********************************************
 * *****************************************************************************
 */
static camera_fr_t * mov_fr_duplicate(
        cm_fr_t *cm_fr,
        cm_pipe_info_t *cm_pipe_info,
        void *_prv,
        camera_fr_t *fr_src
    )
{
    camera_fr_t *fr_dup;

    fr_dup = cm_fr_make(cm_fr);
    if (!fr_dup) {
        mmsdbg(DL_ERROR, "Failed to make Frame Request!");
        goto exit1;
    }

    configurator_config_lock(fr_src->n.cfg.data);
    fr_dup->n.cfg.com.v = fr_src->n.cfg.com.v;
    fr_dup->n.cfg.data = fr_src->n.cfg.data;
    fr_dup->n.mov.mov_fr_type.data = MOV_FR_TYPE_DUPLICATE;

    return fr_dup;
exit1:
    return NULL;
}

/*
* ***************************************************************************
* ** Intrface Part **********************************************************
* ***************************************************************************
*/

static void cfg_notify(cm_cfg_t *cm_cfg, void *private)
{
    camera_mode_mov_t *prv;
    cam_cfg_t *cfg;
    int err;

    prv = private;

    cfg = cm_cfg_get_cur(cm_cfg);

    if (prv->last_request_id != cfg->cam_gzz_cfg.request_id) {
        err = camera_i_port_q_req_enq(
                prv->camera,
                MOV__PORT__CFG,
                cfg
            );
        if (err) {
            mmsdbg(
                    DL_ERROR,
                    "Failed to enqueue config!"
                );
            configurator_config_unlock(cfg);
        }
        prv->last_request_id = cfg->cam_gzz_cfg.request_id;
    } else {
        configurator_config_unlock(cfg);
    }
}

static int mov_start(camera_mode_t *cm)
{
    camera_mode_mov_t *prv;
    int err;

    prv = cm->prv.ptr;

    prv->res.cfg = cm_cfg_get_cur(prv->cfg);

    err = cm_pipe_start_all(prv->pipes, &prv->res);
    if (err) {
        mmsdbg(DL_ERROR, "Failed to start Camera Mode Pipes!");
    }

    configurator_config_unlock(prv->res.cfg);
    prv->res.cfg = NULL;

    return err;
}

static void mov_stop(camera_mode_t *cm)
{
    camera_mode_mov_t *prv;
    void *cfg;
    int err;

    prv = cm->prv.ptr;

    while (!camera_i_port_q_req_is_empty(prv->camera, MOV__PORT__CFG)) {
        err = camera_i_port_q_req_deq(prv->camera, MOV__PORT__CFG, &cfg);
        if (err) {
            mmsdbg(
                    DL_ERROR,
                    "Failed to dequeue config (form non empty camera port)!"
                );
            continue;
        }
        configurator_config_unlock(cfg);
    }

    cm_pipe_flush_all(prv->pipes);
    cm_pipe_stop_all(prv->pipes);
}

static void mov_flush(camera_mode_t *cm)
{
    camera_mode_mov_t *prv;
    void *cfg;
    int err;

    prv = cm->prv.ptr;

    while (!camera_i_port_q_req_is_empty(prv->camera, MOV__PORT__CFG)) {
        err = camera_i_port_q_req_deq(prv->camera, MOV__PORT__CFG, &cfg);
        if (err) {
            mmsdbg(
                    DL_ERROR,
                    "Failed to dequeue config (form non empty camera port)!"
                );
            continue;
        }
        configurator_config_unlock(cfg);
    }

    cm_pipe_flush_all(prv->pipes);
}

static int process_single(camera_mode_mov_t *prv, cm_pipe_info_t *cm_pipe_info)
{
    camera_fr_t *fr;
    cm_port_desc_t *port_desc;
    int err;

    fr = cm_fr_make(cm_pipe_info->cm_fr);
    if (!fr) {
        mmsdbg(DL_ERROR, "Failed to make Frame Request!");
        goto exit1;
    }

    CM_PIPE_PORT_FOR_EACH(port_desc, cm_pipe_info->desc->port_desc) {
        cm_fr_port_entry_set(fr, port_desc->pos_in_fr, port_desc->port); /* TODO: Abort FR on error? */
    }

    fr->n.cfg.com.fmt = CAMERA_FR_ENTRY_FORMAT__CONFIG; /* TODO: better solution */

    err = pipe_process(cm_pipe_info->pipe, fr);
    if (err) {
        mmsdbg(DL_ERROR, "Pipe failed to process!");
        goto exit2;
    }

    return 0;
exit2:
    cm_fr_destroy(fr);
exit1:
    return -1;
}

static int mov_process(camera_mode_t *cm)
{
    camera_mode_mov_t *prv;
    LIST_HEAD(ready_list);
    cm_pipe_info_t *cm_pipe_info;
    int err;

    prv = cm->prv.ptr;
    err = 0;

    cm_pipe_ready_to_process(prv->pipes, &ready_list);
    list_for_each_entry(cm_pipe_info, &ready_list, link_ready) {
        if (process_single(prv, cm_pipe_info)) {
            err = -1;
        }
    }

    return err;
}

static void mov_destroy(camera_mode_t *cm)
{
    camera_mode_mov_t *prv;
    prv = cm->prv.ptr;
    cm_pipe_destroy(prv->pipes);
    cm_cfg_destroy(prv->cfg);
    osal_free(prv);
}

int camera_mode_mov_config_driven_create(camera_mode_t *cm, camera_mode_params_t *params)
{
    camera_mode_mov_t *prv;

    prv = osal_calloc(1, sizeof (*prv));
    if (!prv) {
        mmsdbg(
                DL_ERROR,
                "Failed to allocate memory for new "
                "Mov Camera Mode instance: size=%d!",
                sizeof (*prv)
            );
        goto exit1;
    }
    prv->mode = params->mode;
    prv->camera = params->camera;
    prv->configurator = params->configurator;
    prv->last_request_id = 0;

    prv->cfg = cm_cfg_create(prv->configurator, NULL, cfg_notify, prv);
    if (!prv->cfg) {
        mmsdbg(
                DL_ERROR,
                "Failed to create current Camera Mode Config!"
            );
        goto exit2;
    }

    prv->res.camera_id = camera_i_get_id(prv->camera);
    prv->res.mov_pipe_fr_num_max = MOV_PIPE_FR_NUM_MAX;
    prv->res.cfg = cm_cfg_get_cur(prv->cfg);

    prv->pipes = cm_pipe_create(
            prv->camera,
            prv->configurator,
            CM_DESC_MOV.pipe_desc,
            &prv->res,
            mov_fr_duplicate,
            pipe_callback,
            cm
        );
    if (!prv->pipes) {
        configurator_config_unlock(prv->res.cfg);
        mmsdbg(
                DL_ERROR,
                "Failed to create current Camera Mode Pipe!"
            );
        goto exit3;
    }

    configurator_config_unlock(prv->res.cfg);
    prv->res.cfg = NULL;

    cm->prv.ptr = prv;
    cm->mode_start = mov_start;
    cm->mode_stop = mov_stop;
    cm->mode_flush = mov_flush;
    cm->mode_process = mov_process;
    cm->mode_destroy = mov_destroy;

    return 0;
exit3:
    cm_cfg_destroy(prv->cfg);
exit2:
    osal_free(prv);
exit1:
    return -1;
}

