/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file 
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

/* *INDENT-OFF* */

NODE_DESC_DECLARE(MovCamDrv)
NODE_DESC_DECLARE(MovACA)
NODE_DESC_DECLARE(MovCL)
NODE_DESC_DECLARE(MovFrmInfo)

NODE_DESC_BUILD_BEGIN(MovCamDrv, "camera", 4096, 5, camera_drv_params, 0)
    NODE_DESC_IN_EVENTS_BEGIN(MovCamDrv)
        NODE_DESC_IN_EVENT(MovCL, 0, 0)
    NODE_DESC_IN_EVENTS_END(MovCamDrv)
    NODE_DESC_NEXT_BEGIN(MovCamDrv)
        NODE_DESC_NEXT(MovACA)
    NODE_DESC_NEXT_END(MovCamDrv)
    NODE_DESC_OUT_EVENTS_BEGIN(MovCamDrv)
        NODE_DESC_OUT_EVENT(0, 0)
    NODE_DESC_OUT_EVENTS_END(MovCamDrv)
NODE_DESC_BUILD_END(MovCamDrv)

NODE_DESC_BUILD_BEGIN(MovACA, "auto_cam_algs", 4096, 5, aca_params, 0)
    NODE_DESC_IN_EVENTS_BEGIN(MovACA)
        NODE_DESC_IN_EVENT(MovCamDrv, 0, 0)
    NODE_DESC_IN_EVENTS_END(MovACA)
    NODE_DESC_NEXT_BEGIN(MovACA)
        NODE_DESC_NEXT(MovFrmInfo)
    NODE_DESC_NEXT_END(MovACA)
    NODE_DESC_OUT_EVENTS_BEGIN(MovACA)
        NODE_DESC_OUT_EVENT(0, 0)
    NODE_DESC_OUT_EVENTS_END(MovACA)
NODE_DESC_BUILD_END(MovACA)

NODE_DESC_BUILD_BEGIN(MovCL, "control_logic", 4096, 5, control_logic_params, 0)
    NODE_DESC_IN_EVENTS_BEGIN(MovCL)
        NODE_DESC_EVENT_SOURCE
    NODE_DESC_IN_EVENTS_END(MovCL)
    NODE_DESC_NEXT_BEGIN(MovCL)
        NODE_DESC_NEXT(MovCamDrv)
    NODE_DESC_NEXT_END(MovCL)
    NODE_DESC_OUT_EVENTS_BEGIN(MovCL)
        NODE_DESC_OUT_EVENT(0, 0)
    NODE_DESC_OUT_EVENTS_END(MovCL)
NODE_DESC_BUILD_END(MovCL)

NODE_DESC_BUILD_BEGIN(MovFrmInfo, "frame_info", 4096, 5, frame_info_params, 0)
    NODE_DESC_IN_EVENTS_BEGIN(MovFrmInfo)
        NODE_DESC_IN_EVENT(MovACA, 0, 0)
    NODE_DESC_IN_EVENTS_END(MovFrmInfo)
    NODE_DESC_NEXT_BEGIN(MovFrmInfo)
    NODE_DESC_NEXT_END(MovFrmInfo)
    NODE_DESC_OUT_EVENTS_BEGIN(MovFrmInfo)
        NODE_DESC_OUT_EVENT(MOV__PIPE_NOTIFY_ID__FRAME_READY, 1)
    NODE_DESC_OUT_EVENTS_END(MovFrmInfo)
NODE_DESC_BUILD_END(MovFrmInfo)

PIPE_DESC(mov_pipe, MovCL)

/* *INDENT-ON* */

