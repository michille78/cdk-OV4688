/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file frame_request_named_entries.h
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

union {
    struct {
        camera_fr_entry_t ext_port_0;
        camera_fr_entry_t cl_fr_ctrl_img;   /* list from cam_cl_algo_ctrl_t     */
        camera_fr_entry_t cl_fr_ctrl_aca;   /* list from cam_cl_algo_ctrl_t     */
        camera_fr_entry_t cl_lights_ctrl;   /* list from hat_light_ctrl_entry_t */
        camera_fr_entry_t algo_state;
        camera_fr_entry_t aewb_stats;       /* hat_h3a_aewb_stat_t       */
        camera_fr_entry_t af_stats;         /* hat_h3a_af_stat_t         */
        camera_fr_entry_t vpipe;            /* vpipe_ctrl_settings_t     */
        camera_fr_entry_t cm_dyn_data;      /* virt_cm_dyn_props_t       */
        camera_fr_entry_t mov_fr_type;
    };
} mov;

