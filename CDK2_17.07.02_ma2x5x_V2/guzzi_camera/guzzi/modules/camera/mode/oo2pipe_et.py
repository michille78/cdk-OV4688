#!/usr/bin/python

import sys
import getopt
import zipfile
from xml.etree import ElementTree
import re

############################################################################
# Node Basic ###############################################################
############################################################################
class NodeInEvent:
    def __init__(self, src_node=None, src_event_id=None, category=None, init_cnt=None):
        self.src_node = src_node
        self.src_event_id = src_event_id
        self.category = category
        self.init_cnt = init_cnt

class NodeOutEvent:
    def __init__(self, event_id=None, notify=None):
        self.event_id = event_id
        self.notify= notify

class Node:
    def __init__(self, name=None, inc=None, stack_size=None, priority=None, create_params=None, data_event_num=None, in_events=None, out_events=None, next=None):
        self.name = name
        self.inc = inc
        self.stack_size = stack_size
        self.priority = priority
        self.create_params = create_params
        self.data_event_num = data_event_num

        self.in_events = in_events
        self.out_events = out_events
        self.next = next

class Pipe:
    def __init__(self, name=None, root=None):
        self.name = name
        self.root = root

############################################################################
# Node File Write - C Style ################################################
############################################################################

c_file_node_decl = """
NODE_DESC_DECLARE({node.name})\
"""

c_file_node_in_event_entry = """
        NODE_DESC_IN_EVENT({ie.src_node.name}, {ie.src_event_id}, {ie.init_cnt})\
"""

c_file_node_in_event_src_entry = """
        NODE_DESC_EVENT_SOURCE\
"""

c_file_node_next_entry = """
        NODE_DESC_NEXT({node.name})\
"""

c_file_node_out_events_entry = """
        NODE_DESC_OUT_EVENT({oe.event_id}, {oe.notify})\
"""

c_file_node_build = """
NODE_DESC_BUILD_BEGIN({node.name}, {node.inc}, {node.stack_size}, {node.priority}, {node.create_params}, {node.data_event_num})
    NODE_DESC_IN_EVENTS_BEGIN({node.name})\
{node_in_events}
    NODE_DESC_IN_EVENTS_END({node.name})
    NODE_DESC_NEXT_BEGIN({node.name})\
{node_next}
    NODE_DESC_NEXT_END({node.name})
    NODE_DESC_OUT_EVENTS_BEGIN({node.name})\
{node_out_events}
    NODE_DESC_OUT_EVENTS_END({node.name})
NODE_DESC_BUILD_END({node.name})
"""

c_file_pipe_desc = """PIPE_DESC({pipe.name}, {pipe.root})
"""

c_file = """\
/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file 
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

/* *INDENT-OFF* */
{nodes_declaration}
{nodes_desc}
{pipe_desc}
/* *INDENT-ON* */

"""

def c_file_writer(file, nodes, pipe):
    def make_node(node):
        node_in_event_src  = reduce(lambda s, ie: s + c_file_node_in_event_src_entry.format(ie=ie), filter(lambda ie: ie.category == 'R', node.in_events), '')
        node_in_event_data = reduce(lambda s, ie: s +     c_file_node_in_event_entry.format(ie=ie), filter(lambda ie: ie.category == 'D', node.in_events), '')
        node_in_event_sync = reduce(lambda s, ie: s +     c_file_node_in_event_entry.format(ie=ie), filter(lambda ie: ie.category == 'S', node.in_events), '')
        node_in_events = node_in_event_src + node_in_event_data + node_in_event_sync
        node_next = reduce(lambda s, n: s + c_file_node_next_entry.format(node=n), node.next, '')
        node_out_events = reduce(lambda s, oe: s + c_file_node_out_events_entry.format(oe=oe), node.out_events, '')

        return c_file_node_build.format(node=node, node_in_events=node_in_events, node_next=node_next, node_out_events=node_out_events)

    nodes_declaration = reduce(lambda s, n: s + c_file_node_decl.format(node=n), nodes, '')
    nodes_desc = reduce(lambda s, n: s + make_node(n), nodes, '')

    pipe_desc = c_file_pipe_desc.format(pipe=pipe)

    print >> file, c_file.format(nodes_declaration=nodes_declaration, nodes_desc=nodes_desc, pipe_desc=pipe_desc),

############################################################################
# Open Office Specific #####################################################
############################################################################
OFFICE_NS   = '{urn:oasis:names:tc:opendocument:xmlns:office:1.0}'
DRAW_NS     = '{urn:oasis:names:tc:opendocument:xmlns:drawing:1.0}'
TEXT_NS     = '{urn:oasis:names:tc:opendocument:xmlns:text:1.0}'
SVG_NS      = '{urn:oasis:names:tc:opendocument:xmlns:svg-compatible:1.0}'

def ns(s): return s.format(office=OFFICE_NS, draw=DRAW_NS, text=TEXT_NS, svg=SVG_NS)

class OoNodeInEvent(NodeInEvent):
    def __init__(self, src_node=None, src_event_id=None, category=None, init_cnt=None, oo_id=None):
        NodeInEvent.__init__(self, src_node, src_event_id, category, init_cnt)
        self.oo_id = oo_id

class OoNodeOutEvent(NodeOutEvent):
    def __init__(self, event_id=None, notify=None, oo_id=None, raw_id=None):
        NodeOutEvent.__init__(self, event_id, notify)
        self.oo_id = oo_id
        self.raw_id = raw_id

class OoNode(Node):
    def __init__(self, name=None, inc=None, stack_size=None, priority=None, create_params=None, in_events=None, out_events=None, next=None):
        Node.__init__(self, name, inc, stack_size, priority, create_params, 0, in_events, out_events, next)

    def oo_match_by_in_oo_id(self, id):
        for ie in self.in_events:
            if ie.oo_id == id: return True
        return False

    def oo_match_by_out_oo_id(self, id):
        for oe in self.out_events:
            if oe.oo_id == id: return True
        return False

    def oo_add_next(self, oo_out_id, next_node, next_node_oo_in_id):
        def filter_by_oo_id(oe, id=oo_out_id): return oe.oo_id == id
        oe = filter(filter_by_oo_id, self.out_events)[0]

        if next_node not in self.next: self.next += [next_node]
        next_node.oo_add_in_event(next_node_oo_in_id, self, oe.event_id)

    def oo_add_in_event(self, oo_in_id, src_node, src_event_id):
        def filter_by_oo_id(ie, id=oo_in_id): return ie.oo_id == id
        ie = filter(filter_by_oo_id, self.in_events)[0]
        ie.src_node = src_node
        ie.src_event_id = src_event_id

class OoNet:
    def __init__(self, start=None, end=None):
        self.start = start
        self.end = end

def oo_get_in_events_from_ooet(et):
    in_event_categories = {'R':'R', 'D':'D', 'S':'S'}

    et_cs_event_input_list = filter(lambda ecs: ecs.find(ns('./{svg}title')).text == 'Event Input', et.findall(ns('./{draw}custom-shape')))

    def get_elem_info(et):
        ie_event = et.find(ns('./{text}p/{text}span')).text
        ie_category, ie_init_cunt = re.search('([RDS]):(\d+)', ie_event).group(1,2)

        if ie_category == 'R':
            ie_oo_id = 'root'
        else:
            ie_oo_id = et.attrib[ns('{draw}id')]

        return OoNodeInEvent(category=in_event_categories[ie_category], init_cnt=ie_init_cunt, oo_id=ie_oo_id)

    return map(get_elem_info, et_cs_event_input_list)

def oo_get_out_events_from_ooet(et):
    et_cs_event_output_list = filter(lambda ecs: ecs.find(ns('./{svg}title')).text == 'Event Output', et.findall(ns('./{draw}custom-shape')))

    def get_elem_info(et):
        oe_prop = et.find(ns('./{svg}desc')).text

        oe_event_id = re.search('\s*Event Name\s*:\s*([\w\(\)]+)', oe_prop).group(1)
        oe_notify   = re.search('\s*Notify\s*:\s*([\w\(\)\|\s]+)', oe_prop).group(1)

        oe_raw_id = int(et.find(ns('./{text}p/{text}span')).text.strip())

        if ns('{draw}id') in et.attrib:
            oe_oo_id = et.attrib[ns('{draw}id')]
        else:
            oe_oo_id = 'not_connected'

        return OoNodeOutEvent(event_id=oe_event_id, notify=oe_notify, oo_id=oe_oo_id, raw_id=oe_raw_id)

    oes = map(get_elem_info, et_cs_event_output_list)
    oes.sort(key=lambda x: x.raw_id)
    return oes

def oo_get_next_from_ooet(et):
    return []

def oo_get_node_from_ooet(et):
    et_cs_node = filter(lambda ecs: ecs.find(ns('./{svg}title')).text == 'Node', et.findall(ns('./{draw}custom-shape')))[0]

    np = et_cs_node.find(ns('./{svg}desc')).text
    nn = et_cs_node.find(ns('./{text}p/{text}span')).text

    np_name             = nn
    np_inc              = re.search('\s*INC\s*:\s*([\w\."]+)'           , np).group(1)
    np_stack_size       = re.search('\s*Stack Size\s*:\s*([\d\*\+-/]+)' , np).group(1)
    np_priority         = re.search('\s*Priority\s*:\s*(\d+)'           , np).group(1)
    np_create_params    = re.search('\s*INC Params\s*:\s*([\w\&]+)'     , np).group(1)

    in_events           = oo_get_in_events_from_ooet(et)
    out_events          = oo_get_out_events_from_ooet(et)
    next                = oo_get_next_from_ooet(et)

    return OoNode(np_name, np_inc, np_stack_size, np_priority, np_create_params, in_events, out_events, next)

def oo_get_net_from_ooet(et):
    start = et.attrib[ns('{draw}start-shape')]
    end = et.attrib[ns('{draw}end-shape')]

    return OoNet(start, end)

def oo_fill_net_info_in_nodes(nodes, nets):
    def find_node_with_in_oo_id(nodes, id):
        def filter_by_oo_id(n, id=id): return n.oo_match_by_in_oo_id(id)
        return filter(filter_by_oo_id, nodes)[0]

    def find_node_with_out_oo_id(nodes, id):
        def filter_by_oo_id(n, id=id): return n.oo_match_by_out_oo_id(id)
        return filter(filter_by_oo_id, nodes)[0]

    def proc_net(net, nodes=nodes):
        src = find_node_with_out_oo_id(nodes, net.start)
        dst = find_node_with_in_oo_id(nodes, net.end)
        src.oo_add_next(net.start, dst, net.end)

    map(proc_net, nets)

def oo_get_nodes_form_ooet(ooet):
    nodes_ooet = filter(lambda eg: len(filter(lambda etitle: etitle.text == 'Node', eg.findall(ns('./{draw}custom-shape/{svg}title')))), ooet.findall(ns('./*/*/*/{draw}g')))
    nodes = map(oo_get_node_from_ooet, nodes_ooet)

    nets_ooet = ooet.findall(ns('./*/*/*/{draw}connector'))
    nets = map(oo_get_net_from_ooet, nets_ooet)

    # Error check - wrong groups
    no_group_nodes = filter(lambda eg: len(filter(lambda etitle: etitle.text == 'Node', eg.findall(ns('./{svg}title')))), ooet.findall(ns('./*/*/*/{draw}custom-shape')))
    for no_group_node in no_group_nodes:
        print 'Node "%s" not in group!' % no_group_node.find(ns('./{text}p/{text}span')).text
    no_group_nodes = filter(lambda eg: len(filter(lambda etitle: etitle.text == 'Event Output', eg.findall(ns('./{svg}title')))), ooet.findall(ns('./*/*/*/{draw}custom-shape')))
    for no_group_node in no_group_nodes:
        print '\n>Event Output at position:\n'+\
              '>  x    = %f\n' % (float(re.search('([\d\.]+)*', no_group_node.attrib[ns('{svg}x')]).group(1)) - 1) + \
              '>  y    = %f\n' % (float(re.search('([\d\.]+)*', no_group_node.attrib[ns('{svg}y')]).group(1)) - 1) + \
              '>with desc: "%s"\n' % no_group_node.find(ns('./{svg}desc')).text + \
              '>is not in any group!\n'
    no_group_nodes = filter(lambda eg: len(filter(lambda etitle: etitle.text == 'Event Input', eg.findall(ns('./{svg}title')))), ooet.findall(ns('./*/*/*/{draw}custom-shape')))
    for no_group_node in no_group_nodes:
        print '\n>Event Input at position:\n'+\
              '>  x    = %f\n' % (float(re.search('([\d\.]+)*', no_group_node.attrib[ns('{svg}x')]).group(1)) - 1) + \
              '>  y    = %f\n' % (float(re.search('([\d\.]+)*', no_group_node.attrib[ns('{svg}y')]).group(1)) - 1) + \
              '>is not in any group!\n'

    # Error check - wrong connection
    groups_with_id = filter(lambda g: ns('{draw}id') in g.attrib, ooet.findall(ns('./*/*/*/{draw}g')))
    for g in groups_with_id:
        id = g.attrib[ns('{draw}id')]
        for n in nets:
            if id == n.start or id == n.end:
                ggg = filter(lambda gg: len(filter(lambda etitle: etitle.text == 'Node', gg.findall(ns('./{svg}title')))), g.findall(ns('./{draw}custom-shape')))[0]
                print 'Wrong connection to: ', ggg.find(ns('./{text}p/{text}span')).text

    oo_fill_net_info_in_nodes(nodes, nets)

    return nodes

def oo_get_nodes_from_odg_file(file_name):
    odg = zipfile.ZipFile(file_name)
    ooxml = odg.read('content.xml')

    ooet = ElementTree.fromstring(ooxml)
    return oo_get_nodes_form_ooet(ooet)

############################################################################
# Pipe name and root node ##################################################
############################################################################
def find_root_node(nodes):
    def is_root(node):
        return filter(lambda ie: ie.category == 'R', node.in_events) != []
    return filter(is_root, nodes)[0]

def get_pipe_name(file_name):
    odg = zipfile.ZipFile(file_name)
    ooxml = odg.read('content.xml')

    ooet = ElementTree.fromstring(ooxml)

    pipe_ooet = filter(lambda eg: len(filter(lambda etitle: etitle.text == 'Pipe', eg.findall(ns('./{svg}title')))), ooet.findall(ns('./*/*/*/{draw}custom-shape')))[0]
    pipe_name = pipe_ooet.find(ns('./{text}p/{text}span')).text

    return pipe_name

############################################################################
# Main #####################################################################
############################################################################
input_file = 'pipe.odg'
output_file = False
try:
    opts, args = getopt.getopt(sys.argv[1:], 'i:o:')
except getopt.GetoptError:
    print 'Usage :\noo2pipe_et.py [-i input_file] [-o output_file]\n'
    sys.exit(1)
for o, a in opts:
    if o == '-i': input_file = a
    if o == '-o': output_file = a

if not output_file:
    output_file = input_file[:input_file.rfind('.')] + '.c'

nodes = oo_get_nodes_from_odg_file(input_file)

pipe = Pipe(get_pipe_name(input_file), find_root_node(nodes).name)

f = open(output_file, 'w')
c_file_writer(f, nodes, pipe)
f.close()

