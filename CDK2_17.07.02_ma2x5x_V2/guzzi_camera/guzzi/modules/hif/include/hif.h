/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file hif.h
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#ifndef __HIF_H__
#define __HIF_H__

#ifdef _cplusplus
extern "C"
{
#endif /* _cplusplus */

#include <osal/osal_stdtypes.h>

/* User code goes here */
/* ------compilation control switches --------------------------------------- */
/****************************************************************
 * INCLUDE FILES
 ***************************************************************/
/* ----- system and platform files ----------------------------*/
/*-------program files ----------------------------------------*/
/* **************************************************************
 *  EXTERNAL REFERENCES NOTE: only use if not found in header file
 ************************************************************** */
/*--------data declarations -----------------------------------*/
/*--------function prototypes ---------------------------------*/
/* **************************************************************
 *  PUBLIC DECLARATIONS Defined here, used elsewhere
 ************************************************************** */
/*--------macros ----------------------------------------------*/
/* defines the major version of the Camera component */

/*--------data declarations -----------------------------------*/
/*--------macros ----------------------------------------------*/
/*--------function prototypes ---------------------------------*/
/* **************************************************************
 *  PRIVATE DECLARATIONS Defined here, used only here
 ************************************************************** */
/*--------macros ----------------------------------------------*/
/*--------function prototypes ---------------------------------*/
/*--------macros ----------------------------------------------*/
#define HIF_HW_VERSION = 0x0010
#define HIF_SW_VERSION = 0x0010

typedef struct
{
    // Sequence number
    uint32 seq_num;
    // Read/Write identifier (see HIF_READ_WRITE enum)
    uint16 read_write;
    // Message target (see HIF_TARGET_ID enum)
    uint16 target;
} hif_spi_header_t;

#define HIF_SPI_HEADER_SIZE   sizeof(hif_spi_header_t)

/**
 * Read and write operations are selected by predefined IDs
 *
 * @param HIF_READ  Given SPI buffer contains READ request(s)
 * @param HIF_WRITE Given SPI buffer contains WRITE request(s)
 * @param HIF_ACK   Given SPI buffer contains ACKNOWLEDGE of given request(s)
 */
typedef enum
{

    HIF_READ,
    HIF_WRITE,
    HIF_ACK

} HIF_READ_WRITE;

/**
 * Target sensor IDs for all supported commands
 *
 * @param HIF_TARGET_SYSTEM (always available/open)
 * @param PRIMARY_CAMERA    camera instance 1
 * @param SECONDARY_CAMERA  camera instance 2
 */
typedef enum
{
    HIF_TARGET_SYSTEM,
    HIF_TARGET_PRIMARY_CAMERA,
    HIF_TARGET_SECONDARY_CAMERA
} HIF_TARGET_ID;

typedef enum
{

    HIF_CAMERA_CAPTURE_INTENT = 0x100,       // 256 (0x100) HIF_CAPTURE_INTENT_TYPE_SIZE            hif_capture_intent_type_t

    // Stream properties
    HIF_CAMERA_OUTPUT_PIX_FORMAT,            // 257 (0x101) HIF_OUTPUT_PIX_FORMAT_SIZE              hif_out_pix_format_t
    HIF_CAMERA_OUTPUT_FRAME_DIMENSIONS,      // 258 (0x102) HIF_SCALER_FRAME_DIMENSIONS_SIZE        hif_scaler_frame_dimensions_t
    HIF_CAMERA_CROP_REGION,                  // 259 (0x103) HIF_SCALER_CROP_REGION_SIZE             hif_scaler_crop_region_t
    HIF_CAMERA_ORIENTATION,                  // 260 (0x104) HIF_CAMERA_ORIENTATION_SIZE             hif_camera_orientation_t

    // Request properties
    HIF_CAMERA_OUTPUT_STREAMS,               // 261 (0x105) HIF_REQUEST_OUTPUT_STREAMS_SIZE         hif_request_output_streams_t
    HIF_CAMERA_FRAME_COUNT,                  // 262 (0x106) HIF_REQUEST_FRAME_COUNT_SIZE            hif_request_frame_count_t
    HIF_CAMERA_REQUEST_ID,                   // 263 (0x107) HIF_REQUEST_ID_SIZE                     hif_request_id_t
    HIF_CAMERA_REQUEST_TYPE,                 // 264 (0x108) HIF_REQUEST_TYPE_SIZE                   hif_request_type_t

    HIF_CAMERA_OUTPUT_STREAM_CONTROL,        // 265 (0x109) HIF_OUTPUT_STREAMS_CONTROL_SIZE         hif_output_streams_control_t
    HIF_CAMERA_METADATA_MODE,                // 266 (0x10A) HIF_REQUEST_METADATA_MODE_SIZE          hif_request_metadata_mode_t

    HIF_CAMERA_FEATURES,                     // 267 (0x10B) HIF_CAMERA_FEATURES_SIZE                hif_camera_features_t
    HIF_CAMERA_SUPPORTED_HARDWARE_LEVEL,     // 268 (0x10C) HIF_CAMERA_SUPPORTED_HW_LEVEL_SIZE      hif_camera_hw_level_t
    HIF_CAMERA_AE_STATE,                     // 269 (0x10D) HIF_CAMERA_AE_STATE_SIZE                hif_camera_ae_state_t
    HIF_CAMERA_AWB_STATE,                    // 270 (0x10E) HIF_CAMERA_AWB_STATE_SIZE               hif_camera_awb_state_t
    HIF_CAMERA_AF_STATE,                     // 271 (0x10F) HIF_CAMERA_AF_STATE_SIZE                hif_camera_af_state_t
    HIF_CAMERA_FLASH_STATE,                  // 272 (0x110) HIF_CAMERA_FLASH_STATE_SIZE             hif_camera_flash_state_t
    HIF_CAMERA_COL_COR_MODE,                 // 273 (0x111) HIF_COLOR_CORRECTION_MODE_SIZE          hif_color_correction_mode_t
    HIF_CAMERA_COL_COR_TRANSFORM,            // 274 (0x112) HIF_COLOR_CORRECTION_TRANSFORM_SIZE     hif_color_correction_transform_t
    HIF_CAMERA_COL_COR_GAIN,                 // 275 (0x113) HIF_COLOR_CORRECTION_GAINS_SIZE         hif_color_correction_gains_t
    HIF_CAMERA_EV,                           // 276 (0x114) HIF_AE_EXPOSURE_COMPENSATION_SIZE       hif_ae_exposure_compensation_t
    HIF_CAMERA_EXP_TIME,                     // 277 (0x115) HIF_CAMERA_EXPOSURE_TIME_SIZE           hif_camera_exposure_time_t
    HIF_CAMERA_FRAME_DURATION,               // 278 (0x116) HIF_CAMERA_FRAME_DURATION_SIZE          hif_camera_frame_duration_t
    HIF_CAMERA_TONEMAP,                      // 279 (0x117) HIF_TONEMAP_MODE_SIZE                   hif_tonemap_mode_t
    HIF_CAMERA_GAMMA_BLUE,                   // 280 (0x118) HIF_TONEMAP_CURVE_BLUE_SIZE             hif_tonemap_curve_blue_t
    HIF_CAMERA_GAMMA_GREEN,                  // 281 (0x119) HIF_TONEMAP_CURVE_GREEN_SIZE            hif_tonemap_curve_green_t
    HIF_CAMERA_GAMMA_RED,                    // 282 (0x11A) HIF_TONEMAP_CURVE_RED_SIZE              hif_tonemap_curve_red_t
    HIF_CAMERA_LSC,                          // 283 (0x11B) HIF_SHADING_MODE_SIZE                   hif_shading_mode_t
    HIF_CAMERA_LSC_MAP,                      // 284 (0x11C) HIF_STATISTICS_LENS_SHADING_MAP_SIZE    hif_statistics_lens_shading_map_t
    HIF_CAMERA_LSC_STRENGTH,                 // 285 (0x11D) HIF_SHADING_STRENGTH_SIZE               hif_shading_strength_t
    HIF_CAMERA_AE_MODE,                      // 286 (0x11E) HIF_AE_MODE_SIZE                        hif_ae_mode_t
    HIF_CAMERA_AE_LOCK,                      // 287 (0x11F) HIF_AE_LOCK_TYPES_SIZE                  hif_ae_lock_type_t
    HIF_CAMERA_AE_REGIONS,                   // 288 (0x120) HIF_AE_REGIONS_SIZE                     hif_ae_regions_t
    HIF_CAMERA_AE_FPS_RANGE,                 // 289 (0x121) HIF_AE_TARGET_FPS_RANGE_SIZE            hif_ae_target_fps_range_t
    HIF_CAMERA_AE_PRECAP_TRIGGER,            // 290 (0x122) HIF_AE_PRECAPTURE_TRIGGERLOCK_TYPE_SIZE hif_ae_precapture_triggerlock_type_t
    HIF_CAMERA_AWB_MODE,                     // 291 (0x123) HIF_AWB_TYPE_SIZE                       hif_awb_type_t
    HIF_CAMERA_AWB_LOCK,                     // 292 (0x124) HIF_AWB_LOCK_TYPE_SIZE                  hif_awb_lock_type_t
    HIF_CAMERA_AWB_REGIONS,                  // 293 (0x125) HIF_AWB_REGIONS_SIZE                    hif_awb_regions_t
    HIF_CAMERA_AF_MODE,                      // 294 (0x126) HIF_AF_MODE_SIZE                        hif_af_mode_t
    HIF_CAMERA_AF_REGIONS,                   // 295 (0x127) HIF_AF_REGIONS_SIZE                     hif_af_regions_t
    HIF_CAMERA_AF_TRIGGER,                   // 296 (0x128) HIF_AF_TRIGGER_SIZE                     hif_af_trigger_t
    HIF_CAMERA_AVAILABLE_EFFECTS,            // 297 (0x129) HIF_AVAILABLE_EFFECTS_TYPE_SIZE         hif_camera_available_effects_t
    HIF_CAMERA_EFFECT,                       // 298 (0x12A) HIF_EFFECT_TYPE_SIZE                    hif_effect_type_t

    HIF_CAMERA_AVAILABLE_SCENES,             // 299 (0x12B) HIF_AVAILABLE_SCENE_MODES_SIZE          hif_camera_available_scenes_t
    HIF_CAMERA_SCENE,                        // 300 (0x12C) HIF_SCENE_MODE_SIZE                     hif_scene_mode_t
    HIF_CAMERA_FLASH,                        // 301 (0x12D) HIF_FLASH_MODE_SIZE                     hif_flash_mode_t
    HIF_CAMERA_FLASH_POWER,                  // 302 (0x12E) HIF_FLASH_FIRING_POWER_SIZE             hif_flash_firing_power_t
    HIF_CAMERA_FLASH_TIME,                   // 303 (0x12F) HIF_FLASH_FIRING_TIME_SIZE              hif_flash_firing_time_t
    HIF_CAMERA_ANTIBANDING,                  // 304 (0x130) HIF_ANTIBANDING_MODE_SIZE               hif_antibanding_mode_t
    HIF_CAMERA_VIDEO_STAB,                   // 305 (0x131) HIF_VIDEO_STABILIZATION_MODE_SIZE       hif_video_stabilization_mode_t
    HIF_CAMERA_EDGE,                         // 306 (0x132) HIF_EDGE_TYPE_SIZE                      hif_edge_type_t
    HIF_CAMERA_EDGE_STRENGTH,                // 307 (0x133) HIF_EDGE_STRENGTH_SIZE                  hif_edge_strength_t
    HIF_CAMERA_MAX_FACE_COUNT,               // 308 (0x134) HIF_MAX_FACE_COUNT_SIZE                 hif_max_face_count_t
    HIF_CAMERA_FACE_DETECTION_MODES,         // 309 (0x135) HIF_SUPPORTED_FACE_DETECT_MODE_SIZE     hif_supported_face_detect_mode_t
    HIF_CAMERA_FACE_DETECTION,               // 310 (0x136) HIF_FACE_DETECT_MODE_SIZE               hif_face_detect_mode_t
    HIF_CAMERA_FACE_IDS,                     // 311 (0x137) HIF_FACE_IDS_SIZE                       hif_face_ids_t
    HIF_CAMERA_FACE_LANDMARKS,               // 312 (0x138) HIF_FACE_LANDMARKS_SIZE                 hif_face_landmarks_t
    HIF_CAMERA_FACE_RECTANGLES,              // 313 (0x139) HIF_FACE_RECTANGLES_SIZE                hif_face_rectangles_t
    HIF_CAMERA_FACE_SCORES,                  // 314 (0x13A) HIF_FACE_SCORES_SIZE                    hif_face_scores_t
    HIF_CAMERA_3A_MODE,                      // 315 (0x13B) HIF_3A_MODE_SIZE                        hif_3a_mode_t
    HIF_CAMERA_DEMOSAIC_MODE,                // 316 (0x13C) HIF_DEMOSAIC_MODE_SIZE                  hif_demosaic_mode_t
    HIF_CAMERA_GEOMETRIC_MODE,               // 317 (0x13D) HIF_GEOMETRIC_MODE_SIZE                 hif_geometric_mode_t
    HIF_CAMERA_GEOMETRIC_STRENGTH,           // 318 (0x13E) HIF_GEOMETRIC_STRENGTH_SIZE             hif_geometric_strength_t
    HIF_CAMERA_HOTPIXEL_MODE,                // 319 (0x13F) HIF_HOT_PIXEL_MODE_SIZE                 hif_hot_pixel_mode_t
    HIF_CAMERA_LENS_APERTURE,                // 320 (0x140) HIF_LENS_APERTURE_SIZE                  hif_lens_aperture_t
    HIF_CAMERA_LENS_FLTR_DENSITY,            // 321 (0x141) HIF_LENS_FILTER_DENSITY_SIZE            hif_lens_filter_density_t
    HIF_CAMERA_LENS_FOCAL_LENGTH,            // 322 (0x142) HIF_LENS_FOCAL_LENGTH_SIZE              hif_lens_focal_length_t
    HIF_CAMERA_LENS_FOCUS_DISTANCE,          // 323 (0x143) HIF_LENS_FOCUS_DISTANCE_SIZE            hif_lens_focus_distance_t
    HIF_CAMERA_LENS_OPTICAL_STAB,            // 324 (0x144) HIF_LENS_OPTICAL_STAB_MODE_SIZE         hif_lens_optical_stab_mode_t
    HIF_CAMERA_NOISE_REDUCTION,              // 325 (0x145) HIF_NOISE_REDUCTION_MODE_SIZE           hif_noise_reduction_mode_t
    HIF_CAMERA_NOISE_RED_STRENGTH,           // 326 (0x146) HIF_NOISE_REDUCTION_STRENGTH_SIZE       hif_noise_reduction_strength_t
    HIF_CAMERA_SENSITIVITY,                  // 327 (0x147) HIF_CAMERA_SENSITIVITY_SIZE             hif_camera_sensitivity_t
    HIF_CAMERA_STATS_HIST_BUCKET_COUNT,      // 328 (0x148) HIF_STATS_HIST_BUCKET_COUNT_SIZE        hif_stats_hist_bucket_count_t
    HIF_CAMERA_STATS_MAX_HIST_COUNT,         // 329 (0x149) HIF_STATS_MAX_HIST_COUNT_SIZE           hif_stats_max_hist_count_t
    HIF_CAMERA_STATS_HIST_COUNT,             // 330 (0x14A) HIF_STATS_HIST_COUNT_SIZE               hif_stats_hist_count_t
    HIF_CAMERA_STATS_HISTOGRAM,              // 331 (0x14B) HIF_HISTOGRAM_MODE_SIZE                 hif_histogram_mode_t
    HIF_CAMERA_STATS_SHARPNESS,              // 332 (0x14C) HIF_SHAPNES_MAP_MODE_SIZE               hif_sharpnes_mode_t
    HIF_CAMERA_STATS_MAX_SHARPNESS_MAP_VALUE,         // 333 (0x14D) HIF_STATS_MAX_SHARP_MAP_VALUE_SIZE      hif_stats_max_sharp_map_value_t
    HIF_CAMERA_STATS_SHARPNESS_MAP,          // 334 (0x14E) HIF_STATS_SHARP_MAP_SIZE                hif_stats_sharp_map_t
    HIF_CAMERA_STATS_SHARPNESS_MAP_SIZE,     // 335 (0x14F) HIF_STATS_SHARP_MAP_SIZE_SIZE           hif_stats_sharp_map_size_t
    HIF_CAMERA_STATS_LSC,                    // 336 (0x150) HIF_LSC_MAP_MODE_SIZE                   hif_lsc_map_mode_t
    HIF_CAMERA_AVAILABLE_LEDS,               // 337 (0x151) HIF_HIST_AVAILABLE_LEDS_SIZE            hif_available_leds_t
    HIF_CAMERA_LED_TRANSMIT,                 // 338 (0x152) HIF_LED_TRANSMIT_MODE_SIZE              hif_led_transmit_mode_t
    HIF_CAMERA_BLACKLEVEL_LOCK,              // 339 (0x153) HIF_BLACK_LEVEL_LOCK_SIZE               hif_black_level_lock_t
    HIF_CAMERA_DATA_TRANSFER,                // 340 (0x154) HIF_CAMERA_DATA_TRANSFER_SIZE           hif_data_transfer_t

    HIF_LAST_CMD = 0x7FFF

} HIF_COMMANDS;

/*-----------------------------------------------------------------------------------------*/
// HIF_CAMERA_FEATURES
/*-----------------------------------------------------------------------------------------*/
/**
 * HOST query – to read the target Camera features/capabilities.
 *
 * @param sensor_width  native sensor width [pixels]
 * @param sensor_height native sensor height [pixels]
 * @param mode          color correction mode
 *
 * @see HIF_COLOR_CORRECTION_MODES
 */
typedef struct
{
    int16 sensor_width;
    int16 sensor_height;
    int32 mode;
} hif_camera_features_t;

#define HIF_CAMERA_FEATURES_SIZE   sizeof(hif_camera_features_t)

/*-----------------------------------------------------------------------------------------*/
// HIF_CAMERA_SUPPORTED_HARDWARE_LEVEL
/*-----------------------------------------------------------------------------------------*/
/**
 * Supported HW levels: The camera 3 HAL device can implement one of two possible
 * operational modes; limited and full. Full support is expected from new higher-end devices.
 * Limited mode has hardware requirements roughly in line with those for a camera HAL device
 * v1 implementation, and is expected from older or inexpensive devices.
 * Full is a strict superset of limited, and they share the same essential operational flow.
 * For full details refer to "S3. Operational Modes" in camera3.h  
 *
 * @param HIF_HW_LEVEL_LIMITED
 * @param HIF_HW_LEVEL_FULL
 *
 * @see hif_camera_hw_level_t
 */

typedef enum
{
    HIF_HW_LEVEL_LIMITED,
    HIF_HW_LEVEL_FULL
} HIF_SUPPORTED_HW_LEVEL_STATES;

/**
 * HOST query – to read the target Camera supported hardware level.
 * The camera 3 HAL device can implement one of two possible operational
 * modes; limited and full. Full support is expected from new higher-end devices.
 * Limited mode has hardware requirements roughly in line with those for a camera
 * HAL device v1 implementation, and is expected from older or inexpensive devices.
 * Full is a strict superset of limited, and they share the same essential
 * operational flow.
 *
 * @param hw_supported_level supported HW Level
 *
 * @see HIF_SUPPORTED_HW_LEVEL_STATES
 */
typedef struct
{

    int32 hw_supported_level;
} hif_camera_hw_level_t;

#define HIF_CAMERA_SUPPORTED_HW_LEVEL_SIZE   sizeof(hif_camera_hw_level_t)

/*-----------------------------------------------------------------------------------------*/
// HIF_CAMERA_AE_STATE
/*-----------------------------------------------------------------------------------------*/
/**
 * Auto Exposure states
 *
 * @param HIF_AE_INACTIVE  AE is off. When a camera device is opened, it
 *                         starts in this state
 *
 * @param HIF_AE_SEARCHING AE doesn't yet have a good set of control values
 *                         for the current scene
 *
 * @param HIF_AE_CONVERGED AE has a good set of control values for the current
 *                         scene
 *
 * @param HIF_AE_LOCKED    AE has been locked (aeMode = LOCKED)
 *
 * @param HIF_AE_FLASH_REQUIRED AE has a good set of control values, but flash
 *                              needs to be fired for good quality still capture
 *
 * @param HIF_AE_PRECAPTURE AE has been asked to do a pre-capture sequence
 *                          (through the trigger_action
 *                          (CAMERA2_TRIGGER_PRECAPTURE_METERING) call), and
 *                          is currently executing it. Once PRECAPTURE
 *                          completes, AE will transition to CONVERGED or
 *                          FLASH_REQUIRED as appropriate
 *
 *@see hif_camera_ae_state_t
 */

typedef enum
{
    HIF_AE_INACTIVE,        // 0
    HIF_AE_SEARCHING,       // 1
    HIF_AE_CONVERGED,       // 2
    HIF_AE_LOCKED,          // 3
    HIF_AE_FLASH_REQUIRED,  // 4
    HIF_AE_PRECAPTURE       // 5
} HIF_AE_STATES;

/**
 * Host query - to read Auto Exposure state
 *
 * @param ae_state current AE state
 *
 * @see HIF_AE_STATES
 */
typedef struct
{
    int32 ae_state;
} hif_camera_ae_state_t;

#define HIF_CAMERA_AE_STATE_SIZE   sizeof(hif_camera_ae_state_t)

/*-----------------------------------------------------------------------------------------*/
// HIF_CAMERA_AWB_STATE
/*-----------------------------------------------------------------------------------------*/
/**
 * Auto White Balance states
 *
 * @param HIF_AWB_INACTIVE  AWB is not in auto mode. When a camera device is
 *                      opened, it starts in this state
 *
 * @param HIF_AE_SEARCHING  AWB doesn't yet have a good set of control values
 *                      for the current scene
 *
 * @param HIF_AE_CONVERGED  AWB has a good set of control values for the
 *                      current scene
 *
 * @param HIF_AWB_LOCKED    AWB has been locked (aeMode = LOCKED)
 *
 * @see hif_camera_awb_state_t
 */

typedef enum
{
    HIF_AWB_INACTIVE,       // 0
    HIF_AWB_SEARCHING,       // 1
    HIF_AWB_CONVERGED,       // 2
    HIF_AWB_LOCKED          // 3
} HIF_AWB_STATES;

/**
 * Host query - to read Auto White Balance state
 *
 * @param awb_state current WB state
 *
 * @see HIF_AWB_STATES
 */
typedef struct
{
    int32 awb_state;
} hif_camera_awb_state_t;

#define HIF_CAMERA_AWB_STATE_SIZE   sizeof(hif_camera_awb_state_t)

/*-----------------------------------------------------------------------------------------*/
// HIF_CAMERA_AF_STATE
/*-----------------------------------------------------------------------------------------*/
/**
 * Auto Focus states
 *
 * @param HIF_AF_INACTIVE       AF off or has not yet tried to scan/been asked to
 *                              scan. When a camera device is opened, it starts
 *                              in this state.
 *
 * @param HIF_AF_PASSIVE_SCAN   if CONTINUOUS_* modes are supported. AF is
 *                              currently doing an AF scan initiated by a
 *                              continuous autofocus mode
 *
 * @param HIF_AF_PASSIVE_FOCUSED if CONTINUOUS_* modes are supported. AF currently
 *                              believes it is in focus, but may restart
 *                              scanning at any time.
 *
 * @param HIF_AF_ACTIVE_SCAN    if AUTO or MACRO modes are supported. AF is doing
 *                              an AF scan because it was triggered by AF
 *                              trigger
 *
 * @param HIF_AF_FOCUSED_LOCKED if any AF mode besides OFF is supported. AF
 *                              believes it is focused correctly and is locked
 *
 * @param HIF_AF_NOT_FOCUSED_LOCKED if any AF mode besides OFF is supported. AF has
 *                              failed to focus successfully and is locked
 *
 * @param HIF_AF_PASSIVE_UNFOCUSED if CONTINUOUS_* modes are supported. AF finished
 *                              a passive scan without finding focus, and may
 *                              restart scanning at any time.
 *
 *@see hif_camera_af_state_t
 */

typedef enum
{
    HIF_AF_INACTIVE,          // 0
    HIF_AF_PASSIVE_SCAN,      // 1
    HIF_AF_PASSIVE_FOCUSED,   // 2
    HIF_AF_ACTIVE_SCAN,       // 3
    HIF_AF_FOCUSED_LOCKED,    // 4
    HIF_AF_NOT_FOCUSED_LOCKED,    // 5
    HIF_AF_PASSIVE_UNFOCUSED  // 6
} HIF_AF_STATES;

/**
 * Host query - to read Auto Focus state
 *
 * @param af_state current AF state
 *
 * @see HIF_AF_STATES
 */
typedef struct
{
    int32 af_state;
} hif_camera_af_state_t;

#define HIF_CAMERA_AF_STATE_SIZE   sizeof(hif_camera_af_state_t)

/*-----------------------------------------------------------------------------------------*/
// HIF_CAMERA_FLASH_STATE
/*-----------------------------------------------------------------------------------------*/
/**
 * Flash states
 *
 * @param HIF_FLASH_UNAVAILABLE, // No Flash on Camera
 * @param HIF_FLASH_CHARGING,    // Flash is charging and cannot be fired
 * @param HIF_FLASH_READY,       // Flash is ready to fire
 * @param HIF_FLASH_FIRED        // Flash Fired for the current capture
 *
 * @see hif_camera_flash_state_t
 */
typedef enum
{
    HIF_FLASH_UNAVAILABLE, // No Flash on Camera
    HIF_FLASH_CHARGING,    // Flash is charging and cannot be fired
    HIF_FLASH_READY,       // Flash is ready to fire
    HIF_FLASH_FIRED        // Flash Fired for the current capture
} HIF_FLASH_STATES;

/**
 * Host query - to read Flash state
 *
 * @param af_state current AF state
 *
 * @see HIF_FLASH_STATES
 */
typedef struct
{
    int32 flash_state;
} hif_camera_flash_state_t;

#define HIF_CAMERA_FLASH_STATE_SIZE   sizeof(hif_camera_flash_state_t)

/*-----------------------------------------------------------------------------------------*/
// HIF_CAMERA_COL_COR_MODE
/*-----------------------------------------------------------------------------------------*/
/**
 * A color correction modes
 *
 * @param HIF_COLOR_CORRECTION_TRANSFORM_MATRIX Use the android.colorCorrection.transform
 * matrix and android.colorCorrection.gains to do color conversion
 *
 * @param HIF_COLOR_CORRECTION_FAST             Must not slow down frame rate relative to
 * raw bayer output
 *
 * @param HIF_COLOR_CORRECTION_HIGH_QUALITY     Frame rate may be reduced by high quality
 */
typedef enum
{
    HIF_COLOR_CORRECTION_TRANSFORM_MATRIX,
    HIF_COLOR_CORRECTION_FAST,
    HIF_COLOR_CORRECTION_HIGH_QUALITY
} HIF_COLOR_CORRECTION_MODES;

/**
 * Controls color correction mode.
 *
 * @param mode mode selection
 *
 * @see HIF_COLOR_CORRECTION_MODES
 */
typedef struct
{
    int32 mode;
} hif_color_correction_mode_t;

#define HIF_COLOR_CORRECTION_MODE_SIZE   sizeof(hif_color_correction_mode_t)

/*-----------------------------------------------------------------------------------------*/
// HIF_CAMERA_COL_COR_TRANSFORM
/*-----------------------------------------------------------------------------------------*/
/**
 * A color transform matrix to use to transform
 * from sensor RGB color space to output linear sRGB color space
 *
 * @param values transform coefficients values. Range [0-1]
 */
typedef struct
{
    float values[3][3];  // TODO fix value
} hif_color_correction_transform_t;

#define HIF_COLOR_CORRECTION_TRANSFORM_SIZE   sizeof(hif_color_correction_transform_t)

/*-----------------------------------------------------------------------------------------*/
// HIF_CAMERA_COL_COR_GAIN
/*-----------------------------------------------------------------------------------------*/
/**
 * Gains applying to Bayer color channels for white-balance
 *
 * @param values Gain values
 */
typedef struct
{
    float values[4];        // TODO fix value
} hif_color_correction_gains_t;

#define HIF_COLOR_CORRECTION_GAINS_SIZE   sizeof(hif_color_correction_gains_t)

/*-----------------------------------------------------------------------------------------*/
// HIF_CAMERA_EV
/*-----------------------------------------------------------------------------------------*/
/**
 * Exposure compensation
 *
 * @param compensation count of positive/negative EV steps
 *                     For example, if EV step is 0.333,
 *                     '6' will mean an exposure compensation of +2 EV;
 *                     -3 will mean an exposure compensation of -1
 */
typedef struct
{
    int32 compensation;
} hif_ae_exposure_compensation_t;

#define HIF_AE_EXPOSURE_COMPENSATION_SIZE   sizeof(hif_ae_exposure_compensation_t)

/*-----------------------------------------------------------------------------------------*/
// HIF_CAMERA_EXP_TIME
/*-----------------------------------------------------------------------------------------*/
/**
 * Duration each pixel is exposed to light.
 *
 * @param exposure_time exposure time in nanoseconds
 */
typedef struct
{
    int64 exposure_time;   // nanoseconds   // TODO int64 ?? GOOGLE defining int64...
} hif_camera_exposure_time_t;

#define HIF_CAMERA_EXPOSURE_TIME_SIZE   sizeof(hif_camera_exposure_time_t)

/*-----------------------------------------------------------------------------------------*/
// HIF_CAMERA_FRAME_DURATION
/*-----------------------------------------------------------------------------------------*/
/**
 * Duration from start of frame exposure to start of next frame exposure
 * NOTE: Exposure time has priority, so duration is set to
 *       max(duration, exposure time + overhead)
 *
 * @param frame_duration  frame duration in nanoseconds
 *
 * @see hif_camera_exposure_time_t
 */
typedef struct
{
    int64 frame_duration;  // nanoseconds  // TODO int64 ??  GOOGLE defining int64...
} hif_camera_frame_duration_t;

#define HIF_CAMERA_FRAME_DURATION_SIZE   sizeof(hif_camera_frame_duration_t)

/*-----------------------------------------------------------------------------------------*/
// HIF_CAMERA_OUTPUT_FRAME_DIMENSIONS
/*-----------------------------------------------------------------------------------------*/
/**
 * Camera frame dimensions
 *
 * @param width  frame width  [pixels]
 * @param height frame height [pixels]
 */
typedef struct
{
    int32 width;        // in pixels
    int32 height;       // in pixels
} hif_scaler_frame_dimensions_t;

#define HIF_SCALER_FRAME_DIMENSIONS_SIZE   sizeof(hif_scaler_frame_dimensions_t)

/*-----------------------------------------------------------------------------------------*/
// HIF_CAMERA_CROP_REGION
/*-----------------------------------------------------------------------------------------*/
/**
 * A rectangle with the top-level corner of (x,y) and size (width, height).
 * The region of the sensor that is used for output. Each stream must use
 * this rectangle to produce its output, cropping to a smaller region
 * if necessary to maintain the stream's aspect ratio
 *
 * @param x      start X coordinate of cropped image [pixels]
 * @param y      start Y coordinate of cropped image [pixels]
 * @param width  cropped image width  [pixels]
 * @param height cropped image height [pixels]
 */
typedef struct
{
    int32 x;
    int32 y;
    int32 width;
    int32 height;
} hif_scaler_crop_region_t;

#define HIF_SCALER_CROP_REGION_SIZE   sizeof(hif_scaler_crop_region_t)

/*-----------------------------------------------------------------------------------------*/
// HIF_CAMERA_TONEMAP
/*-----------------------------------------------------------------------------------------*/
/**
 * Tone mapping types
 *
 * @param HIF_TONEMAP_MODE_CONTRAST_CURVE
 * @param HIF_TONEMAP_MODE_FAST
 * @param HIF_TONEMAP_MODE_HIGH_QUALITY
 */
typedef enum
{
    HIF_TONEMAP_MODE_CONTRAST_CURVE,
    HIF_TONEMAP_MODE_FAST,
    HIF_TONEMAP_MODE_HIGH_QUALITY
} HIF_TONEMAP_MODES;

/**
 * Tone mapping mode
 *
 * @param mode mode to be used for tone mapping
 *
 * @see HIF_TONEMAP_MODES
 */
typedef struct
{

    int32 mode;
} hif_tonemap_mode_t;

#define HIF_TONEMAP_MODE_SIZE  sizeof(hif_tonemap_mode_t)

/*---------------------------------- UTILITY --------------------------------------*/

/**
 * Maximum tone mapping points
 *
 * @see tonemap_pair_t
 */
#define HIF_MAX_CURVE_POINTS    128  // TODO fix number

/**
 * Tone mapping point: input to output
 * Since the input and output ranges may vary depending on the camera pipeline,
 * the input and output pixel values are represented by normalized floating-point
 * values between 0 and 1, with 0 == black and 1 == white.
 *
 * @param p_in  input point  [0-1]
 * @param p_out output point [0-1]
 *
 * @see HIF_TONEMAP_MODES
 * @see hif_tonemap_mode_t
 */
typedef struct
{
    float p_in;
    float p_out;
} hif_tonemap_pair_t;

/*-----------------------------------------------------------------------------------------*/
// HIF_CAMERA_GAMMA_BLUE
/*-----------------------------------------------------------------------------------------*/
/**
 *  Table mapping blue (input values to output values)
 *
 *  @param map[HIF_MAX_CURVE_POINTS] map table
 *
 * @see HIF_TONEMAP_MODES
 * @see hif_tonemap_mode_t
 * @see hif_tonemap_pair_t
 */
typedef struct
{
    hif_tonemap_pair_t map[HIF_MAX_CURVE_POINTS];
} hif_tonemap_curve_blue_t;

#define HIF_TONEMAP_CURVE_BLUE_SIZE     sizeof(hif_tonemap_curve_blue_t)

/*-----------------------------------------------------------------------------------------*/
// HIF_CAMERA_GAMMA_GREEN
/*-----------------------------------------------------------------------------------------*/
/**
 *  Table mapping green (input values to output values)
 *
 *  @param map[HIF_MAX_CURVE_POINTS] map table
 *
 * @see HIF_TONEMAP_MODES
 * @see hif_tonemap_mode_t
 * @see hif_tonemap_pair_t
 */
typedef struct
{
    hif_tonemap_pair_t map[HIF_MAX_CURVE_POINTS];
} hif_tonemap_curve_green_t;

#define HIF_TONEMAP_CURVE_GREEN_SIZE    sizeof(hif_tonemap_curve_green_t)

/*-----------------------------------------------------------------------------------------*/
// HIF_CAMERA_GAMMA_RED
/*-----------------------------------------------------------------------------------------*/
/**
 *  Table mapping red (input values to output values)
 *
 *  @param map[HIF_MAX_CURVE_POINTS] map table
 *
 * @see HIF_TONEMAP_MODES
 * @see hif_tonemap_mode_t
 * @see hif_tonemap_pair_t
 */
typedef struct
{
    hif_tonemap_pair_t map[HIF_MAX_CURVE_POINTS];
} hif_tonemap_curve_red_t;

#define HIF_TONEMAP_CURVE_RED_SIZE      sizeof(hif_tonemap_curve_red_t)

/*-----------------------------------------------------------------------------------------*/
// HIF_CAMERA_LSC
/*-----------------------------------------------------------------------------------------*/

/**
 * Quality of lens shading correction applied to the image data
 *
 * @param HIF_SHADING_MODE_OFF
 * @param HIF_SHADING_MODE_FAST
 * @param HIF_SHADING_MODE_HIGH_QUALITY
 */
typedef enum
{
    HIF_SHADING_MODE_OFF,
    HIF_SHADING_MODE_FAST,
    HIF_SHADING_MODE_HIGH_QUALITY
} HIF_SHADING_MODES;

/**
 * lens shading correction mode (Quality)
 *
 * @param mode LSC mode selection
 *
 * @see HIF_SHADING_MODES
 */
typedef struct
{
    int32 mode;
} hif_shading_mode_t;

#define HIF_SHADING_MODE_SIZE   sizeof(hif_shading_mode_t)

/*-----------------------------------------------------------------------------------------*/
// HIF_CAMERA_LSC_MAP
/*-----------------------------------------------------------------------------------------*/
#define HIF_LENS_SHADING_MAP_SIZE   128  // TODO 128 is just an example

/**
 * A low-resolution map of lens shading, per color channel
 *
 * @param factor1   LSC factor for color channel 1
 * @param factor2   LSC factor for color channel 2
 * @param factor3   LSC factor for color channel 3
 * @param factor4   LSC factor for color channel 4
 */
typedef struct
{
    float factor1;
    float factor2;
    float factor3;
    float factor4;
} hif_lsc_gain_t;

/**
 * lens shading map
 *
 * @param width  map table width
 * @param height map table height
 * @param map    map table
 *
 *@see hif_lsc_gain_t
 */
typedef struct
{
    int32 width;
    int32 height;
    hif_lsc_gain_t map[HIF_LENS_SHADING_MAP_SIZE];
} hif_lens_shading_map_t;

#define HIF_STATISTICS_LENS_SHADING_MAP_SIZE(_buffer) \
                         sizeof(hif_statistics_lens_shading_map_t)

/*-----------------------------------------------------------------------------------------*/
// HIF_CAMERA_LSC_STRENGTH
/*-----------------------------------------------------------------------------------------*/
/**
 * Control the amount of shading correction applied to the images
 *
 * @param strength LSC strength - unitless: [1-10]. 10 is full shading compensation
 */
typedef struct
{
    int32 strength;
} hif_shading_strength_t;

#define HIF_SHADING_STRENGTH_SIZE   sizeof(hif_shading_strength_t)

/*-----------------------------------------------------------------------------------------*/
// HIF_CAMERA_AE_MODE
/*-----------------------------------------------------------------------------------------*/
/**
 * Auto Exposure modes
 *
 * @param HIF_AE_MODE_OFF                  Auto exposure is OFF
 * @param HIF_AE_MODE_ON                   Auto exposure is ON
 * @param HIF_AE_MODE_ON_AUTO_FLASH        AE is ON and flash is used if required
 * @param HIF_AE_MODE_ON_ALWAYS_FLASH      AE is ON flash is forced to ON
 * @param HIF_AE_MODE_ON_AUTO_FLASH_REDEYE AE is ON and flash is used if required
 *                                     and red eye reduction is ON
 */
//
typedef enum
{
    HIF_AE_MODE_OFF,
    HIF_AE_MODE_ON,
    HIF_AE_MODE_ON_AUTO_FLASH,
    HIF_AE_MODE_ON_ALWAYS_FLASH,
    HIF_AE_MODE_ON_AUTO_FLASH_REDEYE
} HIF_AE_MODES;

/**
 * Auto Exposure mode
 * Whether AE is currently updating the sensor exposure and sensitivity fields
 *
 * @param mode Auto exposure mode selection
 *
 * @see HIF_AE_MODES
 */

typedef struct
{
    int32 mode;
} hif_ae_mode_t;

#define HIF_AE_MODE_SIZE   sizeof(hif_ae_mode_t)

/*-----------------------------------------------------------------------------------------*/
// HIF_CAMERA_AE_LOCK
/*-----------------------------------------------------------------------------------------*/
/**
 * Auto Exposure lock types
 *
 * @param HIF_AE_LOCK_OFF AE is locked to its latest calculated values
 * @param HIF_AE_LOCK_ON  AE is running
 *
 * @see hif_ae_lock_type_t
 */
typedef enum
{
    HIF_AE_LOCK_OFF,
    HIF_AE_LOCK_ON
} HIF_AE_LOCK_TYPES;

/**
 * Auto Exposure lock
 * Whether AE is currently locked to its latest calculated values
 *
 * @param type AE lock type selection
 *
 * @see HIF_AE_LOCK_TYPES
 */
typedef struct
{
    int32 type;
} hif_ae_lock_type_t;

#define HIF_AE_LOCK_TYPES_SIZE   sizeof(hif_ae_lock_type_t)

/*-----------------------------------------------------------------------------------------*/
// HIF_CAMERA_AE_REGIONS
/*-----------------------------------------------------------------------------------------*/
/**
 * AE region description: Each area is a rectangle plus weight: xmin, ymin, xmax, ymax, weight.
 * The rectangle is defined inclusive of the specified coordinates.
 * The coordinate system is based on the active pixel array, with (0,0) being the top-left pixel
 * in the active pixel array, and (sensor_width - 1), (sensor_height - 1) being the bottom-right
 * pixel in the active pixel array. The weight should be nonnegative.
 * If all regions have 0 weight, then no specific metering area needs to be used by the HAL.
 * If the metering region is outside the current crop region, the HAL should ignore the sections
 * outside the region and output the used sections in the frame metadata 
 *
 * @param xmin [Pixels]
 * @param ymin [Pixels]
 * @param xmax [Pixels]
 * @param ymax [Pixels]
 * @param weight
 */
typedef struct
{
    int32 xmin;
    int32 ymin;
    int32 xmax;
    int32 ymax;
    int32 weight;
} hif_ae_region_t;

/**
 * Max number of regions for metering
 */
#define HIF_AE_REGION_COUNT 16       // TODO fix me Just an example

/**
 * List of areas to use for metering
 *
 * @param regions[HIF_AE_REGION_COUNT] regions list
 *
 * @see ae_region_t
 * @see HIF_AE_REGION_COUNT
 */
typedef struct
{

    hif_ae_region_t regions[HIF_AE_REGION_COUNT];
} hif_ae_regions_t;

#define HIF_AE_REGIONS_SIZE   sizeof(hif_ae_regions_t)

/*-----------------------------------------------------------------------------------------*/
// HIF_CAMERA_AE_FPS_RANGE
/*-----------------------------------------------------------------------------------------*/
/**
 * Frame Rate range
 * Range over which fps can be adjusted to maintain exposure
 *
 * @param fps_min minimal frame rate // TODO Range/Units
 * @param fps_max maximal frame rate // TODO Range/Units
 */
typedef struct
{
    int32 fps_min;
    int32 fps_max;
} hif_ae_target_fps_range_t;

#define HIF_AE_TARGET_FPS_RANGE_SIZE   sizeof(hif_ae_target_fps_range_t)

/*-----------------------------------------------------------------------------------------*/
// HIF_CAMERA_AE_PRECAP_TRIGGER
/*-----------------------------------------------------------------------------------------*/
/**
 * AE pre-capture types: This entry is normally set to IDLE, or is not included at all in
 * the request settings. When included and set to START, the HAL must trigger the autoexposure
 * precapture metering sequence.
 * The effect of AE precapture trigger depends on the current AE mode and state;
 * see the camera HAL device v3 header for details.
 *
 * @param HIF_AE_PRECAPTURE_TRIGGERLOCK_IDLE   normal AE operation
 * @param HIF_AE_PRECAPTURE_TRIGGERLOCK_START  AE should execute pre-capture procedure
 *
 * @see hif_ae_precapture_triggerlock_type_t
 */
typedef enum
{
    HIF_AE_PRECAPTURE_TRIGGERLOCK_IDLE,
    HIF_AE_PRECAPTURE_TRIGGERLOCK_START
} HIF_AE_PRECAPTURE_TRIGGERLOCK_TYPES;

/**
 * Whether the HAL must trigger pre-capture metering.
 * When using START option the pre-capture metering sequence must be started.
 * The exact effect of the pre-capture trigger depends on the current AE mode
 * and state
 *
 * @param type AE operation selection
 *
 * @see HIF_AE_PRECAPTURE_TRIGGERLOCK_TYPES
 */
typedef struct
{
    int32 type;
} hif_ae_precapture_triggerlock_type_t;

// TODO need sequence definition

#define HIF_AE_PRECAPTURE_TRIGGERLOCK_TYPE_SIZE \
                        sizeof(hif_ae_precapture_triggerlock_type_t)

/*-----------------------------------------------------------------------------------------*/
// HIF_CAMERA_AWB_MODE
/*-----------------------------------------------------------------------------------------*/
/**
 * Auto White Balance types
 * Whether AWB is currently setting the color transform fields, and what
 * its illumination target is
 *
 * @param HIF_AWB_OFF,             AWB is OFF
 * @param HIF_AWB_AUTO,            AWB is in auto mode
 * @param HIF_AWB_INCANDESCENT,    AWB is in predefined 'incandescent' mode
 * @param HIF_AWB_FLUORESCENT,     AWB is in predefined 'fluorescent' mode
 * @param HIF_AWB_WARM_FLUORESCENT AWB is in predefined 'warm fluorescent' mode
 * @param HIF_AWB_DAYLIGHT,        AWB is in predefined 'daylight' mode
 * @param HIF_AWB_CLOUDY_DAYLIGHT, AWB is in predefined 'cloudy daylight' mode
 * @param HIF_AWB_TWILIGHT,        AWB is in predefined 'twilight' mode
 * @param HIF_AWB_SHADE            AWB is in predefined 'shade' mode
 */
typedef enum
{
    HIF_AWB_OFF,
    HIF_AWB_AUTO,
    HIF_AWB_INCANDESCENT,
    HIF_AWB_FLUORESCENT,
    HIF_AWB_WARM_FLUORESCENT,
    HIF_AWB_DAYLIGHT,
    HIF_AWB_CLOUDY_DAYLIGHT,
    HIF_AWB_TWILIGHT,
    HIF_AWB_SHADE
} HIF_AWB_TYPES;

/**
 * Auto White Balance type
 *
 * @param type AWB operating mode
 *
 * @see HIF_AWB_TYPES
 */
typedef struct
{
    int32 type;
} hif_awb_type_t;

#define HIF_AWB_TYPE_SIZE   sizeof(hif_awb_type_t)

/*-----------------------------------------------------------------------------------------*/
// HIF_CAMERA_AWB_LOCK
/*-----------------------------------------------------------------------------------------*/
/**
 * AWB lock types
 *
 * @param HIF_AWB_LOCK_OFF lock AWB
 * @param AWB_LOCK_ON  AWB is running
 */

typedef enum
{
    HIF_AWB_LOCK_OFF,
    HIF_AWB_LOCK_ON
} HIF_AWB_LOCK_TYPES;

//
/**
 * AWB lock
 * Whether AWB is currently locked to its latest calculated values
 *
 * @param type AWB lock mode
 *
 * @see HIF_AWB_LOCK_TYPES
 */

typedef struct
{
    int32 type;
} hif_awb_lock_type_t;

#define HIF_AWB_LOCK_TYPE_SIZE   sizeof(hif_awb_lock_type_t)

/*-----------------------------------------------------------------------------------------*/
// HIF_CAMERA_AWB_REGIONS
/*-----------------------------------------------------------------------------------------*/
/**
 * AWB region description : Only used in AUTO mode.
 * Each area is a rectangle plus weight: xmin, ymin, xmax, ymax, weight. The rectangle is
 * defined inclusive of the specified coordinates.
 * The coordinate system is based on the active pixel array, with (0,0) being the top-left pixel
 * in the active pixel array, and (Sensor_width - 1), (sensor_height - 1) being the bottom-right
 * pixel in the active pixel array. The weight should be nonnegative.
 * If all regions have 0 weight, then no specific metering area needs to be used by the HAL.
 * If the metering region is outside the current crop_region, the HAL should ignore the sections
 * outside the region and output the used sections in the frame metadata 
 *
 * @param xmin  [Pixels]
 * @param ymin  [Pixels]
 * @param xmax  [Pixels]
 * @param ymax  [Pixels]
 * @param weight
 */

typedef struct
{
    int32 xmin;
    int32 ymin;
    int32 xmax;
    int32 ymax;
    int32 weight;
} hif_awb_region_t;

/**
 * Max number of regions for metering
 */
#define HIF_AWB_REGION_COUNT 16       // TODO fix me Just an example

/**
 * List of areas to use for metering
 *
 * @param regions[HIF_AWB_REGION_COUNT] regions list
 *
 * @see awb_region_t
 * @see HIF_AWB_REGION_COUNT
 */
typedef struct
{
    hif_awb_region_t regions[HIF_AWB_REGION_COUNT];
} hif_awb_regions_t;

#define HIF_AWB_REGIONS_SIZE   sizeof(hif_awb_regions_t)

/*-----------------------------------------------------------------------------------------*/
// HIF_CAMERA_AF_MODE
/*-----------------------------------------------------------------------------------------*/
/**
 * Auto Focus modes
 * Whether AF is currently enabled, and what mode it is set to
 *
 * @param HIF_AF_MODE_OFF                AF is OFF
 * @param HIF_AF_MODE_AUTO               AF is in auto mode
 * @param HIF_AF_MODE_MACRO              AF is in macro mode
 * @param HIF_AF_MODE_CONTINUOUS_VIDEO   AF is in 'continuous video' mode
 * @param HIF_AF_MODE_CONTINUOUS_PICTURE AF is in 'continuous picture' mode
 * @param HIF_AF_MODE_EDOF               AF is in 'EDOF' mode
 */
typedef enum
{
    HIF_AF_MODE_OFF,
    HIF_AF_MODE_AUTO,
    HIF_AF_MODE_MACRO,
    HIF_AF_MODE_CONTINUOUS_VIDEO,
    HIF_AF_MODE_CONTINUOUS_PICTURE,
    HIF_AF_MODE_EDOF
} HIF_AF_MODES;

/**
 * AF mode
 *
 * @param mode Auto Focus mode
 *
 * @see HIF_AF_MODES
 */

typedef struct
{
    int32 mode;
} hif_af_mode_t;

#define HIF_AF_MODE_SIZE   sizeof(hif_af_mode_t)

/*-----------------------------------------------------------------------------------------*/
// HIF_CAMERA_AF_REGIONS
/*-----------------------------------------------------------------------------------------*/
/**
 * AF region description: Each area is a rectangle plus weight: xmin, ymin, xmax, ymax, weight.
 * The rectangle is defined inclusive of the specified coordinates.
 * The coordinate system is based on the active pixel array, with (0,0) being the top-left pixel
 * in the active pixel array, and (sensor_width - 1), (sensor_height - 1) being the bottom-right
 * pixel in the active pixel array. The weight should be nonnegative.
 * If all regions have 0 weight, then no specific focus area needs to be used by the HAL.
 * If the focusing region is outside the current android.scaler.cropRegion, the HAL should ignore
 * the sections outside the region and output the used sections in the frame metadata 
 *
 * @param xmin  [Pixels]
 * @param ymin  [Pixels]
 * @param xmax  [Pixels]
 * @param ymax  [Pixels]
 * @param weight
 */
typedef struct
{
    int32 xmin;
    int32 ymin;
    int32 xmax;
    int32 ymax;
    int32 weight;
} hif_af_region_t;

/**
 * Max number of regions for focus estimation
 */
#define HIF_AF_REGION_COUNT 16       // TODO fix me Just an example

/**
 * List of areas to use for focus estimation
 *
 * @param regions[AF_REGION_COUNT] regions list
 *
 * @see af_region_t
 * @see AF_REGION_COUNT
 */
typedef struct
{
    hif_af_region_t regions[HIF_AF_REGION_COUNT];
} hif_af_regions_t;

#define HIF_AF_REGIONS_SIZE   sizeof(hif_af_regions_t)

/*-----------------------------------------------------------------------------------------*/
// HIF_CAMERA_AF_TRIGGER
/*-----------------------------------------------------------------------------------------*/
/**
 * Auto Focus triggers
 * Whether the HAL must trigger auto focus.
 *
 * @param HIF_AF_TRIGGER_IDLE   AF is not running
 * @param HIF_AF_TRIGGER_START  start AF
 * @param HIF_AF_TRIGGER_CANCEL cancel currently running AF
 */
typedef enum
{
    HIF_AF_TRIGGER_IDLE,
    HIF_AF_TRIGGER_START,
    HIF_AF_TRIGGER_CANCEL
} HIF_AF_TRIGGERS;

/**
 * Auto Focus trigger
 *
 * @param trigger   AF trigger selection
 *
 * @see AF_TRIGGERS
 */
typedef struct
{
    int32 trigger;
} hif_af_trigger_t;

#define HIF_AF_TRIGGER_SIZE   sizeof(hif_af_trigger_t)

/*-----------------------------------------------------------------------------------------*/
// HIF_CAMERA_AVAILABLE_EFFECTS
/*-----------------------------------------------------------------------------------------*/
/**
 * Supported Effect types
 * Whether Effect is currently enabled, and what mode it is set to
 *
 * @param HIF_EFFECTS_UNSUPPORTED effects are not supported
 * @param HIF_EFFECT_OFF          no effect is applied
 * @param HIF_EFFECT_MONO         set effect 'mono'
 * @param HIF_EFFECT_NEGATIVE     set effect 'negative'
 * @param HIF_EFFECT_SOLARIZE     set effect 'solarize'
 * @param HIF_EFFECT_SEPIA        set effect 'sepia'
 * @param HIF_EFFECT_POSTERIZE    set effect 'posterize'
 * @param HIF_EFFECT_WHITEBOARD   set effect 'white board'
 * @param HIF_EFFECT_BLACKBOARD   set effect 'black board'
 * @param HIF_EFFECT_AQUA         set effect 'aqua'
 */
typedef enum
{
    HIF_EFFECTS_UNSUPPORTED = 0,
    HIF_EFFECT_OFF = (0x00000001 << 0),
    HIF_EFFECT_MONO = (0x00000001 << 1),
    HIF_EFFECT_NEGATIVE = (0x00000001 << 2),
    HIF_EFFECT_SOLARIZE = (0x00000001 << 3),
    HIF_EFFECT_SEPIA = (0x00000001 << 4),
    HIF_EFFECT_POSTERIZE = (0x00000001 << 5),
    HIF_EFFECT_WHITEBOARD = (0x00000001 << 6),
    HIF_EFFECT_BLACKBOARD = (0x00000001 << 7),
    HIF_EFFECT_AQUA = (0x00000001 << 8)
} HIF_AVAILABLE_EFFECTS;

/**
 * Host query for supported effects
 *
 * @param available_effects bit mask from supported effects
 *
 * @see HIF_AVAILABLE_EFFECTS
 */
typedef struct
{
    int32 available_effects;
} hif_camera_available_effects_t;

#define HIF_AVAILABLE_EFFECTS_TYPE_SIZE   sizeof(hif_camera_available_effects_t)

/*-----------------------------------------------------------------------------------------*/
// HIF_CAMERA_EFFECT
/*-----------------------------------------------------------------------------------------*/
/**
 * Control Camera Effect
 * Whether any special color effect is in use.
 *
 * @param type effect type selection
 *
 * @see HIF_AVAILABLE_EFFECTS
 */
typedef struct
{
    int32 type;
} hif_effect_type_t;

#define HIF_EFFECT_TYPE_SIZE   sizeof(hif_effect_type_t)

/*-----------------------------------------------------------------------------------------*/
// HIF_CAMERA_OUTPUT_PIX_FORMAT
/*-----------------------------------------------------------------------------------------*/
/** List of app-visible formats
 *
 * @param HIF_SCALER_FORMAT_RAW_SENSOR              raw sensor (Bayer)
 * @param HIF_SCALER_FORMAT_YV12                    YCrCb 4:2:0 Planar
 * @param HIF_SCALER_FORMAT_YCrCb_420_SP            NV12
 * @param HIF_SCALER_FORMAT_IMPLEMENTATION_DEFINED  Hal Implementation Defined
 * @param HIF_SCALER_FORMAT_YCbCr_420_888           Flexible YUV420 Format
 * @param HIF_SCALER_FORMAT_BLOB                    JPEG
 */
typedef enum
{
    HIF_SCALER_FORMAT_RAW_SENSOR = 0x20,
    HIF_SCALER_FORMAT_YV12 = 0x32315659,
    HIF_SCALER_FORMAT_YCrCb_420_SP = 0x11,
    HIF_SCALER_FORMAT_IMPLEMENTATION_DEFINED = 0x22,
    HIF_SCALER_FORMAT_YCbCr_420_888 = 0x23,
    HIF_SCALER_FORMAT_BLOB = 0x21
} HIF_SCALER_AVAILABLE_FORMATS;

/**
 * Output pixel format
 *
 * @param format output format selection
 *
 * @see HIF_SCALER_AVAILABLE_FORMATS
 */
typedef struct
{
    int32 format;
} hif_out_pix_format_t;

#define HIF_OUTPUT_PIX_FORMAT_SIZE   sizeof(hif_out_pix_format_t)

/*-----------------------------------------------------------------------------------------*/
// HIF_CAMERA_AVAILABLE_SCENES
/*-----------------------------------------------------------------------------------------*/
/**
 * Supported (available) scene modes.
 *
 * @param HIF_SCENE_MODE_UNSUPPORTED    scenes are not supported
 * @param HIF_SCENE_MODE_FACE_PRIORITY  scene mode 'face priority'
 * @param HIF_SCENE_MODE_ACTION         scene mode 'action'
 * @param HIF_SCENE_MODE_PORTRAIT       scene mode 'portrait'
 * @param HIF_SCENE_MODE_LANDSCAPE      scene mode 'landscape'
 * @param HIF_SCENE_MODE_NIGHT          scene mode 'night'
 * @param HIF_SCENE_MODE_NIGHT_PORTRAIT scene mode 'night portrait'
 * @param HIF_SCENE_MODE_THEATRE        scene mode 'theater'
 * @param HIF_SCENE_MODE_BEACH          scene mode 'beach'
 * @param HIF_SCENE_MODE_SNOW           scene mode 'snow'
 * @param HIF_SCENE_MODE_SUNSET         scene mode 'sunset'
 * @param HIF_SCENE_MODE_STEADYPHOTO    scene mode 'steady photo'
 * @param HIF_SCENE_MODE_FIREWORKS      scene mode 'fireworks'
 * @param HIF_SCENE_MODE_SPORTS         scene mode 'spot'
 * @param HIF_SCENE_MODE_PARTY          scene mode 'party'
 * @param HIF_SCENE_MODE_CANDLELIGHT    scene mode 'candle light'
 * @param HIF_SCENE_MODE_BARCODE        scene mode 'barcode'
 */

typedef enum
{
    HIF_SCENE_MODE_UNSUPPORTED = 0,
    HIF_SCENE_MODE_FACE_PRIORITY = (0x00000001 << 0),
    HIF_SCENE_MODE_ACTION = (0x00000001 << 1),
    HIF_SCENE_MODE_PORTRAIT = (0x00000001 << 2),
    HIF_SCENE_MODE_LANDSCAPE = (0x00000001 << 3),
    HIF_SCENE_MODE_NIGHT = (0x00000001 << 4),
    HIF_SCENE_MODE_NIGHT_PORTRAIT = (0x00000001 << 5),
    HIF_SCENE_MODE_THEATRE = (0x00000001 << 6),
    HIF_SCENE_MODE_BEACH = (0x00000001 << 7),
    HIF_SCENE_MODE_SNOW = (0x00000001 << 8),
    HIF_SCENE_MODE_SUNSET = (0x00000001 << 9),
    HIF_SCENE_MODE_STEADYPHOTO = (0x00000001 << 10),
    HIF_SCENE_MODE_FIREWORKS = (0x00000001 << 11),
    HIF_SCENE_MODE_SPORTS = (0x00000001 << 12),
    HIF_SCENE_MODE_PARTY = (0x00000001 << 13),
    HIF_SCENE_MODE_CANDLELIGHT = (0x00000001 << 14),
    HIF_SCENE_MODE_BARCODE = (0x00000001 << 15)
} HIF_AVAILABLE_SCENES;

/**
 *Host query – to read the target Camera available scene modes.
 *
 *@param available_scenes supported scenes
 */
typedef struct
{
    int32 available_scenes;
} hif_camera_available_scenes_t;

#define HIF_AVAILABLE_SCENE_MODES_SIZE   sizeof(hif_camera_available_scenes_t)

/*-----------------------------------------------------------------------------------------*/
// HIF_CAMERA_SCENE
/*-----------------------------------------------------------------------------------------*/
/**
 * Control which scene mode is active
 *
 * @param mode scene mode selection
 *
 * @see HIF_AVAILABLE_SCENES
 */
typedef struct
{
    int32 mode;
} hif_scene_mode_t;

#define HIF_SCENE_MODE_SIZE   sizeof(hif_scene_mode_t)

/*-----------------------------------------------------------------------------------------*/
// HIF_CAMERA_FLASH
/*-----------------------------------------------------------------------------------------*/
/**
 * Flash operation types
 *
 * @param HIF_FLASH_MODE_OFF    Do not fire the flash for this capture
 *
 * @param HIF_FLASH_MODE_SINGLE if flash is available, Fire flash for this
 *                          capture based on Flash Power, Flash Time
 *
 * @param HIF_FLASH_MODE_TORCH  if flash is available, Flash continuously on,
 *                          power set by Flash Power
 */
typedef enum
{
    HIF_FLASH_MODE_OFF,
    HIF_FLASH_MODE_SINGLE,
    HIF_FLASH_MODE_TORCH
} HIF_FLASH_MODES;

/**
 * Select flash operation type
 * only when hif_ae_mode_t is AE_MODE_OFF or AE_MODE_ON
 *
 * @param mode flash type selection
 *
 * @see HIF_FLASH_MODES
 * @see hif_flash_firing_power_t
 * @see hif_flash_firing_time_t
 */
typedef struct
{
    int32 mode;
} hif_flash_mode_t;

#define HIF_FLASH_MODE_SIZE   sizeof(hif_flash_mode_t)

/*-----------------------------------------------------------------------------------------*/
// HIF_CAMERA_FLASH_POWER
/*-----------------------------------------------------------------------------------------*/
/**
 * Power for flash firing/torch
 *
 * @param power flash intensity [0 - 10].
 *              10 is max power
 *               0 is no flash. Linear
 *
 * @see hif_flash_firing_time_t
 */
typedef struct
{
    int32 power;
} hif_flash_firing_power_t;

#define HIF_FLASH_FIRING_POWER_SIZE   sizeof(hif_flash_firing_power_t)

/*-----------------------------------------------------------------------------------------*/
// HIF_CAMERA_FLASH_TIME
/*-----------------------------------------------------------------------------------------*/
/**
 * Firing time of flash relative to start of exposure
 *
 * @param time flashing time [nanoseconds]
 *
 * @see hif_flash_firing_power_t
 */
typedef struct
{
    int64 time;       // nanoseconds
} hif_flash_firing_time_t;

#define HIF_FLASH_FIRING_TIME_SIZE   sizeof(hif_flash_firing_time_t)

/*-----------------------------------------------------------------------------------------*/
// HIF_CAMERA_ANTIBANDING
/*-----------------------------------------------------------------------------------------*/

/**
 * Enum for controlling anti-banding
 *
 * @param HIF_ANTIBANDING_MODE_OFF
 * @param HIF_ANTIBANDING_MODE_50HZ forced to 50 Hz
 * @param HIF_ANTIBANDING_MODE_60HZ forced to 60 Hz
 * @param HIF_ANTIBANDING_MODE_AUTO auto detect
 */
//
typedef enum
{
    HIF_ANTIBANDING_MODE_OFF,
    HIF_ANTIBANDING_MODE_50HZ,
    HIF_ANTIBANDING_MODE_60HZ,
    HIF_ANTIBANDING_MODE_AUTO
} HIF_ANTIBANDING_MODES;

/**
 * Control anti-banding
 *
 * @param mode mode selection
 *
 * @see HIF_ANTIBANDING_MODES
 */
typedef struct
{
    int32 mode;
} hif_antibanding_mode_t;

#define HIF_ANTIBANDING_MODE_SIZE   sizeof(hif_antibanding_mode_t)

/*-----------------------------------------------------------------------------------------*/
// HIF_CAMERA_VIDEO_STAB_MODES
/*-----------------------------------------------------------------------------------------*/
/**
 * video stabilization modes.
 *
 * @param HIF_VIDEO_STABILIZATION_MODE_OFF video stabilization is disabled
 * @param HIF_VIDEO_STABILIZATION_MODE_ON  video stabilization is enabled
 */
typedef enum
{
    HIF_VIDEO_STABILIZATION_MODE_OFF = 0,
    HIF_VIDEO_STABILIZATION_MODE_ON = (0x00000001 << 0)
} HIF_VIDEO_STAB_MODES;

/**
 * Host query – to read supported by the target video stabilization modes.
 *
 * @param video_stab_modes bit mask of supported modes
 *
 * @see HIF_VIDEO_STAB_MODES
 */

typedef struct
{
    int32 video_stab_modes;
} hif_supported_video_stab_modes_t;

#define HIF_SUPPORTED_VIDEO_STAB_MODES_SIZE   sizeof(hif_supported_video_stab_modes_t)
/*-----------------------------------------------------------------------------------------*/
//  HIF_CAMERA_VIDEO_STAB
/*-----------------------------------------------------------------------------------------*/
/**
 * Video stabilization selection
 * Whether video stabilization is active
 *
 * @param mode mode selection
 *
 * @see HIF_VIDEO_STAB_MODES
 */
typedef struct
{
    // see VIDEO_STABILIZATION_MODES
    int32 mode;
} hif_video_stabilization_mode_t;

#define HIF_VIDEO_STABILIZATION_MODE_SIZE   sizeof(hif_video_stabilization_mode_t)

/*-----------------------------------------------------------------------------------------*/
// HIF_CAMERA_ORIENTATION
/*-----------------------------------------------------------------------------------------*/
/**
 * Clockwise angle through which the output image needs to be rotated
 * to be upright on the device screen in its native orientation.
 * Also defines the direction of rolling shutter readout, which is from
 * top to bottom in the sensor's coordinate system
 *
 * @param orientation orientation selection [0, 90, 180, 270]
 */
typedef struct
{
    int32 orientation;    // 0, 90, 180, 270
} hif_camera_orientation_t;

#define HIF_CAMERA_ORIENTATION_SIZE   sizeof(hif_camera_orientation_t)

/*-----------------------------------------------------------------------------------------*/
//   HIF_CAMERA_EDGE
/*-----------------------------------------------------------------------------------------*/
/**
 * Edge enhancement modes
 *
 * @param HIF_EDGE_OFF
 * @param HIF_EDGE_FAST
 * @param HIF_EDGE_HIGH_QUALITY
 */
typedef enum
{
    HIF_EDGE_OFF,
    HIF_EDGE_FAST,
    HIF_EDGE_HIGH_QUALITY
} HIF_EDGE_TYPES;
//

/**
 * Select operating mode for edge enhancement
 *
 * @param type edge type selection
 *
 * @see HIF_EDGE_TYPES
 */
typedef struct
{
    int32 type;
} hif_edge_type_t;

#define HIF_EDGE_TYPE_SIZE   sizeof(hif_edge_type_t)

/*-----------------------------------------------------------------------------------------*/
// HIF_CAMERA_EDGE_STRENGTH
/*-----------------------------------------------------------------------------------------*/

/**
 * Control the amount of edge enhancement applied to the images
 *
 * @param strength strength selection [1-10] 10 is maximum sharpening
 */
//
typedef struct
{
    int32 strength;    // [1-10]. 10 is maximum sharpening
} hif_edge_strength_t;

#define HIF_EDGE_STRENGTH_SIZE   sizeof(hif_edge_strength_t)

/*-----------------------------------------------------------------------------------------*/
// HIF_CAMERA_MAX_FACE_COUNT
/*-----------------------------------------------------------------------------------------*/
/**
 * HOST query – to read supported by the target Camera maximum number of
 * simultaneously detectable faces.
 *
 * @param max_face_count Maximum number of simultaneously detectable
 *                       faces >=4 if availableFaceDetectionModes lists
 *                       modes besides OFF, otherwise 0
 */
typedef struct
{
    int32 max_face_count;
} hif_max_face_count_t;

#define HIF_MAX_FACE_COUNT_SIZE   sizeof(hif_max_face_count_t)

/*-----------------------------------------------------------------------------------------*/
// HIF_CAMERA_FACE_DETECTION_MODES
/*-----------------------------------------------------------------------------------------*/
/**
 * List of supported FD modes
 *
 * @param HIF_FACE_DETECT_MODE_OFF    face detect is OFF
 *
 * @param HIF_FACE_DETECT_MODE_SIMPLE face detect is in fastest mode
 *                                    Return rectangle and confidence only
 *
 * @param HIF_FACE_DETECT_MODE_FULL   face detect is in full featured mode
 *                                    Optional Return all face metadata
 */
typedef enum
{
    HIF_FACE_DETECT_MODE_OFF = 0,
    HIF_FACE_DETECT_MODE_SIMPLE = (0x00000001 << 0),
    HIF_FACE_DETECT_MODE_FULL = (0x00000001 << 1)
} HIF_SUPPORTED_FACE_DETECT_MODES;

/**
 * HOST query – to read supported by the target Camera face detection modes.
 *
 * @param supported_fd_mode bit mask of supported modes
 *
 * @see HIF_SUPPORTED_FACE_DETECT_MODES
 */
typedef struct
{
    int32 supported_fd_mode;
} hif_supported_face_detect_mode_t;

#define HIF_SUPPORTED_FACE_DETECT_MODE_SIZE   sizeof(hif_supported_face_detect_mode_t)

/*-----------------------------------------------------------------------------------------*/
// HIF_CAMERA_FACE_DETECTION
/*-----------------------------------------------------------------------------------------*/
/**
 * Selects Camera face detection mode.
 *
 * @param mode face detection mode selection
 *
 * @see HIF_SUPPORTED_FACE_DETECT_MODES
 */
typedef struct
{
    int32 mode;
} hif_face_detect_mode_t;

#define HIF_FACE_DETECT_MODE_SIZE   sizeof(hif_face_detect_mode_t)

/*-----------------------------------------------------------------------------------------*/
// HIF_CAMERA_FACE_IDS
/*-----------------------------------------------------------------------------------------*/
/**
 * Maximum number of simultaneously detectable faces
 *
 * @see hif_face_ids_t
 */
#define HIF_MAX_FACE_COUNT   16  // TODO fix me

/**
 * HOST query – to read supported by the target Camera face detection list
 * of landmarks for detected faces (Only available if faceDetectMode == FULL).
 *
 * @param face_ids[HIF_MAX_FACE_COUNT] // TODO max_face_count ??? Units
 */
typedef struct
{
    int32 face_ids[HIF_MAX_FACE_COUNT];
} hif_face_ids_t;

#define HIF_FACE_IDS_SIZE   sizeof(hif_face_ids_t)

/*-----------------------------------------------------------------------------------------*/
//	HIF_CAMERA_FACE_LANDMARKS
/*-----------------------------------------------------------------------------------------*/
/**
 * HOST query - to read supported by the target Camera face detection list of unique IDs
 * for detected faces (Only available if faceDetectMode == FULL).
 * 
 * @param leftEyeX;   left eye X coordinate
 * @param leftEyeY;   left eye Y coordinate  
 * @param rightEyeX;  right eye X coordinate 
 * @param rightEyeY;  right eye Y coordinate 
 * @param mouthX;     mouth X coordinate   
 * @param mouthY;     mouth Y coordinate   
 */
typedef struct
{
    int32 leftEyeX;
    int32 leftEyeY;
    int32 rightEyeX;
    int32 rightEyeY;
    int32 mouthX;
    int32 mouthY;
} hif_face_landmark_t;

/**
 * List of unique IDs for detected faces
 * 
 * @param face_landmarks[max_face_count] list of landmarks    // TODO max_face_count
 * 
 * @see hif_face_landmark_t
 */
typedef struct
{
    hif_face_landmark_t face_landmarks[HIF_MAX_FACE_COUNT];
} hif_face_landmarks_t;

#define HIF_FACE_LANDMARKS_SIZE   sizeof(hif_face_landmarks_t)

/*-----------------------------------------------------------------------------------------*/
// HIF_CAMERA_FACE_RECTANGLES
/*-----------------------------------------------------------------------------------------*/
/**
 * Face detection bounding rectangles.
 *
 * @param xmin upper  left X coordinate  [Pixels]
 * @param ymin upper  left Y coordinate  [Pixels]
 * @param xmax bottom right X coordinate [Pixels]
 * @param ymax bottom right Y coordinate [Pixels]
 */
typedef struct
{
    int32 xmin;
    int32 ymin;
    int32 xmax;
    int32 ymax;
} hif_face_rect_t;

/**
 * HOST query - to read supported by the target Camera face detection list of the
 * bounding rectangles for detected faces (Only available if faceDetectMode == FULL).
 *
 * @param face_rectangles[HIF_MAX_FACE_COUNT]  list of the bounding
 * 										   rectangles for detected faces
 *
 * @see hif_face_rectangles_t
 * @see MAX_FACE_COUNT
 */
typedef struct
{
    hif_face_rect_t face_rectangles[HIF_MAX_FACE_COUNT];
} hif_face_rectangles_t;

#define HIF_FACE_RECTANGLES_SIZE   sizeof(hif_face_rectangles_t)

/*-----------------------------------------------------------------------------------------*/
// HIF_CAMERA_FACE_SCORES
/*-----------------------------------------------------------------------------------------*/
/**
 * HOST query - to read supported by the target Camera face detection
 * list of the face confidence scores for detected faces 
 * (Only available if faceDetectMode == FULL).
 * 
 * @param face_scores[HIF_MAX_FACE_COUNT]  score list for detected faces. Range [1-100]
 * 
 */

typedef struct
{
    int32 face_scores[HIF_MAX_FACE_COUNT];
} hif_face_scores_t;

#define HIF_FACE_SCORES_SIZE   sizeof(hif_face_scores_t)

/*-----------------------------------------------------------------------------------------*/
// HIF_CAMERA_CAPTURE_INTENT
/*-----------------------------------------------------------------------------------------*/
/**
 * Intention types
 * 
 * @param HIF_CAPTURE_INTENT_CUSTOM         This request doesn't fall into the other
 *                                          categories. Default to preview-like behavior.
 *
 * @param HIF_CAPTURE_INTENT_PREVIEW        This request is for a preview-like usecase.
 *                                          The precapture trigger may be used to start
 *                                          off a metering w/flash sequence
 *                                         
 * @param HIF_CAPTURE_INTENT_STILL_CAPTURE  This request is for a still capture-type usecase.
 *                                        
 * @param HIF_CAPTURE_INTENT_VIDEO_RECORD   This request is for a video recording usecase.
 *                                         
 * @param HIF_CAPTURE_INTENT_VIDEO_SNAPSHOT This request is for a video snapshot (still
 *                                          image while recording video) usecase
 *
 * @param HIF_CAPTURE_INTENT_ZERO_SHUTTER_LAG This request is for a ZSL usecase; the
 *                                            application will stream full-resolution
 *                                            images and reprocess one or several later
 *                                            for a final capture
 */
typedef enum
{
    HIF_CAPTURE_INTENT_CUSTOM,
    HIF_CAPTURE_INTENT_PREVIEW,
    HIF_CAPTURE_INTENT_STILL_CAPTURE,
    HIF_CAPTURE_INTENT_VIDEO_RECORD,
    HIF_CAPTURE_INTENT_VIDEO_SNAPSHOT,
    HIF_CAPTURE_INTENT_ZERO_SHUTTER_LAG
} HIF_CAPTURE_INTENT_TYPES;

/**
 * Information to 3A routines about the purpose
 * of this capture, to help decide optimal 3A strategy
 * 
 * @param type intention selection
 * 
 * see HIF_CAPTURE_INTENT_TYPES
 */
typedef struct
{
    int32 type;
} hif_capture_intent_type_t;

#define HIF_CAPTURE_INTENT_TYPE_SIZE   sizeof(hif_capture_intent_type_t)

/*-----------------------------------------------------------------------------------------*/
// HIF_CAMERA_3A_MODE
/*-----------------------------------------------------------------------------------------*/
/**
 * Camera 3A algorithms modes
 *
 * @param HIF_MODE_OFF  3A algorithms are OFF
 *                  Full application control of pipeline. All 3A routines
 *                  are disabled, no other settings in  host control have
 *                  any effect
 *
 * @param HIF_MODE_AUTO 3A algorithms operate in auto mode
 *                  Use settings for each individual 3A routine. Manual
 *                  control of capture parameters is disabled. All controls
 *                  besides sceneMode take effect
 *
 * @param HIF_MODE_USE_SCENE_MODE 3A algorithms are in scene mode
 *                            Use specific scene mode. Enabling this disables
 *                            aeMode, awbMode and afMode controls; the HAL must
 *                            ignore those settings while USE_SCENE_MODE is
 *                            active (except for FACE_PRIORITY scene mode).
 *                            Other control entries are still active.
 */
typedef enum
{
    HIF_MODE_OFF,
    HIF_MODE_AUTO,
    HIF_MODE_USE_SCENE_MODE
} HIF_3A_MODES;

/**
 * Overall mode of 3A control routines
 *
 * @param mode mode selection
 *
 * @see HIF_3A_MODES
 */
typedef struct
{
    // see MODES
    int32 mode;
} hif_3a_mode_t;

#define HIF_3A_MODE_SIZE   sizeof(hif_3a_mode_t)

/*-----------------------------------------------------------------------------------------*/
// HIF_CAMERA_DEMOSAIC_MODE
/*-----------------------------------------------------------------------------------------*/
/**
 * Demosaicing modes.
 *
 * @param DEMOSAIC_MODE_FAST         Minimal or no slow down of frame rate compared to Bayer RAW output
 * @param DEMOSAIC_MODE_HIGH_QUALITY High-quality may reduce output frame rate
 */
typedef enum
{
    HIF_DEMOSAIC_MODE_FAST,
    HIF_DEMOSAIC_MODE_HIGH_QUALITY
} HIF_DEMOSAIC_MODES;

//
/**
 * Controls the quality of the demosaicing processing
 *
 * @param mode mode selection
 *
 * @see HIF_DEMOSAIC_MODES
 */
typedef struct
{
    int32 mode;
} hif_demosaic_mode_t;

#define HIF_DEMOSAIC_MODE_SIZE   sizeof(hif_demosaic_mode_t)

/*-----------------------------------------------------------------------------------------*/
//	  HIF_CAMERA_GEOMETRIC_MODE
/*-----------------------------------------------------------------------------------------*/
/**
 * Geometric modes
 *
 * @param HIF_GEOMETRIC_MODE_OFF
 * @param HIF_GEOMETRIC_MODE_FAST         Minimal or no slow down of frame rate compared to Bayer RAW output
 * @param HIF_GEOMETRIC_MODE_HIGH_QUALITY High-quality may reduce output frame rate
 */
typedef enum
{
    HIF_GEOMETRIC_MODE_OFF,
    HIF_GEOMETRIC_MODE_FAST,
    HIF_GEOMETRIC_MODE_HIGH_QUALITY
} HIF_GEOMETRIC_MODES;

/**
 * Operating mode of geometric correction
 *
 * @param mode mode selection
 *
 * @see HIF_GEOMETRIC_MODES
 */
typedef struct
{
    int32 mode;
} hif_geometric_mode_t;

#define HIF_GEOMETRIC_MODE_SIZE   sizeof(hif_geometric_mode_t)

/*-----------------------------------------------------------------------------------------*/
// HIF_CAMERA_GEOMETRIC_STRENGTH
/*-----------------------------------------------------------------------------------------*/
/**
 * Controls strength of geometric correction
 *
 * @param strength strength selection. unitless: [1-10]; 10 is full shading compensation
 *
 * @see hif_geometric_mode_t
 */
typedef struct
{
    int32 strength;
} hif_geometric_strength_t;

#define HIF_GEOMETRIC_STRENGTH_SIZE   sizeof(hif_geometric_strength_t)

/*-----------------------------------------------------------------------------------------*/
//  HIF_CAMERA_HOTPIXEL_MODE
/*-----------------------------------------------------------------------------------------*/
/**
 * hot pixel correction modes
 *
 * @param HIF_HOT_PIXEL_MODE_OFF          Disabled
 * @param HIF_HOT_PIXEL_MODE_FAST         Minimal or no slow down of frame rate compared to Bayer RAW output
 * @param HIF_HOT_PIXEL_MODE_HIGH_QUALITY High-quality may reduce output frame rate
 */
typedef enum
{
    HIF_HOT_PIXEL_MODE_OFF,
    HIF_HOT_PIXEL_MODE_FAST,
    HIF_HOT_PIXEL_MODE_HIGH_QUALITY
} HIF_HOT_PIXEL_MODES;

/**
 * Control the amount of shading correction applied to the images
 * Set operational mode for hot pixel correction
 *
 * @param mode mode selection
 *
 * @see HIF_HOT_PIXEL_MODES
 */
typedef struct
{
    int32 mode;
} hif_hot_pixel_mode_t;

#define HIF_HOT_PIXEL_MODE_SIZE   sizeof(hif_hot_pixel_mode_t)

/*-----------------------------------------------------------------------------------------*/
// HIF_CAMERA_LENS_APERTURE
/*-----------------------------------------------------------------------------------------*/
/**
 * Size of the lens aperture
 *
 * @param aperture aperture selection. Units: f-number (f/NNN);
 * Range: [TODO to create list of available appertures]
 */
typedef struct
{
    float aperture;
} hif_lens_aperture_t;

#define HIF_LENS_APERTURE_SIZE   sizeof(hif_lens_aperture_t)

/*-----------------------------------------------------------------------------------------*/
// HIF_CAMERA_LENS_FLTR_DENSITY
/*-----------------------------------------------------------------------------------------*/
/**
 * State of lens neutral density filter(s)
 *
 * @param density density selection. Units: number of stops of filtering 
 * Range [TODO to create list of available filter densities]
 */
typedef struct
{
    float density;
} hif_lens_filter_density_t;

#define HIF_LENS_FILTER_DENSITY_SIZE   sizeof(hif_lens_filter_density_t)

/*-----------------------------------------------------------------------------------------*/
// HIF_CAMERA_LENS_FOCAL_LENGTH
/*-----------------------------------------------------------------------------------------*/
/**
 * Lens optical zoom setting
 *
 * @param focal_length optical zoom selection
 */
typedef struct
{
    float focal_length;
} hif_lens_focal_length_t;

#define HIF_LENS_FOCAL_LENGTH_SIZE   sizeof(hif_lens_focal_length_t)

/*-----------------------------------------------------------------------------------------*/
//  HIF_CAMERA_LENS_FOCUS_DISTANCE
/*-----------------------------------------------------------------------------------------*/
/**
 * Distance to plane of sharpest focus, measured from frontmost
 * surface of the lens.
 *
 * @param focus_distance Units: diopters (1/m); >= 0
 */
typedef struct
{
    float focus_distance;
} hif_lens_focus_distance_t;

#define HIF_LENS_FOCUS_DISTANCE_SIZE   sizeof(hif_lens_focus_distance_t)

/*-----------------------------------------------------------------------------------------*/
// HIF_CAMERA_LENS_OPTICAL_STAB
/*-----------------------------------------------------------------------------------------*/

/**
 * Optical image stabilization modes.
 *
 * @param HIF_LENS_OPTICAL_STABILIZATION_MODE_OFF disabled
 * @param HIF_LENS_OPTICAL_STABILIZATION_MODE_ON  enabled
 */
typedef enum
{
    HIF_LENS_OPTICAL_STABILIZATION_MODE_OFF,
    HIF_LENS_OPTICAL_STABILIZATION_MODE_ON,
} HIF_LENS_OPTICAL_STABILIZATION_MODES;

/**
 * Controls whether optical image stabilization is enabled.
 *
 * @param mode see HIF_LENS_OPTICAL_STABILIZATION_MODES enum
 *
 * @see HIF_LENS_OPTICAL_STABILIZATION_MODES
 */
typedef struct
{
    int32 mode;
} hif_lens_optical_stab_mode_t;

#define HIF_LENS_OPTICAL_STAB_MODE_SIZE   sizeof(hif_lens_optical_stab_mode_t)

/*-----------------------------------------------------------------------------------------*/
// HIF_CAMERA_NOISE_REDUCTION
/*-----------------------------------------------------------------------------------------*/
/**
 * Noise reduction modes.
 *
 * @param HIF_NOISE_REDUCTION_OFF           Disabled
 * @param HIF_NOISE_REDUCTION_FAST          Minimal or no slow down of frame rate compared to Bayer RAW output
 * @param HIF_NOISE_REDUCTION_HIGH_QUALITY  High-quality may reduce output frame rate
 */
typedef enum
{
    HIF_NOISE_REDUCTION_OFF,
    HIF_NOISE_REDUCTION_FAST,
    HIF_NOISE_REDUCTION_HIGH_QUALITY
} HIF_NOISE_REDUCTION_MODES;

/**
 * Controls mode of operation for the noise reduction algorithm.
 *
 * @param mode mode selection
 *
 * @see HIF_NOISE_REDUCTION_MODES
 */
typedef struct
{
    int32 mode;
} hif_noise_reduction_mode_t;

#define HIF_NOISE_REDUCTION_MODE_SIZE   sizeof(hif_noise_reduction_mode_t)

/*-----------------------------------------------------------------------------------------*/
// HIF_CAMERA_NOISE_RED_STRENGTH
/*-----------------------------------------------------------------------------------------*/
/**
 * Control the amount of noise reduction applied to the images
 *
 * @param strength amount of noise reduction. Range: 1-10; 10 is max noise reduction 
 */
typedef struct
{
    int32 strength;
} hif_noise_reduction_strength_t;

#define HIF_NOISE_REDUCTION_STRENGTH_SIZE   sizeof(hif_noise_reduction_strength_t)

/*-----------------------------------------------------------------------------------------*/
// HIF_CAMERA_FRAME_COUNT
/*-----------------------------------------------------------------------------------------*/
/**
 * A frame counter set by the framework. Must be maintained unchanged in
 * output frame. This value monotonically increases with every new result
 * (that is, each new result has a unique frameCount value).
 *
 * @param frameCount frame counter
 */
typedef struct
{
    int32 frameCount;
} hif_request_frame_count_t;

#define HIF_REQUEST_FRAME_COUNT_SIZE   sizeof(hif_request_frame_count_t)

/*-----------------------------------------------------------------------------------------*/
// HIF_CAMERA_REQUEST_ID
/*-----------------------------------------------------------------------------------------*/
/**
 * An application-specified ID for the current request. Must be
 * maintained unchanged in output frame
 *
 *
 * @param Id application-specified ID
 */
typedef struct
{
    int32 Id;
} hif_request_id_t;

#define HIF_REQUEST_ID_SIZE   sizeof(hif_request_id_t)

/*-----------------------------------------------------------------------------------------*/
//  HIF_CAMERA_REQUEST_TYPE
/*-----------------------------------------------------------------------------------------*/
/**
 * Request types
 *
 * @param HIF_REQUEST_TYPE_CAPTURE   request for capture
 * @param HIF_REQUEST_TYPE_REPROCESS request for re-process
 */
typedef enum
{
    HIF_REQUEST_TYPE_CAPTURE,
    HIF_REQUEST_TYPE_REPROCESS
} HIF_REQUEST_TYPES;

/**
 * The type of the request; either CAPTURE or REPROCESS.
 * For HAL3, this tag is redundant
 *
 * @param type request type
 *
 * @see HIF_REQUEST_TYPES
 */
typedef struct
{
    int32 type;
} hif_request_type_t;

#define HIF_REQUEST_TYPE_SIZE   sizeof(hif_request_type_t)

/*-----------------------------------------------------------------------------------------*/
// HIF_CAMERA_OUTPUT_STREAMS
/*-----------------------------------------------------------------------------------------*/
/**
 * Lists which camera output streams image data from this capture
 * must be sent to.
 *
 * @param streams[5] streams list //TODO Units
 */

#define MAX_HIF_STREAM_COUNT    5     // TODO Just an example for 5 streams
typedef struct
{
    int32 streams[MAX_HIF_STREAM_COUNT];
} hif_request_output_streams_t;

#define HIF_REQUEST_OUTPUT_STREAMS_SIZE    sizeof(hif_request_output_streams_t)

/*-----------------------------------------------------------------------------------------*/
//	HIF_CAMERA_OUTPUT_STREAM_CONTROL
/*-----------------------------------------------------------------------------------------*/
/**
 * Stream controls
 *
 * @param HIF_STREAM_OFF stop stream
 * @param HIF_STREAM_ON  start stream
 */
typedef enum
{
    HIF_STREAM_OFF,
    HIF_STREAM_ON
} HIF_CAMERA_STREAM_CNTR;

/**
 * Control over desired stream
 *
 * @param stream_id      stream ID
 * @param stream_control stream control - On/Off
 *
 * @see HIF_CAMERA_STREAM_CNTR
 */
typedef struct
{
    int32 stream_id;
    int32 stream_control;	// Use HIF_CAMERA_STREAM_CNTR enum values
} hif_output_streams_control_t;

#define HIF_OUTPUT_STREAMS_CONTROL_SIZE    sizeof(hif_output_streams_control_t)

/*-----------------------------------------------------------------------------------------*/
//  HIF_CAMERA_METADATA_MODE
/*-----------------------------------------------------------------------------------------*/
/**
 * Meta data control modes
 *
 * @param HIF_METADATA_MODE_NONE cease metadata production
 *                               No metadata should be produced on output, except
 *                               for application-bound buffer data. If no
 *                               application-bound streams exist, no frame should
 *                               be placed in the output frame queue. If such
 *                               streams exist, a frame should be placed on the
 *                               output queue with null metadata but with the
 *                               necessary output buffer information. Timestamp
 *                               information should still be included with any
 *                               output stream buffers
 *
 * @param HIF_METADATA_MODE_FULL enable meta data production
 *                               All metadata should be produced. Statistics will
 *                               only be produced if they are separately enabled
 */
typedef enum
{
    HIF_METADATA_MODE_NONE,
    HIF_METADATA_MODE_FULL
} HIF_REQUEST_METADATA_MODES;

/**
 * Control over desired stream
 *
 * @param mode metadata mode selection
 *
 * @see HIF_REQUEST_METADATA_MODES
 */
typedef struct
{
    int32 mode;
} hif_request_metadata_mode_t;

#define HIF_REQUEST_METADATA_MODE_SIZE   sizeof(hif_request_metadata_mode_t)

/*-----------------------------------------------------------------------------------------*/
//	  HIF_CAMERA_SENSITIVITY
/*-----------------------------------------------------------------------------------------*/
/**
 * Gain applied to image data. Must be implemented through analog gain
 * only if set to values below 'maximum analog sensitivity'.
 *
 * @param sensitivity gain amount
 *
 * @see
 */
typedef struct
{
    int32 sensitivity;
} hif_camera_sensitivity_t;

#define HIF_CAMERA_SENSITIVITY_SIZE   sizeof(hif_camera_sensitivity_t)

/*-----------------------------------------------------------------------------------------*/
//  HIF_CAMERA_STATS_HIST_BUCKET_COUNT
/*-----------------------------------------------------------------------------------------*/
/**
 * HOST query - to read the target Camera number of histogram buckets supported (>= 64).
 *
 * @param hist_bucket_count Number of histogram buckets supported
 */
typedef struct
{
    int32 hist_bucket_count;
} hif_stats_hist_bucket_count_t;

#define HIF_STATS_HIST_BUCKET_COUNT_SIZE   sizeof(hif_stats_hist_bucket_count_t)

/*-----------------------------------------------------------------------------------------*/
// HIF_CAMERA_STATS_MAX_HIST_COUNT
/*-----------------------------------------------------------------------------------------*/
/**
 * HOST query - to read the target Camera maximum value possible for a histogram bucket
 *
 * @param max_hist_count Maximum value possible for a histogram bucket
 *
 * @see
 */
typedef struct
{
    int32 max_hist_count;
} hif_stats_max_hist_count_t;

#define HIF_STATS_MAX_HIST_COUNT_SIZE   sizeof(hif_stats_max_hist_count_t)

/*-----------------------------------------------------------------------------------------*/
// HIF_CAMERA_STATS_HIST_COUNT
/*-----------------------------------------------------------------------------------------*/

#define HIF_MAX_HIST_COUNT  128 // TODO fix me max_hist_count
/**
 * HOST query - to read the target Camera count of pixels for each color
 *              channel that fall into each histogram bucket, scaled to
 *              be between 0 and �max_hist_count�. The k'th bucket (0-based)
 *              covers the input range. If only a monochrome sharpness map
 *              is supported, all channels should have the same data.
 *
 * @param histogram_count [max_hist_count][3]  A 3-channel histogram based on
 *                                             the raw sensor data
 *
 * @see hif_max_hist_count_t
 */
typedef struct
{
    int32 histogram_count[HIF_MAX_HIST_COUNT][3]; // TODO max_hist_count
} hif_stats_hist_count_t;

#define HIF_STATS_HIST_COUNT_SIZE   sizeof(hif_stats_hist_count_t)

/*-----------------------------------------------------------------------------------------*/
//	HIF_CAMERA_STATS_HISTOGRAM
/*-----------------------------------------------------------------------------------------*/
/**
 * Histogram operating modes
 *
 * @param HIF_HISTOGRAM_MODE_OFF cease histogram generation
 * @param HIF_HISTOGRAM_MODE_ON  enable histogram generation
 */
typedef enum
{
    HIF_HISTOGRAM_MODE_OFF,
    HIF_HISTOGRAM_MODE_ON
} HIF_STATISTICS_HISTOGRAM_MODES;

/**
 * Controls histogram generation operating mode
 *
 * @param mode mode selection
 *
 * @see HIF_STATISTICS_HISTOGRAM_MODES
 */
typedef struct
{
    int32 mode;
} hif_histogram_mode_t;

#define HIF_HISTOGRAM_MODE_SIZE   sizeof(hif_histogram_mode_t)

/*-----------------------------------------------------------------------------------------*/
// HIF_CAMERA_STATS_SHARPNESS
/*-----------------------------------------------------------------------------------------*/
/**
 * Sharpness map operating modes.
 *
 * @param HIF_SHAPNES_MAP_MODE_OFF disable sharpness map
 * @param HIF_SHAPNES_MAP_MODE_ON  enable sharpness map
 */
typedef enum
{
    HIF_SHAPNES_MAP_MODE_OFF,
    HIF_SHAPNES_MAP_MODE_ON
} HIF_STATISTICS_SHAPNES_MAP_MODES;

/**
 * Controls operating mode for sharpness map generation
 *
 * @param mode mode selection
 *
 * @see HIF_STATISTICS_SHAPNES_MAP_MODES
 */
typedef struct
{
    int32 mode;
} hif_sharpnes_mode_t;

#define HIF_SHAPNES_MAP_MODE_SIZE   sizeof(hif_sharpnes_mode_t)

/*-----------------------------------------------------------------------------------------*/
//HIF_CAMERA_STATS_MAX_SHARPNESS_MAP_VALUE
/*-----------------------------------------------------------------------------------------*/
/**
 * Host query -  to read the target Camera maximum value possible for a sharpness map region.
 *
 * @param max_sharp_map_malue Maximum value possible for a sharpness map region.
 *
 * @see
 */
typedef struct
{
    int32 max_sharp_map_malue;
} hif_stats_max_sharp_map_value_t;

#define HIF_STATS_MAX_SHARP_MAP_VALUE_SIZE   sizeof(hif_stats_max_sharp_map_value_t)

/*-----------------------------------------------------------------------------------------*/
// HIF_CAMERA_STATS_SHARPNESS_MAP
/*-----------------------------------------------------------------------------------------*/

#define HIF_MAX_SHARP_MAP_SIZE  128 // TODO fix me

/**
 * Host query - to read the target Camera estimated sharpness for each
 * region of the input image. Normalized to be between 0 and 'max_sharp_map_malue'.
 * Higher values mean sharper (better focused). If only a monochrome
 * sharpness map is supported, all channels should have the same data
 *
 * @param sharpness_map [sharp_map_size][3] A 3-channel sharpness map,
 *                                          based on the raw sensor data
 */
typedef struct
{
    int32 sharpness_map[HIF_MAX_SHARP_MAP_SIZE][3];  // TODO sharp_map_size ?????
} hif_stats_sharp_map_t;

#define HIF_STATS_SHARP_MAP_SIZE   sizeof(hif_stats_sharp_map_t)

/*-----------------------------------------------------------------------------------------*/
// HIF_CAMERA_STATS_SHARPNESS_MAP_SIZE
/*-----------------------------------------------------------------------------------------*/
/**
 * Host query - to read the target Camera dimensions of the sharpness map.
 *
 * @param sharp_map_size Dimensions of the sharpness map.
 */
typedef struct
{
    int32 sharp_map_size;
} hif_stats_sharp_map_size_t;

#define HIF_STATS_SHARP_MAP_SIZE_SIZE   sizeof(hif_stats_sharp_map_size_t)

/*-----------------------------------------------------------------------------------------*/
// HIF_CAMERA_STATS_LSC
/*-----------------------------------------------------------------------------------------*/
/**
 * Lens shading modes
 *
 * @param LENS_SHADING_MODE_OFF disable lens shading map output
 * @param LENS_SHADING_MODE_ON  enable lens shading map output
 */
typedef enum
{
    HIF_LENS_SHADING_MODE_OFF,
    HIF_LENS_SHADING_MODE_ON
} HIF_STATISTICS_LENS_SHADING_MAP_MODES;

/**
 * Controls whether the HAL needs to output the lens shading map in
 * output result metadata. When set to ON, the statistics
 * lensShadingMap must be provided in the output result metdata
 *
 * @param mode mode selection
 *
 * @see HIF_STATISTICS_LENS_SHADING_MAP_MODES
 */
typedef struct
{
    int32 mode;
} hif_lsc_map_mode_t;

#define HIF_LSC_MAP_MODE_SIZE   sizeof(hif_lsc_map_mode_t)

/*-----------------------------------------------------------------------------------------*/
//		HIF_CAMERA_AVAILABLE_LEDS
/*-----------------------------------------------------------------------------------------*/
#define HIF_MAX_AVAILABLE_LEDS  10 // TODO fix  me

/**
 * Host query - to read the target Camera list of camera LEDs that are available on this system.
 *
 * @param leds_list [N] list of available camera LEDs
 */
typedef struct
{
    int32 leds_list[HIF_MAX_AVAILABLE_LEDS];   // TODO N Range []
} hif_available_leds_t;

#define HIF_HIST_AVAILABLE_LEDS_SIZE   sizeof(hif_available_leds_t)

/*-----------------------------------------------------------------------------------------*/
//	 HIF_CAMERA_LED_TRANSMIT
/*-----------------------------------------------------------------------------------------*/
/**
 * Led transmit modes
 *
 * @param HIF_LED_TRANSMIT_ON  enable transmit
 * @param HIF_LED_TRANSMIT_OFF disable transmit
 */
typedef enum
{
    HIF_LED_TRANSMIT_ON,
    HIF_LED_TRANSMIT_OFF
} HIF_LED_TRANSMIT_MODE;

/**
 * Controls Led transmission
 * This LED is nominally used to indicate to the user that the camera
 * is powered on and may be streaming images back to the Application
 * Processor. In certain rare circumstances, the OS may disable this
 * when video is processed locally and not transmitted to any
 * untrusted applications.
 * In particular, the LED must always be on when the data could be
 * transmitted off the device. The LED should always be on whenever
 * data is stored locally on the device.
 * The LED may be off if a trusted application is using the data that
 * doesn't violate the above rules.
 *
 * @param mode mode selection
 *
 * @see HIF_LED_TRANSMIT_MODE
 */
typedef struct
{
    int32 mode;
} hif_led_transmit_mode_t;

#define HIF_LED_TRANSMIT_MODE_SIZE   sizeof(hif_led_transmit_mode_t)

/*-----------------------------------------------------------------------------------------*/
//	 HIF_CAMERA_BLACKLEVEL_LOCK
/*-----------------------------------------------------------------------------------------*/
/**
 * Black level lock modes
 *
 * @param HIF_BLACK_LEVEL_LOCK_OFF black-level compensation is free to vary
 * @param HIF_BLACK_LEVEL_LOCK_ON  black-level compensation must not change
 */
typedef enum
{
    HIF_BLACK_LEVEL_LOCK_OFF,
    HIF_BLACK_LEVEL_LOCK_ON
} HIF_BLACK_LEVEL_LOCK;

/**
 * Controls whether black-level compensation is locked to its current values,
 * or is free to vary.
 * When set to ON, the values used for black-level compensation must not change
 * until the lock is set to OFF.
 * Since changes to certain capture parameters (such as exposure time) may require
 * resetting of black level compensation, the HAL must report whether setting the
 * black level lock was successful in the output result metadata.
 * The black level locking must happen at the sensor, and not at the ISP. If for
 * some reason black level locking is no longer legal (for example, the analog gain
 * has changed, which forces black levels to be recalculated), then the HAL is free
 * to override this request (and it must report 'OFF' when this does happen) until
 * the next time locking is legal again.
 *
 * @param lock black-level mode selection
 *
 * @see HIF_BLACK_LEVEL_LOCK
 */
typedef struct
{
    int32 lock;
} hif_black_level_lock_t;

#define HIF_BLACK_LEVEL_LOCK_SIZE   sizeof(hif_black_level_lock_t)

/*-----------------------------------------------------------------------------------------*/
//	HIF_CAMERA_DATA_TRANSFER
/*-----------------------------------------------------------------------------------------*/
/**
 * Data transfer packet identifiers
 *
 * @param HIF_HIF_DATA_PACKET_FIRST first packet from multi-packet transmission
 * @param HIF_HIF_DATA_PACKET_NEXT  packet from the niddle of multi-packet transmission
 * @param HIF_HIF_DATA_PACKET_LAST  last packet from multi-packet transmission or
 *                              single packet transmission
 */
typedef enum
{
    HIF_DATA_PACKET_FIRST = 0x00000000,
    HIF_DATA_PACKET_NEXT,
    HIF_DATA_PACKET_LAST = 0xFFFFFFFF
} HIF_DATA_TRANSFER_IDS;

#define HIF_MAX_DATA_CHUNK_SIZE 512 // TODO fix_me Just an example for 512 Bytes chunks

/**
 * Transfers chunk of data.
 * This command introduces mechanism for exchanging huge data volumes between
 * the HOST and the target camera device.
 * By using transfer_id field can indicate is this the first, next in the queue
 * or the last data chunk. If there is only one data chunk to be sent must use
 * HIF_DATA_PACKET_LAST identifier.
 * One option to keep tracking the packets in the queue is to not use
 * HIF_DATA_PACKET_NEXT identifier, but just 'sequence number' of the data chunk
 * between in the range 0x00000001 and 0xFFFFFFFE.
 * It is not necessary all chunks to fit completely in the Data Buffer. The actual
 * number of valid Bytes is specified in transfer_byte_count field.
 * HIF_MAX_DATA_CHUNK_SIZE makes the HIF SPI buffers alignment easier (no dynamic sizes)
 * and have to be chosen when the SPI buffer size is chosen (for example 2048 Bytes).
 *
 * @param transfer_id          Indicates chunk position
 * @param transfer_byte_count  number of bytes in this chunk
 * @param data_chunk[HIF_MAX_DATA_CHUNK_SIZE] chunk data
 *
 * @see HIF_DATA_TRANSFER_IDS
 * @see HIF_MAX_DATA_CHUNK_SIZE
 */
typedef struct
{
    uint32 transfer_id;
    int32 transfer_byte_count;
    uint8 data_chunk[HIF_MAX_DATA_CHUNK_SIZE];
} hif_data_transfer_t;

#define HIF_CAMERA_DATA_TRANSFER_SIZE   sizeof(hif_data_transfer_t)
/*-----------------------------------------------------------------------------------------*/

#ifdef _cplusplus
}
#endif /* __cplusplus */

#endif /* __HIF_H__ */

