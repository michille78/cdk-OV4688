
#if defined(__sparc)

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <rtems.h>
#include <bsp.h>

#include <semaphore.h>
#include <pthread.h>
#include <sched.h>
#include <fcntl.h>
#include <mv_types.h>
#include <rtems/cpuuse.h>
#include <DrvLeon.h>

#define CMX_CONFIG_SLICE_7_0       (0x11111111)
#define CMX_CONFIG_SLICE_15_8      (0x11111111)
#define L2CACHE_NORMAL_MODE (0x6)  // In this mode the L2Cache acts as a cache for the DRAM
#define L2CACHE_CFG     (L2CACHE_NORMAL_MODE)
#define BIGENDIANMODE   (0x01000786)
/* Sections decoration is require here for downstream tools */
extern CmxRamLayoutCfgType __cmx_config;
CmxRamLayoutCfgType __attribute__((section(".cmx.ctrl"))) __cmx_config = {CMX_CONFIG_SLICE_7_0, CMX_CONFIG_SLICE_15_8};
uint32_t __l2_config   __attribute__((section(".l2.mode")))  = L2CACHE_CFG;

BSP_SET_CLOCK(180000,0,1,1,DEFAULT_RTEMS_CSS_LOS_CLOCKS,DEFAULT_RTEMS_MSS_LRT_CLOCKS,0,0,0);
BSP_SET_L2C_CONFIG(0,DEFAULT_RTEMS_L2C_REPLACEMENT_POLICY,DEFAULT_RTEMS_L2C_LOCKED_WAYS,DEFAULT_RTEMS_L2C_MODE,0,NULL);



int main(int argc, char **argv);

void* POSIX_Init (void *args)
{
	printf ("\n");
	printf ("RTEMS POSIX Started\n");  /* initialise variables */

	main(0, NULL);

	exit(0);
}

#if !defined (__CONFIG__)
#define __CONFIG__

/* ask the system to generate a configuration table */
#define CONFIGURE_INIT

#define CONFIGURE_MICROSECONDS_PER_TICK 1000 /* 1 milliseconds */

#define CONFIGURE_TICKS_PER_TIMESLICE 1

#define CONFIGURE_APPLICATION_NEEDS_CONSOLE_DRIVER

#define CONFIGURE_APPLICATION_NEEDS_CLOCK_DRIVER

#define CONFIGURE_POSIX_INIT_THREAD_TABLE

//#define  CONFIGURE_MINIMUM_TASK_STACK_SIZE 8192
/*3K is usually enough for a thread*/
#define  CONFIGURE_MINIMUM_TASK_STACK_SIZE 3072

#define CONFIGURE_MAXIMUM_POSIX_THREADS              40

#define CONFIGURE_MAXIMUM_POSIX_MUTEXES              64

#define CONFIGURE_MAXIMUM_POSIX_KEYS                 64

#define CONFIGURE_MAXIMUM_POSIX_SEMAPHORES           64

#include <rtems/confdefs.h>

#endif /* if defined CONFIG */

#endif // defined(__sparc)
