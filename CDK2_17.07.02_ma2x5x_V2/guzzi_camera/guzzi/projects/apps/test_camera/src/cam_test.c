/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file cam_test.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <osal/osal_stdtypes.h>
#include <osal/osal_stdlib.h>
#include <osal/osal_mutex.h>
#include <utils/mms_debug.h>

#include <camera.h>
#include <camera_config_index.h>
#include <camera_config_struct.h>
#include <osal/osal_time.h>

#include <guzzi/nvm_reader.h>

typedef struct {
    camera_t *camera;
    osal_sem *cmd_sem;
    int stop;
    int start_err;
    int capture_in_progress;
    int prof_file_id;
} app_prv_t;

mmsdbg_define_variable(
        vdl_camera_test,
        DL_DEFAULT,
        0,
        "vdl_camera_test",
        "Camera component test."
    );
#define MMSDEBUGLEVEL mmsdbg_use_variable(vdl_camera_test)

static void callback(
        camera_t *camera,
        void *app_prv_void,
        camera_event_t event,
        int datan,
        void *datap
    )
{
    app_prv_t *app_prv;

    app_prv = app_prv_void;

    switch (event.type) {
        case CAMERA_EVENT_ERROR:
            mmsdbg(DL_ERROR, "Camera CB ERROR event.");
            break;
        case CAMERA_EVENT_ERROR_START:
            mmsdbg(DL_MESSAGE, "Camera CB ERROR_START event.");
            app_prv->start_err = -1;
            osal_sem_post(app_prv->cmd_sem);
            break;
        case CAMERA_EVENT_START_DONE:
            mmsdbg(DL_MESSAGE, "Camera CB START_DONE event.");
            osal_sem_post(app_prv->cmd_sem);
            break;
        case CAMERA_EVENT_STOP_DONE:
            mmsdbg(DL_MESSAGE, "Camera CB STOP_DONE event.");
            osal_sem_post(app_prv->cmd_sem);
            break;
        case CAMERA_EVENT_FLUSH_DONE:
            mmsdbg(DL_MESSAGE, "Camera CB FLUSH_DONE event.");
            osal_sem_post(app_prv->cmd_sem);
            break;
        case CAMERA_EVENT_PROCESS_DONE:
            mmsdbg(DL_MESSAGE, "Camera CB PROCESS_DONE event.");
            if (app_prv->capture_in_progress) {
                osal_sem_post(app_prv->cmd_sem);
            }
            if (!app_prv->stop) {
                camera_process(app_prv->camera, datan, datap);
            }
            break;
        case CAMERA_EVENT_BUFFER_FLUSH:
            mmsdbg(DL_MESSAGE, "Camera CB BUFFER_FLUSH event.");
            break;
        case CAMERA_EVENT_GENERIC:
            mmsdbg(DL_FATAL, "Camera CB GENERIC event: sub_type=%d, num=%d.", event.sub_type, datan);
            break;
        default:
            mmsdbg(DL_ERROR, "Unkonw camera event (%d)!", event.v);
    }
}

// set manual exposure time [us], analog gain [ float x.xxx]
int cam_set_manual_exposure_gain(app_prv_t *app_prv, uint32 exp_time, float gain)
{
    int err;
    // AE control mode - AE_MODE_OFF
    {
        static guzzi_camera3_controls_control_ae_mode_t ae_req;

        ae_req.v = GUZZI_CAMERA3_ENUM_CONTROL_AE_MODE_OFF;

        camera_config_set_begin(app_prv->camera);
        err = camera_config_set(app_prv->camera,
                                GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AE_MODE,
                                &ae_req);
        if (err) {
            camera_config_set_cancel(app_prv->camera);
        } else {
            camera_config_set_end(app_prv->camera);
        }
        mmsdbg(DL_PRINT, "\n################\n# AE mode %d \n################", ae_req.v);
    }

    // exposure time
    {
        #define MULT_US_NS  1000
        static guzzi_camera3_dynamic_sensor_exposure_time_t exposure_time_req;

        exposure_time_req.v = exp_time * MULT_US_NS;

        mmsdbg(DL_PRINT, "\n################\n# Exposure %d ns \n################", exposure_time_req.v);

        camera_config_set_begin(app_prv->camera);
        err = camera_config_set(app_prv->camera,
                                GUZZI_CAMERA3_INDEX_CONTROLS_SENSOR_EXPOSURE_TIME,
                                &exposure_time_req);
        if (err) {
            camera_config_set_cancel(app_prv->camera);
        } else {
            camera_config_set_end(app_prv->camera);
        }
    }
    //  GAIN
    {
        static guzzi_camera3_dynamic_sensor_sensitivity_t sensitivity_req;

        // sensitivity is ISO ( 1.0 = 100)
        sensitivity_req.v = (uint32)(100.0 * gain + 0.5);

        mmsdbg(DL_PRINT, "\n################\n# Sensitivity %d  \n################", sensitivity_req.v);
        camera_config_set_begin(app_prv->camera);
        err = camera_config_set(app_prv->camera,
                                GUZZI_CAMERA3_INDEX_CONTROLS_SENSOR_SENSITIVITY,
                                &sensitivity_req);
        if (err) {
            camera_config_set_cancel(app_prv->camera);
        } else {
            camera_config_set_end(app_prv->camera);
        }
    }

    // TODO check error
    return 0;
}

// set manual color temperature [K], WB gains [ float x.xxx]
int cam_set_manual_color_temp_wb_gains(app_prv_t *app_prv, uint32 color_temp,
                                float gain_r, float gain_ge, float gain_go, float gain_b)
{
    int err;

    // AWB control mode AWB_MODE_OFF
    {
        static guzzi_camera3_dynamic_control_awb_mode_t awb_req;

        awb_req.v = GUZZI_CAMERA3_ENUM_CONTROL_AWB_MODE_OFF;

        camera_config_set_begin(app_prv->camera);
        err = camera_config_set(app_prv->camera,
                                GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AWB_MODE,
                                &awb_req);
        if (err) {
            camera_config_set_cancel(app_prv->camera);
        } else {
            camera_config_set_end(app_prv->camera);
        }

        mmsdbg(DL_PRINT, "\n################\n# AWB mode %d \n################", awb_req.v);

    }

    //  MAnual rgb gains
    {
        static guzzi_camera3_controls_color_correction_gains_t wb_gains_req;

        // lens density used as color temperature
        wb_gains_req.v[0] = gain_r;  // R
        wb_gains_req.v[1] = gain_ge; // G_even
        wb_gains_req.v[2] = gain_go; // G_odd
        wb_gains_req.v[3] = gain_b;  // B

        mmsdbg(DL_PRINT, "\n################\n# Color WB gains \n#   R      = %f\n#   G_even = %f\n#   G_odd  = %f\n#   R      = %f\n################",
                wb_gains_req.v[0], wb_gains_req.v[1], wb_gains_req.v[2], wb_gains_req.v[3]);
        camera_config_set_begin(app_prv->camera);
        err = camera_config_set(app_prv->camera,
                                GUZZI_CAMERA3_INDEX_CONTROLS_COLOR_CORRECTION_GAINS,
                                &wb_gains_req);
        if (err) {
            camera_config_set_cancel(app_prv->camera);
        } else {
            camera_config_set_end(app_prv->camera);
        }
    }

    //  Manual color temperature - LENS DENSITY
    {
        static guzzi_camera3_controls_lens_filter_density_t lens_density_req;

        // lens density used as color temperature
        lens_density_req.v = (float)color_temp;

        mmsdbg(DL_PRINT, "\n################\n# Color temperature %d \n################", lens_density_req.v);
        camera_config_set_begin(app_prv->camera);
        err = camera_config_set(app_prv->camera,
                                GUZZI_CAMERA3_INDEX_CONTROLS_LENS_FILTER_DENSITY,
                                &lens_density_req);
        if (err) {
            camera_config_set_cancel(app_prv->camera);
        } else {
            camera_config_set_end(app_prv->camera);
        }
    }

    // TODO check error
    return 0;
}

#define GAINS_DEVIDER 1000.0

#define EXP_MIN     1
#define EXP_MAX     10000000
#define GAIN_MIN    1000
#define GAIN_MAX    127000
#define CT_MIN      1000
#define CT_MAX      12000
#define WB_K_MIN    1000
#define WB_K_MAX    8000

int set_manual_params (app_prv_t *app_prv)
{
    extern int clp_man_exp;
    extern int clp_man_gain;

    // MANUAL EXPOSURE AND GAIN
    int set_exp_gain = 0;
    int err_exp_gain = 0;
    if (clp_man_exp != 0 && clp_man_gain != 0) {
        // set manual exposure and gain
        set_exp_gain = 1;
        if (clp_man_exp < EXP_MIN || clp_man_exp > EXP_MAX) {
            mmsdbg(DL_PRINT, "\nManual exposure out of range: [%d - %d]", EXP_MIN, EXP_MAX);
            set_exp_gain = 0;
            err_exp_gain = 1;
        }
        if (clp_man_gain < GAIN_MIN || clp_man_gain > GAIN_MAX) {
            mmsdbg(DL_PRINT, "\nManual sensor gain out of range: [%d - %d]", GAIN_MIN, GAIN_MAX);
            set_exp_gain = 0;
            err_exp_gain = 1;
        }
    } else {
        if (clp_man_exp != 0 || clp_man_gain != 0) {
            mmsdbg(DL_PRINT, "\nManual exposure and sensor gain must be set!");
            set_exp_gain = 0;
            err_exp_gain = 1;
        }
    }

    if (set_exp_gain != 0) {
        // set manual
        cam_set_manual_exposure_gain(app_prv, clp_man_exp, (float)clp_man_gain/GAINS_DEVIDER);
        mmsdbg(DL_PRINT, "\nSet manual exposure time and sensor gain!\n");
    } else {
        if (err_exp_gain != 0) {
            mmsdbg(DL_PRINT, "\nSet AE auto mode!\n");
        }
    }

    // MANUAL COLOR TEMPERATURE AND BALANCE
    extern int clp_man_ct;
    extern int clp_man_kr;
    extern int clp_man_kg;
    extern int clp_man_kb;

    int set_ct_gains = 0;
    int err_ct_gains = 0;

    if (       clp_man_ct != 0
            && clp_man_kr != 0
            && clp_man_kg != 0
            && clp_man_kb != 0
            ) {
        // set manual color temp and wb gains
        set_ct_gains = 1;

        if ( clp_man_ct < CT_MIN || clp_man_ct > CT_MAX) {
            mmsdbg(DL_PRINT, "\nManual color temperature out of range: [%d - %d]", CT_MIN, CT_MAX);
            set_ct_gains = 0;
            err_ct_gains = 1;
        }

        if (
                (clp_man_kr < WB_K_MIN || clp_man_kr > WB_K_MAX)
             || (clp_man_kg < WB_K_MIN || clp_man_kg > WB_K_MAX)
             || (clp_man_kr < WB_K_MIN || clp_man_kr > WB_K_MAX)
             ) {
            mmsdbg(DL_PRINT, "\nManual WB gains out of range: [%d - %d]", WB_K_MIN, WB_K_MAX);
            set_ct_gains = 0;
            err_ct_gains = 1;
        }
    } else {
        if (       clp_man_ct != 0
                || clp_man_kr != 0
                || clp_man_kg != 0
                || clp_man_kb != 0
                ) {
            mmsdbg(DL_PRINT, "\nManual color temperature and all WB gains must be set!");
            set_ct_gains = 0;
            err_ct_gains = 1;
        }
    }
    if (set_ct_gains != 0) {
        // set manual
        cam_set_manual_color_temp_wb_gains(app_prv, clp_man_ct,
                                        (float)clp_man_kr/GAINS_DEVIDER,
                                        (float)clp_man_kg/GAINS_DEVIDER,
                                        (float)clp_man_kg/GAINS_DEVIDER,
                                        (float)clp_man_kb/GAINS_DEVIDER
                                        );
        mmsdbg(DL_PRINT, "\nSet manual Color Temperature and WB gains!\n");
    } else {
        if (err_ct_gains != 0) {
            mmsdbg(DL_PRINT, "\nSet WB auto mode!\n");
        }
    }

    return 0;
}


app_prv_t cam_test_prv;

void cam_test(void)
{
    app_prv_t *app_prv;
    guzzi_nvm_reader_t *nvm_reader;
    nvm_info_t *nvm_info;
    int nvm_reder_number_of_cameras;
    int exit_flag = 0;
    guzzi_camera3_controls_request_id_t request_id;
    camera_create_params_t create_params;
    int err;

    guzzi_camera3_controls_stream_config_id_t id;
    guzzi_camera3_controls_stream_config_width_t width;
    guzzi_camera3_controls_stream_config_height_t height;
    guzzi_camera3_controls_scaler_crop_region_t crop_region;

#define SET(IDX,DATA) \
    do { \
        err = camera_config_set(app_prv->camera, IDX, DATA); \
        if (err) { \
            mmsdbg(DL_ERROR, "Failed to set camera stream config: " #IDX); \
            camera_config_set_cancel(app_prv->camera); \
            return; \
        } \
    } while (0)

    app_prv = &cam_test_prv;
    app_prv->stop = 0;



    nvm_reader = guzzi_nvm_reader_create();
    nvm_reder_number_of_cameras = guzzi_nvm_reader_get_number_of_cameras(
            nvm_reader
        );

    if(nvm_reder_number_of_cameras == 0) {
        guzzi_nvm_reader_destroy(nvm_reader);
        mmsdbg(
                DL_ERROR,
                "Guzzi NVM Reader: Can't detect a camera - number_of_cameras = %d",
            nvm_reder_number_of_cameras
            );
        return;
    }

    nvm_info = guzzi_nvm_reader_read(nvm_reader, 1);
    guzzi_nvm_reader_destroy(nvm_reader);
    mmsdbg(
            DL_PRINT,
            "Guzzi NVM Reader: number_of_cameras=%d",
            nvm_reder_number_of_cameras
        );
    osal_free(nvm_info);

    app_prv->cmd_sem = osal_sem_create(0);
    if (!app_prv->cmd_sem) {
        mmsdbg(DL_ERROR, "Failed to create camera app command semphore!");
        goto exit1;
    }

    create_params.camera_id = 0;
    create_params.mode = CAMERA_MODE_MOV;
    create_params.dtp_server = NULL;
    create_params.callback = callback;
    create_params.app_prv = app_prv;
    app_prv->camera = camera_create(&create_params);
    if (!app_prv->camera) {
        mmsdbg(DL_ERROR, "Failed to create camera!");
        goto exit2;
    }

    camera_config_get(app_prv->camera, GUZZI_CAMERA3_INDEX_CONTROLS_REQUEST_ID, &request_id);

    width.dim_size_1 = 1;
    width.v[0] = 856; // 4208; //
    height.dim_size_1 = 1;
    height.v[0] = 480; // 3120; //

    { // TODO: Remove me
    extern int clp_resolution;
        if (clp_resolution == 1) {
            width.v[0] = 4208;
            height.v[0] =  3120;
        }
    }


    width.v[0]  /= 2;
    height.v[0] /= 2;

    id.dim_size_1 = 1;
    id.v[0] = 1;

    crop_region.dim_size_1 = 4;
    crop_region.v[0] = 0; // start X
    crop_region.v[1] = 0; // start Y
    crop_region.v[2] = width.v[0]; // Width
    crop_region.v[3] = height.v[0]; // Height

    camera_config_set_begin(app_prv->camera);

    SET(GUZZI_CAMERA3_INDEX_CONTROLS_REQUEST_ID, &request_id);
    SET(GUZZI_CAMERA3_INDEX_CONTROLS_STREAM_CONFIG_ID    , &id    );
    SET(GUZZI_CAMERA3_INDEX_CONTROLS_STREAM_CONFIG_WIDTH , &width );
    SET(GUZZI_CAMERA3_INDEX_CONTROLS_STREAM_CONFIG_HEIGHT, &height);
    SET(GUZZI_CAMERA3_INDEX_CONTROLS_SCALER_CROP_REGION, &crop_region);

    camera_config_set_end(app_prv->camera);

    // Target FPS - 30/10
    {
        static guzzi_camera3_controls_control_ae_target_fps_range_t target_fps_req;

        target_fps_req.dim_size_1 = 2;
        target_fps_req.v[0] =10;
        target_fps_req.v[1] =15;

        mmsdbg(DL_PRINT, "\n################\n# Target FPS %d / %d #\n################", target_fps_req.v[0], target_fps_req.v[1]);
        camera_config_set_begin(app_prv->camera);
        err = camera_config_set(app_prv->camera,
                                GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AE_TARGET_FPS_RANGE,
                                &target_fps_req);
        if (err) {
            camera_config_set_cancel(app_prv->camera);
        } else {
            camera_config_set_end(app_prv->camera);
        }
    }

    // ===========================================================

    set_manual_params(app_prv);

    // ===========================================================

    err = camera_start(app_prv->camera);
    if (err) {
        mmsdbg(DL_ERROR, "Failed to start camera!");
        goto exit3;
    }
    osal_sem_wait(app_prv->cmd_sem);
    if (app_prv->start_err) {
        mmsdbg(DL_ERROR, "Failed to start camera!");
        goto exit3;
    }


    camera_process(app_prv->camera, 0, NULL);
    camera_process(app_prv->camera, 0, NULL);

    extern int clp_capt_time;
    if (clp_capt_time == 0) {

        /**********************************
         * Wait
         **********************************/
        while (!exit_flag){
            int ret;
#if !defined(__sparc)
            ret = getchar();
#else
            osal_sem_wait(app_prv->cmd_sem);
            mmsdbg(DL_PRINT, "\n###################\n# Sem Post #\n###################");
            ret = 0;
#endif  // !defined(__sparc)
            switch (ret)
            {
            case 'c':
                mmsdbg(DL_PRINT, "\n###################\n# Zsl request #\n###################");
                {
                    guzzi_camera3_controls_control_capture_intent_t intent_req;

                    camera_flush(app_prv->camera);
                    osal_sem_wait(app_prv->cmd_sem);
                    app_prv->capture_in_progress = 1;

                    intent_req.v = GUZZI_CAMERA3_ENUM_CONTROL_CAPTURE_INTENT_ZERO_SHUTTER_LAG;

                    camera_config_set_begin(app_prv->camera);
                    err = camera_config_set(app_prv->camera,
                                            GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_CAPTURE_INTENT,
                                            &intent_req);
                    if (err) {
                        camera_config_set_cancel(app_prv->camera);
                    } else {
                        camera_config_set_end(app_prv->camera);
                    }

                    camera_process(app_prv->camera, 0, NULL);
                    osal_sem_wait(app_prv->cmd_sem);
                    app_prv->capture_in_progress = 0;

                    intent_req.v = GUZZI_CAMERA3_ENUM_CONTROL_CAPTURE_INTENT_PREVIEW;

                    camera_config_set_begin(app_prv->camera);
                    err = camera_config_set(app_prv->camera,
                                            GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_CAPTURE_INTENT,
                                            &intent_req);
                    if (err) {
                        camera_config_set_cancel(app_prv->camera);
                    } else {
                        camera_config_set_end(app_prv->camera);
                    }

                    app_prv->capture_in_progress = 0;
                    camera_process(app_prv->camera, 0, NULL);
                    camera_process(app_prv->camera, 0, NULL);
                }
                break;
            case 'C':
                mmsdbg(DL_PRINT, "\n###################\n# Capture request #\n###################");
                {
                    guzzi_camera3_controls_control_capture_intent_t intent_req;

                    camera_flush(app_prv->camera);
                    osal_sem_wait(app_prv->cmd_sem);
                    app_prv->capture_in_progress = 1;

                    intent_req.v = GUZZI_CAMERA3_ENUM_CONTROL_CAPTURE_INTENT_STILL_CAPTURE;

                    camera_config_set_begin(app_prv->camera);
                    err = camera_config_set(app_prv->camera,
                                            GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_CAPTURE_INTENT,
                                            &intent_req);
                    if (err) {
                        camera_config_set_cancel(app_prv->camera);
                    } else {
                        camera_config_set_end(app_prv->camera);
                    }

                    camera_process(app_prv->camera, 0, NULL);
                    osal_sem_wait(app_prv->cmd_sem);

                    intent_req.v = GUZZI_CAMERA3_ENUM_CONTROL_CAPTURE_INTENT_PREVIEW;

                    camera_config_set_begin(app_prv->camera);
                    err = camera_config_set(app_prv->camera,
                                            GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_CAPTURE_INTENT,
                                            &intent_req);
                    if (err) {
                        camera_config_set_cancel(app_prv->camera);
                    } else {
                        camera_config_set_end(app_prv->camera);
                    }

                    app_prv->capture_in_progress = 0;
                    camera_process(app_prv->camera, 0, NULL);
                    camera_process(app_prv->camera, 0, NULL);
                }
                break;
            case 'q':
            case 'Q':
                mmsdbg(DL_PRINT, "\n################\n# Exit request #\n################");
                exit_flag = 1;
                break;

            case 'w':
            case 'W':
            {
                static guzzi_camera3_dynamic_control_awb_mode_t awb_req;

                camera_config_set_begin(app_prv->camera);
                err = camera_config_set(app_prv->camera,
                                        GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AWB_MODE,
                                        &awb_req);
                if (err) {
                    camera_config_set_cancel(app_prv->camera);
                } else {
                    camera_config_set_end(app_prv->camera);
                }

                mmsdbg(DL_PRINT, "\n################\n# AWB mode %d #\n################", awb_req.v);

                awb_req.v++;
                if (awb_req.v > GUZZI_CAMERA3_ENUM_CONTROL_AWB_MODE_SHADE) {
                    awb_req.v = GUZZI_CAMERA3_ENUM_CONTROL_AWB_MODE_OFF;
                }
            }
                break;

            case 'f':
            {
                static guzzi_camera3_dynamic_control_af_trigger_id_t af_trigger_req;

                af_trigger_req.v = GUZZI_CAMERA3_ENUM_CONTROL_AF_TRIGGER_START;
                camera_config_set_begin(app_prv->camera);
                err = camera_config_set(app_prv->camera,
                                        GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AF_TRIGGER,
                                        &af_trigger_req);
                if (err) {
                    camera_config_set_cancel(app_prv->camera);
                } else {
                    camera_config_set_end(app_prv->camera);
                }
                mmsdbg(DL_PRINT, "\n################\n# AF trigger %d #\n################", af_trigger_req.v);
            }
            break;

            case 'F':
            {
                static guzzi_camera3_dynamic_control_af_mode_t af_req;

                camera_config_set_begin(app_prv->camera);
                err = camera_config_set(app_prv->camera,
                                        GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AF_MODE,
                                        &af_req);
                if (err) {
                    camera_config_set_cancel(app_prv->camera);
                } else {
                    camera_config_set_end(app_prv->camera);
                }
                mmsdbg(DL_PRINT, "\n################\n# AF mode %d #\n################", af_req.v);
                af_req.v++;
                if (af_req.v > GUZZI_CAMERA3_ENUM_CONTROL_AF_MODE_EDOF) {
                    af_req.v = GUZZI_CAMERA3_ENUM_CONTROL_AF_MODE_OFF;
                }
            }
                break;

            case 't':
            case 'T':
            {
                static guzzi_camera3_dynamic_control_af_trigger_id_t af_trigger_req;

                camera_config_set_begin(app_prv->camera);
                err = camera_config_set(app_prv->camera,
                                        GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AF_TRIGGER,
                                        &af_trigger_req);
                if (err) {
                    camera_config_set_cancel(app_prv->camera);
                } else {
                    camera_config_set_end(app_prv->camera);
                }
                mmsdbg(DL_PRINT, "\n################\n# AF trigger mode %d #\n################", af_trigger_req.v);
                af_trigger_req.v++;
                if (af_trigger_req.v > GUZZI_CAMERA3_ENUM_CONTROL_AF_TRIGGER_CANCEL) {
                    af_trigger_req.v = GUZZI_CAMERA3_ENUM_CONTROL_AF_TRIGGER_IDLE;
                }
            }
            break;

            case 'l':
            {
                static guzzi_camera3_controls_lens_focal_length_t foc_len;
                static guzzi_camera3_dynamic_control_af_mode_t af_req;

                af_req.v = GUZZI_CAMERA3_ENUM_CONTROL_AF_MODE_OFF;
                camera_config_set_begin(app_prv->camera);
                err = camera_config_set(app_prv->camera,
                                        GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AF_MODE,
                                        &af_req);
                if (err) {
                    camera_config_set_cancel(app_prv->camera);
                } else {
                    camera_config_set_end(app_prv->camera);
                }

                camera_config_set_begin(app_prv->camera);
                err = camera_config_set(app_prv->camera,
                                        GUZZI_CAMERA3_INDEX_CONTROLS_LENS_FOCAL_LENGTH,
                                        &foc_len);
                if (err) {
                    camera_config_set_cancel(app_prv->camera);
                } else {
                    camera_config_set_end(app_prv->camera);
                }

                mmsdbg(DL_PRINT, "\n################\n# Lens  %f #\n################", foc_len.v);
                foc_len.v += 0.1;
                if (foc_len.v > 1.0) {
                   foc_len.v = 0.0;
                }
            }
                break;
            case 'r':
            case 'R':
            {
                static guzzi_camera3_dynamic_sensor_frame_duration_t frame_duration_req;

                frame_duration_req.v += 50000000;
                if (frame_duration_req.v > 500000000) {
                    frame_duration_req.v = 33333333;
                }

                mmsdbg(DL_PRINT, "\n################\n# Frame duration %d ns #\n################", frame_duration_req.v);
                camera_config_set_begin(app_prv->camera);
                err = camera_config_set(app_prv->camera,
                                        GUZZI_CAMERA3_INDEX_CONTROLS_SENSOR_FRAME_DURATION,
                                        &frame_duration_req);
                if (err) {
                    camera_config_set_cancel(app_prv->camera);
                } else {
                    camera_config_set_end(app_prv->camera);
                }
            }
                break;

            case 's':
            case 'S':
            {
                static guzzi_camera3_dynamic_sensor_sensitivity_t sensitivity_req;

                sensitivity_req.v += 100;
                if (sensitivity_req.v > 800) {
                    sensitivity_req.v = 100;
                }

                mmsdbg(DL_PRINT, "\n################\n# Sensitivity %d #\n################", sensitivity_req.v);
                camera_config_set_begin(app_prv->camera);
                err = camera_config_set(app_prv->camera,
                                        GUZZI_CAMERA3_INDEX_CONTROLS_SENSOR_SENSITIVITY,
                                        &sensitivity_req);
                if (err) {
                    camera_config_set_cancel(app_prv->camera);
                } else {
                    camera_config_set_end(app_prv->camera);
                }
            }
                break;

            case 'd':
            case 'D':
            {
                #define MS (1000 * 1000)
                static guzzi_camera3_dynamic_sensor_exposure_time_t exposure_time_req;

                exposure_time_req.v += (5 * MS);
                if (exposure_time_req.v > (33 * MS)) {
                    exposure_time_req.v = 5 * MS;
                }

                mmsdbg(DL_PRINT, "\n################\n# Exposure %d ns#\n################", exposure_time_req.v);

                camera_config_set_begin(app_prv->camera);
                err = camera_config_set(app_prv->camera,
                                        GUZZI_CAMERA3_INDEX_CONTROLS_SENSOR_EXPOSURE_TIME,
                                        &exposure_time_req);
                if (err) {
                    camera_config_set_cancel(app_prv->camera);
                } else {
                    camera_config_set_end(app_prv->camera);
                }
            }
                break;

            case 'm':
            case 'M':
            {
                static guzzi_camera3_dynamic_control_mode_t mode_req;

                camera_config_set_begin(app_prv->camera);
                err = camera_config_set(app_prv->camera,
                                        GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_MODE,
                                        &mode_req);
                if (err) {
                    camera_config_set_cancel(app_prv->camera);
                } else {
                    camera_config_set_end(app_prv->camera);
                }
                mmsdbg(DL_PRINT, "\n################\n# 3A mode %d #\n################", mode_req.v);
                mode_req.v++;
                if (mode_req.v > GUZZI_CAMERA3_ENUM_CONTROL_MODE_USE_SCENE_MODE) {
                    mode_req.v = GUZZI_CAMERA3_ENUM_CONTROL_MODE_OFF;
                }
            }
                break;

            case 'e':
            case 'E':
            {
                static guzzi_camera3_controls_control_ae_mode_t ae_req;

                camera_config_set_begin(app_prv->camera);
                err = camera_config_set(app_prv->camera,
                                        GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AE_MODE,
                                        &ae_req);
                if (err) {
                    camera_config_set_cancel(app_prv->camera);
                } else {
                    camera_config_set_end(app_prv->camera);
                }
                mmsdbg(DL_PRINT, "\n################\n# AE mode %d #\n################", ae_req.v);
                ae_req.v++;
                if (ae_req.v > GUZZI_CAMERA3_ENUM_CONTROL_AE_MODE_ON_AUTO_FLASH_REDEYE) {
                    ae_req.v = GUZZI_CAMERA3_ENUM_CONTROL_AE_MODE_OFF;
                }
            }
                break;

            case '3':
            {
                static guzzi_camera3_controls_control_effect_mode_t eff_req;

                camera_config_set_begin(app_prv->camera);
                err = camera_config_set(app_prv->camera,
                                        GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_EFFECT_MODE,
                                        &eff_req);
                if (err) {
                    camera_config_set_cancel(app_prv->camera);
                } else {
                    camera_config_set_end(app_prv->camera);
                }
                mmsdbg(DL_PRINT, "\n################\n# Eff mode %d #\n################", eff_req.v);

                eff_req.v++;
                if (eff_req.v > GUZZI_CAMERA3_ENUM_CONTROL_EFFECT_MODE_AQUA) {
                    eff_req.v = GUZZI_CAMERA3_ENUM_CONTROL_EFFECT_MODE_OFF;
                }
            }
                break;
            case 'b':
            {
                char *s[] = {
                    " OFF",
                    "50HZ",
                    "60HZ",
                    "AUTO",
                };

                static guzzi_camera3_controls_control_ae_antibanding_mode_t ae_antibanding_req;

                camera_config_set_begin(app_prv->camera);
                err = camera_config_set(app_prv->camera,
                                        GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AE_ANTIBANDING_MODE,
                                        &ae_antibanding_req);
                if (err) {
                    camera_config_set_cancel(app_prv->camera);
                } else {
                    camera_config_set_end(app_prv->camera);
                }
                mmsdbg(DL_PRINT, "\n################\n# AFD mode %d %s #\n################",
                            ae_antibanding_req.v,
                            s[ae_antibanding_req.v]);
                ae_antibanding_req.v++;
                if (ae_antibanding_req.v > GUZZI_CAMERA3_ENUM_CONTROL_AE_ANTIBANDING_MODE_AUTO) {
                    ae_antibanding_req.v = GUZZI_CAMERA3_ENUM_CONTROL_AE_ANTIBANDING_MODE_OFF;
                }
            }
                break;
            }
        }
    } else {
        osal_sleep(clp_capt_time);

        // capture image
        mmsdbg(DL_PRINT, "\n###################\n# Zsl request #\n###################");
        {
            guzzi_camera3_controls_control_capture_intent_t intent_req;

            camera_flush(app_prv->camera);
            osal_sem_wait(app_prv->cmd_sem);
            app_prv->capture_in_progress = 1;

            intent_req.v = GUZZI_CAMERA3_ENUM_CONTROL_CAPTURE_INTENT_ZERO_SHUTTER_LAG;

            camera_config_set_begin(app_prv->camera);
            err = camera_config_set(app_prv->camera,
                                    GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_CAPTURE_INTENT,
                                    &intent_req);
            if (err) {
                camera_config_set_cancel(app_prv->camera);
            } else {
                camera_config_set_end(app_prv->camera);
            }

            camera_process(app_prv->camera, 0, NULL);
            osal_sem_wait(app_prv->cmd_sem);
            app_prv->capture_in_progress = 0;

            intent_req.v = GUZZI_CAMERA3_ENUM_CONTROL_CAPTURE_INTENT_PREVIEW;

            camera_config_set_begin(app_prv->camera);
            err = camera_config_set(app_prv->camera,
                                    GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_CAPTURE_INTENT,
                                    &intent_req);
            if (err) {
                camera_config_set_cancel(app_prv->camera);
            } else {
                camera_config_set_end(app_prv->camera);
            }

            app_prv->capture_in_progress = 0;
            camera_process(app_prv->camera, 0, NULL);
            camera_process(app_prv->camera, 0, NULL);
        }
        // No need sleep
        //osal_sleep(5);
    }
    app_prv->stop = 1;

    camera_flush(app_prv->camera);
    osal_sem_wait(app_prv->cmd_sem);

    camera_stop(app_prv->camera);
    osal_sem_wait(app_prv->cmd_sem);

exit3:
    camera_destroy(app_prv->camera);
exit2:
    osal_sem_destroy(app_prv->cmd_sem);
exit1:
    return;
}

