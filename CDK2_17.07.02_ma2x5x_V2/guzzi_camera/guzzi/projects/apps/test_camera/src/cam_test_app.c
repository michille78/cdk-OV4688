/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file cam_test_app.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <stdio.h>
#include <osal/osal_stdlib.h>
#include <platform/inc/platform.h>
#include <utils/mms_debug.h>
#include <version_info.h>
#include <guzzi_event/include/guzzi_event.h>
#include <guzzi_event_global/include/guzzi_event_global.h>
#include "dtp/dtp_server_defs.h"

void guzzi_camera3_capture_result__x11_delete(void);
void guzzi_camera3_capture_result__x11_init(void);

void cam_test(void);

int clp_init(void *base_register, void *end_register);
int clp_parser(int argc, char **argv);

FILE *profile_file;

dtp_server_hndl_t  dtp_srv_hndl;

extern uint8_t ext_dtp_database[];
extern uint8_t ext_dtp_database_end[];

mmsdbg_define_variable(
        vdl_camera_test_app,
        DL_DEFAULT,
        0,
        "vdl_camera_test_app",
        "Camera test application."
    );
#define MMSDEBUGLEVEL mmsdbg_use_variable(vdl_camera_test_app)

void profile_ready_cb(profile_t *profile, void *prv, void *buffer, unsigned int buffer_size)
{
    fwrite(buffer, buffer_size, 1, profile_file);
    PROFILE_RELEASE_READY(buffer);
}

extern int __start_mmsdbgvarlist;
extern int __stop_mmsdbgvarlist;
int main(int argc, char **argv)
{
    int                         err;
    unsigned char               *dtp_buffer = NULL;

    version_info_init();

    /* Test Version Add and Get */
    err = version_info_cat(
            "{\n"
            "	\"id\": \"CamTestApp - version_info_cat()\",\n"
            "},\n"
        );
    if (err) {
        printf("Fialed to add test version information!\n");
    }
    err = version_info_printf(
            "{\n"
            "	\"id\": \"CamTestApp - version_info_printf()\",\n"
            "	\"int\": %d,\n"
            "},\n",
            1234321
        );
    if (err) {
        printf("Fialed to add test version information!\n");
    }
    printf("%s", version_info_get());

    clp_init(&__start_mmsdbgvarlist, &__stop_mmsdbgvarlist);
    err = clp_parser(argc, argv);
    if (err) {
        goto exit0;
    }

    err = osal_init();
    if (err) {
        mmsdbg(DL_ERROR, "OSAL init failed!");
    }

    dtpsrv_create(&dtp_srv_hndl);
    dtpsrv_import_db(dtp_srv_hndl, ext_dtp_database, (ext_dtp_database_end - ext_dtp_database));

    // DTP data base
    {
        int dtp_file_length;
#define DTP_DATABASE_FILE_NAME  "database.bin"
        extern char *dtp_file_name;
        
        if (!dtp_file_name) {
            dtp_file_name = DTP_DATABASE_FILE_NAME;
        }
        
        FILE* dtp_db_fp = fopen(dtp_file_name, "rb");
        if(!dtp_db_fp){
            mmsdbg(DL_WARNING, "ERROR opening input DTP databse file %s!", dtp_file_name);
            goto exit_dtp_err;
        }
        
        fseek(dtp_db_fp, 0L, SEEK_END);
        if ((dtp_file_length = ftell(dtp_db_fp)) == 0) {
            mmsdbg(DL_ERROR, "Zero length DTP databse file %s!", dtp_file_name);
            goto exit_dtp_err;
        }
        
        fseek(dtp_db_fp, 0L, SEEK_SET);
            
        if ((dtp_buffer = osal_malloc(dtp_file_length)) == NULL) {
            mmsdbg(DL_ERROR, "Can't allocate memory for DTP database!");
            goto exit_dtp_err;
        }
        
        if (fread(dtp_buffer, 1, dtp_file_length, dtp_db_fp) != dtp_file_length) {
            mmsdbg(DL_ERROR, "ERROR reading DTP database file %s!", dtp_file_name);
            goto exit_dtp_err;
        }
        
        if (dtpsrv_import_db(dtp_srv_hndl, dtp_buffer, dtp_file_length) != 0) {
            mmsdbg(DL_ERROR, "ERROR import DTP database from file %s!", dtp_file_name);
            goto exit_dtp_err;
        }
        mmsdbg(DL_WARNING, "Using DTP database file %s!", dtp_file_name);
        
exit_dtp_err:
        if(dtp_db_fp)
            fclose(dtp_db_fp);
    }

    profile_file = fopen("prof_log.bin", "wb");
    if (!profile_file) {
        mmsdbg(DL_ERROR, "Failed to open profile file!");
    }

    PROFILE_INIT(256, 16, profile_ready_cb, NULL);

    err = guzzi_platform_init();
    if (err) {
        mmsdbg(DL_ERROR, "Platform init failed!");
    }

    guzzi_event_global_ctreate();

    guzzi_camera3_capture_result__x11_init();

    cam_test();
    mmsdbg(DL_PRINT, "cam_test() done.");

    guzzi_camera3_capture_result__x11_delete();

    guzzi_event_global_destroy();

    err = guzzi_platform_deinit();
    if (err) {
        mmsdbg(DL_ERROR, "Platform deinit failed!");
    }

    PROFILE_DUMP();
    PROFILE_DESTROY();

    fclose(profile_file);

    dtpsrv_destroy(dtp_srv_hndl);

    if (dtp_buffer != NULL) {
        osal_free(dtp_buffer);
    }

    osal_exit();

exit0:
    return 0;
}

