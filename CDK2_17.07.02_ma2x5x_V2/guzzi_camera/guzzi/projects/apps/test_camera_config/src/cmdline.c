/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file cmdline.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <osal/osal_stdio.h>

#include <utils/mms_debug.h>

#include <utils/linux/cmdline_parser.h>
#include <utils/linux/cmdline_debug.h>

mmsdbg_define_variable(vdl_cmdline, DL_DEFAULT, 0,
    "cmdline", "Example command line parser");
#define MMSDEBUGLEVEL   vdl_cmdline

int clp_resolution     = -1;
int clp_display_scale   = -1;
int clp_display_vmirror = 0;
int clp_focus_select   = 2;
int clp_focus_scale    = 0;
int clp_h3a_scale      = 0;
char *dtp_file_name    = NULL;
int clp_camera_select  = 0;
int clp_raw_bls_select = 1;

// manual parameters
int clp_man_exp = 0;
int clp_man_gain = 0;
int clp_man_ct = 0;
int clp_man_kr = 0;
int clp_man_kg = 0;
int clp_man_kb = 0;
// time to capture
int clp_capt_time = 0;
// file_name_item
char *file_name_item = NULL;
// Input raw file name
char *in_file_name = NULL;
// external shell isp processing script
char *script_name = NULL;

#define CLP_FILENAME            "cam_test_app"
#define CLP_ENVNAME             "MMSDBG_DTPTEST"

static int clphnd_unknown(int val, char *opt, char *arg, int index)
{
    osal_printf("Unknown option: %s\n", opt);

    return CMDLINE_ERROR_UNKNOW;
}

static int clphnd_raw_bls_select (int val, char *opt, char *arg, int index)
{
    char *bls[] = {
        "off",
        "on"
    };

    clp_raw_bls_select = atoi(arg);
    if ((clp_raw_bls_select < 0) || (clp_raw_bls_select > 1)) {
        clp_raw_bls_select = 0;
    }
    printf("raw black level subtract : %s\n", bls[clp_raw_bls_select]);

    return CMDLINE_NOERROR;
}

static int clphnd_camera_select (int val, char *opt, char *arg, int index)
{
    char *camera[] = {
        "AR1330",
        "IMX214",
        "HIGHLANDS(IMX214)",
        "IMX208",
        "OV13860",
        "OV13860_BW"
    };

    int nmb_cameras = sizeof(camera)/sizeof(*camera);

    clp_camera_select = atoi(arg);
    if (clp_camera_select < nmb_cameras) {
        printf("Camera select id: %d - %s\n", clp_camera_select, camera[clp_camera_select]);
    } else {
        printf("Camera select id: %d - %s\n", clp_camera_select, "Unknown camera");
    }

    return CMDLINE_NOERROR;
}

static int clphnd_camera_dtp(int val, char *opt, char *arg, int index)
{
    dtp_file_name = arg;
    printf("Camera dtp file name: %s\n", dtp_file_name);

    return CMDLINE_NOERROR;
}

static int clphnd_camera_resolution(int val, char *opt, char *arg, int index)
{
    clp_resolution = atoi(arg);
    printf("Camera resolution: %d\n", clp_resolution);

    return CMDLINE_NOERROR;
}

static int clphnd_display_resolution(int val, char *opt, char *arg, int index)
{
    clp_display_scale = atoi(arg);
    printf("Camera display scale: %d\n", clp_display_scale);

    return CMDLINE_NOERROR;
}

static int clphnd_display_vmirror(int val, char *opt, char *arg, int index)
{
    clp_display_vmirror = atoi(arg);
    printf("Camera display vertical mirror: %d\n", clp_display_vmirror);

    return CMDLINE_NOERROR;
}

static int clphnd_focus_select(int val, char *opt, char *arg, int index)
{
    clp_focus_select = atoi(arg);
    printf("Focus stats type to display: %d\n", clp_focus_scale);

    return CMDLINE_NOERROR;
}
static int clphnd_focus_scale(int val, char *opt, char *arg, int index)
{
    clp_focus_scale = atoi(arg);
    printf("Focus stats display/scale: %d\n", clp_focus_select);

    return CMDLINE_NOERROR;
}

static int clphnd_h3a_scale(int val, char *opt, char *arg, int index)
{
    clp_h3a_scale = atoi(arg);
    printf("AEWB stats display/scale: %d\n", clp_h3a_scale);

    return CMDLINE_NOERROR;
}

static int clphnd_man_exp(int val, char *opt, char *arg, int index)
{
    clp_man_exp = atoi(arg);
    printf("Manual exposure time: %d [us]\n", clp_man_exp);

    return CMDLINE_NOERROR;
}

static int clphnd_man_gain(int val, char *opt, char *arg, int index)
{
    clp_man_gain = atoi(arg);
    printf("Manual sensor gain: %d /1000\n", clp_man_gain);

    return CMDLINE_NOERROR;
}

static int clphnd_man_ct(int val, char *opt, char *arg, int index)
{
    clp_man_ct = atoi(arg);
    printf("Manual color temperature: %d [K]\n", clp_man_ct);

    return CMDLINE_NOERROR;
}

static int clphnd_man_kr(int val, char *opt, char *arg, int index)
{
    clp_man_kr = atoi(arg);
    printf("Manual gain R: %d /1000\n", clp_man_kr);

    return CMDLINE_NOERROR;
}

static int clphnd_man_kg(int val, char *opt, char *arg, int index)
{
    clp_man_kg = atoi(arg);
    printf("Manual gain G: %d /1000\n", clp_man_kg);

    return CMDLINE_NOERROR;
}

static int clphnd_man_kb(int val, char *opt, char *arg, int index)
{
    clp_man_kb = atoi(arg);
    printf("Manual gain B: %d /1000\n", clp_man_kb);

    return CMDLINE_NOERROR;
}

static int clphnd_capt_time(int val, char *opt, char *arg, int index)
{
    clp_capt_time = atoi(arg);
    printf("Time to capture [s] : %d\n", clp_capt_time);

    return CMDLINE_NOERROR;
}

static int clphnd_file_name_item(int val, char *opt, char *arg, int index)
{
	file_name_item = arg;
    printf("First item of out raw file name: %s\n", file_name_item);

    return CMDLINE_NOERROR;
}

static int clphnd_in_file_name(int val, char *opt, char *arg, int index)
{
    in_file_name = arg;
    printf("Input raw file name: %s\n", in_file_name);

    return CMDLINE_NOERROR;
}

static int clphnd_script_name(int val, char *opt, char *arg, int index)
{
	script_name = arg;
    printf("ISP processing script: %s\n", script_name);

    return CMDLINE_NOERROR;
}
/*
static int clphnd_show(int val, char *opt, char *arg, int index)
{
    clp_flags |= CLP_FLAG_SHOW;
    printf("Set Show\n");

    return CMDLINE_NOERROR;
}

static int clphnd_show_all(int val, char *opt, char *arg, int index)
{
    clp_flags |= CLP_FLAG_SHOW_ALL;
    printf("Set Show All\n");

    return CMDLINE_NOERROR;
}
*/

int clp_init(void *base_register, void *end_register)
{
    int err = 0;
    int count = 1000;

    cmdline_dbg_initialize(base_register, end_register);

    cmdline_help_line(CMDLINE_FLAG_HELP_LEVEL9, "Common:");

    cmdline_set_unknow(clphnd_unknown);

    cmdline_add_option(CMDLINE_FLAG_OPT_OPTIONAL|
        CMDLINE_FLAG_VAL_MANDATORY|CMDLINE_FLAG_HELP_LEVEL0,
        'r', "resolution", "vl", count++, NULL, clphnd_camera_resolution,
        " -r | --resolution        Resolution 0 - low 1 - high\n", NULL);

    cmdline_add_option(CMDLINE_FLAG_OPT_OPTIONAL|
        CMDLINE_FLAG_VAL_MANDATORY|CMDLINE_FLAG_HELP_LEVEL0,
        'd', "dscale ", "vl", count++, NULL, clphnd_display_resolution,
        " -d | --dscale            Main image scale factor 1 - /1 2 - /2 4 - /4\n", NULL);

    cmdline_add_option(CMDLINE_FLAG_OPT_OPTIONAL|
        CMDLINE_FLAG_VAL_MANDATORY|CMDLINE_FLAG_HELP_LEVEL0,
        'v', "vmirror ", "vl", count++, NULL, clphnd_display_vmirror,
        " -v | --vmirror            Main image vertical mirror 0 - disable 1 - enable\n", NULL);

    cmdline_add_option(CMDLINE_FLAG_OPT_OPTIONAL|
        CMDLINE_FLAG_VAL_MANDATORY|CMDLINE_FLAG_HELP_LEVEL0,
        'f', "fscale ", "vl", count++, NULL, clphnd_focus_scale,
        " -f | --fscale            AF stats scale factor 1 - x1 2 - x2 4 - x4\n", NULL);

    cmdline_add_option(CMDLINE_FLAG_OPT_OPTIONAL|
        CMDLINE_FLAG_VAL_MANDATORY|CMDLINE_FLAG_HELP_LEVEL0,
        't', "tdisp ", "vl", count++, NULL, clphnd_focus_select,
        " -t | --fdisp             AF stats type select [1 - 6]\n", NULL);

    cmdline_add_option(CMDLINE_FLAG_OPT_OPTIONAL|
        CMDLINE_FLAG_VAL_MANDATORY|CMDLINE_FLAG_HELP_LEVEL0,
        's', "sscale ", "vl", count++, NULL, clphnd_h3a_scale,
        " -s | --sscale            AEWB stats scale factor 1 - x1 2 - x2 4 - x4\n", NULL);

    cmdline_add_option(CMDLINE_FLAG_OPT_OPTIONAL|
        CMDLINE_FLAG_VAL_MANDATORY|CMDLINE_FLAG_HELP_LEVEL0,
        'x', "dtp", "vl", count++, NULL, clphnd_camera_dtp,
        " -x | --dtp               dtp file name\n", NULL);

    cmdline_add_option(CMDLINE_FLAG_OPT_OPTIONAL|
        CMDLINE_FLAG_VAL_MANDATORY|CMDLINE_FLAG_HELP_LEVEL0,
        'c', "camera_select", "vl", count++, NULL, clphnd_camera_select,
        " -c | --camera_select     0 - AR1330, 1 - IMX214, 2 - HIGHLANDS(IMX214)", NULL);

    cmdline_add_option(CMDLINE_FLAG_OPT_OPTIONAL|
        CMDLINE_FLAG_VAL_MANDATORY|CMDLINE_FLAG_HELP_LEVEL0,
        'b', "bls_select", "vl", count++, NULL, clphnd_raw_bls_select,
        " -b | --raw_bls_select    0 - raw bls: off, 1 - raw bls: on", NULL);

    cmdline_add_option(CMDLINE_FLAG_OPT_OPTIONAL|
        CMDLINE_FLAG_VAL_MANDATORY|CMDLINE_FLAG_HELP_LEVEL0,
        0, "man_exp", "vl", count++, NULL, clphnd_man_exp,
        "      --man_exp           manual exp. time [us]      [1 - 1000000]", NULL);

    cmdline_add_option(CMDLINE_FLAG_OPT_OPTIONAL|
        CMDLINE_FLAG_VAL_MANDATORY|CMDLINE_FLAG_HELP_LEVEL0,
        0, "man_gain", "vl", count++, NULL, clphnd_man_gain,
        "      --man_gain          manual sensor gain x/1000  [1000 - 127000]", NULL);

    cmdline_add_option(CMDLINE_FLAG_OPT_OPTIONAL|
        CMDLINE_FLAG_VAL_MANDATORY|CMDLINE_FLAG_HELP_LEVEL0,
        0, "man_ct", "vl", count++, NULL, clphnd_man_ct,
        "      --man_ct            manual color temp. [K]     [1000 - 12000]", NULL);

    cmdline_add_option(CMDLINE_FLAG_OPT_OPTIONAL|
        CMDLINE_FLAG_VAL_MANDATORY|CMDLINE_FLAG_HELP_LEVEL0,
        0, "man_kr", "vl", count++, NULL, clphnd_man_kr,
        "      --man_kr            manual gain R /1000        [1000 - 8000]", NULL);

    cmdline_add_option(CMDLINE_FLAG_OPT_OPTIONAL|
        CMDLINE_FLAG_VAL_MANDATORY|CMDLINE_FLAG_HELP_LEVEL0,
        0, "man_kg", "vl", count++, NULL, clphnd_man_kg,
        "      --man_kg            manual gain G /1000        [1000 - 8000]", NULL);

    cmdline_add_option(CMDLINE_FLAG_OPT_OPTIONAL|
        CMDLINE_FLAG_VAL_MANDATORY|CMDLINE_FLAG_HELP_LEVEL0,
        0, "man_kb", "vl", count++, NULL, clphnd_man_kb,
        "      --man_kb            manual gain B /1000   [1000 - 8000]", NULL);

    cmdline_add_option(CMDLINE_FLAG_OPT_OPTIONAL|
        CMDLINE_FLAG_VAL_MANDATORY|CMDLINE_FLAG_HELP_LEVEL0,
        0, "capt_time", "vl", count++, NULL, clphnd_capt_time,
        "      --capt_time         auto capture time [s]. The camera starts, do capture after 'capt_time' and exits. If capt_time == 0 - normal mode", NULL);

    cmdline_add_option(CMDLINE_FLAG_OPT_OPTIONAL|
        CMDLINE_FLAG_VAL_MANDATORY|CMDLINE_FLAG_HELP_LEVEL0,
        0, "fnamer", "vl", count++, NULL, clphnd_file_name_item,
        "      --fnamer            first item of raw file name\n", NULL);

    cmdline_add_option(CMDLINE_FLAG_OPT_OPTIONAL|
        CMDLINE_FLAG_VAL_MANDATORY|CMDLINE_FLAG_HELP_LEVEL0,
        0, "infile", "vl", count++, NULL, clphnd_in_file_name,
        "      --infile            input raw file name\n", NULL);

    cmdline_add_option(CMDLINE_FLAG_OPT_OPTIONAL|
        CMDLINE_FLAG_VAL_MANDATORY|CMDLINE_FLAG_HELP_LEVEL0,
        0, "ispscript", "vl", count++, NULL, clphnd_script_name,
        "      --ispscript         ISP processing script\n", NULL);

/*
    cmdline_add_option(CMDLINE_FLAG_OPT_OPTIONAL|
        CMDLINE_FLAG_HELP_LEVEL0,
        's', "show", NULL, count++, NULL, clphnd_show,
        " -s | --show               Show elements\n", NULL);

    cmdline_add_option(CMDLINE_FLAG_OPT_OPTIONAL|
        CMDLINE_FLAG_HELP_LEVEL0,
        'S', "show_all", NULL, count++, NULL, clphnd_show_all,
        " -S | --show_all           Show All elements\n", NULL);
*/
    cmdline_dbg_register_helps(true, true, CLP_FILENAME);
    cmdline_help_line(CMDLINE_FLAG_HELP_LEVEL9, "\nDebugs:");
    cmdline_dbg_register_debug_layers(false, true);

    return err;
}

int clp_parser(int argc, char **argv)
{
    int err = 0;

    err = cmdline_parser(argc, argv);
    err |= cmdline_parser_env(CLP_ENVNAME);
    if (err) {
        if (err == CMDLINE_ERROR_PRINT_HELP) {
            cmdline_dbg_flags |= CMDLINE_DBG_FLAG_HELP;
        } else {
            goto fail;
        }
    }

    if (cmdline_dbg_flags & CMDLINE_DBG_FLAG_HELP) {
        cmdline_help_usage_print(CLP_FILENAME);
        cmdline_help_print(CMDLINE_FLAG_HELP_MASK);
        err = -1;
        goto fail;
    }

    if (cmdline_dbg_flags & CMDLINE_DBG_FLAG_HELP_DEBUG_VAR) {
        cmdline_dbg_print_debug_variables();
        err = -1;
        goto fail;
    }

fail:
    return err;
}

