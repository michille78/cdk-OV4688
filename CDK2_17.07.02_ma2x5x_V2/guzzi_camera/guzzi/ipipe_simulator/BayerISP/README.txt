This project contains a complete Bayer Camera/ISP solution.

It is comprised of:
- components/ipipeClient: the IPIPE Client API.  This is a "component" in
  the MDK sense, and runs on LeonOS.
- ipipe: the core of IPIPE, which runs on LeonRT, and utilises Shave
  processors, as well as SIPP hardware accelerators.  When built, it
  produces leonRTApp.rtlib.
- apps/testIpipe: test app for exercising IPIPE in lieu of the Camera Framework.
  It runs on LeonOS, and calls the ipipeClient API.
- apps/TBD: The Camera Framework application, running on LeonOS.  It
  interfaces with the Host processor, and interfaces with IPIPE via the
  ipipeClient API.

