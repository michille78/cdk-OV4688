# Common Makefile for all apps in the project (PC/Linux build)
CC = cc
LD = c++
CPP = c++

# ----------------------------------------------------------
BUILD_DIR =  $(shell readlink -e $(PWD))
PROJECT_BASE = $(BUILD_DIR)/../../../

# ----------------------------------------------------------


LIBDEPS = ipipeClient.a ipipe.a
LIBS = -lpthread -lrt -lsipp -lhwmodels -lX11 -lapbase -lmidlib2 -lpython3.3m

LIBPATHS = -L$(PROJECT_BASE)/prebuild/mdk/libs -L$(PROJECT_BASE)/prebuild/devware/libs/lib32 \
             -L/opt/pym32/lib

IPIPE_CLIENT_SOURCEDIR += $(PROJECT_BASE)/components/ipipeClient/leon
IPIPE_CLIENT_HDIR += $(PROJECT_BASE)/components/ipipeClient/include

IPIPE_SOURCEDIR += $(PROJECT_BASE)/ipipe/leon_rt

APP_SOURCEDIR = $(PROJECT_BASE)/apps/$(APP_NAME)/leon

IPIPECLIENT_OBJS = ic_api.o ic_event.o ic_misc.o
IPIPE_OBJS = ip_main.o ip_mem.o ip_configure.o ip_quiesce.o ip_event.o \
		ip_process.o ip_capture.o devware.o x11_disp.o zslQueue.o

OBJS = $(IPIPECLIENT_OBJS) $(IPIPE_OBJS) $(APP_OBJS)
INCLUDES = -I$(IPIPE_CLIENT_HDIR) \
	-I$(PROJECT_BASE)/prebuild/mdk/include/sippInc/		\
	-I$(PROJECT_BASE)/prebuild/mdk/include/sipp/		\
	-I$(PROJECT_BASE)/prebuild/mdk/include/socDrivers/	\
	-I$(PROJECT_BASE)/prebuild/mdk/include/toolsInc/	\
	-I$(PROJECT_BASE)/prebuild/devware/include/

#CFLAGS = -Wall -Wno-unknown-pragmas -O2 -g -m32 -fpermissive -DPC_BUILD
CFLAGS = -Wall -g -m32 -DPC_BUILD $(INCLUDES)
LDFLAGS = -g -m32

all:	$(APP_NAME)

$(APP_NAME):	$(OBJS)
	$(LD) $(LDFLAGS) -o$(APP_NAME) $(OBJS) $(LIBPATHS) $(LIBS)

%.o:	$(IPIPE_CLIENT_SOURCEDIR)/%.c
	$(CC) $(CFLAGS) -c $< $(INCFLAGS)

%.o:	$(IPIPE_SOURCEDIR)/%.c
	$(CC) $(CFLAGS) -c $< $(INCFLAGS)

%.o:	$(IPIPE_SOURCEDIR)/%.cpp
	$(CPP) $(CFLAGS) -c $< $(INCFLAGS)

%.o:	$(APP_SOURCEDIR)/%.c
	$(CC) $(CFLAGS) -c $< $(INCFLAGS)

%.o:	%.cpp
	$(CPP) $(CFLAGS) -c $< $(INCFLAGS)

.PHONY:	clean
clean:
	rm -rf *.o $(APP_NAME) 
