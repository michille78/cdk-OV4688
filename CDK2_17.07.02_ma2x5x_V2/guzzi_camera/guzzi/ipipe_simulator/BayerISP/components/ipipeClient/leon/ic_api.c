///
/// @file
/// @copyright All code copyright Movidius Ltd 2014, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     IPipe Client API entry points
///

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

#ifdef PC_BUILD
#include <string.h>
#include <stdint.h>
#else
#include <mv_types.h>
#include <DrvLeon.h>
#endif

#include "ic_internal.h"

#ifdef PC_BUILD
static pthread_t leonRTThread;
extern void *	lrt_start_sim(void *);
extern icCtrl	g_icCtrl;
#else
extern uint32_t lrt_start;
extern icCtrl   lrt_g_icCtrl;

/*
 * The following is OR'd in to an address to access CMX from Leon,
 * bypassing the cache.
 */
#define BYPASS_CACHE	0x08000000
#endif

icInternal_t	icInternal;

/* Reset the shared structure - call only when LeonRT is in the QUIESCED state */
static void
icReset(icCtrl *ctrl)
{
	int	i;

	/* Reset event queues */
	ctrl->hpCmd = IC_HP_CMD_NONE;
	ctrl->exception = IC_EXCEPTION_NONE;

	ctrl->eventQOut.readIdx  = 0;
	ctrl->eventQOut.writeIdx = 0;
	ctrl->eventQIn.readIdx   = 0;
	ctrl->eventQIn.writeIdx  = 0;

	for (i = 0; i < IC_EVENT_QUEUE_SIZE; i++) {
		/* Clear "Owned" bit */
		ctrl->eventQOut.eventQ[i].ctrl = 0;
	}

	for (i = 0; i < IC_EVENT_QUEUE_SIZE; i++) {
		/* Clear "Owned" bit */
		ctrl->eventQIn.eventQ[i].ctrl = 0;
	}
}

icCtrl *
icSetup()
{
	icCtrl		*ctrl;
	unsigned int	i;
	int	rc;

#ifdef PC_BUILD
	ctrl = &g_icCtrl;
#else
	ctrl = (icCtrl *)((uint32_t)&lrt_g_icCtrl | BYPASS_CACHE);
#endif

	if (icInternal.state != IC_STATE_UNINITIALISED) {
		return NULL;
	}

	/*
	 * NB: The "ctrl" structure is *not* zeroed at program initialisation
	 * time, because it is not in the BSS!  Also, *never* access this
	 * structure without bypassing the cache.
	 */
	for (i = 0; i < sizeof (*ctrl) / 4; i++) {
		((uint32_t *)ctrl)[i] = 0;
	}

	icReset(ctrl);

	ctrl->framepoolSize = FRAME_POOL_SIZE;
#ifdef PC_BUILD
	if ((ctrl->framepoolBase = malloc(FRAME_POOL_SIZE)) == NULL) {
		perror("malloc");
		exit(1);
	}

	if ((rc = pthread_create(&leonRTThread, NULL, lrt_start_sim, NULL)) != 0) {
		fprintf(stderr, "pthread_create: %s\n", strerror(rc));
		exit(1);
	}

	if ((rc = pthread_mutex_init(&ctrl->condMutexOut, NULL)) != 0) {
		fprintf(stderr, "pthread_mutex_init: %s\n", strerror(rc));
		exit(1);
	}

	if ((rc = pthread_cond_init(&ctrl->condOut, NULL)) != 0) {
		fprintf(stderr, "pthread_cond_init: %s\n", strerror(rc));
		exit(1);
	}

	if ((rc = pthread_mutex_init(&ctrl->condMutexIn, NULL)) != 0) {
		fprintf(stderr, "pthread_mutex_init: %s\n", strerror(rc));
		exit(1);
	}

	if ((rc = pthread_cond_init(&ctrl->condIn, NULL)) != 0) {
		fprintf(stderr, "pthread_cond_init: %s\n", strerror(rc));
		exit(1);
	}

	if ((rc = pthread_mutex_init(&ctrl->senMutex, NULL)) != 0) {
		fprintf(stderr, "pthread_mutex_init: %s\n", strerror(rc));
		exit(1);
	}
#else
	ctrl->framepoolBase = &_FRAME_POOL_BASE;

	DrvLeonRTStartup((uint32_t)&lrt_start); /* Start the LeonRT application */
#endif
printf("OS: ctrl %x base %x size %d\n", (uint32_t)ctrl,
    (uint32_t)ctrl->framepoolBase, ctrl->framepoolSize);

	icInternal.state = IC_STATE_INITIALISED;

	return ctrl;
}

int
icTeardown(icCtrl **ctrl)
{
	if (icInternal.state == IC_STATE_UNINITIALISED) {
		return IC_ERROR_WRONG_STATE;
	}

	icSendHighPrioEvent(*ctrl, IC_HP_CMD_HALT, 100);

#ifdef PC_BUILD
	/* Stop the LeonRT thread and wait for it to exit */
	pthread_cancel(leonRTThread);
	pthread_join(leonRTThread, NULL);

	free((*ctrl)->framepoolBase);
#endif

	icInternal.state = IC_STATE_UNINITIALISED;

	return IC_ERROR_NONE;
}
//#######################################################################################
// - Configure global params; this triggers on RT side
//   the main buffer allocation (pseudo-dynamic allocation)
int32_t icConfigureGlobal(icCtrl *ctrl, icGlobalConfig *config)
{
   icEvent ev;

   memcpy((void*)&ctrl->globalConfig, config, sizeof(icGlobalConfig));
   ev.ctrl = IC_EVENT_TYPE_CONFIG_GLOBAL;
   icSendEvent(ctrl, &ev);
   icInternal.state = IC_STATE_CONFIGURED;
   return 0;
}

int
icConfigureSource(icCtrl *ctrl,
    icSourceInstance source, icSourceConfig *config)
{
	icEvent	ev;
	int	idx;
/*
	if (icInternal.state != IC_STATE_CONFIGURED) {
		return IC_ERROR_WRONG_STATE;
	}
*/
	switch (source) {
	case IC_SOURCE_RFC:
		if (icInternal.rfcRunning) {
			return IC_ERROR_WRONG_STATE;
		}
		idx = 0;
		break;
#ifdef IC_SUPPORT_FFC
	case IC_SOURCE_FFC:
		if (icInternal.ffcRunning) {
			return IC_ERROR_WRONG_STATE;
		}
		idx = 1;
		break;
#endif
	default:
		return IC_ERROR_BAD_PARAMETER;
	}

	memcpy((void *)&ctrl->sourceConfig[idx],
	    config, sizeof (ctrl->sourceConfig[0]));

	ev.ctrl = IC_EVENT_TYPE_CONFIG_SOURCE;
	ev.u.configureSource.sourceInstance = source;

	icSendEvent(ctrl, &ev);

	return IC_ERROR_NONE;
}

int
icStartSource(icCtrl *ctrl, icSourceInstance source)
{
	icEvent	ev;

//	if (icInternal.state != IC_STATE_CONFIGURED) {
//		return IC_ERROR_WRONG_STATE;
//	}

	switch (source) {
	case IC_SOURCE_RFC:
		icInternal.rfcRunning = 1;
		break;
#ifdef IC_SUPPORT_FFC
	case IC_SOURCE_FFC:
		icInternal.ffcRunning = 1;
		break;
#endif
	default:
		return IC_ERROR_BAD_PARAMETER;
	}

	ev.ctrl = IC_EVENT_TYPE_START_SOURCE;
	ev.u.start.sourceInstance = source;

	icSendEvent(ctrl, &ev);

	return IC_ERROR_NONE;
}

int
icStopSource(icCtrl *ctrl, icSourceInstance source)
{
	icEvent	ev;

	if (icInternal.state != IC_STATE_CONFIGURED) {
		return IC_ERROR_WRONG_STATE;
	}

	switch (source) {
	case IC_SOURCE_RFC:
		icInternal.rfcRunning = 0;
		break;
#ifdef IC_SUPPORT_FFC
	case IC_SOURCE_FFC:
		icInternal.ffcRunning = 0;
		break;
#endif
	default:
		return IC_ERROR_BAD_PARAMETER;
	}

	ev.ctrl = IC_EVENT_TYPE_STOP_SOURCE;
	ev.u.stop.sourceInstance = source;

	icSendEvent(ctrl, &ev);

	return IC_ERROR_NONE;
}

int
icConfigureIsp(icCtrl *ctrl,
    icIspInstance isp, icIspConfig *cfg, uint32_t seqNo)
{
	icEvent	ev;

    if ((icInternal.state != IC_STATE_CONFIGURED) &&
        (icInternal.state != IC_STATE_INITIALISED))
	{
		return IC_ERROR_WRONG_STATE;
	}

	switch (isp) {
	case IC_ISP_RFC_VIDEO:
	case IC_ISP_RFC_STILL:
#ifdef IC_SUPPORT_FFC
	case IC_ISP_FFC_VIDEO:
#endif
		break;
	default:
		return IC_ERROR_BAD_PARAMETER;
	}

#ifndef PC_BUILD
/*
 * TODO: On Myriad2, we have to flush *cfg from the cache, so that the
 * contents are coherent from LeonRT's point of view.
 * Assumption 1: the L1 cache is writethrough, no action needed.
 * Assumption 2: the L2 cache is Writeback, and the appropriate range of
 * memory needs to be flushed from the cache.
 * These assumptions need to be validated, and appropriate action taken!!
 *
 * The cfg structure must not be written to until the structure has been
 * released by LeonRT (ISP_CONFIG_RELEASED event)!
 */
#endif

	ev.ctrl = IC_EVENT_TYPE_CONFIG_ISP;
	ev.u.configureIsp.ispInstance = isp;
	ev.u.configureIsp.pConfigStruct = (uint32_t)cfg;
	ev.u.configureIsp.seqNo = seqNo;

	icSendEvent(ctrl, &ev);

	return IC_ERROR_NONE;
}

int
icLockZSL(icCtrl *ctrl, icSourceInstance source,
    uint32_t frameSel, icLockZSLFlags flags)
{
	icEvent	ev;

	if (icInternal.state != IC_STATE_CONFIGURED) {
		return IC_ERROR_WRONG_STATE;
	}

	ev.ctrl = IC_EVENT_TYPE_LOCK_ZSL;
	ev.u.lockZSL.sourceInstance = source;
	ev.u.lockZSL.frameSel = frameSel;
	ev.u.lockZSL.flags = flags;

	icSendEvent(ctrl, &ev);

	return IC_ERROR_NONE;
}

int
icTriggerCapture(icCtrl *ctrl, icSourceInstance source, void * buff,
    icIspConfig *cfg, icCaptureFlags flags)
{
	icEvent	ev;

	if (icInternal.state != IC_STATE_CONFIGURED) {
		return IC_ERROR_WRONG_STATE;
	}

	ev.ctrl = IC_EVENT_TYPE_CAPTURE;
	ev.u.capture.sourceInstance = source;
	ev.u.capture.buff = buff;
	ev.u.capture.pConfigStruct = (uint32_t)cfg;
	ev.u.capture.flags = flags;

	icSendEvent(ctrl, &ev);

	return IC_ERROR_NONE;
}

int
icUnlockZSL(icCtrl *ctrl, icSourceInstance source)
{
	icEvent	ev;

	if (icInternal.state != IC_STATE_CONFIGURED) {
		return IC_ERROR_WRONG_STATE;
	}

	ev.ctrl = IC_EVENT_TYPE_UNLOCK_ZSL;
	ev.u.unlockZSL.sourceInstance = source;

	icSendEvent(ctrl, &ev);

	return IC_ERROR_NONE;
}

void icOpenMipiClient(icCtrl *ctrl, uint32_t instance, uint32_t dataType, uint32_t dataSizeMax, void* userData)
{
    icEvent ev;

    ev.u.openMipiTxClient.instance = instance;   //identify instance
    ev.u.openMipiTxClient.userData = userData;
    ev.u.openMipiTxClient.dataType = dataType; //ptr to configuration data struct
    ev.u.openMipiTxClient.max_dataSize = dataSizeMax;
    ev.ctrl                          = IC_EVENT_TYPE_OPEN_MIPI_CLIENT; //type of event

    icSendEvent(ctrl, &ev);
}

void icCloseMipiClient(icCtrl *ctrl, uint32_t instance, void* clientHandle, void* userData)
{
    icEvent ev;

    ev.u.closeMipiTxClient.instance = instance;   //identify instance
    ev.u.closeMipiTxClient.clientHandle = clientHandle;
    ev.u.closeMipiTxClient.userData = userData;
    ev.ctrl                          = IC_EVENT_TYPE_CLOSE_MIPI_CLIENT; //type of event

    icSendEvent(ctrl, &ev);
}

void icSendTxClientData(icCtrl *ctrl, uint32_t instance, void* clientHandle, void* inBuffer, uint32_t buffSize, void* userData)
{
    icEvent ev;

    ev.u.sendMipiData.instance = instance;   //identify instance
    ev.u.sendMipiData.clientHandle = clientHandle;
    ev.u.sendMipiData.userData = userData;
    ev.u.sendMipiData.dataSize = buffSize;
    ev.u.sendMipiData.buff = inBuffer;
    ev.ctrl                         = IC_EVENT_TYPE_SEND_MIPI; //type of event

      icSendEvent(ctrl, &ev);
}




