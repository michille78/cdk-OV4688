///
/// @file
/// @copyright All code copyright Movidius Ltd 2014, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     IPipe internals
///

#include <ipipe.h>

#ifndef PC_BUILD
#include <swcLeonUtils.h>

void printk(const char *fmt, ...);
#endif

#ifdef PC_BUILD
#if !defined(__sparc)
#define FRAME_POOL_SIZE		(1024*1024*112)
#else
#define FRAME_POOL_SIZE     (1024*1024*3)
#endif // ! __sparc
#else
extern uint8_t	_FRAME_POOL_BASE;
extern uint8_t	_FRAME_POOL_END;
#define FRAME_POOL_SIZE		(&_FRAME_POOL_END - &_FRAME_POOL_BASE)
#endif

/* Global state of the IPIPE from the client's point of view */
typedef enum {
	IC_STATE_UNINITIALISED =		0,
	IC_STATE_INITIALISED =			1,
	IC_STATE_CONFIGURED =			2,	/* Global configuration has taken place */
} icState;

typedef struct {
	uint32_t	state;
	int		rfcRunning;
#ifdef IC_SUPPORT_FFC
	int		ffcRunning;
#endif
} icInternal_t;

extern icInternal_t	icInternal;

/* ipipe_event.h */
int icSendHighPrioEvent(icCtrl *ctrl, icHpCmd cmd, int timeoutMS);
int icSendEvent(icCtrl *ctrl, icEvent *ev);
void *icEventLoop(void *);

/* ipipe_misc.h */
void icSleepMs(unsigned ms);
