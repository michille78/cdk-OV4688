///
/// @file
/// @copyright All code copyright Movidius Ltd 2014, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     IPipe Client event-related functionality (internal functions)
///

#include <stdio.h>
#include <string.h>

#include "ic_internal.h"

void
icKickLeonRT(icCtrl *ctrl)
{
#ifdef PC_BUILD
	pthread_mutex_lock(&ctrl->condMutexOut);
	ctrl->lrtInterruptPending = 1;
	pthread_cond_signal(&ctrl->condOut);
	pthread_mutex_unlock(&ctrl->condMutexOut);
#else
#pragma message("icKickLeonRT() not implemented for Myriad2 yet!")
#endif
}

int
icSendHighPrioEvent(icCtrl *ctrl, icHpCmd cmd, int timeoutMs)
{
	ctrl->hpCmd = cmd;
	icKickLeonRT(ctrl);

	/* Wait for RT Leon to acknowledge by clearing command */
	while (timeoutMs > 0) {
		if (ctrl->hpCmd == IC_HP_CMD_NONE) {
			return IC_ERROR_NONE;
		}
		icSleepMs(1);
		timeoutMs--;
	}

	return IC_ERROR_RT_LEON_NOT_RESPONDING;
}

int
icSendEvent(icCtrl *ctrl, icEvent *ev)
{
	icEvent		*eqPtr = &ctrl->eventQOut.eventQ[ctrl->eventQOut.writeIdx];
	unsigned	i;

	/* Wait for free entry in the ring buffer */
	while (eqPtr->ctrl & IC_EVENT_CTRL_OWNED)
		;

	/*
	 * Copy the event - "ctrl" field must be set last, since this gives
	 * ownership of the ring buffer entry to the consumer.
	 */
	for (i = 0; i < sizeof (ev->u) / 4; i++) {
		((uint32_t *)eqPtr)[i] = ((uint32_t *)ev)[i];
	}
	eqPtr->ctrl = ev->ctrl | IC_EVENT_CTRL_OWNED;

	/* If the LeonRT is idle, wake it up so it checks for a new event */
	icKickLeonRT(ctrl);

	/* Advance the ring queue ptr */
	if (++ctrl->eventQOut.writeIdx == IC_EVENT_QUEUE_SIZE) {
	    ctrl->eventQOut.writeIdx = 0;
	}

	return 0;
}

int
icIsEventPending(icCtrl *ctrl)
{
	icEvent	*evPtr = &ctrl->eventQIn.eventQ[ctrl->eventQIn.readIdx];

	return (evPtr->ctrl & IC_EVENT_CTRL_OWNED) ? 1 : 0;
}

/* This call is blocking... */
int
icGetEvent(icCtrl *ctrl, icEvent *ev)
{
	icEvent	*evPtr = &ctrl->eventQIn.eventQ[ctrl->eventQIn.readIdx];

	while ((evPtr->ctrl & IC_EVENT_CTRL_OWNED) == 0) {
#ifdef PC_BUILD
			pthread_mutex_lock(&ctrl->condMutexIn);

		while (ctrl->losInterruptPending == 0) {
			pthread_cond_wait(&ctrl->condIn, &ctrl->condMutexIn);
		}

			ctrl->losInterruptPending = 0;

		pthread_mutex_unlock(&ctrl->condMutexIn);
#else
#pragma message("IPC interrupt mechanism not implemented for Myriad2 yet!")
#endif
		evPtr = &ctrl->eventQIn.eventQ[ctrl->eventQIn.readIdx];
	}

	memcpy(ev, evPtr, sizeof (*ev));

	/* Release the ring buffer entry back to the producer */
	evPtr->ctrl = 0;

	/* Advance the ring queue read ptr */
	if (++ctrl->eventQIn.readIdx == IC_EVENT_QUEUE_SIZE) {
	    ctrl->eventQIn.readIdx = 0;
	}

	return 0;
}
