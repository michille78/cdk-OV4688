///
/// @file
/// @copyright All code copyright Movidius Ltd 2014, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     IPipe Client misc. helper routines
///

#include "ic_internal.h"

#include <time.h>

void
icSleepMs(unsigned ms)
{
	struct timespec	t;
	t.tv_sec = 0;
	t.tv_nsec = ms*1000*1000;
	nanosleep(&t, NULL);
}
