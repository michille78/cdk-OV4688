/*
 * IPIPE Client API
 *
 * @file      ipipe.h
 * @brief     IPIPE Client API definitions
 * @copyright All code copyright Movidius Ltd 2013-2014, all rights reserved
 */

#ifndef _IPIPE_H
#define _IPIPE_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h> //Myriad and PC

#include <pthread.h>  //PC-gcc
#include <stdlib.h>   //PC
#include <stdio.h>    //PC


/// L2 Cache bypass macro
#ifndef BYPASS_L2_CACHE
#   define L2THROUGHCACHE(x) ((uint32_t)(x) & ~0x08000000)
#else
#   define L2THROUGHCACHE(x) ((uint32_t)(x) | 0x08000000)
#endif

typedef struct {
    uint32_t    w;
    uint32_t    h;
} icSize;

typedef struct {
    int32_t     x1;
    int32_t     y1;
    int32_t     x2;
    int32_t     y2;
} icRect;

#define IC_MAX_AE_AWB_PATCHES_V   64
#define IC_MAX_AE_AWB_PATCHES_H   64
#define IC_MAX_AF_PATCHES       4096

/*
 * Pass to icConfigureIsp when you don't want to wait for a specific frame
 * before the settings take effect.
 */
#define ISP_SEQNO_IGNORED   0xffffffff

/* Exceptions raised by IPIPE */
typedef enum {
    IC_EXCEPTION_NONE =                 0,
    IC_EXCEPTION_INSUFFICIENT_MEMORY =  1,
    IC_EXCEPTION_BAD_EVENT =            2,
} icException;

/* API usage errors (returned by ic*() calls that return int) */
typedef enum {
    IC_ERROR_NONE =                     0,  /* Happy days */
    IC_ERROR_WRONG_STATE =              1,
    IC_ERROR_BAD_PARAMETER =            3,
    IC_ERROR_RT_LEON_NOT_RESPONDING =   4,
    IC_ERROR_RT_CFG_MISSING =           5,
    IC_ERROR_BAD_SOURCE_CFG_PARAMS =    6,
} icError;

/* Severity of exception or error */
typedef enum {
    IC_SEVERITY_NORMAL =                0,  /* Normal severity - system will continue to function */
    IC_SEVERITY_RESET =                 1,  /* System halted - reset required to recover */
    IC_SEVERITY_CATASTROPHIC =          2,  /* Catastrophic failure - system halted - reset unlikely to recover */
} icSeverity;

/* High priority events */
typedef enum {
    IC_HP_CMD_NONE =                    0,
    IC_HP_CMD_HALT =                    1,  /* Tell LeonRT app to stop everything it's doing and halt.  LeonRT needs to be restarted afterwards. */
    IC_HP_CMD_QUIESCE =                 2,  /* Tell LeonRT app to stop everything it's doing and sleep until futher commanrd arrive */
} icHpCmd;

/* Bayer pattern order */
typedef enum {
    IC_BAYER_FORMAT_GRBG = 0,
    IC_BAYER_FORMAT_RGGB = 1,
    IC_BAYER_FORMAT_GBRG = 2,
    IC_BAYER_FORMAT_BGGR = 3,
} icBayerFormat;

/* ISP parameter groups */
typedef enum {
    IC_ISP_GROUP_BLC =              0x00000001,
    IC_ISP_GROUP_RAW =              0x00000002,
    IC_ISP_GROUP_LSC =              0x00000004,
    IC_ISP_GROUP_DEMOSAIC =         0x00000008,
    IC_ISP_GROUP_CHROMA_GEN =       0x00000010,
    IC_ISP_GROUP_LUMA_DNS =         0x00000020,
    IC_ISP_GROUP_LUMA_DNS_REF =     0x00000040,
    IC_ISP_GROUP_CHROMA_DNS =       0x00000080,
    IC_ISP_GROUP_MEDIAN =           0x00000100,
    IC_ISP_GROUP_MEDIAN_MIX =       0x00000200,
    IC_ISP_GROUP_LOWPASS =          0x00000400,
    IC_ISP_GROUP_SHARPEN =          0x00000800,
    IC_ISP_GROUP_RANDOM_NOISE =     0x00001000,
    IC_ISP_GROUP_COLOR_COMBINE =    0x00002000,
    IC_ISP_GROUP_GAMMA =            0x00004000,
    IC_ISP_GROUP_CROP_RESIZE =      0x00008000,
    IC_ISP_GROUP_PIPELINE_CTL =     0x00010000,
} icIspGroup;

#define DEF_PRV_ISP_GROUP_FLAGS (IC_ISP_GROUP_BLC           | \
                                 IC_ISP_GROUP_RAW           | \
                                 IC_ISP_GROUP_LSC           | \
                                 IC_ISP_GROUP_DEMOSAIC      | \
                                 IC_ISP_GROUP_CHROMA_GEN    | \
                                 IC_ISP_GROUP_LUMA_DNS      | \
                                 IC_ISP_GROUP_LUMA_DNS_REF  | \
                                 IC_ISP_GROUP_CHROMA_DNS    | \
                                 IC_ISP_GROUP_MEDIAN        | \
                                 IC_ISP_GROUP_MEDIAN_MIX    | \
                                 IC_ISP_GROUP_LOWPASS       | \
                                 IC_ISP_GROUP_SHARPEN       | \
                                 IC_ISP_GROUP_RANDOM_NOISE  | \
                                 IC_ISP_GROUP_COLOR_COMBINE | \
                                 IC_ISP_GROUP_GAMMA)

#define DEF_CAPT_ISP_GROUP_FLAGS (DEF_PRV_ISP_GROUP_FLAGS)
#define DEF_ZSL_ISP_GROUP_FLAGS (DEF_PRV_ISP_GROUP_FLAGS)

/* ISP function enable flags */
typedef enum {
    IC_ISP_ENABLE_DPC =             0x00000001,
    IC_ISP_ENABLE_GRGB_IMBALANCE =  0x00000002,
    IC_ISP_ENABLE_AE_AWB_STATS =    0x00000004,
    IC_ISP_ENABLE_AF_STATS =        0x00000008,
    IC_ISP_ENABLE_LSC =             0x00000010,
    IC_ISP_ENABLE_LUMA_DENOISE =    0x00000020,
    IC_ISP_ENABLE_SHARPEN =         0x00000040,
    IC_ISP_ENABLE_CHROMA_DENOISE =  0x00000080,
    IC_ISP_ENABLE_LOWPASS =         0x00000100,
    IC_ISP_ENABLE_GAMMA =           0x00000200,
} icIspEnable;

#define DEF_PRV_ISP_ENABLE_FLAGS (IC_ISP_ENABLE_DPC            |  \
                                  IC_ISP_ENABLE_GRGB_IMBALANCE |  \
                                  IC_ISP_ENABLE_AE_AWB_STATS   |  \
                                  IC_ISP_ENABLE_AF_STATS       |  \
                                  IC_ISP_ENABLE_LSC            |  \
                                  IC_ISP_ENABLE_LUMA_DENOISE   |  \
                                  IC_ISP_ENABLE_SHARPEN        |  \
                                  IC_ISP_ENABLE_CHROMA_DENOISE |  \
                                  IC_ISP_ENABLE_LOWPASS        |  \
                                  IC_ISP_ENABLE_GAMMA)

#define DEF_CAPT_ISP_ENABLE_FLAGS (DEF_PRV_ISP_ENABLE_FLAGS)
#define DEF_ZSL_ISP_ENABLE_FLAGS (DEF_PRV_ISP_ENABLE_FLAGS)

/* ISP instance specifiers */
typedef enum {
    IC_ISP_RFC_VIDEO =      0x00000001,
    IC_ISP_RFC_STILL =      0x00000002,
    IC_ISP_FFC_VIDEO =      0x00000004,
} icIspInstance;

/*
 * By convention, in a mobile device, the Rear-facing camera is source 0, and
 * the Front-facing camera is source 1.  In a stereo camera configuration, the
 * left source is source 0, and the right source is source 1.
 */
typedef enum {
    IC_SOURCE_RFC =         0,
    IC_SOURCE_FFC =         1,
} icSourceInstance;

/* flags passed to icLockZSL() */
typedef enum {
    IC_LOCKZSL_CLEAR_RAW =      0x00000001,
    IC_LOCKZSL_TS_RELATIVE =    0x00000002,
} icLockZSLFlags;

/* flags passed to icTriggerCapture() */
typedef enum {
    IC_CAPTURE_SEND_YUV =           0x00000001,
    IC_CAPTURE_SEND_RAW =           0x00000002,
    IC_CAPTURE_KEEP_ZSL_LOCKED =    0x00000004,
    IC_CAPTURE_STOP_PREVIEW    =    0x00000008,
    IC_CAPTURE_SEND_CLEAR_RAW  =    0x00000010
} icCaptureFlags;

/* icIspConfig.pipeControl */
typedef enum {
    IC_PIPECTL_ZSL_OUT_ENABLE =         0x00000001,
    IC_PIPECTL_PREPROC_OUT_ENABLE  =    0x00000002,
    IC_PIPECTL_MIPI_ENABLE =            0x00000004,
    IC_PIPECTL_ZSL_LOCK =               0x00000008,
} icPipeCtlFlags;

#define DEF_PRV_PIPE_CTRL_FLAGS (IC_PIPECTL_ZSL_OUT_ENABLE      | \
                                 IC_PIPECTL_PREPROC_OUT_ENABLE  | \
                                 IC_PIPECTL_MIPI_ENABLE)

#define DEF_CAPT_PIPE_CTRL_FLAGS (DEF_PRV_PIPE_CTRL_FLAGS | IC_PIPECTL_ZSL_LOCK)
#define DEF_ZSL_PIPE_CTRL_FLAGS  (DEF_PRV_PIPE_CTRL_FLAGS)

/* Global IPIPE configuration settings (not per-source or per-ISP) */
typedef struct {
    uint32_t         zslNBufs;

    /* Max. frame sizes output by the cameras */
    icSize      rfcRawOutputSize;
    icSize      ffcRawOutputSize;

    /* Max. frame sizes output by the pre-processor pipelines */
    icSize      rfcPpOutputSize;
    icSize      ffcPpOutputSize;

    /* Max. frame sizes at ISP outputs (size of frames sent to Host) */
    icSize      rfcVideoOutputSize;
    icSize      rfcStillOutputSize;
    icSize      ffcVideoOutputSize;
} icGlobalConfig;

typedef struct
{
    uint32_t controllerNo;
    uint32_t noLanes;
    uint32_t laneRateMbps;
    uint32_t dataType;
    uint32_t dataMode;
} icMipiConfig;
/*
 * Per-source configuration parameters - mostly information needed to
 * configure the MIPI Rx filter.
 */
typedef struct {
    icSize          cameraOutputSize;
    icRect          cropWindow;

    /*
     * Bayer Format - Raw, Demosaic and LSC blocks should be programmed
     * to match the Bayer order specified here.
     */
    icBayerFormat   bayerFormat;
    uint32_t        bitsPerPixel;

    /*
     * Data is clocked into the MIPI Rx filters using the media clock.
     * This is a pixel clock which is synchronous to the SIPP system
     * clock but runs at either half or a quarter of its frequency.
     * The following value specifies the factor by which the SIPP
     * system clock is divided to determine the Media clock frequency.
     * valid values are 2 and 4.
     */
    uint32_t        clockDiv;

    /* Vertical blanking porch - normally not needed (set to 0) */
    uint32_t        VPB;

    /* mipi RX data configuration     */
    icMipiConfig    mipiRxData;
} icSourceConfig;

/*
 * Per-source configuration of parameters which can be modified dynamically
 * (the source does not need to be stopped).  Settings will take effect during
 * the next blanking interval.
 */
typedef struct {
    /*
     * Line number upon which IC_EVENT_TYPE_LINE REACHED will be sent
     * to the Leon OS.  Set to -1 to disable notification.
     */
    int32_t     notificationLine;
} icSourceConfigDynamic;

/* Per-patch components SUM collected for AE/AWB            */
/* Example: bayer format R-Gr / Gb-B                        */
/*          One can access the stats of Patch(x,y) via:     */
/*          statsData->aeAwb[y].evenLine[x].rawEvenSum //R  */
/*          statsData->aeAwb[y].evenLine[x].rawOddSum  //Gr */
/*          statsData->aeAwb[y].oddLine[x].rawEvenSum  //Gb */
/*          statsData->aeAwb[y].oddLine[x].rawOddSum   //B  */
typedef struct {
    uint32_t    rawEvenSum;
    uint32_t    rawOddSum;
}icAeAwbLineStats;

typedef struct {
    icAeAwbLineStats    evenLine[IC_MAX_AE_AWB_PATCHES_H];
    icAeAwbLineStats    oddLine [IC_MAX_AE_AWB_PATCHES_H];
}icAeAwbStatsPatch;

/* Per-patch saturated pixels count collected for AE/AWB */
typedef struct {
    uint32_t    rawEvenCount;
    uint32_t    rawOddCount;
}icAeAwbLineSatStats;

typedef struct {
    icAeAwbLineSatStats    evenLine[IC_MAX_AE_AWB_PATCHES_H];
    icAeAwbLineSatStats    oddLine [IC_MAX_AE_AWB_PATCHES_H];
}icAeAwbSatStatsPatch;

/* Per-patch stats collected for AF */
typedef struct {
    uint32_t        sum;                /* Sum of all values in the patch prior to filtering */
    uint32_t        f1AboveThreshold;   /* Number of pixels that were above the threshold for Filter 1 */
    uint32_t        f1Sum;              /* Sum of all Filter 1 output values which were above the threshold for filter 1 */
    uint32_t        f1SumRowMaximums;   /* Sum of per-row maximums of filter1 output */
    uint32_t        f2AboveThreshold;   /* Number of pixels that were above the threshold for Filter 2 */
    uint32_t        f2Sum;              /* Sum of all Filter 2 output values which were above the threshold for filter 2 */
    uint32_t        f2SumRowMaximums;   /* Sum of per-row maximums of filter2 output */
} icAfStatsPatch;

/* Main AE-AWB statistics data structure (patch based) */
typedef struct {
    icAeAwbStatsPatch     aeAwb   [IC_MAX_AE_AWB_PATCHES_V];
    icAeAwbSatStatsPatch  aeAwbSat[IC_MAX_AE_AWB_PATCHES_V];
} icAeAwbStats;

/* Main AF statistics data structure (patch based) */
typedef struct {
    icAfStatsPatch        af[IC_MAX_AF_PATCHES];
} icAfStats;

/* Per-ISP configuration parameters - filter parameters etc. */
typedef struct {
    uint32_t        frameCount;     /* Frame counter from the client */
    uint32_t        frameId;        /* Frame ID from the client */
    void            *userData;      /* Private data from the client */
    uint32_t        dirtyFlags;     /* OR'd IC_ISP_GROUP_* flags */
    uint32_t        enableFlags;    /* OR'd IC_ISP_ENABLE_* flags */

    /* Pipeline Control - enable/disable various paths */
    uint32_t        pipeControl;

    /* Number of bits in the data coming from the sensor */
    int32_t         pipelineBits;

    /* Bayer order from sensor */
    int32_t         bayerOrder;

    /*
     * Black Level Correction (implemented in Mipi Rx block).  Values are
     * in the same range as (have same bit depth as) the input data.
     */
    struct {
        uint32_t    gr;
        uint32_t    r;
        uint32_t    b;
        uint32_t    gb;
    } blc;

    /* RAW filter */
    struct {
        /* Digital Gain - 8.8 Fixed Point */
        uint32_t    gainGr;
        uint32_t    gainR;
        uint32_t    gainB;
        uint32_t    gainGb;

        /* Clamp - e.g. set to 0x3fff if RAW block outputs data in 14-bit range */
        uint32_t    clampGr;
        uint32_t    clampR;
        uint32_t    clampB;
        uint32_t    clampGb;

        /* GrGb imbalance */
        uint32_t    grgbImbalPlatDark;
        uint32_t    grgbImbalDecayDark;
        uint32_t    grgbImbalPlatBright;
        uint32_t    grgbImbalDecayBright;
        uint32_t    grgbImbalThr;

        /* Defect pixel correction */
        uint32_t    dpcAlphaHotG;
        uint32_t    dpcAlphaHotRb;
        uint32_t    dpcAlphaColdG;
        uint32_t    dpcAlphaColdRb;
        uint32_t    dpcNoiseLevel;

        /*
         * Bit range of the raw filter output.  Bit range may be
         * promoted in the Raw filter - e.g. from 10 bits to 14 bits -
         * by programming the gains appropriately (in the case of
         * promotion from 10 to 14 bits, multiply by 16).
         */
        uint32_t    outputBits;
    } raw;

    /* Lens Shading Correction filter */
    struct {
        uint32_t    lscWidth;
        uint32_t    lscHeight;
        uint32_t    lscStride;
        /*
         * Mesh entries are in 8.8 Fixed Point format.  Mesh is
         * laid out in a Bayer pattern.  Note: the table that this
         * field points to should not be modified until the
         * ISP update structure has been released via the
         * IC_EVENT_TYPE_ISP_CONFIG_ACCEPTED event.
         */
        uint16_t    *pLscTable;
    } lsc;

    struct {
        /* White Balance Gains - 8.8 Fixed Point */
        uint32_t    gainR;
        uint32_t    gainG;
        uint32_t    gainB;
    } wbGains;

    /* Demosaic */
    struct {
        int32_t     dewormGradientMul;  /* Fixed Point 1.7 */
        uint32_t    dewormSlope;        /* Fixed point 8.8 */
        int32_t     dewormOffset;       /* Signed integer */
    } demosaic;

    /* AE/AWB stats config */
    struct {
        uint32_t    firstPatchX;        /* X coord of top-left pixel of first patch */
        uint32_t    firstPatchY;        /* Y coord of top-left pixel of first patch */
        uint32_t    patchWidth;
        uint32_t    patchHeight;
        uint32_t    patchGapX;
        uint32_t    patchGapY;
        uint32_t    nPatchesX;
        uint32_t    nPatchesY;
        uint32_t    satThresh;          /* 0.16 Fixed Point */
    } aeAwbConfig;

    /*
     * AF stats config.
     *
     * Note: All values are relative to the full field
     * of view, i.e. they are specified in terms of the Still Image
     * resolution, and not the Video/Preview resolution.
     */
    struct {
        uint32_t    firstPatchX;        /* X coord of top-left pixel of first patch */
        uint32_t    firstPatchY;        /* Y coord of top-left pixel of first patch */
        uint32_t    patchWidth;
        uint32_t    patchHeight;
        uint32_t    nPatchesX;
        uint32_t    nPatchesY;
        uint32_t    nSkipRows;          /* Number of skipped lines in calc; 0 = no skipping */
        int32_t     initialSubtractionValue;
        int32_t     f1Threshold;
        int32_t     f1Coeffs[11];
        int32_t     f2Threshold;
        int32_t     f2Coeffs[11];
    } afConfig;


    icAeAwbStats *aeAwbStats;
    icAfStats    *afStats;

    /* Chroma Plane generation */
    struct {
        float    epsilon;
        float    scaleR;
        float    scaleG;
        float    scaleB;
    } chromaGen;

    /* Purple Flare removal */
    struct {
        uint32_t    strength;       /* 8.8 fixed point */
    } purpleFlare;

    /* Weighted averaging filter on Luma channel */
    struct {
     uint32_t  alpha;
     float     strength; /* reference value; we'll be using derived bitpos and lut below*/
     uint32_t  bitpos;   /* derived from strength  */
     uint8_t   lut[32];  /* derived from strength  */
     uint32_t  f2;       /* Spatial weight control */
    } lumaDenoise;

    /* Generate reference plane for Denoise */
    struct {
        float       angle_of_view;  /* Debug/reference only - not used by IPIPE */
        float       gamma;          /* Debug/reference only - not used by IPIPE */
        uint8_t     lutDist[256];   /* derived from angle_of_view               */
        uint8_t     lutGamma[256];  /* derived from gamma                       */
        uint32_t    shift;          /* derived from angle_of_view               */
    } lumaDenoiseRef;

    /* Add Random Noise to Luma channel */
    struct {
        float    strength;
    } randomNoise;

    struct {
       uint16_t  curves[16*10]; //fp16 values; last column/row are
                                //duplicated of Last-1
    } ltm;

    /* Local brightness map generation filter for LTM */
    struct {
        uint32_t    th1;
        uint32_t    th2;
        uint32_t    limit;
    } ltmLBFilter;

    /* Chroma filter on 21 lines */
    struct {
        uint32_t    th_r;
        uint32_t    th_g;
        uint32_t    th_b;
        uint32_t    limit;
        uint32_t    hEnab;
    } chromaDenoise;

    /* Median filter on Chroma channels */
    struct {
        uint32_t    kernelSize;     /* Must be 0, 3, 5 or 7 */
    } median;

    /* Mixing of median filtered output with pre-median filtered */
    struct {
        float    slope;
        float    offset;
    } medianMix;

    /* Separable 3x3 gaussian filter on Chroma channels */
    struct {
        float   coefs[2];
    } lowpass;

    /* Grey pixel desaturation */
    struct {
        uint32_t    slope;          /* 8.8 fixed point */
        int32_t     offset;         /* Signed 8.8 fixed point */
        int32_t     grey_cr;
        int32_t     grey_cg;
        int32_t     grey_cb;
    } greyDesat;

    /* Luma channel sharpening */
    struct {
        float     sigma;              /* reference value */
        uint16_t  sharpen_coeffs[4];  /* fp16 format (derived from Sigma) */
        uint16_t  strength_darken;    /* fp16 format */
        uint16_t  strength_lighten;   /* fp16 format */
        uint16_t  alpha;              /* fp16 format */
        uint16_t  overshoot;          /* fp16 format */
        uint16_t  undershoot;         /* fp16 format */
        uint16_t  rangeStop0;         /* fp16 format */
        uint16_t  rangeStop1;         /* fp16 format */
        uint16_t  rangeStop2;         /* fp16 format */
        uint16_t  rangeStop3;         /* fp16 format */
        uint16_t  minThr;             /* fp16 format */
    } sharpen;

    /* Luma + Chroma -> RGB -> RGB_corrected */
    struct {
       float   desat_mul;
       float   desat_t1;
       float   ccm[9];
    } colorCombine;

    /* Gamma encoding of RGB */
    struct {
        /* Gamma table entries are within range [0, rangetop] */
        uint32_t    rangetop;
        uint32_t    size;
        uint16_t    *table; //fp16 Channel-Mode setup (Rval,Gval,Bval,Pad) for each entry
    } gamma;

    /* Color Conversion */
    struct {
       float  mat[3*3];
       float  offset[3]; //free term
    } colorConvert;

    struct {
       uint32_t exp;
       uint32_t gain;
       uint32_t colour_temp;
    } env;
} icIspConfig;

/* Timestamps are in tenths of milliseconds */
typedef int64_t icTimestamp;

#define IC_EVENT_CTRL_TYPE_MASK 0x000000ff
#define IC_EVENT_CTRL_OWNED     0x80000000  /* Set means event is owned *by consumer* */

typedef enum {
    /* IPIPE -> Client */
    IC_EVENT_TYPE_READOUT_START =           0,
    IC_EVENT_TYPE_READOUT_END =             1,
    IC_EVENT_TYPE_LINE_REACHED =            2,
    IC_EVENT_TYPE_ISP_START =               3,
    IC_EVENT_TYPE_ISP_END =                 4,
    IC_EVENT_TYPE_ZSL_LOCKED =              5,
    IC_EVENT_TYPE_STATS_READY   =           6,
    IC_EVENT_TYPE_SOURCE_STOPPED =          7,
	IC_EVENT_TYPE_ISP_CONFIG_ACCEPTED =	    8,
    IC_EVENT_TYPE_ERROR =                   9,
    IC_EVENT_TYPE_LEON_RT_READY =           10,
    IC_EVENT_TYPE_SOURCE_READY =            11,
    IC_EVENT_TYPE_MIPI_CLIENT_REGISTERED =  12,
    IC_EVENT_TYPE_MIPI_CLIENT_UNREGISTERED = 13,
    IC_EVENT_TYPE_MIPI_SENT =               14,

    /* Client -> IPIPE */
    IC_EVENT_TYPE_CONFIG_GLOBAL =           41,
    IC_EVENT_TYPE_CONFIG_SOURCE =           42,
    IC_EVENT_TYPE_CONFIG_SOURCE_DYNAMIC =   43,
    IC_EVENT_TYPE_CONFIG_ISP =              44,
    IC_EVENT_TYPE_START_SOURCE =            45,
    IC_EVENT_TYPE_STOP_SOURCE =             46,
    IC_EVENT_TYPE_LOCK_ZSL =                47,
    IC_EVENT_TYPE_CAPTURE =                 48,
    IC_EVENT_TYPE_UNLOCK_ZSL =              49,
    IC_EVENT_TYPE_OPEN_MIPI_CLIENT =        50,
    IC_EVENT_TYPE_CLOSE_MIPI_CLIENT =       51,
    IC_EVENT_TYPE_SEND_MIPI =               52,

    IC_EVENT_TYPE_TEAR_DOWN =               80,
} icEventType;

typedef struct {
    union {
        /*------------------------------------------------------------------*/
        /* Events: IPIPE -> Client */
        struct {
            /*
             * This struct is used for the following events:
             * - IC_EVENT_TYPE_READOUT_START
             * - IC_EVENT_TYPE_READOUT_END
             * - IC_EVENT_TYPE_LINE_REACHED
             * - IC_EVENT_TYPE_ZSL_LOCKED
             */
            uint32_t        sourceInstance;
            void            *userData;
            uint32_t        seqNo;
            icTimestamp     ts;
        } lineEvent;
        struct {
            /*
             * This struct is used for the following events:
             * - IC_EVENT_TYPE_ISP_START
             * - IC_EVENT_TYPE_ISP_END
			 * - IC_EVENT_TYPE_ISP_CONFIG_ACCEPTED
             */
            uint32_t        ispInstance;
            void            *userData;
            uint32_t        seqNo;
        } ispEvent;
        struct {
            /* IC_EVENT_TYPE_STATS_READY */
            uint32_t        ispInstance;
            void            *userData;
            uint32_t        seqNo;
        } statsReady;
        struct {
            /* IC_EVENT_TYPE_SOURCE_STOPPED */
            /* IC_EVENT_TYPE_SOURCE_READY */
            uint32_t        sourceInstance;
        } sourceEvent;
        struct {
            /* IC_EVENT_TYPE_ERROR */
            int32_t         errorNo;
            int32_t         severity;
        } error;
        struct {
             // IC_EVENT_TYPE_ZSL_LOCKED
            uint32_t        sourceInstance;
            void            *userData;
            void            *buffZsl;
            uint32_t        seqNo;
            icTimestamp     ts;
        } buffLockedZSL;

        /*------------------------------------------------------------------*/
        /* Events: Client -> IPIPE */
        struct {
            /* IC_EVENT_TYPE_CONFIG_SOURCE */
            uint32_t        sourceInstance;
        } configureSource;
        struct {
            /* IC_EVENT_TYPE_CONFIG_SOURCE_DYNAMIC */
            uint32_t        sourceInstance;
        } configureSourceDynamic;
        struct {
            /* IC_EVENT_TYPE_START_SOURCE */
            uint32_t        sourceInstance;
        } start;
        struct {
            /* IC_EVENT_TYPE_STOP_SOURCE */
            uint32_t        sourceInstance;
            int32_t         stopImmediate;
        } stop;
        struct {
            /* IC_EVENT_TYPE_CONFIG_ISP */
            uint32_t        ispInstance;
            uint32_t        pConfigStruct;
            uint32_t        seqNo;
        } configureIsp;
        struct {
            /* IC_EVENT_TYPE_LOCK_ZSL */
            uint32_t        sourceInstance;
            uint32_t        frameSel;
            uint32_t        flags;
        } lockZSL;
        struct {
            /* IC_EVENT_TYPE_CAPTURE */
            uint32_t        sourceInstance;
            void            *buff;
            uint32_t        pConfigStruct;
            uint32_t        flags;
        } capture;
        struct {
            /* IC_EVENT_TYPE_UNLOCK_ZSL */
            uint32_t        sourceInstance;
            void                *buff;
        } unlockZSL;
        struct {
            /* IC_EVENT_TYPE_OPEN_MIPI_CLIENT */
            uint32_t        instance;
            void            *userData;
            uint32_t        dataType;
            uint32_t        max_dataSize;
        } openMipiTxClient;
        struct {
            /* IC_EVENT_TYPE_MIPI_CLIENT_REGISTERED */
            uint32_t        instance;
            void           *clientHandle; // NULL if open fails
            void           *userData;
            uint32_t        buffSize;    // data size that will be used from mipi Tx on each send
        } mipiTxClientRegistered;
        struct {
            /* IC_EVENT_TYPE_CLOSE_MIPI_CLIENT */
            uint32_t        instance;
            void            *clientHandle;
            void            *userData;
        } closeMipiTxClient;
        struct {
            /* IC_EVENT_TYPE_MIPI_CLIENT_UNREGISTERED */
            uint32_t        instance;
            void            *clientHandle;
            void            *userData;
        } mipiTxClientUnregistered;
        struct {
            /* IC_EVENT_TYPE_SEND_MIPI */
            uint32_t        instance;
            void            *clientHandle;
            void            *userData;
            uint32_t        dataSize;
            void*           buff;
        } sendMipiData;
        struct {
            /* IC_EVENT_TYPE_MIPI_SENT */
            uint32_t        instance;
            void            *clientHandle;
            void            *userData;
            uint32_t        dataSize;
            void*           buff;
        } mipiDataSent;
    } u;

    volatile uint32_t   ctrl;   /* See IC_EVENT_CTRL_* */
} icEvent;

#ifndef IC_EVENT_QUEUE_SIZE
#define IC_EVENT_QUEUE_SIZE    32
#endif 

typedef struct {
    int32_t                 readIdx;  //updated only by consumer 
    int32_t                 writeIdx; //updated only by producer 
    icEvent                 eventQ[IC_EVENT_QUEUE_SIZE];
} icEventQueue;

#define IC_ZSL_BUF_SIZE_MAX     8

struct icCtrlStruct;

typedef struct {
    void    *maptr;     /* Pointer returned by malloc() */
    void    *ptr;       /* Aligned pointer */
    int32_t width;
    int32_t height;
    int32_t lineStride;
    int32_t planeStride;
    int32_t planes;
} icFrame;

/* Primary IPIPE control structure shared between IPIPE and Client */
typedef struct icCtrlStruct {
    pthread_t               LeonRTThread;
    pthread_mutex_t         condMutexOut;
    pthread_cond_t          condOut;
    pthread_mutex_t         condMutexIn;
    pthread_cond_t          condIn;
    int32_t                 losInterruptPending;    /* LeonOS "interrupt" */
    int32_t                 lrtInterruptPending;    /* LeonRT "interrupt" */
    pthread_mutex_t         senMutex;               /* mutex to serialize access to devware */

    /*
     * Current time.  This is maintained on the client side.  It is
     * used by IPIPE to timestamp frames.  This field is only written
     * by OS Leon (atomically, not Read-modify-write!) and only ever
     * read (never written) by RT Loen.
     */
    volatile icTimestamp    curTime;

    /* High-priority command from Client side */
    volatile int32_t        hpCmd;

    /* Exception generated by IPIPE */
    volatile int32_t        exception;

    /*
     * Physical DDR area reserved for frame buffers - frame buffers
     * are allocated by IPIPE (LeonRT side)
     */
    void                    *framepoolBase;
    uint32_t                framepoolSize;

    icGlobalConfig          globalConfig;

    icSourceConfig          sourceConfig[2];
    icSourceConfigDynamic   sourceConfigDynamic[2];

    /*
     * When IPIPE detects an invalid parameter in the icIspConfig
     * structure, it sets these two fields.  The first field indicates
     * the line number at which the error was detected (internally,
     * the __LINE__ compiler macro is used) and the second field
     * indicates the ISP instance (see icIspInstance) which received
     * the invalid parameter.
     */
    volatile uint32_t       ispInvalidLine;
    volatile uint32_t       ispInvalidInstance;

    /*
     * Event Queues.  There are two queues - Out (Client -> IPIPE) and
     * In (IPIPE -> Client).
     */
    icEventQueue            eventQOut; //Client->Server
    icEventQueue            eventQIn;  //Server->Client
 
} icCtrl;

/* Assert/Debug/Panic/Warn macros */
#if !defined(__sparc)
#define IC_ASSERT(x)    { if (!(x)) { IC_PRINTF("IC_ASSERT fail: \"%s\" @ line %d in %s\n", #x, __LINE__, __FILE__); abort(); } }
#define IC_PANIC(m, x)  { IC_PRINTF("IC_PANIC: %s (0x%08x) @ line %d in %s\n", (m), (x), __LINE__, __FILE__); abort(); }
#define IC_PRINTF   printf
#else //Myriad:
#define IC_ASSERT(x)    { if (!(x)) { IC_PRINTF("IC_ASSERT fail: \"%s\" @ line %d in %s\n", #x, __LINE__, __FILE__); asm("ta 1"); } }
#define IC_PANIC(m, x)  { IC_PRINTF("IC_PANIC fail: %s (0x%08x) @ line %d in %s\n", (m), (x), __LINE__, __FILE__); asm("ta 1"); }
#define IC_PRINTF   printf
#endif

#define IC_WARN(m, x)   { IC_PRINTF("IC_WARN: %s (0x%08x) @ line %d in %s\n", (m), (x), __LINE__, __FILE__); }

icCtrl *icSetup();
int32_t icConfigureGlobal       (icCtrl *ctrl, icGlobalConfig *config);
int     icConfigureSource       (icCtrl *ctrl, icSourceInstance source, icSourceConfig *config);
int     icConfigureSourceDynamic(icCtrl *ctrl, icSourceInstance source, icSourceConfigDynamic *config);
int     icStartSource           (icCtrl *ctrl, icSourceInstance source);
int     icStopSource            (icCtrl *ctrl, icSourceInstance source);
int     icConfigureIsp          (icCtrl *ctrl, icIspInstance isp, icIspConfig *cfg, uint32_t seqNo);
int     icLockZSL               (icCtrl *ctrl, icSourceInstance source, uint32_t frameSel, icLockZSLFlags flags);
int     icTriggerCapture        (icCtrl *ctrl, icSourceInstance source, void * buff,icIspConfig *cfg, icCaptureFlags flags);
int     icUnlockZSL             (icCtrl *ctrl, icSourceInstance source);
int     icTeardown              (icCtrl **ctrl);
int     icIsEventPending        (icCtrl *ctrl);
int     icGetEvent              (icCtrl *ctrl, icEvent *ev);
void    icSendHpCommand         (icCtrl *ctrl, icHpCmd cmd);

void icOpenMipiClient(icCtrl *ctrl, uint32_t instance, uint32_t dataType, uint32_t dataSizeMax, void* userData);
void icCloseMipiClient(icCtrl *ctrl, uint32_t instance, void* clientHandle, void* userData);
void icSendTxClientData(icCtrl *ctrl, uint32_t instance, void* clientHandle, void * inBuffer, uint32_t buffSize, void* userData);

#ifdef __cplusplus
}
#endif

#endif  /* _IPIPE_H */
