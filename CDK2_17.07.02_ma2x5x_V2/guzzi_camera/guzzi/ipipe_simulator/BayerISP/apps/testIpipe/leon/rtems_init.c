#ifndef PC_BUILD

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <rtems.h>

#include <semaphore.h>
#include <pthread.h>
#include <sched.h>
#include <fcntl.h>
#include <ipipe.h>

#define CMX_CONFIG      (0x66666666)
#define L2CACHE_NORMAL_MODE (0x6)  // In this mode the L2Cache acts as a cache for the DRAM
#define L2CACHE_CFG     (L2CACHE_NORMAL_MODE)
#define BIGENDIANMODE   (0x01000786)
/* Sections decoration is require here for downstream tools */
uint32_t __cmx_config  __attribute__((section(".cmx.ctrl"))) = CMX_CONFIG;
uint32_t __l2_config   __attribute__((section(".l2.mode")))  = L2CACHE_CFG;

int main(void);

void
POSIX_Init(void *args)
{
	exit(main());
}

#if !defined (__CONFIG__)
#define __CONFIG__

/* ask the system to generate a configuration table */
#define CONFIGURE_INIT


#define CONFIGURE_MICROSECONDS_PER_TICK 1000 /* 1 millisecond */
#define CONFIGURE_TICKS_PER_TIMESLICE 1 /* 1 millisecond */

//#define RTEMS_POSIX_API

//#define CONFIGURE_APPLICATION_NEEDS_CONSOLE_DRIVER
#define CONFIGURE_APPLICATION_NEEDS_CLOCK_DRIVER

#define CONFIGURE_POSIX_INIT_THREAD_TABLE

//#define  RTEMS_MINIMUM_STACK_SIZE 8192
#define  CONFIGURE_MINIMUM_TASK_STACK_SIZE 2048

#define CONFIGURE_MAXIMUM_TASKS                       2

#define CONFIGURE_MAXIMUM_POSIX_THREADS              16
#define CONFIGURE_MAXIMUM_POSIX_MUTEXES              16
#define CONFIGURE_MAXIMUM_POSIX_KEYS                 16
#define CONFIGURE_MAXIMUM_POSIX_SEMAPHORES           10

#define CONFIGURE_APPLICATION_DISABLE_FILESYSTEM

#include <rtems/confdefs.h>

#endif /* if defined CONFIG */

#endif /* !PC_BUILD */
