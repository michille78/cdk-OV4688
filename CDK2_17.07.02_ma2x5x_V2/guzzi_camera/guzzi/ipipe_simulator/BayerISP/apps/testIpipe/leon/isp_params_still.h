// GENERATED TEST FILE (OCTAVE dump) !

//Input image dimensions:
#define ISPC_BAYER_W_STILL	4208
#define ISPC_BAYER_H_STILL	3120
#define ISPC_BAYER_BITS_STILL	10

// AWB digital gains are 8.8 fixed-point format
#define ISPC_GAIN_R_STILL	0x01de	// 8.8 bits
#define ISPC_GAIN_G_STILL	0x0100	// 8.8 bits
#define ISPC_GAIN_B_STILL	0x016e	// 8.8 bits

// Demosaic
#define ISPC_DEMOSAIC_MIX_SLOPE       	 640
#define ISPC_DEMOSAIC_MIX_OFFSET      	 491
#define ISPC_DEMOSAIC_MIX_GRADIENT_MUL	 128

// Chroma generation
#define ISPC_CHROMA_GEN_EPSILON       	0.003922

// Median
#define ISPC_CHROMA_MEDIAN_SIZE       	7

// Median Mix
#define ISPC_CHROMA_MEDIAN_MIX_SLOPE  	15.000000
#define ISPC_CHROMA_MEDIAN_MIX_OFFSET 	-0.100000

// Purple flare reduction
#define ISPC_PURPLE_FLARE_STRENGTH    	8.000000

// LowPass
extern float ispcLowpassKernel[9];

// Sharpen 
#define ISPC_SHARP_SIGMA           	2.500000
#define ISPC_SHARP_STRENGTH_DARKEN 	0.600000
#define ISPC_SHARP_STRENGTH_LIGHTEN	1.000000
#define ISPC_SHARP_ALPHA           	0.700000
#define ISPC_SHARP_OVERSHOOT       	1.100000
#define ISPC_SHARP_UNDERSHOOT      	1.000000
#define ISPC_SHARP_RANGE_STOP_0    	1.000000
#define ISPC_SHARP_RANGE_STOP_1    	5.000000
#define ISPC_SHARP_RANGE_STOP_2    	250.000000
#define ISPC_SHARP_RANGE_STOP_3    	255.000000
#define ISPC_SHARP_MIN_THR         	0.005000

// Luma denoise params:
#define ISPC_LUMA_DNS_STRENGTH	35.000000
#define ISPC_LUMA_DNS_ALPHA   	76	// 8bit value

// The following coefs are mirrored by the HW to obtain 7x7 matrix
// The 16 coefs represent the top-left square of a 7x7 symetrical matrix
// Each entry is 2 bits, and represents a left shift applied to
// the weight at the corresponding location.
#define ISPC_LUMA_DNS_F2         0x00000000

extern uint8_t ispcYDnsDistLut[256];
extern uint8_t ispcYDnsGammaLut[256];

#define ISPC_LUMA_DNS_REF_SHIFT	15

// Luma random noise params:
#define ISPC_LUMA_RAND_NOISE_STRENGTH	0.060000

extern float ispcLtmCurves[15*9];

// Pre-filter for Local Tone Mapping:
#define ISPC_LTM_FILTER_TH1    25 
#define ISPC_LTM_FILTER_TH2    0 
#define ISPC_LTM_FILTER_LIMIT  25 

// Chroma denoise params:
#define ISPC_CHROMA_DNS_TH_R    23 
#define ISPC_CHROMA_DNS_TH_G    9 
#define ISPC_CHROMA_DNS_TH_B    19 
#define ISPC_CHROMA_DNS_LIMIT   5 
#define ISPC_CHROMA_DNS_H_ENAB  1 

// Grey desaturate params:
#define ISPC_GREY_DESAT_OFFSET -2
#define ISPC_GREY_DESAT_SLOPE  30

extern float ispcCCM[9];

#define ISPC_DESAT_T1  0.003922
#define ISPC_DESAT_MUL 255.000000

extern float ispcGammaTable[4096];
