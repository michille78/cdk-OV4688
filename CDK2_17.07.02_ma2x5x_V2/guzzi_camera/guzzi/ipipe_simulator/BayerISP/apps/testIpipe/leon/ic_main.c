///
/// @file
/// @copyright All code copyright Movidius Ltd 2014, all rights reserved.
///			For License Warranty see: common/license.txt
///
/// @brief	 Test app main()
///

#include <stdlib.h>
#include <time.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>
#include <signal.h>
#include <assert.h>

#include <ipipe.h>

#include <utils/mms_debug.h>
#include <utils/profile/profile.h>

#include "isp_params_pp.h"
#include "isp_params_still.h"

#include "ic_mipi_tx_client.h"

//#define PRINT_LOS_RECEIVED_MESSAGES
//#define PRINT_LOS_SENT_MESSAGES

mmsdbg_define_variable(
        vdl_ic,
        DL_DEFAULT,
        0,
        "vdl_ic",
        "Guzzi IC."
    );
#define MMSDEBUGLEVEL mmsdbg_use_variable(vdl_ic)

typedef struct
{
    unsigned int lrt_ready;
    unsigned int ref_cnt;
    icCtrl  *ctrl;
}g_ipipe_ctx_t;

static g_ipipe_ctx_t gctx = {0, 0, NULL};

void icSleepMs(unsigned ms);


static pthread_t	eventThread;
static volatile int     stopped = 0;
static volatile int     source_started = 0;

#ifdef PRINT_LOS_RECEIVED_MESSAGES

static const char* evt2txt[] =
{
    /* IPIPE -> Client */
 "IC_EVENT_TYPE_READOUT_START", // =           0,
 "IC_EVENT_TYPE_READOUT_END", // =             1,
 "IC_EVENT_TYPE_LINE_REACHED", // =            2,
 "IC_EVENT_TYPE_ISP_START", // =               3,
 "IC_EVENT_TYPE_ISP_END", // =                 4,
 "IC_EVENT_TYPE_ZSL_LOCKED", // =              5,
 "IC_EVENT_TYPE_STATS_READY  ", // =           6,
 "IC_EVENT_TYPE_SOURCE_STOPPED", // =          7,
 "IC_EVENT_TYPE_ERROR", // =                   8,
 "IC_EVENT_TYPE_LEON_RT_READY", // =           9,
 "IC_EVENT_TYPE_SOURCE_READY", // =            10,

     /* Client -> IPIPE */
 "IC_EVENT_TYPE_CONFIG_GLOBAL", // =           11,
 "IC_EVENT_TYPE_CONFIG_SOURCE", // =           12,
 "IC_EVENT_TYPE_CONFIG_SOURCE_DYNAMIC", // =   13,
 "IC_EVENT_TYPE_CONFIG_ISP", // =              14,
 "IC_EVENT_TYPE_START_SOURCE", // =            15,
 "IC_EVENT_TYPE_STOP_SOURCE", // =             16,
 "IC_EVENT_TYPE_LOCK_ZSL", // =                17,
 "IC_EVENT_TYPE_CAPTURE", // =                 18,
 "IC_EVENT_TYPE_UNLOCK_ZSL", // =              19,

 "IC_EVENT_TYPE_TEAR_DOWN", // =               20,
 };
#endif // PRINT_LOS_RECEIVED_MESSAGES

typedef struct {
    icCtrl  *ctrl;
    void *guzzi_private;
}evt_loop_args_t;

void inc_cam_stats_ready(void* p_prv, unsigned int seqNo, void* p_cfg_prv);
void inc_cam_capture_ready(void* p_prv, unsigned int seqNo, void* p_cfg_prv);
void inc_cam_ipipe_cfg_ready(void* p_prv, unsigned int seqNo, void* p_cfg_prv);
void inc_cam_ipipe_buff_locked(void* p_prv, void *userData,
                                    unsigned int sourceInstance,
                                    void*         buffZsl,
                                    icTimestamp   ts,
                                    unsigned int seqNo);

void inc_cam_frame_start( void* p_prv,
                             uint32_t sourceInstance,
                             uint32_t seqNo,
                             icTimestamp ts,
                             void *userData);

void inc_cam_frame_line_reached( void* p_prv,
                                 uint32_t sourceInstance,
                                 uint32_t seqNo,
                                 icTimestamp ts,
                                     void *userData);

void inc_cam_frame_end(  void* p_prv,
                         uint32_t sourceInstance,
                         uint32_t seqNo,
                         icTimestamp ts,
                         void *userData);




void icMipiTxClientRegistered(uint32_t instance, void* clientHandle, uint32_t buffSize, void* userData);
void icMipiTxClientUnregistered(uint32_t instance, void* clientHandle, void* userData);
void icMipiTxClientDataSent(uint32_t instance, void* clientHandle, uint32_t dataSize, void* buff, void* userData);


static int ip_running = 0;
/*
 * Should run at a relatively high priority so that the event queue doesn't
 * fill up (if it did, we would drop events).
 */
static void *
eventLoop(void *vCtrl)
{
    evt_loop_args_t *args = vCtrl;
	icCtrl	*ctrl = args->ctrl;
	void *guzzi_private = args->guzzi_private;
	icEvent	ev;
    unsigned int     evno;

    free(args);

	/*
	 * This thread runs until it is cancelled (pthread_cond_wait() is a
	 * cancellation point).
	 */
	while (1) {
		icGetEvent(ctrl, &ev);

        evno = ev.ctrl & IC_EVENT_CTRL_TYPE_MASK;

#ifdef PRINT_LOS_RECEIVED_MESSAGES
		if (evno < sizeof (evt2txt) / sizeof (evt2txt[0])) {
			printf("App EvtLoop: Got event (%d) %s\n",
			    evno, evt2txt[evno]);
		} else {
			printf("**** App EvtLoop: Bad event: %d ****\n", evno);
			continue;
		}
#endif // PRINT_LOS_RECEIVED_MESSAGES
		switch (evno) {
        case IC_EVENT_TYPE_READOUT_START:
            if (ev.u.lineEvent.userData != NULL)
            {
                PROFILE_ADD(PROFILE_ID_LRT_START_FRAME, ev.u.statsReady.userData, ev.u.statsReady.seqNo);
                inc_cam_frame_start( guzzi_private,
                                 ev.u.lineEvent.sourceInstance,
                                 ev.u.lineEvent.seqNo,
                                 ev.u.lineEvent.ts,
                                 ev.u.lineEvent.userData);
            }
            break;

        case IC_EVENT_TYPE_LINE_REACHED:
            if (ev.u.lineEvent.userData != NULL)
            {
                PROFILE_ADD(PROFILE_ID_LRT_LINE_REACHED, ev.u.statsReady.userData, ev.u.statsReady.seqNo);
                inc_cam_frame_line_reached( guzzi_private,
                                           ev.u.lineEvent.sourceInstance,
                                           ev.u.lineEvent.seqNo,
                                           ev.u.lineEvent.ts,
                                           ev.u.lineEvent.userData);
            }
            break;

        case IC_EVENT_TYPE_READOUT_END:
            if (ev.u.lineEvent.userData != NULL)
            {
                PROFILE_ADD(PROFILE_ID_LRT_END_FRAME, ev.u.statsReady.userData, ev.u.statsReady.seqNo);
                inc_cam_frame_end( guzzi_private,
                                   ev.u.lineEvent.sourceInstance,
                                   ev.u.lineEvent.seqNo,
                                   ev.u.lineEvent.ts,
                                   ev.u.lineEvent.userData);
            }
            break;

		case IC_EVENT_TYPE_STATS_READY:
            IC_ASSERT(ev.u.statsReady.userData != NULL);
            if (ev.u.statsReady.userData != NULL)
            {
                PROFILE_ADD(PROFILE_ID_LRT_STATS_READY, ev.u.statsReady.userData, ev.u.statsReady.seqNo);
                inc_cam_stats_ready(guzzi_private,
                                         ev.u.statsReady.seqNo,
                                         ev.u.statsReady.userData
                                         );
            }
            break;
        case IC_EVENT_TYPE_ISP_START:
            if (ev.u.ispEvent.userData != NULL)
            {
                PROFILE_ADD(PROFILE_ID_LRT_ISP_START, ev.u.statsReady.userData, ev.u.statsReady.seqNo);
                inc_cam_ipipe_cfg_ready(guzzi_private,
                                        ev.u.ispEvent.seqNo,
                                        ev.u.ispEvent.userData);
		    } else
		    {
                printf("ERROR: IC_EVENT_TYPE_ISP_START with userdata == NULL\n");
            }
            break;
        case IC_EVENT_TYPE_ISP_CONFIG_ACCEPTED:
            if (ev.u.ispEvent.userData != NULL)
            {
                PROFILE_ADD(PROFILE_ID_LRT_ISP_CFG_ACEPT, ev.u.statsReady.userData, ev.u.statsReady.seqNo);
                inc_cam_ipipe_cfg_ready(guzzi_private,
                                        ev.u.ispEvent.seqNo,
                                        ev.u.ispEvent.userData);
	        }
		    break;
        case IC_EVENT_TYPE_ISP_END:
            if (ev.u.ispEvent.userData != NULL)
            {
                PROFILE_ADD(PROFILE_ID_LRT_ISP_END, ev.u.statsReady.userData, ev.u.statsReady.seqNo);
                inc_cam_capture_ready(guzzi_private,
                                      ev.u.ispEvent.seqNo,
                                      ev.u.ispEvent.userData);
            } else
            {
                printf("ERROR: IC_EVENT_TYPE_ISP_END with userdata == NULL\n");
            }
            break;
        case IC_EVENT_TYPE_ZSL_LOCKED:
            PROFILE_ADD(PROFILE_ID_LRT_ZSL_LOCKED, ev.u.statsReady.userData, ev.u.statsReady.seqNo);
            inc_cam_ipipe_buff_locked(guzzi_private,
                                        ev.u.buffLockedZSL.userData,
                                        ev.u.buffLockedZSL.sourceInstance,
                                        ev.u.buffLockedZSL.buffZsl,
                                        ev.u.buffLockedZSL.ts,
                                        ev.u.buffLockedZSL.seqNo);
		    break;
		case IC_EVENT_TYPE_SOURCE_STOPPED:
			IC_ASSERT(ev.u.sourceEvent.sourceInstance == IC_SOURCE_RFC);
			source_started = 0;
			break;

		case IC_EVENT_TYPE_LEON_RT_READY:
		    ip_running = 1;
		    break;

		case IC_EVENT_TYPE_TEAR_DOWN:
		    return NULL;

        case IC_EVENT_TYPE_MIPI_CLIENT_REGISTERED:
#ifdef PRINT_LOS_RECEIVED_MESSAGES
            printf ("Unhandled evt MIPI CLIENT REGISTERED Hndl: %x Size: %d\n",
                    ev.u.mipiTxClientRegistered.clientHandle,
                    ev.u.mipiTxClientRegistered.buffSize
                    );
#endif // PRINT_LOS_RECEIVED_MESSAGES
            icMipiTxClientRegistered(ev.u.mipiTxClientRegistered.instance,
                                     ev.u.mipiTxClientRegistered.clientHandle,
                                     ev.u.mipiTxClientRegistered.buffSize,
                                     ev.u.mipiTxClientRegistered.userData
                                     );
            break;

        case IC_EVENT_TYPE_MIPI_SENT:
#ifdef PRINT_LOS_RECEIVED_MESSAGES
            printf ("Unhandled evt MIPI SENT Hndl: %x Size: %d buff %p\n",
                    ev.u.mipiDataSent.clientHandle,
                    ev.u.mipiDataSent.dataSize,
                    ev.u.mipiDataSent.buff
                    );
#endif // PRINT_LOS_RECEIVED_MESSAGES
            icMipiTxClientDataSent(ev.u.mipiDataSent.instance,
                                   ev.u.mipiDataSent.clientHandle,
                                   ev.u.mipiDataSent.dataSize,
                                   ev.u.mipiDataSent.buff,
                                   ev.u.mipiDataSent.userData
                                   );
            break;

        case IC_EVENT_TYPE_MIPI_CLIENT_UNREGISTERED:
#ifdef PRINT_LOS_RECEIVED_MESSAGES
            printf ("Unhandled evt MIPI CLIENT UNREGISTERED Hndl: %x\n",
                    ev.u.mipiTxClientUnregistered.clientHandle
                    );
#endif // PRINT_LOS_RECEIVED_MESSAGES

            icMipiTxClientUnregistered(ev.u.mipiTxClientUnregistered.instance,
                                     ev.u.mipiTxClientUnregistered.clientHandle,
                                     ev.u.mipiTxClientUnregistered.userData
                                     );

            break;

        case IC_EVENT_TYPE_SOURCE_READY:
            IC_ASSERT(ev.u.sourceEvent.sourceInstance == IC_SOURCE_RFC);
            source_started = 1;
            break;

        default:
            if (IC_EVENT_TYPE_ERROR != evno)  // in PC implementation during start up and tear down many errors are generated
                printf ("%s: Unhandled evt %d:\n", __func__, evno);
            break;
		}
	}
	return NULL;
}

void los_start(void *arg)
{
	int		rc;
    evt_loop_args_t *args;

#ifdef PRINT_LOS_SENT_MESSAGES
    printf("%s:\n",__func__);
#endif // PRINT_LOS_SENT_MESSAGES
    if (gctx.ref_cnt == 0) {
#ifdef PRINT_LOS_SENT_MESSAGES
        printf("%s: icSetup\n",__func__);
#endif // PRINT_LOS_SENT_MESSAGES
        if (!gctx.lrt_ready) {
            gctx.lrt_ready = 1;
            gctx.ctrl = icSetup();
        }

#ifdef PRINT_LOS_SENT_MESSAGES
        printf("%s: Ctrl %p arg %p\n",__func__, gctx.ctrl, arg);
#endif // PRINT_LOS_SENT_MESSAGES
        args = malloc(sizeof(evt_loop_args_t));
        args->guzzi_private = arg;
        args->ctrl = gctx.ctrl;

        /*
         * Create the Event thread.  This thread blocks waiting for events
         * from the IPIPE client API.
         */
        if ((rc = pthread_create(&eventThread, NULL, eventLoop, args)) != 0) {
            printf("pthread_create: %s\n", strerror(rc));
            exit(1);
        }
        while (!ip_running)
        {
            icSleepMs(1);
        }
    }
    gctx.ref_cnt++;
}

void los_stop(void)
{

    if (gctx.ref_cnt) {
        gctx.ref_cnt--;
        if (0 == gctx.ref_cnt)
        {
            icTeardown(&gctx.ctrl);

            /* Stop the event loop thread and wait for it to exit */
            {
                int ipSendEvent(icCtrl *ctrl, icEvent *ev);
                icEvent ev;
                ev.ctrl = IC_EVENT_TYPE_TEAR_DOWN;
                ipSendEvent(gctx.ctrl, &ev);
            }
            pthread_join(eventThread, NULL);
            ip_running = 0;
        }
    }
}

void los_ConfigureGlobal(icGlobalConfig *gconf)
{
#ifdef PRINT_LOS_SENT_MESSAGES
    printf("%s:\n",__func__);
#endif // PRINT_LOS_SENT_MESSAGES

    icConfigureGlobal(gctx.ctrl, gconf);
}


void los_ConfigureSource(int srcIdx, icSourceConfig *sconf, int pipeId)
{
#ifdef PRINT_LOS_SENT_MESSAGES
    printf("%s:\n",__func__);
#endif // PRINT_LOS_SENT_MESSAGES

    icConfigureSource(gctx.ctrl, IC_SOURCE_RFC, sconf);

#ifdef PRINT_LOS_SENT_MESSAGES
    printf("%s:END\n",__func__);
#endif // PRINT_LOS_SENT_MESSAGES
}

void los_ipipe_LockZSL(void)
{
#ifdef PRINT_LOS_SENT_MESSAGES
    printf("%s:\n",__func__);
#endif // PRINT_LOS_SENT_MESSAGES
    PROFILE_ADD(PROFILE_ID_LOS_LOCK_ZSL, 0, 0);
    icLockZSL(gctx.ctrl, IC_SOURCE_RFC, 0, IC_LOCKZSL_TS_RELATIVE);
}

void los_ipipe_TriggerCapture(void * buff, icIspConfig *iconf)
{
#ifdef PRINT_LOS_SENT_MESSAGES
    printf("%s:\n",__func__);
#endif // PRINT_LOS_SENT_MESSAGES
     PROFILE_ADD(PROFILE_ID_LOS_TRIGGER_CAPTURE, buff, 0);
    icTriggerCapture(gctx.ctrl, IC_SOURCE_RFC, buff, iconf, IC_CAPTURE_SEND_YUV);
}

void los_configIsp(void *iconf, int ispIdx)
{
#ifdef PRINT_LOS_SENT_MESSAGES
    printf("%s: cfg %p, userData %p\n",__func__, iconf, iconf->userData);
#endif // PRINT_LOS_SENT_MESSAGES
    icConfigureIsp(gctx.ctrl, IC_ISP_RFC_VIDEO, iconf, ISP_SEQNO_IGNORED);
}

int  los_isen_start (void)
{
#ifdef PRINT_LOS_SENT_MESSAGES
    printf("%s:\n",__func__);
#endif // PRINT_LOS_SENT_MESSAGES
    icStartSource(gctx.ctrl, IC_SOURCE_RFC);
    while (!source_started) {
        sleep(1);
    }
    return 0;
}

int  los_isen_stop  (void)
{
    stopped = 0;
#ifdef PRINT_LOS_SENT_MESSAGES
    printf("%s:\n",__func__);
#endif // PRINT_LOS_SENT_MESSAGES

    icStopSource(gctx.ctrl, IC_SOURCE_RFC);

    while (source_started) {
        sleep(1);
    }
    return 0;
}

int  los_startSource(int srcIdx)
{
    los_isen_start();
}

int  los_stopSource (int srcIdx)
{
    los_isen_stop();
}


void los_openMipiClient(void* hndl, uint32_t dataType, uint32_t dataSizeMax)
{
    losMipiTxMipiClient_t *losHndl = (losMipiTxMipiClient_t *)hndl;

    assert((losHndl)&&(losHndl->state == IC_MIPI_TX_STATE_CREATED));

    losHndl->state = IC_MIPI_TX_STATE_OPENING;
    icOpenMipiClient(gctx.ctrl, losHndl->reg_params.instance, dataType, dataSizeMax, losHndl);
}

void los_closeMipiClient(void* hndl)
{
    losMipiTxMipiClient_t *losHndl = (losMipiTxMipiClient_t *)hndl;

    assert((losHndl)&&(losHndl->state == IC_MIPI_TX_STATE_IN_USE));

    losHndl->state = IC_MIPI_TX_STATE_CLOSING;
    icCloseMipiClient(gctx.ctrl, losHndl->reg_params.instance, losHndl->lrt_clientHandle, losHndl);
}

void los_sendTxClientData(void* hndl, void* inBuffer, uint32_t buffSize)
{
    losMipiTxMipiClient_t *losHndl = (losMipiTxMipiClient_t *)hndl;

    assert((losHndl)&&(losHndl->state == IC_MIPI_TX_STATE_IN_USE));

    icSendTxClientData(gctx.ctrl, losHndl->reg_params.instance, losHndl->lrt_clientHandle, inBuffer, buffSize, losHndl);
}

void dw_set_sensor_reg(uint32_t r_addr, uint32_t r_val);
void dw_get_sensor_reg(uint32_t r_addr, uint32_t *r_val);

#define DWARE_LINE_PER_uSEC    (87076.0/1000000)

void los_sensor_set_reg(uint32_t r_addr, uint32_t r_val)
{
    printf("Sensor WR addr %d, val %d\n", r_addr, r_val);
    pthread_mutex_lock(&gctx.ctrl->senMutex);
    dw_set_sensor_reg(r_addr, r_val);
    pthread_mutex_unlock(&gctx.ctrl->senMutex);
}

void los_sensor_get_reg(uint32_t r_addr, uint32_t *r_val)
{
    pthread_mutex_lock(&gctx.ctrl->senMutex);
    dw_get_sensor_reg(r_addr, r_val);
    pthread_mutex_unlock(&gctx.ctrl->senMutex);
    printf("Sensor RD addr %d, val %d\n", r_addr, *r_val);
}
