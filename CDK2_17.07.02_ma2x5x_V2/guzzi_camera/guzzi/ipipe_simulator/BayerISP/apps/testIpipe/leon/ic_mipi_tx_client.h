/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file ic_mipi_tx_client.h
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! Dec 9, 2014 : Author aiovtchev
*! Created
* =========================================================================== */



#ifndef LEON_IC_MIPI_TX_CLIENT_H_
#define LEON_IC_MIPI_TX_CLIENT_H_

#include <osal/osal_stdtypes.h>
#include <osal/osal_list.h>


typedef enum {
    IC_MIPI_TX_STATE_CREATED,
    IC_MIPI_TX_STATE_CLOSING,
    IC_MIPI_TX_STATE_OPENING,
    IC_MIPI_TX_STATE_IN_USE
}mipi_tx_client_state_t;


typedef void(*cbMipiTxClientOpenDone_t)(void* cl_prv, uint32_t buffSize);
typedef void(*cbMipiTxClientCloseDone_t)(void* cl_prv);
typedef void(*cbMipiTxClientDataSent_t)(void* cl_prv, uint32_t dataSize, void* buff);

typedef struct {
    uint32_t instance;
    cbMipiTxClientOpenDone_t  cb_mipi_tx_open;
    cbMipiTxClientCloseDone_t cb_mipi_tx_close;
    cbMipiTxClientDataSent_t  cb_mipi_tx_sent;
    void* client_private;
} losRegisterMipiTxClient_t;

typedef struct {
    struct list_head node;
    void* lrt_clientHandle;
    losRegisterMipiTxClient_t reg_params;
    mipi_tx_client_state_t state;
} losMipiTxMipiClient_t;

/*==================== Client Interface ====================*/
void* los_registerMipiClient(losRegisterMipiTxClient_t * reg_params);
void los_unregisterMipiClient(void* hndl);
void los_openMipiClient(void* hndl, uint32_t dataType, uint32_t dataSizeMax);
void los_closeMipiClient(void* hndl);
void los_sendTxClientData(void* hndl, void * inBuffer, uint32_t buffSize);
/*==================== Client Interface ====================*/


#endif /* LEON_IC_MIPI_TX_CLIENT_H_ */
