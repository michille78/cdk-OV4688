///
/// @file
/// @copyright All code copyright Movidius Ltd 2014, all rights reserved.
///			For License Warranty see: common/license.txt
///
/// @brief	 Test app main()
///

#include <stdlib.h>
#include <time.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>
#include <signal.h>

#include <ipipe.h>

#include "isp_params_pp.h"
#include "isp_params_still.h"

#define CAMERA_FRAME_WIDTH	4208
#define CAMERA_FRAME_HEIGHT	3120

typedef struct {
} frameReq_t;

static frameReq_t	frameReq;
static pthread_t	eventThread;
static volatile int	stopped, zslLocked;

const char* evt2txt[] =
{
    /* IPIPE -> Client */
     "IC_EVENT_TYPE_READOUT_START",
     "IC_EVENT_TYPE_READOUT_END",
     "IC_EVENT_TYPE_LINE_REACHED",
     "IC_EVENT_TYPE_ISP_START",
     "IC_EVENT_TYPE_ISP_END",
     "IC_EVENT_TYPE_ZSL_LOCKED",
     "IC_EVENT_TYPE_STATS_READY",
     "IC_EVENT_TYPE_SOURCE_STOPPED",
     "IC_EVENT_TYPE_ISP_CONFIG_ACCEPTED",
     "IC_EVENT_TYPE_ERROR",
     "IC_EVENT_TYPE_LEON_RT_READY",

     /* Client -> IPIPE */
     "IC_EVENT_TYPE_CONFIG_SOURCE",
     "IC_EVENT_TYPE_CONFIG_SOURCE_DYNAMIC",
     "IC_EVENT_TYPE_CONFIG_ISP",
     "IC_EVENT_TYPE_START_SOURCE",
     "IC_EVENT_TYPE_STOP_SOURCE",
     "IC_EVENT_TYPE_STOP_LOCK_ZSL",
     "IC_EVENT_TYPE_CAPTURE",
     "IC_EVENT_TYPE_STOP_UNLOCK_ZSL",
 };

float rgb2rgb_mtrx[] = {
	1.76998139,      -0.56750098,     -0.20151332,
	-0.29251874,     1.84288990,      -0.55056754,
	-0.05341927,     -0.70319087,     1.75662643,
};

//#define GAMMA_LIN

#ifdef GAMMA_LIN
uint16_t gamma_lin[1024];
#else
uint16_t gamma_tbl[] = {
	#include "gamma_1.txt"
};
#endif

/*
 * Should run at a relatively high priority so that the event queue doesn't
 * fill up (if it did, we would drop events).
 */
static void *
eventLoop(void *vCtrl)
{
	icCtrl	*ctrl = vCtrl;
	icEvent	ev;
	int     evno;

	/*
	 * This thread runs until it is cancelled (pthread_cond_wait() is a
	 * cancellation point).
	 */
	while (1) {
		icGetEvent(ctrl, &ev);

		evno = ev.ctrl & IC_EVENT_CTRL_TYPE_MASK;

		if (evno < sizeof (evt2txt) / sizeof (evt2txt[0])) {
			printf("App EvtLoop: Got event (%d) %s\n",
			    evno, evt2txt[evno]);
		} else {
			printf("**** App EvtLoop: Bad event: %d ****\n", evno);
			continue;
		}

		switch (evno) {
		case IC_EVENT_TYPE_STATS_READY:
			IC_ASSERT(ev.u.statsReady.userData == &frameReq);
			break;
		case IC_EVENT_TYPE_ZSL_LOCKED:
			IC_ASSERT(ev.u.lineEvent.userData == &frameReq);
			zslLocked = 1;
			break;
		case IC_EVENT_TYPE_SOURCE_STOPPED:
			IC_ASSERT(ev.u.sourceStopped.sourceInstance == IC_SOURCE_RFC);
			stopped = 1;
			break;
		}
	}
	return NULL;
}

static void
timerTick(union sigval sval)
{
	icCtrl	*ctrl = sval.sival_ptr;

	ctrl->curTime++;
}

/* Want a timer tick every millisecond */
/* TBD: Is 1ms resolution enough?  What resolution can RTEMS give us? */
#define INTERVAL_US 1000

static void
initTimer(icCtrl *ctrl)
{
	timer_t			timer;
	struct sigevent		sev;
	struct itimerspec	its;

	sev.sigev_notify = SIGEV_THREAD;
	sev.sigev_value.sival_ptr = ctrl;
	sev.sigev_notify_function = timerTick;
	sev.sigev_notify_attributes = NULL;
	if (timer_create(CLOCK_REALTIME, &sev, &timer) != 0) {
		perror("timer_create");
		exit(1);
	}

	its.it_interval.tv_sec = 0;
	its.it_interval.tv_nsec = INTERVAL_US*1000;
	its.it_value.tv_sec = 0;
	its.it_value.tv_nsec = INTERVAL_US*1000;

	ctrl->curTime = 0;

	if (timer_settime(timer, 0, &its, NULL) != 0) {
		perror("timer_settime");
		exit(1);
	}
}

void
getIspConf(icIspConfig *conf)
{
	unsigned int i;
	float		maxval;
	int32_t		f1Coeffs[] = { 44, 288, -200, 16, 28, 16, 368, -212, 536, -1076, 536 };
	int32_t		f2Coeffs[] = { 44, -288, -200, 104, 204, 104, -368, -212, 76, -152, 76 };

memset(conf, 0, sizeof (*conf));

	conf->blc.gr = ISPC_BLACK_LEVEL_GR;
	conf->blc.r  = ISPC_BLACK_LEVEL_R;
	conf->blc.b  = ISPC_BLACK_LEVEL_B;
	conf->blc.gb = ISPC_BLACK_LEVEL_GB;

	conf->lsc.lscWidth = ISPC_LSC_GAIN_MAP_W;
	conf->lsc.lscHeight = ISPC_LSC_GAIN_MAP_H;
	conf->lsc.pLscTable = ispcLscMesh;

	/*
	 * Use Raw filter gains to recover full [0, 1023] range after
	 * Black Level subtract.
	 */
	maxval = (1<<ISPC_RAW_OUTPUT_BITS) - 1;
	conf->raw.gainGr = maxval / (maxval-ISPC_BLACK_LEVEL_GR) * 256;
	conf->raw.gainR  = maxval / (maxval-ISPC_BLACK_LEVEL_R ) * 256;
	conf->raw.gainB  = maxval / (maxval-ISPC_BLACK_LEVEL_B ) * 256;
	conf->raw.gainGb = maxval / (maxval-ISPC_BLACK_LEVEL_GB) * 256;

	conf->raw.clampGr = ISPC_RAW_CLAMP_GR;
	conf->raw.clampR  = ISPC_RAW_CLAMP_R;
	conf->raw.clampB  = ISPC_RAW_CLAMP_B;
	conf->raw.clampGb = ISPC_RAW_CLAMP_GB;

	conf->raw.grgbImbalPlatDark = ISPC_GRGB_IMBAL_PLAT_DARK;
	conf->raw.grgbImbalDecayDark = ISPC_GRGB_IMBAL_DECAY_DARK;
	conf->raw.grgbImbalPlatBright = ISPC_GRGB_IMBAL_PLAT_BRIGHT;
	conf->raw.grgbImbalDecayBright = ISPC_GRGB_IMBAL_DECAY_BRIGHT;
	conf->raw.grgbImbalThr = ISPC_GRGB_IMBAL_THRESHOLD;

	conf->raw.dpcAlphaHotG = ISPC_BAD_PIX_ALPHA_G_HOT;
	conf->raw.dpcAlphaHotRb = ISPC_BAD_PIX_ALPHA_RB_HOT;
	conf->raw.dpcAlphaColdG = ISPC_BAD_PIX_ALPHA_G_COLD;
	conf->raw.dpcAlphaColdRb = ISPC_BAD_PIX_ALPHA_RB_COLD;

	conf->raw.dpcNoiseLevel  = ISPC_BAD_PIX_NOISE_LEVEL;

	conf->raw.outputBits = ISPC_RAW_OUTPUT_BITS;

	conf->aeAwbConfig.firstPatchX = 0;
	conf->aeAwbConfig.firstPatchY = 0;
	conf->aeAwbConfig.patchWidth = 32;
	conf->aeAwbConfig.patchHeight = 32;
	conf->aeAwbConfig.patchGapX = 128;
	conf->aeAwbConfig.patchGapY = 128;
	conf->aeAwbConfig.nPatchesX = 27;
	conf->aeAwbConfig.nPatchesY = 20;

	conf->afConfig.firstPatchX = 0;
	conf->afConfig.firstPatchY = 0;
	conf->afConfig.patchWidth = 32;
	conf->afConfig.patchHeight = 32;
    conf->afConfig.nPatchesX   = 27;
    conf->afConfig.nPatchesY   = 20;
	conf->afConfig.initialSubtractionValue = 0;
	conf->afConfig.f1Threshold = 0;
	conf->afConfig.f2Threshold = 0;
	for (i = 0; i < 11; i++) {
		conf->afConfig.f1Coeffs[i] = f1Coeffs[i];
		conf->afConfig.f2Coeffs[i] = f2Coeffs[i];
	}

	/* Level of saturated pixels, normalized to range [0, 0xffff] */
	conf->aeAwbConfig.satThresh = 0xffff;

	/* White balance */
	conf->wbGains.gainR = ISPC_GAIN_R;
	conf->wbGains.gainG = ISPC_GAIN_G;
	conf->wbGains.gainB = ISPC_GAIN_B;

	conf->demosaic.dewormSlope = ISPC_DEMOSAIC_MIX_SLOPE;
	conf->demosaic.dewormOffset = ISPC_DEMOSAIC_MIX_OFFSET;
	conf->demosaic.dewormGradientMul = ISPC_DEMOSAIC_MIX_GRADIENT_MUL;

	conf->chromaGen.epsilon = ISPC_CHROMA_GEN_EPSILON * 255 + .5;

	conf->lumaDenoise.alpha = ISPC_LUMA_DNS_ALPHA;
	conf->lumaDenoise.strength = ISPC_LUMA_DNS_STRENGTH * 256;
	conf->lumaDenoise.f2 = ISPC_LUMA_DNS_F2;

	memcpy(conf->lumaDenoiseRef.lutDist, ispcYDnsDistLut, 256);
	memcpy(conf->lumaDenoiseRef.lutGamma, ispcYDnsGammaLut, 256);
	conf->lumaDenoiseRef.shift = ISPC_LUMA_DNS_REF_SHIFT;

	conf->randomNoise.strength = ISPC_LUMA_RAND_NOISE_STRENGTH * 255;

	for (i = 0; i < 15*9; i++) {
		conf->ltm.curves[i] = ispcLtmCurves[i] * 0xffff;
	}

	conf->ltmLBFilter.th1 = ISPC_LTM_FILTER_TH1;
	conf->ltmLBFilter.th2 = ISPC_LTM_FILTER_TH2;
	conf->ltmLBFilter.limit = ISPC_LTM_FILTER_LIMIT;

	conf->chromaDenoise.th_r = ISPC_CHROMA_DNS_TH_R;
	conf->chromaDenoise.th_g = ISPC_CHROMA_DNS_TH_G;
	conf->chromaDenoise.th_b = ISPC_CHROMA_DNS_TH_B;
	conf->chromaDenoise.limit = ISPC_CHROMA_DNS_LIMIT;
	conf->chromaDenoise.hEnab = ISPC_CHROMA_DNS_H_ENAB;

	conf->median.kernelSize = ISPC_CHROMA_MEDIAN_SIZE;

	conf->medianMix.slope = ISPC_CHROMA_MEDIAN_MIX_SLOPE * 4096;
	conf->medianMix.offset = ISPC_CHROMA_MEDIAN_MIX_OFFSET * 4096;

	conf->purpleFlare.strength = ISPC_PURPLE_FLARE_STRENGTH * 256;

	conf->greyDesat.slope = ISPC_GREY_DESAT_SLOPE;
	conf->greyDesat.offset = ISPC_GREY_DESAT_OFFSET;

	for (i = 0; i < 9; i++) {
		conf->lowpass.kernel[i] = ispcLowpassKernel[i] * 256;
	}

	conf->sharpen.sigma = ISPC_SHARP_SIGMA * 256;
	conf->sharpen.strength_darken = ISPC_SHARP_STRENGTH_DARKEN * 256;
	conf->sharpen.strength_lighten = ISPC_SHARP_STRENGTH_LIGHTEN * 256;
	conf->sharpen.alpha = ISPC_SHARP_ALPHA * 255;
	conf->sharpen.overshoot = ISPC_SHARP_OVERSHOOT * 256;
	conf->sharpen.undershoot = ISPC_SHARP_UNDERSHOOT * 256;
	conf->sharpen.rangeStop0 = ISPC_SHARP_RANGE_STOP_0;
	conf->sharpen.rangeStop1 = ISPC_SHARP_RANGE_STOP_1;
	conf->sharpen.rangeStop2 = ISPC_SHARP_RANGE_STOP_2;
	conf->sharpen.rangeStop3 = ISPC_SHARP_RANGE_STOP_3;
	conf->sharpen.minThr = ISPC_SHARP_MIN_THR * 255;

	conf->colorCombine.desat_mul = ISPC_DESAT_MUL;
	conf->colorCombine.desat_t1 = ISPC_DESAT_T1 * 256;
	conf->colorCombine.kR = 3*256;
	conf->colorCombine.kG = 3*256;
	conf->colorCombine.kB = 3*256;
	conf->colorCombine.epsilon = ISPC_CHROMA_GEN_EPSILON * (1<<12);
	for (i=0;i<9;i++) {
		conf->colorCombine.ccm[i] = (int16_t)(rgb2rgb_mtrx[i]*1024);
	}

#ifdef GAMMA_LIN
	conf->gamma.table = gamma_lin;
	{
		uint16_t *p, i;

		p = gamma_lin;
		for (i=0;i<1024;i++) {
			*p++ = (uint16_t)(i/4);
		}
	}
	conf->gamma.size = 1024;
#else
	conf->gamma.table = gamma_tbl;	// HARD CODED IN ip_process.cpp 10->8 gamma
	conf->gamma.size = sizeof (gamma_tbl) / sizeof (gamma_tbl[0]);
#endif
	conf->gamma.rangetop = gamma_tbl[conf->gamma.size - 1];
	conf->userData = &frameReq;
}

int
main()
{
	int		rc;
	icCtrl		*ctrl;
	icGlobalConfig	gconf;
	icSourceConfig	sconf;
	icIspConfig	iconf;

	ctrl = icSetup();

	/*
	 * Create the Event thread.  This thread blocks waiting for events
	 * from the IPIPE client API.
	 */
	if ((rc = pthread_create(&eventThread, NULL, eventLoop, ctrl)) != 0) {
		printf("pthread_create: %s\n", strerror(rc));
		exit(1);
	}

	initTimer(ctrl);

	sleep(2);

	gconf.zslNBufs = 4;
	gconf.rfcRawOutputSize.w = CAMERA_FRAME_WIDTH;
	gconf.rfcRawOutputSize.h = CAMERA_FRAME_HEIGHT;
	icConfigureGlobal(ctrl, &gconf);

	sconf.cameraOutputSize.w = CAMERA_FRAME_WIDTH;
	sconf.cameraOutputSize.h = CAMERA_FRAME_HEIGHT;
	sconf.cropWindow.x1 = 0;
	sconf.cropWindow.y1 = 0;
	sconf.cropWindow.x2 = CAMERA_FRAME_WIDTH-1;
	sconf.cropWindow.y2 = CAMERA_FRAME_HEIGHT-1;
	sconf.bayerFormat   = IC_BAYER_FORMAT_GRBG;
	sconf.bitsPerPixel  = 10;
	icConfigureSource(ctrl, IC_SOURCE_RFC, &sconf);

	getIspConf(&iconf);
	icConfigureIsp(ctrl, IC_ISP_RFC_VIDEO, &iconf, ISP_SEQNO_IGNORED);

	icStartSource(ctrl, IC_SOURCE_RFC);

	sleep(30);

	zslLocked = 0;

	icLockZSL(ctrl, IC_SOURCE_RFC, 0, IC_LOCKZSL_TS_RELATIVE);

	while (!zslLocked) {
	}

	icTriggerCapture(ctrl, IC_SOURCE_RFC, &iconf, IC_CAPTURE_SEND_YUV);

	stopped = 0;
	icStopSource(ctrl, IC_SOURCE_RFC);

	while (!stopped) {
	}

	icTeardown(ctrl);

	/* Stop the event loop thread and wait for it to exit */
	pthread_cancel(eventThread);
	pthread_join(eventThread, NULL);

	return 0;
}
