///
/// @file
/// @copyright All code copyright Movidius Ltd 2014, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Entry point for LeonRT code - *not* an RTEMS app!
///

#include <stdio.h>

#include "ip_internal.h"

#ifndef PC_BUILD
#include <swcLeonUtils.h>
#include <DrvLeonL2C.h>
#else
#include <string.h>
#include "x11_disp.h"
#endif


/*
 * Place the shared structure in CMX, but keep it out of the BSS, so that
 * it doesn't get zeroed when we start LeonRT.
 */
icCtrl g_icCtrl __attribute__((section(".shbss")));

ipInternal_t	ipInternal;

/*
 * The following is OR'd in to an address to access CMX from Leon,
 * bypassing the cache.
 */
#define BYPASS_CACHE	0x08000000

#ifdef PC_BUILD
#define CAM_ENV_VAR     "CAM_DATA_PATH"

void cam_get_data_path(void)
{
    char *cp;
    const char *env = CAM_ENV_VAR;

    cp = getenv(env);
    if (!cp)
    {
        char file_path[512];

        printf ("\nERROR: PLease specify environment variable %s\n", CAM_ENV_VAR);

        strcpy(file_path, __FILE__);
        cp = strstr(file_path, "/BayerISP/");
        if (cp) {
            strcpy(cp, "/data/");
            cp = file_path;
        } else {
            cp = "./";
        }
    }
    strcpy(ipInternal.cam_base_dir, cp);
}

void * lrt_start_sim(void *arg)
#else
int main(void)
#endif
{
#ifdef PC_BUILD
	icCtrl	*ctrl = &g_icCtrl;
#else
	/* Get a pointer to g_icCtrl that bypasses the cache */
	icCtrl	*ctrl = (icCtrl *)((uint32_t)&g_icCtrl | BYPASS_CACHE);
#endif

	printf("Hello from LeonRT!\n");
	printf("RT: Ctrl %x base %x size %d\n", (uint32_t)ctrl,
	    (uint32_t)ctrl->framepoolBase, (uint32_t)ctrl->framepoolSize);

#ifdef PC_BUILD
	cam_get_data_path();
#endif
	{
	    icEvent ev;
	    ev.ctrl = IC_EVENT_TYPE_LEON_RT_READY;
	    ipSendEvent(ctrl, &ev);
	}
	/*
	 * LeonRT main loop.  For now, it's a busy worker thread, which
	 * periodically checks for events (the latency of handling events
	 * will be much higher here then in the final Myriad implementation).
	 */
	while (1) {
		uint32_t	cmd;

		if (ipInternal.rfc.running) {
			ipDoProcessing(ctrl);
		} else {
				/* Nothing to do - wait for an interrupt */
#ifdef PC_BUILD
			pthread_mutex_lock(&ctrl->condMutexOut);

			while (ctrl->lrtInterruptPending == 0) {
				pthread_cond_wait(&ctrl->condOut, &ctrl->condMutexOut);
			}

			ctrl->lrtInterruptPending = 0;

			pthread_mutex_unlock(&ctrl->condMutexOut);
#else
#pragma message("TODO: implement sleep / wake on interrupt on Leon RT")
#endif
		}
		cmd = ctrl->hpCmd;

		switch (cmd) {
		case IC_HP_CMD_QUIESCE:
			ipQuiesce(ctrl);
			ctrl->hpCmd = IC_HP_CMD_NONE;
			break;
		case IC_HP_CMD_HALT:
			ipQuiesce(ctrl);

printf("Halt!\n");

#ifdef PC_BUILD
            dw_destroy(ipInternal.rfc.h_cam);
            x11_disp_destroy();
            x11_h3a_disp_destroy();
            x11_h3a_af_disp_destroy();
            tearDown_ipProcess();
#endif
			/* Services of LeonRT no longer required */
			return 0;
		case IC_HP_CMD_NONE:
			/*
			 * No high-priority events.  Handle normal events,
			 * if there are any pending.
			 */
			ipReapEventQ(ctrl);
			break;
		default:
			IC_PANIC("Invalid High Prio command", cmd);
			break;
		}
	}

	return 0;
}
