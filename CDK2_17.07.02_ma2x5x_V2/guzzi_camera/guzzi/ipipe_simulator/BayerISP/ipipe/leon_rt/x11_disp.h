#ifndef _X11_DISP_H
#define _X11_DISP_H


#ifdef __cplusplus
extern "C" {
#endif

typedef void * x11_disp_t;

typedef enum {
    DISP_IMAGE_FORMAT_UYVY,
    DISP_IMAGE_FORMAT_NV12,
    DISP_IMAGE_FORMAT_RGB42,
    DISP_IMAGE_FORMAT_RGB888_16BITS
} disp_image_format_t;

typedef struct {
    char *display_name;
    unsigned int width;
    unsigned int height;
    unsigned int scale;
    disp_image_format_t format;
} x11_disp_create_t;

void x11_disp_show(void *img, uint32_t frame_number);
void x11_disp_destroy(void);
void x11_disp_create(x11_disp_create_t *create);

void x11_h3a_disp_destroy(void);
void x11_disp_show_h3a(void* stats, unsigned int pix_pax);
void x11_h3a_disp_create(x11_disp_create_t *create);

void x11_h3a_af_disp_destroy(void);
void x11_disp_show_h3a_af(void* stats, unsigned int offs);
void x11_h3a_af_disp_create(x11_disp_create_t *create);

#ifdef __cplusplus
}
#endif

#endif /* _X11_DISP_H */

