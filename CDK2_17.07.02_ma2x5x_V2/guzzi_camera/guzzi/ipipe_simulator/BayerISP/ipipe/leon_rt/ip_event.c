///
/// @file
/// @copyright All code copyright Movidius Ltd 2014, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Event queue handling
///

#include "ip_internal.h"

void
ipKickLeonOS(icCtrl *ctrl)
{
#ifdef PC_BUILD
	pthread_mutex_lock(&ctrl->condMutexIn);
	ctrl->losInterruptPending = 1;
	pthread_cond_signal(&ctrl->condIn);
	pthread_mutex_unlock(&ctrl->condMutexIn);
#else
#pragma message("ipKickLeonOS() not implemented for Myriad2 yet!")
#endif
}
// ip -> ic error
void
icErrorEvent(icCtrl *ctrl, uint32_t errorNo, uint32_t severity)
{
	icEvent	ev;

	ev.u.error.errorNo = errorNo;
	ev.ctrl = IC_EVENT_TYPE_ERROR;
	ipSendEvent(ctrl, &ev);
}

void
ipReapEventQ(icCtrl *ctrl)
{
	icEvent			*evPtr = &ctrl->eventQOut.eventQ[ctrl->eventQOut.readIdx];
	uint32_t		type;

	while (1) {
		if (!(evPtr->ctrl & IC_EVENT_CTRL_OWNED)) {
			return;
		}

		type = evPtr->ctrl & IC_EVENT_CTRL_TYPE_MASK;

		switch (type) {
		case IC_EVENT_TYPE_CONFIG_SOURCE:
			switch (evPtr->u.configureSource.sourceInstance) {
			case IC_SOURCE_RFC:
				ipInternal.rfc.width =
				    ctrl->sourceConfig[0].cropWindow.x2 -
				    ctrl->sourceConfig[0].cropWindow.x1;
				ipInternal.rfc.height =
				    ctrl->sourceConfig[0].cropWindow.y2 -
				    ctrl->sourceConfig[0].cropWindow.y1;
				ipInternal.rfc.src_fmt = ctrl->sourceConfig[0].bayerFormat;

				if (ipInternal.rfc.h_cam)
				{
				    if ((ipInternal.rfc.dw_width != ipInternal.rfc.width) ||
				        (ipInternal.rfc.dw_height != ipInternal.rfc.height))
				    {
				        printf ("\nCRITICAL ERROR! devware size (%dx%d) is different than expected (%dx%d)\n",
				                ipInternal.rfc.dw_width, ipInternal.rfc.dw_height,
				                ipInternal.rfc.width, ipInternal.rfc.height);
				        exit (1);
				    }
				}
				break;
#ifdef IC_SUPPORT_FFC
			case IC_SOURCE_FFC:
				ipInternal.ffc.width =
				    ctrl->sourceConfig[1].cropWindow.x2 -
				    ctrl->sourceConfig[1].cropWindow.x1 + 1;
				ipInternal.ffc.height =
				    ctrl->sourceConfig[1].cropWindow.y2 -
				    ctrl->sourceConfig[1].cropWindow.y1 + 1;
				break;
#endif
			}
			break;
		case IC_EVENT_TYPE_CONFIG_SOURCE_DYNAMIC:
			switch (evPtr->u.configureSource.sourceInstance) {
			case IC_SOURCE_RFC:
				ipInternal.rfc.notificationLine =
				    ctrl->sourceConfigDynamic[0].notificationLine;
				break;
#ifdef IC_SUPPORT_FFC
			case IC_SOURCE_FFC:
				ipInternal.ffc.notificationLine =
				    ctrl->sourceConfigDynamic[1].notificationLine;
				break;
#endif
			}
			break;
		case IC_EVENT_TYPE_CONFIG_ISP: {
			struct ipStreamState	*pSS;

			switch (evPtr->u.configureIsp.ispInstance) {
			case IC_ISP_RFC_VIDEO:
				pSS = &ipInternal.rfcVidStream;
				break;
			case IC_ISP_RFC_STILL:
				pSS = &ipInternal.rfcStillStream;
				break;
#ifdef IC_SUPPORT_FFC
			case IC_ISP_FFC_VIDEO:
				pSS = &ipInternal.ffcVidStream;
				break;
#endif
			default:
				IC_PANIC("Bad ispInstance",
				    evPtr->u.configureIsp.ispInstance);
				return;
			}
			pSS->pendingConfig =
			    (icIspConfig *)evPtr->u.configureIsp.pConfigStruct;
			pSS->pendingSeqNo = evPtr->u.configureIsp.seqNo;
//printf("IPIPE: Rcv Config OK %d\n", pSS->pendingConfig->waitSeqNo);
			break;
		}
		case IC_EVENT_TYPE_START_SOURCE: {
			int	started = 0;
			switch (evPtr->u.start.sourceInstance) {
			case IC_SOURCE_RFC:
				ipInternal.rfc.running = 1;
				started = 1;
				{
                icEvent ev;
                    ev.u.sourceEvent.sourceInstance = evPtr->u.start.sourceInstance;
                    ev.ctrl = IC_EVENT_TYPE_SOURCE_READY;
                    ipSendEvent(ctrl, &ev);
                }
				break;
#ifdef IC_SUPPORT_FFC
			case IC_SOURCE_FFC:
				ipInternal.ffc.running = 1;
				started = 1;
				break;
#endif
			}
			if (!started) {
				icErrorEvent(ctrl,
				    IC_ERROR_BAD_PARAMETER, IC_SEVERITY_NORMAL);
			}
printf("Start\n");
			break;
		}
		case IC_EVENT_TYPE_STOP_SOURCE: {
			int	stopped = 0;

			switch (evPtr->u.start.sourceInstance) {
			case IC_SOURCE_RFC:
				ipInternal.rfc.running = 0;
				stopped = 1;
				break;
#ifdef IC_SUPPORT_FFC
			case IC_SOURCE_FFC:
				ipInternal.ffc.running = 0;
				stopped = 1;
				break;
#endif
			}
printf("Stop\n");

			if (stopped) {
				icEvent	ev;
				ev.u.sourceEvent.sourceInstance =
				    evPtr->u.start.sourceInstance;
				ev.ctrl = IC_EVENT_TYPE_SOURCE_STOPPED;
				ipSendEvent(ctrl, &ev);
			} else {
				icErrorEvent(ctrl,
				    IC_ERROR_BAD_PARAMETER, IC_SEVERITY_NORMAL);
			}
			break;
		}
		case IC_EVENT_TYPE_LOCK_ZSL: {
			ipLockZSL(ctrl, evPtr->u.lockZSL.sourceInstance,
			                 evPtr->u.lockZSL.frameSel,
			                 evPtr->u.lockZSL.flags);
			break;
		}
		case IC_EVENT_TYPE_CAPTURE: {
			ipCapture(ctrl,
			          (icIspConfig *)evPtr->u.capture.pConfigStruct,
			          evPtr->u.capture.buff,
			          evPtr->u.capture.flags);
			break;
		}
		case IC_EVENT_TYPE_UNLOCK_ZSL: {
			ipUnlockZSL(ctrl, evPtr->u.unlockZSL.buff);
			break;
		}
        case IC_EVENT_TYPE_CONFIG_GLOBAL: {
            ipConfigureGlobal(ctrl);
            break;
        }

        case IC_EVENT_TYPE_OPEN_MIPI_CLIENT: {
        icEvent ev;
/*
            printf ("Open Mipi Client inst %d clHndl 1 dataSz %d userData %p\n",
                    evPtr->u.openMipiTxClient.instance,
                    evPtr->u.openMipiTxClient.max_dataSize,
                    evPtr->u.openMipiTxClient.userData);
*/
            ev.u.mipiTxClientRegistered.instance = evPtr->u.openMipiTxClient.instance;
            ev.u.mipiTxClientRegistered.clientHandle = (void*)1;
            ev.u.mipiTxClientRegistered.buffSize = evPtr->u.openMipiTxClient.max_dataSize;
            ev.u.mipiTxClientRegistered.userData = evPtr->u.openMipiTxClient.userData;

            ev.ctrl = IC_EVENT_TYPE_MIPI_CLIENT_REGISTERED;
            ipSendEvent(ctrl, &ev);
            break;
        }

        case IC_EVENT_TYPE_CLOSE_MIPI_CLIENT: {
        icEvent ev;
/*
            printf ("Close Mipi Client inst %d clHndl %d userData %p\n",
                    evPtr->u.closeMipiTxClient.instance,
                    evPtr->u.closeMipiTxClient.clientHandle,
                    evPtr->u.closeMipiTxClient.userData);
*/
            ev.u.mipiTxClientUnregistered.instance = evPtr->u.closeMipiTxClient.instance;
            ev.u.mipiTxClientUnregistered.clientHandle = evPtr->u.closeMipiTxClient.clientHandle;
            ev.u.mipiTxClientUnregistered.userData = evPtr->u.closeMipiTxClient.userData;

            ev.ctrl = IC_EVENT_TYPE_MIPI_CLIENT_UNREGISTERED;
            ipSendEvent(ctrl, &ev);
            break;
        }

        case IC_EVENT_TYPE_SEND_MIPI: {
        icEvent ev;
/*
            printf ("Send Mipi Client inst %d clHndl %d userData %p Data %p Sz %d\n",
                    evPtr->u.sendMipiData.instance,
                    evPtr->u.sendMipiData.clientHandle,
                    evPtr->u.sendMipiData.userData,
                    evPtr->u.sendMipiData.buff,
                    evPtr->u.sendMipiData.dataSize
                    );
*/
            ev.u.mipiDataSent.instance = evPtr->u.sendMipiData.instance;
            ev.u.mipiDataSent.clientHandle = evPtr->u.sendMipiData.clientHandle;
            ev.u.mipiDataSent.userData = evPtr->u.sendMipiData.userData;
            ev.u.mipiDataSent.buff = evPtr->u.sendMipiData.buff;
            ev.u.mipiDataSent.dataSize = evPtr->u.sendMipiData.dataSize;

            ev.ctrl = IC_EVENT_TYPE_MIPI_SENT;
            ipSendEvent(ctrl, &ev);
            break;
        }


		default:
			IC_PANIC("IPIPE received invalid event", type);
		}

		/* Release the ring buffer entry back to the producer */
		evPtr->ctrl = 0;

		/* Advance the ring queue ptr */
		if (++ctrl->eventQOut.readIdx == IC_EVENT_QUEUE_SIZE) {
		    ctrl->eventQOut.readIdx = 0;
			evPtr = ctrl->eventQOut.eventQ;
		} else {
			evPtr++;
		}
	}
}

/* This function never blocks - it drops the event if the Queue is full */
int
ipSendEvent(icCtrl *ctrl, icEvent *ev)
{
	icEvent		*eqPtr = &ctrl->eventQIn.eventQ[ctrl->eventQIn.writeIdx];
	unsigned	i;
//printf("RT: Send event %d\n", ev->ctrl);

    while (eqPtr->ctrl & IC_EVENT_CTRL_OWNED)
    {
        printf ("ERROR: Can't send Event\n");
        {
            struct timespec t;
            t.tv_sec = 0;
            t.tv_nsec = 100*1000*1000;
            nanosleep(&t, NULL);
        }
    }

	/*
	 * Copy the event - "ctrl" field must be set last, since this gives
	 * ownership of the ring buffer entry to the consumer.
	 */
	for (i = 0; i < sizeof (ev->u) / 4; i++) {
		((uint32_t *)eqPtr)[i] = ((uint32_t *)ev)[i];
	}
	eqPtr->ctrl = ev->ctrl | IC_EVENT_CTRL_OWNED;
	/* Advance the ring queue ptr */
	if (++ctrl->eventQIn.writeIdx == IC_EVENT_QUEUE_SIZE) {
		ctrl->eventQIn.writeIdx = 0;
	}
	/* If the LeonOS is idle, wake it up so it checks for a new event */
	ipKickLeonOS(ctrl);

	return 0;
}
