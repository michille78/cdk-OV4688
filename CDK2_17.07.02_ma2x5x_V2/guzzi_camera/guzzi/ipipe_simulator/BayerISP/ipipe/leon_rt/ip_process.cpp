#ifdef PC_BUILD
///
/// @file
/// @copyright All code copyright Movidius Ltd 2014, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     "IPIPE" core processing
///

#if !defined(__sparc)
#define IP_ENABLE_SIPP_PROCESSING
//#define IP_ENABLE_RAW_FILTER
#endif // spark

#ifdef IP_ENABLE_SIPP_PROCESSING
#include <sipp.h>
//#include <sippTestCommon.h>	/* Temporary workaround - see Bug #20014 */
#else
#undef IP_ENABLE_RAW_FILTER
#endif // IP_ENABLE_SIPP_PROCESSING
#include <string.h>
#include <errno.h>
#include <sys/time.h>
#include <unistd.h>
#include <math.h>

#include "ip_internal.h"
#include "ipipe.h"
#include "x11_disp.h"
#include "zslQueue.h"

#include <math.h>
//#define DEBUG_IP_PROCESS

#ifndef DEBUG_IP_PROCESS
#define  printf(...)
#endif
// Extra border
#define DEMOS_BORDER_ROWS   8
#define DEMOS_BORDER_COLS   8

/*  AF statistics scaling on display  */
#define STATS_SCALE    4

/* AF Statistics display selection
 * offset in icAfStatsPatch structure
 */
#define     AF_DATA_SUM             0   // offset = 0 -  uint32_t    sum;   /* Sum of all values in the patch prior to filtering */
#define     AF_FILTER1_ABOVE_THR    1   // offset = 1 -  f1AboveThreshold;  /* Number of pixels that were above the threshold for Filter 1 */
#define     AF_FILTER1_SUM          2   // offset = 2 -  f1Sum;             /* Sum of all Filter 1 output values which were above the threshold for filter 1 */
#define     AF_FILTER1_MAX          3   // offset = 3 -  f1SumRowMaximums;  /* Sum of per-row maximums of filter1 output */
#define     AF_FILTER2_ABOVE_THR    4   // offset = 4 -  f2AboveThreshold;  /* Number of pixels that were above the threshold for Filter 2 */
#define     AF_FILTER2_SUM          5   // offset = 5 -  f2Sum;             /* Sum of all Filter 2 output values which were above the threshold for filter 2 */
#define     AF_FILTER2_MAX          6   // offset = 6 -  f2SumRowMaximums;  /* Sum of per-row maximums of filter2 output */

//#define AF_DATA_SELECT    AF_FILTER1_SUM
uint32_t AF_DATA_SELECT = AF_FILTER1_SUM;

void
ipSleepMs(unsigned ms)
{
    struct timespec t;
    t.tv_sec = 0;
    t.tv_nsec = ms*1000*1000;
    nanosleep(&t, NULL);
}

/*
 * Kludge "IPIPE" for initial integration.  Only performs LSC and stats.
 * Performs one step of the pipeline per invocation.  Attempts to generate
 * events just like the real IPIPE would.  For the moment it's breaking all
 * of the rules - allocating large private buffers, and doing whole-frame
 * operations.
 */

static uint16_t		*ibuf = NULL, *ibuf1 = NULL, *rawBuf = NULL, *demosOut = NULL, *zslBuf = NULL;
static  zslBuff_t* zslBuff = NULL;
zslBuff_t* capt_zslBuff = NULL;

#ifdef IP_ENABLE_SIPP_PROCESSING
static    DmaParam    *dmaInCfg, *dmaOutCfg;
#endif // IP_ENABLE_SIPP_PROCESSING

static int		initialised = 0;
static int     first_read = 1;

#ifdef IP_ENABLE_SIPP_PROCESSING
static SippPipeline	*pl;
static SippFilter	*lsc, *dmaIn, *dmaOut;
#ifdef IP_ENABLE_RAW_FILTER
static SippFilter	*raw;
#endif // IP_ENABLE_RAW_FILTER
#endif // IP_ENABLE_SIPP_PROCESSING

static icBayerFormat bayerOrder = IC_BAYER_FORMAT_RGGB;

#define FNAME	"input"

#define max(x, y) (((x) > (y)) ? (x) : (y))
#define min(x, y) (((x) < (y)) ? (x) : (y))

static void doBlc(void * out_buffer, void *in_buffer, icIspConfig *cfg);

/* Dump frame to a file - w is width in bytes */
static void
dumpFrame(const char *fname, int w, int h, int linestride, int nplanes,
    int planestride, uint8_t *data)
{
	FILE	*f = fopen(fname, "w");
	int	y, p;
	uint8_t	*lptr;

	if (f == NULL) {
		perror("fopen");
		return;
	}

	for (p = 0; p < nplanes; p++) {
		lptr = data;
		for (y = 0; y < h; y++) {
			fwrite(lptr, 1, w, f);
			lptr += linestride;
		}
		data += planestride;
	}

	fclose(f);
}

/*
 * ****************************************************************************
 * ** Color bars draw *********************************************************
 * ****************************************************************************
 */

typedef struct
{
    unsigned int R;
    unsigned int G;
    unsigned int B;
}PatternRGBColor;

typedef enum frameTypes
{
     YUV422i,   // interleaved 8 bit
     YUV420p,   // planar 4:2:0 format
     YUV422p,   // planar 8 bit
     YUV400p,   // 8-bit greyscale
     RGBA8888,  // RGBA interleaved stored in 32 bit word
     RGB888,    // Planar 8 bit RGB data
     LUT2,      // 1 bit  per pixel, Lookup table (used for graphics layers)
     LUT4,      // 2 bits per pixel, Lookup table (used for graphics layers)
     LUT16,     // 4 bits per pixel, Lookup table (used for graphics layers)
     RAW16,     // save any raw type (8, 10, 12bit) on 16 bits
     RAW8,
     YUV444i,
     NV12,
     NONE
}frameType;

typedef struct frameSpecs
{
     frameType      type;
     unsigned int   height;    // width in pixels
     unsigned int   width;     // width in pixels
     unsigned int   stride;    // defined as distance in bytes from pix(y,x) to pix(y+1,x)
     unsigned int	bytesPP;   // bytes per pixel (for LUT types set this to 1)
}frameSpec;

typedef struct frameElements
{
     frameSpec spec;
     unsigned char* p1;  // Pointer to first image plane
     unsigned char* p2;  // Pointer to second image plane (if used)
     unsigned char* p3;  // Pointer to third image plane  (if used)
} frameBuffer;

typedef struct {
    int R;
    int Gr;
    int Gb;
    int B;
} bayer_pattern_t;

typedef struct {
    unsigned short c[4];
} bayer_color_t;

#define COLOR_BARS_NUM ((int)(sizeof(defaultColor)/sizeof(defaultColor[0])))
#define BORDER 4

static PatternRGBColor defaultColor[] = {
    {255, 255, 255},
    {255, 255,   0},
    {  0, 200, 255},
    {  0, 128,   0},
    {255,   0, 255},
    {255,   0,   0},
    {  0,   0, 255},
    {  0,   0,   0}
};

static void converterRGBtoBAYER10(
        PatternRGBColor *rgb,
        bayer_color_t *bayer10,
        bayer_pattern_t *pattern
    )
{
    bayer10->c[pattern->R ] = (rgb->R << 2) | (rgb->R >> 6);
    bayer10->c[pattern->Gr] = (rgb->G << 2) | (rgb->G >> 6);
    bayer10->c[pattern->Gb] = (rgb->G << 2) | (rgb->G >> 6);
    bayer10->c[pattern->B ] = (rgb->B << 2) | (rgb->B >> 6);
}

static void draw_color_rect_bayer10(
        unsigned short *b,
        int w,
        int h,
        int ls,
        bayer_color_t *color
    )
{
    int ih, iw;

    for (ih = 0; ih < h/2; ih++) {
        for (iw = 0; iw < w; iw += 2) {
            b[iw           ] = color->c[0];
            b[iw + 1       ] = color->c[1];
            b[iw     + ls/2] = color->c[2];
            b[iw + 1 + ls/2] = color->c[3];
        }
        b += ls;
    }
}

#define BAYER10_POS2PTR(P, X, Y, LS) \
    ((unsigned short *)((unsigned char *)(P) + (Y)*(LS) + 2*(X)))

static void hcb_draw_bayer10(
        frameBuffer *frame_buffer,
        bayer_pattern_t *pattern
    )
{
    unsigned short *b;
    int w, h, ls, h_bar;
    int i;
    bayer_color_t color;

    w = frame_buffer->spec.width;
    h = frame_buffer->spec.height;
    ls = frame_buffer->spec.stride;
    h_bar = (h / COLOR_BARS_NUM) & ~1U;

    b = (unsigned short *)frame_buffer->p1;

    converterRGBtoBAYER10(defaultColor, &color, pattern);
    draw_color_rect_bayer10(
            BAYER10_POS2PTR(b, 0, 0, ls),
            w,
            h,
            ls,
            &color
        );
    for (i = 1; i < COLOR_BARS_NUM-1; i++) {
        converterRGBtoBAYER10(defaultColor + i, &color, pattern);
        draw_color_rect_bayer10(
                BAYER10_POS2PTR(b, BORDER, i*h_bar, ls),
                w - 2 * BORDER,
                h_bar,
                ls,
                &color
            );
    }
    converterRGBtoBAYER10(defaultColor + i, &color, pattern);
    draw_color_rect_bayer10(
            BAYER10_POS2PTR(b, BORDER, i*h_bar, ls),
            w - 2 * BORDER,
            h - i*h_bar - BORDER,
            ls,
            &color
        );
}

static void vcb_draw_bayer10(
        frameBuffer *frame_buffer,
        bayer_pattern_t *pattern
    )
{
    unsigned short *b;
    int w, h, ls, w_bar;
    int i;
    bayer_color_t color;

    w = frame_buffer->spec.width;
    h = frame_buffer->spec.height;
    ls = frame_buffer->spec.stride;
    w_bar = (w / COLOR_BARS_NUM) & ~1U;

    b = (unsigned short *)frame_buffer->p1;

    converterRGBtoBAYER10(defaultColor, &color, pattern);
    draw_color_rect_bayer10(
            BAYER10_POS2PTR(b, 0, 0, ls),
            w,
            h,
            ls,
            &color
        );
    for (i = 1; i < COLOR_BARS_NUM-1; i++) {
        converterRGBtoBAYER10(defaultColor + i, &color, pattern);
        draw_color_rect_bayer10(
                BAYER10_POS2PTR(b, i*w_bar, BORDER, ls),
                w_bar,
                h - 2 * BORDER,
                ls,
                &color
            );
    }
    converterRGBtoBAYER10(defaultColor + i, &color, pattern);
    draw_color_rect_bayer10(
            BAYER10_POS2PTR(b, i*w_bar, BORDER, ls),
            w - i*w_bar - BORDER,
            h - 2 * BORDER,
            ls,
            &color
        );
}

/*
 * ****************************************************************************
 * ** End of color bars draw **************************************************
 * ****************************************************************************
 */

static void
readFrame(void *out_buffer)
{
	uint16_t	*ptr = (uint16_t *)out_buffer;
	FILE		*f;
	unsigned	w = ipInternal.rfc.width, h = ipInternal.rfc.height, y;

	/* Just keep processing the same frame over and over, for now... */
#if !defined(__sparc)
	if (first_read) {
        char fname[2048];

        if (in_file_name == NULL) {
        	sprintf (fname, "%s/%s_%dx%d.raw", ipInternal.cam_base_dir, FNAME, w, h);
        } else {
        	sprintf (fname, "%s/%s", ipInternal.cam_base_dir, in_file_name);
        }
		    
		if ((f = fopen(fname, "r")) == NULL) {
			fprintf(stderr, "fopen: %s: %s\n",
			        fname, strerror(errno));
			exit(1);
		}

		for (y = 0; y < h; y++) {
			if (fread(ptr, 2, w, f) != w) {
				fprintf(stderr, "fread: %s: %s\n",
			        fname, strerror(errno));
				exit(1);
			}
			ptr += w;
		}

		fclose(f);

		first_read = 0;
	}
#else
    static int h_bars = 0;
//    static bayer_pattern_t pattern = { .R = 0, .Gr = 1, .Gb = 2, .B = 3 };
    static bayer_pattern_t pattern = { 1, 0, 3, 2 };

    frameBuffer fb;

    fb.p1 = (unsigned char *)out_buffer;
    fb.spec.width = w;
    fb.spec.height = h;
    fb.spec.stride = 2*fb.spec.width;

    if (h_bars) {
        hcb_draw_bayer10(&fb, &pattern);
    } else {
        vcb_draw_bayer10(&fb, &pattern);
    }
    h_bars = !h_bars;
#endif // !defined(__sparc)
}

static void
setup(icCtrl *ctrl, int w, int h)
{
	int		nbytes;


	printf("%s Start\n", __func__);
	nbytes = (ipInternal.rfc.width+40) * (ipInternal.rfc.height + 16) * 2;
	if ((ibuf = (uint16_t *)malloc(nbytes)) == NULL) {
		perror("malloc");
		exit(1);
	}
//	memset(ibuf, 0, nbytes);

    nbytes = (ipInternal.rfc.width+200) * ipInternal.rfc.height * 2;
	if ((zslBuf = (uint16_t *)malloc(nbytes)) == NULL) {
		perror("malloc");
		exit(1);
	}
//	memset(zslBuf, 0, nbytes);

    if ((ibuf1 = (uint16_t *)malloc(nbytes)) == NULL) {
        perror("malloc");
        exit(1);
    }
//    memset(ibuf1, 0, nbytes);

	if ((rawBuf = (uint16_t *)malloc(nbytes)) == NULL) {
		perror("malloc");
		exit(1);
	}
//	memset(rawBuf, 0, nbytes);

	zslQueue_create(4, nbytes);

	nbytes = (ipInternal.rfc.width/2 + 4) * ipInternal.rfc.height/2 * 2 * 3;
	if ((demosOut = (uint16_t *)malloc(nbytes)) == NULL) {
		perror("malloc");
		exit(1);
	}

//    memset(demosOut, 0, nbytes);

#ifdef IP_ENABLE_SIPP_PROCESSING
    extern void    sippPlatformInit();
	sippPlatformInit();	/* Temporary workaround - see Bug #20014 */

	/* Set up a SIPP pipeline to do LSC and RAW filtering */
	pl = sippCreatePipeline(0, 3, 0);

	dmaIn = sippCreateFilter(pl, 0, w, h,
	    N_PL(1), SZ(uint16_t), SIPP_AUTO, (FnSvuRun)SIPP_DMA_ID, 0);

	lsc = sippCreateFilter(pl, 0, w, h,
	    N_PL(1), SZ(uint16_t), SIPP_AUTO, (FnSvuRun)SIPP_LSC_ID, 0);
#ifdef IP_ENABLE_RAW_FILTER
	raw = sippCreateFilter(pl, 0, w, h,
	    N_PL(1), SZ(uint16_t), SIPP_AUTO, (FnSvuRun)SIPP_RAW_ID, 0);
#endif // IP_ENABLE_RAW_FILTER
	dmaOut = sippCreateFilter(pl, 0, w, h,
	    N_PL(1), SZ(uint16_t), SIPP_AUTO, (FnSvuRun)SIPP_DMA_ID, 0);

	sippLinkFilter(lsc,    dmaIn, 1, 1);
#ifdef IP_ENABLE_RAW_FILTER
	sippLinkFilter(raw,    lsc,   5, 5);
	sippLinkFilter(dmaOut, raw,   1, 1);
#else
	sippLinkFilter(dmaOut, lsc,   1, 1);
#endif // IP_ENABLE_RAW_FILTER

	dmaInCfg  = (DmaParam *)dmaIn->params;
	dmaOutCfg = (DmaParam *)dmaOut->params;

	sippDbgLevel(pl, 0);
#endif // IP_ENABLE_SIPP_PROCESSING
    printf("%s Finish\n", __func__);
}

void tearDown_ipProcess (void)
{
    free(rawBuf);
    free(demosOut);
    free(ibuf);
    free(ibuf1);
    free(zslBuf);
    zslQueue_destroy();
    initialised = 0;
}

extern float dbg_flash_sim_multiplier;
/* Black Level Subtraction (as performed by the Mipi Rx filter) */
static void
doBlc(void * out_buffer, void *in_buffer, icIspConfig *cfg)
{
	int		w = ipInternal.rfc.width, h = ipInternal.rfc.height;
	int		x, y, tmp;
	uint16_t	*iptr = (uint16_t *)in_buffer, *optr = (uint16_t *)out_buffer;
	int		blueCol, blueRow;

	switch (bayerOrder) {
	  case IC_BAYER_FORMAT_GRBG:
		blueCol = 0;
		blueRow = 1;
		break;
	  case IC_BAYER_FORMAT_GBRG:
		blueCol = 1;
		blueRow = 0;
		break;
	  case IC_BAYER_FORMAT_RGGB:
		blueCol = 1;
		blueRow = 1;
		break;
	  case IC_BAYER_FORMAT_BGGR:
		blueCol = 0;
		blueRow = 0;
		break;
	}

	for (y = 0; y < h; y++) {
		if ((y & 1) == blueRow) {
			/* BG or GB row */
			for (x = 0; x < w; x++) {
				if ((x & 1) == blueCol) {
					/* Blue pixel */
					tmp = *iptr++ - cfg->blc.b;

				} else {
					/* Green pixel on Blue row */
					tmp = *iptr++ - cfg->blc.gb;
				}
                tmp *= dbg_flash_sim_multiplier;
				if (tmp < 0) {
					tmp = 0;
				}
				*optr++ = tmp;
			}
		} else {
			/* GR or RG row */
			for (x = 0; x < w; x++) {
				if ((x & 1) == blueCol) {
					/* Green pixel on Red row */
					tmp = *iptr++ - cfg->blc.gr;
				} else {
					/* Red pixel */
					tmp = *iptr++ - cfg->blc.r;
				}
                tmp *= dbg_flash_sim_multiplier;
				if (tmp < 0) {
					tmp = 0;
				}
				*optr++ = tmp;
			}
		}
	}
}

/*
 * For realtime demo purposes, we don't want to use use the SIPP Raw filter
 * (the bit exact one) because it's very slow.  We use this one instead,
 * which is missing functionality (GrGb imbalanace, DPC) but is much faster.
 * For the purposes of generating AWB/AE stats, it should be sufficient.
 */
static void
doRaw(void * out_buffer, void *in_buffer, icIspConfig *cfg)
{
	int		w = ipInternal.rfc.width, h = ipInternal.rfc.height;
	int		x, y;
	unsigned	tmp;
	uint16_t	*iptr = (uint16_t *)in_buffer, *optr = (uint16_t *)out_buffer;
	int		blueCol, blueRow;

	switch (bayerOrder) {
	  case IC_BAYER_FORMAT_GRBG:
		blueCol = 0;
		blueRow = 1;
		break;
	  case IC_BAYER_FORMAT_GBRG:
		blueCol = 1;
		blueRow = 0;
		break;
	  case IC_BAYER_FORMAT_RGGB:
		blueCol = 1;
		blueRow = 1;
		break;
	  case IC_BAYER_FORMAT_BGGR:
		blueCol = 0;
		blueRow = 0;
		break;
	}

	for (y = 0; y < h; y++) {
		if ((y & 1) == blueRow) {
			/* BG or GB row */
			for (x = 0; x < w; x++) {
				if ((x & 1) == blueCol) {
					/* Blue pixel */
					tmp = (*iptr++ * cfg->raw.gainB) >> 8;
					if (tmp > cfg->raw.clampB) {
						tmp = cfg->raw.clampB;
			}
		} else {
					/* Green pixel on Blue row */
					tmp = (*iptr++ * cfg->raw.gainGb) >> 8;
					if (tmp > cfg->raw.clampGb) {
						tmp = cfg->raw.clampGb;
					}
				}
				*optr++ = tmp;
			}
		} else {
			/* GR or RG row */
			for (x = 0; x < w; x++) {
				if ((x & 1) == blueCol) {
					/* Green pixel on red row */
					tmp = (*iptr++ * cfg->raw.gainGr) >> 8;
					if (tmp > cfg->raw.clampGr) {
						tmp = cfg->raw.clampGr;
					}
				} else {
					/* Red pixel */
					tmp = (*iptr++ * cfg->raw.gainR) >> 8;
					if (tmp > cfg->raw.clampR) {
						tmp = cfg->raw.clampR;
					}
				}
				*optr++ = tmp;
			}
		}
		optr += (DEMOS_BORDER_COLS*2);
	}
}

static void
generateBorder(void *buffer)
{
    uint32_t w = ipInternal.rfc.width/2 + DEMOS_BORDER_COLS;
    uint32_t h = ipInternal.rfc.height;
    uint32_t *src, *dst, *line;
    unsigned int cnt;

    line = (uint32_t *)buffer;
    line += (DEMOS_BORDER_ROWS * w);
    for (cnt = 0; cnt < h; ++cnt) {

        // left border
        dst = line;
        src = dst + (DEMOS_BORDER_COLS-1);
        while (src > dst)
        {
            *dst++ = *src--;
        }
        // right border
        src = line + w - DEMOS_BORDER_COLS;
        dst = src + (DEMOS_BORDER_COLS-1);
        while (src < dst)
        {
            *dst--= *src++;
        }

        line += w;
    }

    w *= 2; // copy 2 lines
    dst = (uint32_t *)buffer;
    src = dst + w * (DEMOS_BORDER_ROWS - 1);
    while (dst < src) {
        memcpy(dst, src, w * sizeof (uint32_t));
        dst += w;
        src -= w;
    }

    h += (DEMOS_BORDER_ROWS*2);
    dst = (uint32_t *)buffer;
    dst += (w/2) * (h-2);
    src = dst - w*(DEMOS_BORDER_ROWS-1);
    while (src < dst) {
        memcpy(dst, src, w * sizeof (uint32_t));
        dst -= w;
        src += w;
    }
}

static void
getAeAwbStats(icCtrl *ctrl, struct ipStreamState *stream)
{
	icIspConfig		*cfg = stream->ispConfig;
	unsigned		px, py, x, y;
	uint16_t		*p0, *p1, *p2, *p3;
	int	    		ls;
	uint32_t		satVal;
	icAeAwbStatsPatch	    *patchSum = cfg->aeAwbStats->aeAwb;
	icAeAwbSatStatsPatch	*patchSat = cfg->aeAwbStats->aeAwbSat;
	unsigned		pw = cfg->aeAwbConfig.patchWidth;
	unsigned		ph = cfg->aeAwbConfig.patchHeight;
	unsigned		gw = cfg->aeAwbConfig.patchGapX;
	unsigned		gh = cfg->aeAwbConfig.patchGapY;
	unsigned		p0_x = cfg->aeAwbConfig.firstPatchX;
	unsigned		p0_y = cfg->aeAwbConfig.firstPatchY;

	if (pw & 1) {
		IC_WARN("Patch width must be even", pw);
	}
	if (ph & 1) {
		IC_WARN("Patch height must be even", ph);
	}
	if (gw & 1) {
		IC_WARN("Inter-patch width must be even", gw);
	}
	if (gh & 1) {
		IC_WARN("Inter-patch height must be even", gh);
	}
	if (p0_x & 1) {
		IC_WARN("Inter-patch height must be even", p0_x);
	}
	if (p0_y & 1) {
		IC_WARN("Inter-patch height must be even", p0_y);
	}

	/* Line Stride - in pixels */
	ls = ipInternal.rfc.width;

	/* Hardcoded for 14-bit range! */
	satVal = cfg->aeAwbConfig.satThresh;

	for (py = 0; py < cfg->aeAwbConfig.nPatchesY; py++) {
		for (px = 0; px < cfg->aeAwbConfig.nPatchesX; px++) {
			/* Get address of first pixel in patch */
			p0 = rawBuf + p0_x + (pw + gw) * px + (p0_y + (ph + gh) * py) * ls;
			p1 = p0 + 1;
			p2 = p0 + ls;
			p3 = p0 + ls + 1;

            patchSum[py].evenLine[px].rawEvenSum = 0;
            patchSum[py].evenLine[px].rawOddSum  = 0;
            patchSum[py].oddLine[px].rawEvenSum  = 0;
            patchSum[py].oddLine[px].rawOddSum   = 0;

            patchSat[py].evenLine[px].rawEvenCount = 0;
            patchSat[py].evenLine[px].rawOddCount  = 0;
            patchSat[py].oddLine[px].rawEvenCount  = 0;
            patchSat[py].oddLine[px].rawOddCount   = 0;

			for (y = 0; y < ph; y += 2) {
				for (x = 0; x < pw; x += 2) {
                    patchSum[py].evenLine[px].rawEvenSum += p0[x];
                    patchSum[py].evenLine[px].rawOddSum  += p1[x];
                    patchSum[py].oddLine[px].rawEvenSum  += p2[x];
                    patchSum[py].oddLine[px].rawOddSum   += p3[x];
					if (p0[x] >= satVal) {
                        patchSat[py].evenLine[px].rawEvenCount++;
					}
					if (p1[x] >= satVal) {
                        patchSat[py].evenLine[px].rawOddCount++;
				}
					if (p2[x] >= satVal) {
                        patchSat[py].oddLine[px].rawEvenCount++;
                    }
					if (p3[x] >= satVal) {
                        patchSat[py].oddLine[px].rawOddCount++;
                    }
			}

				p0 += ls*2;
				p1 += ls*2;
				p2 += ls*2;
				p3 += ls*2;
			}
		}
	}

#if 0
	patchSum = cfg->aeAwbStats->aeAwb;
	for (py = 0; py < cfg->aeAwbConfig.nPatchesY; py++) {
		for (px = 0; px < cfg->aeAwbConfig.nPatchesX; px++) {
		    fprintf(stderr, "%d %d ",
              patchSum[py].evenLine[px].rawEvenSum,
              patchSum[py].evenLine[px].rawOddSum);
        }
        fprintf(stderr, "\n");
		for (px = 0; px < cfg->aeAwbConfig.nPatchesX; px++) {
		    fprintf(stderr, "%d %d ",
              patchSum[py].oddLine[px].rawEvenSum,
              patchSum[py].oddLine[px].rawOddSum);
        }
        fprintf(stderr, "\n");
	}
#endif
}

/*
 * Note: All config parameter values are relative to the full field of view,
 * i.e. they are specified in terms of the Still Image resolution, and not the
 * Video/Preview resolution.  Hence we divide them by 2.
 *
 * The number of patches in each dimension that are actually processed is
 * derived from roiWidth and roiHeight, rounding down to the nearest multiple
 * of patchWidth and patchHeight respectively.
 *
 * The stats are derived from the green channel only.
 */
#define MAX_SENSOR_SIZE_X   4400
#define MAX_SENSOR_SIZE_Y   3200

static void
getAfStats(icCtrl *ctrl, struct ipStreamState *stream)
{
	icIspConfig	*cfg = stream->ispConfig;
	int		frame_width  = ipInternal.rfc.width/2;
	unsigned	start_x      = cfg->afConfig.firstPatchX/2;
	unsigned	start_y      = cfg->afConfig.firstPatchY/2;
	unsigned	paxel_width  = cfg->afConfig.patchWidth/2;
	unsigned	paxel_height = cfg->afConfig.patchHeight/2;
    unsigned   nx = cfg->afConfig.nPatchesX;
    unsigned   ny = cfg->afConfig.nPatchesY;
    unsigned   roi_width    = paxel_width * nx;
    unsigned   roi_height   = paxel_height * ny;

	uint16_t	*green;
	int			ps;
	int32_t		*f1Coeffs = cfg->afConfig.f1Coeffs;
	int32_t		*f2Coeffs = cfg->afConfig.f2Coeffs;
	int32_t		f1Threshold = cfg->afConfig.f1Threshold;
	int32_t		f2Threshold = cfg->afConfig.f2Threshold;
	icAfStatsPatch	*patch = cfg->afStats->af;

	uint32_t	i, k, j, m, n, output_counter = 0;
	int32_t		pax_in_line, pix_in_pax;

	uint16_t	*current_ptr;
	int32_t		green1, green2;
	int32_t		green_sum1, green_max1, AboveThreshold1;   // temp filter 1
	int32_t		green_sum2, green_max2, AboveThreshold2;   // temp filter 2
	int32_t		green_sum_all; // temp green sum of all used pixels
	int32_t		temp;

	unsigned  ix, iy;

	unsigned  skipped_lines;
	unsigned  raw_cnt;
	unsigned  *raw_process_flag; //[paxel_height];

	if ((raw_process_flag = (unsigned *)malloc(paxel_height * sizeof(unsigned))) == NULL) {
		perror("malloc");
		exit(1);
	}

	for (i = 0; i < paxel_height; i++) {
	    raw_process_flag[i] = 0;
	}

    for (i = 0; i < paxel_height; i += cfg->afConfig.nSkipRows + 1) {
        raw_process_flag[i] = 1;
    }

	/* Plane Stride - in pixels */
	ps = frame_width * ipInternal.rfc.height/2;

	/* Skip red plane */
	green = demosOut + ps;

/* TODO: start the IIR filter a few pixels (20-50 according to Anguel) to the
 * left of the ROI.
 */

	// init the variables for first paxels raw
	raw_cnt = 0;
	output_counter = 0;
    for (j = 0; j < nx; j++) {
    	patch[output_counter + j].sum = 0;
		patch[output_counter + j].f1AboveThreshold = 0;
		patch[output_counter + j].f1Sum = 0;
		patch[output_counter + j].f1SumRowMaximums = 0;
		patch[output_counter + j].f2AboveThreshold = 0;
		patch[output_counter + j].f2Sum = 0;
		patch[output_counter + j].f2SumRowMaximums = 0;
    }

	for (i = start_y; i < start_y + roi_height; i++) {
		/* The following are the temporary values, needed for the calculations of every raw line*/
		int32_t green_f1_z1_val = 0, green_f1_z2_val = 0, green_f2_z1_val = 0, green_f2_z2_val = 0;
		int32_t green2_f1_z1_val = 0, green2_f1_z2_val = 0, green2_f2_z1_val = 0, green2_f2_z2_val = 0;
		int32_t green_temp, green_temp2;

        if (raw_cnt >= paxel_height) {
        	// New paxels raw begins
            raw_cnt = 0;
            output_counter += nx;
            // init the variables for new paxels raw
            for (j = 0; j < nx; j++) {
            	patch[output_counter + j].sum = 0;
    			patch[output_counter + j].f1AboveThreshold = 0;
    			patch[output_counter + j].f1Sum = 0;
    			patch[output_counter + j].f1SumRowMaximums = 0;
    			patch[output_counter + j].f2AboveThreshold = 0;
    			patch[output_counter + j].f2Sum = 0;
    			patch[output_counter + j].f2SumRowMaximums = 0;
            }
        }

		if (raw_process_flag[raw_cnt] == 0) {
			// Must skip the raws to the end of paxel
		    raw_cnt++;
	        continue;
		}
		raw_cnt++;

		/* Move the pointer to the beginning of the picture inside the frame. */
		current_ptr = green + i * frame_width + start_x;

		for ( k = start_x; k < start_x + roi_width; k++, current_ptr++) {
			pax_in_line = (k - start_x) / paxel_width;
			pix_in_pax = (k - start_x) % paxel_width;
			/* Calculate green value, depending on the given pattern. */
			green1 = *current_ptr - cfg->afConfig.initialSubtractionValue;
			/* The filtering for both filters starts here.
			* Note that the operations with 1 additional tab are for filter 2, and the operations with not tabs are for filter 1.
			* Both filters do the same operations, but with different coefficients.
			*/
			green1 <<= MUL_SHIFT_VALUE;

			green1 *= f1Coeffs[0];

			green1 >>= MUL_SHIFT_VALUE;

			green2 = green1;

			green1 += ( green_f1_z1_val * f1Coeffs[1] ) >> MUL_SHIFT_VALUE;
			green1 += ( green_f1_z2_val * f1Coeffs[2] ) >> MUL_SHIFT_VALUE;
			green2 += ( green2_f1_z1_val * f2Coeffs[1] ) >> MUL_SHIFT_VALUE;
			green2 += ( green2_f1_z2_val * f2Coeffs[2] ) >> MUL_SHIFT_VALUE;

			/* Save the RGB values to red_temp, blue_temp and green_temp to transfer them later to the z-1 and z-2 values,
			* as shown in the documentation. "z-1" is the previous frame, and "z-2" is the frame before the previous frame.
			*/
			green_temp = green1;
			green_temp2 = green2;

			green1 *= f1Coeffs[3];
			green1 >>= MUL_SHIFT_VALUE;
			green1 += ( green_f1_z1_val * f1Coeffs[4] ) >> MUL_SHIFT_VALUE;
			green1 += ( green_f1_z2_val * f1Coeffs[5] ) >> MUL_SHIFT_VALUE;
			green2 *= f2Coeffs[3];
			green2 >>= MUL_SHIFT_VALUE;
			green2 += ( green2_f1_z1_val * f2Coeffs[4] ) >> MUL_SHIFT_VALUE;
			green2 += ( green2_f1_z2_val * f2Coeffs[5] ) >> MUL_SHIFT_VALUE;

			/* Save the z-1 and z-2 values */
			green_f1_z2_val = green_f1_z1_val;
			green_f1_z1_val = green_temp;
			green2_f1_z2_val = green2_f1_z1_val;
			green2_f1_z1_val = green_temp2;

			/* Continue the operations... */
			green1 += ( green_f2_z1_val * f1Coeffs[6] ) >> MUL_SHIFT_VALUE;
			green1 += ( green_f2_z2_val * f1Coeffs[7] ) >> MUL_SHIFT_VALUE;
			green2 += ( green2_f2_z1_val * f2Coeffs[6] ) >> MUL_SHIFT_VALUE;
			green2 += ( green2_f2_z2_val * f2Coeffs[7] ) >> MUL_SHIFT_VALUE;
			/* save the RGB values to red_temp, blue_temp and green_temp to transfer them later to the z-1 and z-2 values */
			green_temp = green1;
			green_temp2 = green2;

			/* Continue processing... */
			green1 *= f1Coeffs[8];
			green1 >>= MUL_SHIFT_VALUE;
			green2 *= f2Coeffs[8];
			green2 >>= MUL_SHIFT_VALUE;
			green1 += ( green_f2_z1_val * f1Coeffs[9] ) >> MUL_SHIFT_VALUE;
			green1 += ( green_f2_z2_val * f1Coeffs[10] ) >> MUL_SHIFT_VALUE;
			green2 += ( green2_f2_z1_val * f2Coeffs[9] ) >> MUL_SHIFT_VALUE;
			green2 += ( green2_f2_z2_val * f2Coeffs[10] ) >> MUL_SHIFT_VALUE;

			/* NOTE: Here is the place to output filter values! */
			{
				if (pix_in_pax == 0) {
					// clear variables at the begin of paxel line
					green_sum_all = 0;
					green_sum1 = 0; AboveThreshold1 = 0; green_max1 = 0;
					green_sum2 = 0; AboveThreshold2 = 0;green_max2 = 0;
				}

				// Green sum all
				green_sum_all += *current_ptr;

				// Filter 1
				temp = abs(green1);
				if ( temp >= f1Threshold ) {
					green_sum1 += temp - f1Threshold;
					AboveThreshold1++;
					if (temp > green_max1) {
						green_max1 = temp;
					}
				}

				// Filter 2
				temp = abs(green2);
				if ( temp >= f2Threshold ) {
					green_sum2 += temp - f2Threshold;
					AboveThreshold2++;
					if (temp > green_max2) {
						green_max2 = temp;
					}
				}

				if (pix_in_pax == paxel_width - 1) {
					// save results at the end of paxel line
					// green sum
					patch[output_counter + pax_in_line].sum += 4 * green_sum_all;
					// Filter 1
					patch[output_counter + pax_in_line].f1AboveThreshold += AboveThreshold1;
					patch[output_counter + pax_in_line].f1Sum += green_sum1;
					patch[output_counter + pax_in_line].f1SumRowMaximums += green_max1;
					// Filter 2
					patch[output_counter + pax_in_line].f2AboveThreshold += AboveThreshold2;
					patch[output_counter + pax_in_line].f2Sum += green_sum2;
					patch[output_counter + pax_in_line].f2SumRowMaximums += green_max2;
				}
			}
			/* Save the z-1 and z-2 values */
			green_f2_z2_val = green_f2_z1_val;
			green_f2_z1_val = green_temp;
			green2_f2_z2_val = green2_f2_z1_val;
			green2_f2_z1_val = green_temp2;
		}
	}

	free(raw_process_flag);

#if 0
	patch = cfg->afStats->af;
	for (i = 0; i < nx * ny; i++) {
		printf("%4d %8d %8d %8d %8d %8d %8d %8d\n", i+1, patch->sum, patch->f1AboveThreshold, patch->f1Sum, patch->f1SumRowMaximums, patch->f2AboveThreshold, patch->f2Sum, patch->f2SumRowMaximums);
		patch++;
	}
#endif

}

/* Reduce resolution by 1/4 in each direction, while going from Bayer -> RGB */
void
combDecimDemosaic(uint16_t *input, uint16_t *output[3], int width, int height)
{
	int		x, y, i, out_x;
	int		w2 = width/2;
	uint16_t	*iline[4];
	uint16_t	*o0, *o1, *o2;
	int		start_x, start_y;
	int ppln = width + (DEMOS_BORDER_COLS*2);

	/*
	 * We crop out a few pixels in order to align to GRBG bayer order,
	 * and so that we don't need to implement pixel replication at image
	 * boundaries in the optimized filter implementation.
	 */
	switch (bayerOrder) {
	  case IC_BAYER_FORMAT_GRBG:
	      start_x = 2;      // tested for ar1330
	      start_y = 1;
		break;
	  case IC_BAYER_FORMAT_GBRG:
		start_x = 1;    // NOT tested
		start_y = 2;
		break;
	  case IC_BAYER_FORMAT_RGGB:
		start_x = 1;    // NOT tested
		start_y = 1;
		break;
	  case IC_BAYER_FORMAT_BGGR:
		start_x = 2;    // NOT tested
		start_y = 2;
		break;
	}

	o0 = output[0];
	o1 = output[1];
	o2 = output[2];

	iline[0] = input + start_y*ppln + DEMOS_BORDER_COLS - 4;
	iline[1] = iline[0] + ppln;
	iline[2] = iline[1] + ppln;
	iline[3] = iline[2] + ppln;

#if 0
dumpFrame("dbg.raw",
    ipInternal.rfc.width*2, ipInternal.rfc.height,
    ipInternal.rfc.width*2, 1, 0, (uint8_t *)input);
#endif

	for (y = start_y; y < height+start_y; y += 2) {
		for (out_x = 0, x = start_x; x < start_x + width; x += 2, out_x++) {
			o0[out_x] = ((iline[1][x-1] * 3 +	/* R */
			    iline[1][x+1] * 9 + 
			    iline[3][x-1] * 1 + 
			    iline[3][x+1] * 3) >> 4);

			o1[out_x] = ((iline[1][x] +		/* G */
			    iline[2][x+1]) >> 1);

			o2[out_x] = ((iline[0][x] * 3 +		/* B */
			    iline[0][x+2] * 1 + 
			    iline[2][x] * 9 + 
			    iline[2][x+2] * 3) >> 4);
		}

		/* Advance input by 2 lines */
		for (i = 0; i < 4; i++) {
			iline[i] += ppln * 2;
		}

		/* Advance output by 1 line */
		o0 += w2;
		o1 += w2;
		o2 += w2;
	}
}

#ifdef IP_ENABLE_SIPP_PROCESSING
static void
configureFilters(icCtrl *ctrl, icIspConfig *cfg)
{
	LscParam	*lscCfg = (LscParam *)lsc->params;
#ifdef IP_ENABLE_RAW_FILTER
	RawParam	*rawCfg = (RawParam *)raw->params;
	int		bayer;
#endif // IP_ENABLE_RAW_FILTER

//printf("ConfigISP LSC %p\n", cfg->lsc.pLscTable);
	lscCfg->dataFormat = 1;		/* Bayer */
	lscCfg->dataWidth  = cfg->raw.outputBits;
	lscCfg->gmWidth    = cfg->lsc.lscWidth;
	lscCfg->gmHeight   = cfg->lsc.lscHeight;
	lscCfg->gmBase     = cfg->lsc.pLscTable;
/* TODO - on Myriad, LSC table has to be placed in a buffer in CMX! */

#ifdef IP_ENABLE_RAW_FILTER
	switch (bayerOrder) {
	  default:
	  case IC_BAYER_FORMAT_GRBG:
		bayer = 0;
		break;
	  case IC_BAYER_FORMAT_GBRG:
		bayer = 2;
		break;
	  case IC_BAYER_FORMAT_RGGB:
		bayer = 1;
		break;
	  case IC_BAYER_FORMAT_BGGR:
		bayer = 3;
		break;
	}

	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//RAW block params
	rawCfg->cfg = 1                            | // Bayer (not planar)
                 (bayer                     <<  1) |
                 (1                         <<  3) | //GRGB_IMB_EN
                 (1                         <<  4) | //HOT_PIX_EN
                 (0                         <<  5) | //STATS_PATCH_EN
                 (0                         <<  6) | //STATS_HIST_EN
                 ((cfg->raw.outputBits - 1) <<  8) | //RAW_DATA_WIDTH
                 (0                         << 12) | //Statistics input sel
                 (cfg->raw.grgbImbalThr     << 16) ; //BAD_PIX_THR

	//gains for 1st line
	rawCfg->gainSat[0][0] = cfg->raw.gainGr | (cfg->raw.clampGr << 16);
	rawCfg->gainSat[0][1] = cfg->raw.gainR  | (cfg->raw.clampR  << 16);
	rawCfg->gainSat[0][2] = cfg->raw.gainGr | (cfg->raw.clampGr << 16);
	rawCfg->gainSat[0][3] = cfg->raw.gainR  | (cfg->raw.clampR  << 16);

	//gains for 2nd line
	rawCfg->gainSat[1][0] = cfg->raw.gainB  | (cfg->raw.clampB  << 16);
	rawCfg->gainSat[1][1] = cfg->raw.gainGb | (cfg->raw.clampGb << 16);
	rawCfg->gainSat[1][2] = cfg->raw.gainB  | (cfg->raw.clampB  << 16);
	rawCfg->gainSat[1][3] = cfg->raw.gainGb | (cfg->raw.clampGb << 16);

	rawCfg->grgbPlat   = cfg->raw.grgbImbalPlatDark  | (cfg->raw.grgbImbalPlatBright  << 16);
	rawCfg->grgbDecay  = cfg->raw.grgbImbalDecayDark | (cfg->raw.grgbImbalDecayBright << 16);

	rawCfg->badPixCfg  = cfg->raw.dpcAlphaColdRb       |
                             cfg->raw.dpcAlphaHotRb  << 4  |
                             cfg->raw.dpcAlphaColdG  << 8  |
                             cfg->raw.dpcAlphaHotG   << 12 |
                             cfg->raw.dpcNoiseLevel  << 16;
#endif // IP_ENABLE_RAW_FILTER
}
#endif // IP_ENABLE_SIPP_PROCESSING

static void
lineNotify(icCtrl *ctrl, uint32_t event, uint32_t instance)
{
	icEvent		ev;
	struct timeval  tv;

    gettimeofday(&tv, NULL);

	ev.ctrl = event;
	ev.u.lineEvent.sourceInstance = instance;
	ev.u.lineEvent.userData = ipInternal.rfcVidStream.ispConfig->userData;
	ev.u.lineEvent.seqNo = ipInternal.rfcVidStream.seqNo;
	ev.u.lineEvent.ts = (icTimestamp)tv.tv_sec * 1000000 + tv.tv_usec;
	ipSendEvent(ctrl, &ev);
}


static void
statsNotify(icCtrl *ctrl, uint32_t instance)
{
    icEvent         ev;
    ev.ctrl = IC_EVENT_TYPE_STATS_READY;
    ev.u.statsReady.ispInstance = instance;
    ev.u.statsReady.userData = ipInternal.rfcVidStream.ispConfig->userData;
    ev.u.statsReady.seqNo    = ipInternal.rfcVidStream.seqNo;
    ipSendEvent(ctrl, &ev);
}

#define CFA_OUT_BPP 		10
#define GAMMA_INPUT_BPP		10   // MUST BE SAME IN CONFIGURATION 10->8 gamma
#define GAMMA_OUTPUT_BPP	 8   // MUST BE SAME IN CONFIGURATION 10->8 gamma

#define DOWN_SHIFT_WB		 8
#define DOWN_SHIFT	        (CFA_OUT_BPP - GAMMA_INPUT_BPP)

// LUT is in FP16 format, and is compatible with LUT hardware block.  Since
// the lookups as performed by hardware are a bit expensive, and this pipeline
// is only working in integer domain, with limited precision, create a table
// to map from 10-bit integer to 8-bit integer.
static void
build_gamma_lut(icIspConfig *cfg, uint8_t *table)
{
    unsigned        i, idx;
    unsigned        offLsbTab[11] = {10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0};
    unsigned        offLsb;
    unsigned        lutSize = cfg->gamma.size;
    unsigned        epr = lutSize/16;   // Entries / Range
    unsigned        rsi;                // Range Size Indicator
    float           f1, f2, frac;
    uint32_t        f32;
    uint16_t        *gammaIn = cfg->gamma.table, f16;
    unsigned        expo, mant, fmask, idx1, idx2;

    rsi = roundf(logf(epr)/logf(2));        // rsi = log2(epr)
    offLsb = offLsbTab[rsi];
    fmask = (1<<offLsb) - 1;

    for (i = 0; i < 1024; i++) {
        f1 = (float)i / 1023;
        f32 = *(uint32_t *)&f1;

        /* Turn the fp32 value into FP16, then do the lookup as per hardware */
        expo = (((f32 >> 23) - 127 + 15) & 0xf) << 10;
        mant = (f32 >> (23-10)) & 0x3ff;
        idx1 = (expo | mant) >> offLsb;
        idx2 = idx1 + 1;
        if (idx2 > lutSize - 1) {
            idx2 = lutSize - 1;
        }
        frac = (float)(mant & fmask) / 31;

        /* Get values from LUT and convert from F16 to F32 */
        f16 = gammaIn[idx1<<2];
        *(uint32_t *)&f1 = (f16&0x3ff) << (23-10) | ((f16 >> 10)-15+127) << 23;
        f16 = gammaIn[idx2<<2];
        *(uint32_t *)&f2 = (f16&0x3ff) << (23-10) | ((f16 >> 10)-15+127) << 23;

        table[i] = roundf((f1 * (1.0-frac) + f2 * frac) * 255);
    }
}

void 
proc_rgb2rgb_gamma(uint16_t *in_out, icIspConfig *cfg, int width, int height)
{
    int l, c;
    unsigned short *r;
    unsigned short *g;
    unsigned short *b;

    static uint8_t gamma[1024];

    unsigned int ri, gi, bi;

    float krr, krg, krb,
	  	  	   kgr, kgg, kgb,
	  	  	   kbr, kbg, kbb;

    unsigned int kr, kg, kb;
    if (cfg->gamma.size == 0)
        return;

    kr = cfg->wbGains.gainR;
    kg = cfg->wbGains.gainG;
    kb = cfg->wbGains.gainB;

    build_gamma_lut(cfg, gamma);

    krr = cfg->colorCombine.ccm[0];
    krg = cfg->colorCombine.ccm[1];
    krb = cfg->colorCombine.ccm[2];

    kgr = cfg->colorCombine.ccm[3];
    kgg = cfg->colorCombine.ccm[4];
    kgb = cfg->colorCombine.ccm[5];

    kbr = cfg->colorCombine.ccm[6];
    kbg = cfg->colorCombine.ccm[7];
    kbb = cfg->colorCombine.ccm[8];

    r = in_out;
    g = &r[width*height];
    b = &r[width*height*2];

    for (l = 0; l < height; l++) {
        for (c = 0; c < width; c++) {
			ri = min(((*r) * kr) >> DOWN_SHIFT_WB, ((1<<CFA_OUT_BPP)-1));
			gi = min(((*g) * kg) >> DOWN_SHIFT_WB, ((1<<CFA_OUT_BPP)-1));
			bi = min(((*b) * kb) >> DOWN_SHIFT_WB, ((1<<CFA_OUT_BPP)-1));

			*r++ = gamma[min(max(((int)((int)ri*krr + (int)gi*krg + (int)bi*krb) >> DOWN_SHIFT), 0), 1023)];
			*g++ = gamma[min(max(((int)((int)ri*kgr + (int)gi*kgg + (int)bi*kgb) >> DOWN_SHIFT), 0), 1023)];
			*b++ = gamma[min(max(((int)((int)ri*kbr + (int)gi*kbg + (int)bi*kbb) >> DOWN_SHIFT), 0), 1023)];
        }
    }
}

void 
showBuffer(uint32_t frame_number)
{
    x11_disp_show(demosOut, frame_number);
}

static void
ispEvent(icCtrl *ctrl, uint32_t event, uint32_t instance)
{
	icEvent		ev;

	ev.ctrl = event;
	ev.u.ispEvent.ispInstance = instance;
	ev.u.ispEvent.userData = ipInternal.rfcVidStream.ispConfig->userData;
	ev.u.ispEvent.seqNo = ipInternal.rfcVidStream.seqNo;
	ipSendEvent(ctrl, &ev);
}

void
ipDoProcessing(icCtrl *ctrl)
{
	uint16_t	*olines[3];
	int		w = ipInternal.rfc.width, h = ipInternal.rfc.height;
	struct timeval  tv;
	double		t0, t1;

	if (!initialised) {
	    initialised = 1;
		setup(ctrl, w, h);
		first_read = 1;
	}

	if (ipInternal.rfcVidStream.pendingConfig != NULL) {
		/*
		 * Take the latest available configuration and make it take
		 * effect before we start processing the frame.
		 */
        ipInternal.rfcVidStream.ispConfig = ipInternal.rfcVidStream.pendingConfig;
		ipInternal.rfcVidStream.pendingConfig = NULL;
		printf("IPIPE: Applying Config %d\n", ipInternal.rfcVidStream.seqNo);


		/*
		 * Release the ISP config struct back to the client once
		 * it's been consumed.
		 */
		ispEvent(ctrl,
		    IC_EVENT_TYPE_ISP_CONFIG_ACCEPTED, IC_ISP_RFC_VIDEO);

#ifdef IP_ENABLE_SIPP_PROCESSING
		configureFilters(ctrl, ipInternal.rfcVidStream.ispConfig);
#endif
	} else {
        icErrorEvent(ctrl, IC_ERROR_RT_CFG_MISSING, IC_SEVERITY_NORMAL);
	    printf ("Skipping frame: IPIPE configuration is missing\n");
	    ipSleepMs(33);
	    return;
	}

    if (initialised == 1) {
        x11_disp_create_t create;

        initialised = 2;

        create.display_name = NULL;
        create.format = DISP_IMAGE_FORMAT_RGB42;
        create.width  = w/2;
        create.height = h/2;
        create.scale = 1;           // TODO - works only on AF Stats
        x11_disp_create(&create);

        if ((clp_focus_select >= AF_DATA_SUM) && (clp_focus_select <= AF_FILTER2_MAX))
        {
            AF_DATA_SELECT = clp_focus_select;
        } else {
            AF_DATA_SELECT = AF_FILTER1_SUM;
        }

        create.width  = ipInternal.rfcVidStream.ispConfig->aeAwbConfig.nPatchesX;
        create.height = ipInternal.rfcVidStream.ispConfig->aeAwbConfig.nPatchesY;
        switch  (clp_h3a_scale)
        {
        case 0:
        case 1:
        case 2:
        case 4:
            create.scale  = clp_h3a_scale;
            break;
        default:
            create.scale = STATS_SCALE;
        }
        x11_h3a_disp_create(&create);

        create.width  = ipInternal.rfcVidStream.ispConfig->afConfig.nPatchesX;
        create.height = ipInternal.rfcVidStream.ispConfig->afConfig.nPatchesY;

        switch  (clp_focus_scale)
        {
        case 0:
        case 1:
        case 2:
        case 4:
            create.scale  = clp_focus_scale;
            break;
        default:
            create.scale = STATS_SCALE;
        }
        x11_h3a_af_disp_create(&create);

        ipInternal.rfc.dw_width = w;
        ipInternal.rfc.dw_height = h;
        dw_init(&ipInternal.rfc.h_cam,
                &ipInternal.rfc.dw_width,
                &ipInternal.rfc.dw_height,
                &ipInternal.rfc.dw_img_size);
        bayerOrder  = ipInternal.rfc.src_fmt;
    }

	/* Sent start-of-readout event */
	lineNotify(ctrl, IC_EVENT_TYPE_READOUT_START, IC_SOURCE_RFC);
	printf("READOUT_START\n");
	if (ipInternal.rfc.h_cam) {
        pthread_mutex_lock(&ctrl->senMutex);
#if !defined(__sparc)
        dw_read_frame(ipInternal.rfc.h_cam, ibuf1, ipInternal.rfc.dw_img_size);
#endif
        dw_read_frame(ipInternal.rfc.h_cam, ibuf1, ipInternal.rfc.dw_img_size);
        pthread_mutex_unlock(&ctrl->senMutex);
	} else {
	    readFrame(ibuf1);
	}
    printf("ZSL Buff\n");
    zslBuff = zslQueue_getEmpty();
    if (NULL == zslBuff)
    {
        printf ("%s(%d): Error\n", __func__, __LINE__);
    }
   
	gettimeofday(&tv, NULL);
	t0 = (tv.tv_sec) * 1000 + (tv.tv_usec) / 1000 ;
	printf("FLASH sim mult %f\n", dbg_flash_sim_multiplier);

    if (clp_raw_bls_select == 1) {
        // Normal operations - BLS is applied on raw image
        doBlc(zslBuff->ibuf, ibuf1, ipInternal.rfcVidStream.ispConfig);
    } else {
        // Test - sensor raw
        // copy image in ZSL queue
        int w = ipInternal.rfc.width;
        int h = ipInternal.rfc.height;
        memcpy(zslBuff->ibuf, ibuf1, w * h * sizeof(uint16_t));
        // process for preview
        doBlc(zslBuf, ibuf1, ipInternal.rfcVidStream.ispConfig);
    }
    printf("BLC End\n");
    zslQueue_setFull(zslBuff, ipInternal.rfcVidStream.seqNo,
            (ipInternal.rfcVidStream.ispConfig->pipeControl & IC_PIPECTL_ZSL_LOCK),
            ipInternal.rfcVidStream.ispConfig->userData);

    if (ipInternal.rfcVidStream.ispConfig->pipeControl & IC_PIPECTL_ZSL_LOCK) {

        icEvent     ev;

        printf("FLASH ZSL lock sim mult %f\n", dbg_flash_sim_multiplier);

        ev.ctrl = IC_EVENT_TYPE_ZSL_LOCKED;
        ev.u.buffLockedZSL.sourceInstance = IC_SOURCE_RFC;
        ev.u.buffLockedZSL.userData = ipInternal.rfcVidStream.ispConfig->userData;
        ev.u.buffLockedZSL.buffZsl = zslBuff;
        ev.u.buffLockedZSL.ts = zslBuff->ts;
        ev.u.buffLockedZSL.seqNo = zslBuff->seqNo;
        ipSendEvent(ctrl, &ev);
    }

    /* LSC */

#ifdef IP_ENABLE_SIPP_PROCESSING
    if (clp_raw_bls_select == 1) {
        dmaInCfg->ddrAddr  = (uint32_t)zslBuff->ibuf;
    } else {
        dmaInCfg->ddrAddr  = (uint32_t)zslBuf;
    }

    dmaOutCfg->ddrAddr = (uint32_t)rawBuf;
	sippProcessFrame(pl);
#else
    memcpy(
            rawBuf,
            clp_raw_bls_select ? zslBuff->ibuf : zslBuf,
            (ipInternal.rfc.width+200) * ipInternal.rfc.height * 2
        );
#endif // IP_ENABLE_SIPP_PROCESSING


    getAeAwbStats(ctrl, &ipInternal.rfcVidStream);
    printf("AeAwb End\n");
    #ifndef IP_ENABLE_RAW_FILTER
	{
	    uint16_t *p;
	    p = ibuf + DEMOS_BORDER_ROWS * (ipInternal.rfc.width + (DEMOS_BORDER_COLS *2)) + DEMOS_BORDER_COLS;
	    doRaw(p, rawBuf, ipInternal.rfcVidStream.ispConfig);
	    generateBorder(ibuf);
	}
#endif // IP_ENABLE_RAW_FILTER

	/*
	 * Send "line N reached" event... can't really implement this in a
	 * meaningful way right now...
	 */
	if (ipInternal.rfc.notificationLine != -1) {
	    lineNotify(ctrl, IC_EVENT_TYPE_LINE_REACHED, IC_SOURCE_RFC);
	}

	olines[0] = demosOut;
	olines[1] = demosOut + w/2 * h/2;
	olines[2] = olines[1] + w/2 * h/2;
    {
        uint16_t *p;
        p = ibuf + DEMOS_BORDER_ROWS * (ipInternal.rfc.width + (DEMOS_BORDER_COLS *2)) + DEMOS_BORDER_COLS;
        combDecimDemosaic(p, olines, w, h);
        printf("combDecimDemosaic End\n");
    }

	getAfStats(ctrl, &ipInternal.rfcVidStream);


    x11_disp_show_h3a(ipInternal.rfcVidStream.ispConfig->aeAwbStats->aeAwb,
                      ipInternal.rfcVidStream.ispConfig->aeAwbConfig.patchWidth*
                      ipInternal.rfcVidStream.ispConfig->aeAwbConfig.patchHeight*(1<<(CFA_OUT_BPP-8)));

    x11_disp_show_h3a_af(ipInternal.rfcVidStream.ispConfig->afStats->af, AF_DATA_SELECT);


	proc_rgb2rgb_gamma(demosOut, ipInternal.rfcVidStream.ispConfig, w/2, h/2);

    showBuffer(ipInternal.rfcVidStream.ispConfig->frameCount);

#if 0
	/* Dump pre-processor output (input to Video/Preview ISP) */
	{
		char		fname[64];

		sprintf(fname, "%s/dbg_%dx%d_16b_P444.rgb", ipInternal.cam_base_dir,
		    ipInternal.rfc.width/2, ipInternal.rfc.height/2);
		dumpFrame(fname,
		    ipInternal.rfc.width*2/2, ipInternal.rfc.height/2,
		    ipInternal.rfc.width*2/2, 3,
		    (ipInternal.rfc.width/2*ipInternal.rfc.height/2)*2, (uint8_t *)demosOut);
	}
#endif

	/*
	 * Send end-of-readout event (happens just before the pre-processor
	 * pipe finishes processing the frame arriving from the sensor)
	 */
	lineNotify(ctrl, IC_EVENT_TYPE_READOUT_END, IC_SOURCE_RFC);

    printf("Af End\n");
    statsNotify(ctrl, IC_SOURCE_RFC);

	/* Display the time take to process the last frame */
	gettimeofday(&tv, NULL);
	t1 = (tv.tv_sec) * 1000 + (tv.tv_usec) / 1000 ;
	printf("%f ms/frame (%d)\n", t1-t0, ipInternal.rfcVidStream.seqNo);

    ipInternal.rfcVidStream.seqNo++;
}

/*
 * Dump the frame from the ZSL buffer to a file, and also save the ISP
 * settings to apply.
 */
int
ipDumpRaw(icCtrl *ctrl, char *fname)
{
	int         capt_index, err;

    capt_index = 0;
    do {
		sprintf(fname, "%s/dump_%dx%d_bayer_10b_%d.raw", ipInternal.cam_base_dir,
            ipInternal.rfc.width, ipInternal.rfc.height, capt_index++);

        err = access(fname, F_OK);
        if (capt_index > 100) {
            printf("\nPlease clean captured images in Folder: %s\n", ipInternal.cam_base_dir);
            break;
        }
    } while (!err);
    capt_index--;

	if (capt_zslBuff->ibuf == NULL) {
	    printf ("ibuf == NULL\n");
	    return -1;
	}
	dumpFrame(fname,
	    ipInternal.rfc.width*2, ipInternal.rfc.height,
	    ipInternal.rfc.width*2, 1, 0, (uint8_t *)capt_zslBuff->ibuf);
	return capt_index;
}

int
ipDumpRaw1(icCtrl *ctrl, char *fname)
{
	dumpFrame(fname,
	    ipInternal.rfc.width*2, ipInternal.rfc.height,
	    ipInternal.rfc.width*2, 1, 0, (uint8_t *)capt_zslBuff->ibuf);
	return 0;
}

#else
#pragma message("This fake IPIPE implementation only runs on the PC right now...")
#endif
