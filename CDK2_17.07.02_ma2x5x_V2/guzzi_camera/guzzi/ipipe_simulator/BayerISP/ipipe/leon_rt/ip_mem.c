///
/// @file
/// @copyright All code copyright Movidius Ltd 2014, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Simple frame buffer management
///

#include "ip_internal.h"

static uint32_t	offset;

/*
 * Allocate a frame from the frame buffer pool.  "startGran" is interpreted
 * to mean that all planes in the buffer must be aligned to the specified
 * granularity.
 */
ipFrame_t *
ipFrameAlloc(icCtrl *ctrl,
    int width, int height, int startGran, int stride, int bpp, int nPlanes)
{
	int		planeStride = stride * height;
	int		mod;
	uint8_t		*pool = (uint8_t *)ctrl->framepoolBase;
	ipFrame_t	*frame;
	uint32_t	off = offset;

	/* Make sure ipFrame_t struct is 4-byte aligned */
	mod = off & 3;
	if (mod != 0) {
		off += 4-mod;
	}

	frame = (ipFrame_t *)(pool + off);
	off += sizeof (*frame);

	/*
	 * Make plane stride a multiple of "startGran", so that all planes
	 * will be aligned if the first plane is aligned.
	 */
	mod = planeStride % startGran;
	if (mod != 0) {
		planeStride += startGran - mod;
	}

	/* Make the first plane be aligned */
	mod = off % startGran;
	if (off != 0) {
		off += startGran - mod;
	}

	off += planeStride * nPlanes;

	/* Check if we ran out of memory */
	if (off > ctrl->framepoolSize) {
		IC_WARN("Out of memory", planeStride * nPlanes);
		return NULL;
	}

	frame->ptr = pool + off;
	frame->ps = planeStride;
	frame->ls = stride;
	frame->width = width;
	frame->height = height;
	frame->bpp = bpp;
	frame->nPlanes = nPlanes;

	/* Set aside the memory */
	offset = off;

	return frame;
}

/* All previously allocated frames are invalid when this function is called! */
void
ipResetFramepool(icCtrl *ctrl)
{
	offset = 0;
}
