///
/// @file
/// @copyright All code copyright Movidius Ltd 2014, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     IPipe internals
///

#include <ipipe.h>
#include "devware.h"

#ifdef __cplusplus
extern "C" {
#endif

extern int clp_focus_select;
extern int clp_h3a_scale;
extern int clp_focus_scale;
extern int clp_raw_bls_select;
extern char *in_file_name;
extern char *script_name;

#if !defined(__sparc)
    #define IP_ZSL_MAX_SIZE     8
#else
    #define IP_ZSL_MAX_SIZE       3
#endif // ! __sparc

#ifdef IC_SUPPORT_FFC
#define IC_MAX_SOURCE_IDX		IC_SOURCE_FFC
#define IC_MAX_ISP_IDX			IC_ISP_RFC_STILL
#else
#define IC_MAX_SOURCE_IDX		IC_SOURCE_RFC
#define IC_MAX_ISP_IDX			IC_ISP_FFC_VIDEO
#endif

/*
 * Constant for AF stats algorithm.
 *
 * We work in s32q8 format, and "MUL_SHIFT_VALUE" means how many bits is the fractional part of the number.
 * The format s32q8 means: 32 bit signed fixed point number,
 * with 8 bits for the fractional part. This way, the value "256" represents "1".
 */ 
#define MUL_SHIFT_VALUE     8 

struct ipFrame;

#define MAX_STATS_BUFFERS   2

struct ipSourceState {
	icBayerFormat   src_fmt;
	int             width, height;
	int             running;

	void*           h_cam;  // devware camera handle
	/* Width/height of input image after cropping by Mipi Rx filter */
	uint32_t        dw_width, dw_height;
	uint32_t        dw_img_size;

	int			notificationLine;

};

struct ipStreamState {
	icIspConfig		*pendingConfig;
	uint32_t		pendingSeqNo;
	icIspConfig		*ispConfig;
	uint32_t		seqNo;
};

/*
 * This structure is large - it should be in DDR.  Only the LeonRT may
 * may accesses it, so it can be fully cached.
 */
typedef struct {
	struct ipFrame		*zsl[IP_ZSL_MAX_SIZE];
	int			zslSize;

	struct ipSourceState	rfc;
#ifdef IC_SUPPORT_FFC
	struct ipSourceState	ffc;
#endif

	struct ipStreamState	rfcVidStream;
	struct ipStreamState	rfcStillStream;
#ifdef IC_SUPPORT_FFC
	struct ipStreamState	ffcVidStream;
#endif

	char cam_base_dir[512];
} ipInternal_t;

extern ipInternal_t		ipInternal;

/* For describing frame buffers (usually in DDR) */
typedef struct ipFrame {
	void	*ptr;
	int	ls;		/* Line Stride */
	int	ps;		/* Plane Stride */
	int	width;
	int	height;
	int	bpp;		/* Bytes per pixel */
	int	nPlanes;
} ipFrame_t;

void ipResetFramepool(icCtrl *ctrl);
ipFrame_t * ipFrameAlloc(icCtrl *ctrl,
    int width, int height, int startGran, int stride, int bpp, int nPlanes);

// ip -> ic error
void
icErrorEvent(icCtrl *ctrl, uint32_t errorNo, uint32_t severity);

int ipSendEvent(icCtrl *ctrl, icEvent *ev);
void ipReapEventQ(icCtrl *ctrl);
void ipConfigureGlobal(icCtrl *ctrl);
void ipQuiesce(icCtrl *ctrl);
void ipDoProcessing(icCtrl *ctrl);
void tearDown_ipProcess (void);
int ipDumpRaw_lsc(void);
int  ipDumpRaw(icCtrl *ctrl, char *fname);
int  ipDumpRaw1(icCtrl *ctrl, char *fname);
void ipLockZSL(icCtrl *ctrl, uint32_t source, icTimestamp ts, uint32_t flags);
void ipCapture(icCtrl *ctrl, icIspConfig *cfg, void *buff, uint32_t flags);
void ipUnlockZSL(icCtrl *ctrl, void* buff);
void ipCopyToZSL();

#ifdef __cplusplus
}
#endif
