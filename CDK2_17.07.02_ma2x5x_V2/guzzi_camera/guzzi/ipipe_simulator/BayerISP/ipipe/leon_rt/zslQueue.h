/*
 * zslQueue.h
 *
 *  Created on: May 10, 2014
 *      Author: aiovtchev
 */

#ifndef ZSLQUEUE_H_
#define ZSLQUEUE_H_
#include "ipipe.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef enum {
    ZSL_BUFF_TYPE_CLEAR_RAW,
    ZSL_BUFF_TYPE_PERFECT_RAW,
//    ZSL_BUFF_TYPE_RGB,
//    ZSL_BUFF_TYPE_YUV,
}zslBuff_type;


typedef enum {
    ZSL_BUFF_EMPTY = 0,
    ZSL_BUFF_IN_USE,
    ZSL_BUFF_UNLOCKED,
    ZSL_BUFF_LOCKED
}zslBuff_status;

typedef struct {

    uint16_t        *ibuf;
    uint32_t        seqNo;
    icTimestamp     ts;
    void            *userData;
    zslBuff_type    type;
    zslBuff_status  status;
    // some other useful staff
} zslBuff_t;

typedef struct {
    pthread_mutex_t lock;
    uint32_t depth;
    uint32_t index;
    zslBuff_t *q;
} zslQ_t;

zslBuff_t *zslQueue_getEmpty(void);
void zslQueue_setFull(zslBuff_t *buff, uint32_t seqNo, uint32_t buffLock, void* userData);
zslBuff_t *zslQueue_Lock(icTimestamp ts);
void zslQueue_unLock(zslBuff_t * buff);
int zslQueue_create(uint32_t depth, uint32_t buff_size);
void zslQueue_destroy(void);

#ifdef __cplusplus
}
#endif

#endif /* ZSLQUEUE_H_ */
