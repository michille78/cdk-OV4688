///
/// @file
/// @copyright All code copyright Movidius Ltd 2014, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Simple frame buffer management
///

#include "ip_internal.h"

/* Keep all line starts aligned to this granularity */
#define ALIGNMENT	16

void
ipConfigureGlobal(icCtrl *ctrl)
{
	int	n, i, stride;

	ipResetFramepool(ctrl);

	n = ctrl->globalConfig.zslNBufs;
	if (n > IP_ZSL_MAX_SIZE) {
		n = IP_ZSL_MAX_SIZE;
	}

	/* For now, just allocate the ZSL buffers */
	stride = ctrl->globalConfig.rfcRawOutputSize.w * 2;
	if (stride & 15) {
		stride = (stride & ~15) + 16;
	}
	for (i = 0; i < n; i++) {
		if ((ipInternal.zsl[i] = ipFrameAlloc(ctrl,
		    ctrl->globalConfig.rfcRawOutputSize.w,
		    ctrl->globalConfig.rfcRawOutputSize.h,
		    16, stride, 2, 1)) == NULL) {
			break;
		}
	}

	ipInternal.zslSize = i;
}
