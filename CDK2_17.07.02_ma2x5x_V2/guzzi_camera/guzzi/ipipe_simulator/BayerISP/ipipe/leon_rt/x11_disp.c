#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <assert.h>

#include "ip_internal.h"
#include "x11_disp.h"

#define tr_abort printf
#define tr_assert(x)

extern int clp_display_scale;
extern int clp_display_vmirror;

unsigned int SWAP_IMAGE = 0;
unsigned int DIV_FACTOR = 2;
#define DW_MAIN_DISPLAY_SIZE_W      800
#define DW_MAIN_DISPLAY_SIZE_H      600

#define max(x, y) (((x) > (y)) ? (x) : (y))
#define min(x, y) (((x) < (y)) ? (x) : (y))

void save_image(char * img, unsigned int width, unsigned int height);

typedef struct {
    unsigned char b;
    unsigned char g;
    unsigned char r;
    unsigned char a;
}rgb32_pixel_t;

void put_rgb32_pixel_bw(unsigned char v, rgb32_pixel_t *p, unsigned int line_size, unsigned int scale)
{
    rgb32_pixel_t pix;
    unsigned int x, y;

    pix.b = v;
    pix.g = v;
    pix.r = v;
    pix.a = 255;

    for (y = 0; y < scale; y++) {
        for (x = 0; x < scale; x++) {
            *p++ = pix;
        }
        p = p - scale + line_size;
    }
}

void put_rgb32_pixel(unsigned char r, unsigned char g, unsigned char b, rgb32_pixel_t *p, unsigned int line_size, unsigned int scale)
{
    rgb32_pixel_t pix;
    unsigned int x, y;

    pix.b = b;
    pix.g = g;
    pix.r = r;
    pix.a = 255;

    for (y = 0; y < scale; y++) {
        for (x = 0; x < scale; x++) {
            *p++ = pix;
        }
        p = p - scale + line_size;
    }
}

static void yuv2rgb(unsigned char *rgb, int y, int u, int v)
{
    int r, g, b;
    r = y            + 1.402f*u;
    g = y - 0.344f*v - 0.714f*u;
    b = y + 1.772f*v           ;
    rgb[3] = 255;
    rgb[2] = min(max(r, 0), 255);
    rgb[1] = min(max(g, 0), 255);
    rgb[0] = min(max(b, 0), 255);
}

static void conv_uyvy_to_rgb24(
        void *rgb24,
        void *uyvy,
        unsigned int width,
        unsigned int height
    )
{
    unsigned int l, c;
    int y1, y2, u, v;
    unsigned int line_inc_rgb, line_inc_yuv;
    unsigned char *rgb, *yuv;

    rgb = rgb24; yuv = uyvy;
    line_inc_rgb = 0; line_inc_yuv = 0;

    for (l = 0; l < height; l++) {
        for (c = 0; c < width/2; c++) {
            y1 = yuv[1]; y2 = yuv[3];
            u  = yuv[0]; v  = yuv[2];

            u -= 128; v -= 128;

            yuv2rgb(rgb, y1, u, v);
            rgb += 4;
            yuv2rgb(rgb, y2, u, v);
            rgb += 4;

            yuv += 4;
        }
        rgb += line_inc_rgb;
        yuv += line_inc_yuv;
    }
}

static void conv_nv12_to_rgb24(
        void *rgb24,
        void *nv12,
        unsigned int width,
        unsigned int height
    )
{
    unsigned int l, c;
    int y11, y12, y21, y22, u, v;
    unsigned int line_inc_rgb, line_inc_yuv;
    unsigned char *rgb1, *rgb2, *yuv1, *yuv2, *uv;

    rgb1 = rgb24;
    rgb2 = rgb1 + 4*width;
    yuv1 = nv12;
    yuv2 = yuv1 + width;
    uv   = yuv1 + width*height;
    line_inc_rgb = 4*width; line_inc_yuv = width;

    for (l = 0; l < height/2; l++) {
        for (c = 0; c < width/2; c++) {
            y11 = yuv1[1]; y12 = yuv1[2];
            y21 = yuv2[1]; y22 = yuv2[2];
            u   = uv[0]  ; v   = uv[1];

            u -= 128; v -= 128;

            yuv2rgb(rgb1, y11, u, v);
            rgb1 += 4;
            yuv2rgb(rgb1, y12, u, v);
            rgb1 += 4;
            yuv2rgb(rgb2, y21, u, v);
            rgb2 += 4;
            yuv2rgb(rgb2, y22, u, v);
            rgb2 += 4;

            yuv1 += 2; yuv2 += 2; uv += 2;
        }
        rgb1 += line_inc_rgb;
        rgb2 += line_inc_rgb;
        yuv1 += line_inc_yuv;
        yuv2 += line_inc_yuv;
        uv   += line_inc_yuv;
    }
}

//#define IPIPE2DISPLAY_SHIFT   6
#define IPIPE2DISPLAY_SHIFT   0

static void conv_rgb42_to_rgb24(
        void *rgb24,
        void *rgb42,
        unsigned int width,
        unsigned int height
    )
{
    unsigned int l, c;
    unsigned short *rgb_src_r;
    unsigned short *rgb_src_g;
    unsigned short *rgb_src_b;
    unsigned char *rgb_dst;
    //    unsigned int line_inc_rgb24, line_inc_rgb42;

    rgb_dst = rgb24;

    if (SWAP_IMAGE)
    {
        rgb_dst += (width * height/DIV_FACTOR * 4) - (width - width/DIV_FACTOR) * 4 - 1;
    } // SWAP_IMAGE
    rgb_src_r = rgb42;
    rgb_src_g = &rgb_src_r[width*height];
    rgb_src_b = &rgb_src_r[width*height*2];

/*
    line_inc_rgb24 = 4*width;
    line_inc_rgb42 = 6*width;
*/
    for (l = 0; l < height/DIV_FACTOR; l++) {
        for (c = 0; c < width/DIV_FACTOR; c++) {
            if (SWAP_IMAGE)
            {
                *rgb_dst-- = 255;
                *rgb_dst-- = (char)((*rgb_src_r++) >> IPIPE2DISPLAY_SHIFT);
                *rgb_dst-- = (char)((*rgb_src_g++) >> IPIPE2DISPLAY_SHIFT);
                *rgb_dst-- = (char)((*rgb_src_b++) >> IPIPE2DISPLAY_SHIFT);

            } else {
                *rgb_dst++ = (char)((*rgb_src_b++) >> IPIPE2DISPLAY_SHIFT);
                *rgb_dst++ = (char)((*rgb_src_g++) >> IPIPE2DISPLAY_SHIFT);
                *rgb_dst++ = (char)((*rgb_src_r++) >> IPIPE2DISPLAY_SHIFT);
                *rgb_dst++ = 255;
            } //  SWAP_IMAGE

            rgb_src_r += (DIV_FACTOR-1);
            rgb_src_g += (DIV_FACTOR-1);
            rgb_src_b += (DIV_FACTOR-1);
        }

        rgb_src_r += (width*(DIV_FACTOR-1));
        rgb_src_g += (width*(DIV_FACTOR-1));
        rgb_src_b += (width*(DIV_FACTOR-1));

        if (SWAP_IMAGE) {
            rgb_dst -= (width - width/DIV_FACTOR) * 4;
        } else {
            rgb_dst += (width - width/DIV_FACTOR) * 4;
        }
    }
#if 0
    save_image(rgb24, width, height);
#endif
}

static void conv_rgb888_16bits_to_rgb24(
        void *rgb24,
        void *rgb888_16bits,
        unsigned int width,
        unsigned int height
    )
{
    unsigned int l, c;
    unsigned short *rgb_src_r;
    unsigned short *rgb_src_g;
    unsigned short *rgb_src_b;
    unsigned char *rgb_dst;
    //    unsigned int line_inc_rgb24, line_inc_rgb42;


    rgb_dst = rgb24;
    rgb_src_r = rgb888_16bits;
    rgb_src_g = &rgb_src_r[width*height];
    rgb_src_b = &rgb_src_r[width*height*2];

/*
    line_inc_rgb24 = 4*width;
    line_inc_rgb42 = 6*width;
*/
    for (l = 0; l < height; l++) {
        for (c = 0; c < width; c++) {
            *rgb_dst++ = (char)((*rgb_src_r++) >> 0);
            *rgb_dst++ = (char)((*rgb_src_g++) >> 0);
            *rgb_dst++ = (char)((*rgb_src_b++) >> 0);
            *rgb_dst++ = 255;
        }
    }
    save_image(rgb24, width, height);
}

#if defined(X11_DISPLAY_ENABLE)

#include <X11/Xlib.h>
#include <X11/Xutil.h>

//#include <trace/trace.h>
//#include <osal/osal.h>

typedef struct {
    char *display_name;
    unsigned int width;
    unsigned int height;
    unsigned int scale;         // TODO - works only on AF Stats
    disp_image_format_t format;

    char *rgb32;

    Display *display;
    Visual *visual;
    Window window;
    XImage *ximage;
} x11_disp_prv_t;

x11_disp_t disp = NULL;
x11_disp_t h3a_disp = NULL;
x11_disp_t h3a_af_disp = NULL;

#define FNAME "rgb24.img"

void save_image(char * img, unsigned int width,
                  unsigned int height)
{
FILE        *f;
char fname[256];
    sprintf(fname, "%s/%s", ipInternal.cam_base_dir, FNAME);
    if ((f = fopen(fname, "w+")) == NULL) {
        fprintf(stderr, "fopen: %s: %s\n",
                fname, strerror(errno));
       return;
    }
    fwrite (img, width,height*4, f);
    fclose(f);
}

void x11_disp_destroy(void)
{
    x11_disp_prv_t *x;
    x = disp;
    if (x) {
        free(x->rgb32);
      //  XDestroyImage(x->ximage);
      //  XDestroyWindow(x->display, x->window);
      //  XCloseDisplay(x->display);
        free(x);
        disp = x = NULL;
    }

}

void x11_disp_create(x11_disp_create_t *create)
{

    x11_disp_prv_t *x;
    static XImage g_ximage;


    x = malloc(sizeof(x11_disp_prv_t));
    tr_assert(x);
    if (disp)
        return;

    SWAP_IMAGE = clp_display_vmirror;

    switch (clp_display_scale)
    {
    case 1:
    case 2:
    case 4:
        DIV_FACTOR = clp_display_scale;
        break;
    default:
        DIV_FACTOR = clp_display_scale = 1;
    }
    if (clp_display_scale == -1)
    {
        if ((create->width > (DW_MAIN_DISPLAY_SIZE_W*6/2)) || (create->height > (DW_MAIN_DISPLAY_SIZE_H*6/2)))
        {
            DIV_FACTOR = 4;
        } else
        if ((create->width > (DW_MAIN_DISPLAY_SIZE_W*3/2)) || (create->height > (DW_MAIN_DISPLAY_SIZE_H*3/2)))
        {
            DIV_FACTOR = 2;
        } else
            DIV_FACTOR = 1;
    }
    x->display_name = create->display_name;
    x->width  = create->width;
    x->height = create->height;
    x->format = create->format;

    x->rgb32 = malloc(
            4 * x->width * x->height
        );
    tr_assert(x->rgb32);


    // Dirty hack......
    x->ximage = &g_ximage;
    g_ximage.width  = x->width/DIV_FACTOR;
    g_ximage.height = x->height/DIV_FACTOR;
    x->ximage->data = x->rgb32;

/*
    x->display = XOpenDisplay(x->display_name);
    x->visual = DefaultVisual(x->display, 0);
    x->window = XCreateSimpleWindow(
            x->display,                 // display
            RootWindow(x->display, 0), // parent
            0,                          // x
            0,                          // y
            x->width/DIV_FACTOR,        // width
            x->height/DIV_FACTOR,       // height
            1,                          // border_width
            0,                          // border
            0                           // background
        );
    x->ximage = XCreateImage(
            x->display, // display
            x->visual,  // visual
            24,         // depth
            ZPixmap,    // format
            0,          // offset
            x->rgb32,   // data
            x->width/DIV_FACTOR,  // width
            x->height/DIV_FACTOR, // height
            32,         // bitmap_pad
            0           // bytes_per_line
        );
    XSelectInput(
            x->display,
            x->window,
            ButtonPressMask | ExposureMask
        );
    XMapWindow(x->display, x->window);

    XEvent ev;
    XNextEvent(x->display, &ev);
*/
    disp = x;

    return;
}

void x11_h3a_disp_destroy(void)
{
    x11_disp_prv_t *x;

    x = h3a_disp;
    if (x) {
        XDestroyImage(x->ximage);
        XDestroyWindow(x->display, x->window);
        XCloseDisplay(x->display);
        free(x);
        h3a_disp = x = NULL;
    }
}

void x11_h3a_disp_create(x11_disp_create_t *create)
{
    x11_disp_prv_t *x;

    if (create->scale == 0)
        return;

    x = malloc(sizeof(x11_disp_prv_t));
    tr_assert(x);
    if (h3a_disp)
        return;

    x->display_name = create->display_name;
    x->width  = create->width * create->scale;
    x->height = create->height * create->scale;
    x->format = create->format;
    x->scale = create->scale;

    x->rgb32 = malloc(4 * x->width * x->height);
    tr_assert(x->rgb32);

    x->display = XOpenDisplay(x->display_name);
    x->visual = DefaultVisual(x->display, 0);
    x->window = XCreateSimpleWindow(
            x->display,                 /* display      */
            RootWindow(x->display, 0),  /* parent       */
            0,                          /* x            */
            0,                          /* y            */
            x->width,                   /* width        */
            x->height,                  /* height       */
            1,                          /* border_width */
            0,                          /* border       */
            0                           /* background   */
        );
    x->ximage = XCreateImage(
            x->display, /* display          */
            x->visual,  /* visual           */
            24,         /* depth            */
            ZPixmap,    /* format           */
            0,          /* offset           */
            x->rgb32,   /* data             */
            x->width,  /* width  */
            x->height, /* height */
            32,         /* bitmap_pad       */
            0           /* bytes_per_line   */
        );
    XSelectInput(
            x->display,
            x->window,
            ButtonPressMask | ExposureMask
        );
    XMapWindow(x->display, x->window);

    XEvent ev;
    XNextEvent(x->display, &ev);

    h3a_disp = x;

    return;
}

void x11_disp_show_h3a(void* stats, unsigned int pix_pax)
{
int w,h;
x11_disp_prv_t *x = h3a_disp;
unsigned int *h3a = stats;
char r,g,b;
char * rgb_dst;

    if (x == NULL)
        return;

    rgb_dst = x->ximage->data;

    for (h = 0; h < x->height / x->scale; ++h)
    {
        for (w = 0; w < x->width / x->scale; ++w)
        {
            r = *h3a/pix_pax;
            h3a++;
            g = *h3a/pix_pax;
            h3a++;
            b = *h3a/pix_pax;
            h3a++;

            put_rgb32_pixel(r, g, b, (rgb32_pixel_t*)rgb_dst, x->width, x->scale);
            rgb_dst += 4 * x->scale;

            h3a++; // skip sat cnt
        }
        rgb_dst  = rgb_dst + (x->width * 4) * (x->scale - 1);
    }

    XPutImage(
            x->display,                 /* display  */
            x->window,                  /* d        */
            DefaultGC(x->display, 0),   /* gc       */
            x->ximage,                  /* image    */
            0,                          /* src_x    */
            0,                          /* src_y    */
            0,                          /* dest_x   */
            0,                          /* dest_y   */
            x->width,                   /* width    */
            x->height                   /* height   */
        );
}


#define SIZE_AF_PAXEL_INT_DATA        7  // TODO - Please check icAfStatsPatch size and alignment !!!

void x11_h3a_af_disp_destroy(void)
{
    x11_disp_prv_t *x;
    x = h3a_af_disp;
    if (x) {
        XDestroyImage(x->ximage);
        XDestroyWindow(x->display, x->window);
        XCloseDisplay(x->display);
        free(x);
        h3a_af_disp = x = NULL;
    }
}

void x11_h3a_af_disp_create(x11_disp_create_t *create)
{
    x11_disp_prv_t *x;
    unsigned int width, height;

    if (create->scale == 0)
        return;

    x = malloc(sizeof(x11_disp_prv_t));
    tr_assert(x);
    if (h3a_af_disp)
        return;

    width = create->width * create->scale;
    height = create->height * create->scale;

    x->display_name = create->display_name;
    x->width  = width;
    x->height = height;
    x->format = create->format;
    x->scale = create->scale;

    x->rgb32 = malloc(4 * width * height);
    tr_assert(x->rgb32);

    x->display = XOpenDisplay(x->display_name);
    x->visual = DefaultVisual(x->display, 0);
    x->window = XCreateSimpleWindow(
            x->display,                 /* display      */
            RootWindow(x->display, 0),  /* parent       */
            0,                          /* x            */
            0,                          /* y            */
            x->width + 32,                   /* width        */
            x->height + 32,                  /* height       */
            1,                          /* border_width */
            0,                          /* border       */
            0                           /* background   */
        );
    x->ximage = XCreateImage(
            x->display, /* display          */
            x->visual,  /* visual           */
            24,         /* depth            */
            ZPixmap,    /* format           */
            0,          /* offset           */
            x->rgb32,   /* data             */
            x->width,  /* width  */
            x->height, /* height */
            32,         /* bitmap_pad       */
            0           /* bytes_per_line   */
        );
    XSelectInput(
            x->display,
            x->window,
            ButtonPressMask | ExposureMask
        );
    XMapWindow(x->display, x->window);

    XEvent ev;
    XNextEvent(x->display, &ev);

    h3a_af_disp = x;

    return;
}

void x11_disp_show_h3a_af(void* stats, unsigned int offs)
{
unsigned int scale;
unsigned int width_st, height_st;

int w, h, ix, iy;
x11_disp_prv_t *x = h3a_af_disp;
unsigned int *h3a = stats;
char r,g,b;
char * rgb_dst;
unsigned int *af_st;
unsigned int max_data;
unsigned char val;

    if (x == NULL)
        return;

    af_st = stats;
    max_data = 0;

    rgb_dst = x->ximage->data;

    scale = x->scale;
    width_st = x->width/x->scale;
    height_st = x->height/x->scale;

    // scan statistics for max
    for (h = 0; h < height_st; ++h)
    {
        for (w = 0; w < width_st; ++w)
        {
            if (*(af_st + (h * width_st + w) * SIZE_AF_PAXEL_INT_DATA + offs) > max_data ) {
                max_data = *(af_st + (h * width_st + w) * SIZE_AF_PAXEL_INT_DATA + offs);
            }
        }
    }

    if (max_data == 0) {
        max_data = 1;
    }

    for (h = 0; h < height_st; ++h)
    {
        for (w = 0; w < width_st; ++w)
        {
            val = (unsigned char)((float)(*(af_st + (h * width_st + w) * SIZE_AF_PAXEL_INT_DATA + offs))/(float)(max_data) * 255.0);
            put_rgb32_pixel_bw(255-val, (rgb32_pixel_t*)rgb_dst, x->width, scale);
            rgb_dst += 4 * scale;
        }
        rgb_dst  = rgb_dst + (x->width * 4) * (scale - 1);
    }

    XPutImage(
            x->display,                 /* display  */
            x->window,                  /* d        */
            DefaultGC(x->display, 0),   /* gc       */
            x->ximage,                  /* image    */
            0,                          /* src_x    */
            0,                          /* src_y    */
            0,                          /* dest_x   */
            0,                          /* dest_y   */
            x->width,                   /* width    */
            x->height                   /* height   */
        );
}

#else   // X11_DISPLAY_ENABLE

typedef struct {
    int width;
    int height;
    void *data;
} x_disp_ximage_t;

typedef struct {
    disp_image_format_t format;
    int width;
    int height;
    x_disp_ximage_t *ximage;
} x11_disp_prv_t;

static x_disp_ximage_t x_disp_ximage;
static x11_disp_prv_t x_disp = {
    .ximage = &x_disp_ximage,
};
static x11_disp_prv_t *disp = &x_disp;

void x11_disp_destroy(void)
{
    x11_disp_prv_t *x = disp;
    free(x->ximage->data);
}

void x11_disp_create(x11_disp_create_t *create)
{
    x11_disp_prv_t *x = disp;

    x->format = create->format;
    x->width = create->width;
    x->height = create->height;
    x->ximage->width = create->width;
    x->ximage->height = create->height;
    x->ximage->data = malloc(
            4 * x->ximage->width * x->ximage->height
        );
    assert(x->ximage->data);
}

void x11_h3a_disp_destroy(void){}
void x11_disp_show_h3a(void* stats, unsigned int pix_pax){}
void x11_h3a_disp_create(x11_disp_create_t *create){}

void x11_h3a_af_disp_destroy(void){}
void x11_disp_show_h3a_af(void* stats, unsigned int offs){}
void x11_h3a_af_disp_create(x11_disp_create_t *create){}

void save_image(char * img, unsigned int width, unsigned int height) {}

#endif   // X11_DISPLAY_ENABLE

void x11_disp_show(void *img, uint32_t frame_number)
{
    x11_disp_prv_t *x;

    if (NULL == disp)
        return;

    x = disp;

    switch (x->format) {
        case DISP_IMAGE_FORMAT_UYVY:
            conv_uyvy_to_rgb24(
                    x->ximage->data,
                    img,
                    x->ximage->width,
                    x->ximage->height
                );
            break;
        case DISP_IMAGE_FORMAT_NV12:
            conv_nv12_to_rgb24(
                    x->ximage->data,
                    img,
                    x->ximage->width,
                    x->ximage->height
                );
            break;
        case DISP_IMAGE_FORMAT_RGB42:
            conv_rgb42_to_rgb24(
                    x->ximage->data,
                    img,
                    x->width,
                    x->height
                );
            break;
        case DISP_IMAGE_FORMAT_RGB888_16BITS:
            conv_rgb888_16bits_to_rgb24(
                    x->ximage->data,
                    img,
                    x->ximage->width,
                    x->ximage->height
                );
            break;
        default:
            tr_abort("Unknown image format!\n");
    }
    { // send to remote (android) display
        extern void guzzi_camera3_capture_result(int camera_id, unsigned int stream_id, unsigned int frame_number, void *data, unsigned int data_size);
        guzzi_camera3_capture_result(0,1,
                                     frame_number,
                                     x->ximage->data,
                                     (x->width/DIV_FACTOR * x->height/DIV_FACTOR)*4);
    }
    /*
    XPutImage(
            x->display,                 // display
            x->window,                  // d
            DefaultGC(x->display, 0),  // gc
            x->ximage,                  // image
            0,                          // src_x
            0,                          // src_y
            0,                          // dest_x
            0,                          // dest_y
            x->width/DIV_FACTOR,        // width
            x->height/DIV_FACTOR        // height
        );
        */
}


