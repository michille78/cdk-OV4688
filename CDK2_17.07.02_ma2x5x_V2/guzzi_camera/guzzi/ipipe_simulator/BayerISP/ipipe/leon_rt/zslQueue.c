/*
 * zslQueue.c
 *
 *  Created on: May 10, 2014
 *      Author: aiovtchev
 */
#include <pthread.h>

#include "zslQueue.h"
#include <sys/time.h>
#include <string.h>
#include <limits.h>

static zslQ_t zslQ;

zslBuff_t *zslQueue_getEmpty(void)
{
    int rc;
    int cnt;
    zslBuff_t *empty = NULL;

    pthread_mutex_lock(&zslQ.lock);

    for (cnt = 0; cnt < zslQ.depth; ++cnt)
    {
        zslBuff_t *b;

        b = &zslQ.q[zslQ.index];
        if (b->status != ZSL_BUFF_LOCKED)
        {

            b->status = ZSL_BUFF_IN_USE;
            empty = b;
            break;
        }
        zslQ.index++;
        if (zslQ.index >= zslQ.depth)
            zslQ.index = 0;
    }
    pthread_mutex_unlock(&zslQ.lock);
    return empty;
}

void zslQueue_setFull(zslBuff_t *buff, uint32_t seqNo, uint32_t buffLock, void* userData)
{
struct timeval  tv;

    gettimeofday(&tv, NULL);

    pthread_mutex_lock(&zslQ.lock);

    if (&zslQ.q[zslQ.index] != buff)
    {
        fprintf(stderr, "ERROR: zslQ internal error!!!\n");
    }

    buff->ts = (icTimestamp)tv.tv_sec * 1000000 + tv.tv_usec;
    buff->seqNo = seqNo;
    buff->userData = userData;
    buff->type = ZSL_BUFF_TYPE_PERFECT_RAW;

    if (buffLock) {
        buff->status = ZSL_BUFF_LOCKED;
    } else {
        buff->status = ZSL_BUFF_UNLOCKED;
    }

    zslQ.index++;
    if (zslQ.index >= zslQ.depth)
        zslQ.index = 0;

    pthread_mutex_unlock(&zslQ.lock);
}

zslBuff_t *zslQueue_Lock(icTimestamp ts)
{
unsigned int cnt;
icTimestamp diff;
icTimestamp best_diff = __LONG_LONG_MAX__;
zslBuff_t *best_buff = NULL;
uint32_t index;

    pthread_mutex_lock(&zslQ.lock);
    index  = zslQ.index;
    for (cnt = 0; cnt < zslQ.depth; ++cnt)
    {
     zslBuff_t *b;

        b = &zslQ.q[index];
        if (b->status == ZSL_BUFF_UNLOCKED)
        {
            diff = b->ts - ts;
            if (diff < 0)
                diff = -diff;

            if (diff < best_diff)
            {
                best_buff = b;
                best_diff = diff;
            }
        }
        index++;
        if (index >= zslQ.depth)
            index = 0;
    }
    if (best_buff) {
        best_buff->status = ZSL_BUFF_LOCKED;
    }
    pthread_mutex_unlock(&zslQ.lock);
    return best_buff;
}

void zslQueue_unLock(zslBuff_t * buff)
{
    pthread_mutex_lock(&zslQ.lock);

    if (NULL == buff) {
        fprintf(stderr, "%s(%d): zslQ internal error!!!\n", __func__, __LINE__);
        return;
    }
    if (buff->status != ZSL_BUFF_LOCKED)
    {
        fprintf(stderr, "%s(%d): zslQ internal error!!!\n", __func__, __LINE__);
        fprintf(stderr, "zslQ buffer %p status %d seqNo %d ts = %d userData %p iBuff %p\n",
                buff->status,
                buff->seqNo,
                buff->ts,
                buff->userData,
                buff->ibuf
        );
    }

    buff->status = ZSL_BUFF_UNLOCKED;
    pthread_mutex_unlock(&zslQ.lock);
}

int zslQueue_create(uint32_t depth, uint32_t buff_size)
{
uint8_t *mem;
int     cnt;
zslBuff_t *q;
int rc;

    zslQ.depth = depth;
    zslQ.index = 0;

    if ((rc = pthread_mutex_init(&zslQ.lock, NULL)) != 0)
    {
            fprintf(stderr, "pthread_mutex_init: %s\n", strerror(rc));
            exit (-1);
    }
    zslQ.q = (zslBuff_t*)calloc(depth, sizeof (zslBuff_t));
    if (NULL == zslQ.q)
    {
        fprintf(stderr, "ERROR: No enough memory\n");
        pthread_mutex_destroy (&zslQ.lock);
        return -2;
    }
    mem = (uint8_t *)calloc(depth, buff_size);
    if (NULL == mem)
    {
        pthread_mutex_destroy (&zslQ.lock);
        fprintf(stderr, "ERROR: No enough memory\n");
        free(zslQ.q);
        return -3;
    }
    q = zslQ.q;
    for (cnt = 0; cnt < depth; ++cnt) {
        q->ibuf = (uint16_t *)mem;
        mem += buff_size;
        q++;
    }
    return 0;
}
void zslQueue_destroy(void)
{
    free (zslQ.q[0].ibuf);
    free (zslQ.q);
    pthread_mutex_destroy (&zslQ.lock);
}


