/*
 * devware.c
 *
 *  Created on: Mar 28, 2014
 *      Author: aiovtchev
 */
#include "ip_internal.h"
#include "devware.h"
#include <errno.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include <utils/mms_debug.h>
mmsdbg_define_variable(
        vdl_devware,
        DL_DEFAULT,
        0,
        "devware",
        "DEVWARE"
    );

#define MMSDEBUGLEVEL mmsdbg_use_variable(vdl_devware)

#ifdef USE_DEVWARE

extern icCtrl g_icCtrl;

//#define DW_USE_DEFAULT_LOCATIONS

#ifndef NULL
#define NULL ((void*)0)
#endif

typedef enum {
    AR1330,
    IMX214,
    HIGHLANDS
}sensors_ids;

struct dw_sen_files {
    sensors_ids sensor_id;
    char xsdat_path[512];
    char xsdat[512];
    char ini[512];
    char ini_path[512];
    char log[512];
}dw_files;


#define DW_ENV_VAR "DW_SEN_DATA_PATH"

#define DW_SENS_LOG         "Devware_sensor.log"

#define DW_AR1330_SENS_NAME        "AR1330"
#define DW_AR1330_SENS_XSDAT       "AR1330-REV2.xsdat"
#define DW_AR1330_SENS_INI         "AR1330-REV2.ini"


#define DW_IMX214_SENS_NAME        "IMX214"
#define DW_IMX214_SENS_XSDAT       "IMX214.xsdat"
#define DW_IMX214_SENS_INI         "IMX214.ini"

#define DW_HIGHLANDS_SENS_NAME        "HIGHLANDS"
#define DW_HIGHLANDS_SENS_XSDAT       "HIGHLANDS.xsdat"
#define DW_HIGHLANDS_SENS_INI         "HIGHLANDS.ini"




//#define AR1230_MODE_4016_3016 "12M_Full+Bdr/20FPS"
//#define AR1230_MODE_4000_3000 "12M_Full/15FPS"
//#define AR1230_MODE_2000_1500 "3M_Bin2/30FPS"
//#define AR1230_MODE_640_480   "VGA_HQ/30FPS"
//#define AR1230_MODE_ 640_480 "VGA_HS/90FPS"
//#define AR1230_MODE_ 320_240 "QVGA_HS/90FPS"
//#define AR1230_MODE_4000_2250 "9M_Full/30FPS"
//#define AR1230_MODE_1920_1080 "1080p_HQ/30FPS"
//#define AR1230_MODE_1280_720 "720p_HQ/30FPS"
  #define AR1230_MODE_856_480 "WVGA_HQ/30FPS"
//#define AR1230_MODE_1920_1080 "1080p_HS/60FPS"
//#define AR1230_MODE_1280_720 "720p_HS/60FPS"
//#define AR1230_MODE_ 856_480 "WVGA_HS/60FPS"
#define AR1230_MODE_4208_3120_FULL "13M_Full_100%HFOV_4208x3120_27fps"
#define AR1230_MODE  AR1230_MODE_4208_3120_FULL

//TODO create modes for imx214
#define IMX214_MODE_4208_3120_FULL_15FPS "IMX214_Full_Res_HQ_15FPS"
#define IMX214_MODE_WVGA_15FPS "IMX214_WVGA_HQ_15FPS"
#define IMX214_MODE_4208_3120_FULL_30FPS "IMX214_Full_Res_HQ_30FPS"
#define IMX214_MODE_WVGA_30FPS "IMX214_WVGA_HQ_30FPS"

static AP_HANDLE dw_apbase_handle;
extern int clp_camera_select;

int dw_get_data_path(void)
{
    char *cp;
    const char *env = DW_ENV_VAR;
    int log_index, err;

    cp = getenv(env);
    if (cp == NULL)
    {
        char file_path[512];
        strcpy(file_path, __FILE__);
        cp = strstr(file_path, "/ipipe/");
        if (cp) {
            strcpy(cp, "/prebuild/devware/");
            cp = file_path;
        } else {
            cp = "build/guzzi/projects/apps/camera/";
        }
       printf ("\nERROR: PLease specify environment variable %s\n", DW_ENV_VAR);
       printf ("\nTrying with %s\n", cp);
    }

    snprintf(dw_files.ini_path, sizeof (dw_files.ini), "%s/%s",
             cp, "apps_data");

    snprintf(dw_files.xsdat_path, sizeof (dw_files.xsdat), "%s/%s/",
             cp, "sensor_data");

    log_index = 0;
    do {
        snprintf(dw_files.log, sizeof(dw_files.log)-1, "%s/%s.%d",
                 cp, DW_SENS_LOG, log_index++);

        err = access(dw_files.log, F_OK);
        if (log_index > 100) {
            printf("\nPlease clean LOG files\n");
            break;
        }
    } while (!err);

    return 0;
}

int dw_get_sensor_spec_data_path(const char * dw_sens_ini, const char * dw_sens_xsdat)
{
    char *cp;
    const char *env = DW_ENV_VAR;
    int err;

    cp = getenv(env);
    if (cp == NULL)
    {
        char file_path[512];
        strcpy(file_path, __FILE__);
        cp = strstr(file_path, "/ipipe/");
        if (cp) {
            strcpy(cp, "/prebuild/devware/");
            cp = file_path;
        } else {
            cp = "build/guzzi/projects/apps/camera/";
        }
       printf ("\nERROR: PLease specify environment variable %s\n", DW_ENV_VAR);
       printf ("\nTrying with %s\n", cp);
    }
    snprintf(dw_files.ini, sizeof (dw_files.ini), "%s/%s/%s",
             cp, "apps_data", dw_sens_ini);

    snprintf(dw_files.xsdat, sizeof (dw_files.xsdat), "%s/%s/%s",
             cp, "sensor_data", dw_sens_xsdat);

    err = access(dw_files.ini, F_OK);
    if (err) {
        printf("\nMissing file %s %s\n", dw_files.ini, strerror(errno));
        return -1;
    }

    err = access(dw_files.xsdat, F_OK);
    if (err) {
        printf("\nMissing file %s %s\n", dw_files.xsdat, strerror(errno));
        return -1;
    }

    return 0;
}

int dw_init(AP_HANDLE* h_cam, ap_u32 *width, ap_u32 *height, ap_u32 *buf_size)
{
    AP_HANDLE h_apbase;
    ap_u32 dw_w, dw_h, dw_size;
    const char *devware_mode;
#ifndef DW_USE_DEFAULT_LOCATIONS
    int err;
#endif

    * h_cam = NULL;
    if (dw_get_data_path())
    {
        return -1;
    }

#ifdef DW_USE_DEFAULT_LOCATIONS
    printf ("ap_DeviceProbe\n");
    int retVal = ap_DeviceProbe(NULL);
    if (retVal != AP_CAMERA_SUCCESS)
    {
        printf("\nUnable to either detect a sensor or find a matching SDAT file.\nWill use file for input\n");
        return -1;
    }

    printf ("ap_Create\n");
    h_apbase = ap_Create(0);
    if (h_apbase == NULL)
    {
        printf("\nCamera initialization error!\n");
        return -2;
    }
#endif // DW_USE_DEFAULT_LOCATIONS

#ifdef DEVWARE_X_LOG_FILES
    ap_OpenIoLog(MI_ALL_LOGS, files->log);
#endif

#ifdef DW_USE_DEFAULT_LOCATIONS
    //DW_USE_DEFAULT_LOCATIONS
    //  Callbacks for notifications that we will need
//    ap_SetCallback_MultipleChoice(h_apbase, MultipleChoice_Callback, this);
//    ap_SetCallback_ErrorMessage(h_apbase, ErrorMessage_Callback, this);
//    ap_SetCallback_BeforeSetSensorReg(h_apbase, BeforeSetSensorReg_Callback, this);
//    ap_SetCallback_AfterSetSensorReg(h_apbase, AfterSetSensorReg_Callback, this);

    //  Initialize the sensor
    printf ("ap_LoadPreset\n");
    ap_LoadIniPreset(h_apbase, NULL, NULL);
#else

    err = ap_DeviceProbe(dw_files.xsdat_path);
    if (err) {
        printf("Can't probe devices from: %s\n", dw_files.xsdat_path);
        switch(clp_camera_select)
        {
        case IMX214:
            if (dw_get_sensor_spec_data_path(DW_IMX214_SENS_INI, DW_IMX214_SENS_XSDAT))
                {
                    return -1;
                }
            break;
        case HIGHLANDS:
            if (dw_get_sensor_spec_data_path(DW_HIGHLANDS_SENS_INI, DW_HIGHLANDS_SENS_XSDAT))
                {
                    return -1;
                }
            break;
        default:
            if (dw_get_sensor_spec_data_path(DW_IMX214_SENS_INI, DW_IMX214_SENS_XSDAT))
                {
                    return -1;
                }
            break;
        }

        err = ap_DeviceProbe(dw_files.xsdat);
        if (err) {
            printf("Can't probe device %s\n", dw_files.xsdat);
            return -1;
        }
        printf("Forse probe of device %s\n", dw_files.xsdat);

        dw_files.sensor_id = IMX214;
        if (*width < 4208) {
            devware_mode = IMX214_MODE_WVGA_30FPS;
        } else {
            devware_mode = IMX214_MODE_4208_3120_FULL_30FPS;
        }
    } else {
        if (dw_get_sensor_spec_data_path(DW_AR1330_SENS_INI, DW_AR1330_SENS_XSDAT)) {
            return -1;
        }
        dw_files.sensor_id = AR1330;
        if (*width < 4208) {
            devware_mode = AR1230_MODE_856_480;
        } else {
            devware_mode = AR1230_MODE_4208_3120_FULL;
        }
    }

    h_apbase = ap_Create(0);
    if (!h_apbase) {
        printf("Can't get sensor handle\n");
        return -2;
    }

    err = ap_LoadIniPreset(h_apbase, dw_files.ini, NULL);
    if (err != AP_INI_SUCCESS) {
        printf("Can't load init file %s\n", dw_files.ini);
        return -3;
    }

    err = ap_LoadIniPreset(h_apbase, dw_files.ini, devware_mode);
    if (err != AP_INI_SUCCESS) {
        printf("Can't load mode '%s' on file %s\n",
               devware_mode, dw_files.ini);
        return -3;
    }

#if 0
    err = ap_LoadIniPreset(h_apbase, dw_files.ini, "Lens_correction");
    if (err != AP_INI_SUCCESS) {
        printf("Can't start Lens correction\n");
    }
#endif

#if 0
    err = ap_LoadIniPreset(h_apbase, dw_files.ini, "Color Processing ON");
    if (err != AP_INI_SUCCESS) {
        printf("Can't start color processing\n");
    }
#endif

    err = ap_LoadIniPreset(h_apbase, dw_files.ini, "Start_streaming");
    if (err != AP_INI_SUCCESS) {
        printf("Can't start streaming\n");
        return -3;
    }

#endif //DW_USE_DEFAULT_LOCATIONS

    printf ("ap_SensorState\n");
    ap_CheckSensorState(h_apbase, 0);

    // Call reset if sensor is IMX214 to set "Serial Receiver" to
    // "MIPI, 4Lane"
    if(IMX214 == dw_files.sensor_id) {
        err = ap_LoadIniPreset(h_apbase, dw_files.ini, "Reset");
        if (err != AP_INI_SUCCESS) {
            printf("Can't call Reset\n");
            return -3;
        }
    }

    printf ("ap_GetImageFormat\n");
    printf("xxx %p\n", h_apbase);
    ap_GetImageFormat(h_apbase, &dw_w, &dw_h, NULL, 0);
    printf("DEVWARE: Image width %d height %d\n", dw_w, dw_h);
    printf("Return %p\n", h_apbase);
    dw_size = ap_GrabFrame(h_apbase, NULL, 0);
    printf("Size: %d\n", dw_size);
    *buf_size = dw_size;
    *width = dw_w;
    *height = dw_h;
    dw_apbase_handle = *h_cam = h_apbase;
    return 0;
}

void dw_destroy(AP_HANDLE h_cam)
{
    ap_Destroy(h_cam);
    ap_Finalize();
}


void dw_read_frame(AP_HANDLE h_cam, void* buff, ap_u32 buf_size)
{
    ap_u32 err;

    PROFILE_ADD(PROFILE_ID_SENSOR_READ_FRAME, 0xBBBB, 0);
    err = ap_GrabFrame(h_cam, buff, buf_size);
    PROFILE_ADD(PROFILE_ID_SENSOR_READ_FRAME, 0xEEEE, 0);

//    printf ("Grab Frame result = 0x%x h_cam = %p\n", err, h_cam);

#if 0
    {
        FILE *f;
        char fname[256];
        sprintf (fname, "%s/%s", ipInternal.cam_base_dir, "grab.raw");
        f = fopen (fname, "w+");
        if (f != NULL)
        {
            printf ("Write to : %s\n", fname);
            fwrite(buff, buf_size, 1, f);
            fclose(f);
        } else {
            printf ("Can't open file: %s\n", fname);
        }
    }
#endif
}

void dw_set_sensor_reg(uint32_t r_addr, uint32_t r_val)
{
    ap_s32  error_code, side_effects;

    if (ipInternal.rfc.h_cam == NULL)
        return;


    pthread_mutex_lock(&g_icCtrl.senMutex);
    error_code = ap_SetSensorRegisterAddr(ipInternal.rfc.h_cam,
            MI_REG_ADDR,
            0,                      // ?
            r_addr, // in_params->address,
            16,    // in_params->bitsUsed,    // width of the register
            (uint16_t)r_val, // in_params->value,
            &side_effects);
    pthread_mutex_unlock(&g_icCtrl.senMutex);
}

void dw_get_sensor_reg(uint32_t r_addr, uint32_t *r_val)
{
    printf ("Dummy DevWare reg read!!!");
    *r_val = r_addr;

    //TODO: DevWare read is not working that way:
#if 0
    ap_s32  error_code, side_effects;

    if (ipInternal.rfc.h_cam == NULL)
        return;


    pthread_mutex_lock(&g_icCtrl.senMutex);
    error_code = ap_GetSensorRegisterAddr(ipInternal.rfc.h_cam,
            MI_REG_ADDR,
            0,                      // ?
            r_addr, // in_params->address,
            16,    // in_params->bitsUsed,    // width of the register
            r_val,
            1);
    pthread_mutex_unlock(&g_icCtrl.senMutex);
#endif
}

#define DW_VALUES_SIZE 1024
static ap_u32 dw_values[DW_VALUES_SIZE];

int dw_i2c_write(
        uint32_t bus,
        uint32_t slave_addr,
        uint32_t reg_addr,
        uint32_t reg_addr_size,
        uint32_t num,
        uint8_t *val
    )
{
    int i;
    int err;

    if (DW_VALUES_SIZE < num) {
        printf("Max data size is: %d. Requested: %d", DW_VALUES_SIZE, num);
        return -1;
    }

    pthread_mutex_lock(&g_icCtrl.senMutex);
    PROFILE_ADD(PROFILE_ID_SENSOR_WRITE_SETTINGS, 0xBBBB, reg_addr);
    for (i = 0; i < num; i++) {
        dw_values[i] = val[i];
    }
    err = ap_SetRegisters(
            dw_apbase_handle,
            bus,
            slave_addr,
            reg_addr,
            reg_addr_size * 8,
            8,
            num,
            dw_values
        );
    PROFILE_ADD(PROFILE_ID_SENSOR_WRITE_SETTINGS, 0xEEEE, 0);
    pthread_mutex_unlock(&g_icCtrl.senMutex);

    return err ? -1 : 0;
}

int dw_i2c_read(
        uint32_t bus,
        uint32_t slave_addr,
        uint32_t reg_addr,
        uint32_t reg_addr_size,
        uint32_t num,
        uint8_t *val
    )
{
    int i;
    int err;

    if (DW_VALUES_SIZE < num) {
        printf("Max data size is: %d. Requested: %d", DW_VALUES_SIZE, num);
        return -1;
    }

    pthread_mutex_lock(&g_icCtrl.senMutex);
    err = ap_GetRegisters(
            dw_apbase_handle,
            bus,
            slave_addr,
            reg_addr,
            reg_addr_size * 8,
            8,
            num,
            dw_values
        );
    for (i = 0; i < num; i++) {
        val[i] = dw_values[i];
    }
    pthread_mutex_unlock(&g_icCtrl.senMutex);

    return err ? -1 : 0;
}

#else // USE_DEVWARE

int dw_init(void **h_cam,  uint32_t *width, uint32_t *height, uint32_t *buf_size)
{
    *h_cam = NULL;
    *width  = 1;
    *height = 1;
    *buf_size = 1;
    return 0;
}
void dw_destroy(void* h_cam)
{

}
void dw_read_frame(void* h_cam, void* buff, uint32_t buf_size)
{
    printf ("Error: calling %s in build without devware support!!!", __func__);
}

void dw_set_sensor_gain(float gain)
{}

void dw_set_sensor_exp(uint32_t exp_value)
{}

void dw_set_sensor_reg(uint32_t r_addr, uint32_t r_val)
{}

void dw_get_sensor_reg(uint32_t r_addr, uint32_t *r_val)
{}

int dw_i2c_write(
        uint32_t bus,
        uint32_t slave_addr,
        uint32_t reg_addr,
        uint32_t reg_addr_size,
        uint32_t num,
        uint8_t *val
    )
{
    return 0;
}

int dw_i2c_read(
        uint32_t bus,
        uint32_t slave_addr,
        uint32_t reg_addr,
        uint32_t reg_addr_size,
        uint32_t num,
        uint8_t *val
    )
{
    return 0;
}

#endif // USE_DEVWARE


