/*
 * devware.h
 *
 *  Created on: Mar 28, 2014
 *      Author: aiovtchev
 */

#ifndef DEVWARE_H_
#define DEVWARE_H_

#ifdef __cplusplus
extern "C" {
#endif

#ifdef USE_DEVWARE
#include "apbase.h"

int dw_init(AP_HANDLE* h_cam, ap_u32 *width, ap_u32 *height, ap_u32 *buf_size);
void dw_destroy(AP_HANDLE h_cam);
void dw_read_frame(AP_HANDLE h_cam, void* buff, ap_u32 buf_size);

#else
int dw_init(void **h_cam, uint32_t *width, uint32_t *height, uint32_t *buf_size);
void dw_destroy(void* h_cam);
void dw_read_frame(void* h_cam, void* buff, uint32_t buf_size);
#endif

#ifdef __cplusplus
}
#endif
#endif /* DEVWARE_H_ */
