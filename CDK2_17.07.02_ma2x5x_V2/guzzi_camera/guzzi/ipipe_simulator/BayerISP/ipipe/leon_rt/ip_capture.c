///
/// @file
/// @copyright All code copyright Movidius Ltd 2014, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     Event queue handling
///

#include <string.h>
#include <ctype.h>
#include <math.h>
#include <unistd.h>

#include "ip_internal.h"
#include "zslQueue.h"

#ifdef PC_BUILD

#define NAME_MAX_SIZE       2048

static void
ispEvent(icCtrl *ctrl, uint32_t event, uint32_t instance, void* userData)
{
	icEvent		ev;

	ev.ctrl = event;
	ev.u.ispEvent.ispInstance = instance;
	ev.u.ispEvent.userData = userData;
	ev.u.ispEvent.seqNo = ipInternal.rfcVidStream.seqNo;
	ipSendEvent(ctrl, &ev);
}

static float
f16_to_f32(uint16_t h)
{
    int         sign = ((h>>15) & 1) ? -1 : 1;
    int         exp  = ((h >> 10) & 0x1f);
    unsigned    mant = h & 0x3ff;
    float       f;

    switch (exp) {
      case 31:
        if (mant == 0) {
            return (1.0f/0.0f) * sign;    /* +/- Infinity */
        } else {
            return (0.0f/0.0f) * -sign;   /* +/- nan */
        }
        break;
      case 0:
        return 0;
    }

    exp -= 15;

    f = sign * (1.0 + (float)mant / (1<<10));
    if (exp < 0) {
        return f / (1<<-exp);
    } else {
        return f * (1<<exp);
    }
}

static void
build_gamma_lut(icIspConfig *cfg, float *table, int entries)
{
    unsigned        i, idx;
    unsigned        offLsbTab[11] = {10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0};
    unsigned        offLsb;
    unsigned        lutSize = cfg->gamma.size;
    unsigned        epr = lutSize/16;   // Entries / Range
    unsigned        rsi;                // Range Size Indicator
    float           f1, f2, frac;
    uint32_t        f32;
    uint16_t        *gammaIn = cfg->gamma.table, f16;
    unsigned        expo, mant, fmask, idx1, idx2;

    rsi = roundf(logf(epr)/logf(2));        // rsi = log2(epr)
    offLsb = offLsbTab[rsi];
    fmask = (1<<offLsb) - 1;

    for (i = 0; i < entries; i++) {
        f1 = (float)i / (entries-1);
        f32 = *(uint32_t *)&f1;

        /* Turn the fp32 value into FP16, then do the lookup as per hardware */
        expo = (((f32 >> 23) - 127 + 15) & 0xf) << 10;
        mant = (f32 >> (23-10)) & 0x3ff;
        idx1 = (expo | mant) >> offLsb;
        idx2 = idx1 + 1;
        if (idx2 > lutSize - 1) {
            idx2 = lutSize - 1;
        }
        frac = (float)(mant & fmask) / 31;

        /* Get values from LUT and convert from F16 to F32 */
        f16 = gammaIn[idx1<<2];
        *(uint32_t *)&f1 = (f16&0x3ff) << (23-10) | ((f16 >> 10)-15+127) << 23;
        f16 = gammaIn[idx2<<2];
        *(uint32_t *)&f2 = (f16&0x3ff) << (23-10) | ((f16 >> 10)-15+127) << 23;

        table[i] = (f1 * (1.0-frac) + f2 * frac);
    }
}

static void
icConfigDumpTxtMA2100(icIspConfig *cfg, int w, int h, int bitdepth,
    unsigned bayerOrder, const char *fname, const char *fname_raw)
{
	FILE		*fid;
	unsigned	x, y;
	unsigned	tw, th, ti;
	float   	*pf;
	uint16_t	*p16;
	uint8_t		*p8;
	float		rt;
    char        tmp_fname[NAME_MAX_SIZE];
    const char  *order;
    float       *table;

	switch (bayerOrder) {
	  case IC_BAYER_FORMAT_GRBG:
        order = "GR";
        break;
	  case IC_BAYER_FORMAT_GBRG:
        order = "GB";
        break;
	  case IC_BAYER_FORMAT_RGGB:
        order = "RG";
        break;
	  case IC_BAYER_FORMAT_BGGR:
        order = "BG";
        break;
    }

    /* Dump Pre-Processor config */
    sprintf(tmp_fname, "%s_isp_config_pp.txt", fname);

    if ((fid = fopen(tmp_fname, "w")) == NULL) {
		perror(fname);
		return;
	}

	fprintf(fid, ";Config file for Movidius MA2100 Bayer ISP pipeline (pre-processor)\n");
	fprintf(fid, ";AUTOMATICALLY GENERATED\n");

	fprintf(fid, "\n[General]\n");
	fprintf(fid, "filename = %s\n", fname_raw);
	fprintf(fid, "width = %d\n", w);
	fprintf(fid, "height = %d\n", h);
	fprintf(fid, "skip_lines = 0\n");
	fprintf(fid, "bits = %d\n", bitdepth);
	fprintf(fid, "bayer_order = %s\n", order);

    fprintf(fid, "\n[Capture params]\n");
    fprintf(fid, "exposure_time = %d\n", cfg->env.exp);
    fprintf(fid, "gain_iso = %d\n", cfg->env.gain);
    fprintf(fid, "color_temperature = %d\n", cfg->env.colour_temp);

	fprintf(fid, "\n[Mipi RX]\n");
	fprintf(fid, "black_level_gr = %f\n", (float)cfg->blc.gr / (1<<10));
	fprintf(fid, "black_level_r  = %f\n", (float)cfg->blc.r / (1<<10));
	fprintf(fid, "black_level_b  = %f\n", (float)cfg->blc.b / (1<<10));
	fprintf(fid, "black_level_gb = %f\n", (float)cfg->blc.gb / (1<<10));

	fprintf(fid, "\n[LSC]\n");
	tw = cfg->lsc.lscWidth;
	th = cfg->lsc.lscHeight;
	fprintf(fid, "mesh_width = %d\n", tw);
	fprintf(fid, "mesh_height = %d\n", th);

	fprintf(fid, "mesh =\n");
	p16 = cfg->lsc.pLscTable;
	for (y = 0; y < th; y++) {
		for (x = 0; x < tw; x++) {
			fprintf(fid, "0x%04x\t", p16[y*tw+x]);	/* Gr */
		}
		fprintf(fid, "\n");
	}

	fprintf(fid, "\n[Raw]\n");

	fprintf(fid, "output_bits = 10\n");

	fprintf(fid, "gain_gr = 0x%04x\n", cfg->raw.gainGr);
	fprintf(fid, "gain_gb = 0x%04x\n", cfg->raw.gainGb);
	fprintf(fid, "gain_r = 0x%04x\n", cfg->raw.gainR);
	fprintf(fid, "gain_b = 0x%04x\n", cfg->raw.gainB);
	ti = (1<<10)-1;
	fprintf(fid, "sat_gr = 0x%04x\n", ti);
	fprintf(fid, "sat_gb = 0x%04x\n", ti);
	fprintf(fid, "sat_r = 0x%04x\n", ti);
	fprintf(fid, "sat_b = 0x%04x\n", ti);

	fprintf(fid, "dpc_alpha_g_hot = 0x%02x\n", cfg->raw.dpcAlphaHotG);
	fprintf(fid, "dpc_alpha_rb_hot = 0x%02x\n", cfg->raw.dpcAlphaHotRb);
	fprintf(fid, "dpc_alpha_g_cold = 0x%02x\n", cfg->raw.dpcAlphaColdG);
	fprintf(fid, "dpc_alpha_rb_cold = 0x%02x\n", cfg->raw.dpcAlphaColdRb);
	fprintf(fid, "dpc_noise_level = 0x%04x\n",  cfg->raw.dpcNoiseLevel);

	fprintf(fid, "grgb_imbal_plat_dark = %d\n", cfg->raw.grgbImbalPlatDark);
	fprintf(fid, "grgb_imbal_decay_dark = %d\n", cfg->raw.grgbImbalDecayDark);
	fprintf(fid, "grgb_imbal_plat_bright = %d\n", cfg->raw.grgbImbalPlatBright);
	fprintf(fid, "grgb_imbal_decay_bright = %d\n", cfg->raw.grgbImbalDecayBright);
	fprintf(fid, "grgb_imbal_threshold = %d\n", cfg->raw.grgbImbalThr);

	fprintf(fid, "\n[wbGains]\n");
	fprintf(fid, "gain_r = 0x%04x\n", cfg->wbGains.gainR);
	fprintf(fid, "gain_g = 0x%04x\n", cfg->wbGains.gainG);
	fprintf(fid, "gain_b = 0x%04x\n", cfg->wbGains.gainB);

	fprintf(fid, "\n[AWB Stats]\n");
	fprintf(fid, "first_patch_x = %d\n", cfg->aeAwbConfig.firstPatchX);
	fprintf(fid, "first_patch_y = %d\n", cfg->aeAwbConfig.firstPatchY);
	fprintf(fid, "patch_width = %d\n", cfg->aeAwbConfig.patchWidth);
	fprintf(fid, "patch_height = %d\n", cfg->aeAwbConfig.patchHeight);
	fprintf(fid, "patch_gap_x = %d\n", cfg->aeAwbConfig.patchGapX + cfg->aeAwbConfig.patchWidth);
	fprintf(fid, "patch_gap_y = %d\n", cfg->aeAwbConfig.patchGapY + cfg->aeAwbConfig.patchHeight);
	fprintf(fid, "n_patches_x = %d\n", cfg->aeAwbConfig.nPatchesX);
	fprintf(fid, "n_patches_y = %d\n", cfg->aeAwbConfig.nPatchesY);
	fprintf(fid, "sat_thresh = %d\n", cfg->aeAwbConfig.satThresh);

    fclose(fid);

    /* Dump STILL config */
    sprintf(tmp_fname, "%s_isp_config_still.txt", fname);

    if ((fid = fopen(tmp_fname, "w")) == NULL) {
		perror(fname);
		return;
	}

	fprintf(fid, ";Config file for Movidius MA2100 Bayer ISP pipeline (Still)\n");
	fprintf(fid, ";AUTOMATICALLY GENERATED\n");

	fprintf(fid, "\n[General]\n");
	fprintf(fid, "filename = %s\n", fname_raw);
	fprintf(fid, "width = %d\n", w);
	fprintf(fid, "height = %d\n", h);
	fprintf(fid, "skip_lines = 0\n");
	fprintf(fid, "bits = 10\n");
	fprintf(fid, "bayer_order = %s\n", order);

    fprintf(fid, "\n[Capture params]\n");
    fprintf(fid, "exposure_time = %d\n", cfg->env.exp);
    fprintf(fid, "gain_iso = %d\n", cfg->env.gain);
    fprintf(fid, "color_temperature = %d\n", cfg->env.colour_temp);

	fprintf(fid, "\n[wbGains]\n");
	fprintf(fid, "gain_r = 0x%04x\n", cfg->wbGains.gainR);
	fprintf(fid, "gain_g = 0x%04x\n", cfg->wbGains.gainG);
	fprintf(fid, "gain_b = 0x%04x\n", cfg->wbGains.gainB);

	fprintf(fid, "\n[Demosaic]\n");
	fprintf(fid, "mix_slope = %d\n", cfg->demosaic.dewormSlope);
	fprintf(fid, "mix_offset = %d\n", cfg->demosaic.dewormOffset);
	fprintf(fid, "mix_gradient_mul = %d\n", cfg->demosaic.dewormGradientMul);

	fprintf(fid, "\n[Purple Flare]\n");
	fprintf(fid, "strength = %d\n", cfg->purpleFlare.strength);

	fprintf(fid, "\n[Chroma Gen]\n");
	fprintf(fid, "epsilon = %f\n", cfg->chromaGen.epsilon);
	fprintf(fid, "scale =\n%f %f %f\n",
      cfg->chromaGen.scaleR, cfg->chromaGen.scaleG, cfg->chromaGen.scaleB);

	fprintf(fid, "\n[Median]\n");
	fprintf(fid, "kernel_size = %d\n", cfg->median.kernelSize);

	fprintf(fid, "\n[Median Mix]\n");
	fprintf(fid, "slope = %f\n", cfg->medianMix.slope);
	fprintf(fid, "offset = %f\n", cfg->medianMix.offset);

	fprintf(fid, "\n[LowPass]\n");
	fprintf(fid, "convolution =\n");
	fprintf(fid, "%f\t%f\n",
	    (float)cfg->lowpass.coefs[0],
	    (float)cfg->lowpass.coefs[1]);

	fprintf(fid, "\n[Sharpen]\n");
	fprintf(fid, "sigma = %f\n", cfg->sharpen.sigma);
	fprintf(fid, "strength_darken = %f\n", f16_to_f32(cfg->sharpen.strength_darken));
	fprintf(fid, "strength_lighten = %f\n", f16_to_f32(cfg->sharpen.strength_lighten));
	fprintf(fid, "alpha = %f\n", f16_to_f32(cfg->sharpen.alpha));
	fprintf(fid, "overshoot = %f\n", f16_to_f32(cfg->sharpen.overshoot));
	fprintf(fid, "undershoot = %f\n", f16_to_f32(cfg->sharpen.undershoot));
	fprintf(fid, "range_stop_0 = %f\n", f16_to_f32(cfg->sharpen.rangeStop0));
	fprintf(fid, "range_stop_1 = %f\n", f16_to_f32(cfg->sharpen.rangeStop1));
	fprintf(fid, "range_stop_2 = %f\n", f16_to_f32(cfg->sharpen.rangeStop2));
	fprintf(fid, "range_stop_3 = %f\n", f16_to_f32(cfg->sharpen.rangeStop3));
	fprintf(fid, "min_thr = %f\n", f16_to_f32(cfg->sharpen.minThr));

	fprintf(fid, "\n[Luma Denoise]\n");
	fprintf(fid, "strength = %f\n", cfg->lumaDenoise.strength);
	fprintf(fid, "alpha = %d\n", cfg->lumaDenoise.alpha);
	fprintf(fid, "f2 = 0x%08x\n", cfg->lumaDenoise.f2);

	fprintf(fid, "\n[Luma Denoise Ref]\n");
	fprintf(fid, "angle_of_view = %f\n", cfg->lumaDenoiseRef.angle_of_view);
	fprintf(fid, "gamma = %f\n", cfg->lumaDenoiseRef.gamma);

	fprintf(fid, "\n[Random Noise]\n");
	fprintf(fid, "strength = %f\n", cfg->randomNoise.strength);

	fprintf(fid, "\n[Local Tone Mapping]\n");
	fprintf(fid, ";Each column is a tone mapping curve for a given local intensity\n");
	p16 = cfg->ltm.curves;
	fprintf(fid, "curves =\n");
	for (y = 0; y < 15*10; y += 10) {
		fprintf(fid, "%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t\n",
		    f16_to_f32(p16[y + 0]), f16_to_f32(p16[y + 1]),
            f16_to_f32(p16[y + 2]), f16_to_f32(p16[y + 3]),
		    f16_to_f32(p16[y + 4]), f16_to_f32(p16[y + 5]),
            f16_to_f32(p16[y + 6]), f16_to_f32(p16[y + 7]),
            f16_to_f32(p16[y + 8]));
	}

	fprintf(fid, "\n[Filter Ltm]\n");
	fprintf(fid, "th1 = %d\n", cfg->ltmLBFilter.th1);
	fprintf(fid, "th2 = %d\n", cfg->ltmLBFilter.th2);
	fprintf(fid, "limit = %d\n", cfg->ltmLBFilter.limit);

	fprintf(fid, "\n[Chroma Denoise]\n");
	fprintf(fid, "th_r = %d\n", cfg->chromaDenoise.th_r);
	fprintf(fid, "th_g = %d\n", cfg->chromaDenoise.th_g);
	fprintf(fid, "th_b = %d\n", cfg->chromaDenoise.th_b);
	fprintf(fid, "limit = %d\n", cfg->chromaDenoise.limit);
	fprintf(fid, "h_enab = %d\n", cfg->chromaDenoise.hEnab);

	fprintf(fid, "\n[Grey Desaturate]\n");
	fprintf(fid, "offset = %d\n", cfg->greyDesat.offset);
	fprintf(fid, "slope = %d\n", cfg->greyDesat.slope);

	pf = cfg->colorCombine.ccm;
	fprintf(fid, "\n[Color Combine]\n");
	fprintf(fid, "ccm =\n");
	fprintf(fid, "%f\t%f\t%f\n", pf[0], pf[1], pf[2]);
	fprintf(fid, "%f\t%f\t%f\n", pf[3], pf[4], pf[5]);
	fprintf(fid, "%f\t%f\t%f\n", pf[6], pf[7], pf[8]);
	fprintf(fid, "desat_t1 = %f\n", cfg->colorCombine.desat_t1);
	fprintf(fid, "desat_mul = %f\n", cfg->colorCombine.desat_mul);

	pf = cfg->colorConvert.mat;
	fprintf(fid, "\n[Color Convert]\n");
	fprintf(fid, "mat =\n");
/* Color conversion fields are not currently populated! */
#if 1
//#ifdef COLOR_CONVERSION_POPULATED
	fprintf(fid, "%f\t%f\t%f\n", pf[0], pf[1], pf[2]);
	fprintf(fid, "%f\t%f\t%f\n", pf[3], pf[4], pf[5]);
	fprintf(fid, "%f\t%f\t%f\n", pf[6], pf[7], pf[8]);
	fprintf(fid, "offset =\n");
	pf = cfg->colorConvert.offset;
	fprintf(fid, "%f\t%f\t%f\n", pf[0], pf[1], pf[2]);
#else
	fprintf(fid, "%f\t%f\t%f\n", 0.25700000, 0.50400000, 0.09800000);
	fprintf(fid, "%f\t%f\t%f\n", -0.14800000, -0.29100000, 0.43900000);
	fprintf(fid, "%f\t%f\t%f\n", 0.43900000, -0.36800000, -0.07100000);
	fprintf(fid, "offset =\n");
	fprintf(fid, "%f\t%f\t%f\n", 0.06274510, 0.50196078, 0.50196078);
#endif

	fprintf(fid, "\n[Gamma]\n");
	fprintf(fid, "target_size = 512\n");    /* Likely ignored */
	fprintf(fid, "gamma_table =\n");
	p16 = cfg->gamma.table;

#define GAMMA_TABLE_SIZE    512
    if ((table = malloc(GAMMA_TABLE_SIZE*sizeof (*table))) == NULL) {
        perror("malloc");
        fclose(fid);
        return;
    }

    build_gamma_lut(cfg, table, GAMMA_TABLE_SIZE);

	for (y = 0; y < GAMMA_TABLE_SIZE; y += 8) {
		fprintf(fid, "%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\n",
		    table[y + 0], table[y + 1], table[y + 2], table[y + 3],
		    table[y + 4], table[y + 5], table[y + 6], table[y + 7]);
	}

    free(table);

	fclose(fid);
}

// LTM curves are 9x15 for MA2100 and 8x16 for MA2150, so need to be resampled.
// We use bilinear sampling.  Note: stride of the incoming curve table is 10!
static void
resample_and_dump_ltm_curves(FILE *fid, uint16_t *pin)
{
    int         x, y, y1, x1;
    float       fracx, fracy;
    int         intx, inty, intx1, inty1;

    for (y = 0; y < 16; y++) {
        fracy = y*14.0/15.0;
        inty = (int)fracy;
        fracy -= inty;
        inty1 = inty+1;
        if (inty1 > 14) {
            inty1 = 14;
        }
        for (x = 0; x < 8; x++) {
            fracx = x*8.0/7.0;
            intx = (int)fracx;
            fracx -= intx;
            intx1 = intx+1;
            if (intx1 > 8) {
                intx1 = 8;
            }
            fprintf(fid, "%4d\t",
              (int)((f16_to_f32(pin[inty*10+intx]) * (1.0-fracy)*(1.0-fracx) +
              f16_to_f32(pin[inty*10+intx1]) * (1.0-fracy)*(fracx) +
              f16_to_f32(pin[inty1*10+intx]) * (fracy)*(1.0-fracx) +
              f16_to_f32(pin[inty1*10+intx1]) * (fracy)*(fracx)) * 4095 + .5));
        }
        fprintf(fid, "\n");
    }
}

static void
icConfigDumpTxtMA2150(icIspConfig *cfg, int w, int h, int bitdepth,
    unsigned bayerOrder, const char *fname, const char *fname_raw)
{
	FILE		*fid;
	unsigned	x, y;
	unsigned	tw, th, ti;
	float   	*pf;
	uint8_t		*p8;
    uint16_t    *p16;
	float		rt;
    char        tmp_fname[NAME_MAX_SIZE];
    const char  *order;
    float       *table;
    int         c_corner, c_edge;

	switch (bayerOrder) {
	  case IC_BAYER_FORMAT_GRBG:
        order = "GR";
        break;
	  case IC_BAYER_FORMAT_GBRG:
        order = "GB";
        break;
	  case IC_BAYER_FORMAT_RGGB:
        order = "RG";
        break;
	  case IC_BAYER_FORMAT_BGGR:
        order = "GB";
        break;
    }

    /* Dump Pre-Processor config */
    sprintf(tmp_fname, "%s_isp_config.txt", fname);

    if ((fid = fopen(tmp_fname, "w")) == NULL) {
		perror(fname);
		return;
	}

	fprintf(fid, ";Config file for Movidius MA2150 Bayer ISP pipeline\n");
	fprintf(fid, ";AUTOMATICALLY GENERATED\n");

	fprintf(fid, "\n[General]\n");
	fprintf(fid, "filename = %s\n", fname_raw);
	fprintf(fid, "width = %d\n", w);
	fprintf(fid, "height = %d\n", h);
	fprintf(fid, "skip_lines = 0\n");
	fprintf(fid, "bits = %d\n", bitdepth);
	fprintf(fid, "bayer_order = %s\n", order);

    fprintf(fid, "\n[Capture params]\n");
    fprintf(fid, "exposure_time = %d\n", cfg->env.exp);
    fprintf(fid, "gain_iso = %d\n", cfg->env.gain);
    fprintf(fid, "color_temperature = %d\n", cfg->env.colour_temp);

	fprintf(fid, "\n[Mipi RX]\n");
	fprintf(fid, "black_level_gr = %d\n", cfg->blc.gr);
	fprintf(fid, "black_level_r  = %d\n", cfg->blc.r);
	fprintf(fid, "black_level_b  = %d\n", cfg->blc.b);
	fprintf(fid, "black_level_gb = %d\n", cfg->blc.gb);

	fprintf(fid, "\n[Sigma Denoise]\n");
	fprintf(fid, "noise_floor = 0\n");
	fprintf(fid, "thresh1_p0  = 0\n");
	fprintf(fid, "thresh2_p0  = 0\n");
	fprintf(fid, "thresh1_p1  = 0\n");
	fprintf(fid, "thresh2_p1  = 0\n");
	fprintf(fid, "thresh1_p2  = 0\n");
	fprintf(fid, "thresh2_p2  = 0\n");
	fprintf(fid, "thresh1_p3  = 0\n");
	fprintf(fid, "thresh2_p3  = 0\n");

	fprintf(fid, "\n[LSC]\n");
	tw = cfg->lsc.lscWidth;
	th = cfg->lsc.lscHeight;
	fprintf(fid, "mesh_width = %d\n", tw);
	fprintf(fid, "mesh_height = %d\n", th);

	fprintf(fid, "mesh =\n");
	p16 = cfg->lsc.pLscTable;
	for (y = 0; y < th; y++) {
		for (x = 0; x < tw; x++) {
			fprintf(fid, "0x%04x\t", p16[y*tw+x]);	/* Gr */
		}
		fprintf(fid, "\n");
	}

	fprintf(fid, "\n[Raw]\n");

	fprintf(fid, "output_bits = 10\n");

	fprintf(fid, "gain_gr = 0x%04x\n", (cfg->raw.gainGr * cfg->wbGains.gainG) >> 8);
	fprintf(fid, "gain_gb = 0x%04x\n", (cfg->raw.gainGb * cfg->wbGains.gainG) >> 8);
	fprintf(fid, "gain_r = 0x%04x\n", (cfg->raw.gainR * cfg->wbGains.gainR) >> 8);
	fprintf(fid, "gain_b = 0x%04x\n", (cfg->raw.gainB * cfg->wbGains.gainB) >> 8);
	ti = (1<<10)-1;
	fprintf(fid, "sat_gr = 0x%04x\n", ti);
	fprintf(fid, "sat_gb = 0x%04x\n", ti);
	fprintf(fid, "sat_r = 0x%04x\n", ti);
	fprintf(fid, "sat_b = 0x%04x\n", ti);

	fprintf(fid, "dpc_alpha_g_hot = 0x%02x\n", cfg->raw.dpcAlphaHotG);
	fprintf(fid, "dpc_alpha_rb_hot = 0x%02x\n", cfg->raw.dpcAlphaHotRb);
	fprintf(fid, "dpc_alpha_g_cold = 0x%02x\n", cfg->raw.dpcAlphaColdG);
	fprintf(fid, "dpc_alpha_rb_cold = 0x%02x\n", cfg->raw.dpcAlphaColdRb);
	fprintf(fid, "dpc_noise_level = 0x%04x\n",  cfg->raw.dpcNoiseLevel);

	fprintf(fid, "grgb_imbal_plat_dark = %d\n", cfg->raw.grgbImbalPlatDark);
	fprintf(fid, "grgb_imbal_decay_dark = %d\n", cfg->raw.grgbImbalDecayDark);
	fprintf(fid, "grgb_imbal_plat_bright = %d\n", cfg->raw.grgbImbalPlatBright);
	fprintf(fid, "grgb_imbal_decay_bright = %d\n", cfg->raw.grgbImbalDecayBright);
	fprintf(fid, "grgb_imbal_threshold = %d\n", cfg->raw.grgbImbalThr);

	fprintf(fid, "\n[AWB Stats]\n");
	fprintf(fid, "first_patch_x = %d\n", cfg->aeAwbConfig.firstPatchX);
	fprintf(fid, "first_patch_y = %d\n", cfg->aeAwbConfig.firstPatchY);
	fprintf(fid, "patch_width = %d\n", cfg->aeAwbConfig.patchWidth);
	fprintf(fid, "patch_height = %d\n", cfg->aeAwbConfig.patchHeight);
	fprintf(fid, "patch_gap_x = %d\n", cfg->aeAwbConfig.patchGapX + cfg->aeAwbConfig.patchWidth);
	fprintf(fid, "patch_gap_y = %d\n", cfg->aeAwbConfig.patchGapY + cfg->aeAwbConfig.patchHeight);
	fprintf(fid, "n_patches_x = %d\n", cfg->aeAwbConfig.nPatchesX);
	fprintf(fid, "n_patches_y = %d\n", cfg->aeAwbConfig.nPatchesY);
	fprintf(fid, "sat_thresh = %d\n", cfg->aeAwbConfig.satThresh);

#define LUMA_WEIGHT_R   64
#define LUMA_WEIGHT_G   127
#define LUMA_WEIGHT_B   64
	fprintf(fid, "\n[Demosaic]\n");
	fprintf(fid, "mix_slope = %d\n", cfg->demosaic.dewormSlope);
	fprintf(fid, "mix_offset = %d\n", cfg->demosaic.dewormOffset);
	fprintf(fid, "mix_gradient_mul = %d\n", cfg->demosaic.dewormGradientMul);
    fprintf(fid, "luma_weight_red = %d\n", LUMA_WEIGHT_R);
    fprintf(fid, "luma_weight_green = %d\n", LUMA_WEIGHT_G);
    fprintf(fid, "luma_weight_blue = %d\n", LUMA_WEIGHT_B);

	fprintf(fid, "\n[Chroma Gen]\n");
	fprintf(fid, "epsilon = %d\n", (int)(cfg->chromaGen.epsilon*255+.5));
	fprintf(fid, "kr = %d\n", (int)(106 * cfg->chromaGen.scaleR + .5));
	fprintf(fid, "kg = %d\n", (int)(191 * cfg->chromaGen.scaleG + .5));
	fprintf(fid, "kb = %d\n", (int)(149 * cfg->chromaGen.scaleB + .5));
    fprintf(fid, "luma_coeff_r = %d\n", LUMA_WEIGHT_R);
    fprintf(fid, "luma_coeff_g = %d\n", LUMA_WEIGHT_G);
    fprintf(fid, "luma_coeff_b = %d\n", LUMA_WEIGHT_B);
	fprintf(fid, "pfr_strength = %d\n", cfg->purpleFlare.strength);
	fprintf(fid, "desat_offset = %d\n", (int)(cfg->colorCombine.desat_t1*255+.5));
	fprintf(fid, "desat_slope  = %d\n", (int)(cfg->colorCombine.desat_mul+.5));

	fprintf(fid, "\n[Median]\n");
	fprintf(fid, "kernel_size = %d\n", cfg->median.kernelSize);
	fprintf(fid, "slope = %d\n", (int)cfg->medianMix.slope);
	fprintf(fid, "offset = %d\n", (int)(cfg->medianMix.offset*255+.5));

	fprintf(fid, "\n[Sharpen]\n");
	fprintf(fid, "sigma = %f\n", cfg->sharpen.sigma);
	fprintf(fid, "strength_darken = %f\n", f16_to_f32(cfg->sharpen.strength_darken));
	fprintf(fid, "strength_lighten = %f\n", f16_to_f32(cfg->sharpen.strength_lighten));
	fprintf(fid, "alpha = %f\n", f16_to_f32(cfg->sharpen.alpha));
	fprintf(fid, "overshoot = %f\n", f16_to_f32(cfg->sharpen.overshoot));
	fprintf(fid, "undershoot = %f\n", f16_to_f32(cfg->sharpen.undershoot));
	fprintf(fid, "range_stop_0 = %f\n", f16_to_f32(cfg->sharpen.rangeStop0));
	fprintf(fid, "range_stop_1 = %f\n", f16_to_f32(cfg->sharpen.rangeStop1));
	fprintf(fid, "range_stop_2 = %f\n", f16_to_f32(cfg->sharpen.rangeStop2));
	fprintf(fid, "range_stop_3 = %f\n", f16_to_f32(cfg->sharpen.rangeStop3));
	fprintf(fid, "min_thr = %f\n", f16_to_f32(cfg->sharpen.minThr));

	fprintf(fid, "\n[Luma Denoise]\n");
	fprintf(fid, "strength = %f\n", cfg->lumaDenoise.strength);
	fprintf(fid, "alpha = %d\n", cfg->lumaDenoise.alpha);
	fprintf(fid, "f2 = 0x%08x\n", cfg->lumaDenoise.f2);

	fprintf(fid, "\n[Luma Denoise Ref]\n");
	fprintf(fid, "angle_of_view = %f\n", cfg->lumaDenoiseRef.angle_of_view);
	fprintf(fid, "gamma = %f\n", cfg->lumaDenoiseRef.gamma);

	fprintf(fid, "\n[Local Tone Mapping]\n");
	fprintf(fid, ";Each column is a tone mapping curve for a given local intensity\n");
	p16 = cfg->ltm.curves;
	fprintf(fid, "thr = %d\n", cfg->ltmLBFilter.th1);
	fprintf(fid, "curves =\n");
	resample_and_dump_ltm_curves(fid, cfg->ltm.curves);

	fprintf(fid, "\n[DoG Denoise]\n");
	fprintf(fid, "thr = 0\n");
	fprintf(fid, "strength = 0\n");
	fprintf(fid, "sigma11 = 1.5\n");
	fprintf(fid, "sigma15 = 4.5\n");

	fprintf(fid, "\n[Chroma Denoise]\n");
	fprintf(fid, "th_r = %d\n", cfg->chromaDenoise.th_r);
	fprintf(fid, "th_g = %d\n", cfg->chromaDenoise.th_g);
	fprintf(fid, "th_b = %d\n", cfg->chromaDenoise.th_b);
	fprintf(fid, "limit = %d\n", cfg->chromaDenoise.limit);
	fprintf(fid, "h_enab = %d\n", cfg->chromaDenoise.hEnab);
	fprintf(fid, "grey_cr = %d\n", (int)(106 * cfg->chromaGen.scaleR + .5));
	fprintf(fid, "grey_cg = %d\n", (int)(191 * cfg->chromaGen.scaleG + .5));
	fprintf(fid, "grey_cb = %d\n", (int)(149 * cfg->chromaGen.scaleB + .5));
	fprintf(fid, "slope = %d\n", cfg->greyDesat.slope);
	fprintf(fid, "offset = %d\n", cfg->greyDesat.offset);
    c_corner = cfg->lowpass.coefs[0] * cfg->lowpass.coefs[0] * 255 + .5;
    c_edge   = cfg->lowpass.coefs[0] * cfg->lowpass.coefs[1] * 255 + .5;
	fprintf(fid, "conv_coeff_center = %d\n", 255 - (c_corner + c_edge)*4);
	fprintf(fid, "conv_coeff_edge   = %d\n", c_edge);
	fprintf(fid, "conv_coeff_corner = %d\n", c_corner);

	pf = cfg->colorCombine.ccm;
	fprintf(fid, "\n[Color Combine]\n");
	fprintf(fid, "ccm =\n");
	fprintf(fid, "%f\t%f\t%f\n", pf[0], pf[1], pf[2]);
	fprintf(fid, "%f\t%f\t%f\n", pf[3], pf[4], pf[5]);
	fprintf(fid, "%f\t%f\t%f\n", pf[6], pf[7], pf[8]);
	fprintf(fid, "ccm_off =\n");
	fprintf(fid, "%f\t%f\t%f\n", 0.0f, 0.0f, 0.0f);
	fprintf(fid, "kr = %d\n", (int)(65536 / (106 * cfg->chromaGen.scaleR + .5)));
	fprintf(fid, "kg = %d\n", (int)(65536 / (191 * cfg->chromaGen.scaleG + .5)));
	fprintf(fid, "kb = %d\n", (int)(65536 / (149 * cfg->chromaGen.scaleB + .5)));
	fprintf(fid, "lut_3d =\n");
	{
	    int r, g, b;
        for (b = 0; b < 16; b++) {
            for (g = 0; g < 16; g++) {
                for (r = 0; r < 16; r++) {
                    fprintf(fid, "%4d %4d %4d ",
                      4095 * r / 15, 4095 * g / 15, 4095 * b / 15);
                }
                fprintf(fid, "\n");
            }
        }
	}
	pf = cfg->colorConvert.mat;
	fprintf(fid, "\n[Color Convert]\n");
	fprintf(fid, "mat =\n");
	fprintf(fid, "%f\t%f\t%f\n", pf[0], pf[1], pf[2]);
	fprintf(fid, "%f\t%f\t%f\n", pf[3], pf[4], pf[5]);
	fprintf(fid, "%f\t%f\t%f\n", pf[6], pf[7], pf[8]);
	fprintf(fid, "offset =\n");
	pf = cfg->colorConvert.offset;
	fprintf(fid, "%f\t%f\t%f\n", pf[0], pf[1], pf[2]);

	fprintf(fid, "\n[Gamma]\n");
	fprintf(fid, "target_size = 512\n");    /* Likely ignored */
	fprintf(fid, "gamma_table =\n");
	p16 = cfg->gamma.table;

#define GAMMA_TABLE_SIZE    512
    if ((table = malloc(GAMMA_TABLE_SIZE*sizeof (*table))) == NULL) {
        perror("malloc");
        fclose(fid);
        return;
    }

    build_gamma_lut(cfg, table, GAMMA_TABLE_SIZE);

	for (y = 0; y < GAMMA_TABLE_SIZE; y += 8) {
		fprintf(fid, "%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\n",
		    table[y + 0], table[y + 1], table[y + 2], table[y + 3],
		    table[y + 4], table[y + 5], table[y + 6], table[y + 7]);
	}

    free(table);

	fclose(fid);
}

/*
 * Dump the ISP configuration state to a text file.  This can be used to
 * process the Raw image using the offline simulator.
 */
static void
icConfigDumpTxt(icIspConfig *cfg, int w, int h, int bitdepth,
    unsigned bayerOrder, const char *fname, const char *fname_raw)
{
    icConfigDumpTxtMA2100(cfg, w, h,
      bitdepth, bayerOrder, fname, fname_raw);
    icConfigDumpTxtMA2150(cfg, w, h,
      bitdepth, bayerOrder, fname, fname_raw);
}

#include <dirent.h>

// file name - example
//  file_pre_item     - raw_
//  number            - 6          006
//  image size        - 4208x3120
//  exposure time     - 6ms        6000
//  gain              - 1.5        1500
//  color temperature - 4500K      4500
//
//  raw_006_4208x3120_e6000_g1500_c4500
//

#define FILE_NUM_SIZE		3
#define FILE_MAX_NUMBER 	999 // !!! depends on FILE_NUM_SIZE
#define NUM_DELIM			"_"

static int name_find_number(unsigned int *pn, char *file_pre_item, char *name)
{
	char ntmp[NAME_MAX_SIZE];
	char *p;
	int sz, i;

	sz = strlen(file_pre_item) + FILE_NUM_SIZE + strlen(NUM_DELIM) + 1;

	// check min size
	strncpy(ntmp, name, sz);
	if (strlen(ntmp) < sz - 1) return -1;

	// check if begins with FILE_PRE_ITEM
	p = strstr(ntmp, file_pre_item);
	if (p != &ntmp[0])  return -1;

	// form number string
	p = p + strlen(file_pre_item);
	ntmp[strlen(file_pre_item) + FILE_NUM_SIZE] = 0;

	// check for digits in number
	for (i = 0; i < FILE_NUM_SIZE; i++) {
		if (isdigit(*(p+i)) == 0) return -1;
	}

	*pn = atoi(p);

	return 0;
}


static int ipGenFileName(char *dir_name,
		   char *file_pre_item,
		   unsigned int w,
		   unsigned int h,
		   unsigned int exp,
		   unsigned int gain,
		   unsigned int ct,
		   unsigned int bits,
		   char *file_name_base)
{
	DIR *dirp;
	struct dirent *dp;
	char f_name[NAME_MAX_SIZE];
	char tmp_str[NAME_MAX_SIZE];
	char num_format[NAME_MAX_SIZE];
	unsigned int nmb;
	unsigned int i, err;

	dirp = opendir(ipInternal.cam_base_dir);
    if (dirp == NULL)
            return (-1);


    nmb = 0; // first valid file number
    while ((dp = readdir(dirp)) != NULL) {
#if !defined(__sparc)
        if (dp->d_type == DT_REG)
#endif
			if ((err = name_find_number(&i, file_pre_item, dp->d_name)) == 0) {
				if (nmb <= i) {
					nmb = i + 1;
				}
			}
    }
    closedir(dirp);

    // check max number
    if (nmb > FILE_MAX_NUMBER) return -1;

    // form file base
    sprintf(num_format, "%s%dd", "%0", FILE_NUM_SIZE);
    sprintf(tmp_str, num_format, nmb);

    // first part of name and number
    sprintf(file_name_base, "%s%s%s",file_pre_item, tmp_str, NUM_DELIM);

    // second part - exp, gain, ct
    sprintf(tmp_str, "%dx%d_e%d_g%d_ct%d_bayer_%db", w, h, exp, gain, ct, bits);
    strcat(file_name_base, tmp_str);

    return 0;
}

#endif	/* PC_BUILD */

/*
 * Copy the last captured from to the "ZSL" buffer.  This is obviously a hack.
 * We need to implement a proper circular ZSL buffer, and locking logic.
 */
void
ipLockZSL(icCtrl *ctrl, uint32_t source, icTimestamp ts, uint32_t flags)
{
	icEvent		ev;

	zslBuff_t * buff;

	buff = zslQueue_Lock(ts);

	ev.ctrl = IC_EVENT_TYPE_ZSL_LOCKED;
	ev.u.buffLockedZSL.sourceInstance = IC_SOURCE_RFC;
    ev.u.buffLockedZSL.userData = NULL;
    ev.u.buffLockedZSL.buffZsl = buff;
	ev.u.buffLockedZSL.ts = buff->ts;
	ev.u.buffLockedZSL.seqNo = buff->seqNo;
	ipSendEvent(ctrl, &ev);
}
#if !defined(__sparc)
int run_isp_simulator(char *script, char *base_name)
{
	int err;
	char buffer[NAME_MAX_SIZE];
	// Check if shell script exists
	if (script != NULL) {
		if (script[0] != '/' && script[0] != '.') {
			sprintf(buffer, "./%s ", script);
		} else {
			sprintf(buffer, "%s ", script);
		}

		if ((err = access(script, F_OK)) != 0) {
			printf("\n\n !!!! ERROR: Can't find the script :  %s\n", buffer);
			printf("          Can't run the script :  %s  with parameter 'base_file_name' : %s \n", buffer, base_name);
			return -1;
		}
		strcat(buffer, base_name);
		err = system(buffer);
	}

	return 0;
}
#endif // !defined(__sparc)

extern zslBuff_t* capt_zslBuff;
extern char *file_name_item;
void
ipCapture(icCtrl *ctrl, icIspConfig *cfg, void *buff, uint32_t flags)
{
	if (file_name_item == NULL) {
#ifdef PC_BUILD
		char	fname_raw[NAME_MAX_SIZE];
		char	fname[NAME_MAX_SIZE];
		int	w = ipInternal.rfc.width;
		int	h = ipInternal.rfc.height;
		int	i;

		if (NULL == buff)
		{
			printf("\n\n !!!! Null pointer for capture buffer\n");
			return;
		}

printf("ipCapture: Capture\n");
		ispEvent(ctrl, IC_EVENT_TYPE_ISP_START, IC_ISP_RFC_STILL, cfg->userData);

		capt_zslBuff = (zslBuff_t*)buff;

		if (capt_zslBuff->status != ZSL_BUFF_LOCKED)
		{
			printf("\n\n !!!! ERROR: capture buffer (%p) is not Locked\n", capt_zslBuff);
		}
		i = ipDumpRaw(ctrl, fname_raw);

		sprintf(fname, "%s/isp_config_%dx%d_%d", ipInternal.cam_base_dir, w, h, i);
		icConfigDumpTxt(cfg,
	/* TODO: Don't harcode the sensor bit depth!! */
		w, h, 10, ipInternal.rfc.src_fmt, fname, fname_raw);

		ispEvent(ctrl, IC_EVENT_TYPE_ISP_END, IC_ISP_RFC_STILL, cfg->userData);

	//	cfg->userData = NULL;

#endif	/* PC_BUILD */

		if ((flags & IC_CAPTURE_KEEP_ZSL_LOCKED) == 0) {
			ipUnlockZSL(ctrl, buff);
		}

	} else {
#ifdef PC_BUILD
		char new_file_name_base[256];
		char fname_raw[2048];
		char fname_cfg[2048];
		int	w = ipInternal.rfc.width;
		int	h = ipInternal.rfc.height;
		int	i, err;

		if (NULL == buff)
		{
			printf("\n\n !!!! Null pointer for capture buffer\n");
			return;
		}

printf("ipCapture: Capture\n");
		ispEvent(ctrl, IC_EVENT_TYPE_ISP_START, IC_ISP_RFC_STILL, cfg->userData);

		capt_zslBuff = (zslBuff_t*)buff;

		if (capt_zslBuff->status != ZSL_BUFF_LOCKED)
		{
			printf("\n\n !!!! ERROR: capture buffer (%p) is not Locked\n", capt_zslBuff);
		}

		// TODO: RAW_BITS - should be somewhere
#define RAW_BITS		10
		int bits = RAW_BITS;
		err = ipGenFileName(ipInternal.cam_base_dir, file_name_item,
								w, h,
								cfg->env.exp, cfg->env.gain, cfg->env.colour_temp, bits,
								new_file_name_base);
		if (err == 0) {
            sprintf(fname_raw,"%s%s%s%s", ipInternal.cam_base_dir, "/", new_file_name_base, ".raw");
			i = ipDumpRaw1(ctrl, fname_raw);

            sprintf(fname_cfg,"%s%s%s", ipInternal.cam_base_dir, "/", new_file_name_base);
			icConfigDumpTxt(cfg,
				w, h, bits, ipInternal.rfc.src_fmt, fname_cfg, fname_raw);
		} else {
			printf("\n\n !!!! ERROR: Please clean captured images in Folder: %s\n", ipInternal.cam_base_dir);
		}
#if !defined(__sparc)
		// Run external shell script with Movidius ISP simulator
		if (script_name != NULL) {
			run_isp_simulator(script_name, fname_cfg);
		}
#endif // !defined(__sparc)

		ispEvent(ctrl, IC_EVENT_TYPE_ISP_END, IC_ISP_RFC_STILL, cfg->userData);

	//	cfg->userData = NULL;

#endif	/* PC_BUILD */

		if ((flags & IC_CAPTURE_KEEP_ZSL_LOCKED) == 0) {
			ipUnlockZSL(ctrl, buff);
		}
	}
}

void
ipUnlockZSL(icCtrl *ctrl, void* buff)
{
    zslQueue_unLock((zslBuff_t *)buff);
}
