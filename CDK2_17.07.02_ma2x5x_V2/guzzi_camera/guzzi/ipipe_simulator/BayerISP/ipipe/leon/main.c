///
/// @file
/// @copyright All code copyright Movidius Ltd 2014, all rights reserved.
///            For License Warranty see: common/license.txt
///
/// @brief     main leon file 
///

// Dummy LeonOS application - this is here to keep the build system happy,
// since it's designed to build for both LeonOS and LeonRT.  We only care
// about the LeonRT component (leonRTApp.rtlib).
int
main(void)
{
    return 0;
}
