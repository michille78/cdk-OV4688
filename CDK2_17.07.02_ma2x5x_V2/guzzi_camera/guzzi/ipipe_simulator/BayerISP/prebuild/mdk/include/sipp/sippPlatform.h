#ifndef __SIPP_PLATFORM_H__
#define __SIPP_PLATFORM_H__

//till MDK people wake up, I put these defs here...
//they should be in mvtypes
typedef unsigned long long UInt64;
typedef unsigned int       UInt32;
typedef unsigned short     UInt16;
typedef unsigned char      UInt8;

typedef          int       Int32;
typedef          short     Int16;
typedef          char      Int8;

#if defined(__sparc) || defined(__MOVICOMPILE__)
  #define INLINE inline
  #define ALIGNED(x) __attribute__((aligned(x)))
  #define SECTION(x) __attribute__((section(x)))
  #define DBG_PRINT(x...)
#else // PC world
  #define INLINE     //__inline
  #define ALIGNED(x) //nothing
  #define SECTION(x) //nothing
  #define DBG_PRINT printf
#endif

//#############################################################################
#if defined(__sparc) && !defined(MYRIAD2) //Thus SPARC for Myriad1 (Sabre)
 
 #include <isaac_registers.h>    // various registers used under the hood by some functions
 #include <stdio.h>              // printf
 #include <stdlib.h>             // memset, memcpy, exit
 #include <swcShaveLoader.h>
 #include <swcLeonUtils.h>
 #include <swcSliceUtils.h>
 #include <swcMemoryTransfer.h>
 #include <DrvSvu.h>

 #define VCS_PRINT_INT(a)        //nothing
 
 typedef unsigned short half; //Leon needs to know storage requirements

 void   sippSetShaveWindow(UInt32 svuNo, UInt32 winNo, UInt32 value);
 UInt32 sippSolveShaveAddr(u32 inAddr, u32 sliceNo);

 #define CONCAT(A,B) A ## B
 #define SVU_SYM(s) CONCAT(SS_,s)  // moviLink adds extra "SS_",
 #define SHAVE_MAIN main            //Leon will see something like SS__main through SVU_SYM macro
 #define U32_MEMSET(addr, len, val) swcU32memsetU32((u32*)(addr), (u32)(len), (u32)(val))
 #define U32_MEMCPY(dest, src, len) swcU32memcpy((u32*)(dest), (u32*)(src), (u32)(len))

 #define SIPP_MBIN(x)            (x)
 
 #define DDR_TEXT   __attribute__((section(".ddr_direct.text")))
 #define DDR_DATA   __attribute__((section(".ddr_direct.data")))
 #define DDR_RODATA __attribute__((section(".ddr_direct.rodata")))
 #define DDR_BSS    __attribute__((section(".ddr_direct.bss")))
 #define CMX_TEXT   __attribute__((section(".cmx.text")))
 #define CMX_RODATA __attribute__((section(".cmx.rodata")))
 #define CMX_DATA   __attribute__((section(".cmx.data")))
 #define CMX_BSS    __attribute__((section(".cmx.bss")))
 
//#############################################################################
#elif defined(__sparc) && defined(MYRIAD2) //Thus SPARC for Myriad2 
 #include <sippHwMacros.h>
 #include <registersMyriad2.h>    //various registers used under the hood by some functions
 #include <swcLeonUtilsDefines.h> //for NOP
 #include <DrvSvuDefines.h>
 #include <DrvMutex.h>
 #include <DrvSvu.h>
 #include <swcWhoAmI.h>

#if defined(SIPP_VCS)
 #include <UnitTestApi.h>
 #include <VcsHooksApi.h>
#endif

 #include <stdio.h>               //printf
 #include <swcSliceUtils.h>
 #include <swcShaveLoader.h>
 #include <swcMemoryTransfer.h>
 
 #if defined(SIPP_VCS)
 #define VCS_PRINT_INT(a) printInt((UInt32)a)
 #else
 #define VCS_PRINT_INT(a)        //nothing
 #endif

 typedef unsigned short half; //Leon needs to know storage requirements
 
 void   sippSetShaveWindow(UInt32 svuNo, UInt32 winNo, UInt32 value);
 void   sippLoadMbinMulti(u8 *mbinImg, u32 firstSlc, u32 lastSlc);
 
 UInt32 sippSolveShaveAddr(u32 inAddr, u32 sliceNo);
  
 #define CONCAT(A,B) A ## B
 #define SVU_SYM(s) CONCAT(SS_,s)   // moviLink adds extra "SS_"
 #define SHAVE_MAIN main            //Leon will see something like SS__main through SVU_SYM macro
 #define U32_MEMSET(addr, len, val) swcU32memsetU32((u32*)(addr), (u32)(len), (u32)(val))
 #define U32_MEMCPY(dest, src, len) swcU32memcpy((u32*)(dest), (u32*)(src), (u32)(len))

 #define SIPP_MBIN(x)            (x)
 
//for now dummy
 #define DDR_DATA     __attribute__((section(".ddr.data")))
 
 #ifndef CMX_TEXT
 #define CMX_TEXT
 #endif 
 
 #ifndef CMX_DATA
 #define CMX_DATA   
 #endif

 void sippWaitHWUnit(UInt32 id);

 //#############################################################################
#elif defined(__MOVICOMPILE__)
 //On Shave, Compiler knows half data type by default
 #include <string.h>
 #include <stdint.h>
 #include <svuCommonShave.h>
 
 #define SVU_SYM(s) s
 #define SHAVE_MAIN main     //on Sabre, this is the main function
 typedef half Half;

 //#############################################################################
#else //PC world
 
 #define SIPP_PC
 //#define _CRT_SECURE_NO_DEPRECATE //remove some VisualC warnings....
 #include <stdio.h>
 #include <string.h>
 #include <stdint.h>
 #include <stdlib.h>
 #include <math.h>
 #include "sippHw.h"
 
 typedef fp16 half;
 typedef fp16 Half;
 typedef unsigned long u32;
 #define SVU_SYM(s)              s
 #define SHAVE_MAIN              shave_main 

 #define U32_MEMSET(addr, len, val) memset((void*)(addr), (int)(len),   (size_t)(val))
 #define U32_MEMCPY(dest, src, len) memcpy((void*)(dest), (void*)(src), (size_t)(len))

 #define SIPP_MBIN(x)            0
  
 #define VCS_PRINT_INT(a)        //nothing
 //#define MXI_CMX_BASE_ADR        0x10000000 //what really matters is that lower 24 bits are ZERO

 #define CMX_TEXT   //dummy
 #define CMX_RODATA //dummy
 #define CMX_DATA   //dummy
 #define CMX_BSS    //dummy
 #define DDR_TEXT   //dummy
 #define DDR_RODATA //dummy 
 #define DDR_DATA   //dummy
 #define DDR_BSS    //dummy

 extern int scGetShaveNumber(); //

 #define NOP
#endif




#if defined(SIPP_PC)
 //on PC, can use larger slice size for frame check (must be <= 16MB)
  //#define SIPP_SLICE_SZ (16*1024*1024)
    #define SIPP_SLICE_SZ (128*1024)
#else
  #if !defined(SIPP_SLICE_SZ)
  #define SIPP_SLICE_SZ (128*1024)
  #endif
#endif

//an assertion: virtual slice can't exceed 24 bits of addr
//available through a window register
#if (SIPP_SLICE_SZ > 16*1024*1024)
 #error EXCEEDING_24_bits_of_ADDR
#endif

#if !defined(SIPP_SVU_STACK_SZ)
#define SIPP_SVU_STACK_SZ (8*1024)
#endif

#endif // __PLATFORM_H__
