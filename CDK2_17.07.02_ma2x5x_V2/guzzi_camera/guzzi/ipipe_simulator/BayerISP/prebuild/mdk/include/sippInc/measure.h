#ifndef _TIME_MEASURE_
#define _TIME_MEASURE_

typedef long long int int64;
typedef unsigned long long int uint64;

int64 GetTimeMs64();

#endif