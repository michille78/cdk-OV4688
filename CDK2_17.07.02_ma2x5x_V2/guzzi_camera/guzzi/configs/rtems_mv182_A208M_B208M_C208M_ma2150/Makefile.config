MAKE_TARGET_OS=RTEMS

# C Flags
#CFLAGS += -Wall -Wextra -Wcast-align -Wno-multichar -Wno-unused -Wstrict-aliasing=2
#CFLAGS += -Wall -Wcast-align -Wno-multichar -Wno-unused -Wstrict-aliasing=2

#CFLAGS += -Wstrict-overflow=1 -Wswitch -Wtrigraphs -Wuninitialized -Wunknown-pragmas
#CFLAGS += -Wunused-function -Wunused-label -Wunused-value -Wunused-variable
#CFLAGS += -Wsign-compare -Wmissing-prototypes -Wmissing-declarations
#CFLAGS += -Wunused-parameter -Wsign-conversion

DEFAULT_OPTIMIZATION_LEVEL ?= 0

#suppress some warnings until we remove old code
CFLAGS += -Wall -Wcast-align -Wno-multichar -Wno-unused -Wstrict-aliasing=2
CFLAGS += -Wstrict-overflow=1 -Wswitch -Wtrigraphs -Wuninitialized -Wunknown-pragmas
CFLAGS += -Wunused-function -Wunused-label -Wunused-value -Wunused-variable
CFLAGS += -Wformat=0
#CFLAGS += -Wmissing-prototypes -Wmissing-declarations
CFLAGS += -Wno-unused-parameter -Wno-cast-align
# -Wsign-conversion
CFLAGS += -fno-strict-aliasing -pipe
CFLAGS += -O$(DEFAULT_OPTIMIZATION_LEVEL) -g

CPPFLAGS += -Wenum-compare

CFLAGS += \
    -DMYRIAD2 \
    -DLEON_MVT \
    -D__RTEMS__ \
    -DDRAM_SIZE_MB=64 \
	--sysroot=. 	\
	-mcpu=myriad 	\
	-fno-inline-functions-called-once \
	-ffunction-sections \
	-fdata-sections \
	-fno-builtin-isinff \
	-fno-inline-small-functions  \
	-gdwarf-2 \
	-gstrict-dwarf \
	-g3 \

#	-fno-common \

# Shared Flags
SHRFLAGS +=

# Linker Flags
LDFLAGS += \
    -EL \
    -nostdlib \

LDFLAGS_APP += \
    -Wl,-EL \
    -O9 \
    -Wl,-M \
    -Wl,--gc-sections \
    -L $(MV_MDK_DIR)/common/scripts/ld/myriad2collection/ \
    -T $(MV_MDK_DIR)/common/scripts/ld/myriad2collection/myriad2_default_memory_map_elf.ldscript \

#    -Wl,-warn-common \

CPPFLAGS +=  \
    -fno-rtti \
    -fno-exceptions \


CRT_BEG += \
    $(SPARC_TOOLS_DIR)/lib/gcc/sparc-myriad-elf/4.8.2/le/crti.o \
    $(SPARC_TOOLS_DIR)/lib/gcc/sparc-myriad-elf/4.8.2/le/crtbegin.o \

CRT_MDK += \
    $(MV_MDK_DIR)/common/drivers/myriad2/system/rtems_asm/crt0.S \
    $(MV_MDK_DIR)/common/drivers/myriad2/system/rtems_asm/memmap.S \
    $(MV_MDK_DIR)/common/drivers/myriad2/system/rtems_asm/mp_rom.S \
    $(MV_MDK_DIR)/common/drivers/myriad2/system/rtems_asm/utilities.S \

CRT_END += \
    $(SPARC_TOOLS_DIR)/lib/gcc/sparc-myriad-elf/4.8.2/le/crtend.o \
    $(SPARC_TOOLS_DIR)/lib/gcc/sparc-myriad-elf/4.8.2/le/crtn.o \

LIB +=  rtemsbsp rtemscpu stdc++ gcc c supc++ m g ssp gcov ssp_nonshared
LIBDIR += $(DIRINSLIB)
LIBDIR += $(RTEMS_PREBUILD_DIR)

LIBDIR += $(MV_MDK_DIR)/common/scripts/ld/myriad2collection
LIBDIR += $(SPARC_TOOLS_DIR)/lib/gcc/sparc-myriad-elf/4.8.2/le
LIBDIR += $(SPARC_TOOLS_DIR)/sparc-myriad-elf/lib/le
#LIBDIR += $(TOPDIR)/ipipe_simulator/BayerISP/prebuild/mdk/libs/

# External Include folders
INCDIR += $(TOPDIR)/external
INCDIR += $(TOPDIR)/modules/hal/include/

INCDIR += $(TOPDIR)/
INCDIR += $(TOPDIR)/projects/
INCDIR += $(TOPDIR)/modules
INCDIR += $(TOPDIR)/modules/include
INCDIR += $(TOPDIR)/modules/

INCDIR += $(TOPDIR)/modules/libs/
INCDIR += $(TOPDIR)/modules/camera/
INCDIR += $(TOPDIR)/modules/camera/include/
INCDIR += $(TOPDIR)/modules/components/
INCDIR += $(TOPDIR)/modules/hif/include/
INCDIR += $(TOPDIR)/modules/components/auto_camera_algs/include/
INCDIR += $(TOPDIR)/modules/components/settings_gen/src/algos/sg_ae_distribution/sg_ae_distr_mms4/include/
INCDIR += $(TOPDIR)/modules/components/settings_gen/src/algos/sg_ae_limits/sg_ae_limits_mms/include/
INCDIR += $(TOPDIR)/modules/components/settings_gen/src/algos/sg_ae_smooth/sg_ae_smooth_mms4/include/
INCDIR += $(TOPDIR)/modules/components/settings_gen/include/
INCDIR += $(TOPDIR)/modules/include/
INCDIR += $(TOPDIR)/modules/rpc/transport_socket/include/
INCDIR += $(TOPDIR)/modules/rpc/rpc/include/
INCDIR += $(TOPDIR)/modules/rpc/guzzi/camera3/include/
INCDIR += $(TOPDIR)/modules/rpc/guzzi/camera3_capture_result/include/
INCDIR += $(TOPDIR)/modules/rpc/pool/include/
INCDIR += $(TOPDIR)/modules/version_info/include/
INCDIR += $(MOV_INCDIR_OSAL)

INCDIR += $(MOV_INCDIR_ALGOS)/aca/
INCDIR += $(MOV_INCDIR_ALGOS)/sg/algs/
INCDIR += $(MOV_INCDIR_ALGOS)/sg/modes/
INCDIR += $(MOV_INCDIR_ALGOS)/vpipe_params/

INCDIR += $(MOV_INCDIR_DTP_SRV)

INCDIR += $(MOV_INCDIR_HIF)/camera3/c/include
INCDIR += $(MOV_INCDIR_HIF)/nvm_reader/include
INCDIR += $(MOV_INCDIR_HIF)/rpc/include

INCDIR += $(MOV_INCDIR_SDK)
INCDIR += $(MV_MDK_DIR)/common/drivers/myriad2/socDrivers/shared/arch/$(MYRIAD_VER)/sgl/include

INCDIR += $(MV_MDK_DIR)/common/drivers/myriad2/socDrivers/include $(MV_MDK_DIR)/common/drivers/myriad2/socDrivers/osDrivers/include/ $(MV_MDK_DIR)/common/swCommon/include

DEFAULT_TIMEOUT_MS ?= 100000
CFLAGS +=-DDEFAULT_ALLOC_TIMEOUT_MS=$(DEFAULT_TIMEOUT_MS)

CFLAGS += -DCAMERA_INTERFACE_ANDROID_CAMERA3

# Flash simulator
CFLAGS += -DGZZ_FLASH_SIM

# List of excluding include folders when make check
#CHKDIR += $(TOPDIR)/external

CONFIG_OSAL_MSG = FIFO

