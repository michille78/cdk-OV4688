##############################################
#      Common environment variables          #
##############################################
include $(MAKEINCCFG)
-include $(MAKEINCMAP)

CROSS_CC    ?= gcc
CROSS_CPP   ?= g++
CROSS_AS    ?= as
CROSS_LD    ?= ld
CROSS_APP   ?= gcc
CROSS_SHR   ?= gcc
CROSS_AR    ?= ar
CROSS_STRIP ?= strip
CROSS_OBJCOPY ?= objcopy

TEMPLPROC   ?= $(DIRSCR)/tmpl_proc.sh
VERVARS     ?= $(DIRSCR)/vervars.sh

CC    := $(CROSS_COMPILE)$(CROSS_CC)
CPP   := $(CROSS_COMPILE)$(CROSS_CPP)
AS    := $(CROSS_COMPILE)$(CROSS_AS)
LD    := $(CROSS_COMPILE)$(CROSS_LD)
LDAPP := $(CROSS_COMPILE)$(CROSS_APP)
LDSHR := $(CROSS_COMPILE)$(CROSS_SHR)
AR    := $(CROSS_COMPILE)$(CROSS_AR)
STRIP := $(CROSS_COMPILE)$(CROSS_STRIP)
OBJCOPY := $(CROSS_COMPILE)$(CROSS_OBJCOPY)

COMMONDEP = $(MAKEBUILD) $(MAKEBUILD).defs $(TOPDIR)/Makefile $(TOPDIR)/$(DIRSRC)/Makefile $(MAKEINCCFG) $(MAKEINCMAP)

INCDIRLOCAL = $(subst /, ,$(call abs2loc,$(TOPDIR)/$(DIRSRC)))
INCDIRLIST := $(eval INCDIRTMP:=) $(foreach d,$(INCDIRLOCAL),$(eval INCDIRTMP:=$(addsuffix /,$(INCDIRTMP))$(d)) $(TOPDIR)/$(INCDIRTMP))
INCDIRREQ := $(addsuffix /include,$(TOPDIR) $(INCDIRLIST))
#INCDIRREQ += $(TOPDIR)/$(DIRSRC)

CFLAGS_DEFS += $(addprefix ___,$(addsuffix ___,$(MAKE_TARGET_OS)))
CFLAGS_DEFS += $(if $(MMS_DEBUG),__MMS_DEBUG__ __DEBUG)
CFLAGS += $(addprefix -D,$(CFLAGS_DEFS))

##############################################
#   Object specific environment variables    #
##############################################
include $(TOPDIR)/$(DIRSRC)/Makefile

WHOLE_ARCHIVE ?= NO

OBJECTS = $(obj)
NAME = $(strip $(firstword $(name)))
TARGET = $(addprefix $(DIRSRC)/,$(addsuffix .o,$(NAME)))
OUTAPP = $(addprefix $(DIRSRC)/,$(outapp))
OUTOBJ = $(addprefix $(DIRSRC)/,$(addsuffix .o,$(outobj)))

ifeq ($(WHOLE_ARCHIVE),YES)
    OUTWOBJ = $(addprefix $(DIRSRC)/,$(addsuffix .wo,$(outlib)))
    OUTWLIB = $(addprefix $(DIRSRC)/,$(addprefix lib,$(addsuffix .a,$(outlib))))
    OUTLIB =
    OUTSHR =
else
    OUTLIB = $(addprefix $(DIRSRC)/,$(addprefix lib,$(addsuffix .a,$(outlib))))
    OUTSHR = $(addprefix $(DIRSRC)/,$(addprefix lib,$(addsuffix .so,$(outshr))))
    OUTWOBJ =
    OUTWLIB =
endif


OBJSUB = $(foreach o,$(OBJECTS),$(if $($(o)),$($(o))/$(o).o))
OBJC = $(addprefix $(DIRSRC)/,$(src:%.c=%.o) $(srcc:%.c=%.o))
APP_OBJC = $(addprefix $(DIRSRC)/,$(appsrc:%.c=%.o) $(appsrcc:%.c=%.o))
OBJS = $(addprefix $(DIRSRC)/,$(srcasm:%.S=%.o) $(srcS:%.S=%.o))
OBJCPP = $(addprefix $(DIRSRC)/,$(srcccp:%.cpp=%.o))
OBJVER = $(addprefix $(DIRSRC)/,$(ver:%.ver=%.vero))
VERSUM = $(addprefix $(DIRSRC)/,$(ver:%.ver=%.versum))
DEPEND = $(addsuffix .dep,$(OUTOBJ) $(OUTARC) $(OBJSUB) $(OBJC) $(OBJS) $(OBJCPP))
INCDIR += $(inc) $(INCDIRREQ)
CHKDIR += $(inc) $(INCDIRREQ)

CFLAGS += $(addprefix -I,$(INCDIR))

CPPFLAGS += $(CFLAGS)

APPFLAGS += -Wl,--start-group
APPFLAGS += $(addprefix -l,$(strip $(lib) $(LIB)))
APPFLAGS += -Wl,--end-group
APPFLAGS += $(addprefix -L,$(strip $(libdir) $(LIBDIR)))
APPFLAGS += -m32
#Following two lines can be enabled if one wants to add LDFLAGS to the APP
#FIXAPPLDFLAGS = -Wl,
#APPFLAGS += $(addprefix $(FIXAPPLDFLAGS),$(LDFLAGS))

WLIBLIST += $(addprefix $(DIRINSLIB)/,$(addprefix lib,$(addsuffix .a,$(strip $(lib)))))

-include $(DEPEND)
