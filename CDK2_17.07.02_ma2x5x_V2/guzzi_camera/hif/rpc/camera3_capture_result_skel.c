/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <osal/osal_stdlib.h>
#include <osal/osal_string.h>
#include <utils/mms_debug.h>
#include <guzzi/camera3_capture_result/camera3_capture_result.h>
#include <guzzi/rpc/rpc.h>
#include <guzzi/rpc/camera3_capture_result_rpc.h>

static rpc_t *guzzi_camera3_capture_result_skel_rpc;

mmsdbg_define_variable(
        vdl_guzzi_camera3_capture_result_skel,
        DL_DEFAULT,
        0,
        "vdl_guzzi_camera3_capture_result_skel",
        "Frame info skel."
    );
#define MMSDEBUGLEVEL mmsdbg_use_variable(vdl_guzzi_camera3_capture_result_skel)

/*
 * ****************************************************************************
 * ** New frame info **********************************************************
 * ****************************************************************************
 */
int guzzi_camera3_capture_result_skel(
        rpc_t *rpc,
        rpc_packet_t *call
    )
{
    void *data;
    rpc_packet_t result;
    rpc_guzzi_camera3_capture_result_t *rpc_guzzi_camera3_capture_result;

    rpc_guzzi_camera3_capture_result = call->payload[0].p;
    data = rpc_guzzi_camera3_capture_result->data;

    result.payload[0].p = NULL;
    result.payload[0].size = 0;
    result.header.data_size = 0;

    guzzi_camera3_capture_result(
            rpc_guzzi_camera3_capture_result->camera_id,
            rpc_guzzi_camera3_capture_result->stream_id,
            rpc_guzzi_camera3_capture_result->frame_number,
            data,
            rpc_guzzi_camera3_capture_result->data_size
        );

    RPC_RETURN(rpc, call, &result);
    rpc_service_free_packet(rpc, call->header.service, call);

    return 0;
}

/*
 * ****************************************************************************
 * ** Init ********************************************************************
 * ****************************************************************************
 */
int guzzi_camera3_capture_result_skel_init(rpc_t *rpc)
{
    guzzi_camera3_capture_result_skel_rpc = rpc;
    return 0;
}

