/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <osal/osal_stdlib.h>
#include <osal/pool.h>
#include <utils/mms_debug.h>

mmsdbg_define_variable(
        vdl_guzzi_camera3_rpc_lib,
        DL_DEFAULT,
        0,
        "guzzi_camera3_rpc_lib",
        "Guzzi Camera3 RPC Lib"
    );
#define MMSDEBUGLEVEL mmsdbg_use_variable(vdl_guzzi_camera3_rpc_lib)

#define RPC_MODULE_TIMEOUT DEFAULT_ALLOC_TIMEOUT_MS

/*
 * *****************************************************************************
 * ** RPC Includes
 * *****************************************************************************
 */

/* Common */
#include <guzzi/rpc/namespace.h>
#include <guzzi/rpc/rpc.h>
#include <guzzi/rpc/transport.h>
#include <guzzi/rpc/transport_socket.h>
#include <guzzi/rpc/camera3_rpc.h>
#include <guzzi/rpc/nvm_reader_rpc.h>
#include <guzzi/rpc/camera3_capture_result_rpc.h>

/* Host */
#ifdef RPC_SIDE_HOST
#include <osal/osal_stdlib.h>
#include <guzzi/rpc/camera3_stub.h>
#include <guzzi/rpc/nvm_reader_stub.h>
#include <guzzi/rpc/camera3_capture_result_skel.h>
#endif

/* Guzzi */
#ifdef RPC_SIDE_GUZZI
#include <guzzi/rpc/camera3_skel.h>
#include <guzzi/rpc/nvm_reader_skel.h>
#include <guzzi/rpc/camera3_capture_result_stub.h>
#endif

/*
 * *****************************************************************************
 * ** RPC Modules Params
 * *****************************************************************************
 */

/* Common */
#define RPC_MODULE_SERVICE_ALLOC 0x1
#define RPC_MODULE_SERVICE_CAMERA_BASE 0x10

#define RPC_MODULE_GC3_BASE_SERVICE RPC_MODULE_SERVICE_CAMERA_BASE
#define RPC_MODULE_GC3_GET_INFO_SERVICE RPC_MODULE_SERVICE_ALLOC
#define RPC_MODULE_CAPTURE_RESULT_SERVICE RPC_MODULE_SERVICE_ALLOC
#define RPC_MODULE_TRANSPORT_NS_SIZE 32
#define RPC_MODULE_SERVICE_NS_SIZE 32
#define RPC_MODULE_SERVICE_DEFAULT_PACKET_SIZE_MAX (4*1024)
#define RPC_MODULE_SERVICE_DEFAULT_PACKETS_COUNT 4
#define RPC_MODULE_FUNCTIONS_TO_TRACK 32

#define RPC_MODULE_TCP_IP "127.0.0.1"
#define RPC_MODULE_TCP_PORT 5000

static rpc_t *rpc;
static transport_t *transport_default;
static rpc_rx_thread_t *trasport_default_rx_thread;
static pool_t *service_default_pool;

#define RPC_GUZZI_CAMERA3_FUNCTION_ID_BASE                  0x0100
#define RPC_GUZZI_CAMERA3_CAPTURE_RESULT_FUNCTION_ID_BASE   0x0200
#define RPC_GUZZI_NVM_READER_FUNCTION_ID_BASE               0x0300

#define rpc_guzzi_camera3_function_id_base                  RPC_GUZZI_CAMERA3_FUNCTION_ID_BASE
#define rpc_guzzi_camera3_capture_result_function_id_base   RPC_GUZZI_CAMERA3_CAPTURE_RESULT_FUNCTION_ID_BASE
#define rpc_guzzi_nvm_reader_function_id_base               RPC_GUZZI_NVM_READER_FUNCTION_ID_BASE

/* Host */
#ifdef RPC_SIDE_HOST
static rpc_skel_functions_table_entry_t rpc_functions_table_host[] = {
    { RPC_GUZZI_CAMERA3_CAPTURE_RESULT , guzzi_camera3_capture_result_skel },
    { RPC_GUZZI_CAMERA3_CALLBACK_NOTIFY, guzzi_camera3_stub_calback_notify },

    RPC_SKEL_FUNCTIONS_TABLE_ENTRY_NIL
};
#endif

/* Guzzi */
#ifdef RPC_SIDE_GUZZI
static rpc_skel_functions_table_entry_t rpc_functions_table_guzzi[] = {
    { RPC_GUZZI_CAMERA3_GET_NUMBER_OF_CAMERAS   , guzzi_camera3_skel_get_number_of_cameras   },
    { RPC_GUZZI_CAMERA3_GET_CAMERA_INFO         , guzzi_camera3_skel_get_info                },
    { RPC_GUZZI_CAMERA3_REQUEST_DEFAULTS        , guzzi_camera3_skel_request_defaults        },
    { RPC_GUZZI_CAMERA3_STREAM_CONFIGURATION    , guzzi_camera3_skel_config_streams          },
    { RPC_GUZZI_CAMERA3_CAPTURE_REQUEST         , guzzi_camera3_skel_capture_request         },
    { RPC_GUZZI_CAMERA3_FLUSH                   , guzzi_camera3_skel_flush                   },
    { RPC_GUZZI_CAMERA3_DESTROY                 , guzzi_camera3_skel_destroy                 },
    { RPC_GUZZI_CAMERA3_CREATE                  , guzzi_camera3_skel_create                  },

    { RPC_GUZZI_NVM_READER_GET_NUMBER_OF_CAMERAS, guzzi_nvm_reader_skel_get_number_of_cameras},
    { RPC_GUZZI_NVM_READER_READ                 , guzzi_nvm_reader_skel_read                 },
    { RPC_GUZZI_NVM_READER_DESTROY              , guzzi_nvm_reader_skel_destroy              },
    { RPC_GUZZI_NVM_READER_CREATE               , guzzi_nvm_reader_skel_create               },

    RPC_SKEL_FUNCTIONS_TABLE_ENTRY_NIL
};
#endif

#undef rpc_guzzi_camera3_function_id_base
#undef rpc_guzzi_camera3_capture_result_function_id_base
#undef rpc_guzzi_nvm_reader_function_id_base

const rpc_uint32_t rpc_guzzi_camera3_function_id_base = RPC_GUZZI_CAMERA3_FUNCTION_ID_BASE;
const rpc_uint32_t rpc_guzzi_camera3_capture_result_function_id_base = RPC_GUZZI_CAMERA3_CAPTURE_RESULT_FUNCTION_ID_BASE;
const rpc_uint32_t rpc_guzzi_nvm_reader_function_id_base = RPC_GUZZI_NVM_READER_FUNCTION_ID_BASE;

/*
 * *****************************************************************************
 * ** RPC Functions
 * *****************************************************************************
 */

/* Common */
static rpc_packet_t *service_default_packet_alloc(
        rpc_packet_header_t *packet_header,
        void *prv
    )
{
    rpc_packet_t *packet;

    if (RPC_MODULE_SERVICE_DEFAULT_PACKET_SIZE_MAX < packet_header->data_size) {
        return NULL;
    } 

    packet = pool_alloc_timeout(service_default_pool, RPC_MODULE_TIMEOUT);
    if (!packet) {
        mmsdbg(DL_ERROR, "Failed to allocate new packet!");
        return NULL;
    }

    packet->payload[0].p = packet + 1;
    packet->payload[0].size = packet_header->data_size;
    packet->payload[1].p = NULL;
    packet->payload[1].size = 0;

    return packet;
}

static void service_default_packet_free(
        rpc_packet_t *packet,
        void *prv
    )
{
    osal_free(packet);
}

/* Host */
#ifdef RPC_SIDE_HOST
static rpc_packet_t *service_alloc_packet_alloc(
        rpc_packet_header_t *packet_header,
        void *prv
    )
{
#define MAX_SIZE 32*1024*1024

    rpc_packet_t *packet;

    if (MAX_SIZE < packet_header->data_size) {
        mmsdbg(
                DL_ERROR,
                "Data size too big: data_size=%d; max=%d!",
                packet_header->data_size,
                MAX_SIZE
            );
        return NULL;
    }

    packet = osal_malloc(sizeof (*packet) + packet_header->data_size);
    if (!packet) {
        mmsdbg(DL_ERROR, "Failed to allocate new packet!");
        return NULL;
    }

    packet->payload[0].p = packet + 1;
    packet->payload[0].size = packet_header->data_size;
    packet->payload[1].p = NULL;
    packet->payload[1].size = 0;

    return packet;
}

static void service_alloc_packet_free(
        rpc_packet_t *packet,
        void *prv
    )
{
    osal_free(packet);
}

int rpc_module_init(void)
{
    int err;

    err = osal_init();
    if (err) {
        mmsdbg(DL_ERROR, "OSAL init failed!");
        goto exit1;
    }

    rpc = rpc_create(
            RPC_MODULE_TRANSPORT_NS_SIZE,
            RPC_MODULE_SERVICE_NS_SIZE,
            RPC_MODULE_FUNCTIONS_TO_TRACK,
            rpc_functions_table_host
        );
    if (!rpc) {
        mmsdbg(DL_ERROR, "Failed to create RPC!");
        goto exit2;
    }

    /* Transport */
    transport_default = transport_socket_create();
    if (!transport_default) {
        mmsdbg(DL_ERROR, "Failed to create default RPC transport!");
        goto exit3;
    }

    err = transport_socket_start_server_wait_for_client(
            transport_default,
            RPC_MODULE_TCP_IP,
            RPC_MODULE_TCP_PORT
        );
    if (err) {
        mmsdbg(
                DL_ERROR,
                "Failed to start Socket Transport server on port %d!",
                RPC_MODULE_TCP_PORT
            );
        goto exit4;
    }

    err = rpc_transport_map(
            rpc,
            RPC_TRANSPORT_DEFAULT,
            transport_default
        );
    if (NS_NAME_NIL == err) {
        mmsdbg(DL_ERROR, "Failed to map default RPC transport!");
        goto exit4;
    }

    trasport_default_rx_thread = rpc_rx_thread_create(
            rpc,
            RPC_TRANSPORT_DEFAULT,
            64*1024
        );
    if (!trasport_default_rx_thread) {
        mmsdbg(
                DL_ERROR,
                "Failed to create Rx Thread for default RPC transport!"
            );
        goto exit5;
    }

    /* Service */
    service_default_pool = pool_create(
            "RPC Default Service",
            RPC_MODULE_SERVICE_DEFAULT_PACKET_SIZE_MAX + sizeof (rpc_packet_t),
            RPC_MODULE_SERVICE_DEFAULT_PACKETS_COUNT
        );
    if (!service_default_pool) {
        mmsdbg(DL_ERROR, "Failed to create default service pool!");
        goto exit6;
    }

    err = rpc_service_create(
            rpc,
            RPC_SERVICE_DEFAULT,
            NULL,
            service_default_packet_alloc,
            service_default_packet_free,
            1,
            64*1024
        );
    if (err) {
        mmsdbg(DL_ERROR, "Failed to create Default RPC service!");
        goto exit7;
    }

    err = rpc_service_create(
            rpc,
            RPC_MODULE_SERVICE_ALLOC,
            NULL,
            service_alloc_packet_alloc,
            service_alloc_packet_free,
            1,
            64*1024
        );
    if (err) {
        mmsdbg(DL_ERROR, "Failed to create Alloc RPC service!");
        goto exit8;
    }

    /* Stub/Skel */
    guzzi_camera3_stub_init(
            rpc,
            RPC_MODULE_GC3_BASE_SERVICE
        );
    if (err) {
        mmsdbg(DL_ERROR, "Failed to create Guzzi Camera3 Stub!");
        goto exit9;
    }

    guzzi_nvm_reader_stub_init(rpc);
    if (err) {
        mmsdbg(DL_ERROR, "Failed to create Guzzi NVM Reader Stub!");
        goto exit9;
    }

    guzzi_camera3_capture_result_skel_init(rpc);
    if (err) {
        mmsdbg(DL_ERROR, "Failed to create Capture Result Skel!");
        goto exit9;
    }

    return 0;
exit9:
    rpc_service_destroy(rpc, RPC_MODULE_SERVICE_ALLOC);
exit8:
    rpc_service_destroy(rpc, RPC_SERVICE_DEFAULT);
exit7:
    pool_destroy(service_default_pool);
exit6:
    rpc_rx_thread_destroy(trasport_default_rx_thread);
exit5:
    rpc_transport_unmap(rpc, RPC_TRANSPORT_DEFAULT);
exit4:
    transport_socket_destroy(transport_default);
exit3:
    rpc_destroy(rpc);
exit2:
    osal_exit();
exit1:
    return -1;
}

void rpc_module_deinit(void)
{
    rpc_service_destroy(rpc, RPC_MODULE_SERVICE_ALLOC);
    rpc_service_destroy(rpc, RPC_SERVICE_DEFAULT);
    pool_destroy(service_default_pool);
    rpc_rx_thread_destroy(trasport_default_rx_thread);
    rpc_transport_unmap(rpc, RPC_TRANSPORT_DEFAULT);
    transport_socket_destroy(transport_default);
    rpc_destroy(rpc);
    osal_exit();
}
#endif /* RPC_SIDE_HOST */

/* Guzzi */
#ifdef RPC_SIDE_GUZZI
int rpc_module_init(void)
{
    int err;

    rpc = rpc_create(
            RPC_MODULE_TRANSPORT_NS_SIZE,
            RPC_MODULE_SERVICE_NS_SIZE,
            RPC_MODULE_FUNCTIONS_TO_TRACK,
            rpc_functions_table_guzzi
        );
    if (!rpc) {
        mmsdbg(DL_ERROR, "Failed to create RPC!");
        goto exit1;
    }

    /* Transport */
    transport_default = transport_socket_create();
    if (!transport_default) {
        mmsdbg(DL_ERROR, "Failed to create default RPC transport!");
        goto exit2;
    }

    err = transport_socket_client_connect(
            transport_default,
            RPC_MODULE_TCP_IP,
            RPC_MODULE_TCP_PORT
        );
    if (err) {
        mmsdbg(
                DL_ERROR,
                "Failed to connect Socket Transport to server %s:%d!",
                RPC_MODULE_TCP_IP,
                RPC_MODULE_TCP_PORT
            );
        goto exit3;
    }

    err = rpc_transport_map(
            rpc,
            RPC_TRANSPORT_DEFAULT,
            transport_default
        );
    if (NS_NAME_NIL == err) {
        mmsdbg(DL_ERROR, "Failed to map default RPC transport!");
        goto exit3;
    }

    trasport_default_rx_thread = rpc_rx_thread_create(
            rpc,
            RPC_TRANSPORT_DEFAULT,
            8*1024
        );
    if (!trasport_default_rx_thread) {
        mmsdbg(
                DL_ERROR,
                "Failed to create Rx Thread for default RPC transport!"
            );
        goto exit4;
    }

    /* Service */
    service_default_pool = pool_create(
            "RPC Default Service",
            RPC_MODULE_SERVICE_DEFAULT_PACKET_SIZE_MAX + sizeof (rpc_packet_t),
            RPC_MODULE_SERVICE_DEFAULT_PACKETS_COUNT
        );
    if (!service_default_pool) {
        mmsdbg(DL_ERROR, "Failed to create default service pool!");
        goto exit5;
    }

    err = rpc_service_create(
            rpc,
            RPC_SERVICE_DEFAULT,
            NULL,
            service_default_packet_alloc,
            service_default_packet_free,
            0,
            0
        );
    if (err) {
        mmsdbg(DL_ERROR, "Failed to create Default RPC service!");
        goto exit6;
    }

    /* Stub/Skel */
    err = guzzi_camera3_skel_init(
            rpc,
            RPC_MODULE_GC3_GET_INFO_SERVICE,
            RPC_MODULE_GC3_BASE_SERVICE
        );
    if (err) {
        mmsdbg(DL_ERROR, "Failed to create Guzzi Camera3 Skel!");
        goto exit7;
    }

    err = guzzi_nvm_reader_skel_init(rpc);
    if (err) {
        mmsdbg(DL_ERROR, "Failed to create Guzzi NVM Reader Skel!");
        goto exit7;
    }

    err = guzzi_camera3_capture_result_stub_init(rpc, RPC_MODULE_CAPTURE_RESULT_SERVICE);
    if (err) {
        mmsdbg(DL_ERROR, "Failed to create Capture Result Stub!");
        goto exit7;
    }

    return 0;
exit7:
    rpc_service_destroy(rpc, RPC_SERVICE_DEFAULT);
exit6:
    pool_destroy(service_default_pool);
exit5:
    rpc_rx_thread_destroy(trasport_default_rx_thread);
exit4:
    rpc_transport_unmap(rpc, RPC_TRANSPORT_DEFAULT);
exit3:
    transport_socket_destroy(transport_default);
exit2:
    rpc_destroy(rpc);
exit1:
    return -1;
}

void rpc_module_deinit(void)
{
    rpc_service_destroy(rpc, RPC_SERVICE_DEFAULT);
    pool_destroy(service_default_pool);
    rpc_rx_thread_destroy(trasport_default_rx_thread);
    rpc_transport_unmap(rpc, RPC_TRANSPORT_DEFAULT);
    transport_socket_destroy(transport_default);
    rpc_destroy(rpc);
}
#endif /* RPC_SIDE_GUZZI */

