/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <osal/osal_stdlib.h>
#include <osal/osal_mutex.h>
#include <utils/mms_debug.h>

#include <OsDrvGpio.h>
#include <brdGpioCfgs/brdMv0182GpioDefaults.h>
#include "brdGpioCfgs/brdMv0191GpioDefaults.h"
#include <OsDrvSpiSlaveCP.h>
#include <OsMessageProtocol.h>
#include <rtems/libio.h>

#include <guzzi/rpc/transport.h>

#define container_of(PTR,TYPE,FIELD) \
    (TYPE *)((char *)(PTR) - (char *)&(((TYPE *)0)->FIELD))

#define transport_to_ts(TRANSPORT) container_of((TRANSPORT), ts_t, transport)

typedef struct {
    transport_t transport;
    rtems_device_major_number comm_protocol_major;
    int comm_protocol_channel_id;
    int exit;
    osal_mutex *write_lock;
} ts_t;

mmsdbg_define_variable(
        vdl_transport_spi_rtems,
        DL_DEFAULT,
        0,
        "vdl_transport_spi_rtems",
        "Transport using RTEMS Communication Protocol."
    );
#define MMSDEBUGLEVEL mmsdbg_use_variable(vdl_transport_spi_rtems)

#define RTEMS_DRIVER_AUTO_MAJOR (0)
#define HOST_IRQ_GPIO 22

#define TRANSPORT_SPI_CHANNEL 1

DRVSPI_CONFIGURATION(
        SPI1,           /* dev */
        1,              /* cpol */
        1,              /* cpha */
        1,              /* bytesPerWord */
        0,              /* dmaEnabled */
        HOST_IRQ_GPIO,  /* hostIrqGpio */
        10              /* interruptLevel */
    );
DECLARE_COMMUNICATION_PROTOCOL_DRIVER_TABLE(driver_table);

DECLARE_OS_MESSAGING_VIRTUAL_CHANNEL(
        transport_spi_vc,       /* vcHandle */
        "rpc_transport",        /* channelName */
        TRANSPORT_SPI_CHANNEL,  /* channelId */
        103,                    /* priority */
        250*1024,               /* rx_fifo_size */
        250*1024,               /* tx_fifo_size */
        ".bss"                  /* fifo_data_section */
    );

static int transport_spi_rtems_read(
        transport_t *transport,
        void *p,
        unsigned int size
    )
{
    ts_t *ts;
	rtems_libio_rw_args_t tr;
    rtems_status_code sc;
    int pos;

    ts = transport_to_ts(transport);

    pos = 0;
    while (pos < size) {
        tr.buffer = (char *)p + pos;
        tr.count = size - pos;
        tr.offset = 0;
        tr.flags = 0;
        tr.bytes_moved = 0;
		sc = rtems_io_read(
                ts->comm_protocol_major,
                ts->comm_protocol_channel_id,
                &tr
            );
        if ((sc != RTEMS_SUCCESSFUL)) {
            mmsdbg(
                    DL_ERROR,
                    "Failed to read from RTEMS communication channel (id=%d)!",
                    ts->comm_protocol_channel_id
                );
            continue;
        }
        if (ts->exit) {
            size = 0;
            break;
        }
        pos += tr.bytes_moved;
    }

    return size;
}

static int transport_spi_rtems_write(
        transport_t *transport,
        void *p,
        unsigned int size
    )
{
    ts_t *ts;
	rtems_libio_rw_args_t tr;
    rtems_status_code sc;
    int pos;

    ts = transport_to_ts(transport);

    pos = 0;
    while (pos < size) {
        tr.buffer = (char *)p + pos;
        tr.count = size - pos;
        tr.offset = 0;
        tr.flags = 0;
        tr.bytes_moved = 0;
        sc = rtems_io_write(
                    ts->comm_protocol_major,
                    ts->comm_protocol_channel_id,
                    &tr
                );
	    if (sc != RTEMS_SUCCESSFUL) {
            mmsdbg(DL_ERROR, "Failed to open RTEMS communication channel!");
            continue;
        }
        if (ts->exit) {
            size = 0;
            break;
        }
        pos += tr.bytes_moved;
    }

    return size;
}

static int transport_spi_rtems_write_hack(
        transport_t *transport,
        void *p,
        unsigned int size
    )
{
    ts_t *ts;
    int pos;
    int bytes;
    int bytes_to_send;

    ts = transport_to_ts(transport);

    #define SPI_HACK_MAX_SIZE (20*1024)
    pos = 0;
    while (pos < size) {
        bytes_to_send = size - pos;
        if (SPI_HACK_MAX_SIZE < bytes_to_send) {
            bytes_to_send = SPI_HACK_MAX_SIZE;
        }
        bytes = transport_spi_rtems_write(transport, (char *)p + pos, bytes_to_send);
        pos += bytes;
    }

    return size;
}

static void transport_spi_rtems_write_lock(transport_t *transport)
{
    ts_t *ts;
    ts = transport_to_ts(transport);
    osal_mutex_lock(ts->write_lock);
}

static void transport_spi_rtems_write_unlock(transport_t *transport)
{
    ts_t *ts;
    ts = transport_to_ts(transport);
    osal_mutex_unlock(ts->write_lock);
}

static void transport_spi_rtems_exit(transport_t *transport)
{
    ts_t *ts;
    ts = transport_to_ts(transport);
    ts->exit = 1;
    /* TODO: Notify communication channel */
}

/*
 * ****************************************************************************
 * ** Create/Destroy **********************************************************
 * ****************************************************************************
 */
void transport_spi_rtems_destroy(transport_t *transport)
{
    ts_t *ts;
    ts = transport_to_ts(transport);
	rtems_io_close(
            ts->comm_protocol_major,
            ts->comm_protocol_channel_id,
            NULL
        );
    /* TODO: unregister driver; GPIOs */
    osal_mutex_destroy(ts->write_lock);
    osal_free(ts);
}

transport_t * transport_spi_rtems_create(void)
{
    ts_t *ts;
    rtems_status_code sc;

    ts = osal_calloc(1, sizeof (*ts));
    if (!ts) {
        mmsdbg(
                DL_ERROR,
                "Failed to allocate memory for new instance: size=%d!",
                sizeof (*ts)
            );
        goto exit1;
    }

    ts->transport.read = transport_spi_rtems_read;
    ts->transport.write = transport_spi_rtems_write_hack;
    ts->transport.write_lock = transport_spi_rtems_write_lock;
    ts->transport.write_unlock = transport_spi_rtems_write_unlock;
    ts->transport.exit = transport_spi_rtems_exit;

    ts->write_lock = osal_mutex_create();
    if (!ts->write_lock) {
        mmsdbg(DL_ERROR, "Failed to create send mutex!");
        goto exit2;
    }

    DrvGpioIrqResetAll();
    DrvGpioInitialiseRange(brdMV0182GpioCfgDefault);
    DrvGpioInitialiseRange(brdMV0191GpioCfgDefault);

    sc = rtems_io_register_driver(
            RTEMS_DRIVER_AUTO_MAJOR,
            &driver_table,
            &ts->comm_protocol_major
        );
	if (sc != RTEMS_SUCCESSFUL) {
        mmsdbg(DL_ERROR, "Failed to register RTEMS communication driver!");
        goto exit3;
    }

    sc = rtems_io_initialize(ts->comm_protocol_major, 0, &transport_spi_vc);
    if (sc != RTEMS_SUCCESSFUL) {
        mmsdbg(
                DL_ERROR,
                "Failed to initialize RTEMS communication driver channel %d!",
                TRANSPORT_SPI_CHANNEL
            );
        goto exit3;
    }
    ts->comm_protocol_channel_id = TRANSPORT_SPI_CHANNEL;

    sc = rtems_io_open(
            ts->comm_protocol_major,
            ts->comm_protocol_channel_id,
            NULL
        );
	if (sc != RTEMS_SUCCESSFUL) {
        mmsdbg(DL_ERROR, "Failed to open RTEMS communication channel!");
        goto exit3;
    }

    return &ts->transport;
exit3:
    osal_mutex_destroy(ts->write_lock);
exit2:
    osal_free(ts);
exit1:
    return NULL;
}

