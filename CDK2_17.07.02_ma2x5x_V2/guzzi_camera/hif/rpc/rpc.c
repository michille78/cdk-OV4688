/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <osal/osal_stdlib.h>
#include <osal/osal_stdio.h>
#include <osal/osal_list.h>
#include <osal/osal_mutex.h>
#include <osal/osal_thread.h>
#include <osal/pool.h>
#include <utils/mms_debug.h>

#include <guzzi/camera3/camera3.h>

#include <guzzi/rpc/namespace.h>
#include <guzzi/rpc/transport.h>
#include <guzzi/rpc/rpc.h>

#define RPC_TIMEOUT DEFAULT_ALLOC_TIMEOUT_MS

#define CALL_ID_INVALID 0x3210

typedef struct {
    struct list_head link;
    rpc_uint32_t id;
    void *prv;
    osal_sem *wait_for_return_sem;
} call_tracking_t;

typedef struct {
    struct list_head used;
    struct list_head available;
    osal_mutex *lock;
    osal_sem *available_count_sem;
    rpc_uint32_t max;
    call_tracking_t *call_tracking_storage;
} call_tracker_t;

#define SERVICES_MAX 8

typedef struct {
    int in_use;
    int serv_n;

    rpc_t *rpc;
    void *prv;

    rpc_service_packet_alloc_t *packet_alloc;
    rpc_service_packet_free_t *packet_free;
    int threads;
    int thread_stack_size;

    struct osal_thread **thread_pool;
    int thread_exit;
    struct list_head rx_fifo;
    osal_mutex *rx_fifo_lock;
    osal_sem *rx_fifo_sem;
} rpc_service_t;

struct rpc {
    rpc_skel_functions_table_entry_t *skel_functions_table;
    ns_t *transport_ns;
    ns_t *service_ns;
    call_tracker_t *ct;
};

struct rpc_rx_thread {
    struct osal_thread *thread;
    rpc_t *rpc;
    transport_t *transport;
};

mmsdbg_define_variable(
        vdl_rpc,
        DL_DEFAULT,
        0,
        "vdl_rpc",
        "Remote Procedure Call."
    );
#define MMSDEBUGLEVEL mmsdbg_use_variable(vdl_rpc)

/*
 * ****************************************************************************
 * ** List FIFO ***************************************************************
 * ****************************************************************************
 */
static void list_fifo_put(struct list_head *list, struct list_head *entry)
{
    list_add_tail(entry, list);
}

static struct list_head * list_fifo_get(struct list_head *list)
{
    struct list_head *entry;
    entry = list->next;
    list_del(entry);
    return entry;
}

/*
 * ****************************************************************************
 * ** Call tracking module ****************************************************
 * ****************************************************************************
 */
static rpc_uint32_t call_tracking_id_new(call_tracker_t *ct)
{
    call_tracking_t *call_tracking;
    rpc_uint32_t id;

    osal_sem_wait(ct->available_count_sem); /* TODO: timeout */

    osal_mutex_lock(ct->lock);
    call_tracking = list_entry(ct->available.next, call_tracking_t, link);
    id = call_tracking->id;
    list_move(&call_tracking->link, &ct->used);
    osal_mutex_unlock(ct->lock);

    return id;
}

static int call_tracking_id_wait(call_tracker_t *ct, rpc_uint32_t id, void **pprv)
{
    call_tracking_t *call_tracking, *n;
    
    osal_mutex_lock(ct->lock);
    list_for_each_entry_safe(call_tracking, n, &ct->used, link) {
        if (id == call_tracking->id) {
            osal_mutex_unlock(ct->lock);

            osal_sem_wait(call_tracking->wait_for_return_sem);

            *pprv = call_tracking->prv;

            osal_mutex_lock(ct->lock);
            list_move(&call_tracking->link, &ct->available);
            osal_mutex_unlock(ct->lock);

            osal_sem_post(ct->available_count_sem);

            return 0;
        }
    }
    osal_mutex_unlock(ct->lock);

    return -1;
}

static int call_tracking_id_done(call_tracker_t *ct, rpc_uint32_t id, void *prv)
{
    call_tracking_t *call_tracking, *n;
    
    osal_mutex_lock(ct->lock);
    list_for_each_entry_safe(call_tracking, n, &ct->used, link) {
        if (id == call_tracking->id) {
            osal_mutex_unlock(ct->lock);
            /* TODO: osal_assert(!osal_sem_value(call_tracking->wait_for_return_sem)); */
            call_tracking->prv = prv;
            osal_sem_post(call_tracking->wait_for_return_sem);
            return 0;
        }
    }
    osal_mutex_unlock(ct->lock);

    return -1;
}

static void call_tracking_id_cancel(call_tracker_t *ct, rpc_uint32_t id)
{
    void *prv;
    call_tracking_id_done(ct, id, NULL);
    call_tracking_id_wait(ct, id, &prv);
}

static void call_tracking_destroy(call_tracker_t *ct)
{
    int i;
    for (i = ct->max - 1; 0 <= i; i--) {
        osal_sem_destroy(ct->call_tracking_storage[i].wait_for_return_sem);
    }
    osal_free(ct->call_tracking_storage);
    osal_sem_destroy(ct->available_count_sem);
    osal_mutex_destroy(ct->lock);
    osal_free(ct);
}

static call_tracker_t *call_tracking_create(rpc_uint32_t max)
{
    call_tracker_t *ct;
    int i;

    ct = osal_calloc(1, sizeof (*ct));
    if (!ct) {
        mmsdbg(
                DL_ERROR,
                "Failed to allocate memory for new instance: size=%d!",
                sizeof (*ct)
            );
        goto exit1;
    }
    ct->max = max;
    INIT_LIST_HEAD(&ct->used);
    INIT_LIST_HEAD(&ct->available);

    ct->lock = osal_mutex_create();
    if (!ct->lock) {
        goto exit2;
    }

    ct->available_count_sem = osal_sem_create(ct->max);
    if (!ct->available_count_sem) {
        mmsdbg(DL_ERROR, "Failed to create call tracking count semaphore!");
        goto exit3;
    } 

    ct->call_tracking_storage = osal_calloc(ct->max, sizeof (call_tracking_t));
    if (!ct->call_tracking_storage) {
        mmsdbg(DL_ERROR, "Failed to allocate call tracking storage!");
        goto exit4;
    }

    for (i = 0; i < ct->max; i++) {
        ct->call_tracking_storage[i].id = i;
        ct->call_tracking_storage[i].wait_for_return_sem = osal_sem_create(0);
        if (!ct->call_tracking_storage[i].wait_for_return_sem) {
            mmsdbg(DL_ERROR, "Failed to create call tracking wait semaphore!");
            goto exit5;
        }
        list_add(&ct->call_tracking_storage[i].link, &ct->available);
    }

    return ct;
exit5:
    for (i--; 0 <= i; i--) {
        osal_sem_destroy(ct->call_tracking_storage[i].wait_for_return_sem);
    }
    osal_free(ct->call_tracking_storage);
exit4:
    osal_sem_destroy(ct->available_count_sem);
exit3:
    osal_mutex_destroy(ct->lock);
exit2:
    osal_free(ct);
exit1:
    return NULL;
}

/*
 * ****************************************************************************
 * ** Message handle thread ***************************************************
 * ****************************************************************************
 */
static rpc_function_t *find_func(rpc_t *rpc, rpc_uint32_t id)
{
    rpc_skel_functions_table_entry_t *entry;
    for (entry = rpc->skel_functions_table; !RPC_SKEL_FUNCTIONS_TABLE_ENTRY_IS_NIL(entry); entry++) {
        if (id == entry->id) {
            return entry->func;
        }
    }
    return NULL;
}

static void call_skel_func(rpc_t *rpc, rpc_packet_t *packet)
{
    rpc_function_t *func;

    func = find_func(rpc, packet->header.function);
    if (!func) {
        mmsdbg(
                DL_ERROR,
                "Function not registered for id=%d!",
                packet->header.function
            );
        rpc_service_free_packet(
                rpc,
                packet->header.service,
                packet
            );
        return;
    }

    func(rpc, packet);
}

static void packet_dispatch(rpc_t *rpc, rpc_packet_t *packet)
{
    if (RPC_FUNCTION_RETURN & packet->header.function) {
        call_tracking_id_done(rpc->ct, packet->header.call_id, packet);
    } else {
        call_skel_func(rpc, packet);
    }
}

static void *rpc_service_thread_loop(void *data)
{
    rpc_service_t *service;
    rpc_packet_t *packet;

    service = data;

    for (;;) {
        osal_sem_wait(service->rx_fifo_sem);
        if (service->thread_exit)
            break;

        osal_mutex_lock(service->rx_fifo_lock);
        packet = list_entry(list_fifo_get(&service->rx_fifo), rpc_packet_t, link);
        osal_mutex_unlock(service->rx_fifo_lock);

        packet_dispatch(service->rpc, packet);
    }

    return NULL;
}

static int rpc_service_thread_done_cb(void *arg)
{
    return 0;
}

/*
 * ****************************************************************************
 * ** RPC Service *************************************************************
 * ****************************************************************************
 */
rpc_packet_t *rpc_service_alloc_packet(
        rpc_t *rpc,
        int serv_n,
        rpc_packet_header_t *packet_header
    )
{
    rpc_service_t *service;

    service = ns_n2a(rpc->service_ns, serv_n);
    if (!service) {
        mmsdbg(DL_ERROR, "Failed to obtain service serv_n=%d!", serv_n);
        return NULL;
    }

    return service->packet_alloc(packet_header, service->prv);
}

void rpc_service_free_packet(
        rpc_t *rpc,
        int serv_n,
        rpc_packet_t *packet
    )
{
    rpc_service_t *service;

    service = ns_n2a(rpc->service_ns, serv_n);
    if (!service) {
        mmsdbg(DL_ERROR, "Failed to obtain service serv_n=%d!", serv_n);
        return;
    }

    service->packet_free(packet, service->prv);
}

int rpc_service_enq_packet(rpc_t *rpc, int serv_n, rpc_packet_t *packet)
{
    rpc_service_t *service;

    service = ns_n2a(rpc->service_ns, serv_n);
    if (!service) {
        mmsdbg(DL_ERROR, "Failed to obtain service serv_n=%d!", serv_n);
        return -1;
    }

    if (service->threads) {
        osal_mutex_lock(service->rx_fifo_lock);
        list_fifo_put(&service->rx_fifo, &packet->link);
        osal_mutex_unlock(service->rx_fifo_lock);

        osal_sem_post(service->rx_fifo_sem);
    } else {
        packet_dispatch(rpc, packet);
    }

    return 0;
}

void rpc_service_destroy(rpc_t *rpc, int serv_n)
{
    rpc_service_t *service;
    int j;

    service = ns_n2a(rpc->service_ns, serv_n);
    if (!service) {
        mmsdbg(DL_ERROR, "Failed to obtain service serv_n=%d!", serv_n);
        return;
    }

    if (service->threads) {
        service->thread_exit = 1;
        for (j = service->threads - 1; 0 <= j; j--) {
            osal_sem_post(service->rx_fifo_sem);
        }
        for (j = service->threads - 1; 0 <= j; j--) {
            osal_thread_destroy(service->thread_pool[j]);
        }

        osal_free(service->thread_pool);
        osal_mutex_destroy(service->rx_fifo_lock);
        osal_sem_destroy(service->rx_fifo_sem);
    }

    ns_unmap(rpc->service_ns, serv_n);
}

int rpc_service_create(
        rpc_t *rpc,
        int serv_n,
        void *prv,
        rpc_service_packet_alloc_t *packet_alloc,
        rpc_service_packet_free_t *packet_free,
        int threads,
        int thread_stack_size
    )
{
    rpc_service_t *service;
    int i, j, n;

    service = osal_calloc(1, sizeof (*service));
    if (!service) {
        mmsdbg(DL_ERROR, "Failed to reserve service serv_n=%d!", serv_n);
        goto exit1;
    }

    n = ns_map(rpc->service_ns, serv_n, service);
    if (NS_NAME_NIL == n) {
        mmsdbg(DL_ERROR, "Failed to map service to serv_n=%d!", serv_n);
        goto exit2;
    }

    service->rpc = rpc;
    service->prv = prv;
    service->packet_alloc = packet_alloc;
    service->packet_free = packet_free;
    service->threads = threads;
    service->thread_stack_size = thread_stack_size;

    INIT_LIST_HEAD(&service->rx_fifo);

    if (service->threads) {
        service->rx_fifo_sem = osal_sem_create(0);
        if (!service->rx_fifo_sem) {
            mmsdbg(DL_ERROR, "Failed to create fifo count semaphore!");
            goto exit3;
        }

        service->rx_fifo_lock = osal_mutex_create();
        if (!service->rx_fifo_lock) {
            mmsdbg(DL_ERROR, "Failed to create fifo lock mutex!");
            goto exit4;
        }

        service->thread_pool = osal_calloc(
                service->threads,
                sizeof (*service->thread_pool)
            );
        if (!service->thread_pool) {
            mmsdbg(DL_ERROR, "Failed to allocate thread pool memory!");
            goto exit5;
        }
        for (i = 0; i < service->threads; i++) {
            service->thread_pool[i] = osal_thread_create(
                    "RPC Service Thread",
                    service,
                    rpc_service_thread_loop,
                    rpc_service_thread_done_cb,
                    0,
                    service->thread_stack_size
                );
            if (!service->thread_pool[i]) {
                mmsdbg(DL_ERROR, "Failed to create thread N=%d!", i);
                goto exit6;
            }
        }
    }

    return 0;
exit6:
    service->thread_exit = 1;
    for (j = i - 1; 0 <= j; j--) {
        osal_sem_post(service->rx_fifo_sem);
    }
    for (j = i - 1; 0 <= j; j--) {
        osal_thread_destroy(service->thread_pool[j]);
    }
    osal_free(service->thread_pool);
exit5:
    osal_mutex_destroy(service->rx_fifo_lock);
exit4:
    osal_sem_destroy(service->rx_fifo_sem);
exit3:
    ns_unmap(rpc->service_ns, serv_n);
exit2:
    osal_free(service);
exit1:
    return -1;
}

/*
 * ****************************************************************************
 * ** RPC transport support ***************************************************
 * ****************************************************************************
 */
static void *rpc_rx_thread_loop(void *data)
{
    rpc_rx_thread_t *rpc_rx_thread;
    rpc_t *rpc;
    transport_t *transport;
    rpc_packet_header_t packet_header;
    rpc_packet_t *packet;
    rpc_packet_payload_t *pld;
    int i;
    int err;

    rpc_rx_thread = data;

    rpc = rpc_rx_thread->rpc;
    transport = rpc_rx_thread->transport;

    for (;;) {
        err = transport_read(
                transport,
                &packet_header,
                sizeof (packet_header)
            );
        if (err < 0) {
            mmsdbg(DL_ERROR, "Transport read failed!");
            /* TODO: */
            return NULL;
        } else if (0 == err) {
            /* Normal exit */
            return NULL;
        }

        /* TODO: Check packet header */
        /* TODO: On error flush Rx buffer */

        packet = rpc_service_alloc_packet(
                rpc,
                packet_header.service,
                &packet_header
            );
        if (!packet) {
            mmsdbg(
                    DL_ERROR,
                    "Failed to allocate packet from service %d!",
                    packet_header.service
                );
            /* TODO: Flush Rx data */
            continue;
        }

        memcpy(&packet->header, &packet_header, sizeof (packet->header));

        pld = packet->payload;
        for (i = 0; i < ARRAY_SIZE(packet->payload) && pld[i].p && pld[i].size; i++) {
            err = transport_read(transport, pld->p, pld->size);
            if (err < 0) {
                mmsdbg(DL_ERROR, "Transport read failed!");
                /* TODO: */
                return NULL;
            }
        }

        err = rpc_service_enq_packet(rpc, packet->header.service, packet);
        if (err) {
            mmsdbg(
                    DL_ERROR,
                    "Failed to enqueue packet to service=%d!",
                    packet->header.service
                );
            /* TODO: */
            return NULL;
        }
    }
    return NULL;
}

static int rpc_rx_thread_done_cb(void *arg)
{
    return 0;
}

rpc_rx_thread_t * rpc_rx_thread_create(
        rpc_t *rpc,
        int transport_n,
        unsigned int stack_size
    )
{
    rpc_rx_thread_t *rpc_rx_thread;

    rpc_rx_thread = osal_calloc(1, sizeof (*rpc_rx_thread));
    if (!rpc_rx_thread) {
        mmsdbg(
                DL_ERROR,
                "Failed to allocate memory for new instance: size=%d!",
                sizeof (*rpc_rx_thread)
            );
        goto exit1;
    }
    rpc_rx_thread->rpc = rpc;
    rpc_rx_thread->transport = ns_n2a(
            rpc->transport_ns,
            transport_n
        );
    if (!rpc_rx_thread->transport) {
        mmsdbg(DL_ERROR, "Failed to obtain transport!");
        goto exit2;
    }

    rpc_rx_thread->thread = osal_thread_create(
            "RPC Rx Thread",
            rpc_rx_thread,
            rpc_rx_thread_loop,
            rpc_rx_thread_done_cb,
            0,
            stack_size
        );
    if (!rpc_rx_thread->thread) {
        mmsdbg(DL_ERROR, "Failed to create thread!");
        goto exit2;
    }

    return rpc_rx_thread;
exit2:
    osal_free(rpc_rx_thread);
exit1:
    return NULL;
}

void rpc_rx_thread_destroy(rpc_rx_thread_t *rpc_rx_thread)
{
    transport_exit(rpc_rx_thread->transport); /* TODO: remove this */
    osal_thread_destroy(rpc_rx_thread->thread);
    osal_free(rpc_rx_thread);
}

int rpc_transport_map(rpc_t *rpc, int name, transport_t *transport)
{
    return ns_map(rpc->transport_ns, name, transport);
}

int rpc_transport_unmap(rpc_t *rpc, int name)
{
    return ns_unmap(rpc->transport_ns, name);
}

/*
 * ****************************************************************************
 * ** RPC functions ***********************************************************
 * ****************************************************************************
 */
rpc_packet_t * rpc_call(
        rpc_t *rpc,
        rpc_int32_t transport_n,
        rpc_int32_t serv_n,
        rpc_uint32_t function,
        rpc_packet_t *packet
    )
{
    rpc_packet_t *result;
    rpc_packet_payload_t *pld;
    transport_t *transport;
    int i;
    int err;

    transport = ns_n2a(rpc->transport_ns, transport_n);
    if (!transport) {
        mmsdbg(DL_ERROR, "Failed to obtain transport!");
        goto exit1;
    }

    packet->header.service = serv_n;
    packet->header.function = function;

    packet->header.call_id = call_tracking_id_new(rpc->ct);
    if (CALL_ID_INVALID == packet->header.call_id) {
        mmsdbg(DL_ERROR, "Failed to obtain call ID!");
        goto exit1;
    }

    transport_write_lock(transport);

    err = transport_write(transport, &packet->header, sizeof (packet->header));
    if (err < 0) {
        mmsdbg(DL_ERROR, "Failed to send packet header!"); 
        transport_write_unlock(transport);
        goto exit2;
    }
    pld = packet->payload;
    for (i = 0; i < ARRAY_SIZE(packet->payload) && pld[i].p && pld[i].size; i++) {
        err = transport_write(transport, pld[i].p, pld[i].size);
        if (err < 0) {
            mmsdbg(
                    DL_ERROR,
                    "Failed to send packet payload: p=%p, size=%d!",
                    pld->p,
                    pld->size
                );
            transport_write_unlock(transport);
            goto exit2;
        }
    }

    transport_write_unlock(transport);

    call_tracking_id_wait(rpc->ct, packet->header.call_id, (void **)&result);

    return result;
exit2:
    call_tracking_id_cancel(rpc->ct, packet->header.call_id);
exit1:
    return NULL;
}

int rpc_return(
        rpc_t *rpc,
        rpc_int32_t transport_n,
        rpc_int32_t serv_n,
        rpc_packet_t *call,
        rpc_packet_t *result
    )
{
    rpc_packet_payload_t *pld;
    transport_t *transport;
    int i;
    int err;

    transport = ns_n2a(rpc->transport_ns, transport_n);
    if (!transport) {
        mmsdbg(DL_ERROR, "Failed to obtain transport!");
        goto exit1;
    }

    result->header.service = serv_n;
    result->header.function = RPC_FUNCTION_RETURN | call->header.function;
    result->header.call_id = call->header.call_id;

    transport_write_lock(transport);

    err = transport_write(transport, &result->header, sizeof (result->header));
    if (err < 0) {
        mmsdbg(DL_ERROR, "Failed to send result packet header!"); 
        transport_write_unlock(transport);
        goto exit1;
    }
    pld = result->payload;
    for (i = 0; i < ARRAY_SIZE(result->payload) && pld[i].p && pld[i].size; i++) {
        err = transport_write(transport, pld[i].p, pld[i].size);
        if (err < 0) {
            mmsdbg(
                    DL_ERROR,
                    "Failed to send result packet payload: p=%p, size=%d!",
                    pld->p,
                    pld->size
                );
            transport_write_unlock(transport);
            goto exit1;
        }
    }

    transport_write_unlock(transport);

    return 0;
exit1:
    return -1;
}

/*
 * ****************************************************************************
 * ** RPC create/destroy ******************************************************
 * ****************************************************************************
 */
void rpc_destroy(rpc_t *rpc)
{
    call_tracking_destroy(rpc->ct);
    ns_destroy(rpc->service_ns);
    ns_destroy(rpc->transport_ns);
    osal_free(rpc);
}

rpc_t * rpc_create(
        unsigned int transport_ns_size,
        unsigned int service_ns_size,
        int functions_to_track,
        rpc_skel_functions_table_entry_t *skel_functions_table
    )
{
    rpc_t *rpc;

    rpc = osal_calloc(1, sizeof (*rpc));
    if (!rpc) {
        mmsdbg(
                DL_ERROR,
                "Failed to allocate memory for new instance: size=%d!",
                sizeof (*rpc)
            );
        goto exit1;
    }
    rpc->skel_functions_table = skel_functions_table;

    rpc->transport_ns = ns_create(transport_ns_size);
    if (!rpc->transport_ns) {
        mmsdbg(DL_ERROR, "Failed to create transport namespace!");
        goto exit2;
    }

    rpc->service_ns = ns_create(service_ns_size);
    if (!rpc->service_ns) {
        mmsdbg(DL_ERROR, "Failed to create service namespace!");
        goto exit3;
    }

    rpc->ct = call_tracking_create(functions_to_track);
    if (!rpc->ct) {
        mmsdbg(DL_ERROR, "Failed to create call tracking module!");
        goto exit4;
    }

    return rpc;
exit4:
    ns_destroy(rpc->service_ns);
exit3:
    ns_destroy(rpc->transport_ns);
exit2:
    osal_free(rpc);
exit1:
    return NULL;
}

