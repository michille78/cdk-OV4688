/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <utils/mms_debug.h>
#include <guzzi/camera3/camera3.h>
#include <guzzi/rpc/camera3_capture_result_callback_wrapper.h>

mmsdbg_define_variable(
        vdl_camera3_capture_result_callback_wrapper,
        DL_DEFAULT,
        0,
        "vdl_camera3_capture_result_callback_wrapper",
        "Guzzi Camera3 capture result callback wrapper."
    );
#define MMSDEBUGLEVEL mmsdbg_use_variable(vdl_camera3_capture_result_callback_wrapper)

#define CAMERA_ID_BACK 0
#define CAMERA_ID_FRONT 1

#define MAX_CAMERAS 2

typedef struct {
    void *prv;
    guzzi_camera3_capture_result_callback_t *func;
} callback_storage_t;

static callback_storage_t callback_storage[MAX_CAMERAS] = {{NULL,}};

static callback_storage_t *callback_get(int camera_id)
{
    int idx;

    switch (camera_id) {
        case GUZZI_CAMERA3_FACING_BACK : idx = CAMERA_ID_BACK ; break;
        case GUZZI_CAMERA3_FACING_FRONT: idx = CAMERA_ID_FRONT; break;
        default:
            mmsdbg(DL_ERROR, "Cannot get camera with id=%d!", camera_id);
            return NULL;
    }

    return callback_storage + idx;
}

static int callback_set(
        int camera_id,
        void *prv,
        guzzi_camera3_capture_result_callback_t *func
    )
{
    int idx;

    switch (camera_id) {
        case GUZZI_CAMERA3_FACING_BACK : idx = CAMERA_ID_BACK ; break;
        case GUZZI_CAMERA3_FACING_FRONT: idx = CAMERA_ID_FRONT; break;
        default:
            mmsdbg(DL_ERROR, "Cannot set camera with id=%d!", camera_id);
            return -1;
    }

    /* TODO: Lock */
    callback_storage[idx].prv = prv;
    callback_storage[idx].func = func;
    /* TODO: Unlock */

    return 0;
}

static int callback_clear(
        int camera_id
    )
{
    return callback_set(camera_id, NULL, NULL);
}

void guzzi_camera3_capture_result(
        int camera_id,
        unsigned int stream_id,
        unsigned int frame_number,
        void *data,
        unsigned int data_size
    )
{
    callback_storage_t *callback;

    callback = callback_get(camera_id);
    if (!callback) {
        return;
    }

    /* TODO: Lock */
    if (!callback->func) {
        /* TODO: Unlock */
        mmsdbg(
                DL_ERROR,
                "Callback called but there is "
                "no registered function for camera_id=%d!",
                camera_id
            );
        return;
    }

    callback->func(
            callback->prv,
            camera_id,
            stream_id,
            frame_number,
            data,
            data_size
        );
    /* TODO: Unlock */
}

int guzzi_camera3_capture_result_callback_set(
        int camera_id,
        void *prv,
        guzzi_camera3_capture_result_callback_t *func
    )
{
    return callback_set(camera_id, prv, func);
}

int guzzi_camera3_capture_result_callback_clear(int camera_id)
{
    return callback_clear(camera_id);
}

