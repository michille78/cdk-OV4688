/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <sys/time.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <assert.h>
#include <linux/tcp.h>

#include <osal/osal_stdlib.h>
#include <osal/osal_mutex.h>
#include <osal/osal_thread.h>
#include <utils/mms_debug.h>

#include <guzzi/rpc/transport.h>

#define container_of(PTR,TYPE,FIELD) \
    (TYPE *)((char *)(PTR) - (char *)&(((TYPE *)0)->FIELD))

#define transport_to_ts(TRANSPORT) container_of((TRANSPORT), ts_t, transport)

typedef struct {
    transport_t transport;
    int exit;
    int socket;
    int server_socket;
    osal_mutex *write_lock;
} ts_t;

mmsdbg_define_variable(
        vdl_transport_socket,
        DL_DEFAULT,
        0,
        "vdl_transport_socket",
        "Transport using TCP sockets."
    );
#define MMSDEBUGLEVEL mmsdbg_use_variable(vdl_transport_socket)

static int transport_socket_read(
        transport_t *transport,
        void *p,
        unsigned int size
    )
{
    ts_t *ts;
    fd_set read_fd_set;
    struct timeval timeout;
    int bytes, pos;
    int err;

    ts = transport_to_ts(transport);

    pos = 0;

    for (;;) {
        FD_ZERO(&read_fd_set);
        FD_SET(ts->socket, &read_fd_set);

        timeout.tv_sec = 0;
        timeout.tv_usec = 1000*1000;

        err = select(ts->socket+1, &read_fd_set, NULL, NULL, &timeout);
        if (err < 0) {
            if (EINTR == errno) {
                continue;
            }
            mmsdbg(DL_ERROR, "Select on socket failed!");
            return -1;
        }
        if (ts->exit) {
            return 0;
        }

        if (!FD_ISSET(ts->socket, &read_fd_set)) {
            continue;
        }

        bytes = read(ts->socket, (char *)p + pos, size - pos);
        if (bytes < 0) {
            if (EINTR == errno) {
                continue;
            }
            if (ECONNRESET == errno) {
                return -1;
            }
            mmsdbg(DL_ERROR, "Socket read error!");
            return -1;
        } else if (bytes == 0) {
            return -1;
        } else if ((pos + bytes) < size) {
            pos += bytes;
            continue;
        } else {
            break;
        }
    }

    return size;
}

static int transport_socket_write(
        transport_t *transport,
        void *p,
        unsigned int size
    )
{
    ts_t *ts;
    int bytes;
    int pos;

    ts = transport_to_ts(transport);

    pos = 0;
    while (pos < size) {
        bytes = write(ts->socket, (char *)p + pos, size - pos);
        if (bytes < 0) {
            if (EINTR == errno) {
                continue;
            }
            mmsdbg(DL_ERROR, "Socket write error!");
            return -1;
        } else if (bytes == 0) {
            mmsdbg(DL_ERROR, "Socket close during write!");
            return -1;
        }
        pos += bytes;
    }

    return size;
}

static void transport_socket_write_lock(transport_t *transport)
{
    ts_t *ts;
    ts = transport_to_ts(transport);
    osal_mutex_lock(ts->write_lock);
}

static void transport_socket_write_unlock(transport_t *transport)
{
    ts_t *ts;
    ts = transport_to_ts(transport);
    osal_mutex_unlock(ts->write_lock);
}

static void transport_socket_exit(transport_t *transport)
{
    ts_t *ts;
    ts = transport_to_ts(transport);
    ts->exit = 1;
}

/*
 * ****************************************************************************
 * ** Client/Server ***********************************************************
 * ****************************************************************************
 */
int transport_socket_client_connect(
        transport_t *transport,
        char *addr_str,
        int portno
    )
{
    ts_t *ts;
    struct sockaddr_in addr;
    int err;

    ts = transport_to_ts(transport);

    bzero((void *)&addr, sizeof(addr));

    addr.sin_family = AF_INET;
    addr.sin_port = htons(portno);

    err = inet_pton(AF_INET, addr_str, &addr.sin_addr);
    if (err <= 0) {
        mmsdbg(DL_ERROR, "Invalid server address: %s", addr_str);
        goto exit1;
    }

    ts->socket = socket(AF_INET, SOCK_STREAM, 0);
    if (ts->socket < 0) {
        mmsdbg(DL_ERROR, "Failed to open client socket!");
        goto exit1;
    }

    err = connect(ts->socket, (struct sockaddr *)&addr, sizeof (addr));
    if (err < 0) {
        mmsdbg(
                DL_ERROR,
                "Failed to connect to server: %s:%d!",
                addr_str,
                portno
            );
        goto exit2;
    }

    return 0;
exit2:
    close(ts->socket);
exit1:
    return -1;
}

int transport_socket_start_server_wait_for_client(
        transport_t *transport,
        char *addr_str,
        int portno
    )
{
    ts_t *ts;
    struct sockaddr_in addr;
    socklen_t addrlen;
    fd_set read_fd_set;
    struct timeval timeout;
    int flag;
    int err;

    ts = transport_to_ts(transport);

    ts->server_socket = socket(AF_INET, SOCK_STREAM, 0);
    if (ts->server_socket < 0) {
        mmsdbg(DL_ERROR, "Failed to open server socket!");
        goto exit1;
    }

    flag = 1;
    err = setsockopt(
            ts->server_socket,
            IPPROTO_TCP,
            TCP_NODELAY,
            &flag,
            sizeof (int)
        );
    if (err < 0) {
        mmsdbg(DL_WARNING, "Failed to set set TCP_NODELAY on server socket!");
    }
    flag = 1;
    setsockopt(
            ts->server_socket,
            SOL_SOCKET,
            SO_REUSEADDR,
            &flag,
            sizeof (int)
        );
    if (err < 0) {
        mmsdbg(DL_WARNING, "Failed to set set SO_REUSEADDR on server socket!");
    }

    bzero((void *)&addr, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = htonl(INADDR_ANY);
    addr.sin_port = htons(portno);
    err = bind(
            ts->server_socket,
            (struct sockaddr *)&addr,
            sizeof (addr)
        );
    if (err < 0) {
        mmsdbg(DL_ERROR, "Failed to bind server socket!");
        goto exit2;
    }

    err = listen(ts->server_socket, 1);
    if (err) {
        mmsdbg(
                DL_ERROR,
                "Failed to listen on server socket (port=%d)!",
                portno
            );
        goto exit2;
    }

    for (;;) {
        FD_ZERO(&read_fd_set);
        FD_SET(ts->server_socket, &read_fd_set);

        timeout.tv_sec = 0;
        timeout.tv_usec = 1000*1000;

        err = select(ts->server_socket+1, &read_fd_set, NULL, NULL, &timeout);
        if (err < 0) {
            if (EINTR == errno) {
                continue;
            }
            mmsdbg(DL_ERROR, "Select on server socket failed!");
            goto exit2;
        }
        if (ts->exit) {
            goto exit2;
        }
        if (!FD_ISSET(ts->server_socket, &read_fd_set)) {
            continue;
        }
        break;
    }

    addrlen = sizeof (addr);
    ts->socket = accept(
            ts->server_socket,
            (struct sockaddr *)&addr,
            &addrlen
        );

    return 0;
exit2:
    close(ts->server_socket);
exit1:
    return -1;
}

/*
 * ****************************************************************************
 * ** Create/Destroy **********************************************************
 * ****************************************************************************
 */
void transport_socket_destroy(transport_t *transport)
{
    ts_t *ts;
    ts = transport_to_ts(transport);
    ts->exit = 1;
    if (0 <= ts->server_socket) {
        close(ts->server_socket);
    }
    if (0 <= ts->socket) {
        close(ts->socket);
    }
    osal_mutex_destroy(ts->write_lock);
    osal_free(ts);
}

transport_t * transport_socket_create(void)
{
    ts_t *ts;

    ts = osal_calloc(1, sizeof (*ts));
    if (!ts) {
        mmsdbg(
                DL_ERROR,
                "Failed to allocate memory for new instance: size=%d!",
                sizeof (*ts)
            );
        goto exit1;
    }
    ts->socket = -1;
    ts->server_socket = -1;

    ts->transport.read = transport_socket_read;
    ts->transport.write = transport_socket_write;
    ts->transport.write_lock = transport_socket_write_lock;
    ts->transport.write_unlock = transport_socket_write_unlock;
    ts->transport.exit = transport_socket_exit;

    ts->write_lock = osal_mutex_create();
    if (!ts->write_lock) {
        mmsdbg(DL_ERROR, "Failed to create send mutex!");
        goto exit2;
    }

    return &ts->transport;
exit2:
    osal_free(ts);
exit1:
    return NULL;
}

