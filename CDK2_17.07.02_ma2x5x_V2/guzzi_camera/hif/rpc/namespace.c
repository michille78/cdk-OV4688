/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <osal/osal_stdlib.h>
#include <utils/mms_debug.h>

#include <guzzi/rpc/namespace.h>

mmsdbg_define_variable(
        vdl_namespace,
        DL_DEFAULT,
        0,
        "vdl_namespace",
        "Namespace."
    );
#define MMSDEBUGLEVEL mmsdbg_use_variable(vdl_namespace)

#ifndef NS_SIZE_MAX
#define NS_SIZE_MAX 256
#endif

struct ns {
    unsigned int size;
    void **storage;
};

int ns_a2n(ns_t *ns, void *addr)
{
    unsigned int i;
    if (addr) {
        for (i = 0; i < ns->size; i++) {
            if (ns->storage[i] == addr) {
                return i;
            }
        }
    }
    return NS_NAME_NIL;
}

void * ns_n2a(ns_t *ns, int name)
{
    if ((name < 0) && (ns->size <= name)) {
        mmsdbg(DL_ERROR, "Invalid name=%d!", name);
        return NULL;
    }
    if (!ns->storage[name]) {
        mmsdbg(DL_ERROR, "Name=%d not mapped!", name);
        return NULL;
    }

    return ns->storage[name];
}

int ns_map(ns_t *ns, int name, void *addr)
{
    int mapped_name;
    unsigned int i;

    mapped_name = NS_NAME_NIL;

    if (NS_NAME_NIL == name) {
        for (i = 0; i < ns->size; i++) {
            if (!ns->storage[i]) {
                mapped_name = i;
                ns->storage[mapped_name] = addr;
            }
        }
    } else if ((0 <= name) && (name < ns->size)) {
        if (!ns->storage[name]) {
            mapped_name = name;
            ns->storage[mapped_name] = addr;
        }
    } else {
        mmsdbg(DL_ERROR, "Invalid name=%d!", name);
    }

    return mapped_name; 
}

int ns_unmap(ns_t *ns, int name)
{
    if ((name < 0) && (ns->size <= name)) {
        mmsdbg(DL_ERROR, "Invalid name=%d!", name);
        return -1;
    }
    if (!ns->storage[name]) {
        mmsdbg(DL_ERROR, "Name=%d not mapped!", name);
        return -1;
    }

    ns->storage[name] = NULL;

    return 0;
}

void ns_destroy(ns_t *ns)
{
    osal_free(ns->storage);
    osal_free(ns);
}

ns_t * ns_create(unsigned int size)
{
    ns_t *ns;

    if (NS_SIZE_MAX < size) {
        mmsdbg(DL_ERROR, "Namespace size too big (size=%d)!", size);
        goto exit1;
    }

    ns = osal_calloc(1, sizeof (*ns));
    if (!ns) {
        mmsdbg(
                DL_ERROR,
                "Failed to allocate new namespace instance (size=%d)",
                sizeof (*ns)
            );
        goto exit1;
    }

    ns->size = size;

    ns->storage = osal_calloc(1, ns->size * sizeof (*ns->storage));
    if (!ns->storage) {
        mmsdbg(
                DL_ERROR,
                "Failed to allocate new namespace sotrage (size=%d)",
                ns->size
            );
        goto exit2;
    }

    return ns;
exit2:
    osal_free(ns);
exit1:
    return NULL;
}

