/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <osal/osal_stdlib.h>
#include <osal/pool.h>
#include <utils/mms_debug.h>
#include <guzzi/camera3/camera3.h>
#include <guzzi/rpc/rpc.h>
#include <guzzi/rpc/camera3_rpc.h>

typedef struct {
    rpc_t *rpc;
    int serv_n;
    pool_t *pool_big;
    pool_t *pool_small;
} gc3_rpc_service_t;

typedef struct {
    rpc_t *rpc;
    rpc_int32_t instance;
    guzzi_camera3_t *gc3;
    int camera_id;
    gc3_rpc_service_t *serv;
} guzzi_camera3_skel_t;

static rpc_t *guzzi_camera3_skel_rpc;
static int guzzi_camera3_skel_get_info_serv_n;
static int guzzi_camera3_skel_base_serv_n;

mmsdbg_define_variable(
        vdl_camera3_skel,
        DL_DEFAULT,
        0,
        "vdl_camera3_skel",
        "Camera3 skel."
    );
#define MMSDEBUGLEVEL mmsdbg_use_variable(vdl_camera3_skel)

#define GC3_STUB_TIMEOUT DEFAULT_ALLOC_TIMEOUT_MS

#define INSTANCES_MAX RPC_GUZZI_CAMERA3_INSTANCES_MAX

#define GC3_RPC_SERVICE_THREADS 1
#define GC3_RPC_SERVICE_THREAD_STACK_SIZE 8196

static guzzi_camera3_skel_t *guzzi_camera3_skel_instances[INSTANCES_MAX] = {NULL};

/*
 * ****************************************************************************
 * ** Packet alloc/free *******************************************************
 * ****************************************************************************
 */
static rpc_packet_t *gc3_rpc_service_packet_alloc(
        rpc_packet_header_t *packet_header,
        void *prv
    )
{
    gc3_rpc_service_t *gc3serv;
    pool_t *pool;
    rpc_packet_t *packet;

    gc3serv = prv;

    if (packet_header->data_size <= 512) {
        pool = gc3serv->pool_small;
    } else if (packet_header->data_size <= 64*1024) {
        pool = gc3serv->pool_big;
    } else {
        mmsdbg(
                DL_ERROR,
                "Unable to find suitable pool for data_size=%d!",
                packet_header->data_size
            );
        goto exit1;
    }

    packet = pool_alloc_timeout(pool, GC3_STUB_TIMEOUT);
    if (!packet) {
        mmsdbg(DL_ERROR, "Failed to allocate new packet!");
        goto exit1;
    }

    packet->payload[0].p = packet + 1;
    packet->payload[0].size = packet_header->data_size;
    packet->payload[1].p = NULL;
    packet->payload[1].size = 0;

    return packet;
exit1:
    return NULL;
}

static void gc3_rpc_service_packet_free(
        rpc_packet_t *packet,
        void *prv
    )
{
    osal_free(packet);
}

static void gc3_rpc_service_destroy(gc3_rpc_service_t *gc3serv)
{
    rpc_service_destroy(gc3serv->rpc, gc3serv->serv_n);
    pool_destroy(gc3serv->pool_small);
    pool_destroy(gc3serv->pool_big);
    osal_free(gc3serv);
}

static gc3_rpc_service_t *gc3_rpc_service_create(
        rpc_t *rpc,
        int serv_n,
        int threads,
        int thread_stack_size
    )
{
    gc3_rpc_service_t *gc3serv;
    int err;

    gc3serv = osal_calloc(1, sizeof (*gc3serv));
    if (!gc3serv) {
        mmsdbg(
                DL_ERROR,
                "Failed to allocate memory for new instance: size=%d!",
                sizeof (*gc3serv)
            );
        goto exit1;
    }
    gc3serv->rpc = rpc;
    gc3serv->serv_n = serv_n;

    gc3serv->pool_big = pool_create(
            "GuzziCamera3 RPC Service Big",
            64*1024 + sizeof (rpc_packet_t),
            2
        );
    if (!gc3serv->pool_big) {
        mmsdbg(
                DL_ERROR,
                "Failed to create GuzziCamera3 RPC Service Big pool!"
            );
        goto exit2;
    }

    gc3serv->pool_small = pool_create(
            "GuzziCamera3 RPC Service Small",
            512 + sizeof (rpc_packet_t),
            4
        );
    if (!gc3serv->pool_small) {
        mmsdbg(
                DL_ERROR,
                "Failed to create GuzziCamera3 RPC Service Small pool!"
            );
        goto exit3;
    }

    err = rpc_service_create(
            gc3serv->rpc,
            gc3serv->serv_n,
            gc3serv,
            gc3_rpc_service_packet_alloc,
            gc3_rpc_service_packet_free,
            threads,
            thread_stack_size
        );
    if (err) {
        mmsdbg(DL_ERROR, "Failed to create GuzziCamera3 RPC service!");
        goto exit4;
    }

    return gc3serv;
exit4:
    pool_destroy(gc3serv->pool_small);
exit3:
    pool_destroy(gc3serv->pool_big);
exit2:
    osal_free(gc3serv);
exit1:
    return NULL;
}

/*
 * ****************************************************************************
 * ** Locals ******************************************************************
 * ****************************************************************************
 */
static int store_instance(guzzi_camera3_skel_t *gc3_skel)
{
    int i;
    for (i = 0; i < INSTANCES_MAX; i++) {
        if (!guzzi_camera3_skel_instances[i]) {
            guzzi_camera3_skel_instances[i] = gc3_skel;
            return i;
        }
    }
    return -1;
}

/*
 * ****************************************************************************
 * ** Get number of cameras ***************************************************
 * ****************************************************************************
 */
int guzzi_camera3_skel_get_number_of_cameras(
        rpc_t *rpc,
        rpc_packet_t *call
    )
{
    rpc_packet_t result;
    rpc_guzzi_camera3_get_number_of_cameras_return_t rpc_get_number_of_cameras_return;
    int number_of_cameras;

    result.payload[0].p = &rpc_get_number_of_cameras_return;
    result.payload[0].size = sizeof (rpc_get_number_of_cameras_return);
    result.payload[1].p = NULL;
    result.payload[1].size = 0;
    result.header.data_size = result.payload[0].size;

    if (call->header.data_size != 0) {
        mmsdbg(DL_ERROR, "Packet size mismatch!");
        goto exit1;
    }

    number_of_cameras = guzzi_camera3_get_number_of_cameras();

    rpc_get_number_of_cameras_return.number_of_cameras =
        (rpc_int32_t)number_of_cameras;
    RPC_RETURN(rpc, call, &result);
    rpc_service_free_packet(rpc, call->header.service, call);

    return 0;
exit1:
    rpc_get_number_of_cameras_return.number_of_cameras = 0;
    RPC_RETURN(rpc, call, &result);
    rpc_service_free_packet(rpc, call->header.service, call);
    return -1;
}

/*
 * ****************************************************************************
 * ** Get info ****************************************************************
 * ****************************************************************************
 */
int guzzi_camera3_skel_get_info(
        rpc_t *rpc,
        rpc_packet_t *call
    )
{
    rpc_packet_t result;
    rpc_guzzi_camera3_get_info_t *rpc_get_info;
    rpc_guzzi_camera3_get_info_return_t rpc_get_info_return;
    int camera_id;
    guzzi_camera3_info_t info;
    int err;

    rpc_get_info = call->payload[0].p;
    camera_id = rpc_get_info->camera_id;

    result.payload[0].p = &rpc_get_info_return;
    result.payload[0].size = sizeof (rpc_get_info_return);
    result.payload[1].p = NULL;
    result.payload[1].size = 0;
    result.payload[2].p = NULL;
    result.payload[2].size = 0;
    result.header.data_size = result.payload[0].size;

    if (call->header.data_size != sizeof (*rpc_get_info)) {
        mmsdbg(DL_ERROR, "Packet size mismatch!");
        goto exit1;
    }

    info.metadata_static = osal_malloc(sizeof (*info.metadata_static));
    if (!info.metadata_static) {
        mmsdbg(
                DL_ERROR,
                "Failed to allocate memory for static metada (size=%d)!",
                sizeof (*info.metadata_static)
            );
        goto exit1;
    }

    err = guzzi_camera3_get_info(camera_id, &info);
    if (err) {
        mmsdbg(DL_ERROR, "Failed to get info!");
    }

    result.payload[1].p = info.metadata_static;
    result.payload[1].size = sizeof (*info.metadata_static);
    result.header.data_size += result.payload[1].size;

    rpc_get_info_return.camera_id = camera_id;
    rpc_get_info_return.result = err;
    rpc_get_info_return.facing = info.facing;
    rpc_get_info_return.orientation = info.orientation;
    rpc_return(
            rpc,
            RPC_TRANSPORT_DEFAULT,
            guzzi_camera3_skel_get_info_serv_n,
            call,
            &result
        );
    rpc_service_free_packet(rpc, call->header.service, call);

    osal_free(info.metadata_static);

    return 0;
exit1:
    rpc_get_info_return.camera_id = camera_id;
    rpc_get_info_return.result = -1;
    RPC_RETURN(rpc, call, &result);
    rpc_service_free_packet(rpc, call->header.service, call);
    return -1;
}

/*
 * ****************************************************************************
 * ** Callback ****************************************************************
 * ****************************************************************************
 */
static void guzzi_camera3_skel_calback_notify(
        guzzi_camera3_t *gc3,
        void *client_prv,
        guzzi_camera3_event_t event,
        int num,
        void *ptr
    )
{
    guzzi_camera3_skel_t *gc3_skel;
    rpc_packet_t packet;
    rpc_packet_t *packet_return;
    rpc_guzzi_camera3_callback_notify_t rpc_callback_notify;
    rpc_guzzi_camera3_callback_notify_return_t *rpc_callback_notify_return;
    guzzi_camera3_event_shutter_t *shutter;

    gc3_skel = client_prv;

    packet.payload[0].p = &rpc_callback_notify;
    packet.payload[0].size = sizeof (rpc_callback_notify);
    packet.payload[1].p = NULL;
    packet.payload[1].size = 0;
    packet.header.data_size = packet.payload[0].size;

    rpc_callback_notify.instance = gc3_skel->instance;
    rpc_callback_notify.event = (rpc_uint32_t)event;
    switch (event) {
        case GUZZI_CAMERA3_EVENT_SHUTTER:
            shutter = ptr;
            rpc_callback_notify.shutter.frame_number =
                (rpc_uint32_t)shutter->frame_number;
            rpc_callback_notify.shutter.timestamp =
                (rpc_uint64_t)shutter->timestamp;
            break;
        case GUZZI_CAMERA3_EVENT_ERROR:
            /* TODO: */
            break;
        default:
            mmsdbg(DL_ERROR, "Unknown Guzzi Camera3 Event!");
    }

    packet_return = rpc_call(
            gc3_skel->rpc,
            RPC_TRANSPORT_DEFAULT,
            guzzi_camera3_skel_base_serv_n + gc3_skel->camera_id,
            RPC_GUZZI_CAMERA3_CALLBACK_NOTIFY,
            &packet
        );
    if (!packet_return) {
        mmsdbg(DL_ERROR, "Failed to execute remote call!");
        goto exit1;
    }
    if (packet_return->header.data_size != sizeof (*rpc_callback_notify_return)) {
        mmsdbg(DL_ERROR, "Packet size mismatch!");
        goto exit2;
    }

    rpc_callback_notify_return = packet_return->payload[0].p;
    if (rpc_callback_notify_return->instance != gc3_skel->instance) {
        mmsdbg(DL_ERROR, "Invalid camera instance!");
        goto exit2;
    }

    rpc_service_free_packet(
            gc3_skel->rpc,
            packet_return->header.service,
            packet_return
        );

    return;
exit2:
    rpc_service_free_packet(
            gc3_skel->rpc,
            packet_return->header.service,
            packet_return
        );
exit1:
    return;
}

/*
 * ****************************************************************************
 * ** Request Defaults ********************************************************
 * ****************************************************************************
 */
int guzzi_camera3_skel_request_defaults(
        rpc_t *rpc,
        rpc_packet_t *call
    )
{
    guzzi_camera3_skel_t *gc3_skel;
    rpc_int32_t instance;
    guzzi_camera3_template_t type;
    guzzi_camera3_metadata_controls_t *metadata;
    rpc_packet_t result;
    rpc_guzzi_camera3_request_defaults_t *rpc_request_defaults;
    rpc_guzzi_camera3_request_defaults_return_t rpc_request_defaults_return;
    int err;

    rpc_request_defaults = call->payload[0].p;
    instance = rpc_request_defaults->instance;

    result.payload[0].p = &rpc_request_defaults_return;
    result.payload[0].size = sizeof (rpc_request_defaults_return);
    result.payload[1].p = NULL;
    result.payload[1].size = 0;
    result.payload[2].p = NULL;
    result.payload[2].size = 0;
    result.header.data_size = result.payload[0].size;

    if (call->header.data_size != sizeof (*rpc_request_defaults)) {
        mmsdbg(DL_ERROR, "Packet size mismatch!");
        goto exit1;
    }

    if (INSTANCES_MAX <= instance) {
        mmsdbg(
                DL_ERROR,
                "Invalid instance=%d, max=%d!",
                instance,
                INSTANCES_MAX
            );
        goto exit1;
    }

    gc3_skel = guzzi_camera3_skel_instances[instance];
    if (!gc3_skel) {
        mmsdbg(DL_ERROR, "Instance=%d is not active!", instance);
        goto exit1;
    }

    if (call->header.data_size != sizeof (*rpc_request_defaults)) {
        mmsdbg(DL_ERROR, "Packet size mismatch!");
        goto exit1;
    }

    if (INSTANCES_MAX <= instance) {
        mmsdbg(
                DL_ERROR,
                "Invalid instance=%d, max=%d!",
                instance,
                INSTANCES_MAX
            );
        goto exit1;
    }

    gc3_skel = guzzi_camera3_skel_instances[instance];
    if (!gc3_skel) {
        mmsdbg(DL_ERROR, "Instance=%d is not active!", instance);
        goto exit1;
    }

    type = (guzzi_camera3_template_t)rpc_request_defaults->type;

    metadata = osal_malloc(sizeof (*metadata));
    if (!metadata) {
        mmsdbg(
                DL_ERROR,
                "Failed to allocate memory for metada (size=%d)!",
                sizeof (*metadata)
            );
        goto exit1;
    }

    err = guzzi_camera3_request_defaults(gc3_skel->gc3, metadata, type);
    if (err) {
        mmsdbg(DL_ERROR, "Failed to request default metadata!");
    }

    result.payload[1].p = metadata;
    result.payload[1].size = sizeof (*metadata);
    result.header.data_size += result.payload[1].size;

    rpc_request_defaults_return.instance = instance;
    rpc_request_defaults_return.result = err;
    rpc_return(
            rpc,
            RPC_TRANSPORT_DEFAULT,
            guzzi_camera3_skel_base_serv_n + gc3_skel->camera_id,
            call,
            &result
        );
    rpc_service_free_packet(rpc, call->header.service, call);

    osal_free(metadata);

    return 0;
exit1:
    rpc_request_defaults_return.instance = instance;
    rpc_request_defaults_return.result = -1;
    RPC_RETURN(rpc, call, &result);
    rpc_service_free_packet(rpc, call->header.service, call);
    return -1;
}

/*
 * ****************************************************************************
 * ** Configure Streams *******************************************************
 * ****************************************************************************
 */
int guzzi_camera3_skel_config_streams(
        rpc_t *rpc,
        rpc_packet_t *call
    )
{
    guzzi_camera3_skel_t *gc3_skel;
    rpc_int32_t instance;
    guzzi_camera3_stream_configuration_t stream_configuration;
    guzzi_camera3_stream_t *streams;
    rpc_packet_t result;
    rpc_guzzi_camera3_stream_configuration_t *rpc_stream_configuration;
    rpc_guzzi_camera3_stream_t *rpc_streams;
    rpc_guzzi_camera3_stream_configuration_return_t rpc_stream_configuration_return;
    int i;
    int err;

    rpc_stream_configuration = call->payload[0].p;
    instance = rpc_stream_configuration->instance;

    result.payload[0].p = &rpc_stream_configuration_return;
    result.payload[0].size = sizeof (rpc_stream_configuration_return);
    result.payload[1].p = NULL;
    result.payload[1].size = 0;
    result.header.data_size = result.payload[0].size;

    if (call->header.data_size != sizeof (*rpc_stream_configuration)) {
        mmsdbg(DL_ERROR, "Packet size mismatch!");
        goto exit1;
    }

    if (INSTANCES_MAX <= instance) {
        mmsdbg(
                DL_ERROR,
                "Invalid instance=%d, max=%d!",
                instance,
                INSTANCES_MAX
            );
        goto exit1;
    }

    gc3_skel = guzzi_camera3_skel_instances[instance];
    if (!gc3_skel) {
        mmsdbg(DL_ERROR, "Instance=%d is not active!", instance);
        goto exit1;
    }

    if (GUZZI_CAMERA3_STREAMS_MAX <= rpc_stream_configuration->num_streams) {
        mmsdbg(DL_ERROR, "Instance=%d: too many streams!", instance);
        goto exit1;
    }
    stream_configuration.num_streams = (unsigned int)rpc_stream_configuration->num_streams;
    for (i = 0; i < stream_configuration.num_streams; i++) {
        streams = stream_configuration.streams + i;
        rpc_streams = rpc_stream_configuration->streams + i;

        streams->id = (unsigned int)rpc_streams->id;
        streams->type = (guzzi_camera3_stream_type_t)rpc_streams->type;
        streams->format = (guzzi_camera3_image_format_t)rpc_streams->format;
        streams->width = (unsigned int)rpc_streams->width;
        streams->height = (unsigned int)rpc_streams->height;
    }

    err = guzzi_camera3_config_streams(gc3_skel->gc3, &stream_configuration);
    if (err) {
        mmsdbg(DL_ERROR, "Failed to configure streams!");
    }

    rpc_stream_configuration_return.instance = instance;
    rpc_stream_configuration_return.result = err;
    rpc_return(
            rpc,
            RPC_TRANSPORT_DEFAULT,
            guzzi_camera3_skel_base_serv_n + gc3_skel->camera_id,
            call,
            &result
        );
    rpc_service_free_packet(rpc, call->header.service, call);

    return 0;
exit1:
    rpc_stream_configuration_return.instance = instance;
    rpc_stream_configuration_return.result = -1;
    RPC_RETURN(rpc, call, &result);
    rpc_service_free_packet(rpc, call->header.service, call);
    return -1;
}

/*
 * ****************************************************************************
 * ** Capture Request *********************************************************
 * ****************************************************************************
 */
int guzzi_camera3_skel_capture_request(
        rpc_t *rpc,
        rpc_packet_t *call
    )
{
    guzzi_camera3_skel_t *gc3_skel;
    rpc_int32_t instance;
    rpc_packet_t result;
    guzzi_camera3_capture_request_t capture_request;
    rpc_guzzi_camera3_capture_request_t *rpc_capture_request;
    rpc_guzzi_camera3_capture_request_return_t rpc_capture_request_return;
    int i;
    int err;

    rpc_capture_request = call->payload[0].p;
    instance = rpc_capture_request->instance;

    result.payload[0].p = &rpc_capture_request_return;
    result.payload[0].size = sizeof (rpc_capture_request_return);
    result.payload[1].p = NULL;
    result.payload[1].size = 0;
    result.header.data_size = result.payload[0].size;

    if (   (call->payload[0].size <= sizeof (*rpc_capture_request))
        || ((  sizeof (*rpc_capture_request)
             + sizeof (*rpc_capture_request->settings)) < call->payload[0].size)
        )
    {
        mmsdbg(DL_ERROR, "Packet size mismatch!");
        goto exit1;
    }

    if (INSTANCES_MAX <= instance) {
        mmsdbg(
                DL_ERROR,
                "Invalid instance=%d, max=%d!",
                instance,
                INSTANCES_MAX
            );
        goto exit1;
    }

    gc3_skel = guzzi_camera3_skel_instances[instance];
    if (!gc3_skel) {
        mmsdbg(DL_ERROR, "Instance=%d is not active!", instance);
        goto exit1;
    }

    capture_request.frame_number =
        (unsigned int)rpc_capture_request->frame_number;
    for (i = 0; i < GUZZI_CAMERA3_STREAMS_MAX; i++) {
        capture_request.streams_id[i] =
            (unsigned int)rpc_capture_request->streams_id[i];
    }
    capture_request.settings = (void *)rpc_capture_request->settings; /* TODO */

    err = guzzi_camera3_capture_request(gc3_skel->gc3, &capture_request);
    if (err) {
        mmsdbg(DL_ERROR, "Failed to submit capture request!");
    }

    rpc_capture_request_return.instance = instance;
    rpc_capture_request_return.result = err;
    rpc_return(
            rpc,
            RPC_TRANSPORT_DEFAULT,
            guzzi_camera3_skel_base_serv_n + gc3_skel->camera_id,
            call,
            &result
        );
    rpc_service_free_packet(rpc, call->header.service, call);

    return 0;
exit1:
    rpc_capture_request_return.instance = instance;
    rpc_capture_request_return.result = -1;
    RPC_RETURN(rpc, call, &result);
    rpc_service_free_packet(rpc, call->header.service, call);
    return -1;
}

/*
 * ****************************************************************************
 * ** Flush *******************************************************************
 * ****************************************************************************
 */
int guzzi_camera3_skel_flush(
        rpc_t *rpc,
        rpc_packet_t *call
    )
{
    guzzi_camera3_skel_t *gc3_skel;
    rpc_int32_t instance;
    rpc_packet_t result;
    rpc_guzzi_camera3_flush_t *rpc_flush;
    rpc_guzzi_camera3_flush_return_t rpc_flush_return;

    rpc_flush = call->payload[0].p;
    instance = rpc_flush->instance;

    result.payload[0].p = &rpc_flush_return;
    result.payload[0].size = sizeof (rpc_flush_return);
    result.payload[1].p = NULL;
    result.payload[1].size = 0;
    result.header.data_size = result.payload[0].size;

    if (call->header.data_size != sizeof (*rpc_flush)) {
        mmsdbg(DL_ERROR, "Packet size mismatch!");
        goto exit1;
    }

    if (INSTANCES_MAX <= instance) {
        mmsdbg(
                DL_ERROR,
                "Invalid instance=%d, max=%d!",
                instance,
                INSTANCES_MAX
            );
        goto exit1;
    }

    gc3_skel = guzzi_camera3_skel_instances[instance];
    if (!gc3_skel) {
        mmsdbg(DL_ERROR, "Instance=%d is not active!", instance);
        goto exit1;
    }

    guzzi_camera3_flush(gc3_skel->gc3);

    rpc_flush_return.instance = instance;
    rpc_return(
            rpc,
            RPC_TRANSPORT_DEFAULT,
            guzzi_camera3_skel_base_serv_n + gc3_skel->camera_id,
            call,
            &result
        );
    rpc_service_free_packet(rpc, call->header.service, call);

    return 0;
exit1:
    rpc_flush_return.instance = instance;
    RPC_RETURN(rpc, call, &result);
    rpc_service_free_packet(rpc, call->header.service, call);
    return -1;
}

/*
 * ****************************************************************************
 * ** Destroy *****************************************************************
 * ****************************************************************************
 */
int guzzi_camera3_skel_destroy(
        rpc_t *rpc,
        rpc_packet_t *call
    )
{
    guzzi_camera3_skel_t *gc3_skel;
    rpc_int32_t instance;
    rpc_packet_t result;
    rpc_guzzi_camera3_destroy_t *rpc_destroy;
    rpc_guzzi_camera3_destroy_return_t rpc_destroy_return;

    rpc_destroy = call->payload[0].p;
    instance = rpc_destroy->instance;

    result.payload[0].p = &rpc_destroy_return;
    result.payload[0].size = sizeof (rpc_destroy_return);
    result.payload[1].p = NULL;
    result.payload[1].size = 0;
    result.header.data_size = result.payload[0].size;

    if (call->header.data_size != sizeof (*rpc_destroy)) {
        mmsdbg(DL_ERROR, "Packet size mismatch!");
        goto exit1;
    }

    if (INSTANCES_MAX <= instance) {
        mmsdbg(
                DL_ERROR,
                "Invalid instance=%d, max=%d!",
                instance,
                INSTANCES_MAX
            );
        goto exit1;
    }

    gc3_skel = guzzi_camera3_skel_instances[instance];
    if (!gc3_skel) {
        mmsdbg(DL_ERROR, "Instance=%d is not active!", instance);
        goto exit1;
    }
    guzzi_camera3_skel_instances[instance] = NULL;

    guzzi_camera3_destroy(gc3_skel->gc3);

    rpc_destroy_return.instance = instance;
    rpc_return(
            rpc,
            RPC_TRANSPORT_DEFAULT,
            guzzi_camera3_skel_base_serv_n + gc3_skel->camera_id,
            call,
            &result
        );
    rpc_service_free_packet(rpc, call->header.service, call);

    gc3_rpc_service_destroy(gc3_skel->serv);
    osal_free(gc3_skel);

    return 0;
exit1:
    rpc_destroy_return.instance = instance;
    RPC_RETURN(rpc, call, &result);
    rpc_service_free_packet(rpc, call->header.service, call);
    return -1;
}

/*
 * ****************************************************************************
 * ** Create ******************************************************************
 * ****************************************************************************
 */
int guzzi_camera3_skel_create(
        rpc_t *rpc,
        rpc_packet_t *call
    )
{
    guzzi_camera3_skel_t *gc3_skel;
    rpc_packet_t result;
    rpc_guzzi_camera3_create_t *rpc_create;
    rpc_guzzi_camera3_create_return_t rpc_create_return;
    rpc_int32_t instance;
    int camera_id;

    rpc_create = call->payload[0].p;

    result.payload[0].p = &rpc_create_return;
    result.payload[0].size = sizeof (rpc_create_return);
    result.payload[1].p = NULL;
    result.payload[1].size = 0;
    result.header.data_size = result.payload[0].size;

    if (call->header.data_size != sizeof (*rpc_create)) {
        mmsdbg(DL_ERROR, "Packet size mismatch!");
        goto exit1;
    }

    camera_id = (int)rpc_create->camera_id;

    gc3_skel = osal_calloc(1, sizeof (*gc3_skel));
    if (!gc3_skel) {
        mmsdbg(DL_ERROR, "Failed to allocate new camera3 skel instance!");
        goto exit1;
    }
    gc3_skel->rpc = rpc;
    gc3_skel->camera_id = camera_id;

    gc3_skel->serv = gc3_rpc_service_create(
            gc3_skel->rpc,
            guzzi_camera3_skel_base_serv_n + gc3_skel->camera_id,
            GC3_RPC_SERVICE_THREADS,
            GC3_RPC_SERVICE_THREAD_STACK_SIZE
        );
    if (!gc3_skel->serv) {
        mmsdbg(DL_ERROR, "Failed to create GuzziCamera3 Service!");
        goto exit2;
    }

    gc3_skel->gc3 = guzzi_camera3_create(
            gc3_skel->camera_id,
            guzzi_camera3_skel_calback_notify,
            gc3_skel
        );
    if (!gc3_skel->gc3) {
        mmsdbg(DL_ERROR, "Failed to create new camera3 instance!");
        goto exit3;
    }

    instance = store_instance(gc3_skel);
    if (instance < 0) {
        mmsdbg(DL_ERROR, "Failed store new camera3 instance!");
        goto exit4;
    }

    rpc_create_return.instance = instance;
    rpc_return(
            rpc,
            RPC_TRANSPORT_DEFAULT,
            guzzi_camera3_skel_base_serv_n + gc3_skel->camera_id,
            call,
            &result
        );

    rpc_service_free_packet(rpc, call->header.service, call);

    return 0;
exit4:
    guzzi_camera3_destroy(gc3_skel->gc3);
exit3:
    gc3_rpc_service_destroy(gc3_skel->serv);
exit2:
    osal_free(gc3_skel);
exit1:
    rpc_create_return.instance = RPC_GUZZI_CAMERA3_INSTANCE_INVALID;
    RPC_RETURN(rpc, call, &result);
    rpc_service_free_packet(rpc, call->header.service, call);
    return -1;
}

/*
 * ****************************************************************************
 * ** Init ********************************************************************
 * ****************************************************************************
 */
int guzzi_camera3_skel_init(rpc_t *rpc, int get_info_serv_n, int base_serv_n)
{
    guzzi_camera3_skel_rpc = rpc;
    guzzi_camera3_skel_get_info_serv_n = get_info_serv_n;
    guzzi_camera3_skel_base_serv_n = base_serv_n;
    return 0;
}

