/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <osal/osal_stdlib.h>
#include <osal/osal_string.h>
#include <osal/pool.h>
#include <utils/mms_debug.h>
#include <guzzi/camera3/camera3.h>
#include <guzzi/rpc/rpc.h>
#include <guzzi/rpc/camera3_rpc.h>

#define GC3_STUB_TIMEOUT DEFAULT_ALLOC_TIMEOUT_MS

#define INSTANCES_MAX RPC_GUZZI_CAMERA3_INSTANCES_MAX

#define GC3_RPC_SERVICE_THREADS 1
#define GC3_RPC_SERVICE_THREAD_STACK_SIZE 8196

typedef struct {
    rpc_t *rpc;
    int serv_n;
    pool_t *pool_big;
    pool_t *pool_small;
} gc3_rpc_service_t;

struct guzzi_camera3 {
    int camera_id;
    guzzi_camera3_calback_notify_t *callback_notify;
    void *client_prv;
    rpc_uint32_t instance;
    rpc_t *rpc;
    gc3_rpc_service_t *serv;
};

static rpc_t *guzzi_camera3_stub_rpc;
static int guzzi_camera3_stub_base_serv_n;

mmsdbg_define_variable(
        vdl_camera3_stub,
        DL_DEFAULT,
        0,
        "vdl_camera3_stub",
        "Camera3 skel."
    );
#define MMSDEBUGLEVEL mmsdbg_use_variable(vdl_camera3_stub)

static guzzi_camera3_t *guzzi_camera3_stub_instances[INSTANCES_MAX] = {NULL};

/*
 * ****************************************************************************
 * ** Packet alloc/free *******************************************************
 * ****************************************************************************
 */
static rpc_packet_t *gc3_rpc_service_packet_alloc(
        rpc_packet_header_t *packet_header,
        void *prv
    )
{
    gc3_rpc_service_t *gc3serv;
    pool_t *pool;
    rpc_packet_t *packet;

    gc3serv = prv;

    if (packet_header->data_size <= 512) {
        pool = gc3serv->pool_small;
    } else if (packet_header->data_size <= 64*1024) {
        pool = gc3serv->pool_big;
    } else {
        mmsdbg(
                DL_ERROR,
                "Unable to find suitable pool for data_size=%d!",
                packet_header->data_size
            );
        goto exit1;
    }

    packet = pool_alloc_timeout(pool, GC3_STUB_TIMEOUT);
    if (!packet) {
        mmsdbg(DL_ERROR, "Failed to allocate new packet!");
        goto exit1;
    }

    packet->payload[0].p = packet + 1;
    packet->payload[0].size = packet_header->data_size;
    packet->payload[1].p = NULL;
    packet->payload[1].size = 0;

    return packet;
exit1:
    return NULL;
}

static void gc3_rpc_service_packet_free(
        rpc_packet_t *packet,
        void *prv
    )
{
    osal_free(packet);
}

static void gc3_rpc_service_destroy(gc3_rpc_service_t *gc3serv)
{
    rpc_service_destroy(gc3serv->rpc, gc3serv->serv_n);
    pool_destroy(gc3serv->pool_small);
    pool_destroy(gc3serv->pool_big);
    osal_free(gc3serv);
}

static gc3_rpc_service_t *gc3_rpc_service_create(
        rpc_t *rpc,
        int serv_n,
        int threads,
        int thread_stack_size
    )
{
    gc3_rpc_service_t *gc3serv;
    int err;

    gc3serv = osal_calloc(1, sizeof (*gc3serv));
    if (!gc3serv) {
        mmsdbg(
                DL_ERROR,
                "Failed to allocate memory for new instance: size=%d!",
                sizeof (*gc3serv)
            );
        goto exit1;
    }
    gc3serv->rpc = rpc;
    gc3serv->serv_n = serv_n;

    gc3serv->pool_big = pool_create(
            "GuzziCamera3 RPC Service Big",
            64*1024 + sizeof (rpc_packet_t),
            1
        );
    if (!gc3serv->pool_big) {
        mmsdbg(
                DL_ERROR,
                "Failed to create GuzziCamera3 RPC Service Big pool!"
            );
        goto exit2;
    }

    gc3serv->pool_small = pool_create(
            "GuzziCamera3 RPC Service Small",
            512 + sizeof (rpc_packet_t),
            4
        );
    if (!gc3serv->pool_small) {
        mmsdbg(
                DL_ERROR,
                "Failed to create GuzziCamera3 RPC Service Small pool!"
            );
        goto exit3;
    }

    err = rpc_service_create(
            gc3serv->rpc,
            gc3serv->serv_n,
            gc3serv,
            gc3_rpc_service_packet_alloc,
            gc3_rpc_service_packet_free,
            threads,
            thread_stack_size
        );
    if (err) {
        mmsdbg(DL_ERROR, "Failed to create GuzziCamera3 RPC service!");
        goto exit4;
    }

    return gc3serv;
exit4:
    pool_destroy(gc3serv->pool_small);
exit3:
    pool_destroy(gc3serv->pool_big);
exit2:
    osal_free(gc3serv);
exit1:
    return NULL;
}

/*
 * ****************************************************************************
 * ** Get camera id ***********************************************************
 * ****************************************************************************
 */
int guzzi_camera3_get_id(guzzi_camera3_t *gc3)
{
    return gc3->camera_id;
}

/*
 * ****************************************************************************
 * ** Get number of cameras ***************************************************
 * ****************************************************************************
 */
int guzzi_camera3_get_number_of_cameras(void)
{
    rpc_t *rpc;
    rpc_packet_t packet;
    rpc_packet_t *packet_return;
    rpc_guzzi_camera3_get_number_of_cameras_return_t *rpc_get_number_of_cameras_return;
    int number_of_cameras;

    rpc = guzzi_camera3_stub_rpc;

    packet.payload[0].p = NULL;
    packet.payload[0].size = 0;
    packet.header.data_size = 0;

    packet_return = RPC_CALL(rpc, RPC_GUZZI_CAMERA3_GET_NUMBER_OF_CAMERAS, &packet);
    if (!packet_return) {
        mmsdbg(DL_ERROR, "Failed to execute remote call!");
        goto exit1;
    }
    if (packet_return->header.data_size != sizeof (*rpc_get_number_of_cameras_return)) {
        mmsdbg(DL_ERROR, "Packet size mismatch!");
        goto exit2;
    }

    rpc_get_number_of_cameras_return = packet_return->payload[0].p;
    number_of_cameras = (int)rpc_get_number_of_cameras_return->number_of_cameras;

    rpc_service_free_packet(
            rpc,
            packet_return->header.service,
            packet_return
        );

    return number_of_cameras;
exit2:
    rpc_service_free_packet(
            rpc,
            packet_return->header.service,
            packet_return
        );
exit1:
    return -1;
}

/*
 * ****************************************************************************
 * ** Get Info ****************************************************************
 * ****************************************************************************
 */
int guzzi_camera3_get_info(
        int camera_id,
        guzzi_camera3_info_t *gc3_info
    )
{
    rpc_t *rpc;
    rpc_packet_t packet;
    rpc_packet_t *packet_return;
    rpc_guzzi_camera3_get_info_t rpc_get_info;
    rpc_guzzi_camera3_get_info_return_t *rpc_get_info_return;
    int err;

    rpc = guzzi_camera3_stub_rpc;

    rpc_get_info.camera_id = (rpc_int32_t)camera_id;

    packet.payload[0].p = &rpc_get_info;
    packet.payload[0].size = sizeof (rpc_get_info);
    packet.payload[1].p = NULL;
    packet.payload[1].size = 0;
    packet.header.data_size = packet.payload[0].size;

    packet_return = RPC_CALL(rpc, RPC_GUZZI_CAMERA3_GET_CAMERA_INFO, &packet);
    if (!packet_return) {
        mmsdbg(DL_ERROR, "Failed to execute remote call!");
        goto exit1;
    }
    if (packet_return->header.data_size != sizeof (*rpc_get_info_return) + sizeof (*rpc_get_info_return->metadata_static)) {
        mmsdbg(DL_ERROR, "Packet size mismatch!");
        goto exit2;
    }

    rpc_get_info_return = packet_return->payload[0].p;
    if (rpc_get_info_return->camera_id != camera_id) {
        mmsdbg(DL_ERROR, "Wrong camera id!");
        goto exit2;
    }
    gc3_info->facing = (int)rpc_get_info_return->facing;
    gc3_info->orientation = (int)rpc_get_info_return->orientation;
    memcpy(
            gc3_info->metadata_static,
            &rpc_get_info_return->metadata_static,
            sizeof (*gc3_info->metadata_static)
        );

    err = rpc_get_info_return->result;

    rpc_service_free_packet(
            rpc,
            packet_return->header.service,
            packet_return
        );

    return err;
exit2:
    rpc_service_free_packet(
            rpc,
            packet_return->header.service,
            packet_return
        );
exit1:
    return -1;
}

/*
 * ****************************************************************************
 * ** Callback ****************************************************************
 * ****************************************************************************
 */
int guzzi_camera3_stub_calback_notify(
        rpc_t *rpc,
        rpc_packet_t *call
    )
{
    guzzi_camera3_t *gc3;
    rpc_int32_t instance;
    rpc_packet_t result;
    rpc_guzzi_camera3_callback_notify_t *rpc_callback_notify;
    rpc_guzzi_camera3_callback_notify_return_t rpc_callback_notify_return;
    guzzi_camera3_event_t event;
    guzzi_camera3_event_shutter_t shutter;

    rpc_callback_notify = call->payload[0].p;
    instance = rpc_callback_notify->instance;

    result.payload[0].p = &rpc_callback_notify_return;
    result.payload[0].size = sizeof (rpc_callback_notify_return);
    result.payload[1].p = NULL;
    result.payload[1].size = 0;
    result.header.data_size = result.payload[0].size;

    if (call->header.data_size != sizeof (*rpc_callback_notify)) {
        mmsdbg(DL_ERROR, "Packet size mismatch!");
        goto exit1;
    }

    if (INSTANCES_MAX <= instance) {
        mmsdbg(
                DL_ERROR,
                "Invalid instance=%d, max=%d!",
                instance,
                INSTANCES_MAX
            );
        goto exit1;
    }

    gc3 = guzzi_camera3_stub_instances[instance];
    if (!gc3) {
        mmsdbg(DL_ERROR, "Instance=%d is not active!", instance);
        goto exit1;
    }

    event = (guzzi_camera3_event_t)rpc_callback_notify->event;

    switch (event) {
        case GUZZI_CAMERA3_EVENT_SHUTTER:
            shutter.frame_number =
                (unsigned int)(rpc_callback_notify->shutter.frame_number);
            shutter.timestamp =
                (unsigned long long)(rpc_callback_notify->shutter.timestamp);
            gc3->callback_notify(
                    gc3,
                    gc3->client_prv,
                    event,
                    0,
                    &shutter
                );
            break;
        case GUZZI_CAMERA3_EVENT_ERROR:
            /* TODO: */
            break;
        default:
            mmsdbg(DL_ERROR, "Unknown Guzzi Camera3 Event!");
    }

    rpc_callback_notify_return.instance = instance;
    rpc_return(
            rpc,
            RPC_TRANSPORT_DEFAULT,
            guzzi_camera3_stub_base_serv_n + gc3->camera_id,
            call,
            &result
        );
    rpc_service_free_packet(rpc, call->header.service, call);

    return 0;
exit1:
    rpc_callback_notify_return.instance = instance;
    rpc_return(
            rpc,
            RPC_TRANSPORT_DEFAULT,
            guzzi_camera3_stub_base_serv_n + gc3->camera_id,
            call,
            &result
        );
    rpc_service_free_packet(rpc, call->header.service, call);
    return -1;
}

/*
 * ****************************************************************************
 * ** Request Defaults ********************************************************
 * ****************************************************************************
 */
int guzzi_camera3_request_defaults(
        guzzi_camera3_t *gc3,
        guzzi_camera3_metadata_controls_t *metadata,
        guzzi_camera3_template_t type
    )
{
    rpc_packet_t packet;
    rpc_packet_t *packet_return;
    rpc_guzzi_camera3_request_defaults_t rpc_request_defaults;
    rpc_guzzi_camera3_request_defaults_return_t *rpc_request_defaults_return;
    int err;

    rpc_request_defaults.instance = gc3->instance;
    rpc_request_defaults.type = type;

    packet.payload[0].p = &rpc_request_defaults;
    packet.payload[0].size = sizeof (rpc_request_defaults);
    packet.payload[1].p = NULL;
    packet.payload[1].size = 0;
    packet.header.data_size = packet.payload[0].size;

    packet_return = rpc_call(
            gc3->rpc,
            RPC_TRANSPORT_DEFAULT,
            guzzi_camera3_stub_base_serv_n + gc3->camera_id,
            RPC_GUZZI_CAMERA3_REQUEST_DEFAULTS,
            &packet
        );
    if (!packet_return) {
        mmsdbg(DL_ERROR, "Failed to execute remote call!");
        goto exit1;
    }
    if (packet_return->header.data_size != sizeof (*rpc_request_defaults_return) + sizeof (*rpc_request_defaults_return->metadata)) {
        mmsdbg(DL_ERROR, "Packet size mismatch!");
        goto exit2;
    }

    rpc_request_defaults_return = packet_return->payload[0].p;
    if (rpc_request_defaults_return->instance != gc3->instance) {
        mmsdbg(DL_ERROR, "Wrong camera instance!");
        goto exit2;
    }
    memcpy(metadata, &rpc_request_defaults_return->metadata, sizeof (*metadata));

    err = rpc_request_defaults_return->result;

    rpc_service_free_packet(
            gc3->rpc,
            packet_return->header.service,
            packet_return
        );

    return err;
exit2:
    rpc_service_free_packet(
            gc3->rpc,
            packet_return->header.service,
            packet_return
        );
exit1:
    return -1;
}

/*
 * ****************************************************************************
 * ** Configure Streams *******************************************************
 * ****************************************************************************
 */
int guzzi_camera3_config_streams(
        guzzi_camera3_t *gc3,
        guzzi_camera3_stream_configuration_t *stream_configuration
    )
{
    rpc_packet_t packet;
    rpc_packet_t *packet_return;
    guzzi_camera3_stream_t *streams;
    rpc_guzzi_camera3_stream_configuration_t rpc_stream_configuration;
    rpc_guzzi_camera3_stream_t *rpc_streams;
    rpc_guzzi_camera3_stream_configuration_return_t *rpc_configuration_return;
    int i;
    int err;

    if (GUZZI_CAMERA3_STREAMS_MAX <= stream_configuration->num_streams) {
        mmsdbg(
                DL_ERROR,
                "Maximum supported streams is %d. Requested %d!",
                GUZZI_CAMERA3_STREAMS_MAX,
                stream_configuration->num_streams
            );
        goto exit1;
    }

    rpc_stream_configuration.instance = gc3->instance;

    rpc_stream_configuration.num_streams = stream_configuration->num_streams;
    for (i = 0; i < rpc_stream_configuration.num_streams; i++) {
        streams = stream_configuration->streams + i;
        rpc_streams = rpc_stream_configuration.streams + i;

        rpc_streams->id = streams->id;
        rpc_streams->type = streams->type;
        rpc_streams->format = streams->format;
        rpc_streams->width = streams->width;
        rpc_streams->height = streams->height;
    }

    packet.payload[0].p = &rpc_stream_configuration;
    packet.payload[0].size = sizeof (rpc_stream_configuration);
    packet.payload[1].p = NULL;
    packet.payload[1].size = 0;
    packet.header.data_size = packet.payload[0].size;

    packet_return = rpc_call(
            gc3->rpc,
            RPC_TRANSPORT_DEFAULT,
            guzzi_camera3_stub_base_serv_n + gc3->camera_id,
            RPC_GUZZI_CAMERA3_STREAM_CONFIGURATION,
            &packet
        );
    if (!packet_return) {
        mmsdbg(DL_ERROR, "Failed to execute remote call!");
        goto exit1;
    }
    if (packet_return->header.data_size != sizeof (*rpc_configuration_return)) {
        mmsdbg(DL_ERROR, "Packet size mismatch!");
        goto exit2;
    }

    rpc_configuration_return = packet_return->payload[0].p;
    if (rpc_configuration_return->instance != gc3->instance) {
        mmsdbg(DL_ERROR, "Wrong camera instance!");
        goto exit2;
    }

    err = rpc_configuration_return->result;

    rpc_service_free_packet(
            gc3->rpc,
            packet_return->header.service,
            packet_return
        );

    return err;
exit2:
    rpc_service_free_packet(
            gc3->rpc,
            packet_return->header.service,
            packet_return
        );
exit1:
    return -1;
}

/*
 * ****************************************************************************
 * ** Capture Request *********************************************************
 * ****************************************************************************
 */
int guzzi_camera3_capture_request(
        guzzi_camera3_t *gc3,
        guzzi_camera3_capture_request_t *capture_request
    )
{
    rpc_packet_t packet;
    rpc_packet_t *packet_return;
    rpc_guzzi_camera3_capture_request_t rpc_capture_request;
    rpc_guzzi_camera3_capture_request_return_t *rpc_capture_request_return;
    int settings_size;
    guzzi_camera3_metadata_t *metadata;
    int i;
    int err;

    rpc_capture_request.instance = gc3->instance;
    rpc_capture_request.frame_number = capture_request->frame_number;
    for (i = 0; i < RPC_GUZZI_CAMERA3_STREAMS_MAX; i++) {
        rpc_capture_request.streams_id[i] = capture_request->streams_id[i];
    }

    for_each_metadata(capture_request->settings, metadata) {}
    metadata += 1;
    settings_size = (char *)metadata - (char *)capture_request->settings;
    //settings_size = sizeof (*rpc_capture_request.settings);

    packet.payload[0].p = &rpc_capture_request;
    packet.payload[0].size = sizeof (rpc_capture_request);
    packet.payload[1].p = capture_request->settings;
    packet.payload[1].size = settings_size;
    packet.payload[2].p = NULL;
    packet.payload[2].size = 0;
    packet.header.data_size =
          packet.payload[0].size
        + packet.payload[1].size;

    packet_return = rpc_call(
            gc3->rpc,
            RPC_TRANSPORT_DEFAULT,
            guzzi_camera3_stub_base_serv_n + gc3->camera_id,
            RPC_GUZZI_CAMERA3_CAPTURE_REQUEST,
            &packet
        );
    if (!packet_return) {
        mmsdbg(DL_ERROR, "Failed to execute remote call!");
        goto exit1;
    }
    if (packet_return->header.data_size != sizeof (*rpc_capture_request_return)) {
        mmsdbg(DL_ERROR, "Packet size mismatch!");
        goto exit2;
    }

    rpc_capture_request_return = packet_return->payload[0].p;
    if (rpc_capture_request_return->instance != gc3->instance) {
        mmsdbg(DL_ERROR, "Wrong camera instance!");
        goto exit2;
    }

    err = rpc_capture_request_return->result;

    rpc_service_free_packet(
            gc3->rpc,
            packet_return->header.service,
            packet_return
        );

    return err;
exit2:
    rpc_service_free_packet(
            gc3->rpc,
            packet_return->header.service,
            packet_return
        );
exit1:
    return -1;
}

/*
 * ****************************************************************************
 * ** Flush *******************************************************************
 * ****************************************************************************
 */
void guzzi_camera3_flush(guzzi_camera3_t *gc3)
{
    rpc_packet_t packet;
    rpc_packet_t *packet_return;
    rpc_guzzi_camera3_flush_t rpc_flush;
    rpc_guzzi_camera3_flush_return_t *rpc_flush_return;

    rpc_flush.instance = gc3->instance;

    packet.payload[0].p = &rpc_flush;
    packet.payload[0].size = sizeof (rpc_flush);
    packet.payload[1].p = NULL;
    packet.payload[1].size = 0;
    packet.header.data_size = packet.payload[0].size;

    packet_return = rpc_call(
            gc3->rpc,
            RPC_TRANSPORT_DEFAULT,
            guzzi_camera3_stub_base_serv_n + gc3->camera_id,
            RPC_GUZZI_CAMERA3_FLUSH,
            &packet
        );
    if (!packet_return) {
        mmsdbg(DL_ERROR, "Failed to execute remote call!");
        goto exit1;
    }
    if (packet_return->header.data_size != sizeof (*rpc_flush_return)) {
        mmsdbg(DL_ERROR, "Packet size mismatch!");
        goto exit2;
    }

    rpc_flush_return = packet_return->payload[0].p;
    if (rpc_flush_return->instance != gc3->instance) {
        mmsdbg(DL_ERROR, "Wrong camera instance!");
        goto exit2;
    }

    rpc_service_free_packet(
            gc3->rpc,
            packet_return->header.service,
            packet_return
        );

    return;
exit2:
    rpc_service_free_packet(
            gc3->rpc,
            packet_return->header.service,
            packet_return
        );
exit1:
    return;
}

/*
 * ****************************************************************************
 * ** Destroy *****************************************************************
 * ****************************************************************************
 */
void guzzi_camera3_destroy(guzzi_camera3_t *gc3)
{
    rpc_packet_t packet;
    rpc_packet_t *packet_return;
    rpc_guzzi_camera3_destroy_t rpc_destroy;
    rpc_guzzi_camera3_destroy_return_t *rpc_destroy_return;

    rpc_destroy.instance = gc3->instance;

    packet.payload[0].p = &rpc_destroy;
    packet.payload[0].size = sizeof (rpc_destroy);
    packet.payload[1].p = NULL;
    packet.payload[1].size = 0;
    packet.header.data_size = packet.payload[0].size;

    packet_return = RPC_CALL(gc3->rpc, RPC_GUZZI_CAMERA3_DESTROY, &packet);
    if (!packet_return) {
        mmsdbg(DL_ERROR, "Failed to execute remote call!");
        goto exit1;
    }
    if (packet_return->header.data_size != sizeof (*rpc_destroy_return)) {
        mmsdbg(DL_ERROR, "Packet size mismatch!");
        goto exit2;
    }

    rpc_destroy_return = packet_return->payload[0].p;
    if (rpc_destroy_return->instance != gc3->instance) {
        mmsdbg(DL_ERROR, "Wrong camera instance!");
        goto exit2;
    }

    rpc_service_free_packet(
            gc3->rpc,
            packet_return->header.service,
            packet_return
        );

    guzzi_camera3_stub_instances[gc3->instance] = NULL;
    gc3_rpc_service_destroy(gc3->serv);
    osal_free(gc3);

    return;
exit2:
    rpc_service_free_packet(
            gc3->rpc,
            packet_return->header.service,
            packet_return
        );
exit1:
    guzzi_camera3_stub_instances[gc3->instance] = NULL;
    osal_free(gc3);
    return;
}

/*
 * ****************************************************************************
 * ** Create ******************************************************************
 * ****************************************************************************
 */
guzzi_camera3_t *guzzi_camera3_create(
        int camera_id,
        guzzi_camera3_calback_notify_t *callback_notify,
        void *client_prv
    )
{
    guzzi_camera3_t *gc3;
    rpc_packet_t packet;
    rpc_packet_t *packet_return;
    rpc_guzzi_camera3_create_t rpc_create;
    rpc_guzzi_camera3_create_return_t *rpc_create_return;

    gc3 = osal_calloc(1, sizeof (*gc3));
    if (!gc3) {
        mmsdbg(
                DL_ERROR,
                "Failed to allocate memory for new instance: size=%d!",
                sizeof (*gc3)
            );
        goto exit1;
    }

    gc3->camera_id = camera_id;
    gc3->callback_notify = callback_notify;
    gc3->client_prv = client_prv;
    gc3->rpc = guzzi_camera3_stub_rpc;

    gc3->serv = gc3_rpc_service_create(
            gc3->rpc,
            guzzi_camera3_stub_base_serv_n + gc3->camera_id,
            GC3_RPC_SERVICE_THREADS,
            GC3_RPC_SERVICE_THREAD_STACK_SIZE
        );
    if (!gc3->serv) {
        mmsdbg(DL_ERROR, "Failed to create GuzziCamera3 Service!");
        goto exit2;
    }

    rpc_create.camera_id = (rpc_int32_t)gc3->camera_id;

    packet.payload[0].p = &rpc_create;
    packet.payload[0].size = sizeof (rpc_create);
    packet.payload[1].p = NULL;
    packet.payload[1].size = 0;
    packet.header.data_size = packet.payload[0].size;

    packet_return = RPC_CALL(gc3->rpc, RPC_GUZZI_CAMERA3_CREATE, &packet);
    if (!packet_return) {
        mmsdbg(DL_ERROR, "Failed to execute remote call!");
        goto exit3;
    }
    if (packet_return->header.data_size != sizeof (*rpc_create_return)) {
        mmsdbg(DL_ERROR, "Packet size mismatch!");
        goto exit4;
    }

    rpc_create_return = packet_return->payload[0].p;
    if (RPC_GUZZI_CAMERA3_INSTANCE_INVALID == rpc_create_return->instance) {
        mmsdbg(DL_ERROR, "Invalid camera instance!");
        goto exit4;
    }
    if (INSTANCES_MAX <= rpc_create_return->instance) {
        mmsdbg(
                DL_ERROR,
                "Invalid camera instance=%d, max=%d!",
                rpc_create_return->instance,
                INSTANCES_MAX
            );
        goto exit4;
    }
    if (guzzi_camera3_stub_instances[rpc_create_return->instance]) {
        mmsdbg(
                DL_ERROR,
                "Camera3 Instance %d already in use!",
                rpc_create_return->instance
            );
        goto exit4;
    }

    gc3->instance = rpc_create_return->instance;
    guzzi_camera3_stub_instances[gc3->instance] = gc3;

    rpc_service_free_packet(
            gc3->rpc,
            packet_return->header.service,
            packet_return
        );

    return gc3;
exit4:
    rpc_service_free_packet(
            gc3->rpc,
            packet_return->header.service,
            packet_return
        );
exit3:
    gc3_rpc_service_destroy(gc3->serv);
exit2:
    osal_free(gc3);
exit1:
    return NULL;
}

/*
 * ****************************************************************************
 * ** Init ********************************************************************
 * ****************************************************************************
 */
int guzzi_camera3_stub_init(rpc_t *rpc, int base_serv_n)
{
    guzzi_camera3_stub_rpc = rpc;
    guzzi_camera3_stub_base_serv_n = base_serv_n;
    return 0;
}

