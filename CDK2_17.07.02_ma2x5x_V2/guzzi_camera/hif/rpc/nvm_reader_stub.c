/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <osal/osal_stdlib.h>
#include <utils/mms_debug.h>

#include <guzzi/nvm_reader.h>

#include <guzzi/rpc/rpc.h>
#include <guzzi/rpc/nvm_reader_rpc.h>
#include <guzzi/rpc/nvm_reader_rpc_helpers.h>

#define INSTANCES_MAX RPC_GUZZI_NVM_READER_INSTANCES_MAX

struct guzzi_nvm_reader {
    rpc_t *rpc;
    rpc_uint32_t instance;
};

static rpc_t *guzzi_nvm_reader_stub_rpc;

static guzzi_nvm_reader_t *guzzi_nvm_reader_stub_instances[INSTANCES_MAX] = {NULL};

mmsdbg_define_variable(
        vdl_guzzi_nvm_reader_stub,
        DL_DEFAULT,
        0,
        "vdl_guzzi_nvm_reader_stub",
        "Guzzi NVM Reader Stub."
    );
#define MMSDEBUGLEVEL mmsdbg_use_variable(vdl_guzzi_nvm_reader_stub)

/*
 * ****************************************************************************
 * ** Number of cameras *******************************************************
 * ****************************************************************************
 */
int guzzi_nvm_reader_get_number_of_cameras(guzzi_nvm_reader_t *nvm)
{
    rpc_packet_t packet;
    rpc_packet_t *packet_return;
    rpc_guzzi_nvm_reader_get_number_of_cameras_t rpc_get_number_of_cameras;
    rpc_guzzi_nvm_reader_get_number_of_cameras_return_t *rpc_get_number_of_cameras_return;
    int number_of_cameras;

    rpc_get_number_of_cameras.instance = nvm->instance;

    packet.payload[0].p = &rpc_get_number_of_cameras;
    packet.payload[0].size = sizeof (rpc_get_number_of_cameras);
    packet.payload[1].p = NULL;
    packet.payload[1].size = 0;
    packet.header.data_size = packet.payload[0].size;

    packet_return = RPC_CALL(nvm->rpc, RPC_GUZZI_NVM_READER_GET_NUMBER_OF_CAMERAS, &packet);
    if (!packet_return) {
        mmsdbg(DL_ERROR, "Failed to execute remote call!");
        goto exit1;
    }
    if (packet_return->header.data_size != sizeof (*rpc_get_number_of_cameras_return)) {
        mmsdbg(DL_ERROR, "Packet size mismatch!");
        goto exit2;
    }

    rpc_get_number_of_cameras_return = packet_return->payload[0].p;
    number_of_cameras = (int)rpc_get_number_of_cameras_return->number_of_cameras;

    rpc_service_free_packet(
            nvm->rpc,
            packet_return->header.service,
            packet_return
        );

    return number_of_cameras;
exit2:
    rpc_service_free_packet(
            nvm->rpc,
            packet_return->header.service,
            packet_return
        );
exit1:
    return -1;
}

/*
 * ****************************************************************************
 * ** Read ********************************************************************
 * ****************************************************************************
 */
nvm_info_t * guzzi_nvm_reader_read(
        guzzi_nvm_reader_t *nvm,
        int camera_id /* [1, number_of_cameras] */
    )
{
    rpc_packet_t packet;
    rpc_packet_t *packet_return;
    rpc_guzzi_nvm_reader_read_t rpc_read;
    rpc_guzzi_nvm_reader_read_return_t *rpc_read_return;
    nvm_info_t *nvm_info;

    rpc_read.instance = nvm->instance;
    rpc_read.camera_id = camera_id;

    packet.payload[0].p = &rpc_read;
    packet.payload[0].size = sizeof (rpc_read);
    packet.payload[1].p = NULL;
    packet.payload[1].size = 0;
    packet.header.data_size = packet.payload[0].size;

    packet_return = RPC_CALL(nvm->rpc, RPC_GUZZI_NVM_READER_READ, &packet);
    if (!packet_return) {
        mmsdbg(DL_ERROR, "Failed to execute remote call!");
        goto exit1;
    }
    if ( packet_return->header.data_size
        < (sizeof (*rpc_read_return) + sizeof (rpc_nvm_info_t)) )
    {
        mmsdbg(
                DL_ERROR,
                "Packet too small size (min=%d, received=%d)!",
                sizeof (*rpc_read_return) + sizeof (rpc_nvm_info_t),
                packet_return->header.data_size
            );
        goto exit2;
    }
    if (  (sizeof (*rpc_read_return) + RPC_GUZZI_NVM_READER_BUFFER_SIZE_MAX)
        < packet_return->header.data_size)
    {
        mmsdbg(
                DL_ERROR,
                "Packet too big size (max=%d, received=%d)!",
                sizeof (*rpc_read_return) + RPC_GUZZI_NVM_READER_BUFFER_SIZE_MAX,
                packet_return->header.data_size
            );
        goto exit2;
    }

    rpc_read_return = packet_return->payload[0].p;
    nvm_info = rpc_nvm_info_convert_from(
            (rpc_nvm_info_t *)rpc_read_return->data
        );

    rpc_service_free_packet(
            nvm->rpc,
            packet_return->header.service,
            packet_return
        );

    return nvm_info;
exit2:
    rpc_service_free_packet(
            nvm->rpc,
            packet_return->header.service,
            packet_return
        );
exit1:
    return NULL;
}

/*
 * ****************************************************************************
 * ** Destroy *****************************************************************
 * ****************************************************************************
 */
void guzzi_nvm_reader_destroy(guzzi_nvm_reader_t *nvm)
{
    rpc_packet_t packet;
    rpc_packet_t *packet_return;
    rpc_guzzi_nvm_reader_destroy_t rpc_destroy;
    rpc_guzzi_nvm_reader_destroy_return_t *rpc_destroy_return;

    rpc_destroy.instance = nvm->instance;

    packet.payload[0].p = &rpc_destroy;
    packet.payload[0].size = sizeof (rpc_destroy);
    packet.payload[1].p = NULL;
    packet.payload[1].size = 0;
    packet.header.data_size = packet.payload[0].size;

    packet_return = RPC_CALL(nvm->rpc, RPC_GUZZI_NVM_READER_DESTROY, &packet);
    if (!packet_return) {
        mmsdbg(DL_ERROR, "Failed to execute remote call!");
        goto exit1;
    }
    if (packet_return->header.data_size != sizeof (*rpc_destroy_return)) {
        mmsdbg(DL_ERROR, "Packet size mismatch!");
        goto exit2;
    }

    rpc_destroy_return = packet_return->payload[0].p;
    if (rpc_destroy_return->instance != nvm->instance) {
        mmsdbg(DL_ERROR, "Wrong NVM Reader instance!");
        goto exit2;
    }

    rpc_service_free_packet(
            nvm->rpc,
            packet_return->header.service,
            packet_return
        );

    guzzi_nvm_reader_stub_instances[nvm->instance] = NULL;
    osal_free(nvm);

    return;
exit2:
    rpc_service_free_packet(
            nvm->rpc,
            packet_return->header.service,
            packet_return
        );
exit1:
    guzzi_nvm_reader_stub_instances[nvm->instance] = NULL;
    osal_free(nvm);
    return;
}

/*
 * ****************************************************************************
 * ** Create ******************************************************************
 * ****************************************************************************
 */
guzzi_nvm_reader_t *guzzi_nvm_reader_create(void)
{
    guzzi_nvm_reader_t *nvm;
    rpc_packet_t packet;
    rpc_packet_t *packet_return;
    rpc_guzzi_nvm_reader_create_return_t *rpc_create_return;

    nvm = osal_calloc(1, sizeof (*nvm));
    if (!nvm) {
        mmsdbg(
                DL_ERROR,
                "Failed to allocate memory for new instance: size=%d!",
                sizeof (*nvm)
            );
        goto exit1;
    }

    nvm->rpc = guzzi_nvm_reader_stub_rpc;

    packet.payload[0].p = NULL;
    packet.payload[0].size = 0;
    packet.header.data_size = 0;

    packet_return = RPC_CALL(nvm->rpc, RPC_GUZZI_NVM_READER_CREATE, &packet);
    if (!packet_return) {
        mmsdbg(DL_ERROR, "Failed to execute remote call!");
        goto exit2;
    }
    if (packet_return->header.data_size != sizeof (*rpc_create_return)) {
        mmsdbg(DL_ERROR, "Packet size mismatch!");
        goto exit3;
    }

    rpc_create_return = packet_return->payload[0].p;
    if (RPC_GUZZI_NVM_READER_INSTANCE_INVALID == rpc_create_return->instance) {
        mmsdbg(DL_ERROR, "Invalid NVM Reader instance!");
        goto exit3;
    }
    if (INSTANCES_MAX <= rpc_create_return->instance) {
        mmsdbg(
                DL_ERROR,
                "Invalid NVM Reader instance=%d, max=%d!",
                rpc_create_return->instance,
                INSTANCES_MAX
            );
        goto exit3;
    }
    if (guzzi_nvm_reader_stub_instances[rpc_create_return->instance]) {
        mmsdbg(
                DL_ERROR,
                "NVM Reader Instance %d already in use!",
                rpc_create_return->instance
            );
        goto exit3;
    }

    nvm->instance = rpc_create_return->instance;
    guzzi_nvm_reader_stub_instances[nvm->instance] = nvm;

    rpc_service_free_packet(
            nvm->rpc,
            packet_return->header.service,
            packet_return
        );

    return nvm;
exit3:
    rpc_service_free_packet(
            nvm->rpc,
            packet_return->header.service,
            packet_return
        ); 
exit2:
    osal_free(nvm);
exit1:
    return NULL;
}

/*
 * ****************************************************************************
 * ** Init ********************************************************************
 * ****************************************************************************
 */
int guzzi_nvm_reader_stub_init(rpc_t *rpc)
{
    guzzi_nvm_reader_stub_rpc = rpc;
    return 0;
}

