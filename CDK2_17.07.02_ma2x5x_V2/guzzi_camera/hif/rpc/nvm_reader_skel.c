/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <osal/osal_stdlib.h>
#include <utils/mms_debug.h>

#include <guzzi/nvm_reader.h>

#include <guzzi/rpc/rpc.h>
#include <guzzi/rpc/nvm_reader_rpc.h>
#include <guzzi/rpc/nvm_reader_rpc_helpers.h>

typedef struct {
    rpc_t *rpc;
    rpc_int32_t instance;
    guzzi_nvm_reader_t *nvm_reader;
} guzzi_nvm_reader_skel_t;

static rpc_t *guzzi_nvm_reader_skel_rpc;

mmsdbg_define_variable(
        vdl_guzzi_nvm_reader_skel,
        DL_DEFAULT,
        0,
        "vdl_guzzi_nvm_reader_skel",
        "NVM Reader skel."
    );
#define MMSDEBUGLEVEL mmsdbg_use_variable(vdl_guzzi_nvm_reader_skel)

#define INSTANCES_MAX RPC_GUZZI_NVM_READER_INSTANCES_MAX

static guzzi_nvm_reader_skel_t *guzzi_nvm_reader_skel_instances[INSTANCES_MAX] = {NULL};
/*
 * ****************************************************************************
 * ** Locals ******************************************************************
 * ****************************************************************************
 */
static int store_instance(guzzi_nvm_reader_skel_t *nvm_reader_skel)
{
    int i;
    for (i = 0; i < INSTANCES_MAX; i++) {
        if (!guzzi_nvm_reader_skel_instances[i]) {
            guzzi_nvm_reader_skel_instances[i] = nvm_reader_skel;
            return i;
        }
    }
    return -1;
}

/*
 * ****************************************************************************
 * ** Get number of cameras ***************************************************
 * ****************************************************************************
 */
int guzzi_nvm_reader_skel_get_number_of_cameras(
        rpc_t *rpc,
        rpc_packet_t *call
    )
{
    guzzi_nvm_reader_skel_t *nvm_reader_skel;
    rpc_packet_t result;
    rpc_guzzi_nvm_reader_get_number_of_cameras_t *rpc_get_number_of_cameras;
    rpc_guzzi_nvm_reader_get_number_of_cameras_return_t rpc_get_number_of_cameras_return;
    rpc_uint32_t instance;
    int number_of_cameras;

    rpc_get_number_of_cameras = call->payload[0].p;
    instance = rpc_get_number_of_cameras->instance;

    result.payload[0].p = &rpc_get_number_of_cameras_return;
    result.payload[0].size = sizeof (rpc_get_number_of_cameras_return);
    result.payload[1].p = NULL;
    result.payload[1].size = 0;
    result.header.data_size = result.payload[0].size;

    if (call->header.data_size != sizeof (*rpc_get_number_of_cameras)) {
        mmsdbg(DL_ERROR, "Packet size mismatch!");
        goto exit1;
    }
    if (INSTANCES_MAX <= instance) {
        mmsdbg(
                DL_ERROR,
                "Invalid instance=%d, max=%d!",
                instance,
                INSTANCES_MAX
            );
        goto exit1;
    }

    nvm_reader_skel = guzzi_nvm_reader_skel_instances[instance];
    if (!nvm_reader_skel) {
        mmsdbg(DL_ERROR, "Instance=%d is not active!", instance);
        goto exit1;
    }

    number_of_cameras = guzzi_nvm_reader_get_number_of_cameras(
            nvm_reader_skel->nvm_reader
        );
    rpc_get_number_of_cameras_return.instance = instance;
    rpc_get_number_of_cameras_return.number_of_cameras =
        (rpc_int32_t)number_of_cameras;
    RPC_RETURN(rpc, call, &result);

    rpc_service_free_packet(rpc, call->header.service, call);

    return 0;
exit1:
    rpc_get_number_of_cameras_return.instance = instance;
    rpc_get_number_of_cameras_return.number_of_cameras = 0;
    RPC_RETURN(rpc, call, &result);
    rpc_service_free_packet(rpc, call->header.service, call);
    return -1;
}

/*
 * ****************************************************************************
 * ** Read ********************************************************************
 * ****************************************************************************
 */
int guzzi_nvm_reader_skel_read(
        rpc_t *rpc,
        rpc_packet_t *call
    )
{
    guzzi_nvm_reader_skel_t *nvm_reader_skel;
    rpc_packet_t result;
    rpc_guzzi_nvm_reader_read_t *rpc_read;
    rpc_guzzi_nvm_reader_read_return_t rpc_read_return;
    rpc_uint32_t instance;
    rpc_int32_t camera_id;
    nvm_info_t *nvm_info;
    rpc_nvm_info_t *rpc_nvm_info;
    unsigned int nvm_info_size;

    rpc_read = call->payload[0].p;
    instance = rpc_read->instance;
    camera_id = rpc_read->camera_id;

    result.payload[0].p = &rpc_read_return;
    result.payload[0].size = sizeof (rpc_read_return);
    result.payload[1].p = NULL;
    result.payload[1].size = 0;
    result.payload[2].p = NULL;
    result.payload[2].size = 0;
    result.header.data_size = result.payload[0].size;

    if (call->header.data_size != sizeof (*rpc_read)) {
        mmsdbg(DL_ERROR, "Packet size mismatch!");
        goto exit1;
    }
    if (INSTANCES_MAX <= instance) {
        mmsdbg(
                DL_ERROR,
                "Invalid instance=%d, max=%d!",
                instance,
                INSTANCES_MAX
            );
        goto exit1;
    }

    nvm_reader_skel = guzzi_nvm_reader_skel_instances[instance];
    if (!nvm_reader_skel) {
        mmsdbg(DL_ERROR, "Instance=%d is not active!", instance);
        goto exit1;
    }

    nvm_info = guzzi_nvm_reader_read(
            nvm_reader_skel->nvm_reader,
            camera_id
        );
    if (!nvm_info) {
        mmsdbg(DL_ERROR, "Failed to read NMV Info!");
        goto exit1;
    }

    nvm_info_size = rpc_nvm_info_size_of_nvm_info(nvm_info);
    if (RPC_GUZZI_NVM_READER_BUFFER_SIZE_MAX < nvm_info_size) {
        mmsdbg(
                DL_ERROR,
                "NMV Info size too big (nvm_info_size=%d, max=%d)!",
                nvm_info_size,
                RPC_GUZZI_NVM_READER_BUFFER_SIZE_MAX
            );
        goto exit2;
    }
    rpc_nvm_info = rpc_nvm_info_convert_to(nvm_info);
    if (!rpc_nvm_info) {
        mmsdbg(DL_ERROR, "Failed to convert NMV Info!");
        goto exit2;
    }

    osal_free(nvm_info);

    rpc_read_return.instance = instance;
    rpc_read_return.camera_id = camera_id;

    result.payload[1].p = rpc_nvm_info;
    result.payload[1].size = nvm_info_size;
    result.header.data_size += result.payload[1].size;

    RPC_RETURN(rpc, call, &result);

    rpc_service_free_packet(rpc, call->header.service, call);

    osal_free(rpc_nvm_info);

    return 0;
exit2:
    osal_free(nvm_info);
exit1:
    rpc_read_return.instance = instance;
    rpc_read_return.camera_id = camera_id;
    RPC_RETURN(rpc, call, &result);
    rpc_service_free_packet(rpc, call->header.service, call);
    return -1;
}

/*
 * ****************************************************************************
 * ** Destroy *****************************************************************
 * ****************************************************************************
 */
int guzzi_nvm_reader_skel_destroy(
        rpc_t *rpc,
        rpc_packet_t *call
    )
{
    guzzi_nvm_reader_skel_t *nvm_reader_skel;
    rpc_int32_t instance;
    rpc_packet_t result;
    rpc_guzzi_nvm_reader_destroy_t *rpc_destroy;
    rpc_guzzi_nvm_reader_destroy_return_t rpc_destroy_return;

    rpc_destroy = call->payload[0].p;
    instance = rpc_destroy->instance;

    result.payload[0].p = &rpc_destroy_return;
    result.payload[0].size = sizeof (rpc_destroy_return);
    result.payload[1].p = NULL;
    result.payload[1].size = 0;
    result.header.data_size = result.payload[0].size;

    if (call->header.data_size!= sizeof (*rpc_destroy)) {
        mmsdbg(DL_ERROR, "Packet size mismatch!");
        goto exit1;
    }

    if (INSTANCES_MAX <= instance) {
        mmsdbg(
                DL_ERROR,
                "Invalid instance=%d, max=%d!",
                instance,
                INSTANCES_MAX
            );
        goto exit1;
    }

    nvm_reader_skel = guzzi_nvm_reader_skel_instances[instance];
    if (!nvm_reader_skel) {
        mmsdbg(DL_ERROR, "Instance=%d is not active!", instance);
        goto exit1;
    }
    guzzi_nvm_reader_skel_instances[instance] = NULL;

    guzzi_nvm_reader_destroy(nvm_reader_skel->nvm_reader);
    osal_free(nvm_reader_skel);

    rpc_destroy_return.instance = instance;
    RPC_RETURN(rpc, call, &result);

    rpc_service_free_packet(rpc, call->header.service, call);

    return 0;
exit1:
    rpc_destroy_return.instance = instance;
    RPC_RETURN(rpc, call, &result);
    rpc_service_free_packet(rpc, call->header.service, call);
    return -1;
}

/*
 * ****************************************************************************
 * ** Create ******************************************************************
 * ****************************************************************************
 */
int guzzi_nvm_reader_skel_create(
        rpc_t *rpc,
        rpc_packet_t *call
    )
{
    guzzi_nvm_reader_skel_t *nvm_reader_skel;
    rpc_packet_t result;
    rpc_guzzi_nvm_reader_create_t *rpc_create;
    rpc_guzzi_nvm_reader_create_return_t rpc_create_return;
    rpc_int32_t instance;

    rpc_create = call->payload[0].p;

    result.payload[0].p = &rpc_create_return;
    result.payload[0].size = sizeof (rpc_create_return);
    result.payload[1].p = NULL;
    result.payload[1].size = 0;
    result.header.data_size = result.payload[0].size;

    if (call->header.data_size != sizeof (*rpc_create)) {
        mmsdbg(DL_ERROR, "Packet size mismatch!");
        goto exit1;
    }

    nvm_reader_skel = osal_calloc(1, sizeof (*nvm_reader_skel));
    if (!nvm_reader_skel) {
        mmsdbg(DL_ERROR, "Failed to allocate new NVM Reader skel instance!");
        goto exit1;
    }
    nvm_reader_skel->rpc = rpc;

    nvm_reader_skel->nvm_reader = guzzi_nvm_reader_create();
    if (!nvm_reader_skel->nvm_reader) {
        mmsdbg(DL_ERROR, "Failed to create new NVM Reader instance!");
        goto exit2;
    }

    instance = store_instance(nvm_reader_skel);
    if (instance < 0) {
        mmsdbg(DL_ERROR, "Failed store new NVM Reader instance!");
        goto exit3;
    }

    rpc_create_return.instance = instance;
    RPC_RETURN(rpc, call, &result);

    rpc_service_free_packet(rpc, call->header.service, call);

    return 0;
exit3:
    guzzi_nvm_reader_destroy(nvm_reader_skel->nvm_reader);
exit2:
    osal_free(nvm_reader_skel);
exit1:
    rpc_create_return.instance = RPC_GUZZI_NVM_READER_INSTANCE_INVALID;
    RPC_RETURN(rpc, call, &result);
    rpc_service_free_packet(rpc, call->header.service, call);
    return -1;
}

/*
 * ****************************************************************************
 * ** Init ********************************************************************
 * ****************************************************************************
 */
int guzzi_nvm_reader_skel_init(rpc_t *rpc)
{
    guzzi_nvm_reader_skel_rpc = rpc;
    return 0;
}

