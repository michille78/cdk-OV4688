/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <osal/osal_stdlib.h>
#include <osal/osal_string.h>
#include <utils/mms_debug.h>
#include <guzzi/camera3_capture_result/camera3_capture_result.h>
#include <guzzi/rpc/rpc.h>
#include <guzzi/rpc/camera3_capture_result_rpc.h>

static rpc_t *guzzi_camera3_capture_result_stub_rpc;
static int guzzi_camera3_capture_result_stub_serv_n;

mmsdbg_define_variable(
        vdl_guzzi_camera3_capture_result_stub,
        DL_DEFAULT,
        0,
        "vdl_guzzi_camera3_capture_result_stub",
        "Guzzi Camera3 Capture Result Stub."
    );
#define MMSDEBUGLEVEL mmsdbg_use_variable(vdl_guzzi_camera3_capture_result_stub)

/*
 * ****************************************************************************
 * ** Capture Result **********************************************************
 * ****************************************************************************
 */
void guzzi_camera3_capture_result(
        int camera_id,
        unsigned int stream_id,
        unsigned int frame_number,
        void *data,
        unsigned int data_size
    )
{
    rpc_t *rpc;
    rpc_packet_t packet;
    rpc_packet_t *packet_return;
    rpc_guzzi_camera3_capture_result_t rpc_guzzi_camera3_capture_result;

    rpc = guzzi_camera3_capture_result_stub_rpc;

    rpc_guzzi_camera3_capture_result.camera_id = (rpc_int32_t)camera_id;
    rpc_guzzi_camera3_capture_result.stream_id = (rpc_uint32_t)stream_id;
    rpc_guzzi_camera3_capture_result.frame_number = (rpc_uint32_t)frame_number;
    rpc_guzzi_camera3_capture_result.data_size = (rpc_uint32_t)data_size;

    packet.payload[0].p = &rpc_guzzi_camera3_capture_result;
    packet.payload[0].size = sizeof (rpc_guzzi_camera3_capture_result);
    packet.payload[1].p = data;
    packet.payload[1].size = data_size;
    packet.payload[2].p = NULL;
    packet.payload[2].size = 0;
    packet.header.data_size =
          packet.payload[0].size
        + packet.payload[1].size;

    packet_return = rpc_call(
            rpc,
            RPC_TRANSPORT_DEFAULT,
            guzzi_camera3_capture_result_stub_serv_n,
            RPC_GUZZI_CAMERA3_CAPTURE_RESULT,
            &packet
        );
    if (!packet_return) {
        mmsdbg(DL_ERROR, "Failed to execute remote call!");
        goto exit1;
    }
    if (packet_return->header.data_size != 0) {
        mmsdbg(DL_ERROR, "Packet size mismatch!");
        goto exit2;
    }

    rpc_service_free_packet(
            rpc,
            packet_return->header.service,
            packet_return
        );

    return;
exit2:
    rpc_service_free_packet(
            rpc,
            packet_return->header.service,
            packet_return
        );
exit1:
    return;
}

/*
 * ****************************************************************************
 * ** Init ********************************************************************
 * ****************************************************************************
 */
int guzzi_camera3_capture_result_stub_init(rpc_t *rpc, int serv_n)
{
    guzzi_camera3_capture_result_stub_rpc = rpc;
    guzzi_camera3_capture_result_stub_serv_n = serv_n;
    return 0;
}

