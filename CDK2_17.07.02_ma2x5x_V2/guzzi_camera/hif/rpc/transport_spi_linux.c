/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include <osal/osal_stdlib.h>
#include <osal/osal_mutex.h>
#include <utils/mms_debug.h>

#include <guzzi/rpc/transport.h>

#define container_of(PTR,TYPE,FIELD) \
    (TYPE *)((char *)(PTR) - (char *)&(((TYPE *)0)->FIELD))

#define transport_to_ts(TRANSPORT) container_of((TRANSPORT), ts_t, transport)

#define SPI_DEV "/dev/myriad1" 

typedef struct {
    transport_t transport;
    int exit;
    int dev;
    osal_mutex *write_lock;
} ts_t;

mmsdbg_define_variable(
        vdl_transport_spi_linux,
        DL_DEFAULT,
        0,
        "vdl_transport_spi_linux",
        "Transport using SPI."
    );
#define MMSDEBUGLEVEL mmsdbg_use_variable(vdl_transport_spi_linux)

static int transport_spi_linux_read(
        transport_t *transport,
        void *p,
        unsigned int size
    )
{
    ts_t *ts;
    int bytes, pos;

    ts = transport_to_ts(transport);

    pos = 0;
    while (pos < size) {
        bytes = read(ts->dev, (char *)p + pos, size - pos);
        if (ts->exit) {
            return 0;
        }
        if (bytes < 0) {
            if (EINTR == errno) {
                continue;
            }
            mmsdbg(DL_ERROR, "SPI read error!");
            return -1;
        } else if (bytes == 0) {
            mmsdbg(DL_ERROR, "Error during read!");
            return -1;
        }
        pos += bytes;
    }

    return size;
}

static int transport_spi_linux_write(
        transport_t *transport,
        void *p,
        unsigned int size
    )
{
    ts_t *ts;
    int bytes;
    int pos;

    ts = transport_to_ts(transport);

    pos = 0;
    while (pos < size) {
        bytes = write(ts->dev, (char *)p + pos, size - pos);
        if (bytes < 0) {
            if (EINTR == errno) {
                continue;
            }
            mmsdbg(DL_ERROR, "Socket write error!");
            return -1;
        } else if (bytes == 0) {
            mmsdbg(DL_ERROR, "Error during write!");
            return -1;
        }
        pos += bytes;
    }

    return size;
}

static void transport_spi_linux_write_lock(transport_t *transport)
{
    ts_t *ts;
    ts = transport_to_ts(transport);
    osal_mutex_lock(ts->write_lock);
}

static void transport_spi_linux_write_unlock(transport_t *transport)
{
    ts_t *ts;
    ts = transport_to_ts(transport);
    osal_mutex_unlock(ts->write_lock);
}

static void transport_spi_linux_exit(transport_t *transport)
{
    ts_t *ts;
    ts = transport_to_ts(transport);
    ts->exit = 1;
}

/*
 * ****************************************************************************
 * ** Create/Destroy **********************************************************
 * ****************************************************************************
 */
void transport_spi_linux_destroy(transport_t *transport)
{
    ts_t *ts;
    ts = transport_to_ts(transport);
    close(ts->dev);
    osal_mutex_destroy(ts->write_lock);
    osal_free(ts);
}

transport_t * transport_spi_linux_create(void)
{
    ts_t *ts;

    ts = osal_calloc(1, sizeof (*ts));
    if (!ts) {
        mmsdbg(
                DL_ERROR,
                "Failed to allocate memory for new instance: size=%d!",
                sizeof (*ts)
            );
        goto exit1;
    }

    ts->transport.read = transport_spi_linux_read;
    ts->transport.write = transport_spi_linux_write;
    ts->transport.write_lock = transport_spi_linux_write_lock;
    ts->transport.write_unlock = transport_spi_linux_write_unlock;
    ts->transport.exit = transport_spi_linux_exit;

    ts->write_lock = osal_mutex_create();
    if (!ts->write_lock) {
        mmsdbg(DL_ERROR, "Failed to create send mutex!");
        goto exit2;
    }

    ts->dev = open(SPI_DEV, O_RDWR);
    if (ts->dev < 0) {
        mmsdbg(DL_ERROR, "Failed to open \"%s\" !", SPI_DEV);
        goto exit3;
    }

    return &ts->transport;
exit3:
    osal_mutex_destroy(ts->write_lock);
exit2:
    osal_free(ts);
exit1:
    return NULL;
}

