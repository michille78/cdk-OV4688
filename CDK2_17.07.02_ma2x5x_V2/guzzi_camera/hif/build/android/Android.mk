# Copyright (C) 2011 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

LOCAL_PATH := $(abspath $(call my-dir)/../../)

# Possible transport types:
#  - TCP_SOCKET
#  - MYRIAD2_SPI
MMS_HIF_TRANSPORT_TYPE ?= MYRIAD2_SPI

################################################################################
##### Guzzi Camera3 RPC Lib
################################################################################
include $(CLEAR_VARS)

LOCAL_ALLOW_UNDEFINED_SYMBOLS := true

LOCAL_CFLAGS += \
    -fno-short-enums \

LOCAL_CFLAGS += \
    -Wno-unused-parameter \
    -Wno-sign-compare \
    -Wno-pointer-arith \

LOCAL_CFLAGS += \
    -DQEMU_HARDWARE \
    -D___ANDROID___ \
    -D____ANDROID____ \
    -D__MMS_DEBUG__ \
    -DRPC_SIDE_HOST \
    -DDEFAULT_ALLOC_TIMEOUT_MS=4000 \

LOCAL_SHARED_LIBRARIES += \
    libmms_osal_v1 \

LOCAL_C_INCLUDES += \
    hardware/mms/osal/include \
    $(LOCAL_PATH)/camera3/c/include \
    $(LOCAL_PATH)/nvm_reader/include \
    $(LOCAL_PATH)/rpc/include \

LOCAL_SRC_FILES += \
    rpc/rpc.c \
    rpc/namespace.c \
    rpc/camera3_stub.c \
    rpc/nvm_reader_stub.c \
    rpc/camera3_capture_result_skel.c \
    rpc/camera3_capture_result_callback_wrapper.c \

ifeq ($(MMS_HIF_TRANSPORT_TYPE),TCP_SOCKET)
LOCAL_SRC_FILES += \
    rpc/module_gc3_cap_res_socket.c \
    rpc/transport_socket.c
endif
ifeq ($(MMS_HIF_TRANSPORT_TYPE),MYRIAD2_SPI)
LOCAL_SRC_FILES += \
    rpc/module_gc3_cap_res_spi.c \
    rpc/transport_spi_linux.c
endif

LOCAL_MODULE := libguzzi_camera3_rpc

include $(BUILD_STATIC_LIBRARY)

################################################################################
##### Guzzi Camera3 Metadata Lib
################################################################################
include $(CLEAR_VARS)

LOCAL_ALLOW_UNDEFINED_SYMBOLS := true

LOCAL_CFLAGS += \
    -fno-short-enums \

LOCAL_CFLAGS += \
    -Wno-unused-parameter \
    -Wno-sign-compare \
    -Wno-pointer-arith \

LOCAL_CFLAGS += \
    -DQEMU_HARDWARE \
    -D___ANDROID___ \
    -D____ANDROID____ \
    -D__MMS_DEBUG__ \
    -DDEFAULT_ALLOC_TIMEOUT_MS=4000 \

LOCAL_SHARED_LIBRARIES += \
    libcamera_metadata \
    libmms_osal_v1 \

LOCAL_C_INCLUDES += \
    system/media/camera/include \
    hardware/mms/osal/include \
    $(LOCAL_PATH)/camera3/c/include \
    $(LOCAL_PATH)/nvm_reader/include \

LOCAL_SRC_FILES += \
    camera3/c/auto_gen/camera3/helpers.c \
    camera3/c/auto_gen/camera3/convert_hal3.c \
    camera3/c/camera3/convert_guzzi2hal_types.c \
    camera3/c/camera3/convert_hal2guzzi_types.c \
    camera3/c/camera3/convert_hal3.c \

LOCAL_MODULE := libguzzi_camera3_metadata

include $(BUILD_STATIC_LIBRARY)

################################################################################
##### Guzzi Camera3 Shared Lib
################################################################################
include $(CLEAR_VARS)

LOCAL_ALLOW_UNDEFINED_SYMBOLS := true

LOCAL_CFLAGS += \
    -fno-short-enums \

LOCAL_CFLAGS += \
    -Wno-unused-parameter \
    -Wno-sign-compare \
    -Wno-pointer-arith \

LOCAL_CFLAGS += \
    -DQEMU_HARDWARE \
    -D___ANDROID___ \
    -D____ANDROID____ \
    -D__MMS_DEBUG__ \
    -DDEFAULT_ALLOC_TIMEOUT_MS=4000 \

LOCAL_STATIC_LIBRARIES += \

LOCAL_WHOLE_STATIC_LIBRARIES += \
    libguzzi_camera3_rpc \
    libguzzi_camera3_metadata \

LOCAL_SHARED_LIBRARIES += \
    libcamera_metadata \
    libmms_osal_v1 \
    liblog

LOCAL_C_INCLUDES += \
    system/media/camera/include \
    hardware/mms/osal/include \
    $(LOCAL_PATH)/camera3/c/include \

LOCAL_SRC_FILES += \

LOCAL_MODULE := librpc_guzzi_camera3

include $(BUILD_SHARED_LIBRARY)

################################################################################
##### Test App: Simple RPC Client
################################################################################
include $(CLEAR_VARS)

LOCAL_CFLAGS += \
    -fno-short-enums \

LOCAL_CFLAGS += \
    -Wno-unused-parameter \
    -Wno-sign-compare \
    -Wno-pointer-arith \

LOCAL_CFLAGS += \
    -DQEMU_HARDWARE \
    -D___ANDROID___ \
    -D____ANDROID____ \
    -D__MMS_DEBUG__ \
    -DDEFAULT_ALLOC_TIMEOUT_MS=4000 \

LOCAL_STATIC_LIBRARIES += \

LOCAL_SHARED_LIBRARIES += \
    liblog \
    libmms_osal_v1 \
    librpc_guzzi_camera3 \

LOCAL_C_INCLUDES += \
    hardware/mms/osal/include \
    $(LOCAL_PATH)/rpc/include \
    $(LOCAL_PATH)/camera3/c/include \
    $(LOCAL_PATH)/nvm_reader/include \

LOCAL_SRC_FILES += \
    test/rpc_client_pc/cmdline.c \
    test/rpc_client_pc/rpc_client_pc.c \
    test/rpc_client_pc/main.c \
    test/rpc_client_pc/disp_dummy.c \

LOCAL_MODULE := test_rpc_client_pc

include $(BUILD_EXECUTABLE)

################################################################################
##### Test App: Transport over TCP Socket
################################################################################
include $(CLEAR_VARS)

LOCAL_CFLAGS += \
    -fno-short-enums \

LOCAL_CFLAGS += \
    -Wno-unused-parameter \
    -Wno-sign-compare \
    -Wno-pointer-arith \
    -Wformat=0 \

LOCAL_CFLAGS += \
    -DQEMU_HARDWARE \
    -D___ANDROID___ \
    -D____ANDROID____ \
    -D__MMS_DEBUG__ \
    -DDEFAULT_ALLOC_TIMEOUT_MS=4000 \

LOCAL_STATIC_LIBRARIES += \
    libguzzi_camera3_rpc \

LOCAL_SHARED_LIBRARIES += \
    liblog \
    libmms_osal_v1 \

LOCAL_C_INCLUDES += \
    hardware/mms/osal/include \
    $(LOCAL_PATH)/rpc/include \

LOCAL_SRC_FILES += \

ifeq ($(MMS_HIF_TRANSPORT_TYPE),TCP_SOCKET)
LOCAL_SRC_FILES += \
    test/transport_socket/main.c
endif
ifeq ($(MMS_HIF_TRANSPORT_TYPE),MYRIAD2_SPI)
LOCAL_SRC_FILES += \
    test/transport_spi_linux/main.c
endif

LOCAL_MODULE := test_transport

include $(BUILD_EXECUTABLE)

