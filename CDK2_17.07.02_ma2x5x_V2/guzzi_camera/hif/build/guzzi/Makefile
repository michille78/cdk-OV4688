export TOPDIR := $(shell readlink -e $(CURDIR))
DIRSRCTOP := $(abspath $(TOPDIR)/../../)

include $(TOPDIR)/scripts/Makefile.defs
-include $(MAKEINCCFG)
-include $(MAKEINCMAP)

TARGET ?= all
OBJECTS = $(objects)
PROJECTS = $(foreach obj,$(OBJECTS),$($(obj)))
DEFAULTRULES = build install mrproper clean distclean mrproper
DEFAULTRULES += show_all show_wrong table_all table_wrong
DEFAULTRULES += maps help configs files
CHECKCFG = $(if $(filter 2,$(words $(strip $(wildcard $(MAKEINCCFG) $(MAKEINCMAP))))),,NO_CONFIG)
CHECKTARGET = $(if $(MAKE_TARGET_OS),,NO_TARGET)

ifneq ($(MAKEINCDEP),)
$(MAKEINCDEP): $(MAKEINCCFG) $(MAKEINCMAP)
	$(Q)$(MAKE) -C $(TOPDIR) -f $(MAKEDEPENDS) depends
endif

-include $(MAKEINCDEP)

.PHONY: $(DEFAULTRULES)

all: build

build: target_build $(OBJECTS)

clean: target_clean $(OBJECTS)
	$(Q)$(RM) $(MAKEINCDEP)

show_all: target_show_all $(OBJECTS)

show_wrong: target_show_wrong $(OBJECTS)

table_all table_wrong: $(CHECKCFG)
	$(Q)$(MAKE) $(patsubst table_%,show_%,$@) V= > $(DIRBLD)/Makefile.variables
	$(Q)$(MAKE) -f $(MAKESHOWINC) table INCFILE=$(DIRBLD)/Makefile.variables
	$(Q)$(RM) $(DIRBLD)/Makefile.variables

distclean:
	$(Q)$(MAKE) -f $(MAKEMAIN) mrproper
	$(Q)$(MAKE) -f $(MAKECONFIG) clean

install: target_install $(if $(filter all build,$(MAKECMDGOALS)),build,$(OBJECTS))
ifeq ($(MAKELEVEL),0)
	$(Q)$(MAKE) -C $(TOPDIR) install
else
	$(Q)true
endif

mrproper:
	$(Q)$(call quiet,MRPROP,$(call abs2loc,$(DIRBLD)))
	$(Q)rm -fr $(DIRBLD)

maps: $(DIRBLD)
	$(Q)$(MAKE) -f $(MAKEMAPS) maps
	
configs:
	$(Q)$(MAKE) -f $(MAKECONFIG) configs

files:
	$(Q)$(MAKE) -f $(MAKEUTILS) files

configs/%: FORCE
	$(Q)$(MAKE) -f $(MAKEMAIN) mrproper
	$(Q)$(MAKE) -f $(MAKECONFIG) clean
	$(Q)$(MAKE) -f $(MAKECONFIG) configure CFGDIR=$@

target_%: $(CHECKCFG)
	$(eval TARGET=$(patsubst target_%,%,$@))


#ifneq ($(OBJECTS),)
#$(OBJECTS): $(CHECKCFG)
#	$(Q)$(if $($@),$(foreach p,$($@),$(MAKE) -C $(DIRBLD) -f $(MAKEBUILD) $(TARGET) DIRSRC=$p;),echo "Missing mapping for object '$@'")
#endif

ifneq ($(PROJECTS),)
$(PROJECTS): FORCE $(CHECKCFG) $(CHECKTARGET) $(DIRBLD)
	$(Q)if [ ! -e $(DIRBLD)/$@ ]; then mkdir -p "$(DIRBLD)/$@"; fi
	$(Q)$(MAKE) -C $(DIRBLD) -f $(MAKEBUILD) $(TARGET) DIRSRC=$@ DIRSRCTOP=$(DIRSRCTOP)
endif

NO_CONFIG:
	$(Q)echo -e "\033[1;5;31mWARNING:\033[0m Configuration files missed\n"
	$(Q)$(MAKE) -f $(MAKECONFIG) configs
	$(Q)false

NO_TARGET:
	$(Q)echo -e "\033[1;5;31mWARNING:\033[0m Environment MAKE_TARGET_OS does not exist."
	$(Q)echo "   Available is LINUX | WINDOWS | ANDROID | DSPBIOS | RTEMS";
	$(Q)false

help:
	$(Q)echo "Generic targets:"
	$(Q)echo "  all             - Build all projects listed in Makefile.maps"
	$(Q)echo "  clean           - Remove all objects and dependences"
	$(Q)echo "  check           - Show all incorrect include files, file by file"
	$(Q)echo "  mrproper        - Remove all generated files in folder build/"
	$(Q)echo "  distclean       - Same as mrproper and remove configuration files"
	$(Q)echo "  intall          - Install all files"
	$(Q)echo "  configs         - Show available configurations"
	$(Q)echo "  configs/<prj>   - Perform project configure"
	$(Q)echo "  maps            - Generate new Makefile.maps, includes all available objects"
	$(Q)echo "  show_all        - Show all used includes"
	$(Q)echo "  show_wrong      - Show all wrong used includes"
	$(Q)echo "  table_all       - Show all used includes in VCS format"
	$(Q)echo "  table_wrong     - Show all wrong used includes in VCS format"
	$(Q)echo ""
	$(Q)echo "Variables:"
	$(Q)echo "  V=1             - Verbose mode"
	$(Q)echo "  filter=file.h   - file filters, available for show_all/wrong & table_all/wrong"
	$(Q)echo ""

