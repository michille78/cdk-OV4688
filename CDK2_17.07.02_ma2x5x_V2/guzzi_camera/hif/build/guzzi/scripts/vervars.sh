SCRIPTDIR=$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd) # $TOPDIR/hif/build/guzzi/scripts
PRJDIR=$(dirname $(dirname $(dirname $SCRIPTDIR))) # $TOPDIR/hif
TOPDIR=$(dirname $PRJDIR)

function git_version {
    (cd $PRJDIR && git describe --long --dirty --always 2>/dev/null)
}

