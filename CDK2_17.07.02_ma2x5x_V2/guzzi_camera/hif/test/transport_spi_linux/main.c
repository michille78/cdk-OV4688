/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <stdio.h>
#include <osal/osal_stdlib.h>
#include <osal/osal_time.h>
#include <osal/osal_thread.h>
#include <utils/mms_debug.h>

#include <guzzi/rpc/transport.h>
#include <guzzi/rpc/transport_spi_linux.h>

mmsdbg_define_variable(
        vdl_test_transport_socket,
        DL_DEFAULT,
        0,
        "vdl_test_transport_socket",
        "Test TCP socket transport."
    );
#define MMSDEBUGLEVEL mmsdbg_use_variable(vdl_test_transport_socket)

transport_t *transport;
char msg_from_master[] = "Test Transmit socket: From Master!";
char buffer[1024];

static void *thread_loop(void *arg)
{
    char *str;
    uint32 size;

    for (;;) {
        transport_read(transport, &size, 4);
        transport_read(transport, buffer, size);

        mmsdbg(DL_PRINT, "Master RX: %s", buffer);

        size = strlen(msg_from_master) + 1;
        transport_write(transport, &size, 4);
        transport_write(transport, msg_from_master, size);
    }

    return NULL;
}

static int thread_done_cb(void *arg)
{
    return 0;
}

int main(int argc, char **argv)
{
    struct osal_thread *thread;
    int err;

    err = osal_init();
    if (err) {
        mmsdbg(DL_ERROR, "OSAL init failed!");
        goto exit1;
    }

    transport = transport_spi_linux_create();

    uint32 size = strlen(msg_from_master) + 1;
    transport_write(transport, &size, 4);
    transport_write(transport, msg_from_master, size);

    thread = osal_thread_create(
            "Transport Thread",
            NULL,
            thread_loop,
            thread_done_cb,
            0,
            4096
        );

    getchar();

    osal_thread_destroy(thread);

    transport_spi_linux_destroy(transport);

    err = osal_exit();
    if (err) {
        mmsdbg(DL_ERROR, "OSAL exit failed!");
        goto exit1;
    }

exit1:
    return 0;
}

