/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <utils/mms_debug.h>

void android_control_client(void);

int clp_init(void *base_register, void *end_register);
int clp_parser(int argc, char **argv);

mmsdbg_define_variable(
        vdl_test_android_control_client,
        DL_DEFAULT,
        0,
        "vdl_test_android_control_client",
        "Guzzi Android control client application."
    );
#define MMSDEBUGLEVEL mmsdbg_use_variable(vdl_test_android_control_client)

extern int __start_mmsdbgvarlist;
extern int __stop_mmsdbgvarlist;

int main(int argc, char **argv)
{
    int err;

    clp_init(&__start_mmsdbgvarlist, &__stop_mmsdbgvarlist);
    err = clp_parser(argc, argv);
    if (err) {
        goto exit0;
    }

    android_control_client();
    mmsdbg(DL_PRINT, "cam_test() done.");

exit0:
    return 0;
}

