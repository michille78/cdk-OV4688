#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>

#include "disp.h"

#define max(x, y) (((x) > (y)) ? (x) : (y))
#define min(x, y) (((x) < (y)) ? (x) : (y))

struct disp {
    char *display_name;
    unsigned int width;
    unsigned int height;
    disp_image_format_t format;

    char *rgb32;

    Display *display;
    Visual *visual;
    Window window;
    XImage *ximage;
} x11_disp_prv_t;

static void yuv2rgb(unsigned char *rgb, int y, int u, int v)
{
    int r, g, b;
    r = y            + 1.402f*u;
    g = y - 0.344f*v - 0.714f*u;
    b = y + 1.772f*v           ;
    rgb[3] = 255;
    rgb[2] = min(max(r, 0), 255);
    rgb[1] = min(max(g, 0), 255);
    rgb[0] = min(max(b, 0), 255);
}

static void conv_uyvy_to_rgb24(
        void *rgb24,
        void *uyvy,
        unsigned int width,
        unsigned int height
    )
{
    unsigned int l, c;
    int y1, y2, u, v;
    unsigned int line_inc_rgb, line_inc_yuv;
    unsigned char *rgb, *yuv;

    rgb = rgb24; yuv = uyvy;
    line_inc_rgb = 0; line_inc_yuv = 0;

    for (l = 0; l < height; l++) {
        for (c = 0; c < width/2; c++) {
            y1 = yuv[1]; y2 = yuv[3];
            u  = yuv[0]; v  = yuv[2];

            u -= 128; v -= 128;

            yuv2rgb(rgb, y1, u, v);
            rgb += 4;
            yuv2rgb(rgb, y2, u, v);
            rgb += 4;

            yuv += 4;
        }
        rgb += line_inc_rgb;
        yuv += line_inc_yuv;
    }
}

static void conv_nv12_to_rgb24(
        void *rgb24,
        void *nv12,
        unsigned int width,
        unsigned int height
    )
{
    unsigned int l, c;
    int y11, y12, y21, y22, u, v;
    unsigned int line_inc_rgb, line_inc_yuv;
    unsigned char *rgb1, *rgb2, *yuv1, *yuv2, *uv;

    rgb1 = rgb24;
    rgb2 = rgb1 + 4*width;
    yuv1 = nv12;
    yuv2 = yuv1 + width;
    uv   = yuv1 + width*height;
    line_inc_rgb = 4*width; line_inc_yuv = width;

    for (l = 0; l < height/2; l++) {
        for (c = 0; c < width/2; c++) {
            y11 = yuv1[1]; y12 = yuv1[2];
            y21 = yuv2[1]; y22 = yuv2[2];
            u   = uv[0]  ; v   = uv[1];

            u -= 128; v -= 128;

            yuv2rgb(rgb1, y11, u, v);
            rgb1 += 4;
            yuv2rgb(rgb1, y12, u, v);
            rgb1 += 4;
            yuv2rgb(rgb2, y21, u, v);
            rgb2 += 4;
            yuv2rgb(rgb2, y22, u, v);
            rgb2 += 4;

            yuv1 += 2; yuv2 += 2; uv += 2;
        }
        rgb1 += line_inc_rgb;
        rgb2 += line_inc_rgb;
        yuv1 += line_inc_yuv;
        yuv2 += line_inc_yuv;
    }
}

void disp_show(disp_t *disp, void *img)
{
    switch (disp->format) {
        case DISP_IMAGE_FORMAT_UYVY:
            conv_uyvy_to_rgb24(
                    disp->ximage->data,
                    img,
                    disp->ximage->width,
                    disp->ximage->height
                );
            break;
        case DISP_IMAGE_FORMAT_NV12:
            conv_nv12_to_rgb24(
                    disp->ximage->data,
                    img,
                    disp->ximage->width,
                    disp->ximage->height
                );
            break;
        case DISP_IMAGE_FORMAT_RGBA8888:
            memcpy(
                    disp->ximage->data,
                    img,
                    4 * disp->width * disp->height
                );
            break;
        default:
            break;
    }

    XPutImage(
            disp->display,               /* display  */
            disp->window,                /* d        */
            DefaultGC(disp->display, 0), /* gc       */
            disp->ximage,                /* image    */
            0,                           /* src_x    */
            0,                           /* src_y    */
            0,                           /* dest_x   */
            0,                           /* dest_y   */
            disp->width,                 /* width    */
            disp->height                 /* height   */
        );
}

void disp_destroy(disp_t *disp)
{
    //free(disp->rgb32);
    XDestroyImage(disp->ximage);
    XDestroyWindow(disp->display, disp->window);
    XCloseDisplay(disp->display);
    free(disp);
}

disp_t * disp_create(disp_create_t *create)
{
    disp_t *disp;

    disp = malloc(sizeof(*disp));

    disp->display_name = create->display_name;
    disp->width = create->width;
    disp->height = create->height;
    disp->format = create->format;

    disp->rgb32 = malloc(
            4 * disp->width * disp->height
        );

    disp->display = XOpenDisplay(disp->display_name);
    disp->visual = DefaultVisual(disp->display, 0);
    disp->window = XCreateSimpleWindow(
            disp->display,                /* display      */
            RootWindow(disp->display, 0), /* parent       */
            0,                            /* x            */
            0,                            /* y            */
            disp->width,                  /* width        */
            disp->height,                 /* height       */
            1,                            /* border_width */
            0,                            /* border       */
            0                             /* background   */
        );
    disp->ximage = XCreateImage(
            disp->display, /* display          */
            disp->visual,  /* visual           */
            24,            /* depth            */
            ZPixmap,       /* format           */
            0,             /* offset           */
            disp->rgb32,   /* data             */
            disp->width,   /* width            */
            disp->height,  /* height           */
            32,            /* bitmap_pad       */
            0              /* bytes_per_line   */
        );
    XSelectInput(
            disp->display,
            disp->window,
            ButtonPressMask | ExposureMask
        );
    XMapWindow(disp->display, disp->window);

    XEvent ev;
    XNextEvent(disp->display, &ev);

    return disp;
}

