/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <osal/osal_stdio.h>

#include <utils/mms_debug.h>

#include <utils/linux/cmdline_parser.h>
#include <utils/linux/cmdline_debug.h>

mmsdbg_define_variable(
        vdl_cmdline,
        DL_DEFAULT,
        0,
        "cmdline",
        "Example command line parser"
    );
#define MMSDEBUGLEVEL vdl_cmdline

int clp_resolution = -1;

#define CLP_FILENAME "test_app"
#define CLP_ENVNAME "MMSDBG_TEST"

static int clphnd_unknown(int val, char *opt, char *arg, int index)
{
    osal_printf("Unknown option: %s\n", opt);

    return CMDLINE_ERROR_UNKNOW;
}

static int clphnd_camera_resolution(int val, char *opt, char *arg, int index)
{
    clp_resolution = atoi(arg);
    printf("Camera resolution: %d\n", clp_resolution);

    return CMDLINE_NOERROR;
}

int clp_init(void *base_register, void *end_register)
{
    int err = 0;
    int count = 1000;

    cmdline_dbg_initialize(base_register, end_register);

    cmdline_help_line(CMDLINE_FLAG_HELP_LEVEL9, "Common:");

    cmdline_set_unknow(clphnd_unknown);

    cmdline_add_option(CMDLINE_FLAG_OPT_OPTIONAL|
        CMDLINE_FLAG_VAL_MANDATORY|CMDLINE_FLAG_HELP_LEVEL0,
        'r', "resolution", "vl", count++, NULL, clphnd_camera_resolution,
        " -r | --resolution        Resolution 0 - low 1 - high\n", NULL);

    cmdline_dbg_register_helps(true, true, CLP_FILENAME);
    cmdline_help_line(CMDLINE_FLAG_HELP_LEVEL9, "\nDebugs:");
    cmdline_dbg_register_debug_layers(false, true);

    return err;
}

int clp_parser(int argc, char **argv)
{
    int err = 0;

    err = cmdline_parser(argc, argv);
    err |= cmdline_parser_env(CLP_ENVNAME);
    if (err) {
        if (err == CMDLINE_ERROR_PRINT_HELP) {
            cmdline_dbg_flags |= CMDLINE_DBG_FLAG_HELP;
        } else {
            goto fail;
        }
    }

    if (cmdline_dbg_flags & CMDLINE_DBG_FLAG_HELP) {
        cmdline_help_usage_print(CLP_FILENAME);
        cmdline_help_print(CMDLINE_FLAG_HELP_MASK);
        err = -1;
        goto fail;
    }

    if (cmdline_dbg_flags & CMDLINE_DBG_FLAG_HELP_DEBUG_VAR) {
        cmdline_dbg_print_debug_variables();
        err = -1;
        goto fail;
    }

fail:
    return err;
}

