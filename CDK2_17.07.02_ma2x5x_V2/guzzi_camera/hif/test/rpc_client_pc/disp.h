#ifndef _DISP_H
#define _DISP_H

#ifdef __cplusplus
extern "C" {
#endif

typedef struct disp disp_t;

typedef enum {
    DISP_IMAGE_FORMAT_UYVY,
    DISP_IMAGE_FORMAT_NV12,
    DISP_IMAGE_FORMAT_RGBA8888
} disp_image_format_t;

typedef struct {
    char *display_name;
    unsigned int width;
    unsigned int height;
    disp_image_format_t format;
} disp_create_t;

void disp_show(disp_t *disp, void *img);
void disp_destroy(disp_t *disp);
disp_t * disp_create(disp_create_t *create);

#ifdef __cplusplus
}
#endif

#endif /* _DISP_H */

