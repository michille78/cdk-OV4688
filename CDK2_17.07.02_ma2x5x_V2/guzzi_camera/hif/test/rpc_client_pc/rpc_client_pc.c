/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <osal/osal_string.h>
#include <osal/osal_stdtypes.h>
#include <osal/osal_stdlib.h>
#include <osal/osal_mutex.h>
#include <utils/mms_debug.h>

#include <guzzi/camera3/metadata.h>
#include <guzzi/camera3/camera3.h>
#include <guzzi/camera3_capture_result/camera3_capture_result.h>
#include <guzzi/rpc/rpc_module.h>
#include <guzzi/rpc/camera3_capture_result_callback_wrapper.h>
#include <guzzi/nvm_reader.h>

#include "disp.h"

typedef struct {
    disp_t *disp;
    guzzi_camera3_t *gc3;
} app_prv_t;

mmsdbg_define_variable(
        vdl_guzzi_android_control,
        DL_DEFAULT,
        0,
        "vdl_guzzi_android_control",
        "Guzzi android control."
    );
#define MMSDEBUGLEVEL mmsdbg_use_variable(vdl_guzzi_android_control)

static app_prv_t cam_test_prv;

static guzzi_camera3_metadata_static_t test_guzzi_camera3_metadata_static;
static guzzi_camera3_info_t test_guzzi_camera3_info = {
    .facing = GUZZI_CAMERA3_FACING_BACK,
    .orientation = 0,
    .metadata_static = &test_guzzi_camera3_metadata_static
};

struct {
    guzzi_camera3_controls_request_id_identity_t id;
    guzzi_camera3_controls_sensor_exposure_time_identity_t et;
    guzzi_camera3_metadata_t end;
} dummy_settings = {
    .id = {
        .identity = {{{
            .index = GUZZI_CAMERA3_INDEX_CONTROLS_REQUEST_ID,
            .size = sizeof (guzzi_camera3_controls_request_id_identity_t)
        }}},
        .v = {
            .v = 1
        }
    },
    .et = {
        .identity = {{{
            .index = GUZZI_CAMERA3_INDEX_CONTROLS_SENSOR_EXPOSURE_TIME,
            .size = sizeof (guzzi_camera3_controls_sensor_exposure_time_identity_t)
        }}},
        .v = {
            .v = 0x500000004
        }
    },
    .end = {
        .identity = {{{
            .index = GUZZI_CAMERA3_INDEX_METADATA_END_MARKER,
            .size = sizeof (guzzi_camera3_metadata_t)
        }}}
    }
};



static void capture_result(
        void *prv,
        int camera_id,
        unsigned int stream_id,
        unsigned int frame_number,
        void *data,
        unsigned int data_size
    )
{
    app_prv_t *app_prv;

    app_prv = prv;

    if (GUZZI_CAMERA3_STREAM_ID_METADATA == stream_id) {
        mmsdbg(DL_PRINT, "Metadata: camera_id=%d, stream_id=%d", camera_id, stream_id);
    } else {
        mmsdbg(DL_PRINT, "Image: camera_id=%d, stream_id=%d", camera_id, stream_id);
        disp_show(app_prv->disp, data);
    }
}



static void guzzi_camera3_calback_notify(
        guzzi_camera3_t *gc3,
        void *prv,
        guzzi_camera3_event_t event,
        int num,
        void *ptr
    )
{
    guzzi_camera3_event_shutter_t *shutter;

    switch (event) {
        case GUZZI_CAMERA3_EVENT_SHUTTER:
            shutter = ptr;
            mmsdbg(
                    DL_ERROR,
                    "Guzzi Camera3 Event: FN=%u, TIME=%llu",
                    shutter->frame_number,
                    shutter->timestamp
                );
            break;
        case GUZZI_CAMERA3_EVENT_ERROR:
            mmsdbg(DL_ERROR, "Guzzi Camera3 Event: ERROR!");
            break;
        default:
            mmsdbg(DL_ERROR, "Unknown Guzzi Camera3 Event!");
    }
}



static char dummy_settings_buffer[90*1024];

void android_control_client(void)
{
    app_prv_t *app_prv;
    guzzi_nvm_reader_t *nvm_reader;
    nvm_info_t *nvm_info;
    int nvm_reder_number_of_cameras;
    guzzi_camera3_stream_configuration_t stream_config;
    guzzi_camera3_capture_request_t capture_request;
    int number_of_cameras;
    disp_create_t disp_params;
    int i;
    int err;

    app_prv = &cam_test_prv;



    err = rpc_module_init();
    if (err) {
        mmsdbg(DL_ERROR, "Failed to init RPC module!");
        goto exit1;
    }



    nvm_reader = guzzi_nvm_reader_create();
    nvm_reder_number_of_cameras = guzzi_nvm_reader_get_number_of_cameras(
            nvm_reader
        );
    nvm_info = guzzi_nvm_reader_read(nvm_reader, 1);
    guzzi_nvm_reader_destroy(nvm_reader);
    mmsdbg(
            DL_PRINT,
            "Guzzi NVM Reader: number_of_cameras=%d",
            nvm_reder_number_of_cameras
        );
    mmsdbg(
            DL_PRINT,
            "Guzzi NVM Reader: module_assembling_id=%d",
            nvm_info->version.module_assembling_id
        );
    osal_free(nvm_info);



    number_of_cameras = guzzi_camera3_get_number_of_cameras();
    mmsdbg(DL_PRINT, "Guzzi Camera3: number_of_cameras=%d", number_of_cameras);

    err = guzzi_camera3_get_info(1, &test_guzzi_camera3_info);
    mmsdbg(DL_PRINT, "Guzzi Camera3: get info err=%d", err);



    app_prv->gc3 = guzzi_camera3_create(
            GUZZI_CAMERA3_FACING_BACK,
            guzzi_camera3_calback_notify,
            app_prv
        );
    if (!app_prv->gc3) {
        mmsdbg(DL_ERROR, "Failed to create Guzzi Camera3!");
        goto exit2;
    }

    err = guzzi_camera3_capture_result_callback_set(
            GUZZI_CAMERA3_FACING_BACK,
            app_prv,
            capture_result
        );
    if (err) {
        mmsdbg(DL_ERROR, "Failed to set Capture Result Callback!");
        goto exit3;
    }



    disp_params.display_name = NULL;
    disp_params.width = 856/2;
    disp_params.height = 480/2;
    disp_params.format = DISP_IMAGE_FORMAT_RGBA8888;
    app_prv->disp = disp_create(&disp_params);




    stream_config.num_streams = 1;
    stream_config.streams[0].id     = 1;
    stream_config.streams[0].type   = GUZZI_CAMERA3_STREAM_TYPE_OUTPUT;
    stream_config.streams[0].format = GUZZI_CAMERA3_IMAGE_FORMAT_RAW10;
    stream_config.streams[0].width  = 856/2;
    stream_config.streams[0].height = 480/2;
    guzzi_camera3_config_streams(app_prv->gc3, &stream_config);

    osal_memcpy(dummy_settings_buffer, &dummy_settings, sizeof (dummy_settings));
    capture_request.frame_number = 1;
    capture_request.settings = (void *)dummy_settings_buffer;
    capture_request.streams_id[0] = 1;
    capture_request.streams_id[1] = 0;
    for (i = 0; i < 100; i++) {
        err = guzzi_camera3_capture_request(app_prv->gc3, &capture_request);
        if (err) {
            mmsdbg(DL_ERROR, "Failed to submit capture request!");
            break;
        }
    }

    //getchar();

    guzzi_camera3_flush(app_prv->gc3);

    stream_config.num_streams = 0;
    guzzi_camera3_config_streams(app_prv->gc3, &stream_config);



    disp_destroy(app_prv->disp);



exit3:
    guzzi_camera3_destroy(app_prv->gc3);
exit2:
    rpc_module_deinit();
exit1:
    return;
}

