#!/usr/bin/python

from guzzi_camera3_metadata import *
from guzzi_camera3 import *
import pygame
import threading

class myGuzziCamera3(guzzi_camera3):
    def callback_capture_result(self, stream_id, frame_number, data, data_size):
        if not self.running_lock.acquire(False):
            return

        if stream_id == 1:
            b = ctypes.string_at(data, data_size)
            sb = pygame.image.fromstring(b, (self.w, self.h), "RGBA")

            self.screen_lock()
            screen = pygame.display.get_surface()
            pygame.transform.scale(sb, screen.get_size(), screen)
            pygame.display.update()
            self.screen_unlock()

            self.req_sem.release()

        self.running_lock.release()

    def req_thread(self):
        while True:
            self.req_sem.acquire()
            if self.req_thread_exit: return
            self.capture_request(self.cap_req)

    def start(self, w, h):
        self.w = w
        self.h = h

        self.running_lock = threading.Lock()
        self.scrn_lock = threading.Lock()

        self.req_sem = threading.Semaphore(0)
        self.req_thread_exit = 0
        threading.Thread(None, self.req_thread).start()

        self.c = control_metadata()
        stream_config = guzzi_camera3_stream_configuration_t()
        stream_config.num_streams = 1
        stream_config.streams[0].id     = 1
        stream_config.streams[0].type   = GUZZI_CAMERA3_STREAM_TYPE_OUTPUT
        stream_config.streams[0].format = GUZZI_CAMERA3_IMAGE_FORMAT_RAW10
        stream_config.streams[0].width  = self.w
        stream_config.streams[0].height = self.h
        self.config_streams(stream_config)

        req_id = guzzi_camera3_controls_request_id_identity_t();
        req_id.identity.index = GUZZI_CAMERA3_INDEX_CONTROLS_REQUEST_ID
        req_id.identity.size = ctypes.sizeof(req_id)
        req_id.v.v = 1
        self.c.append(req_id)
        
        exp_time = guzzi_camera3_controls_sensor_exposure_time_identity_t();
        exp_time.identity.index = GUZZI_CAMERA3_INDEX_CONTROLS_SENSOR_EXPOSURE_TIME
        exp_time.identity.size = ctypes.sizeof(exp_time)
        exp_time.v.v = 0x500000004
        self.c.append(exp_time)
        
        end = guzzi_camera3_metadata_t()
        end.identity.index = GUZZI_CAMERA3_INDEX_METADATA_END_MARKER
        end.identity.size = 0
        self.c.append(end)

        self.cap_req = guzzi_camera3_capture_request_t()
        self.cap_req.frame_number = 1;
        self.cap_req.settings = self.c.addr()
        self.cap_req.streams_id[0] = 1;
        self.cap_req.streams_id[1] = 0;
        for i in range(3):
            self.capture_request(self.cap_req)

    def stop(self):
        self.running_lock.acquire()

        self.req_thread_exit = 1
        self.req_sem.release()

        self.flush()
        stream_config = guzzi_camera3_stream_configuration_t()
        stream_config.num_streams = 0
        self.config_streams(stream_config)
        self.running_lock.release()

    def screen_lock(self):
        self.scrn_lock.acquire()

    def screen_unlock(self):
        self.scrn_lock.release()

w = 856/2
h = 480/2

pygame.init()
pygame.display.set_mode((w, h), pygame.RESIZABLE)

gc3 = myGuzziCamera3(GUZZI_CAMERA3_FACING_BACK)
gc3.start(w, h)

loop = 1
while loop:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            loop = 0
            break
        elif event.type == pygame.VIDEORESIZE:
            gc3.screen_lock()
            pygame.display.set_mode(event.size, pygame.RESIZABLE)
            gc3.screen_unlock()

gc3.stop()
gc3.destroy()

pygame.quit()

