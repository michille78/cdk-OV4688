#!/usr/bin/python


from guzzi_camera3_metadata import *
from guzzi_camera3 import *
import pic2ascii

p2a = pic2ascii.PIC_TO_ASCII()

c = control_metadata()

class myGuzziCamera3(guzzi_camera3):
    def callback_capture_result(self, stream_id, frame_number, data, data_size):
        if stream_id == 1:
            b = ctypes.string_at(data, data_size)
            w, h = 856/2, 480/2
            ppln = w
            print "\033[2J"
            print "Stream: %5d; FrameN: %5d, DataSize: %5d" % (
                    stream_id,
                    frame_number,
                    data_size
                )
            print p2a.convert(b, w, h, ppln, "RGB24")

gc3 = myGuzziCamera3(GUZZI_CAMERA3_FACING_BACK)

stream_config = guzzi_camera3_stream_configuration_t()
stream_config.num_streams = 1
stream_config.streams[0].id     = 1;
stream_config.streams[0].type   = GUZZI_CAMERA3_STREAM_TYPE_OUTPUT;
stream_config.streams[0].format = GUZZI_CAMERA3_IMAGE_FORMAT_RAW10;
stream_config.streams[0].width  = 856/2;
stream_config.streams[0].height = 480/2;
gc3.config_streams(stream_config)

req_id = guzzi_camera3_controls_request_id_identity_t();
req_id.identity.index = GUZZI_CAMERA3_INDEX_CONTROLS_REQUEST_ID
req_id.identity.size = ctypes.sizeof(req_id)
req_id.v.v = 1
c.append(req_id)

exp_time = guzzi_camera3_controls_sensor_exposure_time_identity_t();
exp_time.identity.index = GUZZI_CAMERA3_INDEX_CONTROLS_SENSOR_EXPOSURE_TIME
exp_time.identity.size = ctypes.sizeof(exp_time)
exp_time.v.v = 0x500000004
c.append(exp_time)

end = guzzi_camera3_metadata_t()
end.identity.index = GUZZI_CAMERA3_INDEX_METADATA_END_MARKER
end.identity.size = 0
c.append(end)

cap_req = guzzi_camera3_capture_request_t()
cap_req.frame_number = 1;
cap_req.settings = c.addr()
cap_req.streams_id[0] = 1;
cap_req.streams_id[1] = 0;
for i in range(100):
    gc3.capture_request(cap_req)

gc3.flush()

stream_config.num_streams = 0
gc3.config_streams(stream_config)

gc3.destroy()

