#!/usr/bin/python

import ctypes
from guzzi_camera3_metadata import *

guzzi_camera3_calback_notify_t = ctypes.PYFUNCTYPE(
        None,               # return void
        ctypes.c_void_p,    # gc3
        ctypes.c_void_p,	# prv
        ctypes.c_int,       # event
        ctypes.c_int,       # num
        ctypes.c_void_p     # ptr
    )

guzzi_camera3_capture_result_callback_t = ctypes.CFUNCTYPE(
        None,               # return void
        ctypes.c_void_p,    # prv
        ctypes.c_int,       # camera_id
        ctypes.c_uint,      # stream_id
        ctypes.c_uint,      # frame_number
        ctypes.c_void_p,    # data
        ctypes.c_uint       # data_size
    )

class LIBGUZZI_CAMERA3():
    def __init__(self, lib_name='./librpc_guzzi_camera3.so'):
        self.lib = ctypes.CDLL(lib_name)

        self.rpc_module_init = self.lib.rpc_module_init
        self.rpc_module_init.restype = ctypes.c_int
        self.rpc_module_init.argtypes = []

        self.rpc_module_deinit = self.lib.rpc_module_deinit
        self.rpc_module_deinit.restype = None
        self.rpc_module_deinit.argtypes = []

        self.get_number_of_cameras = self.lib.guzzi_camera3_get_number_of_cameras
        self.get_number_of_cameras.restype = ctypes.c_int
        self.get_number_of_cameras.argtypes = []

        self.get_info = self.lib.guzzi_camera3_get_info
        self.get_info.restype = ctypes.c_int
        self.get_info.argtypes = [
               ctypes.c_int,       # camera_id
               ctypes.c_void_p     # gc3_info
           ]

        self.get_id = self.lib.guzzi_camera3_get_id
        self.get_id.restype = ctypes.c_int
        self.get_id.argtypes = [
               ctypes.c_void_p     # gc3
           ]

        self.request_defaults = self.lib.guzzi_camera3_request_defaults
        self.request_defaults.restype = ctypes.c_int
        self.request_defaults.argtypes = [
               ctypes.c_void_p,    # gc3
               ctypes.c_void_p,    # metadata
               ctypes.c_int        # type
           ]

        self.config_streams = self.lib.guzzi_camera3_config_streams
        self.config_streams.restype = ctypes.c_int
        self.config_streams.argtypes = [
               ctypes.c_void_p,    # gc3
               ctypes.c_void_p     # stream_configuration
           ]

        self.capture_request = self.lib.guzzi_camera3_capture_request
        self.capture_request.restype = ctypes.c_int
        self.capture_request.argtypes = [
               ctypes.c_void_p,    # gc3
               ctypes.c_void_p     # capture_request
           ]

        self.flush = self.lib.guzzi_camera3_flush
        self.flush.restype = None
        self.flush.argtypes = [
               ctypes.c_void_p     # gc3
           ]

        self.destroy = self.lib.guzzi_camera3_destroy
        self.destroy.restype = None
        self.destroy.argtypes = [
               ctypes.c_void_p     # gc3
           ]

        self.create = self.lib.guzzi_camera3_create
        self.create.restype = ctypes.c_void_p
        self.create.argtypes = [
               ctypes.c_int,                   # camera_id
               guzzi_camera3_calback_notify_t, # callback_notify
               ctypes.py_object                # client_prv
           ]

        self.capture_result_callback_set = self.lib.guzzi_camera3_capture_result_callback_set
        self.capture_result_callback_set.restype = ctypes.c_int
        self.capture_result_callback_set.argtypes = [
               ctypes.c_int,                            # camera_id
               ctypes.py_object,                        # prv
               guzzi_camera3_capture_result_callback_t  # func
           ]

        self.capture_result_callback_clear = self.lib.guzzi_camera3_capture_result_callback_clear
        self.capture_result_callback_clear.restype = ctypes.c_int
        self.capture_result_callback_clear.argtypes = [
               ctypes.c_int        # camera_id
           ]

        self.rpc_module_init()

    def __del__(self):
        self.rpc_module_deinit()

GUZZI_CAMERA3_STREAMS_MAX = 8

GUZZI_CAMERA3_FACING_BACK = 0
GUZZI_CAMERA3_FACING_FRONT = 1

class guzzi_camera3_info_t(ctypes.Structure):
    _fields_ = [
        ("facing", ctypes.c_int),
        ("orientation", ctypes.c_int),
        ("metadata_static", ctypes.POINTER(guzzi_camera3_metadata_static_t))
    ]

GUZZI_CAMERA3_EVENT_SHUTTER = 0
GUZZI_CAMERA3_EVENT_ERROR = 1

class guzzi_camera3_event_shutter_t(ctypes.Structure):
    _fields_ = [
        ("frame_number", ctypes.c_uint),
        ("timestamp", ctypes.c_int64)
    ]

GUZZI_CAMERA3_TEMPLATE_PREVIEW = 0
GUZZI_CAMERA3_TEMPLATE_STILL_CAPTURE = 1
GUZZI_CAMERA3_TEMPLATE_VIDEO_RECORD = 2
GUZZI_CAMERA3_TEMPLATE_VIDEO_SNAPSHOT = 3
GUZZI_CAMERA3_TEMPLATE_ZERO_SHUTTER_LAG = 4

GUZZI_CAMERA3_STREAM_TYPE_OUTPUT = 0
GUZZI_CAMERA3_STREAM_TYPE_INPUT = 1
GUZZI_CAMERA3_STREAM_TYPE_BIDIR = 2

GUZZI_CAMERA3_IMAGE_FORMAT_RAW10 = 0
GUZZI_CAMERA3_IMAGE_FORMAT_UYVY = 1
GUZZI_CAMERA3_IMAGE_FORMAT_NV12 = 2

class guzzi_camera3_stream_t(ctypes.Structure):
    _fields_ = [
        ("id", ctypes.c_uint),
        ("type", ctypes.c_uint),
        ("format", ctypes.c_uint),
        ("width", ctypes.c_uint),
        ("height", ctypes.c_uint)
    ]

class guzzi_camera3_stream_configuration_t(ctypes.Structure):
    _fields_ = [
        ("num_streams", ctypes.c_uint),
        ("streams", guzzi_camera3_stream_t * GUZZI_CAMERA3_STREAMS_MAX)
    ]

class guzzi_camera3_capture_request_t(ctypes.Structure):
    _fields_ = [
        ("frame_number", ctypes.c_uint),
        ("settings", ctypes.c_void_p),
        ("streams_id", ctypes.c_uint * GUZZI_CAMERA3_STREAMS_MAX)
    ]

class control_metadata():
    def __init__(self):
        self.max_size = 30*1024
        container = ctypes.c_uint8 * self.max_size
        self.container = container()
        self.pos = 0

    def append(self, metadata):
        dst = ctypes.addressof(self.container) + self.pos
        src = ctypes.addressof(metadata)
        length = ctypes.sizeof(metadata)
        if self.max_size < self.pos + length:
            return
        ctypes.memmove(dst, src, length)
        self.pos += length

    def addr(self):
        return ctypes.addressof(self.container)

libguzzi_camera3 = LIBGUZZI_CAMERA3()

def guzzi_camera3_get_number_of_cameras():
    return libguzzi_camera3.get_number_of_cameras()

def guzzi_camera3_get_info(camera_id):
    info = guzzi_camera3_info_t()
    static = guzzi_camera3_metadata_static_t()
    info.metadata_static = ctypes.pointer(static)
    err = libguzzi_camera3.get_info(camera_id, ctypes.byref(info))
    if err:
        return None
    return info

class guzzi_camera3():
    def __init__(self, camera_id, lib=libguzzi_camera3):
        self.camera_id = camera_id
        self.lib = lib
        self.gc3 = self.lib.create(
                self.camera_id,
                guzzi_camera3_calback_notify_t(self._callback_notify),
                ctypes.py_object(self)
            )
        lib.capture_result_callback_set( # TODO: handle error
                self.camera_id,
                ctypes.py_object(self),
                guzzi_camera3_capture_result_callback_t(self._callback_capture_result)
            )

    def __del__(self):
        self.lib.capture_result_callback_clear(self.camera_id) # TODO: handle error

    @staticmethod
    def _callback_notify(gc3, prv, event, num, ptr):
        self = ctypes.cast(prv, ctypes.py_object).value
        self.callback_notify(event, num, ptr)

    @staticmethod
    def _callback_capture_result(prv, camera_id, stream_id, frame_number, data, data_size):
        self = ctypes.cast(prv, ctypes.py_object).value
        self.callback_capture_result(stream_id, frame_number, data, data_size)

    def callback_notify(self, event, num, ptr):
        pass

    def callback_capture_result(self, stream_id, frame_number, data, data_size):
        pass

    def get_id(self):
        return self.camera_id

    def request_defaults(self, t):
        metadata = guzzi_camera3_metadata_controls_t()
        err = self.lib.request_defaults(self.gc3, ctypes.byref(metadata), t)
        if err:
            return None
        return metadata

    def config_streams(self, stremas):
        return self.lib.config_streams(self.gc3, ctypes.byref(stremas))

    def capture_request(self, request):
        return self.lib.capture_request(self.gc3, ctypes.byref(request))

    def flush(self):
        self.lib.flush(self.gc3)

    def destroy(self):
        self.lib.destroy(self.gc3)

if __name__ == '__main__':
    from guzzi_camera3_metadata import *

    c = control_metadata()

    num_of_cameras = guzzi_camera3_get_number_of_cameras()
    print "Number of cameras: %d" % (num_of_cameras)

    info = guzzi_camera3_get_info(GUZZI_CAMERA3_FACING_BACK)

    class myGuzziCamera3(guzzi_camera3):
        def callback_capture_result(self, stream_id, frame_number, data, data_size):
            print "Strem: %5d; FrameNumber: %5d; DataSize: %5d" % (
                    stream_id,
                    frame_number,
                    data_size
                )

    gc3 = myGuzziCamera3(GUZZI_CAMERA3_FACING_BACK)

    stream_config = guzzi_camera3_stream_configuration_t()
    stream_config.num_streams = 1
    stream_config.streams[0].id     = 1;
    stream_config.streams[0].type   = GUZZI_CAMERA3_STREAM_TYPE_OUTPUT;
    stream_config.streams[0].format = GUZZI_CAMERA3_IMAGE_FORMAT_RAW10;
    stream_config.streams[0].width  = 856/2;
    stream_config.streams[0].height = 480/2;
    gc3.config_streams(stream_config)

    req_id = guzzi_camera3_controls_request_id_identity_t();
    req_id.identity.index = GUZZI_CAMERA3_INDEX_CONTROLS_REQUEST_ID
    req_id.identity.size = ctypes.sizeof(req_id)
    req_id.v.v = 1
    c.append(req_id)

    exp_time = guzzi_camera3_controls_sensor_exposure_time_identity_t();
    exp_time.identity.index = GUZZI_CAMERA3_INDEX_CONTROLS_SENSOR_EXPOSURE_TIME
    exp_time.identity.size = ctypes.sizeof(exp_time)
    exp_time.v.v = 0x500000004
    c.append(exp_time)

    end = guzzi_camera3_metadata_t()
    end.identity.index = GUZZI_CAMERA3_INDEX_METADATA_END_MARKER
    end.identity.size = 0
    c.append(end)

    cap_req = guzzi_camera3_capture_request_t()
    cap_req.frame_number = 1;
    cap_req.settings = c.addr()
    cap_req.streams_id[0] = 1;
    cap_req.streams_id[1] = 0;
    for i in range(100):
        gc3.capture_request(cap_req)

    gc3.flush()

    stream_config.num_streams = 0
    gc3.config_streams(stream_config)

    gc3.destroy()

