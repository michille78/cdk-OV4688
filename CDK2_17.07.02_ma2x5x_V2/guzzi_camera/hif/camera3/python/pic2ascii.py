import string

class PIC_TO_ASCII():
    TRANS_TABLE = ' .oO@#'

    def __init__(self, width=64, disp_pix_w=8, disp_pix_h=14, trans_table=TRANS_TABLE):
        self.pw = width
        self.pix_w = disp_pix_w
        self.pix_h = disp_pix_h
        self.trans_table = trans_table

        self.t = string.maketrans(
                ''.join([chr(i) for i in range(256)]),
                ''.join([self.trans_table[int(i*len(self.trans_table)/256)] for i in range(256)]))

    def __norm_255(self, min, max, x):
        return (x - min)*255/(max-min)

    def convert(self, b, w, h, ppln, fmt):
        ph = (self.pw*h*self.pix_w)/(w*self.pix_h)
        if 'NV12' == fmt:
            w_inc = w/self.pw
            h_inc = ppln*(h/ph)

            lns = [b[i*h_inc:i*h_inc+w] for i in range(ph)]
            ss = [ln[0:w:w_inc] for ln in lns]
        elif 'UYVY' == fmt:
            w_inc = w/self.pw
            h_inc = 2*ppln*(h/ph)

            lns = [b[i*h_inc:i*h_inc+2*w] for i in range(ph)]
            ss = [ln[1:2*w:2*w_inc] for ln in lns]
        elif 'RGB24' == fmt:
            w_inc = w/self.pw
            h_inc = 4*ppln*(h/ph)

            lns = [b[i*h_inc:i*h_inc+4*w] for i in range(ph)]
            ss = [map(sum, zip( # R,G,B, R,G,B .. R,G,B
                                map(ord, ln[0:4*w:4*w_inc]), # R,R,R..R
                                map(ord, ln[1:4*w:4*w_inc]), # G,G,G..G
                                map(ord, ln[2:4*w:4*w_inc])  # B,B,B..B
                            )) for ln in lns]

        y_min = min([min(s) for s in ss])
        y_max = max([max(s) for s in ss])

        return ''.join(
                [''.join([chr(self.__norm_255(y_min, y_max, c)) for c in s])
                   .translate(self.t)+'\n'
                   for s in ss]
            )

