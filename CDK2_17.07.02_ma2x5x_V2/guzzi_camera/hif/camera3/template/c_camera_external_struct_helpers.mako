/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file helpers.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

<%namespace name="c" module="helpers"/>\
\
#include <osal/osal_stdio.h>

#include <guzzi/camera3/metadata_index.h>
#include <guzzi/camera3/metadata_struct.h>
#include <guzzi/camera_external/struct.h>
#include <guzzi/camera_external/struct_helpers.h>

#ifndef STR
#define STR(X) #X
#endif

#ifndef offset_of
#define offset_of(type, field) ((char *)&(((type *)0)->field) - (char *)0)
#endif

typedef struct configurator configurator_t;

<%def name="make_func_proto_by_metadata(mdata)">\
    %for e in c.all_entries_by_kind(mdata, 'controls'):
int camera_external_convert_${c.struct_instance_name(e)}(
            configurator_t *c,
            void *plugin_prv,
            void *config
        );
    %endfor
</%def>\
\
${make_func_proto_by_metadata(metadata)}\

/* Guzzi specific */
${make_func_proto_by_metadata(c.guzzi_metadata())}\

<%def name="make_helper_struct_by_metadata(mdata)">\
    %for e in c.all_entries_by_kind(mdata, 'controls'):
    {
        .index_name = STR(${c.index_name(e)}),
        .struct_name = STR(${c.struct_name(e)}_t),
        .offset = offset_of(camera_external_struct_t, ${c.struct_instance_name(e)}),
        .size = sizeof (${c.struct_name(e)}_t),
        .index = ${c.index_name(e)},
        .convert = camera_external_convert_${c.struct_instance_name(e)}
    },
    %endfor
</%def>\
\
static camera_external_struct_helper_t camera_external_struct_helper[] = {
${make_helper_struct_by_metadata(metadata)}\

    /* Guzzi specific */
${make_helper_struct_by_metadata(c.guzzi_metadata())}\
};

camera_external_struct_helper_t *camera_external_index_to_helper(guzzi_camera3_metadata_index_t index)
{
<% offset = 0 %>\
    switch (index) {
%for e in c.all_entries_by_kind(metadata, 'controls'):
        case ${c.index_name(e)}:
            return camera_external_struct_helper + ${offset};
<% offset += 1 %>\
%endfor

        /* Guzzi specific */
%for e in c.all_entries_by_kind(c.guzzi_metadata(), 'controls'):
        case ${c.index_name(e)}:
            return camera_external_struct_helper + ${offset};
<% offset += 1 %>\
%endfor

        default:
            return NULL;
    }
}

camera_external_struct_helper_t *camera_external_next_helper(camera_external_struct_helper_t *helper)
{
    if (!helper) {
        return camera_external_struct_helper;
    }

    /* TODO: assert(camera_external_struct_helper <= helper); */
    if ((helper + 1) < (camera_external_struct_helper + sizeof (camera_external_struct_helper) / sizeof (camera_external_struct_helper_t))) {
        return helper + 1;
    } else {
        return NULL;
    }
}

