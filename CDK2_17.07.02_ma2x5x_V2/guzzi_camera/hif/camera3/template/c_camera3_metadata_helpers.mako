/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file helpers.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

<%namespace name="c" module="helpers"/>\
\
<%def name="make_helper_by_entry(e)">\
    {
        .index_name = STR(${c.index_name(e)}),
        .struct_name = STR(${c.struct_name(e)}_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_${e.kind}_valid_t, ${c.struct_instance_name(e)}),
        .struct_offset = offset_of(guzzi_camera3_metadata_${e.kind}_struct_t, ${c.struct_instance_name(e)}),
        .v_offset = offset_of(guzzi_camera3_metadata_${e.kind}_t, valid.${c.struct_instance_name(e)}),
        .s_offset = offset_of(guzzi_camera3_metadata_${e.kind}_t, s.${c.struct_instance_name(e)}),
        .size = sizeof (${c.struct_name(e)}_t),
        .ident_size = sizeof (${c.struct_name(e)}_identity_t),
    %if c.e_container(e) == "array": 
        .dim_size_1 = ${c.array_dim_max_size_name(e)}_1,
    %else:
        .dim_size_1 = 0,
    %endif
        .index = ${c.index_name(e)},
    },
</%def>\
\
<%def name="make_helper_by_kind(k_name)">\
static guzzi_camera3_metadata_helper_t guzzi_camera3_metadata_${k_name}_helper[] = {
    %for e in c.all_entries_by_kind(metadata, k_name):
${make_helper_by_entry(e)}\
    %endfor
};
</%def>\
\
<%def name="make_helper()">\
    %for k in c.all_kind_names(metadata):
${make_helper_by_kind(k)}
    %endfor
</%def>\
\
###############################################################################
\
<%def name="make_index_to_helper_by_kind(k_name)">\
guzzi_camera3_metadata_helper_t *guzzi_camera3_metadata_${k_name}_index_to_helper(guzzi_camera3_metadata_index_t index)
{
    switch (index) {
%for e,n in zip(c.all_entries_by_kind(metadata, k_name), range(len(c.all_entries_by_kind(metadata, k_name)))):
        case ${c.index_name(e)}:
            return guzzi_camera3_metadata_${k_name}_helper + ${n};
%endfor
        default:
            return (void *)0;
    }
}
</%def>\
\
<%def name="make_index_to_helper()">\
    %for k in c.all_kind_names(metadata):
${make_index_to_helper_by_kind(k)}
    %endfor
</%def>\
\
###############################################################################
\
<%def name="make_next_helper_by_kind(k_name)">\
guzzi_camera3_metadata_helper_t *guzzi_camera3_metadata_${k_name}_next_helper(guzzi_camera3_metadata_helper_t *helper)
{
    if (!helper) {
        return guzzi_camera3_metadata_${k_name}_helper;
    }

    /* TODO: assert(guzzi_camera3_metadata_${k_name}_helper <= helper); */
    if ((helper + 1) < (guzzi_camera3_metadata_${k_name}_helper + sizeof (guzzi_camera3_metadata_${k_name}_helper) / sizeof (guzzi_camera3_metadata_helper_t))) {
        return helper + 1;
    } else {
        return (void *)0;
    }
}
</%def>\
\
<%def name="make_next_helper()">\
    %for k in c.all_kind_names(metadata):
${make_next_helper_by_kind(k)}
    %endfor
</%def>\
\
###############################################################################
\
#include <guzzi/camera3/metadata_index.h>
#include <guzzi/camera3/metadata.h>
#include <guzzi/camera3/helpers.h>

#ifndef STR
#define STR(X) #X
#endif

#ifndef offset_of
#define offset_of(type, field) ((char *)&(((type *)0)->field) - (char *)0)
#endif

${make_helper()}\
${make_index_to_helper()}\
${make_next_helper()}\
