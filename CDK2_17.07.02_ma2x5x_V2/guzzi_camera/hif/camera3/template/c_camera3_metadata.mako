/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file metadata.h
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

<%namespace name="c" module="helpers"/>\
\
<%def name="make_identity_by_kind(k_name)">\
    %for e in c.all_entries_by_kind(metadata, k_name):
typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    ${c.struct_name(e)}_t v;
} ${c.struct_name(e)}_identity_t;

    %endfor
</%def>\
\
<%def name="make_valid_group_by_kind(k_name)">\
typedef struct {
    %for e in c.all_entries_by_kind(metadata, k_name):
    guzzi_camera3_int32_t ${c.struct_instance_name(e)};
    %endfor
} guzzi_camera3_metadata_${k_name}_valid_t;
</%def>\
\
<%def name="make_struct_group_by_kind(k_name)">\
typedef struct {
    %for e in c.all_entries_by_kind(metadata, k_name):
    ${c.struct_name(e)}_identity_t ${c.struct_instance_name(e)};
    %endfor
    guzzi_camera3_metadata_t metadata_end_marker;
} guzzi_camera3_metadata_${k_name}_struct_t;
</%def>\
\
/*
 * ****************************************************************************
 * * Control Metadata Identity ************************************************
 * ****************************************************************************
 */
${make_identity_by_kind('controls')}\
/*
 * ****************************************************************************
 * * Dynamic Metadata Identity ************************************************
 * ****************************************************************************
 */
${make_identity_by_kind('dynamic')}\
/*
 * ****************************************************************************
 * * Static Metadata Identity *************************************************
 * ****************************************************************************
 */
${make_identity_by_kind('static')}\
/*
 * ****************************************************************************
 * * Control Metadata Valid ***************************************************
 * ****************************************************************************
 */
${make_valid_group_by_kind('controls')}
/*
 * ****************************************************************************
 * * Dynamic Metadata Valid ***************************************************
 * ****************************************************************************
 */
${make_valid_group_by_kind('dynamic')}
/*
 * ****************************************************************************
 * * Static Metadata Valid ****************************************************
 * ****************************************************************************
 */
${make_valid_group_by_kind('static')}
/*
 * ****************************************************************************
 * * Control Metadata Group ***************************************************
 * ****************************************************************************
 */
${make_struct_group_by_kind('controls')}
/*
 * ****************************************************************************
 * * Dynamic Metadata Group ***************************************************
 * ****************************************************************************
 */
${make_struct_group_by_kind('dynamic')}
/*
 * ****************************************************************************
 * * Static Metadata Group ****************************************************
 * ****************************************************************************
 */
${make_struct_group_by_kind('static')}
/*
 * ****************************************************************************
 * * Valid + struct ***********************************************************
 * ****************************************************************************
 */
typedef struct {
    guzzi_camera3_metadata_controls_valid_t valid;
    guzzi_camera3_metadata_controls_struct_t s;
} guzzi_camera3_metadata_controls_t;

typedef struct {
    guzzi_camera3_metadata_dynamic_valid_t valid;
    guzzi_camera3_metadata_dynamic_struct_t s;
} guzzi_camera3_metadata_dynamic_t;

typedef struct {
    guzzi_camera3_metadata_static_valid_t valid;
    guzzi_camera3_metadata_static_struct_t s;
} guzzi_camera3_metadata_static_t;

