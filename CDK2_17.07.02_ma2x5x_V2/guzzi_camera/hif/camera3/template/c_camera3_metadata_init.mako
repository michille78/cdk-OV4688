/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file init.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

<%namespace name="c" module="helpers"/>\
\
<%def name="make_identity(e)">\
                {
                    {
                        .index = ${c.index_name(e)},
                        .size = sizeof (${c.struct_name(e)}_identity_t)
                    }
                }
</%def>\
\
<%def name="make_val(e)">\
    %if c.e_container(e) == "array":
        %for n in range(1, len(list(c.e_container_sizes(e)))+1):
                .dim_size_${n} = ${c.array_dim_max_size_name(e)}_${n},
        %endfor
            %if c.type_name_base(e) == "rational":
                .v = {{.nom = 0, .denom = 0}} /* ${c.type_name(e)} */
            %else:
                .v = {0} /* ${c.type_name(e)} */
            %endif
    %else:
        %if c.type_name_base(e) == "rational":
                .v = {.nom = 0, .denom = 0} /* ${c.type_name(e)} */
        %else:
                .v = 0 /* ${c.type_name(e)} */
        %endif
    %endif
</%def>\
\
<%def name="make_valid_by_kind(k_name)">\
    %for e in c.all_entries_by_kind(metadata, k_name):
        .${c.struct_instance_name(e)} = 1,
    %endfor
</%def>\
\
<%def name="make_struct_by_kind(k_name)">\
    %for e in c.all_entries_by_kind(metadata, k_name):
        .${c.struct_instance_name(e)} = {
            .identity = {
${make_identity(e)}\
            },
            .v = {
${make_val(e)}\
            }
        },
    %endfor
        .metadata_end_marker = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_METADATA_END_MARKER,
                        .size = 0
                    }
                }
            }
        }
</%def>\
\
<%def name="make_init()">\
    %for k_name in c.all_kind_names(metadata):
guzzi_camera3_metadata_${k_name}_t guzzi_camera3_metadata_${k_name}_init = {
    .valid = {
${make_valid_by_kind(k_name)}\
    },
    .s = {
${make_struct_by_kind(k_name)}\
    }
};
    %endfor
</%def>\
\
${make_init()}
