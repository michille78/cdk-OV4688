import os
from metadata_parser_xml import MetadataParserXml

def c_name(ctx, name):
    cname = "".join([i.isupper() and ("_" + i) or i for i in name])
    return cname.replace(".", "_").lower()

def tag_name(ctx, e):
    tname = c_name(ctx, e.name)
    return tname.upper()

def index_name(ctx, e):
    tname = e.name[e.name.find('.') + 1:]
    tname = "guzzi_camera3_index_%s_%s" % (e.kind, c_name(ctx, tname))
    return tname.upper()

def enum_name(ctx, e):
    name = e.name[e.name.find('.') + 1:]
    return "guzzi_camera3_enum_%s" % (c_name(ctx, name))

def enum_val_name(ctx, e, v):
    val_name = "%s_%s" % (enum_name(ctx, e), v.name)
    return val_name.upper()

def struct_instance_name(ctx, e):
    name = e.name[e.name.find('.') + 1:]
    return c_name(ctx, name)

def struct_name(ctx, e):
    return "guzzi_camera3_%s_%s" % (e.kind, struct_instance_name(ctx, e))

def array_size_name(ctx, e):
    name = "%s_size" % (struct_name(ctx, e))
    return name.upper()

def array_dim_max_size_name(ctx, e):
    name = "%s_dim_max_size" % (struct_name(ctx, e))
    return name.upper()

def type_name_base(ctx, e):
    if e.is_clone():
        type = e.entry.type
    else:
        type = e.type
    return type

def type_name(ctx, e):
    return "guzzi_camera3_%s_t" % (type_name_base(ctx, e))

def doc(ctx, text, indent = 4):
  comment_prefix = " " * indent + " * "
  comment_para = comment_prefix + "\n"
  c_text = ""

  in_body = False
  first_paragraph = True
  for line in (line.strip() for line in text.splitlines()):
    if not line:
      in_body = False
    else:
      if not in_body and not first_paragraph:
        c_text = c_text + comment_para

      in_body = True
      first_paragraph = False

      c_text = c_text + comment_prefix + line + "\n";

  return c_text

def all_entries(ctx, metadata):
    entries = []
    for ons in metadata.outer_namespaces:
        for sec in ons.sections:
            for k in sec.kinds:
                for ins in k.namespaces:
                    for e in ins.entries:
                        entries.append(e)
                for e in k.entries:
                    entries.append(e)
    return sorted(entries, key=lambda e: e.name)

def all_entries_except_clones(ctx, metadata):
    entries = all_entries(ctx, metadata)
    return [e for e in entries if not e.is_clone()]

def all_entries_by_kind(ctx, metadata, kind_name):
    entries = all_entries(ctx, metadata)
    return [e for e in entries if e.kind == kind_name]

def all_kind_names(ctx, metadata):
    kinds = {}
    for ons in metadata.outer_namespaces:
        for sec in ons.sections:
            for k in sec.kinds:
                kinds[k.name] = k.name
    return sorted(kinds.keys())

def e_container(ctx, e):
    if e.is_clone():
        return e.entry.container
    else:
        return e.container

def e_container_sizes(ctx, e):
    if e.is_clone():
        return e.entry.container_sizes
    else:
        return e.container_sizes


_guzzi_metadata_file = os.path.dirname(os.path.realpath(__file__)) + "/../guzzi_specific/guzzi_metadata_properties.xml"
_guzzi_metadata_parser = MetadataParserXml(_guzzi_metadata_file)
_guzzi_metadata = _guzzi_metadata_parser.metadata
def guzzi_metadata(ctx):
    return _guzzi_metadata

