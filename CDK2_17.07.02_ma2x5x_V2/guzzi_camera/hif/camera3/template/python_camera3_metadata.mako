# ==============================================================================
# Copyright (c) 2013-2014 MM Solutions AD
# All rights reserved. Property of MM Solutions AD.
#
# This source code may not be used against the terms and conditions stipulated
# in the licensing agreement under which it has been supplied, or without the
# written permission of MM Solutions. Rights to use, copy, modify, and
# distribute or disclose this source code and its documentation are granted only
# through signed licensing agreement, provided that this copyright notice
# appears in all copies, modifications, and distributions and subject to the
# following conditions:
# THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
# WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
# ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
# NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
# INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
# PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
# THIS SOURCE CODE AND ITS DOCUMENTATION.
# ==============================================================================
#
# @file
#
# @author ( MM Solutions AD )
#
#
# ------------------------------------------------------------------------------
#!
#! Revision History
#! ===================================
#! 05-Nov-2013 : Author ( MM Solutions AD )
#! Created
# ==============================================================================

import ctypes
from guzzi_camera3_metadata_struct_defs import *
from guzzi_camera3_metadata_struct_auto_gen import *

<%namespace name="c" module="helpers"/>\
\
<%def name="make_identity_by_kind(k_name)">\
    %for e in c.all_entries_by_kind(metadata, k_name):
class ${c.struct_name(e)}_identity_t(ctypes.Structure):
    _fields_ = [
        ("identity", guzzi_camera3_metadata_identity_t),
        ("v", ${c.struct_name(e)}_t)
    ]

    %endfor
</%def>\
\
<%def name="make_valid_group_by_kind(k_name)">\
class guzzi_camera3_metadata_${k_name}_valid_t(ctypes.Structure):
    _fields_ = [
    %for e in c.all_entries_by_kind(metadata, k_name)[:-1]:
        ("${c.struct_instance_name(e)}", guzzi_camera3_int32_t),
    %endfor
<% e = c.all_entries_by_kind(metadata, k_name)[-1] %>\
        ("${c.struct_instance_name(e)}", guzzi_camera3_int32_t)
    ]
</%def>\
\
<%def name="make_struct_group_by_kind(k_name)">\
class guzzi_camera3_metadata_${k_name}_struct_t(ctypes.Structure):
    _fields_ = [
    %for e in c.all_entries_by_kind(metadata, k_name):
        ("${c.struct_instance_name(e)}", ${c.struct_name(e)}_identity_t),
    %endfor
        ("metadata_end_marker", guzzi_camera3_metadata_t)
    ]
</%def>\
\
class guzzi_camera3_metadata_identity_struct_t(ctypes.Structure):
    _fields_ = [
        ("index", guzzi_camera3_int32_t),
        ("size", guzzi_camera3_int32_t)
    ]

class guzzi_camera3_metadata_identity_union_t(ctypes.Union):
    _anonymous_ = ("u",)
    _fields_ = [
        ("u", guzzi_camera3_metadata_identity_struct_t),
        ("d", guzzi_camera3_double_t),
        ("i64", guzzi_camera3_int64_t)
    ]

class guzzi_camera3_metadata_identity_t(ctypes.Structure):
    _anonymous_ = ("u",)
    _fields_ = [
        ("u", guzzi_camera3_metadata_identity_union_t)
    ]

class guzzi_camera3_metadata_t(ctypes.Structure):
    _fields_ = [
        ("identity", guzzi_camera3_metadata_identity_t)
    ]

#
# *****************************************************************************
# ** Control Metadata Identity ************************************************
# *****************************************************************************
#
${make_identity_by_kind('controls')}\
#
# *****************************************************************************
# ** Dynamic Metadata Identity ************************************************
# *****************************************************************************
#
${make_identity_by_kind('dynamic')}\
#
# *****************************************************************************
# ** Static Metadata Identity *************************************************
# *****************************************************************************
#
${make_identity_by_kind('static')}\
#
# *****************************************************************************
# ** Control Metadata Valid ***************************************************
# *****************************************************************************
#
${make_valid_group_by_kind('controls')}
#
# *****************************************************************************
# ** Dynamic Metadata Valid ***************************************************
# *****************************************************************************
#
${make_valid_group_by_kind('dynamic')}
#
# *****************************************************************************
# ** Static Metadata Valid ****************************************************
# *****************************************************************************
#
${make_valid_group_by_kind('static')}
#
# *****************************************************************************
# ** Control Metadata Group ***************************************************
# *****************************************************************************
#
${make_struct_group_by_kind('controls')}
#
# *****************************************************************************
# ** Dynamic Metadata Group ***************************************************
# *****************************************************************************
#
${make_struct_group_by_kind('dynamic')}
#
# *****************************************************************************
# ** Static Metadata Group ****************************************************
# *****************************************************************************
#
${make_struct_group_by_kind('static')}
#
# *****************************************************************************
# ** Valid + struct ***********************************************************
# *****************************************************************************
#
class guzzi_camera3_metadata_controls_t(ctypes.Structure):
    _fields_ = [
        ("valid", guzzi_camera3_metadata_controls_valid_t),
        ("s", guzzi_camera3_metadata_controls_struct_t)
    ]

class guzzi_camera3_metadata_dynamic_t(ctypes.Structure):
    _fields_ = [
        ("valid", guzzi_camera3_metadata_dynamic_valid_t),
        ("s", guzzi_camera3_metadata_dynamic_struct_t)
    ]

class guzzi_camera3_metadata_static_t(ctypes.Structure):
    _fields_ = [
        ("valid", guzzi_camera3_metadata_static_valid_t),
        ("s", guzzi_camera3_metadata_static_struct_t)
    ]

