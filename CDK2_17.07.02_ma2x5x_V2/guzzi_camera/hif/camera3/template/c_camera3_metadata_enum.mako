/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file camera3_metadata_enum.h
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

<%namespace name="c" module="helpers"/>\
\
<%def name="generate_enum(e)">\
/*
 * ${c.enum_name(e)}_t
    %if e.type_notes:
${c.doc(e.type_notes, 0)}\
    %endif
 */
typedef enum {
    %for v in e.enum.values:
            %if v.notes:
    /*
${c.doc(v.notes)}\
     */
            %endif
    %if v.id:
    ${c.enum_val_name(e, v)} = ${v.id},
    %else:
    ${c.enum_val_name(e, v)},
    %endif
    %endfor
} ${c.enum_name(e)}_t;

</%def>\
\
<%def name="generate_enum_by_metadata(mdata)">\
    %for e in c.all_entries_except_clones(mdata):
        %if e.enum:
${generate_enum(e)}\
        %endif
    %endfor
</%def>\
\
${generate_enum_by_metadata(metadata)}\
/*
 * ****************************************************************************
 * ** Guzzi specific **********************************************************
 * ****************************************************************************
 */
${generate_enum_by_metadata(c.guzzi_metadata())}\
