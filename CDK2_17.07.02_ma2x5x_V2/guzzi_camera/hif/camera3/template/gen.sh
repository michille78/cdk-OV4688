#!/bin/bash

SRC="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

HIF_TOP=$(dirname $(dirname $SRC))

# TODO: Use android build/envsetup.sh ?
# Expect $TOP/hardware/mms/hif/camera3/template/gen.sh
TOP=$(dirname $(dirname $(dirname $HIF_TOP)))
if [ "$TOP" == "" ]; then
    echo "Has something wrong. Can't find root folder!!!"
    exit 1
fi;

TMPLT_SRC=$HIF_TOP/camera3/template

HAL3_SRC=$TOP/system/media/camera/docs
XML_PARSER=$HAL3_SRC/metadata_parser_xml.py
XML_PROPERTIES=$HAL3_SRC/metadata_properties.xml

export PYTHONPATH=$TMPLT_SRC:$HAL3_SRC:$PYTHONPATH

################################################################################
# Guzzi specific docs
################################################################################
GUZZI_SRC=$HIF_TOP/camera3/guzzi_specific

python -B $XML_PARSER $GUZZI_SRC/guzzi_metadata_properties.xml $HAL3_SRC/html.mako $GUZZI_SRC/guzzi_docs.html

################################################################################
# C
################################################################################
C_INCLUIDE=$HIF_TOP/camera3/c/include/guzzi/auto_gen
C_SRC=$HIF_TOP/camera3/c/auto_gen

python -B $XML_PARSER $XML_PROPERTIES $TMPLT_SRC/c_camera3_metadata.mako                       $C_INCLUIDE/camera3/metadata.h
python -B $XML_PARSER $XML_PROPERTIES $TMPLT_SRC/c_camera3_metadata_enum.mako                  $C_INCLUIDE/camera3/metadata_enum.h
python -B $XML_PARSER $XML_PROPERTIES $TMPLT_SRC/c_camera3_metadata_index.mako                 $C_INCLUIDE/camera3/metadata_index.h
python -B $XML_PARSER $XML_PROPERTIES $TMPLT_SRC/c_camera3_metadata_struct.mako                $C_INCLUIDE/camera3/metadata_struct.h
python -B $XML_PARSER $XML_PROPERTIES $TMPLT_SRC/c_camera3_metadata_struct_defs.mako           $C_INCLUIDE/camera3/metadata_struct_defs.h
python -B $XML_PARSER $XML_PROPERTIES $TMPLT_SRC/c_camera3_metadata_helpers.mako               $C_SRC/camera3/helpers.c
python -B $XML_PARSER $XML_PROPERTIES $TMPLT_SRC/c_camera3_metadata_convert_hal3.mako          $C_SRC/camera3/convert_hal3.c
python -B $XML_PARSER $XML_PROPERTIES $TMPLT_SRC/c_camera3_metadata_init.mako                  $C_SRC/camera3/init.c

python -B $XML_PARSER $XML_PROPERTIES $TMPLT_SRC/c_camera_external_struct.mako                 $C_INCLUIDE/camera_external/struct.h
python -B $XML_PARSER $XML_PROPERTIES $TMPLT_SRC/c_camera_external_convert.mako                $C_SRC/camera_external/convert__do_not_compile.c
python -B $XML_PARSER $XML_PROPERTIES $TMPLT_SRC/c_camera_external_struct_helpers.mako         $C_SRC/camera_external/helpers.c

################################################################################
# Python
################################################################################
PY_SRC=$HIF_TOP/camera3/python

python -B $XML_PARSER $XML_PROPERTIES $TMPLT_SRC/python_camera3_metadata.mako                  $PY_SRC/guzzi_camera3_metadata_auto_gen.py
python -B $XML_PARSER $XML_PROPERTIES $TMPLT_SRC/python_camera3_metadata_enum.mako             $PY_SRC/guzzi_camera3_metadata_enum_auto_gen.py
python -B $XML_PARSER $XML_PROPERTIES $TMPLT_SRC/python_camera3_metadata_index.mako            $PY_SRC/guzzi_camera3_metadata_index_auto_gen.py
python -B $XML_PARSER $XML_PROPERTIES $TMPLT_SRC/python_camera3_metadata_struct.mako           $PY_SRC/guzzi_camera3_metadata_struct_auto_gen.py
python -B $XML_PARSER $XML_PROPERTIES $TMPLT_SRC/python_camera3_metadata_struct_defs.mako      $PY_SRC/guzzi_camera3_metadata_struct_defs_auto_gen.py

