/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file camera3_metadata_struct.h
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

<%namespace name="c" module="helpers"/>\
\
<%def name="make_array_dim_multiply(e)">\
    %if c.e_container(e) == "array":
#define ${c.array_size_name(e)} ${"\\"}
    ( ${"\\"}
        %for n in range(1, len(list(c.e_container_sizes(e)))+1):
            %if n == 1:
          ${c.array_dim_max_size_name(e)}_${n} ${"\\"}
            %else:
        * ${c.array_dim_max_size_name(e)}_${n} ${"\\"}
            %endif
        %endfor
    )
    %endif
</%def>\
\
<%def name="make_array_max_size_by_kind(mdata, k_name)">\
    %for e in c.all_entries_by_kind(mdata, k_name):
${make_array_dim_multiply(e)}\
    %endfor
</%def>\
\
<%def name="make_array_max_size_by_metadata(mdata)">\
    %for k_name in c.all_kind_names(mdata):
/*
 * ****************************************************************************
 * * ${k_name}
 * ****************************************************************************
 */
${make_array_max_size_by_kind(mdata, k_name)}\

    %endfor
</%def>\
\
\
\
<%def name="make_struct(e)">\
typedef struct {
    %if c.e_container(e) == "array":
        %for n in range(1, len(list(c.e_container_sizes(e)))+1):
    guzzi_camera3_int32_t dim_size_${n};
        %endfor
    %if len(list(c.e_container_sizes(e))) % 2:
    guzzi_camera3_int32_t dummy;
    %endif
    ${c.type_name(e)} v[${c.array_size_name(e)}];
    %else:
    ${c.type_name(e)} v;
    %endif
} ${c.struct_name(e)}_t;

</%def>\
\
<%def name="make_struct_by_kind(mdata, k_name)">\
    %for e in c.all_entries_by_kind(mdata, k_name):
${make_struct(e)}\
    %endfor
</%def>\
\
<%def name="make_struct_by_metadata(mdata)">\
    %for k_name in c.all_kind_names(mdata):
/*
 * ****************************************************************************
 * * ${k_name}
 * ****************************************************************************
 */
${make_struct_by_kind(mdata, k_name)}\
    %endfor
</%def>\
\
${make_array_max_size_by_metadata(metadata)}\
/*
 * ****************************************************************************
 * ** Guzzi specific **********************************************************
 * ****************************************************************************
 */
${make_array_max_size_by_metadata(c.guzzi_metadata())}\
${make_struct_by_metadata(metadata)}\
/*
 * ****************************************************************************
 * ** Guzzi specific **********************************************************
 * ****************************************************************************
 */
${make_struct_by_metadata(c.guzzi_metadata())}\
