/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file convert_hal3.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

<%namespace name="c" module="helpers"/>\
\
<%def name="make_hal2guzzi_function_name(e)">\
    %if c.e_container(e) == "array":
guzzi_camera3_metadata_convert_hal2guzzi_${c.type_name_base(e)}_arr${len(list(c.e_container_sizes(e)))}\
    %else:
guzzi_camera3_metadata_convert_hal2guzzi_${c.type_name_base(e)}\
    %endif
</%def>\
\
<%def name="make_hal2guzzi_struct_entry_by_entry(e)">\
    {
        .func = ${make_hal2guzzi_function_name(e)},
    },
</%def>\
\
<%def name="make_hal2guzzi_struct()">\
static guzzi_camera3_metadata_hal2guzzi_t guzzi_camera3_metadata_hal2guzzi[] = {
    %for e in c.all_entries_by_kind(metadata, 'controls'):
${make_hal2guzzi_struct_entry_by_entry(e)}\
    %endfor
};
</%def>\
\
###############################################################################
<%def name="make_guzzi2hal_function_name(e)">\
    %if c.e_container(e) == "array":
guzzi_camera3_metadata_convert_guzzi2hal_${c.type_name_base(e)}_arr${len(list(c.e_container_sizes(e)))}\
    %else:
guzzi_camera3_metadata_convert_guzzi2hal_${c.type_name_base(e)}\
    %endif
</%def>\
\
<%def name="make_guzzi2hal_struct_entry_by_entry(e)">\
    {
        .func = ${make_guzzi2hal_function_name(e)},
    },
</%def>\
\
<%def name="make_guzzi2hal_struct()">\
static guzzi_camera3_metadata_guzzi2hal_t guzzi_camera3_metadata_guzzi2hal[] = {
    %for k_name in c.all_kind_names(metadata):
        %for e in c.all_entries_by_kind(metadata, k_name):
${make_guzzi2hal_struct_entry_by_entry(e)}\
        %endfor
    %endfor
};
</%def>\
\
###############################################################################
<%def name="make_tag_to_hal2guzzi_struct()">\
guzzi_camera3_metadata_hal2guzzi_t *guzzi_camera3_metadata_tag_to_convert(camera_metadata_tag_t tag)
{
    switch (tag) {
    %for e,n in zip(c.all_entries_by_kind(metadata, 'controls'), range(len(c.all_entries_by_kind(metadata, 'controls')))):
        case ${c.tag_name(e)}:
            return guzzi_camera3_metadata_hal2guzzi + ${n};
    %endfor
        default:
            return (void *)0;
    }
}
</%def>\
\
<%def name="make_index_to_guzzi2hal_struct()">\
guzzi_camera3_metadata_guzzi2hal_t *guzzi_camera3_metadata_index_to_convert(guzzi_camera3_metadata_index_t index)
{
    switch (index) {
<% n = 0 %>\
    %for k_name in c.all_kind_names(metadata):
        %for e in c.all_entries_by_kind(metadata, k_name):
        case ${c.index_name(e)}:
            return guzzi_camera3_metadata_guzzi2hal + ${n};
<% n += 1 %>\
        %endfor

    %endfor
        default:
            return (void *)0;
    }
}
</%def>\
\
###############################################################################
\
<%def name="make_tag2idx(k_name)">\
guzzi_camera3_metadata_index_t guzzi_camera3_metadata_${k_name}_tag2idx(camera_metadata_tag_t tag)
{
    switch (tag) {
%for e in c.all_entries_by_kind(metadata, k_name):
        case ${c.tag_name(e)}:
            return ${c.index_name(e)};
%endfor
        default:
            return GUZZI_CAMERA3_INDEX_INVALID;
    }
}
</%def>\
###############################################################################
\
<%def name="make_idx2tag()">\
camera_metadata_tag_t guzzi_camera3_metadata_idx2tag(guzzi_camera3_metadata_index_t index)
{
    switch (index) {
%for e in c.all_entries(metadata):
        case ${c.index_name(e)}:
            return ${c.tag_name(e)};
%endfor
        default:
            return ANDROID_BLACK_LEVEL_END; /* TODO: Invalid */
    }
}
</%def>\
###############################################################################
\
#include <system/camera_metadata.h>
#include <guzzi/camera3/metadata_index.h>
#include <guzzi/camera3/metadata.h>
#include <guzzi/camera3/convert_hal3.h>

<%
    func_dict_hal2guzzi = {}
    func_dict_hal2guzzi_arr = {}
    for e in c.all_entries_by_kind(metadata, 'controls'):
        if c.e_container(e) == "array":
            name = "guzzi_camera3_metadata_convert_hal2guzzi_%s_arr%d" % (c.type_name_base(e), len(list(c.e_container_sizes(e))))
            func_dict_hal2guzzi_arr[name] = name 
        else:
            name = "guzzi_camera3_metadata_convert_hal2guzzi_%s" % (c.type_name_base(e))
            func_dict_hal2guzzi[name] = name 

    func_dict_guzzi2hal = {}
    func_dict_guzzi2hal_arr = {}
    for e in c.all_entries(metadata):
        if c.e_container(e) == "array":
            name = "guzzi_camera3_metadata_convert_guzzi2hal_%s_arr%d" % (c.type_name_base(e), len(list(c.e_container_sizes(e))))
            func_dict_guzzi2hal_arr[name] = name 
        else:
            name = "guzzi_camera3_metadata_convert_guzzi2hal_%s" % (c.type_name_base(e))
            func_dict_guzzi2hal[name] = name 

%>\
\
%for func in sorted(func_dict_hal2guzzi.keys()):
int ${func}(guzzi_camera3_metadata_type_identity_t *guzzi, camera_metadata_entry_t *hal);
%endfor
%for func in sorted(func_dict_hal2guzzi_arr.keys()):
int ${func}(guzzi_camera3_metadata_type_identity_t *guzzi, camera_metadata_entry_t *hal);
%endfor

%for func in sorted(func_dict_guzzi2hal.keys()):
int ${func}(camera_metadata_t *metadata, guzzi_camera3_metadata_type_identity_t *guzzi);
%endfor
%for func in sorted(func_dict_guzzi2hal_arr.keys()):
int ${func}(camera_metadata_t *metadata, guzzi_camera3_metadata_type_identity_t *guzzi);
%endfor

${make_hal2guzzi_struct()}
${make_guzzi2hal_struct()}
${make_tag_to_hal2guzzi_struct()}
${make_index_to_guzzi2hal_struct()}
${make_tag2idx('controls')}
${make_idx2tag()}
