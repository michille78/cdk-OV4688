/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file convert_hal2guzzi_types.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <string.h>
#include <system/camera_metadata.h>
#include <guzzi/camera3/metadata.h>

int guzzi_camera3_metadata_convert_hal2guzzi_byte(guzzi_camera3_metadata_type_identity_t *guzzi, camera_metadata_entry_t *hal)
{
    guzzi->u8.v = *hal->data.u8;
    return 0;
}

int guzzi_camera3_metadata_convert_hal2guzzi_float(guzzi_camera3_metadata_type_identity_t *guzzi, camera_metadata_entry_t *hal)
{
    guzzi->f.v = *hal->data.f;
    return 0;
}

int guzzi_camera3_metadata_convert_hal2guzzi_int32(guzzi_camera3_metadata_type_identity_t *guzzi, camera_metadata_entry_t *hal)
{
    guzzi->i32.v = *hal->data.i32;
    return 0;
}

int guzzi_camera3_metadata_convert_hal2guzzi_int64(guzzi_camera3_metadata_type_identity_t *guzzi, camera_metadata_entry_t *hal)
{
    guzzi->i64.v = *hal->data.i64;
    return 0;
}

int guzzi_camera3_metadata_convert_hal2guzzi_double_arr1(guzzi_camera3_metadata_type_identity_t *guzzi, camera_metadata_entry_t *hal)
{
    memcpy(guzzi->arr1.v, hal->data.d, calculate_camera_metadata_entry_data_size(hal->type, hal->count));
    guzzi->arr1.dim_size_1 = hal->count;
    return 0;
}

int guzzi_camera3_metadata_convert_hal2guzzi_float_arr1(guzzi_camera3_metadata_type_identity_t *guzzi, camera_metadata_entry_t *hal)
{
    memcpy(guzzi->arr1.v, hal->data.f, calculate_camera_metadata_entry_data_size(hal->type, hal->count));
    guzzi->arr1.dim_size_1 = hal->count;
    return 0;
}

int guzzi_camera3_metadata_convert_hal2guzzi_float_arr2(guzzi_camera3_metadata_type_identity_t *guzzi, camera_metadata_entry_t *hal)
{
    if (hal->count % guzzi->arr2.dim_size_1) {
        return -1;
    }
    memcpy(guzzi->arr2.v, hal->data.f, calculate_camera_metadata_entry_data_size(hal->type, hal->count));
    guzzi->arr2.dim_size_2 = hal->count / guzzi->arr2.dim_size_1;
    return 0;
}

int guzzi_camera3_metadata_convert_hal2guzzi_int32_arr1(guzzi_camera3_metadata_type_identity_t *guzzi, camera_metadata_entry_t *hal)
{
    memcpy(guzzi->arr1.v, hal->data.i32, calculate_camera_metadata_entry_data_size(hal->type, hal->count));
    guzzi->arr1.dim_size_1 = hal->count;
    return 0;
}

int guzzi_camera3_metadata_convert_hal2guzzi_int32_arr2(guzzi_camera3_metadata_type_identity_t *guzzi, camera_metadata_entry_t *hal)
{
    if (hal->count % guzzi->arr2.dim_size_1) {
        return -1;
    }
    memcpy(guzzi->arr2.v, hal->data.i32, calculate_camera_metadata_entry_data_size(hal->type, hal->count));
    guzzi->arr2.dim_size_2 = hal->count / guzzi->arr2.dim_size_1;
    return 0;
}

int guzzi_camera3_metadata_convert_hal2guzzi_rational_arr2(guzzi_camera3_metadata_type_identity_t *guzzi, camera_metadata_entry_t *hal)
{
    if (hal->count % guzzi->arr2.dim_size_1) {
        return -1;
    }
    memcpy(guzzi->arr2.v, hal->data.r, calculate_camera_metadata_entry_data_size(hal->type, hal->count));
    guzzi->arr2.dim_size_2 = hal->count / guzzi->arr2.dim_size_1;
    return 0;
}

