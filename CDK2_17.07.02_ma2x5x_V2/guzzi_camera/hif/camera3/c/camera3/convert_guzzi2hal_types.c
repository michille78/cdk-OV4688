/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file convert_guzzi2hal_types.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <system/camera_metadata.h>
#include <guzzi/camera3/metadata.h>
#include <guzzi/camera3/convert_hal3.h>

int guzzi_camera3_metadata_convert_guzzi2hal_byte(camera_metadata_t *metadata, guzzi_camera3_metadata_type_identity_t *guzzi)
{
    return add_camera_metadata_entry(
            metadata,
            guzzi_camera3_metadata_idx2tag(guzzi->identity.index),
            &guzzi->u8.v,
            sizeof (guzzi->u8.v) / sizeof (guzzi_camera3_byte_t)
        );
}

int guzzi_camera3_metadata_convert_guzzi2hal_double(camera_metadata_t *metadata, guzzi_camera3_metadata_type_identity_t *guzzi)
{
    return add_camera_metadata_entry(
            metadata,
            guzzi_camera3_metadata_idx2tag(guzzi->identity.index),
            &guzzi->d.v,
            sizeof (guzzi->d.v) / sizeof (guzzi_camera3_double_t)
        );
}

int guzzi_camera3_metadata_convert_guzzi2hal_float(camera_metadata_t *metadata, guzzi_camera3_metadata_type_identity_t *guzzi)
{
    return add_camera_metadata_entry(
            metadata,
            guzzi_camera3_metadata_idx2tag(guzzi->identity.index),
            &guzzi->f.v,
            sizeof (guzzi->f.v) / sizeof (guzzi_camera3_float_t)
        );
}

int guzzi_camera3_metadata_convert_guzzi2hal_int32(camera_metadata_t *metadata, guzzi_camera3_metadata_type_identity_t *guzzi)
{
    return add_camera_metadata_entry(
            metadata,
            guzzi_camera3_metadata_idx2tag(guzzi->identity.index),
            &guzzi->i32.v,
            sizeof (guzzi->i32.v) / sizeof (guzzi_camera3_int32_t)
        );
}

int guzzi_camera3_metadata_convert_guzzi2hal_int64(camera_metadata_t *metadata, guzzi_camera3_metadata_type_identity_t *guzzi)
{
    return add_camera_metadata_entry(
            metadata,
            guzzi_camera3_metadata_idx2tag(guzzi->identity.index),
            &guzzi->i64.v,
            sizeof (guzzi->i64.v) / sizeof (guzzi_camera3_int64_t)
        );
}

int guzzi_camera3_metadata_convert_guzzi2hal_rational(camera_metadata_t *metadata, guzzi_camera3_metadata_type_identity_t *guzzi)
{
    return add_camera_metadata_entry(
            metadata,
            guzzi_camera3_metadata_idx2tag(guzzi->identity.index),
            &guzzi->r.v,
            sizeof (guzzi->r.v) / sizeof (guzzi_camera3_rational_t)
        );
}

int guzzi_camera3_metadata_convert_guzzi2hal_byte_arr1(camera_metadata_t *metadata, guzzi_camera3_metadata_type_identity_t *guzzi)
{
    return add_camera_metadata_entry(
            metadata,
            guzzi_camera3_metadata_idx2tag(guzzi->identity.index),
            &guzzi->arr1.v,
            META_ARR_SIZE_1(&guzzi->arr1)
        );
}

int guzzi_camera3_metadata_convert_guzzi2hal_byte_arr2(camera_metadata_t *metadata, guzzi_camera3_metadata_type_identity_t *guzzi)
{
    return add_camera_metadata_entry(
            metadata,
            guzzi_camera3_metadata_idx2tag(guzzi->identity.index),
            &guzzi->arr2.v,
            META_ARR_SIZE_2(&guzzi->arr2)
        );
}

int guzzi_camera3_metadata_convert_guzzi2hal_double_arr1(camera_metadata_t *metadata, guzzi_camera3_metadata_type_identity_t *guzzi)
{
    return add_camera_metadata_entry(
            metadata,
            guzzi_camera3_metadata_idx2tag(guzzi->identity.index),
            &guzzi->arr1.v,
            META_ARR_SIZE_1(&guzzi->arr1)
        );
}

int guzzi_camera3_metadata_convert_guzzi2hal_float_arr1(camera_metadata_t *metadata, guzzi_camera3_metadata_type_identity_t *guzzi)
{
    return add_camera_metadata_entry(
            metadata,
            guzzi_camera3_metadata_idx2tag(guzzi->identity.index),
            &guzzi->arr1.v,
            META_ARR_SIZE_1(&guzzi->arr1)
        );
}

int guzzi_camera3_metadata_convert_guzzi2hal_float_arr2(camera_metadata_t *metadata, guzzi_camera3_metadata_type_identity_t *guzzi)
{
    return add_camera_metadata_entry(
            metadata,
            guzzi_camera3_metadata_idx2tag(guzzi->identity.index),
            &guzzi->arr2.v,
            META_ARR_SIZE_2(&guzzi->arr2)
        );
}

int guzzi_camera3_metadata_convert_guzzi2hal_float_arr3(camera_metadata_t *metadata, guzzi_camera3_metadata_type_identity_t *guzzi)
{
    return add_camera_metadata_entry(
            metadata,
            guzzi_camera3_metadata_idx2tag(guzzi->identity.index),
            &guzzi->arr3.v,
            META_ARR_SIZE_3(&guzzi->arr3)
        );
}

int guzzi_camera3_metadata_convert_guzzi2hal_float_arr4(camera_metadata_t *metadata, guzzi_camera3_metadata_type_identity_t *guzzi)
{
    return add_camera_metadata_entry(
            metadata,
            guzzi_camera3_metadata_idx2tag(guzzi->identity.index),
            &guzzi->arr4.v,
            META_ARR_SIZE_4(&guzzi->arr4)
        );
}

int guzzi_camera3_metadata_convert_guzzi2hal_int32_arr1(camera_metadata_t *metadata, guzzi_camera3_metadata_type_identity_t *guzzi)
{
    return add_camera_metadata_entry(
            metadata,
            guzzi_camera3_metadata_idx2tag(guzzi->identity.index),
            &guzzi->arr1.v,
            META_ARR_SIZE_1(&guzzi->arr1)
        );
}

int guzzi_camera3_metadata_convert_guzzi2hal_int32_arr2(camera_metadata_t *metadata, guzzi_camera3_metadata_type_identity_t *guzzi)
{
    return add_camera_metadata_entry(
            metadata,
            guzzi_camera3_metadata_idx2tag(guzzi->identity.index),
            &guzzi->arr2.v,
            META_ARR_SIZE_2(&guzzi->arr2)
        );
}

int guzzi_camera3_metadata_convert_guzzi2hal_int32_arr3(camera_metadata_t *metadata, guzzi_camera3_metadata_type_identity_t *guzzi)
{
    return add_camera_metadata_entry(
            metadata,
            guzzi_camera3_metadata_idx2tag(guzzi->identity.index),
            &guzzi->arr3.v,
            META_ARR_SIZE_3(&guzzi->arr3)
        );
}

int guzzi_camera3_metadata_convert_guzzi2hal_int64_arr1(camera_metadata_t *metadata, guzzi_camera3_metadata_type_identity_t *guzzi)
{
    return add_camera_metadata_entry(
            metadata,
            guzzi_camera3_metadata_idx2tag(guzzi->identity.index),
            &guzzi->arr1.v,
            META_ARR_SIZE_1(&guzzi->arr1)
        );
}

int guzzi_camera3_metadata_convert_guzzi2hal_int64_arr2(camera_metadata_t *metadata, guzzi_camera3_metadata_type_identity_t *guzzi)
{
    return add_camera_metadata_entry(
            metadata,
            guzzi_camera3_metadata_idx2tag(guzzi->identity.index),
            &guzzi->arr2.v,
            META_ARR_SIZE_2(&guzzi->arr2)
        );
}

int guzzi_camera3_metadata_convert_guzzi2hal_rational_arr1(camera_metadata_t *metadata, guzzi_camera3_metadata_type_identity_t *guzzi)
{
    return add_camera_metadata_entry(
            metadata,
            guzzi_camera3_metadata_idx2tag(guzzi->identity.index),
            &guzzi->arr1.v,
            META_ARR_SIZE_1(&guzzi->arr1)
        );
}

int guzzi_camera3_metadata_convert_guzzi2hal_rational_arr2(camera_metadata_t *metadata, guzzi_camera3_metadata_type_identity_t *guzzi)
{
    return add_camera_metadata_entry(
            metadata,
            guzzi_camera3_metadata_idx2tag(guzzi->identity.index),
            &guzzi->arr2.v,
            META_ARR_SIZE_2(&guzzi->arr2)
        );
}

