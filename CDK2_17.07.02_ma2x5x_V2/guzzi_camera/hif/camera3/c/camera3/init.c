/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file init.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 29-May-2014 : Hristo Hristov ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <osal/osal_stdtypes.h>
#include <guzzi/camera3/metadata_enum.h>
#include <guzzi/camera3/metadata.h>

#define MIN(a,b) ((a) < (b) ? (a) : (b))

guzzi_camera3_metadata_controls_t guzzi_camera3_metadata_controls_init = {
    .valid = {
        .black_level_lock = 1,
        .color_correction_gains = 1,
        .color_correction_mode = 1,
        .color_correction_transform = 1,
        .control_ae_antibanding_mode = 1,
        .control_ae_exposure_compensation = 1,
        .control_ae_lock = 1,
        .control_ae_mode = 1,
        .control_ae_precapture_trigger = 1,
        .control_ae_regions = 1,
        .control_ae_target_fps_range = 1,
        .control_af_mode = 1,
        .control_af_regions = 1,
        .control_af_trigger = 1,
        .control_awb_lock = 1,
        .control_awb_mode = 1,
        .control_awb_regions = 1,
        .control_capture_intent = 1,
        .control_effect_mode = 1,
        .control_mode = 1,
        .control_scene_mode = 1,
        .control_video_stabilization_mode = 1,
        .demosaic_mode = 1,
        .edge_mode = 1,
        .edge_strength = 1,
        .flash_firing_power = 1,
        .flash_firing_time = 1,
        .flash_mode = 1,
        .geometric_mode = 1,
        .geometric_strength = 1,
        .hot_pixel_mode = 1,
        .jpeg_gps_coordinates = 1,
        .jpeg_gps_processing_method = 1,
        .jpeg_gps_timestamp = 1,
        .jpeg_orientation = 1,
        .jpeg_quality = 1,
        .jpeg_thumbnail_quality = 1,
        .jpeg_thumbnail_size = 1,
        .led_transmit = 0,                      // no LED-s supported
        .lens_aperture = 1,
        .lens_filter_density = 1,
        .lens_focal_length = 1,
        .lens_focus_distance = 1,
        .lens_optical_stabilization_mode = 1,
        .noise_reduction_mode = 1,
        .noise_reduction_strength = 1,
        .request_frame_count = 1,
        .request_id = 1,
        .request_input_streams = 1,
        .request_metadata_mode = 1,
        .request_output_streams = 1,
        .request_type = 1,
        .scaler_crop_region = 1,
        .sensor_exposure_time = 1,
        .sensor_frame_duration = 1,
        .sensor_sensitivity = 1,
        .shading_mode = 1,
        .shading_strength = 1,
        .statistics_face_detect_mode = 1,
        .statistics_histogram_mode = 1,
        .statistics_lens_shading_map_mode = 1,
        .statistics_sharpness_map_mode = 1,
        .tonemap_curve_blue = 1,
        .tonemap_curve_green = 1,
        .tonemap_curve_red = 1,
        .tonemap_mode = 1,
    },
    .s = {
        .black_level_lock = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_CONTROLS_BLACK_LEVEL_LOCK,
                        .size = sizeof (guzzi_camera3_controls_black_level_lock_identity_t)
                    }
                }
            },
            .v = {
                .v = GUZZI_CAMERA3_ENUM_BLACK_LEVEL_LOCK_OFF /* guzzi_camera3_byte_t */
            }
        },
        .color_correction_gains = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_CONTROLS_COLOR_CORRECTION_GAINS,
                        .size = sizeof (guzzi_camera3_controls_color_correction_gains_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_CONTROLS_COLOR_CORRECTION_GAINS_DIM_MAX_SIZE_1,
                .v = {
                        1.0f, 1.0f, 1.0f, 1.0f
                } /* guzzi_camera3_float_t */
            }
        },
        .color_correction_mode = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_CONTROLS_COLOR_CORRECTION_MODE,
                        .size = sizeof (guzzi_camera3_controls_color_correction_mode_identity_t)
                    }
                }
            },
            .v = {
                .v = GUZZI_CAMERA3_ENUM_COLOR_CORRECTION_MODE_FAST /* guzzi_camera3_byte_t */
            }
        },
        .color_correction_transform = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_CONTROLS_COLOR_CORRECTION_TRANSFORM,
                        .size = sizeof (guzzi_camera3_controls_color_correction_transform_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_CONTROLS_COLOR_CORRECTION_TRANSFORM_DIM_MAX_SIZE_1,
                .dim_size_2 = GUZZI_CAMERA3_CONTROLS_COLOR_CORRECTION_TRANSFORM_DIM_MAX_SIZE_2,
                .v = {
                        {.nom = 1, .denom = 1}, {.nom = 0, .denom = 1}, {.nom = 0, .denom = 1},
                        {.nom = 0, .denom = 1}, {.nom = 1, .denom = 1}, {.nom = 0, .denom = 1},
                        {.nom = 0, .denom = 1}, {.nom = 0, .denom = 1}, {.nom = 1, .denom = 1}
                } /* guzzi_camera3_rational_t */
            }
        },
        .control_ae_antibanding_mode = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AE_ANTIBANDING_MODE,
                        .size = sizeof (guzzi_camera3_controls_control_ae_antibanding_mode_identity_t)
                    }
                }
            },
            .v = {
                .v = GUZZI_CAMERA3_ENUM_CONTROL_AE_ANTIBANDING_MODE_OFF /* guzzi_camera3_byte_t */
            }
        },
        .control_ae_exposure_compensation = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AE_EXPOSURE_COMPENSATION,
                        .size = sizeof (guzzi_camera3_controls_control_ae_exposure_compensation_identity_t)
                    }
                }
            },
            .v = {
                .v = 0 /* guzzi_camera3_int32_t */
            }
        },
        .control_ae_lock = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AE_LOCK,
                        .size = sizeof (guzzi_camera3_controls_control_ae_lock_identity_t)
                    }
                }
            },
            .v = {
                .v = GUZZI_CAMERA3_ENUM_CONTROL_AE_LOCK_OFF /* guzzi_camera3_byte_t */
            }
        },
        .control_ae_mode = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AE_MODE,
                        .size = sizeof (guzzi_camera3_controls_control_ae_mode_identity_t)
                    }
                }
            },
            .v = {
                .v = GUZZI_CAMERA3_ENUM_CONTROL_AE_MODE_ON /* guzzi_camera3_byte_t */
            }
        },
        .control_ae_precapture_trigger = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AE_PRECAPTURE_TRIGGER,
                        .size = sizeof (guzzi_camera3_controls_control_ae_precapture_trigger_identity_t)
                    }
                }
            },
            .v = {
                .v = GUZZI_CAMERA3_ENUM_CONTROL_AE_PRECAPTURE_TRIGGER_IDLE /* guzzi_camera3_byte_t */
            }
        },
        .control_ae_regions = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AE_REGIONS,
                        .size = sizeof (guzzi_camera3_controls_control_ae_regions_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_CONTROLS_CONTROL_AE_REGIONS_DIM_MAX_SIZE_1,
                .dim_size_2 = GUZZI_CAMERA3_CONTROLS_CONTROL_AE_REGIONS_DIM_MAX_SIZE_2 - 8,
                .v = {
                        0, 0, 4208 - 1, 3120 - 1, 0     // weight = 0 for all regions => no metering
                } /* guzzi_camera3_int32_t */
            }
        },
        .control_ae_target_fps_range = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AE_TARGET_FPS_RANGE,
                        .size = sizeof (guzzi_camera3_controls_control_ae_target_fps_range_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_CONTROLS_CONTROL_AE_TARGET_FPS_RANGE_DIM_MAX_SIZE_1,
                .v = {
                        2, 27
                } /* guzzi_camera3_int32_t */
            }
        },
        .control_af_mode = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AF_MODE,
                        .size = sizeof (guzzi_camera3_controls_control_af_mode_identity_t)
                    }
                }
            },
            .v = {
                .v = GUZZI_CAMERA3_ENUM_CONTROL_AF_MODE_OFF /* guzzi_camera3_byte_t */
            }
        },
        .control_af_regions = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AF_REGIONS,
                        .size = sizeof (guzzi_camera3_controls_control_af_regions_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_CONTROLS_CONTROL_AF_REGIONS_DIM_MAX_SIZE_1,
                .dim_size_2 = GUZZI_CAMERA3_CONTROLS_CONTROL_AF_REGIONS_DIM_MAX_SIZE_2 - 8,
                .v = {
                        0, 0, 4208 - 1, 3120 - 1, 0     // weight = 0 for all regions => no metering
                } /* guzzi_camera3_int32_t */
            }
        },
        .control_af_trigger = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AF_TRIGGER,
                        .size = sizeof (guzzi_camera3_controls_control_af_trigger_identity_t)
                    }
                }
            },
            .v = {
                .v = GUZZI_CAMERA3_ENUM_CONTROL_AF_TRIGGER_IDLE /* guzzi_camera3_byte_t */
            }
        },
        .control_awb_lock = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AWB_LOCK,
                        .size = sizeof (guzzi_camera3_controls_control_awb_lock_identity_t)
                    }
                }
            },
            .v = {
                .v = GUZZI_CAMERA3_ENUM_CONTROL_AWB_LOCK_OFF /* guzzi_camera3_byte_t */
            }
        },
        .control_awb_mode = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AWB_MODE,
                        .size = sizeof (guzzi_camera3_controls_control_awb_mode_identity_t)
                    }
                }
            },
            .v = {
                .v = GUZZI_CAMERA3_ENUM_CONTROL_AWB_MODE_AUTO /* guzzi_camera3_byte_t */
            }
        },
        .control_awb_regions = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AWB_REGIONS,
                        .size = sizeof (guzzi_camera3_controls_control_awb_regions_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_CONTROLS_CONTROL_AWB_REGIONS_DIM_MAX_SIZE_1,
                .dim_size_2 = GUZZI_CAMERA3_CONTROLS_CONTROL_AWB_REGIONS_DIM_MAX_SIZE_2 - 8,
                .v = {
                        0, 0, 4208 - 1, 3120 - 1, 0     // weight = 0 for all regions => no metering
                } /* guzzi_camera3_int32_t */
            }
        },
        .control_capture_intent = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_CAPTURE_INTENT,
                        .size = sizeof (guzzi_camera3_controls_control_capture_intent_identity_t)
                    }
                }
            },
            .v = {
                .v = GUZZI_CAMERA3_ENUM_CONTROL_CAPTURE_INTENT_PREVIEW /* guzzi_camera3_byte_t */
            }
        },
        .control_effect_mode = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_EFFECT_MODE,
                        .size = sizeof (guzzi_camera3_controls_control_effect_mode_identity_t)
                    }
                }
            },
            .v = {
                .v = GUZZI_CAMERA3_ENUM_CONTROL_EFFECT_MODE_OFF /* guzzi_camera3_byte_t */
            }
        },
        .control_mode = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_MODE,
                        .size = sizeof (guzzi_camera3_controls_control_mode_identity_t)
                    }
                }
            },
            .v = {
                .v = GUZZI_CAMERA3_ENUM_CONTROL_MODE_AUTO /* guzzi_camera3_byte_t */
            }
        },
        .control_scene_mode = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_SCENE_MODE,
                        .size = sizeof (guzzi_camera3_controls_control_scene_mode_identity_t)
                    }
                }
            },
            .v = {
                .v = GUZZI_CAMERA3_ENUM_CONTROL_SCENE_MODE_UNSUPPORTED /* guzzi_camera3_byte_t */
            }
        },
        .control_video_stabilization_mode = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_VIDEO_STABILIZATION_MODE,
                        .size = sizeof (guzzi_camera3_controls_control_video_stabilization_mode_identity_t)
                    }
                }
            },
            .v = {
                .v = GUZZI_CAMERA3_ENUM_CONTROL_VIDEO_STABILIZATION_MODE_OFF /* guzzi_camera3_byte_t */
            }
        },
        .demosaic_mode = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_CONTROLS_DEMOSAIC_MODE,
                        .size = sizeof (guzzi_camera3_controls_demosaic_mode_identity_t)
                    }
                }
            },
            .v = {
                .v = GUZZI_CAMERA3_ENUM_DEMOSAIC_MODE_FAST /* guzzi_camera3_byte_t */
            }
        },
        .edge_mode = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_CONTROLS_EDGE_MODE,
                        .size = sizeof (guzzi_camera3_controls_edge_mode_identity_t)
                    }
                }
            },
            .v = {
                .v = GUZZI_CAMERA3_ENUM_EDGE_MODE_FAST /* guzzi_camera3_byte_t */
            }
        },
        .edge_strength = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_CONTROLS_EDGE_STRENGTH,
                        .size = sizeof (guzzi_camera3_controls_edge_strength_identity_t)
                    }
                }
            },
            .v = {
                .v = 5 /* guzzi_camera3_byte_t */
            }
        },
        .flash_firing_power = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_CONTROLS_FLASH_FIRING_POWER,
                        .size = sizeof (guzzi_camera3_controls_flash_firing_power_identity_t)
                    }
                }
            },
            .v = {
                .v = 0 /* guzzi_camera3_byte_t */
            }
        },
        .flash_firing_time = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_CONTROLS_FLASH_FIRING_TIME,
                        .size = sizeof (guzzi_camera3_controls_flash_firing_time_identity_t)
                    }
                }
            },
            .v = {
                .v = 0 /* guzzi_camera3_int64_t */
            }
        },
        .flash_mode = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_CONTROLS_FLASH_MODE,
                        .size = sizeof (guzzi_camera3_controls_flash_mode_identity_t)
                    }
                }
            },
            .v = {
                .v = GUZZI_CAMERA3_ENUM_FLASH_MODE_OFF /* guzzi_camera3_byte_t */
            }
        },
        .geometric_mode = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_CONTROLS_GEOMETRIC_MODE,
                        .size = sizeof (guzzi_camera3_controls_geometric_mode_identity_t)
                    }
                }
            },
            .v = {
                .v = GUZZI_CAMERA3_ENUM_GEOMETRIC_MODE_OFF /* guzzi_camera3_byte_t */
            }
        },
        .geometric_strength = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_CONTROLS_GEOMETRIC_STRENGTH,
                        .size = sizeof (guzzi_camera3_controls_geometric_strength_identity_t)
                    }
                }
            },
            .v = {
                .v = 1 /* guzzi_camera3_byte_t */
            }
        },
        .hot_pixel_mode = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_CONTROLS_HOT_PIXEL_MODE,
                        .size = sizeof (guzzi_camera3_controls_hot_pixel_mode_identity_t)
                    }
                }
            },
            .v = {
                .v = GUZZI_CAMERA3_ENUM_HOT_PIXEL_MODE_FAST /* guzzi_camera3_byte_t */
            }
        },
        .jpeg_gps_coordinates = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_CONTROLS_JPEG_GPS_COORDINATES,
                        .size = sizeof (guzzi_camera3_controls_jpeg_gps_coordinates_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_CONTROLS_JPEG_GPS_COORDINATES_DIM_MAX_SIZE_1,
                .v = {
                        42.666699,  // Latitude
                        23.351155,  // Longitude
                        569.0       // Altitude
                } /* guzzi_camera3_double_t */
            }
        },
        .jpeg_gps_processing_method = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_CONTROLS_JPEG_GPS_PROCESSING_METHOD,
                        .size = sizeof (guzzi_camera3_controls_jpeg_gps_processing_method_identity_t)
                    }
                }
            },
            .v = {
                .v = 0 /* guzzi_camera3_byte_t */
            }
        },
        .jpeg_gps_timestamp = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_CONTROLS_JPEG_GPS_TIMESTAMP,
                        .size = sizeof (guzzi_camera3_controls_jpeg_gps_timestamp_identity_t)
                    }
                }
            },
            .v = {
                .v = 0 /* guzzi_camera3_int64_t */
            }
        },
        .jpeg_orientation = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_CONTROLS_JPEG_ORIENTATION,
                        .size = sizeof (guzzi_camera3_controls_jpeg_orientation_identity_t)
                    }
                }
            },
            .v = {
                .v = 0 /* guzzi_camera3_int32_t */
            }
        },
        .jpeg_quality = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_CONTROLS_JPEG_QUALITY,
                        .size = sizeof (guzzi_camera3_controls_jpeg_quality_identity_t)
                    }
                }
            },
            .v = {
                .v = 95 /* guzzi_camera3_byte_t */
            }
        },
        .jpeg_thumbnail_quality = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_CONTROLS_JPEG_THUMBNAIL_QUALITY,
                        .size = sizeof (guzzi_camera3_controls_jpeg_thumbnail_quality_identity_t)
                    }
                }
            },
            .v = {
                .v = 60 /* guzzi_camera3_byte_t */
            }
        },
        .jpeg_thumbnail_size = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_CONTROLS_JPEG_THUMBNAIL_SIZE,
                        .size = sizeof (guzzi_camera3_controls_jpeg_thumbnail_size_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_CONTROLS_JPEG_THUMBNAIL_SIZE_DIM_MAX_SIZE_1,
                .v = {
                        320, 240
                } /* guzzi_camera3_int32_t */
            }
        },
        .led_transmit = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_CONTROLS_LED_TRANSMIT,
                        .size = sizeof (guzzi_camera3_controls_led_transmit_identity_t)
                    }
                }
            },
            .v = {
                .v = GUZZI_CAMERA3_ENUM_LED_TRANSMIT_OFF /* guzzi_camera3_byte_t */
            }
        },
        .lens_aperture = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_CONTROLS_LENS_APERTURE,
                        .size = sizeof (guzzi_camera3_controls_lens_aperture_identity_t)
                    }
                }
            },
            .v = {
                .v = 2.8f /* guzzi_camera3_float_t */
            }
        },
        .lens_filter_density = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_CONTROLS_LENS_FILTER_DENSITY,
                        .size = sizeof (guzzi_camera3_controls_lens_filter_density_identity_t)
                    }
                }
            },
            .v = {
                .v = 0 /* guzzi_camera3_float_t */
            }
        },
        .lens_focal_length = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_CONTROLS_LENS_FOCAL_LENGTH,
                        .size = sizeof (guzzi_camera3_controls_lens_focal_length_identity_t)
                    }
                }
            },
            .v = {
                .v = 4.76f /* guzzi_camera3_float_t */
            }
        },
        .lens_focus_distance = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_CONTROLS_LENS_FOCUS_DISTANCE,
                        .size = sizeof (guzzi_camera3_controls_lens_focus_distance_identity_t)
                    }
                }
            },
            .v = {
                .v = 1 / 2.6973f /* guzzi_camera3_float_t */
            }
        },
        .lens_optical_stabilization_mode = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_CONTROLS_LENS_OPTICAL_STABILIZATION_MODE,
                        .size = sizeof (guzzi_camera3_controls_lens_optical_stabilization_mode_identity_t)
                    }
                }
            },
            .v = {
                .v = GUZZI_CAMERA3_ENUM_LENS_OPTICAL_STABILIZATION_MODE_OFF /* guzzi_camera3_byte_t */
            }
        },
        .noise_reduction_mode = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_CONTROLS_NOISE_REDUCTION_MODE,
                        .size = sizeof (guzzi_camera3_controls_noise_reduction_mode_identity_t)
                    }
                }
            },
            .v = {
                .v = GUZZI_CAMERA3_ENUM_NOISE_REDUCTION_MODE_FAST /* guzzi_camera3_byte_t */
            }
        },
        .noise_reduction_strength = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_CONTROLS_NOISE_REDUCTION_STRENGTH,
                        .size = sizeof (guzzi_camera3_controls_noise_reduction_strength_identity_t)
                    }
                }
            },
            .v = {
                .v = 5 /* guzzi_camera3_byte_t */
            }
        },
        .request_frame_count = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_CONTROLS_REQUEST_FRAME_COUNT,
                        .size = sizeof (guzzi_camera3_controls_request_frame_count_identity_t)
                    }
                }
            },
            .v = {
                .v = 0 /* guzzi_camera3_int32_t */
            }
        },
        .request_id = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_CONTROLS_REQUEST_ID,
                        .size = sizeof (guzzi_camera3_controls_request_id_identity_t)
                    }
                }
            },
            .v = {
                .v = 0 /* guzzi_camera3_int32_t */
            }
        },
        .request_input_streams = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_CONTROLS_REQUEST_INPUT_STREAMS,
                        .size = sizeof (guzzi_camera3_controls_request_input_streams_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_CONTROLS_REQUEST_INPUT_STREAMS_DIM_MAX_SIZE_1 - 1,
                .v = {
                        // Reprocess not supported
                } /* guzzi_camera3_int32_t */
            }
        },
        .request_metadata_mode = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_CONTROLS_REQUEST_METADATA_MODE,
                        .size = sizeof (guzzi_camera3_controls_request_metadata_mode_identity_t)
                    }
                }
            },
            .v = {
                .v = GUZZI_CAMERA3_ENUM_REQUEST_METADATA_MODE_FULL /* guzzi_camera3_byte_t */
            }
        },
        .request_output_streams = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_CONTROLS_REQUEST_OUTPUT_STREAMS,
                        .size = sizeof (guzzi_camera3_controls_request_output_streams_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_CONTROLS_REQUEST_OUTPUT_STREAMS_DIM_MAX_SIZE_1 - 3,
                .v = {
                        1
                } /* guzzi_camera3_int32_t */
            }
        },
        .request_type = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_CONTROLS_REQUEST_TYPE,
                        .size = sizeof (guzzi_camera3_controls_request_type_identity_t)
                    }
                }
            },
            .v = {
                .v = GUZZI_CAMERA3_ENUM_REQUEST_TYPE_CAPTURE /* guzzi_camera3_byte_t */
            }
        },
        .scaler_crop_region = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_CONTROLS_SCALER_CROP_REGION,
                        .size = sizeof (guzzi_camera3_controls_scaler_crop_region_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_CONTROLS_SCALER_CROP_REGION_DIM_MAX_SIZE_1,
                .v = {
                        0, 0, 4208, 3120
                } /* guzzi_camera3_int32_t */
            }
        },
        .sensor_exposure_time = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_CONTROLS_SENSOR_EXPOSURE_TIME,
                        .size = sizeof (guzzi_camera3_controls_sensor_exposure_time_identity_t)
                    }
                }
            },
            .v = {
                .v = 1000000LL /* guzzi_camera3_int64_t */
            }
        },
        .sensor_frame_duration = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_CONTROLS_SENSOR_FRAME_DURATION,
                        .size = sizeof (guzzi_camera3_controls_sensor_frame_duration_identity_t)
                    }
                }
            },
            .v = {
                .v = 37048518LL /* guzzi_camera3_int64_t */
            }
        },
        .sensor_sensitivity = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_CONTROLS_SENSOR_SENSITIVITY,
                        .size = sizeof (guzzi_camera3_controls_sensor_sensitivity_identity_t)
                    }
                }
            },
            .v = {
                .v = 100 /* guzzi_camera3_int32_t */
            }
        },
        .shading_mode = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_CONTROLS_SHADING_MODE,
                        .size = sizeof (guzzi_camera3_controls_shading_mode_identity_t)
                    }
                }
            },
            .v = {
                .v = GUZZI_CAMERA3_ENUM_SHADING_MODE_FAST /* guzzi_camera3_byte_t */
            }
        },
        .shading_strength = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_CONTROLS_SHADING_STRENGTH,
                        .size = sizeof (guzzi_camera3_controls_shading_strength_identity_t)
                    }
                }
            },
            .v = {
                .v = 8 /* guzzi_camera3_byte_t */
            }
        },
        .statistics_face_detect_mode = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_CONTROLS_STATISTICS_FACE_DETECT_MODE,
                        .size = sizeof (guzzi_camera3_controls_statistics_face_detect_mode_identity_t)
                    }
                }
            },
            .v = {
                .v = GUZZI_CAMERA3_ENUM_STATISTICS_FACE_DETECT_MODE_OFF /* guzzi_camera3_byte_t */
            }
        },
        .statistics_histogram_mode = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_CONTROLS_STATISTICS_HISTOGRAM_MODE,
                        .size = sizeof (guzzi_camera3_controls_statistics_histogram_mode_identity_t)
                    }
                }
            },
            .v = {
                .v = GUZZI_CAMERA3_ENUM_STATISTICS_HISTOGRAM_MODE_OFF /* guzzi_camera3_byte_t */
            }
        },
        .statistics_lens_shading_map_mode = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_CONTROLS_STATISTICS_LENS_SHADING_MAP_MODE,
                        .size = sizeof (guzzi_camera3_controls_statistics_lens_shading_map_mode_identity_t)
                    }
                }
            },
            .v = {
                .v = GUZZI_CAMERA3_ENUM_STATISTICS_LENS_SHADING_MAP_MODE_OFF /* guzzi_camera3_byte_t */
            }
        },
        .statistics_sharpness_map_mode = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_CONTROLS_STATISTICS_SHARPNESS_MAP_MODE,
                        .size = sizeof (guzzi_camera3_controls_statistics_sharpness_map_mode_identity_t)
                    }
                }
            },
            .v = {
                .v = GUZZI_CAMERA3_ENUM_STATISTICS_SHARPNESS_MAP_MODE_OFF /* guzzi_camera3_byte_t */
            }
        },
        .tonemap_curve_blue = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_CONTROLS_TONEMAP_CURVE_BLUE,
                        .size = sizeof (guzzi_camera3_controls_tonemap_curve_blue_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_CONTROLS_TONEMAP_CURVE_BLUE_DIM_MAX_SIZE_1,
                .dim_size_2 = GUZZI_CAMERA3_CONTROLS_TONEMAP_CURVE_BLUE_DIM_MAX_SIZE_2 - 126,
                .v = {
                           0,    0,
                        1.0f, 1.0f
                } /* guzzi_camera3_float_t */
            }
        },
        .tonemap_curve_green = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_CONTROLS_TONEMAP_CURVE_GREEN,
                        .size = sizeof (guzzi_camera3_controls_tonemap_curve_green_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_CONTROLS_TONEMAP_CURVE_GREEN_DIM_MAX_SIZE_1,
                .dim_size_2 = GUZZI_CAMERA3_CONTROLS_TONEMAP_CURVE_GREEN_DIM_MAX_SIZE_2 - 126,
                .v = {
                           0,    0,
                        1.0f, 1.0f
                } /* guzzi_camera3_float_t */
            }
        },
        .tonemap_curve_red = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_CONTROLS_TONEMAP_CURVE_RED,
                        .size = sizeof (guzzi_camera3_controls_tonemap_curve_red_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_CONTROLS_TONEMAP_CURVE_RED_DIM_MAX_SIZE_1,
                .dim_size_2 = GUZZI_CAMERA3_CONTROLS_TONEMAP_CURVE_RED_DIM_MAX_SIZE_2 - 126,
                .v = {
                           0,    0,
                        1.0f, 1.0f
                } /* guzzi_camera3_float_t */
            }
        },
        .tonemap_mode = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_CONTROLS_TONEMAP_MODE,
                        .size = sizeof (guzzi_camera3_controls_tonemap_mode_identity_t)
                    }
                }
            },
            .v = {
                .v = GUZZI_CAMERA3_ENUM_TONEMAP_MODE_FAST /* guzzi_camera3_byte_t */
            }
        },
        .metadata_end_marker = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_METADATA_END_MARKER,
                        .size = 0
                    }
                }
            }
        }
    }
};
guzzi_camera3_metadata_dynamic_t guzzi_camera3_metadata_dynamic_init = {
    .valid = {
        .black_level_lock = 1,                      // init
        .color_correction_gains = 1,                // dynamic
        .color_correction_transform = 1,            // dynamic
        .control_ae_precapture_id = 1,              // init
        .control_ae_regions = 1,                    // init
        .control_ae_state = 1,                      // dynamic
        .control_af_mode = 1,                       // init
        .control_af_regions = 1,                    // init
        .control_af_state = 1,                      // dynamic
        .control_af_trigger_id = 1,                 // init
        .control_awb_mode = 1,                      // init
        .control_awb_regions = 1,                   // init
        .control_awb_state = 1,                     // dynamic
        .control_mode = 1,                          // init
        .edge_mode = 1,                             // init
        .flash_firing_power = 1,                    // init
        .flash_firing_time = 1,                     // init
        .flash_mode = 1,                            // init
        .flash_state = 1,                           // init
        .hot_pixel_mode = 1,                        // init
        .jpeg_gps_coordinates = 1,                  // init
        .jpeg_gps_processing_method = 1,            // init
        .jpeg_gps_timestamp = 1,                    // init
        .jpeg_orientation = 1,                      // init
        .jpeg_quality = 1,                          // init
        .jpeg_size = 1,                             // dynamic
        .jpeg_thumbnail_quality = 1,                // init
        .jpeg_thumbnail_size = 1,                   // init
        .led_transmit = 0,                          // no LED-s supported
        .lens_aperture = 1,                         // init
        .lens_filter_density = 1,                   // init
        .lens_focal_length = 1,                     // init
        .lens_focus_distance = 1,                   // dynamic
        .lens_focus_range = 1,                      // dynamic
        .lens_optical_stabilization_mode = 1,       // init
        .lens_state = 1,                            // dynamic
        .noise_reduction_mode = 1,                  // init
        .quirks_partial_result = 1,                 // init
        .request_frame_count = 1,                   // dynamic
        .request_id = 1,                            // dynamic
        .request_metadata_mode = 1,                 // init
        .request_output_streams = 1,                // dynamic
        .scaler_crop_region = 1,                    // init
        .sensor_exposure_time = 1,                  // dynamic
        .sensor_frame_duration = 1,                 // dynamic
        .sensor_sensitivity = 1,                    // dynamic
        .sensor_temperature = 1,                    // dynamic
        .sensor_timestamp = 1,                      // dynamic
        .shading_mode = 1,                          // init
        .statistics_face_detect_mode = 1,           // init
        .statistics_face_ids = 0,                   // face detection is disabled
        .statistics_face_landmarks = 0,             // face detection is disabled
        .statistics_face_rectangles = 0,            // face detection is disabled
        .statistics_face_scores = 0,                // face detection is disabled
        .statistics_histogram = 0,                  // histogram is disabled
        .statistics_histogram_mode = 1,             // init
        .statistics_lens_shading_map = 0,           // lens-shading map is disabled
        .statistics_predicted_color_gains = 1,      // dynamic
        .statistics_predicted_color_transform = 1,  // dynamic
        .statistics_scene_flicker = 1,              // dynamic
        .statistics_sharpness_map = 0,              // sharpness map is disabled
        .statistics_sharpness_map_mode = 1,         // init
        .tonemap_curve_blue = 1,                    // dynamic
        .tonemap_curve_green = 1,                   // dynamic
        .tonemap_curve_red = 1,                     // dynamic
        .tonemap_mode = 1,                          // init
    },
    .s = {
        .black_level_lock = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_BLACK_LEVEL_LOCK,
                        .size = sizeof (guzzi_camera3_dynamic_black_level_lock_identity_t)
                    }
                }
            },
            .v = {
                .v = GUZZI_CAMERA3_ENUM_BLACK_LEVEL_LOCK_OFF /* guzzi_camera3_byte_t */
            }
        },
        .color_correction_gains = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_COLOR_CORRECTION_GAINS,
                        .size = sizeof (guzzi_camera3_dynamic_color_correction_gains_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_DYNAMIC_COLOR_CORRECTION_GAINS_DIM_MAX_SIZE_1,
                .v = {0} /* guzzi_camera3_float_t */
            }
        },
        .color_correction_transform = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_COLOR_CORRECTION_TRANSFORM,
                        .size = sizeof (guzzi_camera3_dynamic_color_correction_transform_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_DYNAMIC_COLOR_CORRECTION_TRANSFORM_DIM_MAX_SIZE_1,
                .dim_size_2 = GUZZI_CAMERA3_DYNAMIC_COLOR_CORRECTION_TRANSFORM_DIM_MAX_SIZE_2,
                .v = {{.nom = 0, .denom = 0}} /* guzzi_camera3_rational_t */
            }
        },
        .control_ae_precapture_id = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_CONTROL_AE_PRECAPTURE_ID,
                        .size = sizeof (guzzi_camera3_dynamic_control_ae_precapture_id_identity_t)
                    }
                }
            },
            .v = {
                .v = 0 /* guzzi_camera3_int32_t */
            }
        },
        .control_ae_regions = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_CONTROL_AE_REGIONS,
                        .size = sizeof (guzzi_camera3_dynamic_control_ae_regions_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_DYNAMIC_CONTROL_AE_REGIONS_DIM_MAX_SIZE_1,
                .dim_size_2 = GUZZI_CAMERA3_DYNAMIC_CONTROL_AE_REGIONS_DIM_MAX_SIZE_2 - 8,
                .v = {
                        0, 0, 4208 - 1, 3120 - 1, 0     // weight = 0 for all regions => no metering
                } /* guzzi_camera3_int32_t */
            }
        },
        .control_ae_state = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_CONTROL_AE_STATE,
                        .size = sizeof (guzzi_camera3_dynamic_control_ae_state_identity_t)
                    }
                }
            },
            .v = {
                .v = 0 /* guzzi_camera3_byte_t */
            }
        },
        .control_af_mode = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_CONTROL_AF_MODE,
                        .size = sizeof (guzzi_camera3_dynamic_control_af_mode_identity_t)
                    }
                }
            },
            .v = {
                .v = GUZZI_CAMERA3_ENUM_CONTROL_AF_MODE_OFF /* guzzi_camera3_byte_t */
            }
        },
        .control_af_regions = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_CONTROL_AF_REGIONS,
                        .size = sizeof (guzzi_camera3_dynamic_control_af_regions_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_DYNAMIC_CONTROL_AF_REGIONS_DIM_MAX_SIZE_1,
                .dim_size_2 = GUZZI_CAMERA3_DYNAMIC_CONTROL_AF_REGIONS_DIM_MAX_SIZE_2 - 8,
                .v = {
                        0, 0, 4208 - 1, 3120 - 1, 0     // weight = 0 for all regions => no metering
                } /* guzzi_camera3_int32_t */
            }
        },
        .control_af_state = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_CONTROL_AF_STATE,
                        .size = sizeof (guzzi_camera3_dynamic_control_af_state_identity_t)
                    }
                }
            },
            .v = {
                .v = 0 /* guzzi_camera3_byte_t */
            }
        },
        .control_af_trigger_id = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_CONTROL_AF_TRIGGER_ID,
                        .size = sizeof (guzzi_camera3_dynamic_control_af_trigger_id_identity_t)
                    }
                }
            },
            .v = {
                .v = 0 /* guzzi_camera3_int32_t */
            }
        },
        .control_awb_mode = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_CONTROL_AWB_MODE,
                        .size = sizeof (guzzi_camera3_dynamic_control_awb_mode_identity_t)
                    }
                }
            },
            .v = {
                .v = GUZZI_CAMERA3_ENUM_CONTROL_AWB_MODE_AUTO /* guzzi_camera3_byte_t */
            }
        },
        .control_awb_regions = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_CONTROL_AWB_REGIONS,
                        .size = sizeof (guzzi_camera3_dynamic_control_awb_regions_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_DYNAMIC_CONTROL_AWB_REGIONS_DIM_MAX_SIZE_1,
                .dim_size_2 = GUZZI_CAMERA3_DYNAMIC_CONTROL_AWB_REGIONS_DIM_MAX_SIZE_2 - 8,
                .v = {
                        0, 0, 4208 - 1, 3120 - 1, 0     // weight = 0 for all regions => no metering
                } /* guzzi_camera3_int32_t */
            }
        },
        .control_awb_state = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_CONTROL_AWB_STATE,
                        .size = sizeof (guzzi_camera3_dynamic_control_awb_state_identity_t)
                    }
                }
            },
            .v = {
                .v = 0 /* guzzi_camera3_byte_t */
            }
        },
        .control_mode = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_CONTROL_MODE,
                        .size = sizeof (guzzi_camera3_dynamic_control_mode_identity_t)
                    }
                }
            },
            .v = {
                .v = GUZZI_CAMERA3_ENUM_CONTROL_MODE_AUTO /* guzzi_camera3_byte_t */
            }
        },
        .edge_mode = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_EDGE_MODE,
                        .size = sizeof (guzzi_camera3_dynamic_edge_mode_identity_t)
                    }
                }
            },
            .v = {
                .v = GUZZI_CAMERA3_ENUM_EDGE_MODE_FAST /* guzzi_camera3_byte_t */
            }
        },
        .flash_firing_power = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_FLASH_FIRING_POWER,
                        .size = sizeof (guzzi_camera3_dynamic_flash_firing_power_identity_t)
                    }
                }
            },
            .v = {
                .v = 0 /* guzzi_camera3_byte_t */
            }
        },
        .flash_firing_time = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_FLASH_FIRING_TIME,
                        .size = sizeof (guzzi_camera3_dynamic_flash_firing_time_identity_t)
                    }
                }
            },
            .v = {
                .v = 0 /* guzzi_camera3_int64_t */
            }
        },
        .flash_mode = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_FLASH_MODE,
                        .size = sizeof (guzzi_camera3_dynamic_flash_mode_identity_t)
                    }
                }
            },
            .v = {
                .v = GUZZI_CAMERA3_ENUM_FLASH_MODE_OFF /* guzzi_camera3_byte_t */
            }
        },
        .flash_state = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_FLASH_STATE,
                        .size = sizeof (guzzi_camera3_dynamic_flash_state_identity_t)
                    }
                }
            },
            .v = {
                .v = GUZZI_CAMERA3_ENUM_FLASH_STATE_UNAVAILABLE /* guzzi_camera3_byte_t */
            }
        },
        .hot_pixel_mode = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_HOT_PIXEL_MODE,
                        .size = sizeof (guzzi_camera3_dynamic_hot_pixel_mode_identity_t)
                    }
                }
            },
            .v = {
                .v = GUZZI_CAMERA3_ENUM_HOT_PIXEL_MODE_FAST /* guzzi_camera3_byte_t */
            }
        },
        .jpeg_gps_coordinates = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_JPEG_GPS_COORDINATES,
                        .size = sizeof (guzzi_camera3_dynamic_jpeg_gps_coordinates_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_DYNAMIC_JPEG_GPS_COORDINATES_DIM_MAX_SIZE_1,
                .v = {
                        42.666699,  // Latitude
                        23.351155,  // Longitude
                        569.0       // Altitude
                } /* guzzi_camera3_double_t */
            }
        },
        .jpeg_gps_processing_method = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_JPEG_GPS_PROCESSING_METHOD,
                        .size = sizeof (guzzi_camera3_dynamic_jpeg_gps_processing_method_identity_t)
                    }
                }
            },
            .v = {
                .v = 0 /* guzzi_camera3_byte_t */
            }
        },
        .jpeg_gps_timestamp = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_JPEG_GPS_TIMESTAMP,
                        .size = sizeof (guzzi_camera3_dynamic_jpeg_gps_timestamp_identity_t)
                    }
                }
            },
            .v = {
                .v = 0 /* guzzi_camera3_int64_t */
            }
        },
        .jpeg_orientation = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_JPEG_ORIENTATION,
                        .size = sizeof (guzzi_camera3_dynamic_jpeg_orientation_identity_t)
                    }
                }
            },
            .v = {
                .v = 0 /* guzzi_camera3_int32_t */
            }
        },
        .jpeg_quality = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_JPEG_QUALITY,
                        .size = sizeof (guzzi_camera3_dynamic_jpeg_quality_identity_t)
                    }
                }
            },
            .v = {
                .v = 95 /* guzzi_camera3_byte_t */
            }
        },
        .jpeg_size = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_JPEG_SIZE,
                        .size = sizeof (guzzi_camera3_dynamic_jpeg_size_identity_t)
                    }
                }
            },
            .v = {
                .v = 0 /* guzzi_camera3_int32_t */
            }
        },
        .jpeg_thumbnail_quality = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_JPEG_THUMBNAIL_QUALITY,
                        .size = sizeof (guzzi_camera3_dynamic_jpeg_thumbnail_quality_identity_t)
                    }
                }
            },
            .v = {
                .v = 60 /* guzzi_camera3_byte_t */
            }
        },
        .jpeg_thumbnail_size = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_JPEG_THUMBNAIL_SIZE,
                        .size = sizeof (guzzi_camera3_dynamic_jpeg_thumbnail_size_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_DYNAMIC_JPEG_THUMBNAIL_SIZE_DIM_MAX_SIZE_1,
                .v = {
                        320, 240
                } /* guzzi_camera3_int32_t */
            }
        },
        .led_transmit = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_LED_TRANSMIT,
                        .size = sizeof (guzzi_camera3_dynamic_led_transmit_identity_t)
                    }
                }
            },
            .v = {
                .v = GUZZI_CAMERA3_ENUM_LED_TRANSMIT_OFF /* guzzi_camera3_byte_t */
            }
        },
        .lens_aperture = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_LENS_APERTURE,
                        .size = sizeof (guzzi_camera3_dynamic_lens_aperture_identity_t)
                    }
                }
            },
            .v = {
                .v = 2.8f /* guzzi_camera3_float_t */
            }
        },
        .lens_filter_density = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_LENS_FILTER_DENSITY,
                        .size = sizeof (guzzi_camera3_dynamic_lens_filter_density_identity_t)
                    }
                }
            },
            .v = {
                .v = 0 /* guzzi_camera3_float_t */
            }
        },
        .lens_focal_length = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_LENS_FOCAL_LENGTH,
                        .size = sizeof (guzzi_camera3_dynamic_lens_focal_length_identity_t)
                    }
                }
            },
            .v = {
                .v = 4.76f /* guzzi_camera3_float_t */
            }
        },
        .lens_focus_distance = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_LENS_FOCUS_DISTANCE,
                        .size = sizeof (guzzi_camera3_dynamic_lens_focus_distance_identity_t)
                    }
                }
            },
            .v = {
                .v = 0 /* guzzi_camera3_float_t */
            }
        },
        .lens_focus_range = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_LENS_FOCUS_RANGE,
                        .size = sizeof (guzzi_camera3_dynamic_lens_focus_range_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_DYNAMIC_LENS_FOCUS_RANGE_DIM_MAX_SIZE_1,
                .v = {0} /* guzzi_camera3_float_t */
            }
        },
        .lens_optical_stabilization_mode = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_LENS_OPTICAL_STABILIZATION_MODE,
                        .size = sizeof (guzzi_camera3_dynamic_lens_optical_stabilization_mode_identity_t)
                    }
                }
            },
            .v = {
                .v = GUZZI_CAMERA3_ENUM_LENS_OPTICAL_STABILIZATION_MODE_OFF /* guzzi_camera3_byte_t */
            }
        },
        .lens_state = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_LENS_STATE,
                        .size = sizeof (guzzi_camera3_dynamic_lens_state_identity_t)
                    }
                }
            },
            .v = {
                .v = 0 /* guzzi_camera3_byte_t */
            }
        },
        .noise_reduction_mode = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_NOISE_REDUCTION_MODE,
                        .size = sizeof (guzzi_camera3_dynamic_noise_reduction_mode_identity_t)
                    }
                }
            },
            .v = {
                .v = GUZZI_CAMERA3_ENUM_NOISE_REDUCTION_MODE_FAST /* guzzi_camera3_byte_t */
            }
        },
        .quirks_partial_result = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_QUIRKS_PARTIAL_RESULT,
                        .size = sizeof (guzzi_camera3_dynamic_quirks_partial_result_identity_t)
                    }
                }
            },
            .v = {
                .v = 0 /* guzzi_camera3_byte_t */
            }
        },
        .request_frame_count = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_REQUEST_FRAME_COUNT,
                        .size = sizeof (guzzi_camera3_dynamic_request_frame_count_identity_t)
                    }
                }
            },
            .v = {
                .v = 0 /* guzzi_camera3_int32_t */
            }
        },
        .request_id = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_REQUEST_ID,
                        .size = sizeof (guzzi_camera3_dynamic_request_id_identity_t)
                    }
                }
            },
            .v = {
                .v = 0 /* guzzi_camera3_int32_t */
            }
        },
        .request_metadata_mode = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_REQUEST_METADATA_MODE,
                        .size = sizeof (guzzi_camera3_dynamic_request_metadata_mode_identity_t)
                    }
                }
            },
            .v = {
                .v = GUZZI_CAMERA3_ENUM_REQUEST_METADATA_MODE_FULL /* guzzi_camera3_byte_t */
            }
        },
        .request_output_streams = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_REQUEST_OUTPUT_STREAMS,
                        .size = sizeof (guzzi_camera3_dynamic_request_output_streams_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_DYNAMIC_REQUEST_OUTPUT_STREAMS_DIM_MAX_SIZE_1,
                .v = {0} /* guzzi_camera3_int32_t */
            }
        },
        .scaler_crop_region = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_SCALER_CROP_REGION,
                        .size = sizeof (guzzi_camera3_dynamic_scaler_crop_region_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_DYNAMIC_SCALER_CROP_REGION_DIM_MAX_SIZE_1,
                .v = {
                        0, 0, 4208, 3120
                } /* guzzi_camera3_int32_t */
            }
        },
        .sensor_exposure_time = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_SENSOR_EXPOSURE_TIME,
                        .size = sizeof (guzzi_camera3_dynamic_sensor_exposure_time_identity_t)
                    }
                }
            },
            .v = {
                .v = 0 /* guzzi_camera3_int64_t */
            }
        },
        .sensor_frame_duration = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_SENSOR_FRAME_DURATION,
                        .size = sizeof (guzzi_camera3_dynamic_sensor_frame_duration_identity_t)
                    }
                }
            },
            .v = {
                .v = 0 /* guzzi_camera3_int64_t */
            }
        },
        .sensor_sensitivity = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_SENSOR_SENSITIVITY,
                        .size = sizeof (guzzi_camera3_dynamic_sensor_sensitivity_identity_t)
                    }
                }
            },
            .v = {
                .v = 0 /* guzzi_camera3_int32_t */
            }
        },
        .sensor_temperature = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_SENSOR_TEMPERATURE,
                        .size = sizeof (guzzi_camera3_dynamic_sensor_temperature_identity_t)
                    }
                }
            },
            .v = {
                .v = 0 /* guzzi_camera3_float_t */
            }
        },
        .sensor_timestamp = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_SENSOR_TIMESTAMP,
                        .size = sizeof (guzzi_camera3_dynamic_sensor_timestamp_identity_t)
                    }
                }
            },
            .v = {
                .v = 0 /* guzzi_camera3_int64_t */
            }
        },
        .shading_mode = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_SHADING_MODE,
                        .size = sizeof (guzzi_camera3_dynamic_shading_mode_identity_t)
                    }
                }
            },
            .v = {
                .v = GUZZI_CAMERA3_ENUM_SHADING_MODE_FAST /* guzzi_camera3_byte_t */
            }
        },
        .statistics_face_detect_mode = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_STATISTICS_FACE_DETECT_MODE,
                        .size = sizeof (guzzi_camera3_dynamic_statistics_face_detect_mode_identity_t)
                    }
                }
            },
            .v = {
                .v = GUZZI_CAMERA3_ENUM_STATISTICS_FACE_DETECT_MODE_OFF /* guzzi_camera3_byte_t */
            }
        },
        .statistics_face_ids = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_STATISTICS_FACE_IDS,
                        .size = sizeof (guzzi_camera3_dynamic_statistics_face_ids_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_DYNAMIC_STATISTICS_FACE_IDS_DIM_MAX_SIZE_1,
                .v = {0} /* guzzi_camera3_int32_t */
            }
        },
        .statistics_face_landmarks = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_STATISTICS_FACE_LANDMARKS,
                        .size = sizeof (guzzi_camera3_dynamic_statistics_face_landmarks_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_DYNAMIC_STATISTICS_FACE_LANDMARKS_DIM_MAX_SIZE_1,
                .dim_size_2 = GUZZI_CAMERA3_DYNAMIC_STATISTICS_FACE_LANDMARKS_DIM_MAX_SIZE_2,
                .v = {0} /* guzzi_camera3_int32_t */
            }
        },
        .statistics_face_rectangles = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_STATISTICS_FACE_RECTANGLES,
                        .size = sizeof (guzzi_camera3_dynamic_statistics_face_rectangles_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_DYNAMIC_STATISTICS_FACE_RECTANGLES_DIM_MAX_SIZE_1,
                .dim_size_2 = GUZZI_CAMERA3_DYNAMIC_STATISTICS_FACE_RECTANGLES_DIM_MAX_SIZE_2,
                .v = {0} /* guzzi_camera3_int32_t */
            }
        },
        .statistics_face_scores = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_STATISTICS_FACE_SCORES,
                        .size = sizeof (guzzi_camera3_dynamic_statistics_face_scores_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_DYNAMIC_STATISTICS_FACE_SCORES_DIM_MAX_SIZE_1,
                .v = {0} /* guzzi_camera3_byte_t */
            }
        },
        .statistics_histogram = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_STATISTICS_HISTOGRAM,
                        .size = sizeof (guzzi_camera3_dynamic_statistics_histogram_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_DYNAMIC_STATISTICS_HISTOGRAM_DIM_MAX_SIZE_1,
                .dim_size_2 = GUZZI_CAMERA3_DYNAMIC_STATISTICS_HISTOGRAM_DIM_MAX_SIZE_2,
                .v = {0} /* guzzi_camera3_int32_t */
            }
        },
        .statistics_histogram_mode = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_STATISTICS_HISTOGRAM_MODE,
                        .size = sizeof (guzzi_camera3_dynamic_statistics_histogram_mode_identity_t)
                    }
                }
            },
            .v = {
                .v = GUZZI_CAMERA3_ENUM_STATISTICS_HISTOGRAM_MODE_OFF /* guzzi_camera3_byte_t */
            }
        },
        .statistics_lens_shading_map = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_STATISTICS_LENS_SHADING_MAP,
                        .size = sizeof (guzzi_camera3_dynamic_statistics_lens_shading_map_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_DYNAMIC_STATISTICS_LENS_SHADING_MAP_DIM_MAX_SIZE_1,
                .dim_size_2 = GUZZI_CAMERA3_DYNAMIC_STATISTICS_LENS_SHADING_MAP_DIM_MAX_SIZE_2,
                .dim_size_3 = GUZZI_CAMERA3_DYNAMIC_STATISTICS_LENS_SHADING_MAP_DIM_MAX_SIZE_3,
                .v = {0} /* guzzi_camera3_float_t */
            }
        },
        .statistics_predicted_color_gains = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_STATISTICS_PREDICTED_COLOR_GAINS,
                        .size = sizeof (guzzi_camera3_dynamic_statistics_predicted_color_gains_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_DYNAMIC_STATISTICS_PREDICTED_COLOR_GAINS_DIM_MAX_SIZE_1,
                .v = {0} /* guzzi_camera3_float_t */
            }
        },
        .statistics_predicted_color_transform = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_STATISTICS_PREDICTED_COLOR_TRANSFORM,
                        .size = sizeof (guzzi_camera3_dynamic_statistics_predicted_color_transform_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_DYNAMIC_STATISTICS_PREDICTED_COLOR_TRANSFORM_DIM_MAX_SIZE_1,
                .dim_size_2 = GUZZI_CAMERA3_DYNAMIC_STATISTICS_PREDICTED_COLOR_TRANSFORM_DIM_MAX_SIZE_2,
                .v = {{.nom = 0, .denom = 0}} /* guzzi_camera3_rational_t */
            }
        },
        .statistics_scene_flicker = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_STATISTICS_SCENE_FLICKER,
                        .size = sizeof (guzzi_camera3_dynamic_statistics_scene_flicker_identity_t)
                    }
                }
            },
            .v = {
                .v = 0 /* guzzi_camera3_byte_t */
            }
        },
        .statistics_sharpness_map = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_STATISTICS_SHARPNESS_MAP,
                        .size = sizeof (guzzi_camera3_dynamic_statistics_sharpness_map_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_DYNAMIC_STATISTICS_SHARPNESS_MAP_DIM_MAX_SIZE_1,
                .dim_size_2 = GUZZI_CAMERA3_DYNAMIC_STATISTICS_SHARPNESS_MAP_DIM_MAX_SIZE_2,
                .dim_size_3 = GUZZI_CAMERA3_DYNAMIC_STATISTICS_SHARPNESS_MAP_DIM_MAX_SIZE_3,
                .v = {0} /* guzzi_camera3_int32_t */
            }
        },
        .statistics_sharpness_map_mode = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_STATISTICS_SHARPNESS_MAP_MODE,
                        .size = sizeof (guzzi_camera3_dynamic_statistics_sharpness_map_mode_identity_t)
                    }
                }
            },
            .v = {
                .v = GUZZI_CAMERA3_ENUM_STATISTICS_SHARPNESS_MAP_MODE_OFF /* guzzi_camera3_byte_t */
            }
        },
        .tonemap_curve_blue = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_TONEMAP_CURVE_BLUE,
                        .size = sizeof (guzzi_camera3_dynamic_tonemap_curve_blue_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_DYNAMIC_TONEMAP_CURVE_BLUE_DIM_MAX_SIZE_1,
                .dim_size_2 = GUZZI_CAMERA3_DYNAMIC_TONEMAP_CURVE_BLUE_DIM_MAX_SIZE_2,
                .v = {0} /* guzzi_camera3_float_t */
            }
        },
        .tonemap_curve_green = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_TONEMAP_CURVE_GREEN,
                        .size = sizeof (guzzi_camera3_dynamic_tonemap_curve_green_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_DYNAMIC_TONEMAP_CURVE_GREEN_DIM_MAX_SIZE_1,
                .dim_size_2 = GUZZI_CAMERA3_DYNAMIC_TONEMAP_CURVE_GREEN_DIM_MAX_SIZE_2,
                .v = {0} /* guzzi_camera3_float_t */
            }
        },
        .tonemap_curve_red = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_TONEMAP_CURVE_RED,
                        .size = sizeof (guzzi_camera3_dynamic_tonemap_curve_red_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_DYNAMIC_TONEMAP_CURVE_RED_DIM_MAX_SIZE_1,
                .dim_size_2 = GUZZI_CAMERA3_DYNAMIC_TONEMAP_CURVE_RED_DIM_MAX_SIZE_2,
                .v = {0} /* guzzi_camera3_float_t */
            }
        },
        .tonemap_mode = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_TONEMAP_MODE,
                        .size = sizeof (guzzi_camera3_dynamic_tonemap_mode_identity_t)
                    }
                }
            },
            .v = {
                .v = GUZZI_CAMERA3_ENUM_TONEMAP_MODE_FAST /* guzzi_camera3_byte_t */
            }
        },
        .metadata_end_marker = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_METADATA_END_MARKER,
                        .size = 0
                    }
                }
            }
        }
    }
};
guzzi_camera3_metadata_static_t guzzi_camera3_metadata_static_init = {
    .valid = {
        .control_ae_available_antibanding_modes = 1,
        .control_ae_available_modes = 1,
        .control_ae_available_target_fps_ranges = 1,
        .control_ae_compensation_range = 1,
        .control_ae_compensation_step = 1,
        .control_af_available_modes = 1,
        .control_available_effects = 1,
        .control_available_scene_modes = 1,
        .control_available_video_stabilization_modes = 1,
        .control_awb_available_modes = 1,
        .control_max_regions = 1,
        .control_scene_mode_overrides = 1,
        .flash_color_temperature = 1,
        .flash_info_available = 1,
        .flash_info_charge_duration = 1,
        .flash_max_energy = 1,
        .hot_pixel_info_map = 0,
        .info_supported_hardware_level = 1,
        .jpeg_available_thumbnail_sizes = 1,
        .jpeg_max_size = 1,
        .led_available_leds = 1,
        .lens_facing = 1,
        .lens_info_available_apertures = 1,
        .lens_info_available_filter_densities = 1,
        .lens_info_available_focal_lengths = 1,
        .lens_info_available_optical_stabilization = 1,
        .lens_info_geometric_correction_map = 1,
        .lens_info_geometric_correction_map_size = 1,
        .lens_info_hyperfocal_distance = 1,
        .lens_info_minimum_focus_distance = 1,
        .lens_info_shading_map_size = 1,
        .lens_optical_axis_angle = 1,
        .lens_position = 1,
        .quirks_metering_crop_region = 1,
        .quirks_trigger_af_with_auto = 1,
        .quirks_use_partial_result = 1,
        .quirks_use_zsl_format = 1,
        .request_max_num_output_streams = 1,
        .request_max_num_reprocess_streams = 1,
        .scaler_available_formats = 1,
        .scaler_available_jpeg_min_durations = 1,
        .scaler_available_jpeg_sizes = 1,
        .scaler_available_max_digital_zoom = 1,
        .scaler_available_processed_min_durations = 1,
        .scaler_available_processed_sizes = 1,
        .scaler_available_raw_min_durations = 1,
        .scaler_available_raw_sizes = 1,
        .sensor_base_gain_factor = 1,
        .sensor_black_level_pattern = 1,
        .sensor_calibration_transform1 = 0,
        .sensor_calibration_transform2 = 0,
        .sensor_color_transform1 = 0,
        .sensor_color_transform2 = 0,
        .sensor_forward_matrix1 = 0,
        .sensor_forward_matrix2 = 0,
        .sensor_info_active_array_size = 1,
        .sensor_info_color_filter_arrangement = 1,
        .sensor_info_exposure_time_range = 1,
        .sensor_info_max_frame_duration = 1,
        .sensor_info_physical_size = 1,
        .sensor_info_pixel_array_size = 1,
        .sensor_info_sensitivity_range = 1,
        .sensor_info_white_level = 1,
        .sensor_max_analog_sensitivity = 1,
        .sensor_noise_model_coefficients = 0,
        .sensor_orientation = 1,
        .sensor_reference_illuminant1 = 0,
        .sensor_reference_illuminant2 = 0,
        .statistics_info_available_face_detect_modes = 1,
        .statistics_info_histogram_bucket_count = 1,
        .statistics_info_max_face_count = 1,
        .statistics_info_max_histogram_count = 1,
        .statistics_info_max_sharpness_map_value = 1,
        .statistics_info_sharpness_map_size = 1,
        .tonemap_max_curve_points = 1,
    },
    .s = {
        .control_ae_available_antibanding_modes = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_STATIC_CONTROL_AE_AVAILABLE_ANTIBANDING_MODES,
                        .size = sizeof (guzzi_camera3_static_control_ae_available_antibanding_modes_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_STATIC_CONTROL_AE_AVAILABLE_ANTIBANDING_MODES_DIM_MAX_SIZE_1,
                .v = {
                        GUZZI_CAMERA3_ENUM_CONTROL_AE_ANTIBANDING_MODE_OFF,
                        GUZZI_CAMERA3_ENUM_CONTROL_AE_ANTIBANDING_MODE_50HZ,
                        GUZZI_CAMERA3_ENUM_CONTROL_AE_ANTIBANDING_MODE_60HZ,
                        GUZZI_CAMERA3_ENUM_CONTROL_AE_ANTIBANDING_MODE_AUTO
                } /* guzzi_camera3_byte_t */
            }
        },
        .control_ae_available_modes = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_STATIC_CONTROL_AE_AVAILABLE_MODES,
                        .size = sizeof (guzzi_camera3_static_control_ae_available_modes_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_STATIC_CONTROL_AE_AVAILABLE_MODES_DIM_MAX_SIZE_1 - 3,
                .v = {
                        GUZZI_CAMERA3_ENUM_CONTROL_AE_MODE_OFF,
                        GUZZI_CAMERA3_ENUM_CONTROL_AE_MODE_ON,
                        //GUZZI_CAMERA3_ENUM_CONTROL_AE_MODE_ON_AUTO_FLASH,
                        //GUZZI_CAMERA3_ENUM_CONTROL_AE_MODE_ON_ALWAYS_FLASH,
                        //GUZZI_CAMERA3_ENUM_CONTROL_AE_MODE_ON_AUTO_FLASH_REDEYE
                } /* guzzi_camera3_byte_t */
            }
        },
        .control_ae_available_target_fps_ranges = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_STATIC_CONTROL_AE_AVAILABLE_TARGET_FPS_RANGES,
                        .size = sizeof (guzzi_camera3_static_control_ae_available_target_fps_ranges_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_STATIC_CONTROL_AE_AVAILABLE_TARGET_FPS_RANGES_DIM_MAX_SIZE_1,
                .dim_size_2 = GUZZI_CAMERA3_STATIC_CONTROL_AE_AVAILABLE_TARGET_FPS_RANGES_DIM_MAX_SIZE_2 - 4,
                .v = {
                        2, 27
                } /* guzzi_camera3_int32_t */
            }
        },
        .control_ae_compensation_range = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_STATIC_CONTROL_AE_COMPENSATION_RANGE,
                        .size = sizeof (guzzi_camera3_static_control_ae_compensation_range_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_STATIC_CONTROL_AE_COMPENSATION_RANGE_DIM_MAX_SIZE_1,
                .v = {
                        -9, 9
                } /* guzzi_camera3_int32_t */
            }
        },
        .control_ae_compensation_step = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_STATIC_CONTROL_AE_COMPENSATION_STEP,
                        .size = sizeof (guzzi_camera3_static_control_ae_compensation_step_identity_t)
                    }
                }
            },
            .v = {
                .v = {.nom = 1, .denom = 3} /* guzzi_camera3_rational_t */
            }
        },
        .control_af_available_modes = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_STATIC_CONTROL_AF_AVAILABLE_MODES,
                        .size = sizeof (guzzi_camera3_static_control_af_available_modes_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_STATIC_CONTROL_AF_AVAILABLE_MODES_DIM_MAX_SIZE_1 - 1,
                .v = {
                        GUZZI_CAMERA3_ENUM_CONTROL_AF_MODE_OFF,
                        GUZZI_CAMERA3_ENUM_CONTROL_AF_MODE_AUTO,
                        GUZZI_CAMERA3_ENUM_CONTROL_AF_MODE_MACRO,
                        GUZZI_CAMERA3_ENUM_CONTROL_AF_MODE_CONTINUOUS_VIDEO,
                        GUZZI_CAMERA3_ENUM_CONTROL_AF_MODE_CONTINUOUS_PICTURE,
                        //GUZZI_CAMERA3_ENUM_CONTROL_AF_MODE_EDOF
                } /* guzzi_camera3_byte_t */
            }
        },
        .control_available_effects = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_STATIC_CONTROL_AVAILABLE_EFFECTS,
                        .size = sizeof (guzzi_camera3_static_control_available_effects_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_STATIC_CONTROL_AVAILABLE_EFFECTS_DIM_MAX_SIZE_1,
                .v = {
                        GUZZI_CAMERA3_ENUM_CONTROL_EFFECT_MODE_OFF,
                        GUZZI_CAMERA3_ENUM_CONTROL_EFFECT_MODE_MONO,
                        GUZZI_CAMERA3_ENUM_CONTROL_EFFECT_MODE_NEGATIVE,
                        GUZZI_CAMERA3_ENUM_CONTROL_EFFECT_MODE_SOLARIZE,
                        GUZZI_CAMERA3_ENUM_CONTROL_EFFECT_MODE_SEPIA,
                        GUZZI_CAMERA3_ENUM_CONTROL_EFFECT_MODE_POSTERIZE,
                        GUZZI_CAMERA3_ENUM_CONTROL_EFFECT_MODE_WHITEBOARD,
                        GUZZI_CAMERA3_ENUM_CONTROL_EFFECT_MODE_BLACKBOARD,
                        GUZZI_CAMERA3_ENUM_CONTROL_EFFECT_MODE_AQUA
                } /* guzzi_camera3_byte_t */
            }
        },
        .control_available_scene_modes = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_STATIC_CONTROL_AVAILABLE_SCENE_MODES,
                        .size = sizeof (guzzi_camera3_static_control_available_scene_modes_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_STATIC_CONTROL_AVAILABLE_SCENE_MODES_DIM_MAX_SIZE_1 - 2,
                .v = {
                        //GUZZI_CAMERA3_ENUM_CONTROL_SCENE_MODE_UNSUPPORTED,  // DISABLED
                        //GUZZI_CAMERA3_ENUM_CONTROL_SCENE_MODE_FACE_PRIORITY,
                        GUZZI_CAMERA3_ENUM_CONTROL_SCENE_MODE_ACTION,
                        GUZZI_CAMERA3_ENUM_CONTROL_SCENE_MODE_PORTRAIT,
                        GUZZI_CAMERA3_ENUM_CONTROL_SCENE_MODE_LANDSCAPE,
                        GUZZI_CAMERA3_ENUM_CONTROL_SCENE_MODE_NIGHT,
                        GUZZI_CAMERA3_ENUM_CONTROL_SCENE_MODE_NIGHT_PORTRAIT,
                        GUZZI_CAMERA3_ENUM_CONTROL_SCENE_MODE_THEATRE,
                        GUZZI_CAMERA3_ENUM_CONTROL_SCENE_MODE_BEACH,
                        GUZZI_CAMERA3_ENUM_CONTROL_SCENE_MODE_SNOW,
                        GUZZI_CAMERA3_ENUM_CONTROL_SCENE_MODE_SUNSET,
                        GUZZI_CAMERA3_ENUM_CONTROL_SCENE_MODE_STEADYPHOTO,
                        GUZZI_CAMERA3_ENUM_CONTROL_SCENE_MODE_FIREWORKS,
                        GUZZI_CAMERA3_ENUM_CONTROL_SCENE_MODE_SPORTS,
                        GUZZI_CAMERA3_ENUM_CONTROL_SCENE_MODE_PARTY,
                        GUZZI_CAMERA3_ENUM_CONTROL_SCENE_MODE_CANDLELIGHT,
                        GUZZI_CAMERA3_ENUM_CONTROL_SCENE_MODE_BARCODE
                } /* guzzi_camera3_byte_t */
            }
        },
        .control_available_video_stabilization_modes = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_STATIC_CONTROL_AVAILABLE_VIDEO_STABILIZATION_MODES,
                        .size = sizeof (guzzi_camera3_static_control_available_video_stabilization_modes_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_STATIC_CONTROL_AVAILABLE_VIDEO_STABILIZATION_MODES_DIM_MAX_SIZE_1 - 1,
                .v = {
                        GUZZI_CAMERA3_ENUM_CONTROL_VIDEO_STABILIZATION_MODE_OFF,
                        //GUZZI_CAMERA3_ENUM_CONTROL_VIDEO_STABILIZATION_MODE_ON
                } /* guzzi_camera3_byte_t */
            }
        },
        .control_awb_available_modes = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_STATIC_CONTROL_AWB_AVAILABLE_MODES,
                        .size = sizeof (guzzi_camera3_static_control_awb_available_modes_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_STATIC_CONTROL_AWB_AVAILABLE_MODES_DIM_MAX_SIZE_1,
                .v = {
                        GUZZI_CAMERA3_ENUM_CONTROL_AWB_MODE_OFF,
                        GUZZI_CAMERA3_ENUM_CONTROL_AWB_MODE_AUTO,
                        GUZZI_CAMERA3_ENUM_CONTROL_AWB_MODE_INCANDESCENT,
                        GUZZI_CAMERA3_ENUM_CONTROL_AWB_MODE_FLUORESCENT,
                        GUZZI_CAMERA3_ENUM_CONTROL_AWB_MODE_WARM_FLUORESCENT,
                        GUZZI_CAMERA3_ENUM_CONTROL_AWB_MODE_DAYLIGHT,
                        GUZZI_CAMERA3_ENUM_CONTROL_AWB_MODE_CLOUDY_DAYLIGHT,
                        GUZZI_CAMERA3_ENUM_CONTROL_AWB_MODE_TWILIGHT,
                        GUZZI_CAMERA3_ENUM_CONTROL_AWB_MODE_SHADE
                } /* guzzi_camera3_byte_t */
            }
        },
        .control_max_regions = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_STATIC_CONTROL_MAX_REGIONS,
                        .size = sizeof (guzzi_camera3_static_control_max_regions_identity_t)
                    }
                }
            },
            .v = {
                .v = MIN(   GUZZI_CAMERA3_CONTROLS_CONTROL_AE_REGIONS_DIM_MAX_SIZE_2,   \
                        MIN(GUZZI_CAMERA3_CONTROLS_CONTROL_AF_REGIONS_DIM_MAX_SIZE_2,   \
                            GUZZI_CAMERA3_CONTROLS_CONTROL_AWB_REGIONS_DIM_MAX_SIZE_2)) /* guzzi_camera3_int32_t */
            }
        },
        .control_scene_mode_overrides = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_STATIC_CONTROL_SCENE_MODE_OVERRIDES,
                        .size = sizeof (guzzi_camera3_static_control_scene_mode_overrides_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_STATIC_CONTROL_SCENE_MODE_OVERRIDES_DIM_MAX_SIZE_1,
                .dim_size_2 = GUZZI_CAMERA3_STATIC_CONTROL_SCENE_MODE_OVERRIDES_DIM_MAX_SIZE_2 - 2,
                .v = {
                        //// GUZZI_CAMERA3_ENUM_CONTROL_SCENE_MODE_UNSUPPORTED
                        //GUZZI_CAMERA3_ENUM_CONTROL_AE_MODE_ON, GUZZI_CAMERA3_ENUM_CONTROL_AWB_MODE_AUTO, GUZZI_CAMERA3_ENUM_CONTROL_AF_MODE_AUTO,
                        //// GUZZI_CAMERA3_ENUM_CONTROL_SCENE_MODE_FACE_PRIORITY
                        //0, 0, 0,
                        // GUZZI_CAMERA3_ENUM_CONTROL_SCENE_MODE_ACTION
                        GUZZI_CAMERA3_ENUM_CONTROL_AE_MODE_ON_AUTO_FLASH, GUZZI_CAMERA3_ENUM_CONTROL_AWB_MODE_AUTO, GUZZI_CAMERA3_ENUM_CONTROL_AF_MODE_CONTINUOUS_PICTURE,
                        // GUZZI_CAMERA3_ENUM_CONTROL_SCENE_MODE_PORTRAIT
                        GUZZI_CAMERA3_ENUM_CONTROL_AE_MODE_ON_AUTO_FLASH_REDEYE, GUZZI_CAMERA3_ENUM_CONTROL_AWB_MODE_AUTO, GUZZI_CAMERA3_ENUM_CONTROL_AF_MODE_AUTO,
                        // GUZZI_CAMERA3_ENUM_CONTROL_SCENE_MODE_LANDSCAPE
                        GUZZI_CAMERA3_ENUM_CONTROL_AE_MODE_ON_AUTO_FLASH, GUZZI_CAMERA3_ENUM_CONTROL_AWB_MODE_DAYLIGHT, GUZZI_CAMERA3_ENUM_CONTROL_AF_MODE_AUTO,
                        // GUZZI_CAMERA3_ENUM_CONTROL_SCENE_MODE_NIGHT
                        GUZZI_CAMERA3_ENUM_CONTROL_AE_MODE_ON_AUTO_FLASH, GUZZI_CAMERA3_ENUM_CONTROL_AWB_MODE_AUTO, GUZZI_CAMERA3_ENUM_CONTROL_AF_MODE_AUTO,
                        // GUZZI_CAMERA3_ENUM_CONTROL_SCENE_MODE_NIGHT_PORTRAIT
                        GUZZI_CAMERA3_ENUM_CONTROL_AE_MODE_ON_AUTO_FLASH_REDEYE, GUZZI_CAMERA3_ENUM_CONTROL_AWB_MODE_AUTO, GUZZI_CAMERA3_ENUM_CONTROL_AF_MODE_AUTO,
                        // GUZZI_CAMERA3_ENUM_CONTROL_SCENE_MODE_THEATRE
                        GUZZI_CAMERA3_ENUM_CONTROL_AE_MODE_ON, GUZZI_CAMERA3_ENUM_CONTROL_AWB_MODE_AUTO, GUZZI_CAMERA3_ENUM_CONTROL_AF_MODE_AUTO,
                        // GUZZI_CAMERA3_ENUM_CONTROL_SCENE_MODE_BEACH
                        GUZZI_CAMERA3_ENUM_CONTROL_AE_MODE_ON_AUTO_FLASH, GUZZI_CAMERA3_ENUM_CONTROL_AWB_MODE_DAYLIGHT, GUZZI_CAMERA3_ENUM_CONTROL_AF_MODE_AUTO,
                        // GUZZI_CAMERA3_ENUM_CONTROL_SCENE_MODE_SNOW
                        GUZZI_CAMERA3_ENUM_CONTROL_AE_MODE_ON_AUTO_FLASH, GUZZI_CAMERA3_ENUM_CONTROL_AWB_MODE_DAYLIGHT, GUZZI_CAMERA3_ENUM_CONTROL_AF_MODE_AUTO,
                        // GUZZI_CAMERA3_ENUM_CONTROL_SCENE_MODE_SUNSET
                        GUZZI_CAMERA3_ENUM_CONTROL_AE_MODE_ON, GUZZI_CAMERA3_ENUM_CONTROL_AWB_MODE_INCANDESCENT, GUZZI_CAMERA3_ENUM_CONTROL_AF_MODE_AUTO,
                        // GUZZI_CAMERA3_ENUM_CONTROL_SCENE_MODE_STEADYPHOTO
                        GUZZI_CAMERA3_ENUM_CONTROL_AE_MODE_ON_AUTO_FLASH, GUZZI_CAMERA3_ENUM_CONTROL_AWB_MODE_AUTO, GUZZI_CAMERA3_ENUM_CONTROL_AF_MODE_AUTO,
                        // GUZZI_CAMERA3_ENUM_CONTROL_SCENE_MODE_FIREWORKS
                        GUZZI_CAMERA3_ENUM_CONTROL_AE_MODE_ON, GUZZI_CAMERA3_ENUM_CONTROL_AWB_MODE_WARM_FLUORESCENT, GUZZI_CAMERA3_ENUM_CONTROL_AF_MODE_AUTO,
                        // GUZZI_CAMERA3_ENUM_CONTROL_SCENE_MODE_SPORTS
                        GUZZI_CAMERA3_ENUM_CONTROL_AE_MODE_ON_AUTO_FLASH, GUZZI_CAMERA3_ENUM_CONTROL_AWB_MODE_AUTO, GUZZI_CAMERA3_ENUM_CONTROL_AF_MODE_CONTINUOUS_PICTURE,
                        // GUZZI_CAMERA3_ENUM_CONTROL_SCENE_MODE_PARTY
                        GUZZI_CAMERA3_ENUM_CONTROL_AE_MODE_ON_AUTO_FLASH, GUZZI_CAMERA3_ENUM_CONTROL_AWB_MODE_AUTO, GUZZI_CAMERA3_ENUM_CONTROL_AF_MODE_AUTO,
                        // GUZZI_CAMERA3_ENUM_CONTROL_SCENE_MODE_CANDLELIGHT
                        GUZZI_CAMERA3_ENUM_CONTROL_AE_MODE_ON, GUZZI_CAMERA3_ENUM_CONTROL_AWB_MODE_INCANDESCENT, GUZZI_CAMERA3_ENUM_CONTROL_AF_MODE_AUTO,
                        // GUZZI_CAMERA3_ENUM_CONTROL_SCENE_MODE_BARCODE
                        GUZZI_CAMERA3_ENUM_CONTROL_AE_MODE_ON, GUZZI_CAMERA3_ENUM_CONTROL_AWB_MODE_AUTO, GUZZI_CAMERA3_ENUM_CONTROL_AF_MODE_MACRO
                } /* guzzi_camera3_byte_t */
            }
        },
        .flash_color_temperature = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_STATIC_FLASH_COLOR_TEMPERATURE,
                        .size = sizeof (guzzi_camera3_static_flash_color_temperature_identity_t)
                    }
                }
            },
            .v = {
                .v = 0 /* guzzi_camera3_byte_t */
            }
        },
        .flash_info_available = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_STATIC_FLASH_INFO_AVAILABLE,
                        .size = sizeof (guzzi_camera3_static_flash_info_available_identity_t)
                    }
                }
            },
            .v = {
                .v = FALSE /* guzzi_camera3_byte_t */
            }
        },
        .flash_info_charge_duration = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_STATIC_FLASH_INFO_CHARGE_DURATION,
                        .size = sizeof (guzzi_camera3_static_flash_info_charge_duration_identity_t)
                    }
                }
            },
            .v = {
                .v = 0 /* guzzi_camera3_int64_t */
            }
        },
        .flash_max_energy = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_STATIC_FLASH_MAX_ENERGY,
                        .size = sizeof (guzzi_camera3_static_flash_max_energy_identity_t)
                    }
                }
            },
            .v = {
                .v = 0 /* guzzi_camera3_byte_t */
            }
        },
        .hot_pixel_info_map = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_STATIC_HOT_PIXEL_INFO_MAP,
                        .size = sizeof (guzzi_camera3_static_hot_pixel_info_map_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_STATIC_HOT_PIXEL_INFO_MAP_DIM_MAX_SIZE_1,
                .dim_size_2 = GUZZI_CAMERA3_STATIC_HOT_PIXEL_INFO_MAP_DIM_MAX_SIZE_2,
                .v = {0} /* guzzi_camera3_int32_t */
            }
        },
        .info_supported_hardware_level = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_STATIC_INFO_SUPPORTED_HARDWARE_LEVEL,
                        .size = sizeof (guzzi_camera3_static_info_supported_hardware_level_identity_t)
                    }
                }
            },
            .v = {
                .v = GUZZI_CAMERA3_ENUM_INFO_SUPPORTED_HARDWARE_LEVEL_FULL /* guzzi_camera3_byte_t */
            }
        },
        .jpeg_available_thumbnail_sizes = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_STATIC_JPEG_AVAILABLE_THUMBNAIL_SIZES,
                        .size = sizeof (guzzi_camera3_static_jpeg_available_thumbnail_sizes_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_STATIC_JPEG_AVAILABLE_THUMBNAIL_SIZES_DIM_MAX_SIZE_1,
                .dim_size_2 = GUZZI_CAMERA3_STATIC_JPEG_AVAILABLE_THUMBNAIL_SIZES_DIM_MAX_SIZE_2 - 2,
                .v = {
                          0,   0,
                        320, 240,
                        640, 480
                } /* guzzi_camera3_int32_t */
            }
        },
        .jpeg_max_size = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_STATIC_JPEG_MAX_SIZE,
                        .size = sizeof (guzzi_camera3_static_jpeg_max_size_identity_t)
                    }
                }
            },
            .v = {
                .v = 14 * 1024 * 1024 /* guzzi_camera3_int32_t */
            }
        },
        .led_available_leds = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_STATIC_LED_AVAILABLE_LEDS,
                        .size = sizeof (guzzi_camera3_static_led_available_leds_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_STATIC_LED_AVAILABLE_LEDS_DIM_MAX_SIZE_1 - 3,
                .v = {
                        //GUZZI_CAMERA3_ENUM_LED_AVAILABLE_LEDS_TRANSMIT    // no LED-s supported
                } /* guzzi_camera3_byte_t */
            }
        },
        .lens_facing = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_STATIC_LENS_FACING,
                        .size = sizeof (guzzi_camera3_static_lens_facing_identity_t)
                    }
                }
            },
            .v = {
                .v = GUZZI_CAMERA3_ENUM_LENS_FACING_BACK /* guzzi_camera3_byte_t */
            }
        },
        .lens_info_available_apertures = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_STATIC_LENS_INFO_AVAILABLE_APERTURES,
                        .size = sizeof (guzzi_camera3_static_lens_info_available_apertures_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_STATIC_LENS_INFO_AVAILABLE_APERTURES_DIM_MAX_SIZE_1 - 2,
                .v = {
                        2.8f
                } /* guzzi_camera3_float_t */
            }
        },
        .lens_info_available_filter_densities = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_STATIC_LENS_INFO_AVAILABLE_FILTER_DENSITIES,
                        .size = sizeof (guzzi_camera3_static_lens_info_available_filter_densities_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_STATIC_LENS_INFO_AVAILABLE_FILTER_DENSITIES_DIM_MAX_SIZE_1 - 2,
                .v = {
                        0
                } /* guzzi_camera3_float_t */
            }
        },
        .lens_info_available_focal_lengths = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_STATIC_LENS_INFO_AVAILABLE_FOCAL_LENGTHS,
                        .size = sizeof (guzzi_camera3_static_lens_info_available_focal_lengths_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_STATIC_LENS_INFO_AVAILABLE_FOCAL_LENGTHS_DIM_MAX_SIZE_1 - 2,
                .v = {
                        4.76f
                } /* guzzi_camera3_float_t */
            }
        },
        .lens_info_available_optical_stabilization = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_STATIC_LENS_INFO_AVAILABLE_OPTICAL_STABILIZATION,
                        .size = sizeof (guzzi_camera3_static_lens_info_available_optical_stabilization_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_STATIC_LENS_INFO_AVAILABLE_OPTICAL_STABILIZATION_DIM_MAX_SIZE_1 - 1,
                .v = {
                        GUZZI_CAMERA3_ENUM_LENS_OPTICAL_STABILIZATION_MODE_OFF,
                        //GUZZI_CAMERA3_ENUM_LENS_OPTICAL_STABILIZATION_MODE_ON
                } /* guzzi_camera3_byte_t */
            }
        },
        .lens_info_geometric_correction_map = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_STATIC_LENS_INFO_GEOMETRIC_CORRECTION_MAP,
                        .size = sizeof (guzzi_camera3_static_lens_info_geometric_correction_map_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_STATIC_LENS_INFO_GEOMETRIC_CORRECTION_MAP_DIM_MAX_SIZE_1,
                .dim_size_2 = GUZZI_CAMERA3_STATIC_LENS_INFO_GEOMETRIC_CORRECTION_MAP_DIM_MAX_SIZE_2,
                .dim_size_3 = GUZZI_CAMERA3_STATIC_LENS_INFO_GEOMETRIC_CORRECTION_MAP_DIM_MAX_SIZE_3 - 10,
                .dim_size_4 = GUZZI_CAMERA3_STATIC_LENS_INFO_GEOMETRIC_CORRECTION_MAP_DIM_MAX_SIZE_4 - 7,
                .v = {
                           0,    0,        0,    0,        0,    0,
                        1.0f,    0,     1.0f,    0,     1.0f,    0,
                           0, 1.0f,        0, 1.0f,        0, 1.0f,
                        1.0f, 1.0f,     1.0f, 1.0f,     1.0f, 1.0f
                } /* guzzi_camera3_float_t */
            }
        },
        .lens_info_geometric_correction_map_size = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_STATIC_LENS_INFO_GEOMETRIC_CORRECTION_MAP_SIZE,
                        .size = sizeof (guzzi_camera3_static_lens_info_geometric_correction_map_size_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_STATIC_LENS_INFO_GEOMETRIC_CORRECTION_MAP_SIZE_DIM_MAX_SIZE_1,
                .v = {
                        2, 2
                } /* guzzi_camera3_int32_t */
            }
        },
        .lens_info_hyperfocal_distance = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_STATIC_LENS_INFO_HYPERFOCAL_DISTANCE,
                        .size = sizeof (guzzi_camera3_static_lens_info_hyperfocal_distance_identity_t)
                    }
                }
            },
            .v = {
                .v = 1 / 2.6973f /* guzzi_camera3_float_t */
            }
        },
        .lens_info_minimum_focus_distance = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_STATIC_LENS_INFO_MINIMUM_FOCUS_DISTANCE,
                        .size = sizeof (guzzi_camera3_static_lens_info_minimum_focus_distance_identity_t)
                    }
                }
            },
            .v = {
                .v = 1 / 0.1f /* guzzi_camera3_float_t */
            }
        },
        .lens_info_shading_map_size = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_STATIC_LENS_INFO_SHADING_MAP_SIZE,
                        .size = sizeof (guzzi_camera3_static_lens_info_shading_map_size_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_STATIC_LENS_INFO_SHADING_MAP_SIZE_DIM_MAX_SIZE_1,
                .v = {
                        17, 13
                } /* guzzi_camera3_int32_t */
            }
        },
        .lens_optical_axis_angle = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_STATIC_LENS_OPTICAL_AXIS_ANGLE,
                        .size = sizeof (guzzi_camera3_static_lens_optical_axis_angle_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_STATIC_LENS_OPTICAL_AXIS_ANGLE_DIM_MAX_SIZE_1,
                .v = {
                        0, 0
                } /* guzzi_camera3_float_t */
            }
        },
        .lens_position = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_STATIC_LENS_POSITION,
                        .size = sizeof (guzzi_camera3_static_lens_position_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_STATIC_LENS_POSITION_DIM_MAX_SIZE_1,
                .v = {
                        5.0f, 5.0f, 4.7f
                } /* guzzi_camera3_float_t */
            }
        },
        .quirks_metering_crop_region = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_STATIC_QUIRKS_METERING_CROP_REGION,
                        .size = sizeof (guzzi_camera3_static_quirks_metering_crop_region_identity_t)
                    }
                }
            },
            .v = {
                .v = 0 /* guzzi_camera3_byte_t */
            }
        },
        .quirks_trigger_af_with_auto = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_STATIC_QUIRKS_TRIGGER_AF_WITH_AUTO,
                        .size = sizeof (guzzi_camera3_static_quirks_trigger_af_with_auto_identity_t)
                    }
                }
            },
            .v = {
                .v = 0 /* guzzi_camera3_byte_t */
            }
        },
        .quirks_use_partial_result = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_STATIC_QUIRKS_USE_PARTIAL_RESULT,
                        .size = sizeof (guzzi_camera3_static_quirks_use_partial_result_identity_t)
                    }
                }
            },
            .v = {
                .v = 0 /* guzzi_camera3_byte_t */
            }
        },
        .quirks_use_zsl_format = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_STATIC_QUIRKS_USE_ZSL_FORMAT,
                        .size = sizeof (guzzi_camera3_static_quirks_use_zsl_format_identity_t)
                    }
                }
            },
            .v = {
                .v = 0 /* guzzi_camera3_byte_t */
            }
        },
        .request_max_num_output_streams = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_STATIC_REQUEST_MAX_NUM_OUTPUT_STREAMS,
                        .size = sizeof (guzzi_camera3_static_request_max_num_output_streams_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_STATIC_REQUEST_MAX_NUM_OUTPUT_STREAMS_DIM_MAX_SIZE_1,
                .v = {
                        1,  // RAW
                        2,  // YUV
                        0   // JPEG
                } /* guzzi_camera3_int32_t */
            }
        },
        .request_max_num_reprocess_streams = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_STATIC_REQUEST_MAX_NUM_REPROCESS_STREAMS,
                        .size = sizeof (guzzi_camera3_static_request_max_num_reprocess_streams_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_STATIC_REQUEST_MAX_NUM_REPROCESS_STREAMS_DIM_MAX_SIZE_1,
                .v = {
                        0   // Reprocess not supported
                } /* guzzi_camera3_int32_t */
            }
        },
        .scaler_available_formats = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_STATIC_SCALER_AVAILABLE_FORMATS,
                        .size = sizeof (guzzi_camera3_static_scaler_available_formats_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_STATIC_SCALER_AVAILABLE_FORMATS_DIM_MAX_SIZE_1 - 1,
                .v = {
                        GUZZI_CAMERA3_ENUM_SCALER_AVAILABLE_FORMATS_RAW_SENSOR,
                        //GUZZI_CAMERA3_ENUM_SCALER_AVAILABLE_FORMATS_RAW_OPAQUE,
                        GUZZI_CAMERA3_ENUM_SCALER_AVAILABLE_FORMATS_YV12,
                        GUZZI_CAMERA3_ENUM_SCALER_AVAILABLE_FORMATS_YCRCB_420_SP,
                        GUZZI_CAMERA3_ENUM_SCALER_AVAILABLE_FORMATS_IMPLEMENTATION_DEFINED,
                        GUZZI_CAMERA3_ENUM_SCALER_AVAILABLE_FORMATS_YCBCR_420_888,
                        GUZZI_CAMERA3_ENUM_SCALER_AVAILABLE_FORMATS_BLOB
                } /* guzzi_camera3_int32_t */
            }
        },
        .scaler_available_jpeg_min_durations = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_STATIC_SCALER_AVAILABLE_JPEG_MIN_DURATIONS,
                        .size = sizeof (guzzi_camera3_static_scaler_available_jpeg_min_durations_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_STATIC_SCALER_AVAILABLE_JPEG_MIN_DURATIONS_DIM_MAX_SIZE_1 - 4,
                .v = {
                        37048518LL
                } /* guzzi_camera3_int64_t */
            }
        },
        .scaler_available_jpeg_sizes = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_STATIC_SCALER_AVAILABLE_JPEG_SIZES,
                        .size = sizeof (guzzi_camera3_static_scaler_available_jpeg_sizes_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_STATIC_SCALER_AVAILABLE_JPEG_SIZES_DIM_MAX_SIZE_1,
                .dim_size_2 = GUZZI_CAMERA3_STATIC_SCALER_AVAILABLE_JPEG_SIZES_DIM_MAX_SIZE_2 - 4,
                .v = {
                        4208, 3120
                } /* guzzi_camera3_int32_t */
            }
        },
        .scaler_available_max_digital_zoom = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_STATIC_SCALER_AVAILABLE_MAX_DIGITAL_ZOOM,
                        .size = sizeof (guzzi_camera3_static_scaler_available_max_digital_zoom_identity_t)
                    }
                }
            },
            .v = {
                .v = 1.0f /* guzzi_camera3_float_t */
            }
        },
        .scaler_available_processed_min_durations = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_STATIC_SCALER_AVAILABLE_PROCESSED_MIN_DURATIONS,
                        .size = sizeof (guzzi_camera3_static_scaler_available_processed_min_durations_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_STATIC_SCALER_AVAILABLE_PROCESSED_MIN_DURATIONS_DIM_MAX_SIZE_1 - 7,
                .v = {
                        37048518LL,
                        37048518LL
                } /* guzzi_camera3_int64_t */
            }
        },
        .scaler_available_processed_sizes = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_STATIC_SCALER_AVAILABLE_PROCESSED_SIZES,
                        .size = sizeof (guzzi_camera3_static_scaler_available_processed_sizes_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_STATIC_SCALER_AVAILABLE_PROCESSED_SIZES_DIM_MAX_SIZE_1,
                .dim_size_2 = GUZZI_CAMERA3_STATIC_SCALER_AVAILABLE_PROCESSED_SIZES_DIM_MAX_SIZE_2 - 7,
                .v = {
                        4208  , 3120,
                         856/2,  480/2
                } /* guzzi_camera3_int32_t */
            }
        },
        .scaler_available_raw_min_durations = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_STATIC_SCALER_AVAILABLE_RAW_MIN_DURATIONS,
                        .size = sizeof (guzzi_camera3_static_scaler_available_raw_min_durations_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_STATIC_SCALER_AVAILABLE_RAW_MIN_DURATIONS_DIM_MAX_SIZE_1,
                .v = {
                        37048518LL
                } /* guzzi_camera3_int64_t */
            }
        },
        .scaler_available_raw_sizes = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_STATIC_SCALER_AVAILABLE_RAW_SIZES,
                        .size = sizeof (guzzi_camera3_static_scaler_available_raw_sizes_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_STATIC_SCALER_AVAILABLE_RAW_SIZES_DIM_MAX_SIZE_1,
                .dim_size_2 = GUZZI_CAMERA3_STATIC_SCALER_AVAILABLE_RAW_SIZES_DIM_MAX_SIZE_2,
                .v = {
                        4208, 3120
                } /* guzzi_camera3_int32_t */
            }
        },
        .sensor_base_gain_factor = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_STATIC_SENSOR_BASE_GAIN_FACTOR,
                        .size = sizeof (guzzi_camera3_static_sensor_base_gain_factor_identity_t)
                    }
                }
            },
            .v = {
                .v = {.nom = 1, .denom = 1} /* guzzi_camera3_rational_t */
            }
        },
        .sensor_black_level_pattern = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_STATIC_SENSOR_BLACK_LEVEL_PATTERN,
                        .size = sizeof (guzzi_camera3_static_sensor_black_level_pattern_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_STATIC_SENSOR_BLACK_LEVEL_PATTERN_DIM_MAX_SIZE_1,
                .v = {
                        42, 42, 42, 42
                } /* guzzi_camera3_int32_t */
            }
        },
        .sensor_calibration_transform1 = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_STATIC_SENSOR_CALIBRATION_TRANSFORM1,
                        .size = sizeof (guzzi_camera3_static_sensor_calibration_transform1_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_STATIC_SENSOR_CALIBRATION_TRANSFORM1_DIM_MAX_SIZE_1,
                .v = {{.nom = 0, .denom = 0}} /* guzzi_camera3_rational_t */
            }
        },
        .sensor_calibration_transform2 = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_STATIC_SENSOR_CALIBRATION_TRANSFORM2,
                        .size = sizeof (guzzi_camera3_static_sensor_calibration_transform2_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_STATIC_SENSOR_CALIBRATION_TRANSFORM2_DIM_MAX_SIZE_1,
                .v = {{.nom = 0, .denom = 0}} /* guzzi_camera3_rational_t */
            }
        },
        .sensor_color_transform1 = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_STATIC_SENSOR_COLOR_TRANSFORM1,
                        .size = sizeof (guzzi_camera3_static_sensor_color_transform1_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_STATIC_SENSOR_COLOR_TRANSFORM1_DIM_MAX_SIZE_1,
                .v = {{.nom = 0, .denom = 0}} /* guzzi_camera3_rational_t */
            }
        },
        .sensor_color_transform2 = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_STATIC_SENSOR_COLOR_TRANSFORM2,
                        .size = sizeof (guzzi_camera3_static_sensor_color_transform2_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_STATIC_SENSOR_COLOR_TRANSFORM2_DIM_MAX_SIZE_1,
                .v = {{.nom = 0, .denom = 0}} /* guzzi_camera3_rational_t */
            }
        },
        .sensor_forward_matrix1 = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_STATIC_SENSOR_FORWARD_MATRIX1,
                        .size = sizeof (guzzi_camera3_static_sensor_forward_matrix1_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_STATIC_SENSOR_FORWARD_MATRIX1_DIM_MAX_SIZE_1,
                .v = {{.nom = 0, .denom = 0}} /* guzzi_camera3_rational_t */
            }
        },
        .sensor_forward_matrix2 = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_STATIC_SENSOR_FORWARD_MATRIX2,
                        .size = sizeof (guzzi_camera3_static_sensor_forward_matrix2_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_STATIC_SENSOR_FORWARD_MATRIX2_DIM_MAX_SIZE_1,
                .v = {{.nom = 0, .denom = 0}} /* guzzi_camera3_rational_t */
            }
        },
        .sensor_info_active_array_size = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_STATIC_SENSOR_INFO_ACTIVE_ARRAY_SIZE,
                        .size = sizeof (guzzi_camera3_static_sensor_info_active_array_size_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_STATIC_SENSOR_INFO_ACTIVE_ARRAY_SIZE_DIM_MAX_SIZE_1,
                .v = {
                        0, 0, 4208, 3120
                } /* guzzi_camera3_int32_t */
            }
        },
        .sensor_info_color_filter_arrangement = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_STATIC_SENSOR_INFO_COLOR_FILTER_ARRANGEMENT,
                        .size = sizeof (guzzi_camera3_static_sensor_info_color_filter_arrangement_identity_t)
                    }
                }
            },
            .v = {
                .v = GUZZI_CAMERA3_ENUM_SENSOR_INFO_COLOR_FILTER_ARRANGEMENT_GRBG /* guzzi_camera3_byte_t */
            }
        },
        .sensor_info_exposure_time_range = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_STATIC_SENSOR_INFO_EXPOSURE_TIME_RANGE,
                        .size = sizeof (guzzi_camera3_static_sensor_info_exposure_time_range_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_STATIC_SENSOR_INFO_EXPOSURE_TIME_RANGE_DIM_MAX_SIZE_1,
                .v = {
                        8 * 11538LL,        // Min = 8 rows
                        65534 * 11538LL     // Max = 65k rows
                } /* guzzi_camera3_int64_t */
            }
        },
        .sensor_info_max_frame_duration = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_STATIC_SENSOR_INFO_MAX_FRAME_DURATION,
                        .size = sizeof (guzzi_camera3_static_sensor_info_max_frame_duration_identity_t)
                    }
                }
            },
            .v = {
                .v = (65534 + 1) * 11538LL /* guzzi_camera3_int64_t */
            }
        },
        .sensor_info_physical_size = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_STATIC_SENSOR_INFO_PHYSICAL_SIZE,
                        .size = sizeof (guzzi_camera3_static_sensor_info_physical_size_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_STATIC_SENSOR_INFO_PHYSICAL_SIZE_DIM_MAX_SIZE_1,
                .v = {
                        6.78f, 5.89f
                } /* guzzi_camera3_float_t */
            }
        },
        .sensor_info_pixel_array_size = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_STATIC_SENSOR_INFO_PIXEL_ARRAY_SIZE,
                        .size = sizeof (guzzi_camera3_static_sensor_info_pixel_array_size_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_STATIC_SENSOR_INFO_PIXEL_ARRAY_SIZE_DIM_MAX_SIZE_1,
                .v = {
                        4208, 3120
                } /* guzzi_camera3_int32_t */
            }
        },
        .sensor_info_sensitivity_range = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_STATIC_SENSOR_INFO_SENSITIVITY_RANGE,
                        .size = sizeof (guzzi_camera3_static_sensor_info_sensitivity_range_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_STATIC_SENSOR_INFO_SENSITIVITY_RANGE_DIM_MAX_SIZE_1,
                .v = {
                        100, 1600
                } /* guzzi_camera3_int32_t */
            }
        },
        .sensor_info_white_level = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_STATIC_SENSOR_INFO_WHITE_LEVEL,
                        .size = sizeof (guzzi_camera3_static_sensor_info_white_level_identity_t)
                    }
                }
            },
            .v = {
                .v = 1023 /* guzzi_camera3_int32_t */
            }
        },
        .sensor_max_analog_sensitivity = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_STATIC_SENSOR_MAX_ANALOG_SENSITIVITY,
                        .size = sizeof (guzzi_camera3_static_sensor_max_analog_sensitivity_identity_t)
                    }
                }
            },
            .v = {
                .v = 800 /* guzzi_camera3_int32_t */
            }
        },
        .sensor_noise_model_coefficients = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_STATIC_SENSOR_NOISE_MODEL_COEFFICIENTS,
                        .size = sizeof (guzzi_camera3_static_sensor_noise_model_coefficients_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_STATIC_SENSOR_NOISE_MODEL_COEFFICIENTS_DIM_MAX_SIZE_1,
                .v = {0} /* guzzi_camera3_float_t */
            }
        },
        .sensor_orientation = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_STATIC_SENSOR_ORIENTATION,
                        .size = sizeof (guzzi_camera3_static_sensor_orientation_identity_t)
                    }
                }
            },
            .v = {
                .v = 0 /* guzzi_camera3_int32_t */
            }
        },
        .sensor_reference_illuminant1 = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_STATIC_SENSOR_REFERENCE_ILLUMINANT1,
                        .size = sizeof (guzzi_camera3_static_sensor_reference_illuminant1_identity_t)
                    }
                }
            },
            .v = {
                .v = 0 /* guzzi_camera3_byte_t */
            }
        },
        .sensor_reference_illuminant2 = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_STATIC_SENSOR_REFERENCE_ILLUMINANT2,
                        .size = sizeof (guzzi_camera3_static_sensor_reference_illuminant2_identity_t)
                    }
                }
            },
            .v = {
                .v = 0 /* guzzi_camera3_byte_t */
            }
        },
        .statistics_info_available_face_detect_modes = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_STATIC_STATISTICS_INFO_AVAILABLE_FACE_DETECT_MODES,
                        .size = sizeof (guzzi_camera3_static_statistics_info_available_face_detect_modes_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_STATIC_STATISTICS_INFO_AVAILABLE_FACE_DETECT_MODES_DIM_MAX_SIZE_1 - 2,
                .v = {
                        GUZZI_CAMERA3_ENUM_STATISTICS_FACE_DETECT_MODE_OFF,
                        //GUZZI_CAMERA3_ENUM_STATISTICS_FACE_DETECT_MODE_SIMPLE,
                        //GUZZI_CAMERA3_ENUM_STATISTICS_FACE_DETECT_MODE_FULL
                } /* guzzi_camera3_byte_t */
            }
        },
        .statistics_info_histogram_bucket_count = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_STATIC_STATISTICS_INFO_HISTOGRAM_BUCKET_COUNT,
                        .size = sizeof (guzzi_camera3_static_statistics_info_histogram_bucket_count_identity_t)
                    }
                }
            },
            .v = {
                .v = GUZZI_CAMERA3_DYNAMIC_STATISTICS_HISTOGRAM_DIM_MAX_SIZE_2 /* guzzi_camera3_int32_t */
            }
        },
        .statistics_info_max_face_count = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_STATIC_STATISTICS_INFO_MAX_FACE_COUNT,
                        .size = sizeof (guzzi_camera3_static_statistics_info_max_face_count_identity_t)
                    }
                }
            },
            .v = {
                .v = 0 //GUZZI_CAMERA3_DYNAMIC_STATISTICS_FACE_SCORES_DIM_MAX_SIZE_1 /* guzzi_camera3_int32_t */
            }
        },
        .statistics_info_max_histogram_count = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_STATIC_STATISTICS_INFO_MAX_HISTOGRAM_COUNT,
                        .size = sizeof (guzzi_camera3_static_statistics_info_max_histogram_count_identity_t)
                    }
                }
            },
            .v = {
                .v = 1023 /* guzzi_camera3_int32_t */
            }
        },
        .statistics_info_max_sharpness_map_value = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_STATIC_STATISTICS_INFO_MAX_SHARPNESS_MAP_VALUE,
                        .size = sizeof (guzzi_camera3_static_statistics_info_max_sharpness_map_value_identity_t)
                    }
                }
            },
            .v = {
                .v = 1023 /* guzzi_camera3_int32_t */
            }
        },
        .statistics_info_sharpness_map_size = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_STATIC_STATISTICS_INFO_SHARPNESS_MAP_SIZE,
                        .size = sizeof (guzzi_camera3_static_statistics_info_sharpness_map_size_identity_t)
                    }
                }
            },
            .v = {
                .dim_size_1 = GUZZI_CAMERA3_STATIC_STATISTICS_INFO_SHARPNESS_MAP_SIZE_DIM_MAX_SIZE_1,
                .v = {
                        GUZZI_CAMERA3_DYNAMIC_STATISTICS_SHARPNESS_MAP_DIM_MAX_SIZE_2,
                        GUZZI_CAMERA3_DYNAMIC_STATISTICS_SHARPNESS_MAP_DIM_MAX_SIZE_3
                } /* guzzi_camera3_int32_t */
            }
        },
        .tonemap_max_curve_points = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_STATIC_TONEMAP_MAX_CURVE_POINTS,
                        .size = sizeof (guzzi_camera3_static_tonemap_max_curve_points_identity_t)
                    }
                }
            },
            .v = {
                .v = GUZZI_CAMERA3_DYNAMIC_TONEMAP_CURVE_RED_DIM_MAX_SIZE_2 /* guzzi_camera3_int32_t */
            }
        },
        .metadata_end_marker = {
            .identity = {
                {
                    {
                        .index = GUZZI_CAMERA3_INDEX_METADATA_END_MARKER,
                        .size = 0
                    }
                }
            }
        }
    }
};
