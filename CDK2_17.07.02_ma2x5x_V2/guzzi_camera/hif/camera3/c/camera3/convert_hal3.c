/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file convert_hal3.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <system/camera_metadata.h>
#include <guzzi/camera3/metadata_index.h>
#include <guzzi/camera3/metadata.h>
#include <guzzi/camera3/helpers.h>
#include <guzzi/camera3/convert_hal3.h>

static int add_guzzi_entry_to_metadata(
        camera_metadata_t *metadata,
        guzzi_camera3_metadata_type_identity_t *guzzi_entry
    )
{
    guzzi_camera3_metadata_guzzi2hal_t *convert_entry;
    int err;

    convert_entry = guzzi_camera3_metadata_index_to_convert(
            guzzi_entry->identity.index
        );
    if (!convert_entry) {
        return -1;
    }

    err = convert_entry->func(metadata, guzzi_entry);
    if (err) {
        return -1;
    }

    return 0;
}

int guzzi_camera3_metadata_convert_hal2guzzi(
        void *guzzi,
        camera_metadata_t *metadata,
        int max_size
    )
{
    guzzi_camera3_metadata_type_identity_t *guzzi_entry;
    camera_metadata_entry_t hal_entry;
    guzzi_camera3_metadata_helper_t *helper;
    guzzi_camera3_metadata_hal2guzzi_t *convert_entry;
    int entry_count;
    int pos;
    int i;
    int err;

    entry_count = get_camera_metadata_entry_count(metadata);

    pos = 0;
    for (i = 0; i < entry_count; i++) {
        err =  get_camera_metadata_entry(metadata, i, &hal_entry);
        if (err) {
            return -1;
        }
        helper = guzzi_camera3_metadata_controls_index_to_helper(
                guzzi_camera3_metadata_controls_tag2idx(hal_entry.tag)
            );
        if (!helper) {
            continue;
        }
        convert_entry = guzzi_camera3_metadata_tag_to_convert(hal_entry.tag);
        if (!convert_entry) {
            return -1;
        }
        if (max_size < pos + helper->ident_size) {
            return -1;
        }

        guzzi_entry = (void *)((char *)guzzi + pos);
        guzzi_entry->identity.index = helper->index;
        guzzi_entry->identity.size = helper->ident_size;
        guzzi_entry->arr2.dim_size_1 = helper->dim_size_1;
        err = convert_entry->func(guzzi_entry, &hal_entry);
        if (err) {
            return -1;
        }

        pos += guzzi_entry->identity.size;
    }

    if (max_size < pos + sizeof (guzzi_camera3_metadata_t)) {
        return -1;
    }
    guzzi_entry = (void *)((char *)guzzi + pos);
    guzzi_entry->identity.index = GUZZI_CAMERA3_INDEX_METADATA_END_MARKER;
    guzzi_entry->identity.size = sizeof (guzzi_camera3_metadata_t);

    return 0;
}

int guzzi_camera3_metadata_convert_guzzi2hal_controls(
        camera_metadata_t *metadata,
        guzzi_camera3_metadata_controls_t *guzzi
    )
{
    guzzi_camera3_metadata_type_identity_t *guzzi_entry;
    for_each_valid_metadata(controls, guzzi, guzzi_entry) {
        if (add_guzzi_entry_to_metadata(metadata, guzzi_entry))
            return -1;
    }
    return 0;
}

int guzzi_camera3_metadata_convert_guzzi2hal_dynamic(
        camera_metadata_t *metadata,
        guzzi_camera3_metadata_dynamic_t *guzzi
    )
{
    guzzi_camera3_metadata_type_identity_t *guzzi_entry;
    for_each_valid_metadata(dynamic, guzzi, guzzi_entry) {
        if (add_guzzi_entry_to_metadata(metadata, guzzi_entry))
            return -1;
    }
    return 0;
}

int guzzi_camera3_metadata_convert_guzzi2hal_static(
        camera_metadata_t *metadata,
        guzzi_camera3_metadata_static_t *guzzi
    )
{
    guzzi_camera3_metadata_type_identity_t *guzzi_entry;
    for_each_valid_metadata(static, guzzi, guzzi_entry) {
        if (add_guzzi_entry_to_metadata(metadata, guzzi_entry))
            return -1;
    }
    return 0;
}

