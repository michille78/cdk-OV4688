/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file struct_helpers.h
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#ifndef _CAMERA_EXTERNAL_STRUCT_HELPERS_H
#define _CAMERA_EXTERNAL_STRUCT_HELPERS_H

#ifdef __cplusplus
extern "C" {
#endif

#include <guzzi/camera3/metadata_index.h>

#define for_each_ext_helper(HELPER) \
    for ((HELPER) = camera_external_next_helper(NULL); (HELPER); (HELPER) = camera_external_next_helper(HELPER))

typedef struct configurator configurator_t;

typedef struct {
    char *index_name;
    char *struct_name;
    unsigned int offset;
    unsigned int size;
    guzzi_camera3_metadata_index_t index;
    int (*convert)(
            configurator_t *c,
            void *plugin_prv,
            void *config
        );
} camera_external_struct_helper_t;

camera_external_struct_helper_t *camera_external_index_to_helper(
        guzzi_camera3_metadata_index_t index
    );
camera_external_struct_helper_t *camera_external_next_helper(
        camera_external_struct_helper_t *helper
    );

#ifdef __cplusplus
}
#endif

#endif /* _CAMERA_EXTERNAL_STRUCT_HELPERS_H */
