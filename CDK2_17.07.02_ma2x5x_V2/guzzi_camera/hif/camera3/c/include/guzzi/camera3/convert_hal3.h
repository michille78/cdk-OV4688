/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file convert_hal3.h
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#ifndef _GUZZI_CAMERA3_CONVERT_HAL3_H
#define _GUZZI_CAMERA3_CONVERT_HAL3_H

#ifdef __cplusplus
extern "C" {
#endif

#include <system/camera_metadata.h>
#include <guzzi/camera3/metadata_index.h>
#include <guzzi/camera3/metadata.h>

typedef int guzzi_camera3_metadata_conv_func_hal2guzzi_t(
        guzzi_camera3_metadata_type_identity_t *guzzi,
        camera_metadata_entry_t *hal
    );

typedef int guzzi_camera3_metadata_conv_func_guzzi2hal_t(
        camera_metadata_t *metadata,
        guzzi_camera3_metadata_type_identity_t *guzzi
    );

typedef struct {
    guzzi_camera3_metadata_conv_func_hal2guzzi_t *func;
} guzzi_camera3_metadata_hal2guzzi_t;

typedef struct {
    guzzi_camera3_metadata_conv_func_guzzi2hal_t *func;
} guzzi_camera3_metadata_guzzi2hal_t;

guzzi_camera3_metadata_hal2guzzi_t *guzzi_camera3_metadata_tag_to_convert(
        camera_metadata_tag_t tag
    );
guzzi_camera3_metadata_guzzi2hal_t *guzzi_camera3_metadata_index_to_convert(
        guzzi_camera3_metadata_index_t index
    );

guzzi_camera3_metadata_index_t guzzi_camera3_metadata_controls_tag2idx(
        camera_metadata_tag_t tag
    );
camera_metadata_tag_t guzzi_camera3_metadata_idx2tag(
        guzzi_camera3_metadata_index_t index
    );



int guzzi_camera3_metadata_convert_hal2guzzi(
        void *guzzi,
        camera_metadata_t *metadata,
        int max_size
    );

int guzzi_camera3_metadata_convert_guzzi2hal_controls(
        camera_metadata_t *metadata,
        guzzi_camera3_metadata_controls_t *guzzi
    );

int guzzi_camera3_metadata_convert_guzzi2hal_dynamic(
        camera_metadata_t *metadata,
        guzzi_camera3_metadata_dynamic_t *guzzi
    );

int guzzi_camera3_metadata_convert_guzzi2hal_static(
        camera_metadata_t *metadata,
        guzzi_camera3_metadata_static_t *guzzi
    );

#ifdef __cplusplus
}
#endif

#endif /* _GUZZI_CAMERA3_CONVERT_HAL3_H */

