/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file metadata.h
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#ifndef _GUZZI_CAMERA3_METADATA_H
#define _GUZZI_CAMERA3_METADATA_H

#ifdef __cplusplus
extern "C" {
#endif

#include <guzzi/camera3/metadata_struct.h>
#include <guzzi/camera3/helpers.h>

typedef struct {
    union {
        struct {
            guzzi_camera3_int32_t index;
            guzzi_camera3_int32_t size;
        };
        guzzi_camera3_double_t d;
        guzzi_camera3_int64_t i64;
    };
} guzzi_camera3_metadata_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    char v[0];
} guzzi_camera3_metadata_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    union {
        struct {
            guzzi_camera3_double_t v;
        } d;
        struct {
            guzzi_camera3_int32_t v;
        } i32;
        struct {
            guzzi_camera3_int64_t v;
        } i64;
        struct {
            guzzi_camera3_byte_t v;
        } u8;
        struct {
            guzzi_camera3_float_t v;
        } f;
        struct {
            guzzi_camera3_rational_t v;
        } r;
        struct {
            guzzi_camera3_int32_t dim_size_1;
            guzzi_camera3_int32_t dummy;
            char v[0];
        } arr1;
        struct {
            guzzi_camera3_int32_t dim_size_1;
            guzzi_camera3_int32_t dim_size_2;
            char v[0];
        } arr2;
        struct {
            guzzi_camera3_int32_t dim_size_1;
            guzzi_camera3_int32_t dim_size_2;
            guzzi_camera3_int32_t dim_size_3;
            guzzi_camera3_int32_t dummy;
            char v[0];
        } arr3;
        struct {
            guzzi_camera3_int32_t dim_size_1;
            guzzi_camera3_int32_t dim_size_2;
            guzzi_camera3_int32_t dim_size_3;
            guzzi_camera3_int32_t dim_size_4;
            char v[0];
        } arr4;
    };
} guzzi_camera3_metadata_type_identity_t;

#include <guzzi/auto_gen/camera3/metadata.h>

/*
 * Expands to one of guzzi_camera3_metadata_controls_struct_t, guzzi_camera3_metadata_dynamic_struct_t
 * or guzzi_camera3_metadata_static_struct_t
 *
 * KIND - controls, dynamic ot static
 * m - pointer to identity metadata structure (guzzi_camera3_metadata_identity_t)
 *     which MUST be a part of initialized guzzi_camera3_metadata_controls_t,
 *     guzzi_camera3_metadata_dynamic_t or guzzi_camera3_metadata_static_t structure
 */
#define metadata_m_to_M(KIND,m) \
    ((guzzi_camera3_metadata_##KIND##_struct_t *)(((char *)(m)) - guzzi_camera3_metadata_##KIND##_index_to_helper((m)->identity.index)->offset))

/*
 * Expands to one of guzzi_camera3_metadata_controls_t, guzzi_camera3_metadata_dynamic_t
 * or guzzi_camera3_metadata_static_t
 * 
 * KIND - controls, dynamic ot static
 * m - pointer to identity metadata structure (guzzi_camera3_metadata_identity_t)
 *     which MUST be a part of initialized guzzi_camera3_metadata_controls_t,
 *     guzzi_camera3_metadata_dynamic_t or guzzi_camera3_metadata_static_t structure
 */
#define metadata_m_to_MV(KIND,m) \
    ((guzzi_camera3_metadata_##KIND##_t *)(((char *)(m)) - guzzi_camera3_metadata_##KIND##_index_to_helper((m)->identity.index)->s_offset))

/*
 * KIND - controls, dynamic ot static
 * m - pointer to identity metadata structure (guzzi_camera3_metadata_identity_t)
 *     which MUST be a part of initialized guzzi_camera3_metadata_controls_t,
 *     guzzi_camera3_metadata_dynamic_t or guzzi_camera3_metadata_static_t structure
 */
#define metadata_is_valid(KIND,m) \
    (*(int *)((char *)metadata_m_to_MV(KIND,m) + guzzi_camera3_metadata_##KIND##_index_to_helper((m)->identity.index)->v_offset) != 0) 

/*
 * KIND - controls, dynamic ot static
 * MV   - pointer to initialized guzzi_camera3_metadata_controls_t, guzzi_camera3_metadata_dynamic_t
 *        or guzzi_camera3_metadata_static_t
 * m    - pointer to identity metadata structure (guzzi_camera3_metadata_identity_t)
 *        which MUST be a part of initialized guzzi_camera3_metadata_controls_t,
 *        guzzi_camera3_metadata_dynamic_t or guzzi_camera3_metadata_static_t structure
 */
#define metadata_is_valid2(KIND,MV,m) \
    (*(int *)((char *)(MV) + guzzi_camera3_metadata_##KIND##_index_to_helper((m)->identity.index)->v_offset) != 0) 

/*
 * M - initialized stream buffer of identity metadata
 * m - pointer to N'th identity metadata (guzzi_camera3_metadata_identity_t)
 */
#define for_each_metadata(M,m) \
    for ( (m) = (void *)(M); \
          (m)->identity.index != GUZZI_CAMERA3_INDEX_METADATA_END_MARKER; \
          (m) = (void *)(((char *)(m)) + (m)->identity.size) \
         )

/*
 * MV - pointer to initialized guzzi_camera3_metadata_controls_t, guzzi_camera3_metadata_dynamic_t
 *      or guzzi_camera3_metadata_static_t
 * m  - pointer to N'th identity metadata (guzzi_camera3_metadata_identity_t)
 */
#define for_each_valid_metadata(KIND,MV,m) \
    for ( (m) = (void *)&(MV)->s; \
          (m)->identity.index != GUZZI_CAMERA3_INDEX_METADATA_END_MARKER; \
          (m) = (void *)(((char *)(m)) + (m)->identity.size) \
         ) \
        if (metadata_is_valid2(KIND,MV,m))

#ifdef __cplusplus
}
#endif

#endif /* _GUZZI_CAMERA3_METADATA_H */

