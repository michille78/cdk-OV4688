/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file camera3.h
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#ifndef _GUZZI_CAMERA3_H
#define _GUZZI_CAMERA3_H

#ifdef __cplusplus
extern "C" {
#endif

#include <guzzi/camera3/metadata.h>

#define GUZZI_CAMERA3_STREAMS_MAX 8

typedef struct guzzi_camera3 guzzi_camera3_t;

/*
 * ****************************************************************************
 * ** Camera info *************************************************************
 * ****************************************************************************
 */
int guzzi_camera3_get_number_of_cameras(void);

#define GUZZI_CAMERA3_FACING_BACK 0
#define GUZZI_CAMERA3_FACING_FRONT 1

typedef struct {
    int facing;
    int orientation; /* 0, 90, 180, 270 */
    guzzi_camera3_metadata_static_t *metadata_static;
} guzzi_camera3_info_t;

int guzzi_camera3_get_info(
        int camera_id,
        guzzi_camera3_info_t *gc3_info
    );

/*
 * ****************************************************************************
 * ** Helpers *****************************************************************
 * ****************************************************************************
 */
int guzzi_camera3_get_id(guzzi_camera3_t *gc3);

/*
 * ****************************************************************************
 * ** Callback ****************************************************************
 * ****************************************************************************
 */
typedef enum {
    GUZZI_CAMERA3_EVENT_SHUTTER,
    GUZZI_CAMERA3_EVENT_ERROR, /* TODO */
} guzzi_camera3_event_t;

typedef struct {
    unsigned int frame_number;
    unsigned long long timestamp; /* TODO */
} guzzi_camera3_event_shutter_t;

typedef void guzzi_camera3_calback_notify_t(
        guzzi_camera3_t *gc3,
        void *prv,
        guzzi_camera3_event_t event,
        int num,
        void *ptr
    );

/*
 * ****************************************************************************
 * ** Request default *********************************************************
 * ****************************************************************************
 */
typedef enum {
    GUZZI_CAMERA3_TEMPLATE_PREVIEW,
    GUZZI_CAMERA3_TEMPLATE_STILL_CAPTURE,
    GUZZI_CAMERA3_TEMPLATE_VIDEO_RECORD,
    GUZZI_CAMERA3_TEMPLATE_VIDEO_SNAPSHOT,
    GUZZI_CAMERA3_TEMPLATE_ZERO_SHUTTER_LAG
} guzzi_camera3_template_t;

int guzzi_camera3_request_defaults(
        guzzi_camera3_t *gc3,
        guzzi_camera3_metadata_controls_t *metadata,
        guzzi_camera3_template_t type
    );

/*
 * ****************************************************************************
 * ** Stream Config ***********************************************************
 * ****************************************************************************
 */
typedef enum {
    GUZZI_CAMERA3_STREAM_TYPE_OUTPUT,
    GUZZI_CAMERA3_STREAM_TYPE_INPUT,
    GUZZI_CAMERA3_STREAM_TYPE_BIDIR
} guzzi_camera3_stream_type_t;

typedef enum {
    GUZZI_CAMERA3_IMAGE_FORMAT_RAW10,
    GUZZI_CAMERA3_IMAGE_FORMAT_UYVY,
    GUZZI_CAMERA3_IMAGE_FORMAT_NV12
} guzzi_camera3_image_format_t;

typedef struct {
    unsigned int id;
    guzzi_camera3_stream_type_t type;
    guzzi_camera3_image_format_t format;
    unsigned int width;
    unsigned int height;
} guzzi_camera3_stream_t;

typedef struct {
    unsigned int num_streams;
    guzzi_camera3_stream_t streams[GUZZI_CAMERA3_STREAMS_MAX];
} guzzi_camera3_stream_configuration_t;

int guzzi_camera3_config_streams(
        guzzi_camera3_t *gc3,
        guzzi_camera3_stream_configuration_t *stream_configuration
    );

/*
 * ****************************************************************************
 * ** Capture Request *********************************************************
 * ****************************************************************************
 */
typedef struct {
    unsigned int frame_number;
    guzzi_camera3_metadata_t *settings;
    unsigned int streams_id[GUZZI_CAMERA3_STREAMS_MAX];
} guzzi_camera3_capture_request_t;

int guzzi_camera3_capture_request(
        guzzi_camera3_t *gc3,
        guzzi_camera3_capture_request_t *capture_request
    );

void guzzi_camera3_flush(guzzi_camera3_t *gc3);

/*
 * ****************************************************************************
 * ** Create/Destroy **********************************************************
 * ****************************************************************************
 */
void guzzi_camera3_destroy(guzzi_camera3_t *gc3);

guzzi_camera3_t *guzzi_camera3_create(
        int camera_id,
        guzzi_camera3_calback_notify_t *callback_notify,
        void *client_prv
    );

#ifdef __cplusplus
}
#endif

#endif /* _GUZZI_CAMERA3_H */

