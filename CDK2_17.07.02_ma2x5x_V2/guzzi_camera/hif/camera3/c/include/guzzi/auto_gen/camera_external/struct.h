/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file struct.h
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

/*
 * ****************************************************************************
 * * Control Metadata **********************************************************
 * ****************************************************************************
 */
typedef struct {
    guzzi_camera3_controls_black_level_lock_t black_level_lock;
    guzzi_camera3_controls_color_correction_gains_t color_correction_gains;
    guzzi_camera3_controls_color_correction_mode_t color_correction_mode;
    guzzi_camera3_controls_color_correction_transform_t color_correction_transform;
    guzzi_camera3_controls_control_ae_antibanding_mode_t control_ae_antibanding_mode;
    guzzi_camera3_controls_control_ae_exposure_compensation_t control_ae_exposure_compensation;
    guzzi_camera3_controls_control_ae_lock_t control_ae_lock;
    guzzi_camera3_controls_control_ae_mode_t control_ae_mode;
    guzzi_camera3_controls_control_ae_precapture_trigger_t control_ae_precapture_trigger;
    guzzi_camera3_controls_control_ae_regions_t control_ae_regions;
    guzzi_camera3_controls_control_ae_target_fps_range_t control_ae_target_fps_range;
    guzzi_camera3_controls_control_af_mode_t control_af_mode;
    guzzi_camera3_controls_control_af_regions_t control_af_regions;
    guzzi_camera3_controls_control_af_trigger_t control_af_trigger;
    guzzi_camera3_controls_control_awb_lock_t control_awb_lock;
    guzzi_camera3_controls_control_awb_mode_t control_awb_mode;
    guzzi_camera3_controls_control_awb_regions_t control_awb_regions;
    guzzi_camera3_controls_control_capture_intent_t control_capture_intent;
    guzzi_camera3_controls_control_effect_mode_t control_effect_mode;
    guzzi_camera3_controls_control_mode_t control_mode;
    guzzi_camera3_controls_control_scene_mode_t control_scene_mode;
    guzzi_camera3_controls_control_video_stabilization_mode_t control_video_stabilization_mode;
    guzzi_camera3_controls_demosaic_mode_t demosaic_mode;
    guzzi_camera3_controls_edge_mode_t edge_mode;
    guzzi_camera3_controls_edge_strength_t edge_strength;
    guzzi_camera3_controls_flash_firing_power_t flash_firing_power;
    guzzi_camera3_controls_flash_firing_time_t flash_firing_time;
    guzzi_camera3_controls_flash_mode_t flash_mode;
    guzzi_camera3_controls_geometric_mode_t geometric_mode;
    guzzi_camera3_controls_geometric_strength_t geometric_strength;
    guzzi_camera3_controls_hot_pixel_mode_t hot_pixel_mode;
    guzzi_camera3_controls_jpeg_gps_coordinates_t jpeg_gps_coordinates;
    guzzi_camera3_controls_jpeg_gps_processing_method_t jpeg_gps_processing_method;
    guzzi_camera3_controls_jpeg_gps_timestamp_t jpeg_gps_timestamp;
    guzzi_camera3_controls_jpeg_orientation_t jpeg_orientation;
    guzzi_camera3_controls_jpeg_quality_t jpeg_quality;
    guzzi_camera3_controls_jpeg_thumbnail_quality_t jpeg_thumbnail_quality;
    guzzi_camera3_controls_jpeg_thumbnail_size_t jpeg_thumbnail_size;
    guzzi_camera3_controls_led_transmit_t led_transmit;
    guzzi_camera3_controls_lens_aperture_t lens_aperture;
    guzzi_camera3_controls_lens_filter_density_t lens_filter_density;
    guzzi_camera3_controls_lens_focal_length_t lens_focal_length;
    guzzi_camera3_controls_lens_focus_distance_t lens_focus_distance;
    guzzi_camera3_controls_lens_optical_stabilization_mode_t lens_optical_stabilization_mode;
    guzzi_camera3_controls_noise_reduction_mode_t noise_reduction_mode;
    guzzi_camera3_controls_noise_reduction_strength_t noise_reduction_strength;
    guzzi_camera3_controls_request_frame_count_t request_frame_count;
    guzzi_camera3_controls_request_id_t request_id;
    guzzi_camera3_controls_request_input_streams_t request_input_streams;
    guzzi_camera3_controls_request_metadata_mode_t request_metadata_mode;
    guzzi_camera3_controls_request_output_streams_t request_output_streams;
    guzzi_camera3_controls_request_type_t request_type;
    guzzi_camera3_controls_scaler_crop_region_t scaler_crop_region;
    guzzi_camera3_controls_sensor_exposure_time_t sensor_exposure_time;
    guzzi_camera3_controls_sensor_frame_duration_t sensor_frame_duration;
    guzzi_camera3_controls_sensor_sensitivity_t sensor_sensitivity;
    guzzi_camera3_controls_shading_mode_t shading_mode;
    guzzi_camera3_controls_shading_strength_t shading_strength;
    guzzi_camera3_controls_statistics_face_detect_mode_t statistics_face_detect_mode;
    guzzi_camera3_controls_statistics_histogram_mode_t statistics_histogram_mode;
    guzzi_camera3_controls_statistics_lens_shading_map_mode_t statistics_lens_shading_map_mode;
    guzzi_camera3_controls_statistics_sharpness_map_mode_t statistics_sharpness_map_mode;
    guzzi_camera3_controls_tonemap_curve_blue_t tonemap_curve_blue;
    guzzi_camera3_controls_tonemap_curve_green_t tonemap_curve_green;
    guzzi_camera3_controls_tonemap_curve_red_t tonemap_curve_red;
    guzzi_camera3_controls_tonemap_mode_t tonemap_mode;

    /* Guzzi specific */
    guzzi_camera3_controls_capture_request_active_t capture_request_active;
    guzzi_camera3_controls_capture_request_frame_number_t capture_request_frame_number;
    guzzi_camera3_controls_capture_request_guzzi_fr_id_t capture_request_guzzi_fr_id;
    guzzi_camera3_controls_stream_config_format_t stream_config_format;
    guzzi_camera3_controls_stream_config_height_t stream_config_height;
    guzzi_camera3_controls_stream_config_id_t stream_config_id;
    guzzi_camera3_controls_stream_config_type_t stream_config_type;
    guzzi_camera3_controls_stream_config_width_t stream_config_width;
    guzzi_camera3_controls_z_custom_capture_number_shots_t z_custom_capture_number_shots;
    guzzi_camera3_controls_z_custom_capture_mode_selection_t z_custom_capture_mode_selection;
    guzzi_camera3_controls_z_custom_crtl_brigthness_t z_custom_crtl_brigthness;
    guzzi_camera3_controls_z_custom_crtl_contrast_t z_custom_crtl_contrast;
    guzzi_camera3_controls_z_custom_crtl_hue_t z_custom_crtl_hue;
    guzzi_camera3_controls_z_custom_crtl_saturation_t z_custom_crtl_saturation;
    guzzi_camera3_controls_z_custom_exposure_bracketing_sequence_t z_custom_exposure_bracketing_sequence;
    guzzi_camera3_controls_z_custom_usecase_selection_t z_custom_usecase_selection;
    guzzi_camera3_controls_z_exposure_merger_weight_t z_exposure_merger_weight;
    guzzi_camera3_controls_z_wb_manual_temperature_t z_wb_manual_temperature;
    guzzi_camera3_controls_z_white_balance_merger_weight_t z_white_balance_merger_weight;
} camera_external_struct_t;

