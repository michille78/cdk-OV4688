/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file metadata.h
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

/*
 * ****************************************************************************
 * * Control Metadata Identity ************************************************
 * ****************************************************************************
 */
typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_controls_black_level_lock_t v;
} guzzi_camera3_controls_black_level_lock_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_controls_color_correction_gains_t v;
} guzzi_camera3_controls_color_correction_gains_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_controls_color_correction_mode_t v;
} guzzi_camera3_controls_color_correction_mode_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_controls_color_correction_transform_t v;
} guzzi_camera3_controls_color_correction_transform_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_controls_control_ae_antibanding_mode_t v;
} guzzi_camera3_controls_control_ae_antibanding_mode_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_controls_control_ae_exposure_compensation_t v;
} guzzi_camera3_controls_control_ae_exposure_compensation_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_controls_control_ae_lock_t v;
} guzzi_camera3_controls_control_ae_lock_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_controls_control_ae_mode_t v;
} guzzi_camera3_controls_control_ae_mode_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_controls_control_ae_precapture_trigger_t v;
} guzzi_camera3_controls_control_ae_precapture_trigger_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_controls_control_ae_regions_t v;
} guzzi_camera3_controls_control_ae_regions_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_controls_control_ae_target_fps_range_t v;
} guzzi_camera3_controls_control_ae_target_fps_range_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_controls_control_af_mode_t v;
} guzzi_camera3_controls_control_af_mode_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_controls_control_af_regions_t v;
} guzzi_camera3_controls_control_af_regions_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_controls_control_af_trigger_t v;
} guzzi_camera3_controls_control_af_trigger_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_controls_control_awb_lock_t v;
} guzzi_camera3_controls_control_awb_lock_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_controls_control_awb_mode_t v;
} guzzi_camera3_controls_control_awb_mode_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_controls_control_awb_regions_t v;
} guzzi_camera3_controls_control_awb_regions_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_controls_control_capture_intent_t v;
} guzzi_camera3_controls_control_capture_intent_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_controls_control_effect_mode_t v;
} guzzi_camera3_controls_control_effect_mode_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_controls_control_mode_t v;
} guzzi_camera3_controls_control_mode_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_controls_control_scene_mode_t v;
} guzzi_camera3_controls_control_scene_mode_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_controls_control_video_stabilization_mode_t v;
} guzzi_camera3_controls_control_video_stabilization_mode_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_controls_demosaic_mode_t v;
} guzzi_camera3_controls_demosaic_mode_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_controls_edge_mode_t v;
} guzzi_camera3_controls_edge_mode_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_controls_edge_strength_t v;
} guzzi_camera3_controls_edge_strength_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_controls_flash_firing_power_t v;
} guzzi_camera3_controls_flash_firing_power_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_controls_flash_firing_time_t v;
} guzzi_camera3_controls_flash_firing_time_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_controls_flash_mode_t v;
} guzzi_camera3_controls_flash_mode_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_controls_geometric_mode_t v;
} guzzi_camera3_controls_geometric_mode_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_controls_geometric_strength_t v;
} guzzi_camera3_controls_geometric_strength_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_controls_hot_pixel_mode_t v;
} guzzi_camera3_controls_hot_pixel_mode_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_controls_jpeg_gps_coordinates_t v;
} guzzi_camera3_controls_jpeg_gps_coordinates_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_controls_jpeg_gps_processing_method_t v;
} guzzi_camera3_controls_jpeg_gps_processing_method_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_controls_jpeg_gps_timestamp_t v;
} guzzi_camera3_controls_jpeg_gps_timestamp_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_controls_jpeg_orientation_t v;
} guzzi_camera3_controls_jpeg_orientation_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_controls_jpeg_quality_t v;
} guzzi_camera3_controls_jpeg_quality_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_controls_jpeg_thumbnail_quality_t v;
} guzzi_camera3_controls_jpeg_thumbnail_quality_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_controls_jpeg_thumbnail_size_t v;
} guzzi_camera3_controls_jpeg_thumbnail_size_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_controls_led_transmit_t v;
} guzzi_camera3_controls_led_transmit_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_controls_lens_aperture_t v;
} guzzi_camera3_controls_lens_aperture_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_controls_lens_filter_density_t v;
} guzzi_camera3_controls_lens_filter_density_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_controls_lens_focal_length_t v;
} guzzi_camera3_controls_lens_focal_length_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_controls_lens_focus_distance_t v;
} guzzi_camera3_controls_lens_focus_distance_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_controls_lens_optical_stabilization_mode_t v;
} guzzi_camera3_controls_lens_optical_stabilization_mode_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_controls_noise_reduction_mode_t v;
} guzzi_camera3_controls_noise_reduction_mode_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_controls_noise_reduction_strength_t v;
} guzzi_camera3_controls_noise_reduction_strength_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_controls_request_frame_count_t v;
} guzzi_camera3_controls_request_frame_count_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_controls_request_id_t v;
} guzzi_camera3_controls_request_id_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_controls_request_input_streams_t v;
} guzzi_camera3_controls_request_input_streams_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_controls_request_metadata_mode_t v;
} guzzi_camera3_controls_request_metadata_mode_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_controls_request_output_streams_t v;
} guzzi_camera3_controls_request_output_streams_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_controls_request_type_t v;
} guzzi_camera3_controls_request_type_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_controls_scaler_crop_region_t v;
} guzzi_camera3_controls_scaler_crop_region_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_controls_sensor_exposure_time_t v;
} guzzi_camera3_controls_sensor_exposure_time_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_controls_sensor_frame_duration_t v;
} guzzi_camera3_controls_sensor_frame_duration_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_controls_sensor_sensitivity_t v;
} guzzi_camera3_controls_sensor_sensitivity_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_controls_shading_mode_t v;
} guzzi_camera3_controls_shading_mode_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_controls_shading_strength_t v;
} guzzi_camera3_controls_shading_strength_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_controls_statistics_face_detect_mode_t v;
} guzzi_camera3_controls_statistics_face_detect_mode_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_controls_statistics_histogram_mode_t v;
} guzzi_camera3_controls_statistics_histogram_mode_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_controls_statistics_lens_shading_map_mode_t v;
} guzzi_camera3_controls_statistics_lens_shading_map_mode_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_controls_statistics_sharpness_map_mode_t v;
} guzzi_camera3_controls_statistics_sharpness_map_mode_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_controls_tonemap_curve_blue_t v;
} guzzi_camera3_controls_tonemap_curve_blue_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_controls_tonemap_curve_green_t v;
} guzzi_camera3_controls_tonemap_curve_green_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_controls_tonemap_curve_red_t v;
} guzzi_camera3_controls_tonemap_curve_red_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_controls_tonemap_mode_t v;
} guzzi_camera3_controls_tonemap_mode_identity_t;

/*
 * ****************************************************************************
 * * Dynamic Metadata Identity ************************************************
 * ****************************************************************************
 */
typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_dynamic_black_level_lock_t v;
} guzzi_camera3_dynamic_black_level_lock_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_dynamic_color_correction_gains_t v;
} guzzi_camera3_dynamic_color_correction_gains_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_dynamic_color_correction_transform_t v;
} guzzi_camera3_dynamic_color_correction_transform_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_dynamic_control_ae_precapture_id_t v;
} guzzi_camera3_dynamic_control_ae_precapture_id_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_dynamic_control_ae_regions_t v;
} guzzi_camera3_dynamic_control_ae_regions_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_dynamic_control_ae_state_t v;
} guzzi_camera3_dynamic_control_ae_state_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_dynamic_control_af_mode_t v;
} guzzi_camera3_dynamic_control_af_mode_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_dynamic_control_af_regions_t v;
} guzzi_camera3_dynamic_control_af_regions_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_dynamic_control_af_state_t v;
} guzzi_camera3_dynamic_control_af_state_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_dynamic_control_af_trigger_id_t v;
} guzzi_camera3_dynamic_control_af_trigger_id_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_dynamic_control_awb_mode_t v;
} guzzi_camera3_dynamic_control_awb_mode_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_dynamic_control_awb_regions_t v;
} guzzi_camera3_dynamic_control_awb_regions_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_dynamic_control_awb_state_t v;
} guzzi_camera3_dynamic_control_awb_state_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_dynamic_control_mode_t v;
} guzzi_camera3_dynamic_control_mode_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_dynamic_edge_mode_t v;
} guzzi_camera3_dynamic_edge_mode_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_dynamic_flash_firing_power_t v;
} guzzi_camera3_dynamic_flash_firing_power_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_dynamic_flash_firing_time_t v;
} guzzi_camera3_dynamic_flash_firing_time_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_dynamic_flash_mode_t v;
} guzzi_camera3_dynamic_flash_mode_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_dynamic_flash_state_t v;
} guzzi_camera3_dynamic_flash_state_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_dynamic_hot_pixel_mode_t v;
} guzzi_camera3_dynamic_hot_pixel_mode_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_dynamic_jpeg_gps_coordinates_t v;
} guzzi_camera3_dynamic_jpeg_gps_coordinates_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_dynamic_jpeg_gps_processing_method_t v;
} guzzi_camera3_dynamic_jpeg_gps_processing_method_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_dynamic_jpeg_gps_timestamp_t v;
} guzzi_camera3_dynamic_jpeg_gps_timestamp_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_dynamic_jpeg_orientation_t v;
} guzzi_camera3_dynamic_jpeg_orientation_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_dynamic_jpeg_quality_t v;
} guzzi_camera3_dynamic_jpeg_quality_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_dynamic_jpeg_size_t v;
} guzzi_camera3_dynamic_jpeg_size_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_dynamic_jpeg_thumbnail_quality_t v;
} guzzi_camera3_dynamic_jpeg_thumbnail_quality_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_dynamic_jpeg_thumbnail_size_t v;
} guzzi_camera3_dynamic_jpeg_thumbnail_size_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_dynamic_led_transmit_t v;
} guzzi_camera3_dynamic_led_transmit_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_dynamic_lens_aperture_t v;
} guzzi_camera3_dynamic_lens_aperture_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_dynamic_lens_filter_density_t v;
} guzzi_camera3_dynamic_lens_filter_density_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_dynamic_lens_focal_length_t v;
} guzzi_camera3_dynamic_lens_focal_length_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_dynamic_lens_focus_distance_t v;
} guzzi_camera3_dynamic_lens_focus_distance_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_dynamic_lens_focus_range_t v;
} guzzi_camera3_dynamic_lens_focus_range_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_dynamic_lens_optical_stabilization_mode_t v;
} guzzi_camera3_dynamic_lens_optical_stabilization_mode_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_dynamic_lens_state_t v;
} guzzi_camera3_dynamic_lens_state_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_dynamic_noise_reduction_mode_t v;
} guzzi_camera3_dynamic_noise_reduction_mode_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_dynamic_quirks_partial_result_t v;
} guzzi_camera3_dynamic_quirks_partial_result_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_dynamic_request_frame_count_t v;
} guzzi_camera3_dynamic_request_frame_count_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_dynamic_request_id_t v;
} guzzi_camera3_dynamic_request_id_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_dynamic_request_metadata_mode_t v;
} guzzi_camera3_dynamic_request_metadata_mode_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_dynamic_request_output_streams_t v;
} guzzi_camera3_dynamic_request_output_streams_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_dynamic_scaler_crop_region_t v;
} guzzi_camera3_dynamic_scaler_crop_region_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_dynamic_sensor_exposure_time_t v;
} guzzi_camera3_dynamic_sensor_exposure_time_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_dynamic_sensor_frame_duration_t v;
} guzzi_camera3_dynamic_sensor_frame_duration_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_dynamic_sensor_sensitivity_t v;
} guzzi_camera3_dynamic_sensor_sensitivity_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_dynamic_sensor_temperature_t v;
} guzzi_camera3_dynamic_sensor_temperature_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_dynamic_sensor_timestamp_t v;
} guzzi_camera3_dynamic_sensor_timestamp_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_dynamic_shading_mode_t v;
} guzzi_camera3_dynamic_shading_mode_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_dynamic_statistics_face_detect_mode_t v;
} guzzi_camera3_dynamic_statistics_face_detect_mode_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_dynamic_statistics_face_ids_t v;
} guzzi_camera3_dynamic_statistics_face_ids_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_dynamic_statistics_face_landmarks_t v;
} guzzi_camera3_dynamic_statistics_face_landmarks_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_dynamic_statistics_face_rectangles_t v;
} guzzi_camera3_dynamic_statistics_face_rectangles_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_dynamic_statistics_face_scores_t v;
} guzzi_camera3_dynamic_statistics_face_scores_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_dynamic_statistics_histogram_t v;
} guzzi_camera3_dynamic_statistics_histogram_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_dynamic_statistics_histogram_mode_t v;
} guzzi_camera3_dynamic_statistics_histogram_mode_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_dynamic_statistics_lens_shading_map_t v;
} guzzi_camera3_dynamic_statistics_lens_shading_map_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_dynamic_statistics_predicted_color_gains_t v;
} guzzi_camera3_dynamic_statistics_predicted_color_gains_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_dynamic_statistics_predicted_color_transform_t v;
} guzzi_camera3_dynamic_statistics_predicted_color_transform_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_dynamic_statistics_scene_flicker_t v;
} guzzi_camera3_dynamic_statistics_scene_flicker_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_dynamic_statistics_sharpness_map_t v;
} guzzi_camera3_dynamic_statistics_sharpness_map_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_dynamic_statistics_sharpness_map_mode_t v;
} guzzi_camera3_dynamic_statistics_sharpness_map_mode_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_dynamic_tonemap_curve_blue_t v;
} guzzi_camera3_dynamic_tonemap_curve_blue_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_dynamic_tonemap_curve_green_t v;
} guzzi_camera3_dynamic_tonemap_curve_green_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_dynamic_tonemap_curve_red_t v;
} guzzi_camera3_dynamic_tonemap_curve_red_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_dynamic_tonemap_mode_t v;
} guzzi_camera3_dynamic_tonemap_mode_identity_t;

/*
 * ****************************************************************************
 * * Static Metadata Identity *************************************************
 * ****************************************************************************
 */
typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_static_control_ae_available_antibanding_modes_t v;
} guzzi_camera3_static_control_ae_available_antibanding_modes_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_static_control_ae_available_modes_t v;
} guzzi_camera3_static_control_ae_available_modes_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_static_control_ae_available_target_fps_ranges_t v;
} guzzi_camera3_static_control_ae_available_target_fps_ranges_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_static_control_ae_compensation_range_t v;
} guzzi_camera3_static_control_ae_compensation_range_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_static_control_ae_compensation_step_t v;
} guzzi_camera3_static_control_ae_compensation_step_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_static_control_af_available_modes_t v;
} guzzi_camera3_static_control_af_available_modes_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_static_control_available_effects_t v;
} guzzi_camera3_static_control_available_effects_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_static_control_available_scene_modes_t v;
} guzzi_camera3_static_control_available_scene_modes_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_static_control_available_video_stabilization_modes_t v;
} guzzi_camera3_static_control_available_video_stabilization_modes_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_static_control_awb_available_modes_t v;
} guzzi_camera3_static_control_awb_available_modes_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_static_control_max_regions_t v;
} guzzi_camera3_static_control_max_regions_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_static_control_scene_mode_overrides_t v;
} guzzi_camera3_static_control_scene_mode_overrides_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_static_flash_color_temperature_t v;
} guzzi_camera3_static_flash_color_temperature_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_static_flash_info_available_t v;
} guzzi_camera3_static_flash_info_available_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_static_flash_info_charge_duration_t v;
} guzzi_camera3_static_flash_info_charge_duration_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_static_flash_max_energy_t v;
} guzzi_camera3_static_flash_max_energy_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_static_hot_pixel_info_map_t v;
} guzzi_camera3_static_hot_pixel_info_map_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_static_info_supported_hardware_level_t v;
} guzzi_camera3_static_info_supported_hardware_level_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_static_jpeg_available_thumbnail_sizes_t v;
} guzzi_camera3_static_jpeg_available_thumbnail_sizes_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_static_jpeg_max_size_t v;
} guzzi_camera3_static_jpeg_max_size_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_static_led_available_leds_t v;
} guzzi_camera3_static_led_available_leds_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_static_lens_facing_t v;
} guzzi_camera3_static_lens_facing_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_static_lens_info_available_apertures_t v;
} guzzi_camera3_static_lens_info_available_apertures_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_static_lens_info_available_filter_densities_t v;
} guzzi_camera3_static_lens_info_available_filter_densities_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_static_lens_info_available_focal_lengths_t v;
} guzzi_camera3_static_lens_info_available_focal_lengths_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_static_lens_info_available_optical_stabilization_t v;
} guzzi_camera3_static_lens_info_available_optical_stabilization_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_static_lens_info_geometric_correction_map_t v;
} guzzi_camera3_static_lens_info_geometric_correction_map_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_static_lens_info_geometric_correction_map_size_t v;
} guzzi_camera3_static_lens_info_geometric_correction_map_size_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_static_lens_info_hyperfocal_distance_t v;
} guzzi_camera3_static_lens_info_hyperfocal_distance_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_static_lens_info_minimum_focus_distance_t v;
} guzzi_camera3_static_lens_info_minimum_focus_distance_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_static_lens_info_shading_map_size_t v;
} guzzi_camera3_static_lens_info_shading_map_size_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_static_lens_optical_axis_angle_t v;
} guzzi_camera3_static_lens_optical_axis_angle_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_static_lens_position_t v;
} guzzi_camera3_static_lens_position_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_static_quirks_metering_crop_region_t v;
} guzzi_camera3_static_quirks_metering_crop_region_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_static_quirks_trigger_af_with_auto_t v;
} guzzi_camera3_static_quirks_trigger_af_with_auto_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_static_quirks_use_partial_result_t v;
} guzzi_camera3_static_quirks_use_partial_result_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_static_quirks_use_zsl_format_t v;
} guzzi_camera3_static_quirks_use_zsl_format_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_static_request_max_num_output_streams_t v;
} guzzi_camera3_static_request_max_num_output_streams_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_static_request_max_num_reprocess_streams_t v;
} guzzi_camera3_static_request_max_num_reprocess_streams_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_static_scaler_available_formats_t v;
} guzzi_camera3_static_scaler_available_formats_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_static_scaler_available_jpeg_min_durations_t v;
} guzzi_camera3_static_scaler_available_jpeg_min_durations_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_static_scaler_available_jpeg_sizes_t v;
} guzzi_camera3_static_scaler_available_jpeg_sizes_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_static_scaler_available_max_digital_zoom_t v;
} guzzi_camera3_static_scaler_available_max_digital_zoom_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_static_scaler_available_processed_min_durations_t v;
} guzzi_camera3_static_scaler_available_processed_min_durations_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_static_scaler_available_processed_sizes_t v;
} guzzi_camera3_static_scaler_available_processed_sizes_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_static_scaler_available_raw_min_durations_t v;
} guzzi_camera3_static_scaler_available_raw_min_durations_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_static_scaler_available_raw_sizes_t v;
} guzzi_camera3_static_scaler_available_raw_sizes_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_static_sensor_base_gain_factor_t v;
} guzzi_camera3_static_sensor_base_gain_factor_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_static_sensor_black_level_pattern_t v;
} guzzi_camera3_static_sensor_black_level_pattern_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_static_sensor_calibration_transform1_t v;
} guzzi_camera3_static_sensor_calibration_transform1_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_static_sensor_calibration_transform2_t v;
} guzzi_camera3_static_sensor_calibration_transform2_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_static_sensor_color_transform1_t v;
} guzzi_camera3_static_sensor_color_transform1_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_static_sensor_color_transform2_t v;
} guzzi_camera3_static_sensor_color_transform2_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_static_sensor_forward_matrix1_t v;
} guzzi_camera3_static_sensor_forward_matrix1_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_static_sensor_forward_matrix2_t v;
} guzzi_camera3_static_sensor_forward_matrix2_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_static_sensor_info_active_array_size_t v;
} guzzi_camera3_static_sensor_info_active_array_size_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_static_sensor_info_color_filter_arrangement_t v;
} guzzi_camera3_static_sensor_info_color_filter_arrangement_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_static_sensor_info_exposure_time_range_t v;
} guzzi_camera3_static_sensor_info_exposure_time_range_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_static_sensor_info_max_frame_duration_t v;
} guzzi_camera3_static_sensor_info_max_frame_duration_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_static_sensor_info_physical_size_t v;
} guzzi_camera3_static_sensor_info_physical_size_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_static_sensor_info_pixel_array_size_t v;
} guzzi_camera3_static_sensor_info_pixel_array_size_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_static_sensor_info_sensitivity_range_t v;
} guzzi_camera3_static_sensor_info_sensitivity_range_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_static_sensor_info_white_level_t v;
} guzzi_camera3_static_sensor_info_white_level_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_static_sensor_max_analog_sensitivity_t v;
} guzzi_camera3_static_sensor_max_analog_sensitivity_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_static_sensor_noise_model_coefficients_t v;
} guzzi_camera3_static_sensor_noise_model_coefficients_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_static_sensor_orientation_t v;
} guzzi_camera3_static_sensor_orientation_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_static_sensor_reference_illuminant1_t v;
} guzzi_camera3_static_sensor_reference_illuminant1_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_static_sensor_reference_illuminant2_t v;
} guzzi_camera3_static_sensor_reference_illuminant2_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_static_statistics_info_available_face_detect_modes_t v;
} guzzi_camera3_static_statistics_info_available_face_detect_modes_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_static_statistics_info_histogram_bucket_count_t v;
} guzzi_camera3_static_statistics_info_histogram_bucket_count_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_static_statistics_info_max_face_count_t v;
} guzzi_camera3_static_statistics_info_max_face_count_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_static_statistics_info_max_histogram_count_t v;
} guzzi_camera3_static_statistics_info_max_histogram_count_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_static_statistics_info_max_sharpness_map_value_t v;
} guzzi_camera3_static_statistics_info_max_sharpness_map_value_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_static_statistics_info_sharpness_map_size_t v;
} guzzi_camera3_static_statistics_info_sharpness_map_size_identity_t;

typedef struct {
    guzzi_camera3_metadata_identity_t identity;
    guzzi_camera3_static_tonemap_max_curve_points_t v;
} guzzi_camera3_static_tonemap_max_curve_points_identity_t;

/*
 * ****************************************************************************
 * * Control Metadata Valid ***************************************************
 * ****************************************************************************
 */
typedef struct {
    guzzi_camera3_int32_t black_level_lock;
    guzzi_camera3_int32_t color_correction_gains;
    guzzi_camera3_int32_t color_correction_mode;
    guzzi_camera3_int32_t color_correction_transform;
    guzzi_camera3_int32_t control_ae_antibanding_mode;
    guzzi_camera3_int32_t control_ae_exposure_compensation;
    guzzi_camera3_int32_t control_ae_lock;
    guzzi_camera3_int32_t control_ae_mode;
    guzzi_camera3_int32_t control_ae_precapture_trigger;
    guzzi_camera3_int32_t control_ae_regions;
    guzzi_camera3_int32_t control_ae_target_fps_range;
    guzzi_camera3_int32_t control_af_mode;
    guzzi_camera3_int32_t control_af_regions;
    guzzi_camera3_int32_t control_af_trigger;
    guzzi_camera3_int32_t control_awb_lock;
    guzzi_camera3_int32_t control_awb_mode;
    guzzi_camera3_int32_t control_awb_regions;
    guzzi_camera3_int32_t control_capture_intent;
    guzzi_camera3_int32_t control_effect_mode;
    guzzi_camera3_int32_t control_mode;
    guzzi_camera3_int32_t control_scene_mode;
    guzzi_camera3_int32_t control_video_stabilization_mode;
    guzzi_camera3_int32_t demosaic_mode;
    guzzi_camera3_int32_t edge_mode;
    guzzi_camera3_int32_t edge_strength;
    guzzi_camera3_int32_t flash_firing_power;
    guzzi_camera3_int32_t flash_firing_time;
    guzzi_camera3_int32_t flash_mode;
    guzzi_camera3_int32_t geometric_mode;
    guzzi_camera3_int32_t geometric_strength;
    guzzi_camera3_int32_t hot_pixel_mode;
    guzzi_camera3_int32_t jpeg_gps_coordinates;
    guzzi_camera3_int32_t jpeg_gps_processing_method;
    guzzi_camera3_int32_t jpeg_gps_timestamp;
    guzzi_camera3_int32_t jpeg_orientation;
    guzzi_camera3_int32_t jpeg_quality;
    guzzi_camera3_int32_t jpeg_thumbnail_quality;
    guzzi_camera3_int32_t jpeg_thumbnail_size;
    guzzi_camera3_int32_t led_transmit;
    guzzi_camera3_int32_t lens_aperture;
    guzzi_camera3_int32_t lens_filter_density;
    guzzi_camera3_int32_t lens_focal_length;
    guzzi_camera3_int32_t lens_focus_distance;
    guzzi_camera3_int32_t lens_optical_stabilization_mode;
    guzzi_camera3_int32_t noise_reduction_mode;
    guzzi_camera3_int32_t noise_reduction_strength;
    guzzi_camera3_int32_t request_frame_count;
    guzzi_camera3_int32_t request_id;
    guzzi_camera3_int32_t request_input_streams;
    guzzi_camera3_int32_t request_metadata_mode;
    guzzi_camera3_int32_t request_output_streams;
    guzzi_camera3_int32_t request_type;
    guzzi_camera3_int32_t scaler_crop_region;
    guzzi_camera3_int32_t sensor_exposure_time;
    guzzi_camera3_int32_t sensor_frame_duration;
    guzzi_camera3_int32_t sensor_sensitivity;
    guzzi_camera3_int32_t shading_mode;
    guzzi_camera3_int32_t shading_strength;
    guzzi_camera3_int32_t statistics_face_detect_mode;
    guzzi_camera3_int32_t statistics_histogram_mode;
    guzzi_camera3_int32_t statistics_lens_shading_map_mode;
    guzzi_camera3_int32_t statistics_sharpness_map_mode;
    guzzi_camera3_int32_t tonemap_curve_blue;
    guzzi_camera3_int32_t tonemap_curve_green;
    guzzi_camera3_int32_t tonemap_curve_red;
    guzzi_camera3_int32_t tonemap_mode;
} guzzi_camera3_metadata_controls_valid_t;

/*
 * ****************************************************************************
 * * Dynamic Metadata Valid ***************************************************
 * ****************************************************************************
 */
typedef struct {
    guzzi_camera3_int32_t black_level_lock;
    guzzi_camera3_int32_t color_correction_gains;
    guzzi_camera3_int32_t color_correction_transform;
    guzzi_camera3_int32_t control_ae_precapture_id;
    guzzi_camera3_int32_t control_ae_regions;
    guzzi_camera3_int32_t control_ae_state;
    guzzi_camera3_int32_t control_af_mode;
    guzzi_camera3_int32_t control_af_regions;
    guzzi_camera3_int32_t control_af_state;
    guzzi_camera3_int32_t control_af_trigger_id;
    guzzi_camera3_int32_t control_awb_mode;
    guzzi_camera3_int32_t control_awb_regions;
    guzzi_camera3_int32_t control_awb_state;
    guzzi_camera3_int32_t control_mode;
    guzzi_camera3_int32_t edge_mode;
    guzzi_camera3_int32_t flash_firing_power;
    guzzi_camera3_int32_t flash_firing_time;
    guzzi_camera3_int32_t flash_mode;
    guzzi_camera3_int32_t flash_state;
    guzzi_camera3_int32_t hot_pixel_mode;
    guzzi_camera3_int32_t jpeg_gps_coordinates;
    guzzi_camera3_int32_t jpeg_gps_processing_method;
    guzzi_camera3_int32_t jpeg_gps_timestamp;
    guzzi_camera3_int32_t jpeg_orientation;
    guzzi_camera3_int32_t jpeg_quality;
    guzzi_camera3_int32_t jpeg_size;
    guzzi_camera3_int32_t jpeg_thumbnail_quality;
    guzzi_camera3_int32_t jpeg_thumbnail_size;
    guzzi_camera3_int32_t led_transmit;
    guzzi_camera3_int32_t lens_aperture;
    guzzi_camera3_int32_t lens_filter_density;
    guzzi_camera3_int32_t lens_focal_length;
    guzzi_camera3_int32_t lens_focus_distance;
    guzzi_camera3_int32_t lens_focus_range;
    guzzi_camera3_int32_t lens_optical_stabilization_mode;
    guzzi_camera3_int32_t lens_state;
    guzzi_camera3_int32_t noise_reduction_mode;
    guzzi_camera3_int32_t quirks_partial_result;
    guzzi_camera3_int32_t request_frame_count;
    guzzi_camera3_int32_t request_id;
    guzzi_camera3_int32_t request_metadata_mode;
    guzzi_camera3_int32_t request_output_streams;
    guzzi_camera3_int32_t scaler_crop_region;
    guzzi_camera3_int32_t sensor_exposure_time;
    guzzi_camera3_int32_t sensor_frame_duration;
    guzzi_camera3_int32_t sensor_sensitivity;
    guzzi_camera3_int32_t sensor_temperature;
    guzzi_camera3_int32_t sensor_timestamp;
    guzzi_camera3_int32_t shading_mode;
    guzzi_camera3_int32_t statistics_face_detect_mode;
    guzzi_camera3_int32_t statistics_face_ids;
    guzzi_camera3_int32_t statistics_face_landmarks;
    guzzi_camera3_int32_t statistics_face_rectangles;
    guzzi_camera3_int32_t statistics_face_scores;
    guzzi_camera3_int32_t statistics_histogram;
    guzzi_camera3_int32_t statistics_histogram_mode;
    guzzi_camera3_int32_t statistics_lens_shading_map;
    guzzi_camera3_int32_t statistics_predicted_color_gains;
    guzzi_camera3_int32_t statistics_predicted_color_transform;
    guzzi_camera3_int32_t statistics_scene_flicker;
    guzzi_camera3_int32_t statistics_sharpness_map;
    guzzi_camera3_int32_t statistics_sharpness_map_mode;
    guzzi_camera3_int32_t tonemap_curve_blue;
    guzzi_camera3_int32_t tonemap_curve_green;
    guzzi_camera3_int32_t tonemap_curve_red;
    guzzi_camera3_int32_t tonemap_mode;
} guzzi_camera3_metadata_dynamic_valid_t;

/*
 * ****************************************************************************
 * * Static Metadata Valid ****************************************************
 * ****************************************************************************
 */
typedef struct {
    guzzi_camera3_int32_t control_ae_available_antibanding_modes;
    guzzi_camera3_int32_t control_ae_available_modes;
    guzzi_camera3_int32_t control_ae_available_target_fps_ranges;
    guzzi_camera3_int32_t control_ae_compensation_range;
    guzzi_camera3_int32_t control_ae_compensation_step;
    guzzi_camera3_int32_t control_af_available_modes;
    guzzi_camera3_int32_t control_available_effects;
    guzzi_camera3_int32_t control_available_scene_modes;
    guzzi_camera3_int32_t control_available_video_stabilization_modes;
    guzzi_camera3_int32_t control_awb_available_modes;
    guzzi_camera3_int32_t control_max_regions;
    guzzi_camera3_int32_t control_scene_mode_overrides;
    guzzi_camera3_int32_t flash_color_temperature;
    guzzi_camera3_int32_t flash_info_available;
    guzzi_camera3_int32_t flash_info_charge_duration;
    guzzi_camera3_int32_t flash_max_energy;
    guzzi_camera3_int32_t hot_pixel_info_map;
    guzzi_camera3_int32_t info_supported_hardware_level;
    guzzi_camera3_int32_t jpeg_available_thumbnail_sizes;
    guzzi_camera3_int32_t jpeg_max_size;
    guzzi_camera3_int32_t led_available_leds;
    guzzi_camera3_int32_t lens_facing;
    guzzi_camera3_int32_t lens_info_available_apertures;
    guzzi_camera3_int32_t lens_info_available_filter_densities;
    guzzi_camera3_int32_t lens_info_available_focal_lengths;
    guzzi_camera3_int32_t lens_info_available_optical_stabilization;
    guzzi_camera3_int32_t lens_info_geometric_correction_map;
    guzzi_camera3_int32_t lens_info_geometric_correction_map_size;
    guzzi_camera3_int32_t lens_info_hyperfocal_distance;
    guzzi_camera3_int32_t lens_info_minimum_focus_distance;
    guzzi_camera3_int32_t lens_info_shading_map_size;
    guzzi_camera3_int32_t lens_optical_axis_angle;
    guzzi_camera3_int32_t lens_position;
    guzzi_camera3_int32_t quirks_metering_crop_region;
    guzzi_camera3_int32_t quirks_trigger_af_with_auto;
    guzzi_camera3_int32_t quirks_use_partial_result;
    guzzi_camera3_int32_t quirks_use_zsl_format;
    guzzi_camera3_int32_t request_max_num_output_streams;
    guzzi_camera3_int32_t request_max_num_reprocess_streams;
    guzzi_camera3_int32_t scaler_available_formats;
    guzzi_camera3_int32_t scaler_available_jpeg_min_durations;
    guzzi_camera3_int32_t scaler_available_jpeg_sizes;
    guzzi_camera3_int32_t scaler_available_max_digital_zoom;
    guzzi_camera3_int32_t scaler_available_processed_min_durations;
    guzzi_camera3_int32_t scaler_available_processed_sizes;
    guzzi_camera3_int32_t scaler_available_raw_min_durations;
    guzzi_camera3_int32_t scaler_available_raw_sizes;
    guzzi_camera3_int32_t sensor_base_gain_factor;
    guzzi_camera3_int32_t sensor_black_level_pattern;
    guzzi_camera3_int32_t sensor_calibration_transform1;
    guzzi_camera3_int32_t sensor_calibration_transform2;
    guzzi_camera3_int32_t sensor_color_transform1;
    guzzi_camera3_int32_t sensor_color_transform2;
    guzzi_camera3_int32_t sensor_forward_matrix1;
    guzzi_camera3_int32_t sensor_forward_matrix2;
    guzzi_camera3_int32_t sensor_info_active_array_size;
    guzzi_camera3_int32_t sensor_info_color_filter_arrangement;
    guzzi_camera3_int32_t sensor_info_exposure_time_range;
    guzzi_camera3_int32_t sensor_info_max_frame_duration;
    guzzi_camera3_int32_t sensor_info_physical_size;
    guzzi_camera3_int32_t sensor_info_pixel_array_size;
    guzzi_camera3_int32_t sensor_info_sensitivity_range;
    guzzi_camera3_int32_t sensor_info_white_level;
    guzzi_camera3_int32_t sensor_max_analog_sensitivity;
    guzzi_camera3_int32_t sensor_noise_model_coefficients;
    guzzi_camera3_int32_t sensor_orientation;
    guzzi_camera3_int32_t sensor_reference_illuminant1;
    guzzi_camera3_int32_t sensor_reference_illuminant2;
    guzzi_camera3_int32_t statistics_info_available_face_detect_modes;
    guzzi_camera3_int32_t statistics_info_histogram_bucket_count;
    guzzi_camera3_int32_t statistics_info_max_face_count;
    guzzi_camera3_int32_t statistics_info_max_histogram_count;
    guzzi_camera3_int32_t statistics_info_max_sharpness_map_value;
    guzzi_camera3_int32_t statistics_info_sharpness_map_size;
    guzzi_camera3_int32_t tonemap_max_curve_points;
} guzzi_camera3_metadata_static_valid_t;

/*
 * ****************************************************************************
 * * Control Metadata Group ***************************************************
 * ****************************************************************************
 */
typedef struct {
    guzzi_camera3_controls_black_level_lock_identity_t black_level_lock;
    guzzi_camera3_controls_color_correction_gains_identity_t color_correction_gains;
    guzzi_camera3_controls_color_correction_mode_identity_t color_correction_mode;
    guzzi_camera3_controls_color_correction_transform_identity_t color_correction_transform;
    guzzi_camera3_controls_control_ae_antibanding_mode_identity_t control_ae_antibanding_mode;
    guzzi_camera3_controls_control_ae_exposure_compensation_identity_t control_ae_exposure_compensation;
    guzzi_camera3_controls_control_ae_lock_identity_t control_ae_lock;
    guzzi_camera3_controls_control_ae_mode_identity_t control_ae_mode;
    guzzi_camera3_controls_control_ae_precapture_trigger_identity_t control_ae_precapture_trigger;
    guzzi_camera3_controls_control_ae_regions_identity_t control_ae_regions;
    guzzi_camera3_controls_control_ae_target_fps_range_identity_t control_ae_target_fps_range;
    guzzi_camera3_controls_control_af_mode_identity_t control_af_mode;
    guzzi_camera3_controls_control_af_regions_identity_t control_af_regions;
    guzzi_camera3_controls_control_af_trigger_identity_t control_af_trigger;
    guzzi_camera3_controls_control_awb_lock_identity_t control_awb_lock;
    guzzi_camera3_controls_control_awb_mode_identity_t control_awb_mode;
    guzzi_camera3_controls_control_awb_regions_identity_t control_awb_regions;
    guzzi_camera3_controls_control_capture_intent_identity_t control_capture_intent;
    guzzi_camera3_controls_control_effect_mode_identity_t control_effect_mode;
    guzzi_camera3_controls_control_mode_identity_t control_mode;
    guzzi_camera3_controls_control_scene_mode_identity_t control_scene_mode;
    guzzi_camera3_controls_control_video_stabilization_mode_identity_t control_video_stabilization_mode;
    guzzi_camera3_controls_demosaic_mode_identity_t demosaic_mode;
    guzzi_camera3_controls_edge_mode_identity_t edge_mode;
    guzzi_camera3_controls_edge_strength_identity_t edge_strength;
    guzzi_camera3_controls_flash_firing_power_identity_t flash_firing_power;
    guzzi_camera3_controls_flash_firing_time_identity_t flash_firing_time;
    guzzi_camera3_controls_flash_mode_identity_t flash_mode;
    guzzi_camera3_controls_geometric_mode_identity_t geometric_mode;
    guzzi_camera3_controls_geometric_strength_identity_t geometric_strength;
    guzzi_camera3_controls_hot_pixel_mode_identity_t hot_pixel_mode;
    guzzi_camera3_controls_jpeg_gps_coordinates_identity_t jpeg_gps_coordinates;
    guzzi_camera3_controls_jpeg_gps_processing_method_identity_t jpeg_gps_processing_method;
    guzzi_camera3_controls_jpeg_gps_timestamp_identity_t jpeg_gps_timestamp;
    guzzi_camera3_controls_jpeg_orientation_identity_t jpeg_orientation;
    guzzi_camera3_controls_jpeg_quality_identity_t jpeg_quality;
    guzzi_camera3_controls_jpeg_thumbnail_quality_identity_t jpeg_thumbnail_quality;
    guzzi_camera3_controls_jpeg_thumbnail_size_identity_t jpeg_thumbnail_size;
    guzzi_camera3_controls_led_transmit_identity_t led_transmit;
    guzzi_camera3_controls_lens_aperture_identity_t lens_aperture;
    guzzi_camera3_controls_lens_filter_density_identity_t lens_filter_density;
    guzzi_camera3_controls_lens_focal_length_identity_t lens_focal_length;
    guzzi_camera3_controls_lens_focus_distance_identity_t lens_focus_distance;
    guzzi_camera3_controls_lens_optical_stabilization_mode_identity_t lens_optical_stabilization_mode;
    guzzi_camera3_controls_noise_reduction_mode_identity_t noise_reduction_mode;
    guzzi_camera3_controls_noise_reduction_strength_identity_t noise_reduction_strength;
    guzzi_camera3_controls_request_frame_count_identity_t request_frame_count;
    guzzi_camera3_controls_request_id_identity_t request_id;
    guzzi_camera3_controls_request_input_streams_identity_t request_input_streams;
    guzzi_camera3_controls_request_metadata_mode_identity_t request_metadata_mode;
    guzzi_camera3_controls_request_output_streams_identity_t request_output_streams;
    guzzi_camera3_controls_request_type_identity_t request_type;
    guzzi_camera3_controls_scaler_crop_region_identity_t scaler_crop_region;
    guzzi_camera3_controls_sensor_exposure_time_identity_t sensor_exposure_time;
    guzzi_camera3_controls_sensor_frame_duration_identity_t sensor_frame_duration;
    guzzi_camera3_controls_sensor_sensitivity_identity_t sensor_sensitivity;
    guzzi_camera3_controls_shading_mode_identity_t shading_mode;
    guzzi_camera3_controls_shading_strength_identity_t shading_strength;
    guzzi_camera3_controls_statistics_face_detect_mode_identity_t statistics_face_detect_mode;
    guzzi_camera3_controls_statistics_histogram_mode_identity_t statistics_histogram_mode;
    guzzi_camera3_controls_statistics_lens_shading_map_mode_identity_t statistics_lens_shading_map_mode;
    guzzi_camera3_controls_statistics_sharpness_map_mode_identity_t statistics_sharpness_map_mode;
    guzzi_camera3_controls_tonemap_curve_blue_identity_t tonemap_curve_blue;
    guzzi_camera3_controls_tonemap_curve_green_identity_t tonemap_curve_green;
    guzzi_camera3_controls_tonemap_curve_red_identity_t tonemap_curve_red;
    guzzi_camera3_controls_tonemap_mode_identity_t tonemap_mode;
    guzzi_camera3_metadata_t metadata_end_marker;
} guzzi_camera3_metadata_controls_struct_t;

/*
 * ****************************************************************************
 * * Dynamic Metadata Group ***************************************************
 * ****************************************************************************
 */
typedef struct {
    guzzi_camera3_dynamic_black_level_lock_identity_t black_level_lock;
    guzzi_camera3_dynamic_color_correction_gains_identity_t color_correction_gains;
    guzzi_camera3_dynamic_color_correction_transform_identity_t color_correction_transform;
    guzzi_camera3_dynamic_control_ae_precapture_id_identity_t control_ae_precapture_id;
    guzzi_camera3_dynamic_control_ae_regions_identity_t control_ae_regions;
    guzzi_camera3_dynamic_control_ae_state_identity_t control_ae_state;
    guzzi_camera3_dynamic_control_af_mode_identity_t control_af_mode;
    guzzi_camera3_dynamic_control_af_regions_identity_t control_af_regions;
    guzzi_camera3_dynamic_control_af_state_identity_t control_af_state;
    guzzi_camera3_dynamic_control_af_trigger_id_identity_t control_af_trigger_id;
    guzzi_camera3_dynamic_control_awb_mode_identity_t control_awb_mode;
    guzzi_camera3_dynamic_control_awb_regions_identity_t control_awb_regions;
    guzzi_camera3_dynamic_control_awb_state_identity_t control_awb_state;
    guzzi_camera3_dynamic_control_mode_identity_t control_mode;
    guzzi_camera3_dynamic_edge_mode_identity_t edge_mode;
    guzzi_camera3_dynamic_flash_firing_power_identity_t flash_firing_power;
    guzzi_camera3_dynamic_flash_firing_time_identity_t flash_firing_time;
    guzzi_camera3_dynamic_flash_mode_identity_t flash_mode;
    guzzi_camera3_dynamic_flash_state_identity_t flash_state;
    guzzi_camera3_dynamic_hot_pixel_mode_identity_t hot_pixel_mode;
    guzzi_camera3_dynamic_jpeg_gps_coordinates_identity_t jpeg_gps_coordinates;
    guzzi_camera3_dynamic_jpeg_gps_processing_method_identity_t jpeg_gps_processing_method;
    guzzi_camera3_dynamic_jpeg_gps_timestamp_identity_t jpeg_gps_timestamp;
    guzzi_camera3_dynamic_jpeg_orientation_identity_t jpeg_orientation;
    guzzi_camera3_dynamic_jpeg_quality_identity_t jpeg_quality;
    guzzi_camera3_dynamic_jpeg_size_identity_t jpeg_size;
    guzzi_camera3_dynamic_jpeg_thumbnail_quality_identity_t jpeg_thumbnail_quality;
    guzzi_camera3_dynamic_jpeg_thumbnail_size_identity_t jpeg_thumbnail_size;
    guzzi_camera3_dynamic_led_transmit_identity_t led_transmit;
    guzzi_camera3_dynamic_lens_aperture_identity_t lens_aperture;
    guzzi_camera3_dynamic_lens_filter_density_identity_t lens_filter_density;
    guzzi_camera3_dynamic_lens_focal_length_identity_t lens_focal_length;
    guzzi_camera3_dynamic_lens_focus_distance_identity_t lens_focus_distance;
    guzzi_camera3_dynamic_lens_focus_range_identity_t lens_focus_range;
    guzzi_camera3_dynamic_lens_optical_stabilization_mode_identity_t lens_optical_stabilization_mode;
    guzzi_camera3_dynamic_lens_state_identity_t lens_state;
    guzzi_camera3_dynamic_noise_reduction_mode_identity_t noise_reduction_mode;
    guzzi_camera3_dynamic_quirks_partial_result_identity_t quirks_partial_result;
    guzzi_camera3_dynamic_request_frame_count_identity_t request_frame_count;
    guzzi_camera3_dynamic_request_id_identity_t request_id;
    guzzi_camera3_dynamic_request_metadata_mode_identity_t request_metadata_mode;
    guzzi_camera3_dynamic_request_output_streams_identity_t request_output_streams;
    guzzi_camera3_dynamic_scaler_crop_region_identity_t scaler_crop_region;
    guzzi_camera3_dynamic_sensor_exposure_time_identity_t sensor_exposure_time;
    guzzi_camera3_dynamic_sensor_frame_duration_identity_t sensor_frame_duration;
    guzzi_camera3_dynamic_sensor_sensitivity_identity_t sensor_sensitivity;
    guzzi_camera3_dynamic_sensor_temperature_identity_t sensor_temperature;
    guzzi_camera3_dynamic_sensor_timestamp_identity_t sensor_timestamp;
    guzzi_camera3_dynamic_shading_mode_identity_t shading_mode;
    guzzi_camera3_dynamic_statistics_face_detect_mode_identity_t statistics_face_detect_mode;
    guzzi_camera3_dynamic_statistics_face_ids_identity_t statistics_face_ids;
    guzzi_camera3_dynamic_statistics_face_landmarks_identity_t statistics_face_landmarks;
    guzzi_camera3_dynamic_statistics_face_rectangles_identity_t statistics_face_rectangles;
    guzzi_camera3_dynamic_statistics_face_scores_identity_t statistics_face_scores;
    guzzi_camera3_dynamic_statistics_histogram_identity_t statistics_histogram;
    guzzi_camera3_dynamic_statistics_histogram_mode_identity_t statistics_histogram_mode;
    guzzi_camera3_dynamic_statistics_lens_shading_map_identity_t statistics_lens_shading_map;
    guzzi_camera3_dynamic_statistics_predicted_color_gains_identity_t statistics_predicted_color_gains;
    guzzi_camera3_dynamic_statistics_predicted_color_transform_identity_t statistics_predicted_color_transform;
    guzzi_camera3_dynamic_statistics_scene_flicker_identity_t statistics_scene_flicker;
    guzzi_camera3_dynamic_statistics_sharpness_map_identity_t statistics_sharpness_map;
    guzzi_camera3_dynamic_statistics_sharpness_map_mode_identity_t statistics_sharpness_map_mode;
    guzzi_camera3_dynamic_tonemap_curve_blue_identity_t tonemap_curve_blue;
    guzzi_camera3_dynamic_tonemap_curve_green_identity_t tonemap_curve_green;
    guzzi_camera3_dynamic_tonemap_curve_red_identity_t tonemap_curve_red;
    guzzi_camera3_dynamic_tonemap_mode_identity_t tonemap_mode;
    guzzi_camera3_metadata_t metadata_end_marker;
} guzzi_camera3_metadata_dynamic_struct_t;

/*
 * ****************************************************************************
 * * Static Metadata Group ****************************************************
 * ****************************************************************************
 */
typedef struct {
    guzzi_camera3_static_control_ae_available_antibanding_modes_identity_t control_ae_available_antibanding_modes;
    guzzi_camera3_static_control_ae_available_modes_identity_t control_ae_available_modes;
    guzzi_camera3_static_control_ae_available_target_fps_ranges_identity_t control_ae_available_target_fps_ranges;
    guzzi_camera3_static_control_ae_compensation_range_identity_t control_ae_compensation_range;
    guzzi_camera3_static_control_ae_compensation_step_identity_t control_ae_compensation_step;
    guzzi_camera3_static_control_af_available_modes_identity_t control_af_available_modes;
    guzzi_camera3_static_control_available_effects_identity_t control_available_effects;
    guzzi_camera3_static_control_available_scene_modes_identity_t control_available_scene_modes;
    guzzi_camera3_static_control_available_video_stabilization_modes_identity_t control_available_video_stabilization_modes;
    guzzi_camera3_static_control_awb_available_modes_identity_t control_awb_available_modes;
    guzzi_camera3_static_control_max_regions_identity_t control_max_regions;
    guzzi_camera3_static_control_scene_mode_overrides_identity_t control_scene_mode_overrides;
    guzzi_camera3_static_flash_color_temperature_identity_t flash_color_temperature;
    guzzi_camera3_static_flash_info_available_identity_t flash_info_available;
    guzzi_camera3_static_flash_info_charge_duration_identity_t flash_info_charge_duration;
    guzzi_camera3_static_flash_max_energy_identity_t flash_max_energy;
    guzzi_camera3_static_hot_pixel_info_map_identity_t hot_pixel_info_map;
    guzzi_camera3_static_info_supported_hardware_level_identity_t info_supported_hardware_level;
    guzzi_camera3_static_jpeg_available_thumbnail_sizes_identity_t jpeg_available_thumbnail_sizes;
    guzzi_camera3_static_jpeg_max_size_identity_t jpeg_max_size;
    guzzi_camera3_static_led_available_leds_identity_t led_available_leds;
    guzzi_camera3_static_lens_facing_identity_t lens_facing;
    guzzi_camera3_static_lens_info_available_apertures_identity_t lens_info_available_apertures;
    guzzi_camera3_static_lens_info_available_filter_densities_identity_t lens_info_available_filter_densities;
    guzzi_camera3_static_lens_info_available_focal_lengths_identity_t lens_info_available_focal_lengths;
    guzzi_camera3_static_lens_info_available_optical_stabilization_identity_t lens_info_available_optical_stabilization;
    guzzi_camera3_static_lens_info_geometric_correction_map_identity_t lens_info_geometric_correction_map;
    guzzi_camera3_static_lens_info_geometric_correction_map_size_identity_t lens_info_geometric_correction_map_size;
    guzzi_camera3_static_lens_info_hyperfocal_distance_identity_t lens_info_hyperfocal_distance;
    guzzi_camera3_static_lens_info_minimum_focus_distance_identity_t lens_info_minimum_focus_distance;
    guzzi_camera3_static_lens_info_shading_map_size_identity_t lens_info_shading_map_size;
    guzzi_camera3_static_lens_optical_axis_angle_identity_t lens_optical_axis_angle;
    guzzi_camera3_static_lens_position_identity_t lens_position;
    guzzi_camera3_static_quirks_metering_crop_region_identity_t quirks_metering_crop_region;
    guzzi_camera3_static_quirks_trigger_af_with_auto_identity_t quirks_trigger_af_with_auto;
    guzzi_camera3_static_quirks_use_partial_result_identity_t quirks_use_partial_result;
    guzzi_camera3_static_quirks_use_zsl_format_identity_t quirks_use_zsl_format;
    guzzi_camera3_static_request_max_num_output_streams_identity_t request_max_num_output_streams;
    guzzi_camera3_static_request_max_num_reprocess_streams_identity_t request_max_num_reprocess_streams;
    guzzi_camera3_static_scaler_available_formats_identity_t scaler_available_formats;
    guzzi_camera3_static_scaler_available_jpeg_min_durations_identity_t scaler_available_jpeg_min_durations;
    guzzi_camera3_static_scaler_available_jpeg_sizes_identity_t scaler_available_jpeg_sizes;
    guzzi_camera3_static_scaler_available_max_digital_zoom_identity_t scaler_available_max_digital_zoom;
    guzzi_camera3_static_scaler_available_processed_min_durations_identity_t scaler_available_processed_min_durations;
    guzzi_camera3_static_scaler_available_processed_sizes_identity_t scaler_available_processed_sizes;
    guzzi_camera3_static_scaler_available_raw_min_durations_identity_t scaler_available_raw_min_durations;
    guzzi_camera3_static_scaler_available_raw_sizes_identity_t scaler_available_raw_sizes;
    guzzi_camera3_static_sensor_base_gain_factor_identity_t sensor_base_gain_factor;
    guzzi_camera3_static_sensor_black_level_pattern_identity_t sensor_black_level_pattern;
    guzzi_camera3_static_sensor_calibration_transform1_identity_t sensor_calibration_transform1;
    guzzi_camera3_static_sensor_calibration_transform2_identity_t sensor_calibration_transform2;
    guzzi_camera3_static_sensor_color_transform1_identity_t sensor_color_transform1;
    guzzi_camera3_static_sensor_color_transform2_identity_t sensor_color_transform2;
    guzzi_camera3_static_sensor_forward_matrix1_identity_t sensor_forward_matrix1;
    guzzi_camera3_static_sensor_forward_matrix2_identity_t sensor_forward_matrix2;
    guzzi_camera3_static_sensor_info_active_array_size_identity_t sensor_info_active_array_size;
    guzzi_camera3_static_sensor_info_color_filter_arrangement_identity_t sensor_info_color_filter_arrangement;
    guzzi_camera3_static_sensor_info_exposure_time_range_identity_t sensor_info_exposure_time_range;
    guzzi_camera3_static_sensor_info_max_frame_duration_identity_t sensor_info_max_frame_duration;
    guzzi_camera3_static_sensor_info_physical_size_identity_t sensor_info_physical_size;
    guzzi_camera3_static_sensor_info_pixel_array_size_identity_t sensor_info_pixel_array_size;
    guzzi_camera3_static_sensor_info_sensitivity_range_identity_t sensor_info_sensitivity_range;
    guzzi_camera3_static_sensor_info_white_level_identity_t sensor_info_white_level;
    guzzi_camera3_static_sensor_max_analog_sensitivity_identity_t sensor_max_analog_sensitivity;
    guzzi_camera3_static_sensor_noise_model_coefficients_identity_t sensor_noise_model_coefficients;
    guzzi_camera3_static_sensor_orientation_identity_t sensor_orientation;
    guzzi_camera3_static_sensor_reference_illuminant1_identity_t sensor_reference_illuminant1;
    guzzi_camera3_static_sensor_reference_illuminant2_identity_t sensor_reference_illuminant2;
    guzzi_camera3_static_statistics_info_available_face_detect_modes_identity_t statistics_info_available_face_detect_modes;
    guzzi_camera3_static_statistics_info_histogram_bucket_count_identity_t statistics_info_histogram_bucket_count;
    guzzi_camera3_static_statistics_info_max_face_count_identity_t statistics_info_max_face_count;
    guzzi_camera3_static_statistics_info_max_histogram_count_identity_t statistics_info_max_histogram_count;
    guzzi_camera3_static_statistics_info_max_sharpness_map_value_identity_t statistics_info_max_sharpness_map_value;
    guzzi_camera3_static_statistics_info_sharpness_map_size_identity_t statistics_info_sharpness_map_size;
    guzzi_camera3_static_tonemap_max_curve_points_identity_t tonemap_max_curve_points;
    guzzi_camera3_metadata_t metadata_end_marker;
} guzzi_camera3_metadata_static_struct_t;

/*
 * ****************************************************************************
 * * Valid + struct ***********************************************************
 * ****************************************************************************
 */
typedef struct {
    guzzi_camera3_metadata_controls_valid_t valid;
    guzzi_camera3_metadata_controls_struct_t s;
} guzzi_camera3_metadata_controls_t;

typedef struct {
    guzzi_camera3_metadata_dynamic_valid_t valid;
    guzzi_camera3_metadata_dynamic_struct_t s;
} guzzi_camera3_metadata_dynamic_t;

typedef struct {
    guzzi_camera3_metadata_static_valid_t valid;
    guzzi_camera3_metadata_static_struct_t s;
} guzzi_camera3_metadata_static_t;

