/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file convert_hal3.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <system/camera_metadata.h>
#include <guzzi/camera3/metadata_index.h>
#include <guzzi/camera3/metadata.h>
#include <guzzi/camera3/convert_hal3.h>

int guzzi_camera3_metadata_convert_hal2guzzi_byte(guzzi_camera3_metadata_type_identity_t *guzzi, camera_metadata_entry_t *hal);
int guzzi_camera3_metadata_convert_hal2guzzi_float(guzzi_camera3_metadata_type_identity_t *guzzi, camera_metadata_entry_t *hal);
int guzzi_camera3_metadata_convert_hal2guzzi_int32(guzzi_camera3_metadata_type_identity_t *guzzi, camera_metadata_entry_t *hal);
int guzzi_camera3_metadata_convert_hal2guzzi_int64(guzzi_camera3_metadata_type_identity_t *guzzi, camera_metadata_entry_t *hal);
int guzzi_camera3_metadata_convert_hal2guzzi_double_arr1(guzzi_camera3_metadata_type_identity_t *guzzi, camera_metadata_entry_t *hal);
int guzzi_camera3_metadata_convert_hal2guzzi_float_arr1(guzzi_camera3_metadata_type_identity_t *guzzi, camera_metadata_entry_t *hal);
int guzzi_camera3_metadata_convert_hal2guzzi_float_arr2(guzzi_camera3_metadata_type_identity_t *guzzi, camera_metadata_entry_t *hal);
int guzzi_camera3_metadata_convert_hal2guzzi_int32_arr1(guzzi_camera3_metadata_type_identity_t *guzzi, camera_metadata_entry_t *hal);
int guzzi_camera3_metadata_convert_hal2guzzi_int32_arr2(guzzi_camera3_metadata_type_identity_t *guzzi, camera_metadata_entry_t *hal);
int guzzi_camera3_metadata_convert_hal2guzzi_rational_arr2(guzzi_camera3_metadata_type_identity_t *guzzi, camera_metadata_entry_t *hal);

int guzzi_camera3_metadata_convert_guzzi2hal_byte(camera_metadata_t *metadata, guzzi_camera3_metadata_type_identity_t *guzzi);
int guzzi_camera3_metadata_convert_guzzi2hal_float(camera_metadata_t *metadata, guzzi_camera3_metadata_type_identity_t *guzzi);
int guzzi_camera3_metadata_convert_guzzi2hal_int32(camera_metadata_t *metadata, guzzi_camera3_metadata_type_identity_t *guzzi);
int guzzi_camera3_metadata_convert_guzzi2hal_int64(camera_metadata_t *metadata, guzzi_camera3_metadata_type_identity_t *guzzi);
int guzzi_camera3_metadata_convert_guzzi2hal_rational(camera_metadata_t *metadata, guzzi_camera3_metadata_type_identity_t *guzzi);
int guzzi_camera3_metadata_convert_guzzi2hal_byte_arr1(camera_metadata_t *metadata, guzzi_camera3_metadata_type_identity_t *guzzi);
int guzzi_camera3_metadata_convert_guzzi2hal_byte_arr2(camera_metadata_t *metadata, guzzi_camera3_metadata_type_identity_t *guzzi);
int guzzi_camera3_metadata_convert_guzzi2hal_double_arr1(camera_metadata_t *metadata, guzzi_camera3_metadata_type_identity_t *guzzi);
int guzzi_camera3_metadata_convert_guzzi2hal_float_arr1(camera_metadata_t *metadata, guzzi_camera3_metadata_type_identity_t *guzzi);
int guzzi_camera3_metadata_convert_guzzi2hal_float_arr2(camera_metadata_t *metadata, guzzi_camera3_metadata_type_identity_t *guzzi);
int guzzi_camera3_metadata_convert_guzzi2hal_float_arr3(camera_metadata_t *metadata, guzzi_camera3_metadata_type_identity_t *guzzi);
int guzzi_camera3_metadata_convert_guzzi2hal_float_arr4(camera_metadata_t *metadata, guzzi_camera3_metadata_type_identity_t *guzzi);
int guzzi_camera3_metadata_convert_guzzi2hal_int32_arr1(camera_metadata_t *metadata, guzzi_camera3_metadata_type_identity_t *guzzi);
int guzzi_camera3_metadata_convert_guzzi2hal_int32_arr2(camera_metadata_t *metadata, guzzi_camera3_metadata_type_identity_t *guzzi);
int guzzi_camera3_metadata_convert_guzzi2hal_int32_arr3(camera_metadata_t *metadata, guzzi_camera3_metadata_type_identity_t *guzzi);
int guzzi_camera3_metadata_convert_guzzi2hal_int64_arr1(camera_metadata_t *metadata, guzzi_camera3_metadata_type_identity_t *guzzi);
int guzzi_camera3_metadata_convert_guzzi2hal_rational_arr1(camera_metadata_t *metadata, guzzi_camera3_metadata_type_identity_t *guzzi);
int guzzi_camera3_metadata_convert_guzzi2hal_rational_arr2(camera_metadata_t *metadata, guzzi_camera3_metadata_type_identity_t *guzzi);

static guzzi_camera3_metadata_hal2guzzi_t guzzi_camera3_metadata_hal2guzzi[] = {
    {
        .func = guzzi_camera3_metadata_convert_hal2guzzi_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_hal2guzzi_float_arr1,
    },
    {
        .func = guzzi_camera3_metadata_convert_hal2guzzi_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_hal2guzzi_rational_arr2,
    },
    {
        .func = guzzi_camera3_metadata_convert_hal2guzzi_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_hal2guzzi_int32,
    },
    {
        .func = guzzi_camera3_metadata_convert_hal2guzzi_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_hal2guzzi_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_hal2guzzi_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_hal2guzzi_int32_arr2,
    },
    {
        .func = guzzi_camera3_metadata_convert_hal2guzzi_int32_arr1,
    },
    {
        .func = guzzi_camera3_metadata_convert_hal2guzzi_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_hal2guzzi_int32_arr2,
    },
    {
        .func = guzzi_camera3_metadata_convert_hal2guzzi_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_hal2guzzi_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_hal2guzzi_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_hal2guzzi_int32_arr2,
    },
    {
        .func = guzzi_camera3_metadata_convert_hal2guzzi_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_hal2guzzi_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_hal2guzzi_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_hal2guzzi_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_hal2guzzi_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_hal2guzzi_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_hal2guzzi_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_hal2guzzi_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_hal2guzzi_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_hal2guzzi_int64,
    },
    {
        .func = guzzi_camera3_metadata_convert_hal2guzzi_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_hal2guzzi_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_hal2guzzi_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_hal2guzzi_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_hal2guzzi_double_arr1,
    },
    {
        .func = guzzi_camera3_metadata_convert_hal2guzzi_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_hal2guzzi_int64,
    },
    {
        .func = guzzi_camera3_metadata_convert_hal2guzzi_int32,
    },
    {
        .func = guzzi_camera3_metadata_convert_hal2guzzi_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_hal2guzzi_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_hal2guzzi_int32_arr1,
    },
    {
        .func = guzzi_camera3_metadata_convert_hal2guzzi_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_hal2guzzi_float,
    },
    {
        .func = guzzi_camera3_metadata_convert_hal2guzzi_float,
    },
    {
        .func = guzzi_camera3_metadata_convert_hal2guzzi_float,
    },
    {
        .func = guzzi_camera3_metadata_convert_hal2guzzi_float,
    },
    {
        .func = guzzi_camera3_metadata_convert_hal2guzzi_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_hal2guzzi_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_hal2guzzi_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_hal2guzzi_int32,
    },
    {
        .func = guzzi_camera3_metadata_convert_hal2guzzi_int32,
    },
    {
        .func = guzzi_camera3_metadata_convert_hal2guzzi_int32_arr1,
    },
    {
        .func = guzzi_camera3_metadata_convert_hal2guzzi_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_hal2guzzi_int32_arr1,
    },
    {
        .func = guzzi_camera3_metadata_convert_hal2guzzi_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_hal2guzzi_int32_arr1,
    },
    {
        .func = guzzi_camera3_metadata_convert_hal2guzzi_int64,
    },
    {
        .func = guzzi_camera3_metadata_convert_hal2guzzi_int64,
    },
    {
        .func = guzzi_camera3_metadata_convert_hal2guzzi_int32,
    },
    {
        .func = guzzi_camera3_metadata_convert_hal2guzzi_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_hal2guzzi_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_hal2guzzi_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_hal2guzzi_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_hal2guzzi_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_hal2guzzi_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_hal2guzzi_float_arr2,
    },
    {
        .func = guzzi_camera3_metadata_convert_hal2guzzi_float_arr2,
    },
    {
        .func = guzzi_camera3_metadata_convert_hal2guzzi_float_arr2,
    },
    {
        .func = guzzi_camera3_metadata_convert_hal2guzzi_byte,
    },
};

static guzzi_camera3_metadata_guzzi2hal_t guzzi_camera3_metadata_guzzi2hal[] = {
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_float_arr1,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_rational_arr2,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_int32,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_int32_arr2,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_int32_arr1,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_int32_arr2,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_int32_arr2,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_int64,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_double_arr1,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_int64,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_int32,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_int32_arr1,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_float,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_float,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_float,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_float,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_int32,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_int32,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_int32_arr1,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_int32_arr1,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_int32_arr1,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_int64,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_int64,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_int32,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_float_arr2,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_float_arr2,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_float_arr2,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_float_arr1,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_rational_arr2,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_int32,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_int32_arr2,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_int32_arr2,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_int32,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_int32_arr2,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_int64,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_double_arr1,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_int64,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_int32,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_int32,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_int32_arr1,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_float,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_float,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_float,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_float,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_float_arr1,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_int32,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_int32,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_int32_arr1,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_int32_arr1,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_int64,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_int64,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_int32,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_float,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_int64,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_int32_arr1,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_int32_arr2,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_int32_arr2,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_byte_arr1,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_int32_arr2,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_float_arr3,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_float_arr1,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_rational_arr2,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_int32_arr3,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_float_arr2,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_float_arr2,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_float_arr2,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_byte_arr1,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_byte_arr1,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_int32_arr2,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_int32_arr1,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_rational,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_byte_arr1,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_byte_arr1,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_byte_arr1,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_byte_arr1,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_byte_arr1,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_int32,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_byte_arr2,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_int64,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_int32_arr2,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_int32_arr2,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_int32,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_byte_arr1,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_float_arr1,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_float_arr1,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_float_arr1,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_byte_arr1,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_float_arr4,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_int32_arr1,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_float,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_float,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_int32_arr1,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_float_arr1,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_float_arr1,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_int32_arr1,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_int32_arr1,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_int32_arr1,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_int64_arr1,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_int32_arr2,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_float,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_int64_arr1,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_int32_arr2,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_int64_arr1,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_int32_arr2,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_rational,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_int32_arr1,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_rational_arr1,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_rational_arr1,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_rational_arr1,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_rational_arr1,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_rational_arr1,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_rational_arr1,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_int32_arr1,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_int64_arr1,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_int64,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_float_arr1,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_int32_arr1,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_int32_arr1,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_int32,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_int32,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_float_arr1,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_int32,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_byte,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_byte_arr1,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_int32,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_int32,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_int32,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_int32,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_int32_arr1,
    },
    {
        .func = guzzi_camera3_metadata_convert_guzzi2hal_int32,
    },
};

guzzi_camera3_metadata_hal2guzzi_t *guzzi_camera3_metadata_tag_to_convert(camera_metadata_tag_t tag)
{
    switch (tag) {
        case ANDROID_BLACK_LEVEL_LOCK:
            return guzzi_camera3_metadata_hal2guzzi + 0;
        case ANDROID_COLOR_CORRECTION_GAINS:
            return guzzi_camera3_metadata_hal2guzzi + 1;
        case ANDROID_COLOR_CORRECTION_MODE:
            return guzzi_camera3_metadata_hal2guzzi + 2;
        case ANDROID_COLOR_CORRECTION_TRANSFORM:
            return guzzi_camera3_metadata_hal2guzzi + 3;
        case ANDROID_CONTROL_AE_ANTIBANDING_MODE:
            return guzzi_camera3_metadata_hal2guzzi + 4;
        case ANDROID_CONTROL_AE_EXPOSURE_COMPENSATION:
            return guzzi_camera3_metadata_hal2guzzi + 5;
        case ANDROID_CONTROL_AE_LOCK:
            return guzzi_camera3_metadata_hal2guzzi + 6;
        case ANDROID_CONTROL_AE_MODE:
            return guzzi_camera3_metadata_hal2guzzi + 7;
        case ANDROID_CONTROL_AE_PRECAPTURE_TRIGGER:
            return guzzi_camera3_metadata_hal2guzzi + 8;
        case ANDROID_CONTROL_AE_REGIONS:
            return guzzi_camera3_metadata_hal2guzzi + 9;
        case ANDROID_CONTROL_AE_TARGET_FPS_RANGE:
            return guzzi_camera3_metadata_hal2guzzi + 10;
        case ANDROID_CONTROL_AF_MODE:
            return guzzi_camera3_metadata_hal2guzzi + 11;
        case ANDROID_CONTROL_AF_REGIONS:
            return guzzi_camera3_metadata_hal2guzzi + 12;
        case ANDROID_CONTROL_AF_TRIGGER:
            return guzzi_camera3_metadata_hal2guzzi + 13;
        case ANDROID_CONTROL_AWB_LOCK:
            return guzzi_camera3_metadata_hal2guzzi + 14;
        case ANDROID_CONTROL_AWB_MODE:
            return guzzi_camera3_metadata_hal2guzzi + 15;
        case ANDROID_CONTROL_AWB_REGIONS:
            return guzzi_camera3_metadata_hal2guzzi + 16;
        case ANDROID_CONTROL_CAPTURE_INTENT:
            return guzzi_camera3_metadata_hal2guzzi + 17;
        case ANDROID_CONTROL_EFFECT_MODE:
            return guzzi_camera3_metadata_hal2guzzi + 18;
        case ANDROID_CONTROL_MODE:
            return guzzi_camera3_metadata_hal2guzzi + 19;
        case ANDROID_CONTROL_SCENE_MODE:
            return guzzi_camera3_metadata_hal2guzzi + 20;
        case ANDROID_CONTROL_VIDEO_STABILIZATION_MODE:
            return guzzi_camera3_metadata_hal2guzzi + 21;
        case ANDROID_DEMOSAIC_MODE:
            return guzzi_camera3_metadata_hal2guzzi + 22;
        case ANDROID_EDGE_MODE:
            return guzzi_camera3_metadata_hal2guzzi + 23;
        case ANDROID_EDGE_STRENGTH:
            return guzzi_camera3_metadata_hal2guzzi + 24;
        case ANDROID_FLASH_FIRING_POWER:
            return guzzi_camera3_metadata_hal2guzzi + 25;
        case ANDROID_FLASH_FIRING_TIME:
            return guzzi_camera3_metadata_hal2guzzi + 26;
        case ANDROID_FLASH_MODE:
            return guzzi_camera3_metadata_hal2guzzi + 27;
        case ANDROID_GEOMETRIC_MODE:
            return guzzi_camera3_metadata_hal2guzzi + 28;
        case ANDROID_GEOMETRIC_STRENGTH:
            return guzzi_camera3_metadata_hal2guzzi + 29;
        case ANDROID_HOT_PIXEL_MODE:
            return guzzi_camera3_metadata_hal2guzzi + 30;
        case ANDROID_JPEG_GPS_COORDINATES:
            return guzzi_camera3_metadata_hal2guzzi + 31;
        case ANDROID_JPEG_GPS_PROCESSING_METHOD:
            return guzzi_camera3_metadata_hal2guzzi + 32;
        case ANDROID_JPEG_GPS_TIMESTAMP:
            return guzzi_camera3_metadata_hal2guzzi + 33;
        case ANDROID_JPEG_ORIENTATION:
            return guzzi_camera3_metadata_hal2guzzi + 34;
        case ANDROID_JPEG_QUALITY:
            return guzzi_camera3_metadata_hal2guzzi + 35;
        case ANDROID_JPEG_THUMBNAIL_QUALITY:
            return guzzi_camera3_metadata_hal2guzzi + 36;
        case ANDROID_JPEG_THUMBNAIL_SIZE:
            return guzzi_camera3_metadata_hal2guzzi + 37;
        case ANDROID_LED_TRANSMIT:
            return guzzi_camera3_metadata_hal2guzzi + 38;
        case ANDROID_LENS_APERTURE:
            return guzzi_camera3_metadata_hal2guzzi + 39;
        case ANDROID_LENS_FILTER_DENSITY:
            return guzzi_camera3_metadata_hal2guzzi + 40;
        case ANDROID_LENS_FOCAL_LENGTH:
            return guzzi_camera3_metadata_hal2guzzi + 41;
        case ANDROID_LENS_FOCUS_DISTANCE:
            return guzzi_camera3_metadata_hal2guzzi + 42;
        case ANDROID_LENS_OPTICAL_STABILIZATION_MODE:
            return guzzi_camera3_metadata_hal2guzzi + 43;
        case ANDROID_NOISE_REDUCTION_MODE:
            return guzzi_camera3_metadata_hal2guzzi + 44;
        case ANDROID_NOISE_REDUCTION_STRENGTH:
            return guzzi_camera3_metadata_hal2guzzi + 45;
        case ANDROID_REQUEST_FRAME_COUNT:
            return guzzi_camera3_metadata_hal2guzzi + 46;
        case ANDROID_REQUEST_ID:
            return guzzi_camera3_metadata_hal2guzzi + 47;
        case ANDROID_REQUEST_INPUT_STREAMS:
            return guzzi_camera3_metadata_hal2guzzi + 48;
        case ANDROID_REQUEST_METADATA_MODE:
            return guzzi_camera3_metadata_hal2guzzi + 49;
        case ANDROID_REQUEST_OUTPUT_STREAMS:
            return guzzi_camera3_metadata_hal2guzzi + 50;
        case ANDROID_REQUEST_TYPE:
            return guzzi_camera3_metadata_hal2guzzi + 51;
        case ANDROID_SCALER_CROP_REGION:
            return guzzi_camera3_metadata_hal2guzzi + 52;
        case ANDROID_SENSOR_EXPOSURE_TIME:
            return guzzi_camera3_metadata_hal2guzzi + 53;
        case ANDROID_SENSOR_FRAME_DURATION:
            return guzzi_camera3_metadata_hal2guzzi + 54;
        case ANDROID_SENSOR_SENSITIVITY:
            return guzzi_camera3_metadata_hal2guzzi + 55;
        case ANDROID_SHADING_MODE:
            return guzzi_camera3_metadata_hal2guzzi + 56;
        case ANDROID_SHADING_STRENGTH:
            return guzzi_camera3_metadata_hal2guzzi + 57;
        case ANDROID_STATISTICS_FACE_DETECT_MODE:
            return guzzi_camera3_metadata_hal2guzzi + 58;
        case ANDROID_STATISTICS_HISTOGRAM_MODE:
            return guzzi_camera3_metadata_hal2guzzi + 59;
        case ANDROID_STATISTICS_LENS_SHADING_MAP_MODE:
            return guzzi_camera3_metadata_hal2guzzi + 60;
        case ANDROID_STATISTICS_SHARPNESS_MAP_MODE:
            return guzzi_camera3_metadata_hal2guzzi + 61;
        case ANDROID_TONEMAP_CURVE_BLUE:
            return guzzi_camera3_metadata_hal2guzzi + 62;
        case ANDROID_TONEMAP_CURVE_GREEN:
            return guzzi_camera3_metadata_hal2guzzi + 63;
        case ANDROID_TONEMAP_CURVE_RED:
            return guzzi_camera3_metadata_hal2guzzi + 64;
        case ANDROID_TONEMAP_MODE:
            return guzzi_camera3_metadata_hal2guzzi + 65;
        default:
            return (void *)0;
    }
}

guzzi_camera3_metadata_guzzi2hal_t *guzzi_camera3_metadata_index_to_convert(guzzi_camera3_metadata_index_t index)
{
    switch (index) {
        case GUZZI_CAMERA3_INDEX_CONTROLS_BLACK_LEVEL_LOCK:
            return guzzi_camera3_metadata_guzzi2hal + 0;
        case GUZZI_CAMERA3_INDEX_CONTROLS_COLOR_CORRECTION_GAINS:
            return guzzi_camera3_metadata_guzzi2hal + 1;
        case GUZZI_CAMERA3_INDEX_CONTROLS_COLOR_CORRECTION_MODE:
            return guzzi_camera3_metadata_guzzi2hal + 2;
        case GUZZI_CAMERA3_INDEX_CONTROLS_COLOR_CORRECTION_TRANSFORM:
            return guzzi_camera3_metadata_guzzi2hal + 3;
        case GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AE_ANTIBANDING_MODE:
            return guzzi_camera3_metadata_guzzi2hal + 4;
        case GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AE_EXPOSURE_COMPENSATION:
            return guzzi_camera3_metadata_guzzi2hal + 5;
        case GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AE_LOCK:
            return guzzi_camera3_metadata_guzzi2hal + 6;
        case GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AE_MODE:
            return guzzi_camera3_metadata_guzzi2hal + 7;
        case GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AE_PRECAPTURE_TRIGGER:
            return guzzi_camera3_metadata_guzzi2hal + 8;
        case GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AE_REGIONS:
            return guzzi_camera3_metadata_guzzi2hal + 9;
        case GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AE_TARGET_FPS_RANGE:
            return guzzi_camera3_metadata_guzzi2hal + 10;
        case GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AF_MODE:
            return guzzi_camera3_metadata_guzzi2hal + 11;
        case GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AF_REGIONS:
            return guzzi_camera3_metadata_guzzi2hal + 12;
        case GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AF_TRIGGER:
            return guzzi_camera3_metadata_guzzi2hal + 13;
        case GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AWB_LOCK:
            return guzzi_camera3_metadata_guzzi2hal + 14;
        case GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AWB_MODE:
            return guzzi_camera3_metadata_guzzi2hal + 15;
        case GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AWB_REGIONS:
            return guzzi_camera3_metadata_guzzi2hal + 16;
        case GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_CAPTURE_INTENT:
            return guzzi_camera3_metadata_guzzi2hal + 17;
        case GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_EFFECT_MODE:
            return guzzi_camera3_metadata_guzzi2hal + 18;
        case GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_MODE:
            return guzzi_camera3_metadata_guzzi2hal + 19;
        case GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_SCENE_MODE:
            return guzzi_camera3_metadata_guzzi2hal + 20;
        case GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_VIDEO_STABILIZATION_MODE:
            return guzzi_camera3_metadata_guzzi2hal + 21;
        case GUZZI_CAMERA3_INDEX_CONTROLS_DEMOSAIC_MODE:
            return guzzi_camera3_metadata_guzzi2hal + 22;
        case GUZZI_CAMERA3_INDEX_CONTROLS_EDGE_MODE:
            return guzzi_camera3_metadata_guzzi2hal + 23;
        case GUZZI_CAMERA3_INDEX_CONTROLS_EDGE_STRENGTH:
            return guzzi_camera3_metadata_guzzi2hal + 24;
        case GUZZI_CAMERA3_INDEX_CONTROLS_FLASH_FIRING_POWER:
            return guzzi_camera3_metadata_guzzi2hal + 25;
        case GUZZI_CAMERA3_INDEX_CONTROLS_FLASH_FIRING_TIME:
            return guzzi_camera3_metadata_guzzi2hal + 26;
        case GUZZI_CAMERA3_INDEX_CONTROLS_FLASH_MODE:
            return guzzi_camera3_metadata_guzzi2hal + 27;
        case GUZZI_CAMERA3_INDEX_CONTROLS_GEOMETRIC_MODE:
            return guzzi_camera3_metadata_guzzi2hal + 28;
        case GUZZI_CAMERA3_INDEX_CONTROLS_GEOMETRIC_STRENGTH:
            return guzzi_camera3_metadata_guzzi2hal + 29;
        case GUZZI_CAMERA3_INDEX_CONTROLS_HOT_PIXEL_MODE:
            return guzzi_camera3_metadata_guzzi2hal + 30;
        case GUZZI_CAMERA3_INDEX_CONTROLS_JPEG_GPS_COORDINATES:
            return guzzi_camera3_metadata_guzzi2hal + 31;
        case GUZZI_CAMERA3_INDEX_CONTROLS_JPEG_GPS_PROCESSING_METHOD:
            return guzzi_camera3_metadata_guzzi2hal + 32;
        case GUZZI_CAMERA3_INDEX_CONTROLS_JPEG_GPS_TIMESTAMP:
            return guzzi_camera3_metadata_guzzi2hal + 33;
        case GUZZI_CAMERA3_INDEX_CONTROLS_JPEG_ORIENTATION:
            return guzzi_camera3_metadata_guzzi2hal + 34;
        case GUZZI_CAMERA3_INDEX_CONTROLS_JPEG_QUALITY:
            return guzzi_camera3_metadata_guzzi2hal + 35;
        case GUZZI_CAMERA3_INDEX_CONTROLS_JPEG_THUMBNAIL_QUALITY:
            return guzzi_camera3_metadata_guzzi2hal + 36;
        case GUZZI_CAMERA3_INDEX_CONTROLS_JPEG_THUMBNAIL_SIZE:
            return guzzi_camera3_metadata_guzzi2hal + 37;
        case GUZZI_CAMERA3_INDEX_CONTROLS_LED_TRANSMIT:
            return guzzi_camera3_metadata_guzzi2hal + 38;
        case GUZZI_CAMERA3_INDEX_CONTROLS_LENS_APERTURE:
            return guzzi_camera3_metadata_guzzi2hal + 39;
        case GUZZI_CAMERA3_INDEX_CONTROLS_LENS_FILTER_DENSITY:
            return guzzi_camera3_metadata_guzzi2hal + 40;
        case GUZZI_CAMERA3_INDEX_CONTROLS_LENS_FOCAL_LENGTH:
            return guzzi_camera3_metadata_guzzi2hal + 41;
        case GUZZI_CAMERA3_INDEX_CONTROLS_LENS_FOCUS_DISTANCE:
            return guzzi_camera3_metadata_guzzi2hal + 42;
        case GUZZI_CAMERA3_INDEX_CONTROLS_LENS_OPTICAL_STABILIZATION_MODE:
            return guzzi_camera3_metadata_guzzi2hal + 43;
        case GUZZI_CAMERA3_INDEX_CONTROLS_NOISE_REDUCTION_MODE:
            return guzzi_camera3_metadata_guzzi2hal + 44;
        case GUZZI_CAMERA3_INDEX_CONTROLS_NOISE_REDUCTION_STRENGTH:
            return guzzi_camera3_metadata_guzzi2hal + 45;
        case GUZZI_CAMERA3_INDEX_CONTROLS_REQUEST_FRAME_COUNT:
            return guzzi_camera3_metadata_guzzi2hal + 46;
        case GUZZI_CAMERA3_INDEX_CONTROLS_REQUEST_ID:
            return guzzi_camera3_metadata_guzzi2hal + 47;
        case GUZZI_CAMERA3_INDEX_CONTROLS_REQUEST_INPUT_STREAMS:
            return guzzi_camera3_metadata_guzzi2hal + 48;
        case GUZZI_CAMERA3_INDEX_CONTROLS_REQUEST_METADATA_MODE:
            return guzzi_camera3_metadata_guzzi2hal + 49;
        case GUZZI_CAMERA3_INDEX_CONTROLS_REQUEST_OUTPUT_STREAMS:
            return guzzi_camera3_metadata_guzzi2hal + 50;
        case GUZZI_CAMERA3_INDEX_CONTROLS_REQUEST_TYPE:
            return guzzi_camera3_metadata_guzzi2hal + 51;
        case GUZZI_CAMERA3_INDEX_CONTROLS_SCALER_CROP_REGION:
            return guzzi_camera3_metadata_guzzi2hal + 52;
        case GUZZI_CAMERA3_INDEX_CONTROLS_SENSOR_EXPOSURE_TIME:
            return guzzi_camera3_metadata_guzzi2hal + 53;
        case GUZZI_CAMERA3_INDEX_CONTROLS_SENSOR_FRAME_DURATION:
            return guzzi_camera3_metadata_guzzi2hal + 54;
        case GUZZI_CAMERA3_INDEX_CONTROLS_SENSOR_SENSITIVITY:
            return guzzi_camera3_metadata_guzzi2hal + 55;
        case GUZZI_CAMERA3_INDEX_CONTROLS_SHADING_MODE:
            return guzzi_camera3_metadata_guzzi2hal + 56;
        case GUZZI_CAMERA3_INDEX_CONTROLS_SHADING_STRENGTH:
            return guzzi_camera3_metadata_guzzi2hal + 57;
        case GUZZI_CAMERA3_INDEX_CONTROLS_STATISTICS_FACE_DETECT_MODE:
            return guzzi_camera3_metadata_guzzi2hal + 58;
        case GUZZI_CAMERA3_INDEX_CONTROLS_STATISTICS_HISTOGRAM_MODE:
            return guzzi_camera3_metadata_guzzi2hal + 59;
        case GUZZI_CAMERA3_INDEX_CONTROLS_STATISTICS_LENS_SHADING_MAP_MODE:
            return guzzi_camera3_metadata_guzzi2hal + 60;
        case GUZZI_CAMERA3_INDEX_CONTROLS_STATISTICS_SHARPNESS_MAP_MODE:
            return guzzi_camera3_metadata_guzzi2hal + 61;
        case GUZZI_CAMERA3_INDEX_CONTROLS_TONEMAP_CURVE_BLUE:
            return guzzi_camera3_metadata_guzzi2hal + 62;
        case GUZZI_CAMERA3_INDEX_CONTROLS_TONEMAP_CURVE_GREEN:
            return guzzi_camera3_metadata_guzzi2hal + 63;
        case GUZZI_CAMERA3_INDEX_CONTROLS_TONEMAP_CURVE_RED:
            return guzzi_camera3_metadata_guzzi2hal + 64;
        case GUZZI_CAMERA3_INDEX_CONTROLS_TONEMAP_MODE:
            return guzzi_camera3_metadata_guzzi2hal + 65;

        case GUZZI_CAMERA3_INDEX_DYNAMIC_BLACK_LEVEL_LOCK:
            return guzzi_camera3_metadata_guzzi2hal + 66;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_COLOR_CORRECTION_GAINS:
            return guzzi_camera3_metadata_guzzi2hal + 67;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_COLOR_CORRECTION_TRANSFORM:
            return guzzi_camera3_metadata_guzzi2hal + 68;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_CONTROL_AE_PRECAPTURE_ID:
            return guzzi_camera3_metadata_guzzi2hal + 69;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_CONTROL_AE_REGIONS:
            return guzzi_camera3_metadata_guzzi2hal + 70;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_CONTROL_AE_STATE:
            return guzzi_camera3_metadata_guzzi2hal + 71;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_CONTROL_AF_MODE:
            return guzzi_camera3_metadata_guzzi2hal + 72;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_CONTROL_AF_REGIONS:
            return guzzi_camera3_metadata_guzzi2hal + 73;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_CONTROL_AF_STATE:
            return guzzi_camera3_metadata_guzzi2hal + 74;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_CONTROL_AF_TRIGGER_ID:
            return guzzi_camera3_metadata_guzzi2hal + 75;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_CONTROL_AWB_MODE:
            return guzzi_camera3_metadata_guzzi2hal + 76;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_CONTROL_AWB_REGIONS:
            return guzzi_camera3_metadata_guzzi2hal + 77;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_CONTROL_AWB_STATE:
            return guzzi_camera3_metadata_guzzi2hal + 78;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_CONTROL_MODE:
            return guzzi_camera3_metadata_guzzi2hal + 79;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_EDGE_MODE:
            return guzzi_camera3_metadata_guzzi2hal + 80;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_FLASH_FIRING_POWER:
            return guzzi_camera3_metadata_guzzi2hal + 81;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_FLASH_FIRING_TIME:
            return guzzi_camera3_metadata_guzzi2hal + 82;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_FLASH_MODE:
            return guzzi_camera3_metadata_guzzi2hal + 83;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_FLASH_STATE:
            return guzzi_camera3_metadata_guzzi2hal + 84;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_HOT_PIXEL_MODE:
            return guzzi_camera3_metadata_guzzi2hal + 85;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_JPEG_GPS_COORDINATES:
            return guzzi_camera3_metadata_guzzi2hal + 86;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_JPEG_GPS_PROCESSING_METHOD:
            return guzzi_camera3_metadata_guzzi2hal + 87;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_JPEG_GPS_TIMESTAMP:
            return guzzi_camera3_metadata_guzzi2hal + 88;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_JPEG_ORIENTATION:
            return guzzi_camera3_metadata_guzzi2hal + 89;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_JPEG_QUALITY:
            return guzzi_camera3_metadata_guzzi2hal + 90;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_JPEG_SIZE:
            return guzzi_camera3_metadata_guzzi2hal + 91;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_JPEG_THUMBNAIL_QUALITY:
            return guzzi_camera3_metadata_guzzi2hal + 92;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_JPEG_THUMBNAIL_SIZE:
            return guzzi_camera3_metadata_guzzi2hal + 93;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_LED_TRANSMIT:
            return guzzi_camera3_metadata_guzzi2hal + 94;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_LENS_APERTURE:
            return guzzi_camera3_metadata_guzzi2hal + 95;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_LENS_FILTER_DENSITY:
            return guzzi_camera3_metadata_guzzi2hal + 96;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_LENS_FOCAL_LENGTH:
            return guzzi_camera3_metadata_guzzi2hal + 97;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_LENS_FOCUS_DISTANCE:
            return guzzi_camera3_metadata_guzzi2hal + 98;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_LENS_FOCUS_RANGE:
            return guzzi_camera3_metadata_guzzi2hal + 99;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_LENS_OPTICAL_STABILIZATION_MODE:
            return guzzi_camera3_metadata_guzzi2hal + 100;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_LENS_STATE:
            return guzzi_camera3_metadata_guzzi2hal + 101;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_NOISE_REDUCTION_MODE:
            return guzzi_camera3_metadata_guzzi2hal + 102;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_QUIRKS_PARTIAL_RESULT:
            return guzzi_camera3_metadata_guzzi2hal + 103;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_REQUEST_FRAME_COUNT:
            return guzzi_camera3_metadata_guzzi2hal + 104;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_REQUEST_ID:
            return guzzi_camera3_metadata_guzzi2hal + 105;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_REQUEST_METADATA_MODE:
            return guzzi_camera3_metadata_guzzi2hal + 106;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_REQUEST_OUTPUT_STREAMS:
            return guzzi_camera3_metadata_guzzi2hal + 107;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_SCALER_CROP_REGION:
            return guzzi_camera3_metadata_guzzi2hal + 108;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_SENSOR_EXPOSURE_TIME:
            return guzzi_camera3_metadata_guzzi2hal + 109;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_SENSOR_FRAME_DURATION:
            return guzzi_camera3_metadata_guzzi2hal + 110;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_SENSOR_SENSITIVITY:
            return guzzi_camera3_metadata_guzzi2hal + 111;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_SENSOR_TEMPERATURE:
            return guzzi_camera3_metadata_guzzi2hal + 112;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_SENSOR_TIMESTAMP:
            return guzzi_camera3_metadata_guzzi2hal + 113;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_SHADING_MODE:
            return guzzi_camera3_metadata_guzzi2hal + 114;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_STATISTICS_FACE_DETECT_MODE:
            return guzzi_camera3_metadata_guzzi2hal + 115;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_STATISTICS_FACE_IDS:
            return guzzi_camera3_metadata_guzzi2hal + 116;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_STATISTICS_FACE_LANDMARKS:
            return guzzi_camera3_metadata_guzzi2hal + 117;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_STATISTICS_FACE_RECTANGLES:
            return guzzi_camera3_metadata_guzzi2hal + 118;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_STATISTICS_FACE_SCORES:
            return guzzi_camera3_metadata_guzzi2hal + 119;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_STATISTICS_HISTOGRAM:
            return guzzi_camera3_metadata_guzzi2hal + 120;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_STATISTICS_HISTOGRAM_MODE:
            return guzzi_camera3_metadata_guzzi2hal + 121;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_STATISTICS_LENS_SHADING_MAP:
            return guzzi_camera3_metadata_guzzi2hal + 122;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_STATISTICS_PREDICTED_COLOR_GAINS:
            return guzzi_camera3_metadata_guzzi2hal + 123;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_STATISTICS_PREDICTED_COLOR_TRANSFORM:
            return guzzi_camera3_metadata_guzzi2hal + 124;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_STATISTICS_SCENE_FLICKER:
            return guzzi_camera3_metadata_guzzi2hal + 125;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_STATISTICS_SHARPNESS_MAP:
            return guzzi_camera3_metadata_guzzi2hal + 126;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_STATISTICS_SHARPNESS_MAP_MODE:
            return guzzi_camera3_metadata_guzzi2hal + 127;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_TONEMAP_CURVE_BLUE:
            return guzzi_camera3_metadata_guzzi2hal + 128;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_TONEMAP_CURVE_GREEN:
            return guzzi_camera3_metadata_guzzi2hal + 129;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_TONEMAP_CURVE_RED:
            return guzzi_camera3_metadata_guzzi2hal + 130;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_TONEMAP_MODE:
            return guzzi_camera3_metadata_guzzi2hal + 131;

        case GUZZI_CAMERA3_INDEX_STATIC_CONTROL_AE_AVAILABLE_ANTIBANDING_MODES:
            return guzzi_camera3_metadata_guzzi2hal + 132;
        case GUZZI_CAMERA3_INDEX_STATIC_CONTROL_AE_AVAILABLE_MODES:
            return guzzi_camera3_metadata_guzzi2hal + 133;
        case GUZZI_CAMERA3_INDEX_STATIC_CONTROL_AE_AVAILABLE_TARGET_FPS_RANGES:
            return guzzi_camera3_metadata_guzzi2hal + 134;
        case GUZZI_CAMERA3_INDEX_STATIC_CONTROL_AE_COMPENSATION_RANGE:
            return guzzi_camera3_metadata_guzzi2hal + 135;
        case GUZZI_CAMERA3_INDEX_STATIC_CONTROL_AE_COMPENSATION_STEP:
            return guzzi_camera3_metadata_guzzi2hal + 136;
        case GUZZI_CAMERA3_INDEX_STATIC_CONTROL_AF_AVAILABLE_MODES:
            return guzzi_camera3_metadata_guzzi2hal + 137;
        case GUZZI_CAMERA3_INDEX_STATIC_CONTROL_AVAILABLE_EFFECTS:
            return guzzi_camera3_metadata_guzzi2hal + 138;
        case GUZZI_CAMERA3_INDEX_STATIC_CONTROL_AVAILABLE_SCENE_MODES:
            return guzzi_camera3_metadata_guzzi2hal + 139;
        case GUZZI_CAMERA3_INDEX_STATIC_CONTROL_AVAILABLE_VIDEO_STABILIZATION_MODES:
            return guzzi_camera3_metadata_guzzi2hal + 140;
        case GUZZI_CAMERA3_INDEX_STATIC_CONTROL_AWB_AVAILABLE_MODES:
            return guzzi_camera3_metadata_guzzi2hal + 141;
        case GUZZI_CAMERA3_INDEX_STATIC_CONTROL_MAX_REGIONS:
            return guzzi_camera3_metadata_guzzi2hal + 142;
        case GUZZI_CAMERA3_INDEX_STATIC_CONTROL_SCENE_MODE_OVERRIDES:
            return guzzi_camera3_metadata_guzzi2hal + 143;
        case GUZZI_CAMERA3_INDEX_STATIC_FLASH_COLOR_TEMPERATURE:
            return guzzi_camera3_metadata_guzzi2hal + 144;
        case GUZZI_CAMERA3_INDEX_STATIC_FLASH_INFO_AVAILABLE:
            return guzzi_camera3_metadata_guzzi2hal + 145;
        case GUZZI_CAMERA3_INDEX_STATIC_FLASH_INFO_CHARGE_DURATION:
            return guzzi_camera3_metadata_guzzi2hal + 146;
        case GUZZI_CAMERA3_INDEX_STATIC_FLASH_MAX_ENERGY:
            return guzzi_camera3_metadata_guzzi2hal + 147;
        case GUZZI_CAMERA3_INDEX_STATIC_HOT_PIXEL_INFO_MAP:
            return guzzi_camera3_metadata_guzzi2hal + 148;
        case GUZZI_CAMERA3_INDEX_STATIC_INFO_SUPPORTED_HARDWARE_LEVEL:
            return guzzi_camera3_metadata_guzzi2hal + 149;
        case GUZZI_CAMERA3_INDEX_STATIC_JPEG_AVAILABLE_THUMBNAIL_SIZES:
            return guzzi_camera3_metadata_guzzi2hal + 150;
        case GUZZI_CAMERA3_INDEX_STATIC_JPEG_MAX_SIZE:
            return guzzi_camera3_metadata_guzzi2hal + 151;
        case GUZZI_CAMERA3_INDEX_STATIC_LED_AVAILABLE_LEDS:
            return guzzi_camera3_metadata_guzzi2hal + 152;
        case GUZZI_CAMERA3_INDEX_STATIC_LENS_FACING:
            return guzzi_camera3_metadata_guzzi2hal + 153;
        case GUZZI_CAMERA3_INDEX_STATIC_LENS_INFO_AVAILABLE_APERTURES:
            return guzzi_camera3_metadata_guzzi2hal + 154;
        case GUZZI_CAMERA3_INDEX_STATIC_LENS_INFO_AVAILABLE_FILTER_DENSITIES:
            return guzzi_camera3_metadata_guzzi2hal + 155;
        case GUZZI_CAMERA3_INDEX_STATIC_LENS_INFO_AVAILABLE_FOCAL_LENGTHS:
            return guzzi_camera3_metadata_guzzi2hal + 156;
        case GUZZI_CAMERA3_INDEX_STATIC_LENS_INFO_AVAILABLE_OPTICAL_STABILIZATION:
            return guzzi_camera3_metadata_guzzi2hal + 157;
        case GUZZI_CAMERA3_INDEX_STATIC_LENS_INFO_GEOMETRIC_CORRECTION_MAP:
            return guzzi_camera3_metadata_guzzi2hal + 158;
        case GUZZI_CAMERA3_INDEX_STATIC_LENS_INFO_GEOMETRIC_CORRECTION_MAP_SIZE:
            return guzzi_camera3_metadata_guzzi2hal + 159;
        case GUZZI_CAMERA3_INDEX_STATIC_LENS_INFO_HYPERFOCAL_DISTANCE:
            return guzzi_camera3_metadata_guzzi2hal + 160;
        case GUZZI_CAMERA3_INDEX_STATIC_LENS_INFO_MINIMUM_FOCUS_DISTANCE:
            return guzzi_camera3_metadata_guzzi2hal + 161;
        case GUZZI_CAMERA3_INDEX_STATIC_LENS_INFO_SHADING_MAP_SIZE:
            return guzzi_camera3_metadata_guzzi2hal + 162;
        case GUZZI_CAMERA3_INDEX_STATIC_LENS_OPTICAL_AXIS_ANGLE:
            return guzzi_camera3_metadata_guzzi2hal + 163;
        case GUZZI_CAMERA3_INDEX_STATIC_LENS_POSITION:
            return guzzi_camera3_metadata_guzzi2hal + 164;
        case GUZZI_CAMERA3_INDEX_STATIC_QUIRKS_METERING_CROP_REGION:
            return guzzi_camera3_metadata_guzzi2hal + 165;
        case GUZZI_CAMERA3_INDEX_STATIC_QUIRKS_TRIGGER_AF_WITH_AUTO:
            return guzzi_camera3_metadata_guzzi2hal + 166;
        case GUZZI_CAMERA3_INDEX_STATIC_QUIRKS_USE_PARTIAL_RESULT:
            return guzzi_camera3_metadata_guzzi2hal + 167;
        case GUZZI_CAMERA3_INDEX_STATIC_QUIRKS_USE_ZSL_FORMAT:
            return guzzi_camera3_metadata_guzzi2hal + 168;
        case GUZZI_CAMERA3_INDEX_STATIC_REQUEST_MAX_NUM_OUTPUT_STREAMS:
            return guzzi_camera3_metadata_guzzi2hal + 169;
        case GUZZI_CAMERA3_INDEX_STATIC_REQUEST_MAX_NUM_REPROCESS_STREAMS:
            return guzzi_camera3_metadata_guzzi2hal + 170;
        case GUZZI_CAMERA3_INDEX_STATIC_SCALER_AVAILABLE_FORMATS:
            return guzzi_camera3_metadata_guzzi2hal + 171;
        case GUZZI_CAMERA3_INDEX_STATIC_SCALER_AVAILABLE_JPEG_MIN_DURATIONS:
            return guzzi_camera3_metadata_guzzi2hal + 172;
        case GUZZI_CAMERA3_INDEX_STATIC_SCALER_AVAILABLE_JPEG_SIZES:
            return guzzi_camera3_metadata_guzzi2hal + 173;
        case GUZZI_CAMERA3_INDEX_STATIC_SCALER_AVAILABLE_MAX_DIGITAL_ZOOM:
            return guzzi_camera3_metadata_guzzi2hal + 174;
        case GUZZI_CAMERA3_INDEX_STATIC_SCALER_AVAILABLE_PROCESSED_MIN_DURATIONS:
            return guzzi_camera3_metadata_guzzi2hal + 175;
        case GUZZI_CAMERA3_INDEX_STATIC_SCALER_AVAILABLE_PROCESSED_SIZES:
            return guzzi_camera3_metadata_guzzi2hal + 176;
        case GUZZI_CAMERA3_INDEX_STATIC_SCALER_AVAILABLE_RAW_MIN_DURATIONS:
            return guzzi_camera3_metadata_guzzi2hal + 177;
        case GUZZI_CAMERA3_INDEX_STATIC_SCALER_AVAILABLE_RAW_SIZES:
            return guzzi_camera3_metadata_guzzi2hal + 178;
        case GUZZI_CAMERA3_INDEX_STATIC_SENSOR_BASE_GAIN_FACTOR:
            return guzzi_camera3_metadata_guzzi2hal + 179;
        case GUZZI_CAMERA3_INDEX_STATIC_SENSOR_BLACK_LEVEL_PATTERN:
            return guzzi_camera3_metadata_guzzi2hal + 180;
        case GUZZI_CAMERA3_INDEX_STATIC_SENSOR_CALIBRATION_TRANSFORM1:
            return guzzi_camera3_metadata_guzzi2hal + 181;
        case GUZZI_CAMERA3_INDEX_STATIC_SENSOR_CALIBRATION_TRANSFORM2:
            return guzzi_camera3_metadata_guzzi2hal + 182;
        case GUZZI_CAMERA3_INDEX_STATIC_SENSOR_COLOR_TRANSFORM1:
            return guzzi_camera3_metadata_guzzi2hal + 183;
        case GUZZI_CAMERA3_INDEX_STATIC_SENSOR_COLOR_TRANSFORM2:
            return guzzi_camera3_metadata_guzzi2hal + 184;
        case GUZZI_CAMERA3_INDEX_STATIC_SENSOR_FORWARD_MATRIX1:
            return guzzi_camera3_metadata_guzzi2hal + 185;
        case GUZZI_CAMERA3_INDEX_STATIC_SENSOR_FORWARD_MATRIX2:
            return guzzi_camera3_metadata_guzzi2hal + 186;
        case GUZZI_CAMERA3_INDEX_STATIC_SENSOR_INFO_ACTIVE_ARRAY_SIZE:
            return guzzi_camera3_metadata_guzzi2hal + 187;
        case GUZZI_CAMERA3_INDEX_STATIC_SENSOR_INFO_COLOR_FILTER_ARRANGEMENT:
            return guzzi_camera3_metadata_guzzi2hal + 188;
        case GUZZI_CAMERA3_INDEX_STATIC_SENSOR_INFO_EXPOSURE_TIME_RANGE:
            return guzzi_camera3_metadata_guzzi2hal + 189;
        case GUZZI_CAMERA3_INDEX_STATIC_SENSOR_INFO_MAX_FRAME_DURATION:
            return guzzi_camera3_metadata_guzzi2hal + 190;
        case GUZZI_CAMERA3_INDEX_STATIC_SENSOR_INFO_PHYSICAL_SIZE:
            return guzzi_camera3_metadata_guzzi2hal + 191;
        case GUZZI_CAMERA3_INDEX_STATIC_SENSOR_INFO_PIXEL_ARRAY_SIZE:
            return guzzi_camera3_metadata_guzzi2hal + 192;
        case GUZZI_CAMERA3_INDEX_STATIC_SENSOR_INFO_SENSITIVITY_RANGE:
            return guzzi_camera3_metadata_guzzi2hal + 193;
        case GUZZI_CAMERA3_INDEX_STATIC_SENSOR_INFO_WHITE_LEVEL:
            return guzzi_camera3_metadata_guzzi2hal + 194;
        case GUZZI_CAMERA3_INDEX_STATIC_SENSOR_MAX_ANALOG_SENSITIVITY:
            return guzzi_camera3_metadata_guzzi2hal + 195;
        case GUZZI_CAMERA3_INDEX_STATIC_SENSOR_NOISE_MODEL_COEFFICIENTS:
            return guzzi_camera3_metadata_guzzi2hal + 196;
        case GUZZI_CAMERA3_INDEX_STATIC_SENSOR_ORIENTATION:
            return guzzi_camera3_metadata_guzzi2hal + 197;
        case GUZZI_CAMERA3_INDEX_STATIC_SENSOR_REFERENCE_ILLUMINANT1:
            return guzzi_camera3_metadata_guzzi2hal + 198;
        case GUZZI_CAMERA3_INDEX_STATIC_SENSOR_REFERENCE_ILLUMINANT2:
            return guzzi_camera3_metadata_guzzi2hal + 199;
        case GUZZI_CAMERA3_INDEX_STATIC_STATISTICS_INFO_AVAILABLE_FACE_DETECT_MODES:
            return guzzi_camera3_metadata_guzzi2hal + 200;
        case GUZZI_CAMERA3_INDEX_STATIC_STATISTICS_INFO_HISTOGRAM_BUCKET_COUNT:
            return guzzi_camera3_metadata_guzzi2hal + 201;
        case GUZZI_CAMERA3_INDEX_STATIC_STATISTICS_INFO_MAX_FACE_COUNT:
            return guzzi_camera3_metadata_guzzi2hal + 202;
        case GUZZI_CAMERA3_INDEX_STATIC_STATISTICS_INFO_MAX_HISTOGRAM_COUNT:
            return guzzi_camera3_metadata_guzzi2hal + 203;
        case GUZZI_CAMERA3_INDEX_STATIC_STATISTICS_INFO_MAX_SHARPNESS_MAP_VALUE:
            return guzzi_camera3_metadata_guzzi2hal + 204;
        case GUZZI_CAMERA3_INDEX_STATIC_STATISTICS_INFO_SHARPNESS_MAP_SIZE:
            return guzzi_camera3_metadata_guzzi2hal + 205;
        case GUZZI_CAMERA3_INDEX_STATIC_TONEMAP_MAX_CURVE_POINTS:
            return guzzi_camera3_metadata_guzzi2hal + 206;

        default:
            return (void *)0;
    }
}

guzzi_camera3_metadata_index_t guzzi_camera3_metadata_controls_tag2idx(camera_metadata_tag_t tag)
{
    switch (tag) {
        case ANDROID_BLACK_LEVEL_LOCK:
            return GUZZI_CAMERA3_INDEX_CONTROLS_BLACK_LEVEL_LOCK;
        case ANDROID_COLOR_CORRECTION_GAINS:
            return GUZZI_CAMERA3_INDEX_CONTROLS_COLOR_CORRECTION_GAINS;
        case ANDROID_COLOR_CORRECTION_MODE:
            return GUZZI_CAMERA3_INDEX_CONTROLS_COLOR_CORRECTION_MODE;
        case ANDROID_COLOR_CORRECTION_TRANSFORM:
            return GUZZI_CAMERA3_INDEX_CONTROLS_COLOR_CORRECTION_TRANSFORM;
        case ANDROID_CONTROL_AE_ANTIBANDING_MODE:
            return GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AE_ANTIBANDING_MODE;
        case ANDROID_CONTROL_AE_EXPOSURE_COMPENSATION:
            return GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AE_EXPOSURE_COMPENSATION;
        case ANDROID_CONTROL_AE_LOCK:
            return GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AE_LOCK;
        case ANDROID_CONTROL_AE_MODE:
            return GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AE_MODE;
        case ANDROID_CONTROL_AE_PRECAPTURE_TRIGGER:
            return GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AE_PRECAPTURE_TRIGGER;
        case ANDROID_CONTROL_AE_REGIONS:
            return GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AE_REGIONS;
        case ANDROID_CONTROL_AE_TARGET_FPS_RANGE:
            return GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AE_TARGET_FPS_RANGE;
        case ANDROID_CONTROL_AF_MODE:
            return GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AF_MODE;
        case ANDROID_CONTROL_AF_REGIONS:
            return GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AF_REGIONS;
        case ANDROID_CONTROL_AF_TRIGGER:
            return GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AF_TRIGGER;
        case ANDROID_CONTROL_AWB_LOCK:
            return GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AWB_LOCK;
        case ANDROID_CONTROL_AWB_MODE:
            return GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AWB_MODE;
        case ANDROID_CONTROL_AWB_REGIONS:
            return GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AWB_REGIONS;
        case ANDROID_CONTROL_CAPTURE_INTENT:
            return GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_CAPTURE_INTENT;
        case ANDROID_CONTROL_EFFECT_MODE:
            return GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_EFFECT_MODE;
        case ANDROID_CONTROL_MODE:
            return GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_MODE;
        case ANDROID_CONTROL_SCENE_MODE:
            return GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_SCENE_MODE;
        case ANDROID_CONTROL_VIDEO_STABILIZATION_MODE:
            return GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_VIDEO_STABILIZATION_MODE;
        case ANDROID_DEMOSAIC_MODE:
            return GUZZI_CAMERA3_INDEX_CONTROLS_DEMOSAIC_MODE;
        case ANDROID_EDGE_MODE:
            return GUZZI_CAMERA3_INDEX_CONTROLS_EDGE_MODE;
        case ANDROID_EDGE_STRENGTH:
            return GUZZI_CAMERA3_INDEX_CONTROLS_EDGE_STRENGTH;
        case ANDROID_FLASH_FIRING_POWER:
            return GUZZI_CAMERA3_INDEX_CONTROLS_FLASH_FIRING_POWER;
        case ANDROID_FLASH_FIRING_TIME:
            return GUZZI_CAMERA3_INDEX_CONTROLS_FLASH_FIRING_TIME;
        case ANDROID_FLASH_MODE:
            return GUZZI_CAMERA3_INDEX_CONTROLS_FLASH_MODE;
        case ANDROID_GEOMETRIC_MODE:
            return GUZZI_CAMERA3_INDEX_CONTROLS_GEOMETRIC_MODE;
        case ANDROID_GEOMETRIC_STRENGTH:
            return GUZZI_CAMERA3_INDEX_CONTROLS_GEOMETRIC_STRENGTH;
        case ANDROID_HOT_PIXEL_MODE:
            return GUZZI_CAMERA3_INDEX_CONTROLS_HOT_PIXEL_MODE;
        case ANDROID_JPEG_GPS_COORDINATES:
            return GUZZI_CAMERA3_INDEX_CONTROLS_JPEG_GPS_COORDINATES;
        case ANDROID_JPEG_GPS_PROCESSING_METHOD:
            return GUZZI_CAMERA3_INDEX_CONTROLS_JPEG_GPS_PROCESSING_METHOD;
        case ANDROID_JPEG_GPS_TIMESTAMP:
            return GUZZI_CAMERA3_INDEX_CONTROLS_JPEG_GPS_TIMESTAMP;
        case ANDROID_JPEG_ORIENTATION:
            return GUZZI_CAMERA3_INDEX_CONTROLS_JPEG_ORIENTATION;
        case ANDROID_JPEG_QUALITY:
            return GUZZI_CAMERA3_INDEX_CONTROLS_JPEG_QUALITY;
        case ANDROID_JPEG_THUMBNAIL_QUALITY:
            return GUZZI_CAMERA3_INDEX_CONTROLS_JPEG_THUMBNAIL_QUALITY;
        case ANDROID_JPEG_THUMBNAIL_SIZE:
            return GUZZI_CAMERA3_INDEX_CONTROLS_JPEG_THUMBNAIL_SIZE;
        case ANDROID_LED_TRANSMIT:
            return GUZZI_CAMERA3_INDEX_CONTROLS_LED_TRANSMIT;
        case ANDROID_LENS_APERTURE:
            return GUZZI_CAMERA3_INDEX_CONTROLS_LENS_APERTURE;
        case ANDROID_LENS_FILTER_DENSITY:
            return GUZZI_CAMERA3_INDEX_CONTROLS_LENS_FILTER_DENSITY;
        case ANDROID_LENS_FOCAL_LENGTH:
            return GUZZI_CAMERA3_INDEX_CONTROLS_LENS_FOCAL_LENGTH;
        case ANDROID_LENS_FOCUS_DISTANCE:
            return GUZZI_CAMERA3_INDEX_CONTROLS_LENS_FOCUS_DISTANCE;
        case ANDROID_LENS_OPTICAL_STABILIZATION_MODE:
            return GUZZI_CAMERA3_INDEX_CONTROLS_LENS_OPTICAL_STABILIZATION_MODE;
        case ANDROID_NOISE_REDUCTION_MODE:
            return GUZZI_CAMERA3_INDEX_CONTROLS_NOISE_REDUCTION_MODE;
        case ANDROID_NOISE_REDUCTION_STRENGTH:
            return GUZZI_CAMERA3_INDEX_CONTROLS_NOISE_REDUCTION_STRENGTH;
        case ANDROID_REQUEST_FRAME_COUNT:
            return GUZZI_CAMERA3_INDEX_CONTROLS_REQUEST_FRAME_COUNT;
        case ANDROID_REQUEST_ID:
            return GUZZI_CAMERA3_INDEX_CONTROLS_REQUEST_ID;
        case ANDROID_REQUEST_INPUT_STREAMS:
            return GUZZI_CAMERA3_INDEX_CONTROLS_REQUEST_INPUT_STREAMS;
        case ANDROID_REQUEST_METADATA_MODE:
            return GUZZI_CAMERA3_INDEX_CONTROLS_REQUEST_METADATA_MODE;
        case ANDROID_REQUEST_OUTPUT_STREAMS:
            return GUZZI_CAMERA3_INDEX_CONTROLS_REQUEST_OUTPUT_STREAMS;
        case ANDROID_REQUEST_TYPE:
            return GUZZI_CAMERA3_INDEX_CONTROLS_REQUEST_TYPE;
        case ANDROID_SCALER_CROP_REGION:
            return GUZZI_CAMERA3_INDEX_CONTROLS_SCALER_CROP_REGION;
        case ANDROID_SENSOR_EXPOSURE_TIME:
            return GUZZI_CAMERA3_INDEX_CONTROLS_SENSOR_EXPOSURE_TIME;
        case ANDROID_SENSOR_FRAME_DURATION:
            return GUZZI_CAMERA3_INDEX_CONTROLS_SENSOR_FRAME_DURATION;
        case ANDROID_SENSOR_SENSITIVITY:
            return GUZZI_CAMERA3_INDEX_CONTROLS_SENSOR_SENSITIVITY;
        case ANDROID_SHADING_MODE:
            return GUZZI_CAMERA3_INDEX_CONTROLS_SHADING_MODE;
        case ANDROID_SHADING_STRENGTH:
            return GUZZI_CAMERA3_INDEX_CONTROLS_SHADING_STRENGTH;
        case ANDROID_STATISTICS_FACE_DETECT_MODE:
            return GUZZI_CAMERA3_INDEX_CONTROLS_STATISTICS_FACE_DETECT_MODE;
        case ANDROID_STATISTICS_HISTOGRAM_MODE:
            return GUZZI_CAMERA3_INDEX_CONTROLS_STATISTICS_HISTOGRAM_MODE;
        case ANDROID_STATISTICS_LENS_SHADING_MAP_MODE:
            return GUZZI_CAMERA3_INDEX_CONTROLS_STATISTICS_LENS_SHADING_MAP_MODE;
        case ANDROID_STATISTICS_SHARPNESS_MAP_MODE:
            return GUZZI_CAMERA3_INDEX_CONTROLS_STATISTICS_SHARPNESS_MAP_MODE;
        case ANDROID_TONEMAP_CURVE_BLUE:
            return GUZZI_CAMERA3_INDEX_CONTROLS_TONEMAP_CURVE_BLUE;
        case ANDROID_TONEMAP_CURVE_GREEN:
            return GUZZI_CAMERA3_INDEX_CONTROLS_TONEMAP_CURVE_GREEN;
        case ANDROID_TONEMAP_CURVE_RED:
            return GUZZI_CAMERA3_INDEX_CONTROLS_TONEMAP_CURVE_RED;
        case ANDROID_TONEMAP_MODE:
            return GUZZI_CAMERA3_INDEX_CONTROLS_TONEMAP_MODE;
        default:
            return GUZZI_CAMERA3_INDEX_INVALID;
    }
}

camera_metadata_tag_t guzzi_camera3_metadata_idx2tag(guzzi_camera3_metadata_index_t index)
{
    switch (index) {
        case GUZZI_CAMERA3_INDEX_CONTROLS_BLACK_LEVEL_LOCK:
            return ANDROID_BLACK_LEVEL_LOCK;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_BLACK_LEVEL_LOCK:
            return ANDROID_BLACK_LEVEL_LOCK;
        case GUZZI_CAMERA3_INDEX_CONTROLS_COLOR_CORRECTION_GAINS:
            return ANDROID_COLOR_CORRECTION_GAINS;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_COLOR_CORRECTION_GAINS:
            return ANDROID_COLOR_CORRECTION_GAINS;
        case GUZZI_CAMERA3_INDEX_CONTROLS_COLOR_CORRECTION_MODE:
            return ANDROID_COLOR_CORRECTION_MODE;
        case GUZZI_CAMERA3_INDEX_CONTROLS_COLOR_CORRECTION_TRANSFORM:
            return ANDROID_COLOR_CORRECTION_TRANSFORM;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_COLOR_CORRECTION_TRANSFORM:
            return ANDROID_COLOR_CORRECTION_TRANSFORM;
        case GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AE_ANTIBANDING_MODE:
            return ANDROID_CONTROL_AE_ANTIBANDING_MODE;
        case GUZZI_CAMERA3_INDEX_STATIC_CONTROL_AE_AVAILABLE_ANTIBANDING_MODES:
            return ANDROID_CONTROL_AE_AVAILABLE_ANTIBANDING_MODES;
        case GUZZI_CAMERA3_INDEX_STATIC_CONTROL_AE_AVAILABLE_MODES:
            return ANDROID_CONTROL_AE_AVAILABLE_MODES;
        case GUZZI_CAMERA3_INDEX_STATIC_CONTROL_AE_AVAILABLE_TARGET_FPS_RANGES:
            return ANDROID_CONTROL_AE_AVAILABLE_TARGET_FPS_RANGES;
        case GUZZI_CAMERA3_INDEX_STATIC_CONTROL_AE_COMPENSATION_RANGE:
            return ANDROID_CONTROL_AE_COMPENSATION_RANGE;
        case GUZZI_CAMERA3_INDEX_STATIC_CONTROL_AE_COMPENSATION_STEP:
            return ANDROID_CONTROL_AE_COMPENSATION_STEP;
        case GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AE_EXPOSURE_COMPENSATION:
            return ANDROID_CONTROL_AE_EXPOSURE_COMPENSATION;
        case GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AE_LOCK:
            return ANDROID_CONTROL_AE_LOCK;
        case GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AE_MODE:
            return ANDROID_CONTROL_AE_MODE;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_CONTROL_AE_PRECAPTURE_ID:
            return ANDROID_CONTROL_AE_PRECAPTURE_ID;
        case GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AE_PRECAPTURE_TRIGGER:
            return ANDROID_CONTROL_AE_PRECAPTURE_TRIGGER;
        case GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AE_REGIONS:
            return ANDROID_CONTROL_AE_REGIONS;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_CONTROL_AE_REGIONS:
            return ANDROID_CONTROL_AE_REGIONS;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_CONTROL_AE_STATE:
            return ANDROID_CONTROL_AE_STATE;
        case GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AE_TARGET_FPS_RANGE:
            return ANDROID_CONTROL_AE_TARGET_FPS_RANGE;
        case GUZZI_CAMERA3_INDEX_STATIC_CONTROL_AF_AVAILABLE_MODES:
            return ANDROID_CONTROL_AF_AVAILABLE_MODES;
        case GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AF_MODE:
            return ANDROID_CONTROL_AF_MODE;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_CONTROL_AF_MODE:
            return ANDROID_CONTROL_AF_MODE;
        case GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AF_REGIONS:
            return ANDROID_CONTROL_AF_REGIONS;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_CONTROL_AF_REGIONS:
            return ANDROID_CONTROL_AF_REGIONS;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_CONTROL_AF_STATE:
            return ANDROID_CONTROL_AF_STATE;
        case GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AF_TRIGGER:
            return ANDROID_CONTROL_AF_TRIGGER;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_CONTROL_AF_TRIGGER_ID:
            return ANDROID_CONTROL_AF_TRIGGER_ID;
        case GUZZI_CAMERA3_INDEX_STATIC_CONTROL_AVAILABLE_EFFECTS:
            return ANDROID_CONTROL_AVAILABLE_EFFECTS;
        case GUZZI_CAMERA3_INDEX_STATIC_CONTROL_AVAILABLE_SCENE_MODES:
            return ANDROID_CONTROL_AVAILABLE_SCENE_MODES;
        case GUZZI_CAMERA3_INDEX_STATIC_CONTROL_AVAILABLE_VIDEO_STABILIZATION_MODES:
            return ANDROID_CONTROL_AVAILABLE_VIDEO_STABILIZATION_MODES;
        case GUZZI_CAMERA3_INDEX_STATIC_CONTROL_AWB_AVAILABLE_MODES:
            return ANDROID_CONTROL_AWB_AVAILABLE_MODES;
        case GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AWB_LOCK:
            return ANDROID_CONTROL_AWB_LOCK;
        case GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AWB_MODE:
            return ANDROID_CONTROL_AWB_MODE;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_CONTROL_AWB_MODE:
            return ANDROID_CONTROL_AWB_MODE;
        case GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AWB_REGIONS:
            return ANDROID_CONTROL_AWB_REGIONS;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_CONTROL_AWB_REGIONS:
            return ANDROID_CONTROL_AWB_REGIONS;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_CONTROL_AWB_STATE:
            return ANDROID_CONTROL_AWB_STATE;
        case GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_CAPTURE_INTENT:
            return ANDROID_CONTROL_CAPTURE_INTENT;
        case GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_EFFECT_MODE:
            return ANDROID_CONTROL_EFFECT_MODE;
        case GUZZI_CAMERA3_INDEX_STATIC_CONTROL_MAX_REGIONS:
            return ANDROID_CONTROL_MAX_REGIONS;
        case GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_MODE:
            return ANDROID_CONTROL_MODE;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_CONTROL_MODE:
            return ANDROID_CONTROL_MODE;
        case GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_SCENE_MODE:
            return ANDROID_CONTROL_SCENE_MODE;
        case GUZZI_CAMERA3_INDEX_STATIC_CONTROL_SCENE_MODE_OVERRIDES:
            return ANDROID_CONTROL_SCENE_MODE_OVERRIDES;
        case GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_VIDEO_STABILIZATION_MODE:
            return ANDROID_CONTROL_VIDEO_STABILIZATION_MODE;
        case GUZZI_CAMERA3_INDEX_CONTROLS_DEMOSAIC_MODE:
            return ANDROID_DEMOSAIC_MODE;
        case GUZZI_CAMERA3_INDEX_CONTROLS_EDGE_MODE:
            return ANDROID_EDGE_MODE;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_EDGE_MODE:
            return ANDROID_EDGE_MODE;
        case GUZZI_CAMERA3_INDEX_CONTROLS_EDGE_STRENGTH:
            return ANDROID_EDGE_STRENGTH;
        case GUZZI_CAMERA3_INDEX_STATIC_FLASH_COLOR_TEMPERATURE:
            return ANDROID_FLASH_COLOR_TEMPERATURE;
        case GUZZI_CAMERA3_INDEX_CONTROLS_FLASH_FIRING_POWER:
            return ANDROID_FLASH_FIRING_POWER;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_FLASH_FIRING_POWER:
            return ANDROID_FLASH_FIRING_POWER;
        case GUZZI_CAMERA3_INDEX_CONTROLS_FLASH_FIRING_TIME:
            return ANDROID_FLASH_FIRING_TIME;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_FLASH_FIRING_TIME:
            return ANDROID_FLASH_FIRING_TIME;
        case GUZZI_CAMERA3_INDEX_STATIC_FLASH_INFO_AVAILABLE:
            return ANDROID_FLASH_INFO_AVAILABLE;
        case GUZZI_CAMERA3_INDEX_STATIC_FLASH_INFO_CHARGE_DURATION:
            return ANDROID_FLASH_INFO_CHARGE_DURATION;
        case GUZZI_CAMERA3_INDEX_STATIC_FLASH_MAX_ENERGY:
            return ANDROID_FLASH_MAX_ENERGY;
        case GUZZI_CAMERA3_INDEX_CONTROLS_FLASH_MODE:
            return ANDROID_FLASH_MODE;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_FLASH_MODE:
            return ANDROID_FLASH_MODE;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_FLASH_STATE:
            return ANDROID_FLASH_STATE;
        case GUZZI_CAMERA3_INDEX_CONTROLS_GEOMETRIC_MODE:
            return ANDROID_GEOMETRIC_MODE;
        case GUZZI_CAMERA3_INDEX_CONTROLS_GEOMETRIC_STRENGTH:
            return ANDROID_GEOMETRIC_STRENGTH;
        case GUZZI_CAMERA3_INDEX_STATIC_HOT_PIXEL_INFO_MAP:
            return ANDROID_HOT_PIXEL_INFO_MAP;
        case GUZZI_CAMERA3_INDEX_CONTROLS_HOT_PIXEL_MODE:
            return ANDROID_HOT_PIXEL_MODE;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_HOT_PIXEL_MODE:
            return ANDROID_HOT_PIXEL_MODE;
        case GUZZI_CAMERA3_INDEX_STATIC_INFO_SUPPORTED_HARDWARE_LEVEL:
            return ANDROID_INFO_SUPPORTED_HARDWARE_LEVEL;
        case GUZZI_CAMERA3_INDEX_STATIC_JPEG_AVAILABLE_THUMBNAIL_SIZES:
            return ANDROID_JPEG_AVAILABLE_THUMBNAIL_SIZES;
        case GUZZI_CAMERA3_INDEX_CONTROLS_JPEG_GPS_COORDINATES:
            return ANDROID_JPEG_GPS_COORDINATES;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_JPEG_GPS_COORDINATES:
            return ANDROID_JPEG_GPS_COORDINATES;
        case GUZZI_CAMERA3_INDEX_CONTROLS_JPEG_GPS_PROCESSING_METHOD:
            return ANDROID_JPEG_GPS_PROCESSING_METHOD;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_JPEG_GPS_PROCESSING_METHOD:
            return ANDROID_JPEG_GPS_PROCESSING_METHOD;
        case GUZZI_CAMERA3_INDEX_CONTROLS_JPEG_GPS_TIMESTAMP:
            return ANDROID_JPEG_GPS_TIMESTAMP;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_JPEG_GPS_TIMESTAMP:
            return ANDROID_JPEG_GPS_TIMESTAMP;
        case GUZZI_CAMERA3_INDEX_STATIC_JPEG_MAX_SIZE:
            return ANDROID_JPEG_MAX_SIZE;
        case GUZZI_CAMERA3_INDEX_CONTROLS_JPEG_ORIENTATION:
            return ANDROID_JPEG_ORIENTATION;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_JPEG_ORIENTATION:
            return ANDROID_JPEG_ORIENTATION;
        case GUZZI_CAMERA3_INDEX_CONTROLS_JPEG_QUALITY:
            return ANDROID_JPEG_QUALITY;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_JPEG_QUALITY:
            return ANDROID_JPEG_QUALITY;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_JPEG_SIZE:
            return ANDROID_JPEG_SIZE;
        case GUZZI_CAMERA3_INDEX_CONTROLS_JPEG_THUMBNAIL_QUALITY:
            return ANDROID_JPEG_THUMBNAIL_QUALITY;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_JPEG_THUMBNAIL_QUALITY:
            return ANDROID_JPEG_THUMBNAIL_QUALITY;
        case GUZZI_CAMERA3_INDEX_CONTROLS_JPEG_THUMBNAIL_SIZE:
            return ANDROID_JPEG_THUMBNAIL_SIZE;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_JPEG_THUMBNAIL_SIZE:
            return ANDROID_JPEG_THUMBNAIL_SIZE;
        case GUZZI_CAMERA3_INDEX_STATIC_LED_AVAILABLE_LEDS:
            return ANDROID_LED_AVAILABLE_LEDS;
        case GUZZI_CAMERA3_INDEX_CONTROLS_LED_TRANSMIT:
            return ANDROID_LED_TRANSMIT;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_LED_TRANSMIT:
            return ANDROID_LED_TRANSMIT;
        case GUZZI_CAMERA3_INDEX_CONTROLS_LENS_APERTURE:
            return ANDROID_LENS_APERTURE;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_LENS_APERTURE:
            return ANDROID_LENS_APERTURE;
        case GUZZI_CAMERA3_INDEX_STATIC_LENS_FACING:
            return ANDROID_LENS_FACING;
        case GUZZI_CAMERA3_INDEX_CONTROLS_LENS_FILTER_DENSITY:
            return ANDROID_LENS_FILTER_DENSITY;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_LENS_FILTER_DENSITY:
            return ANDROID_LENS_FILTER_DENSITY;
        case GUZZI_CAMERA3_INDEX_CONTROLS_LENS_FOCAL_LENGTH:
            return ANDROID_LENS_FOCAL_LENGTH;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_LENS_FOCAL_LENGTH:
            return ANDROID_LENS_FOCAL_LENGTH;
        case GUZZI_CAMERA3_INDEX_CONTROLS_LENS_FOCUS_DISTANCE:
            return ANDROID_LENS_FOCUS_DISTANCE;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_LENS_FOCUS_DISTANCE:
            return ANDROID_LENS_FOCUS_DISTANCE;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_LENS_FOCUS_RANGE:
            return ANDROID_LENS_FOCUS_RANGE;
        case GUZZI_CAMERA3_INDEX_STATIC_LENS_INFO_AVAILABLE_APERTURES:
            return ANDROID_LENS_INFO_AVAILABLE_APERTURES;
        case GUZZI_CAMERA3_INDEX_STATIC_LENS_INFO_AVAILABLE_FILTER_DENSITIES:
            return ANDROID_LENS_INFO_AVAILABLE_FILTER_DENSITIES;
        case GUZZI_CAMERA3_INDEX_STATIC_LENS_INFO_AVAILABLE_FOCAL_LENGTHS:
            return ANDROID_LENS_INFO_AVAILABLE_FOCAL_LENGTHS;
        case GUZZI_CAMERA3_INDEX_STATIC_LENS_INFO_AVAILABLE_OPTICAL_STABILIZATION:
            return ANDROID_LENS_INFO_AVAILABLE_OPTICAL_STABILIZATION;
        case GUZZI_CAMERA3_INDEX_STATIC_LENS_INFO_GEOMETRIC_CORRECTION_MAP:
            return ANDROID_LENS_INFO_GEOMETRIC_CORRECTION_MAP;
        case GUZZI_CAMERA3_INDEX_STATIC_LENS_INFO_GEOMETRIC_CORRECTION_MAP_SIZE:
            return ANDROID_LENS_INFO_GEOMETRIC_CORRECTION_MAP_SIZE;
        case GUZZI_CAMERA3_INDEX_STATIC_LENS_INFO_HYPERFOCAL_DISTANCE:
            return ANDROID_LENS_INFO_HYPERFOCAL_DISTANCE;
        case GUZZI_CAMERA3_INDEX_STATIC_LENS_INFO_MINIMUM_FOCUS_DISTANCE:
            return ANDROID_LENS_INFO_MINIMUM_FOCUS_DISTANCE;
        case GUZZI_CAMERA3_INDEX_STATIC_LENS_INFO_SHADING_MAP_SIZE:
            return ANDROID_LENS_INFO_SHADING_MAP_SIZE;
        case GUZZI_CAMERA3_INDEX_STATIC_LENS_OPTICAL_AXIS_ANGLE:
            return ANDROID_LENS_OPTICAL_AXIS_ANGLE;
        case GUZZI_CAMERA3_INDEX_CONTROLS_LENS_OPTICAL_STABILIZATION_MODE:
            return ANDROID_LENS_OPTICAL_STABILIZATION_MODE;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_LENS_OPTICAL_STABILIZATION_MODE:
            return ANDROID_LENS_OPTICAL_STABILIZATION_MODE;
        case GUZZI_CAMERA3_INDEX_STATIC_LENS_POSITION:
            return ANDROID_LENS_POSITION;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_LENS_STATE:
            return ANDROID_LENS_STATE;
        case GUZZI_CAMERA3_INDEX_CONTROLS_NOISE_REDUCTION_MODE:
            return ANDROID_NOISE_REDUCTION_MODE;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_NOISE_REDUCTION_MODE:
            return ANDROID_NOISE_REDUCTION_MODE;
        case GUZZI_CAMERA3_INDEX_CONTROLS_NOISE_REDUCTION_STRENGTH:
            return ANDROID_NOISE_REDUCTION_STRENGTH;
        case GUZZI_CAMERA3_INDEX_STATIC_QUIRKS_METERING_CROP_REGION:
            return ANDROID_QUIRKS_METERING_CROP_REGION;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_QUIRKS_PARTIAL_RESULT:
            return ANDROID_QUIRKS_PARTIAL_RESULT;
        case GUZZI_CAMERA3_INDEX_STATIC_QUIRKS_TRIGGER_AF_WITH_AUTO:
            return ANDROID_QUIRKS_TRIGGER_AF_WITH_AUTO;
        case GUZZI_CAMERA3_INDEX_STATIC_QUIRKS_USE_PARTIAL_RESULT:
            return ANDROID_QUIRKS_USE_PARTIAL_RESULT;
        case GUZZI_CAMERA3_INDEX_STATIC_QUIRKS_USE_ZSL_FORMAT:
            return ANDROID_QUIRKS_USE_ZSL_FORMAT;
        case GUZZI_CAMERA3_INDEX_CONTROLS_REQUEST_FRAME_COUNT:
            return ANDROID_REQUEST_FRAME_COUNT;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_REQUEST_FRAME_COUNT:
            return ANDROID_REQUEST_FRAME_COUNT;
        case GUZZI_CAMERA3_INDEX_CONTROLS_REQUEST_ID:
            return ANDROID_REQUEST_ID;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_REQUEST_ID:
            return ANDROID_REQUEST_ID;
        case GUZZI_CAMERA3_INDEX_CONTROLS_REQUEST_INPUT_STREAMS:
            return ANDROID_REQUEST_INPUT_STREAMS;
        case GUZZI_CAMERA3_INDEX_STATIC_REQUEST_MAX_NUM_OUTPUT_STREAMS:
            return ANDROID_REQUEST_MAX_NUM_OUTPUT_STREAMS;
        case GUZZI_CAMERA3_INDEX_STATIC_REQUEST_MAX_NUM_REPROCESS_STREAMS:
            return ANDROID_REQUEST_MAX_NUM_REPROCESS_STREAMS;
        case GUZZI_CAMERA3_INDEX_CONTROLS_REQUEST_METADATA_MODE:
            return ANDROID_REQUEST_METADATA_MODE;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_REQUEST_METADATA_MODE:
            return ANDROID_REQUEST_METADATA_MODE;
        case GUZZI_CAMERA3_INDEX_CONTROLS_REQUEST_OUTPUT_STREAMS:
            return ANDROID_REQUEST_OUTPUT_STREAMS;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_REQUEST_OUTPUT_STREAMS:
            return ANDROID_REQUEST_OUTPUT_STREAMS;
        case GUZZI_CAMERA3_INDEX_CONTROLS_REQUEST_TYPE:
            return ANDROID_REQUEST_TYPE;
        case GUZZI_CAMERA3_INDEX_STATIC_SCALER_AVAILABLE_FORMATS:
            return ANDROID_SCALER_AVAILABLE_FORMATS;
        case GUZZI_CAMERA3_INDEX_STATIC_SCALER_AVAILABLE_JPEG_MIN_DURATIONS:
            return ANDROID_SCALER_AVAILABLE_JPEG_MIN_DURATIONS;
        case GUZZI_CAMERA3_INDEX_STATIC_SCALER_AVAILABLE_JPEG_SIZES:
            return ANDROID_SCALER_AVAILABLE_JPEG_SIZES;
        case GUZZI_CAMERA3_INDEX_STATIC_SCALER_AVAILABLE_MAX_DIGITAL_ZOOM:
            return ANDROID_SCALER_AVAILABLE_MAX_DIGITAL_ZOOM;
        case GUZZI_CAMERA3_INDEX_STATIC_SCALER_AVAILABLE_PROCESSED_MIN_DURATIONS:
            return ANDROID_SCALER_AVAILABLE_PROCESSED_MIN_DURATIONS;
        case GUZZI_CAMERA3_INDEX_STATIC_SCALER_AVAILABLE_PROCESSED_SIZES:
            return ANDROID_SCALER_AVAILABLE_PROCESSED_SIZES;
        case GUZZI_CAMERA3_INDEX_STATIC_SCALER_AVAILABLE_RAW_MIN_DURATIONS:
            return ANDROID_SCALER_AVAILABLE_RAW_MIN_DURATIONS;
        case GUZZI_CAMERA3_INDEX_STATIC_SCALER_AVAILABLE_RAW_SIZES:
            return ANDROID_SCALER_AVAILABLE_RAW_SIZES;
        case GUZZI_CAMERA3_INDEX_CONTROLS_SCALER_CROP_REGION:
            return ANDROID_SCALER_CROP_REGION;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_SCALER_CROP_REGION:
            return ANDROID_SCALER_CROP_REGION;
        case GUZZI_CAMERA3_INDEX_STATIC_SENSOR_BASE_GAIN_FACTOR:
            return ANDROID_SENSOR_BASE_GAIN_FACTOR;
        case GUZZI_CAMERA3_INDEX_STATIC_SENSOR_BLACK_LEVEL_PATTERN:
            return ANDROID_SENSOR_BLACK_LEVEL_PATTERN;
        case GUZZI_CAMERA3_INDEX_STATIC_SENSOR_CALIBRATION_TRANSFORM1:
            return ANDROID_SENSOR_CALIBRATION_TRANSFORM1;
        case GUZZI_CAMERA3_INDEX_STATIC_SENSOR_CALIBRATION_TRANSFORM2:
            return ANDROID_SENSOR_CALIBRATION_TRANSFORM2;
        case GUZZI_CAMERA3_INDEX_STATIC_SENSOR_COLOR_TRANSFORM1:
            return ANDROID_SENSOR_COLOR_TRANSFORM1;
        case GUZZI_CAMERA3_INDEX_STATIC_SENSOR_COLOR_TRANSFORM2:
            return ANDROID_SENSOR_COLOR_TRANSFORM2;
        case GUZZI_CAMERA3_INDEX_CONTROLS_SENSOR_EXPOSURE_TIME:
            return ANDROID_SENSOR_EXPOSURE_TIME;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_SENSOR_EXPOSURE_TIME:
            return ANDROID_SENSOR_EXPOSURE_TIME;
        case GUZZI_CAMERA3_INDEX_STATIC_SENSOR_FORWARD_MATRIX1:
            return ANDROID_SENSOR_FORWARD_MATRIX1;
        case GUZZI_CAMERA3_INDEX_STATIC_SENSOR_FORWARD_MATRIX2:
            return ANDROID_SENSOR_FORWARD_MATRIX2;
        case GUZZI_CAMERA3_INDEX_CONTROLS_SENSOR_FRAME_DURATION:
            return ANDROID_SENSOR_FRAME_DURATION;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_SENSOR_FRAME_DURATION:
            return ANDROID_SENSOR_FRAME_DURATION;
        case GUZZI_CAMERA3_INDEX_STATIC_SENSOR_INFO_ACTIVE_ARRAY_SIZE:
            return ANDROID_SENSOR_INFO_ACTIVE_ARRAY_SIZE;
        case GUZZI_CAMERA3_INDEX_STATIC_SENSOR_INFO_COLOR_FILTER_ARRANGEMENT:
            return ANDROID_SENSOR_INFO_COLOR_FILTER_ARRANGEMENT;
        case GUZZI_CAMERA3_INDEX_STATIC_SENSOR_INFO_EXPOSURE_TIME_RANGE:
            return ANDROID_SENSOR_INFO_EXPOSURE_TIME_RANGE;
        case GUZZI_CAMERA3_INDEX_STATIC_SENSOR_INFO_MAX_FRAME_DURATION:
            return ANDROID_SENSOR_INFO_MAX_FRAME_DURATION;
        case GUZZI_CAMERA3_INDEX_STATIC_SENSOR_INFO_PHYSICAL_SIZE:
            return ANDROID_SENSOR_INFO_PHYSICAL_SIZE;
        case GUZZI_CAMERA3_INDEX_STATIC_SENSOR_INFO_PIXEL_ARRAY_SIZE:
            return ANDROID_SENSOR_INFO_PIXEL_ARRAY_SIZE;
        case GUZZI_CAMERA3_INDEX_STATIC_SENSOR_INFO_SENSITIVITY_RANGE:
            return ANDROID_SENSOR_INFO_SENSITIVITY_RANGE;
        case GUZZI_CAMERA3_INDEX_STATIC_SENSOR_INFO_WHITE_LEVEL:
            return ANDROID_SENSOR_INFO_WHITE_LEVEL;
        case GUZZI_CAMERA3_INDEX_STATIC_SENSOR_MAX_ANALOG_SENSITIVITY:
            return ANDROID_SENSOR_MAX_ANALOG_SENSITIVITY;
        case GUZZI_CAMERA3_INDEX_STATIC_SENSOR_NOISE_MODEL_COEFFICIENTS:
            return ANDROID_SENSOR_NOISE_MODEL_COEFFICIENTS;
        case GUZZI_CAMERA3_INDEX_STATIC_SENSOR_ORIENTATION:
            return ANDROID_SENSOR_ORIENTATION;
        case GUZZI_CAMERA3_INDEX_STATIC_SENSOR_REFERENCE_ILLUMINANT1:
            return ANDROID_SENSOR_REFERENCE_ILLUMINANT1;
        case GUZZI_CAMERA3_INDEX_STATIC_SENSOR_REFERENCE_ILLUMINANT2:
            return ANDROID_SENSOR_REFERENCE_ILLUMINANT2;
        case GUZZI_CAMERA3_INDEX_CONTROLS_SENSOR_SENSITIVITY:
            return ANDROID_SENSOR_SENSITIVITY;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_SENSOR_SENSITIVITY:
            return ANDROID_SENSOR_SENSITIVITY;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_SENSOR_TEMPERATURE:
            return ANDROID_SENSOR_TEMPERATURE;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_SENSOR_TIMESTAMP:
            return ANDROID_SENSOR_TIMESTAMP;
        case GUZZI_CAMERA3_INDEX_CONTROLS_SHADING_MODE:
            return ANDROID_SHADING_MODE;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_SHADING_MODE:
            return ANDROID_SHADING_MODE;
        case GUZZI_CAMERA3_INDEX_CONTROLS_SHADING_STRENGTH:
            return ANDROID_SHADING_STRENGTH;
        case GUZZI_CAMERA3_INDEX_CONTROLS_STATISTICS_FACE_DETECT_MODE:
            return ANDROID_STATISTICS_FACE_DETECT_MODE;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_STATISTICS_FACE_DETECT_MODE:
            return ANDROID_STATISTICS_FACE_DETECT_MODE;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_STATISTICS_FACE_IDS:
            return ANDROID_STATISTICS_FACE_IDS;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_STATISTICS_FACE_LANDMARKS:
            return ANDROID_STATISTICS_FACE_LANDMARKS;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_STATISTICS_FACE_RECTANGLES:
            return ANDROID_STATISTICS_FACE_RECTANGLES;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_STATISTICS_FACE_SCORES:
            return ANDROID_STATISTICS_FACE_SCORES;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_STATISTICS_HISTOGRAM:
            return ANDROID_STATISTICS_HISTOGRAM;
        case GUZZI_CAMERA3_INDEX_CONTROLS_STATISTICS_HISTOGRAM_MODE:
            return ANDROID_STATISTICS_HISTOGRAM_MODE;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_STATISTICS_HISTOGRAM_MODE:
            return ANDROID_STATISTICS_HISTOGRAM_MODE;
        case GUZZI_CAMERA3_INDEX_STATIC_STATISTICS_INFO_AVAILABLE_FACE_DETECT_MODES:
            return ANDROID_STATISTICS_INFO_AVAILABLE_FACE_DETECT_MODES;
        case GUZZI_CAMERA3_INDEX_STATIC_STATISTICS_INFO_HISTOGRAM_BUCKET_COUNT:
            return ANDROID_STATISTICS_INFO_HISTOGRAM_BUCKET_COUNT;
        case GUZZI_CAMERA3_INDEX_STATIC_STATISTICS_INFO_MAX_FACE_COUNT:
            return ANDROID_STATISTICS_INFO_MAX_FACE_COUNT;
        case GUZZI_CAMERA3_INDEX_STATIC_STATISTICS_INFO_MAX_HISTOGRAM_COUNT:
            return ANDROID_STATISTICS_INFO_MAX_HISTOGRAM_COUNT;
        case GUZZI_CAMERA3_INDEX_STATIC_STATISTICS_INFO_MAX_SHARPNESS_MAP_VALUE:
            return ANDROID_STATISTICS_INFO_MAX_SHARPNESS_MAP_VALUE;
        case GUZZI_CAMERA3_INDEX_STATIC_STATISTICS_INFO_SHARPNESS_MAP_SIZE:
            return ANDROID_STATISTICS_INFO_SHARPNESS_MAP_SIZE;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_STATISTICS_LENS_SHADING_MAP:
            return ANDROID_STATISTICS_LENS_SHADING_MAP;
        case GUZZI_CAMERA3_INDEX_CONTROLS_STATISTICS_LENS_SHADING_MAP_MODE:
            return ANDROID_STATISTICS_LENS_SHADING_MAP_MODE;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_STATISTICS_PREDICTED_COLOR_GAINS:
            return ANDROID_STATISTICS_PREDICTED_COLOR_GAINS;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_STATISTICS_PREDICTED_COLOR_TRANSFORM:
            return ANDROID_STATISTICS_PREDICTED_COLOR_TRANSFORM;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_STATISTICS_SCENE_FLICKER:
            return ANDROID_STATISTICS_SCENE_FLICKER;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_STATISTICS_SHARPNESS_MAP:
            return ANDROID_STATISTICS_SHARPNESS_MAP;
        case GUZZI_CAMERA3_INDEX_CONTROLS_STATISTICS_SHARPNESS_MAP_MODE:
            return ANDROID_STATISTICS_SHARPNESS_MAP_MODE;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_STATISTICS_SHARPNESS_MAP_MODE:
            return ANDROID_STATISTICS_SHARPNESS_MAP_MODE;
        case GUZZI_CAMERA3_INDEX_CONTROLS_TONEMAP_CURVE_BLUE:
            return ANDROID_TONEMAP_CURVE_BLUE;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_TONEMAP_CURVE_BLUE:
            return ANDROID_TONEMAP_CURVE_BLUE;
        case GUZZI_CAMERA3_INDEX_CONTROLS_TONEMAP_CURVE_GREEN:
            return ANDROID_TONEMAP_CURVE_GREEN;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_TONEMAP_CURVE_GREEN:
            return ANDROID_TONEMAP_CURVE_GREEN;
        case GUZZI_CAMERA3_INDEX_CONTROLS_TONEMAP_CURVE_RED:
            return ANDROID_TONEMAP_CURVE_RED;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_TONEMAP_CURVE_RED:
            return ANDROID_TONEMAP_CURVE_RED;
        case GUZZI_CAMERA3_INDEX_STATIC_TONEMAP_MAX_CURVE_POINTS:
            return ANDROID_TONEMAP_MAX_CURVE_POINTS;
        case GUZZI_CAMERA3_INDEX_CONTROLS_TONEMAP_MODE:
            return ANDROID_TONEMAP_MODE;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_TONEMAP_MODE:
            return ANDROID_TONEMAP_MODE;
        default:
            return ANDROID_BLACK_LEVEL_END; /* TODO: Invalid */
    }
}

