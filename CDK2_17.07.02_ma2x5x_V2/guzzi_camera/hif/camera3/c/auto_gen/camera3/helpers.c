/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file helpers.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <guzzi/camera3/metadata_index.h>
#include <guzzi/camera3/metadata.h>
#include <guzzi/camera3/helpers.h>

#ifndef STR
#define STR(X) #X
#endif

#ifndef offset_of
#define offset_of(type, field) ((char *)&(((type *)0)->field) - (char *)0)
#endif

static guzzi_camera3_metadata_helper_t guzzi_camera3_metadata_controls_helper[] = {
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_BLACK_LEVEL_LOCK),
        .struct_name = STR(guzzi_camera3_controls_black_level_lock_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_controls_valid_t, black_level_lock),
        .struct_offset = offset_of(guzzi_camera3_metadata_controls_struct_t, black_level_lock),
        .v_offset = offset_of(guzzi_camera3_metadata_controls_t, valid.black_level_lock),
        .s_offset = offset_of(guzzi_camera3_metadata_controls_t, s.black_level_lock),
        .size = sizeof (guzzi_camera3_controls_black_level_lock_t),
        .ident_size = sizeof (guzzi_camera3_controls_black_level_lock_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_BLACK_LEVEL_LOCK,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_COLOR_CORRECTION_GAINS),
        .struct_name = STR(guzzi_camera3_controls_color_correction_gains_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_controls_valid_t, color_correction_gains),
        .struct_offset = offset_of(guzzi_camera3_metadata_controls_struct_t, color_correction_gains),
        .v_offset = offset_of(guzzi_camera3_metadata_controls_t, valid.color_correction_gains),
        .s_offset = offset_of(guzzi_camera3_metadata_controls_t, s.color_correction_gains),
        .size = sizeof (guzzi_camera3_controls_color_correction_gains_t),
        .ident_size = sizeof (guzzi_camera3_controls_color_correction_gains_identity_t),
        .dim_size_1 = GUZZI_CAMERA3_CONTROLS_COLOR_CORRECTION_GAINS_DIM_MAX_SIZE_1,
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_COLOR_CORRECTION_GAINS,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_COLOR_CORRECTION_MODE),
        .struct_name = STR(guzzi_camera3_controls_color_correction_mode_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_controls_valid_t, color_correction_mode),
        .struct_offset = offset_of(guzzi_camera3_metadata_controls_struct_t, color_correction_mode),
        .v_offset = offset_of(guzzi_camera3_metadata_controls_t, valid.color_correction_mode),
        .s_offset = offset_of(guzzi_camera3_metadata_controls_t, s.color_correction_mode),
        .size = sizeof (guzzi_camera3_controls_color_correction_mode_t),
        .ident_size = sizeof (guzzi_camera3_controls_color_correction_mode_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_COLOR_CORRECTION_MODE,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_COLOR_CORRECTION_TRANSFORM),
        .struct_name = STR(guzzi_camera3_controls_color_correction_transform_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_controls_valid_t, color_correction_transform),
        .struct_offset = offset_of(guzzi_camera3_metadata_controls_struct_t, color_correction_transform),
        .v_offset = offset_of(guzzi_camera3_metadata_controls_t, valid.color_correction_transform),
        .s_offset = offset_of(guzzi_camera3_metadata_controls_t, s.color_correction_transform),
        .size = sizeof (guzzi_camera3_controls_color_correction_transform_t),
        .ident_size = sizeof (guzzi_camera3_controls_color_correction_transform_identity_t),
        .dim_size_1 = GUZZI_CAMERA3_CONTROLS_COLOR_CORRECTION_TRANSFORM_DIM_MAX_SIZE_1,
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_COLOR_CORRECTION_TRANSFORM,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AE_ANTIBANDING_MODE),
        .struct_name = STR(guzzi_camera3_controls_control_ae_antibanding_mode_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_controls_valid_t, control_ae_antibanding_mode),
        .struct_offset = offset_of(guzzi_camera3_metadata_controls_struct_t, control_ae_antibanding_mode),
        .v_offset = offset_of(guzzi_camera3_metadata_controls_t, valid.control_ae_antibanding_mode),
        .s_offset = offset_of(guzzi_camera3_metadata_controls_t, s.control_ae_antibanding_mode),
        .size = sizeof (guzzi_camera3_controls_control_ae_antibanding_mode_t),
        .ident_size = sizeof (guzzi_camera3_controls_control_ae_antibanding_mode_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AE_ANTIBANDING_MODE,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AE_EXPOSURE_COMPENSATION),
        .struct_name = STR(guzzi_camera3_controls_control_ae_exposure_compensation_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_controls_valid_t, control_ae_exposure_compensation),
        .struct_offset = offset_of(guzzi_camera3_metadata_controls_struct_t, control_ae_exposure_compensation),
        .v_offset = offset_of(guzzi_camera3_metadata_controls_t, valid.control_ae_exposure_compensation),
        .s_offset = offset_of(guzzi_camera3_metadata_controls_t, s.control_ae_exposure_compensation),
        .size = sizeof (guzzi_camera3_controls_control_ae_exposure_compensation_t),
        .ident_size = sizeof (guzzi_camera3_controls_control_ae_exposure_compensation_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AE_EXPOSURE_COMPENSATION,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AE_LOCK),
        .struct_name = STR(guzzi_camera3_controls_control_ae_lock_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_controls_valid_t, control_ae_lock),
        .struct_offset = offset_of(guzzi_camera3_metadata_controls_struct_t, control_ae_lock),
        .v_offset = offset_of(guzzi_camera3_metadata_controls_t, valid.control_ae_lock),
        .s_offset = offset_of(guzzi_camera3_metadata_controls_t, s.control_ae_lock),
        .size = sizeof (guzzi_camera3_controls_control_ae_lock_t),
        .ident_size = sizeof (guzzi_camera3_controls_control_ae_lock_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AE_LOCK,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AE_MODE),
        .struct_name = STR(guzzi_camera3_controls_control_ae_mode_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_controls_valid_t, control_ae_mode),
        .struct_offset = offset_of(guzzi_camera3_metadata_controls_struct_t, control_ae_mode),
        .v_offset = offset_of(guzzi_camera3_metadata_controls_t, valid.control_ae_mode),
        .s_offset = offset_of(guzzi_camera3_metadata_controls_t, s.control_ae_mode),
        .size = sizeof (guzzi_camera3_controls_control_ae_mode_t),
        .ident_size = sizeof (guzzi_camera3_controls_control_ae_mode_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AE_MODE,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AE_PRECAPTURE_TRIGGER),
        .struct_name = STR(guzzi_camera3_controls_control_ae_precapture_trigger_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_controls_valid_t, control_ae_precapture_trigger),
        .struct_offset = offset_of(guzzi_camera3_metadata_controls_struct_t, control_ae_precapture_trigger),
        .v_offset = offset_of(guzzi_camera3_metadata_controls_t, valid.control_ae_precapture_trigger),
        .s_offset = offset_of(guzzi_camera3_metadata_controls_t, s.control_ae_precapture_trigger),
        .size = sizeof (guzzi_camera3_controls_control_ae_precapture_trigger_t),
        .ident_size = sizeof (guzzi_camera3_controls_control_ae_precapture_trigger_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AE_PRECAPTURE_TRIGGER,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AE_REGIONS),
        .struct_name = STR(guzzi_camera3_controls_control_ae_regions_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_controls_valid_t, control_ae_regions),
        .struct_offset = offset_of(guzzi_camera3_metadata_controls_struct_t, control_ae_regions),
        .v_offset = offset_of(guzzi_camera3_metadata_controls_t, valid.control_ae_regions),
        .s_offset = offset_of(guzzi_camera3_metadata_controls_t, s.control_ae_regions),
        .size = sizeof (guzzi_camera3_controls_control_ae_regions_t),
        .ident_size = sizeof (guzzi_camera3_controls_control_ae_regions_identity_t),
        .dim_size_1 = GUZZI_CAMERA3_CONTROLS_CONTROL_AE_REGIONS_DIM_MAX_SIZE_1,
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AE_REGIONS,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AE_TARGET_FPS_RANGE),
        .struct_name = STR(guzzi_camera3_controls_control_ae_target_fps_range_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_controls_valid_t, control_ae_target_fps_range),
        .struct_offset = offset_of(guzzi_camera3_metadata_controls_struct_t, control_ae_target_fps_range),
        .v_offset = offset_of(guzzi_camera3_metadata_controls_t, valid.control_ae_target_fps_range),
        .s_offset = offset_of(guzzi_camera3_metadata_controls_t, s.control_ae_target_fps_range),
        .size = sizeof (guzzi_camera3_controls_control_ae_target_fps_range_t),
        .ident_size = sizeof (guzzi_camera3_controls_control_ae_target_fps_range_identity_t),
        .dim_size_1 = GUZZI_CAMERA3_CONTROLS_CONTROL_AE_TARGET_FPS_RANGE_DIM_MAX_SIZE_1,
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AE_TARGET_FPS_RANGE,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AF_MODE),
        .struct_name = STR(guzzi_camera3_controls_control_af_mode_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_controls_valid_t, control_af_mode),
        .struct_offset = offset_of(guzzi_camera3_metadata_controls_struct_t, control_af_mode),
        .v_offset = offset_of(guzzi_camera3_metadata_controls_t, valid.control_af_mode),
        .s_offset = offset_of(guzzi_camera3_metadata_controls_t, s.control_af_mode),
        .size = sizeof (guzzi_camera3_controls_control_af_mode_t),
        .ident_size = sizeof (guzzi_camera3_controls_control_af_mode_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AF_MODE,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AF_REGIONS),
        .struct_name = STR(guzzi_camera3_controls_control_af_regions_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_controls_valid_t, control_af_regions),
        .struct_offset = offset_of(guzzi_camera3_metadata_controls_struct_t, control_af_regions),
        .v_offset = offset_of(guzzi_camera3_metadata_controls_t, valid.control_af_regions),
        .s_offset = offset_of(guzzi_camera3_metadata_controls_t, s.control_af_regions),
        .size = sizeof (guzzi_camera3_controls_control_af_regions_t),
        .ident_size = sizeof (guzzi_camera3_controls_control_af_regions_identity_t),
        .dim_size_1 = GUZZI_CAMERA3_CONTROLS_CONTROL_AF_REGIONS_DIM_MAX_SIZE_1,
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AF_REGIONS,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AF_TRIGGER),
        .struct_name = STR(guzzi_camera3_controls_control_af_trigger_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_controls_valid_t, control_af_trigger),
        .struct_offset = offset_of(guzzi_camera3_metadata_controls_struct_t, control_af_trigger),
        .v_offset = offset_of(guzzi_camera3_metadata_controls_t, valid.control_af_trigger),
        .s_offset = offset_of(guzzi_camera3_metadata_controls_t, s.control_af_trigger),
        .size = sizeof (guzzi_camera3_controls_control_af_trigger_t),
        .ident_size = sizeof (guzzi_camera3_controls_control_af_trigger_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AF_TRIGGER,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AWB_LOCK),
        .struct_name = STR(guzzi_camera3_controls_control_awb_lock_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_controls_valid_t, control_awb_lock),
        .struct_offset = offset_of(guzzi_camera3_metadata_controls_struct_t, control_awb_lock),
        .v_offset = offset_of(guzzi_camera3_metadata_controls_t, valid.control_awb_lock),
        .s_offset = offset_of(guzzi_camera3_metadata_controls_t, s.control_awb_lock),
        .size = sizeof (guzzi_camera3_controls_control_awb_lock_t),
        .ident_size = sizeof (guzzi_camera3_controls_control_awb_lock_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AWB_LOCK,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AWB_MODE),
        .struct_name = STR(guzzi_camera3_controls_control_awb_mode_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_controls_valid_t, control_awb_mode),
        .struct_offset = offset_of(guzzi_camera3_metadata_controls_struct_t, control_awb_mode),
        .v_offset = offset_of(guzzi_camera3_metadata_controls_t, valid.control_awb_mode),
        .s_offset = offset_of(guzzi_camera3_metadata_controls_t, s.control_awb_mode),
        .size = sizeof (guzzi_camera3_controls_control_awb_mode_t),
        .ident_size = sizeof (guzzi_camera3_controls_control_awb_mode_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AWB_MODE,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AWB_REGIONS),
        .struct_name = STR(guzzi_camera3_controls_control_awb_regions_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_controls_valid_t, control_awb_regions),
        .struct_offset = offset_of(guzzi_camera3_metadata_controls_struct_t, control_awb_regions),
        .v_offset = offset_of(guzzi_camera3_metadata_controls_t, valid.control_awb_regions),
        .s_offset = offset_of(guzzi_camera3_metadata_controls_t, s.control_awb_regions),
        .size = sizeof (guzzi_camera3_controls_control_awb_regions_t),
        .ident_size = sizeof (guzzi_camera3_controls_control_awb_regions_identity_t),
        .dim_size_1 = GUZZI_CAMERA3_CONTROLS_CONTROL_AWB_REGIONS_DIM_MAX_SIZE_1,
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AWB_REGIONS,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_CAPTURE_INTENT),
        .struct_name = STR(guzzi_camera3_controls_control_capture_intent_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_controls_valid_t, control_capture_intent),
        .struct_offset = offset_of(guzzi_camera3_metadata_controls_struct_t, control_capture_intent),
        .v_offset = offset_of(guzzi_camera3_metadata_controls_t, valid.control_capture_intent),
        .s_offset = offset_of(guzzi_camera3_metadata_controls_t, s.control_capture_intent),
        .size = sizeof (guzzi_camera3_controls_control_capture_intent_t),
        .ident_size = sizeof (guzzi_camera3_controls_control_capture_intent_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_CAPTURE_INTENT,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_EFFECT_MODE),
        .struct_name = STR(guzzi_camera3_controls_control_effect_mode_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_controls_valid_t, control_effect_mode),
        .struct_offset = offset_of(guzzi_camera3_metadata_controls_struct_t, control_effect_mode),
        .v_offset = offset_of(guzzi_camera3_metadata_controls_t, valid.control_effect_mode),
        .s_offset = offset_of(guzzi_camera3_metadata_controls_t, s.control_effect_mode),
        .size = sizeof (guzzi_camera3_controls_control_effect_mode_t),
        .ident_size = sizeof (guzzi_camera3_controls_control_effect_mode_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_EFFECT_MODE,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_MODE),
        .struct_name = STR(guzzi_camera3_controls_control_mode_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_controls_valid_t, control_mode),
        .struct_offset = offset_of(guzzi_camera3_metadata_controls_struct_t, control_mode),
        .v_offset = offset_of(guzzi_camera3_metadata_controls_t, valid.control_mode),
        .s_offset = offset_of(guzzi_camera3_metadata_controls_t, s.control_mode),
        .size = sizeof (guzzi_camera3_controls_control_mode_t),
        .ident_size = sizeof (guzzi_camera3_controls_control_mode_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_MODE,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_SCENE_MODE),
        .struct_name = STR(guzzi_camera3_controls_control_scene_mode_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_controls_valid_t, control_scene_mode),
        .struct_offset = offset_of(guzzi_camera3_metadata_controls_struct_t, control_scene_mode),
        .v_offset = offset_of(guzzi_camera3_metadata_controls_t, valid.control_scene_mode),
        .s_offset = offset_of(guzzi_camera3_metadata_controls_t, s.control_scene_mode),
        .size = sizeof (guzzi_camera3_controls_control_scene_mode_t),
        .ident_size = sizeof (guzzi_camera3_controls_control_scene_mode_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_SCENE_MODE,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_VIDEO_STABILIZATION_MODE),
        .struct_name = STR(guzzi_camera3_controls_control_video_stabilization_mode_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_controls_valid_t, control_video_stabilization_mode),
        .struct_offset = offset_of(guzzi_camera3_metadata_controls_struct_t, control_video_stabilization_mode),
        .v_offset = offset_of(guzzi_camera3_metadata_controls_t, valid.control_video_stabilization_mode),
        .s_offset = offset_of(guzzi_camera3_metadata_controls_t, s.control_video_stabilization_mode),
        .size = sizeof (guzzi_camera3_controls_control_video_stabilization_mode_t),
        .ident_size = sizeof (guzzi_camera3_controls_control_video_stabilization_mode_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_VIDEO_STABILIZATION_MODE,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_DEMOSAIC_MODE),
        .struct_name = STR(guzzi_camera3_controls_demosaic_mode_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_controls_valid_t, demosaic_mode),
        .struct_offset = offset_of(guzzi_camera3_metadata_controls_struct_t, demosaic_mode),
        .v_offset = offset_of(guzzi_camera3_metadata_controls_t, valid.demosaic_mode),
        .s_offset = offset_of(guzzi_camera3_metadata_controls_t, s.demosaic_mode),
        .size = sizeof (guzzi_camera3_controls_demosaic_mode_t),
        .ident_size = sizeof (guzzi_camera3_controls_demosaic_mode_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_DEMOSAIC_MODE,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_EDGE_MODE),
        .struct_name = STR(guzzi_camera3_controls_edge_mode_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_controls_valid_t, edge_mode),
        .struct_offset = offset_of(guzzi_camera3_metadata_controls_struct_t, edge_mode),
        .v_offset = offset_of(guzzi_camera3_metadata_controls_t, valid.edge_mode),
        .s_offset = offset_of(guzzi_camera3_metadata_controls_t, s.edge_mode),
        .size = sizeof (guzzi_camera3_controls_edge_mode_t),
        .ident_size = sizeof (guzzi_camera3_controls_edge_mode_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_EDGE_MODE,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_EDGE_STRENGTH),
        .struct_name = STR(guzzi_camera3_controls_edge_strength_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_controls_valid_t, edge_strength),
        .struct_offset = offset_of(guzzi_camera3_metadata_controls_struct_t, edge_strength),
        .v_offset = offset_of(guzzi_camera3_metadata_controls_t, valid.edge_strength),
        .s_offset = offset_of(guzzi_camera3_metadata_controls_t, s.edge_strength),
        .size = sizeof (guzzi_camera3_controls_edge_strength_t),
        .ident_size = sizeof (guzzi_camera3_controls_edge_strength_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_EDGE_STRENGTH,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_FLASH_FIRING_POWER),
        .struct_name = STR(guzzi_camera3_controls_flash_firing_power_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_controls_valid_t, flash_firing_power),
        .struct_offset = offset_of(guzzi_camera3_metadata_controls_struct_t, flash_firing_power),
        .v_offset = offset_of(guzzi_camera3_metadata_controls_t, valid.flash_firing_power),
        .s_offset = offset_of(guzzi_camera3_metadata_controls_t, s.flash_firing_power),
        .size = sizeof (guzzi_camera3_controls_flash_firing_power_t),
        .ident_size = sizeof (guzzi_camera3_controls_flash_firing_power_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_FLASH_FIRING_POWER,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_FLASH_FIRING_TIME),
        .struct_name = STR(guzzi_camera3_controls_flash_firing_time_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_controls_valid_t, flash_firing_time),
        .struct_offset = offset_of(guzzi_camera3_metadata_controls_struct_t, flash_firing_time),
        .v_offset = offset_of(guzzi_camera3_metadata_controls_t, valid.flash_firing_time),
        .s_offset = offset_of(guzzi_camera3_metadata_controls_t, s.flash_firing_time),
        .size = sizeof (guzzi_camera3_controls_flash_firing_time_t),
        .ident_size = sizeof (guzzi_camera3_controls_flash_firing_time_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_FLASH_FIRING_TIME,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_FLASH_MODE),
        .struct_name = STR(guzzi_camera3_controls_flash_mode_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_controls_valid_t, flash_mode),
        .struct_offset = offset_of(guzzi_camera3_metadata_controls_struct_t, flash_mode),
        .v_offset = offset_of(guzzi_camera3_metadata_controls_t, valid.flash_mode),
        .s_offset = offset_of(guzzi_camera3_metadata_controls_t, s.flash_mode),
        .size = sizeof (guzzi_camera3_controls_flash_mode_t),
        .ident_size = sizeof (guzzi_camera3_controls_flash_mode_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_FLASH_MODE,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_GEOMETRIC_MODE),
        .struct_name = STR(guzzi_camera3_controls_geometric_mode_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_controls_valid_t, geometric_mode),
        .struct_offset = offset_of(guzzi_camera3_metadata_controls_struct_t, geometric_mode),
        .v_offset = offset_of(guzzi_camera3_metadata_controls_t, valid.geometric_mode),
        .s_offset = offset_of(guzzi_camera3_metadata_controls_t, s.geometric_mode),
        .size = sizeof (guzzi_camera3_controls_geometric_mode_t),
        .ident_size = sizeof (guzzi_camera3_controls_geometric_mode_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_GEOMETRIC_MODE,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_GEOMETRIC_STRENGTH),
        .struct_name = STR(guzzi_camera3_controls_geometric_strength_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_controls_valid_t, geometric_strength),
        .struct_offset = offset_of(guzzi_camera3_metadata_controls_struct_t, geometric_strength),
        .v_offset = offset_of(guzzi_camera3_metadata_controls_t, valid.geometric_strength),
        .s_offset = offset_of(guzzi_camera3_metadata_controls_t, s.geometric_strength),
        .size = sizeof (guzzi_camera3_controls_geometric_strength_t),
        .ident_size = sizeof (guzzi_camera3_controls_geometric_strength_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_GEOMETRIC_STRENGTH,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_HOT_PIXEL_MODE),
        .struct_name = STR(guzzi_camera3_controls_hot_pixel_mode_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_controls_valid_t, hot_pixel_mode),
        .struct_offset = offset_of(guzzi_camera3_metadata_controls_struct_t, hot_pixel_mode),
        .v_offset = offset_of(guzzi_camera3_metadata_controls_t, valid.hot_pixel_mode),
        .s_offset = offset_of(guzzi_camera3_metadata_controls_t, s.hot_pixel_mode),
        .size = sizeof (guzzi_camera3_controls_hot_pixel_mode_t),
        .ident_size = sizeof (guzzi_camera3_controls_hot_pixel_mode_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_HOT_PIXEL_MODE,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_JPEG_GPS_COORDINATES),
        .struct_name = STR(guzzi_camera3_controls_jpeg_gps_coordinates_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_controls_valid_t, jpeg_gps_coordinates),
        .struct_offset = offset_of(guzzi_camera3_metadata_controls_struct_t, jpeg_gps_coordinates),
        .v_offset = offset_of(guzzi_camera3_metadata_controls_t, valid.jpeg_gps_coordinates),
        .s_offset = offset_of(guzzi_camera3_metadata_controls_t, s.jpeg_gps_coordinates),
        .size = sizeof (guzzi_camera3_controls_jpeg_gps_coordinates_t),
        .ident_size = sizeof (guzzi_camera3_controls_jpeg_gps_coordinates_identity_t),
        .dim_size_1 = GUZZI_CAMERA3_CONTROLS_JPEG_GPS_COORDINATES_DIM_MAX_SIZE_1,
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_JPEG_GPS_COORDINATES,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_JPEG_GPS_PROCESSING_METHOD),
        .struct_name = STR(guzzi_camera3_controls_jpeg_gps_processing_method_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_controls_valid_t, jpeg_gps_processing_method),
        .struct_offset = offset_of(guzzi_camera3_metadata_controls_struct_t, jpeg_gps_processing_method),
        .v_offset = offset_of(guzzi_camera3_metadata_controls_t, valid.jpeg_gps_processing_method),
        .s_offset = offset_of(guzzi_camera3_metadata_controls_t, s.jpeg_gps_processing_method),
        .size = sizeof (guzzi_camera3_controls_jpeg_gps_processing_method_t),
        .ident_size = sizeof (guzzi_camera3_controls_jpeg_gps_processing_method_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_JPEG_GPS_PROCESSING_METHOD,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_JPEG_GPS_TIMESTAMP),
        .struct_name = STR(guzzi_camera3_controls_jpeg_gps_timestamp_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_controls_valid_t, jpeg_gps_timestamp),
        .struct_offset = offset_of(guzzi_camera3_metadata_controls_struct_t, jpeg_gps_timestamp),
        .v_offset = offset_of(guzzi_camera3_metadata_controls_t, valid.jpeg_gps_timestamp),
        .s_offset = offset_of(guzzi_camera3_metadata_controls_t, s.jpeg_gps_timestamp),
        .size = sizeof (guzzi_camera3_controls_jpeg_gps_timestamp_t),
        .ident_size = sizeof (guzzi_camera3_controls_jpeg_gps_timestamp_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_JPEG_GPS_TIMESTAMP,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_JPEG_ORIENTATION),
        .struct_name = STR(guzzi_camera3_controls_jpeg_orientation_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_controls_valid_t, jpeg_orientation),
        .struct_offset = offset_of(guzzi_camera3_metadata_controls_struct_t, jpeg_orientation),
        .v_offset = offset_of(guzzi_camera3_metadata_controls_t, valid.jpeg_orientation),
        .s_offset = offset_of(guzzi_camera3_metadata_controls_t, s.jpeg_orientation),
        .size = sizeof (guzzi_camera3_controls_jpeg_orientation_t),
        .ident_size = sizeof (guzzi_camera3_controls_jpeg_orientation_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_JPEG_ORIENTATION,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_JPEG_QUALITY),
        .struct_name = STR(guzzi_camera3_controls_jpeg_quality_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_controls_valid_t, jpeg_quality),
        .struct_offset = offset_of(guzzi_camera3_metadata_controls_struct_t, jpeg_quality),
        .v_offset = offset_of(guzzi_camera3_metadata_controls_t, valid.jpeg_quality),
        .s_offset = offset_of(guzzi_camera3_metadata_controls_t, s.jpeg_quality),
        .size = sizeof (guzzi_camera3_controls_jpeg_quality_t),
        .ident_size = sizeof (guzzi_camera3_controls_jpeg_quality_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_JPEG_QUALITY,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_JPEG_THUMBNAIL_QUALITY),
        .struct_name = STR(guzzi_camera3_controls_jpeg_thumbnail_quality_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_controls_valid_t, jpeg_thumbnail_quality),
        .struct_offset = offset_of(guzzi_camera3_metadata_controls_struct_t, jpeg_thumbnail_quality),
        .v_offset = offset_of(guzzi_camera3_metadata_controls_t, valid.jpeg_thumbnail_quality),
        .s_offset = offset_of(guzzi_camera3_metadata_controls_t, s.jpeg_thumbnail_quality),
        .size = sizeof (guzzi_camera3_controls_jpeg_thumbnail_quality_t),
        .ident_size = sizeof (guzzi_camera3_controls_jpeg_thumbnail_quality_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_JPEG_THUMBNAIL_QUALITY,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_JPEG_THUMBNAIL_SIZE),
        .struct_name = STR(guzzi_camera3_controls_jpeg_thumbnail_size_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_controls_valid_t, jpeg_thumbnail_size),
        .struct_offset = offset_of(guzzi_camera3_metadata_controls_struct_t, jpeg_thumbnail_size),
        .v_offset = offset_of(guzzi_camera3_metadata_controls_t, valid.jpeg_thumbnail_size),
        .s_offset = offset_of(guzzi_camera3_metadata_controls_t, s.jpeg_thumbnail_size),
        .size = sizeof (guzzi_camera3_controls_jpeg_thumbnail_size_t),
        .ident_size = sizeof (guzzi_camera3_controls_jpeg_thumbnail_size_identity_t),
        .dim_size_1 = GUZZI_CAMERA3_CONTROLS_JPEG_THUMBNAIL_SIZE_DIM_MAX_SIZE_1,
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_JPEG_THUMBNAIL_SIZE,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_LED_TRANSMIT),
        .struct_name = STR(guzzi_camera3_controls_led_transmit_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_controls_valid_t, led_transmit),
        .struct_offset = offset_of(guzzi_camera3_metadata_controls_struct_t, led_transmit),
        .v_offset = offset_of(guzzi_camera3_metadata_controls_t, valid.led_transmit),
        .s_offset = offset_of(guzzi_camera3_metadata_controls_t, s.led_transmit),
        .size = sizeof (guzzi_camera3_controls_led_transmit_t),
        .ident_size = sizeof (guzzi_camera3_controls_led_transmit_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_LED_TRANSMIT,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_LENS_APERTURE),
        .struct_name = STR(guzzi_camera3_controls_lens_aperture_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_controls_valid_t, lens_aperture),
        .struct_offset = offset_of(guzzi_camera3_metadata_controls_struct_t, lens_aperture),
        .v_offset = offset_of(guzzi_camera3_metadata_controls_t, valid.lens_aperture),
        .s_offset = offset_of(guzzi_camera3_metadata_controls_t, s.lens_aperture),
        .size = sizeof (guzzi_camera3_controls_lens_aperture_t),
        .ident_size = sizeof (guzzi_camera3_controls_lens_aperture_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_LENS_APERTURE,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_LENS_FILTER_DENSITY),
        .struct_name = STR(guzzi_camera3_controls_lens_filter_density_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_controls_valid_t, lens_filter_density),
        .struct_offset = offset_of(guzzi_camera3_metadata_controls_struct_t, lens_filter_density),
        .v_offset = offset_of(guzzi_camera3_metadata_controls_t, valid.lens_filter_density),
        .s_offset = offset_of(guzzi_camera3_metadata_controls_t, s.lens_filter_density),
        .size = sizeof (guzzi_camera3_controls_lens_filter_density_t),
        .ident_size = sizeof (guzzi_camera3_controls_lens_filter_density_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_LENS_FILTER_DENSITY,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_LENS_FOCAL_LENGTH),
        .struct_name = STR(guzzi_camera3_controls_lens_focal_length_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_controls_valid_t, lens_focal_length),
        .struct_offset = offset_of(guzzi_camera3_metadata_controls_struct_t, lens_focal_length),
        .v_offset = offset_of(guzzi_camera3_metadata_controls_t, valid.lens_focal_length),
        .s_offset = offset_of(guzzi_camera3_metadata_controls_t, s.lens_focal_length),
        .size = sizeof (guzzi_camera3_controls_lens_focal_length_t),
        .ident_size = sizeof (guzzi_camera3_controls_lens_focal_length_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_LENS_FOCAL_LENGTH,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_LENS_FOCUS_DISTANCE),
        .struct_name = STR(guzzi_camera3_controls_lens_focus_distance_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_controls_valid_t, lens_focus_distance),
        .struct_offset = offset_of(guzzi_camera3_metadata_controls_struct_t, lens_focus_distance),
        .v_offset = offset_of(guzzi_camera3_metadata_controls_t, valid.lens_focus_distance),
        .s_offset = offset_of(guzzi_camera3_metadata_controls_t, s.lens_focus_distance),
        .size = sizeof (guzzi_camera3_controls_lens_focus_distance_t),
        .ident_size = sizeof (guzzi_camera3_controls_lens_focus_distance_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_LENS_FOCUS_DISTANCE,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_LENS_OPTICAL_STABILIZATION_MODE),
        .struct_name = STR(guzzi_camera3_controls_lens_optical_stabilization_mode_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_controls_valid_t, lens_optical_stabilization_mode),
        .struct_offset = offset_of(guzzi_camera3_metadata_controls_struct_t, lens_optical_stabilization_mode),
        .v_offset = offset_of(guzzi_camera3_metadata_controls_t, valid.lens_optical_stabilization_mode),
        .s_offset = offset_of(guzzi_camera3_metadata_controls_t, s.lens_optical_stabilization_mode),
        .size = sizeof (guzzi_camera3_controls_lens_optical_stabilization_mode_t),
        .ident_size = sizeof (guzzi_camera3_controls_lens_optical_stabilization_mode_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_LENS_OPTICAL_STABILIZATION_MODE,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_NOISE_REDUCTION_MODE),
        .struct_name = STR(guzzi_camera3_controls_noise_reduction_mode_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_controls_valid_t, noise_reduction_mode),
        .struct_offset = offset_of(guzzi_camera3_metadata_controls_struct_t, noise_reduction_mode),
        .v_offset = offset_of(guzzi_camera3_metadata_controls_t, valid.noise_reduction_mode),
        .s_offset = offset_of(guzzi_camera3_metadata_controls_t, s.noise_reduction_mode),
        .size = sizeof (guzzi_camera3_controls_noise_reduction_mode_t),
        .ident_size = sizeof (guzzi_camera3_controls_noise_reduction_mode_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_NOISE_REDUCTION_MODE,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_NOISE_REDUCTION_STRENGTH),
        .struct_name = STR(guzzi_camera3_controls_noise_reduction_strength_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_controls_valid_t, noise_reduction_strength),
        .struct_offset = offset_of(guzzi_camera3_metadata_controls_struct_t, noise_reduction_strength),
        .v_offset = offset_of(guzzi_camera3_metadata_controls_t, valid.noise_reduction_strength),
        .s_offset = offset_of(guzzi_camera3_metadata_controls_t, s.noise_reduction_strength),
        .size = sizeof (guzzi_camera3_controls_noise_reduction_strength_t),
        .ident_size = sizeof (guzzi_camera3_controls_noise_reduction_strength_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_NOISE_REDUCTION_STRENGTH,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_REQUEST_FRAME_COUNT),
        .struct_name = STR(guzzi_camera3_controls_request_frame_count_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_controls_valid_t, request_frame_count),
        .struct_offset = offset_of(guzzi_camera3_metadata_controls_struct_t, request_frame_count),
        .v_offset = offset_of(guzzi_camera3_metadata_controls_t, valid.request_frame_count),
        .s_offset = offset_of(guzzi_camera3_metadata_controls_t, s.request_frame_count),
        .size = sizeof (guzzi_camera3_controls_request_frame_count_t),
        .ident_size = sizeof (guzzi_camera3_controls_request_frame_count_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_REQUEST_FRAME_COUNT,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_REQUEST_ID),
        .struct_name = STR(guzzi_camera3_controls_request_id_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_controls_valid_t, request_id),
        .struct_offset = offset_of(guzzi_camera3_metadata_controls_struct_t, request_id),
        .v_offset = offset_of(guzzi_camera3_metadata_controls_t, valid.request_id),
        .s_offset = offset_of(guzzi_camera3_metadata_controls_t, s.request_id),
        .size = sizeof (guzzi_camera3_controls_request_id_t),
        .ident_size = sizeof (guzzi_camera3_controls_request_id_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_REQUEST_ID,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_REQUEST_INPUT_STREAMS),
        .struct_name = STR(guzzi_camera3_controls_request_input_streams_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_controls_valid_t, request_input_streams),
        .struct_offset = offset_of(guzzi_camera3_metadata_controls_struct_t, request_input_streams),
        .v_offset = offset_of(guzzi_camera3_metadata_controls_t, valid.request_input_streams),
        .s_offset = offset_of(guzzi_camera3_metadata_controls_t, s.request_input_streams),
        .size = sizeof (guzzi_camera3_controls_request_input_streams_t),
        .ident_size = sizeof (guzzi_camera3_controls_request_input_streams_identity_t),
        .dim_size_1 = GUZZI_CAMERA3_CONTROLS_REQUEST_INPUT_STREAMS_DIM_MAX_SIZE_1,
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_REQUEST_INPUT_STREAMS,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_REQUEST_METADATA_MODE),
        .struct_name = STR(guzzi_camera3_controls_request_metadata_mode_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_controls_valid_t, request_metadata_mode),
        .struct_offset = offset_of(guzzi_camera3_metadata_controls_struct_t, request_metadata_mode),
        .v_offset = offset_of(guzzi_camera3_metadata_controls_t, valid.request_metadata_mode),
        .s_offset = offset_of(guzzi_camera3_metadata_controls_t, s.request_metadata_mode),
        .size = sizeof (guzzi_camera3_controls_request_metadata_mode_t),
        .ident_size = sizeof (guzzi_camera3_controls_request_metadata_mode_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_REQUEST_METADATA_MODE,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_REQUEST_OUTPUT_STREAMS),
        .struct_name = STR(guzzi_camera3_controls_request_output_streams_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_controls_valid_t, request_output_streams),
        .struct_offset = offset_of(guzzi_camera3_metadata_controls_struct_t, request_output_streams),
        .v_offset = offset_of(guzzi_camera3_metadata_controls_t, valid.request_output_streams),
        .s_offset = offset_of(guzzi_camera3_metadata_controls_t, s.request_output_streams),
        .size = sizeof (guzzi_camera3_controls_request_output_streams_t),
        .ident_size = sizeof (guzzi_camera3_controls_request_output_streams_identity_t),
        .dim_size_1 = GUZZI_CAMERA3_CONTROLS_REQUEST_OUTPUT_STREAMS_DIM_MAX_SIZE_1,
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_REQUEST_OUTPUT_STREAMS,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_REQUEST_TYPE),
        .struct_name = STR(guzzi_camera3_controls_request_type_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_controls_valid_t, request_type),
        .struct_offset = offset_of(guzzi_camera3_metadata_controls_struct_t, request_type),
        .v_offset = offset_of(guzzi_camera3_metadata_controls_t, valid.request_type),
        .s_offset = offset_of(guzzi_camera3_metadata_controls_t, s.request_type),
        .size = sizeof (guzzi_camera3_controls_request_type_t),
        .ident_size = sizeof (guzzi_camera3_controls_request_type_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_REQUEST_TYPE,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_SCALER_CROP_REGION),
        .struct_name = STR(guzzi_camera3_controls_scaler_crop_region_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_controls_valid_t, scaler_crop_region),
        .struct_offset = offset_of(guzzi_camera3_metadata_controls_struct_t, scaler_crop_region),
        .v_offset = offset_of(guzzi_camera3_metadata_controls_t, valid.scaler_crop_region),
        .s_offset = offset_of(guzzi_camera3_metadata_controls_t, s.scaler_crop_region),
        .size = sizeof (guzzi_camera3_controls_scaler_crop_region_t),
        .ident_size = sizeof (guzzi_camera3_controls_scaler_crop_region_identity_t),
        .dim_size_1 = GUZZI_CAMERA3_CONTROLS_SCALER_CROP_REGION_DIM_MAX_SIZE_1,
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_SCALER_CROP_REGION,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_SENSOR_EXPOSURE_TIME),
        .struct_name = STR(guzzi_camera3_controls_sensor_exposure_time_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_controls_valid_t, sensor_exposure_time),
        .struct_offset = offset_of(guzzi_camera3_metadata_controls_struct_t, sensor_exposure_time),
        .v_offset = offset_of(guzzi_camera3_metadata_controls_t, valid.sensor_exposure_time),
        .s_offset = offset_of(guzzi_camera3_metadata_controls_t, s.sensor_exposure_time),
        .size = sizeof (guzzi_camera3_controls_sensor_exposure_time_t),
        .ident_size = sizeof (guzzi_camera3_controls_sensor_exposure_time_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_SENSOR_EXPOSURE_TIME,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_SENSOR_FRAME_DURATION),
        .struct_name = STR(guzzi_camera3_controls_sensor_frame_duration_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_controls_valid_t, sensor_frame_duration),
        .struct_offset = offset_of(guzzi_camera3_metadata_controls_struct_t, sensor_frame_duration),
        .v_offset = offset_of(guzzi_camera3_metadata_controls_t, valid.sensor_frame_duration),
        .s_offset = offset_of(guzzi_camera3_metadata_controls_t, s.sensor_frame_duration),
        .size = sizeof (guzzi_camera3_controls_sensor_frame_duration_t),
        .ident_size = sizeof (guzzi_camera3_controls_sensor_frame_duration_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_SENSOR_FRAME_DURATION,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_SENSOR_SENSITIVITY),
        .struct_name = STR(guzzi_camera3_controls_sensor_sensitivity_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_controls_valid_t, sensor_sensitivity),
        .struct_offset = offset_of(guzzi_camera3_metadata_controls_struct_t, sensor_sensitivity),
        .v_offset = offset_of(guzzi_camera3_metadata_controls_t, valid.sensor_sensitivity),
        .s_offset = offset_of(guzzi_camera3_metadata_controls_t, s.sensor_sensitivity),
        .size = sizeof (guzzi_camera3_controls_sensor_sensitivity_t),
        .ident_size = sizeof (guzzi_camera3_controls_sensor_sensitivity_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_SENSOR_SENSITIVITY,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_SHADING_MODE),
        .struct_name = STR(guzzi_camera3_controls_shading_mode_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_controls_valid_t, shading_mode),
        .struct_offset = offset_of(guzzi_camera3_metadata_controls_struct_t, shading_mode),
        .v_offset = offset_of(guzzi_camera3_metadata_controls_t, valid.shading_mode),
        .s_offset = offset_of(guzzi_camera3_metadata_controls_t, s.shading_mode),
        .size = sizeof (guzzi_camera3_controls_shading_mode_t),
        .ident_size = sizeof (guzzi_camera3_controls_shading_mode_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_SHADING_MODE,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_SHADING_STRENGTH),
        .struct_name = STR(guzzi_camera3_controls_shading_strength_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_controls_valid_t, shading_strength),
        .struct_offset = offset_of(guzzi_camera3_metadata_controls_struct_t, shading_strength),
        .v_offset = offset_of(guzzi_camera3_metadata_controls_t, valid.shading_strength),
        .s_offset = offset_of(guzzi_camera3_metadata_controls_t, s.shading_strength),
        .size = sizeof (guzzi_camera3_controls_shading_strength_t),
        .ident_size = sizeof (guzzi_camera3_controls_shading_strength_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_SHADING_STRENGTH,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_STATISTICS_FACE_DETECT_MODE),
        .struct_name = STR(guzzi_camera3_controls_statistics_face_detect_mode_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_controls_valid_t, statistics_face_detect_mode),
        .struct_offset = offset_of(guzzi_camera3_metadata_controls_struct_t, statistics_face_detect_mode),
        .v_offset = offset_of(guzzi_camera3_metadata_controls_t, valid.statistics_face_detect_mode),
        .s_offset = offset_of(guzzi_camera3_metadata_controls_t, s.statistics_face_detect_mode),
        .size = sizeof (guzzi_camera3_controls_statistics_face_detect_mode_t),
        .ident_size = sizeof (guzzi_camera3_controls_statistics_face_detect_mode_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_STATISTICS_FACE_DETECT_MODE,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_STATISTICS_HISTOGRAM_MODE),
        .struct_name = STR(guzzi_camera3_controls_statistics_histogram_mode_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_controls_valid_t, statistics_histogram_mode),
        .struct_offset = offset_of(guzzi_camera3_metadata_controls_struct_t, statistics_histogram_mode),
        .v_offset = offset_of(guzzi_camera3_metadata_controls_t, valid.statistics_histogram_mode),
        .s_offset = offset_of(guzzi_camera3_metadata_controls_t, s.statistics_histogram_mode),
        .size = sizeof (guzzi_camera3_controls_statistics_histogram_mode_t),
        .ident_size = sizeof (guzzi_camera3_controls_statistics_histogram_mode_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_STATISTICS_HISTOGRAM_MODE,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_STATISTICS_LENS_SHADING_MAP_MODE),
        .struct_name = STR(guzzi_camera3_controls_statistics_lens_shading_map_mode_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_controls_valid_t, statistics_lens_shading_map_mode),
        .struct_offset = offset_of(guzzi_camera3_metadata_controls_struct_t, statistics_lens_shading_map_mode),
        .v_offset = offset_of(guzzi_camera3_metadata_controls_t, valid.statistics_lens_shading_map_mode),
        .s_offset = offset_of(guzzi_camera3_metadata_controls_t, s.statistics_lens_shading_map_mode),
        .size = sizeof (guzzi_camera3_controls_statistics_lens_shading_map_mode_t),
        .ident_size = sizeof (guzzi_camera3_controls_statistics_lens_shading_map_mode_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_STATISTICS_LENS_SHADING_MAP_MODE,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_STATISTICS_SHARPNESS_MAP_MODE),
        .struct_name = STR(guzzi_camera3_controls_statistics_sharpness_map_mode_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_controls_valid_t, statistics_sharpness_map_mode),
        .struct_offset = offset_of(guzzi_camera3_metadata_controls_struct_t, statistics_sharpness_map_mode),
        .v_offset = offset_of(guzzi_camera3_metadata_controls_t, valid.statistics_sharpness_map_mode),
        .s_offset = offset_of(guzzi_camera3_metadata_controls_t, s.statistics_sharpness_map_mode),
        .size = sizeof (guzzi_camera3_controls_statistics_sharpness_map_mode_t),
        .ident_size = sizeof (guzzi_camera3_controls_statistics_sharpness_map_mode_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_STATISTICS_SHARPNESS_MAP_MODE,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_TONEMAP_CURVE_BLUE),
        .struct_name = STR(guzzi_camera3_controls_tonemap_curve_blue_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_controls_valid_t, tonemap_curve_blue),
        .struct_offset = offset_of(guzzi_camera3_metadata_controls_struct_t, tonemap_curve_blue),
        .v_offset = offset_of(guzzi_camera3_metadata_controls_t, valid.tonemap_curve_blue),
        .s_offset = offset_of(guzzi_camera3_metadata_controls_t, s.tonemap_curve_blue),
        .size = sizeof (guzzi_camera3_controls_tonemap_curve_blue_t),
        .ident_size = sizeof (guzzi_camera3_controls_tonemap_curve_blue_identity_t),
        .dim_size_1 = GUZZI_CAMERA3_CONTROLS_TONEMAP_CURVE_BLUE_DIM_MAX_SIZE_1,
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_TONEMAP_CURVE_BLUE,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_TONEMAP_CURVE_GREEN),
        .struct_name = STR(guzzi_camera3_controls_tonemap_curve_green_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_controls_valid_t, tonemap_curve_green),
        .struct_offset = offset_of(guzzi_camera3_metadata_controls_struct_t, tonemap_curve_green),
        .v_offset = offset_of(guzzi_camera3_metadata_controls_t, valid.tonemap_curve_green),
        .s_offset = offset_of(guzzi_camera3_metadata_controls_t, s.tonemap_curve_green),
        .size = sizeof (guzzi_camera3_controls_tonemap_curve_green_t),
        .ident_size = sizeof (guzzi_camera3_controls_tonemap_curve_green_identity_t),
        .dim_size_1 = GUZZI_CAMERA3_CONTROLS_TONEMAP_CURVE_GREEN_DIM_MAX_SIZE_1,
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_TONEMAP_CURVE_GREEN,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_TONEMAP_CURVE_RED),
        .struct_name = STR(guzzi_camera3_controls_tonemap_curve_red_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_controls_valid_t, tonemap_curve_red),
        .struct_offset = offset_of(guzzi_camera3_metadata_controls_struct_t, tonemap_curve_red),
        .v_offset = offset_of(guzzi_camera3_metadata_controls_t, valid.tonemap_curve_red),
        .s_offset = offset_of(guzzi_camera3_metadata_controls_t, s.tonemap_curve_red),
        .size = sizeof (guzzi_camera3_controls_tonemap_curve_red_t),
        .ident_size = sizeof (guzzi_camera3_controls_tonemap_curve_red_identity_t),
        .dim_size_1 = GUZZI_CAMERA3_CONTROLS_TONEMAP_CURVE_RED_DIM_MAX_SIZE_1,
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_TONEMAP_CURVE_RED,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_TONEMAP_MODE),
        .struct_name = STR(guzzi_camera3_controls_tonemap_mode_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_controls_valid_t, tonemap_mode),
        .struct_offset = offset_of(guzzi_camera3_metadata_controls_struct_t, tonemap_mode),
        .v_offset = offset_of(guzzi_camera3_metadata_controls_t, valid.tonemap_mode),
        .s_offset = offset_of(guzzi_camera3_metadata_controls_t, s.tonemap_mode),
        .size = sizeof (guzzi_camera3_controls_tonemap_mode_t),
        .ident_size = sizeof (guzzi_camera3_controls_tonemap_mode_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_TONEMAP_MODE,
    },
};

static guzzi_camera3_metadata_helper_t guzzi_camera3_metadata_dynamic_helper[] = {
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_DYNAMIC_BLACK_LEVEL_LOCK),
        .struct_name = STR(guzzi_camera3_dynamic_black_level_lock_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_dynamic_valid_t, black_level_lock),
        .struct_offset = offset_of(guzzi_camera3_metadata_dynamic_struct_t, black_level_lock),
        .v_offset = offset_of(guzzi_camera3_metadata_dynamic_t, valid.black_level_lock),
        .s_offset = offset_of(guzzi_camera3_metadata_dynamic_t, s.black_level_lock),
        .size = sizeof (guzzi_camera3_dynamic_black_level_lock_t),
        .ident_size = sizeof (guzzi_camera3_dynamic_black_level_lock_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_BLACK_LEVEL_LOCK,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_DYNAMIC_COLOR_CORRECTION_GAINS),
        .struct_name = STR(guzzi_camera3_dynamic_color_correction_gains_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_dynamic_valid_t, color_correction_gains),
        .struct_offset = offset_of(guzzi_camera3_metadata_dynamic_struct_t, color_correction_gains),
        .v_offset = offset_of(guzzi_camera3_metadata_dynamic_t, valid.color_correction_gains),
        .s_offset = offset_of(guzzi_camera3_metadata_dynamic_t, s.color_correction_gains),
        .size = sizeof (guzzi_camera3_dynamic_color_correction_gains_t),
        .ident_size = sizeof (guzzi_camera3_dynamic_color_correction_gains_identity_t),
        .dim_size_1 = GUZZI_CAMERA3_DYNAMIC_COLOR_CORRECTION_GAINS_DIM_MAX_SIZE_1,
        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_COLOR_CORRECTION_GAINS,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_DYNAMIC_COLOR_CORRECTION_TRANSFORM),
        .struct_name = STR(guzzi_camera3_dynamic_color_correction_transform_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_dynamic_valid_t, color_correction_transform),
        .struct_offset = offset_of(guzzi_camera3_metadata_dynamic_struct_t, color_correction_transform),
        .v_offset = offset_of(guzzi_camera3_metadata_dynamic_t, valid.color_correction_transform),
        .s_offset = offset_of(guzzi_camera3_metadata_dynamic_t, s.color_correction_transform),
        .size = sizeof (guzzi_camera3_dynamic_color_correction_transform_t),
        .ident_size = sizeof (guzzi_camera3_dynamic_color_correction_transform_identity_t),
        .dim_size_1 = GUZZI_CAMERA3_DYNAMIC_COLOR_CORRECTION_TRANSFORM_DIM_MAX_SIZE_1,
        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_COLOR_CORRECTION_TRANSFORM,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_DYNAMIC_CONTROL_AE_PRECAPTURE_ID),
        .struct_name = STR(guzzi_camera3_dynamic_control_ae_precapture_id_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_dynamic_valid_t, control_ae_precapture_id),
        .struct_offset = offset_of(guzzi_camera3_metadata_dynamic_struct_t, control_ae_precapture_id),
        .v_offset = offset_of(guzzi_camera3_metadata_dynamic_t, valid.control_ae_precapture_id),
        .s_offset = offset_of(guzzi_camera3_metadata_dynamic_t, s.control_ae_precapture_id),
        .size = sizeof (guzzi_camera3_dynamic_control_ae_precapture_id_t),
        .ident_size = sizeof (guzzi_camera3_dynamic_control_ae_precapture_id_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_CONTROL_AE_PRECAPTURE_ID,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_DYNAMIC_CONTROL_AE_REGIONS),
        .struct_name = STR(guzzi_camera3_dynamic_control_ae_regions_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_dynamic_valid_t, control_ae_regions),
        .struct_offset = offset_of(guzzi_camera3_metadata_dynamic_struct_t, control_ae_regions),
        .v_offset = offset_of(guzzi_camera3_metadata_dynamic_t, valid.control_ae_regions),
        .s_offset = offset_of(guzzi_camera3_metadata_dynamic_t, s.control_ae_regions),
        .size = sizeof (guzzi_camera3_dynamic_control_ae_regions_t),
        .ident_size = sizeof (guzzi_camera3_dynamic_control_ae_regions_identity_t),
        .dim_size_1 = GUZZI_CAMERA3_DYNAMIC_CONTROL_AE_REGIONS_DIM_MAX_SIZE_1,
        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_CONTROL_AE_REGIONS,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_DYNAMIC_CONTROL_AE_STATE),
        .struct_name = STR(guzzi_camera3_dynamic_control_ae_state_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_dynamic_valid_t, control_ae_state),
        .struct_offset = offset_of(guzzi_camera3_metadata_dynamic_struct_t, control_ae_state),
        .v_offset = offset_of(guzzi_camera3_metadata_dynamic_t, valid.control_ae_state),
        .s_offset = offset_of(guzzi_camera3_metadata_dynamic_t, s.control_ae_state),
        .size = sizeof (guzzi_camera3_dynamic_control_ae_state_t),
        .ident_size = sizeof (guzzi_camera3_dynamic_control_ae_state_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_CONTROL_AE_STATE,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_DYNAMIC_CONTROL_AF_MODE),
        .struct_name = STR(guzzi_camera3_dynamic_control_af_mode_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_dynamic_valid_t, control_af_mode),
        .struct_offset = offset_of(guzzi_camera3_metadata_dynamic_struct_t, control_af_mode),
        .v_offset = offset_of(guzzi_camera3_metadata_dynamic_t, valid.control_af_mode),
        .s_offset = offset_of(guzzi_camera3_metadata_dynamic_t, s.control_af_mode),
        .size = sizeof (guzzi_camera3_dynamic_control_af_mode_t),
        .ident_size = sizeof (guzzi_camera3_dynamic_control_af_mode_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_CONTROL_AF_MODE,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_DYNAMIC_CONTROL_AF_REGIONS),
        .struct_name = STR(guzzi_camera3_dynamic_control_af_regions_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_dynamic_valid_t, control_af_regions),
        .struct_offset = offset_of(guzzi_camera3_metadata_dynamic_struct_t, control_af_regions),
        .v_offset = offset_of(guzzi_camera3_metadata_dynamic_t, valid.control_af_regions),
        .s_offset = offset_of(guzzi_camera3_metadata_dynamic_t, s.control_af_regions),
        .size = sizeof (guzzi_camera3_dynamic_control_af_regions_t),
        .ident_size = sizeof (guzzi_camera3_dynamic_control_af_regions_identity_t),
        .dim_size_1 = GUZZI_CAMERA3_DYNAMIC_CONTROL_AF_REGIONS_DIM_MAX_SIZE_1,
        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_CONTROL_AF_REGIONS,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_DYNAMIC_CONTROL_AF_STATE),
        .struct_name = STR(guzzi_camera3_dynamic_control_af_state_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_dynamic_valid_t, control_af_state),
        .struct_offset = offset_of(guzzi_camera3_metadata_dynamic_struct_t, control_af_state),
        .v_offset = offset_of(guzzi_camera3_metadata_dynamic_t, valid.control_af_state),
        .s_offset = offset_of(guzzi_camera3_metadata_dynamic_t, s.control_af_state),
        .size = sizeof (guzzi_camera3_dynamic_control_af_state_t),
        .ident_size = sizeof (guzzi_camera3_dynamic_control_af_state_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_CONTROL_AF_STATE,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_DYNAMIC_CONTROL_AF_TRIGGER_ID),
        .struct_name = STR(guzzi_camera3_dynamic_control_af_trigger_id_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_dynamic_valid_t, control_af_trigger_id),
        .struct_offset = offset_of(guzzi_camera3_metadata_dynamic_struct_t, control_af_trigger_id),
        .v_offset = offset_of(guzzi_camera3_metadata_dynamic_t, valid.control_af_trigger_id),
        .s_offset = offset_of(guzzi_camera3_metadata_dynamic_t, s.control_af_trigger_id),
        .size = sizeof (guzzi_camera3_dynamic_control_af_trigger_id_t),
        .ident_size = sizeof (guzzi_camera3_dynamic_control_af_trigger_id_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_CONTROL_AF_TRIGGER_ID,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_DYNAMIC_CONTROL_AWB_MODE),
        .struct_name = STR(guzzi_camera3_dynamic_control_awb_mode_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_dynamic_valid_t, control_awb_mode),
        .struct_offset = offset_of(guzzi_camera3_metadata_dynamic_struct_t, control_awb_mode),
        .v_offset = offset_of(guzzi_camera3_metadata_dynamic_t, valid.control_awb_mode),
        .s_offset = offset_of(guzzi_camera3_metadata_dynamic_t, s.control_awb_mode),
        .size = sizeof (guzzi_camera3_dynamic_control_awb_mode_t),
        .ident_size = sizeof (guzzi_camera3_dynamic_control_awb_mode_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_CONTROL_AWB_MODE,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_DYNAMIC_CONTROL_AWB_REGIONS),
        .struct_name = STR(guzzi_camera3_dynamic_control_awb_regions_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_dynamic_valid_t, control_awb_regions),
        .struct_offset = offset_of(guzzi_camera3_metadata_dynamic_struct_t, control_awb_regions),
        .v_offset = offset_of(guzzi_camera3_metadata_dynamic_t, valid.control_awb_regions),
        .s_offset = offset_of(guzzi_camera3_metadata_dynamic_t, s.control_awb_regions),
        .size = sizeof (guzzi_camera3_dynamic_control_awb_regions_t),
        .ident_size = sizeof (guzzi_camera3_dynamic_control_awb_regions_identity_t),
        .dim_size_1 = GUZZI_CAMERA3_DYNAMIC_CONTROL_AWB_REGIONS_DIM_MAX_SIZE_1,
        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_CONTROL_AWB_REGIONS,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_DYNAMIC_CONTROL_AWB_STATE),
        .struct_name = STR(guzzi_camera3_dynamic_control_awb_state_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_dynamic_valid_t, control_awb_state),
        .struct_offset = offset_of(guzzi_camera3_metadata_dynamic_struct_t, control_awb_state),
        .v_offset = offset_of(guzzi_camera3_metadata_dynamic_t, valid.control_awb_state),
        .s_offset = offset_of(guzzi_camera3_metadata_dynamic_t, s.control_awb_state),
        .size = sizeof (guzzi_camera3_dynamic_control_awb_state_t),
        .ident_size = sizeof (guzzi_camera3_dynamic_control_awb_state_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_CONTROL_AWB_STATE,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_DYNAMIC_CONTROL_MODE),
        .struct_name = STR(guzzi_camera3_dynamic_control_mode_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_dynamic_valid_t, control_mode),
        .struct_offset = offset_of(guzzi_camera3_metadata_dynamic_struct_t, control_mode),
        .v_offset = offset_of(guzzi_camera3_metadata_dynamic_t, valid.control_mode),
        .s_offset = offset_of(guzzi_camera3_metadata_dynamic_t, s.control_mode),
        .size = sizeof (guzzi_camera3_dynamic_control_mode_t),
        .ident_size = sizeof (guzzi_camera3_dynamic_control_mode_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_CONTROL_MODE,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_DYNAMIC_EDGE_MODE),
        .struct_name = STR(guzzi_camera3_dynamic_edge_mode_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_dynamic_valid_t, edge_mode),
        .struct_offset = offset_of(guzzi_camera3_metadata_dynamic_struct_t, edge_mode),
        .v_offset = offset_of(guzzi_camera3_metadata_dynamic_t, valid.edge_mode),
        .s_offset = offset_of(guzzi_camera3_metadata_dynamic_t, s.edge_mode),
        .size = sizeof (guzzi_camera3_dynamic_edge_mode_t),
        .ident_size = sizeof (guzzi_camera3_dynamic_edge_mode_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_EDGE_MODE,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_DYNAMIC_FLASH_FIRING_POWER),
        .struct_name = STR(guzzi_camera3_dynamic_flash_firing_power_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_dynamic_valid_t, flash_firing_power),
        .struct_offset = offset_of(guzzi_camera3_metadata_dynamic_struct_t, flash_firing_power),
        .v_offset = offset_of(guzzi_camera3_metadata_dynamic_t, valid.flash_firing_power),
        .s_offset = offset_of(guzzi_camera3_metadata_dynamic_t, s.flash_firing_power),
        .size = sizeof (guzzi_camera3_dynamic_flash_firing_power_t),
        .ident_size = sizeof (guzzi_camera3_dynamic_flash_firing_power_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_FLASH_FIRING_POWER,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_DYNAMIC_FLASH_FIRING_TIME),
        .struct_name = STR(guzzi_camera3_dynamic_flash_firing_time_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_dynamic_valid_t, flash_firing_time),
        .struct_offset = offset_of(guzzi_camera3_metadata_dynamic_struct_t, flash_firing_time),
        .v_offset = offset_of(guzzi_camera3_metadata_dynamic_t, valid.flash_firing_time),
        .s_offset = offset_of(guzzi_camera3_metadata_dynamic_t, s.flash_firing_time),
        .size = sizeof (guzzi_camera3_dynamic_flash_firing_time_t),
        .ident_size = sizeof (guzzi_camera3_dynamic_flash_firing_time_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_FLASH_FIRING_TIME,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_DYNAMIC_FLASH_MODE),
        .struct_name = STR(guzzi_camera3_dynamic_flash_mode_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_dynamic_valid_t, flash_mode),
        .struct_offset = offset_of(guzzi_camera3_metadata_dynamic_struct_t, flash_mode),
        .v_offset = offset_of(guzzi_camera3_metadata_dynamic_t, valid.flash_mode),
        .s_offset = offset_of(guzzi_camera3_metadata_dynamic_t, s.flash_mode),
        .size = sizeof (guzzi_camera3_dynamic_flash_mode_t),
        .ident_size = sizeof (guzzi_camera3_dynamic_flash_mode_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_FLASH_MODE,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_DYNAMIC_FLASH_STATE),
        .struct_name = STR(guzzi_camera3_dynamic_flash_state_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_dynamic_valid_t, flash_state),
        .struct_offset = offset_of(guzzi_camera3_metadata_dynamic_struct_t, flash_state),
        .v_offset = offset_of(guzzi_camera3_metadata_dynamic_t, valid.flash_state),
        .s_offset = offset_of(guzzi_camera3_metadata_dynamic_t, s.flash_state),
        .size = sizeof (guzzi_camera3_dynamic_flash_state_t),
        .ident_size = sizeof (guzzi_camera3_dynamic_flash_state_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_FLASH_STATE,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_DYNAMIC_HOT_PIXEL_MODE),
        .struct_name = STR(guzzi_camera3_dynamic_hot_pixel_mode_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_dynamic_valid_t, hot_pixel_mode),
        .struct_offset = offset_of(guzzi_camera3_metadata_dynamic_struct_t, hot_pixel_mode),
        .v_offset = offset_of(guzzi_camera3_metadata_dynamic_t, valid.hot_pixel_mode),
        .s_offset = offset_of(guzzi_camera3_metadata_dynamic_t, s.hot_pixel_mode),
        .size = sizeof (guzzi_camera3_dynamic_hot_pixel_mode_t),
        .ident_size = sizeof (guzzi_camera3_dynamic_hot_pixel_mode_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_HOT_PIXEL_MODE,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_DYNAMIC_JPEG_GPS_COORDINATES),
        .struct_name = STR(guzzi_camera3_dynamic_jpeg_gps_coordinates_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_dynamic_valid_t, jpeg_gps_coordinates),
        .struct_offset = offset_of(guzzi_camera3_metadata_dynamic_struct_t, jpeg_gps_coordinates),
        .v_offset = offset_of(guzzi_camera3_metadata_dynamic_t, valid.jpeg_gps_coordinates),
        .s_offset = offset_of(guzzi_camera3_metadata_dynamic_t, s.jpeg_gps_coordinates),
        .size = sizeof (guzzi_camera3_dynamic_jpeg_gps_coordinates_t),
        .ident_size = sizeof (guzzi_camera3_dynamic_jpeg_gps_coordinates_identity_t),
        .dim_size_1 = GUZZI_CAMERA3_DYNAMIC_JPEG_GPS_COORDINATES_DIM_MAX_SIZE_1,
        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_JPEG_GPS_COORDINATES,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_DYNAMIC_JPEG_GPS_PROCESSING_METHOD),
        .struct_name = STR(guzzi_camera3_dynamic_jpeg_gps_processing_method_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_dynamic_valid_t, jpeg_gps_processing_method),
        .struct_offset = offset_of(guzzi_camera3_metadata_dynamic_struct_t, jpeg_gps_processing_method),
        .v_offset = offset_of(guzzi_camera3_metadata_dynamic_t, valid.jpeg_gps_processing_method),
        .s_offset = offset_of(guzzi_camera3_metadata_dynamic_t, s.jpeg_gps_processing_method),
        .size = sizeof (guzzi_camera3_dynamic_jpeg_gps_processing_method_t),
        .ident_size = sizeof (guzzi_camera3_dynamic_jpeg_gps_processing_method_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_JPEG_GPS_PROCESSING_METHOD,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_DYNAMIC_JPEG_GPS_TIMESTAMP),
        .struct_name = STR(guzzi_camera3_dynamic_jpeg_gps_timestamp_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_dynamic_valid_t, jpeg_gps_timestamp),
        .struct_offset = offset_of(guzzi_camera3_metadata_dynamic_struct_t, jpeg_gps_timestamp),
        .v_offset = offset_of(guzzi_camera3_metadata_dynamic_t, valid.jpeg_gps_timestamp),
        .s_offset = offset_of(guzzi_camera3_metadata_dynamic_t, s.jpeg_gps_timestamp),
        .size = sizeof (guzzi_camera3_dynamic_jpeg_gps_timestamp_t),
        .ident_size = sizeof (guzzi_camera3_dynamic_jpeg_gps_timestamp_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_JPEG_GPS_TIMESTAMP,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_DYNAMIC_JPEG_ORIENTATION),
        .struct_name = STR(guzzi_camera3_dynamic_jpeg_orientation_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_dynamic_valid_t, jpeg_orientation),
        .struct_offset = offset_of(guzzi_camera3_metadata_dynamic_struct_t, jpeg_orientation),
        .v_offset = offset_of(guzzi_camera3_metadata_dynamic_t, valid.jpeg_orientation),
        .s_offset = offset_of(guzzi_camera3_metadata_dynamic_t, s.jpeg_orientation),
        .size = sizeof (guzzi_camera3_dynamic_jpeg_orientation_t),
        .ident_size = sizeof (guzzi_camera3_dynamic_jpeg_orientation_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_JPEG_ORIENTATION,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_DYNAMIC_JPEG_QUALITY),
        .struct_name = STR(guzzi_camera3_dynamic_jpeg_quality_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_dynamic_valid_t, jpeg_quality),
        .struct_offset = offset_of(guzzi_camera3_metadata_dynamic_struct_t, jpeg_quality),
        .v_offset = offset_of(guzzi_camera3_metadata_dynamic_t, valid.jpeg_quality),
        .s_offset = offset_of(guzzi_camera3_metadata_dynamic_t, s.jpeg_quality),
        .size = sizeof (guzzi_camera3_dynamic_jpeg_quality_t),
        .ident_size = sizeof (guzzi_camera3_dynamic_jpeg_quality_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_JPEG_QUALITY,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_DYNAMIC_JPEG_SIZE),
        .struct_name = STR(guzzi_camera3_dynamic_jpeg_size_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_dynamic_valid_t, jpeg_size),
        .struct_offset = offset_of(guzzi_camera3_metadata_dynamic_struct_t, jpeg_size),
        .v_offset = offset_of(guzzi_camera3_metadata_dynamic_t, valid.jpeg_size),
        .s_offset = offset_of(guzzi_camera3_metadata_dynamic_t, s.jpeg_size),
        .size = sizeof (guzzi_camera3_dynamic_jpeg_size_t),
        .ident_size = sizeof (guzzi_camera3_dynamic_jpeg_size_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_JPEG_SIZE,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_DYNAMIC_JPEG_THUMBNAIL_QUALITY),
        .struct_name = STR(guzzi_camera3_dynamic_jpeg_thumbnail_quality_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_dynamic_valid_t, jpeg_thumbnail_quality),
        .struct_offset = offset_of(guzzi_camera3_metadata_dynamic_struct_t, jpeg_thumbnail_quality),
        .v_offset = offset_of(guzzi_camera3_metadata_dynamic_t, valid.jpeg_thumbnail_quality),
        .s_offset = offset_of(guzzi_camera3_metadata_dynamic_t, s.jpeg_thumbnail_quality),
        .size = sizeof (guzzi_camera3_dynamic_jpeg_thumbnail_quality_t),
        .ident_size = sizeof (guzzi_camera3_dynamic_jpeg_thumbnail_quality_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_JPEG_THUMBNAIL_QUALITY,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_DYNAMIC_JPEG_THUMBNAIL_SIZE),
        .struct_name = STR(guzzi_camera3_dynamic_jpeg_thumbnail_size_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_dynamic_valid_t, jpeg_thumbnail_size),
        .struct_offset = offset_of(guzzi_camera3_metadata_dynamic_struct_t, jpeg_thumbnail_size),
        .v_offset = offset_of(guzzi_camera3_metadata_dynamic_t, valid.jpeg_thumbnail_size),
        .s_offset = offset_of(guzzi_camera3_metadata_dynamic_t, s.jpeg_thumbnail_size),
        .size = sizeof (guzzi_camera3_dynamic_jpeg_thumbnail_size_t),
        .ident_size = sizeof (guzzi_camera3_dynamic_jpeg_thumbnail_size_identity_t),
        .dim_size_1 = GUZZI_CAMERA3_DYNAMIC_JPEG_THUMBNAIL_SIZE_DIM_MAX_SIZE_1,
        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_JPEG_THUMBNAIL_SIZE,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_DYNAMIC_LED_TRANSMIT),
        .struct_name = STR(guzzi_camera3_dynamic_led_transmit_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_dynamic_valid_t, led_transmit),
        .struct_offset = offset_of(guzzi_camera3_metadata_dynamic_struct_t, led_transmit),
        .v_offset = offset_of(guzzi_camera3_metadata_dynamic_t, valid.led_transmit),
        .s_offset = offset_of(guzzi_camera3_metadata_dynamic_t, s.led_transmit),
        .size = sizeof (guzzi_camera3_dynamic_led_transmit_t),
        .ident_size = sizeof (guzzi_camera3_dynamic_led_transmit_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_LED_TRANSMIT,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_DYNAMIC_LENS_APERTURE),
        .struct_name = STR(guzzi_camera3_dynamic_lens_aperture_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_dynamic_valid_t, lens_aperture),
        .struct_offset = offset_of(guzzi_camera3_metadata_dynamic_struct_t, lens_aperture),
        .v_offset = offset_of(guzzi_camera3_metadata_dynamic_t, valid.lens_aperture),
        .s_offset = offset_of(guzzi_camera3_metadata_dynamic_t, s.lens_aperture),
        .size = sizeof (guzzi_camera3_dynamic_lens_aperture_t),
        .ident_size = sizeof (guzzi_camera3_dynamic_lens_aperture_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_LENS_APERTURE,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_DYNAMIC_LENS_FILTER_DENSITY),
        .struct_name = STR(guzzi_camera3_dynamic_lens_filter_density_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_dynamic_valid_t, lens_filter_density),
        .struct_offset = offset_of(guzzi_camera3_metadata_dynamic_struct_t, lens_filter_density),
        .v_offset = offset_of(guzzi_camera3_metadata_dynamic_t, valid.lens_filter_density),
        .s_offset = offset_of(guzzi_camera3_metadata_dynamic_t, s.lens_filter_density),
        .size = sizeof (guzzi_camera3_dynamic_lens_filter_density_t),
        .ident_size = sizeof (guzzi_camera3_dynamic_lens_filter_density_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_LENS_FILTER_DENSITY,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_DYNAMIC_LENS_FOCAL_LENGTH),
        .struct_name = STR(guzzi_camera3_dynamic_lens_focal_length_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_dynamic_valid_t, lens_focal_length),
        .struct_offset = offset_of(guzzi_camera3_metadata_dynamic_struct_t, lens_focal_length),
        .v_offset = offset_of(guzzi_camera3_metadata_dynamic_t, valid.lens_focal_length),
        .s_offset = offset_of(guzzi_camera3_metadata_dynamic_t, s.lens_focal_length),
        .size = sizeof (guzzi_camera3_dynamic_lens_focal_length_t),
        .ident_size = sizeof (guzzi_camera3_dynamic_lens_focal_length_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_LENS_FOCAL_LENGTH,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_DYNAMIC_LENS_FOCUS_DISTANCE),
        .struct_name = STR(guzzi_camera3_dynamic_lens_focus_distance_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_dynamic_valid_t, lens_focus_distance),
        .struct_offset = offset_of(guzzi_camera3_metadata_dynamic_struct_t, lens_focus_distance),
        .v_offset = offset_of(guzzi_camera3_metadata_dynamic_t, valid.lens_focus_distance),
        .s_offset = offset_of(guzzi_camera3_metadata_dynamic_t, s.lens_focus_distance),
        .size = sizeof (guzzi_camera3_dynamic_lens_focus_distance_t),
        .ident_size = sizeof (guzzi_camera3_dynamic_lens_focus_distance_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_LENS_FOCUS_DISTANCE,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_DYNAMIC_LENS_FOCUS_RANGE),
        .struct_name = STR(guzzi_camera3_dynamic_lens_focus_range_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_dynamic_valid_t, lens_focus_range),
        .struct_offset = offset_of(guzzi_camera3_metadata_dynamic_struct_t, lens_focus_range),
        .v_offset = offset_of(guzzi_camera3_metadata_dynamic_t, valid.lens_focus_range),
        .s_offset = offset_of(guzzi_camera3_metadata_dynamic_t, s.lens_focus_range),
        .size = sizeof (guzzi_camera3_dynamic_lens_focus_range_t),
        .ident_size = sizeof (guzzi_camera3_dynamic_lens_focus_range_identity_t),
        .dim_size_1 = GUZZI_CAMERA3_DYNAMIC_LENS_FOCUS_RANGE_DIM_MAX_SIZE_1,
        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_LENS_FOCUS_RANGE,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_DYNAMIC_LENS_OPTICAL_STABILIZATION_MODE),
        .struct_name = STR(guzzi_camera3_dynamic_lens_optical_stabilization_mode_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_dynamic_valid_t, lens_optical_stabilization_mode),
        .struct_offset = offset_of(guzzi_camera3_metadata_dynamic_struct_t, lens_optical_stabilization_mode),
        .v_offset = offset_of(guzzi_camera3_metadata_dynamic_t, valid.lens_optical_stabilization_mode),
        .s_offset = offset_of(guzzi_camera3_metadata_dynamic_t, s.lens_optical_stabilization_mode),
        .size = sizeof (guzzi_camera3_dynamic_lens_optical_stabilization_mode_t),
        .ident_size = sizeof (guzzi_camera3_dynamic_lens_optical_stabilization_mode_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_LENS_OPTICAL_STABILIZATION_MODE,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_DYNAMIC_LENS_STATE),
        .struct_name = STR(guzzi_camera3_dynamic_lens_state_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_dynamic_valid_t, lens_state),
        .struct_offset = offset_of(guzzi_camera3_metadata_dynamic_struct_t, lens_state),
        .v_offset = offset_of(guzzi_camera3_metadata_dynamic_t, valid.lens_state),
        .s_offset = offset_of(guzzi_camera3_metadata_dynamic_t, s.lens_state),
        .size = sizeof (guzzi_camera3_dynamic_lens_state_t),
        .ident_size = sizeof (guzzi_camera3_dynamic_lens_state_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_LENS_STATE,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_DYNAMIC_NOISE_REDUCTION_MODE),
        .struct_name = STR(guzzi_camera3_dynamic_noise_reduction_mode_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_dynamic_valid_t, noise_reduction_mode),
        .struct_offset = offset_of(guzzi_camera3_metadata_dynamic_struct_t, noise_reduction_mode),
        .v_offset = offset_of(guzzi_camera3_metadata_dynamic_t, valid.noise_reduction_mode),
        .s_offset = offset_of(guzzi_camera3_metadata_dynamic_t, s.noise_reduction_mode),
        .size = sizeof (guzzi_camera3_dynamic_noise_reduction_mode_t),
        .ident_size = sizeof (guzzi_camera3_dynamic_noise_reduction_mode_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_NOISE_REDUCTION_MODE,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_DYNAMIC_QUIRKS_PARTIAL_RESULT),
        .struct_name = STR(guzzi_camera3_dynamic_quirks_partial_result_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_dynamic_valid_t, quirks_partial_result),
        .struct_offset = offset_of(guzzi_camera3_metadata_dynamic_struct_t, quirks_partial_result),
        .v_offset = offset_of(guzzi_camera3_metadata_dynamic_t, valid.quirks_partial_result),
        .s_offset = offset_of(guzzi_camera3_metadata_dynamic_t, s.quirks_partial_result),
        .size = sizeof (guzzi_camera3_dynamic_quirks_partial_result_t),
        .ident_size = sizeof (guzzi_camera3_dynamic_quirks_partial_result_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_QUIRKS_PARTIAL_RESULT,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_DYNAMIC_REQUEST_FRAME_COUNT),
        .struct_name = STR(guzzi_camera3_dynamic_request_frame_count_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_dynamic_valid_t, request_frame_count),
        .struct_offset = offset_of(guzzi_camera3_metadata_dynamic_struct_t, request_frame_count),
        .v_offset = offset_of(guzzi_camera3_metadata_dynamic_t, valid.request_frame_count),
        .s_offset = offset_of(guzzi_camera3_metadata_dynamic_t, s.request_frame_count),
        .size = sizeof (guzzi_camera3_dynamic_request_frame_count_t),
        .ident_size = sizeof (guzzi_camera3_dynamic_request_frame_count_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_REQUEST_FRAME_COUNT,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_DYNAMIC_REQUEST_ID),
        .struct_name = STR(guzzi_camera3_dynamic_request_id_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_dynamic_valid_t, request_id),
        .struct_offset = offset_of(guzzi_camera3_metadata_dynamic_struct_t, request_id),
        .v_offset = offset_of(guzzi_camera3_metadata_dynamic_t, valid.request_id),
        .s_offset = offset_of(guzzi_camera3_metadata_dynamic_t, s.request_id),
        .size = sizeof (guzzi_camera3_dynamic_request_id_t),
        .ident_size = sizeof (guzzi_camera3_dynamic_request_id_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_REQUEST_ID,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_DYNAMIC_REQUEST_METADATA_MODE),
        .struct_name = STR(guzzi_camera3_dynamic_request_metadata_mode_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_dynamic_valid_t, request_metadata_mode),
        .struct_offset = offset_of(guzzi_camera3_metadata_dynamic_struct_t, request_metadata_mode),
        .v_offset = offset_of(guzzi_camera3_metadata_dynamic_t, valid.request_metadata_mode),
        .s_offset = offset_of(guzzi_camera3_metadata_dynamic_t, s.request_metadata_mode),
        .size = sizeof (guzzi_camera3_dynamic_request_metadata_mode_t),
        .ident_size = sizeof (guzzi_camera3_dynamic_request_metadata_mode_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_REQUEST_METADATA_MODE,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_DYNAMIC_REQUEST_OUTPUT_STREAMS),
        .struct_name = STR(guzzi_camera3_dynamic_request_output_streams_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_dynamic_valid_t, request_output_streams),
        .struct_offset = offset_of(guzzi_camera3_metadata_dynamic_struct_t, request_output_streams),
        .v_offset = offset_of(guzzi_camera3_metadata_dynamic_t, valid.request_output_streams),
        .s_offset = offset_of(guzzi_camera3_metadata_dynamic_t, s.request_output_streams),
        .size = sizeof (guzzi_camera3_dynamic_request_output_streams_t),
        .ident_size = sizeof (guzzi_camera3_dynamic_request_output_streams_identity_t),
        .dim_size_1 = GUZZI_CAMERA3_DYNAMIC_REQUEST_OUTPUT_STREAMS_DIM_MAX_SIZE_1,
        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_REQUEST_OUTPUT_STREAMS,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_DYNAMIC_SCALER_CROP_REGION),
        .struct_name = STR(guzzi_camera3_dynamic_scaler_crop_region_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_dynamic_valid_t, scaler_crop_region),
        .struct_offset = offset_of(guzzi_camera3_metadata_dynamic_struct_t, scaler_crop_region),
        .v_offset = offset_of(guzzi_camera3_metadata_dynamic_t, valid.scaler_crop_region),
        .s_offset = offset_of(guzzi_camera3_metadata_dynamic_t, s.scaler_crop_region),
        .size = sizeof (guzzi_camera3_dynamic_scaler_crop_region_t),
        .ident_size = sizeof (guzzi_camera3_dynamic_scaler_crop_region_identity_t),
        .dim_size_1 = GUZZI_CAMERA3_DYNAMIC_SCALER_CROP_REGION_DIM_MAX_SIZE_1,
        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_SCALER_CROP_REGION,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_DYNAMIC_SENSOR_EXPOSURE_TIME),
        .struct_name = STR(guzzi_camera3_dynamic_sensor_exposure_time_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_dynamic_valid_t, sensor_exposure_time),
        .struct_offset = offset_of(guzzi_camera3_metadata_dynamic_struct_t, sensor_exposure_time),
        .v_offset = offset_of(guzzi_camera3_metadata_dynamic_t, valid.sensor_exposure_time),
        .s_offset = offset_of(guzzi_camera3_metadata_dynamic_t, s.sensor_exposure_time),
        .size = sizeof (guzzi_camera3_dynamic_sensor_exposure_time_t),
        .ident_size = sizeof (guzzi_camera3_dynamic_sensor_exposure_time_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_SENSOR_EXPOSURE_TIME,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_DYNAMIC_SENSOR_FRAME_DURATION),
        .struct_name = STR(guzzi_camera3_dynamic_sensor_frame_duration_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_dynamic_valid_t, sensor_frame_duration),
        .struct_offset = offset_of(guzzi_camera3_metadata_dynamic_struct_t, sensor_frame_duration),
        .v_offset = offset_of(guzzi_camera3_metadata_dynamic_t, valid.sensor_frame_duration),
        .s_offset = offset_of(guzzi_camera3_metadata_dynamic_t, s.sensor_frame_duration),
        .size = sizeof (guzzi_camera3_dynamic_sensor_frame_duration_t),
        .ident_size = sizeof (guzzi_camera3_dynamic_sensor_frame_duration_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_SENSOR_FRAME_DURATION,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_DYNAMIC_SENSOR_SENSITIVITY),
        .struct_name = STR(guzzi_camera3_dynamic_sensor_sensitivity_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_dynamic_valid_t, sensor_sensitivity),
        .struct_offset = offset_of(guzzi_camera3_metadata_dynamic_struct_t, sensor_sensitivity),
        .v_offset = offset_of(guzzi_camera3_metadata_dynamic_t, valid.sensor_sensitivity),
        .s_offset = offset_of(guzzi_camera3_metadata_dynamic_t, s.sensor_sensitivity),
        .size = sizeof (guzzi_camera3_dynamic_sensor_sensitivity_t),
        .ident_size = sizeof (guzzi_camera3_dynamic_sensor_sensitivity_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_SENSOR_SENSITIVITY,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_DYNAMIC_SENSOR_TEMPERATURE),
        .struct_name = STR(guzzi_camera3_dynamic_sensor_temperature_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_dynamic_valid_t, sensor_temperature),
        .struct_offset = offset_of(guzzi_camera3_metadata_dynamic_struct_t, sensor_temperature),
        .v_offset = offset_of(guzzi_camera3_metadata_dynamic_t, valid.sensor_temperature),
        .s_offset = offset_of(guzzi_camera3_metadata_dynamic_t, s.sensor_temperature),
        .size = sizeof (guzzi_camera3_dynamic_sensor_temperature_t),
        .ident_size = sizeof (guzzi_camera3_dynamic_sensor_temperature_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_SENSOR_TEMPERATURE,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_DYNAMIC_SENSOR_TIMESTAMP),
        .struct_name = STR(guzzi_camera3_dynamic_sensor_timestamp_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_dynamic_valid_t, sensor_timestamp),
        .struct_offset = offset_of(guzzi_camera3_metadata_dynamic_struct_t, sensor_timestamp),
        .v_offset = offset_of(guzzi_camera3_metadata_dynamic_t, valid.sensor_timestamp),
        .s_offset = offset_of(guzzi_camera3_metadata_dynamic_t, s.sensor_timestamp),
        .size = sizeof (guzzi_camera3_dynamic_sensor_timestamp_t),
        .ident_size = sizeof (guzzi_camera3_dynamic_sensor_timestamp_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_SENSOR_TIMESTAMP,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_DYNAMIC_SHADING_MODE),
        .struct_name = STR(guzzi_camera3_dynamic_shading_mode_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_dynamic_valid_t, shading_mode),
        .struct_offset = offset_of(guzzi_camera3_metadata_dynamic_struct_t, shading_mode),
        .v_offset = offset_of(guzzi_camera3_metadata_dynamic_t, valid.shading_mode),
        .s_offset = offset_of(guzzi_camera3_metadata_dynamic_t, s.shading_mode),
        .size = sizeof (guzzi_camera3_dynamic_shading_mode_t),
        .ident_size = sizeof (guzzi_camera3_dynamic_shading_mode_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_SHADING_MODE,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_DYNAMIC_STATISTICS_FACE_DETECT_MODE),
        .struct_name = STR(guzzi_camera3_dynamic_statistics_face_detect_mode_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_dynamic_valid_t, statistics_face_detect_mode),
        .struct_offset = offset_of(guzzi_camera3_metadata_dynamic_struct_t, statistics_face_detect_mode),
        .v_offset = offset_of(guzzi_camera3_metadata_dynamic_t, valid.statistics_face_detect_mode),
        .s_offset = offset_of(guzzi_camera3_metadata_dynamic_t, s.statistics_face_detect_mode),
        .size = sizeof (guzzi_camera3_dynamic_statistics_face_detect_mode_t),
        .ident_size = sizeof (guzzi_camera3_dynamic_statistics_face_detect_mode_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_STATISTICS_FACE_DETECT_MODE,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_DYNAMIC_STATISTICS_FACE_IDS),
        .struct_name = STR(guzzi_camera3_dynamic_statistics_face_ids_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_dynamic_valid_t, statistics_face_ids),
        .struct_offset = offset_of(guzzi_camera3_metadata_dynamic_struct_t, statistics_face_ids),
        .v_offset = offset_of(guzzi_camera3_metadata_dynamic_t, valid.statistics_face_ids),
        .s_offset = offset_of(guzzi_camera3_metadata_dynamic_t, s.statistics_face_ids),
        .size = sizeof (guzzi_camera3_dynamic_statistics_face_ids_t),
        .ident_size = sizeof (guzzi_camera3_dynamic_statistics_face_ids_identity_t),
        .dim_size_1 = GUZZI_CAMERA3_DYNAMIC_STATISTICS_FACE_IDS_DIM_MAX_SIZE_1,
        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_STATISTICS_FACE_IDS,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_DYNAMIC_STATISTICS_FACE_LANDMARKS),
        .struct_name = STR(guzzi_camera3_dynamic_statistics_face_landmarks_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_dynamic_valid_t, statistics_face_landmarks),
        .struct_offset = offset_of(guzzi_camera3_metadata_dynamic_struct_t, statistics_face_landmarks),
        .v_offset = offset_of(guzzi_camera3_metadata_dynamic_t, valid.statistics_face_landmarks),
        .s_offset = offset_of(guzzi_camera3_metadata_dynamic_t, s.statistics_face_landmarks),
        .size = sizeof (guzzi_camera3_dynamic_statistics_face_landmarks_t),
        .ident_size = sizeof (guzzi_camera3_dynamic_statistics_face_landmarks_identity_t),
        .dim_size_1 = GUZZI_CAMERA3_DYNAMIC_STATISTICS_FACE_LANDMARKS_DIM_MAX_SIZE_1,
        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_STATISTICS_FACE_LANDMARKS,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_DYNAMIC_STATISTICS_FACE_RECTANGLES),
        .struct_name = STR(guzzi_camera3_dynamic_statistics_face_rectangles_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_dynamic_valid_t, statistics_face_rectangles),
        .struct_offset = offset_of(guzzi_camera3_metadata_dynamic_struct_t, statistics_face_rectangles),
        .v_offset = offset_of(guzzi_camera3_metadata_dynamic_t, valid.statistics_face_rectangles),
        .s_offset = offset_of(guzzi_camera3_metadata_dynamic_t, s.statistics_face_rectangles),
        .size = sizeof (guzzi_camera3_dynamic_statistics_face_rectangles_t),
        .ident_size = sizeof (guzzi_camera3_dynamic_statistics_face_rectangles_identity_t),
        .dim_size_1 = GUZZI_CAMERA3_DYNAMIC_STATISTICS_FACE_RECTANGLES_DIM_MAX_SIZE_1,
        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_STATISTICS_FACE_RECTANGLES,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_DYNAMIC_STATISTICS_FACE_SCORES),
        .struct_name = STR(guzzi_camera3_dynamic_statistics_face_scores_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_dynamic_valid_t, statistics_face_scores),
        .struct_offset = offset_of(guzzi_camera3_metadata_dynamic_struct_t, statistics_face_scores),
        .v_offset = offset_of(guzzi_camera3_metadata_dynamic_t, valid.statistics_face_scores),
        .s_offset = offset_of(guzzi_camera3_metadata_dynamic_t, s.statistics_face_scores),
        .size = sizeof (guzzi_camera3_dynamic_statistics_face_scores_t),
        .ident_size = sizeof (guzzi_camera3_dynamic_statistics_face_scores_identity_t),
        .dim_size_1 = GUZZI_CAMERA3_DYNAMIC_STATISTICS_FACE_SCORES_DIM_MAX_SIZE_1,
        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_STATISTICS_FACE_SCORES,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_DYNAMIC_STATISTICS_HISTOGRAM),
        .struct_name = STR(guzzi_camera3_dynamic_statistics_histogram_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_dynamic_valid_t, statistics_histogram),
        .struct_offset = offset_of(guzzi_camera3_metadata_dynamic_struct_t, statistics_histogram),
        .v_offset = offset_of(guzzi_camera3_metadata_dynamic_t, valid.statistics_histogram),
        .s_offset = offset_of(guzzi_camera3_metadata_dynamic_t, s.statistics_histogram),
        .size = sizeof (guzzi_camera3_dynamic_statistics_histogram_t),
        .ident_size = sizeof (guzzi_camera3_dynamic_statistics_histogram_identity_t),
        .dim_size_1 = GUZZI_CAMERA3_DYNAMIC_STATISTICS_HISTOGRAM_DIM_MAX_SIZE_1,
        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_STATISTICS_HISTOGRAM,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_DYNAMIC_STATISTICS_HISTOGRAM_MODE),
        .struct_name = STR(guzzi_camera3_dynamic_statistics_histogram_mode_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_dynamic_valid_t, statistics_histogram_mode),
        .struct_offset = offset_of(guzzi_camera3_metadata_dynamic_struct_t, statistics_histogram_mode),
        .v_offset = offset_of(guzzi_camera3_metadata_dynamic_t, valid.statistics_histogram_mode),
        .s_offset = offset_of(guzzi_camera3_metadata_dynamic_t, s.statistics_histogram_mode),
        .size = sizeof (guzzi_camera3_dynamic_statistics_histogram_mode_t),
        .ident_size = sizeof (guzzi_camera3_dynamic_statistics_histogram_mode_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_STATISTICS_HISTOGRAM_MODE,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_DYNAMIC_STATISTICS_LENS_SHADING_MAP),
        .struct_name = STR(guzzi_camera3_dynamic_statistics_lens_shading_map_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_dynamic_valid_t, statistics_lens_shading_map),
        .struct_offset = offset_of(guzzi_camera3_metadata_dynamic_struct_t, statistics_lens_shading_map),
        .v_offset = offset_of(guzzi_camera3_metadata_dynamic_t, valid.statistics_lens_shading_map),
        .s_offset = offset_of(guzzi_camera3_metadata_dynamic_t, s.statistics_lens_shading_map),
        .size = sizeof (guzzi_camera3_dynamic_statistics_lens_shading_map_t),
        .ident_size = sizeof (guzzi_camera3_dynamic_statistics_lens_shading_map_identity_t),
        .dim_size_1 = GUZZI_CAMERA3_DYNAMIC_STATISTICS_LENS_SHADING_MAP_DIM_MAX_SIZE_1,
        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_STATISTICS_LENS_SHADING_MAP,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_DYNAMIC_STATISTICS_PREDICTED_COLOR_GAINS),
        .struct_name = STR(guzzi_camera3_dynamic_statistics_predicted_color_gains_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_dynamic_valid_t, statistics_predicted_color_gains),
        .struct_offset = offset_of(guzzi_camera3_metadata_dynamic_struct_t, statistics_predicted_color_gains),
        .v_offset = offset_of(guzzi_camera3_metadata_dynamic_t, valid.statistics_predicted_color_gains),
        .s_offset = offset_of(guzzi_camera3_metadata_dynamic_t, s.statistics_predicted_color_gains),
        .size = sizeof (guzzi_camera3_dynamic_statistics_predicted_color_gains_t),
        .ident_size = sizeof (guzzi_camera3_dynamic_statistics_predicted_color_gains_identity_t),
        .dim_size_1 = GUZZI_CAMERA3_DYNAMIC_STATISTICS_PREDICTED_COLOR_GAINS_DIM_MAX_SIZE_1,
        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_STATISTICS_PREDICTED_COLOR_GAINS,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_DYNAMIC_STATISTICS_PREDICTED_COLOR_TRANSFORM),
        .struct_name = STR(guzzi_camera3_dynamic_statistics_predicted_color_transform_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_dynamic_valid_t, statistics_predicted_color_transform),
        .struct_offset = offset_of(guzzi_camera3_metadata_dynamic_struct_t, statistics_predicted_color_transform),
        .v_offset = offset_of(guzzi_camera3_metadata_dynamic_t, valid.statistics_predicted_color_transform),
        .s_offset = offset_of(guzzi_camera3_metadata_dynamic_t, s.statistics_predicted_color_transform),
        .size = sizeof (guzzi_camera3_dynamic_statistics_predicted_color_transform_t),
        .ident_size = sizeof (guzzi_camera3_dynamic_statistics_predicted_color_transform_identity_t),
        .dim_size_1 = GUZZI_CAMERA3_DYNAMIC_STATISTICS_PREDICTED_COLOR_TRANSFORM_DIM_MAX_SIZE_1,
        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_STATISTICS_PREDICTED_COLOR_TRANSFORM,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_DYNAMIC_STATISTICS_SCENE_FLICKER),
        .struct_name = STR(guzzi_camera3_dynamic_statistics_scene_flicker_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_dynamic_valid_t, statistics_scene_flicker),
        .struct_offset = offset_of(guzzi_camera3_metadata_dynamic_struct_t, statistics_scene_flicker),
        .v_offset = offset_of(guzzi_camera3_metadata_dynamic_t, valid.statistics_scene_flicker),
        .s_offset = offset_of(guzzi_camera3_metadata_dynamic_t, s.statistics_scene_flicker),
        .size = sizeof (guzzi_camera3_dynamic_statistics_scene_flicker_t),
        .ident_size = sizeof (guzzi_camera3_dynamic_statistics_scene_flicker_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_STATISTICS_SCENE_FLICKER,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_DYNAMIC_STATISTICS_SHARPNESS_MAP),
        .struct_name = STR(guzzi_camera3_dynamic_statistics_sharpness_map_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_dynamic_valid_t, statistics_sharpness_map),
        .struct_offset = offset_of(guzzi_camera3_metadata_dynamic_struct_t, statistics_sharpness_map),
        .v_offset = offset_of(guzzi_camera3_metadata_dynamic_t, valid.statistics_sharpness_map),
        .s_offset = offset_of(guzzi_camera3_metadata_dynamic_t, s.statistics_sharpness_map),
        .size = sizeof (guzzi_camera3_dynamic_statistics_sharpness_map_t),
        .ident_size = sizeof (guzzi_camera3_dynamic_statistics_sharpness_map_identity_t),
        .dim_size_1 = GUZZI_CAMERA3_DYNAMIC_STATISTICS_SHARPNESS_MAP_DIM_MAX_SIZE_1,
        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_STATISTICS_SHARPNESS_MAP,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_DYNAMIC_STATISTICS_SHARPNESS_MAP_MODE),
        .struct_name = STR(guzzi_camera3_dynamic_statistics_sharpness_map_mode_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_dynamic_valid_t, statistics_sharpness_map_mode),
        .struct_offset = offset_of(guzzi_camera3_metadata_dynamic_struct_t, statistics_sharpness_map_mode),
        .v_offset = offset_of(guzzi_camera3_metadata_dynamic_t, valid.statistics_sharpness_map_mode),
        .s_offset = offset_of(guzzi_camera3_metadata_dynamic_t, s.statistics_sharpness_map_mode),
        .size = sizeof (guzzi_camera3_dynamic_statistics_sharpness_map_mode_t),
        .ident_size = sizeof (guzzi_camera3_dynamic_statistics_sharpness_map_mode_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_STATISTICS_SHARPNESS_MAP_MODE,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_DYNAMIC_TONEMAP_CURVE_BLUE),
        .struct_name = STR(guzzi_camera3_dynamic_tonemap_curve_blue_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_dynamic_valid_t, tonemap_curve_blue),
        .struct_offset = offset_of(guzzi_camera3_metadata_dynamic_struct_t, tonemap_curve_blue),
        .v_offset = offset_of(guzzi_camera3_metadata_dynamic_t, valid.tonemap_curve_blue),
        .s_offset = offset_of(guzzi_camera3_metadata_dynamic_t, s.tonemap_curve_blue),
        .size = sizeof (guzzi_camera3_dynamic_tonemap_curve_blue_t),
        .ident_size = sizeof (guzzi_camera3_dynamic_tonemap_curve_blue_identity_t),
        .dim_size_1 = GUZZI_CAMERA3_DYNAMIC_TONEMAP_CURVE_BLUE_DIM_MAX_SIZE_1,
        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_TONEMAP_CURVE_BLUE,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_DYNAMIC_TONEMAP_CURVE_GREEN),
        .struct_name = STR(guzzi_camera3_dynamic_tonemap_curve_green_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_dynamic_valid_t, tonemap_curve_green),
        .struct_offset = offset_of(guzzi_camera3_metadata_dynamic_struct_t, tonemap_curve_green),
        .v_offset = offset_of(guzzi_camera3_metadata_dynamic_t, valid.tonemap_curve_green),
        .s_offset = offset_of(guzzi_camera3_metadata_dynamic_t, s.tonemap_curve_green),
        .size = sizeof (guzzi_camera3_dynamic_tonemap_curve_green_t),
        .ident_size = sizeof (guzzi_camera3_dynamic_tonemap_curve_green_identity_t),
        .dim_size_1 = GUZZI_CAMERA3_DYNAMIC_TONEMAP_CURVE_GREEN_DIM_MAX_SIZE_1,
        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_TONEMAP_CURVE_GREEN,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_DYNAMIC_TONEMAP_CURVE_RED),
        .struct_name = STR(guzzi_camera3_dynamic_tonemap_curve_red_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_dynamic_valid_t, tonemap_curve_red),
        .struct_offset = offset_of(guzzi_camera3_metadata_dynamic_struct_t, tonemap_curve_red),
        .v_offset = offset_of(guzzi_camera3_metadata_dynamic_t, valid.tonemap_curve_red),
        .s_offset = offset_of(guzzi_camera3_metadata_dynamic_t, s.tonemap_curve_red),
        .size = sizeof (guzzi_camera3_dynamic_tonemap_curve_red_t),
        .ident_size = sizeof (guzzi_camera3_dynamic_tonemap_curve_red_identity_t),
        .dim_size_1 = GUZZI_CAMERA3_DYNAMIC_TONEMAP_CURVE_RED_DIM_MAX_SIZE_1,
        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_TONEMAP_CURVE_RED,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_DYNAMIC_TONEMAP_MODE),
        .struct_name = STR(guzzi_camera3_dynamic_tonemap_mode_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_dynamic_valid_t, tonemap_mode),
        .struct_offset = offset_of(guzzi_camera3_metadata_dynamic_struct_t, tonemap_mode),
        .v_offset = offset_of(guzzi_camera3_metadata_dynamic_t, valid.tonemap_mode),
        .s_offset = offset_of(guzzi_camera3_metadata_dynamic_t, s.tonemap_mode),
        .size = sizeof (guzzi_camera3_dynamic_tonemap_mode_t),
        .ident_size = sizeof (guzzi_camera3_dynamic_tonemap_mode_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_DYNAMIC_TONEMAP_MODE,
    },
};

static guzzi_camera3_metadata_helper_t guzzi_camera3_metadata_static_helper[] = {
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_STATIC_CONTROL_AE_AVAILABLE_ANTIBANDING_MODES),
        .struct_name = STR(guzzi_camera3_static_control_ae_available_antibanding_modes_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_static_valid_t, control_ae_available_antibanding_modes),
        .struct_offset = offset_of(guzzi_camera3_metadata_static_struct_t, control_ae_available_antibanding_modes),
        .v_offset = offset_of(guzzi_camera3_metadata_static_t, valid.control_ae_available_antibanding_modes),
        .s_offset = offset_of(guzzi_camera3_metadata_static_t, s.control_ae_available_antibanding_modes),
        .size = sizeof (guzzi_camera3_static_control_ae_available_antibanding_modes_t),
        .ident_size = sizeof (guzzi_camera3_static_control_ae_available_antibanding_modes_identity_t),
        .dim_size_1 = GUZZI_CAMERA3_STATIC_CONTROL_AE_AVAILABLE_ANTIBANDING_MODES_DIM_MAX_SIZE_1,
        .index = GUZZI_CAMERA3_INDEX_STATIC_CONTROL_AE_AVAILABLE_ANTIBANDING_MODES,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_STATIC_CONTROL_AE_AVAILABLE_MODES),
        .struct_name = STR(guzzi_camera3_static_control_ae_available_modes_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_static_valid_t, control_ae_available_modes),
        .struct_offset = offset_of(guzzi_camera3_metadata_static_struct_t, control_ae_available_modes),
        .v_offset = offset_of(guzzi_camera3_metadata_static_t, valid.control_ae_available_modes),
        .s_offset = offset_of(guzzi_camera3_metadata_static_t, s.control_ae_available_modes),
        .size = sizeof (guzzi_camera3_static_control_ae_available_modes_t),
        .ident_size = sizeof (guzzi_camera3_static_control_ae_available_modes_identity_t),
        .dim_size_1 = GUZZI_CAMERA3_STATIC_CONTROL_AE_AVAILABLE_MODES_DIM_MAX_SIZE_1,
        .index = GUZZI_CAMERA3_INDEX_STATIC_CONTROL_AE_AVAILABLE_MODES,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_STATIC_CONTROL_AE_AVAILABLE_TARGET_FPS_RANGES),
        .struct_name = STR(guzzi_camera3_static_control_ae_available_target_fps_ranges_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_static_valid_t, control_ae_available_target_fps_ranges),
        .struct_offset = offset_of(guzzi_camera3_metadata_static_struct_t, control_ae_available_target_fps_ranges),
        .v_offset = offset_of(guzzi_camera3_metadata_static_t, valid.control_ae_available_target_fps_ranges),
        .s_offset = offset_of(guzzi_camera3_metadata_static_t, s.control_ae_available_target_fps_ranges),
        .size = sizeof (guzzi_camera3_static_control_ae_available_target_fps_ranges_t),
        .ident_size = sizeof (guzzi_camera3_static_control_ae_available_target_fps_ranges_identity_t),
        .dim_size_1 = GUZZI_CAMERA3_STATIC_CONTROL_AE_AVAILABLE_TARGET_FPS_RANGES_DIM_MAX_SIZE_1,
        .index = GUZZI_CAMERA3_INDEX_STATIC_CONTROL_AE_AVAILABLE_TARGET_FPS_RANGES,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_STATIC_CONTROL_AE_COMPENSATION_RANGE),
        .struct_name = STR(guzzi_camera3_static_control_ae_compensation_range_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_static_valid_t, control_ae_compensation_range),
        .struct_offset = offset_of(guzzi_camera3_metadata_static_struct_t, control_ae_compensation_range),
        .v_offset = offset_of(guzzi_camera3_metadata_static_t, valid.control_ae_compensation_range),
        .s_offset = offset_of(guzzi_camera3_metadata_static_t, s.control_ae_compensation_range),
        .size = sizeof (guzzi_camera3_static_control_ae_compensation_range_t),
        .ident_size = sizeof (guzzi_camera3_static_control_ae_compensation_range_identity_t),
        .dim_size_1 = GUZZI_CAMERA3_STATIC_CONTROL_AE_COMPENSATION_RANGE_DIM_MAX_SIZE_1,
        .index = GUZZI_CAMERA3_INDEX_STATIC_CONTROL_AE_COMPENSATION_RANGE,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_STATIC_CONTROL_AE_COMPENSATION_STEP),
        .struct_name = STR(guzzi_camera3_static_control_ae_compensation_step_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_static_valid_t, control_ae_compensation_step),
        .struct_offset = offset_of(guzzi_camera3_metadata_static_struct_t, control_ae_compensation_step),
        .v_offset = offset_of(guzzi_camera3_metadata_static_t, valid.control_ae_compensation_step),
        .s_offset = offset_of(guzzi_camera3_metadata_static_t, s.control_ae_compensation_step),
        .size = sizeof (guzzi_camera3_static_control_ae_compensation_step_t),
        .ident_size = sizeof (guzzi_camera3_static_control_ae_compensation_step_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_STATIC_CONTROL_AE_COMPENSATION_STEP,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_STATIC_CONTROL_AF_AVAILABLE_MODES),
        .struct_name = STR(guzzi_camera3_static_control_af_available_modes_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_static_valid_t, control_af_available_modes),
        .struct_offset = offset_of(guzzi_camera3_metadata_static_struct_t, control_af_available_modes),
        .v_offset = offset_of(guzzi_camera3_metadata_static_t, valid.control_af_available_modes),
        .s_offset = offset_of(guzzi_camera3_metadata_static_t, s.control_af_available_modes),
        .size = sizeof (guzzi_camera3_static_control_af_available_modes_t),
        .ident_size = sizeof (guzzi_camera3_static_control_af_available_modes_identity_t),
        .dim_size_1 = GUZZI_CAMERA3_STATIC_CONTROL_AF_AVAILABLE_MODES_DIM_MAX_SIZE_1,
        .index = GUZZI_CAMERA3_INDEX_STATIC_CONTROL_AF_AVAILABLE_MODES,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_STATIC_CONTROL_AVAILABLE_EFFECTS),
        .struct_name = STR(guzzi_camera3_static_control_available_effects_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_static_valid_t, control_available_effects),
        .struct_offset = offset_of(guzzi_camera3_metadata_static_struct_t, control_available_effects),
        .v_offset = offset_of(guzzi_camera3_metadata_static_t, valid.control_available_effects),
        .s_offset = offset_of(guzzi_camera3_metadata_static_t, s.control_available_effects),
        .size = sizeof (guzzi_camera3_static_control_available_effects_t),
        .ident_size = sizeof (guzzi_camera3_static_control_available_effects_identity_t),
        .dim_size_1 = GUZZI_CAMERA3_STATIC_CONTROL_AVAILABLE_EFFECTS_DIM_MAX_SIZE_1,
        .index = GUZZI_CAMERA3_INDEX_STATIC_CONTROL_AVAILABLE_EFFECTS,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_STATIC_CONTROL_AVAILABLE_SCENE_MODES),
        .struct_name = STR(guzzi_camera3_static_control_available_scene_modes_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_static_valid_t, control_available_scene_modes),
        .struct_offset = offset_of(guzzi_camera3_metadata_static_struct_t, control_available_scene_modes),
        .v_offset = offset_of(guzzi_camera3_metadata_static_t, valid.control_available_scene_modes),
        .s_offset = offset_of(guzzi_camera3_metadata_static_t, s.control_available_scene_modes),
        .size = sizeof (guzzi_camera3_static_control_available_scene_modes_t),
        .ident_size = sizeof (guzzi_camera3_static_control_available_scene_modes_identity_t),
        .dim_size_1 = GUZZI_CAMERA3_STATIC_CONTROL_AVAILABLE_SCENE_MODES_DIM_MAX_SIZE_1,
        .index = GUZZI_CAMERA3_INDEX_STATIC_CONTROL_AVAILABLE_SCENE_MODES,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_STATIC_CONTROL_AVAILABLE_VIDEO_STABILIZATION_MODES),
        .struct_name = STR(guzzi_camera3_static_control_available_video_stabilization_modes_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_static_valid_t, control_available_video_stabilization_modes),
        .struct_offset = offset_of(guzzi_camera3_metadata_static_struct_t, control_available_video_stabilization_modes),
        .v_offset = offset_of(guzzi_camera3_metadata_static_t, valid.control_available_video_stabilization_modes),
        .s_offset = offset_of(guzzi_camera3_metadata_static_t, s.control_available_video_stabilization_modes),
        .size = sizeof (guzzi_camera3_static_control_available_video_stabilization_modes_t),
        .ident_size = sizeof (guzzi_camera3_static_control_available_video_stabilization_modes_identity_t),
        .dim_size_1 = GUZZI_CAMERA3_STATIC_CONTROL_AVAILABLE_VIDEO_STABILIZATION_MODES_DIM_MAX_SIZE_1,
        .index = GUZZI_CAMERA3_INDEX_STATIC_CONTROL_AVAILABLE_VIDEO_STABILIZATION_MODES,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_STATIC_CONTROL_AWB_AVAILABLE_MODES),
        .struct_name = STR(guzzi_camera3_static_control_awb_available_modes_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_static_valid_t, control_awb_available_modes),
        .struct_offset = offset_of(guzzi_camera3_metadata_static_struct_t, control_awb_available_modes),
        .v_offset = offset_of(guzzi_camera3_metadata_static_t, valid.control_awb_available_modes),
        .s_offset = offset_of(guzzi_camera3_metadata_static_t, s.control_awb_available_modes),
        .size = sizeof (guzzi_camera3_static_control_awb_available_modes_t),
        .ident_size = sizeof (guzzi_camera3_static_control_awb_available_modes_identity_t),
        .dim_size_1 = GUZZI_CAMERA3_STATIC_CONTROL_AWB_AVAILABLE_MODES_DIM_MAX_SIZE_1,
        .index = GUZZI_CAMERA3_INDEX_STATIC_CONTROL_AWB_AVAILABLE_MODES,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_STATIC_CONTROL_MAX_REGIONS),
        .struct_name = STR(guzzi_camera3_static_control_max_regions_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_static_valid_t, control_max_regions),
        .struct_offset = offset_of(guzzi_camera3_metadata_static_struct_t, control_max_regions),
        .v_offset = offset_of(guzzi_camera3_metadata_static_t, valid.control_max_regions),
        .s_offset = offset_of(guzzi_camera3_metadata_static_t, s.control_max_regions),
        .size = sizeof (guzzi_camera3_static_control_max_regions_t),
        .ident_size = sizeof (guzzi_camera3_static_control_max_regions_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_STATIC_CONTROL_MAX_REGIONS,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_STATIC_CONTROL_SCENE_MODE_OVERRIDES),
        .struct_name = STR(guzzi_camera3_static_control_scene_mode_overrides_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_static_valid_t, control_scene_mode_overrides),
        .struct_offset = offset_of(guzzi_camera3_metadata_static_struct_t, control_scene_mode_overrides),
        .v_offset = offset_of(guzzi_camera3_metadata_static_t, valid.control_scene_mode_overrides),
        .s_offset = offset_of(guzzi_camera3_metadata_static_t, s.control_scene_mode_overrides),
        .size = sizeof (guzzi_camera3_static_control_scene_mode_overrides_t),
        .ident_size = sizeof (guzzi_camera3_static_control_scene_mode_overrides_identity_t),
        .dim_size_1 = GUZZI_CAMERA3_STATIC_CONTROL_SCENE_MODE_OVERRIDES_DIM_MAX_SIZE_1,
        .index = GUZZI_CAMERA3_INDEX_STATIC_CONTROL_SCENE_MODE_OVERRIDES,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_STATIC_FLASH_COLOR_TEMPERATURE),
        .struct_name = STR(guzzi_camera3_static_flash_color_temperature_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_static_valid_t, flash_color_temperature),
        .struct_offset = offset_of(guzzi_camera3_metadata_static_struct_t, flash_color_temperature),
        .v_offset = offset_of(guzzi_camera3_metadata_static_t, valid.flash_color_temperature),
        .s_offset = offset_of(guzzi_camera3_metadata_static_t, s.flash_color_temperature),
        .size = sizeof (guzzi_camera3_static_flash_color_temperature_t),
        .ident_size = sizeof (guzzi_camera3_static_flash_color_temperature_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_STATIC_FLASH_COLOR_TEMPERATURE,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_STATIC_FLASH_INFO_AVAILABLE),
        .struct_name = STR(guzzi_camera3_static_flash_info_available_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_static_valid_t, flash_info_available),
        .struct_offset = offset_of(guzzi_camera3_metadata_static_struct_t, flash_info_available),
        .v_offset = offset_of(guzzi_camera3_metadata_static_t, valid.flash_info_available),
        .s_offset = offset_of(guzzi_camera3_metadata_static_t, s.flash_info_available),
        .size = sizeof (guzzi_camera3_static_flash_info_available_t),
        .ident_size = sizeof (guzzi_camera3_static_flash_info_available_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_STATIC_FLASH_INFO_AVAILABLE,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_STATIC_FLASH_INFO_CHARGE_DURATION),
        .struct_name = STR(guzzi_camera3_static_flash_info_charge_duration_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_static_valid_t, flash_info_charge_duration),
        .struct_offset = offset_of(guzzi_camera3_metadata_static_struct_t, flash_info_charge_duration),
        .v_offset = offset_of(guzzi_camera3_metadata_static_t, valid.flash_info_charge_duration),
        .s_offset = offset_of(guzzi_camera3_metadata_static_t, s.flash_info_charge_duration),
        .size = sizeof (guzzi_camera3_static_flash_info_charge_duration_t),
        .ident_size = sizeof (guzzi_camera3_static_flash_info_charge_duration_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_STATIC_FLASH_INFO_CHARGE_DURATION,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_STATIC_FLASH_MAX_ENERGY),
        .struct_name = STR(guzzi_camera3_static_flash_max_energy_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_static_valid_t, flash_max_energy),
        .struct_offset = offset_of(guzzi_camera3_metadata_static_struct_t, flash_max_energy),
        .v_offset = offset_of(guzzi_camera3_metadata_static_t, valid.flash_max_energy),
        .s_offset = offset_of(guzzi_camera3_metadata_static_t, s.flash_max_energy),
        .size = sizeof (guzzi_camera3_static_flash_max_energy_t),
        .ident_size = sizeof (guzzi_camera3_static_flash_max_energy_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_STATIC_FLASH_MAX_ENERGY,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_STATIC_HOT_PIXEL_INFO_MAP),
        .struct_name = STR(guzzi_camera3_static_hot_pixel_info_map_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_static_valid_t, hot_pixel_info_map),
        .struct_offset = offset_of(guzzi_camera3_metadata_static_struct_t, hot_pixel_info_map),
        .v_offset = offset_of(guzzi_camera3_metadata_static_t, valid.hot_pixel_info_map),
        .s_offset = offset_of(guzzi_camera3_metadata_static_t, s.hot_pixel_info_map),
        .size = sizeof (guzzi_camera3_static_hot_pixel_info_map_t),
        .ident_size = sizeof (guzzi_camera3_static_hot_pixel_info_map_identity_t),
        .dim_size_1 = GUZZI_CAMERA3_STATIC_HOT_PIXEL_INFO_MAP_DIM_MAX_SIZE_1,
        .index = GUZZI_CAMERA3_INDEX_STATIC_HOT_PIXEL_INFO_MAP,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_STATIC_INFO_SUPPORTED_HARDWARE_LEVEL),
        .struct_name = STR(guzzi_camera3_static_info_supported_hardware_level_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_static_valid_t, info_supported_hardware_level),
        .struct_offset = offset_of(guzzi_camera3_metadata_static_struct_t, info_supported_hardware_level),
        .v_offset = offset_of(guzzi_camera3_metadata_static_t, valid.info_supported_hardware_level),
        .s_offset = offset_of(guzzi_camera3_metadata_static_t, s.info_supported_hardware_level),
        .size = sizeof (guzzi_camera3_static_info_supported_hardware_level_t),
        .ident_size = sizeof (guzzi_camera3_static_info_supported_hardware_level_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_STATIC_INFO_SUPPORTED_HARDWARE_LEVEL,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_STATIC_JPEG_AVAILABLE_THUMBNAIL_SIZES),
        .struct_name = STR(guzzi_camera3_static_jpeg_available_thumbnail_sizes_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_static_valid_t, jpeg_available_thumbnail_sizes),
        .struct_offset = offset_of(guzzi_camera3_metadata_static_struct_t, jpeg_available_thumbnail_sizes),
        .v_offset = offset_of(guzzi_camera3_metadata_static_t, valid.jpeg_available_thumbnail_sizes),
        .s_offset = offset_of(guzzi_camera3_metadata_static_t, s.jpeg_available_thumbnail_sizes),
        .size = sizeof (guzzi_camera3_static_jpeg_available_thumbnail_sizes_t),
        .ident_size = sizeof (guzzi_camera3_static_jpeg_available_thumbnail_sizes_identity_t),
        .dim_size_1 = GUZZI_CAMERA3_STATIC_JPEG_AVAILABLE_THUMBNAIL_SIZES_DIM_MAX_SIZE_1,
        .index = GUZZI_CAMERA3_INDEX_STATIC_JPEG_AVAILABLE_THUMBNAIL_SIZES,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_STATIC_JPEG_MAX_SIZE),
        .struct_name = STR(guzzi_camera3_static_jpeg_max_size_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_static_valid_t, jpeg_max_size),
        .struct_offset = offset_of(guzzi_camera3_metadata_static_struct_t, jpeg_max_size),
        .v_offset = offset_of(guzzi_camera3_metadata_static_t, valid.jpeg_max_size),
        .s_offset = offset_of(guzzi_camera3_metadata_static_t, s.jpeg_max_size),
        .size = sizeof (guzzi_camera3_static_jpeg_max_size_t),
        .ident_size = sizeof (guzzi_camera3_static_jpeg_max_size_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_STATIC_JPEG_MAX_SIZE,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_STATIC_LED_AVAILABLE_LEDS),
        .struct_name = STR(guzzi_camera3_static_led_available_leds_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_static_valid_t, led_available_leds),
        .struct_offset = offset_of(guzzi_camera3_metadata_static_struct_t, led_available_leds),
        .v_offset = offset_of(guzzi_camera3_metadata_static_t, valid.led_available_leds),
        .s_offset = offset_of(guzzi_camera3_metadata_static_t, s.led_available_leds),
        .size = sizeof (guzzi_camera3_static_led_available_leds_t),
        .ident_size = sizeof (guzzi_camera3_static_led_available_leds_identity_t),
        .dim_size_1 = GUZZI_CAMERA3_STATIC_LED_AVAILABLE_LEDS_DIM_MAX_SIZE_1,
        .index = GUZZI_CAMERA3_INDEX_STATIC_LED_AVAILABLE_LEDS,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_STATIC_LENS_FACING),
        .struct_name = STR(guzzi_camera3_static_lens_facing_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_static_valid_t, lens_facing),
        .struct_offset = offset_of(guzzi_camera3_metadata_static_struct_t, lens_facing),
        .v_offset = offset_of(guzzi_camera3_metadata_static_t, valid.lens_facing),
        .s_offset = offset_of(guzzi_camera3_metadata_static_t, s.lens_facing),
        .size = sizeof (guzzi_camera3_static_lens_facing_t),
        .ident_size = sizeof (guzzi_camera3_static_lens_facing_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_STATIC_LENS_FACING,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_STATIC_LENS_INFO_AVAILABLE_APERTURES),
        .struct_name = STR(guzzi_camera3_static_lens_info_available_apertures_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_static_valid_t, lens_info_available_apertures),
        .struct_offset = offset_of(guzzi_camera3_metadata_static_struct_t, lens_info_available_apertures),
        .v_offset = offset_of(guzzi_camera3_metadata_static_t, valid.lens_info_available_apertures),
        .s_offset = offset_of(guzzi_camera3_metadata_static_t, s.lens_info_available_apertures),
        .size = sizeof (guzzi_camera3_static_lens_info_available_apertures_t),
        .ident_size = sizeof (guzzi_camera3_static_lens_info_available_apertures_identity_t),
        .dim_size_1 = GUZZI_CAMERA3_STATIC_LENS_INFO_AVAILABLE_APERTURES_DIM_MAX_SIZE_1,
        .index = GUZZI_CAMERA3_INDEX_STATIC_LENS_INFO_AVAILABLE_APERTURES,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_STATIC_LENS_INFO_AVAILABLE_FILTER_DENSITIES),
        .struct_name = STR(guzzi_camera3_static_lens_info_available_filter_densities_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_static_valid_t, lens_info_available_filter_densities),
        .struct_offset = offset_of(guzzi_camera3_metadata_static_struct_t, lens_info_available_filter_densities),
        .v_offset = offset_of(guzzi_camera3_metadata_static_t, valid.lens_info_available_filter_densities),
        .s_offset = offset_of(guzzi_camera3_metadata_static_t, s.lens_info_available_filter_densities),
        .size = sizeof (guzzi_camera3_static_lens_info_available_filter_densities_t),
        .ident_size = sizeof (guzzi_camera3_static_lens_info_available_filter_densities_identity_t),
        .dim_size_1 = GUZZI_CAMERA3_STATIC_LENS_INFO_AVAILABLE_FILTER_DENSITIES_DIM_MAX_SIZE_1,
        .index = GUZZI_CAMERA3_INDEX_STATIC_LENS_INFO_AVAILABLE_FILTER_DENSITIES,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_STATIC_LENS_INFO_AVAILABLE_FOCAL_LENGTHS),
        .struct_name = STR(guzzi_camera3_static_lens_info_available_focal_lengths_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_static_valid_t, lens_info_available_focal_lengths),
        .struct_offset = offset_of(guzzi_camera3_metadata_static_struct_t, lens_info_available_focal_lengths),
        .v_offset = offset_of(guzzi_camera3_metadata_static_t, valid.lens_info_available_focal_lengths),
        .s_offset = offset_of(guzzi_camera3_metadata_static_t, s.lens_info_available_focal_lengths),
        .size = sizeof (guzzi_camera3_static_lens_info_available_focal_lengths_t),
        .ident_size = sizeof (guzzi_camera3_static_lens_info_available_focal_lengths_identity_t),
        .dim_size_1 = GUZZI_CAMERA3_STATIC_LENS_INFO_AVAILABLE_FOCAL_LENGTHS_DIM_MAX_SIZE_1,
        .index = GUZZI_CAMERA3_INDEX_STATIC_LENS_INFO_AVAILABLE_FOCAL_LENGTHS,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_STATIC_LENS_INFO_AVAILABLE_OPTICAL_STABILIZATION),
        .struct_name = STR(guzzi_camera3_static_lens_info_available_optical_stabilization_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_static_valid_t, lens_info_available_optical_stabilization),
        .struct_offset = offset_of(guzzi_camera3_metadata_static_struct_t, lens_info_available_optical_stabilization),
        .v_offset = offset_of(guzzi_camera3_metadata_static_t, valid.lens_info_available_optical_stabilization),
        .s_offset = offset_of(guzzi_camera3_metadata_static_t, s.lens_info_available_optical_stabilization),
        .size = sizeof (guzzi_camera3_static_lens_info_available_optical_stabilization_t),
        .ident_size = sizeof (guzzi_camera3_static_lens_info_available_optical_stabilization_identity_t),
        .dim_size_1 = GUZZI_CAMERA3_STATIC_LENS_INFO_AVAILABLE_OPTICAL_STABILIZATION_DIM_MAX_SIZE_1,
        .index = GUZZI_CAMERA3_INDEX_STATIC_LENS_INFO_AVAILABLE_OPTICAL_STABILIZATION,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_STATIC_LENS_INFO_GEOMETRIC_CORRECTION_MAP),
        .struct_name = STR(guzzi_camera3_static_lens_info_geometric_correction_map_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_static_valid_t, lens_info_geometric_correction_map),
        .struct_offset = offset_of(guzzi_camera3_metadata_static_struct_t, lens_info_geometric_correction_map),
        .v_offset = offset_of(guzzi_camera3_metadata_static_t, valid.lens_info_geometric_correction_map),
        .s_offset = offset_of(guzzi_camera3_metadata_static_t, s.lens_info_geometric_correction_map),
        .size = sizeof (guzzi_camera3_static_lens_info_geometric_correction_map_t),
        .ident_size = sizeof (guzzi_camera3_static_lens_info_geometric_correction_map_identity_t),
        .dim_size_1 = GUZZI_CAMERA3_STATIC_LENS_INFO_GEOMETRIC_CORRECTION_MAP_DIM_MAX_SIZE_1,
        .index = GUZZI_CAMERA3_INDEX_STATIC_LENS_INFO_GEOMETRIC_CORRECTION_MAP,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_STATIC_LENS_INFO_GEOMETRIC_CORRECTION_MAP_SIZE),
        .struct_name = STR(guzzi_camera3_static_lens_info_geometric_correction_map_size_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_static_valid_t, lens_info_geometric_correction_map_size),
        .struct_offset = offset_of(guzzi_camera3_metadata_static_struct_t, lens_info_geometric_correction_map_size),
        .v_offset = offset_of(guzzi_camera3_metadata_static_t, valid.lens_info_geometric_correction_map_size),
        .s_offset = offset_of(guzzi_camera3_metadata_static_t, s.lens_info_geometric_correction_map_size),
        .size = sizeof (guzzi_camera3_static_lens_info_geometric_correction_map_size_t),
        .ident_size = sizeof (guzzi_camera3_static_lens_info_geometric_correction_map_size_identity_t),
        .dim_size_1 = GUZZI_CAMERA3_STATIC_LENS_INFO_GEOMETRIC_CORRECTION_MAP_SIZE_DIM_MAX_SIZE_1,
        .index = GUZZI_CAMERA3_INDEX_STATIC_LENS_INFO_GEOMETRIC_CORRECTION_MAP_SIZE,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_STATIC_LENS_INFO_HYPERFOCAL_DISTANCE),
        .struct_name = STR(guzzi_camera3_static_lens_info_hyperfocal_distance_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_static_valid_t, lens_info_hyperfocal_distance),
        .struct_offset = offset_of(guzzi_camera3_metadata_static_struct_t, lens_info_hyperfocal_distance),
        .v_offset = offset_of(guzzi_camera3_metadata_static_t, valid.lens_info_hyperfocal_distance),
        .s_offset = offset_of(guzzi_camera3_metadata_static_t, s.lens_info_hyperfocal_distance),
        .size = sizeof (guzzi_camera3_static_lens_info_hyperfocal_distance_t),
        .ident_size = sizeof (guzzi_camera3_static_lens_info_hyperfocal_distance_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_STATIC_LENS_INFO_HYPERFOCAL_DISTANCE,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_STATIC_LENS_INFO_MINIMUM_FOCUS_DISTANCE),
        .struct_name = STR(guzzi_camera3_static_lens_info_minimum_focus_distance_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_static_valid_t, lens_info_minimum_focus_distance),
        .struct_offset = offset_of(guzzi_camera3_metadata_static_struct_t, lens_info_minimum_focus_distance),
        .v_offset = offset_of(guzzi_camera3_metadata_static_t, valid.lens_info_minimum_focus_distance),
        .s_offset = offset_of(guzzi_camera3_metadata_static_t, s.lens_info_minimum_focus_distance),
        .size = sizeof (guzzi_camera3_static_lens_info_minimum_focus_distance_t),
        .ident_size = sizeof (guzzi_camera3_static_lens_info_minimum_focus_distance_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_STATIC_LENS_INFO_MINIMUM_FOCUS_DISTANCE,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_STATIC_LENS_INFO_SHADING_MAP_SIZE),
        .struct_name = STR(guzzi_camera3_static_lens_info_shading_map_size_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_static_valid_t, lens_info_shading_map_size),
        .struct_offset = offset_of(guzzi_camera3_metadata_static_struct_t, lens_info_shading_map_size),
        .v_offset = offset_of(guzzi_camera3_metadata_static_t, valid.lens_info_shading_map_size),
        .s_offset = offset_of(guzzi_camera3_metadata_static_t, s.lens_info_shading_map_size),
        .size = sizeof (guzzi_camera3_static_lens_info_shading_map_size_t),
        .ident_size = sizeof (guzzi_camera3_static_lens_info_shading_map_size_identity_t),
        .dim_size_1 = GUZZI_CAMERA3_STATIC_LENS_INFO_SHADING_MAP_SIZE_DIM_MAX_SIZE_1,
        .index = GUZZI_CAMERA3_INDEX_STATIC_LENS_INFO_SHADING_MAP_SIZE,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_STATIC_LENS_OPTICAL_AXIS_ANGLE),
        .struct_name = STR(guzzi_camera3_static_lens_optical_axis_angle_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_static_valid_t, lens_optical_axis_angle),
        .struct_offset = offset_of(guzzi_camera3_metadata_static_struct_t, lens_optical_axis_angle),
        .v_offset = offset_of(guzzi_camera3_metadata_static_t, valid.lens_optical_axis_angle),
        .s_offset = offset_of(guzzi_camera3_metadata_static_t, s.lens_optical_axis_angle),
        .size = sizeof (guzzi_camera3_static_lens_optical_axis_angle_t),
        .ident_size = sizeof (guzzi_camera3_static_lens_optical_axis_angle_identity_t),
        .dim_size_1 = GUZZI_CAMERA3_STATIC_LENS_OPTICAL_AXIS_ANGLE_DIM_MAX_SIZE_1,
        .index = GUZZI_CAMERA3_INDEX_STATIC_LENS_OPTICAL_AXIS_ANGLE,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_STATIC_LENS_POSITION),
        .struct_name = STR(guzzi_camera3_static_lens_position_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_static_valid_t, lens_position),
        .struct_offset = offset_of(guzzi_camera3_metadata_static_struct_t, lens_position),
        .v_offset = offset_of(guzzi_camera3_metadata_static_t, valid.lens_position),
        .s_offset = offset_of(guzzi_camera3_metadata_static_t, s.lens_position),
        .size = sizeof (guzzi_camera3_static_lens_position_t),
        .ident_size = sizeof (guzzi_camera3_static_lens_position_identity_t),
        .dim_size_1 = GUZZI_CAMERA3_STATIC_LENS_POSITION_DIM_MAX_SIZE_1,
        .index = GUZZI_CAMERA3_INDEX_STATIC_LENS_POSITION,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_STATIC_QUIRKS_METERING_CROP_REGION),
        .struct_name = STR(guzzi_camera3_static_quirks_metering_crop_region_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_static_valid_t, quirks_metering_crop_region),
        .struct_offset = offset_of(guzzi_camera3_metadata_static_struct_t, quirks_metering_crop_region),
        .v_offset = offset_of(guzzi_camera3_metadata_static_t, valid.quirks_metering_crop_region),
        .s_offset = offset_of(guzzi_camera3_metadata_static_t, s.quirks_metering_crop_region),
        .size = sizeof (guzzi_camera3_static_quirks_metering_crop_region_t),
        .ident_size = sizeof (guzzi_camera3_static_quirks_metering_crop_region_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_STATIC_QUIRKS_METERING_CROP_REGION,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_STATIC_QUIRKS_TRIGGER_AF_WITH_AUTO),
        .struct_name = STR(guzzi_camera3_static_quirks_trigger_af_with_auto_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_static_valid_t, quirks_trigger_af_with_auto),
        .struct_offset = offset_of(guzzi_camera3_metadata_static_struct_t, quirks_trigger_af_with_auto),
        .v_offset = offset_of(guzzi_camera3_metadata_static_t, valid.quirks_trigger_af_with_auto),
        .s_offset = offset_of(guzzi_camera3_metadata_static_t, s.quirks_trigger_af_with_auto),
        .size = sizeof (guzzi_camera3_static_quirks_trigger_af_with_auto_t),
        .ident_size = sizeof (guzzi_camera3_static_quirks_trigger_af_with_auto_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_STATIC_QUIRKS_TRIGGER_AF_WITH_AUTO,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_STATIC_QUIRKS_USE_PARTIAL_RESULT),
        .struct_name = STR(guzzi_camera3_static_quirks_use_partial_result_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_static_valid_t, quirks_use_partial_result),
        .struct_offset = offset_of(guzzi_camera3_metadata_static_struct_t, quirks_use_partial_result),
        .v_offset = offset_of(guzzi_camera3_metadata_static_t, valid.quirks_use_partial_result),
        .s_offset = offset_of(guzzi_camera3_metadata_static_t, s.quirks_use_partial_result),
        .size = sizeof (guzzi_camera3_static_quirks_use_partial_result_t),
        .ident_size = sizeof (guzzi_camera3_static_quirks_use_partial_result_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_STATIC_QUIRKS_USE_PARTIAL_RESULT,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_STATIC_QUIRKS_USE_ZSL_FORMAT),
        .struct_name = STR(guzzi_camera3_static_quirks_use_zsl_format_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_static_valid_t, quirks_use_zsl_format),
        .struct_offset = offset_of(guzzi_camera3_metadata_static_struct_t, quirks_use_zsl_format),
        .v_offset = offset_of(guzzi_camera3_metadata_static_t, valid.quirks_use_zsl_format),
        .s_offset = offset_of(guzzi_camera3_metadata_static_t, s.quirks_use_zsl_format),
        .size = sizeof (guzzi_camera3_static_quirks_use_zsl_format_t),
        .ident_size = sizeof (guzzi_camera3_static_quirks_use_zsl_format_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_STATIC_QUIRKS_USE_ZSL_FORMAT,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_STATIC_REQUEST_MAX_NUM_OUTPUT_STREAMS),
        .struct_name = STR(guzzi_camera3_static_request_max_num_output_streams_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_static_valid_t, request_max_num_output_streams),
        .struct_offset = offset_of(guzzi_camera3_metadata_static_struct_t, request_max_num_output_streams),
        .v_offset = offset_of(guzzi_camera3_metadata_static_t, valid.request_max_num_output_streams),
        .s_offset = offset_of(guzzi_camera3_metadata_static_t, s.request_max_num_output_streams),
        .size = sizeof (guzzi_camera3_static_request_max_num_output_streams_t),
        .ident_size = sizeof (guzzi_camera3_static_request_max_num_output_streams_identity_t),
        .dim_size_1 = GUZZI_CAMERA3_STATIC_REQUEST_MAX_NUM_OUTPUT_STREAMS_DIM_MAX_SIZE_1,
        .index = GUZZI_CAMERA3_INDEX_STATIC_REQUEST_MAX_NUM_OUTPUT_STREAMS,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_STATIC_REQUEST_MAX_NUM_REPROCESS_STREAMS),
        .struct_name = STR(guzzi_camera3_static_request_max_num_reprocess_streams_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_static_valid_t, request_max_num_reprocess_streams),
        .struct_offset = offset_of(guzzi_camera3_metadata_static_struct_t, request_max_num_reprocess_streams),
        .v_offset = offset_of(guzzi_camera3_metadata_static_t, valid.request_max_num_reprocess_streams),
        .s_offset = offset_of(guzzi_camera3_metadata_static_t, s.request_max_num_reprocess_streams),
        .size = sizeof (guzzi_camera3_static_request_max_num_reprocess_streams_t),
        .ident_size = sizeof (guzzi_camera3_static_request_max_num_reprocess_streams_identity_t),
        .dim_size_1 = GUZZI_CAMERA3_STATIC_REQUEST_MAX_NUM_REPROCESS_STREAMS_DIM_MAX_SIZE_1,
        .index = GUZZI_CAMERA3_INDEX_STATIC_REQUEST_MAX_NUM_REPROCESS_STREAMS,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_STATIC_SCALER_AVAILABLE_FORMATS),
        .struct_name = STR(guzzi_camera3_static_scaler_available_formats_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_static_valid_t, scaler_available_formats),
        .struct_offset = offset_of(guzzi_camera3_metadata_static_struct_t, scaler_available_formats),
        .v_offset = offset_of(guzzi_camera3_metadata_static_t, valid.scaler_available_formats),
        .s_offset = offset_of(guzzi_camera3_metadata_static_t, s.scaler_available_formats),
        .size = sizeof (guzzi_camera3_static_scaler_available_formats_t),
        .ident_size = sizeof (guzzi_camera3_static_scaler_available_formats_identity_t),
        .dim_size_1 = GUZZI_CAMERA3_STATIC_SCALER_AVAILABLE_FORMATS_DIM_MAX_SIZE_1,
        .index = GUZZI_CAMERA3_INDEX_STATIC_SCALER_AVAILABLE_FORMATS,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_STATIC_SCALER_AVAILABLE_JPEG_MIN_DURATIONS),
        .struct_name = STR(guzzi_camera3_static_scaler_available_jpeg_min_durations_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_static_valid_t, scaler_available_jpeg_min_durations),
        .struct_offset = offset_of(guzzi_camera3_metadata_static_struct_t, scaler_available_jpeg_min_durations),
        .v_offset = offset_of(guzzi_camera3_metadata_static_t, valid.scaler_available_jpeg_min_durations),
        .s_offset = offset_of(guzzi_camera3_metadata_static_t, s.scaler_available_jpeg_min_durations),
        .size = sizeof (guzzi_camera3_static_scaler_available_jpeg_min_durations_t),
        .ident_size = sizeof (guzzi_camera3_static_scaler_available_jpeg_min_durations_identity_t),
        .dim_size_1 = GUZZI_CAMERA3_STATIC_SCALER_AVAILABLE_JPEG_MIN_DURATIONS_DIM_MAX_SIZE_1,
        .index = GUZZI_CAMERA3_INDEX_STATIC_SCALER_AVAILABLE_JPEG_MIN_DURATIONS,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_STATIC_SCALER_AVAILABLE_JPEG_SIZES),
        .struct_name = STR(guzzi_camera3_static_scaler_available_jpeg_sizes_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_static_valid_t, scaler_available_jpeg_sizes),
        .struct_offset = offset_of(guzzi_camera3_metadata_static_struct_t, scaler_available_jpeg_sizes),
        .v_offset = offset_of(guzzi_camera3_metadata_static_t, valid.scaler_available_jpeg_sizes),
        .s_offset = offset_of(guzzi_camera3_metadata_static_t, s.scaler_available_jpeg_sizes),
        .size = sizeof (guzzi_camera3_static_scaler_available_jpeg_sizes_t),
        .ident_size = sizeof (guzzi_camera3_static_scaler_available_jpeg_sizes_identity_t),
        .dim_size_1 = GUZZI_CAMERA3_STATIC_SCALER_AVAILABLE_JPEG_SIZES_DIM_MAX_SIZE_1,
        .index = GUZZI_CAMERA3_INDEX_STATIC_SCALER_AVAILABLE_JPEG_SIZES,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_STATIC_SCALER_AVAILABLE_MAX_DIGITAL_ZOOM),
        .struct_name = STR(guzzi_camera3_static_scaler_available_max_digital_zoom_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_static_valid_t, scaler_available_max_digital_zoom),
        .struct_offset = offset_of(guzzi_camera3_metadata_static_struct_t, scaler_available_max_digital_zoom),
        .v_offset = offset_of(guzzi_camera3_metadata_static_t, valid.scaler_available_max_digital_zoom),
        .s_offset = offset_of(guzzi_camera3_metadata_static_t, s.scaler_available_max_digital_zoom),
        .size = sizeof (guzzi_camera3_static_scaler_available_max_digital_zoom_t),
        .ident_size = sizeof (guzzi_camera3_static_scaler_available_max_digital_zoom_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_STATIC_SCALER_AVAILABLE_MAX_DIGITAL_ZOOM,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_STATIC_SCALER_AVAILABLE_PROCESSED_MIN_DURATIONS),
        .struct_name = STR(guzzi_camera3_static_scaler_available_processed_min_durations_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_static_valid_t, scaler_available_processed_min_durations),
        .struct_offset = offset_of(guzzi_camera3_metadata_static_struct_t, scaler_available_processed_min_durations),
        .v_offset = offset_of(guzzi_camera3_metadata_static_t, valid.scaler_available_processed_min_durations),
        .s_offset = offset_of(guzzi_camera3_metadata_static_t, s.scaler_available_processed_min_durations),
        .size = sizeof (guzzi_camera3_static_scaler_available_processed_min_durations_t),
        .ident_size = sizeof (guzzi_camera3_static_scaler_available_processed_min_durations_identity_t),
        .dim_size_1 = GUZZI_CAMERA3_STATIC_SCALER_AVAILABLE_PROCESSED_MIN_DURATIONS_DIM_MAX_SIZE_1,
        .index = GUZZI_CAMERA3_INDEX_STATIC_SCALER_AVAILABLE_PROCESSED_MIN_DURATIONS,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_STATIC_SCALER_AVAILABLE_PROCESSED_SIZES),
        .struct_name = STR(guzzi_camera3_static_scaler_available_processed_sizes_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_static_valid_t, scaler_available_processed_sizes),
        .struct_offset = offset_of(guzzi_camera3_metadata_static_struct_t, scaler_available_processed_sizes),
        .v_offset = offset_of(guzzi_camera3_metadata_static_t, valid.scaler_available_processed_sizes),
        .s_offset = offset_of(guzzi_camera3_metadata_static_t, s.scaler_available_processed_sizes),
        .size = sizeof (guzzi_camera3_static_scaler_available_processed_sizes_t),
        .ident_size = sizeof (guzzi_camera3_static_scaler_available_processed_sizes_identity_t),
        .dim_size_1 = GUZZI_CAMERA3_STATIC_SCALER_AVAILABLE_PROCESSED_SIZES_DIM_MAX_SIZE_1,
        .index = GUZZI_CAMERA3_INDEX_STATIC_SCALER_AVAILABLE_PROCESSED_SIZES,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_STATIC_SCALER_AVAILABLE_RAW_MIN_DURATIONS),
        .struct_name = STR(guzzi_camera3_static_scaler_available_raw_min_durations_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_static_valid_t, scaler_available_raw_min_durations),
        .struct_offset = offset_of(guzzi_camera3_metadata_static_struct_t, scaler_available_raw_min_durations),
        .v_offset = offset_of(guzzi_camera3_metadata_static_t, valid.scaler_available_raw_min_durations),
        .s_offset = offset_of(guzzi_camera3_metadata_static_t, s.scaler_available_raw_min_durations),
        .size = sizeof (guzzi_camera3_static_scaler_available_raw_min_durations_t),
        .ident_size = sizeof (guzzi_camera3_static_scaler_available_raw_min_durations_identity_t),
        .dim_size_1 = GUZZI_CAMERA3_STATIC_SCALER_AVAILABLE_RAW_MIN_DURATIONS_DIM_MAX_SIZE_1,
        .index = GUZZI_CAMERA3_INDEX_STATIC_SCALER_AVAILABLE_RAW_MIN_DURATIONS,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_STATIC_SCALER_AVAILABLE_RAW_SIZES),
        .struct_name = STR(guzzi_camera3_static_scaler_available_raw_sizes_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_static_valid_t, scaler_available_raw_sizes),
        .struct_offset = offset_of(guzzi_camera3_metadata_static_struct_t, scaler_available_raw_sizes),
        .v_offset = offset_of(guzzi_camera3_metadata_static_t, valid.scaler_available_raw_sizes),
        .s_offset = offset_of(guzzi_camera3_metadata_static_t, s.scaler_available_raw_sizes),
        .size = sizeof (guzzi_camera3_static_scaler_available_raw_sizes_t),
        .ident_size = sizeof (guzzi_camera3_static_scaler_available_raw_sizes_identity_t),
        .dim_size_1 = GUZZI_CAMERA3_STATIC_SCALER_AVAILABLE_RAW_SIZES_DIM_MAX_SIZE_1,
        .index = GUZZI_CAMERA3_INDEX_STATIC_SCALER_AVAILABLE_RAW_SIZES,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_STATIC_SENSOR_BASE_GAIN_FACTOR),
        .struct_name = STR(guzzi_camera3_static_sensor_base_gain_factor_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_static_valid_t, sensor_base_gain_factor),
        .struct_offset = offset_of(guzzi_camera3_metadata_static_struct_t, sensor_base_gain_factor),
        .v_offset = offset_of(guzzi_camera3_metadata_static_t, valid.sensor_base_gain_factor),
        .s_offset = offset_of(guzzi_camera3_metadata_static_t, s.sensor_base_gain_factor),
        .size = sizeof (guzzi_camera3_static_sensor_base_gain_factor_t),
        .ident_size = sizeof (guzzi_camera3_static_sensor_base_gain_factor_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_STATIC_SENSOR_BASE_GAIN_FACTOR,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_STATIC_SENSOR_BLACK_LEVEL_PATTERN),
        .struct_name = STR(guzzi_camera3_static_sensor_black_level_pattern_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_static_valid_t, sensor_black_level_pattern),
        .struct_offset = offset_of(guzzi_camera3_metadata_static_struct_t, sensor_black_level_pattern),
        .v_offset = offset_of(guzzi_camera3_metadata_static_t, valid.sensor_black_level_pattern),
        .s_offset = offset_of(guzzi_camera3_metadata_static_t, s.sensor_black_level_pattern),
        .size = sizeof (guzzi_camera3_static_sensor_black_level_pattern_t),
        .ident_size = sizeof (guzzi_camera3_static_sensor_black_level_pattern_identity_t),
        .dim_size_1 = GUZZI_CAMERA3_STATIC_SENSOR_BLACK_LEVEL_PATTERN_DIM_MAX_SIZE_1,
        .index = GUZZI_CAMERA3_INDEX_STATIC_SENSOR_BLACK_LEVEL_PATTERN,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_STATIC_SENSOR_CALIBRATION_TRANSFORM1),
        .struct_name = STR(guzzi_camera3_static_sensor_calibration_transform1_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_static_valid_t, sensor_calibration_transform1),
        .struct_offset = offset_of(guzzi_camera3_metadata_static_struct_t, sensor_calibration_transform1),
        .v_offset = offset_of(guzzi_camera3_metadata_static_t, valid.sensor_calibration_transform1),
        .s_offset = offset_of(guzzi_camera3_metadata_static_t, s.sensor_calibration_transform1),
        .size = sizeof (guzzi_camera3_static_sensor_calibration_transform1_t),
        .ident_size = sizeof (guzzi_camera3_static_sensor_calibration_transform1_identity_t),
        .dim_size_1 = GUZZI_CAMERA3_STATIC_SENSOR_CALIBRATION_TRANSFORM1_DIM_MAX_SIZE_1,
        .index = GUZZI_CAMERA3_INDEX_STATIC_SENSOR_CALIBRATION_TRANSFORM1,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_STATIC_SENSOR_CALIBRATION_TRANSFORM2),
        .struct_name = STR(guzzi_camera3_static_sensor_calibration_transform2_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_static_valid_t, sensor_calibration_transform2),
        .struct_offset = offset_of(guzzi_camera3_metadata_static_struct_t, sensor_calibration_transform2),
        .v_offset = offset_of(guzzi_camera3_metadata_static_t, valid.sensor_calibration_transform2),
        .s_offset = offset_of(guzzi_camera3_metadata_static_t, s.sensor_calibration_transform2),
        .size = sizeof (guzzi_camera3_static_sensor_calibration_transform2_t),
        .ident_size = sizeof (guzzi_camera3_static_sensor_calibration_transform2_identity_t),
        .dim_size_1 = GUZZI_CAMERA3_STATIC_SENSOR_CALIBRATION_TRANSFORM2_DIM_MAX_SIZE_1,
        .index = GUZZI_CAMERA3_INDEX_STATIC_SENSOR_CALIBRATION_TRANSFORM2,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_STATIC_SENSOR_COLOR_TRANSFORM1),
        .struct_name = STR(guzzi_camera3_static_sensor_color_transform1_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_static_valid_t, sensor_color_transform1),
        .struct_offset = offset_of(guzzi_camera3_metadata_static_struct_t, sensor_color_transform1),
        .v_offset = offset_of(guzzi_camera3_metadata_static_t, valid.sensor_color_transform1),
        .s_offset = offset_of(guzzi_camera3_metadata_static_t, s.sensor_color_transform1),
        .size = sizeof (guzzi_camera3_static_sensor_color_transform1_t),
        .ident_size = sizeof (guzzi_camera3_static_sensor_color_transform1_identity_t),
        .dim_size_1 = GUZZI_CAMERA3_STATIC_SENSOR_COLOR_TRANSFORM1_DIM_MAX_SIZE_1,
        .index = GUZZI_CAMERA3_INDEX_STATIC_SENSOR_COLOR_TRANSFORM1,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_STATIC_SENSOR_COLOR_TRANSFORM2),
        .struct_name = STR(guzzi_camera3_static_sensor_color_transform2_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_static_valid_t, sensor_color_transform2),
        .struct_offset = offset_of(guzzi_camera3_metadata_static_struct_t, sensor_color_transform2),
        .v_offset = offset_of(guzzi_camera3_metadata_static_t, valid.sensor_color_transform2),
        .s_offset = offset_of(guzzi_camera3_metadata_static_t, s.sensor_color_transform2),
        .size = sizeof (guzzi_camera3_static_sensor_color_transform2_t),
        .ident_size = sizeof (guzzi_camera3_static_sensor_color_transform2_identity_t),
        .dim_size_1 = GUZZI_CAMERA3_STATIC_SENSOR_COLOR_TRANSFORM2_DIM_MAX_SIZE_1,
        .index = GUZZI_CAMERA3_INDEX_STATIC_SENSOR_COLOR_TRANSFORM2,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_STATIC_SENSOR_FORWARD_MATRIX1),
        .struct_name = STR(guzzi_camera3_static_sensor_forward_matrix1_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_static_valid_t, sensor_forward_matrix1),
        .struct_offset = offset_of(guzzi_camera3_metadata_static_struct_t, sensor_forward_matrix1),
        .v_offset = offset_of(guzzi_camera3_metadata_static_t, valid.sensor_forward_matrix1),
        .s_offset = offset_of(guzzi_camera3_metadata_static_t, s.sensor_forward_matrix1),
        .size = sizeof (guzzi_camera3_static_sensor_forward_matrix1_t),
        .ident_size = sizeof (guzzi_camera3_static_sensor_forward_matrix1_identity_t),
        .dim_size_1 = GUZZI_CAMERA3_STATIC_SENSOR_FORWARD_MATRIX1_DIM_MAX_SIZE_1,
        .index = GUZZI_CAMERA3_INDEX_STATIC_SENSOR_FORWARD_MATRIX1,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_STATIC_SENSOR_FORWARD_MATRIX2),
        .struct_name = STR(guzzi_camera3_static_sensor_forward_matrix2_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_static_valid_t, sensor_forward_matrix2),
        .struct_offset = offset_of(guzzi_camera3_metadata_static_struct_t, sensor_forward_matrix2),
        .v_offset = offset_of(guzzi_camera3_metadata_static_t, valid.sensor_forward_matrix2),
        .s_offset = offset_of(guzzi_camera3_metadata_static_t, s.sensor_forward_matrix2),
        .size = sizeof (guzzi_camera3_static_sensor_forward_matrix2_t),
        .ident_size = sizeof (guzzi_camera3_static_sensor_forward_matrix2_identity_t),
        .dim_size_1 = GUZZI_CAMERA3_STATIC_SENSOR_FORWARD_MATRIX2_DIM_MAX_SIZE_1,
        .index = GUZZI_CAMERA3_INDEX_STATIC_SENSOR_FORWARD_MATRIX2,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_STATIC_SENSOR_INFO_ACTIVE_ARRAY_SIZE),
        .struct_name = STR(guzzi_camera3_static_sensor_info_active_array_size_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_static_valid_t, sensor_info_active_array_size),
        .struct_offset = offset_of(guzzi_camera3_metadata_static_struct_t, sensor_info_active_array_size),
        .v_offset = offset_of(guzzi_camera3_metadata_static_t, valid.sensor_info_active_array_size),
        .s_offset = offset_of(guzzi_camera3_metadata_static_t, s.sensor_info_active_array_size),
        .size = sizeof (guzzi_camera3_static_sensor_info_active_array_size_t),
        .ident_size = sizeof (guzzi_camera3_static_sensor_info_active_array_size_identity_t),
        .dim_size_1 = GUZZI_CAMERA3_STATIC_SENSOR_INFO_ACTIVE_ARRAY_SIZE_DIM_MAX_SIZE_1,
        .index = GUZZI_CAMERA3_INDEX_STATIC_SENSOR_INFO_ACTIVE_ARRAY_SIZE,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_STATIC_SENSOR_INFO_COLOR_FILTER_ARRANGEMENT),
        .struct_name = STR(guzzi_camera3_static_sensor_info_color_filter_arrangement_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_static_valid_t, sensor_info_color_filter_arrangement),
        .struct_offset = offset_of(guzzi_camera3_metadata_static_struct_t, sensor_info_color_filter_arrangement),
        .v_offset = offset_of(guzzi_camera3_metadata_static_t, valid.sensor_info_color_filter_arrangement),
        .s_offset = offset_of(guzzi_camera3_metadata_static_t, s.sensor_info_color_filter_arrangement),
        .size = sizeof (guzzi_camera3_static_sensor_info_color_filter_arrangement_t),
        .ident_size = sizeof (guzzi_camera3_static_sensor_info_color_filter_arrangement_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_STATIC_SENSOR_INFO_COLOR_FILTER_ARRANGEMENT,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_STATIC_SENSOR_INFO_EXPOSURE_TIME_RANGE),
        .struct_name = STR(guzzi_camera3_static_sensor_info_exposure_time_range_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_static_valid_t, sensor_info_exposure_time_range),
        .struct_offset = offset_of(guzzi_camera3_metadata_static_struct_t, sensor_info_exposure_time_range),
        .v_offset = offset_of(guzzi_camera3_metadata_static_t, valid.sensor_info_exposure_time_range),
        .s_offset = offset_of(guzzi_camera3_metadata_static_t, s.sensor_info_exposure_time_range),
        .size = sizeof (guzzi_camera3_static_sensor_info_exposure_time_range_t),
        .ident_size = sizeof (guzzi_camera3_static_sensor_info_exposure_time_range_identity_t),
        .dim_size_1 = GUZZI_CAMERA3_STATIC_SENSOR_INFO_EXPOSURE_TIME_RANGE_DIM_MAX_SIZE_1,
        .index = GUZZI_CAMERA3_INDEX_STATIC_SENSOR_INFO_EXPOSURE_TIME_RANGE,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_STATIC_SENSOR_INFO_MAX_FRAME_DURATION),
        .struct_name = STR(guzzi_camera3_static_sensor_info_max_frame_duration_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_static_valid_t, sensor_info_max_frame_duration),
        .struct_offset = offset_of(guzzi_camera3_metadata_static_struct_t, sensor_info_max_frame_duration),
        .v_offset = offset_of(guzzi_camera3_metadata_static_t, valid.sensor_info_max_frame_duration),
        .s_offset = offset_of(guzzi_camera3_metadata_static_t, s.sensor_info_max_frame_duration),
        .size = sizeof (guzzi_camera3_static_sensor_info_max_frame_duration_t),
        .ident_size = sizeof (guzzi_camera3_static_sensor_info_max_frame_duration_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_STATIC_SENSOR_INFO_MAX_FRAME_DURATION,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_STATIC_SENSOR_INFO_PHYSICAL_SIZE),
        .struct_name = STR(guzzi_camera3_static_sensor_info_physical_size_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_static_valid_t, sensor_info_physical_size),
        .struct_offset = offset_of(guzzi_camera3_metadata_static_struct_t, sensor_info_physical_size),
        .v_offset = offset_of(guzzi_camera3_metadata_static_t, valid.sensor_info_physical_size),
        .s_offset = offset_of(guzzi_camera3_metadata_static_t, s.sensor_info_physical_size),
        .size = sizeof (guzzi_camera3_static_sensor_info_physical_size_t),
        .ident_size = sizeof (guzzi_camera3_static_sensor_info_physical_size_identity_t),
        .dim_size_1 = GUZZI_CAMERA3_STATIC_SENSOR_INFO_PHYSICAL_SIZE_DIM_MAX_SIZE_1,
        .index = GUZZI_CAMERA3_INDEX_STATIC_SENSOR_INFO_PHYSICAL_SIZE,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_STATIC_SENSOR_INFO_PIXEL_ARRAY_SIZE),
        .struct_name = STR(guzzi_camera3_static_sensor_info_pixel_array_size_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_static_valid_t, sensor_info_pixel_array_size),
        .struct_offset = offset_of(guzzi_camera3_metadata_static_struct_t, sensor_info_pixel_array_size),
        .v_offset = offset_of(guzzi_camera3_metadata_static_t, valid.sensor_info_pixel_array_size),
        .s_offset = offset_of(guzzi_camera3_metadata_static_t, s.sensor_info_pixel_array_size),
        .size = sizeof (guzzi_camera3_static_sensor_info_pixel_array_size_t),
        .ident_size = sizeof (guzzi_camera3_static_sensor_info_pixel_array_size_identity_t),
        .dim_size_1 = GUZZI_CAMERA3_STATIC_SENSOR_INFO_PIXEL_ARRAY_SIZE_DIM_MAX_SIZE_1,
        .index = GUZZI_CAMERA3_INDEX_STATIC_SENSOR_INFO_PIXEL_ARRAY_SIZE,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_STATIC_SENSOR_INFO_SENSITIVITY_RANGE),
        .struct_name = STR(guzzi_camera3_static_sensor_info_sensitivity_range_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_static_valid_t, sensor_info_sensitivity_range),
        .struct_offset = offset_of(guzzi_camera3_metadata_static_struct_t, sensor_info_sensitivity_range),
        .v_offset = offset_of(guzzi_camera3_metadata_static_t, valid.sensor_info_sensitivity_range),
        .s_offset = offset_of(guzzi_camera3_metadata_static_t, s.sensor_info_sensitivity_range),
        .size = sizeof (guzzi_camera3_static_sensor_info_sensitivity_range_t),
        .ident_size = sizeof (guzzi_camera3_static_sensor_info_sensitivity_range_identity_t),
        .dim_size_1 = GUZZI_CAMERA3_STATIC_SENSOR_INFO_SENSITIVITY_RANGE_DIM_MAX_SIZE_1,
        .index = GUZZI_CAMERA3_INDEX_STATIC_SENSOR_INFO_SENSITIVITY_RANGE,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_STATIC_SENSOR_INFO_WHITE_LEVEL),
        .struct_name = STR(guzzi_camera3_static_sensor_info_white_level_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_static_valid_t, sensor_info_white_level),
        .struct_offset = offset_of(guzzi_camera3_metadata_static_struct_t, sensor_info_white_level),
        .v_offset = offset_of(guzzi_camera3_metadata_static_t, valid.sensor_info_white_level),
        .s_offset = offset_of(guzzi_camera3_metadata_static_t, s.sensor_info_white_level),
        .size = sizeof (guzzi_camera3_static_sensor_info_white_level_t),
        .ident_size = sizeof (guzzi_camera3_static_sensor_info_white_level_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_STATIC_SENSOR_INFO_WHITE_LEVEL,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_STATIC_SENSOR_MAX_ANALOG_SENSITIVITY),
        .struct_name = STR(guzzi_camera3_static_sensor_max_analog_sensitivity_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_static_valid_t, sensor_max_analog_sensitivity),
        .struct_offset = offset_of(guzzi_camera3_metadata_static_struct_t, sensor_max_analog_sensitivity),
        .v_offset = offset_of(guzzi_camera3_metadata_static_t, valid.sensor_max_analog_sensitivity),
        .s_offset = offset_of(guzzi_camera3_metadata_static_t, s.sensor_max_analog_sensitivity),
        .size = sizeof (guzzi_camera3_static_sensor_max_analog_sensitivity_t),
        .ident_size = sizeof (guzzi_camera3_static_sensor_max_analog_sensitivity_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_STATIC_SENSOR_MAX_ANALOG_SENSITIVITY,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_STATIC_SENSOR_NOISE_MODEL_COEFFICIENTS),
        .struct_name = STR(guzzi_camera3_static_sensor_noise_model_coefficients_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_static_valid_t, sensor_noise_model_coefficients),
        .struct_offset = offset_of(guzzi_camera3_metadata_static_struct_t, sensor_noise_model_coefficients),
        .v_offset = offset_of(guzzi_camera3_metadata_static_t, valid.sensor_noise_model_coefficients),
        .s_offset = offset_of(guzzi_camera3_metadata_static_t, s.sensor_noise_model_coefficients),
        .size = sizeof (guzzi_camera3_static_sensor_noise_model_coefficients_t),
        .ident_size = sizeof (guzzi_camera3_static_sensor_noise_model_coefficients_identity_t),
        .dim_size_1 = GUZZI_CAMERA3_STATIC_SENSOR_NOISE_MODEL_COEFFICIENTS_DIM_MAX_SIZE_1,
        .index = GUZZI_CAMERA3_INDEX_STATIC_SENSOR_NOISE_MODEL_COEFFICIENTS,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_STATIC_SENSOR_ORIENTATION),
        .struct_name = STR(guzzi_camera3_static_sensor_orientation_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_static_valid_t, sensor_orientation),
        .struct_offset = offset_of(guzzi_camera3_metadata_static_struct_t, sensor_orientation),
        .v_offset = offset_of(guzzi_camera3_metadata_static_t, valid.sensor_orientation),
        .s_offset = offset_of(guzzi_camera3_metadata_static_t, s.sensor_orientation),
        .size = sizeof (guzzi_camera3_static_sensor_orientation_t),
        .ident_size = sizeof (guzzi_camera3_static_sensor_orientation_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_STATIC_SENSOR_ORIENTATION,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_STATIC_SENSOR_REFERENCE_ILLUMINANT1),
        .struct_name = STR(guzzi_camera3_static_sensor_reference_illuminant1_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_static_valid_t, sensor_reference_illuminant1),
        .struct_offset = offset_of(guzzi_camera3_metadata_static_struct_t, sensor_reference_illuminant1),
        .v_offset = offset_of(guzzi_camera3_metadata_static_t, valid.sensor_reference_illuminant1),
        .s_offset = offset_of(guzzi_camera3_metadata_static_t, s.sensor_reference_illuminant1),
        .size = sizeof (guzzi_camera3_static_sensor_reference_illuminant1_t),
        .ident_size = sizeof (guzzi_camera3_static_sensor_reference_illuminant1_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_STATIC_SENSOR_REFERENCE_ILLUMINANT1,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_STATIC_SENSOR_REFERENCE_ILLUMINANT2),
        .struct_name = STR(guzzi_camera3_static_sensor_reference_illuminant2_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_static_valid_t, sensor_reference_illuminant2),
        .struct_offset = offset_of(guzzi_camera3_metadata_static_struct_t, sensor_reference_illuminant2),
        .v_offset = offset_of(guzzi_camera3_metadata_static_t, valid.sensor_reference_illuminant2),
        .s_offset = offset_of(guzzi_camera3_metadata_static_t, s.sensor_reference_illuminant2),
        .size = sizeof (guzzi_camera3_static_sensor_reference_illuminant2_t),
        .ident_size = sizeof (guzzi_camera3_static_sensor_reference_illuminant2_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_STATIC_SENSOR_REFERENCE_ILLUMINANT2,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_STATIC_STATISTICS_INFO_AVAILABLE_FACE_DETECT_MODES),
        .struct_name = STR(guzzi_camera3_static_statistics_info_available_face_detect_modes_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_static_valid_t, statistics_info_available_face_detect_modes),
        .struct_offset = offset_of(guzzi_camera3_metadata_static_struct_t, statistics_info_available_face_detect_modes),
        .v_offset = offset_of(guzzi_camera3_metadata_static_t, valid.statistics_info_available_face_detect_modes),
        .s_offset = offset_of(guzzi_camera3_metadata_static_t, s.statistics_info_available_face_detect_modes),
        .size = sizeof (guzzi_camera3_static_statistics_info_available_face_detect_modes_t),
        .ident_size = sizeof (guzzi_camera3_static_statistics_info_available_face_detect_modes_identity_t),
        .dim_size_1 = GUZZI_CAMERA3_STATIC_STATISTICS_INFO_AVAILABLE_FACE_DETECT_MODES_DIM_MAX_SIZE_1,
        .index = GUZZI_CAMERA3_INDEX_STATIC_STATISTICS_INFO_AVAILABLE_FACE_DETECT_MODES,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_STATIC_STATISTICS_INFO_HISTOGRAM_BUCKET_COUNT),
        .struct_name = STR(guzzi_camera3_static_statistics_info_histogram_bucket_count_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_static_valid_t, statistics_info_histogram_bucket_count),
        .struct_offset = offset_of(guzzi_camera3_metadata_static_struct_t, statistics_info_histogram_bucket_count),
        .v_offset = offset_of(guzzi_camera3_metadata_static_t, valid.statistics_info_histogram_bucket_count),
        .s_offset = offset_of(guzzi_camera3_metadata_static_t, s.statistics_info_histogram_bucket_count),
        .size = sizeof (guzzi_camera3_static_statistics_info_histogram_bucket_count_t),
        .ident_size = sizeof (guzzi_camera3_static_statistics_info_histogram_bucket_count_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_STATIC_STATISTICS_INFO_HISTOGRAM_BUCKET_COUNT,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_STATIC_STATISTICS_INFO_MAX_FACE_COUNT),
        .struct_name = STR(guzzi_camera3_static_statistics_info_max_face_count_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_static_valid_t, statistics_info_max_face_count),
        .struct_offset = offset_of(guzzi_camera3_metadata_static_struct_t, statistics_info_max_face_count),
        .v_offset = offset_of(guzzi_camera3_metadata_static_t, valid.statistics_info_max_face_count),
        .s_offset = offset_of(guzzi_camera3_metadata_static_t, s.statistics_info_max_face_count),
        .size = sizeof (guzzi_camera3_static_statistics_info_max_face_count_t),
        .ident_size = sizeof (guzzi_camera3_static_statistics_info_max_face_count_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_STATIC_STATISTICS_INFO_MAX_FACE_COUNT,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_STATIC_STATISTICS_INFO_MAX_HISTOGRAM_COUNT),
        .struct_name = STR(guzzi_camera3_static_statistics_info_max_histogram_count_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_static_valid_t, statistics_info_max_histogram_count),
        .struct_offset = offset_of(guzzi_camera3_metadata_static_struct_t, statistics_info_max_histogram_count),
        .v_offset = offset_of(guzzi_camera3_metadata_static_t, valid.statistics_info_max_histogram_count),
        .s_offset = offset_of(guzzi_camera3_metadata_static_t, s.statistics_info_max_histogram_count),
        .size = sizeof (guzzi_camera3_static_statistics_info_max_histogram_count_t),
        .ident_size = sizeof (guzzi_camera3_static_statistics_info_max_histogram_count_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_STATIC_STATISTICS_INFO_MAX_HISTOGRAM_COUNT,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_STATIC_STATISTICS_INFO_MAX_SHARPNESS_MAP_VALUE),
        .struct_name = STR(guzzi_camera3_static_statistics_info_max_sharpness_map_value_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_static_valid_t, statistics_info_max_sharpness_map_value),
        .struct_offset = offset_of(guzzi_camera3_metadata_static_struct_t, statistics_info_max_sharpness_map_value),
        .v_offset = offset_of(guzzi_camera3_metadata_static_t, valid.statistics_info_max_sharpness_map_value),
        .s_offset = offset_of(guzzi_camera3_metadata_static_t, s.statistics_info_max_sharpness_map_value),
        .size = sizeof (guzzi_camera3_static_statistics_info_max_sharpness_map_value_t),
        .ident_size = sizeof (guzzi_camera3_static_statistics_info_max_sharpness_map_value_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_STATIC_STATISTICS_INFO_MAX_SHARPNESS_MAP_VALUE,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_STATIC_STATISTICS_INFO_SHARPNESS_MAP_SIZE),
        .struct_name = STR(guzzi_camera3_static_statistics_info_sharpness_map_size_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_static_valid_t, statistics_info_sharpness_map_size),
        .struct_offset = offset_of(guzzi_camera3_metadata_static_struct_t, statistics_info_sharpness_map_size),
        .v_offset = offset_of(guzzi_camera3_metadata_static_t, valid.statistics_info_sharpness_map_size),
        .s_offset = offset_of(guzzi_camera3_metadata_static_t, s.statistics_info_sharpness_map_size),
        .size = sizeof (guzzi_camera3_static_statistics_info_sharpness_map_size_t),
        .ident_size = sizeof (guzzi_camera3_static_statistics_info_sharpness_map_size_identity_t),
        .dim_size_1 = GUZZI_CAMERA3_STATIC_STATISTICS_INFO_SHARPNESS_MAP_SIZE_DIM_MAX_SIZE_1,
        .index = GUZZI_CAMERA3_INDEX_STATIC_STATISTICS_INFO_SHARPNESS_MAP_SIZE,
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_STATIC_TONEMAP_MAX_CURVE_POINTS),
        .struct_name = STR(guzzi_camera3_static_tonemap_max_curve_points_t),
        .valid_offset = offset_of(guzzi_camera3_metadata_static_valid_t, tonemap_max_curve_points),
        .struct_offset = offset_of(guzzi_camera3_metadata_static_struct_t, tonemap_max_curve_points),
        .v_offset = offset_of(guzzi_camera3_metadata_static_t, valid.tonemap_max_curve_points),
        .s_offset = offset_of(guzzi_camera3_metadata_static_t, s.tonemap_max_curve_points),
        .size = sizeof (guzzi_camera3_static_tonemap_max_curve_points_t),
        .ident_size = sizeof (guzzi_camera3_static_tonemap_max_curve_points_identity_t),
        .dim_size_1 = 0,
        .index = GUZZI_CAMERA3_INDEX_STATIC_TONEMAP_MAX_CURVE_POINTS,
    },
};

guzzi_camera3_metadata_helper_t *guzzi_camera3_metadata_controls_index_to_helper(guzzi_camera3_metadata_index_t index)
{
    switch (index) {
        case GUZZI_CAMERA3_INDEX_CONTROLS_BLACK_LEVEL_LOCK:
            return guzzi_camera3_metadata_controls_helper + 0;
        case GUZZI_CAMERA3_INDEX_CONTROLS_COLOR_CORRECTION_GAINS:
            return guzzi_camera3_metadata_controls_helper + 1;
        case GUZZI_CAMERA3_INDEX_CONTROLS_COLOR_CORRECTION_MODE:
            return guzzi_camera3_metadata_controls_helper + 2;
        case GUZZI_CAMERA3_INDEX_CONTROLS_COLOR_CORRECTION_TRANSFORM:
            return guzzi_camera3_metadata_controls_helper + 3;
        case GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AE_ANTIBANDING_MODE:
            return guzzi_camera3_metadata_controls_helper + 4;
        case GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AE_EXPOSURE_COMPENSATION:
            return guzzi_camera3_metadata_controls_helper + 5;
        case GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AE_LOCK:
            return guzzi_camera3_metadata_controls_helper + 6;
        case GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AE_MODE:
            return guzzi_camera3_metadata_controls_helper + 7;
        case GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AE_PRECAPTURE_TRIGGER:
            return guzzi_camera3_metadata_controls_helper + 8;
        case GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AE_REGIONS:
            return guzzi_camera3_metadata_controls_helper + 9;
        case GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AE_TARGET_FPS_RANGE:
            return guzzi_camera3_metadata_controls_helper + 10;
        case GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AF_MODE:
            return guzzi_camera3_metadata_controls_helper + 11;
        case GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AF_REGIONS:
            return guzzi_camera3_metadata_controls_helper + 12;
        case GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AF_TRIGGER:
            return guzzi_camera3_metadata_controls_helper + 13;
        case GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AWB_LOCK:
            return guzzi_camera3_metadata_controls_helper + 14;
        case GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AWB_MODE:
            return guzzi_camera3_metadata_controls_helper + 15;
        case GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AWB_REGIONS:
            return guzzi_camera3_metadata_controls_helper + 16;
        case GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_CAPTURE_INTENT:
            return guzzi_camera3_metadata_controls_helper + 17;
        case GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_EFFECT_MODE:
            return guzzi_camera3_metadata_controls_helper + 18;
        case GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_MODE:
            return guzzi_camera3_metadata_controls_helper + 19;
        case GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_SCENE_MODE:
            return guzzi_camera3_metadata_controls_helper + 20;
        case GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_VIDEO_STABILIZATION_MODE:
            return guzzi_camera3_metadata_controls_helper + 21;
        case GUZZI_CAMERA3_INDEX_CONTROLS_DEMOSAIC_MODE:
            return guzzi_camera3_metadata_controls_helper + 22;
        case GUZZI_CAMERA3_INDEX_CONTROLS_EDGE_MODE:
            return guzzi_camera3_metadata_controls_helper + 23;
        case GUZZI_CAMERA3_INDEX_CONTROLS_EDGE_STRENGTH:
            return guzzi_camera3_metadata_controls_helper + 24;
        case GUZZI_CAMERA3_INDEX_CONTROLS_FLASH_FIRING_POWER:
            return guzzi_camera3_metadata_controls_helper + 25;
        case GUZZI_CAMERA3_INDEX_CONTROLS_FLASH_FIRING_TIME:
            return guzzi_camera3_metadata_controls_helper + 26;
        case GUZZI_CAMERA3_INDEX_CONTROLS_FLASH_MODE:
            return guzzi_camera3_metadata_controls_helper + 27;
        case GUZZI_CAMERA3_INDEX_CONTROLS_GEOMETRIC_MODE:
            return guzzi_camera3_metadata_controls_helper + 28;
        case GUZZI_CAMERA3_INDEX_CONTROLS_GEOMETRIC_STRENGTH:
            return guzzi_camera3_metadata_controls_helper + 29;
        case GUZZI_CAMERA3_INDEX_CONTROLS_HOT_PIXEL_MODE:
            return guzzi_camera3_metadata_controls_helper + 30;
        case GUZZI_CAMERA3_INDEX_CONTROLS_JPEG_GPS_COORDINATES:
            return guzzi_camera3_metadata_controls_helper + 31;
        case GUZZI_CAMERA3_INDEX_CONTROLS_JPEG_GPS_PROCESSING_METHOD:
            return guzzi_camera3_metadata_controls_helper + 32;
        case GUZZI_CAMERA3_INDEX_CONTROLS_JPEG_GPS_TIMESTAMP:
            return guzzi_camera3_metadata_controls_helper + 33;
        case GUZZI_CAMERA3_INDEX_CONTROLS_JPEG_ORIENTATION:
            return guzzi_camera3_metadata_controls_helper + 34;
        case GUZZI_CAMERA3_INDEX_CONTROLS_JPEG_QUALITY:
            return guzzi_camera3_metadata_controls_helper + 35;
        case GUZZI_CAMERA3_INDEX_CONTROLS_JPEG_THUMBNAIL_QUALITY:
            return guzzi_camera3_metadata_controls_helper + 36;
        case GUZZI_CAMERA3_INDEX_CONTROLS_JPEG_THUMBNAIL_SIZE:
            return guzzi_camera3_metadata_controls_helper + 37;
        case GUZZI_CAMERA3_INDEX_CONTROLS_LED_TRANSMIT:
            return guzzi_camera3_metadata_controls_helper + 38;
        case GUZZI_CAMERA3_INDEX_CONTROLS_LENS_APERTURE:
            return guzzi_camera3_metadata_controls_helper + 39;
        case GUZZI_CAMERA3_INDEX_CONTROLS_LENS_FILTER_DENSITY:
            return guzzi_camera3_metadata_controls_helper + 40;
        case GUZZI_CAMERA3_INDEX_CONTROLS_LENS_FOCAL_LENGTH:
            return guzzi_camera3_metadata_controls_helper + 41;
        case GUZZI_CAMERA3_INDEX_CONTROLS_LENS_FOCUS_DISTANCE:
            return guzzi_camera3_metadata_controls_helper + 42;
        case GUZZI_CAMERA3_INDEX_CONTROLS_LENS_OPTICAL_STABILIZATION_MODE:
            return guzzi_camera3_metadata_controls_helper + 43;
        case GUZZI_CAMERA3_INDEX_CONTROLS_NOISE_REDUCTION_MODE:
            return guzzi_camera3_metadata_controls_helper + 44;
        case GUZZI_CAMERA3_INDEX_CONTROLS_NOISE_REDUCTION_STRENGTH:
            return guzzi_camera3_metadata_controls_helper + 45;
        case GUZZI_CAMERA3_INDEX_CONTROLS_REQUEST_FRAME_COUNT:
            return guzzi_camera3_metadata_controls_helper + 46;
        case GUZZI_CAMERA3_INDEX_CONTROLS_REQUEST_ID:
            return guzzi_camera3_metadata_controls_helper + 47;
        case GUZZI_CAMERA3_INDEX_CONTROLS_REQUEST_INPUT_STREAMS:
            return guzzi_camera3_metadata_controls_helper + 48;
        case GUZZI_CAMERA3_INDEX_CONTROLS_REQUEST_METADATA_MODE:
            return guzzi_camera3_metadata_controls_helper + 49;
        case GUZZI_CAMERA3_INDEX_CONTROLS_REQUEST_OUTPUT_STREAMS:
            return guzzi_camera3_metadata_controls_helper + 50;
        case GUZZI_CAMERA3_INDEX_CONTROLS_REQUEST_TYPE:
            return guzzi_camera3_metadata_controls_helper + 51;
        case GUZZI_CAMERA3_INDEX_CONTROLS_SCALER_CROP_REGION:
            return guzzi_camera3_metadata_controls_helper + 52;
        case GUZZI_CAMERA3_INDEX_CONTROLS_SENSOR_EXPOSURE_TIME:
            return guzzi_camera3_metadata_controls_helper + 53;
        case GUZZI_CAMERA3_INDEX_CONTROLS_SENSOR_FRAME_DURATION:
            return guzzi_camera3_metadata_controls_helper + 54;
        case GUZZI_CAMERA3_INDEX_CONTROLS_SENSOR_SENSITIVITY:
            return guzzi_camera3_metadata_controls_helper + 55;
        case GUZZI_CAMERA3_INDEX_CONTROLS_SHADING_MODE:
            return guzzi_camera3_metadata_controls_helper + 56;
        case GUZZI_CAMERA3_INDEX_CONTROLS_SHADING_STRENGTH:
            return guzzi_camera3_metadata_controls_helper + 57;
        case GUZZI_CAMERA3_INDEX_CONTROLS_STATISTICS_FACE_DETECT_MODE:
            return guzzi_camera3_metadata_controls_helper + 58;
        case GUZZI_CAMERA3_INDEX_CONTROLS_STATISTICS_HISTOGRAM_MODE:
            return guzzi_camera3_metadata_controls_helper + 59;
        case GUZZI_CAMERA3_INDEX_CONTROLS_STATISTICS_LENS_SHADING_MAP_MODE:
            return guzzi_camera3_metadata_controls_helper + 60;
        case GUZZI_CAMERA3_INDEX_CONTROLS_STATISTICS_SHARPNESS_MAP_MODE:
            return guzzi_camera3_metadata_controls_helper + 61;
        case GUZZI_CAMERA3_INDEX_CONTROLS_TONEMAP_CURVE_BLUE:
            return guzzi_camera3_metadata_controls_helper + 62;
        case GUZZI_CAMERA3_INDEX_CONTROLS_TONEMAP_CURVE_GREEN:
            return guzzi_camera3_metadata_controls_helper + 63;
        case GUZZI_CAMERA3_INDEX_CONTROLS_TONEMAP_CURVE_RED:
            return guzzi_camera3_metadata_controls_helper + 64;
        case GUZZI_CAMERA3_INDEX_CONTROLS_TONEMAP_MODE:
            return guzzi_camera3_metadata_controls_helper + 65;
        default:
            return (void *)0;
    }
}

guzzi_camera3_metadata_helper_t *guzzi_camera3_metadata_dynamic_index_to_helper(guzzi_camera3_metadata_index_t index)
{
    switch (index) {
        case GUZZI_CAMERA3_INDEX_DYNAMIC_BLACK_LEVEL_LOCK:
            return guzzi_camera3_metadata_dynamic_helper + 0;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_COLOR_CORRECTION_GAINS:
            return guzzi_camera3_metadata_dynamic_helper + 1;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_COLOR_CORRECTION_TRANSFORM:
            return guzzi_camera3_metadata_dynamic_helper + 2;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_CONTROL_AE_PRECAPTURE_ID:
            return guzzi_camera3_metadata_dynamic_helper + 3;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_CONTROL_AE_REGIONS:
            return guzzi_camera3_metadata_dynamic_helper + 4;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_CONTROL_AE_STATE:
            return guzzi_camera3_metadata_dynamic_helper + 5;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_CONTROL_AF_MODE:
            return guzzi_camera3_metadata_dynamic_helper + 6;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_CONTROL_AF_REGIONS:
            return guzzi_camera3_metadata_dynamic_helper + 7;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_CONTROL_AF_STATE:
            return guzzi_camera3_metadata_dynamic_helper + 8;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_CONTROL_AF_TRIGGER_ID:
            return guzzi_camera3_metadata_dynamic_helper + 9;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_CONTROL_AWB_MODE:
            return guzzi_camera3_metadata_dynamic_helper + 10;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_CONTROL_AWB_REGIONS:
            return guzzi_camera3_metadata_dynamic_helper + 11;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_CONTROL_AWB_STATE:
            return guzzi_camera3_metadata_dynamic_helper + 12;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_CONTROL_MODE:
            return guzzi_camera3_metadata_dynamic_helper + 13;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_EDGE_MODE:
            return guzzi_camera3_metadata_dynamic_helper + 14;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_FLASH_FIRING_POWER:
            return guzzi_camera3_metadata_dynamic_helper + 15;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_FLASH_FIRING_TIME:
            return guzzi_camera3_metadata_dynamic_helper + 16;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_FLASH_MODE:
            return guzzi_camera3_metadata_dynamic_helper + 17;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_FLASH_STATE:
            return guzzi_camera3_metadata_dynamic_helper + 18;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_HOT_PIXEL_MODE:
            return guzzi_camera3_metadata_dynamic_helper + 19;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_JPEG_GPS_COORDINATES:
            return guzzi_camera3_metadata_dynamic_helper + 20;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_JPEG_GPS_PROCESSING_METHOD:
            return guzzi_camera3_metadata_dynamic_helper + 21;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_JPEG_GPS_TIMESTAMP:
            return guzzi_camera3_metadata_dynamic_helper + 22;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_JPEG_ORIENTATION:
            return guzzi_camera3_metadata_dynamic_helper + 23;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_JPEG_QUALITY:
            return guzzi_camera3_metadata_dynamic_helper + 24;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_JPEG_SIZE:
            return guzzi_camera3_metadata_dynamic_helper + 25;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_JPEG_THUMBNAIL_QUALITY:
            return guzzi_camera3_metadata_dynamic_helper + 26;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_JPEG_THUMBNAIL_SIZE:
            return guzzi_camera3_metadata_dynamic_helper + 27;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_LED_TRANSMIT:
            return guzzi_camera3_metadata_dynamic_helper + 28;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_LENS_APERTURE:
            return guzzi_camera3_metadata_dynamic_helper + 29;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_LENS_FILTER_DENSITY:
            return guzzi_camera3_metadata_dynamic_helper + 30;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_LENS_FOCAL_LENGTH:
            return guzzi_camera3_metadata_dynamic_helper + 31;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_LENS_FOCUS_DISTANCE:
            return guzzi_camera3_metadata_dynamic_helper + 32;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_LENS_FOCUS_RANGE:
            return guzzi_camera3_metadata_dynamic_helper + 33;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_LENS_OPTICAL_STABILIZATION_MODE:
            return guzzi_camera3_metadata_dynamic_helper + 34;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_LENS_STATE:
            return guzzi_camera3_metadata_dynamic_helper + 35;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_NOISE_REDUCTION_MODE:
            return guzzi_camera3_metadata_dynamic_helper + 36;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_QUIRKS_PARTIAL_RESULT:
            return guzzi_camera3_metadata_dynamic_helper + 37;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_REQUEST_FRAME_COUNT:
            return guzzi_camera3_metadata_dynamic_helper + 38;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_REQUEST_ID:
            return guzzi_camera3_metadata_dynamic_helper + 39;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_REQUEST_METADATA_MODE:
            return guzzi_camera3_metadata_dynamic_helper + 40;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_REQUEST_OUTPUT_STREAMS:
            return guzzi_camera3_metadata_dynamic_helper + 41;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_SCALER_CROP_REGION:
            return guzzi_camera3_metadata_dynamic_helper + 42;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_SENSOR_EXPOSURE_TIME:
            return guzzi_camera3_metadata_dynamic_helper + 43;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_SENSOR_FRAME_DURATION:
            return guzzi_camera3_metadata_dynamic_helper + 44;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_SENSOR_SENSITIVITY:
            return guzzi_camera3_metadata_dynamic_helper + 45;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_SENSOR_TEMPERATURE:
            return guzzi_camera3_metadata_dynamic_helper + 46;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_SENSOR_TIMESTAMP:
            return guzzi_camera3_metadata_dynamic_helper + 47;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_SHADING_MODE:
            return guzzi_camera3_metadata_dynamic_helper + 48;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_STATISTICS_FACE_DETECT_MODE:
            return guzzi_camera3_metadata_dynamic_helper + 49;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_STATISTICS_FACE_IDS:
            return guzzi_camera3_metadata_dynamic_helper + 50;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_STATISTICS_FACE_LANDMARKS:
            return guzzi_camera3_metadata_dynamic_helper + 51;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_STATISTICS_FACE_RECTANGLES:
            return guzzi_camera3_metadata_dynamic_helper + 52;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_STATISTICS_FACE_SCORES:
            return guzzi_camera3_metadata_dynamic_helper + 53;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_STATISTICS_HISTOGRAM:
            return guzzi_camera3_metadata_dynamic_helper + 54;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_STATISTICS_HISTOGRAM_MODE:
            return guzzi_camera3_metadata_dynamic_helper + 55;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_STATISTICS_LENS_SHADING_MAP:
            return guzzi_camera3_metadata_dynamic_helper + 56;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_STATISTICS_PREDICTED_COLOR_GAINS:
            return guzzi_camera3_metadata_dynamic_helper + 57;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_STATISTICS_PREDICTED_COLOR_TRANSFORM:
            return guzzi_camera3_metadata_dynamic_helper + 58;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_STATISTICS_SCENE_FLICKER:
            return guzzi_camera3_metadata_dynamic_helper + 59;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_STATISTICS_SHARPNESS_MAP:
            return guzzi_camera3_metadata_dynamic_helper + 60;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_STATISTICS_SHARPNESS_MAP_MODE:
            return guzzi_camera3_metadata_dynamic_helper + 61;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_TONEMAP_CURVE_BLUE:
            return guzzi_camera3_metadata_dynamic_helper + 62;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_TONEMAP_CURVE_GREEN:
            return guzzi_camera3_metadata_dynamic_helper + 63;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_TONEMAP_CURVE_RED:
            return guzzi_camera3_metadata_dynamic_helper + 64;
        case GUZZI_CAMERA3_INDEX_DYNAMIC_TONEMAP_MODE:
            return guzzi_camera3_metadata_dynamic_helper + 65;
        default:
            return (void *)0;
    }
}

guzzi_camera3_metadata_helper_t *guzzi_camera3_metadata_static_index_to_helper(guzzi_camera3_metadata_index_t index)
{
    switch (index) {
        case GUZZI_CAMERA3_INDEX_STATIC_CONTROL_AE_AVAILABLE_ANTIBANDING_MODES:
            return guzzi_camera3_metadata_static_helper + 0;
        case GUZZI_CAMERA3_INDEX_STATIC_CONTROL_AE_AVAILABLE_MODES:
            return guzzi_camera3_metadata_static_helper + 1;
        case GUZZI_CAMERA3_INDEX_STATIC_CONTROL_AE_AVAILABLE_TARGET_FPS_RANGES:
            return guzzi_camera3_metadata_static_helper + 2;
        case GUZZI_CAMERA3_INDEX_STATIC_CONTROL_AE_COMPENSATION_RANGE:
            return guzzi_camera3_metadata_static_helper + 3;
        case GUZZI_CAMERA3_INDEX_STATIC_CONTROL_AE_COMPENSATION_STEP:
            return guzzi_camera3_metadata_static_helper + 4;
        case GUZZI_CAMERA3_INDEX_STATIC_CONTROL_AF_AVAILABLE_MODES:
            return guzzi_camera3_metadata_static_helper + 5;
        case GUZZI_CAMERA3_INDEX_STATIC_CONTROL_AVAILABLE_EFFECTS:
            return guzzi_camera3_metadata_static_helper + 6;
        case GUZZI_CAMERA3_INDEX_STATIC_CONTROL_AVAILABLE_SCENE_MODES:
            return guzzi_camera3_metadata_static_helper + 7;
        case GUZZI_CAMERA3_INDEX_STATIC_CONTROL_AVAILABLE_VIDEO_STABILIZATION_MODES:
            return guzzi_camera3_metadata_static_helper + 8;
        case GUZZI_CAMERA3_INDEX_STATIC_CONTROL_AWB_AVAILABLE_MODES:
            return guzzi_camera3_metadata_static_helper + 9;
        case GUZZI_CAMERA3_INDEX_STATIC_CONTROL_MAX_REGIONS:
            return guzzi_camera3_metadata_static_helper + 10;
        case GUZZI_CAMERA3_INDEX_STATIC_CONTROL_SCENE_MODE_OVERRIDES:
            return guzzi_camera3_metadata_static_helper + 11;
        case GUZZI_CAMERA3_INDEX_STATIC_FLASH_COLOR_TEMPERATURE:
            return guzzi_camera3_metadata_static_helper + 12;
        case GUZZI_CAMERA3_INDEX_STATIC_FLASH_INFO_AVAILABLE:
            return guzzi_camera3_metadata_static_helper + 13;
        case GUZZI_CAMERA3_INDEX_STATIC_FLASH_INFO_CHARGE_DURATION:
            return guzzi_camera3_metadata_static_helper + 14;
        case GUZZI_CAMERA3_INDEX_STATIC_FLASH_MAX_ENERGY:
            return guzzi_camera3_metadata_static_helper + 15;
        case GUZZI_CAMERA3_INDEX_STATIC_HOT_PIXEL_INFO_MAP:
            return guzzi_camera3_metadata_static_helper + 16;
        case GUZZI_CAMERA3_INDEX_STATIC_INFO_SUPPORTED_HARDWARE_LEVEL:
            return guzzi_camera3_metadata_static_helper + 17;
        case GUZZI_CAMERA3_INDEX_STATIC_JPEG_AVAILABLE_THUMBNAIL_SIZES:
            return guzzi_camera3_metadata_static_helper + 18;
        case GUZZI_CAMERA3_INDEX_STATIC_JPEG_MAX_SIZE:
            return guzzi_camera3_metadata_static_helper + 19;
        case GUZZI_CAMERA3_INDEX_STATIC_LED_AVAILABLE_LEDS:
            return guzzi_camera3_metadata_static_helper + 20;
        case GUZZI_CAMERA3_INDEX_STATIC_LENS_FACING:
            return guzzi_camera3_metadata_static_helper + 21;
        case GUZZI_CAMERA3_INDEX_STATIC_LENS_INFO_AVAILABLE_APERTURES:
            return guzzi_camera3_metadata_static_helper + 22;
        case GUZZI_CAMERA3_INDEX_STATIC_LENS_INFO_AVAILABLE_FILTER_DENSITIES:
            return guzzi_camera3_metadata_static_helper + 23;
        case GUZZI_CAMERA3_INDEX_STATIC_LENS_INFO_AVAILABLE_FOCAL_LENGTHS:
            return guzzi_camera3_metadata_static_helper + 24;
        case GUZZI_CAMERA3_INDEX_STATIC_LENS_INFO_AVAILABLE_OPTICAL_STABILIZATION:
            return guzzi_camera3_metadata_static_helper + 25;
        case GUZZI_CAMERA3_INDEX_STATIC_LENS_INFO_GEOMETRIC_CORRECTION_MAP:
            return guzzi_camera3_metadata_static_helper + 26;
        case GUZZI_CAMERA3_INDEX_STATIC_LENS_INFO_GEOMETRIC_CORRECTION_MAP_SIZE:
            return guzzi_camera3_metadata_static_helper + 27;
        case GUZZI_CAMERA3_INDEX_STATIC_LENS_INFO_HYPERFOCAL_DISTANCE:
            return guzzi_camera3_metadata_static_helper + 28;
        case GUZZI_CAMERA3_INDEX_STATIC_LENS_INFO_MINIMUM_FOCUS_DISTANCE:
            return guzzi_camera3_metadata_static_helper + 29;
        case GUZZI_CAMERA3_INDEX_STATIC_LENS_INFO_SHADING_MAP_SIZE:
            return guzzi_camera3_metadata_static_helper + 30;
        case GUZZI_CAMERA3_INDEX_STATIC_LENS_OPTICAL_AXIS_ANGLE:
            return guzzi_camera3_metadata_static_helper + 31;
        case GUZZI_CAMERA3_INDEX_STATIC_LENS_POSITION:
            return guzzi_camera3_metadata_static_helper + 32;
        case GUZZI_CAMERA3_INDEX_STATIC_QUIRKS_METERING_CROP_REGION:
            return guzzi_camera3_metadata_static_helper + 33;
        case GUZZI_CAMERA3_INDEX_STATIC_QUIRKS_TRIGGER_AF_WITH_AUTO:
            return guzzi_camera3_metadata_static_helper + 34;
        case GUZZI_CAMERA3_INDEX_STATIC_QUIRKS_USE_PARTIAL_RESULT:
            return guzzi_camera3_metadata_static_helper + 35;
        case GUZZI_CAMERA3_INDEX_STATIC_QUIRKS_USE_ZSL_FORMAT:
            return guzzi_camera3_metadata_static_helper + 36;
        case GUZZI_CAMERA3_INDEX_STATIC_REQUEST_MAX_NUM_OUTPUT_STREAMS:
            return guzzi_camera3_metadata_static_helper + 37;
        case GUZZI_CAMERA3_INDEX_STATIC_REQUEST_MAX_NUM_REPROCESS_STREAMS:
            return guzzi_camera3_metadata_static_helper + 38;
        case GUZZI_CAMERA3_INDEX_STATIC_SCALER_AVAILABLE_FORMATS:
            return guzzi_camera3_metadata_static_helper + 39;
        case GUZZI_CAMERA3_INDEX_STATIC_SCALER_AVAILABLE_JPEG_MIN_DURATIONS:
            return guzzi_camera3_metadata_static_helper + 40;
        case GUZZI_CAMERA3_INDEX_STATIC_SCALER_AVAILABLE_JPEG_SIZES:
            return guzzi_camera3_metadata_static_helper + 41;
        case GUZZI_CAMERA3_INDEX_STATIC_SCALER_AVAILABLE_MAX_DIGITAL_ZOOM:
            return guzzi_camera3_metadata_static_helper + 42;
        case GUZZI_CAMERA3_INDEX_STATIC_SCALER_AVAILABLE_PROCESSED_MIN_DURATIONS:
            return guzzi_camera3_metadata_static_helper + 43;
        case GUZZI_CAMERA3_INDEX_STATIC_SCALER_AVAILABLE_PROCESSED_SIZES:
            return guzzi_camera3_metadata_static_helper + 44;
        case GUZZI_CAMERA3_INDEX_STATIC_SCALER_AVAILABLE_RAW_MIN_DURATIONS:
            return guzzi_camera3_metadata_static_helper + 45;
        case GUZZI_CAMERA3_INDEX_STATIC_SCALER_AVAILABLE_RAW_SIZES:
            return guzzi_camera3_metadata_static_helper + 46;
        case GUZZI_CAMERA3_INDEX_STATIC_SENSOR_BASE_GAIN_FACTOR:
            return guzzi_camera3_metadata_static_helper + 47;
        case GUZZI_CAMERA3_INDEX_STATIC_SENSOR_BLACK_LEVEL_PATTERN:
            return guzzi_camera3_metadata_static_helper + 48;
        case GUZZI_CAMERA3_INDEX_STATIC_SENSOR_CALIBRATION_TRANSFORM1:
            return guzzi_camera3_metadata_static_helper + 49;
        case GUZZI_CAMERA3_INDEX_STATIC_SENSOR_CALIBRATION_TRANSFORM2:
            return guzzi_camera3_metadata_static_helper + 50;
        case GUZZI_CAMERA3_INDEX_STATIC_SENSOR_COLOR_TRANSFORM1:
            return guzzi_camera3_metadata_static_helper + 51;
        case GUZZI_CAMERA3_INDEX_STATIC_SENSOR_COLOR_TRANSFORM2:
            return guzzi_camera3_metadata_static_helper + 52;
        case GUZZI_CAMERA3_INDEX_STATIC_SENSOR_FORWARD_MATRIX1:
            return guzzi_camera3_metadata_static_helper + 53;
        case GUZZI_CAMERA3_INDEX_STATIC_SENSOR_FORWARD_MATRIX2:
            return guzzi_camera3_metadata_static_helper + 54;
        case GUZZI_CAMERA3_INDEX_STATIC_SENSOR_INFO_ACTIVE_ARRAY_SIZE:
            return guzzi_camera3_metadata_static_helper + 55;
        case GUZZI_CAMERA3_INDEX_STATIC_SENSOR_INFO_COLOR_FILTER_ARRANGEMENT:
            return guzzi_camera3_metadata_static_helper + 56;
        case GUZZI_CAMERA3_INDEX_STATIC_SENSOR_INFO_EXPOSURE_TIME_RANGE:
            return guzzi_camera3_metadata_static_helper + 57;
        case GUZZI_CAMERA3_INDEX_STATIC_SENSOR_INFO_MAX_FRAME_DURATION:
            return guzzi_camera3_metadata_static_helper + 58;
        case GUZZI_CAMERA3_INDEX_STATIC_SENSOR_INFO_PHYSICAL_SIZE:
            return guzzi_camera3_metadata_static_helper + 59;
        case GUZZI_CAMERA3_INDEX_STATIC_SENSOR_INFO_PIXEL_ARRAY_SIZE:
            return guzzi_camera3_metadata_static_helper + 60;
        case GUZZI_CAMERA3_INDEX_STATIC_SENSOR_INFO_SENSITIVITY_RANGE:
            return guzzi_camera3_metadata_static_helper + 61;
        case GUZZI_CAMERA3_INDEX_STATIC_SENSOR_INFO_WHITE_LEVEL:
            return guzzi_camera3_metadata_static_helper + 62;
        case GUZZI_CAMERA3_INDEX_STATIC_SENSOR_MAX_ANALOG_SENSITIVITY:
            return guzzi_camera3_metadata_static_helper + 63;
        case GUZZI_CAMERA3_INDEX_STATIC_SENSOR_NOISE_MODEL_COEFFICIENTS:
            return guzzi_camera3_metadata_static_helper + 64;
        case GUZZI_CAMERA3_INDEX_STATIC_SENSOR_ORIENTATION:
            return guzzi_camera3_metadata_static_helper + 65;
        case GUZZI_CAMERA3_INDEX_STATIC_SENSOR_REFERENCE_ILLUMINANT1:
            return guzzi_camera3_metadata_static_helper + 66;
        case GUZZI_CAMERA3_INDEX_STATIC_SENSOR_REFERENCE_ILLUMINANT2:
            return guzzi_camera3_metadata_static_helper + 67;
        case GUZZI_CAMERA3_INDEX_STATIC_STATISTICS_INFO_AVAILABLE_FACE_DETECT_MODES:
            return guzzi_camera3_metadata_static_helper + 68;
        case GUZZI_CAMERA3_INDEX_STATIC_STATISTICS_INFO_HISTOGRAM_BUCKET_COUNT:
            return guzzi_camera3_metadata_static_helper + 69;
        case GUZZI_CAMERA3_INDEX_STATIC_STATISTICS_INFO_MAX_FACE_COUNT:
            return guzzi_camera3_metadata_static_helper + 70;
        case GUZZI_CAMERA3_INDEX_STATIC_STATISTICS_INFO_MAX_HISTOGRAM_COUNT:
            return guzzi_camera3_metadata_static_helper + 71;
        case GUZZI_CAMERA3_INDEX_STATIC_STATISTICS_INFO_MAX_SHARPNESS_MAP_VALUE:
            return guzzi_camera3_metadata_static_helper + 72;
        case GUZZI_CAMERA3_INDEX_STATIC_STATISTICS_INFO_SHARPNESS_MAP_SIZE:
            return guzzi_camera3_metadata_static_helper + 73;
        case GUZZI_CAMERA3_INDEX_STATIC_TONEMAP_MAX_CURVE_POINTS:
            return guzzi_camera3_metadata_static_helper + 74;
        default:
            return (void *)0;
    }
}

guzzi_camera3_metadata_helper_t *guzzi_camera3_metadata_controls_next_helper(guzzi_camera3_metadata_helper_t *helper)
{
    if (!helper) {
        return guzzi_camera3_metadata_controls_helper;
    }

    /* TODO: assert(guzzi_camera3_metadata_controls_helper <= helper); */
    if ((helper + 1) < (guzzi_camera3_metadata_controls_helper + sizeof (guzzi_camera3_metadata_controls_helper) / sizeof (guzzi_camera3_metadata_helper_t))) {
        return helper + 1;
    } else {
        return (void *)0;
    }
}

guzzi_camera3_metadata_helper_t *guzzi_camera3_metadata_dynamic_next_helper(guzzi_camera3_metadata_helper_t *helper)
{
    if (!helper) {
        return guzzi_camera3_metadata_dynamic_helper;
    }

    /* TODO: assert(guzzi_camera3_metadata_dynamic_helper <= helper); */
    if ((helper + 1) < (guzzi_camera3_metadata_dynamic_helper + sizeof (guzzi_camera3_metadata_dynamic_helper) / sizeof (guzzi_camera3_metadata_helper_t))) {
        return helper + 1;
    } else {
        return (void *)0;
    }
}

guzzi_camera3_metadata_helper_t *guzzi_camera3_metadata_static_next_helper(guzzi_camera3_metadata_helper_t *helper)
{
    if (!helper) {
        return guzzi_camera3_metadata_static_helper;
    }

    /* TODO: assert(guzzi_camera3_metadata_static_helper <= helper); */
    if ((helper + 1) < (guzzi_camera3_metadata_static_helper + sizeof (guzzi_camera3_metadata_static_helper) / sizeof (guzzi_camera3_metadata_helper_t))) {
        return helper + 1;
    } else {
        return (void *)0;
    }
}

