/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file convert__do_not_compile.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

int camera_external_convert_black_level_lock(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_color_correction_gains(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_color_correction_mode(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_color_correction_transform(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_control_ae_antibanding_mode(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_control_ae_exposure_compensation(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_control_ae_lock(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_control_ae_mode(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_control_ae_precapture_trigger(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_control_ae_regions(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_control_ae_target_fps_range(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_control_af_mode(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_control_af_regions(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_control_af_trigger(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_control_awb_lock(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_control_awb_mode(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_control_awb_regions(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_control_capture_intent(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_control_effect_mode(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_control_mode(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_control_scene_mode(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_control_video_stabilization_mode(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_demosaic_mode(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_edge_mode(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_edge_strength(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_flash_firing_power(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_flash_firing_time(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_flash_mode(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_geometric_mode(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_geometric_strength(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_hot_pixel_mode(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_jpeg_gps_coordinates(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_jpeg_gps_processing_method(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_jpeg_gps_timestamp(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_jpeg_orientation(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_jpeg_quality(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_jpeg_thumbnail_quality(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_jpeg_thumbnail_size(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_led_transmit(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_lens_aperture(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_lens_filter_density(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_lens_focal_length(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_lens_focus_distance(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_lens_optical_stabilization_mode(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_noise_reduction_mode(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_noise_reduction_strength(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_request_frame_count(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_request_id(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_request_input_streams(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_request_metadata_mode(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_request_output_streams(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_request_type(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_scaler_crop_region(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_sensor_exposure_time(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_sensor_frame_duration(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_sensor_sensitivity(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_shading_mode(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_shading_strength(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_statistics_face_detect_mode(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_statistics_histogram_mode(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_statistics_lens_shading_map_mode(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_statistics_sharpness_map_mode(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_tonemap_curve_blue(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_tonemap_curve_green(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_tonemap_curve_red(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_tonemap_mode(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

/*
 * ****************************************************************************
 * ** Guzzi specific **********************************************************
 * ****************************************************************************
 */
int camera_external_convert_capture_request_active(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_capture_request_frame_number(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_capture_request_guzzi_fr_id(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_stream_config_format(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_stream_config_height(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_stream_config_id(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_stream_config_type(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_stream_config_width(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_z_custom_capture_number_shots(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_z_custom_capture_mode_selection(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_z_custom_crtl_brigthness(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_z_custom_crtl_contrast(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_z_custom_crtl_hue(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_z_custom_crtl_saturation(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_z_custom_exposure_bracketing_sequence(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_z_custom_usecase_selection(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_z_exposure_merger_weight(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_z_wb_manual_temperature(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

int camera_external_convert_z_white_balance_merger_weight(
            configurator_t *c,
            void *plugin_prv,
            void *config
        )
{
    mmsdbg(DL_WARNING, "Function \"%s()\" not implemented!", __FUNCTION__);
    return 0;
}

