/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file helpers.c
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#include <osal/osal_stdio.h>

#include <guzzi/camera3/metadata_index.h>
#include <guzzi/camera3/metadata_struct.h>
#include <guzzi/camera_external/struct.h>
#include <guzzi/camera_external/struct_helpers.h>

#ifndef STR
#define STR(X) #X
#endif

#ifndef offset_of
#define offset_of(type, field) ((char *)&(((type *)0)->field) - (char *)0)
#endif

typedef struct configurator configurator_t;

int camera_external_convert_black_level_lock(
            configurator_t *c,
            void *plugin_prv,
            void *config
        );
int camera_external_convert_color_correction_gains(
            configurator_t *c,
            void *plugin_prv,
            void *config
        );
int camera_external_convert_color_correction_mode(
            configurator_t *c,
            void *plugin_prv,
            void *config
        );
int camera_external_convert_color_correction_transform(
            configurator_t *c,
            void *plugin_prv,
            void *config
        );
int camera_external_convert_control_ae_antibanding_mode(
            configurator_t *c,
            void *plugin_prv,
            void *config
        );
int camera_external_convert_control_ae_exposure_compensation(
            configurator_t *c,
            void *plugin_prv,
            void *config
        );
int camera_external_convert_control_ae_lock(
            configurator_t *c,
            void *plugin_prv,
            void *config
        );
int camera_external_convert_control_ae_mode(
            configurator_t *c,
            void *plugin_prv,
            void *config
        );
int camera_external_convert_control_ae_precapture_trigger(
            configurator_t *c,
            void *plugin_prv,
            void *config
        );
int camera_external_convert_control_ae_regions(
            configurator_t *c,
            void *plugin_prv,
            void *config
        );
int camera_external_convert_control_ae_target_fps_range(
            configurator_t *c,
            void *plugin_prv,
            void *config
        );
int camera_external_convert_control_af_mode(
            configurator_t *c,
            void *plugin_prv,
            void *config
        );
int camera_external_convert_control_af_regions(
            configurator_t *c,
            void *plugin_prv,
            void *config
        );
int camera_external_convert_control_af_trigger(
            configurator_t *c,
            void *plugin_prv,
            void *config
        );
int camera_external_convert_control_awb_lock(
            configurator_t *c,
            void *plugin_prv,
            void *config
        );
int camera_external_convert_control_awb_mode(
            configurator_t *c,
            void *plugin_prv,
            void *config
        );
int camera_external_convert_control_awb_regions(
            configurator_t *c,
            void *plugin_prv,
            void *config
        );
int camera_external_convert_control_capture_intent(
            configurator_t *c,
            void *plugin_prv,
            void *config
        );
int camera_external_convert_control_effect_mode(
            configurator_t *c,
            void *plugin_prv,
            void *config
        );
int camera_external_convert_control_mode(
            configurator_t *c,
            void *plugin_prv,
            void *config
        );
int camera_external_convert_control_scene_mode(
            configurator_t *c,
            void *plugin_prv,
            void *config
        );
int camera_external_convert_control_video_stabilization_mode(
            configurator_t *c,
            void *plugin_prv,
            void *config
        );
int camera_external_convert_demosaic_mode(
            configurator_t *c,
            void *plugin_prv,
            void *config
        );
int camera_external_convert_edge_mode(
            configurator_t *c,
            void *plugin_prv,
            void *config
        );
int camera_external_convert_edge_strength(
            configurator_t *c,
            void *plugin_prv,
            void *config
        );
int camera_external_convert_flash_firing_power(
            configurator_t *c,
            void *plugin_prv,
            void *config
        );
int camera_external_convert_flash_firing_time(
            configurator_t *c,
            void *plugin_prv,
            void *config
        );
int camera_external_convert_flash_mode(
            configurator_t *c,
            void *plugin_prv,
            void *config
        );
int camera_external_convert_geometric_mode(
            configurator_t *c,
            void *plugin_prv,
            void *config
        );
int camera_external_convert_geometric_strength(
            configurator_t *c,
            void *plugin_prv,
            void *config
        );
int camera_external_convert_hot_pixel_mode(
            configurator_t *c,
            void *plugin_prv,
            void *config
        );
int camera_external_convert_jpeg_gps_coordinates(
            configurator_t *c,
            void *plugin_prv,
            void *config
        );
int camera_external_convert_jpeg_gps_processing_method(
            configurator_t *c,
            void *plugin_prv,
            void *config
        );
int camera_external_convert_jpeg_gps_timestamp(
            configurator_t *c,
            void *plugin_prv,
            void *config
        );
int camera_external_convert_jpeg_orientation(
            configurator_t *c,
            void *plugin_prv,
            void *config
        );
int camera_external_convert_jpeg_quality(
            configurator_t *c,
            void *plugin_prv,
            void *config
        );
int camera_external_convert_jpeg_thumbnail_quality(
            configurator_t *c,
            void *plugin_prv,
            void *config
        );
int camera_external_convert_jpeg_thumbnail_size(
            configurator_t *c,
            void *plugin_prv,
            void *config
        );
int camera_external_convert_led_transmit(
            configurator_t *c,
            void *plugin_prv,
            void *config
        );
int camera_external_convert_lens_aperture(
            configurator_t *c,
            void *plugin_prv,
            void *config
        );
int camera_external_convert_lens_filter_density(
            configurator_t *c,
            void *plugin_prv,
            void *config
        );
int camera_external_convert_lens_focal_length(
            configurator_t *c,
            void *plugin_prv,
            void *config
        );
int camera_external_convert_lens_focus_distance(
            configurator_t *c,
            void *plugin_prv,
            void *config
        );
int camera_external_convert_lens_optical_stabilization_mode(
            configurator_t *c,
            void *plugin_prv,
            void *config
        );
int camera_external_convert_noise_reduction_mode(
            configurator_t *c,
            void *plugin_prv,
            void *config
        );
int camera_external_convert_noise_reduction_strength(
            configurator_t *c,
            void *plugin_prv,
            void *config
        );
int camera_external_convert_request_frame_count(
            configurator_t *c,
            void *plugin_prv,
            void *config
        );
int camera_external_convert_request_id(
            configurator_t *c,
            void *plugin_prv,
            void *config
        );
int camera_external_convert_request_input_streams(
            configurator_t *c,
            void *plugin_prv,
            void *config
        );
int camera_external_convert_request_metadata_mode(
            configurator_t *c,
            void *plugin_prv,
            void *config
        );
int camera_external_convert_request_output_streams(
            configurator_t *c,
            void *plugin_prv,
            void *config
        );
int camera_external_convert_request_type(
            configurator_t *c,
            void *plugin_prv,
            void *config
        );
int camera_external_convert_scaler_crop_region(
            configurator_t *c,
            void *plugin_prv,
            void *config
        );
int camera_external_convert_sensor_exposure_time(
            configurator_t *c,
            void *plugin_prv,
            void *config
        );
int camera_external_convert_sensor_frame_duration(
            configurator_t *c,
            void *plugin_prv,
            void *config
        );
int camera_external_convert_sensor_sensitivity(
            configurator_t *c,
            void *plugin_prv,
            void *config
        );
int camera_external_convert_shading_mode(
            configurator_t *c,
            void *plugin_prv,
            void *config
        );
int camera_external_convert_shading_strength(
            configurator_t *c,
            void *plugin_prv,
            void *config
        );
int camera_external_convert_statistics_face_detect_mode(
            configurator_t *c,
            void *plugin_prv,
            void *config
        );
int camera_external_convert_statistics_histogram_mode(
            configurator_t *c,
            void *plugin_prv,
            void *config
        );
int camera_external_convert_statistics_lens_shading_map_mode(
            configurator_t *c,
            void *plugin_prv,
            void *config
        );
int camera_external_convert_statistics_sharpness_map_mode(
            configurator_t *c,
            void *plugin_prv,
            void *config
        );
int camera_external_convert_tonemap_curve_blue(
            configurator_t *c,
            void *plugin_prv,
            void *config
        );
int camera_external_convert_tonemap_curve_green(
            configurator_t *c,
            void *plugin_prv,
            void *config
        );
int camera_external_convert_tonemap_curve_red(
            configurator_t *c,
            void *plugin_prv,
            void *config
        );
int camera_external_convert_tonemap_mode(
            configurator_t *c,
            void *plugin_prv,
            void *config
        );

/* Guzzi specific */
int camera_external_convert_capture_request_active(
            configurator_t *c,
            void *plugin_prv,
            void *config
        );
int camera_external_convert_capture_request_frame_number(
            configurator_t *c,
            void *plugin_prv,
            void *config
        );
int camera_external_convert_capture_request_guzzi_fr_id(
            configurator_t *c,
            void *plugin_prv,
            void *config
        );
int camera_external_convert_stream_config_format(
            configurator_t *c,
            void *plugin_prv,
            void *config
        );
int camera_external_convert_stream_config_height(
            configurator_t *c,
            void *plugin_prv,
            void *config
        );
int camera_external_convert_stream_config_id(
            configurator_t *c,
            void *plugin_prv,
            void *config
        );
int camera_external_convert_stream_config_type(
            configurator_t *c,
            void *plugin_prv,
            void *config
        );
int camera_external_convert_stream_config_width(
            configurator_t *c,
            void *plugin_prv,
            void *config
        );
int camera_external_convert_z_custom_capture_number_shots(
            configurator_t *c,
            void *plugin_prv,
            void *config
        );
int camera_external_convert_z_custom_capture_mode_selection(
            configurator_t *c,
            void *plugin_prv,
            void *config
        );
int camera_external_convert_z_custom_crtl_brigthness(
            configurator_t *c,
            void *plugin_prv,
            void *config
        );
int camera_external_convert_z_custom_crtl_contrast(
            configurator_t *c,
            void *plugin_prv,
            void *config
        );
int camera_external_convert_z_custom_crtl_hue(
            configurator_t *c,
            void *plugin_prv,
            void *config
        );
int camera_external_convert_z_custom_crtl_saturation(
            configurator_t *c,
            void *plugin_prv,
            void *config
        );
int camera_external_convert_z_custom_exposure_bracketing_sequence(
            configurator_t *c,
            void *plugin_prv,
            void *config
        );
int camera_external_convert_z_custom_usecase_selection(
            configurator_t *c,
            void *plugin_prv,
            void *config
        );
int camera_external_convert_z_exposure_merger_weight(
            configurator_t *c,
            void *plugin_prv,
            void *config
        );
int camera_external_convert_z_wb_manual_temperature(
            configurator_t *c,
            void *plugin_prv,
            void *config
        );
int camera_external_convert_z_white_balance_merger_weight(
            configurator_t *c,
            void *plugin_prv,
            void *config
        );

static camera_external_struct_helper_t camera_external_struct_helper[] = {
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_BLACK_LEVEL_LOCK),
        .struct_name = STR(guzzi_camera3_controls_black_level_lock_t),
        .offset = offset_of(camera_external_struct_t, black_level_lock),
        .size = sizeof (guzzi_camera3_controls_black_level_lock_t),
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_BLACK_LEVEL_LOCK,
        .convert = camera_external_convert_black_level_lock
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_COLOR_CORRECTION_GAINS),
        .struct_name = STR(guzzi_camera3_controls_color_correction_gains_t),
        .offset = offset_of(camera_external_struct_t, color_correction_gains),
        .size = sizeof (guzzi_camera3_controls_color_correction_gains_t),
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_COLOR_CORRECTION_GAINS,
        .convert = camera_external_convert_color_correction_gains
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_COLOR_CORRECTION_MODE),
        .struct_name = STR(guzzi_camera3_controls_color_correction_mode_t),
        .offset = offset_of(camera_external_struct_t, color_correction_mode),
        .size = sizeof (guzzi_camera3_controls_color_correction_mode_t),
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_COLOR_CORRECTION_MODE,
        .convert = camera_external_convert_color_correction_mode
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_COLOR_CORRECTION_TRANSFORM),
        .struct_name = STR(guzzi_camera3_controls_color_correction_transform_t),
        .offset = offset_of(camera_external_struct_t, color_correction_transform),
        .size = sizeof (guzzi_camera3_controls_color_correction_transform_t),
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_COLOR_CORRECTION_TRANSFORM,
        .convert = camera_external_convert_color_correction_transform
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AE_ANTIBANDING_MODE),
        .struct_name = STR(guzzi_camera3_controls_control_ae_antibanding_mode_t),
        .offset = offset_of(camera_external_struct_t, control_ae_antibanding_mode),
        .size = sizeof (guzzi_camera3_controls_control_ae_antibanding_mode_t),
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AE_ANTIBANDING_MODE,
        .convert = camera_external_convert_control_ae_antibanding_mode
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AE_EXPOSURE_COMPENSATION),
        .struct_name = STR(guzzi_camera3_controls_control_ae_exposure_compensation_t),
        .offset = offset_of(camera_external_struct_t, control_ae_exposure_compensation),
        .size = sizeof (guzzi_camera3_controls_control_ae_exposure_compensation_t),
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AE_EXPOSURE_COMPENSATION,
        .convert = camera_external_convert_control_ae_exposure_compensation
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AE_LOCK),
        .struct_name = STR(guzzi_camera3_controls_control_ae_lock_t),
        .offset = offset_of(camera_external_struct_t, control_ae_lock),
        .size = sizeof (guzzi_camera3_controls_control_ae_lock_t),
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AE_LOCK,
        .convert = camera_external_convert_control_ae_lock
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AE_MODE),
        .struct_name = STR(guzzi_camera3_controls_control_ae_mode_t),
        .offset = offset_of(camera_external_struct_t, control_ae_mode),
        .size = sizeof (guzzi_camera3_controls_control_ae_mode_t),
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AE_MODE,
        .convert = camera_external_convert_control_ae_mode
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AE_PRECAPTURE_TRIGGER),
        .struct_name = STR(guzzi_camera3_controls_control_ae_precapture_trigger_t),
        .offset = offset_of(camera_external_struct_t, control_ae_precapture_trigger),
        .size = sizeof (guzzi_camera3_controls_control_ae_precapture_trigger_t),
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AE_PRECAPTURE_TRIGGER,
        .convert = camera_external_convert_control_ae_precapture_trigger
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AE_REGIONS),
        .struct_name = STR(guzzi_camera3_controls_control_ae_regions_t),
        .offset = offset_of(camera_external_struct_t, control_ae_regions),
        .size = sizeof (guzzi_camera3_controls_control_ae_regions_t),
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AE_REGIONS,
        .convert = camera_external_convert_control_ae_regions
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AE_TARGET_FPS_RANGE),
        .struct_name = STR(guzzi_camera3_controls_control_ae_target_fps_range_t),
        .offset = offset_of(camera_external_struct_t, control_ae_target_fps_range),
        .size = sizeof (guzzi_camera3_controls_control_ae_target_fps_range_t),
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AE_TARGET_FPS_RANGE,
        .convert = camera_external_convert_control_ae_target_fps_range
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AF_MODE),
        .struct_name = STR(guzzi_camera3_controls_control_af_mode_t),
        .offset = offset_of(camera_external_struct_t, control_af_mode),
        .size = sizeof (guzzi_camera3_controls_control_af_mode_t),
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AF_MODE,
        .convert = camera_external_convert_control_af_mode
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AF_REGIONS),
        .struct_name = STR(guzzi_camera3_controls_control_af_regions_t),
        .offset = offset_of(camera_external_struct_t, control_af_regions),
        .size = sizeof (guzzi_camera3_controls_control_af_regions_t),
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AF_REGIONS,
        .convert = camera_external_convert_control_af_regions
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AF_TRIGGER),
        .struct_name = STR(guzzi_camera3_controls_control_af_trigger_t),
        .offset = offset_of(camera_external_struct_t, control_af_trigger),
        .size = sizeof (guzzi_camera3_controls_control_af_trigger_t),
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AF_TRIGGER,
        .convert = camera_external_convert_control_af_trigger
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AWB_LOCK),
        .struct_name = STR(guzzi_camera3_controls_control_awb_lock_t),
        .offset = offset_of(camera_external_struct_t, control_awb_lock),
        .size = sizeof (guzzi_camera3_controls_control_awb_lock_t),
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AWB_LOCK,
        .convert = camera_external_convert_control_awb_lock
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AWB_MODE),
        .struct_name = STR(guzzi_camera3_controls_control_awb_mode_t),
        .offset = offset_of(camera_external_struct_t, control_awb_mode),
        .size = sizeof (guzzi_camera3_controls_control_awb_mode_t),
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AWB_MODE,
        .convert = camera_external_convert_control_awb_mode
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AWB_REGIONS),
        .struct_name = STR(guzzi_camera3_controls_control_awb_regions_t),
        .offset = offset_of(camera_external_struct_t, control_awb_regions),
        .size = sizeof (guzzi_camera3_controls_control_awb_regions_t),
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AWB_REGIONS,
        .convert = camera_external_convert_control_awb_regions
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_CAPTURE_INTENT),
        .struct_name = STR(guzzi_camera3_controls_control_capture_intent_t),
        .offset = offset_of(camera_external_struct_t, control_capture_intent),
        .size = sizeof (guzzi_camera3_controls_control_capture_intent_t),
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_CAPTURE_INTENT,
        .convert = camera_external_convert_control_capture_intent
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_EFFECT_MODE),
        .struct_name = STR(guzzi_camera3_controls_control_effect_mode_t),
        .offset = offset_of(camera_external_struct_t, control_effect_mode),
        .size = sizeof (guzzi_camera3_controls_control_effect_mode_t),
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_EFFECT_MODE,
        .convert = camera_external_convert_control_effect_mode
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_MODE),
        .struct_name = STR(guzzi_camera3_controls_control_mode_t),
        .offset = offset_of(camera_external_struct_t, control_mode),
        .size = sizeof (guzzi_camera3_controls_control_mode_t),
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_MODE,
        .convert = camera_external_convert_control_mode
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_SCENE_MODE),
        .struct_name = STR(guzzi_camera3_controls_control_scene_mode_t),
        .offset = offset_of(camera_external_struct_t, control_scene_mode),
        .size = sizeof (guzzi_camera3_controls_control_scene_mode_t),
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_SCENE_MODE,
        .convert = camera_external_convert_control_scene_mode
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_VIDEO_STABILIZATION_MODE),
        .struct_name = STR(guzzi_camera3_controls_control_video_stabilization_mode_t),
        .offset = offset_of(camera_external_struct_t, control_video_stabilization_mode),
        .size = sizeof (guzzi_camera3_controls_control_video_stabilization_mode_t),
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_VIDEO_STABILIZATION_MODE,
        .convert = camera_external_convert_control_video_stabilization_mode
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_DEMOSAIC_MODE),
        .struct_name = STR(guzzi_camera3_controls_demosaic_mode_t),
        .offset = offset_of(camera_external_struct_t, demosaic_mode),
        .size = sizeof (guzzi_camera3_controls_demosaic_mode_t),
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_DEMOSAIC_MODE,
        .convert = camera_external_convert_demosaic_mode
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_EDGE_MODE),
        .struct_name = STR(guzzi_camera3_controls_edge_mode_t),
        .offset = offset_of(camera_external_struct_t, edge_mode),
        .size = sizeof (guzzi_camera3_controls_edge_mode_t),
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_EDGE_MODE,
        .convert = camera_external_convert_edge_mode
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_EDGE_STRENGTH),
        .struct_name = STR(guzzi_camera3_controls_edge_strength_t),
        .offset = offset_of(camera_external_struct_t, edge_strength),
        .size = sizeof (guzzi_camera3_controls_edge_strength_t),
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_EDGE_STRENGTH,
        .convert = camera_external_convert_edge_strength
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_FLASH_FIRING_POWER),
        .struct_name = STR(guzzi_camera3_controls_flash_firing_power_t),
        .offset = offset_of(camera_external_struct_t, flash_firing_power),
        .size = sizeof (guzzi_camera3_controls_flash_firing_power_t),
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_FLASH_FIRING_POWER,
        .convert = camera_external_convert_flash_firing_power
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_FLASH_FIRING_TIME),
        .struct_name = STR(guzzi_camera3_controls_flash_firing_time_t),
        .offset = offset_of(camera_external_struct_t, flash_firing_time),
        .size = sizeof (guzzi_camera3_controls_flash_firing_time_t),
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_FLASH_FIRING_TIME,
        .convert = camera_external_convert_flash_firing_time
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_FLASH_MODE),
        .struct_name = STR(guzzi_camera3_controls_flash_mode_t),
        .offset = offset_of(camera_external_struct_t, flash_mode),
        .size = sizeof (guzzi_camera3_controls_flash_mode_t),
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_FLASH_MODE,
        .convert = camera_external_convert_flash_mode
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_GEOMETRIC_MODE),
        .struct_name = STR(guzzi_camera3_controls_geometric_mode_t),
        .offset = offset_of(camera_external_struct_t, geometric_mode),
        .size = sizeof (guzzi_camera3_controls_geometric_mode_t),
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_GEOMETRIC_MODE,
        .convert = camera_external_convert_geometric_mode
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_GEOMETRIC_STRENGTH),
        .struct_name = STR(guzzi_camera3_controls_geometric_strength_t),
        .offset = offset_of(camera_external_struct_t, geometric_strength),
        .size = sizeof (guzzi_camera3_controls_geometric_strength_t),
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_GEOMETRIC_STRENGTH,
        .convert = camera_external_convert_geometric_strength
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_HOT_PIXEL_MODE),
        .struct_name = STR(guzzi_camera3_controls_hot_pixel_mode_t),
        .offset = offset_of(camera_external_struct_t, hot_pixel_mode),
        .size = sizeof (guzzi_camera3_controls_hot_pixel_mode_t),
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_HOT_PIXEL_MODE,
        .convert = camera_external_convert_hot_pixel_mode
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_JPEG_GPS_COORDINATES),
        .struct_name = STR(guzzi_camera3_controls_jpeg_gps_coordinates_t),
        .offset = offset_of(camera_external_struct_t, jpeg_gps_coordinates),
        .size = sizeof (guzzi_camera3_controls_jpeg_gps_coordinates_t),
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_JPEG_GPS_COORDINATES,
        .convert = camera_external_convert_jpeg_gps_coordinates
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_JPEG_GPS_PROCESSING_METHOD),
        .struct_name = STR(guzzi_camera3_controls_jpeg_gps_processing_method_t),
        .offset = offset_of(camera_external_struct_t, jpeg_gps_processing_method),
        .size = sizeof (guzzi_camera3_controls_jpeg_gps_processing_method_t),
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_JPEG_GPS_PROCESSING_METHOD,
        .convert = camera_external_convert_jpeg_gps_processing_method
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_JPEG_GPS_TIMESTAMP),
        .struct_name = STR(guzzi_camera3_controls_jpeg_gps_timestamp_t),
        .offset = offset_of(camera_external_struct_t, jpeg_gps_timestamp),
        .size = sizeof (guzzi_camera3_controls_jpeg_gps_timestamp_t),
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_JPEG_GPS_TIMESTAMP,
        .convert = camera_external_convert_jpeg_gps_timestamp
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_JPEG_ORIENTATION),
        .struct_name = STR(guzzi_camera3_controls_jpeg_orientation_t),
        .offset = offset_of(camera_external_struct_t, jpeg_orientation),
        .size = sizeof (guzzi_camera3_controls_jpeg_orientation_t),
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_JPEG_ORIENTATION,
        .convert = camera_external_convert_jpeg_orientation
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_JPEG_QUALITY),
        .struct_name = STR(guzzi_camera3_controls_jpeg_quality_t),
        .offset = offset_of(camera_external_struct_t, jpeg_quality),
        .size = sizeof (guzzi_camera3_controls_jpeg_quality_t),
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_JPEG_QUALITY,
        .convert = camera_external_convert_jpeg_quality
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_JPEG_THUMBNAIL_QUALITY),
        .struct_name = STR(guzzi_camera3_controls_jpeg_thumbnail_quality_t),
        .offset = offset_of(camera_external_struct_t, jpeg_thumbnail_quality),
        .size = sizeof (guzzi_camera3_controls_jpeg_thumbnail_quality_t),
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_JPEG_THUMBNAIL_QUALITY,
        .convert = camera_external_convert_jpeg_thumbnail_quality
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_JPEG_THUMBNAIL_SIZE),
        .struct_name = STR(guzzi_camera3_controls_jpeg_thumbnail_size_t),
        .offset = offset_of(camera_external_struct_t, jpeg_thumbnail_size),
        .size = sizeof (guzzi_camera3_controls_jpeg_thumbnail_size_t),
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_JPEG_THUMBNAIL_SIZE,
        .convert = camera_external_convert_jpeg_thumbnail_size
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_LED_TRANSMIT),
        .struct_name = STR(guzzi_camera3_controls_led_transmit_t),
        .offset = offset_of(camera_external_struct_t, led_transmit),
        .size = sizeof (guzzi_camera3_controls_led_transmit_t),
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_LED_TRANSMIT,
        .convert = camera_external_convert_led_transmit
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_LENS_APERTURE),
        .struct_name = STR(guzzi_camera3_controls_lens_aperture_t),
        .offset = offset_of(camera_external_struct_t, lens_aperture),
        .size = sizeof (guzzi_camera3_controls_lens_aperture_t),
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_LENS_APERTURE,
        .convert = camera_external_convert_lens_aperture
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_LENS_FILTER_DENSITY),
        .struct_name = STR(guzzi_camera3_controls_lens_filter_density_t),
        .offset = offset_of(camera_external_struct_t, lens_filter_density),
        .size = sizeof (guzzi_camera3_controls_lens_filter_density_t),
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_LENS_FILTER_DENSITY,
        .convert = camera_external_convert_lens_filter_density
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_LENS_FOCAL_LENGTH),
        .struct_name = STR(guzzi_camera3_controls_lens_focal_length_t),
        .offset = offset_of(camera_external_struct_t, lens_focal_length),
        .size = sizeof (guzzi_camera3_controls_lens_focal_length_t),
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_LENS_FOCAL_LENGTH,
        .convert = camera_external_convert_lens_focal_length
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_LENS_FOCUS_DISTANCE),
        .struct_name = STR(guzzi_camera3_controls_lens_focus_distance_t),
        .offset = offset_of(camera_external_struct_t, lens_focus_distance),
        .size = sizeof (guzzi_camera3_controls_lens_focus_distance_t),
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_LENS_FOCUS_DISTANCE,
        .convert = camera_external_convert_lens_focus_distance
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_LENS_OPTICAL_STABILIZATION_MODE),
        .struct_name = STR(guzzi_camera3_controls_lens_optical_stabilization_mode_t),
        .offset = offset_of(camera_external_struct_t, lens_optical_stabilization_mode),
        .size = sizeof (guzzi_camera3_controls_lens_optical_stabilization_mode_t),
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_LENS_OPTICAL_STABILIZATION_MODE,
        .convert = camera_external_convert_lens_optical_stabilization_mode
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_NOISE_REDUCTION_MODE),
        .struct_name = STR(guzzi_camera3_controls_noise_reduction_mode_t),
        .offset = offset_of(camera_external_struct_t, noise_reduction_mode),
        .size = sizeof (guzzi_camera3_controls_noise_reduction_mode_t),
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_NOISE_REDUCTION_MODE,
        .convert = camera_external_convert_noise_reduction_mode
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_NOISE_REDUCTION_STRENGTH),
        .struct_name = STR(guzzi_camera3_controls_noise_reduction_strength_t),
        .offset = offset_of(camera_external_struct_t, noise_reduction_strength),
        .size = sizeof (guzzi_camera3_controls_noise_reduction_strength_t),
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_NOISE_REDUCTION_STRENGTH,
        .convert = camera_external_convert_noise_reduction_strength
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_REQUEST_FRAME_COUNT),
        .struct_name = STR(guzzi_camera3_controls_request_frame_count_t),
        .offset = offset_of(camera_external_struct_t, request_frame_count),
        .size = sizeof (guzzi_camera3_controls_request_frame_count_t),
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_REQUEST_FRAME_COUNT,
        .convert = camera_external_convert_request_frame_count
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_REQUEST_ID),
        .struct_name = STR(guzzi_camera3_controls_request_id_t),
        .offset = offset_of(camera_external_struct_t, request_id),
        .size = sizeof (guzzi_camera3_controls_request_id_t),
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_REQUEST_ID,
        .convert = camera_external_convert_request_id
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_REQUEST_INPUT_STREAMS),
        .struct_name = STR(guzzi_camera3_controls_request_input_streams_t),
        .offset = offset_of(camera_external_struct_t, request_input_streams),
        .size = sizeof (guzzi_camera3_controls_request_input_streams_t),
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_REQUEST_INPUT_STREAMS,
        .convert = camera_external_convert_request_input_streams
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_REQUEST_METADATA_MODE),
        .struct_name = STR(guzzi_camera3_controls_request_metadata_mode_t),
        .offset = offset_of(camera_external_struct_t, request_metadata_mode),
        .size = sizeof (guzzi_camera3_controls_request_metadata_mode_t),
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_REQUEST_METADATA_MODE,
        .convert = camera_external_convert_request_metadata_mode
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_REQUEST_OUTPUT_STREAMS),
        .struct_name = STR(guzzi_camera3_controls_request_output_streams_t),
        .offset = offset_of(camera_external_struct_t, request_output_streams),
        .size = sizeof (guzzi_camera3_controls_request_output_streams_t),
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_REQUEST_OUTPUT_STREAMS,
        .convert = camera_external_convert_request_output_streams
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_REQUEST_TYPE),
        .struct_name = STR(guzzi_camera3_controls_request_type_t),
        .offset = offset_of(camera_external_struct_t, request_type),
        .size = sizeof (guzzi_camera3_controls_request_type_t),
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_REQUEST_TYPE,
        .convert = camera_external_convert_request_type
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_SCALER_CROP_REGION),
        .struct_name = STR(guzzi_camera3_controls_scaler_crop_region_t),
        .offset = offset_of(camera_external_struct_t, scaler_crop_region),
        .size = sizeof (guzzi_camera3_controls_scaler_crop_region_t),
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_SCALER_CROP_REGION,
        .convert = camera_external_convert_scaler_crop_region
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_SENSOR_EXPOSURE_TIME),
        .struct_name = STR(guzzi_camera3_controls_sensor_exposure_time_t),
        .offset = offset_of(camera_external_struct_t, sensor_exposure_time),
        .size = sizeof (guzzi_camera3_controls_sensor_exposure_time_t),
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_SENSOR_EXPOSURE_TIME,
        .convert = camera_external_convert_sensor_exposure_time
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_SENSOR_FRAME_DURATION),
        .struct_name = STR(guzzi_camera3_controls_sensor_frame_duration_t),
        .offset = offset_of(camera_external_struct_t, sensor_frame_duration),
        .size = sizeof (guzzi_camera3_controls_sensor_frame_duration_t),
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_SENSOR_FRAME_DURATION,
        .convert = camera_external_convert_sensor_frame_duration
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_SENSOR_SENSITIVITY),
        .struct_name = STR(guzzi_camera3_controls_sensor_sensitivity_t),
        .offset = offset_of(camera_external_struct_t, sensor_sensitivity),
        .size = sizeof (guzzi_camera3_controls_sensor_sensitivity_t),
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_SENSOR_SENSITIVITY,
        .convert = camera_external_convert_sensor_sensitivity
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_SHADING_MODE),
        .struct_name = STR(guzzi_camera3_controls_shading_mode_t),
        .offset = offset_of(camera_external_struct_t, shading_mode),
        .size = sizeof (guzzi_camera3_controls_shading_mode_t),
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_SHADING_MODE,
        .convert = camera_external_convert_shading_mode
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_SHADING_STRENGTH),
        .struct_name = STR(guzzi_camera3_controls_shading_strength_t),
        .offset = offset_of(camera_external_struct_t, shading_strength),
        .size = sizeof (guzzi_camera3_controls_shading_strength_t),
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_SHADING_STRENGTH,
        .convert = camera_external_convert_shading_strength
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_STATISTICS_FACE_DETECT_MODE),
        .struct_name = STR(guzzi_camera3_controls_statistics_face_detect_mode_t),
        .offset = offset_of(camera_external_struct_t, statistics_face_detect_mode),
        .size = sizeof (guzzi_camera3_controls_statistics_face_detect_mode_t),
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_STATISTICS_FACE_DETECT_MODE,
        .convert = camera_external_convert_statistics_face_detect_mode
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_STATISTICS_HISTOGRAM_MODE),
        .struct_name = STR(guzzi_camera3_controls_statistics_histogram_mode_t),
        .offset = offset_of(camera_external_struct_t, statistics_histogram_mode),
        .size = sizeof (guzzi_camera3_controls_statistics_histogram_mode_t),
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_STATISTICS_HISTOGRAM_MODE,
        .convert = camera_external_convert_statistics_histogram_mode
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_STATISTICS_LENS_SHADING_MAP_MODE),
        .struct_name = STR(guzzi_camera3_controls_statistics_lens_shading_map_mode_t),
        .offset = offset_of(camera_external_struct_t, statistics_lens_shading_map_mode),
        .size = sizeof (guzzi_camera3_controls_statistics_lens_shading_map_mode_t),
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_STATISTICS_LENS_SHADING_MAP_MODE,
        .convert = camera_external_convert_statistics_lens_shading_map_mode
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_STATISTICS_SHARPNESS_MAP_MODE),
        .struct_name = STR(guzzi_camera3_controls_statistics_sharpness_map_mode_t),
        .offset = offset_of(camera_external_struct_t, statistics_sharpness_map_mode),
        .size = sizeof (guzzi_camera3_controls_statistics_sharpness_map_mode_t),
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_STATISTICS_SHARPNESS_MAP_MODE,
        .convert = camera_external_convert_statistics_sharpness_map_mode
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_TONEMAP_CURVE_BLUE),
        .struct_name = STR(guzzi_camera3_controls_tonemap_curve_blue_t),
        .offset = offset_of(camera_external_struct_t, tonemap_curve_blue),
        .size = sizeof (guzzi_camera3_controls_tonemap_curve_blue_t),
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_TONEMAP_CURVE_BLUE,
        .convert = camera_external_convert_tonemap_curve_blue
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_TONEMAP_CURVE_GREEN),
        .struct_name = STR(guzzi_camera3_controls_tonemap_curve_green_t),
        .offset = offset_of(camera_external_struct_t, tonemap_curve_green),
        .size = sizeof (guzzi_camera3_controls_tonemap_curve_green_t),
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_TONEMAP_CURVE_GREEN,
        .convert = camera_external_convert_tonemap_curve_green
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_TONEMAP_CURVE_RED),
        .struct_name = STR(guzzi_camera3_controls_tonemap_curve_red_t),
        .offset = offset_of(camera_external_struct_t, tonemap_curve_red),
        .size = sizeof (guzzi_camera3_controls_tonemap_curve_red_t),
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_TONEMAP_CURVE_RED,
        .convert = camera_external_convert_tonemap_curve_red
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_TONEMAP_MODE),
        .struct_name = STR(guzzi_camera3_controls_tonemap_mode_t),
        .offset = offset_of(camera_external_struct_t, tonemap_mode),
        .size = sizeof (guzzi_camera3_controls_tonemap_mode_t),
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_TONEMAP_MODE,
        .convert = camera_external_convert_tonemap_mode
    },

    /* Guzzi specific */
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_CAPTURE_REQUEST_ACTIVE),
        .struct_name = STR(guzzi_camera3_controls_capture_request_active_t),
        .offset = offset_of(camera_external_struct_t, capture_request_active),
        .size = sizeof (guzzi_camera3_controls_capture_request_active_t),
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_CAPTURE_REQUEST_ACTIVE,
        .convert = camera_external_convert_capture_request_active
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_CAPTURE_REQUEST_FRAME_NUMBER),
        .struct_name = STR(guzzi_camera3_controls_capture_request_frame_number_t),
        .offset = offset_of(camera_external_struct_t, capture_request_frame_number),
        .size = sizeof (guzzi_camera3_controls_capture_request_frame_number_t),
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_CAPTURE_REQUEST_FRAME_NUMBER,
        .convert = camera_external_convert_capture_request_frame_number
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_CAPTURE_REQUEST_GUZZI_FR_ID),
        .struct_name = STR(guzzi_camera3_controls_capture_request_guzzi_fr_id_t),
        .offset = offset_of(camera_external_struct_t, capture_request_guzzi_fr_id),
        .size = sizeof (guzzi_camera3_controls_capture_request_guzzi_fr_id_t),
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_CAPTURE_REQUEST_GUZZI_FR_ID,
        .convert = camera_external_convert_capture_request_guzzi_fr_id
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_STREAM_CONFIG_FORMAT),
        .struct_name = STR(guzzi_camera3_controls_stream_config_format_t),
        .offset = offset_of(camera_external_struct_t, stream_config_format),
        .size = sizeof (guzzi_camera3_controls_stream_config_format_t),
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_STREAM_CONFIG_FORMAT,
        .convert = camera_external_convert_stream_config_format
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_STREAM_CONFIG_HEIGHT),
        .struct_name = STR(guzzi_camera3_controls_stream_config_height_t),
        .offset = offset_of(camera_external_struct_t, stream_config_height),
        .size = sizeof (guzzi_camera3_controls_stream_config_height_t),
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_STREAM_CONFIG_HEIGHT,
        .convert = camera_external_convert_stream_config_height
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_STREAM_CONFIG_ID),
        .struct_name = STR(guzzi_camera3_controls_stream_config_id_t),
        .offset = offset_of(camera_external_struct_t, stream_config_id),
        .size = sizeof (guzzi_camera3_controls_stream_config_id_t),
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_STREAM_CONFIG_ID,
        .convert = camera_external_convert_stream_config_id
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_STREAM_CONFIG_TYPE),
        .struct_name = STR(guzzi_camera3_controls_stream_config_type_t),
        .offset = offset_of(camera_external_struct_t, stream_config_type),
        .size = sizeof (guzzi_camera3_controls_stream_config_type_t),
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_STREAM_CONFIG_TYPE,
        .convert = camera_external_convert_stream_config_type
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_STREAM_CONFIG_WIDTH),
        .struct_name = STR(guzzi_camera3_controls_stream_config_width_t),
        .offset = offset_of(camera_external_struct_t, stream_config_width),
        .size = sizeof (guzzi_camera3_controls_stream_config_width_t),
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_STREAM_CONFIG_WIDTH,
        .convert = camera_external_convert_stream_config_width
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_Z_CUSTOM_CAPTURE_NUMBER_SHOTS),
        .struct_name = STR(guzzi_camera3_controls_z_custom_capture_number_shots_t),
        .offset = offset_of(camera_external_struct_t, z_custom_capture_number_shots),
        .size = sizeof (guzzi_camera3_controls_z_custom_capture_number_shots_t),
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_Z_CUSTOM_CAPTURE_NUMBER_SHOTS,
        .convert = camera_external_convert_z_custom_capture_number_shots
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_Z_CUSTOM_CAPTURE_MODE_SELECTION),
        .struct_name = STR(guzzi_camera3_controls_z_custom_capture_mode_selection_t),
        .offset = offset_of(camera_external_struct_t, z_custom_capture_mode_selection),
        .size = sizeof (guzzi_camera3_controls_z_custom_capture_mode_selection_t),
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_Z_CUSTOM_CAPTURE_MODE_SELECTION,
        .convert = camera_external_convert_z_custom_capture_mode_selection
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_Z_CUSTOM_CRTL_BRIGTHNESS),
        .struct_name = STR(guzzi_camera3_controls_z_custom_crtl_brigthness_t),
        .offset = offset_of(camera_external_struct_t, z_custom_crtl_brigthness),
        .size = sizeof (guzzi_camera3_controls_z_custom_crtl_brigthness_t),
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_Z_CUSTOM_CRTL_BRIGTHNESS,
        .convert = camera_external_convert_z_custom_crtl_brigthness
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_Z_CUSTOM_CRTL_CONTRAST),
        .struct_name = STR(guzzi_camera3_controls_z_custom_crtl_contrast_t),
        .offset = offset_of(camera_external_struct_t, z_custom_crtl_contrast),
        .size = sizeof (guzzi_camera3_controls_z_custom_crtl_contrast_t),
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_Z_CUSTOM_CRTL_CONTRAST,
        .convert = camera_external_convert_z_custom_crtl_contrast
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_Z_CUSTOM_CRTL_HUE),
        .struct_name = STR(guzzi_camera3_controls_z_custom_crtl_hue_t),
        .offset = offset_of(camera_external_struct_t, z_custom_crtl_hue),
        .size = sizeof (guzzi_camera3_controls_z_custom_crtl_hue_t),
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_Z_CUSTOM_CRTL_HUE,
        .convert = camera_external_convert_z_custom_crtl_hue
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_Z_CUSTOM_CRTL_SATURATION),
        .struct_name = STR(guzzi_camera3_controls_z_custom_crtl_saturation_t),
        .offset = offset_of(camera_external_struct_t, z_custom_crtl_saturation),
        .size = sizeof (guzzi_camera3_controls_z_custom_crtl_saturation_t),
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_Z_CUSTOM_CRTL_SATURATION,
        .convert = camera_external_convert_z_custom_crtl_saturation
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_Z_CUSTOM_EXPOSURE_BRACKETING_SEQUENCE),
        .struct_name = STR(guzzi_camera3_controls_z_custom_exposure_bracketing_sequence_t),
        .offset = offset_of(camera_external_struct_t, z_custom_exposure_bracketing_sequence),
        .size = sizeof (guzzi_camera3_controls_z_custom_exposure_bracketing_sequence_t),
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_Z_CUSTOM_EXPOSURE_BRACKETING_SEQUENCE,
        .convert = camera_external_convert_z_custom_exposure_bracketing_sequence
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_Z_CUSTOM_USECASE_SELECTION),
        .struct_name = STR(guzzi_camera3_controls_z_custom_usecase_selection_t),
        .offset = offset_of(camera_external_struct_t, z_custom_usecase_selection),
        .size = sizeof (guzzi_camera3_controls_z_custom_usecase_selection_t),
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_Z_CUSTOM_USECASE_SELECTION,
        .convert = camera_external_convert_z_custom_usecase_selection
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_Z_EXPOSURE_MERGER_WEIGHT),
        .struct_name = STR(guzzi_camera3_controls_z_exposure_merger_weight_t),
        .offset = offset_of(camera_external_struct_t, z_exposure_merger_weight),
        .size = sizeof (guzzi_camera3_controls_z_exposure_merger_weight_t),
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_Z_EXPOSURE_MERGER_WEIGHT,
        .convert = camera_external_convert_z_exposure_merger_weight
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_Z_WB_MANUAL_TEMPERATURE),
        .struct_name = STR(guzzi_camera3_controls_z_wb_manual_temperature_t),
        .offset = offset_of(camera_external_struct_t, z_wb_manual_temperature),
        .size = sizeof (guzzi_camera3_controls_z_wb_manual_temperature_t),
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_Z_WB_MANUAL_TEMPERATURE,
        .convert = camera_external_convert_z_wb_manual_temperature
    },
    {
        .index_name = STR(GUZZI_CAMERA3_INDEX_CONTROLS_Z_WHITE_BALANCE_MERGER_WEIGHT),
        .struct_name = STR(guzzi_camera3_controls_z_white_balance_merger_weight_t),
        .offset = offset_of(camera_external_struct_t, z_white_balance_merger_weight),
        .size = sizeof (guzzi_camera3_controls_z_white_balance_merger_weight_t),
        .index = GUZZI_CAMERA3_INDEX_CONTROLS_Z_WHITE_BALANCE_MERGER_WEIGHT,
        .convert = camera_external_convert_z_white_balance_merger_weight
    },
};

camera_external_struct_helper_t *camera_external_index_to_helper(guzzi_camera3_metadata_index_t index)
{
    switch (index) {
        case GUZZI_CAMERA3_INDEX_CONTROLS_BLACK_LEVEL_LOCK:
            return camera_external_struct_helper + 0;
        case GUZZI_CAMERA3_INDEX_CONTROLS_COLOR_CORRECTION_GAINS:
            return camera_external_struct_helper + 1;
        case GUZZI_CAMERA3_INDEX_CONTROLS_COLOR_CORRECTION_MODE:
            return camera_external_struct_helper + 2;
        case GUZZI_CAMERA3_INDEX_CONTROLS_COLOR_CORRECTION_TRANSFORM:
            return camera_external_struct_helper + 3;
        case GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AE_ANTIBANDING_MODE:
            return camera_external_struct_helper + 4;
        case GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AE_EXPOSURE_COMPENSATION:
            return camera_external_struct_helper + 5;
        case GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AE_LOCK:
            return camera_external_struct_helper + 6;
        case GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AE_MODE:
            return camera_external_struct_helper + 7;
        case GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AE_PRECAPTURE_TRIGGER:
            return camera_external_struct_helper + 8;
        case GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AE_REGIONS:
            return camera_external_struct_helper + 9;
        case GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AE_TARGET_FPS_RANGE:
            return camera_external_struct_helper + 10;
        case GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AF_MODE:
            return camera_external_struct_helper + 11;
        case GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AF_REGIONS:
            return camera_external_struct_helper + 12;
        case GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AF_TRIGGER:
            return camera_external_struct_helper + 13;
        case GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AWB_LOCK:
            return camera_external_struct_helper + 14;
        case GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AWB_MODE:
            return camera_external_struct_helper + 15;
        case GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_AWB_REGIONS:
            return camera_external_struct_helper + 16;
        case GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_CAPTURE_INTENT:
            return camera_external_struct_helper + 17;
        case GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_EFFECT_MODE:
            return camera_external_struct_helper + 18;
        case GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_MODE:
            return camera_external_struct_helper + 19;
        case GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_SCENE_MODE:
            return camera_external_struct_helper + 20;
        case GUZZI_CAMERA3_INDEX_CONTROLS_CONTROL_VIDEO_STABILIZATION_MODE:
            return camera_external_struct_helper + 21;
        case GUZZI_CAMERA3_INDEX_CONTROLS_DEMOSAIC_MODE:
            return camera_external_struct_helper + 22;
        case GUZZI_CAMERA3_INDEX_CONTROLS_EDGE_MODE:
            return camera_external_struct_helper + 23;
        case GUZZI_CAMERA3_INDEX_CONTROLS_EDGE_STRENGTH:
            return camera_external_struct_helper + 24;
        case GUZZI_CAMERA3_INDEX_CONTROLS_FLASH_FIRING_POWER:
            return camera_external_struct_helper + 25;
        case GUZZI_CAMERA3_INDEX_CONTROLS_FLASH_FIRING_TIME:
            return camera_external_struct_helper + 26;
        case GUZZI_CAMERA3_INDEX_CONTROLS_FLASH_MODE:
            return camera_external_struct_helper + 27;
        case GUZZI_CAMERA3_INDEX_CONTROLS_GEOMETRIC_MODE:
            return camera_external_struct_helper + 28;
        case GUZZI_CAMERA3_INDEX_CONTROLS_GEOMETRIC_STRENGTH:
            return camera_external_struct_helper + 29;
        case GUZZI_CAMERA3_INDEX_CONTROLS_HOT_PIXEL_MODE:
            return camera_external_struct_helper + 30;
        case GUZZI_CAMERA3_INDEX_CONTROLS_JPEG_GPS_COORDINATES:
            return camera_external_struct_helper + 31;
        case GUZZI_CAMERA3_INDEX_CONTROLS_JPEG_GPS_PROCESSING_METHOD:
            return camera_external_struct_helper + 32;
        case GUZZI_CAMERA3_INDEX_CONTROLS_JPEG_GPS_TIMESTAMP:
            return camera_external_struct_helper + 33;
        case GUZZI_CAMERA3_INDEX_CONTROLS_JPEG_ORIENTATION:
            return camera_external_struct_helper + 34;
        case GUZZI_CAMERA3_INDEX_CONTROLS_JPEG_QUALITY:
            return camera_external_struct_helper + 35;
        case GUZZI_CAMERA3_INDEX_CONTROLS_JPEG_THUMBNAIL_QUALITY:
            return camera_external_struct_helper + 36;
        case GUZZI_CAMERA3_INDEX_CONTROLS_JPEG_THUMBNAIL_SIZE:
            return camera_external_struct_helper + 37;
        case GUZZI_CAMERA3_INDEX_CONTROLS_LED_TRANSMIT:
            return camera_external_struct_helper + 38;
        case GUZZI_CAMERA3_INDEX_CONTROLS_LENS_APERTURE:
            return camera_external_struct_helper + 39;
        case GUZZI_CAMERA3_INDEX_CONTROLS_LENS_FILTER_DENSITY:
            return camera_external_struct_helper + 40;
        case GUZZI_CAMERA3_INDEX_CONTROLS_LENS_FOCAL_LENGTH:
            return camera_external_struct_helper + 41;
        case GUZZI_CAMERA3_INDEX_CONTROLS_LENS_FOCUS_DISTANCE:
            return camera_external_struct_helper + 42;
        case GUZZI_CAMERA3_INDEX_CONTROLS_LENS_OPTICAL_STABILIZATION_MODE:
            return camera_external_struct_helper + 43;
        case GUZZI_CAMERA3_INDEX_CONTROLS_NOISE_REDUCTION_MODE:
            return camera_external_struct_helper + 44;
        case GUZZI_CAMERA3_INDEX_CONTROLS_NOISE_REDUCTION_STRENGTH:
            return camera_external_struct_helper + 45;
        case GUZZI_CAMERA3_INDEX_CONTROLS_REQUEST_FRAME_COUNT:
            return camera_external_struct_helper + 46;
        case GUZZI_CAMERA3_INDEX_CONTROLS_REQUEST_ID:
            return camera_external_struct_helper + 47;
        case GUZZI_CAMERA3_INDEX_CONTROLS_REQUEST_INPUT_STREAMS:
            return camera_external_struct_helper + 48;
        case GUZZI_CAMERA3_INDEX_CONTROLS_REQUEST_METADATA_MODE:
            return camera_external_struct_helper + 49;
        case GUZZI_CAMERA3_INDEX_CONTROLS_REQUEST_OUTPUT_STREAMS:
            return camera_external_struct_helper + 50;
        case GUZZI_CAMERA3_INDEX_CONTROLS_REQUEST_TYPE:
            return camera_external_struct_helper + 51;
        case GUZZI_CAMERA3_INDEX_CONTROLS_SCALER_CROP_REGION:
            return camera_external_struct_helper + 52;
        case GUZZI_CAMERA3_INDEX_CONTROLS_SENSOR_EXPOSURE_TIME:
            return camera_external_struct_helper + 53;
        case GUZZI_CAMERA3_INDEX_CONTROLS_SENSOR_FRAME_DURATION:
            return camera_external_struct_helper + 54;
        case GUZZI_CAMERA3_INDEX_CONTROLS_SENSOR_SENSITIVITY:
            return camera_external_struct_helper + 55;
        case GUZZI_CAMERA3_INDEX_CONTROLS_SHADING_MODE:
            return camera_external_struct_helper + 56;
        case GUZZI_CAMERA3_INDEX_CONTROLS_SHADING_STRENGTH:
            return camera_external_struct_helper + 57;
        case GUZZI_CAMERA3_INDEX_CONTROLS_STATISTICS_FACE_DETECT_MODE:
            return camera_external_struct_helper + 58;
        case GUZZI_CAMERA3_INDEX_CONTROLS_STATISTICS_HISTOGRAM_MODE:
            return camera_external_struct_helper + 59;
        case GUZZI_CAMERA3_INDEX_CONTROLS_STATISTICS_LENS_SHADING_MAP_MODE:
            return camera_external_struct_helper + 60;
        case GUZZI_CAMERA3_INDEX_CONTROLS_STATISTICS_SHARPNESS_MAP_MODE:
            return camera_external_struct_helper + 61;
        case GUZZI_CAMERA3_INDEX_CONTROLS_TONEMAP_CURVE_BLUE:
            return camera_external_struct_helper + 62;
        case GUZZI_CAMERA3_INDEX_CONTROLS_TONEMAP_CURVE_GREEN:
            return camera_external_struct_helper + 63;
        case GUZZI_CAMERA3_INDEX_CONTROLS_TONEMAP_CURVE_RED:
            return camera_external_struct_helper + 64;
        case GUZZI_CAMERA3_INDEX_CONTROLS_TONEMAP_MODE:
            return camera_external_struct_helper + 65;

        /* Guzzi specific */
        case GUZZI_CAMERA3_INDEX_CONTROLS_CAPTURE_REQUEST_ACTIVE:
            return camera_external_struct_helper + 66;
        case GUZZI_CAMERA3_INDEX_CONTROLS_CAPTURE_REQUEST_FRAME_NUMBER:
            return camera_external_struct_helper + 67;
        case GUZZI_CAMERA3_INDEX_CONTROLS_CAPTURE_REQUEST_GUZZI_FR_ID:
            return camera_external_struct_helper + 68;
        case GUZZI_CAMERA3_INDEX_CONTROLS_STREAM_CONFIG_FORMAT:
            return camera_external_struct_helper + 69;
        case GUZZI_CAMERA3_INDEX_CONTROLS_STREAM_CONFIG_HEIGHT:
            return camera_external_struct_helper + 70;
        case GUZZI_CAMERA3_INDEX_CONTROLS_STREAM_CONFIG_ID:
            return camera_external_struct_helper + 71;
        case GUZZI_CAMERA3_INDEX_CONTROLS_STREAM_CONFIG_TYPE:
            return camera_external_struct_helper + 72;
        case GUZZI_CAMERA3_INDEX_CONTROLS_STREAM_CONFIG_WIDTH:
            return camera_external_struct_helper + 73;
        case GUZZI_CAMERA3_INDEX_CONTROLS_Z_CUSTOM_CAPTURE_NUMBER_SHOTS:
            return camera_external_struct_helper + 74;
        case GUZZI_CAMERA3_INDEX_CONTROLS_Z_CUSTOM_CAPTURE_MODE_SELECTION:
            return camera_external_struct_helper + 75;
        case GUZZI_CAMERA3_INDEX_CONTROLS_Z_CUSTOM_CRTL_BRIGTHNESS:
            return camera_external_struct_helper + 76;
        case GUZZI_CAMERA3_INDEX_CONTROLS_Z_CUSTOM_CRTL_CONTRAST:
            return camera_external_struct_helper + 77;
        case GUZZI_CAMERA3_INDEX_CONTROLS_Z_CUSTOM_CRTL_HUE:
            return camera_external_struct_helper + 78;
        case GUZZI_CAMERA3_INDEX_CONTROLS_Z_CUSTOM_CRTL_SATURATION:
            return camera_external_struct_helper + 79;
        case GUZZI_CAMERA3_INDEX_CONTROLS_Z_CUSTOM_EXPOSURE_BRACKETING_SEQUENCE:
            return camera_external_struct_helper + 80;
        case GUZZI_CAMERA3_INDEX_CONTROLS_Z_CUSTOM_USECASE_SELECTION:
            return camera_external_struct_helper + 81;
        case GUZZI_CAMERA3_INDEX_CONTROLS_Z_EXPOSURE_MERGER_WEIGHT:
            return camera_external_struct_helper + 82;
        case GUZZI_CAMERA3_INDEX_CONTROLS_Z_WB_MANUAL_TEMPERATURE:
            return camera_external_struct_helper + 83;
        case GUZZI_CAMERA3_INDEX_CONTROLS_Z_WHITE_BALANCE_MERGER_WEIGHT:
            return camera_external_struct_helper + 84;

        default:
            return NULL;
    }
}

camera_external_struct_helper_t *camera_external_next_helper(camera_external_struct_helper_t *helper)
{
    if (!helper) {
        return camera_external_struct_helper;
    }

    /* TODO: assert(camera_external_struct_helper <= helper); */
    if ((helper + 1) < (camera_external_struct_helper + sizeof (camera_external_struct_helper) / sizeof (camera_external_struct_helper_t))) {
        return helper + 1;
    } else {
        return NULL;
    }
}

