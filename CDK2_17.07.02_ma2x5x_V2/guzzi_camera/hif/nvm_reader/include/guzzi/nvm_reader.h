/* =============================================================================
* Copyright (c) 2013-2014 MM Solutions AD
* All rights reserved. Property of MM Solutions AD.
*
* This source code may not be used against the terms and conditions stipulated
* in the licensing agreement under which it has been supplied, or without the
* written permission of MM Solutions. Rights to use, copy, modify, and
* distribute or disclose this source code and its documentation are granted only
* through signed licensing agreement, provided that this copyright notice
* appears in all copies, modifications, and distributions and subject to the
* following conditions:
* THIS SOURCE CODE AND ACCOMPANYING DOCUMENTATION, IS PROVIDED AS IS, WITHOUT
* WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. MM SOLUTIONS SPECIFICALLY DISCLAIMS
* ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN
* NO EVENT SHALL MM SOLUTIONS BE LIABLE TO ANY PARTY FOR ANY CLAIM, DIRECT,
* INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST
* PROFITS, OR OTHER LIABILITY, ARISING OUT OF THE USE OF OR IN CONNECTION WITH
* THIS SOURCE CODE AND ITS DOCUMENTATION.
* =========================================================================== */
/**
* @file nvm_reader.h
*
* @author ( MM Solutions AD )
*
*/
/* -----------------------------------------------------------------------------
*!
*! Revision History
*! ===================================
*! 05-Nov-2013 : Author ( MM Solutions AD )
*! Created
* =========================================================================== */

#ifndef _GUZZI_NVM_READER_H
#define _GUZZI_NVM_READER_H

#ifdef __cplusplus
extern "C" {
#endif

#include <hal/hal_camera_module/hat_cm_nvm_data_struct.h>

typedef struct guzzi_nvm_reader guzzi_nvm_reader_t;

/*
 * ****************************************************************************
 * ** Number of cameras *******************************************************
 * ****************************************************************************
 */

int guzzi_nvm_reader_get_number_of_cameras(guzzi_nvm_reader_t *nvm_reader);

/*
 * ****************************************************************************
 * ** Read ********************************************************************
 * ****************************************************************************
 */
nvm_info_t * guzzi_nvm_reader_read(
        guzzi_nvm_reader_t *nvm_reader,
        int camera_id  /* [1, number_of_cameras] */
    );

/*
 * ****************************************************************************
 * ** Create/Destroy **********************************************************
 * ****************************************************************************
 */
void guzzi_nvm_reader_destroy(guzzi_nvm_reader_t *nvm_reader);

guzzi_nvm_reader_t *guzzi_nvm_reader_create(void);

#ifdef __cplusplus
}
#endif

#endif /* _GUZZI_NVM_READER_H */

