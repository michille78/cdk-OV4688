This folder contains the firmware for the Cypress Cx3 device on the mv0214 board. 

To download, run the download_fx3.exe utility from a Linux terminal window.

Syntax:
download_fx3 -t [RAM | SPI | I2C] -i <FIRMWARE_IMAGE>

Usage:
$ ./download_fx3 -t RAM -i mv214cx3fw_2L400Mhz.img

In case of success the following message is displayed: FX3 firmware programming to RAM completed.

In case of error, un-plug then reconnect USB cable from MV0214 board.
