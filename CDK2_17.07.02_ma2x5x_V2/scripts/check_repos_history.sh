#! /bin/bash

if [ $# -ne 3 ]; then
  echo "illegal number of parameters"
  echo "Usage: $0 <int_rel_name> <ext_rel_name> <manifest>"
  exit 1 
fi

YELLOW='\e[33;1m'
ENDCOLOR='\e[0m'

SCRIPT_DIR=$(pwd)
INT_RLS_DIR=$1
echo INT_RLS_DIR=$INT_RLS_DIR
EXT_RLS_DIR=$2
echo EXT_RLS_DIR=$EXT_RLS_DIR
MANIFEST=$3

grep "project name" $MANIFEST | while read line
do
  # Extract project path  
  name=$(echo $line | grep -o -E 'name="[a-zA-Z/_.]+"' | cut -d '"' -f2)
  path=$(echo $line | grep -o -E 'path="[a-zA-Z/_.]+"' | cut -d '"' -f2)
  revision=$(echo $line | grep -o -E "[[:alnum:]]{40}")
  prj_path=

  if [ ! -z "$path" ]
    then
      prj_path=$path   
    else
      prj_path=$name      
  fi  
  
  PATCH_DIR=$SCRIPT_DIR/patches/$prj_path
  # Skip check if patch directory does not exist
#  if [ ! -d $PATCH_DIR ]; then
#     continue
#  fi
  # Skip check if patch directory is empty
#  if [[ ! $(ls $PATCH_DIR) ]]; then
 #    continue;
#  fi  
    
  echo prj_path=$prj_path

echo $INT_RLS_DIR/$prj_path
echo $EXT_RLS_DIR/$prj_path
  
  if [ -d $INT_RLS_DIR/$prj_path -a -d $EXT_RLS_DIR/$prj_path ]; then
   # echo $prj_path
    exclude_file_name=$(echo $prj_path | tr / _)
    exclude_file_path=$SCRIPT_DIR/exclude_repos_history/$exclude_file_name
    #echo diff -r --exclude-from=$SCRIPT_DIR/exclude/exclude --exclude-from=$exclude_file_path  $INT_RLS_DIR/$prj_path $EXT_RLS_DIR/$prj_path
   # diff -r --exclude-from=$SCRIPT_DIR/exclude/exclude --exclude-from=$exclude_file_path  $INT_RLS_DIR/$prj_path $EXT_RLS_DIR/$prj_path
    diff -r --exclude-from=$SCRIPT_DIR/exclude_repos_history/exclude --exclude-from=$exclude_file_path  $INT_RLS_DIR/$prj_path $EXT_RLS_DIR/$prj_path
  fi
done
