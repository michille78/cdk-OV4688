#!/usr/bin/python

import os
import sys

full_base = sys.argv[1]
clean_base = sys.argv[2]

def list_missing(full, clean):
    for x in os.listdir(full):
        full_path = os.path.join(full, x)
        clean_path = os.path.join(clean, x)

        if os.path.isfile(full_path) and not os.path.exists(clean_path):
            print 'rm -f  \"%s\"' % (full_path.partition(full_base)[2])

        if os.path.isdir(full_path):
            if not os.path.exists(clean_path):
                print 'rm -fr \"%s\"' % (full_path.partition(full_base)[2])
            else:
                list_missing(full_path, clean_path)

list_missing(full_base, clean_base)

