#!/bin/bash

if [ -z "$1" ]; then
    echo Missing target!
    echo Use \'linux\' or \'rtems\'
    echo Abort.
    exit 1
fi

SCRPTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
TOP=$(dirname $SCRPTDIR)

LIST_FILE=guzzi_bin_install_list_$1
if [ -z $BIN_BRANCH_DIR ]; then
BIN_BRANCH_DIR=$TOP/guzzi_bin_release_$1
echo "Using default $BIN_BRANCH_DIR"
fi

LIBDIR=$BIN_BRANCH_DIR/build/lib

if [ "$1" == "rtems" ]; then
    STRIP=$TOP/mdk/tools/linux64/sparc-myriad-elf-4.8.2/bin/sparc-myriad-elf-strip
else
    STRIP=strip
fi

if [ ! -f $SCRPTDIR/$LIST_FILE ]; then
    echo Missing \"$SCRPTDIR/$LIST_FILE\" file!
    echo Abort.
    exit 1
fi

if [ ! -f $BIN_BRANCH_DIR/$LIST_FILE ]; then
    echo Missing \"$BIN_BRANCH_DIR/$LIST_FILE\" file!
    echo Abort.
    exit 1
fi

TO_REMOVE=(`cat $BIN_BRANCH_DIR/$LIST_FILE`)
for X in "${TO_REMOVE[@]}"; do
    TEMP=$BIN_BRANCH_DIR/$X
    echo -e "  [RM]\t   $TEMP"
    rm -rf $TEMP
done

find $BIN_BRANCH_DIR -type d -empty -delete

cp -f $SCRPTDIR/$LIST_FILE $BIN_BRANCH_DIR/$LIST_FILE

pushd ./ > /dev/null
cd $TOP
TO_COPY=(`cat $SCRPTDIR/$LIST_FILE`)
for SRC in "${TO_COPY[@]}"; do
    echo -e "  [CP]\t   $SRC --> $BIN_BRANCH_DIR"
    cp -rf --parent $SRC $BIN_BRANCH_DIR
done
popd > /dev/null

for LIB in $LIBDIR/*.a; do
    echo -e "  [STRIP]  $LIB"
    $STRIP -d $LIB
done

NOW=$(date +%F_%Hh%Mm%Ss)

repo manifest -r -o $BIN_BRANCH_DIR/manifest_$NOW.xml
echo manifest_$NOW.xml >> $BIN_BRANCH_DIR/$LIST_FILE

