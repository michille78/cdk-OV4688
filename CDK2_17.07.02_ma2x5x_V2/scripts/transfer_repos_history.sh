#! /bin/bash

if [ $# -ne 3 ]; then
  echo "illegal number of parameters"
  echo "Usage: $0 <int_rel_name> <ext_rel_name> <old_manifest>"
  exit 1
fi

MAGENTA='\e[35;1m'
RED='\e[31;1m'
GREEN='\e[32;1m'
YELLOW='\e[33;1m'
ENDCOLOR='\e[0m'

GENERATE_PATCHES=1

SCRIPT_DIR=$(pwd)
echo SCRIPT_DIR=$SCRIPT_DIR
PATCH_ROOT_DIR=$HOME/patches_$(date +"%F")
echo PATCH_ROOT_DIR=$PATCH_ROOT_DIR
INT_RLS_DIR=$1
echo INT_RLS_DIR=$INT_RLS_DIR
EXT_RLS_DIR=$2
echo EXT_RLS_DIR=$EXT_RLS_DIR
MANIFEST=$3
echo

if [ $GENERATE_PATCHES -eq 1 ]; then
	if [ ! -d $PATCH_ROOT_DIR ]; then
    echo "Creating patches directory: $PATCH_ROOT_DIR"
    mkdir $PATCH_ROOT_DIR
  else
    echo "Removing patches from patches directory"
    rm -rf $PATCH_ROOT_DIR/*
  fi
fi

# Interate over all projects described in the manifest file
grep "project name" $MANIFEST | while read line
do
  # because of cd subdir
  echo cd $PATCH_ROOT_DIR
  cd $PATCH_ROOT_DIR

  # Extract project path
  name=$(echo $line | grep -o -E 'name="[a-zA-Z/_.]+"' | cut -d '"' -f2)
  path=$(echo $line | grep -o -E 'path="[a-zA-Z/_.]+"' | cut -d '"' -f2)
  revision=$(echo $line | grep -o -E "[[:alnum:]]{40}")
  prj_path=

  if [ ! -z "$path" ]
    then
      prj_path=$path
    else
      prj_path=$name
  fi

  echo
  # Skip if target directory does not exist
  echo "TARGET_PATH=$EXT_RLS_DIR/$prj_path"
  if [ ! -d $EXT_RLS_DIR/$prj_path ]; then
    echo -e "$YELLOW Target directory does not exist: $EXT_RLS_DIR/$prj_path $ENDCOLOR"
    echo -e "$YELLOW Skipping... $ENDCOLOR"
    continue
  fi
      patch_dir=$PATCH_ROOT_DIR/$prj_path
  if [ $GENERATE_PATCHES -eq 1 ]; then
      #Make subdirs in patch directory
      testpath=$(echo $prj_path | grep '/')
      if [ ! -z $testpath ]; then
        cnt=1
        while [ true ]
        do
            subdir=$(echo $prj_path | cut -d / -f $cnt)
            if [ ! -z "$subdir" ]
            then
              if [ ! -d "$subdir" ]; then
                mkdir $subdir
              fi
              cd $subdir
              cnt=$(($cnt+1))
            else
              break
            fi
        done
      else
        subdir=$PATCH_ROOT_DIR/$prj_path
         if [ ! -d "$subdir" ]; then
            mkdir $subdir
            cd $subdir
         fi
      fi

      # Generate patches
      echo cd $INT_RLS_DIR/$prj_path
      cd $INT_RLS_DIR/$prj_path
      patch_dir=$PATCH_ROOT_DIR/$prj_path
      echo PATCH_DIR=$patch_dir
      echo git format-patch $revision -o $patch_dir
      git format-patch $revision -o $patch_dir
  fi

  # Apply patches
  echo -e "$YELLOW cd $EXT_RLS_DIR $ENDCOLOR"
  cd $EXT_RLS_DIR/
  for patchname in $(ls $patch_dir)
  do
    echo -e "$MAGENTA git am --directory=$EXT_RLS_DIR/$prj_path  $patch_dir/$patchname $ENDCOLOR"
    echo  "Applying: $patchname"
    #msg=$(git am --directory=$prj_path  $patch_dir/$patchname 2>&1)
    echo "eval git am --directory=$prj_path $patch_dir/$patchname >/dev/null 2>&1"
    eval git am --directory=$prj_path $patch_dir/$patchname >/dev/null 2>&1

    if [[ $? -ne 0 ]] ; then
      eval git apply --reject --directory=$prj_path $patch_dir/$patchname  2>&1;

      if test -n "$(git status --porcelain | grep -E ".rej" | grep "$prj_path")"; then
        echo -e "$RED Patch $patchname can't be applied cleanly due to conflict. Manual work is required. Skipping... $ENDCOLOR"
        printf "\nMake sure \`git status --porcelain\` does not contain .rej or other junk files\n\n"

        # debug line
        # cp $(git status --porcelain | grep ".rej" | cut -d ' ' -f2) ~/Desktop/rej/

        git am --abort
        break
      fi

      # Staging modified files
      $dbg git add --all;

      # Committing the patch
      if git am --resolved 2>&1 | grep -q 'No changes'; then
        echo -e "$YELLOW WARNING: patch does not introduce changes, hence skipping $ENDCOLOR"
        git am --skip
      else
        echo -e "$GREEN Patch $patchname applied partially $ENDCOLOR"
      fi

    else
        echo -e "$GREEN Patch $patchname applied successfully $ENDCOLOR"
    fi



    if $(grep -q "new file mode" $patch_dir/$patchname); then
      echo -e "$RED Patch $patchname creates new file mode(s). Please check them manually. $ENDCOLOR"
    fi

  done

#  echo -e " $YELLOW diff -r --exclude-from=$SCRIPT_DIR/exclude $INT_RLS_DIR/$prj_path $EXT_RLS_DIR/$prj_path >> $SCRIPT_DIR/diffile_$prj_path $ENDCOLOR"
#  diff -r --exclude-from=$SCRIPT_DIR/exclude $INT_RLS_DIR/$prj_path $EXT_RLS_DIR/$prj_path >> $SCRIPT_DIR/diffile_$prj_path

done

cd $INT_RLS_DIR/scripts/
export BIN_BRANCH_DIR=$EXT_RLS_DIR
echo "BIN_BRANCH_DIR=$BIN_BRANCH_DIR"
echo "Calling ./prepare_guzzi_bin_release.sh rtems"
./prepare_guzzi_bin_release.sh rtems
echo "Exit status: `echo $?` "
