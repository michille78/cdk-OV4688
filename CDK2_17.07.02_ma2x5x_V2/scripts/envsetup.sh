MOV_PRJDIR_ROOT=$(readlink -e $(dirname $(dirname ${BASH_SOURCE[0]})))

if [ "$MOV_PRJDIR_ROOT" == "" ]; then
	echo "Has something wrong. Can't find root folder!!!"
fi;

export MOV_INCDIR_OSAL=$MOV_PRJDIR_ROOT/osal/include
export MOV_INCDIR_HAL=$MOV_PRJDIR_ROOT/guzzi/modules/hal/include/
export MOV_INCDIR_DTP_SRV=$MOV_PRJDIR_ROOT/dtp/server/source/include/
export MOV_INCDIR_ALGOS=$MOV_PRJDIR_ROOT/algorithms/source/include
export MOV_INCDIR_GZZ=$MOV_PRJDIR_ROOT/guzzi/modules/include/
export MOV_INCDIR_IPIPE = $(TOPDIR)/guzzi/ipipe_simulator/BayerISP/components/ipipeClient/include/
export MOV_INCDIR_HIF=$MOV_PRJDIR_ROOT/hif

export MOV_BLDDIR=$MOV_PRJDIR_ROOT/build
export BLDDIR=$MOV_BLDDIR
export INSDIR=$MOV_BLDDIR
export DIRINSLIB=$INSDIR/lib/

echo "=============================================================================="
echo "ROOT folders:"
echo "  Source:         $MOV_PRJDIR_ROOT"
echo "  Build:          $MOV_BLDDIR"
echo "  Install:        $INSDIR"
echo "  Ins_lib:        $DIRINSLIB"
echo ""
echo "Include folders:"
echo "  OSAL:           $MOV_INCDIR_OSAL"
echo "  DTP Server:     $MOV_INCDIR_DTP_SRV"
echo "  HAL:            $MOV_INCDIR_HAL"
echo "  ALGOS:          $MOV_INCDIR_ALGOS"
echo "  HIF:            $MOV_INCDIR_HIF"
echo "  GZZ:            $MOV_INCDIR_GZZ"
echo "=============================================================================="
echo ""

