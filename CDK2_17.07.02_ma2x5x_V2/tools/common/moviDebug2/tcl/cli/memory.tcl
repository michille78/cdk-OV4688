# ***************************************************************************
# Copyright (C) 2015 Movidius Ltd. All rights reserved
# ---------------------------------------------------------------------------
# File       : cli/memory.tcl
# Description: Getting and setting data from memory/registers
# Created on : Sep 14, 2015
# Author     : Hanos-Puskai Peter (peter.hanos@movidius.com)
# ***************************************************************************

namespace eval mdbg {

    namespace eval cli::Memory {

        namespace path ::mdbg

        cli::MultiCmdArgParser create ArgumentParser -auto -brief {
            Memory operations (get/set/fill)
        } memCmd {
            get {
                -brief "Get value from memory location."
                -description {
                    Display or return value of specified address, register, symbol, variable or expression.

                    *** ALIASES

                    ALIAS       COMMAND
                    mgetv       mget -value
                    mgetb       mget -binary
                    mgets       mget -asciiz

                    *** EXAMPLES

                    Display 32-bit unsigned value at some address:
                    % mget 0x14400010

                    Display the value of the LRT PC register:
                    % mget PC -target LRT

                    Display the value of the local variable called PC on LRT:
                    % mget -target LRT -var PC

                    Display the value of the global symbol called PC as the LRT sees it:
                    % mget -target LRT -sym PC

                    Display the values of 32 input registers of LRT as signed integers, starting from I0:
                    % mget I0 -reg -type int -count 32 -target LRT

                    Display the V0 register value of Shave 0 (will show four rows of values):
                    % mget V0 -target S0 -type float

                    Get PC register value and the value of the next 2 registers of LRT:
                    % mget pc -target lrt -count 3

                    Get the value of the I0 register as two half-precision floating point values:
                    % mget i0 -type float -size 2 -count 2

                    Return 8 double precision and 16 single precision LEON floating point register values\
                    (little endian) as a list of two lists, first element has 8 values, second has 16 values:
                    % mget f0 -size [expr 8*8+16*4] -binary -scan q8r16
                }
            }
            set {
                -brief "Set value to memory location."
                -description {
                    Set value of specified address, register, symbol, variable or expression.

                    *** EXAMPLES

                    Set a 32-bit word representing the value 10 at address:
                    % mset 0x14400010 10

                    Set PC register value of LRT:
                    % mset -target LRT -reg PC 0x76000000

                    Set the variable named "PC" variable tLRT:
                    % mset -target LRT -var PC  10

                    Set the value of the global symbol named "PC",\
                    as if performed by the LeonRT (updating the L1+L2 cache):
                    % mset -target LRT -sym PC 15

                    Set athe IEEE 32-bit float representation of 4.3 in the I0 register:
                    % mset -reg i0 -type float 4.3

                }
            }
            fill {
                -brief "Fill memory location with pattern."
                -description {
                    Fill value of specified address, register, symbol, variable or expression with pattern.

                    *** EXAMPLES

                    Fill an array with the byte value 255:
                    % mfill my255Array -format c 255

                    Fill an array with the Ascii pattern "Hello!":
                    % mfill myHelloArray "Hello!"

                    Fill an array with the  pattern `0xCAFEBABE`:
                    % mfill myCafeArray -format i 0xCAFEBABE

                    Fill an array with the 4 byte pattern `0xDEAD`:
                    % mfill myDeadArray -format s 0xDEAD

                    Fill the first 256KiB of DDR with zeros, using the JTAG interface (faster):
                    % mfill -jtag -reg DDR_BASE_ADR -size [expr 256*1024] -zero

                    Fill the same area with the pattern "Hello!\r\n":
                    % mfill -jtag -reg DDR_BASE_ADR -size [expr 256*1204] -format a*cc Hello! 13 10
                }
            }
            dump {
                -brief "Dump memory in a canonical hexdump-like format."
                -description {
                    Display specified number of values from specified address in a humman readable format.

                    *** EXAMPLES

                    Display 10 32-bit words from DDR in hexdump-like format\
                    (hexadecimal little endian 32-bit words + ASCII):
                    % mdump 0x70000000 10
                }
            }
            get - set - fill {
                -args {
                    location(1) {
                        -optional
                        -var where
                        -brief "Memory location - address, register, symbol, variable or expression (before switches)."
                        -description {
                            One and only one of the `location(*)` arguments must be provided.

                            The type will be tried to be detected automatically, unless one of\
                            "-addr", "-reg", "-sym", "-var", "-expr" options is also given.
                        }
                    }
                    {-addr -reg -sym -var -expr -id} {
                        -flag
                        -opt-param    location(2)
                        -opt-var      where
                        -hidden
                    }

                    -addr     {-brief "Treat `location(*)` as numeric address."}
                    -reg      {-brief "Treat `location(*)` as register name."}
                    -sym      {-brief "Treat `location(*)` as (global) symbol."}
                    -var      {-brief "Treat `location(*)` as (local) variable."}
                    -expr     {-brief "Treat `location(*)` as expression."}
                    -id       {-brief "Treat `location(*)` as TCF Symbol ID"}
                    {-addr -reg -sym -var -expr -id} {
                        -description {
                            One and only one of the `location(*)` arguments must be provided.
                        }
                    }
                }
                -mutex {
                    {-addr -reg -sym -var -expr -id}
                    {-frame {-addr -reg}}
                }
            }
            get - set {
                -args {
                    -type {
                        -choice {int unsigned float pointer binary asciiz}
                        -optional
                        -synopsis {
                            ?-type typeclass?
                        }
                        -brief "Force type class for value."
                        -description {
                            Affects display/returned value (get) or binary format of scanned string value (set).

                            The following type classes are recognized:

                            int      - signed integer 1, 2, 4, 8 bytes
                            unsigned - unsigned int   1, 2, 4, 8 bytes
                            float    - floating point    2, 4, 8 bytes
                            pointer  - pointer              4, 8 bytes
                            binary   - raw binary string, any size
                            asciiz   - null terminated ASCII string or string pointer
                        }
                    }
                }
            }
            fill {
                -args {
                    -size {
                        -type integer
                        -var size
                        -optional
                        -brief "Specifiy fill size"
                        -description {
                            Default is to fill the entire `location`.
                        }
                    }
                    -zero {
                        -flag
                        -brief "Fill with zero"
                        -description {
                            Data is zeros.

                            Cannot be used with "-offset", "-length", "-format" and "-data"
                        }
                    }
                    -offset {
                        -type integer
                        -var srcSliceOffset
                        -default 0
                        -brief "Take data from offset `srcSliceOffset` of input."
                        -description {
                            Default is to take from beginning of the binary formatted input.
                        }
                    }
                    -length {
                        -type integer
                        -var srcSliceLength
                        -optional
                        -brief "Take only `srcSliceLength` bytes from input."
                        -description {
                            Default is to take all of the binary formatted input from ?srcSliceOffset? to end (see "-offset").
                        }
                    }
                    -format {
                        -optional
                        -var tclBinaryFormat
                        -brief "Format non-binary input argument(s)."
                        -description {
                            Normally the input data should already be in raw binary format.
                            This switch allows formatting non-binary data using Tcl's built-in "binary format" command.
                            The `tclBinaryFormat` argument is the same as for the aforementioned comand's `format` argument.

                            The formatted binary input will be optionally sliced according to the "-offset" and "-length" arguments\
                            and used as fill pattern.

                            Tcl `binary format` specfiers quick reference:

                            `a` : Stores a byte string in the output string. Every character is taken as modulo 256 (i.e. the low byte of every character is used, and the high byte discarded)
                            `A` : Same as `a`, except that spaces are used for padding instead of nulls.
                            `b` : Stores a string of binary digits in low-to-high order within each byte in the output string.
                            `B` : Same as `b` except that the bits are stored in high-to-low order within each byte.
                            `H` : Stores a string of count hexadecimal digits in high-to-low within each byte in the output string.
                            `h` : Same as `H` except that the digits are stored in low-to-high order within each byte.
                            `c` : Stores one or more 8-bit integer values in the output string.
                            `s` : Same as `c` except that it stores one or more 16-bit integers in little-endian byte order in the output string.
                            `S`:  Same as `s` except that it stores one or more 16-bit integers in big-endian byte order in the output string.
                            `t`:  (mnemonically "tiny") same as `s` and `S` except that it stores the 16-bit integers in the output string in the native byte order of the machine.
                            `i`:  Same as `c` except that it stores one or more 32-bit integers in little-endian byte order in the output string.
                            `I`:  Same as `i` except that it stores one or more one or more 32-bit integers in big-endian byte order in the output string.
                            `w`:  Same as `c` except that it stores one or more 64-bit integers in little-endian byte order in the output string.
                            `W`:  Same as `w` except that it stores one or more one or more 64-bit integers in big-endian byte order in the output string.
                            `m`:  (mnemonically the mirror of `w`) is the same as `w` and `W` except that it stores the 64-bit integers in the output string in the native byte order of the machine where the Tcl script is running.
                            `f`:  Same as `c` except that it stores one or more one or more single-precision floating point numbers in the machine's native representation in the output string.
                            `r`:  (mnemonically "real") same as `f` except that it stores the single-precision floating point numbers in little-endian order.
                            `R`:  Same as `r` except that it stores the single-precision floating point numbers in big-endian order.
                            `d`:  Same as `f` except that it stores one or more one or more double-precision floating point numbers in the machine's native representation in the output string.
                            `q`:  (mnemonically the mirror of `d`) is the same as `d` except that it stores the double-precision floating point numbers in little-endian order.
                            `Q`:  Same as `q` except that it stores the double-precision floating point numbers in big-endian order.
                            `x`:  Stores count `null` bytes in the output string. This type does not consume an argument.
                            `X`:  Moves the cursor back count bytes in the output string. If count is * or is larger than the current cursor position, then the cursor is positioned at location 0 so that the next byte stored will be the first byte in the result string. If count is omitted then the cursor is moved back one byte. This type does not consume an argument.
                            `@`:  Moves the cursor to the absolute location in the output string specified by count. Position 0 refers to the first byte in the output string. If count refers to a position beyond the last byte stored so far, then null bytes will be placed in the uninitialized locations and the cursor will be placed at the specified location. If count is *, then the cursor is moved to the current end of the output string. If count is omitted, then an error will be generated. This type does not consume an argument.

                            To determine what the native byte order of the machine is, refer to the `byteOrder` element of the `tcl_platform` array.
                        }

                        -complete-script {
                            mdbg::shell::autocomplete::filterList {
                                c s i w a* b* r q
                                H* H2 H4 H8 B*
                                S I W R Q
                            } $PREFIX*
                        }
                    }
                }
            }
            get - set {
                -args {
                    -size {
                        -type integer
                        -optional
                        -brief "Specifiy size of location."
                        -description {
                            This is usually used in conjuction with the "-type" argument,
                            to override the detected size of memory location.

                            NOTE: not all -type(class) / -size combinations are legal, see "-type".
                        }
                    }
                    -endian {
                        -choice {little big}
                        -default little
                        -brief "Force endiannes of value."
                        -description {
                            Only little-endian is supported.
                        }
                        -hidden
                    }
                }
            }
            get {
                -args {
                    -value {
                        -flag
                        -brief "Return value."
                        -description {
                            Only return the (formatted/structured) value of the memory location.

                            NOTE:
                            Supresses display by default, but this can be reenabled by the "-full" flag.
                            Structures and arrays may be returned as Tcl dictionary/list hierarchies.

                            Incompatible with "-quiet" flag.
                        }
                    }
                    -binary {
                        -brief "-type binary -value"
                        -description {
                            Return binary value directly.

                            Cannot be used in conjuction with "-type" and\
                            whatever the "-value" switch is incompatible with\
                            (e.g. "-quiet").
                        }
                        -flag
                    }
                    -scan {
                        -var tclBinaryFormat
                        -optional
                        -requires {-binary}
                        -brief "Scan binary value."
                        -description {
                            Allows scanning non-binary data using Tcl's built-in "binary scan" command.
                            The `tclBinaryFormat` argument is passed along as `binary scan`'s `format` argument.

                            NOTE
                            A list will be returned if the `binary scan` command would have required more than one\
                            variable as its remaining arguments.

                            This switch requires the use of the "-binary" switch.

                            Tcl `binary scan` specifiers quick reference:

                            `a`: The data is a byte string.
                            'A': This form is the same as `a`, except trailing blanks and nulls are stripped from the scanned value before it is stored.
                            `b`: The data is turned into a string of count binary digits in low-to-high order represented as a sequence of “1” and “0” characters.
                            `B`: Same as `b`, except the bits are taken in high-to-low order within each byte
                            `H`: The data is turned into a string of hexadecimal digits in high-to-low order.
                            `h`: Same as `H`, except the digits are taken in reverse (low-to-high) order within each byte
                            `c`: The data is turned into 8-bit signed integers and stored as a list.\
                            `s`: The data is interpreted as (a list of) 16-bit signed integers represented in little-endian byte order.
                            `S`: Same as `s` except that the data is interpreted as count 16-bit signed integers represented in big-endian byte order.
                            `t`: The data is interpreted as 16-bit signed integers represented in the native byte order.
                            `i`: The data is interpreted as 32-bit signed integers represented in little-endian byte order.
                            `I`: Same as `i` except that the data is interpreted as count 32-bit signed integers represented in big-endian byte order. For example,
                            `n`: The data is interpreted as 32-bit signed integers represented in the native byte order of the machine running the Tcl script.
                            `w`: The data is interpreted as 64-bit signed integers represented in little-endian byte order.
                            `W`: Same as `w` except that the data is interpreted as count 64-bit signed integers represented in big-endian byte order.
                            `m`: The data is interpreted as 64-bit signed integers represented in the native byte order of the machine running the Tcl script.
                            `f`: The data is interpreted as single-precision floating point numbers in the machine's native representation.
                            `r`: Same as `f` except that the data is interpreted as count single-precision floating point number in little-endian order.
                            `R`: Same as `f` except that the data is interpreted as count single-precision floating point number in big-endian order.
                            `d`: Same as `f` except that the data is interpreted as count double-precision floating point numbers in the machine's native representation.
                            `q`: Same as `d` except that the data is interpreted as count double-precision floating point number in little-endian order.
                            `Q`: Same as `d` except that the data is interpreted as count double-precision floating point number in big-endian order.
                            `x`: Moves the cursor forward `count` bytes in string. If `count` is "*" or is larger than the number of bytes after the current cursor position,\
                            then the cursor is positioned after the last byte in string. If `count` is omitted, then the cursor is moved forward one byte.\
                            Note that this type does not consume an argument.
                            `X`: Moves the cursor back `count` bytes in string. If count is "*" or is larger than the current cursor position, then the cursor is positioned at location 0 so that the next byte scanned will be the first byte in string. If count is omitted then the cursor is moved back one byte. Note that this type does not consume an argument.
                            `@`: Moves the cursor to the absolute location in the data string specified by `count`. Note that position 0 refers to the first byte in string.\
                            If count refers to a position beyond the end of string, then the cursor is positioned after the last byte.

                            To determine what the native byte order of the machine is, refer to the `byteOrder` element of the `tcl_platform` array.
                        }
                    }
                    -asciiz {
                        -brief "-type asciiz -value"
                        -description {
                            Return value as decoded ASCII string.

                            Location will be treated as if start address of a null terminated string,\
                            EXCEPT when it's detected to be a pointer, in this case it will be dereferenced.
                            Location is read until the null terminator or the limit is reached.

                            The "-size" option limits the size of the returned string.

                            Cannot be used in conjuction with "-binary" switch.
                        }
                        -flag
                    }
                    -count {
                        -type integer
                        -optional
                        -brief "Number of items beginning at location."
                        -description {
                            Default is 1.

                            For scalars and structures, if specified, `count - 1` items following the specified
                            location will also be retrieved.

                            For arrays, it will retrieve the first `count` elements of the array.

                            NOTES:
                            Causes the return value to be a list!
                            Some local vector register names are configured to set this option automatically!
                            "-structdepth" is adjusted to at least 1 in when this is specified for an array variable.

                            Cannot be used together with final `count` argument.
                        }
                    }
                    -structdepth {
                        -var maxDepth
                        -type integer
                        -optional
                        -synopsis {
                            ?-[struct]depth?
                        }
                        -brief "Structure fields/Array items recursion limit."
                        -description {
                            Default is 0.
                            Determines how deeply recursively the display of a structure/array should be performed.

                            NOTE
                            ALL OF THE DATA is retrieved first and this only affects the display!
                        }
                    }
                    -depth {
                        -var maxDepth
                        -type integer
                        -optional
                        -hidden
                    }
                    -quiet {
                        -flag
                        -brief "Don't print anything. Return data as Tcl structure (list/dictionary)."
                        -description {
                            Normally the command shows a nice tabular display of the information and does not return a value.
                            This can be suppressed and the machine-readable format will be returned instead.

                            NOTES:
                            This option is for advanced cases and internal reuse.
                            The "-value" flag is better suited for typical use-cases.

                            Specifying the "-count" option makes the return value a list of dictionaries, even when "count" is 1.
                            This also applies for some vector registers where's an implicit count.
                        }
                        -hidden
                    }
                    -full {
                        -flag
                        -brief "Display full information."
                        -description {
                            This is the default behaviour, so this switch only needs to be explicitly given when "-value" is also present.

                            This option is for scripting, when value is needed but the table should be also displayed.
                        }
                        -hidden
                    }
                    {-A -R -S -V -X} {
                        -flag -hidden
                        -opt-param location(2)
                        -opt-var   where
                        -opt-hidden
                    }
                    -A {-brief "-value -addr"}
                    -R {-brief "-value -reg"}
                    -S {-brief "-value -sym"}
                    -V {-brief "-value -var"}
                    -X {-brief "-value -expr"}
                }
                -mutex {
                    {{-value -binary -asciiz} -quiet}
                    {-type -binary -asciiz}
                    {-depth -structdepth}
                    {-A -R -S -V -X {
                        -addr -reg -sym -var -expr -id
                        -value -quiet -full
                        }
                    }
                    {-frame {-A -R}}
                }
            }
            set {
                -args {
                    {-list -dict} {
                        -flag
                    }
                    -binary {
                        -brief "-type binary -size <length of `what`>"
                        -description {
                            Treat `what` as binary data and write all of it
                            at the specified location.

                            Cannot be used in conjuction with "-type" and "-size"
                        }
                        -flag
                    }
                    -list {
                        -brief "Set array from list."
                        -description {
                            !! NOT IMPLEMENTED !!
                            Takes each list element from supplied input and sets the array's elements individually.
                        }
                    }
                    -dict {
                        -brief "Set array/structure from list/dictionary."
                        -description {
                            !! NOT IMPLEMENTED !!
                            Takes each dictionary key/value item from supplied input and sets the structure fields individually.
                        }
                    }
                }
                -mutex {
                    {-list -dict}
                    {-binary {-type -size}}
                }

            }
            get - set - fill {
                -args {
                    -target {
                        -default {}
                        -brief "Operate on specific target."
                        -description {
                            If missing, current target is used.
                        }
                        -complete-script {
                            cli::Target::getValidTargets $PREFIX*
                        }
                    }
                    -frame {
                        -type integer
                        -optional
                        -brief "Specify reference stack frame."
                        -description {
                            Search for variable(s) on stack frame context #`frame`.
                            Default is 0 (top frame) for "-var", otherwise uses non-frame context.
                        }
                    }
                    -jtag {
                        -flag
                        -brief "Use direct JTAG interface for memory I/O."
                        -description {
                            This flag forces the command to bypass TCF's memory context infrastructure to do memory I/O.
                            "JTAG I/O" is done directly using the "jtag" command which will use direct protocol commands
                            from the "::mvproto" namespace.
                            It is useful in certain scenarios where cache coherence is not a problem.
                            Incompatible with "-mem" argument.
                        }
                    }
                    -mem {
                        -brief "Target for memory I/O."
                        -description {
                            Normally the memory operations go through the target context's associated memory context.
                            This argument allows specifying a different context to do I/O through.
                            Incompatible with "-jtag" flag.
                        }
                        -optional
                        -var memTarget
                        -complete-script {
                            cli::Target::getValidTargets $PREFIX*
                        }
                    }
                    location(3) {
                        -optional
                        -var where
                        -brief "Memory location - address, register, symbol. variable or expression (after switches)."
                        -description {
                            One and only one of the `location(*)` arguments must be provided.

                            The type will be tried to be detected automatically, unless one of\
                            "-addr", "-reg", "-sym", "-var", "-expr" flags is also given.
                        }
                    }
                }
                -mutex {
                    {-jtag -mem}
                    {location(1) location(2) location(3)}
                }
                -requires {
                    {location(1) location(2) location(3)}
                }
                -arg-complete-script {
                    {location(1) location(3)} {
                        # no need to provide for location(2) since it's handled by location(3)
                        mdbg::cli::Memory::ArgumentParser autocomplete-location $PARAMS $PREFIX
                    }
                }
            }
            set {
                -args {
                    what {
                        -brief "Input value."
                        -description {
                            Its form should be compatible with the memory location's type.
                        }
                    }
                }
            }
            get {
                -args {
                    count {
                        -type integer
                        -optional
                        -brief "Number of values beginning with address."
                        -description {
                            Same as "-count" option. Added for convenience.

                            Cannot be used together with "-count".
                        }
                    }
                }
                -mutex {
                    {-count count}
                }
            }
            fill {
                -args {
                   data {
                       -brief "Input value(s)."
                       -description {
                           Normally it will be a binary/ascii string value, unless the "-format" option is also present.

                           In this case there can be multiple arguments, but at least one must be present.

                           See: "-format" option.
                       }
                       -multi
                       -optional
                   }
                }
                -mutex {
                    {-zero {-offset -length -format data}}
                }
                -requires {
                    {data -zero}
                }
            }
            dump {
                -args {
                    -jtag {
                        -flag
                        -brief "Use direct JTAG interface"
                    }

                    location {
                        -var where
                        -brief "Memory location - address, register, symbol, variable or expression."
                        -description {
                            Start Address from where values are being displayed.

                            The type will be tried to be detected automatically.
                        }
                        -complete-script {
                            mdbg::cli::Memory::ArgumentParser autocomplete-location $PARAMS $PREFIX
                        }
                    }
                    count {
                        -type integer
                        -brief "The number of words being displayed."
                        -description {
                            Must be an integer.
                        }
                    }
                }
            }
        }

        oo::objdefine ArgumentParser {

            method setupArgs { {level 0} } {
                incr level
                upvar $level memCmd memCmd
                if {$memCmd == "get"} {
                    upvar $level value value
                    foreach {shortVar longVar} {
                             A        addr
                             S        sym
                             R        reg
                             V        var
                             X        expr
                    } {
                        upvar $level $shortVar short
                        upvar $level $longVar long
                        if {$short} {
                            lassign {1 1} long value
                        }
                        unset short
                    }
                    upvar $level maxDepth maxDepth
                    if {![info exists maxDepth]} {
                        set maxDepth 0
                    }
                }

                my CheckArgs $level
                my SetupCommonArgs $level
                my SetupContext $level
            }

            method createSymInfo {} {
                uplevel 1 {
                    set symInfo [expr {
                        $addr ? [cli::Memory::createAddrSymInfo $where $contextId] :
                        $reg ? [cli::Memory::createRegSymInfo $where $contextId] :
                        $sym ? [cli::Memory::createVarSymInfo $where $contextId] :
                        $id ? [cli::Memory::createIdSymInfo $where $symCache] :
                        $var ? [cli::Memory::createVarSymInfo $where $frameId $PC] :
                        $expr ? [cli::Memory::createExprSymInfo $where $frameId] :
                        [cli::Memory::getSymInfo $where $contextId $frameId]
                    }]
                    if {![dict exists $symInfo NAME]} {
                        dict set symInfo NAME $where
                    }
                }
            }

            method fillSymInfo {} {
                uplevel 1 {
                    if {[info exists binary] && $binary} {
                        dict set symInfo TYPE binary
                        dict set symInfo TYPE_NAME <binary>
                        # mset -binary ... ... what
                        if {[info exists what]} {
                            set what [binary format a* $what]
                            dict set symInfo SIZE [string length $what]
                        }
                        # mget -binary ?-scan tclBinaryFormat? ...
                        if {[info exists value]} {
                            if {[info exists tclBinaryFormat]} {
                                dict set symInfo TCL_BINARY_FORMAT $tclBinaryFormat
                            }
                            set value 1
                        }
                    } elseif {[info exists asciiz] && $asciiz || [info exists type] && $type == "asciiz"} {
                        # mget -asciiz ?-size size?
                        dict set symInfo ASCIIZ_INDIRECT 0
                        if {[dict exists $symInfo TYPE]} {
                            switch -exact [dict get $symInfo TYPE] {
                                POINTER {
                                    dict set symInfo ASCIIZ_INDIRECT 1
                                    dict set symInfo ASCIIZ_MAX_LEN 4096
                                }
                                default {
                                    switch -exact [dict get $symInfo KIND] {
                                        -addr {
                                            dict set symInfo ASCIIZ_MAX_LEN 4096
                                        }
                                    }
                                }
                            }
                        }
                        if {![dict get $symInfo ASCIIZ_INDIRECT]} {
                            if {![dict exists $symInfo ASCIIZ_MAX_LEN]} {
                                if {[dict exists $symInfo SIZE]} {
                                    dict set symInfo ASCIIZ_MAX_LEN [dict get $symInfo SIZE]
                                }
                            }
                        }
                        if {[info exists size]} {
                            dict set symInfo ASCIIZ_MAX_LEN $size
                        }
                        dict set symInfo TYPE ASCIIZ
                        dict set symInfo TYPE_NAME <asciiz>

                        if {[info exists asciiz] && $asciiz} {
                            if {[info exists value]} {
                                set value 1
                            }
                        }
                    } else {
                        if {[info exists type]} {
                            dict set symInfo TYPE $type
                            dict set symInfo TYPE_NAME "<$type>"
                        }
                    }
                    if {[info exists size]} {
                        dict set symInfo SIZE $size
                    }
                    dict set symInfo TARGET_ID $contextId

                    if {$jtag} {
                        dict set symInfo JTAG JTAG
                    } else {
                        if {![dict exists $symInfo JTAG]
                            || [info exists memTarget]
                        } {
                            dict set symInfo MEMORY_ID $memContextId
                        }
                    }
                    if {$memCmd in {fill}} {
                        dict set symInfo FILL FILL
                    }
                }
            }

            method autocomplete-location {params prefix} {
                if {[llength $params] > 0 && ![string match -* [lindex $params 0]]} {
                    return
                }

                set kind [my AutoGetSymKind $params]
                if {$kind == "-addr" || [string match -* $prefix]} {
                    return
                }
                set context [my AutoGetContext $params]

                set SORT mdbg::shell::autocomplete::sortNames

                set localRegs [list]
                set globalRegs [list]

                if {$kind in {ALL -reg}} {
                    set localRegisterMap $::mdbg::cli::Memory::LOCAL_REGISTER_MAP
                    if {$context != ""} {
                        set target [mdbg::cli::Target::getTargetNameFromContextId $context]
                        if {[dict exists $localRegisterMap $target]} {
                            set targetRegDict [dict get $localRegisterMap $target]
                            set localRegPrefix [string toupper $prefix]
                            set localRegs [dict keys $targetRegDict $localRegPrefix*]
                        }
                    }
                    set globalRegs [mdbg::findReg $prefix*]
                }

                set varNames [list]
                set symNames [list]

                if {$context != "" && $kind in {ALL -sym -var -expr}} {

                    if {[::tcf::hasContextState $context] && [::tcf::isContextSuspended $context]} {
                        set frame [my AutoGetFrame $params $context]
                        if {$kind in {-var ALL} || $frame != $context} {
                            set varNames [mdbg::cli::Symbols::getLocalVars name $frame $prefix*]
                        }
                    }
                    set afterid [after 1000 {puts $mdbg::Channels::event "please wait..."}]
                    try {
                        set symNames [mdbg::cli::Symbols::getElfSymbols name $context $prefix*]
                    } finally {
                        after cancel $afterid
                    }
                }

                concat [$SORT $varNames] [$SORT $localRegs] \
                       [$SORT [concat $symNames $globalRegs] ]
            }

            #
            #  Private
            #

            method CheckArgs {{level 0}} {
                incr level
                uplevel $level {
                    if {[info exists endian] && $endian != "little"} {
                        return -code error {-endian other than "little" not implemented}
                    }
                }
            }

            method SetupCommonArgs { {level 0} } {
                incr level
                uplevel $level {
                    if {[cli::Target::isSet] || !$jtag} {
                        set contextId [cli::Target::getContextIdFromTargetVar target]
                        set memContextId [cli::Target::getContextIdFromTargetVar memTarget target]
                    } else {
                        set contextId ""
                        set memoContextId ""
                    }
                }
            }

            method SetupContext { {level 0} } {
                incr level
                uplevel $level {
                    if {$contextId != ""} {
                        if {[info exists frame]} {
                            set frameId [cli::CallStack::getFrameId $contextId $frame]
                        } else {
                            set frameId $contextId
                            if {$sym || $reg || $addr} {
                                # symbols, registers, addresses are frame-independent
                            } else {
                                if {[catch {
                                    set frameId [tcf::getTopStackFrameId $contextId]
                                } error options]} {
                                    if {$var} {
                                        return -code error -options $options $error
                                    }
                                    if {!$expr} {
                                        cli::Warning $error
                                    }
                                }
                            }
                        }
                    } else {
                        set frameId ""
                    }
                    if {$sym} {
                        set PC 0
                    } else {
                        if {[catch {
                            set PC [tcf::getContextPC $frameId]
                        } error options]} {
                            if {$var} {
                                return -code error -options $options $error
                            }
                            set PC 0
                        }
                    }
                }
            }

            #
            #  Private (autocomplete)
            #

            method AutoGetContext {params} {
                set index [lsearch -exact $params "-target"]
                if {$index >= 0} {
                    incr index
                    set target [lindex $params $index]
                }
                if {[catch {
                    mdbg::cli::Target::getContextIdFromTargetVar target
                } context]} {
                    return
                }
                return $context
            }

            method AutoGetFrame {params context} {
                set index [lsearch -exact $params "-frame"]
                if {$index >= 0} {
                    incr index
                    set frame [lindex $params $index]
                } else {
                    set frame 0
                }
                set frameId [tcf::StackTrace::getChildrenRange $context $frame $frame]
                expr {$frameId == {} ? $context : $frameId}
            }

            method AutoGetSymKind {params} {
                if {{-id} in $params} {
                    return -id
                }
                foreach {short full} {-V -var -S -sym -X -expr -R -reg -A -addr} {
                    if {$short in $params || $full in $params} {
                        return $full
                    }
                }
                return ALL
            }

        } ; # oo::objdefine ArgumentParser

        # If Tcf not available
        oo::class create DummySymbolCache {
            method unknown {args} {
                return
            }
        }

        proc SymbolCacheClass {} {
            if {[info object isa class ::tcf::SymbolCache]} {
                return ::tcf::SymbolCache
            }
            return [namespace current]::DummySymbolCache
        }

        proc getSymInfo {name contextId frameId {pc 0}} {
            if {[string is integer -strict $name]} {
                tailcall createAddrSymInfo $name $contextId
            }
            if {![catch {
                createRegSymInfo $name $contextId
            } result]} {
                return $result
            }
            if {![catch {
                createVarSymInfo $name $frameId $pc
            } result]} {
                return $result
            }
            if {![catch {
                createExprSymInfo $name $frameId
            } result]} {
                return $result
            }
            return -code error "symbol \"$name\" not found"
        }

        proc createAddrSymInfo {addr contextId} {
            dict set result KIND -addr
            dict set result ADDRESS [cli::hexAddr $addr]
        }

        proc createRegSymInfo {regName contextId} {
            variable LOCAL_REGISTER_MAP
            if {$contextId != ""} {
                set target [cli::Target::getTargetNameFromContextId $contextId]
                set localRegName [string toupper $regName]
                if {[dict exists $LOCAL_REGISTER_MAP $target $localRegName]} {
                    lassign [dict get $LOCAL_REGISTER_MAP $target $localRegName] regName count
                    dict set result NAME $regName
                    dict set result JTAG JTAG
                    if {$count != {}} {
                        dict set result COUNT $count
                    }
                } else {
                    unset localRegName
                }
            }
            dict set result KIND -reg
            dict set result ADDRESS [mdbg::getRegAddr $regName]
        }

        proc createIdSymInfo {id symCache} {
            dict set result KIND "-id"
            dict set result SYMBOL_ID $id
            set name [$symCache getField $id NAME]
            if {"VARARG" in [$symCache getField $id FLAGS]} {
                append name "..."
            }
            dict set result NAME $name
        }

        proc createVarSymInfo {name contextId {pc 0}} {
            dict set result KIND [expr {$pc ? "-var" : "-sym"}]
            set sym [cli::Symbols::findByName $contextId $name $pc]
            dict set result SYMBOL_ID [dict get $sym ID]
            dict set result ADDRESS [tcf::getSymbolField $sym ADDRESS]
            dict set result SIZE [tcf::getSymbolField $sym SIZE]
            dict set result CONTEXT_ID $contextId
        }

        proc createExprSymInfo {name contextId {lang {}}} {
            dict set result KIND -expr
            dict set result ExprObj [tcf::ExprContext new $contextId $lang $name]
        }

        proc fillSymTypeAddrSize {symInfoVar symbolCache} {
            upvar 1 $symInfoVar symInfo
            set kind [dict get $symInfo KIND]
            switch -exact $kind {
                -sym -
                -var -
                -id {
                    set symId [dict get $symInfo SYMBOL_ID]
                    dict set symInfo SIZE [$symbolCache getField $symId SIZE]
                    dict set symInfo TYPE [$symbolCache getField $symId TYPE_CLASS]
                    dict set symInfo TYPE_ID [$symbolCache getField $symId TYPE_ID]
                    lassign [$symbolCache getFieldList $symId {
                        ADDRESS REGISTER VALUE
                    }]  address register value
                    if {$address != {}} {
                        dict set symInfo ADDRESS $address
                    } else {
                        if {$register != {}} {
                            dict set symInfo REGISTER $register
                            if {$value != {}} {
                                set data [cli::Binary::formatBase64 $value]
                                dict set symInfo DATA $data
                            }
                        }
                    }
                }
                -expr {
                    set exprObj [dict get $symInfo ExprObj]
                    dict set symInfo SIZE [$exprObj getField SIZE]
                    dict set symInfo TYPE [$exprObj getField CLASS]
                    set typeId [$exprObj getType]
                    dict set symInfo TYPE_ID $typeId
                }
                -addr - -reg {
                    dict set symInfo TYPE unsigned
                    dict set symInfo SIZE 4
                }
                default {
                    return -code error "$kind not supported"
                }
            }
        }

        proc binaryTypeClass {type} {
            switch -exact $type {
                int - INTEGER {
                    return INT
                }
                unsigned - CARDINAL {
                    return UNSIGNED
                }
                float - REAL {
                    return FLOAT
                }
                pointer - POINTER {
                    return POINTER
                }
                "" - binary {
                    return BINARY
                }
                default {
                    return -code error "type \"$type\" not supported"
                }
            }
        }

        proc scanData {data binaryClass dataSize} {
            set value [cli::Binary::scanData $data $binaryClass $dataSize]
            if {$binaryClass == {UNSIGNED}} {
                set value [format "%#.[expr {$dataSize * 2}]x" $value]
            }
            set value
        }

        proc parseEnum {data symInfoVar symCache} {
            upvar 1 $symInfoVar symInfo
            if {[dict exists $symInfo TYPE_ID]} {
                set typeId [dict get $symInfo TYPE_ID]
                set base64 [binary encode base64 $data]
                foreach enumId [$symCache getChildren $typeId] {
                    lassign [$symCache getFieldList $enumId {
                        CLASS NAME VALUE}] symClass enumName enumValue
                    if {$base64 == $enumValue} {
                        if {$symClass ni {{} VALUE}} {
                            cli::Warning "enum value found but has class $symClass"
                        }
                        return $enumName
                    }
                }
            }
            set size [dict get $symInfo SIZE]
            cli::Binary::scanInt $data $size
        }

        proc formatEnum {valueOrName symInfoVar symCache} {
            upvar 1 $symInfoVar symInfo

            if {![string is integer -strict $valueOrName]} {
                if {[dict exists $symInfo TYPE_ID]} {
                    set typeId [dict get $symInfo TYPE_ID]
                    foreach enumId [$symCache getChildren $typeId] {
                        lassign [$symCache getFieldList $enumId {
                            CLASS NAME VALUE}] symClass enumName enumValue
                        if {$valueOrName == $enumName} {
                            if {$symClass ni {{} VALUE}} {
                                cli::Warning "enum value found but has class $symClass"
                            }
                            return [cli::Binary::formatBase64 $enumValue]
                        }
                    }
                }
            }
            set size [dict get $symInfo SIZE]
            cli::Binary::formatInt $size $valueOrName
        }

        proc parseComposite {data structInfoVar symCache maxDepth {level 0}} {
            incr level
            if {$maxDepth < $level} {
                return
            }
            set indent [string repeat {  } $level ]

            upvar 1 $structInfoVar parentInfo
            set parentType [dict get $parentInfo TYPE_ID]
            set parentName [dict get $parentInfo NAME]
            foreach childId [$symCache getChildren $parentType] {
                cli::CheckInterrupt

                lassign [$symCache getFieldList $childId {
                    NAME CLASS TYPE_CLASS OFFSET SIZE TYPE_ID FLAGS
                }] name symClass dataType dataOffset dataSize childTypeId flags

                if {$symClass ni {{} REFERENCE}} {
                    continue
                }
                if {$dataOffset == {} || $dataSize == {}} {
                    continue
                }
                set childTypeName [getTypeName $childTypeId $symCache]

                set isBase [expr {{INHERITANCE} in $flags}]
                if {$isBase} {
                    set childName ${indent}::${childTypeName}
                } else {
                    set childName ${indent}.${name}
                }

                set childInfo [dict create \
                    NAME $childName \
                    TYPE_ID $childTypeId TYPE_NAME $childTypeName \
                    TYPE $dataType OFFSET $dataOffset SIZE $dataSize \
                ]

                parseSubData $parentInfo $data $dataOffset $dataSize $dataType \
                    childInfo $symCache $maxDepth $level

                if {$isBase} {
                    dict lappend parentInfo BASES $childInfo
                } else {
                    dict lappend parentInfo FIELDS $childInfo
                }
            }
        }

        proc getArrayElemSize {arrayInfo symCache} {
            set arrayType [dict get $arrayInfo TYPE_ID]
            $symCache get $arrayType BASE_TYPE SIZE
        }

        proc getArrayLength {arrayInfo symCache} {
            if {[dict exists $arrayInfo LENGTH]} {
                dict get $arrayInfo LENGTH
            } else {
                set arrayType [dict get $arrayInfo TYPE_ID]
                $symCache getField $arrayType LENGTH
            }
        }

        proc parseArray {data arrayInfoVar symCache maxDepth {level 0}} {
            incr level
            if {$maxDepth < $level} {
                return
            }
            set indent [string repeat {  } $level ]

            upvar 1 $arrayInfoVar parentInfo
            set arrayType [dict get $parentInfo TYPE_ID]
            set arrayLength [getArrayLength $parentInfo $symCache]

            lassign [$symCache get $arrayType BASE_TYPE ID NAME TYPE_CLASS SIZE] \
                childTypeId childTypeName dataType dataSize

            for {set index 0} {$index < $arrayLength} {incr index} {
                cli::CheckInterrupt
                set dataOffset [expr {$index * $dataSize}]
                set childName "$indent[format {[%d]} $index]"

                set childInfo [dict create \
                    NAME $childName \
                    TYPE_ID $childTypeId TYPE_NAME $childTypeName \
                    TYPE $dataType OFFSET $dataOffset SIZE $dataSize \
                ]

                parseSubData $parentInfo $data $dataOffset $dataSize $dataType \
                    childInfo $symCache $maxDepth $level

                dict lappend parentInfo ELEMENTS $childInfo
            }
        }

        proc parseSubData {parentInfo data dataOffset dataSize dataType childInfoVar symCache maxDepth level} {
            upvar 1 $childInfoVar childInfo

            if {[dict exists $parentInfo ADDRESS]} {
                set parentAddress [dict get $parentInfo ADDRESS]
                if {![dict exists $childInfo ADDRESS]} {
                    dict set childInfo ADDRESS [cli::hexAddr \
                        [expr {$parentAddress + $dataOffset}]
                    ]
                }
            }

            set subData [string range $data $dataOffset [expr {$dataOffset + $dataSize - 1}]]
            switch -exact $dataType {
                ENUMERATION {
                    set value [cli::Memory::parseEnum $subData childInfo $symCache]
                }
                COMPOSITE {
                    set value [cli::Memory::fillSymTypeName childInfo $symCache]
                    cli::Memory::parseComposite $subData childInfo $symCache $maxDepth $level
                }
                ARRAY {
                    set value [cli::Memory::fillSymTypeName childInfo $symCache]
                    cli::Memory::parseArray $subData childInfo $symCache $maxDepth $level
                }
                default {
                    set binaryClass [cli::Memory::binaryTypeClass $dataType]
                    set value [cli::Memory::scanData $subData $binaryClass $dataSize]
                }
            }
            dict set childInfo VALUE $value
        }

        proc getSymData {symInfo dataVar} {
            upvar 1 $dataVar data
            if {[dict exists $symInfo DATA]} {
                set data [dict get $symInfo DATA]
                return 1
            }
            unset -nocomplain data
            return 0
        }

        proc getSymAddress {symInfo} {
            if {[dict exists $symInfo ADDRESS]} {
                set addr [dict get $symInfo ADDRESS]
                if {$addr != {}} {
                    return $addr
                }
            }
            return -code error "symbol has no address in this context"
        }

        proc readBinaryValue {symInfoVar} {
            upvar 1 $symInfoVar symInfo
            set kind [dict get $symInfo KIND]
            set size [dict get $symInfo SIZE]
            if {$size == ""} {
                return
            }
            set jtag [dict exists $symInfo JTAG]
            if {!$jtag} {
                set targetId [dict get $symInfo TARGET_ID]
                set memContextId [dict get $symInfo MEMORY_ID]
            }
            set index [dict get $symInfo INDEX]
            set offset [expr {$index * $size}]

            switch -exact $kind {
                -addr - -reg -
                -sym  - -var -
                -id {
                    if {[getSymData $symInfo data]} {
                        return $data
                    } else {
                        dict update symInfo ADDRESS addr {
                            if {![info exists addr]} {
                                return
                            }
                            if {$offset} {
                                cli::incrAddr addr $offset
                            }
                        }
                        if {$jtag} {
                            jtag readBlock $addr $size
                        } else {
                            tcf::Memory::getBytes $memContextId $addr $size
                        }
                    }
                }
                -expr {
                    if {[dict exists $symInfo ExprObj]} {
                        set exprObj [dict get $symInfo ExprObj]
                    }
                    if {!$jtag && $targetId == $memContextId && $offset == 0} {
                        set value [$exprObj evaluate]
                        set addr [$exprObj getValueInfoField ADDRESS]
                        dict set symInfo ADDRESS $addr
                        set len [string length $value]
                        if {$len < $size} {
                            if {$addr == {}} {
                                return -code error "cannot read arbitrary size: expression has no address"
                            }
                            append value [tcf::Memory::getBytes $memContextId \
                                [expr {$addr + $len}] \
                                [expr {$size - $len}] \
                            ]
                        } elseif {$len > $size} {
                            set value [string range $value 0 [expr {$size - 1}]]
                        }
                        return $value
                    }
                    if {[dict exists symInfo ADDRESS]} {
                        set addr [dict get symInfo ADDRESS]
                    } else {
                        cli::UsingObj ::tcf::ExprContext addrExpr \
                            [$exprObj getParentID] \
                            [$exprObj getLanguage] \
                            [format {&(%s)} [$exprObj getExpression]] \
                        {
                            set ptrData [$addrExpr evaluate]
                            set ptrSize [string length $ptrData]
                            set addr [cli::Binary::scanPointer $ptrData $ptrSize]
                        }
                    }
                    if {$offset} {
                        cli::incrAddr addr $offset
                    }
                    dict set symInfo ADDRESS $addr
                    if {$jtag} {
                        jtag readBlock $addr $size
                    } else {
                        tcf::Memory::getBytes $memContextId $addr $size
                    }
                }
            }
        }

        proc readAsciiZ {symInfoVar} {
            upvar 1 $symInfoVar symInfo
            set jtag [dict exists $symInfo JTAG]
            if {!$jtag} {
                # set targetId [dict get $symInfo TARGET_ID]
                set memContextId [dict get $symInfo MEMORY_ID]
            }
            if {[dict exists $symInfo ADDRESS]} {
                set addr [dict get $symInfo ADDRESS]
            } else {
                switch -exact [dict get $symInfo KIND] {
                    -expr {
                        set exprObj [dict get $symInfo ExprObj]
                        cli::UsingObj ::tcf::ExprContext addrExpr \
                            [$exprObj getParentID] [$exprObj getLanguage] \
                            [format {&(%s)} [$exprObj getExpression]] \
                        {
                            set ptrData [$addrExpr evaluate]
                            set ptrSize [string length $ptrData]
                            set addr [cli::Binary::scanPointer $ptrData $ptrSize]
                            dict set symInfo ADDRESS $addr
                        }
                    }
                    default {
                        return -code error "<asciiz> location has no address"
                    }
                }
            }
            set maxLen [dict get $symInfo ASCIIZ_MAX_LEN]
            set blockLen 64
            set data [binary format a* ""]
            set term [binary format c 0]
            set readLen 0
            while { $maxLen > 0 } {
                set size [expr {$maxLen < $blockLen ? $maxLen : $blockLen - $addr % $blockLen}]
                set block [
                    if {$jtag} {
                        jtag readBlock $addr $size
                    } else {
                        tcf::Memory::getBytes $memContextId $addr $size
                    }
                ];
                set len [string first $term $block]
                if {$len >= 0} {
                    set block [string range $block 0 $len-1]
                    set maxLen 0
                } else {
                    incr maxLen -$size
                    incr addr $size
                }
                append data $block
            }
            dict set symInfo SIZE [string length $data]
            encoding convertfrom ascii $data
        }

        proc writeBinaryValue {symInfoVar data} {
            set WRITE {
                SET:JTAG { jtag writeBlock $addr $data }
                SET:MEM  { tcf::Memory::setBytes $memContextId $addr $data }
                FILL:JTAG { jtag fillBlock $addr $fillSize $data }
                FILL:MEM { tcf::Memory::fillBytes $memContextId $addr $fillSize $data }
            }
            upvar 1 $symInfoVar symInfo
            set kind [dict get $symInfo KIND]
            if {! [set jtag [dict exists $symInfo JTAG]]} {
                set targetId     [dict get $symInfo TARGET_ID]
                set memContextId [dict get $symInfo MEMORY_ID]
            }
            if {[set fill [dict exists $symInfo FILL]]} {
                set fillSize [dict get $symInfo SIZE]
            }
            set MODE [expr {$fill ? "FILL" : "SET" }]:[expr {$jtag ? "JTAG" : "MEM"}]
            set writeBody [dict get $WRITE $MODE]

            switch -exact $kind {
                -addr - -reg -
                -sym  - -var -
                -id {
                    set addr [getSymAddress $symInfo]
                    uplevel 0 $writeBody
                }
                -expr {
                    set exprObj [dict get $symInfo ExprObj]
                    if {![$exprObj canAssign]} {
                        error "expression is not assignable"
                    }
                    if {!$jtag && $targetId == $memContextId} {
                        if 1 {
                            $exprObj assign $data
                        } else {
                            # TODO check if this is required
                            set size [dict get $symInfo SIZE]
                            set dataSize [string length $data]
                            if {$size >= $dataSize} {
                                $exprObj assign $data
                                if {$size == $dataSize} {
                                    return
                                }
                                set data [string range $data $size end]
                                set OFFSET $size
                            }
                        }
                        return
                    }
                    set addrExpr [tcf::ExprContext new \
                        [$exprObj getParentID] [$exprObj getLanguage] \
                        [format {&(%s)} [$exprObj getExpression]]
                    ]
                    try {
                        set ptrData [$addrExpr evaluate]
                        set ptrSize [string length $ptrData]
                        set addr [cli::Binary::scanPointer $ptrData $ptrSize]
                        dict set symInfo ADDRESS $addr
                        if [info exists OFFSET] {
                            cli::incrAddr addr $OFFSET
                        }
                        uplevel 0 $writeBody
                    } finally {
                        $addrExpr destroy
                    }
                }
            }
        }

        proc cleanupSymInfo {symInfoVar} {
            upvar 1 $symInfoVar symInfo
            if {[info exists symInfo]} {
                if {[dict exists $symInfo ExprObj]} {
                    set exprObj [dict get $symInfo ExprObj]
                    dict unset symInfo ExprObj
                    $exprObj destroy
                }
            }
        }

        proc getTypeName {id symCache} {
            lassign [$symCache getFieldList $id {
                NAME TYPE_CLASS TYPE_ID BASE_TYPE_ID FLAGS}
            ]   name typeClass  typeId  baseTypeId   flags

            if {{TYPEDEF} ni $flags} {
                switch -exact $typeClass {
                    COMPOSITE {
                        set composite [expr {
                            {CLASS_TYPE} in $flags ? "class" :
                            {UNION_TYPE} in $flags ? "union" :
                            "struct"
                        }]
                        return [list $composite $name]
                    }
                    ENUMERATION {
                        return [list enum $name]
                    }
                }
            }
            if {$name != {}} {
                return $name
            }

            set isVolatile [expr {{VOLATILE_TYPE} in $flags ? " volatile" : {}}]
            set isConst [expr {{CONST_TYPE} in $flags ? " const" : {}}]

            if {$isVolatile != {} || $isConst != {}} {
                return "[getTypeName $typeId $symCache]$isConst$isVolatile"
            }

            switch -exact $typeClass {
                UNKNOWN {
                    return "<unknown>"
                }
                POINTER {
                    set OP [expr { {REFERENCE} in $flags ? "&" : "*" }]
                    return "[getTypeName $baseTypeId $symCache] $OP"
                }
                FUNCTION {
                    return "<function>"
                }
                MEMBER_POINTER {
                    return "<ptr-to-member>"
                }
                COMPLEX {
                    return "<complex>"
                }
                ARRAY {
                    set LEN [$symCache getField $id LENGTH]
                    return "[getTypeName $baseTypeId $symCache] \[$LEN\]"
                }
            }
        }

        cli::TabularDisplay create SymInfoDisplay {
            {ADDRESS TYPE_NAME SIZE} {
                -header ADDRESS
                -transform {
                    if {$ADDRESS == {}} {
                        return "<no address>"
                    }
                    if {(
                        $TYPE_NAME in {"" "<binary>" "<asciiz>" } &&
                        [string is integer -strict $ADDRESS] &&
                        [string is integer -strict $SIZE] && $SIZE > 0
                    )} {
                        set increment 16
                        if {$TYPE_NAME == "<asciiz>"} {
                            set increment 32
                        }
                        set result [list]
                        set end [expr {$ADDRESS + $SIZE}]
                        while {$ADDRESS < $end} {
                            lappend result $ADDRESS
                            mdbg::cli::incrAddr ADDRESS $increment
                        }
                        return [join $result \n]
                    }
                    return $ADDRESS
                }
            }
            {NAME} {
                -wrap char
                -expand
            }
            {INDEX SIZE} {
                -header ""
                -align right
                -transform {
                    if {$INDEX == {} || $INDEX == 0} {
                        return
                    }
                    format {<+%#x>} [expr {$INDEX * $SIZE}]
                }
            }

            {VALUE TYPE_NAME TYPE SIZE} {
                -header VALUE
                -transform {
                    if {$VALUE == "" && $SIZE > 0 && $TYPE_NAME != "<asciiz>" } {
                        return "<no value>"
                    }
                    if {$TYPE_NAME == "" && $TYPE == ""} {
                        set TYPE_NAME <binary>
                    }
                    switch -exact $TYPE_NAME {
                        <binary> {
                            join [mdbg::cli::GroupList [
                                mdbg::cli::Binary::scanByteList $VALUE %.2X
                            ] 16 ] \n
                        }
                        char -
                        "const char" {
                            set char [format %c $VALUE]
                            if {$VALUE < 0} {
                                set VALUE [expr {$VALUE & 0xFF}]
                            } else {
                                if {[string is print $char]} {
                                    return "'$char'"
                                }
                                switch -exact $char {
                                    "\n" {return {'\n'}}
                                    "\r" {return {'\r'}}
                                    "\t" {return {'\t'}}
                                    "\a" {return {'\a'}}
                                    "\b" {return {'\b'}}
                                    "\v" {return {'\v'}}
                                    "\f" {return {'\f'}}
                                }
                            }
                            set fmt [expr {
                                $char < 0x20 ? {'\%.3o'} : {'\x%.2X'}
                            }]
                            format $fmt $VALUE
                        }
                        <asciiz> {
                            multiLineCStringLiteral $VALUE 32
                        }
                        default {
                            return $VALUE
                        }
                    }
                }
            }
            KIND  {
                -skip
            }
            SIZE {
                -align right
            }
            TYPE {
                -skip
            }
            TYPE_NAME {
            }
            CONTEXT_ID {
            }
            TYPE_ID {
                -skip
            }
        }

        oo::objdefine SymInfoDisplay {
            mixin cli::ResultTable
        }

        proc multiLineCStringLiteral {str width} {
            set result [list]
            set len [string length $str]
            set first 0
            set last [expr {$width - 1}]

            while {$first < $len} {
                lappend result [
                    format {"%s"} [string map {
                        \n   \\n   \r   \\r   \t   \\t   \a   \\a   \b   \\b   \v   \\v   \f   \\f
                        \x00 \\x00 \x01 \\x01 \x02 \\x02 \x03 \\x03 \x04 \\x04 \x05 \\x05 \x06 \\x06 \x07 \\x07
                        \x08 \\x08 \x09 \\x09 \x0A \\x0A \x0B \\x0B \x0C \\x0C \x0D \\x0D \x0E \\x0E \x0F \\x0F
                        \x10 \\x10 \x11 \\x11 \x12 \\x12 \x13 \\x13 \x14 \\x14 \x15 \\x15 \x16 \\x16 \x17 \\x17
                        \x18 \\x18 \x19 \\x19 \x1A \\x1A \x1B \\x1B \x1C \\x1C \x1D \\x1D \x1E \\x1E \x1F \\x1F
                    } [string range $str $first $last] ]
                ]
                incr first $width
                incr last  $width
            }
            join $result \n
        }

        proc fillSymInfoList {resultListVar symInfo} {
            upvar 1 $resultListVar resultList
            lappend resultList $symInfo

            if {[dict exists $symInfo BASES]} {
                foreach baseInfo [dict get $symInfo BASES] {
                    fillSymInfoList resultList $baseInfo
                }
            }
            if {[dict exists $symInfo FIELDS]} {
                foreach fieldInfo [dict get $symInfo FIELDS] {
                    fillSymInfoList resultList $fieldInfo
                }
            }
            if {[dict exists $symInfo ELEMENTS]} {
                foreach elementInfo [dict get $symInfo ELEMENTS] {
                    fillSymInfoList resultList $elementInfo
                }
            }
        }

        proc displaySymInfo {symInfo} {
            set symInfoList [list]
            fillSymInfoList symInfoList $symInfo
            SymInfoDisplay display $symInfoList
        }

        proc displaySymInfoItemList {itemInfoList} {
            set symInfoList [list]
            foreach symInfo $itemInfoList {
                fillSymInfoList symInfoList $symInfo
            }
            SymInfoDisplay display $symInfoList
        }

        proc cleanupAndFillSymInfoForDisplay {symInfoVar symCache} {
            upvar 1 $symInfoVar symInfo
            dict update symInfo \
                TYPE_ID typeId \
                TYPE_NAME typeName \
                BASES bases \
                FIELDS fields \
                ELEMENTS elements \
            {
                if {![info exists typeName] && [info exists typeId] && $typeId != {}} {
                    set typeName [getTypeName $typeId $symCache]
                }

                cleanupAndFillSymInfoListForDisplay bases $symCache
                cleanupAndFillSymInfoListForDisplay fields $symCache
                cleanupAndFillSymInfoListForDisplay elements $symCache

            }

            cleanupSymInfo symInfo
        }

        proc cleanupAndFillSymInfoListForDisplay {listVar symCache} {
            upvar 1 $listVar symList
            if {[info exists symList]} {
                cli::UpdateList symList symInfo {
                    cleanupAndFillSymInfoForDisplay symInfo $symCache
                }
            }
        }

        proc getSymInfoValue {symInfo} {
            if {[dict exists $symInfo ELEMENTS]} {
                set symList [list]
                foreach elemInfo [dict get $symInfo ELEMENTS] {
                    lappend symList [getSymInfoValue $elemInfo]
                }
                return $symList
            }
            if {[dict exists $symInfo BASES]} {
                set symDict [dict create]
                foreach baseInfo [dict get $symInfo BASES] {
                    set baseName [dict get $baseInfo TYPE_NAME]
                    dict set symDict [string trim $baseName] [getSymInfoValue $baseInfo]
                }
            }
            if {[dict exist $symInfo FIELDS]} {
                if {![info exists symDict]} {
                    set symDict [dict create]
                }
                foreach fieldInfo [dict get $symInfo FIELDS] {
                    set fieldName [dict get $fieldInfo NAME]
                    dict set symDict [string trim $fieldName] [getSymInfoValue $fieldInfo]
                }
            }
            if {[info exists symDict]} {
                return $symDict
            }
            if {[dict exists $symInfo VALUE]} {
                set value [dict get $symInfo VALUE]
                if {[dict exist $symInfo TCL_BINARY_FORMAT]} {
                    set tclBinaryFormat [dict get $symInfo TCL_BINARY_FORMAT]
                    return [cli::Binary::scanTclFmt $value $tclBinaryFormat]
                }
                return $value
            }

            return
        }

        proc getSymInfoListValue {symInfoList} {
            set symList [list]
            foreach symInfo $symInfoList {
                lappend symList [getSymInfoValue $symInfo]
            }
            set symList
        }

        proc getSymTypeName {symInfo symCache} {
            set typeId [dict get $symInfo TYPE_ID]
            getTypeName $typeId $symCache
        }

        proc fillSymTypeName {symInfoVar symCache} {
            upvar 1 $symInfoVar symInfo
            set typeName [cli::Memory::getSymTypeName $symInfo $symCache]
            dict set symInfo TYPE_NAME $typeName
            return "`$typeName {...}`"
        }

        proc BUILD_LOCAL_REGISTER_MAP {} {
            variable LOCAL_REGISTER_MAP
            upvar 0 LOCAL_REGISTER_MAP MAP

            set MAP [dict create]

            for {set shaveNumber 0} {$shaveNumber < 12} { incr shaveNumber } {
                set core S$shaveNumber

                for {set regIndex 0} {$regIndex < 32} { incr regIndex } {
                    dict set MAP $core I$regIndex SVU_REG_I$regIndex\($shaveNumber\)

                    dict set MAP $core V${regIndex} [list SVU_REG_V${regIndex}_0\($shaveNumber\) 4]

                    for {set vIndex 0} {$vIndex < 4} {incr vIndex} {
                        dict set MAP $core V${regIndex}_$vIndex SVU_REG_V${regIndex}_${vIndex}\($shaveNumber\)
                    }
                }

                foreach {name reg} {
                    IP                          SVU_TRF
                    L1_CACHE_DATA               L1_CACHE_DATA
                    L1_CACHE_TAG                L1_CACHE_TAG
                    SLICE_DBG                   SLICE_DBG
                    DCU                         SVU_DCU
                    DCR                         SVU_DCR
                    OCR                         SVU_OCR
                    OSR                         SVU_OSR
                    ICR                         SVU_ICR
                    ISR                         SVU_ISR
                    IRR                         SVU_IRR
                    FXE                         SVU_FXE
                    FXF                         SVU_FXF
                    IXE                         SVU_IXE
                    IXF                         SVU_IXF
                    PTR                         SVU_PTR
                    IHR                         SVU_IHR
                    BTH                         SVU_BTH
                    PTH                         SVU_PTH
                    RF_CFG                      SVU_RF_CFG
                    INEXT                       SVU_INEXT
                    IH                          SVU_IH
                    BRUCTRL                     SVU_BRUCTRL
                    IDCCRC                      SVU_IDCCRC
                    TRF                         SVU_TRF
                    B_IP_0                      SVU_B_IP_0
                    B_LBEG                      SVU_B_LBEG
                    B_LEND                      SVU_B_LEND
                    B_MREPS                     SVU_B_MREPS
                    B_STATE                     SVU_B_STATE
                    B_RFB                       SVU_B_RFB
                    I_STATE                     SVU_I_STATE
                    S_STATE                     SVU_S_STATE
                    V_STATE                     SVU_V_STATE
                    S_ACC                       SVU_S_ACC
                    V_ACC0                      SVU_V_ACC0
                    V_ACC1                      SVU_V_ACC1
                    V_ACC2                      SVU_V_ACC2
                    V_ACC3                      SVU_V_ACC3
                    NONE                        SVU_NONE
                    I_AE                        SVU_I_AE
                    F_AE                        SVU_F_AE
                    C_CSI                       SVU_C_CSI
                    C_CMU0                      SVU_C_CMU0
                    C_CMU1                      SVU_C_CMU1
                    B_SREPS                     SVU_B_SREPS
                    ISR_STAT                    SVU_ISR_STAT
                    ISR_RA                      SVU_ISR_RA
                    ISR_SA                      SVU_ISR_SA
                    P_CFG                       SVU_P_CFG
                    P_SVID                      SVU_P_SVID
                    P_VERS                      SVU_P_VERS
                    P_GPI                       SVU_P_GPI
                    P_GPR                       SVU_P_GPR
                    IRF                         SVU_IRF
                    SRF                         SVU_SRF
                    VRF                         SVU_VRF
                    DCR                         SVU_DCR
                    DSR                         SVU_DSR
                    IBA0                        SVU_IBA0
                    IBC0                        SVU_IBC0
                    IBA1                        SVU_IBA1
                    IBC1                        SVU_IBC1
                    DBA0                        SVU_DBA0
                    DBD0                        SVU_DBD0
                    DBC0                        SVU_DBC0
                    DBA1                        SVU_DBA1
                    DBD1                        SVU_DBD1
                    DBC1                        SVU_DBC1
                    PC0                         SVU_PC0
                    PCC0                        SVU_PCC0
                    PC1                         SVU_PC1
                    PCC1                        SVU_PCC1
                    DBK_BYTE                    SVU_DBK_BYTE
                    PC2                         SVU_PC2
                    PCC2                        SVU_PCC2
                    PC3                         SVU_PC3
                    PCC3                        SVU_PCC3
                    IDC_FIFO                    SVU_IDC_FIFO
                    IDC_FIFO_PTR                SVU_IDC_FIFO_PTR
                    TLB_DBL_ENTRY               SVU_TLB_DBL_ENTRY
                    CACHE_CTRL                  SVU_CACHE_CTRL
                    CACHE_SVU_CTRL              SVU_CACHE_SVU_CTRL
                    CACHE_CNT_IN                SVU_CACHE_CNT_IN
                    CACHE_CNT_OUT               SVU_CACHE_CNT_OUT
                    CACHE_CTRL_TXN_ADDR         SVU_CACHE_CTRL_TXN_ADDR
                    CACHE_CTRL_TXN_TYPE         SVU_CACHE_CTRL_TXN_TYPE
                    CACHE_STATUS                SVU_CACHE_STATUS
                    CACHE_RAM_CFG               SVU_CACHE_RAM_CFG
                    CACHE_DATA_RAM_ACCESS       SVU_CACHE_DATA_RAM_ACCESS
                    CACHE_TAG_RAM_ACCESS        SVU_CACHE_TAG_RAM_ACCESS
                    MUTEX_CTRL                  SVU_MUTEX_CTRL
                    MUTEX_STATUS                SVU_MUTEX_STATUS
                } {
                    dict set MAP $core $name $reg\($shaveNumber\)
                }
            }

            foreach leon {OS RT} {
                set core L$leon
                foreach R {I L O G} {
                    for {set regIndex 0} {$regIndex < 8} {incr regIndex} {
                        set name ${R}${regIndex}
                        dict set MAP $core $name LEON_${leon}_REG_${name}_ADR
                    }
                }
                for {set CWP 0} {$CWP < 8} {incr CWP} {
                    foreach R {I L O} {
                        for {set regIndex 0} {$regIndex < 8} {incr regIndex} {
                            set name ${R}${regIndex}
                            dict set MAP $core $name\($CWP\) LEON_${leon}_REG_${name}_\($CWP\)
                        }
                    }
                }

                for {set regIndex 0} {$regIndex < 32} {incr regIndex} {
                    set name F${regIndex}
                    dict set MAP $core F${regIndex} LEON_${leon}_REG_F${regIndex}_ADR
                }
                foreach {name reg} {
                    Y      Y_REG
                    PSR    PSR
                    WIM    WIM
                    TBR    TBR
                    PC     PC_REG
                    NPC    NPC_REG
                    FSR    FSR_REG
                } {
                    dict set MAP $core $name LEON_${leon}_DSU_${reg}_ADR
                }
            }
        }

        namespace eval Autocomplete {

        } ; # namespace eval Autocomplete

        proc setInteractiveAutocomplete {baseopts opts} {
            set origCmdName ::mdbg::shell::autocomplete
            if {[info commands $origCmdName] == ""} {
                return
            }
            set newCmdNamePrefix [namespace current]::Autocomplete::autocomplete
            set i 1
            while {[info commands [set newCmdName $newCmdNamePrefix#$i]] != ""} {
                incr i
            }
            ::rename $origCmdName $newCmdName

            uplevel #0 "
                proc mdbg::shell::autocomplete {line} {
                    mdbg::cli::Memory::InteractiveAutocomplete \$line [list $baseopts] [list $opts]
                }
            "

            return $newCmdName
        }
        proc restoreAutocomplete {cmdName} {
            if {$cmdName != "" && [info commands $cmdName] != ""} {
                set origCmdName ::mdbg::shell::autocomplete
                ::rename $origCmdName ""
                ::rename $cmdName $origCmdName
            }
        }

        proc Autocomplete-mget/i {cmd params prefix} {
            set Parser [ArgumentParser SubCmdParser get]
            $Parser autocomplete mget [concat {""} $params] $prefix
        }

        proc InteractiveAutocomplete {line baseopts opts} {
            cli::CatchAndPrintError {
                if {[regexp {^((?:[^/]+|/[^/]+)*(//+))(.*)$} $line -> lineprefix // options]} {
                    regexp {^\s*(.*?)(\s*)$} $options -> options space
                    if {$space == "" && [llength $options] > 0} {
                        set prefix [lindex $options end]
                        set options [lrange $options 0 end-1]
                    } else {
                        set prefix ""
                    }
                    if {[string match ///* ${//}]} {
                         set params $options
                    } else {
                        set params [concat $baseopts $options]
                    }
                    if {[llength $options]} {
                        append lineprefix " $options "
                    } else {
                        append lineprefix " "
                    }
                    set suggestions [Autocomplete-mget/i {} $params $prefix]
                } else {
                    regexp -nocase {^(.*?)(\w*)$} $line -> lineprefix prefix
                    set suggestions [ArgumentParser autocomplete-location [concat $baseopts $opts] $prefix]
                }
                set result [list]
                foreach word $suggestions {
                    lappend result "$lineprefix$word"
                }
                return $result
            }
        }

    } ; # namespace eval cli::Memory

    proc mem {memCmd args} {
        cli::NoErrorStack {
            switch -exact $memCmd {
                get - set - fill - dump {
                    set mMemCmd "m${memCmd}"
                    cli::DeprecatedWarning $mMemCmd 1
                    $mMemCmd {*}$args
                }
                default {
                    return -code error {invalid subcommand: must be "get", "set", "fill", or "dump"}
                }
            }
        }
    }

    shell::autocomplete::addCommand {mem} cli::Memory::ArgumentParser autocomplete

    proc mget {args} {
        set memCmd get
        set parser [cli::Memory::ArgumentParser SubCmdParser $memCmd]
        $parser parseCallerArgs 1 $args
        cli::NoErrorStack {
            cli::TrapInterrupt {
                cli::Memory::ArgumentParser setupArgs

                cli::UsingObj [cli::Memory::SymbolCacheClass] symCache {
                    cli::Memory::ArgumentParser createSymInfo
                    cli::Memory::fillSymTypeAddrSize symInfo $symCache
                    cli::Memory::ArgumentParser fillSymInfo

                    set dataSize [dict get $symInfo SIZE]
                    set dataType [dict get $symInfo TYPE]

                    set hasCount [info exists count]
                    if {!$hasCount} {
                        if {[dict exists $symInfo COUNT]} {
                            set hasCount 1
                            set count [dict get $symInfo COUNT]
                            dict unset symInfo COUNT
                        } else {
                            set count 1
                        }
                    }
                    set scalarResult [expr {
                        !$hasCount || $dataType in {ARRAY FUNCTION}
                    }]

                    try {
                        if {$scalarResult} {
                            set result {}
                        } else {
                            set resultList [list]
                            set itemInfoList [list]
                        }

                        for {set index 0} {$index < $count} {incr index} {
                            cli::CheckInterrupt

                            set itemInfo $symInfo
                            dict set itemInfo INDEX $index

                            unset -nocomplain itemResult

                            switch -exact $dataType {
                                ENUMERATION {
                                    set data [cli::Memory::readBinaryValue itemInfo]
                                    if {[string length $data]} {
                                        set itemResult [cli::Memory::parseEnum $data itemInfo $symCache]
                                    }
                                }
                                COMPOSITE {
                                    set itemResult [cli::Memory::fillSymTypeName itemInfo $symCache]
                                    set data [cli::Memory::readBinaryValue itemInfo]
                                    if {[string length $data]} {
                                        cli::Memory::parseComposite $data itemInfo $symCache $maxDepth
                                    }
                                }
                                ARRAY {
                                    set itemResult [cli::Memory::fillSymTypeName itemInfo $symCache]
                                    if {$hasCount} {
                                        set elemSize [cli::Memory::getArrayElemSize $itemInfo $symCache]
                                        dict set itemInfo LENGTH $count
                                        dict set itemInfo SIZE [expr {$elemSize * $count}]
                                        if {$maxDepth < 1} {
                                            set maxDepth 1
                                        }
                                    }
                                    if {$maxDepth > 0} {
                                        set data [cli::Memory::readBinaryValue itemInfo]
                                        if {[string length $data]} {
                                            cli::Memory::parseArray $data itemInfo $symCache $maxDepth
                                        }
                                    }
                                }
                                FUNCTION {
                                    if {[dict get $itemInfo TYPE_ID] == {}} {
                                        dict set itemInfo TYPE_NAME "<function>"
                                    }
                                    set itemResult [dict get $itemInfo ADDRESS]
                                }
                                ASCIIZ {
                                    if {[dict get $itemInfo ASCIIZ_INDIRECT]} {
                                        set data [cli::Memory::readBinaryValue itemInfo]
                                        dict set itemInfo ADDRESS [cli::Memory::scanData $data POINTER 4]
                                    }
                                    set itemResult [cli::Memory::readAsciiZ itemInfo]
                                }
                                default {
                                    set binaryClass [cli::Memory::binaryTypeClass $dataType]
                                    set data [cli::Memory::readBinaryValue itemInfo]
                                    if {[string length $data]} {
                                        set itemResult [cli::Memory::scanData $data $binaryClass $dataSize]
                                    }
                                }
                            }

                            if {[info exists itemResult]} {
                                if {$dataType != {FUNCTION} || $dataSize != 0} {
                                    dict set itemInfo VALUE $itemResult
                                }
                            }

                            if {$index == 0} {
                                if {[dict exists $itemInfo ADDRESS]} {
                                    dict set symInfo ADDRESS [
                                        dict get $itemInfo ADDRESS
                                    ]
                                }
                            } else {
                                dict unset itemInfo ExprObj
                            }
                            if {![info exists itemResult]} {
                                set itemResult {}
                            }
                            if {$scalarResult} {
                                set symInfo $itemInfo
                                dict unset symInfo INDEX
                                set result $itemResult
                                break;
                            } else {
                                lappend resultList $itemResult
                                lappend itemInfoList $itemInfo
                            }
                        }

                        if {$full || !$value} {
                            if {$scalarResult} {
                                cli::Memory::cleanupAndFillSymInfoForDisplay symInfo $symCache
                            } else {
                                cli::Memory::cleanupAndFillSymInfoListForDisplay itemInfoList $symCache
                            }

                            if {$quiet} {
                                return [expr {$scalarResult ? $symInfo : $itemInfoList}]
                            }
                            if {$scalarResult} {
                                cli::Memory::displaySymInfo $symInfo
                            } else {
                                cli::Memory::displaySymInfoItemList $itemInfoList
                            }
                            if {!$value} {
                                return
                            }
                        }
                        if {$scalarResult} {
                            cli::Memory::getSymInfoValue $symInfo
                        } else {
                            cli::Memory::getSymInfoListValue $itemInfoList
                        }
                    } finally {
                        if {$scalarResult} {
                            cli::Memory::cleanupSymInfo symInfo
                        } else {
                            cli::UpdateList itemInfoList itemInfo {
                                cli::Memory::cleanupSymInfo itemInfo
                            }
                        }
                    }
                }  ; # cli::UsingObj
            } ; # cli::TrapInterrupt
        } ; # cli:: NoErrorStack
    }

    shell::autocomplete::addCommand {mget} [cli::Memory::ArgumentParser SubCmdParser get] autocomplete
    cli::Help::Manager add mget -parser [cli::Memory::ArgumentParser SubCmdParser get]

    proc mset {args} {
        set memCmd set
        set parser [cli::Memory::ArgumentParser SubCmdParser $memCmd]
        $parser parseCallerArgs 1 $args
        cli::NoErrorStack {
            cli::Memory::ArgumentParser setupArgs

            cli::UsingObj [cli::Memory::SymbolCacheClass] symCache {
                cli::Memory::ArgumentParser createSymInfo
                cli::Memory::fillSymTypeAddrSize symInfo $symCache
                cli::Memory::ArgumentParser fillSymInfo

                try {
                    set dataType [dict get $symInfo TYPE]
                    set dataSize [dict get $symInfo SIZE]

                    switch -exact $dataType {
                        ENUMERATION {
                            set data [cli::Memory::formatEnum $what symInfo $symCache]
                            cli::Memory::writeBinaryValue symInfo $data
                        }
                        COMPOSITE -
                        ARRAY {
                            set typeName [cli::Memory::getTypeName $symInfo $symCache]
                            return -code error "set -type `$typeName` not implemented"
                        }
                        FUNCTION {
                            set addr [dict get $symInfo ADDRESS]
                            return -code error "set -type `function @$addr` not allowed"
                        }
                        default {
                            set binaryClass [cli::Memory::binaryTypeClass $dataType]
                            set data [cli::Binary::formatData $binaryClass $dataSize $what]
                            cli::Memory::writeBinaryValue symInfo $data
                        }
                    }
                } finally {
                    cli::Memory::cleanupSymInfo symInfo
                }
            } ; # cli::UsingObj
        }
    }

    shell::autocomplete::addCommand {mset} [cli::Memory::ArgumentParser SubCmdParser set] autocomplete
    cli::Help::Manager add mset -parser [cli::Memory::ArgumentParser SubCmdParser set]

    proc mfill {args} {
        set memCmd fill
        set parser [cli::Memory::ArgumentParser SubCmdParser $memCmd]
        $parser parseCallerArgs 1 $args
        cli::NoErrorStack {
            cli::Memory::ArgumentParser setupArgs

            cli::UsingObj [cli::Memory::SymbolCacheClass] symCache {
                cli::Memory::ArgumentParser createSymInfo
                cli::Memory::fillSymTypeAddrSize symInfo $symCache
                cli::Memory::ArgumentParser fillSymInfo

                try {
                    set dataType [dict get $symInfo TYPE]
                    set dataSize [dict get $symInfo SIZE]

                    if {$zero} {
                        set data [binary format x]
                    } else {
                        if {[info exists tclBinaryFormat]} {
                            set data [binary format $tclBinaryFormat {*}$data]
                        } else {
                            if {[llength $data] > 1} {
                                return -code error "too many data arguments"
                            }
                            set data [binary format a* [lindex $data 0]]
                        }
                    }
                    if {[info exists srcSliceLength]} {
                        if {[info exists srcSliceOffset]} {
                            set data [string range $data $srcSliceOffset [expr {$srcSliceOffset + $srcSliceLength - 1}]]
                        } else {
                            set data [string range $data 0 [expr {$srcSliceLength - 1}]]
                        }
                        if {[string length $data] != $srcSliceLength} {
                            return -code error "insufficient source slice data"
                        }
                    } else {
                        if {[info exists srcSliceOffset]} {
                            set data [string range $data $srcSliceOffset end]
                        }
                    }
                    if {[string length $data] == 0} {
                        return -code error "empty data"
                    }

                    cli::Memory::writeBinaryValue symInfo $data

                } finally {
                    cli::Memory::cleanupSymInfo symInfo
                }
            } ; # cli::UsingObj
        }
    }

    shell::autocomplete::addCommand {mfill} [cli::Memory::ArgumentParser SubCmdParser fill] autocomplete
    cli::Help::Manager add mfill -parser [cli::Memory::ArgumentParser SubCmdParser fill]

    proc mdump {args} {
        set memCmd dump
        set parser [cli::Memory::ArgumentParser SubCmdParser $memCmd]
        $parser parseCallerArgs 1 $args

        set size [expr {4 * $count}]
        set options [list -quiet -type binary -count 1 -size $size]
        if {$jtag} {
            lappend options -jtag
        }

        set result [lindex [mget $where {*}$options] 0]

        set value [dict get $result VALUE]
        set address [dict get $result ADDRESS]

        cli::Binary::forEachBlock 16 row $value {
            set numbers [cli::Binary::scanU32List $row "0x%.8X"]
            set ascii [cli::Binary::scanAscii $row]
            cli::Putf {[%.8X] = %-48s %s} $address $numbers $ascii
            incr address 16
        }
    }

    shell::autocomplete::addCommand {mdump} [cli::Memory::ArgumentParser SubCmdParser dump] autocomplete
    cli::Help::Manager add mdump -parser [cli::Memory::ArgumentParser SubCmdParser dump]

    proc mget/i {args} {
        cli::NoErrorStack {
            cli::PutInfo "-------------------------------------------------------------------"
            cli::PutInfo "Entering interactive `mget` session, leave with Ctrl-D or no input."
            cli::PutInfo "- Multiple locations should be separated by \";\""
            cli::PutInfo "- Change `mget` options using \"... // -option \[value\]...\""
            cli::PutInfo "- Tcl Event dispatch is blocked during text input!"
            cli::PutInfo "-------------------------------------------------------------------"

            set textattr {blue bold}
            set target ""
            if {[info exists ::mdbg::TARGET]} {
                set target $::mdbg::TARGET
                if {[state -issuspended]} {
                    lassign [state -pc -sym -basedir -file -line] pc sym basedir file line
                    set context "IP=$pc"
                    if {$sym != ""} {
                        append context " <$sym>"
                    }
                    if {$file != ""} {
                        append context ", $file:$line"
                    }
                    cli::PutInfo $context
                }
            }
            set baseopts $args
            set opts ""
            try {
                while true {
                    set command mget
                    if {[llength $baseopts]} {
                        append command " $baseopts"
                    }
                    if {[llength $opts]} {
                        append command " $opts"
                    }
                    cli::PutInfo "$target% $command"

                    set completionCmdName [cli::Memory::setInteractiveAutocomplete $baseopts $opts]
                    try {
                        set input [cli::ColorGets $textattr ">>> "]
                    } finally {
                        cli::Memory::restoreAutocomplete $completionCmdName
                    }

                    update; # Process pending events

                    if {$input == ""} {
                        break;
                    }

                    regexp {^\s*(.*?)\s*(?:(//+)\s*(.*)\s*)?$} $input -> location // newopts

                    switch -glob ${//} {
                        // {
                            set opts $newopts
                        }
                        ///* {
                            set baseopts $newopts
                            set opts ""
                        }
                        "" {
                            if {$location == ""} {
                                unset -nocomplain lastLocation
                                continue
                            }
                        }
                    }
                    if {$location == ""} {
                        if {![info exists lastLocation]} {
                            continue
                        }
                        set location $lastLocation
                    } else {
                        set lastLocation $location
                    }
                    foreach location [split $location ";"] {
                        if {![string is space $location]} {
                            cli::CatchAndPrintError {
                                mdbg::eval% [list mget {*}$baseopts {*}$opts [string trim $location]]
                            }
                        }
                    }
                }
            } finally {
                cli::PutInfo "-----------------------------------"
                cli::PutInfo "Leaving interactive `mget` session."
                cli::PutInfo "-----------------------------------"
            }
        }
    }
    shell::autocomplete::addCommand {mget/i} cli::Memory::Autocomplete-mget/i
    cli::Help::Manager add {mget/i} \
        -short "Interactive `mget` session" \
        -synopsis "mget/i ?options?..." \
        -args {
            options {
                -synopsis {?options?...}
                -short "Any option available for `mget` except `location(*)`"
                -long {
                    These will be the base options for the interactive `mget` session.
                }
            }
        } \
        -long {
            Start an interactive `mget` session.

            The standard input is read for locations separated by `;` or newline.
            Empty input or end-of-file (Ctrl-D) ends the session.
            Empty locations are ignored.

            `mget` is performed for each location separately, using the base options and additional parameters

            Text after C-style `//` comments modify the additional parameters for `mget`.
            Text after C-style `///` comments sets the base options and clears any additional parameters.

            Basic autocomplete is avaiable in the interactive `mget` shell.

            !!! Tcl event dispatch is blocked while user input is being entered!
        }
    cli::Memory::BUILD_LOCAL_REGISTER_MAP

    cli::Alias mgetv mget -value
    cli::Alias mgetb mget -binary
    cli::Alias mgets mget -asciiz

    namespace export mget mset mdump

} ; # namespace eval mdbg
