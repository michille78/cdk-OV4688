namespace eval mdbg {

    cli::ArgumentParser create cli::JtagPins::ArgumentParser {
        -auto
        -args {
            -trst {
                -choice {low high toggle}
                -default {}
            }
            -srst {
                -choice {low high toggle}
                -default {}
            }
            -led {
                -choice {low high toggle}
                -default {}
            }
        }
    }

    cli::ArgumentParser create cli::JtagScan::ArgumentParser {
        -args {
            reg {
                -synopsis "dr|ir"
                -complete-script {
                    mdbg::shell::autocomplete::filterList \
                        [list dr ir] \
                        [string tolower $PREFIX]*
                }
            }
            count {
                -var numBits
                -type integer
                -not-match {-*}
            }
            pattern {
                -synopsis "pattern ?...?"
            }
            args {
                -hidden
            }
        }
    }

    memory::JtagMem create jtag

    oo::objdefine jtag {
        mixin mdbg::memory::BlockFormatter

        method get32 {address} {
            my CheckAligned $address
            ::mvproto::get32 $address
        }

        method get64 {address} {
            my CheckAligned $address 8
            ::mvproto::get64 $address
        }

        method set32 {address value} {
            my CheckAligned $address
            ::mvproto::set32 $address $value
        }

        method set64 {address value} {
            my CheckAligned $address 8
            ::mvproto::set64 $address $value
        }

        method getBurst32 {address numWords32} {
            my CheckAligned $address
            my CheckBurstSize $address $numWords32
            ::mvproto::getBurst32 $address $numWords32
        }

        method setBurst32 {address wordList} {
            my CheckAligned $address
            my CheckBurstSize $address [expr {4 * [llength $wordList]}]
            ::mvproto::setBurst32 $address $wordList
        }

        method CheckAligned {address {alignment 4}} {
            if {$address & ( $alignment - 1 )} {
                return -level 2 -code error "Address \"$address\" is not aligned to $alignment bytes."
            }
        }

        method CheckBurstSize {address numWords32} {
            if { (( $address + $numWords32 * 4 ) & ~1023) != ( $address & ~1023 ) } {
                return -level 2 -code error "Burst across 1K boundary!"
            }
        }

        # otherwise it shows up in autocomplete
        unexport destroy

        method SetPinVal {setbitsVar clearbitsVar state bit} {
            upvar 1 $setbitsVar setbits
            upvar 1 $clearbitsVar clearbits
            if { $state in {toggle high} } {
                set setbits [expr {$setbits | $bit}]
            }
            if { $state in {toggle low} } {
                set clearbits [expr {$clearbits | $bit}]
            }
        }

        method pins {args} {
            ::mdbg::cli::JtagPins::ArgumentParser parseCallerArgs 1 $args
            ::mdbg::cli::NoErrorStack {
               set setbits   0
               set clearbits 0
               my SetPinVal setbits clearbits $trst 1
               my SetPinVal setbits clearbits $srst 2
               my SetPinVal setbits clearbits $led  4
               if {[::mvproto::jtagPins $setbits $clearbits]} {
                   return
               }
            }
            return -code error "Not supported by server"
        }

        method ir {value} {
            if {[::mvproto::jtagSendIR $value]} {
                return
            }
            return -code error "Not supported by server"
        }

        method scan {args} {

            mdbg::cli::JtagScan::ArgumentParser parseCallerArgs 2 $args

            mdbg::cli::NoErrorStack {
                set regIdx [lsearch {dr ir} [string tolower $reg]]
                if {$regIdx < 0} {
                    error "Invalid JTAG register: must be \"dr\" or \"ir\"."
                }

                set lsbFormat [list]
                set lsbData ""

                set values [list $pattern {*}$args]

                foreach value [lreverse $values] {
                    if {[regexp -nocase {^0x([0-9a-z]+)$} $value -> hexStr]} {
                        set len [string length $hexStr]
                        set binStr [format %0*b [expr {4 * $len}] $value]
                        lappend lsbFormat 0x $len
                    } elseif {[regexp -nocase {^0b([01]+)$} $value -> binStr]} {
                        lappend lsbFormat 0b [string length $binStr]
                    } else {
                        error "`pattern`: expected 0b<bin> or 0x<hex> number, got \"$value\""
                    }
                    append lsbData [string reverse $binStr]
                }
                set actualBits [string length $lsbData]
                if {$actualBits != $numBits} {
                    error "bit count mismatch: expected $numBits, got $actualBits"
                }

                # mdbg::cli::PutDebug "0b[string reverse $lsbData]"

                set inOutByteData [binary format b* $lsbData]

                ::mvproto::jtagScan $regIdx inOutByteData $numBits

                if {![binary scan $inOutByteData b${numBits} lsbResult]} {
                    error "Server returned invalid data (length does not match)."
                }

                # mdbg::cli::PutDebug "0b[string reverse $lsbResult]"

                set lsbValues [list]
                set pos 0
                foreach {fmt len} $lsbFormat {
                    set beg $pos
                    if {$fmt == "0x"} {
                        incr pos [expr {4 * $len}]
                    } else {
                        incr pos $len
                    }
                    set end [expr {$pos-1}]
                    set value 0b[string reverse [string range $lsbResult $beg $end]]
                    if {$fmt == "0x"} {
                        set value [format 0x%0*X $len $value]
                    }
                    lappend lsbValues $value
                }

                lreverse $lsbValues
            }
        }

    } ; # oo objdefine jtag

    proc ADD_JTAG_HELP {} {
        foreach {subcmd params short extra} {
            {}                 {subcommand ?args?...}              "Direct JTAG interface."                 {
                -long {
                    Provides interface to various JTAG operations.

                    See "help jtag *" for list of subcommands.
                }
            }
            {get32}            {address}                           "Read word (32bit)."                     {}
            {set32}            {address value32}                   "Write word (32bit)."                    {}
            {get64}            {address}                           "Read double word (64-bit)."             {}
            {set64}            {address value64}                   "Write double word (64-bit)."            {}
            {getBurst32}       {address numWords32}                "Burst read 32-bit words."               {}
            {setBurst32}       {address listOfWords}               "Burst write 32-bit words."              {}
            {ir}               {number}                            "Send JTAG instruction register value."  {}
            {scan}             {dr|ir count pattern ...}           "Scan JTAG register bits."               {
                -long {
                    Shifts bits of JTAG register in and out.

                    The bits can be specified in groups of either binary or hexadecimal format.

                    The concatenated patterns will be shifted in least significant bit first.
                    The result will be in exactly the same format as the input.
                }
                -args {
                    dr|ir {
                        -short "JTAG register."
                    }
                    count {
                        -short "Total bit count."
                        -long {
                            This value is checked against the bit-length of the pattern list provided.
                            An error is generated on mismatch.
                        }
                    }
                    pattern {
                        -synopsis "pattern ?...?"
                        -short "Bit patterns."
                        -long {
                            One or more binary or hexadecimal bit patterns.
                            (Given as separate arguments).

                            Binary patterns must begin with "0b", hex patterns with "0x".

                            The number of digits in the pattern will determine the number of bits.
                            (One hex digit equals four bits.)

                            The big endian bit patterns are concatenated and shifted in least significant bit first.
                            The result will be a list of patterns having the same format.
                        }
                    }
                }
            }
            {pins}             {?-trst high|low|toggle? ?-srst high|low|toggle? ?-led <high|low|toggle>?}
                                                                   "Set JTAG pins state." {
                -args {
                    -trst {
                        -synopsis "?-trst high|low|toggle?"
                        -short "Change TRST pin state."
                    }
                    -srst {
                        -synopsis "?-srst high|low|toggle?"
                        -short "Change SRST pin state."
                    }
                    -led {
                        -synopsis "?-led high|low|toggle?"
                        -short "Change LED pin state."
                    }
                }
            }

            {readBlock}        {address size}                      "Read block."                            {}
            {writeBlock}       {address data}                      "Write block."                           {}
            {fillBlock}        {address size pattern}              "Fill block."                            {}

            {scanBlock}        {format address size varnames...}   "Read+scan block into variables."        {}
            {formatBlock}      {format address size arguments...}  "Format+write arguments."                {}

            {getBlock}         {?-scan binFmt? address size}        "Read (and scan) block."                {
                -args {
                    binFmt {
                        -synopsis "?-scan binFmt?"
                        -short "Format specifier compatible with Tcl built-in `binary format`"
                    }
                }
            }
            {setBlock}         {?-format binFmt? address data}     "(Format data and) write block."         {
                    -args {
                        binFmt {
                            -synopsis "?-format binFmt?"
                            -short "Format specifier compatible with Tcl built-in `binary format`"
                        }
                    }
            }
        } {
            uplevel 1 [list \
                cli::Help::Manager add "jtag $subcmd" \
                -short $short \
                -synopsis "jtag $subcmd $params" \
                {*}$extra
            ]
        }
        rename [lindex [info level 0] 0] ""
    }

    ADD_JTAG_HELP

    shell::autocomplete::addScript jtag {
        # $COMMAND $PARAMS $PREFIX
        if {$PARAMS == {}} {
            shell::autocomplete::filterList \
                [info object methods $COMMAND -all] $PREFIX*
        } else {
            set PARAMS [lassign $PARAMS COMMAND]
            switch -exact $COMMAND {
                pins {
                    cli::JtagPins::ArgumentParser autocomplete $COMMAND $PARAMS $PREFIX
                }
                scan {
                    cli::JtagScan::ArgumentParser autocomplete $COMMAND $PARAMS $PREFIX
                }
                default {
                    list $PREFIX
                }
            }
        }
    }

    namespace export jtag

} ; # namespace eval mdbg