# ***************************************************************************
# Copyright (C) 2015 Movidius Ltd. All rights reserved
# ---------------------------------------------------------------------------
# File       : cli.tcl
# Description: Loads the moviDebug2 command implementation files
# Created on : Jun 16, 2015
# Author     : Hanos-Puskai Peter (peter.hanos@movidius.com)
# ***************************************************************************
namespace eval mdbg {

    proc LOAD_CLI_SCRIPTS {baseDir} {
        foreach script {
            Puts
            Text
            ArgumentParser
            HelpManager
            Monochrome
            TabularDisplay

            common
            help
            binary

            platform
            registers
            loadfile
            savefile
            target
            run
            dasm
            halt
            continue
            wait
            breakpoint
            linenumbers
            step
            symbols
            memory
            memmap
            jtag
            pipe
            state
            state-reg
            callstack
            history
            uart
            cpr
            gpio
            unsupported/leoncallstack
            utility
            coreconfig

            DisplayState
            mutex
        } {
            uplevel 1 [list namespace inscope :: \
                source [file join $baseDir cli $script.tcl] \
            ]
        }

        set name [lindex [info level 0] 0]
        uplevel 1 rename $name {{}}
    }

    LOAD_CLI_SCRIPTS [file dirname [info script]]

    catch {
        target [cli::Target::getRootContext]
        target [tcf::Platform::getMainCore [target]]
    }
} ; # namespace eval mdbg
