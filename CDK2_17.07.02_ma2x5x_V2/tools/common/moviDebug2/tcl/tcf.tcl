# ***************************************************************************
# Copyright (C) 2015 Movidius Ltd. All rights reserved
# ---------------------------------------------------------------------------
# File       : tcf.tcl
# Description: Loads the TCF/Tcl subsystem implementation
# Created on : Aug 13, 2015
# Author     : Hanos-Puskai Peter (peter.hanos@movidius.com)
# ***************************************************************************

namespace eval mdbg::tcf {

    variable SCRIPT_DIR [file dirname [info script]]

    proc LOAD_SCRIPT {script} {
        # a primitive form of comments
        if {[string equal [string index $script 0] "#"]} {
            return
        }
        variable SCRIPT_DIR
        if {[info exists ::mdbg::verbose] && $mdbg::verbose} {
            puts $mdbg::Channels::verbose "loading tcf/$script"
        }

        set script [file join $SCRIPT_DIR tcf $script]

        if [file exists $script] {
            uplevel 1 namespace inscope :: source $script
        } else {
            puts $mdbg::Channels::warning "warning: $script not found!"
            return
        }
    }

    foreach script {
        json
        protocol
        fields

        Breakpoints
        StackTrace
        ContextQuery
        Disassembly
        Memory
        MemoryMap
        Processes
        Registers
        RunControl
        SysMonitor
        Symbols
        Expressions
        LineNumbers

        Platform
        MyriadCore

        BreakpointMonitor
        SymbolCache
        ExprObj

        utility
    } {
        LOAD_SCRIPT $script.tcl
    }
    unset script
}

