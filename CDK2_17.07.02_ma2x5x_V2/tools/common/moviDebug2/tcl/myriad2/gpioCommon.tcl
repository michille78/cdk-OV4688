# ***************************************************************************
# Copyright (C) 2015 Movidius Ltd. All rights reserved
# ---------------------------------------------------------------------------
# File       : gpioCommon.tcl
# Description: Common classes, procedures for platforms
# Created on : Feb 10, 2016
# Author     : Daraban Luminita (luminita.daraban@movidius.com)
# ***************************************************************************

namespace eval mdbg::gpio {

    # struct Coordinates
    #    unsigned int pin;
    #    unsigned int mode;
    #    string signalName;
    oo::class create Coordinates {

        variable Pin Mode SignalName

        constructor {pin mode signalName} {
            set Pin $pin
            set Mode $mode
            set SignalName $signalName
        }

        method getPin {} {
            return $Pin
        }

        method getMode {} {
            return $Mode
        }

        method getSignalName {} {
            return $SignalName
        }

    }; # oo::class Coordinates

    # struct GpioNames
    #    const char* signals[6];
    #    unsigned int direction[6];
    #    int bypassGroup;
    oo::class create GpioNames {

        variable Signals Directions BypassGroup

        constructor {signals direction bypassGroup} {
            set Signals $signals
            set Directions $direction
            set BypassGroup $bypassGroup
        }

        method getSignals {} {
            # variable Signals
            return $Signals
        }

        method getSignal {mode} {
            # variable Signals
            lindex $Signals $mode
        }

        method hasSignal {mode} {
            expr {
                [string length [my getSignal $mode]] != 0
            }
        }

        method getDirection {mode} {
            # variable Directions
            format %d [
                lindex $Directions $mode
            ]
        }

        method getBypassGroup {} {
            # variable BypassGroup
            return $BypassGroup
        }

    }; # oo::class GpioNames

} #namespace eval mdbg::gpio

