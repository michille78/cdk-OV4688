# ***************************************************************************
# Copyright (C) 2015 Movidius Ltd. All rights reserved
# ---------------------------------------------------------------------------
# File       : init.tcl
# Description: moviDebug2 Shell init script
# Created on : May 25, 2015
# Author     : Hanos-Puskai Peter (peter.hanos@movidius.com)
# ***************************************************************************

if {[info exists ::mdbg::verbose] && $::mdbg::verbose} {
    puts $mdbg::Channels::verbose "moviDebug2 init script running"
}

namespace eval mdbg::shell {
    set SCRIPT_DIR [file dirname [info script]]

    proc LOAD_SCRIPT {script} {
        variable SCRIPT_DIR
        if [string equal [string index $script 0] "#"] {
            return
        }
        if {[info exists ::mdbg::verbose] && $::mdbg::verbose} {
            puts $mdbg::Channels::verbose "loading shell/$script"
        }
        uplevel 1 namespace inscope :: source [file join $SCRIPT_DIR shell $script]
    }

    foreach script {
        ParamSplitter
        autocomplete
        eclipse_terminal
        tcf_log
        vcshooks_log
        commands
    } {
        LOAD_SCRIPT $script.tcl
    }

    if {[info exists ::mdbg::CLASSIC_TCL] && $::mdbg::CLASSIC_TCL} {
        LOAD_SCRIPT classic.tcl
    }

}
