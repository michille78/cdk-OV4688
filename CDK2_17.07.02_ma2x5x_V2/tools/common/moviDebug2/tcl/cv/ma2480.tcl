proc mdbg::MV_SOC_REV {} { return ma2480 }
source [file join [file dirname [info script]] myriad2.tcl]
mdbg::REGISTERS::FROM_HEADER [file join [file dirname [file dirname [info script]]] myriad2 registersMyriadMa2x8x.h]
source [file join [file dirname [info script]] ma2x8x uart.tcl]
mdbg::uart::AUTO_INIT