namespace eval mdbg::cv::myriad2 {
    set SCRIPT_DIR [file dirname [file dirname [info script]]]

    proc LOAD_SCRIPT {script {subdir myriad2}} {
        if [string equal [string index $script 0] "#"] {
            return
        }
        if {[info exists ::mdbg::verbose] && $mdbg::verbose} {
            puts $mdbg::Channels::verbose "loading $subdir/$script"
        }
        variable SCRIPT_DIR
        set fileName [file join $SCRIPT_DIR $subdir $script]
        if [file isfile $fileName] {
            uplevel 1 namespace inscope :: source $fileName
        } else {
            puts $mdbg::Channels::warning "warning: $fileName not found"
        }
    }

    foreach script {
        fragrakRegisters
        registersM2
        leonDefs
        board
        coreRegisters
        uartCommon
        showPC
        cprCompat
        cpr
        gpioCommon
        mutexCommon
    } {
        LOAD_SCRIPT $script.tcl
    }
}
