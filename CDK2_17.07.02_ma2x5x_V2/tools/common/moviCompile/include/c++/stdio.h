// -*- C++ -*-
//===---------------------------- stdio.h ---------------------------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is dual licensed under the MIT and the University of Illinois Open
// Source Licenses. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//

#if defined(__need_FILE) || defined(__need___FILE)

#if !defined(_LIBCPP_HAS_NO_PRAGMA_SYSTEM_HEADER)
#pragma GCC system_header
#endif

#include_next <stdio.h>

#elif !defined(_LIBCPP_STDIO_H)
#define _LIBCPP_STDIO_H

/*
    stdio.h synopsis

Macros:

    BUFSIZ
    EOF
    FILENAME_MAX
    FOPEN_MAX
    L_tmpnam
    NULL
    SEEK_CUR
    SEEK_END
    SEEK_SET
    TMP_MAX
    _IOFBF
    _IOLBF
    _IONBF
    stderr
    stdin
    stdout

Types:

FILE
fpos_t
size_t

int remove(const char* filename);
int rename(const char* old, const char* new);
FILE* tmpfile(void);
char* tmpnam(char* s);
int fclose(FILE* stream);
int fflush(FILE* stream);
FILE* fopen(const char* restrict filename, const char* restrict mode);
FILE* freopen(const char* restrict filename, const char * restrict mode,
              FILE * restrict stream);
void setbuf(FILE* restrict stream, char* restrict buf);
int setvbuf(FILE* restrict stream, char* restrict buf, int mode, size_t size);
int fprintf(FILE* restrict stream, const char* restrict format, ...);
int fscanf(FILE* restrict stream, const char * restrict format, ...);
int printf(const char* restrict format, ...);
int scanf(const char* restrict format, ...);
int snprintf(char* restrict s, size_t n, const char* restrict format, ...);    // C99
int sprintf(char* restrict s, const char* restrict format, ...);
int sscanf(const char* restrict s, const char* restrict format, ...);
int vfprintf(FILE* restrict stream, const char* restrict format, va_list arg);
int vfscanf(FILE* restrict stream, const char* restrict format, va_list arg);  // C99
int vprintf(const char* restrict format, va_list arg);
int vscanf(const char* restrict format, va_list arg);                          // C99
int vsnprintf(char* restrict s, size_t n, const char* restrict format,         // C99
              va_list arg);
int vsprintf(char* restrict s, const char* restrict format, va_list arg);
int vsscanf(const char* restrict s, const char* restrict format, va_list arg); // C99
int fgetc(FILE* stream);
char* fgets(char* restrict s, int n, FILE* restrict stream);
int fputc(int c, FILE* stream);
int fputs(const char* restrict s, FILE* restrict stream);
int getc(FILE* stream);
int getchar(void);
char* gets(char* s);  // removed in C++14
int putc(int c, FILE* stream);
int putchar(int c);
int puts(const char* s);
int ungetc(int c, FILE* stream);
size_t fread(void* restrict ptr, size_t size, size_t nmemb,
             FILE* restrict stream);
size_t fwrite(const void* restrict ptr, size_t size, size_t nmemb,
              FILE* restrict stream);
int fgetpos(FILE* restrict stream, fpos_t* restrict pos);
int fseek(FILE* stream, long offset, int whence);
int fsetpos(FILE*stream, const fpos_t* pos);
long ftell(FILE* stream);
void rewind(FILE* stream);
void clearerr(FILE* stream);
int feof(FILE* stream);
int ferror(FILE* stream);
void perror(const char* s);
*/

#include <__config>

#if !defined(_LIBCPP_HAS_NO_PRAGMA_SYSTEM_HEADER)
#pragma GCC system_header
#endif

#include_next <stdio.h>

#if defined(__cplusplus)

// snprintf
#if defined(_LIBCPP_MSVCRT)
extern "C++" {
#include "support/win32/support.h"
}
#endif

// getc

#ifdef getc

extern "C++" inline _LIBCPP_INLINE_VISIBILITY
int __libcpp_getc(FILE *__lcpp_x) _NOEXCEPT {
  return getc(__lcpp_x);
}

#undef getc

extern "C" inline _LIBCPP_INLINE_VISIBILITY
int getc(FILE *__lcpp_x) _NOEXCEPT {
  return __libcpp_getc(__lcpp_x);
}

#endif // getc

// putc

#ifdef putc

extern "C++" inline _LIBCPP_INLINE_VISIBILITY
int __libcpp_putc(int __lcpp_x, FILE *__lcpp_y) _NOEXCEPT {
  return putc(__lcpp_x, __lcpp_y);
}

#undef putc

extern "C" inline _LIBCPP_INLINE_VISIBILITY
int putc(int __lcpp_x, FILE *__lcpp_y) _NOEXCEPT {
  return __libcpp_putc(__lcpp_x, __lcpp_y);
}

#endif // putc

// clearerr

#ifdef clearerr

extern "C++" inline _LIBCPP_INLINE_VISIBILITY
void __libcpp_clearerr(FILE *__lcpp_x) _NOEXCEPT {
  clearerr(__lcpp_x);
}

#undef clearerr

extern "C" inline _LIBCPP_INLINE_VISIBILITY
void clearerr(FILE *__lcpp_x) _NOEXCEPT {
  __libcpp_clearerr(__lcpp_x);
}

#endif // clearerr

// feof

#ifdef feof

extern "C++" inline _LIBCPP_INLINE_VISIBILITY
int __libcpp_feof(FILE *__lcpp_x) _NOEXCEPT {
  return feof(__lcpp_x);
}

#undef feof

extern "C" inline _LIBCPP_INLINE_VISIBILITY
int feof(FILE *__lcpp_x) _NOEXCEPT {
  return __libcpp_feof(__lcpp_x);
}

#endif // feof

// ferror

#ifdef ferror

extern "C++" inline _LIBCPP_INLINE_VISIBILITY
int __libcpp_ferror(FILE *__lcpp_x) _NOEXCEPT {
  return ferror(__lcpp_x);
}

#undef ferror

extern "C" inline _LIBCPP_INLINE_VISIBILITY
int ferror(FILE *__lcpp_x) _NOEXCEPT {
  return __libcpp_ferror(__lcpp_x);
}

#endif // ferror

#endif // __cplusplus

#endif  // _LIBCPP_STDIO_H
