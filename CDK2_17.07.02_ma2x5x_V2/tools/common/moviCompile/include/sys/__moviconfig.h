/*  ---------------------------------------------------------------------------
 *  Copyright (C) 2016-2017 Movidius Ltd. All rights reserved
 *  ---------------------------------------------------------------------------
 *  File       :   sys/__moviconfig.h
 *  Description:   Set of utility definitions and macros used in the implementation
 *                 of the SHAVE libraries.
 *  --------------------------------------------------------------------------- */

#ifndef _MCC_SYS_MOVICONFIG_H_
#define _MCC_SYS_MOVICONFIG_H_ (1)

#pragma GCC system_header


/* Conversion utilities: */
#define _MV_AS_FLOAT(x)             (__builtin_astype((unsigned int)(x), float))
#define _MV_AS_HALF(x)              (__builtin_astype((unsigned short)(x), __fp16))
#define _MV_AS_UINT(x)              (__builtin_astype((float)(x), unsigned int))
#define _MV_AS_USHORT(x)            (__builtin_astype((__fp16)(x), unsigned short))

#define _MV_UINT64_AS_FP64(x)       (__builtin_astype((unsigned long long)(x), long double))
#define _MV_UINT32_AS_FP32(x)       (__builtin_astype((unsigned int)(x), float))
#define _MV_UINT16_AS_FP16(x)       (__builtin_astype((unsigned short)(x), __fp16))
#define _MV_FP64_AS_UINT64(x)       (__builtin_astype((long double)(x), unsigned long long))
#define _MV_FP32_AS_UINT32(x)       (__builtin_astype((float)(x), unsigned int))
#define _MV_FP16_AS_UINT16(x)       (__builtin_astype((__fp16)(x), unsigned short))

/* Declaration forms: */
#define _MV_EXTERN_DECL             extern
#define _MV_CONST_DECL              extern __attribute__((const))
#define _MV_PURE_DECL               extern __attribute__((__pure__))
#define _MV_NORETURN_DECL           extern __attribute__((__noreturn__))

#define _MV_ENTRYPOINT_DEFN         __attribute__((__dllexport__,__no_instrument_function__))
#define _MV_NORETURN_DEFN           __attribute__((__noreturn__,__no_instrument_function__))

#ifdef __cplusplus
# define _MV_OVERLOAD_DECL          extern

# define _MV_INLINE_DEFN            inline __attribute__((__always_inline__,__no_instrument_function__))
# define _MV_CONST_INLINE_DEFN      inline __attribute__((const, __always_inline__,__no_instrument_function__))
# define _MV_PURE_INLINE_DEFN       inline __attribute__((__pure__, __always_inline__,__no_instrument_function__))
# define _MV_OVERLOAD_INLINE_DEFN   inline __attribute__((__always_inline__,__no_instrument_function__))

# define _MV_EXTERNC_BEGIN          extern "C" {
# define _MV_EXTERNC_END            }

# define _MV_UNUSED(x)
#else /* __cplusplus */
# define _MV_OVERLOAD_DECL          extern __attribute__((__overloadable__))

# define _MV_INLINE_DEFN            __inline __attribute__((__always_inline__,__no_instrument_function__))
# define _MV_CONST_INLINE_DEFN      __inline __attribute__((const, __always_inline__,__no_instrument_function__))
# define _MV_PURE_INLINE_DEFN       __inline __attribute__((__pure__, __always_inline__,__no_instrument_function__))
# define _MV_OVERLOAD_INLINE_DEFN   __inline __attribute__((__always_inline__, __overloadable__,__no_instrument_function__))

# define _MV_EXTERNC_BEGIN
# define _MV_EXTERNC_END

# define _MV_UNUSED(x)              x __attribute__((__unused__))
#endif /* __cplusplus */


#endif /* _MCC_SYS_MOVICONFIG_H_ */
