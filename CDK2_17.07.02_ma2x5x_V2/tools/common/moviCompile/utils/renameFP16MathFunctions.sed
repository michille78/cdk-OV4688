#  ---------------------------------------------------------------------------
#  Copyright (C) 2016 Movidius Ltd. All rights reserved
#  ---------------------------------------------------------------------------
#  File       :   renameFP16MathFunctions.sed
#  Description:   This script will rename the 16-bit ISO C math functions that
#                 were introduced with 'moviCompile' v00.50.76.1 to the new
#                 names used in the subsequent releases.
#
#                 Previously the FP16 math functions followed the name of the
#                 corresponding ISO C math 'double' function, but were prefixed
#                 with '__h'.
#
#                 The names have been revised to better follow the ISO C naming
#                 convention, and are now prefixed by '__' and are suffixed by
#                 's' for 'short float', which has the same type as '__fp16'.
#  ---------------------------------------------------------------------------


# Map '__hacos' to '__acoss'
s/\([^_a-zA-Z0-9]\)__hacos\([^_a-zA-Z0-9]\)/\1__acoss\2/g

# Map '__hasin' to '__asins'
s/\([^_a-zA-Z0-9]\)__hasin\([^_a-zA-Z0-9]\)/\1__asins\2/g

# Map '__hatan' to '__atans'
s/\([^_a-zA-Z0-9]\)__hatan\([^_a-zA-Z0-9]\)/\1__atans\2/g

# Map '__hatan2' to '__atan2s'
s/\([^_a-zA-Z0-9]\)__hatan2\([^_a-zA-Z0-9]\)/\1__atan2s\2/g

# Map '__hcos' to '__coss'
s/\([^_a-zA-Z0-9]\)__hcos\([^_a-zA-Z0-9]\)/\1__coss\2/g

# Map '__hsin' to '__sins'
s/\([^_a-zA-Z0-9]\)__hsin\([^_a-zA-Z0-9]\)/\1__sins\2/g

# Map '__htan' to '__tans'
s/\([^_a-zA-Z0-9]\)__htan\([^_a-zA-Z0-9]\)/\1__tans\2/g

# Map '__hacosh' to '__acoshs'
s/\([^_a-zA-Z0-9]\)__hacosh\([^_a-zA-Z0-9]\)/\1__acoshs\2/g

# Map '__hasinh' to '__asinhs'
s/\([^_a-zA-Z0-9]\)__hasinh\([^_a-zA-Z0-9]\)/\1__asinhs\2/g

# Map '__hatanh' to '__atanhs'
s/\([^_a-zA-Z0-9]\)__hatanh\([^_a-zA-Z0-9]\)/\1__atanhs\2/g

# Map '__hcosh' to '__coshs'
s/\([^_a-zA-Z0-9]\)__hcosh\([^_a-zA-Z0-9]\)/\1__coshs\2/g

# Map '__hsinh' to '__sinhs'
s/\([^_a-zA-Z0-9]\)__hsinh\([^_a-zA-Z0-9]\)/\1__sinhs\2/g

# Map '__htanh' to '__tanhs'
s/\([^_a-zA-Z0-9]\)__htanh\([^_a-zA-Z0-9]\)/\1__tanhs\2/g

# Map '__hexp' to '__exps'
s/\([^_a-zA-Z0-9]\)__hexp\([^_a-zA-Z0-9]\)/\1__exps\2/g

# Map '__hexp2' to '__exp2s'
s/\([^_a-zA-Z0-9]\)__hexp2\([^_a-zA-Z0-9]\)/\1__exp2s\2/g

# Map '__hexpm1' to '__expm1s'
s/\([^_a-zA-Z0-9]\)__hexpm1\([^_a-zA-Z0-9]\)/\1__expm1s\2/g

# Map '__hfrexp' to '__frexps'
s/\([^_a-zA-Z0-9]\)__hfrexp\([^_a-zA-Z0-9]\)/\1__frexps\2/g

# Map '__hilogb' to '__ilogbs'
s/\([^_a-zA-Z0-9]\)__hilogb\([^_a-zA-Z0-9]\)/\1__ilogbs\2/g

# Map '__hldexp' to '__ldexps'
s/\([^_a-zA-Z0-9]\)__hldexp\([^_a-zA-Z0-9]\)/\1__ldexps\2/g

# Map '__hlog' to '__logs'
s/\([^_a-zA-Z0-9]\)__hlog\([^_a-zA-Z0-9]\)/\1__logs\2/g

# Map '__hlog10' to '__log10s'
s/\([^_a-zA-Z0-9]\)__hlog10\([^_a-zA-Z0-9]\)/\1__log10s\2/g

# Map '__hlog1p' to '__log1ps'
s/\([^_a-zA-Z0-9]\)__hlog1p\([^_a-zA-Z0-9]\)/\1__log1ps\2/g

# Map '__hlog2' to '__log2s'
s/\([^_a-zA-Z0-9]\)__hlog2\([^_a-zA-Z0-9]\)/\1__log2s\2/g

# Map '__hlogb' to '__logbs'
s/\([^_a-zA-Z0-9]\)__hlogb\([^_a-zA-Z0-9]\)/\1__logbs\2/g

# Map '__hmodf' to '__modfs'
s/\([^_a-zA-Z0-9]\)__hmodf\([^_a-zA-Z0-9]\)/\1__modfs\2/g

# Map '__hscalbn' to '__scalbns'
s/\([^_a-zA-Z0-9]\)__hscalbn\([^_a-zA-Z0-9]\)/\1__scalbns\2/g

# Map '__hscalbln' to '__scalblns'
s/\([^_a-zA-Z0-9]\)__hscalbln\([^_a-zA-Z0-9]\)/\1__scalblns\2/g

# Map '__hcbrt' to '__cbrts'
s/\([^_a-zA-Z0-9]\)__hcbrt\([^_a-zA-Z0-9]\)/\1__cbrts\2/g

# Map '__hfabs' to '__fabss'
s/\([^_a-zA-Z0-9]\)__hfabs\([^_a-zA-Z0-9]\)/\1__fabss\2/g

# Map '__hhypot' to '__hypots'
s/\([^_a-zA-Z0-9]\)__hhypot\([^_a-zA-Z0-9]\)/\1__hypots\2/g

# Map '__hpow' to '__pows'
s/\([^_a-zA-Z0-9]\)__hpow\([^_a-zA-Z0-9]\)/\1__pows\2/g

# Map '__hsqrt' to '__sqrts'
s/\([^_a-zA-Z0-9]\)__hsqrt\([^_a-zA-Z0-9]\)/\1__sqrts\2/g

# Map '__herf' to '__erfs'
s/\([^_a-zA-Z0-9]\)__herf\([^_a-zA-Z0-9]\)/\1__erfs\2/g

# Map '__herfc' to '__erfcs'
s/\([^_a-zA-Z0-9]\)__herfc\([^_a-zA-Z0-9]\)/\1__erfcs\2/g

# Map '__hlgamma' to '__lgammas'
s/\([^_a-zA-Z0-9]\)__hlgamma\([^_a-zA-Z0-9]\)/\1__lgammas\2/g

# Map '__htgamma' to '__tgammas'
s/\([^_a-zA-Z0-9]\)__htgamma\([^_a-zA-Z0-9]\)/\1__tgammas\2/g

# Map '__hceil' to '__ceils'
s/\([^_a-zA-Z0-9]\)__hceil\([^_a-zA-Z0-9]\)/\1__ceils\2/g

# Map '__hfloor' to '__floors'
s/\([^_a-zA-Z0-9]\)__hfloor\([^_a-zA-Z0-9]\)/\1__floors\2/g

# Map '__hnearbyint' to '__nearbyints'
s/\([^_a-zA-Z0-9]\)__hnearbyint\([^_a-zA-Z0-9]\)/\1__nearbyints\2/g

# Map '__hrint' to '__rints'
s/\([^_a-zA-Z0-9]\)__hrint\([^_a-zA-Z0-9]\)/\1__rints\2/g

# Map '__hlrint' to '__lrints'
s/\([^_a-zA-Z0-9]\)__hlrint\([^_a-zA-Z0-9]\)/\1__lrints\2/g

# Map '__hllrint' to '__llrints'
s/\([^_a-zA-Z0-9]\)__hllrint\([^_a-zA-Z0-9]\)/\1__llrints\2/g

# Map '__hround' to '__rounds'
s/\([^_a-zA-Z0-9]\)__hround\([^_a-zA-Z0-9]\)/\1__rounds\2/g

# Map '__hlround' to '__lrounds'
s/\([^_a-zA-Z0-9]\)__hlround\([^_a-zA-Z0-9]\)/\1__lrounds\2/g

# Map '__hllround' to '__llrounds'
s/\([^_a-zA-Z0-9]\)__hllround\([^_a-zA-Z0-9]\)/\1__llrounds\2/g

# Map '__htrunc' to '__truncs'
s/\([^_a-zA-Z0-9]\)__htrunc\([^_a-zA-Z0-9]\)/\1__truncs\2/g

# Map '__hfmod' to '__fmods'
s/\([^_a-zA-Z0-9]\)__hfmod\([^_a-zA-Z0-9]\)/\1__fmods\2/g

# Map '__hremainder' to '__remainders'
s/\([^_a-zA-Z0-9]\)__hremainder\([^_a-zA-Z0-9]\)/\1__remainders\2/g

# Map '__hremquo' to '__remquos'
s/\([^_a-zA-Z0-9]\)__hremquo\([^_a-zA-Z0-9]\)/\1__remquos\2/g

# Map '__hcopysign' to '__copysigns'
s/\([^_a-zA-Z0-9]\)__hcopysign\([^_a-zA-Z0-9]\)/\1__copysigns\2/g

# Map '__hnan' to '__nans'
s/\([^_a-zA-Z0-9]\)__hnan\([^_a-zA-Z0-9]\)/\1__nans\2/g

# Map '__hnextafter' to '__nextafters'
s/\([^_a-zA-Z0-9]\)__hnextafter\([^_a-zA-Z0-9]\)/\1__nextafters\2/g

# Map '__hnexttoward' to '__nexttowards'
s/\([^_a-zA-Z0-9]\)__hnexttoward\([^_a-zA-Z0-9]\)/\1__nexttowards\2/g

# Map '__hfdim' to '__fdims'
s/\([^_a-zA-Z0-9]\)__hfdim\([^_a-zA-Z0-9]\)/\1__fdims\2/g

# Map '__hfmax' to '__fmaxs'
s/\([^_a-zA-Z0-9]\)__hfmax\([^_a-zA-Z0-9]\)/\1__fmaxs\2/g

# Map '__hfmin' to '__fmins'
s/\([^_a-zA-Z0-9]\)__hfmin\([^_a-zA-Z0-9]\)/\1__fmins\2/g

# Map '__hfma' to '__fmas'
s/\([^_a-zA-Z0-9]\)__hfma\([^_a-zA-Z0-9]\)/\1__fmas\2/g

# Map '__hclamp' to '__clamps'
s/\([^_a-zA-Z0-9]\)__hclamp\([^_a-zA-Z0-9]\)/\1__clamps\2/g

# Map '__hpowr' to '__powrs'
s/\([^_a-zA-Z0-9]\)__hpowr\([^_a-zA-Z0-9]\)/\1__powrs\2/g
