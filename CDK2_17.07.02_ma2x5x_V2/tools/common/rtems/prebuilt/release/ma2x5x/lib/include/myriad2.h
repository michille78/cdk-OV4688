/*
 *  BASED on LEON3 BSP data types and macros.
 *
 *  COPYRIGHT (c) 1989-1998.
 *  On-Line Applications Research Corporation (OAR).
 *
 *  Modified for LEON3 BSP.
 *  COPYRIGHT (c) 2004.
 *  Gaisler Research.
 *
 *  The license and distribution terms for this file may be
 *  found in the file LICENSE in this distribution or at
 *  http://www.rtems.org/license/LICENSE.
 *
 *  Modified for MYRIAD2 BSP.
 *  COPYRIGHT (c) 2013-2016.
 *  Movidius.
 *
 */

#ifndef _INCLUDE_MYRIAD2_H
#define _INCLUDE_MYRIAD2_H

#include <rtems.h>
#include <rtems/score/sparc.h>
#include <myriad2_registers.h>
#include <reg_utils_defines.h>
#include <icb_defines.h>

#ifdef __cplusplus
extern "C" {
#endif

#define MYRIAD2_INTERRUPT_EXTERNAL_1 5

#ifndef ASM
/*
 *  Trap Types for on-chip peripherals
 *
 *  Source: Table 8 - Interrupt Trap Type and Default Priority Assignments
 *
 *  NOTE: The priority level for each source corresponds to the least
 *        significant nibble of the trap type.
 */

#define MYRIAD2_TRAP_TYPE( _source ) SPARC_ASYNCHRONOUS_TRAP((_source) + 0x10)

#define MYRIAD2_TRAP_SOURCE( _trap ) ((_trap) - 0x10)

#define MYRIAD2_INT_TRAP( _trap ) \
  ( (_trap) >= 0x11 && \
    (_trap) <= 0x1F )

#endif

/*
 *  The following defines the bits in the Timer Control Register.
 */
#define MYRIAD_TIMER_EN         (1<<0)
#define MYRIAD_TIMER_RS         (1<<1)
#define MYRIAD_TIMER_IRQEN      (1<<2)
#define MYRIAD_TIMER_CH         (1<<3)
#define MYRIAD_TIMER_IRQPEND    (1<<4)
#define MYRIAD_TIMER_FR         (1<<5)

/*
 *  The following defines the bits in the MYRIAD2 UART Status Register.
 */

#define D_UART_USR_TFNF  (1<<1) // Transmit FIFO Not Full
#define D_UART_USR_RFNE  (1<<3) // Receive FIFO Not Empty

/*
 *  The following defines the bits in the MYRIAD2 UART Control Register.
 */
#define D_UART_MCR_LB    (1<<4) // LoopBack Bit - Modem control

/* Calculate the ICB byte offset we want to access:
 * source >> 5 is the n-th word in the bit array
 */
#define GET_SOURCE_OFFSET(source) (((source) >> 5) << 2 )

void bsp_debug_uart_init(void);

/*
 *  The following defines the bits in the MYRIAD2 Cache Control Register.
 */

/*
 * To determine which ICB and TIM modules should be used by the processor
 */
extern uint32_t __icb_base__;
extern uint32_t __tim_base__;


#ifndef ASM

void myriad2_power_down_loop(void) RTEMS_NO_RETURN;

/*
 *  Macros to manipulate the Interrupt Clear, Interrupt Force, Interrupt Mask,
 *  and the Interrupt Pending Registers.
 *
 *  NOTE: For operations which are not atomic, this code disables interrupts
 *        to guarantee there are no intervening accesses to the same register.
 *        The operations which read the register, modify the value and then
 *        store the result back are vulnerable.
 */


#define MYRIAD2_Clear_interrupt( _source ) \
  do { \
    uint32_t t = GET_SOURCE_OFFSET(_source); \
    SET_REG_WORD( __icb_base__ + ICB_CLEAR_0_OFFSET + t, 1 << ( (_source) - ( t * 8 ) ) ); \
  } while (0)

#define MYRIAD2_Force_interrupt( _source ) \
  do { \
    uint32_t t = GET_SOURCE_OFFSET(_source); \
    SET_REG_WORD( __icb_base__ + ICB_SETINT_0_OFFSET + t, 1 << ( (_source) - ( t * 8 ) ) ); \
  } while (0)

#define MYRIAD2_Is_interrupt_pending( _source ) \
  ({ \
    uint32_t t = GET_SOURCE_OFFSET(_source); \
    GET_REG_WORD_VAL( __icb_base__ + ICB_PEND_0_OFFSET + t ) & ( 1 << ( (_source) - ( t * 8 ) ) ); \
  })

#define MYRIAD2_Cpu_Is_interrupt_masked( _source, _cpu ) \
  ({ \
    uint32_t t = GET_SOURCE_OFFSET(_source); \
    ( GET_REG_WORD_VAL( __icb_base__ + ICB_ENABLE_0_OFFSET + t ) & ( 1 << ( (_source) - ( t * 8 ) ) ) ) ? 0 : 1; \
  })

#define MYRIAD2_Cpu_Mask_interrupt( _source, _cpu ) \
  do { \
    uint32_t t = GET_SOURCE_OFFSET(_source); \
    uint32_t status; \
    rtems_interrupt_disable(status); \
    SET_REG_WORD( __icb_base__ + ICB_ENABLE_0_OFFSET + t, GET_REG_WORD_VAL( __icb_base__ + ICB_ENABLE_0_OFFSET + t ) & ~( 1 << ( (_source) - ( t * 8 ) ) ) ); \
    rtems_interrupt_enable(status); \
  } while (0)

#define MYRIAD2_Cpu_Unmask_interrupt( _source, _cpu ) \
  do { \
    uint32_t t = GET_SOURCE_OFFSET(_source); \
    uint32_t status; \
    rtems_interrupt_disable(status); \
    SET_REG_WORD( __icb_base__ + ICB_ENABLE_0_OFFSET + t, GET_REG_WORD_VAL( __icb_base__ + ICB_ENABLE_0_OFFSET + t ) | ( 1 << ( (_source) - ( t * 8 ) ) ) ); \
    rtems_interrupt_enable(status); \
  } while (0)

/* Map single-cpu operations to local CPU */
#define MYRIAD2_Is_interrupt_masked( _source )      MYRIAD2_Cpu_Is_interrupt_masked(_source, 0)
#define MYRIAD2_Mask_interrupt(_source)             MYRIAD2_Cpu_Mask_interrupt(_source, 0)
#define MYRIAD2_Unmask_interrupt(_source)           MYRIAD2_Cpu_Unmask_interrupt(_source, 0)

/* Make BSPs have common macros for interrupt handling */
#define BSP_Clear_interrupt(_source)                MYRIAD2_Clear_interrupt(_source)
#define BSP_Force_interrupt(_source)                MYRIAD2_Force_interrupt(_source)
#define BSP_Is_interrupt_pending(_source)           MYRIAD2_Is_interrupt_pending(_source)
#define BSP_Is_interrupt_masked(_source)            MYRIAD2_Is_interrupt_masked(_source)
#define BSP_Unmask_interrupt(_source)               MYRIAD2_Unmask_interrupt(_source)
#define BSP_Mask_interrupt(_source)                 MYRIAD2_Mask_interrupt(_source)

/* Make BSPs have common macros for interrupt handling on any CPU */
#define BSP_Cpu_Is_interrupt_masked(_source, _cpu)  MYRIAD2_Cpu_Is_interrupt_masked(_source, _cpu)
#define BSP_Cpu_Unmask_interrupt(_source, _cpu)     MYRIAD2_Cpu_Unmask_interrupt(_source, _cpu)
#define BSP_Cpu_Mask_interrupt(_source, _cpu)       MYRIAD2_Cpu_Mask_interrupt(_source, _cpu)

/* Load 32-bit word by forcing a cache-miss */
static inline unsigned int leon_r32_no_cache(uintptr_t addr)
{
  unsigned int tmp;
  __asm__ volatile (" lda [%1] 1, %0\n" : "=r"(tmp) : "r"(addr));
  return tmp;
}

/****************** ADDITIONAL Function calls ***************/

/* Set the type of interrupt level or edge ... */
static inline void MYRIAD2_Set_interrupt_type(uint32_t irq, uint32_t type)
{
    uint32_t tmp;
    uint32_t status;
    rtems_interrupt_disable(status);
    tmp = GET_REG_WORD_VAL(__icb_base__ + ICB_CTRL0_OFFSET + ( irq << 2 ));
    SET_REG_WORD(__icb_base__ + ICB_CTRL0_OFFSET + ( irq << 2 ), tmp | type);
    rtems_interrupt_enable(status);
}

/* Set the type of interrupt level or edge ... */
static inline void MYRIAD2_Set_interrupt_priority(uint32_t irq, uint32_t priority)
{
    uint32_t tmp;
    uint32_t status;
    rtems_interrupt_disable(status);
    tmp = GET_REG_WORD_VAL(__icb_base__ + ICB_CTRL0_OFFSET + ( irq << 2 ));
    SET_REG_WORD(__icb_base__ + ICB_CTRL0_OFFSET + ( irq << 2 ), tmp | ( priority << 2));
    rtems_interrupt_enable(status);
}

/* Set the type of interrupt level or edge ... */
static inline void MYRIAD2_Set_interrupt_type_priority(uint32_t irq, uint32_t type, uint32_t priority)
{
    uint32_t status;
    rtems_interrupt_disable(status);
    SET_REG_WORD(__icb_base__ + ICB_CTRL0_OFFSET + ( irq << 2 ), ( priority << 2) | type);
    rtems_interrupt_enable(status);
}

/* Allows to re-route interrupts from the other Leon */
static inline void MYRIAD2_Dynamic_irq_reroute(uint32_t source_irq, uint32_t target_irq, uint32_t enable)
{

    uint32_t c2c_interconfig_offset;
    uint32_t bit_offset;
    uint32_t config_value;
    uint32_t tmp;
    uint32_t status;
    uint32_t remote_icb_base = __icb_base__ == ICB1_BASE_ADR ? ICB0_BASE_ADR : ICB1_BASE_ADR;
    // There are 12 slot configurations divided across 3 registers with 4 configs in each
    c2c_interconfig_offset = (((target_irq - IRQ_DYNAMIC_0 ) >> 2) & 0x3) * 4; // Bits 2,3 define the register WORD offset
    bit_offset = (((target_irq - IRQ_DYNAMIC_0 ) >> 0) & 0x3) * 8; // Bottom 2 bits of the offset define byte within the 32 bit word
    config_value = (enable << 7) | (source_irq & 0x3F) ;
    // Protect from concurrent accesses
    rtems_interrupt_disable(status);
    // Read current config
    tmp = GET_REG_WORD_VAL(remote_icb_base + ICB_C2C_INT_CFG0_OFFSET + c2c_interconfig_offset);
    // Clear the part we are about to modify
    tmp &= ~((uint32_t)(0xFF << bit_offset));
    // Apply changes
    tmp |= (config_value << bit_offset);
    // Write the new value
    SET_REG_WORD(remote_icb_base + ICB_C2C_INT_CFG0_OFFSET + c2c_interconfig_offset ,tmp);
    // Restore system interrupts
    rtems_interrupt_enable(status);
}

// Allows to trigger an IRQ on the remote Leon
static inline void MYRIAD2_Force_remote_interrupt(uint32_t target_irq)
{
    uint32_t t = GET_SOURCE_OFFSET(target_irq);
    uint32_t remote_icb = __icb_base__ == ICB1_BASE_ADR ? ICB0_BASE_ADR : ICB1_BASE_ADR;
    SET_REG_WORD( remote_icb + ICB_SETINT_0_OFFSET + t, 1 << ( target_irq - ( t * 8 ) ) );
}

/* common macros for the additional interrupt handling functions */
#define BSP_Set_interrupt_type(irq, type)                       MYRIAD2_Set_interrupt_type(irq, type)
#define BSP_Set_interrupt_priority(irq, type)                   MYRIAD2_Set_interrupt_priority(irq, type)
#define BSP_Set_interrupt_type_priority(irq, type, priority)    MYRIAD2_Set_interrupt_type_priority(irq, type, priority)
#define BSP_Dynamic_irq_reroute(source_irq, target_irq, enable) MYRIAD2_Dynamic_irq_reroute(source_irq, target_irq, enable)
#define BSP_Force_remote_interrupt(target_irq)                  MYRIAD2_Force_remote_interrupt(target_irq)

#endif /* !ASM */

#ifdef __cplusplus
}
#endif

#endif /* !_INCLUDE_MYRIAD2_H */

